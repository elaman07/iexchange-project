<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            $table->index('profit');
            $table->index('profit_s');
            $table->index('is_main');
            $table->index('bestchange_position');
            $table->index('id_bestchange_rates');
            $table->index('id_crypto_parser');
            $table->index('id_group_commission');
            $table->index('is_error_rate');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            //
        });
    }
};
