<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserves_files', function (Blueprint $table) {
            $table->id();
            $table->integer('id_group')->default(0);
            $table->string('name')->nullable();
            $table->double('amount')->default(0);
            $table->integer('status')->default(0);
            $table->integer('number_format')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserves_files');
    }
};
