<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->index('transfer_to_account');
            $table->index('telegram_id');
        });

        Schema::table('tasks_info', function (Blueprint $table) {
            $table->index('aml_riskscore');
            $table->index('city_name');
            $table->index('country_name');
            $table->index('is_aml_analysis');
            $table->index('newbie');
            $table->index('is_local_scam');
        });

        Schema::table('pay_transaction_hash', function (Blueprint $table) {
            $table->index('transaction_hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
        });
    }
};
