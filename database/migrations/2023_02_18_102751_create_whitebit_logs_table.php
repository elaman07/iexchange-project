<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whitebit_withdraw', function (Blueprint $table) {
            $table->id();
            $table->string('method_type')->nullable();
            $table->integer('id_task')->default(0);
            $table->string('address')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('ticker')->nullable();
            $table->string('fee')->nullable();
            $table->integer('status')->default(0);
            $table->string('transaction_hash')->nullable();
            $table->string('unique_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whitebit_logs');
    }
};
