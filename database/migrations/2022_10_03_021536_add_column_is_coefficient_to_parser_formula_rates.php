<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parser_formula_rates', function (Blueprint $table) {
            $table->integer('is_coefficient')->default(0);
            $table->string('coefficient_formula')->nullable();
            $table->string('coefficient_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parser_formula_rates', function (Blueprint $table) {
            //
        });
    }
};
