<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('code_currency', function (Blueprint $table) {
            $table->integer('id_parser_formula')->default(0);
            $table->string('add_to_course_formula')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('code_currencies', function (Blueprint $table) {
            //
        });
    }
};
