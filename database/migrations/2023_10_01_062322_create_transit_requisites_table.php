<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transit_requisites', function (Blueprint $table) {
            $table->id();
            $table->integer('id_currency')->default(0);
            $table->string('account')->nullable();
            $table->integer('status')->default(0);
            $table->integer('view')->default(0);
            $table->double('received')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transit_requisites');
    }
};
