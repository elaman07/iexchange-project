<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks_requisites', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->string('account_number')->nullable();
            $table->integer('id_task')->default(0);
            $table->integer('id_direction_exchange')->default(0);
            $table->integer('id_currency')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks_requisites');
    }
};
