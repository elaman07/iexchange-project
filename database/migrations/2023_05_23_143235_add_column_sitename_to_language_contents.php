<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('language_contents', function (Blueprint $table) {
            $table->text('sitename')->nullable();
            $table->longText('sitename_desc')->nullable();
            $table->longText('description')->nullable();
            $table->longText('keywords')->nullable();
            $table->text('username_new_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('language_contents', function (Blueprint $table) {
            //
        });
    }
};
