<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('give_price_default')->default(0);
            $table->string('give_price_with_comm')->default(0);
            $table->string('give_price_with_comm_pay')->default(0);
            $table->string('give_price_fee_comm')->default(0);
            $table->string('give_price_fee_pay')->default(0);
            $table->string('give_price_reserve')->default(0);


            $table->string('receiving_price_default')->default(0);
            $table->string('receiving_price_with_comm')->default(0);
            $table->string('receiving_price_with_comm_pay')->default(0);
            $table->string('receiving_price_fee_comm')->default(0);
            $table->string('receiving_price_fee_pay')->default(0);
            $table->string('receiving_price_reserve')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
        });
    }
};
