<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directions_has_requisites', function (Blueprint $table) {
            $table->id();
            $table->integer('model_id');
            $table->string('model_type');
            $table->bigInteger('direction_requisite_id')->unsigned();
            $table->foreign('direction_requisite_id')->references('id')->on('direction_requisites')->onDelete('cascade');
            $table->foreign('model_id')->references('id')->on('direction_exchange');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directions_has_requisites');
    }
};
