<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIsMassPayoutsGatewaysPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gateways_payments', function (Blueprint $table) {
            $table->integer('is_mass_payouts')->default(0)->comment('Статус массовых выплат');
            $table->string('mass_coins')->default(0)->comment('Валюты, используемые для массовых выплат');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gateways_payments', function (Blueprint $table) {
            //
        });
    }
}
