<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->index('email');
            $table->index('auto_withdrawal');
            $table->index('provider');
            $table->index('last_activity_at');
            $table->index('is_unique_user');
            $table->index('email_verified_at');
            $table->index('deactivation');
            $table->index('is_order');
            $table->index('name');
            $table->index('ip_address');
            $table->index('created_at');
            $table->index('last_login_at');
            $table->index('last_logout_at');
            $table->index('order_num');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('email');
            $table->dropIndex('auto_withdrawal');
            $table->dropIndex('provider');
            $table->dropIndex('last_activity_at');
            $table->dropIndex('is_unique_user');
            $table->dropIndex('email_verified_at');
            $table->dropIndex('deactivation');
            $table->dropIndex('is_order');
            $table->dropIndex('name');
            $table->dropIndex('ip_address');
            $table->dropIndex('created_at');
            $table->dropIndex('last_login_at');
            $table->dropIndex('last_logout_at');
            $table->dropIndex('order_num');
        });
    }
}
