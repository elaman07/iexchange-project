<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('notices_exchange', function (Blueprint $table) {
            $table->dropColumn(['is_fixed', 'title_button', 'title', 'is_marquee', 'text_alt']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('notice_exchange', function (Blueprint $table) {
            //
        });
    }
};
