<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notices_exchange', function (Blueprint $table) {
            $table->string('icon_notice')->nullable();
            $table->string('text_color')->nullable();
            $table->string('bg_color')->nullable();
            $table->string('text_size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notices_exchange', function (Blueprint $table) {
            //
        });
    }
};
