<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_transaction_hash', function (Blueprint $table) {
            $table->id();
            $table->integer('id_task')->default(0);
            $table->integer('api_transfer_id')->default(0);
            $table->integer('id_currency')->default(0);
            $table->string('transaction_hash')->nullable();
            $table->string('provider')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_transaction_hash');
    }
};
