<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('language_contents', function (Blueprint $table) {
            $table->longText('referral_system_text')->nullable();
            $table->longText('referral_system_text_footer')->nullable();
            $table->longText('cashback_text')->nullable();
            $table->longText('cashback_footer')->nullable();
            $table->longText('monitoring_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('language_contents', function (Blueprint $table) {
            //
        });
    }
};
