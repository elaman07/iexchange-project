<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser_formula_rates', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable();
            $table->string('exchange_in')->nullable();
            $table->string('exchange_out')->nullable();
            $table->string('formula')->nullable();
            $table->integer('value')->default(0);
            $table->string('summa')->nullable();
            $table->integer('status')->default(0);
            $table->integer('number_format')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parser_formula_rates');
    }
};
