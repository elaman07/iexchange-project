<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('gateways_merchants', function (Blueprint $table) {
            $table->integer('pay_amount')->default(0);
            $table->integer('credit_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('gateways_merchants', function (Blueprint $table) {
            //
        });
    }
};
