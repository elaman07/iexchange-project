<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('currencies', function (Blueprint $table)
        {
            $table->dropIndex(['is_getblockbot_tx_in']);
            $table->dropIndex(['is_amlbot_tx_in']);
            $table->dropColumn('aml_service_name');
            $table->dropColumn('is_getblockbot_in');
            $table->dropColumn('is_getblockbot_out');
            $table->dropColumn('is_getblockbot_tx_in');
            $table->dropColumn('getblockbot_tx_in_amount');
            $table->dropColumn('amlbot_tx_in_amount');
            $table->dropColumn('is_amlbot_tx_in');
            $table->dropColumn('is_amlbot_in');
            $table->dropColumn('is_amlbot_out');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('currencies', function (Blueprint $table) {
            //
        });
    }
};
