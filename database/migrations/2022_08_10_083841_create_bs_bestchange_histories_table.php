<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bestchange_bl_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('bs_id')->default(0);
            $table->string('type')->nullable();
            $table->string('date')->nullable();
            $table->string('author')->nullable();
            $table->text('contacts')->nullable();
            $table->longText('desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bs_bestchange_histories');
    }
};
