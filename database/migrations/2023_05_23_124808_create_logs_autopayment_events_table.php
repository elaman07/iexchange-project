<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('logs_autopayment_events', function (Blueprint $table) {
            $table->id();
            $table->string('provider')->nullable();
            $table->integer('id_order')->default(0);
            $table->string('ip_address')->nullable();
            $table->string('url')->nullable();
            $table->json('headers')->nullable();
            $table->json('content')->nullable();
            $table->json('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('logs_autopayment_events');
    }
};
