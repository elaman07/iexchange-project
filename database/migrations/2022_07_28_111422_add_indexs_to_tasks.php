<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexsToTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->index('status');
            $table->index('id_user');
            $table->index('ip');
            $table->index('give_price');
            $table->index('receiving_price');
            $table->index('id_direction_exchange');
            $table->index('from_shot');
            $table->index('to_shot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropIndex('status');
            $table->dropIndex('id_user');
            $table->dropIndex('ip');
            $table->dropIndex('give_price');
            $table->dropIndex('receiving_price');
            $table->dropIndex('id_direction_exchange');
            $table->dropIndex('from_shot');
            $table->dropIndex('to_shot');
        });
    }
}
