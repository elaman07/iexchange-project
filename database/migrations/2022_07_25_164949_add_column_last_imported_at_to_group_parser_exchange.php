<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLastImportedAtToGroupParserExchange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_parser_exchange', function (Blueprint $table) {
            $table->timestamp('last_imported_at')->nullable();
            $table->integer('is_import_rates')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_parser_exchange', function (Blueprint $table) {
            //
        });
    }
}
