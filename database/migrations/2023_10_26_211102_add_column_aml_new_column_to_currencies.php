<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->string('aml_service')->nullable();
            $table->integer('is_in_aml_check_wallet')->default(0);
            $table->integer('is_out_aml_check_wallet')->default(0);
            $table->integer('is_in_aml_check_tx')->default(0);
            $table->string('in_aml_tx_amount')->nullable();

            $table->index(['is_in_aml_check_tx']);
            $table->index(['aml_service']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('currencies', function (Blueprint $table) {
            //
        });
    }
};
