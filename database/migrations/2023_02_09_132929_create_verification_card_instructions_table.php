<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verification_card_instructions', function (Blueprint $table) {
            $table->id();
            $table->integer('id_category')->default(0);
            $table->text('name')->nullable();
            $table->longText('text')->nullable();
            $table->longText('notice_text')->nullable();
            $table->integer('status')->default(0);
            $table->integer('sorting')->default(0);
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verification_card_instructions');
    }
};
