<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('logs_autopayment_orders_events', function (Blueprint $table) {
            $table->id();
            $table->string('provider')->nullable();
            $table->string('id_order')->nullable();
            $table->string('url')->nullable();
            $table->string('headers')->nullable();
            $table->text('content')->nullable();
            $table->longText('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('logs_autopayment_orders_events');
    }
};
