<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerParserRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_parser_rates', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('exchange_in')->nullable();
            $table->string('exchange_out')->nullable();
            $table->integer('id_group')->default(0);
            $table->double('rate')->default(0);
            $table->string('partner_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_parser_rates');
    }
}
