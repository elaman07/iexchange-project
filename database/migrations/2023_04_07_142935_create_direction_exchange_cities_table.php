<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direction_exchange_cities', function (Blueprint $table) {
            $table->id();
            $table->integer('id_direction_exchange')->default(0);
            $table->integer('city_id')->default(0);
            $table->double('add_comm')->default(0);
            $table->string('param')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direction_exchange_cities');
    }
};
