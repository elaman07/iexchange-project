<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('banners_buttons', function (Blueprint $table) {
            $table->string('color_text_button')->nullable();
            $table->string('color_bg_button')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('banners_buttons', function (Blueprint $table) {
            //
        });
    }
};
