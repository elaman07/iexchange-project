<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('direction_exchange_percent_amount', function (Blueprint $table) {
            $table->id();
            $table->integer('id_direction_exchange')->default(0);
            $table->string('percentage')->default(0);
            $table->string('amount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('direction_exchange_exchange_amount');
    }
};
