<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            $table->string('pay_comm_percent')->default(0);
            $table->string('pay_comm_currency')->default(0);
            $table->string('pay_comm2_percent')->default(0);
            $table->string('pay_comm2_currency')->default(0);
            $table->string('pay_min_comm')->default(0);
            $table->string('pay_min2_comm')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            //
        });
    }
};
