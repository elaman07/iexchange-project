<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->index('id_payment');
            $table->index('id_code_currency');
            $table->index('designation_xml');
            $table->index('status');
            $table->index('id_pay');
            $table->index('id_group');
            $table->index('is_user_verification');
            $table->index('is_amlbot_tx_in');
            $table->index('network_code');
            $table->index('is_getblockbot_tx_in');
            $table->index('id_filter_currency');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('currencies', function (Blueprint $table) {
            //
        });
    }
};
