<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direction_requisites', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('account_number')->nullable();
            $table->integer('view')->default(0);
            $table->float('limit_day')->default(0);
            $table->float('limit_month')->default(0);
            $table->integer('limit_views')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direction_requisites');
    }
};
