<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLastOrderAtToDirectionExchange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            $table->dateTime('last_order_at')->nullable();
            $table->bigInteger('last_order_id')->default(0);

            $table->bigInteger('first_order_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            //
        });
    }
}
