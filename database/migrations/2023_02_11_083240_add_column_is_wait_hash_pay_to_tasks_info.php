<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks_info', function (Blueprint $table) {
            $table->integer('is_wait_hash_pay')->default(0)->comment('Включаем когда не выдается hash оплаты сразу');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks_info', function (Blueprint $table) {
            //
        });
    }
};
