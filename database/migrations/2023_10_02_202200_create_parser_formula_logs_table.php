<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('parser_formula_logs', function (Blueprint $table) {
            $table->id();
            $table->integer('id_parser_formula')->default(0);
            $table->string('name')->nullable();
            $table->string('amount')->nullable(0);
            $table->string('formula')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parser_formula_logs');
    }
};
