<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('parser_exchange', function (Blueprint $table) {
            $table->index('name');
            $table->index('id_group');
            $table->index('summa');
            $table->index('status');
            $table->index('summa_default');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('parser_exchange', function (Blueprint $table) {
            //
        });
    }
};
