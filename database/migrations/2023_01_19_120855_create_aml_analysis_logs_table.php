<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aml_analysis_logs', function (Blueprint $table) {
            $table->id();
            $table->string('provider_name')->nullable();
            $table->integer('id_task')->default(0);
            $table->string('tx_id')->nullable();
            $table->string('address')->nullable();
            $table->string('code_name')->nullable();
            $table->string('riskscore')->nullable();
            $table->string('status')->nullable();
            $table->string('event_type')->nullable();
            $table->longText('json_value')->nullable();
            $table->text('event_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aml_analysis_logs');
    }
};
