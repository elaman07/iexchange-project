<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_single_log_confirm', function (Blueprint $table) {
            $table->id();
            $table->integer('id_task')->default(0);
            $table->integer('needed_confirm')->default(0);
            $table->integer('received_confirm')->default(0);
            $table->string('transaction_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_single_log_confirm');
    }
};
