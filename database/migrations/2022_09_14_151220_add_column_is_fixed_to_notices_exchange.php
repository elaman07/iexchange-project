<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notices_exchange', function (Blueprint $table) {
            $table->integer('is_fixed')->default(0);
            $table->longText('text')->change();
            $table->text('title')->nullable();
            $table->text('title_button')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notices_exchange', function (Blueprint $table) {
            //
        });
    }
};
