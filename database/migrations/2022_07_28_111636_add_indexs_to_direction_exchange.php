<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexsToDirectionExchange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            $table->index('id_currency1');
            $table->index('id_currency2');
            $table->index('id_group_direction');
            $table->index('status');
            $table->index('enable_bestchange');
            $table->index('is_holding_direction');
            $table->index('is_not_bonus');
            $table->index('is_not_partner');
            $table->index('allow_export');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('direction_exchange', function (Blueprint $table) {
            $table->dropIndex('id_currency1');
            $table->dropIndex('id_currency2');
            $table->dropIndex('id_group_direction');
            $table->dropIndex('status');
            $table->dropIndex('enable_bestchange');
            $table->dropIndex('is_holding_direction');
            $table->dropIndex('is_not_bonus');
            $table->dropIndex('is_not_partner');
            $table->dropIndex('allow_export');
        });
    }
}
