<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('language_contents', function (Blueprint $table) {
            $table->longText('interface_regauth_text1')->nullable();
            $table->longText('interface_regauth_text2')->nullable();
            $table->longText('telegram_bot_name_button')->nullable();
            $table->longText('tech_breach_title')->nullable();
            $table->longText('tech_breach_text')->nullable();
            $table->longText('input_footer_title')->nullable();
            $table->longText('description_footer_text')->nullable();
            $table->longText('welcome_title')->nullable();
            $table->longText('welcome_description')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('language_contents', function (Blueprint $table) {
            //
        });
    }
};
