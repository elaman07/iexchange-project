-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 09 2023 г., 14:43
-- Версия сервера: 5.7.38
-- Версия PHP: 8.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- База данных: `iexbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin_auth_log`
--

CREATE TABLE `admin_auth_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prev_ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_successful` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `admin_desktops`
--

CREATE TABLE `admin_desktops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `columns` int(11) NOT NULL DEFAULT '2',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `flex_num1` int(11) NOT NULL DEFAULT '0',
  `flex_num2` int(11) NOT NULL DEFAULT '0',
  `flex_num3` int(11) NOT NULL DEFAULT '0',
  `flex_nums` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `admin_desktops`
--

INSERT INTO `admin_desktops` (`id`, `id_user`, `name`, `columns`, `sorting`, `created_at`, `updated_at`, `flex_num1`, `flex_num2`, `flex_num3`, `flex_nums`) VALUES
(1, 1, 'Рабочий стол 1', 2, 0, '2023-04-09 11:06:42', '2023-04-09 11:06:42', 50, 50, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `admin_desktop_gadgets`
--

CREATE TABLE `admin_desktop_gadgets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_desktop` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `column_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `hash_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_widget` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `admin_filters_user`
--

CREATE TABLE `admin_filters_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_filter_header` int(11) NOT NULL DEFAULT '0',
  `type_filter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `options` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `section` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `admin_filter_header`
--

CREATE TABLE `admin_filter_header` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_common` int(11) NOT NULL DEFAULT '0',
  `type_filter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `advantage`
--

CREATE TABLE `advantage` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `title` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_target` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `affiliate_settings`
--

CREATE TABLE `affiliate_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `amlbot_histories`
--

CREATE TABLE `amlbot_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `riskscore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asset` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dark_market` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dark_service` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_fraudulent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_high` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_low` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_moderate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_veryhigh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambling` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `illegal_service` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marketplace` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `miner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mixer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p2p_exchange_mlrisk_high` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p2p_exchange_mlrisk_low` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ransom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scam` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stolen_coins` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `aml_analysis_logs`
--

CREATE TABLE `aml_analysis_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `tx_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `riskscore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `json_value` longtext COLLATE utf8mb4_unicode_ci,
  `event_message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `id_currency` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `archive_reports`
--

CREATE TABLE `archive_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hash_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `audits`
--

CREATE TABLE `audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `event` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` bigint(20) UNSIGNED NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci,
  `new_values` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `autosender_payment`
--

CREATE TABLE `autosender_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_order` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `backgrounds`
--

CREATE TABLE `backgrounds` (
  `id` int(10) UNSIGNED NOT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `backgrounds`
--

INSERT INTO `backgrounds` (`id`, `background`, `created_at`, `updated_at`) VALUES
(1, 'aTkqgVfjp0f7YWB.png', '2017-10-05 15:14:24', '2017-10-05 15:14:24'),
(2, 'Qfc09PbeOnQgTcH.png', '2017-10-05 15:22:57', '2017-10-05 15:22:57');

-- --------------------------------------------------------

--
-- Структура таблицы `backup_codes`
--

CREATE TABLE `backup_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `num` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `banned`
--

CREATE TABLE `banned` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `expired_at` datetime DEFAULT NULL,
  `filter_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `banned_user`
--

CREATE TABLE `banned_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_task` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bans`
--

CREATE TABLE `bans` (
  `id` int(10) UNSIGNED NOT NULL,
  `bannable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bannable_id` bigint(20) UNSIGNED NOT NULL,
  `created_by_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `expired_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bestchange_bl_histories`
--

CREATE TABLE `bestchange_bl_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bs_id` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts` text COLLATE utf8mb4_unicode_ci,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bestchange_currencies`
--

CREATE TABLE `bestchange_currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bestchange_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bestchange_data_log`
--

CREATE TABLE `bestchange_data_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_log` int(11) NOT NULL DEFAULT '0',
  `where_from` int(11) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bestchange_parser_error`
--

CREATE TABLE `bestchange_parser_error` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_bestchange` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bestchange_rates`
--

CREATE TABLE `bestchange_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchanger_id1` int(11) NOT NULL,
  `exchanger_id2` int(11) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `number_format` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_not_pair` int(11) NOT NULL DEFAULT '0',
  `source_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `bestchange_rates_log`
--

CREATE TABLE `bestchange_rates_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_bestchange_rates` int(11) NOT NULL DEFAULT '0',
  `in_price` double NOT NULL DEFAULT '0',
  `out_price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `blacklist_order`
--

CREATE TABLE `blacklist_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_bestchange` int(11) NOT NULL DEFAULT '0',
  `is_iex` int(11) NOT NULL DEFAULT '0',
  `hash_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cashback_error_log`
--

CREATE TABLE `cashback_error_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `in_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `checks`
--

CREATE TABLE `checks` (
  `id` int(10) UNSIGNED NOT NULL,
  `host_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `last_run_message` text COLLATE utf8mb4_unicode_ci,
  `last_run_output` json DEFAULT NULL,
  `last_ran_at` timestamp NULL DEFAULT NULL,
  `next_run_in_minutes` int(11) DEFAULT NULL,
  `started_throttling_failing_notifications_at` timestamp NULL DEFAULT NULL,
  `custom_properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `designation_xml` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_user_id` int(11) NOT NULL DEFAULT '0',
  `updated_user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `code_currency`
--

CREATE TABLE `code_currency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sign` varchar(100) NOT NULL,
  `id_parser_exchange` int(11) NOT NULL DEFAULT '0',
  `commission` double NOT NULL DEFAULT '0',
  `is_trashed` int(11) NOT NULL DEFAULT '0',
  `internal_rate` varchar(191) NOT NULL DEFAULT '0',
  `add_to_course` varchar(191) NOT NULL DEFAULT '0',
  `is_import` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `collaboration_pr`
--

CREATE TABLE `collaboration_pr` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_button` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `competitor_links`
--

CREATE TABLE `competitor_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `competitor_rates`
--

CREATE TABLE `competitor_rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_competitor` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  `summa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_format` int(11) NOT NULL DEFAULT '0',
  `exchange_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `competitor_rates_log`
--

CREATE TABLE `competitor_rates_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_competitors` int(11) NOT NULL DEFAULT '0',
  `in_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `value` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `block_size` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `is_home` int(11) NOT NULL DEFAULT '0',
  `is_button` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `contests`
--

CREATE TABLE `contests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_limit_user` int(11) NOT NULL DEFAULT '0',
  `is_manual_bank` int(11) NOT NULL DEFAULT '0',
  `bank_base` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `bank` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_code_currency` int(11) NOT NULL DEFAULT '0',
  `code_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_sign` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `percent` double(8,2) NOT NULL DEFAULT '0.00',
  `title` text COLLATE utf8mb4_unicode_ci,
  `subtitle` text COLLATE utf8mb4_unicode_ci,
  `button_name` text COLLATE utf8mb4_unicode_ci,
  `subtitle_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `contests_conditions`
--

CREATE TABLE `contests_conditions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `contests_faq`
--

CREATE TABLE `contests_faq` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `contests_has_contests_users`
--

CREATE TABLE `contests_has_contests_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contests_user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `contests_users`
--

CREATE TABLE `contests_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_monitoring` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bonus` double(8,2) NOT NULL DEFAULT '0.00',
  `id_contest` int(11) NOT NULL DEFAULT '0',
  `code_sign_bonus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_name_bonus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `course_logs`
--

CREATE TABLE `course_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `old_course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `who` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cron`
--

CREATE TABLE `cron` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_group` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `interval` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cron`
--

INSERT INTO `cron` (`id`, `id_group`, `name`, `interval`, `created_at`, `updated_at`) VALUES
(1, 3, 'Парсер курсов', '10', '2017-10-08 21:00:00', '2017-10-08 21:00:00'),
(2, 1, 'Удаление неоплаченных заявок', '0', '2017-10-08 21:00:00', '2017-10-09 17:52:21'),
(3, 6, 'Документ(XML/TXT/JSON) актуальных курсов ', NULL, NULL, NULL),
(4, 5, 'Проверка на создание внутреннего баланса', NULL, NULL, NULL),
(5, 4, 'Контроль над реферальной системой', NULL, NULL, NULL),
(6, 6, 'Парсер курсов (BestChange)', '0', '2017-11-08 00:00:00', '2017-11-08 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `cron_category`
--

CREATE TABLE `cron_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `pos` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plus` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cron_category`
--

INSERT INTO `cron_category` (`id`, `pos`, `name`, `plus`, `created_at`, `updated_at`) VALUES
(1, 1, 'Полночь', 110, '2017-10-08 21:00:00', '2018-02-13 00:00:41'),
(2, 3, 'Интервал 5 минут', 0, '2017-10-08 21:00:00', '2017-10-08 21:00:00'),
(3, 4, 'Интервал 10 минут', 16717, '2017-10-08 21:00:00', '2018-01-22 20:00:07'),
(4, 5, 'Интервал 1 час', 210719, '2017-10-08 21:00:00', '2018-01-22 20:08:08'),
(5, 6, 'Интервал 1 день', 2864, '2017-10-08 21:00:00', '2018-02-13 21:00:42'),
(6, 2, 'Интервал 1 минута', 210711, '2017-11-08 00:00:00', '2018-01-24 14:15:02');

-- --------------------------------------------------------

--
-- Структура таблицы `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `id_payment` int(11) NOT NULL DEFAULT '0' COMMENT 'ID Платежной системы',
  `id_code_currency` int(11) NOT NULL DEFAULT '0' COMMENT 'ID Кода валют',
  `id_filter_currency` int(11) NOT NULL DEFAULT '0',
  `designation_xml` varchar(191) DEFAULT NULL COMMENT 'Обозначение для XML',
  `convert_by` double NOT NULL DEFAULT '1' COMMENT 'Конвертировать по',
  `number_format` int(11) NOT NULL DEFAULT '4' COMMENT 'Знаков, после запятой',
  `day_limit_give` double NOT NULL DEFAULT '0' COMMENT 'Дневной лимит для Отдаю',
  `day_limit_receive` double NOT NULL DEFAULT '0' COMMENT 'Дневной лимит для Получаю',
  `min_char` int(11) NOT NULL DEFAULT '0' COMMENT 'Мин. кол-во символов',
  `max_char` int(11) NOT NULL DEFAULT '0' COMMENT 'Мак. кол-во символов',
  `field_name_from` text COMMENT 'Первые символы',
  `placeholder` varchar(255) NOT NULL,
  `allowed_char` int(11) NOT NULL DEFAULT '0' COMMENT 'Разрешенные символы',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Статус',
  `visible_give` int(11) NOT NULL DEFAULT '0',
  `visible_receiving` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `visible` int(11) NOT NULL DEFAULT '0',
  `visible_code_currency` int(11) NOT NULL DEFAULT '0',
  `text_color` varchar(191) DEFAULT NULL,
  `background_color` varchar(191) DEFAULT NULL,
  `char_default` varchar(191) DEFAULT NULL,
  `mask_input` varchar(191) DEFAULT NULL,
  `unlimited_reserve` int(11) NOT NULL DEFAULT '0',
  `sorting_1` int(11) NOT NULL DEFAULT '0',
  `id_pay` int(11) NOT NULL DEFAULT '0' COMMENT 'Выплаты',
  `is_valid_account` varchar(100) DEFAULT NULL,
  `is_qrcode` int(11) DEFAULT '0',
  `prefix_qrcode` varchar(191) DEFAULT NULL,
  `is_payment_default` int(11) NOT NULL DEFAULT '0',
  `is_payment_unique` int(11) NOT NULL DEFAULT '0',
  `month_limit_in` double(8,2) NOT NULL DEFAULT '0.00',
  `month_limit_out` double(8,2) NOT NULL DEFAULT '0.00',
  `account_number_field` text,
  `notice_in` text,
  `transfer_percent_reserve` double NOT NULL DEFAULT '0',
  `transfer_amount_reserve` double NOT NULL DEFAULT '0',
  `remove_spaces_requisite` tinyint(1) NOT NULL DEFAULT '0',
  `payout_commission` double NOT NULL DEFAULT '0',
  `sorting_reserve` int(11) NOT NULL DEFAULT '0',
  `is_archive` int(11) NOT NULL DEFAULT '0',
  `id_group` int(11) NOT NULL DEFAULT '0',
  `is_hold` int(11) NOT NULL DEFAULT '0',
  `is_qrcode_amount` int(11) NOT NULL DEFAULT '0',
  `is_enabled_verification` int(11) NOT NULL DEFAULT '0',
  `min_amount_verification` float NOT NULL DEFAULT '0',
  `hold_in_hours` int(11) NOT NULL DEFAULT '0',
  `is_user_verification` int(11) NOT NULL DEFAULT '0',
  `commission_merchant_percent` double NOT NULL DEFAULT '0',
  `fee_no_verified_merchant` double NOT NULL DEFAULT '0',
  `hold_delay` int(11) NOT NULL DEFAULT '0',
  `payout_commission_amount` double NOT NULL DEFAULT '0',
  `id_auto_reserve` int(11) NOT NULL DEFAULT '0',
  `sorting_tariffs` int(11) NOT NULL DEFAULT '0',
  `min_out_amount_verification` varchar(191) NOT NULL DEFAULT '0',
  `is_out_enabled_verification` int(11) NOT NULL DEFAULT '0',
  `max_limit_in_reserve` varchar(191) DEFAULT NULL,
  `hour_limit_order_pending` int(11) NOT NULL DEFAULT '0',
  `hour_limit_order_process` int(11) NOT NULL DEFAULT '0',
  `profit_percent_reserve` double NOT NULL DEFAULT '0',
  `is_card_detail` int(11) NOT NULL DEFAULT '0',
  `max_display_reserve` varchar(191) DEFAULT NULL,
  `is_email_verification_modal` int(11) NOT NULL DEFAULT '0',
  `commission_merchant_currency` double NOT NULL DEFAULT '0',
  `is_income_banner` int(11) NOT NULL DEFAULT '0',
  `is_auto_check_modal` int(11) NOT NULL DEFAULT '0',
  `commission_payment_currency` double(8,2) NOT NULL DEFAULT '0.00',
  `recount_percent` int(11) NOT NULL DEFAULT '0',
  `is_recount_default` int(11) NOT NULL DEFAULT '0',
  `notice_out` longtext,
  `is_unique_recount_order` int(11) NOT NULL DEFAULT '0',
  `is_enable_auto_recount_order` int(11) NOT NULL DEFAULT '0',
  `unique_recount_percent` double(8,2) NOT NULL DEFAULT '0.00',
  `recount_time_hours` int(11) NOT NULL DEFAULT '0',
  `recount_time_minutes` int(11) NOT NULL DEFAULT '0',
  `desc_exchange` text,
  `is_amlbot_in` int(11) NOT NULL DEFAULT '0',
  `is_amlbot_out` int(11) NOT NULL DEFAULT '0',
  `out_pay_min_amount` varchar(191) NOT NULL DEFAULT '0',
  `out_pay_max_amount` varchar(191) NOT NULL DEFAULT '0',
  `out_pay_day_limit` varchar(191) NOT NULL DEFAULT '0',
  `out_pay_month_limit` varchar(191) NOT NULL DEFAULT '0',
  `is_allow_amount_space` int(11) NOT NULL DEFAULT '0',
  `is_getblockbot_in` int(11) NOT NULL DEFAULT '0',
  `is_getblockbot_out` int(11) NOT NULL DEFAULT '0',
  `created_user_id` int(11) NOT NULL DEFAULT '0',
  `updated_user_id` int(11) NOT NULL DEFAULT '0',
  `is_fire` int(11) NOT NULL DEFAULT '0',
  `field_name_to` text,
  `field_comment_from` text,
  `field_comment_to` text,
  `tech_currency_name` varchar(191) DEFAULT NULL,
  `button_create_order` text,
  `button_create_order_text` longtext,
  `is_getblockbot_tx_in` int(11) NOT NULL DEFAULT '0',
  `kyc_enabled` int(11) NOT NULL DEFAULT '0',
  `formalization_text` longtext,
  `is_aml_check_cost_reserve` int(11) NOT NULL DEFAULT '0',
  `aml_check_cost` double NOT NULL DEFAULT '0',
  `network_code` varchar(191) DEFAULT NULL,
  `aml_text_in` longtext,
  `aml_text_out` longtext,
  `aml_analyses_count` int(11) NOT NULL DEFAULT '0',
  `aml_analyses_price` double(8,2) NOT NULL DEFAULT '0.00',
  `aml_day_limit_count` int(11) NOT NULL DEFAULT '0',
  `getblockbot_tx_in_amount` double NOT NULL DEFAULT '0',
  `amlbot_tx_in_amount` double NOT NULL DEFAULT '0',
  `is_amlbot_tx_in` int(11) NOT NULL DEFAULT '0',
  `aml_service_name` varchar(191) DEFAULT NULL,
  `instruction_exchange` longtext,
  `blockchain_network_congestion` int(11) NOT NULL DEFAULT '0',
  `is_kyc_checkbox` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_analytics`
--

CREATE TABLE `currencies_analytics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `in_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_count_exchange` int(11) NOT NULL DEFAULT '0',
  `out_count_exchange` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `in_order_count` int(11) NOT NULL DEFAULT '0',
  `out_order_count` int(11) NOT NULL DEFAULT '0',
  `in_amount_usd` double(8,2) NOT NULL DEFAULT '0.00',
  `out_amount_usd` double(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_commands`
--

CREATE TABLE `currencies_commands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_groups`
--

CREATE TABLE `currencies_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `currencies_groups`
--

INSERT INTO `currencies_groups` (`id`, `name`, `id_user`, `sorting`, `created_at`, `updated_at`) VALUES
(2, 'Общее', 1, 0, '2019-05-31 03:23:51', '2019-05-31 03:23:51');

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_info`
--

CREATE TABLE `currencies_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `aml_analyses_type` int(11) NOT NULL DEFAULT '0',
  `aml_analyses_count` int(11) NOT NULL DEFAULT '0',
  `aml_analyses_price` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_log`
--

CREATE TABLE `currencies_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `from_where` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_networks`
--

CREATE TABLE `currencies_networks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currencies_notification`
--

CREATE TABLE `currencies_notification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_fields`
--

CREATE TABLE `currency_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key_id` varchar(100) DEFAULT NULL,
  `name` text NOT NULL,
  `id_currency` int(11) NOT NULL,
  `when_print` int(11) NOT NULL,
  `min_char` int(11) NOT NULL,
  `max_char` int(11) NOT NULL,
  `first_char` text,
  `obligatory_field` int(11) NOT NULL,
  `checkbox_title` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `field_type` varchar(191) DEFAULT NULL,
  `language_field` int(11) NOT NULL DEFAULT '0',
  `description` text,
  `remove_spaces` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_in_has_fields`
--

CREATE TABLE `currency_in_has_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_merchants`
--

CREATE TABLE `currency_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateway_merchant_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_out_has_fields`
--

CREATE TABLE `currency_out_has_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_requisites_has_fields`
--

CREATE TABLE `currency_requisites_has_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `debtors`
--

CREATE TABLE `debtors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `is_bestchange` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_fields`
--

CREATE TABLE `directions_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `min_char` int(11) NOT NULL DEFAULT '0',
  `max_char` int(11) NOT NULL DEFAULT '0',
  `obligatory_field` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `key_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `remove_spaces` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_allowed_countries`
--

CREATE TABLE `directions_has_allowed_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geo_country_list_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_cities`
--

CREATE TABLE `directions_has_cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_fields`
--

CREATE TABLE `directions_has_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction_field_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_forbidden_countries`
--

CREATE TABLE `directions_has_forbidden_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geo_country_list_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_modes`
--

CREATE TABLE `directions_has_modes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction_exchange_mode_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_networks`
--

CREATE TABLE `directions_has_networks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `network_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `directions_has_requisites`
--

CREATE TABLE `directions_has_requisites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction_requisite_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_day`
--

CREATE TABLE `direction_day` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_direction_exchange` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_exchange`
--

CREATE TABLE `direction_exchange` (
  `id` int(11) NOT NULL,
  `id_currency1` int(11) NOT NULL COMMENT 'Направление 1',
  `id_currency2` int(11) NOT NULL COMMENT 'Направление 2',
  `exchange_rate1` varchar(250) NOT NULL DEFAULT '0' COMMENT 'Курс обмена 1',
  `exchange_rate2` varchar(250) NOT NULL DEFAULT '0' COMMENT 'Курс обмена 2',
  `id_crypto_parser` int(11) NOT NULL DEFAULT '0' COMMENT 'Автокорректировка курса',
  `id_your_exchange` int(11) NOT NULL DEFAULT '0',
  `your_add_course1` double NOT NULL DEFAULT '0',
  `your_add_course2` double NOT NULL DEFAULT '0',
  `id_merchant` int(11) NOT NULL DEFAULT '0',
  `add_course1` double NOT NULL DEFAULT '0' COMMENT 'Прибавление к курсу 1',
  `add_course2` double NOT NULL DEFAULT '0' COMMENT 'Прибавление к курсу 2',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Статус',
  `seo_title` varchar(255) DEFAULT NULL COMMENT 'SEO Заголовок',
  `seo_description` text COMMENT 'SEO Описание',
  `seo_keywords` text COMMENT 'SEO Ключевые слова',
  `deadline` text COMMENT 'Срок выполнения обмена',
  `instructions` longtext COMMENT 'Инструкция по оплате',
  `profit` double NOT NULL DEFAULT '0' COMMENT 'Прибыль',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `auto_conversion` int(11) NOT NULL,
  `min_price1` double NOT NULL DEFAULT '0',
  `min_price2` double NOT NULL DEFAULT '0',
  `max_price1` double NOT NULL DEFAULT '0',
  `max_price2` double NOT NULL DEFAULT '0',
  `commission1` double NOT NULL DEFAULT '0',
  `commission2` double NOT NULL DEFAULT '0',
  `sign` int(11) NOT NULL DEFAULT '0',
  `enable_bestchange` int(11) NOT NULL DEFAULT '0' COMMENT 'Статус парсинга "Bestchange" 0 - Отключен 1 - Включена	',
  `id_bestchange_rates` int(11) NOT NULL DEFAULT '0',
  `bestchange_position` int(11) NOT NULL DEFAULT '0',
  `id_group_commission` int(11) NOT NULL DEFAULT '0',
  `sorting_1` int(11) DEFAULT '0',
  `sorting_2` int(11) NOT NULL DEFAULT '0',
  `parent_url` varchar(191) DEFAULT NULL,
  `is_not_bonus` int(11) NOT NULL DEFAULT '0',
  `is_not_partner` int(11) NOT NULL DEFAULT '0',
  `individual_percentage` float NOT NULL DEFAULT '0',
  `fixed_payout` float NOT NULL DEFAULT '0',
  `export_label_param` varchar(191) DEFAULT NULL,
  `export_label_city` varchar(191) DEFAULT NULL,
  `export_label_minamount` int(11) NOT NULL DEFAULT '0',
  `export_label_maxamount` int(11) NOT NULL DEFAULT '0',
  `minimum_payout` float NOT NULL DEFAULT '0',
  `maximum_payout` float NOT NULL DEFAULT '0',
  `allow_export` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  `commission_s1` double(8,2) NOT NULL DEFAULT '0.00',
  `commission_s2` double(8,2) NOT NULL DEFAULT '0.00',
  `allow_export_from` varchar(191) DEFAULT NULL,
  `allow_export_to` varchar(191) DEFAULT NULL,
  `profit_s` double NOT NULL DEFAULT '0',
  `id_group_direction` int(11) NOT NULL DEFAULT '0',
  `is_restrict_editing` int(11) NOT NULL DEFAULT '0',
  `is_enabled_exchange` int(11) NOT NULL DEFAULT '0',
  `from_on_time` varchar(191) DEFAULT NULL,
  `to_on_time` varchar(191) DEFAULT NULL,
  `hidden_export_label_param` int(11) NOT NULL DEFAULT '0',
  `is_manual_min_price2` int(11) NOT NULL DEFAULT '0',
  `is_manual_max_price2` int(11) NOT NULL DEFAULT '0',
  `is_manual_min_price1` int(11) NOT NULL DEFAULT '0',
  `is_manual_max_price1` int(11) NOT NULL DEFAULT '0',
  `in_who_pay_commission` int(11) NOT NULL DEFAULT '0',
  `out_who_pay_commission` int(11) NOT NULL DEFAULT '0',
  `id_competitor` int(11) NOT NULL DEFAULT '0',
  `course_in` varchar(191) DEFAULT NULL,
  `course_out` varchar(191) DEFAULT NULL,
  `bc_min_sum` varchar(191) DEFAULT NULL,
  `bc_max_sum` varchar(191) DEFAULT NULL,
  `bc_id_new_rate` int(11) NOT NULL DEFAULT '0',
  `bc_add_course` int(11) NOT NULL DEFAULT '0',
  `max_order_one_ip` int(11) NOT NULL DEFAULT '0' COMMENT 'Макс. кол-во заявок на обмен с одного IP',
  `max_order_one_account1` int(11) NOT NULL DEFAULT '0' COMMENT 'Макс. кол-во заявок на обмен с одного счета Отдаю',
  `max_order_one_account2` int(11) NOT NULL DEFAULT '0' COMMENT 'Макс. кол-во заявок на обмен с одного счета Получаю',
  `max_order_one_user` int(11) NOT NULL DEFAULT '0' COMMENT 'Макс. кол-во заявок на обмен от одного пользователя',
  `max_order_one_email` int(11) NOT NULL DEFAULT '0' COMMENT 'Макс. кол-во заявок на обмен с одного e-mail',
  `not_ip` text,
  `cr_min_sum` varchar(191) DEFAULT NULL,
  `cr_max_sum` varchar(191) DEFAULT NULL,
  `cr_id_new_rate` int(11) NOT NULL DEFAULT '0',
  `cr_add_course` int(11) NOT NULL DEFAULT '0',
  `max_percent_partner` double(8,2) NOT NULL DEFAULT '0.00',
  `xml_juridical` int(11) NOT NULL DEFAULT '0',
  `languages` varchar(191) DEFAULT NULL,
  `is_hidden_not_locale` int(11) NOT NULL DEFAULT '0',
  `sorting_tariffs` int(11) NOT NULL DEFAULT '0',
  `device` varchar(191) DEFAULT NULL,
  `is_hidden_not_device` int(11) NOT NULL DEFAULT '0',
  `bc_new_commission` int(11) NOT NULL DEFAULT '0',
  `bestchange_bl` varchar(191) DEFAULT NULL,
  `bestchange_wl` varchar(191) DEFAULT NULL,
  `bestchange_step` varchar(191) NOT NULL DEFAULT '0',
  `bestchange_min_reserve` varchar(191) DEFAULT NULL,
  `bc_enable_your_course` int(11) NOT NULL DEFAULT '0',
  `bc_id_your_exchange` int(11) NOT NULL DEFAULT '0',
  `bc_your_add_course` double(8,2) NOT NULL DEFAULT '0.00',
  `rl_min2_course` varchar(191) DEFAULT NULL,
  `rl_max2_course` varchar(191) DEFAULT NULL,
  `rl_id_parser_exchange` int(11) NOT NULL DEFAULT '0',
  `rl_add_course` varchar(191) NOT NULL DEFAULT '0',
  `is_change_bestchange_range` int(11) NOT NULL DEFAULT '0',
  `bestchange_range` varchar(191) DEFAULT NULL,
  `oth_comm_percent` varchar(191) DEFAULT NULL,
  `oth_comm_currency` varchar(191) DEFAULT NULL,
  `oth_min_comm` varchar(191) DEFAULT NULL,
  `oth_deduct_comm` int(11) NOT NULL DEFAULT '0',
  `auto_del_order_status` varchar(191) DEFAULT NULL,
  `bestchange_max_reserve` varchar(191) NOT NULL DEFAULT '0',
  `is_hidden_tariffs` int(11) NOT NULL DEFAULT '0',
  `is_holding_direction` int(11) NOT NULL DEFAULT '0',
  `bestchange_range_from` varchar(191) DEFAULT NULL,
  `bestchange_range_to` varchar(191) DEFAULT NULL,
  `rl_id_your_exchange` int(11) NOT NULL DEFAULT '0',
  `rl_your_add_course` varchar(191) DEFAULT NULL,
  `reserve_max_limit` varchar(191) DEFAULT NULL,
  `reserve_limit_day` varchar(191) DEFAULT NULL,
  `reserve_limit_month` varchar(191) DEFAULT NULL,
  `is_num_transaction` varchar(191) DEFAULT NULL,
  `num_transaction_label` varchar(191) DEFAULT NULL,
  `max_order_one_ip_day` int(11) NOT NULL DEFAULT '0',
  `max_order_one_user_day` int(11) NOT NULL DEFAULT '0',
  `max_order_one_email_day` int(11) NOT NULL DEFAULT '0',
  `max_order_one_account1_day` int(11) NOT NULL DEFAULT '0',
  `max_order_one_account2_day` int(11) NOT NULL DEFAULT '0',
  `enable_file_parser_rate` int(11) NOT NULL DEFAULT '0',
  `id_file_parser_rate` int(11) NOT NULL DEFAULT '0',
  `id_bs_alt_parser` int(11) NOT NULL DEFAULT '0',
  `bs_alt_parser_course` varchar(191) NOT NULL DEFAULT '0',
  `is_enable_alt_bs_parser` int(11) NOT NULL DEFAULT '0',
  `is_disable_bs_error` int(11) NOT NULL DEFAULT '0',
  `profit_partner` double(8,2) NOT NULL DEFAULT '0.00',
  `is_note_tx` int(11) NOT NULL DEFAULT '0',
  `note_tx_label` varchar(191) DEFAULT NULL,
  `x19_mode` int(11) NOT NULL DEFAULT '0',
  `is_email_verification_modal` int(11) NOT NULL DEFAULT '0',
  `max_amount_newbie` double NOT NULL DEFAULT '0',
  `auto_del_order_time` varchar(191) DEFAULT NULL,
  `course_value` varchar(191) NOT NULL DEFAULT '0',
  `exchange_rate` varchar(191) DEFAULT NULL COMMENT 'Курс обмена',
  `is_error_rate` int(11) NOT NULL DEFAULT '0',
  `exchange_rate_str` varchar(191) DEFAULT NULL,
  `error_rate_text` varchar(191) DEFAULT NULL,
  `tech_name` varchar(191) DEFAULT NULL,
  `last_order_at` datetime DEFAULT NULL,
  `last_order_id` bigint(20) NOT NULL DEFAULT '0',
  `first_order_id` bigint(20) NOT NULL DEFAULT '0',
  `desc_exchange` text,
  `id_partner_parser_rate` int(11) NOT NULL DEFAULT '0',
  `id_parser_formula_rate` int(11) NOT NULL DEFAULT '0',
  `type_output_requisites` int(11) NOT NULL DEFAULT '0',
  `formalization_text` longtext,
  `min_count_exchanges_client` int(11) NOT NULL DEFAULT '0',
  `order_button_i_pay` text,
  `order_button_i_pay_text` text,
  `is_allow_telegram_bot` int(11) NOT NULL DEFAULT '0',
  `manual_rate_value` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_exchange_cities`
--

CREATE TABLE `direction_exchange_cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `add_comm` double NOT NULL DEFAULT '0',
  `param` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `min_price` double NOT NULL DEFAULT '0',
  `max_price` double NOT NULL DEFAULT '0',
  `id_country` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_exchange_error_log`
--

CREATE TABLE `direction_exchange_error_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0',
  `direction_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `where_error` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level_risk` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_exchange_group`
--

CREATE TABLE `direction_exchange_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `direction_exchange_group`
--

INSERT INTO `direction_exchange_group` (`id`, `name`, `id_user`, `created_at`, `updated_at`) VALUES
(3, 'Банки', 1, '2019-05-24 03:12:33', '2019-05-24 03:12:33'),
(4, 'Электронные деньги', 1, '2019-05-24 03:12:49', '2019-05-24 03:12:49'),
(5, 'Криптовалюты', 1, '2019-05-24 03:12:58', '2019-05-24 03:12:58');

-- --------------------------------------------------------

--
-- Структура таблицы `direction_exchange_merchants`
--

CREATE TABLE `direction_exchange_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateway_merchant_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_exchange_modes`
--

CREATE TABLE `direction_exchange_modes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_notification`
--

CREATE TABLE `direction_notification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0',
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_enabled_schedule` int(11) NOT NULL DEFAULT '0',
  `from_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `is_order_detail` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `direction_requisites`
--

CREATE TABLE `direction_requisites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `limit_day` double(8,2) NOT NULL DEFAULT '0.00',
  `limit_month` double(8,2) NOT NULL DEFAULT '0.00',
  `limit_views` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `docs_category`
--

CREATE TABLE `docs_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_type` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `docs_category_type`
--

CREATE TABLE `docs_category_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `docs_items`
--

CREATE TABLE `docs_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `parent_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `mailable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `html_template` longtext COLLATE utf8mb4_unicode_ci,
  `text_template` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_event_type` int(11) NOT NULL DEFAULT '0',
  `email_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `template` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `started_work` timestamp NULL DEFAULT NULL,
  `job_from` int(11) NOT NULL DEFAULT '0',
  `job_to` int(11) NOT NULL DEFAULT '0',
  `salary` float NOT NULL DEFAULT '0',
  `fines` int(11) NOT NULL DEFAULT '0',
  `total` float DEFAULT '0',
  `percent` int(11) NOT NULL DEFAULT '0',
  `bonus_rub` float NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `event_employees`
--

CREATE TABLE `event_employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_employees` int(11) NOT NULL,
  `id_task` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `event_reserve`
--

CREATE TABLE `event_reserve` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `id_reserve` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `value_after` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'Значение после',
  `value_before` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'Значение до',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `export_data`
--

CREATE TABLE `export_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_cron` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_filter` int(11) NOT NULL DEFAULT '0',
  `is_allow_filter` int(11) NOT NULL DEFAULT '0',
  `format_export` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `export_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `export_data`
--

INSERT INTO `export_data` (`id`, `name`, `is_cron`, `is_filter`, `is_allow_filter`, `format_export`, `count`, `export_value`, `created_at`, `updated_at`) VALUES
(1, 'Пользователи', '1', 1, 0, 'XLSX', 67, 'UsersExport', NULL, '2019-12-11 23:30:05'),
(2, 'Заявки', '0', 0, 0, 'XLSX', 7, 'OrdersExport', NULL, '2019-03-10 13:05:02'),
(3, 'Валюты', '0', 0, 1, 'XLSX', 1, 'CurrencyExport', NULL, '2019-03-10 12:50:54'),
(4, 'Направлении обменов', '0', 1, 0, 'XLSX', 1, 'DirectionExport', NULL, '2019-03-10 12:57:03'),
(5, 'Реквизиты', '0', 1, 0, 'XLSX', 4, 'RequisitesExport', NULL, '2019-03-14 17:59:03');

-- --------------------------------------------------------

--
-- Структура таблицы `e_voucher_codes`
--

CREATE TABLE `e_voucher_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `voucher_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_group` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

CREATE TABLE `faq_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_direction_exchange` int(11) NOT NULL,
  `id_currency1` int(11) NOT NULL DEFAULT '0',
  `id_currency2` int(11) NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `favorites_links`
--

CREATE TABLE `favorites_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `file_parser_groups`
--

CREATE TABLE `file_parser_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `file_parser_rates`
--

CREATE TABLE `file_parser_rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  `summa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `number_format` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `filter_currency`
--

CREATE TABLE `filter_currency` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `filter_currency`
--

INSERT INTO `filter_currency` (`id`, `name`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Руб', 0, '2017-10-20 04:49:18', '2019-10-02 04:47:22'),
(2, 'USD', 1, '2017-10-20 04:49:33', '2019-10-02 04:47:22'),
(3, 'Coin', 2, '2017-10-20 04:49:38', '2019-10-02 04:21:18');

-- --------------------------------------------------------

--
-- Структура таблицы `fine_employees`
--

CREATE TABLE `fine_employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_employees` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `firewall`
--

CREATE TABLE `firewall` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `fund`
--

CREATE TABLE `fund` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `gateways`
--

CREATE TABLE `gateways` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_merchant` int(11) NOT NULL DEFAULT '0' COMMENT 'Доступен прием?',
  `is_pay` int(11) NOT NULL DEFAULT '0' COMMENT 'Доступны выплаты?',
  `is_rpc` int(11) NOT NULL DEFAULT '0' COMMENT 'Использует RPC соединение',
  `is_check_pay` int(11) NOT NULL DEFAULT '0' COMMENT 'Проверка поступления средств',
  `version` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `security_options` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `gateways`
--

INSERT INTO `gateways` (`id`, `alias`, `name`, `class_name`, `is_merchant`, `is_pay`, `is_rpc`, `is_check_pay`, `version`, `created_at`, `updated_at`, `security_options`) VALUES
(1, 'advcash', 'AdvCash', 'AdvCash', 1, 1, 0, 0, '2.1', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"is_check_api\",\"security_hash\"]}'),
(2, 'perfectmoney', 'PerfectMoney', 'PerfectMoney', 1, 1, 0, 0, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"is_check_api\",\"security_hash\"]}'),
(3, 'payeer', 'Payeer', 'Payeer', 1, 1, 0, 0, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"is_check_api\",\"security_hash\"]}'),
(4, 'blockio', 'Block.io', 'BlockIo', 1, 1, 0, 1, '4.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(5, 'epaycore', 'ePayCore', 'EpayCore', 1, 1, 0, 0, '2.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"is_check_api\",\"security_hash\"]}'),
(6, 'obmenka', 'Obmenka', 'Obmenka', 1, 1, 0, 0, '2.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(7, 'nixmoney', 'NixMoney', 'NixMoney', 1, 1, 0, 0, '4.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"is_check_api\",\"security_hash\"]}'),
(8, 'adgroup', 'ADGroup (PayAssist.io)', 'AdGroup', 1, 1, 0, 0, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(9, 'webmoney', 'WebMoney', 'WebMoney', 1, 1, 0, 0, '4.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(10, 'westwallet', 'WestWallet', 'WestWallet', 1, 1, 0, 1, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(11, 'whitebitcrypto', 'WhiteBit Crypto', 'WhiteBitCrypto', 1, 1, 0, 1, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(12, 'fourbill', '4Bill', 'FourBill', 1, 1, 0, 0, '3.5', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(13, 'binancechain', 'Binance Chain', 'BinanceChain', 1, 1, 0, 1, '2.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(14, 'tron', 'Tron', 'Tron', 1, 1, 0, 1, '5.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(15, 'rpc', 'Crypto RPC', 'Rpc', 1, 1, 0, 1, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(16, 'paypal', 'PayPal', 'PayPal', 1, 0, 0, 0, '2.1', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(17, 'paykassa', 'PayKassa', 'PayKassa', 1, 1, 0, 1, '3.5', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(18, 'liqpay', 'LiqPay', 'LiqPay', 1, 0, 0, 0, '2.3', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(19, 'whitebitcodes', 'WhiteBit Коды', 'WhiteBitCodes', 1, 1, 0, 0, '2.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(20, 'ripple', 'Ripple', 'Ripple', 1, 1, 0, 0, '4.7', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(21, 'binance', 'Binance (Биржа)', 'Binance', 1, 1, 0, 0, '2.1', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(22, 'billline', 'Billline', 'Billline', 1, 1, 0, 0, '2.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(23, 'alikassa', 'Alikassa', 'Alikassa', 1, 1, 0, 0, '2.5', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(24, 'kunacodes', 'Kuna (Коды)', 'KunaCodes', 1, 1, 0, 0, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(25, 'exmogiftcard', 'EXMO Gift Card', 'ExmoGiftCard', 1, 1, 0, 0, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(26, 'kunapay', 'Kuna Pay (Фиат)', 'KunaPay', 1, 1, 0, 0, '2.5', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(27, 'cryptoprocessing', 'CryptoProcessing', 'CryptoProcessing', 1, 1, 0, 0, '1.6', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(28, 'ftx', 'FTX (Биржа)', 'Ftx', 1, 1, 0, 0, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(29, 'stellar', 'Stellar', 'Stellar', 1, 1, 0, 0, '3.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[]}'),
(30, 'yoomoney', 'Ю.Money', 'YooMoney', 1, 1, 0, 0, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"is_check_api\",\"security_hash\"]}'),
(31, 'fixedfloat', 'Fixed Float', 'FixedFloat', 1, 0, 0, 1, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(32, 'mpcvip', 'Master Processing VIP', 'MpcVip', 1, 1, 0, 1, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(33, 'changecoins', 'Changecoins', 'Changecoins', 1, 1, 0, 0, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(34, 'firekassa', 'Firekassa', 'Firekassa', 1, 1, 0, 1, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(35, 'privatbank', 'PrivatBank', 'PrivatBank', 0, 1, 0, 1, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(36, 'garantex', 'Garantex (Crypto)', 'Garantex', 1, 1, 0, 0, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(37, 'cryptomus', 'Cryptomus (Crypto)', 'Cryptomus', 1, 1, 0, 0, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}'),
(38, 'alfabitpay', 'AlfaBitPay', 'AlfaBitPay', 1, 1, 0, 1, '1.0', '2023-04-09 10:47:37', '2023-04-09 10:47:37', '{\"fields\":[\"is_merchant_log\",\"allow_ip_address\",\"security_hash\"]}');

-- --------------------------------------------------------

--
-- Структура таблицы `gateways_merchants`
--

CREATE TABLE `gateways_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_gateways` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `instruction_payment` text COLLATE utf8mb4_unicode_ci,
  `allow_ip_address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `security_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_check_from_shot` int(11) NOT NULL DEFAULT '0',
  `is_disable_code_currency` int(11) NOT NULL DEFAULT '0',
  `is_merchant_log` int(11) NOT NULL DEFAULT '0',
  `is_check_api` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `is_deny_ip_address` int(11) NOT NULL DEFAULT '0',
  `is_pay_commission` int(11) NOT NULL DEFAULT '0',
  `day_limit_merchant` int(11) NOT NULL DEFAULT '0',
  `max_limit_amount_order` double NOT NULL DEFAULT '0',
  `amount_fault` double NOT NULL DEFAULT '0',
  `day_limit_amount_merchant` double NOT NULL DEFAULT '0',
  `is_enable_merchant_button` int(11) NOT NULL DEFAULT '0',
  `method_pay` int(11) NOT NULL DEFAULT '0',
  `fixed_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_confirm` int(11) NOT NULL DEFAULT '0',
  `max_register_blockchain` int(11) NOT NULL DEFAULT '0',
  `max_first_confirm_blockchain` int(11) NOT NULL DEFAULT '0',
  `total_usd` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_order_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_config_done` int(11) NOT NULL DEFAULT '0',
  `is_card_found` int(11) NOT NULL DEFAULT '0',
  `exchange_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `gateways_payments`
--

CREATE TABLE `gateways_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_gateways` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `max_register_blockchain` int(11) NOT NULL DEFAULT '0',
  `max_first_confirm_blockchain` int(11) NOT NULL DEFAULT '0',
  `min_confirm` int(11) NOT NULL DEFAULT '0',
  `id_proxy` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `manual_pay_order` int(11) NOT NULL DEFAULT '0',
  `method_pay` int(11) NOT NULL DEFAULT '0',
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_request` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_buy` int(11) NOT NULL DEFAULT '0',
  `exchange_fee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `exchange_buy_type` int(11) NOT NULL DEFAULT '0',
  `time_in_force` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_buy_curr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_auto_take_fee` int(11) NOT NULL DEFAULT '0',
  `volume_to_usd` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_order_id` int(11) NOT NULL DEFAULT '0',
  `order_count` int(11) NOT NULL DEFAULT '0',
  `is_mass_payouts` int(11) NOT NULL DEFAULT '0' COMMENT 'Статус массовых выплат',
  `mass_coins` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT 'Валюты, используемые для массовых выплат',
  `hide_check_balance` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `geo_country_list`
--

CREATE TABLE `geo_country_list` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `geo_country_list`
--

INSERT INTO `geo_country_list` (`id`, `code`, `value`, `created_at`, `updated_at`) VALUES
(1, 'AU', 'Австралия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(2, 'AT', 'Австрия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(3, 'AZ', 'Азербайджан', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(4, 'AX', 'Аландские о-ва', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(5, 'AL', 'Албания', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(6, 'DZ', 'Алжир', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(7, 'AS', 'Американское Самоа', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(8, 'AI', 'Ангилья', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(9, 'AO', 'Ангола', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(10, 'AD', 'Андорра', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(11, 'AQ', 'Антарктида', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(12, 'AG', 'Антигуа и Барбуда', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(13, 'AR', 'Аргентина', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(14, 'AM', 'Армения', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(15, 'AW', 'Аруба', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(16, 'AF', 'Афганистан', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(17, 'BS', 'Багамы', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(18, 'BD', 'Бангладеш', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(19, 'BB', 'Барбадос', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(20, 'BH', 'Бахрейн', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(21, 'BY', 'Беларусь', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(22, 'BZ', 'Белиз', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(23, 'BE', 'Бельгия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(24, 'BJ', 'Бенин', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(25, 'BM', 'Бермудские о-ва', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(26, 'BG', 'Болгария', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(27, 'BO', 'Боливия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(28, 'BQ', 'Бонэйр, Синт-Эстатиус и Саба', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(29, 'BA', 'Босния и Герцеговина', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(30, 'BW', 'Ботсвана', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(31, 'BR', 'Бразилия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(32, 'IO', 'Британская территория в Индийском океане', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(33, 'BN', 'Бруней-Даруссалам', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(34, 'BF', 'Буркина-Фасо', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(35, 'BI', 'Бурунди', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(36, 'BT', 'Бутан', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(37, 'VU', 'Вануату', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(38, 'VA', 'Ватикан', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(39, 'GB', 'Великобритания', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(40, 'HU', 'Венгрия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(41, 'VE', 'Венесуэла', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(42, 'VG', 'Виргинские о-ва (Великобритания)', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(43, 'VI', 'Виргинские о-ва (США)', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(44, 'UM', 'Внешние малые о-ва (США)', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(45, 'TL', 'Восточный Тимор', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(46, 'VN', 'Вьетнам', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(47, 'GA', 'Габон', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(48, 'HT', 'Гаити', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(49, 'GY', 'Гайана', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(50, 'GM', 'Гамбия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(51, 'GH', 'Гана', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(52, 'GP', 'Гваделупа', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(53, 'GT', 'Гватемала', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(54, 'GN', 'Гвинея', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(55, 'GW', 'Гвинея-Бисау', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(56, 'DE', 'Германия', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(57, 'GG', 'Гернси', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(58, 'GI', 'Гибралтар', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(59, 'HN', 'Гондурас', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(60, 'HK', 'Гонконг (САР)', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(61, 'GD', 'Гренада', '2023-04-09 10:47:37', '2023-04-09 10:47:37'),
(62, 'GL', 'Гренландия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(63, 'GR', 'Греция', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(64, 'GE', 'Грузия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(65, 'GU', 'Гуам', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(66, 'DK', 'Дания', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(67, 'JE', 'Джерси', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(68, 'DJ', 'Джибути', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(69, 'DM', 'Доминика', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(70, 'DO', 'Доминиканская Республика', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(71, 'EG', 'Египет', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(72, 'ZM', 'Замбия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(73, 'EH', 'Западная Сахара', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(74, 'ZW', 'Зимбабве', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(75, 'IL', 'Израиль', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(76, 'IN', 'Индия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(77, 'ID', 'Индонезия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(78, 'JO', 'Иордания', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(79, 'IQ', 'Ирак', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(80, 'IR', 'Иран', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(81, 'IE', 'Ирландия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(82, 'IS', 'Исландия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(83, 'ES', 'Испания', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(84, 'IT', 'Италия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(85, 'YE', 'Йемен', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(86, 'CV', 'Кабо-Верде', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(87, 'KZ', 'Казахстан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(88, 'KH', 'Камбоджа', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(89, 'CM', 'Камерун', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(90, 'CA', 'Канада', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(91, 'QA', 'Катар', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(92, 'KE', 'Кения', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(93, 'CY', 'Кипр', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(94, 'KG', 'Киргизия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(95, 'KI', 'Кирибати', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(96, 'CN', 'Китай', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(97, 'KP', 'КНДР', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(98, 'CC', 'Кокосовые о-ва', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(99, 'CO', 'Колумбия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(100, 'KM', 'Коморы', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(101, 'CG', 'Конго - Браззавиль', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(102, 'CD', 'Конго - Киншаса', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(103, 'CR', 'Коста-Рика', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(104, 'CI', 'Кот-д’Ивуар', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(105, 'CU', 'Куба', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(106, 'KW', 'Кувейт', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(107, 'CW', 'Кюрасао', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(108, 'LA', 'Лаос', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(109, 'LV', 'Латвия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(110, 'LS', 'Лесото', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(111, 'LR', 'Либерия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(112, 'LB', 'Ливан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(113, 'LY', 'Ливия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(114, 'LT', 'Литва', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(115, 'LI', 'Лихтенштейн', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(116, 'LU', 'Люксембург', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(117, 'MU', 'Маврикий', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(118, 'MR', 'Мавритания', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(119, 'MG', 'Мадагаскар', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(120, 'YT', 'Майотта', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(121, 'MO', 'Макао (САР)', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(122, 'MW', 'Малави', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(123, 'MY', 'Малайзия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(124, 'ML', 'Мали', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(125, 'MV', 'Мальдивы', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(126, 'MT', 'Мальта', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(127, 'MA', 'Марокко', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(128, 'MQ', 'Мартиника', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(129, 'MH', 'Маршалловы Острова', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(130, 'MX', 'Мексика', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(131, 'MZ', 'Мозамбик', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(132, 'MD', 'Молдова', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(133, 'MC', 'Монако', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(134, 'MN', 'Монголия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(135, 'MS', 'Монтсеррат', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(136, 'MM', 'Мьянма (Бирма)', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(137, 'NA', 'Намибия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(138, 'NR', 'Науру', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(139, 'NP', 'Непал', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(140, 'NE', 'Нигер', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(141, 'NG', 'Нигерия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(142, 'NL', 'Нидерланды', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(143, 'NI', 'Никарагуа', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(144, 'NU', 'Ниуэ', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(145, 'NZ', 'Новая Зеландия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(146, 'NC', 'Новая Каледония', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(147, 'NO', 'Норвегия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(148, 'BV', 'о-в Буве', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(149, 'IM', 'о-в Мэн', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(150, 'NF', 'о-в Норфолк', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(151, 'CX', 'о-в Рождества', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(152, 'SH', 'о-в Св. Елены', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(153, 'PN', 'о-ва Питкэрн', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(154, 'TC', 'о-ва Тёркс и Кайкос', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(155, 'HM', 'о-ва Херд и Макдональд', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(156, 'AE', 'ОАЭ', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(157, 'OM', 'Оман', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(158, 'KY', 'Острова Кайман', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(159, 'CK', 'Острова Кука', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(160, 'PK', 'Пакистан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(161, 'PW', 'Палау', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(162, 'PS', 'Палестинские территории', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(163, 'PA', 'Панама', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(164, 'PG', 'Папуа — Новая Гвинея', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(165, 'PY', 'Парагвай', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(166, 'PE', 'Перу', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(167, 'PL', 'Польша', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(168, 'PT', 'Португалия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(169, 'PR', 'Пуэрто-Рико', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(170, 'KR', 'Республика Корея', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(171, 'RE', 'Реюньон', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(172, 'RU', 'Россия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(173, 'RW', 'Руанда', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(174, 'RO', 'Румыния', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(175, 'SV', 'Сальвадор', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(176, 'WS', 'Самоа', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(177, 'SM', 'Сан-Марино', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(178, 'ST', 'Сан-Томе и Принсипи', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(179, 'SA', 'Саудовская Аравия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(180, 'MK', 'Северная Македония', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(181, 'MP', 'Северные Марианские о-ва', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(182, 'SC', 'Сейшельские Острова', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(183, 'BL', 'Сен-Бартелеми', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(184, 'MF', 'Сен-Мартен', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(185, 'PM', 'Сен-Пьер и Микелон', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(186, 'SN', 'Сенегал', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(187, 'VC', 'Сент-Винсент и Гренадины', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(188, 'KN', 'Сент-Китс и Невис', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(189, 'LC', 'Сент-Люсия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(190, 'RS', 'Сербия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(191, 'SG', 'Сингапур', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(192, 'SX', 'Синт-Мартен', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(193, 'SY', 'Сирия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(194, 'SK', 'Словакия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(195, 'SI', 'Словения', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(196, 'US', 'Соединенные Штаты', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(197, 'SB', 'Соломоновы Острова', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(198, 'SO', 'Сомали', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(199, 'SD', 'Судан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(200, 'SR', 'Суринам', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(201, 'SL', 'Сьерра-Леоне', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(202, 'TJ', 'Таджикистан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(203, 'TH', 'Таиланд', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(204, 'TW', 'Тайвань', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(205, 'TZ', 'Танзания', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(206, 'TG', 'Того', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(207, 'TK', 'Токелау', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(208, 'TO', 'Тонга', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(209, 'TT', 'Тринидад и Тобаго', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(210, 'TV', 'Тувалу', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(211, 'TN', 'Тунис', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(212, 'TM', 'Туркменистан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(213, 'TR', 'Турция', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(214, 'UG', 'Уганда', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(215, 'UZ', 'Узбекистан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(216, 'UA', 'Украина', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(217, 'WF', 'Уоллис и Футуна', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(218, 'UY', 'Уругвай', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(219, 'FO', 'Фарерские о-ва', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(220, 'FM', 'Федеративные Штаты Микронезии', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(221, 'FJ', 'Фиджи', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(222, 'PH', 'Филиппины', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(223, 'FI', 'Финляндия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(224, 'FK', 'Фолклендские о-ва', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(225, 'FR', 'Франция', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(226, 'GF', 'Французская Гвиана', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(227, 'PF', 'Французская Полинезия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(228, 'TF', 'Французские Южные территории', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(229, 'HR', 'Хорватия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(230, 'CF', 'Центрально-Африканская Республика', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(231, 'TD', 'Чад', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(232, 'ME', 'Черногория', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(233, 'CZ', 'Чехия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(234, 'CL', 'Чили', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(235, 'CH', 'Швейцария', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(236, 'SE', 'Швеция', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(237, 'SJ', 'Шпицберген и Ян-Майен', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(238, 'LK', 'Шри-Ланка', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(239, 'EC', 'Эквадор', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(240, 'GQ', 'Экваториальная Гвинея', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(241, 'ER', 'Эритрея', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(242, 'SZ', 'Эсватини', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(243, 'EE', 'Эстония', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(244, 'ET', 'Эфиопия', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(245, 'GS', 'Южная Георгия и Южные Сандвичевы о-ва', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(246, 'ZA', 'Южно-Африканская Республика', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(247, 'SS', 'Южный Судан', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(248, 'JM', 'Ямайка', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(249, 'JP', 'Япония', '2023-04-09 10:47:38', '2023-04-09 10:47:38');

-- --------------------------------------------------------

--
-- Структура таблицы `getblockbot_histories`
--

CREATE TABLE `getblockbot_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `riskscore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asset` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `risky_volume_fiat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `risky_volume` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiat_code_effective` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dark_market` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dark_service` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_fraudulent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_high` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_low` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_moderate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_mlrisk_veryhigh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambling` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `illegal_service` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marketplace` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `miner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mixer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p2p_exchange_mlrisk_high` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p2p_exchange_mlrisk_low` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ransom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scam` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stolen_coins` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tx_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `give` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiving` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `group_parser_exchange`
--

CREATE TABLE `group_parser_exchange` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `type_amount` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `alias` varchar(191) DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `provider_url` varchar(191) DEFAULT NULL,
  `provider_id` varchar(191) DEFAULT NULL,
  `last_updated_at` timestamp NULL DEFAULT NULL,
  `proxy_id` int(11) NOT NULL DEFAULT '0',
  `last_imported_at` timestamp NULL DEFAULT NULL,
  `is_import_rates` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `group_parser_exchange`
--

INSERT INTO `group_parser_exchange` (`id`, `name`, `status`, `type_amount`, `created_at`, `updated_at`, `alias`, `sorting`, `priority`, `provider_url`, `provider_id`, `last_updated_at`, `proxy_id`, `last_imported_at`, `is_import_rates`) VALUES
(1, 'Центральный Банк РФ', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'RussianCentralBank', 0, 0, 'https://www.cbr.ru/scripts/XML_daily.asp', NULL, NULL, 0, NULL, 0),
(2, 'Exmo', 0, 'buy_price', '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'ExMo', 13, 0, 'https://api.exmo.com/v1/ticker', NULL, NULL, 0, NULL, 0),
(3, 'CoinMarketCap', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'CoinMarketCap', 0, 0, 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest', 'coinmarketcap', NULL, 0, NULL, 0),
(4, 'Европейский центральный банк', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'EuropeanCentralBank', 17, 0, 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml', NULL, NULL, 0, NULL, 0),
(5, 'Национальный банк Румынии', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'NationalBankOfRomania', 19, 0, 'https://www.bnr.ro/nbrfxrates.xml', NULL, NULL, 0, NULL, 0),
(6, 'Национальный банк Киргиции', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'NationalBankOfKyrgyz', 20, 0, 'https://www.nbkr.kg/XML/daily.xml', NULL, NULL, 0, NULL, 0),
(7, 'Национальный банк Беларусcии', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'NationalBankOfBelarus', 21, 0, 'http://www.nbrb.by/Services/XmlExRates.aspx', NULL, NULL, 0, NULL, 0),
(8, 'Национальный банк Казахстана', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'NationalBankOfKazakhstan', 22, 0, 'http://www.nationalbank.kz/rss/rates_all.xml', NULL, NULL, 0, NULL, 0),
(9, 'Национальный банк Молдовы', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'NationalBankOfMoldova', 23, 0, 'https://www.bnm.md/ru/official_exchange_rates?get_xml=1&date={date_now}', NULL, NULL, 0, NULL, 0),
(10, 'Binance', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'Binance', 1, 0, 'https://api.binance.com/api/v3/ticker/price', NULL, NULL, 0, NULL, 0),
(11, 'Blockchain', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'blockchain', 1, 0, 'https://blockchain.info/ticker', NULL, NULL, 0, NULL, 0),
(12, 'Poloniex', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'Poloniex', 15, 0, 'https://poloniex.com/public?command=returnTicker', NULL, NULL, 0, NULL, 0),
(13, 'Bitfinex', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'Bitfinex', 3, 0, 'https://api-pub.bitfinex.com/v2/tickers?symbols=ALL', NULL, NULL, 0, NULL, 0),
(14, 'HitBtc', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'HitBtc', 11, 0, 'https://api.hitbtc.com/api/2/public/ticker', NULL, NULL, 0, NULL, 0),
(15, 'KuCoin', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'KuCoin', 2, 0, 'https://openapi-v2.kucoin.com/api/v1/market/allTickers', NULL, NULL, 0, NULL, 0),
(16, 'Binance DEX', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'BinanceDEX', 5, 0, 'https://dex.binance.org/api/v1/ticker/24hr', NULL, NULL, 0, NULL, 0),
(17, 'OKEx', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'OKEx', 0, 0, 'https://www.okex.com/api/spot/v3/instruments/ticker', NULL, NULL, 0, NULL, 0),
(18, 'BitPay', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'BitPay', 0, 0, 'https://bitpay.com/rates', NULL, NULL, 0, NULL, 0),
(19, 'Cryptonex', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'cryptonex', 0, 0, 'https://stats.cryptonex.org/get_rate_list', NULL, NULL, 0, NULL, 0),
(20, 'Bitmart', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'bitmart', 0, 0, 'https://api-cloud.bitmart.com/spot/v1/ticker', NULL, NULL, 0, NULL, 0),
(21, 'FloatRates', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'floatrates', 0, 0, 'http://www.floatrates.com/daily/usd.xml', NULL, NULL, 0, NULL, 0),
(22, 'FloatRates CNY', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'floatratescny', 0, 0, 'http://www.floatrates.com/daily/cny.xml', NULL, NULL, 0, NULL, 0),
(23, 'FloatRates INR', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'floatratesinr', 0, 0, 'http://www.floatrates.com/daily/inr.xml', NULL, NULL, 0, NULL, 0),
(24, 'FloatRates EUR', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'floatrateseur', 0, 0, 'http://www.floatrates.com/daily/eur.xml', NULL, NULL, 0, NULL, 0),
(25, 'Узбекистанский центральный банк', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'UzbekistanCentralBank', 0, 0, 'https://cbu.uz/ru/arkhiv-kursov-valyut/xml/', NULL, NULL, 0, NULL, 0),
(26, 'Израильский центральный банк', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'nationalbankofisrael', 0, 0, 'https://www.boi.org.il/currency.xml', NULL, NULL, 0, NULL, 0),
(27, 'WhiteBit', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'whitebit', 0, 0, 'https://whitebit.com/api/v1/public/tickers', NULL, NULL, 0, NULL, 0),
(28, 'WMExchanger', 0, '', '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'wmexchanger', 0, 0, 'https://wm.exchanger.ru/asp/JSONbestRates.asp', NULL, NULL, 0, NULL, 0),
(29, 'Garantex', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'garantex', 0, 0, 'https://garantex.io/api/v2/trades?market=btcrub', NULL, NULL, 0, NULL, 0),
(30, 'FloatRates UAH', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'floatratesuah', 0, 0, 'http://www.floatrates.com/daily/uah.xml', NULL, NULL, 0, NULL, 0),
(31, 'Fixer API (USD)', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'fixerapiusd', 0, 0, 'https://api.apilayer.com/fixer/latest?base=USD', NULL, NULL, 0, NULL, 0),
(32, 'Fixer API (UAH)', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'fixerapiuah', 0, 0, 'https://api.apilayer.com/fixer/latest?base=UAH', 'fixerapi', NULL, 0, NULL, 0),
(33, 'Fixer API (EUR)', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'fixerapieur', 0, 0, 'https://api.apilayer.com/fixer/latest?base=EUR', 'fixerapi', NULL, 0, NULL, 0),
(34, 'PancakeSwap', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'pancakeswap', 0, 0, 'https://api.pancakeswap.info/api/v2/pairs', 'fixerapi', NULL, 0, NULL, 0),
(35, 'CoinStats', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'coinstats', 0, 0, 'https://api.coinstats.app/public/v1/coins?limit=1000', NULL, NULL, 0, NULL, 0),
(36, 'CoinStatsFiat', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'coinstatsfiat', 0, 0, 'https://api.coinstats.app/public/v1/fiats', NULL, NULL, 0, NULL, 0),
(37, 'Crypto.com', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'cryptocom', 0, 0, 'https://api.crypto.com/v1/ticker/price', NULL, NULL, 0, NULL, 0),
(38, 'Gate.io', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'gateio', 0, 0, 'https://data.gateapi.io/api2/1/tickers', NULL, NULL, 0, NULL, 0),
(39, 'Coinbase API', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'coinbase', 0, 0, 'https://api.coinbase.com/v2/exchange-rates', NULL, NULL, 0, NULL, 0),
(40, 'Alcor.Exchange', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'alcorexchange', 0, 0, 'https://alcor.exchange/api/markets', NULL, NULL, 0, NULL, 0),
(41, 'Mexc.Exchange', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'mexcexchange', 0, 0, 'https://www.mexc.com/open/api/v2/market/ticker', NULL, NULL, 0, NULL, 0),
(42, 'Kraken', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'kraken', 0, 0, 'https://api.coinstats.app/public/v1/tickers?exchange=Kraken', NULL, NULL, 0, NULL, 0),
(43, 'Московская биржа', 0, NULL, '2023-04-09 10:48:58', '2023-04-09 10:48:58', 'moex', 0, 0, 'https://iss.moex.com/iss/statistics/engines/currency/markets/selt/rates.json', NULL, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `health_checks`
--

CREATE TABLE `health_checks` (
  `id` int(10) UNSIGNED NOT NULL,
  `resource_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_display` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `healthy` tinyint(1) NOT NULL,
  `error_message` text COLLATE utf8mb4_unicode_ci,
  `runtime` double(8,2) NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_human` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `histories_codes`
--

CREATE TABLE `histories_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pay` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `history_excode`
--

CREATE TABLE `history_excode` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Тип получения данных',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `history_fields`
--

CREATE TABLE `history_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_currency1` int(11) NOT NULL DEFAULT '0',
  `id_currency2` int(11) NOT NULL DEFAULT '0',
  `filed_give` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filed_receiving` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `history_internal_accounts`
--

CREATE TABLE `history_internal_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_internal_account` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_task` int(11) NOT NULL DEFAULT '0',
  `type_history` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `history_payment_transactions`
--

CREATE TABLE `history_payment_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transfer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `history_recalculation`
--

CREATE TABLE `history_recalculation` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `amount` float NOT NULL,
  `old_amount` float NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `hosts`
--

CREATE TABLE `hosts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssh_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_properties` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `iex_settings`
--

CREATE TABLE `iex_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `info_statistics`
--

CREATE TABLE `info_statistics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_automatic` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `internal_accounts`
--

CREATE TABLE `internal_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_code_currency` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `job_settings`
--

CREATE TABLE `job_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_job` int(11) NOT NULL,
  `weekdays` int(11) NOT NULL DEFAULT '0',
  `manual_status` int(11) NOT NULL,
  `manual_date` timestamp NULL DEFAULT NULL,
  `cron_status` int(11) NOT NULL,
  `cron_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cron_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_startup` timestamp NULL DEFAULT NULL,
  `auto_operator_auth` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `job_settings`
--

INSERT INTO `job_settings` (`id`, `type_job`, `weekdays`, `manual_status`, `manual_date`, `cron_status`, `cron_from`, `cron_to`, `site_startup`, `auto_operator_auth`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, NULL, 1, '09:46', '20:46', '2019-12-02 04:16:00', 0, '2017-10-31 21:00:00', '2019-12-01 07:29:46');

-- --------------------------------------------------------

--
-- Структура таблицы `links_footers`
--

CREATE TABLE `links_footers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `links_footer_groups`
--

CREATE TABLE `links_footer_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `links_reviews`
--

CREATE TABLE `links_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_review` tinyint(1) NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `is_bot` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `links_review_groups`
--

CREATE TABLE `links_review_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `live_notification`
--

CREATE TABLE `live_notification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `text_alt` text COLLATE utf8mb4_unicode_ci,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timeout` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `logs_email`
--

CREATE TABLE `logs_email` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `headers` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `log_autopayments`
--

CREATE TABLE `log_autopayments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `event_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `log_check_pay`
--

CREATE TABLE `log_check_pay` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `event_type` int(11) NOT NULL DEFAULT '0',
  `event_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `log_drain`
--

CREATE TABLE `log_drain` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `id_requisites` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `from_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `log_error_merchants`
--

CREATE TABLE `log_error_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_value` text COLLATE utf8mb4_unicode_ci,
  `event_type` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `log_merchants`
--

CREATE TABLE `log_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `event_type` int(11) NOT NULL DEFAULT '0',
  `event_value` text COLLATE utf8mb4_unicode_ci,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `main_event_logs`
--

CREATE TABLE `main_event_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `type_event` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci,
  `sorting` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name_alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `name`, `sorting`, `slug`, `status`, `created_at`, `updated_at`, `name_alt`) VALUES
(1, '{\"ru\":\"\\u041d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438\",\"en\":null}', 1, '/news', 0, '2017-10-25 05:45:46', '2023-04-09 11:08:30', 'News'),
(2, '{\"ru\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u0430 \\u043e\\u0431\\u043c\\u0435\\u043d\\u0430\",\"en\":null}', 1, '/rules', 1, '2017-10-25 05:46:17', '2023-04-09 11:08:42', 'Exchange rules'),
(3, '{\"ru\":\"\\u041f\\u0430\\u0440\\u0442\\u043d\\u0435\\u0440\\u0430\\u043c\",\"en\":null}', 0, '/partners', 1, '2017-10-25 05:46:34', '2023-04-09 11:07:27', 'Partners'),
(4, '{\"ru\":\"\\u0422\\u0430\\u0440\\u0438\\u0444\\u044b\",\"en\":null}', 0, '/tariffs', 0, '2017-10-25 05:46:47', '2023-04-09 11:07:49', 'Our partners'),
(5, '{\"ru\":\"FAQ\",\"en\":null}', 2, '/faq', 1, '2017-10-25 05:46:59', '2023-04-09 11:08:52', 'Questions and answers'),
(8, '{\"ru\":\"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b\",\"en\":null}', 3, '/contacts', 1, '2017-11-01 13:21:35', '2023-04-09 11:09:03', 'Contacts');

-- --------------------------------------------------------

--
-- Структура таблицы `merchant_account`
--

CREATE TABLE `merchant_account` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `merchant_transaction_hash`
--

CREATE TABLE `merchant_transaction_hash` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `transaction_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `merchant_transaction_ids`
--

CREATE TABLE `merchant_transaction_ids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_09_16_161337_create_permission_tables', 1),
(4, '2017_09_16_174415_create_sessions_table', 1),
(5, '2017_09_16_174538_create_notifications_table', 1),
(6, '2017_09_16_174559_create_cache_table', 1),
(9, '2017_10_03_232930_create_payout_address_table', 4),
(11, '2017_10_04_233720_create_requisites_list_table', 5),
(13, '2017_10_05_101321_create_reserve_log_table', 6),
(14, '2017_10_05_113501_create_settings_theme_table', 7),
(15, '2017_10_05_154154_create_partners_table', 8),
(16, '2017_10_05_171612_create_backgrounds_table', 9),
(18, '2017_10_06_054328_create_discounts_table', 10),
(19, '2017_10_06_063139_create_unpaid_items_table', 11),
(21, '2017_10_09_172644_create_cron_table', 12),
(22, '2017_10_09_193412_create_cron_category_table', 12),
(23, '2017_10_10_084322_create_transaction_table', 13),
(24, '2017_10_10_165846_create_faq_category_table', 14),
(25, '2017_10_10_165915_create_faq_table', 14),
(26, '2016_06_01_000001_create_oauth_auth_codes_table', 15),
(27, '2016_06_01_000002_create_oauth_access_tokens_table', 15),
(28, '2016_06_01_000003_create_oauth_refresh_tokens_table', 15),
(29, '2016_06_01_000004_create_oauth_clients_table', 15),
(30, '2016_06_01_000005_create_oauth_personal_access_clients_table', 15),
(31, '2017_10_12_123532_create_generator_currency_table', 15),
(32, '2017_10_20_073637_create_filter_currency_table', 16),
(131, '2017_10_21_002533_create_monitors_table', 17),
(132, '2015_03_07_311070_create_tracker_paths_table', 18),
(133, '2015_03_07_311071_create_tracker_queries_table', 18),
(134, '2015_03_07_311072_create_tracker_queries_arguments_table', 18),
(135, '2015_03_07_311073_create_tracker_routes_table', 18),
(136, '2015_03_07_311074_create_tracker_routes_paths_table', 18),
(137, '2015_03_07_311075_create_tracker_route_path_parameters_table', 18),
(138, '2015_03_07_311076_create_tracker_agents_table', 18),
(139, '2015_03_07_311077_create_tracker_cookies_table', 18),
(140, '2015_03_07_311078_create_tracker_devices_table', 18),
(141, '2015_03_07_311079_create_tracker_domains_table', 18),
(142, '2015_03_07_311080_create_tracker_referers_table', 18),
(143, '2015_03_07_311081_create_tracker_geoip_table', 18),
(144, '2015_03_07_311082_create_tracker_sessions_table', 18),
(145, '2015_03_07_311083_create_tracker_errors_table', 18),
(146, '2015_03_07_311084_create_tracker_system_classes_table', 18),
(147, '2015_03_07_311085_create_tracker_log_table', 18),
(148, '2015_03_07_311086_create_tracker_events_table', 18),
(149, '2015_03_07_311087_create_tracker_events_log_table', 18),
(150, '2015_03_07_311088_create_tracker_sql_queries_table', 18),
(151, '2015_03_07_311089_create_tracker_sql_query_bindings_table', 18),
(152, '2015_03_07_311090_create_tracker_sql_query_bindings_parameters_table', 18),
(153, '2015_03_07_311091_create_tracker_sql_queries_log_table', 18),
(154, '2015_03_07_311092_create_tracker_connections_table', 18),
(155, '2015_03_07_311093_create_tracker_tables_relations', 18),
(156, '2015_03_13_311094_create_tracker_referer_search_term_table', 18),
(157, '2015_03_13_311095_add_tracker_referer_columns', 18),
(158, '2015_11_23_311096_add_tracker_referer_column_to_log', 18),
(159, '2015_11_23_311097_create_tracker_languages_table', 18),
(160, '2015_11_23_311098_add_language_id_column_to_sessions', 18),
(161, '2015_11_23_311099_add_tracker_language_foreign_key_to_sessions', 18),
(162, '2015_11_23_311100_add_nullable_to_tracker_error', 18),
(163, '2017_01_31_311101_fix_agent_name', 18),
(164, '2017_06_20_311102_add_agent_name_hash', 18),
(165, '2014_02_01_311070_create_firewall_table', 19),
(166, '2017_10_21_235416_create_sessions_table', 20),
(167, '2017_10_22_124906_create_task_log_table', 20),
(182, '2017_10_22_201100_create_referral_programs_table', 21),
(183, '2017_10_22_201101_create_referral_links_table', 21),
(184, '2017_10_22_201102_create_referral_relationships_table', 21),
(185, '2017_10_22_201103_add_allowed_ref_program_to_users', 21),
(187, '2017_10_23_175416_create_user_balance_table', 22),
(188, '2017_10_23_204855_create_referral_log_table', 23),
(189, '2017_10_24_085006_create_reward_table', 24),
(190, '2017_10_24_085144_create_reward_programs_table', 24),
(191, '2017_10_24_103816_create_reward_log_table', 25),
(192, '2017_10_24_233013_create_pages_table', 26),
(193, '2017_10_25_083556_create_menu_table', 27),
(194, '2017_10_25_190125_create_parser_type_table', 28),
(195, '2017_10_25_211438_create_user_settings_table', 29),
(196, '2017_10_25_223218_create_fund_table', 29),
(197, '2017_03_04_000000_create_bans_table', 30),
(198, '2018_06_28_000847_add_banned_at_column_to_users_table', 31),
(199, '2018_06_29_101540_create_docs_category_table', 32),
(200, '2018_06_29_102056_create_docs_category_type_table', 32),
(201, '2018_06_29_102139_create_docs_items_table', 32),
(202, '2018_07_05_184029_create_push_subscriptions_table', 33),
(203, '2018_07_05_194923_add_uuid_column_to_users', 34),
(204, '2018_09_04_064813_create_favorites_table', 35),
(205, '2018_09_06_203049_create_banned_user_table', 36),
(206, '2018_09_07_063952_create_tasks_rates_data_table', 37),
(207, '2019_02_04_141815_create_tasks_status_log_table', 38),
(208, '2019_02_06_165323_create_notices_exchange_table', 39),
(209, '2019_02_06_191328_add_username_to_users', 40),
(210, '2018_09_09_000001_create_table_health_checks', 41),
(211, '2019_02_14_132323_create_banned_table', 42),
(212, '2018_08_08_100000_create_telescope_entries_table', 43),
(213, '2019_03_05_181556_create_audits_table', 44),
(214, '2019_03_10_115333_create_export_lite_table', 45),
(215, '2019_03_10_122508_create_export_data_table', 46),
(216, '2017_08_24_000000_create_settings_table', 47),
(218, '2019_03_13_185017_create_admin_log_operation_table', 48),
(219, '2019_03_14_094533_create_blacklist_order_table', 49),
(222, '2019_03_24_111951_create_advantage_table', 50),
(223, '2019_03_27_140256_create_failed_jobs_table', 51),
(225, '2019_03_29_093039_add_is_delete_to_reserve_request', 52),
(226, '2019_03_29_095945_create_requisites_group_table', 53),
(227, '2019_03_29_102747_add_id_group_to_requisites', 54),
(228, '2019_03_30_064705_add_is_delete_to_payments', 55),
(230, '2019_03_31_063131_add_column_to_requisites', 56),
(231, '2019_03_31_091521_add_column_id_check_pay_to_currencies', 57),
(232, '2019_03_31_141711_add_column_field_type_currency_fields', 58),
(233, '2019_03_31_180948_add_column_month_limit_to_currency', 59),
(234, '2019_04_01_183150_add_column_limit_week_to_requisites', 60),
(235, '2019_04_03_214916_add_column_ip_changed_to_users', 61),
(236, '2019_04_04_203154_add_column_role_expired_at_to_users', 62),
(238, '2019_04_05_074739_create_password_histories_table', 63),
(239, '2019_04_07_061554_rename_column_random_to_requisites', 64),
(240, '2019_04_07_070407_add_column_history_at_to_requisites', 65),
(241, '2019_04_07_131424_add_columns_to_generator_currency', 66),
(242, '2019_04_07_192046_delete_column_account_number_field_to_requisites', 67),
(243, '2019_04_07_192134_add_column_account_number_field_to_currencies', 67),
(245, '2019_04_08_082718_add_columns_week_limit_to_currencies', 68),
(246, '2019_04_08_090710_add_column_limit_views_to_requisites', 69),
(247, '2019_04_16_205714_add_column_is_main_to_direction_exchange', 70),
(248, '2019_04_18_144745_add_column_notice_in_to_currencies', 71),
(249, '2019_04_28_195032_add_column_transfer_percent_reserve_to_currencies', 72),
(250, '2019_04_28_195530_add_column_transfer_amount_reserve_to_currencies', 73),
(251, '2019_04_30_161321_add_columns_commission_s_to_direction_exchange', 74),
(252, '2019_05_02_194739_add_column_is_follow_referral_to_users', 75),
(254, '2019_05_03_061553_add_column_allows_exports_to_direction_exchange', 76),
(255, '2019_05_09_110548_add_comment_to_event_reserve', 77),
(256, '2019_05_10_062257_add_column_remove_spaces_requisite_to_currencies', 78),
(257, '2019_05_10_064441_add_column_payout_commission_to_currencies', 79),
(258, '2019_05_10_070414_add_column_name_alt_to_payments', 80),
(259, '2019_05_10_074607_add_column_sorting_reserve_to_currencies', 81),
(261, '2019_05_11_074239_add_column_old_ip_address_to_user_auth', 83),
(262, '2019_05_11_101834_add_column_number_format_to_generator_currency', 84),
(263, '2019_05_11_102914_add_column_type_number_format_to_generator_currency', 85),
(264, '2019_05_11_110733_add_column_operator_started_at_to_tasks', 86),
(265, '2019_05_12_152911_add_column_blockchain_url_to_payments', 87),
(266, '2019_05_12_190658_create_reserve_group_table', 88),
(267, '2019_05_12_192008_add_column_id_group_to_reserves', 89),
(268, '2019_05_13_131939_add_column_sorting_to_reserves', 90),
(269, '2019_05_14_054521_add_column_deleted_at_to_tasks_info', 91),
(270, '2019_05_14_054543_add_column_deleted_at_to_tasks_rates_data', 91),
(271, '2019_05_14_054556_add_column_deleted_at_to_tasks_status_log', 91),
(272, '2019_05_14_054723_add_column_deleted_at_to_transactions', 92),
(273, '2019_05_14_054923_add_column_deleted_at_to_history_recalculation', 93),
(274, '2019_05_14_054947_add_column_deleted_at_to_wallets_history', 93),
(275, '2019_05_14_055011_add_column_deleted_at_to_tasks_convert_log', 93),
(276, '2019_05_14_055048_add_column_deleted_at_to_history_payment_transactions', 94),
(277, '2019_05_14_055138_add_column_deleted_at_to_history_excode', 95),
(278, '2019_05_14_055205_add_column_deleted_at_to_reward_log', 96),
(279, '2019_05_14_055227_add_column_deleted_at_to_wallet_transactions', 96),
(280, '2019_05_14_085623_add_column_is_server_tx_merchant_to_currencies', 97),
(282, '2019_05_14_091823_create_payment_explorer_table', 98),
(283, '2019_05_14_144610_create_requisites_blacklist_table', 99),
(284, '2019_05_14_145001_add_column_text_to_requisites_blacklist', 100),
(285, '2019_05_14_165043_add_column_first_char_alt_to_currencies', 101),
(286, '2019_05_14_180651_add_column_task_cancel_to_unpaid_items', 102),
(287, '2019_05_14_181335_add_column_allow_delete_to_tasks_status', 103),
(289, '2019_05_14_181724_custom_data_to_unpaid_items', 104),
(290, '2019_05_14_204856_create_tasks_chat_table', 105),
(291, '2019_05_14_213844_create_tasks_rejection_status_table', 106),
(292, '2019_05_14_215838_add_column_rejection_status_to_tasks', 107),
(293, '2019_05_14_222437_add_column_id_rejection_status_to_tasks', 108),
(294, '2019_05_15_062052_create_pending_order_status', 109),
(295, '2019_05_15_062212_add_column_id_pending_status_to_tasks', 110),
(296, '2019_05_15_213011_add_column_is_not_delete_to_tasks_rejection_status', 111),
(297, '2019_05_15_220025_add_column_is_not_delete_to_pending_order_status', 112),
(298, '2019_05_16_112155_add_column_name_alt_to_currency_fields', 113),
(299, '2019_05_11_073216_add_columns_in_out_price_to_task_status_log', 114),
(300, '2019_05_17_082400_add_column_link_to_notices_exchange', 115),
(302, '2019_05_17_150206_create_task_reports_table', 116),
(303, '2019_05_17_205144_add_column_profit_usd_to_task_reports', 117),
(304, '2019_05_17_210852_add_column_profit_s_to_direction_exchange', 118),
(305, '2019_05_17_215300_add_column_profit_s_to_task_reports', 119),
(306, '2019_05_18_092848_add_column_profit_rub_to_task_reports', 120),
(307, '2019_05_18_095741_add_column_is_bestchange_to_task_reports', 121),
(308, '2019_05_18_170351_create_task_log_confirmation_table', 122),
(309, '2019_05_18_183933_add_column_sorting_to_notices_exchange', 123),
(310, '2019_05_18_191752_add_column_sorting_to_advantage', 124),
(311, '2019_05_18_192822_create_parser_exchange_log_table', 125),
(312, '2019_05_18_203740_add_column_sorting_to_partners', 126),
(313, '2019_05_18_204823_create_referral_statistics_table', 127),
(314, '2019_05_18_220157_add_column_ip_to_referral_statistics', 128),
(315, '2019_05_18_220222_add_column_id_user_to_referral_statistics', 128),
(316, '2019_05_19_083508_add_column_percent_to_reward_log', 129),
(317, '2019_05_19_084304_add_column_is_cashback_to_reward_log', 130),
(318, '2019_05_19_175500_add_column_is_favorites_to_tasks', 131),
(319, '2019_05_19_191417_add_column_is_spam_to_tasks', 132),
(320, '2019_05_20_075350_create_main_event_logs_table', 133),
(321, '2019_05_20_082651_add_column_ip_to_main_event_logs', 134),
(322, '2019_05_20_083346_add_column_id_admin_to_main_event_logs', 135),
(323, '2019_05_21_055132_add_column_name_alt_to_menu', 136),
(324, '2019_05_22_200043_create_links_reviews_table', 137),
(325, '2019_05_23_072005_add_column_hash_id_to_archive_reports', 138),
(326, '2019_05_23_081635_create_bestchange_rates_log', 139),
(327, '2019_05_23_090604_create_admin_auth_log_table', 140),
(328, '2019_05_23_090942_add_column_is_successful_to_admin_auth_log', 141),
(329, '2019_05_23_152149_add_column_place_change_to_tasks_status_log', 142),
(330, '2019_05_23_174344_add_columns_to_code_currencies', 143),
(331, '2019_05_24_060006_create_direction_exchange_group_table', 144),
(332, '2019_05_24_061343_add_column_id_group_direction_to_direction_exchange', 145),
(333, '2019_05_24_063629_add_column_is_restrict_editing_to_direction_exchange', 146),
(334, '2019_05_24_072029_create_reserve_alerts_table', 147),
(336, '2019_05_24_072512_add_column_count_alert_to_reserves_alerts', 148),
(337, '2019_05_27_091858_add_column_language_field_to_currency_fields', 149),
(338, '2019_05_27_101040_add_3_columns_to_direction_exchange', 150),
(339, '2019_05_27_111922_create_whitelist_order_table', 151),
(340, '2019_05_28_162141_add_3_columns_to_notices_exchange', 152),
(341, '2019_05_28_164142_add_column_text_alt_to_notices_exchange', 153),
(342, '2019_05_28_164951_add_column_comment_to_requisites', 154),
(343, '2019_05_28_193114_create_users_history_profiles_table', 155),
(344, '2019_05_28_195845_add_columns_to_history_profiles', 156),
(345, '2019_05_29_075545_add_column_verified_at_to_withdrawal_request', 157),
(346, '2019_05_29_081630_add_column_tx_id_to_withdrawal_request', 158),
(350, '2019_05_29_110938_create_log_merchants_table', 159),
(351, '2019_05_29_193757_add_column_status_to_log_merchants', 159),
(352, '2019_05_29_200326_add_column_event_to_log_merchants', 159),
(353, '2019_05_29_213216_add_column_provider_to_log_merchants', 160),
(354, '2019_05_30_193419_add_column_is_archive_to_currencies', 161),
(355, '2019_05_31_060815_create_currencies_groups_table', 162),
(356, '2019_05_31_060909_add_column_id_group_to_currencies', 163),
(358, '2019_06_03_113703_create_log_autopayments_table', 165),
(359, '2019_06_02_134815_create_currencies_autopayment_table', 166),
(360, '2019_06_05_172833_add_column_is_check_pay_cron_to_currencies', 167),
(361, '2019_06_05_202326_add_column_is_bot_to_tasks', 168),
(362, '2019_06_06_102107_create_log_check_pay_table', 169),
(363, '2019_06_06_102412_add_column_provider_to_log_check_pay', 170),
(364, '2019_06_06_155312_add_column_register_tx_to_tasks', 171),
(365, '2019_06_11_065537_add_column_hidden_export_label_param_to_direction_exchange', 172),
(366, '2019_06_13_185842_add_column_custom_field_comment_to_requisites', 173),
(367, '2019_06_16_074529_add_column_alias_to_group_parser_exchange', 174),
(368, '2019_06_16_144006_add_column_to_group_parser_exchange', 175),
(369, '2019_06_16_153942_add_column_provider_to_log_autopayments', 176),
(370, '2019_06_18_065040_add_column_merchant_incomplete_payment_to_tasks', 177),
(371, '2019_06_18_174451_add_column_is_paid_to_currencies_autopayment', 178),
(372, '2019_06_18_194027_add_column_max_amount_day_to_currencies_autopayment', 179),
(373, '2019_06_19_070539_add_column_merchant_overpayment_to_tasks', 180),
(374, '2019_06_21_190023_change_type_export_label_param_to_direction_exchange', 181),
(375, '2019_06_22_071432_add_column_is_notify_login_to_users', 182),
(376, '2019_06_22_072058_add_column_is_password_reset_to_users', 183),
(377, '2019_06_22_072826_add_column_user_agent_to_users', 183),
(378, '2019_06_22_150801_add_column_is_pay_referral_to_users', 184),
(379, '2019_06_22_152844_add_column_is_pay_cashback_to_users', 185),
(380, '2019_06_22_185817_add_column_is_archive_to_referral_statistics', 186),
(381, '2019_06_22_212623_add_column_user_agent_to_referral_statistics', 187),
(386, '2019_06_23_122500_create_direction_notification_table', 188),
(387, '2019_06_25_090925_add_columns_to_direction_notification', 189),
(388, '2019_06_26_055618_add_column_sort_by_to_bestchange_rates', 190),
(389, '2019_06_30_055117_add_columns_to_tasks_info', 191),
(390, '2019_06_30_061024_add_column_to_referral_statistics', 192),
(391, '2019_06_30_065309_add_column_to_favorites', 193),
(392, '2019_07_04_113722_add_columns_to_direction_exchange', 194),
(393, '2019_07_04_114003_add_columns_to_direction_exchange', 195),
(394, '2019_07_04_220500_add_column_to_currencies', 196),
(395, '2019_07_04_221526_add_column_to_tasks', 196),
(396, '2019_07_05_195018_add_column_to_direction_exchange', 197),
(397, '2019_07_07_121431_add_column_to_wallets_addresses', 198),
(398, '2019_07_07_202917_add_column_in_flow_funds_to_tasks', 199),
(399, '2019_07_08_061505_add_column_is_qrcode_amount_to_currencies', 200),
(400, '2019_07_08_082205_add_column_in_min_amount_to_tasks_info', 201),
(401, '2019_07_08_082424_add_columns_in_max_amount_to_tasks_info', 202),
(402, '2019_07_08_094656_add_column_is_failed_send_to_wallets_addresses', 203),
(403, '2019_07_08_192231_create_verification_card__table', 204),
(404, '2019_07_08_193753_add_column_to_currencies', 205),
(405, '2019_07_08_211204_add_column_hash_id_to_verification_card', 206),
(406, '2019_07_08_211758_add_column_id_currency_to_verification_card', 207),
(407, '2019_07_08_213531_add_column_name_to_verification_card', 208),
(408, '2019_07_08_213810_add_column_status_to_verification_card', 209),
(409, '2019_07_09_084510_add_column_to_verification_card', 210),
(410, '2019_07_09_120829_add_column_to_currencies', 211),
(411, '2019_07_11_101130_change_type_notice_in_to_currencies', 212),
(412, '2019_07_12_054646_add_column_card_number_string_to_verification_card', 213),
(413, '2019_07_12_083524_add_column_hold_in_hours_to_currencies', 214),
(414, '2019_07_16_120455_add_column_is_verification_to_users', 215),
(415, '2019_07_16_160402_add_column_is_user_verification_to_currencies', 216),
(416, '2019_07_19_080537_add_columns_who_pays_commission_to_direction_exchange', 217),
(417, '2019_07_19_085052_add_column_commission_merchant_to_currencies', 218),
(418, '2019_07_19_091318_change_type_commission_merchant_percent_to_currencies', 219),
(419, '2019_07_21_143130_add_column_is_allow_split_to_code_currency', 220),
(420, '2019_07_23_084152_add_column_token_value_to_code_currency', 221),
(421, '2019_07_23_210927_add_column_is_send_fee_network_to_currencies', 222),
(422, '2019_07_23_223251_add_column_fee_sent_to_wallets_addresses', 223),
(423, '2019_07_25_080443_add_column_fee_no_verified_merchant_to_currencies', 224),
(424, '2019_07_25_134305_add_column_class_name_to_merchants', 225),
(425, '2019_07_25_205538_add_column_summa_not_fee_to_reserve_log', 226),
(426, '2019_07_25_205735_add_column_summa_with_fee_to_reserve_log', 226),
(427, '2019_07_25_213852_add_column_next_checkout_at_to_tasks', 226),
(428, '2019_07_28_091432_add_column_hold_delay_to_currencies', 227),
(429, '2019_08_28_113735_add_column_income_outcome_to_tasks', 228),
(430, '2019_08_31_063128_add_column_provider_to_wallets_addresses', 229),
(431, '2019_08_31_073249_add_column_custom_field_prefix_to_requisites', 230),
(432, '2019_08_31_083246_create_requirements_table', 231),
(433, '2019_09_01_191859_create_logs_404_table', 232),
(434, '2019_09_01_192106_add_column_message_to_logs_404', 233),
(435, '2019_09_02_105516_create_merchant_account_table', 234),
(436, '2019_09_04_101305_add_column_provider_to_merchant_account', 235),
(437, '2019_09_05_162508_create_qiwi_currencies_table', 236),
(438, '2019_09_06_224531_add_column_is_enabled_merchant_to_requisites', 237),
(439, '2019_09_07_111925_create_yandex_currencies_table', 238),
(440, '2019_09_11_133856_add_column_custom_field_comment_alt_to_requisites', 239),
(441, '2019_09_13_085214_add_column_is_marquee_to_notices_exchange', 240),
(442, '2019_09_14_193809_add_column_num_auth_to_users', 241),
(443, '2019_09_16_221532_create_autosender_payment_table', 242),
(444, '2019_09_17_092625_add_column_double_withdrawal_to_tasks', 243),
(445, '2019_10_02_083733_add_column_telegram_to_users', 244),
(446, '2019_10_03_073341_add_column_id_currency_to_reserve_request', 245),
(447, '2019_10_05_075529_create_currencies_commands', 246),
(448, '2019_10_05_081343_add_column_id_currency_to_currencies_commands', 247),
(449, '2019_10_05_213258_add_column_description_to_currency_fields', 248),
(450, '2019_10_05_232438_add_column_phone_to_tasks', 249),
(451, '2019_10_06_105509_add_column_payout_commission_amount_to_currencies', 250),
(452, '2019_10_06_194016_add_columns_is_review_icon_to_links_reviews', 251),
(453, '2019_10_06_194959_add_column_description_to_links_reviews', 252),
(454, '2019_10_08_195259_create_links_review_groups_table', 253),
(455, '2019_10_08_200227_add_column_id_group_to_links_reviews', 254),
(456, '2019_10_08_230139_add_column_id_auto_reserve_to_currencies', 255),
(457, '2019_10_10_195911_create_proxies_qiwi_table', 256),
(458, '2019_10_10_203828_add_column_id_proxy_to_requisites', 257),
(459, '2019_10_10_220151_create_proxies_payment_table', 258),
(460, '2019_10_11_113204_add_column_drains_to_requisites', 259),
(461, '2019_10_11_124513_create_log_drain_table', 260),
(462, '2019_10_11_124800_add_column_from_value_to_log_drain', 261),
(463, '2019_10_11_125018_add_column_id_task_to_log_drain', 262),
(464, '2019_10_11_184842_add_column_freeze_scam_to_tasks_info', 263),
(465, '2019_10_12_214638_create_live_notification_table', 264),
(466, '2019_10_13_075207_add_column_id_client_to_tasks_chat', 265),
(467, '2019_10_13_094256_add_column_is_drain_mertchant_to_tasks', 266),
(468, '2019_10_13_210128_add_column_big_id_to_withdrawal_request', 266),
(469, '2019_10_17_130759_add_column_user_agent_to_proxies_payment', 267),
(470, '2019_10_18_193613_create_competitor_links_table', 268),
(471, '2019_10_18_193712_create_competitor_rates_table', 269),
(472, '2019_10_18_220118_add_column_number_format_to_competitor_rates', 270),
(473, '2019_10_18_222659_add_column_id_competitor_to_direction_exchange', 271),
(474, '2019_10_18_223449_add_column_enable_competitors_to_direction_exchange', 272),
(475, '2019_10_19_071753_create_competitor_rates_log_table', 273),
(476, '2019_10_19_084447_add_column_exchange_in_out_to_competitor_rates', 274),
(477, '2019_10_19_155051_add_column_sorting_to_faq_category', 275),
(478, '2019_10_19_165008_add_column_status_to_faq', 276),
(479, '2019_10_19_165139_add_column_sorting_to_faq', 277),
(480, '2019_10_19_203855_create_reviews_table', 278),
(481, '2019_10_20_063552_add_column_user_agent_to_reviews', 279),
(482, '2019_10_20_063639_add_column_id_admin_to_reviews', 280),
(483, '2019_10_20_100535_create_selected_courses_table', 281),
(484, '2019_10_20_133254_add_column_number_format_to_selected_courses', 282),
(485, '2019_10_20_183151_add_column_provider_to_users', 283),
(486, '2019_10_21_070312_create_social_auth_system_table', 284),
(487, '2019_10_21_190046_change_type_to_blacklist_order', 285),
(488, '2019_10_21_195444_add_column_is_bestchange_to_blacklist_order', 286),
(489, '2019_10_21_211845_create_contacts_table', 287),
(490, '2019_10_21_212111_add_column_sorting_to_contacts', 288),
(491, '2019_10_21_212247_add_column_is_home_to_contacts', 289),
(492, '2019_10_21_214327_change_type_to_contacts', 290),
(493, '2019_10_23_195411_create_backup_codes_table', 291),
(494, '2019_10_23_202738_add_column_num_to_backup_codes', 292),
(495, '2019_10_23_214323_add_column_is_download_codes_to_users', 293),
(496, '2019_10_24_075039_add_column_backup_code_secret_to_users', 294),
(497, '2019_10_24_111024_create_social_reviews_table', 295),
(498, '2019_10_24_124356_change_type_type_to_social_reviews', 296),
(499, '2019_10_25_062143_create_collaboration_pr_table', 297),
(500, '2019_10_25_065708_add_column_is_button_to_collaboration_pr', 298),
(501, '2019_10_25_065724_add_column_is_button_to_contacts', 298),
(502, '2019_10_26_074248_create_info_statistics_table', 299),
(503, '2019_10_26_081610_change_type_to_info_statistics_table', 300),
(504, '2019_10_26_185348_add_column_accoutn_number_to_info_statistics', 301),
(505, '2019_10_26_204735_add_columns_to_users', 302),
(506, '2019_10_27_070853_add_column_link_to_info_statistics', 303),
(507, '2019_10_28_215528_add_columns_courses_in_out_to_direction_exchange', 304),
(508, '2019_10_28_220117_create_course_logs_table', 305),
(509, '2019_10_29_064341_add_column_bc_min_max_to_direction_exchange', 306),
(511, '2019_10_29_085617_add_columns_bc_id_new_rate_to_direction_exchange', 307),
(512, '2019_10_29_113749_add_column_bc_add_course_direction_exchange', 308),
(513, '2019_10_29_181144_add_column_napsip_to_direction_exchange', 309),
(514, '2019_10_29_195839_add_column_not_ip_to_direction_exchange', 310),
(515, '2019_10_30_124856_add_column_cr_max_min_to_direction_exchange', 311),
(516, '2019_10_30_142730_add_column_cr_fields_to_direction_exchange', 312),
(517, '2019_10_30_150046_add_column_max_percent_partner_to_direction_exchange', 313),
(518, '2019_10_30_190314_add_column_xml_juridical_to_direction_exchange', 314),
(519, '2019_10_30_195538_add_column_languages_to_direction_exchange', 315),
(520, '2019_10_30_205437_add_column_is_hidden_noauth_user_to_direction_exchange', 316),
(521, '2019_10_30_220421_add_column_is_hidden_not_locale_to_direction_exchange', 317),
(522, '2019_10_30_222414_add_column_sorting_tariffs_to_direction_exchange', 318),
(523, '2019_10_30_223658_add_column_sorting_tariffs_to_currencies', 319),
(524, '2019_10_31_162940_add_column_pay_num_to_tasks', 320),
(525, '2019_10_31_203032_add_column_type_to_reviews', 321),
(526, '2019_10_31_212033_add_column_device_to_direction_exchange', 322),
(527, '2019_10_31_213532_add_column_is_hidden_not_device_to_direction_exchange', 323),
(528, '2019_11_01_221959_add_column_is_autopay_off_tasks', 324),
(529, '2019_11_03_165336_create_tasks_shots_table', 325),
(530, '2019_11_05_062706_add_column_bc_new_commission_to_direction_exchange', 326),
(531, '2019_11_06_202434_create_currencies_notification_table', 327),
(532, '2019_11_10_081729_add_column_is_other_service_to_merchants', 328),
(533, '2019_11_10_082410_add_column_service_name_to_wallets_addresses', 329),
(534, '2019_11_10_111305_create_task_private_hash_table', 330),
(535, '2019_11_10_203944_add_column_min_out_amount_verification_to_currencies', 331),
(536, '2019_11_10_224006_create_logs_email_table', 332),
(537, '2019_11_11_105817_add_column_is_out_enabled_verification_to_currencies', 333),
(538, '2019_11_12_070443_add_column_bestchange_bl_to_direction_exchange', 334),
(539, '2019_11_12_121035_add_column_bestchange_step_to_direction_exchange', 335),
(540, '2019_11_12_131907_add_column_bc_enable_your_to_direction_exchange', 336),
(541, '2019_11_12_180452_add_column_limit_min_course_to_direction_exchange', 337),
(542, '2019_11_12_192745_add_column_bestchange_range_to_direction_exchange', 338),
(543, '2019_11_12_201931_add_column_other_comm_amount_to_direction_exchange', 339),
(544, '2019_11_13_064424_add_column_max_amount_month_to_currencies_autopayment', 340),
(545, '2019_11_13_075352_add_column_delay_to_currencies_autopayment', 341),
(546, '2019_11_14_073553_add_column_autodel_status_taks_to_direction_exchange', 342),
(547, '2019_11_14_120030_add_column_is_vip_client_to_users', 343),
(548, '2019_11_16_082626_add_column_bestchange_max_reserve_to_direction_exchange', 344),
(549, '2019_11_16_181746_create_your_exchange_group_table', 345),
(550, '2019_11_16_182002_add_column_id_group_to_your_exchange', 346),
(551, '2019_11_17_082413_create_bestchange_data_log_table', 347),
(552, '2019_11_17_184248_create_e_voucher_codes_table', 348),
(553, '2019_11_17_213529_create_currencies_log_table', 349),
(554, '2019_11_19_000627_add_column_is_hidden_tariffs_to_direction_exchange', 350),
(555, '2019_11_19_093737_add_column_is_holding_direction_to_direction_exchange', 351),
(556, '2019_11_19_105458_add_column_is_automatic_to_bestchange_rates', 352),
(557, '2019_11_20_081620_add_column_bestchange_range_to_to_direction_exchange', 353),
(558, '2019_11_20_085624_add_column_rl_id_your_exchange_to_direction_exchange', 354),
(559, '2019_11_24_103259_add_column_reserve_limit_to_direction_exchange', 355),
(560, '2019_11_24_115016_add_column_is_email_verification_to_direction_exchange', 356),
(561, '2019_11_24_121027_add_column_number_transaction_to_direction_exchange', 357),
(562, '2019_11_24_121742_add_column_num_transaction_to_tasks_info', 358),
(563, '2019_11_24_163045_add_column_other_limit_to_direction_exchange', 359),
(564, '2019_11_24_191521_add_column_auto_del_order_day_to_direction_exchange', 360),
(565, '2019_11_24_213620_create_referrals_info_logs', 361),
(566, '2019_12_01_170534_add_column_currency_sign_payout_to_tasks_info', 362),
(567, '2019_12_05_171739_add_column_text_to_payment_explorer', 362),
(568, '2019_12_06_072806_add_column_max_limit_in_reserve_to_currencies', 362),
(569, '2019_12_06_091255_change_type_bestchange_step_to_direction_exchange', 362),
(570, '2019_12_06_142239_add_column_code_base_to_code_currency', 362),
(571, '2019_12_07_092033_add_column_user_agent_to_user_auth', 362),
(572, '2019_12_07_234636_add_column_token_decimal_to_code_currency', 362),
(573, '2019_12_07_234901_change_type_token_decimal_to_code_currency', 362),
(574, '2019_12_12_073739_add_column_merchant_provider_to_tasks', 362),
(575, '2019_12_14_000001_create_personal_access_tokens_table', 362),
(576, '2019_12_16_220833_add_column_income_outcome_5_to_tasks', 362),
(577, '2020_01_02_120627_create_parser_api_keys_table', 362),
(578, '2020_01_02_170107_create_gateways_merchants_table', 362),
(579, '2020_01_02_231912_create_gateways_payments_table', 362),
(580, '2020_01_03_170911_add_column_to_security_hash_to_gateways_merchants', 362),
(581, '2020_01_04_003546_add_column_is_check_from_shot_to_gateways_merchants', 362),
(582, '2020_01_04_113745_add_column_comment_to_gateways_merchants', 362),
(583, '2020_01_04_161024_add_column_is_deny_ip_address_to_gateways_merchants', 362),
(584, '2020_01_04_171400_create_file_parser_groups_table', 362),
(585, '2020_01_04_172628_create_file_parser_rates_table', 362),
(586, '2020_01_04_183340_add_column_id_file_parser_rate_to_direction_exchange', 362),
(587, '2020_01_04_211928_add_column_hour_limit_order_tocurrencies', 362),
(588, '2020_01_04_232646_add_column_referral_link_id_to_tasks', 362),
(589, '2020_01_05_081557_create_bestchange_currencies_table', 362),
(590, '2020_01_05_095359_create_requisites_fields_table', 362),
(591, '2020_01_06_221954_add_column_profit_percent_reserve_to_currencies', 362),
(592, '2020_01_06_222820_create_reserve_log_profit_table', 362),
(593, '2020_01_07_122646_create_operation_level_groups_table', 362),
(594, '2020_01_07_155257_create_operation_levels_table', 362),
(595, '2020_01_09_160914_add_column_is_not_pair_to_bestchange_rates', 362),
(596, '2020_01_09_173024_add_column_id_bs_alt_parser_to_direction_exchange', 362),
(597, '2020_01_09_192018_add_column_bs_alt_parser_course_to_direction_exchange', 362),
(598, '2020_01_09_193144_is_enable_alt_bs_parser_to_direction_exchange', 362),
(599, '2020_01_09_205731_create_bestchange_parser_error_table', 362),
(600, '2020_01_09_210039_add_column_id_direction_exchange_to_bestchange_parser_error', 362),
(601, '2020_01_10_183324_add_column_is_disable_bs_error_to_direction_exchange', 362),
(602, '2020_01_11_083922_change_type_amount_2_to_reserve_log_profit', 362),
(603, '2020_01_11_092711_add_column_amount_usd_to_reserve_log_profit', 362),
(604, '2020_01_13_183703_create_tasks_fields_table', 362),
(605, '2020_01_13_230315_add_column_type_field_to_tasks_fields', 362),
(606, '2020_01_13_235114_create_currency_fields_relationships_table', 362),
(607, '2020_01_14_000438_add_column_type_field_to_currency_fields_relationships', 362),
(608, '2020_01_14_100900_remove_column_outcome_to_tasks', 362),
(609, '2020_01_14_102253_add_column_view_to_parser_api_keys', 362),
(610, '2020_01_14_112109_create_direction_exchange_error_log_table', 362),
(611, '2020_01_14_114116_add_column_level_risk_to_direction_exchanger_error_log', 362),
(612, '2020_01_15_092957_create_user_balance_log_table', 362),
(613, '2020_01_15_093404_add_column_route_type_to_user_balance_log', 362),
(614, '2020_01_17_073911_change_type_name_to_menu', 362),
(615, '2020_01_18_102305_add_column_is_new_user_to_tasks', 362),
(616, '2020_01_27_212657_create_task_card_details_table', 362),
(617, '2020_01_27_225607_add_column_phone_and_url_to_task_card_details', 362),
(618, '2020_01_27_234335_add_column_is_card_detail_to_currencies', 362),
(619, '2020_01_29_001109_add_column_max_register_blockchain_to_gateways_payments', 362),
(620, '2020_01_31_134240_add_column_max_display_reserve_to_currencies', 362),
(621, '2020_01_31_222803_add_columns_is_black_list_to_withdrawal_request', 362),
(622, '2020_02_03_211632_add_column_notify_statusss_to_tasks_info', 362),
(623, '2020_02_05_091258_change_type_page_title_to_pages', 362),
(624, '2020_02_05_223942_create_affiliate_settings_table', 362),
(625, '2020_02_05_232146_add_column_profit_partner_to_direction_exchange', 362),
(626, '2020_02_06_223239_add_column_is_backup_to_users', 362),
(627, '2020_02_15_002103_add_column_min_confirm_to_gateways_payments', 362),
(628, '2020_02_17_222606_create_update_systems_table', 362),
(629, '2020_02_18_230710_change_type_column_page_content_to_pages', 362),
(630, '2020_02_23_210640_add_column_id_proxy_to_gateways_payments', 362),
(631, '2020_02_24_095954_create_debtors_table', 362),
(632, '2020_02_24_101821_add_column_is_freeze_local_to_tasks_info', 362),
(633, '2020_02_26_120552_create_admin_desktops_table', 362),
(634, '2020_02_26_124451_create_admin_desktop_gadgets_table', 362),
(635, '2020_02_27_011033_add_column_column_id_to_admin_desktop_gadgets', 362),
(636, '2020_02_27_074123_add_column_id_user_to_admin_desktop_gadgets', 362),
(637, '2020_02_27_080153_add_column_hash_id_to_admin_desktop_gadgets', 362),
(638, '2020_02_27_114938_add_columns_flex_nums_to_admin_gadgets', 362),
(639, '2020_03_01_192450_add_column_notes_to_direction_exchange', 362),
(640, '2020_03_01_194627_add_column_note_tx_to_tasks_info', 362),
(641, '2020_03_03_180055_create_merchant_transaction_ids_table', 362),
(642, '2020_03_07_172020_create_favorites_links_table', 362),
(643, '2020_03_07_202634_add_column_id_user_to_favorites_links', 362),
(644, '2020_03_08_095558_create_internal_accounts_table', 362),
(645, '2020_03_08_095858_add_column_balance_to_internal_accounts', 362),
(646, '2020_03_08_154439_create_history_internal_accounts_table', 362),
(647, '2020_03_11_182041_add_column_x19_mode_to_direction_exchange', 362),
(648, '2020_03_11_194019_create_directions_fields_table', 362),
(649, '2020_03_11_200306_create_direction_fields_relationships_table', 362),
(650, '2020_03_12_104211_create_tasks_direction_fields_table', 362),
(651, '2020_03_12_210045_add_column_is_email_verification_modal_to_direction_exchange', 362),
(652, '2020_03_12_214910_add_column_is_merchant_fround_to_currencies', 362),
(653, '2020_03_14_092804_add_column_is_email_verification_modal_to_currencies', 362),
(654, '2020_03_14_102453_add_column_type_finished_order_to_tasks', 362),
(655, '2020_03_21_232437_add_column_commission_merchant_currency_to_currencies', 362),
(656, '2020_03_21_234926_add_column_is_pay_commission_to_gateways_merchants', 362),
(657, '2020_03_22_081433_add_column_unique_security_code_to_tasks', 362),
(658, '2020_03_28_094420_add_column_comment_to_gateways_payments', 362),
(659, '2020_03_31_001848_add_column_is_in_banner_to_currencies', 362),
(660, '2020_04_07_110002_create_currencies_analytics_table', 362),
(661, '2020_04_07_110248_add_column_id_currency_to_currencies_analytics', 362),
(662, '2020_04_07_122326_add_column_in_out_orders_to_currencies_analytics', 362),
(663, '2020_04_07_173238_add_column_is_blank_to_notices_exchange', 362),
(664, '2020_04_07_221109_add_column_views_to_news', 362),
(665, '2020_04_08_213847_add_column_kunacode_to_tasks', 362),
(666, '2020_04_08_214158_add_column_provider_id_to_history_excode', 362),
(667, '2020_04_10_002908_change_type_logo_to_payments', 362),
(668, '2020_04_18_001701_add_column_source_name_to_bestchange_rates', 362),
(669, '2020_04_26_182637_add_column_flex_num3_to_admin_desktops', 362),
(670, '2020_04_26_183048_add_column_flex_nums_to_admin_desktops', 362),
(671, '2020_05_04_161332_create_log_error_merchants_table', 362),
(672, '2020_05_05_162328_add_column_is_auto_check_modal_to_currencies', 362),
(673, '2020_05_05_162415_add_column_is_autopay_modal_to_tasks', 362),
(674, '2020_05_05_232250_change_type_custom_field_comment_to_requisites', 362),
(675, '2020_05_08_163228_add_column_is_autopay_limit_to_tasks', 362),
(676, '2020_05_30_214628_add_column_id_edit_data_manager_to_tasks', 362),
(677, '2020_06_02_164512_add_column_is_report_referral_data_to_users', 362),
(678, '2020_06_08_2227091_create_hosts_table', 362),
(679, '2020_06_08_2227092_create_checks_table', 362),
(680, '2020_06_13_174912_create_cashback_error_log_table', 362),
(681, '2020_07_02_115432_add_column_commission_payment_currency_to_currencies', 362),
(682, '2020_07_05_213001_add_column_is_fixed_reserve_to_reserves', 362),
(683, '2020_08_14_194403_change_type_add_course1_to_direction_exchange', 362),
(684, '2020_08_30_123059_add_column_text_to_cashback_error_log', 362),
(685, '2020_09_04_232155_add_column_telegram_id_to_tasks', 362),
(686, '2020_09_05_101950_add_column_telegram_id_to_reviews', 362),
(687, '2020_09_07_222402_add_columns_recount_to_currencies', 362),
(688, '2020_09_07_224945_add_column_max_amount_newbie_direction_exchange', 362),
(689, '2020_09_08_102627_add_column_is_star_to_reserves', 362),
(690, '2020_09_15_084624_add_column_notice_out_to_currencies', 362),
(691, '2020_09_15_085436_add_column_out_price_fee_to_tasks', 362),
(692, '2020_09_16_220221_add_column_is_ban_order_data_to_tasks', 362),
(693, '2020_09_18_100221_create_admin_filters_user_table', 362),
(694, '2020_09_18_130244_create_admin_filter_header_table', 362),
(695, '2020_09_19_075222_add_column_is_common_to_admin_filter_header', 362),
(696, '2020_09_21_002100_create_iex_config_table', 362),
(697, '2020_09_22_102116_add_column_memo_id_to_wallets_addresses', 362),
(698, '2020_09_25_090107_create_email_templates_table', 362),
(699, '2020_09_25_172606_create_template_type_events_table', 362),
(700, '2020_09_26_000528_add_column_id_event_type_to_email_templates', 362),
(701, '2020_09_26_141435_add_column_email_to_to_email_templates', 362),
(702, '2020_09_26_142438_add_column_template_to_email_templates', 362),
(703, '2020_10_02_142238_create_tasks_history_operators_table', 362),
(704, '2020_10_02_142842_add_column_id_from_manager_to_tasks_history_operators', 362),
(705, '2020_10_02_145453_add_column_id_main_operator_to_tasks', 362),
(706, '2020_10_02_150923_add_column_count_change_operator_to_tasks_info', 362),
(707, '2020_10_16_102546_add_column_uuid_to_failed_jobs', 362),
(708, '2020_10_31_184026_create_tasks_comments_table', 362),
(709, '2020_11_06_004142_add_column_day_limit_merchant_to_gateways_merchants', 362),
(710, '2020_11_06_010242_add_column_amount_fault_to_gateways_merchants', 362),
(711, '2020_11_06_092038_add_column_day_limit_amount_merchant_to_gateways_merchants', 362),
(712, '2020_11_06_100036_add_column_manual_pay_order_to_gateways_payments', 362),
(713, '2020_11_08_080511_add_column_is_random_view_shot_to_currencies_table', 362),
(714, '2020_11_09_104143_add_column_is_enable_merchant_button_to_gateways_merchants', 362),
(715, '2020_11_28_105312_add_columns_recount_unique_tome_to_currencies', 362),
(716, '2020_11_29_101150_create_tasks_profits_table', 362),
(717, '2020_12_05_105459_add_column_desc_exchange_to_currencies', 362),
(718, '2021_01_14_083737_add_column_provider_to_oauth_clients', 362),
(719, '2021_02_09_091620_add_columns_in_out_amount_usd_to_currencies_analytics', 362),
(720, '2021_02_16_122100_add_column_rate_speed_to_reviews', 362),
(721, '2021_02_17_095346_add_column_id_widget_to_admin_desktop_gadgets', 362),
(722, '2021_03_02_084714_add_column_slug_name_to_news', 362),
(723, '2021_03_02_210603_add_column_pay_adapter_code_to_currencies', 362),
(724, '2021_03_08_142215_create_user_wallet_historicals_table', 362),
(725, '2021_04_25_171050_add_columns_aml_in_out_to_currencies', 362),
(726, '2021_04_25_191120_create_amlbot_histories', 362),
(727, '2021_04_25_200701_add_columns_is_amlbot_in_to_tasks_info', 362),
(728, '2021_04_28_124333_add_column_view_balance_reward_to_withdrawal_request', 362),
(729, '2021_06_01_100957_create_contests_table', 362),
(730, '2021_06_01_125405_create_contests_conditions_table', 362),
(731, '2021_06_01_181730_create_contests_users_table', 362),
(732, '2021_06_01_211943_add_percent_to_contests_table', 362),
(733, '2021_06_03_122006_add_column_priority_to_group_parser_exchange', 362),
(734, '2021_06_07_103159_create_gateways_table', 362),
(735, '2021_06_27_105740_add_column_method_pay_to_gateways_payments', 362),
(736, '2021_06_28_002227_add_column_method_pay_to_gateways_merchants', 362),
(737, '2021_07_03_192024_add_column_country_code_to_gateways_payments', 362),
(738, '2021_07_04_000006_add_column_num_request_to_gateways_payments', 362),
(739, '2021_07_04_101725_add_column_priority_fee_to_gateways_payments', 362),
(740, '2021_07_07_104320_drop_column_generate_address_to_requisites', 362),
(741, '2021_07_10_082548_add_column_fixed_fee_to_gateways_merchants', 362),
(742, '2021_07_11_001308_add_column_auto_del_order_time_to_direction_exchange', 362),
(743, '2021_07_12_070857_add_column_course_value_to_direction_exchange', 362),
(744, '2021_07_13_200129_add_column_requisites_receive_to_tasks', 362),
(745, '2021_07_13_213800_delete_columns_to_code_currencies', 362),
(746, '2021_07_13_214314_add_column_is_trash_to_code_currency', 362),
(747, '2021_07_13_223722_delete_status_to_filter_currency', 362),
(748, '2021_07_13_230255_add_column_exchange_rate_status_to_your_exchange', 362),
(749, '2021_07_14_004909_delete_columns_to_requisites', 362),
(750, '2021_07_14_071602_add_columns_in_custom_fields_to_currencies', 362),
(751, '2021_07_14_233658_add_columns_out_pay_min_amount_to_currencies', 362),
(752, '2021_07_14_234017_delete_columns_many_to_currencies', 362),
(753, '2021_07_15_150744_delete_id_group_menu_to_menu', 362),
(754, '2021_07_15_175140_add_column_is_allow_amount_space_to_currencies', 362),
(755, '2021_07_15_181426_delete_columns_to_direction_exchange', 362),
(756, '2021_07_15_185128_create_histories_codes_table', 362),
(757, '2021_07_15_213223_change_column_excode_to_tasks', 362),
(758, '2021_07_17_174209_add_column_type_order_to_gateways_payments', 362),
(759, '2021_07_18_000752_add_column_is_auto_take_fee__to_gateways_payments', 362),
(760, '2021_07_24_220048_add_column_ids_merchant_to_currency', 362),
(761, '2021_07_25_005902_add_columns_id_merchant_id_pay_to_tasks', 362),
(762, '2021_07_25_084440_add_column_ids_direction_fields_to_direction_exchange', 362),
(763, '2021_07_25_123106_add_column_comment_to_requisites_fields', 362),
(764, '2021_07_25_132656_delete_columns_custom_fields_to_requisites', 362),
(765, '2021_07_25_163327_change_type_field_to_tasks_fields', 362),
(766, '2021_07_28_190209_add_column_is_pending_blockchain_hash_to_tasks_info', 362),
(767, '2021_07_29_090054_add_column_volume_to_usd_to_gateways_payments', 362),
(768, '2021_07_29_184435_change_column_is_disable_to_gateway_merchants', 362),
(769, '2021_07_31_012559_delete_column_pos_to_bestchange_rates', 362),
(770, '2021_08_04_011528_add_column_class_style_to_tasks_comments', 362),
(771, '2021_08_13_071224_drop_column_uri_to_referral_programs', 362),
(772, '2021_08_22_130433_add_column_min_confirm_to_gateways_merchants', 362),
(773, '2021_09_04_130132_create_merchants_has_currencies_table', 362),
(774, '2021_09_04_145759_drop_column_ids_merchant_to_currencies', 362),
(775, '2021_09_04_171559_add_column_total_usd_to_gateways_merchants', 362),
(776, '2021_09_05_085104_add_column_is_config_done_to_gateways_merchants', 362),
(777, '2021_09_06_075300_add_column_is_auto_check_pay_to_tasks', 362),
(778, '2021_09_07_085521_add_column_internal_rate_to_code_currency', 362),
(779, '2021_09_07_130058_add_column_add_to_course_to_code_currency', 362),
(780, '2021_09_07_155558_add_column_is_import_to_code_currency', 362),
(781, '2021_09_08_090411_create_directions_has_fields_table', 362),
(782, '2021_09_08_091454_drop_column_ids_custom_fields_to_direction_exchange', 362),
(783, '2021_09_11_105749_change_column_id_to_currency_fields', 362),
(784, '2021_09_11_105831_create_currency_in_has_fields_table', 362),
(785, '2021_09_11_114232_create_currency_out_has_fields_table', 362),
(786, '2021_09_11_115208_drop_column_in_custom_fields_to_currencies', 362),
(787, '2021_09_11_222346_add_column_is_import_to_payments', 362),
(788, '2021_09_12_172714_add_column_security_options_to_gateways', 362),
(789, '2021_09_13_070846_add_column_remove_spaces_to_currency_fields', 362),
(790, '2021_09_13_084813_drop_column_name_alt_to_currency_fields', 362),
(791, '2021_09_13_160819_drop_column_name_alt_to_payments', 362),
(792, '2021_09_15_071723_add_column_code_in_to_parser_exchange', 362),
(793, '2021_09_15_133756_create_currency_requisites_has_fields_table', 362),
(794, '2021_09_15_140354_change_type_name_to_requisites_fields', 362),
(795, '2021_09_15_161724_add_column_description_to_directions_fields', 362),
(796, '2021_10_01_143016_add_column_provider_url_to_group_parser_exchange', 362),
(797, '2021_10_02_114558_add_column_is_not_update_to_parser_exchange', 362),
(798, '2021_10_14_093357_add_column_button_name_to_contests', 362),
(799, '2021_10_18_100350_change_type_columns_to_currencies', 362),
(800, '2021_10_18_215714_add_column_is_merchant_fround_to_gateways_merchants', 362),
(801, '2021_10_21_101818_add_column_has_status_to_code_currency', 362),
(802, '2021_11_08_140312_add_column_is_bot_to_links_reviews', 362),
(803, '2022_03_11_111846_create_requisites_has_fields_table', 362),
(804, '2022_03_11_153017_create_getblockbot_histories_table', 362),
(805, '2022_03_11_153055_add_columns_is_getblockbot_in_to_tasks_info', 362),
(806, '2022_03_11_172809_add_columns_is_getblockbot_in_to_currencies', 362),
(807, '2022_05_17_092959_create_cities_table', 362),
(808, '2022_05_18_084918_create_directions_has_cities_table', 362),
(809, '2022_05_18_121716_add_column_city_id_to_tasks_info', 362),
(810, '2022_05_18_183458_add_column_provider_id_to_group_parser_exchange', 362),
(811, '2022_05_18_183908_add_column_provider_id_to_parser_api_keys', 362),
(812, '2022_05_29_114045_add_column_user_browser_to_users', 362),
(813, '2022_06_06_095242_add_column_tech_name_to_direction_exchange', 362),
(814, '2022_06_11_145948_add_column_last_order_at_to_direction_exchange', 362),
(815, '2022_06_11_172644_add_column_user_id_to_pages', 362),
(816, '2022_06_12_010900_add_column_order_queue_to_tasks', 362),
(817, '2022_06_12_011104_add_column_is_mass_payouts_gateways_payments', 362),
(818, '2022_06_12_080157_add_column_id_payment_gateway_to_tasks', 362),
(819, '2022_07_02_230733_add_column_desc_exchange_to_direction_exchange', 362),
(820, '2022_07_04_114312_create_requisites_info_fields_table', 362),
(821, '2022_07_04_114546_create_requisites_has_info_fields_table', 362),
(822, '2022_07_10_205445_create_contests_has_contests_users_table', 362),
(823, '2022_07_11_064619_create_contests_faq_table', 362),
(824, '2022_07_19_162323_create_partner_parser_groups', 362),
(825, '2022_07_19_163645_create_partner_parser_rates_table', 362),
(826, '2022_07_19_173233_add_column_partner_id_to_partner_parser_rates', 362),
(827, '2022_07_19_210756_add_column_id_partner_parser_rate_to_direction_exchange', 362),
(828, '2022_07_19_212312_add_column_number_format_to_partner_parser_rates', 362),
(829, '2022_07_20_084400_add_column_last_updated_at_to_partner_parser_rates', 362),
(830, '2022_07_24_124615_add_column_created_user_id_to_currencies', 362),
(831, '2022_07_25_095946_add_column_last_updated_at_to_group_parser_exchange', 362),
(832, '2022_07_25_160635_add_column_proxy_id_to_group_parser_exchange', 362),
(833, '2022_07_25_164949_add_column_last_imported_at_to_group_parser_exchange', 362),
(834, '2022_07_26_151929_change_default_status_to_parser_exchange', 362),
(835, '2022_07_28_111422_add_indexs_to_tasks', 362),
(836, '2022_07_28_111429_add_indexs_to_user', 362),
(837, '2022_07_28_111636_add_indexs_to_direction_exchange', 362),
(838, '2022_07_31_191308_add_column_section_to_admin_filters_user', 362),
(839, '2022_07_31_191651_add_column_type_filter_to_admin_filter_header', 362),
(840, '2022_08_10_083841_create_bs_bestchange_histories_table', 362),
(841, '2022_08_14_150313_create_promo_codes_table', 362),
(842, '2022_08_14_171451_add_column_id_promo_code_to_tasks', 362),
(843, '2022_08_15_101132_add_column_is_fire_to_currencies', 362),
(844, '2022_08_18_174915_change_typ_name_to_partners', 362),
(845, '2022_08_18_225950_add_column_colors_to_contests', 362),
(846, '2022_08_24_090001_change_column_title_to_faq', 362),
(847, '2022_09_04_121407_change_type_name_value_to_contacts', 362),
(848, '2022_09_13_131053_change_type_desc_link_reviews', 362),
(849, '2022_09_14_151220_add_column_is_fixed_to_notices_exchange', 362),
(850, '2022_09_16_170627_change_type_title_to_advantage', 362),
(851, '2022_09_16_193955_create_direction_exchange_merchants', 362),
(852, '2022_09_28_092836_add_column_name_to_promo_codes', 362),
(853, '2022_10_02_142352_add_column_code_to_parser_exchange', 362),
(854, '2022_10_02_144508_create_parser_formula_table', 362),
(855, '2022_10_02_225346_add_column_id_parser_formula_rate_to_direction_exchange', 362),
(856, '2022_10_03_020154_create_parser_formula_coefficient_table', 362),
(857, '2022_10_03_021536_add_column_is_coefficient_to_parser_formula_rates', 362),
(858, '2022_10_03_090947_add_column_title_to_parser_formula_rates', 362),
(859, '2022_10_06_113128_change_type_instructions_to_direction_exchange', 362),
(860, '2022_10_08_121119_create_currencies_networks_table', 362),
(861, '2022_10_08_185825_create_directions_has_networks_table', 362),
(862, '2022_10_10_102840_add_column_user_style_to_users', 362),
(863, '2022_10_12_094811_add_columns_direction_to_promo_codes', 362),
(864, '2022_10_13_112501_add_column_network_name_to_tasks_info', 362),
(865, '2022_10_17_104249_change_type_first_char_to_currencies', 362),
(866, '2022_10_18_111851_drop_column_first_char_alt_to_currencies', 362),
(867, '2022_10_18_112916_rename_column_first_char_to_currencies', 362),
(868, '2022_10_18_123205_add_column_field_comments_to_currencies', 362),
(869, '2022_10_20_172612_add_columns_id_order_detail_to_direction_notification', 362),
(870, '2022_12_10_180543_add_column_api_key_to_users', 362),
(871, '2022_12_11_165318_add_column_tech_currency_name_to_currencies', 362),
(872, '2022_12_20_192501_change_type_account_number_field_to_currencies', 362),
(873, '2022_12_20_212121_add_column_button_create_order_to_currencies', 362),
(874, '2022_12_21_073924_add_column_button_create_order_text_to_currencies', 362),
(875, '2023_01_19_104847_add_column_tx_hash_to_getblockbot_histories', 362);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(876, '2023_01_19_120855_create_aml_analysis_logs_table', 362),
(877, '2023_01_19_124037_add_column_aml_riskscore_to_tasks_info', 362),
(878, '2023_01_19_124738_add_column_is_getblockbot_tx_in_to_currencies', 362),
(879, '2023_01_24_151221_add_column_is_iex_to_blacklist_order', 362),
(880, '2023_01_24_151925_add_column_hash_id_to_blacklist_order', 362),
(881, '2023_01_27_112429_add_column_colors_to_notices_exchange', 362),
(882, '2023_01_30_104340_add_column_kyc_enabled_to_currencies', 362),
(883, '2023_01_31_110119_create_direction_requisites_table', 362),
(884, '2023_01_31_182820_create_directions_has_requisites_table', 362),
(885, '2023_01_31_184401_add_column_id_direction_requisites_to_tasks', 362),
(886, '2023_01_31_201337_create_tasks_requisites_table', 362),
(887, '2023_01_31_202617_add_column_type_output_requisites_to_direction_exchange', 362),
(888, '2023_02_01_011419_add_column_formalization_text_to_direction_exchange', 362),
(889, '2023_02_01_014140_add_column_formalization_text_to_currencies', 362),
(890, '2023_02_01_024210_add_column_exchange_fee_to_gateways_merchants', 362),
(891, '2023_02_04_141904_create_task_single_log_confirm_table', 362),
(892, '2023_02_05_095356_add_columns_is_aml_check_cost_reserve_to_currencies', 362),
(893, '2023_02_05_101347_add_column_int_status_verification_card_to_tasks', 362),
(894, '2023_02_05_130611_create_currencies_info_table', 362),
(895, '2023_02_05_222203_create_geo_country_list_table', 362),
(896, '2023_02_05_225526_create_direction_has_forbidden_countries_table', 362),
(897, '2023_02_06_000934_create_direction_has_allowed_countries_table', 362),
(898, '2023_02_07_093706_add_column_network_code_to_currencies', 362),
(899, '2023_02_09_132920_create_verification_card_category_table', 362),
(900, '2023_02_09_132929_create_verification_card_instructions_table', 362),
(901, '2023_02_11_021346_create_merchant_transaction_hash_table', 362),
(902, '2023_02_11_083240_add_column_is_wait_hash_pay_to_tasks_info', 362),
(903, '2023_02_11_083822_create_pay_transaction_hash_table', 362),
(904, '2023_02_12_094143_create_direction_exchange_modes_table', 362),
(905, '2023_02_12_094225_create_directions_has_modes_table', 362),
(906, '2023_02_13_011426_create_links_footer_groups_table', 362),
(907, '2023_02_13_011633_create_links_footers_table', 362),
(908, '2023_02_13_152210_add_column_give_price_merhant_to_tasks', 362),
(909, '2023_02_14_080835_add_column_order_id_to_tasks', 362),
(910, '2023_02_14_161436_add_column_aml_text_to_currencies', 362),
(911, '2023_02_14_173111_add_column_aml_analyses_count_to_currencies', 362),
(912, '2023_02_14_174112_add_column_price_to_aml_analysis_logs', 362),
(913, '2023_02_14_180900_add_column_aml_day_limit_count_to_currencies', 362),
(914, '2023_02_17_001304_add_column_min_count_exchanges_client_to_direction_exchange', 362),
(915, '2023_02_17_072833_add_column_getblockbot_tx_in_amount_to_currencies', 362),
(916, '2023_02_17_204125_change_column_type_page_title_to_pages', 362),
(917, '2023_02_17_213000_add_index_public_id_to_tasks', 362),
(918, '2023_02_18_102751_create_whitebit_logs_table', 362),
(919, '2023_02_18_180633_add_column_amlbot_tx_in_amount_to_currencies', 362),
(920, '2023_02_18_183553_add_column_id_currency_to_aml_analysis_logs', 362),
(921, '2023_02_18_192345_add_column_aml_service_name_to_currencies', 362),
(922, '2023_03_17_191608_add_column_instruction_exchange_to_currencies', 362),
(923, '2023_03_18_003311_add_column_order_button_i_pay_to_direction_exchange', 362),
(924, '2023_03_18_102800_add_column_blockchain_network_congestion_to_currencies', 362),
(925, '2023_03_31_195258_add_column_hide_check_balance_to_gateways_payments', 362),
(926, '2023_04_05_104512_add_column_is_allow_telegram_bot_to_direction_exchange', 362),
(927, '2023_04_06_133413_add_column_recalculated_at_to_tasks_info', 362),
(928, '2023_04_06_190726_create_reserves_files_table', 362),
(929, '2023_04_06_191017_create_reserves_files_groups_table', 362),
(930, '2023_04_06_200016_add_column_id_file_reserve_to_reserves', 362),
(931, '2023_04_06_223518_add_column_id_server_reserve_to_reserves', 362),
(932, '2023_04_07_013252_add_column_is_kyc_checkbox_to_currencies', 362),
(933, '2023_04_07_120748_add_column_is_unique_shot_to_requisites', 362),
(934, '2023_04_07_125130_add_column_transfer_to_account_tasks', 362),
(935, '2023_04_07_142935_create_direction_exchange_cities_table', 362),
(936, '2023_04_07_170937_add_column_min_price_max_price_to_direction_exchange_cities', 362),
(937, '2023_04_07_195102_add_column_id_country_to_direction_exchange_cities', 362),
(938, '2023_04_07_215432_add_column_country_id_name_to_tasks_info', 362),
(939, '2023_04_08_125936_add_column_manual_rate_value_to_direction_exchange', 362);

-- --------------------------------------------------------

--
-- Структура таблицы `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\Models\\User'),
(2, 1, 'App\\Models\\User');

-- --------------------------------------------------------

--
-- Структура таблицы `monitors`
--

CREATE TABLE `monitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uptime_check_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `look_for_string` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uptime_check_interval_in_minutes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '5',
  `uptime_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not yet checked',
  `uptime_check_failure_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uptime_check_times_failed_in_a_row` int(11) NOT NULL DEFAULT '0',
  `uptime_status_last_change_date` timestamp NULL DEFAULT NULL,
  `uptime_last_check_date` timestamp NULL DEFAULT NULL,
  `uptime_check_failed_event_fired_on_date` timestamp NULL DEFAULT NULL,
  `uptime_check_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'get',
  `certificate_check_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `certificate_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not yet checked',
  `certificate_expiration_date` timestamp NULL DEFAULT NULL,
  `certificate_issuer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_check_failure_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cr_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) DEFAULT NULL,
  `parent_url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `views` int(11) NOT NULL DEFAULT '0',
  `slug_name` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `notices_exchange`
--

CREATE TABLE `notices_exchange` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `color` int(191) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `is_enabled_schedule` int(11) NOT NULL DEFAULT '0',
  `from_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_alt` text COLLATE utf8mb4_unicode_ci,
  `is_marquee` int(11) NOT NULL DEFAULT '0',
  `is_blank` int(11) NOT NULL DEFAULT '0',
  `is_fixed` int(11) NOT NULL DEFAULT '0',
  `title` text COLLATE utf8mb4_unicode_ci,
  `title_button` text COLLATE utf8mb4_unicode_ci,
  `icon_notice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bg_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `operation_levels`
--

CREATE TABLE `operation_levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_level_group` int(11) NOT NULL DEFAULT '0',
  `id_operator` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `operation_level_groups`
--

CREATE TABLE `operation_level_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `from_limit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_limit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `page_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_content` longtext COLLATE utf8mb4_unicode_ci,
  `page_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`page_id`, `page_title`, `page_content`, `page_slug`, `created_at`, `updated_at`, `deleted_at`, `user_id`) VALUES
(1, '{\"ru\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u043b\\u0430 \\u043e\\u0431\\u043c\\u0435\\u043d\\u0430\",\"en\":null}', '{\"ru\":\"<p>\\u0422\\u0435\\u043a\\u0441\\u0442 \\u043f\\u0440\\u0430\\u0432\\u0438\\u043b\\u0430....<\\/p>\",\"en\":null}', 'pravila-obmena', '2017-10-24 20:54:45', '2023-04-09 11:07:10', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pages_static`
--

CREATE TABLE `pages_static` (
  `id` int(11) NOT NULL,
  `title_about` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_about` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_visible` int(11) NOT NULL DEFAULT '0',
  `text_contact` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_faq` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_faq` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_exchange_rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages_static`
--

INSERT INTO `pages_static` (`id`, `title_about`, `text_about`, `about_visible`, `text_contact`, `title_faq`, `text_faq`, `text_exchange_rules`, `created_at`, `updated_at`) VALUES
(1, 'Добро пожаловать', '<p>Компания &mdash; сервис обмена электронных валют, созданный специально для профессиональных игроков &mdash; трейдеров бирж крипто-валют. Данный сервис предназначен для тех, кто предпочитает не ограничиваться только заработками от торговли на одной конкретной бирже, получая дополнительную прибыль от межбиржевого арбитража.</p>\n\n<p>Нас не интересует как Вы зарабатываете ту или иную валюту. Наша основная задача &mdash; оперативно и без лишних хлопот предоставить Вам выгодный обмен.&nbsp;</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &laquo;Сайт&raquo; делает основную ставку на анонимность операций, а еще наши менеджеры крайне вежливы и приятны в общении, убедитесь в этом сами!</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; Все данные и операции проходят по защищенному протоколу &laquo;Comodo Secure&raquo;, что обеспечивает Вам гарантию, что они не достанутся третьим лицам. Мы тщательно следим за качеством сервиса, поэтому настоятельно просим оставлять отзывы после каждого обмена.</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; Также, обращаем ваше внимание, что все обмены осуществляются строго через контактные телефоны или форму общения с оператором у нас на сайте. Не используйте мессенджеры без подтверждения через форму, дабы избежать действий злоумышленников.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><b>Работаем круглосуточно</b><br />\nНаши операторы обрабатывают заявки в ручном режиме.<br />\nВремя обработки заявок от 5 до 20 минут.</p>\n\n<p><b>У нас Вы можете обменять:</b>&nbsp;QIWI, Яндекс. Деньги, Perfect Money, Payeer, Bitcoin, BTC-e USD, BTC-e RUR, Сбербанк, Альфа Банк, Тинькофф Банк, ВТБ24 Банк.</p>\n\n<p>&nbsp;</p>', 0, '<h2>Техническая поддержка</h2>\r\n\r\n<p>Если у Вас возникли вопросы технического или финансового плана, напишите нам и мы поможем Вам в решении вашего вопроса. Мы отвечаем на вопросы в течение 15-60 минут, в зависимости от загрузки сервиса.</p>\r\n\r\n<p>Время работы: <strong>Круглосуточно</strong></p>', '', '<h3>1. Как быстро происходит обмен?</h3>\r\n\r\n<ul>\r\n	<li>Обмен между Bitcoin, Ethereum, Litecoin, Monero, Dash, Advanced Cash, WEX, Bitcoin.co.id, EXMO, Perfect Money и Яндекс Деньгами происходит в автоматическом режиме, то есть мгновенно. Однако случаются ситуации, когда Perfect Money или OkPay могут заморозить вашу операцию. В этом случае для проведения заявки потребуется несколько часов.</li>\r\n	<li>Переводы с Яндекс Денег на счета, на которые вы ранее не переводили средств через наш сервис, удерживаются в течение 48 часов и проходят автоматически по истечении данного срока. Последующие переводы на этот счет проходят мгновенно.</li>\r\n</ul>\r\n\r\n<h3>2. Могу ли я отказаться от обмена?</h3>\r\n\r\n<p>В том случае, если операция обмена не завершена &mdash; можете. Вам будут возвращены ваши деньги за вычетом комиссии платежной системы, в которой производилась оплата, то есть возврат будет произведен за ваш счет.</p>\r\n\r\n<h3>3. Что делать, если я оформил и оплатил заявку, а деньги все еще не пришли?</h3>\r\n\r\n<ol>\r\n	<li>Ознакомьтесь с ответом на вопрос #1 текущего раздела.</li>\r\n	<li>Внимательно прочитайте письмо, присланное вам на e-mail сразу после оформления заявки.</li>\r\n	<li>Воспользовавшись любым удобным для вас способом, сообщите оператору номер своей заявки, и он в кратчайшие сроки проверит ее.</li>\r\n</ol>\r\n\r\n<h3>4. Что делать, если я оплатил заявку, но её статус остался &laquo;ожидает оплаты клиентом&raquo;.</h3>\r\n\r\n<p>Данная ситуация возникает в том случае, когда платежная система передает информацию о платеже с задержкой. Вам необходимо подождать около 5 минут, после чего обновить страницу. Если изменений не произошло, свяжитесь со службой поддержки.</p>', '<p><strong>1. Общие положения</strong></p>\r\n\r\n<p>Настоящее соглашение (далее по тексту Соглашение) описывает правила и условия, на основании которых предоставляются услуги обменного сервиса OwlPay и является официальной письменной публичной офертой адресованной физическим и юридическим лицам (далее по тексту - Пользователь), заключить Соглашение о предоставлении услуг сервисом OwlPay на изложенных ниже условиях.<br />\r\nПеред тем как воспользоваться услугами сервиса OwlPay , Пользователь обязан ознакомиться в полном объеме с условиями &laquo;Соглашения о предоставлении услуг сервисом OwlPay&raquo;.<br />\r\nИспользование услуг сервиса OwlPay возможно только при условии, что Пользователь принимает все условия Соглашения.<br />\r\nДействующая версия Соглашения расположена для публичного доступа на интернет-сайте сервиса OwlPay.</p>\r\n\r\n<p><strong>2. Термины и определения, используемые в Соглашении</strong></p>\r\n\r\n<p><em>Сервис&nbsp;</em>OwlPay&nbsp;<em>(Сервис)</em>&nbsp;&ndash; система предоставления интернет-услуг по обмену, продаже и покупке электронных валют.<br />\r\n<em>Интернет-сайт Сервиса</em>&nbsp;&ndash; www.owlpay.cc<br />\r\n<em>Пользователь</em>&nbsp;&ndash;&nbsp; любое физическое или юридическое лицо, использующее услуги Сервиса OwlPay и осуществившее акцепт Соглашения в соответствии с его условиями.</p>\r\n\r\n<p><em>Партнер</em>&nbsp;&ndash; лицо, оказывающее Сервису услуги по привлечению Пользователей, условия оказания которых описаны в настоящем Соглашении.<br />\r\n<em>Платежная система</em>&nbsp;&ndash;&nbsp; программный продукт, созданный третьей стороной, представляющий собой механизм реализации учета денежных (электронные деньги) и/или иных обязательств, оплату товаров и услуг в сети Интернет, а также организацию взаиморасчетов между пользователями.</p>\r\n\r\n<p>Основными платежными системами в рамках настоящего Соглашения являются:<br />\r\nPerfectMoney, e-Voucher, Bitcoin, AdvCash, OkPay, Payeer, QIWI, Yandex.Money, BTC-E, Exmo и другие. Точный список Платежных систем указан на Интернет-сайте Сервиса. &nbsp;</p>\r\n\r\n<p><em>Клиент платежной системы</em>&nbsp;&ndash; лицо, заключившее соглашение с соответствующей платежной системой на приобретение имущественных прав требования к ней, измеряемых в условных единицах, принятых в соответствующей платежной системе.<br />\r\n<em>Электронная валюта</em>&nbsp;&ndash; денежное и/или иное обязательство между разработчиком данной валюты и ее пользователем, выраженное цифровым способом.</p>\r\n\r\n<p><em>Платеж/Операция</em>&nbsp;&ndash;&nbsp; перевод электронной и/или иной валюты от плательщика к получателю.<br />\r\n<em>Заявка</em>&nbsp;&ndash; выражение намерения Пользователя воспользоваться одной из услуг, предлагаемых Сервисом OwlPay , путем заполнения электронной формы через Интернет-сайт Сервиса, на условиях, описанных в Соглашении и указанных в параметрах этой Заявки.<br />\r\n<em>Верификация карты</em>&nbsp;- это проверка принадлежности карты (или счета) её владельцу. Условия проверки принадлежности устанавливает Сервис, производиться единовременно для каждого нового счета (карты) клиента.<br />\r\n<em>Исходная валюта</em>&nbsp;&ndash; электронная валюта, которую Пользователь желает продать или обменять.<br />\r\n<em>Исходный счет</em>&nbsp;&ndash; номер кошелька или любое другое обозначения счета Пользователя в Платежной системе, с которого была отправлена Исходная валюта.</p>\r\n\r\n<p><em>Полученная валюта</em>&nbsp;&ndash; электронная валюта, которую Пользователь получает в результате продажи или обмена Исходной валюты.<br />\r\n<em>Счет получателя</em>&nbsp;&ndash; номер кошелька или любое другое обозначения счета Пользователя в Платежной системе, на который будет отправлена Полученная валюта.</p>\r\n\r\n<p><em>Резерв валюты</em>&nbsp;&ndash; имеющийся в распоряжении Сервиса OwlPay , на момент создания Заявки, объем определенной Электронной валюты.</p>\r\n\r\n<p><em>Обмен валюты</em>&nbsp;&ndash;&nbsp; обмен электронной валюты одной платежной системы на электронную валюту другой платежной системы.</p>\r\n\r\n<p><em>Курс</em>&nbsp;&mdash; стоимостное соотношение двух электронных валют при их обмене.</p>\r\n\r\n<p><em>Резервы электронных валют</em>&nbsp;&ndash; суммы имеющихся в наличии у Сервиса Электронных валют или денежных средств для совершения услуг. Суммы резервов указаны на Интернет-сайте Сервиса на главной странице.</p>\r\n\r\n<p><strong>3. Предмет Соглашения и порядок вступления его в силу</strong></p>\r\n\r\n<ul>\r\n	<li>3.1. Предметом настоящего Соглашения является предоставление Пользователю Сервисом OwlPay следующих услуг:</li>\r\n	<li>3.1.1. обмен электронной валюты;</li>\r\n	<li>3.1.2. продажа Пользователю электронной валюты;</li>\r\n	<li>3.1.3. покупка у Пользователя электронной валюты.</li>\r\n	<li>3.2. Акцепт (принятие условий) настоящего Соглашения (Оферты) производится Пользователем в случае:</li>\r\n	<li>3.2.1. Направления Заявки на совершение одной из услуг, предлагаемых Сервисом;</li>\r\n	<li>3.2.2. Регистрации на Интернет-сайте Сервиса.</li>\r\n	<li>3.3. При направлении Заявки акцептом публичной оферты признается совершение Пользователем действий по завершению формирования Заявки, подтверждающих его намерение совершить сделку с Сервисом на условиях, предложенных Сервисом. Дата и время акцепта, а также параметры условий заявки фиксируются Сервисом автоматически в момент завершения формирования заявки. Настоящее Соглашение вступает в силу в момент поступления от Пользователя на реквизиты Сервиса электронной валюты или денежных средств в сумме, предусмотренной параметрами Заявки.</li>\r\n	<li>3.4. В случае регистрации на Интернет-сайте Сервиса настоящее Соглашение вступает в действие в момент проставления галочки перед словами &laquo;Я согласен с условиями оферты&raquo; и нажатия кнопки &laquo;Зарегистрироваться&raquo;.</li>\r\n	<li>3.5. Срок действия договора устанавливается бессрочно до момента расторжения договора по инициативе одной из сторон на условиях указанных ниже.</li>\r\n</ul>\r\n\r\n<p><strong>4. Услуги и порядок их оказания</strong></p>\r\n\r\n<ul>\r\n	<li>4.1. Общий&nbsp;порядок оказания услуг Сервисом</li>\r\n	<li>4.1.1. Заказ услуг Сервисом OwlPay осуществляется Пользователем путем направления Заявки через Интернет-сайт Сервиса.</li>\r\n	<li>4.1.2. Управление процессом обмена или получение информации о ходе выполнения услуги Пользователем производятся при помощи соответствующего пользовательского интерфейса, расположенного на Интернет-сайте Сервиса.</li>\r\n	<li>4.1.3. Сервис OwlPay осуществляет исполнение Заявок на безотзывной основе в соответствии с условиями работы соответствующих Платежных систем. Особые условия работы с некоторыми Платежными системами указаны ниже.</li>\r\n	<li>4.1.4. Сервис OwlPay не несет ответственности за действия Платежной системы перед ее Клиентом. Права и обязанности платежной системы и ее Клиента регулируются условиями предоставления услуг соответствующих Платежных систем.</li>\r\n	<li>4.1.5. Воспользовавшись услугами Сервиса OwlPay , Пользователь подтверждает, что законно владеет и распоряжается денежными средствами и электронной валютой, участвующими в соответствующем Платеже.</li>\r\n	<li>4.1.6. Пользователь обязуется самостоятельно исчислять и уплачивать все налоги, требуемые в соответствии с налоговым законодательством места нахождения Пользователя.</li>\r\n	<li>4.2. Услуга по Обмену Электронной валюты</li>\r\n	<li>4.2.1. Путем оформления Заявки Пользователь поручает, а Сервис OwlPay от своего имени и за счет Пользователя, совершает действия по обмену Электронной валюты одной Платежной системы (Исходная валюта) на Электронную валюту другой Платежной системы (Полученная валюта) выбранной Пользователем.</li>\r\n	<li>4.2.2. Пользователь обязуется перечислить (передать) Исходную валюту, в размере указанном в Заявке, а Сервис OwlPay , после получения соответствующей Электронной валюты, обязуется перечислить (передать) Пользователю Полученную валюту, рассчитанную по Курсу и в соответствии с тарифами Сервиса.</li>\r\n	<li>4.2.3. Размер вознаграждения Сервиса OwlPay отражается в Заявке и подтверждается Пользователем на одной из страниц пользовательского интерфейса при оформлении Заявки.</li>\r\n	<li>4.2.4. Обязанность Сервиса OwlPay по перечислению (передаче) Электронной валюты Пользователю считается исполненной в момент списания Электронной валюты в соответствующей Платежной системе со счета Сервиса OwlPay , что регистрируется в истории операций соответствующей Платежной системы.</li>\r\n	<li>4.3. Услуга по продаже Пользователю электронной валюты</li>\r\n	<li>4.3.1. Путем оформления Заявки Пользователь поручает, а Сервис OwlPay от своего имени и за счет Пользователя, совершает действия по приобретению и передаче Электронной валюты Пользователю.</li>\r\n	<li>4.3.2. Размер вознаграждения Сервиса OwlPay за указанные действия отражается в Заявке и подтверждается Пользователем на одной из страниц пользовательского интерфейса.</li>\r\n	<li>4.3.3. В течение 24 часов с момента получения денежных средств от Пользователя, в размере указанном в соответствующей Заявке, Сервис OwlPay обязан перечислить (передать) Полученную Электронную валюту на реквизиты и в размере, указанном Пользователем в Заявке.</li>\r\n	<li>4.3.4. Сервис OwlPay вправе отменить созданную Пользователем заявку на покупку Электронной валюты, если оплата по такой заявке не поступила на расчетный счет сервиса по истечению 24 часов с момента создания такой заявки.</li>\r\n	<li>4.3.5. Обязанность Сервиса OwlPay по перечислению (передаче) Полученной валюты Пользователю считается исполненной в момент списания Электронной валюты в соответствующей Платежной системе со счета Сервиса OwlPay , что регистрируется в истории операций соответствующей Платежной системы.</li>\r\n	<li>4.4. Услуга по покупке у Пользователя электронной валюты</li>\r\n	<li>4.4.1. Путем оформления Заявки Пользователь поручает, а Сервис OwlPay от своего имени и за счет Пользователя, покупает электронную валюту у Пользователя, а также совершает действия по передаче денежного эквивалента Пользователю в сумме, указанной в Заявке.</li>\r\n	<li>4.4.2. В течение 24 (Двадцати четырех) часов с момента получения Исходной валюты от Пользователя, в размере указанном в соответствующей Заявке, Сервис OwlPay обязан передать Пользователю денежный эквивалент перечисленной Электронной валюты, способом выбранным Пользователем при подаче Заявки.</li>\r\n	<li>4.4.3. Размер вознаграждения Сервиса OwlPay за указанные действия отражается в Заявке и подтверждается Пользователем на одной из страниц пользовательского интерфейса.</li>\r\n	<li>4.3.4. Обязанность Сервиса OwlPay по перечислению денежного эквивалента переданной Электронной валюты считается исполненной в момент списания соответствующей суммы со счета Сервиса OwlPay .</li>\r\n</ul>\r\n\r\n<p><strong>5. Дополнительные условия оказания услуг</strong></p>\r\n\r\n<ul>\r\n	<li>5.1. В случае непоступления от Пользователя к Сервису OwlPay Электронной валюты или денежных средств в течение 24 часов с момента совершения Заявки, Сервис имеет право аннулировать такую Заявку при ручной обработке поступивших платежей, и в течение 2 часов при автоматической обработке. Электронная валюта или денежные средства, поступившие после указанного выше срока, подлежат возврату на реквизиты плательщика. При осуществлении возврата, все комиссионные расходы на перевод производятся из поступивших средств за счет Пользователя.</li>\r\n	<li>5.2. В случае поступления от Пользователя к Сервису OwlPay Электронной валюты или денежных средств в сумме, отличающейся от указанной в Заявке, Сервис имеет право рассматривать это как распоряжение Пользователя сделать перерасчет по заявке согласно фактически поступившей сумме Электронной валюты. В случае, если количество поступившей Электронной валюты или денежных средств отличается от заявленного Пользователем более чем на 10%, Сервис может в одностороннем порядке аннулировать Заявку и возвратить поступившие средства на реквизиты плательщика. При осуществлении возврата, все комиссионные расходы на перевод средств производятся из поступивших средств за счет Пользователя.</li>\r\n	<li>5.3. В случае не исполнения Сервисом OwlPay условия отправки Электронной валюты или денежных средств по Заявке на реквизиты, указанные Пользователем, в течение 24 часов с момента отправки Заявки, Пользователь имеет права требовать возврата Электронной валюты или денежных средств в полном объеме, кроме случаев, указанных в настоящем Соглашении. Требование о возврате Электронной валюты или денежных средств может быть исполнено Сервисом только в том случае, если на момент получения такого требования денежный эквивалент не был отправлен на реквизиты, указанные Пользователем. Увеличение срока перечисления Электронной валюты или денежных средств может быть вызвано условиями обработки заявок отдельных Платежных систем, в указанном случае Сервис ответственности не несет и возврат не осуществляется.</li>\r\n	<li>5.4. Курс Электронной валюты фиксируется Системой не более, чем на 18 минут с момента совершения Заявки. В случае, если Пользователь произвел оплату по прошествии 18 минут, Система автоматически обновляет курс и блокирует возможность создания заявки. В случае блокировки создания заявки необходимо создать новую заявку по актуальному курсу, пропустив пункт с оплатой, а также обратиться в техническую поддержку Сервиса OwlPay .<br />\r\n	В случае с Банковскими платежами, курс обновляется всегда.</li>\r\n	<li>5.5. Особые условия некоторых Платежных систем:<br />\r\n	- Банковские платежи обрабатываются Сервисом в течение 24 часов, при необходимости Сервис может потребовать Верификацию карты(счета) клиента;<br />\r\n	- PerfectMoney может задерживать переводы более, чем на 24 часа;<br />\r\n	- Если Заявка на Bitcoin завершена, возврат денежных средств невозможен;<br />\r\n	- Если присланная сумма Bitcoin меньше 0,0010 ВТС, деньги назад не возвращаются<br />\r\n	- BTC-e: Если полученная сумма не вписывается в лимиты/резервы Сервиса, новый купон будет создан за вычетом минимальной комиссии Сервиса;<br />\r\n	- YandexMoney: На первый платеж от Незарегистрированного/Неверифицированного Пользователя установлена задержка на 48 часов.<br />\r\n	- Банковские переводы в направлении Visa/Mastercard RUB в большинстве случаев зачисляются мгновенно, но в некоторых случаях могут занимать до 5 банковских дней.</li>\r\n</ul>\r\n\r\n<p><strong>6. Стоимость услуг</strong></p>\r\n\r\n<ul>\r\n	<li>6.1.&nbsp; Cтоимость услуг Сервиса OwlPay устанавливается руководством Сервиса и публикуется на Интернет-сайте Сервиса.</li>\r\n	<li>6.2. Сервис OwlPay вправе самостоятельно изменять курсы обмена Электронных валют и взимаемых комиссионных в любое время в одностороннем порядке, о чем уведомляет Пользователей Сервиса предварительным размещением информации об этих изменениях на Интернет-сайте Сервиса.</li>\r\n	<li>6.3. В Заявке, создаваемой Пользователем на Интернет-сайте Сервиса, указывается Курс, размер комиссии, взимаемый соответствующей Платежной системой, за проведение Операции, размер вознаграждения Сервиса, способ Обмена, а также общая сумма перечисляемых денежных средств или электронной валюты.</li>\r\n	<li>6.4. Сервис OwlPay взимает стоимость своего вознаграждения в момент проведения соответствующей Операции.</li>\r\n	<li>6.5. Сервис предлагает следующую систему вознаграждений (только для зарегистрированных Пользователей). Узнайте статус своей скидки, и повысьте процент вознаграждения при обращении в техническую поддержку! Подробнее об условиях получения скидки на обмен:<br />\r\n	5% - за обмен от 0 до 1000 $<br />\r\n	10% - от 1000 до 10 000 $<br />\r\n	15% - от 10 000 и выше</li>\r\n</ul>\r\n\r\n<p>Процент вознаграждения рассчитывается от прибыли Сервиса по каждому доступному направлению обмена, по некоторым направлениям обмена комиссия сервиса может быть 0%, может быть меньше 0% (то есть комиссия отсутствует) или Сервис не получает выгоду от обмена, значит Сервис с этого обмена не имеет прибыли, в этих ситуациях вознаграждение не начисляется.</p>\r\n\r\n<p>&nbsp;</p>', '2023-04-09 11:12:02', '2017-10-10 17:51:13');

-- --------------------------------------------------------

--
-- Структура таблицы `parser_api_keys`
--

CREATE TABLE `parser_api_keys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_type` int(11) NOT NULL DEFAULT '0',
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `parser_exchange`
--

CREATE TABLE `parser_exchange` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `summa` varchar(250) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL DEFAULT '0',
  `id_type_price` int(11) NOT NULL DEFAULT '0',
  `number_format` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `value_default` int(11) NOT NULL DEFAULT '1',
  `summa_default` varchar(250) NOT NULL,
  `code_in` varchar(191) DEFAULT NULL,
  `code_out` varchar(191) DEFAULT NULL,
  `is_not_update` int(11) NOT NULL DEFAULT '0',
  `code` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `parser_exchange_log`
--

CREATE TABLE `parser_exchange_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_parser_exchange` int(11) NOT NULL DEFAULT '0',
  `in_price` double NOT NULL DEFAULT '0',
  `out_price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `parser_formula_coefficient`
--

CREATE TABLE `parser_formula_coefficient` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `summa` double(8,2) NOT NULL DEFAULT '0.00',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `parser_formula_rates`
--

CREATE TABLE `parser_formula_rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `exchange_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formula` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `summa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `number_format` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_coefficient` int(11) NOT NULL DEFAULT '0',
  `coefficient_formula` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coefficient_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `parser_type`
--

CREATE TABLE `parser_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `parser_type`
--

INSERT INTO `parser_type` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Максимальная цена сделки за 24 часа', 'high', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(2, 'Минимальная цена сделки за 24 часа', 'low', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(3, 'Средняя цена сделки за 24 часа', 'avg', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(4, 'Объем всех сделок за 24 часа', 'vol', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(5, 'Сумма всех сделок за 24 часа', 'vol_curr', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(6, 'Цена последней сделки', 'last_trade', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(7, 'Текущая максимальная цена покупки', 'buy_price', '2017-10-24 21:00:00', '2017-10-24 21:00:00'),
(8, 'Текущая минимальная цена продажи', 'sell_price', '2017-10-24 21:00:00', '2017-10-24 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `link` text COLLATE utf8mb4_unicode_ci,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `partner_parser_groups`
--

CREATE TABLE `partner_parser_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `partner_parser_rates`
--

CREATE TABLE `partner_parser_rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `partner_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `partner_id` int(11) NOT NULL DEFAULT '0',
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_format` int(11) NOT NULL DEFAULT '0',
  `last_updated_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `password_histories`
--

CREATE TABLE `password_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `logo` varchar(191) DEFAULT NULL,
  `enable_svg` int(11) NOT NULL DEFAULT '0',
  `svg_name` varchar(191) DEFAULT NULL,
  `svg_class` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `blockchain_url` varchar(191) DEFAULT NULL,
  `is_import` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_explorer`
--

CREATE TABLE `payment_explorer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_payment` int(11) NOT NULL DEFAULT '0',
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `payout_address`
--

CREATE TABLE `payout_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pay_transaction_hash`
--

CREATE TABLE `pay_transaction_hash` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `api_transfer_id` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `transaction_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `pending_order_status`
--

CREATE TABLE `pending_order_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_not_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pending_order_status`
--

INSERT INTO `pending_order_status` (`id`, `name`, `created_at`, `updated_at`, `is_not_delete`) VALUES
(1, 'Без причины', NULL, NULL, 0),
(2, 'Ждем поступления средств с NiceHash', NULL, NULL, 0),
(3, 'Вы указали не верный адрес биткоин кошелька. Пожалуйста пришлите верный адрес биткоин кошелька на почту.', NULL, NULL, 0),
(4, 'Вы указали не верные реквизиты получателя. Пожалуйста проверьте введённые данные и пришлите верные реквизиты на почту', NULL, NULL, 0),
(5, 'Данные найдены в списке должников', '2019-10-11 18:36:21', '2023-04-09 11:39:44', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_permissions_group` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `id_permissions_group`, `name`, `guard_name`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 2, 'currencies', 'web', 'Разрешить управление валютами в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и редактировать валюты в админпанели.', '2017-10-03 07:07:51', '2019-03-11 08:40:29'),
(2, 2, 'currency_codes', 'web', 'Разрешить управление кодами валют в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и редактировать коды валют в админпанели.	\r\n', '2017-10-03 07:08:04', '2017-12-25 06:42:04'),
(3, 2, 'currency_filters', 'web', 'Разрешить управление фильтрами валют в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и редактировать фильтры валют в админпанели.	\r\n', '2017-10-03 07:08:09', '2017-12-25 06:42:21'),
(4, 2, 'your_course', 'web', 'Разрешить управление своим курсом в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать свои курсы в админпанели.	\r\n', '2017-10-03 07:08:15', '2017-12-25 06:42:36'),
(5, 2, 'payment_system', 'web', 'Разрешить управление платежной системой\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и управлять платежными системами в админпанели.	\r\n', '2017-10-03 07:08:23', '2017-12-25 06:42:54'),
(6, 2, 'direction_exchange', 'web', 'Разрешить доступ к направлениям в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять направлениями обмена в админпанели.	\r\n', '2017-10-03 07:08:31', '2017-12-25 06:43:11'),
(7, 2, 'additional_fields', 'web', 'Разрешить управление дополнительными полями в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять дополнительными полями в админпанели.	\r\n', '2017-10-03 07:08:37', '2017-12-25 06:43:27'),
(8, 2, 'partners', 'web', 'Разрешить управление партнерами в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять партнерами в админпанели.	\r\n', '2017-11-14 00:05:39', '2017-12-25 06:43:38'),
(9, 2, 'user_discounts', 'web', 'Разрешить управление скидками пользователей в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять скидками пользователей в админпанели.	\r\n', '2017-11-22 09:19:12', '2017-12-25 06:43:56'),
(10, 2, 'reserves', 'web', 'Разрешить управление резервами в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять резервами в админпанели.	\r\n', '2017-11-22 09:38:57', '2017-12-25 06:44:07'),
(11, 2, 'claims_payment', 'web', 'Разрешить управление заявками на выплату в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять заявками на выплату в админпанели.	\r\n', '2017-12-25 06:40:15', '2017-12-25 06:44:26'),
(12, 3, 'allow_admin', 'web', 'Разрешить доступ в админпанель\r\n', 'Данная опция не предоставляет полного доступа ко всем разделам админпанели, а только разрешает добавление или редактирование новостей в админпанели.	\r\n', '2017-12-25 06:41:29', '2017-12-25 06:41:29'),
(13, 3, 'admin_settings', 'web', 'Разрешить управление настройками в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять настройками в админпанели.	\r\n', '2017-12-25 06:45:13', '2017-12-25 06:45:13'),
(14, 3, 'admin_users', 'web', 'Разрешить управление пользователями в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять пользователями в админпанели.	\r\n', '2017-12-25 06:45:22', '2017-12-25 06:45:22'),
(15, 3, 'admin_tasks', 'web', 'Разрешить управление заявками в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, принимать, и управлять заявками в админпанели.	\r\n', '2017-12-25 06:45:53', '2017-12-25 06:45:53'),
(16, 3, 'admin_news', 'web', 'Разрешить управление новостями в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, редактировать, создавать новости в админпанели.	\r\n', '2017-12-25 06:46:01', '2017-12-25 06:46:01'),
(17, 3, 'admin_pages', 'web', 'Разрешить управление страницами в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать, редактировать и удалять статические страницы в админпанели.	\r\n', '2017-12-25 06:46:12', '2017-12-25 06:46:12'),
(18, 3, 'admin_parser', 'web', 'Разрешить управление парсером в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять парсерами в админпанели.	\r\n', '2017-12-25 06:46:22', '2017-12-25 06:46:22'),
(19, 3, 'admin_affiliate_program', 'web', 'Разрешить управление партнерской программой в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и редактировать партнерскую программу в админпанели.	\r\n', '2017-12-25 06:46:43', '2017-12-25 06:46:43'),
(20, 3, 'admin_status_job', 'web', 'Разрешить управление статусом работы в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять статусом работы сервисама в админпанели.	\r\n', '2017-12-25 06:46:55', '2017-12-25 06:46:55'),
(21, 3, 'admin_faq', 'web', 'Разрешить управление FAQ в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать, редактировать вопросы и ответы в админпанели.	\r\n', '2017-12-25 06:47:04', '2017-12-25 06:47:04'),
(23, 3, 'admin_export_courses', 'web', 'Разрешить управление экспортом курсов в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять экспортом курсов в админпанели.	\r\n', '2017-12-25 06:47:34', '2017-12-25 06:47:34'),
(24, 2, 'admin_backup', 'web', 'Разрешить управление резервными копиями базы данных в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять резервными копиями в админпанели.', '2017-12-25 06:47:56', '2019-03-11 08:40:05'),
(25, 3, 'admin_statistics', 'web', 'Разрешить доступ к статистике в админпанели\r\n\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, получить полный доступ к статистике в админпанели.', '2017-12-28 17:53:11', '2017-12-28 17:53:11'),
(26, 3, 'admin_merchant', 'web', 'Разрешить доступ к мерчанту', 'Данная опция позволит пользователям, имеющим доступ в админпанель, получить полный контроль над платежными системами (история транзакций, баланс)', '2018-03-04 17:48:08', '2018-03-04 17:48:08'),
(27, 3, 'admin_analytics', 'web', 'Разрешить доступ к аналитической статистике', 'Данная опция позволит определенным администраторам, получить детальную аналитику сервиса', '2018-06-12 15:34:50', '2018-06-12 15:34:50'),
(28, 2, 'requisites_system', 'web', 'Разрешить управление реквизитами в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и управлять резвизитами в админпанели.	\r\n', '2018-06-20 19:49:49', '2018-06-20 19:49:49'),
(29, 2, 'course_designer', 'web', 'Разрешить управление конструктором курсов', 'Данная опция позволит пользователям, имеющим доступ в админпанель, полностью управлять курсами и направлениями.	\r\n', '2018-06-20 19:50:02', '2018-06-20 19:50:02'),
(30, 3, 'user_wallets', 'web', 'Разрешить доступ к счетам пользователей', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять счетами пользователей', '2019-02-08 03:22:45', '2019-02-08 03:22:45'),
(31, 3, 'admin_blockip', 'web', 'Разрешить управление фильтрами блокировки по IP или E-Mail в админпанели\r\n', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать, редактировать и удалять фильтры блокировки по IP или E-Mail.	', '2019-02-20 03:25:38', '2019-02-20 03:25:38'),
(32, 3, 'admin_cache', 'web', 'Разрешить очистку кэша', 'Данная опция позволит пользователям, имеющим доступ в админпанель, очищать кэш сайта', '2019-02-20 03:26:51', '2019-02-20 03:26:51'),
(33, 3, 'order_blacklist', 'web', 'Разрешить управление черным список в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять черными списками.', '2019-03-14 08:16:35', '2019-03-14 08:17:22'),
(34, 3, 'admin_roles', 'web', 'Разрешить управление группами прав в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять группами прав пользователей в админпанели.', '2019-04-04 14:16:54', '2019-04-04 17:09:22'),
(35, 2, 'requisites_blacklist', 'web', 'Разрешить управление черным списком реквизитов в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и управлять черным список резвизитов в админпанели.', '2019-05-14 12:39:09', '2019-05-14 12:39:09'),
(36, 3, 'admin_order_trashed', 'web', 'Разрешить управление удаленными заявками в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, принимать, и управлять удаленными заявками в админпанели.', '2019-05-14 13:24:57', '2019-05-14 13:24:57'),
(37, 3, 'admin_limit_editing_directions', 'web', 'Ограничить редактирование направлений', 'Данная опция позволит главным админам ограничивать доступ к редактированию некоторых направлений', '2019-05-24 03:44:48', '2019-05-24 03:44:48'),
(38, 2, 'order_whitelist', 'web', 'Разрешить управление белым список в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять белым списком.', '2019-05-27 08:28:21', '2019-05-27 08:28:21'),
(39, 3, 'admin_reviews', 'web', 'Разрешить управление отзывами', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять отзывами', '2019-10-20 05:01:57', '2019-10-20 05:01:57'),
(40, 2, 'admin_verification_card', 'web', 'Разрешить управление верификациями в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять верификациями в админпанели.', '2019-10-20 05:03:55', '2019-10-20 05:03:55'),
(41, 3, 'admin_selected_course', 'web', 'Разрешить управление избранными курсами', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять избранными курсами', '2019-10-20 13:10:43', '2019-10-20 13:10:43'),
(42, 3, 'admin_social_auth', 'web', 'Разрешить доступ к Системе авторизаций', 'Данная опция позволит пользователям, имеющим доступ в админпанель, получить полный контроль над ключами системы авторизаций', '2019-10-21 09:21:52', '2019-10-21 09:21:52'),
(43, 3, 'admin_contact', 'web', 'Разрешить управление контактами', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять контактами в админпанели.', '2019-10-21 19:38:06', '2019-10-21 19:38:06'),
(44, 3, 'admin_other_permissions', 'web', 'Разрешить управление дополнительными модулям', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять всеми дополнительными возможностями в админпанели.', '2019-11-11 20:07:30', '2019-11-11 20:07:30'),
(45, 3, 'admin_update_system', 'web', 'Разрешить управление системой обновления', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управления системой обновлений в админпанели.', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(46, 3, 'admin_order_limits', 'web', 'Разрешить управление лимитами для операторов в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и управлять лимитами обменов для операторов в админпанели.', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(47, 3, 'order_debtors', 'web', 'Разрешить управление должниками в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, управлять списками должников', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(48, 3, 'admin_access_cloudflare', 'web', 'Разрешить управление Cloudflare API в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать и редактировать правила в Cloudflare API в админпанели.', '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(49, 3, 'admin_access_global_control', 'web', 'Разрешить контроль над всеми функциями в админпанели', 'Данная опция позволит пользователям, имеющим доступ в админпанель, создавать общие группы, фильтры и т.д', '2023-04-09 10:47:38', '2023-04-09 10:47:38');

-- --------------------------------------------------------

--
-- Структура таблицы `permissions_group`
--

CREATE TABLE `permissions_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions_group`
--

INSERT INTO `permissions_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Базовое управление', '2017-12-24 21:00:00', '2017-12-24 21:00:00'),
(3, 'Админпанель', '2017-12-24 21:00:00', '2017-12-24 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `count_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Количество использований',
  `used` int(11) NOT NULL DEFAULT '0',
  `discount_percent` double(8,2) NOT NULL DEFAULT '0.00',
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permitted_directions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forbidden_directions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `proxies_payment`
--

CREATE TABLE `proxies_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `proxies_qiwi`
--

CREATE TABLE `proxies_qiwi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `qiwi_currencies`
--

CREATE TABLE `qiwi_currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `selected` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `referrals_info_logs`
--

CREATE TABLE `referrals_info_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_referral` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `referral_links`
--

CREATE TABLE `referral_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `referral_program_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `referral_links`
--

INSERT INTO `referral_links` (`id`, `user_id`, `referral_program_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Gz', '2019-11-28 22:23:48', '2019-11-29 14:16:03');

-- --------------------------------------------------------

--
-- Структура таблицы `referral_log`
--

CREATE TABLE `referral_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_referral_link` int(11) NOT NULL,
  `id_referral` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bonus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bonus_number` float NOT NULL DEFAULT '0',
  `fixed_bonus` float NOT NULL DEFAULT '0',
  `current_percent` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `referral_programs`
--

CREATE TABLE `referral_programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `percent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `count` int(11) NOT NULL DEFAULT '0',
  `is_reg` int(11) NOT NULL DEFAULT '0',
  `lifetime_minutes` int(11) NOT NULL DEFAULT '10080',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `referral_programs`
--

INSERT INTO `referral_programs` (`id`, `name`, `title`, `description`, `percent`, `type`, `count`, `is_reg`, `lifetime_minutes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'level1', 'после регистрации на сайте', 'После  регистрации на сайте', '0.5', 1, 0, 1, 10080, '2017-10-23 11:38:25', '2019-02-21 07:20:14', NULL),
(5, 'Professional', 'Индивидуальный', 'Эта партнерская программа доступна индивидуальным клиентам', '0.6', 2, 100000, 0, 10080, '2018-03-27 13:54:08', '2019-02-21 07:23:20', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `referral_relationships`
--

CREATE TABLE `referral_relationships` (
  `id` int(10) UNSIGNED NOT NULL,
  `referral_link_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `referral_statistics`
--

CREATE TABLE `referral_statistics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ref_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `is_archive` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cur_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cur_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_and_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requirements_verification`
--

CREATE TABLE `requirements_verification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites`
--

CREATE TABLE `requisites` (
  `id` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `account_number` varchar(255) DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `limit_day` int(11) DEFAULT NULL,
  `limit_month` int(11) DEFAULT NULL,
  `random` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `pick_certain` int(11) NOT NULL DEFAULT '0' COMMENT 'Подобрать реквизит после определенного условия',
  `change_amount` varchar(191) DEFAULT NULL COMMENT 'Сменить реквизит если сумма превышает:',
  `is_history` int(11) NOT NULL DEFAULT '0' COMMENT 'История П.С	',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `account_number_field` varchar(191) DEFAULT NULL,
  `history_at` timestamp NULL DEFAULT NULL,
  `limit_views` int(11) NOT NULL DEFAULT '0',
  `is_enabled_merchant` int(11) NOT NULL DEFAULT '0',
  `id_proxy` int(11) NOT NULL DEFAULT '0',
  `is_drain` int(11) NOT NULL DEFAULT '0',
  `max_wallet_limit` int(11) NOT NULL DEFAULT '0',
  `drain_comment` varchar(191) DEFAULT NULL,
  `drain_room` varchar(191) DEFAULT NULL,
  `is_unique_shot` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_blacklist`
--

CREATE TABLE `requisites_blacklist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `score` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_fields`
--

CREATE TABLE `requisites_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_group`
--

CREATE TABLE `requisites_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `requisites_group`
--

INSERT INTO `requisites_group` (`id`, `name`, `status`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'All', 1, 1, '2019-11-29 10:31:44', '2019-11-29 10:31:44');

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_has_fields`
--

CREATE TABLE `requisites_has_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_has_info_fields`
--

CREATE TABLE `requisites_has_info_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` int(11) NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_info_fields`
--

CREATE TABLE `requisites_info_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key_name` text COLLATE utf8mb4_unicode_ci,
  `value_name` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_list`
--

CREATE TABLE `requisites_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_requisites` int(11) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reserves`
--

CREATE TABLE `reserves` (
  `id` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `summa` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_code_currency` int(11) NOT NULL,
  `is_non_standard` int(11) DEFAULT '0',
  `black_amount` float DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_main` int(11) NOT NULL DEFAULT '0' COMMENT 'ID базового резерва',
  `id_group` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `is_fixed_reserve` int(11) NOT NULL DEFAULT '0',
  `is_star` int(11) NOT NULL DEFAULT '0',
  `id_file_reserve` int(11) NOT NULL DEFAULT '0',
  `id_server_reserve` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `reserves_alerts`
--

CREATE TABLE `reserves_alerts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_reserve` int(11) NOT NULL,
  `is_telegram` int(11) NOT NULL DEFAULT '0',
  `is_email` int(11) NOT NULL DEFAULT '0',
  `threshold` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `count_alert` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reserves_files`
--

CREATE TABLE `reserves_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_group` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `number_format` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reserves_files_groups`
--

CREATE TABLE `reserves_files_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reserve_group`
--

CREATE TABLE `reserve_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reserve_group`
--

INSERT INTO `reserve_group` (`id`, `name`, `id_user`, `status`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Общее', 0, 1, 1, '2019-05-12 16:17:37', '2019-05-12 17:36:54'),
(2, 'Банки', 0, 1, 2, '2019-05-12 17:38:47', '2019-05-12 17:40:38');

-- --------------------------------------------------------

--
-- Структура таблицы `reserve_log`
--

CREATE TABLE `reserve_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_reserve` int(11) NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL,
  `summa` float DEFAULT NULL,
  `commission` float DEFAULT NULL,
  `history` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `summa_not_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summa_with_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reserve_log_profit`
--

CREATE TABLE `reserve_log_profit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `percent` double(8,2) NOT NULL DEFAULT '0.00',
  `amount` double NOT NULL DEFAULT '0',
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount_usd` double(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reserve_request`
--

CREATE TABLE `reserve_request` (
  `id` int(11) NOT NULL,
  `id_direction_exchange` int(11) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_admin` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `telegram_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate_full` int(11) NOT NULL DEFAULT '0',
  `rate_speed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reward`
--

CREATE TABLE `reward` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reward_program_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reward`
--

INSERT INTO `reward` (`id`, `user_id`, `reward_program_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-11-28 22:23:49', '2019-11-28 22:23:49');

-- --------------------------------------------------------

--
-- Структура таблицы `reward_log`
--

CREATE TABLE `reward_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_reward_link` int(11) NOT NULL,
  `id_task` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bonus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bonus_string` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `percent` double DEFAULT '0',
  `is_cashback` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reward_programs`
--

CREATE TABLE `reward_programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_reg` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reward_programs`
--

INSERT INTO `reward_programs` (`id`, `name`, `percent`, `sign`, `is_reg`, `amount`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'reward1', '0.2', NULL, 1, 0, 'после регистрации на сайте', 'после регистрации на сайте', '2019-01-04 17:18:19', '2019-02-21 07:37:57', NULL),
(2, 'reward2', '0.5', NULL, 0, 2000, 'при обмене от 2 000$', 'при обмене от 2 000$', '2019-01-04 17:19:11', '2019-02-21 07:38:58', NULL),
(3, 'reward3', '0.9', NULL, 0, 10000, 'при обмене от 10 000$', 'при обмене от 10 000$', '2019-01-04 17:19:33', '2019-02-21 07:39:11', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_export` int(11) NOT NULL DEFAULT '0',
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_export`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Разработчик', 1, 'web', '2017-10-03 07:08:58', '2019-03-10 10:07:13'),
(2, 'Администратор', 0, 'web', '2017-10-03 07:09:53', '2017-10-03 07:09:53'),
(3, 'Модератор', 0, 'web', '2017-10-03 07:10:03', '2017-10-03 07:10:03'),
(4, 'Служба поддержки', 0, 'web', '2017-10-03 07:10:16', '2017-10-03 07:10:16'),
(6, 'Менеджер', 0, 'web', '2017-10-11 05:11:52', '2017-10-11 05:11:52');

-- --------------------------------------------------------

--
-- Структура таблицы `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(1, 2),
(5, 2),
(6, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(20, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(2, 3),
(6, 3),
(7, 3),
(2, 4),
(10, 6),
(11, 6),
(12, 6),
(15, 6),
(20, 6),
(25, 6),
(28, 6),
(37, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `selected_courses`
--

CREATE TABLE `selected_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_crypto_parser` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_format` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `sitename` varchar(255) DEFAULT NULL,
  `sitename_desc` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text,
  `description_home` text,
  `allow_reserve` int(11) NOT NULL,
  `allow_partner` int(11) NOT NULL DEFAULT '0',
  `block_visible_reserve` int(11) NOT NULL DEFAULT '0',
  `notify_change_status` int(11) NOT NULL,
  `auto_register` int(11) NOT NULL,
  `max_time_task` int(11) NOT NULL,
  `service_break` int(11) NOT NULL DEFAULT '0' COMMENT 'Технический перерыв',
  `service_break_time` int(11) NOT NULL DEFAULT '0',
  `service_break_interval` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `update_parser` timestamp NULL DEFAULT NULL,
  `language` int(11) NOT NULL DEFAULT '0',
  `multi_language` int(11) NOT NULL DEFAULT '0',
  `visible_description` int(11) NOT NULL DEFAULT '0',
  `visible_last_exchange` int(11) NOT NULL DEFAULT '0',
  `count_last_exchange` int(11) NOT NULL DEFAULT '2',
  `fund` varchar(11) NOT NULL DEFAULT '0',
  `email_support` varchar(191) DEFAULT NULL,
  `lead_time_task` int(11) NOT NULL DEFAULT '0',
  `network_bitcoin` int(11) NOT NULL DEFAULT '0' COMMENT 'Загруженность сети bitcoin	',
  `telegram_notify` int(11) NOT NULL DEFAULT '0',
  `telegram_chat_id` varchar(191) NOT NULL DEFAULT '0',
  `tg_stat_chat_id` varchar(191) NOT NULL,
  `show_task_page` int(11) NOT NULL DEFAULT '0' COMMENT 'Количество, отображаемых заявок на странице',
  `active_direction` text,
  `generate_min_price` int(11) DEFAULT '0',
  `position_bestchange` int(11) NOT NULL DEFAULT '0',
  `unlimited_reserve` int(11) NOT NULL DEFAULT '0',
  `analytics_email` varchar(191) DEFAULT NULL,
  `analytics_status` int(11) NOT NULL DEFAULT '0',
  `visible_my_order` int(11) NOT NULL DEFAULT '0',
  `disable_frame` int(11) NOT NULL DEFAULT '0',
  `reg_multi_ip` int(11) NOT NULL DEFAULT '0',
  `hide_exchange_guest` int(11) NOT NULL DEFAULT '0',
  `enable_cache` int(11) NOT NULL DEFAULT '0',
  `admin_allowed_ip` text,
  `offline_reason` text,
  `allow_filter_currency` int(11) NOT NULL DEFAULT '0',
  `contact_job` varchar(191) DEFAULT NULL,
  `contact_email` varchar(191) DEFAULT NULL,
  `contact_telegram` varchar(191) DEFAULT NULL,
  `contact_phone` varchar(191) DEFAULT NULL,
  `is_contact_job` int(11) NOT NULL DEFAULT '0',
  `is_contact_email` int(11) NOT NULL DEFAULT '0',
  `is_contact_telegram` int(11) NOT NULL DEFAULT '0',
  `is_contact_phone` int(11) NOT NULL DEFAULT '0',
  `monitoring_description` text,
  `captcha_restoring_password` int(11) NOT NULL DEFAULT '0',
  `order_priority` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `settings_referrals`
--

CREATE TABLE `settings_referrals` (
  `id` int(11) NOT NULL,
  `account_text` text,
  `partners_text` text,
  `block_text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings_referrals`
--

INSERT INTO `settings_referrals` (`id`, `account_text`, `partners_text`, `block_text`, `created_at`, `updated_at`) VALUES
(1, '<p>Обратите внимание! Реферальные начисляются от заработка сервиса с каждой вашей транзакции. В случаях, когда сервис не получает выгоду от обмена, реферальные не начисляются.</p>', '<p>Посоветуйте наш сервис друзьям и партнерам и зарабатывайте вместе с нами! Ваша реферальная ссылка находится в личном кабинете, в разделе &laquo;Реферальная программа&raquo;, скопируйте её и поделитесь с друзьями!</p>\n\n<p>Для вашего удобства в личном кабинете хранится история реферальных начислений, где отражено, сколько человек было зарегистрировано по вашей рекомендации, а так же информация о вашем кешбеке.</p>\n\n<p>Мы разработали поэтапную реферальную программу с хорошим вознаграждением:</p>', '', '2019-02-21 07:25:37', '2019-02-21 07:25:37');

-- --------------------------------------------------------

--
-- Структура таблицы `settings_reward`
--

CREATE TABLE `settings_reward` (
  `id` int(11) NOT NULL,
  `account_text` text,
  `partners_text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `block_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings_reward`
--

INSERT INTO `settings_reward` (`id`, `account_text`, `partners_text`, `created_at`, `updated_at`, `block_text`) VALUES
(1, '<p>Обратите внимание! Кэшбэк начисляется от заработка сервиса с каждой вашей транзакции. В случаях, когда сервис не получает выгоду от обмена, кэшбэк не начисляется.3</p>', '<p>Каждый зарегистрировавшийся на сайте пользователь сможет получать кэшбэк за обмен! Кэшбэк за обмен будет доступен Вам сразу после регистрации и составит 0,1%! При обмене свыше 2 000$, размер вознаграждения будет увеличен до 0,3%, а если вы произведете обмен на сумму более 10 000$, бонус будет увеличен до 0,6%!</p>\n\n<p>Как рассчитывается скидка по накопительной системе кэшбэк?3</p>', '2019-05-19 05:27:28', '2019-05-19 05:27:28', '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings_theme`
--

CREATE TABLE `settings_theme` (
  `id` int(10) UNSIGNED NOT NULL,
  `visible_fon` int(11) NOT NULL,
  `background_svg` int(11) NOT NULL DEFAULT '0',
  `start_sharing` int(11) DEFAULT '0' COMMENT 'Стиль кнопки "Начать обмен"',
  `fon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_position_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_position_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_repeat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_size` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_background` int(11) NOT NULL DEFAULT '0',
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logotype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `textlogotype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `select_logotype` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings_theme`
--

INSERT INTO `settings_theme` (`id`, `visible_fon`, `background_svg`, `start_sharing`, `fon`, `background_position_1`, `background_position_2`, `background_repeat`, `background_size`, `full_background`, `favicon`, `logotype`, `textlogotype`, `select_logotype`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1, '66AfW4ipllmv4v2.png', 'left', 'left', 'no-repeat', 'auto', 0, NULL, 'logo.png', 'OwlChange', 0, '2017-10-04 21:00:00', '2017-12-13 15:19:43');

-- --------------------------------------------------------

--
-- Структура таблицы `social_auth_system`
--

CREATE TABLE `social_auth_system` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `social_reviews`
--

CREATE TABLE `social_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `big_id` varchar(255) NOT NULL DEFAULT '0',
  `random_id` varchar(255) NOT NULL DEFAULT '0',
  `type` varchar(191) NOT NULL DEFAULT 'order',
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0',
  `id_payment_requisites` int(11) NOT NULL DEFAULT '0',
  `id_wallets_addresses` int(11) DEFAULT NULL,
  `give_price` varchar(255) DEFAULT NULL,
  `receiving_price` varchar(255) DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `ip` varchar(255) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '2',
  `from_shot` varchar(255) DEFAULT NULL,
  `to_shot` varchar(255) DEFAULT NULL,
  `passed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `started_at` timestamp NULL DEFAULT NULL,
  `description_receiving` text,
  `description_give` text,
  `discount1` double NOT NULL,
  `discount2` double NOT NULL,
  `markup1` double NOT NULL DEFAULT '0',
  `markup2` double NOT NULL DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `scans` int(11) NOT NULL DEFAULT '0' COMMENT 'Кто просматривает заявку',
  `id_payment_list` int(11) NOT NULL DEFAULT '0',
  `payment_address` varchar(191) DEFAULT NULL COMMENT 'Генерируемые номера счетов (Bitcoin, Eth ...)	',
  `rejection_reason` varchar(191) DEFAULT NULL COMMENT 'Причина отклонения заявки	',
  `start` int(11) NOT NULL DEFAULT '0' COMMENT 'Запускаем выполнение заявки (0 -> Не запущен процесс) (1 -> Запущен)	',
  `lead_time` timestamp NULL DEFAULT NULL COMMENT 'Время выполнения заявки со статусов "Ожидается выполнение"	',
  `message_success` text COMMENT 'Текст при успешном завершении заявки',
  `check_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Статус отправки чека клиенту	',
  `id_manager` int(11) NOT NULL DEFAULT '0' COMMENT 'ID менеджера, который выполнил заявку.',
  `course_display` varchar(191) DEFAULT NULL,
  `merchant_status` int(11) NOT NULL DEFAULT '0' COMMENT 'Был ли переход на мерчант',
  `income_code` text,
  `check_income_code` int(11) NOT NULL DEFAULT '0',
  `sender_fullname` varchar(191) DEFAULT NULL,
  `recipient_fullname` varchar(191) DEFAULT NULL,
  `is_frozen` int(11) NOT NULL DEFAULT '0' COMMENT 'Замороженные средства',
  `is_reserve` int(11) NOT NULL DEFAULT '0' COMMENT 'Каким образом будут сниматься резервы.	',
  `skip_reserve` int(11) NOT NULL DEFAULT '0' COMMENT 'Пропустить пополнение резерва	',
  `course_float` varchar(191) DEFAULT NULL,
  `payment_field` varchar(191) DEFAULT NULL,
  `category_reject` int(11) NOT NULL DEFAULT '0',
  `outcome_unk` varchar(191) DEFAULT NULL,
  `is_archive` int(11) NOT NULL DEFAULT '0',
  `operator_started_at` timestamp NULL DEFAULT NULL,
  `id_rejection_status` int(11) NOT NULL DEFAULT '0',
  `id_pending_status` int(11) NOT NULL DEFAULT '0',
  `is_favorites` int(11) NOT NULL DEFAULT '0',
  `is_spam` int(11) NOT NULL DEFAULT '0',
  `is_bot` int(11) NOT NULL DEFAULT '0',
  `register_tx` int(11) NOT NULL DEFAULT '0',
  `merchant_incomplete_payment` int(11) NOT NULL DEFAULT '0',
  `merchant_overpayment` int(11) NOT NULL DEFAULT '0',
  `is_hold` int(11) NOT NULL DEFAULT '0',
  `in_flow_funds` int(11) NOT NULL DEFAULT '0',
  `next_checkout_at` timestamp NULL DEFAULT NULL,
  `double_withdrawal` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(191) DEFAULT NULL,
  `is_drain_merchant` int(11) NOT NULL DEFAULT '0',
  `pay_num` int(11) NOT NULL DEFAULT '0',
  `is_autopay_off` int(11) NOT NULL DEFAULT '0',
  `merchant_provider` varchar(191) DEFAULT NULL,
  `id_referral_link` int(11) NOT NULL DEFAULT '0',
  `is_new_user` int(11) NOT NULL DEFAULT '0',
  `type_finished_order` int(11) NOT NULL DEFAULT '0',
  `unique_security_code` varchar(191) DEFAULT NULL,
  `kunacode` varchar(191) DEFAULT NULL,
  `is_autopay_modal` int(11) NOT NULL DEFAULT '0',
  `is_autopay_limit` int(11) NOT NULL DEFAULT '0',
  `id_edit_data_manager` int(11) NOT NULL DEFAULT '0',
  `telegram_id` varchar(191) DEFAULT NULL,
  `in_price_fee` varchar(191) NOT NULL DEFAULT '0',
  `out_price_fee` varchar(191) NOT NULL DEFAULT '0',
  `is_ban_order_data` int(11) NOT NULL DEFAULT '0',
  `id_main_operator` int(11) NOT NULL DEFAULT '0',
  `requisites_receive` varchar(191) DEFAULT NULL COMMENT 'Реквизиты, куда принимаются средства',
  `id_merchant` int(11) NOT NULL DEFAULT '0',
  `id_pay` int(11) NOT NULL DEFAULT '0',
  `is_auto_check_pay` int(11) NOT NULL DEFAULT '0',
  `queue_status` int(11) NOT NULL DEFAULT '0' COMMENT 'В очереди на выплату',
  `id_payment_gateway` bigint(20) NOT NULL DEFAULT '0',
  `id_promo_code` int(11) NOT NULL DEFAULT '0',
  `promo_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `id_direction_requisites` int(11) NOT NULL DEFAULT '0',
  `int_status_verification_card` int(11) NOT NULL DEFAULT '0',
  `in_amount_merchant` varchar(191) DEFAULT NULL,
  `out_amount_pay` varchar(191) DEFAULT NULL,
  `public_id` bigint(20) NOT NULL DEFAULT '0',
  `transfer_to_account` varchar(191) DEFAULT NULL COMMENT 'Записанный адрес куда клиенты отправляют средства'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_card_details`
--

CREATE TABLE `tasks_card_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_system` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bank_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_chat`
--

CREATE TABLE `tasks_chat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_task` int(11) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_client` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_comments`
--

CREATE TABLE `tasks_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `class_style` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_convert_log`
--

CREATE TABLE `tasks_convert_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `to_usd` float DEFAULT NULL,
  `to_rub` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_fields`
--

CREATE TABLE `tasks_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_field` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `field_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_history_operators`
--

CREATE TABLE `tasks_history_operators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `id_status` int(11) NOT NULL DEFAULT '0',
  `in_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_from_manager` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_info`
--

CREATE TABLE `tasks_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0' COMMENT 'ID заявки',
  `code_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Код страны',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Страна',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP адрес',
  `device` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'С какого устройства была создана заявка',
  `newbie` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Определяем, новичек ли создал заявку',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_scam` int(11) NOT NULL DEFAULT '0',
  `is_local_scam` int(11) NOT NULL DEFAULT '0',
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ru',
  `is_not_bonus` int(11) NOT NULL DEFAULT '0',
  `is_not_partner` int(11) NOT NULL DEFAULT '0',
  `is_bestchange_parser` int(11) NOT NULL DEFAULT '0',
  `bestchange_position` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_switch_not_cashback` int(11) NOT NULL DEFAULT '0' COMMENT 'В процессе закрытия заявки',
  `is_switch_not_partner` int(11) NOT NULL DEFAULT '0' COMMENT 'В процессе закрытия заявки',
  `in_min_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_max_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_freeze_scam` tinyint(1) NOT NULL DEFAULT '0',
  `num_transaction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_sign_payout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_name_payout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_position_payout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify_statuses` int(11) NOT NULL DEFAULT '0',
  `is_freeze_local` int(11) NOT NULL DEFAULT '0',
  `note_tx` text COLLATE utf8mb4_unicode_ci,
  `count_change_operator` int(11) NOT NULL DEFAULT '0',
  `amlbot_risk_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amlbot_risk_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_pending` int(11) NOT NULL DEFAULT '0',
  `blockchain_confirm` int(11) NOT NULL DEFAULT '0',
  `blockchain_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_transaction_merchant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_transaction_pay` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `getblockbot_risk_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `getblockbot_risk_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL DEFAULT '0',
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `network_id` int(11) NOT NULL DEFAULT '0',
  `network_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_aml_analysis` int(11) NOT NULL DEFAULT '0',
  `aml_riskscore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_wait_hash_pay` int(11) NOT NULL DEFAULT '0' COMMENT 'Включаем когда не выдается hash оплаты сразу',
  `recalculated_at` timestamp NULL DEFAULT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_profits`
--

CREATE TABLE `tasks_profits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `profit_percent` double NOT NULL DEFAULT '0',
  `profit_currency` double NOT NULL DEFAULT '0',
  `profit_usd` double(8,2) NOT NULL DEFAULT '0.00',
  `profit_rub` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_rates_data`
--

CREATE TABLE `tasks_rates_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `exchange_course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `market_course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_rejection_status`
--

CREATE TABLE `tasks_rejection_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_not_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tasks_rejection_status`
--

INSERT INTO `tasks_rejection_status` (`id`, `name`, `created_at`, `updated_at`, `is_not_delete`) VALUES
(1, 'Платёж не поступил', NULL, '2019-05-14 21:00:00', 1),
(2, 'Возврат', NULL, '2019-05-14 21:00:00', 1),
(3, 'Повторная заявка (дубликат)', NULL, '2019-05-14 21:00:00', 1),
(4, 'Ваша заявка признана мошеннической', NULL, '2019-05-14 21:00:00', 1),
(5, 'Мошенник из черного списка Bestchange', '2019-10-11 17:14:18', '2019-10-11 17:14:18', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_requisites`
--

CREATE TABLE `tasks_requisites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_direction_exchange` int(11) NOT NULL DEFAULT '0',
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_shots`
--

CREATE TABLE `tasks_shots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) DEFAULT NULL,
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_status`
--

CREATE TABLE `tasks_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(191) DEFAULT NULL,
  `class` varchar(191) DEFAULT NULL,
  `is_export` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `allow_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks_status`
--

INSERT INTO `tasks_status` (`id`, `name`, `color`, `class`, `is_export`, `updated_at`, `allow_delete`) VALUES
(1, 'Время истекло', '#f44336', 'st-delete', 0, '2019-03-19 07:11:44', 1),
(2, 'Ожидается оплата', '#7b93a3', 'st-pending-payment', 0, '2019-03-19 07:11:44', 1),
(3, 'Ожидает обработки', '#2196f3', 'st-handler', 0, '2019-03-19 07:11:44', 0),
(4, 'Заявка исполнена', '#4caf50', 'st-success', 1, '2019-03-19 07:11:44', 0),
(5, 'Заявка отклонена', '#f44336', 'st-delete', 0, '2019-03-19 07:11:44', 1),
(6, 'Заявка отменена пользователем', '#333', 'st-cancel', 0, '2019-03-19 07:11:44', 1),
(7, 'Оплаченная заявка', '#af4c88', 'st-merchant', 0, '2019-03-19 07:11:44', 0),
(8, 'Отложенная заявка', '#b2ba0f', 'st-frozen', 0, '2019-03-19 07:11:44', 0),
(9, 'В процессе оплаты', '#ab804b', 'st-process-payment', 0, '2019-03-19 07:11:44', 0),
(10, 'Недействительна', '#a70000', 'st-error', 0, '2019-03-19 07:11:44', 0),
(11, 'Заявка удалена', '#a70000', 'st-error', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_status_log`
--

CREATE TABLE `tasks_status_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `id_task` int(11) NOT NULL DEFAULT '0',
  `old_status` int(11) NOT NULL DEFAULT '0',
  `new_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `in_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `out_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_change` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks_user`
--

CREATE TABLE `tasks_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `convert_to_usd` float NOT NULL,
  `convert_to_rub` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `task_log`
--

CREATE TABLE `task_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `task_log_confirmation`
--

CREATE TABLE `task_log_confirmation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `confirmation` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `task_private_hash`
--

CREATE TABLE `task_private_hash` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `task_reports`
--

CREATE TABLE `task_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `profit` double NOT NULL DEFAULT '0',
  `add_course` double NOT NULL DEFAULT '0',
  `your_add_course` double NOT NULL DEFAULT '0',
  `commission` double NOT NULL DEFAULT '0',
  `commission_s` double NOT NULL DEFAULT '0',
  `is_group_commission` int(11) NOT NULL DEFAULT '0',
  `group_commission_value` double NOT NULL DEFAULT '0',
  `sign` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profit_usd` double NOT NULL DEFAULT '0',
  `profit_s` double NOT NULL DEFAULT '0',
  `profit_rub` double NOT NULL DEFAULT '0',
  `is_bestchange` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `task_single_log_confirm`
--

CREATE TABLE `task_single_log_confirm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `needed_confirm` int(11) NOT NULL DEFAULT '0',
  `received_confirm` int(11) NOT NULL DEFAULT '0',
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `telescope_entries`
--

CREATE TABLE `telescope_entries` (
  `sequence` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `telescope_entries_tags`
--

CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `telescope_monitoring`
--

CREATE TABLE `telescope_monitoring` (
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `template_type_events`
--

CREATE TABLE `template_type_events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `template_type_events`
--

INSERT INTO `template_type_events` (`id`, `event_name`, `event_type`, `name`, `description`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'CHANGE_RESERVE_NOTIFY', 0, 'Корректировка резерва', '<ul>\r\n	<li>[manager_name] - ID Пользователя, который скорректировал резерв</li>\r\n	<li>[created_at] - Дата корректировки резерва</li>\r\n	<li>[type] - Тип резерва (Пополнение, снятие)</li>\r\n	<li>[event] - Текст события</li>\r\n	<li>[comment] - Комментарий пользователя, который скорректировал резерв</li>\r\n	<li>[currency] - Название валюты</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(2, 'CONFIRM_WITHDRAWAL_BONUSES', 0, 'Подтвердите вывод средств', '<ul>\r\n	<li>[balance] - Сумма бонусов</li>\r\n	<li>[currency] - Валюта выплаты</li>\r\n	<li>[code_currency] - Код валюты</li>\r\n	<li>[user_id] - ID пользователя</li>\r\n	<li>[user_email] - Email пользователя</li>\r\n	<li>[user_name] - Имя пользователя</li>\r\n	<li>[button] - Кнопка для подтверждения вывода средств</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(3, 'NEW_BONUS_WITHDRAWAL', 0, 'Заявка на выплату бонусных вознаграждений', '<ul>\r\n	<li>[id] - ID</li>\r\n	<li>[currency] - Название валюты</li>\r\n	<li>[user_id] - ID Пользователя</li>\r\n	<li>[user_email] - Email Пользователя</li>\r\n	<li>[user_name] - Имя пользователя</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(4, 'ORDER_STATUS_PENDING_PROCESS', 0, 'Статус заявки \"Ожидает обработки\"', '<ul>\r\n	<li>[id] - ID Заявки</li>\r\n	<li>[currency_in] - Название валюты Отдаю</li>\r\n	<li>[currency_out] - Название валюты Получаю</li>\r\n	<li>[status] - Статус заявки</li>\r\n	<li>[direction] - Направление обмена</li>\r\n	<li>[from_shot] - С счета</li>\r\n	<li>[to_shot] - На счет</li>\r\n	<li>[created_at] - Дата создания заявки</li>\r\n	<li>[amount_in] - Сумма Отдаю</li>\r\n	<li>[amount_out] - Сумма Получаю</li>\r\n	<li>[user_id] - ID Пользователя</li>\r\n	<li>[user_email] - Email пользователя</li>\r\n	<li>[user_name] - Имя пользователя</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(5, 'ORDER_CHAT_NOTIFY', 0, 'Чат заявки', '<ul>\r\n	<li>[order_id] - ID Заявки</li>\r\n	<li>[message] - Текст сообщения</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(6, 'ORDER_RECALCULATION', 0, 'Пересчет заявки', '<ul>\r\n	<li>[order_id] - ID Заявки</li>\r\n	<li>[new_course] - Новый курс обмена</li>\r\n	<li>[amount_in] - Сумма Отдаю</li>\r\n	<li>[amount_out] - Сумма Получаю</li>\r\n	<li>[code_currency_in] - Код валюты Отдаю</li>\r\n	<li>[code_currency_out] - Код валюты Получаю</li>\r\n	<li>[currency_in] - Название валюты Отдаю</li>\r\n	<li>[currency_out] - Название валюты Получаю</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(7, 'ORDER_RESTORE', 0, 'Восстановление заявки', '<ul>\r\n	<li>[order_id] - ID Заявки</li>\r\n	<li>[status] - Статус заявки</li>\r\n	<li>[user_name] - Имя пользователя</li>\r\n	<li>[user_email] - Email пользователя</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(8, 'ORDER_CHECK_CREATED', 0, 'Номер чека для клиентов', '<ul>\r\n	<li>[order_id] - ID Заявки</li>\r\n	<li>[message] - Текст сообщения&nbsp;</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(9, 'USER_LOGIN_SUCCESSFUL', 0, 'Уведомление об успешной авторизации', '<ul>\r\n	<li>[user_id] - ID Пользователя</li>\r\n	<li>[user_name] - Имя пользователя</li>\r\n	<li>[user_email] - Email пользователя</li>\r\n	<li>[user_username] - Ник пользователя</li>\r\n	<li>[city_country] - Страна, Город пользователя</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(10, 'NEW_USER', 0, 'Зарегистрировался новый пользователь', '<ul>\r\n	<li>[id] - ID Пользователя</li>\r\n	<li>[name] - Имя пользователя</li>\r\n	<li>[email] - Email пользователя</li>\r\n	<li>[username] - Ник пользователя</li>\r\n	<li>[password] - Пароль пользователя</li>\r\n	<li>[user_ip] - IP Пользователя</li>\r\n	<li>[user_host] - Хост пользователя</li>\r\n</ul>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38'),
(11, 'ORDER_SHOT_BLACK_LIST_NOTIFY', 0, 'Черный список реквизитов', '<ol>\r\n	<li>[order_id] - ID Заявки</li>\r\n	<li>[message] - Текст сообщения</li>\r\n</ol>', 1, '2023-04-09 10:47:38', '2023-04-09 10:47:38');

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL,
  `transaction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `unpaid_items`
--

CREATE TABLE `unpaid_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `auto_delete` int(11) NOT NULL DEFAULT '0',
  `removal_time` int(11) NOT NULL DEFAULT '0',
  `removal_minute` int(11) NOT NULL DEFAULT '0',
  `rules_cron` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `unpaid_items`
--

INSERT INTO `unpaid_items` (`id`, `auto_delete`, `removal_time`, `removal_minute`, `rules_cron`, `created_at`, `updated_at`, `order_status`) VALUES
(1, 1, 5, 0, 1, NULL, '2019-11-14 14:20:13', '2');

-- --------------------------------------------------------

--
-- Структура таблицы `update_systems`
--

CREATE TABLE `update_systems` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `update_site_proxy_addr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_site_proxy_port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_site_proxy_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_site_proxy_pass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stable_versions_only` int(11) NOT NULL DEFAULT '1',
  `update_autocheck` int(11) NOT NULL DEFAULT '0',
  `update_stop_autocheck` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `is_root` int(11) NOT NULL DEFAULT '0',
  `last_login_at` timestamp NULL DEFAULT NULL COMMENT 'Последний вход',
  `last_logout_at` timestamp NULL DEFAULT NULL COMMENT 'Последний выход',
  `last_activity_at` timestamp NULL DEFAULT NULL COMMENT 'Последняя активность',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `referral_program_id` int(10) UNSIGNED DEFAULT NULL,
  `google2fa_secret` text COLLATE utf8mb4_unicode_ci,
  `session_id` text COLLATE utf8mb4_unicode_ci,
  `banned_at` timestamp NULL DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safe_input` int(11) NOT NULL DEFAULT '0',
  `restriction` int(11) NOT NULL DEFAULT '0',
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deactivation` int(11) NOT NULL DEFAULT '0',
  `is_telescope` int(11) NOT NULL DEFAULT '0',
  `is_order` int(11) NOT NULL DEFAULT '0',
  `auto_withdrawal` int(11) NOT NULL DEFAULT '0',
  `min_withdrawal` float NOT NULL DEFAULT '0',
  `current_page` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_unique_user` int(11) NOT NULL DEFAULT '0',
  `order_num` int(11) NOT NULL DEFAULT '0',
  `is_horizon` int(11) NOT NULL DEFAULT '0',
  `ip_changed` tinyint(1) DEFAULT NULL,
  `role_expired_at` timestamp NULL DEFAULT NULL,
  `is_follow_referral` tinyint(1) NOT NULL DEFAULT '0',
  `is_notify_email` int(11) NOT NULL DEFAULT '0',
  `is_password_reset` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_pay_referral` int(11) NOT NULL DEFAULT '0',
  `is_pay_cashback` int(11) NOT NULL DEFAULT '0',
  `is_verification` int(11) NOT NULL DEFAULT '0',
  `num_auth` int(11) NOT NULL DEFAULT '0',
  `telegram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_download_codes` int(11) NOT NULL DEFAULT '0',
  `backup_code_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_vip_client` int(11) NOT NULL DEFAULT '0',
  `is_backup` int(11) NOT NULL DEFAULT '0',
  `is_report_referral_data` int(11) NOT NULL DEFAULT '0',
  `user_browser` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_device` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_style` int(11) NOT NULL DEFAULT '0',
  `is_enabled_restapi` int(11) NOT NULL DEFAULT '0',
  `restapi_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `phone`, `name`, `email`, `email_verified_at`, `password`, `is_admin`, `is_root`, `last_login_at`, `last_logout_at`, `last_activity_at`, `remember_token`, `created_at`, `updated_at`, `referral_program_id`, `google2fa_secret`, `session_id`, `banned_at`, `language`, `safe_input`, `restriction`, `username`, `ip_address`, `deactivation`, `is_telescope`, `is_order`, `auto_withdrawal`, `min_withdrawal`, `current_page`, `is_unique_user`, `order_num`, `is_horizon`, `ip_changed`, `role_expired_at`, `is_follow_referral`, `is_notify_email`, `is_password_reset`, `user_agent`, `is_pay_referral`, `is_pay_cashback`, `is_verification`, `num_auth`, `telegram`, `provider`, `provider_id`, `is_download_codes`, `backup_code_secret`, `phone_code`, `admin_phone`, `phone_hash`, `is_vip_client`, `is_backup`, `is_report_referral_data`, `user_browser`, `user_device`, `user_style`, `is_enabled_restapi`, `restapi_key`) VALUES
(1, NULL, 'admin', 'user@iexbase.com', NULL, '$2y$10$EpZizzK8eBSvH87myAvaHewWgYlF3UWB.ZJWKRNcrybpWVxyMEreS', 0, 0, '2023-04-09 11:06:39', '2023-04-09 11:26:28', '2023-04-09 11:10:53', 'w0HXvJtNF8yTvdJSvkDDm4PpzI8lRLhZhIHHJGxKkPfbfl69BdjoPLBPrSkB', '2019-11-28 22:23:46', '2023-04-09 11:10:53', NULL, '', 'LSsxqBdE9CICvabeuO9fC5BcQ1FRqmH3bRuzCpQ0', NULL, 'ru', 0, 0, NULL, '127.0.0.1', 0, 1, 0, 0, 0, 'admin/ajax/order', 0, 0, 1, 0, NULL, 0, 1, 0, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36', 0, 0, 0, 41, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 0, 0, 'Chrome', NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users_history_profiles`
--

CREATE TABLE `users_history_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_changed` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_auth`
--

CREATE TABLE `user_auth` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `source` int(11) NOT NULL DEFAULT '0',
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iso_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_balance`
--

CREATE TABLE `user_balance` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `balance` float DEFAULT '0',
  `balance_reward` float DEFAULT '0',
  `id_code_currency` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_balance`
--

INSERT INTO `user_balance` (`id`, `id_user`, `balance`, `balance_reward`, `id_code_currency`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 1, '2019-11-28 22:23:48', '2019-11-28 22:23:48');

-- --------------------------------------------------------

--
-- Структура таблицы `user_balance_log`
--

CREATE TABLE `user_balance_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `text` text COLLATE utf8mb4_unicode_ci,
  `from_balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_balance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_settings`
--

CREATE TABLE `user_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user_wallet_stories`
--

CREATE TABLE `user_wallet_stories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `wallet` longtext COLLATE utf8mb4_unicode_ci,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `verification_card`
--

CREATE TABLE `verification_card` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL DEFAULT '0',
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hash_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number_string` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `verification_card_category`
--

CREATE TABLE `verification_card_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `verification_card_instructions`
--

CREATE TABLE `verification_card_instructions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_category` int(11) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8mb4_unicode_ci,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `notice_text` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `sorting` int(11) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wallets_addresses`
--

CREATE TABLE `wallets_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_requisites` int(11) NOT NULL DEFAULT '0' COMMENT 'ID платежной системы',
  `address` text COLLATE utf8mb4_unicode_ci COMMENT 'Адрес кошелька',
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Метки',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `private_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_failed_send` int(11) NOT NULL DEFAULT '0',
  `is_fee_sent` int(11) NOT NULL DEFAULT '0',
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wallets_history`
--

CREATE TABLE `wallets_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0' COMMENT 'ID заявки',
  `txid` text COLLATE utf8mb4_unicode_ci COMMENT 'ID транзакции',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wallet_transactions`
--

CREATE TABLE `wallet_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmations` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `whitebit_withdraw`
--

CREATE TABLE `whitebit_withdraw` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `method_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_task` int(11) NOT NULL DEFAULT '0',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticker` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `transaction_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `whitelist_order`
--

CREATE TABLE `whitelist_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `withdrawal_request`
--

CREATE TABLE `withdrawal_request` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_manager` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `referral` int(11) DEFAULT '0',
  `reward` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `score` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance_referral` float DEFAULT '0',
  `balance_reward` float NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_referral` float DEFAULT '0',
  `base_reward` float NOT NULL DEFAULT '0',
  `verified_at` timestamp NULL DEFAULT NULL,
  `tx_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `big_id` bigint(20) NOT NULL DEFAULT '0',
  `is_black_list` int(11) NOT NULL DEFAULT '0',
  `black_list_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_balance_referral` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_balance_reward` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `withdrawal_request_log`
--

CREATE TABLE `withdrawal_request_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_withdrawal_request` int(11) NOT NULL,
  `id_manager` int(11) NOT NULL DEFAULT '0',
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remainder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `yandex_currencies`
--

CREATE TABLE `yandex_currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_currency` int(11) NOT NULL DEFAULT '0',
  `selected` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `your_exchange`
--

CREATE TABLE `your_exchange` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `exchange_rate` double NOT NULL,
  `number_format` int(11) NOT NULL DEFAULT '4',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin_auth_log`
--
ALTER TABLE `admin_auth_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_desktops`
--
ALTER TABLE `admin_desktops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_desktop_gadgets`
--
ALTER TABLE `admin_desktop_gadgets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_filters_user`
--
ALTER TABLE `admin_filters_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_filter_header`
--
ALTER TABLE `admin_filter_header`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `advantage`
--
ALTER TABLE `advantage`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `affiliate_settings`
--
ALTER TABLE `affiliate_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `amlbot_histories`
--
ALTER TABLE `amlbot_histories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `aml_analysis_logs`
--
ALTER TABLE `aml_analysis_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `archive_reports`
--
ALTER TABLE `archive_reports`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audits_auditable_type_auditable_id_index` (`auditable_type`,`auditable_id`),
  ADD KEY `audits_user_id_user_type_index` (`user_id`,`user_type`);

--
-- Индексы таблицы `autosender_payment`
--
ALTER TABLE `autosender_payment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `backgrounds`
--
ALTER TABLE `backgrounds`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `backup_codes`
--
ALTER TABLE `backup_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banned`
--
ALTER TABLE `banned`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banned_user`
--
ALTER TABLE `banned_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bans_bannable_type_bannable_id_index` (`bannable_type`,`bannable_id`),
  ADD KEY `bans_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  ADD KEY `bans_expired_at_index` (`expired_at`);

--
-- Индексы таблицы `bestchange_bl_histories`
--
ALTER TABLE `bestchange_bl_histories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bestchange_currencies`
--
ALTER TABLE `bestchange_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bestchange_data_log`
--
ALTER TABLE `bestchange_data_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bestchange_parser_error`
--
ALTER TABLE `bestchange_parser_error`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bestchange_rates`
--
ALTER TABLE `bestchange_rates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bestchange_rates_log`
--
ALTER TABLE `bestchange_rates_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blacklist_order`
--
ALTER TABLE `blacklist_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Индексы таблицы `cashback_error_log`
--
ALTER TABLE `cashback_error_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `checks`
--
ALTER TABLE `checks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `checks_host_id_foreign` (`host_id`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `code_currency`
--
ALTER TABLE `code_currency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `collaboration_pr`
--
ALTER TABLE `collaboration_pr`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `competitor_links`
--
ALTER TABLE `competitor_links`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `competitor_rates`
--
ALTER TABLE `competitor_rates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `competitor_rates_log`
--
ALTER TABLE `competitor_rates_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contests`
--
ALTER TABLE `contests`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contests_conditions`
--
ALTER TABLE `contests_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contests_faq`
--
ALTER TABLE `contests_faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contests_has_contests_users`
--
ALTER TABLE `contests_has_contests_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contests_has_contests_users_contests_user_id_foreign` (`contests_user_id`),
  ADD KEY `contests_has_contests_users_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `contests_users`
--
ALTER TABLE `contests_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `course_logs`
--
ALTER TABLE `course_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cron`
--
ALTER TABLE `cron`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cron_category`
--
ALTER TABLE `cron_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_analytics`
--
ALTER TABLE `currencies_analytics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_commands`
--
ALTER TABLE `currencies_commands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_groups`
--
ALTER TABLE `currencies_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_info`
--
ALTER TABLE `currencies_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_log`
--
ALTER TABLE `currencies_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_networks`
--
ALTER TABLE `currencies_networks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currencies_notification`
--
ALTER TABLE `currencies_notification`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency_fields`
--
ALTER TABLE `currency_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency_in_has_fields`
--
ALTER TABLE `currency_in_has_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency_in_has_fields_field_id_foreign` (`field_id`),
  ADD KEY `currency_in_has_fields_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `currency_merchants`
--
ALTER TABLE `currency_merchants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency_merchants_gateway_merchant_id_foreign` (`gateway_merchant_id`),
  ADD KEY `currency_merchants_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `currency_out_has_fields`
--
ALTER TABLE `currency_out_has_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency_out_has_fields_field_id_foreign` (`field_id`),
  ADD KEY `currency_out_has_fields_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `currency_requisites_has_fields`
--
ALTER TABLE `currency_requisites_has_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency_requisites_has_fields_field_id_foreign` (`field_id`),
  ADD KEY `currency_requisites_has_fields_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `debtors`
--
ALTER TABLE `debtors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `directions_fields`
--
ALTER TABLE `directions_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `directions_has_allowed_countries`
--
ALTER TABLE `directions_has_allowed_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_allowed_countries_geo_country_list_id_foreign` (`geo_country_list_id`),
  ADD KEY `directions_has_allowed_countries_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `directions_has_cities`
--
ALTER TABLE `directions_has_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_cities_city_id_foreign` (`city_id`),
  ADD KEY `directions_has_cities_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `directions_has_fields`
--
ALTER TABLE `directions_has_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_fields_direction_field_id_foreign` (`direction_field_id`),
  ADD KEY `directions_has_fields_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `directions_has_forbidden_countries`
--
ALTER TABLE `directions_has_forbidden_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_forbidden_countries_geo_country_list_id_foreign` (`geo_country_list_id`),
  ADD KEY `directions_has_forbidden_countries_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `directions_has_modes`
--
ALTER TABLE `directions_has_modes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_modes_direction_exchange_mode_id_foreign` (`direction_exchange_mode_id`),
  ADD KEY `directions_has_modes_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `directions_has_networks`
--
ALTER TABLE `directions_has_networks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_networks_network_id_foreign` (`network_id`),
  ADD KEY `directions_has_networks_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `directions_has_requisites`
--
ALTER TABLE `directions_has_requisites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_has_requisites_direction_requisite_id_foreign` (`direction_requisite_id`),
  ADD KEY `directions_has_requisites_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `direction_day`
--
ALTER TABLE `direction_day`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `direction_exchange`
--
ALTER TABLE `direction_exchange`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direction_exchange_id_currency1_index` (`id_currency1`),
  ADD KEY `direction_exchange_id_currency2_index` (`id_currency2`),
  ADD KEY `direction_exchange_id_group_direction_index` (`id_group_direction`),
  ADD KEY `direction_exchange_status_index` (`status`),
  ADD KEY `direction_exchange_enable_bestchange_index` (`enable_bestchange`),
  ADD KEY `direction_exchange_is_holding_direction_index` (`is_holding_direction`),
  ADD KEY `direction_exchange_is_not_bonus_index` (`is_not_bonus`),
  ADD KEY `direction_exchange_is_not_partner_index` (`is_not_partner`),
  ADD KEY `direction_exchange_allow_export_index` (`allow_export`);

--
-- Индексы таблицы `direction_exchange_cities`
--
ALTER TABLE `direction_exchange_cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `direction_exchange_error_log`
--
ALTER TABLE `direction_exchange_error_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `direction_exchange_group`
--
ALTER TABLE `direction_exchange_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `direction_exchange_merchants`
--
ALTER TABLE `direction_exchange_merchants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direction_exchange_merchants_model_id_foreign` (`model_id`),
  ADD KEY `direction_exchange_merchants_gateway_merchant_id_foreign` (`gateway_merchant_id`);

--
-- Индексы таблицы `direction_exchange_modes`
--
ALTER TABLE `direction_exchange_modes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `direction_notification`
--
ALTER TABLE `direction_notification`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `direction_requisites`
--
ALTER TABLE `direction_requisites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `docs_category`
--
ALTER TABLE `docs_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `docs_category_type`
--
ALTER TABLE `docs_category_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `docs_items`
--
ALTER TABLE `docs_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `event_employees`
--
ALTER TABLE `event_employees`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `event_reserve`
--
ALTER TABLE `event_reserve`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `export_data`
--
ALTER TABLE `export_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `e_voucher_codes`
--
ALTER TABLE `e_voucher_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `favorites_links`
--
ALTER TABLE `favorites_links`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `file_parser_groups`
--
ALTER TABLE `file_parser_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `file_parser_rates`
--
ALTER TABLE `file_parser_rates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filter_currency`
--
ALTER TABLE `filter_currency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fine_employees`
--
ALTER TABLE `fine_employees`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `firewall`
--
ALTER TABLE `firewall`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `firewall_ip_address_unique` (`ip_address`);

--
-- Индексы таблицы `fund`
--
ALTER TABLE `fund`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gateways_merchants`
--
ALTER TABLE `gateways_merchants`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gateways_payments`
--
ALTER TABLE `gateways_payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `geo_country_list`
--
ALTER TABLE `geo_country_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `getblockbot_histories`
--
ALTER TABLE `getblockbot_histories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `group_parser_exchange`
--
ALTER TABLE `group_parser_exchange`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `health_checks`
--
ALTER TABLE `health_checks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `health_checks_resource_slug_index` (`resource_slug`),
  ADD KEY `health_checks_target_slug_index` (`target_slug`),
  ADD KEY `health_checks_created_at_index` (`created_at`);

--
-- Индексы таблицы `histories_codes`
--
ALTER TABLE `histories_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `history_excode`
--
ALTER TABLE `history_excode`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `history_fields`
--
ALTER TABLE `history_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `history_internal_accounts`
--
ALTER TABLE `history_internal_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `history_payment_transactions`
--
ALTER TABLE `history_payment_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `history_recalculation`
--
ALTER TABLE `history_recalculation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hosts`
--
ALTER TABLE `hosts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `iex_settings`
--
ALTER TABLE `iex_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iex_settings_key_index` (`key`);

--
-- Индексы таблицы `info_statistics`
--
ALTER TABLE `info_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `internal_accounts`
--
ALTER TABLE `internal_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Индексы таблицы `job_settings`
--
ALTER TABLE `job_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `links_footers`
--
ALTER TABLE `links_footers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `links_footer_groups`
--
ALTER TABLE `links_footer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `links_reviews`
--
ALTER TABLE `links_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `links_review_groups`
--
ALTER TABLE `links_review_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `live_notification`
--
ALTER TABLE `live_notification`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logs_email`
--
ALTER TABLE `logs_email`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log_autopayments`
--
ALTER TABLE `log_autopayments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log_check_pay`
--
ALTER TABLE `log_check_pay`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log_drain`
--
ALTER TABLE `log_drain`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log_error_merchants`
--
ALTER TABLE `log_error_merchants`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log_merchants`
--
ALTER TABLE `log_merchants`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_event_logs`
--
ALTER TABLE `main_event_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menu_slug_unique` (`slug`);

--
-- Индексы таблицы `merchant_account`
--
ALTER TABLE `merchant_account`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `merchant_transaction_hash`
--
ALTER TABLE `merchant_transaction_hash`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `merchant_transaction_ids`
--
ALTER TABLE `merchant_transaction_ids`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Индексы таблицы `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Индексы таблицы `monitors`
--
ALTER TABLE `monitors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `monitors_url_unique` (`url`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notices_exchange`
--
ALTER TABLE `notices_exchange`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Индексы таблицы `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Индексы таблицы `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Индексы таблицы `operation_levels`
--
ALTER TABLE `operation_levels`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `operation_level_groups`
--
ALTER TABLE `operation_level_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `pages_page_slug_unique` (`page_slug`);

--
-- Индексы таблицы `pages_static`
--
ALTER TABLE `pages_static`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parser_api_keys`
--
ALTER TABLE `parser_api_keys`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parser_exchange`
--
ALTER TABLE `parser_exchange`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parser_exchange_log`
--
ALTER TABLE `parser_exchange_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parser_exchange_log_id_parser_exchange_foreign` (`id_parser_exchange`);

--
-- Индексы таблицы `parser_formula_coefficient`
--
ALTER TABLE `parser_formula_coefficient`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parser_formula_rates`
--
ALTER TABLE `parser_formula_rates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `parser_type`
--
ALTER TABLE `parser_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partner_parser_groups`
--
ALTER TABLE `partner_parser_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partner_parser_rates`
--
ALTER TABLE `partner_parser_rates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_histories`
--
ALTER TABLE `password_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_histories_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payment_explorer`
--
ALTER TABLE `payment_explorer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payout_address`
--
ALTER TABLE `payout_address`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pay_transaction_hash`
--
ALTER TABLE `pay_transaction_hash`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pending_order_status`
--
ALTER TABLE `pending_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `permissions_group`
--
ALTER TABLE `permissions_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Индексы таблицы `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `proxies_payment`
--
ALTER TABLE `proxies_payment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `proxies_qiwi`
--
ALTER TABLE `proxies_qiwi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `qiwi_currencies`
--
ALTER TABLE `qiwi_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `referrals_info_logs`
--
ALTER TABLE `referrals_info_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `referral_links`
--
ALTER TABLE `referral_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `referral_links_referral_program_id_user_id_unique` (`referral_program_id`,`user_id`),
  ADD KEY `referral_links_code_index` (`code`);

--
-- Индексы таблицы `referral_log`
--
ALTER TABLE `referral_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `referral_programs`
--
ALTER TABLE `referral_programs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `referral_programs_name_unique` (`name`);

--
-- Индексы таблицы `referral_relationships`
--
ALTER TABLE `referral_relationships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referral_relationships_referral_link_id_foreign` (`referral_link_id`);

--
-- Индексы таблицы `referral_statistics`
--
ALTER TABLE `referral_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requirements_verification`
--
ALTER TABLE `requirements_verification`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites`
--
ALTER TABLE `requisites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites_blacklist`
--
ALTER TABLE `requisites_blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites_fields`
--
ALTER TABLE `requisites_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites_group`
--
ALTER TABLE `requisites_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites_has_fields`
--
ALTER TABLE `requisites_has_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requisites_has_fields_field_id_foreign` (`field_id`),
  ADD KEY `requisites_has_fields_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `requisites_has_info_fields`
--
ALTER TABLE `requisites_has_info_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requisites_has_info_fields_field_id_foreign` (`field_id`),
  ADD KEY `requisites_has_info_fields_model_id_foreign` (`model_id`);

--
-- Индексы таблицы `requisites_info_fields`
--
ALTER TABLE `requisites_info_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites_list`
--
ALTER TABLE `requisites_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserves`
--
ALTER TABLE `reserves`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserves_alerts`
--
ALTER TABLE `reserves_alerts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reserves_alerts_id_reserve_foreign` (`id_reserve`);

--
-- Индексы таблицы `reserves_files`
--
ALTER TABLE `reserves_files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserves_files_groups`
--
ALTER TABLE `reserves_files_groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserve_group`
--
ALTER TABLE `reserve_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserve_log`
--
ALTER TABLE `reserve_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserve_log_profit`
--
ALTER TABLE `reserve_log_profit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reserve_request`
--
ALTER TABLE `reserve_request`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reward`
--
ALTER TABLE `reward`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reward_reward_program_id_user_id_unique` (`reward_program_id`,`user_id`);

--
-- Индексы таблицы `reward_log`
--
ALTER TABLE `reward_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reward_programs`
--
ALTER TABLE `reward_programs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reward_programs_name_unique` (`name`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `selected_courses`
--
ALTER TABLE `selected_courses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings_referrals`
--
ALTER TABLE `settings_referrals`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings_reward`
--
ALTER TABLE `settings_reward`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings_theme`
--
ALTER TABLE `settings_theme`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `social_auth_system`
--
ALTER TABLE `social_auth_system`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `social_reviews`
--
ALTER TABLE `social_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_status_index` (`status`),
  ADD KEY `tasks_id_user_index` (`id_user`),
  ADD KEY `tasks_ip_index` (`ip`),
  ADD KEY `tasks_give_price_index` (`give_price`),
  ADD KEY `tasks_receiving_price_index` (`receiving_price`),
  ADD KEY `tasks_id_direction_exchange_index` (`id_direction_exchange`),
  ADD KEY `tasks_from_shot_index` (`from_shot`),
  ADD KEY `tasks_to_shot_index` (`to_shot`),
  ADD KEY `tasks_public_id_index` (`public_id`);

--
-- Индексы таблицы `tasks_card_details`
--
ALTER TABLE `tasks_card_details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_chat`
--
ALTER TABLE `tasks_chat`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_comments`
--
ALTER TABLE `tasks_comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_convert_log`
--
ALTER TABLE `tasks_convert_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_fields`
--
ALTER TABLE `tasks_fields`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_history_operators`
--
ALTER TABLE `tasks_history_operators`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_info`
--
ALTER TABLE `tasks_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_profits`
--
ALTER TABLE `tasks_profits`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_rates_data`
--
ALTER TABLE `tasks_rates_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_rejection_status`
--
ALTER TABLE `tasks_rejection_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_requisites`
--
ALTER TABLE `tasks_requisites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_shots`
--
ALTER TABLE `tasks_shots`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_status`
--
ALTER TABLE `tasks_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_status_log`
--
ALTER TABLE `tasks_status_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks_user`
--
ALTER TABLE `tasks_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `task_log`
--
ALTER TABLE `task_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `task_log_confirmation`
--
ALTER TABLE `task_log_confirmation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_log_confirmation_id_task_foreign` (`id_task`);

--
-- Индексы таблицы `task_private_hash`
--
ALTER TABLE `task_private_hash`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `task_reports`
--
ALTER TABLE `task_reports`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_reports_id_task_unique` (`id_task`);

--
-- Индексы таблицы `task_single_log_confirm`
--
ALTER TABLE `task_single_log_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `telescope_entries`
--
ALTER TABLE `telescope_entries`
  ADD PRIMARY KEY (`sequence`),
  ADD UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  ADD KEY `telescope_entries_batch_id_index` (`batch_id`),
  ADD KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`),
  ADD KEY `telescope_entries_family_hash_index` (`family_hash`);

--
-- Индексы таблицы `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  ADD KEY `telescope_entries_tags_tag_index` (`tag`);

--
-- Индексы таблицы `template_type_events`
--
ALTER TABLE `template_type_events`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `unpaid_items`
--
ALTER TABLE `unpaid_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `update_systems`
--
ALTER TABLE `update_systems`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_referral_program_id_foreign` (`referral_program_id`),
  ADD KEY `users_email_index` (`email`),
  ADD KEY `users_auto_withdrawal_index` (`auto_withdrawal`),
  ADD KEY `users_provider_index` (`provider`),
  ADD KEY `users_last_activity_at_index` (`last_activity_at`),
  ADD KEY `users_is_unique_user_index` (`is_unique_user`),
  ADD KEY `users_email_verified_at_index` (`email_verified_at`),
  ADD KEY `users_deactivation_index` (`deactivation`),
  ADD KEY `users_is_order_index` (`is_order`),
  ADD KEY `users_name_index` (`name`),
  ADD KEY `users_ip_address_index` (`ip_address`),
  ADD KEY `users_created_at_index` (`created_at`),
  ADD KEY `users_last_login_at_index` (`last_login_at`),
  ADD KEY `users_last_logout_at_index` (`last_logout_at`),
  ADD KEY `users_order_num_index` (`order_num`);

--
-- Индексы таблицы `users_history_profiles`
--
ALTER TABLE `users_history_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_auth`
--
ALTER TABLE `user_auth`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_balance`
--
ALTER TABLE `user_balance`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_balance_log`
--
ALTER TABLE `user_balance_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_wallet_stories`
--
ALTER TABLE `user_wallet_stories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `verification_card`
--
ALTER TABLE `verification_card`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `verification_card_category`
--
ALTER TABLE `verification_card_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `verification_card_instructions`
--
ALTER TABLE `verification_card_instructions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `wallets_addresses`
--
ALTER TABLE `wallets_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `wallets_history`
--
ALTER TABLE `wallets_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `whitebit_withdraw`
--
ALTER TABLE `whitebit_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `whitelist_order`
--
ALTER TABLE `whitelist_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `withdrawal_request`
--
ALTER TABLE `withdrawal_request`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `withdrawal_request_tx_id_unique` (`tx_id`);

--
-- Индексы таблицы `withdrawal_request_log`
--
ALTER TABLE `withdrawal_request_log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yandex_currencies`
--
ALTER TABLE `yandex_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `your_exchange`
--
ALTER TABLE `your_exchange`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin_auth_log`
--
ALTER TABLE `admin_auth_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `admin_desktops`
--
ALTER TABLE `admin_desktops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `admin_desktop_gadgets`
--
ALTER TABLE `admin_desktop_gadgets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `admin_filters_user`
--
ALTER TABLE `admin_filters_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `admin_filter_header`
--
ALTER TABLE `admin_filter_header`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `advantage`
--
ALTER TABLE `advantage`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `affiliate_settings`
--
ALTER TABLE `affiliate_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `amlbot_histories`
--
ALTER TABLE `amlbot_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `aml_analysis_logs`
--
ALTER TABLE `aml_analysis_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `archive_reports`
--
ALTER TABLE `archive_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `autosender_payment`
--
ALTER TABLE `autosender_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `backgrounds`
--
ALTER TABLE `backgrounds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `backup_codes`
--
ALTER TABLE `backup_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `banned`
--
ALTER TABLE `banned`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `banned_user`
--
ALTER TABLE `banned_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bans`
--
ALTER TABLE `bans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bestchange_bl_histories`
--
ALTER TABLE `bestchange_bl_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bestchange_currencies`
--
ALTER TABLE `bestchange_currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bestchange_data_log`
--
ALTER TABLE `bestchange_data_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bestchange_parser_error`
--
ALTER TABLE `bestchange_parser_error`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bestchange_rates`
--
ALTER TABLE `bestchange_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `bestchange_rates_log`
--
ALTER TABLE `bestchange_rates_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `blacklist_order`
--
ALTER TABLE `blacklist_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cashback_error_log`
--
ALTER TABLE `cashback_error_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `checks`
--
ALTER TABLE `checks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `code_currency`
--
ALTER TABLE `code_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `collaboration_pr`
--
ALTER TABLE `collaboration_pr`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `competitor_links`
--
ALTER TABLE `competitor_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `competitor_rates`
--
ALTER TABLE `competitor_rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `competitor_rates_log`
--
ALTER TABLE `competitor_rates_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `contests`
--
ALTER TABLE `contests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `contests_conditions`
--
ALTER TABLE `contests_conditions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `contests_faq`
--
ALTER TABLE `contests_faq`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `contests_has_contests_users`
--
ALTER TABLE `contests_has_contests_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `contests_users`
--
ALTER TABLE `contests_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `course_logs`
--
ALTER TABLE `course_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cron`
--
ALTER TABLE `cron`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `cron_category`
--
ALTER TABLE `cron_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currencies_analytics`
--
ALTER TABLE `currencies_analytics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currencies_commands`
--
ALTER TABLE `currencies_commands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currencies_groups`
--
ALTER TABLE `currencies_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `currencies_info`
--
ALTER TABLE `currencies_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currencies_log`
--
ALTER TABLE `currencies_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currencies_networks`
--
ALTER TABLE `currencies_networks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currencies_notification`
--
ALTER TABLE `currencies_notification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currency_fields`
--
ALTER TABLE `currency_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currency_in_has_fields`
--
ALTER TABLE `currency_in_has_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currency_merchants`
--
ALTER TABLE `currency_merchants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currency_out_has_fields`
--
ALTER TABLE `currency_out_has_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `currency_requisites_has_fields`
--
ALTER TABLE `currency_requisites_has_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `debtors`
--
ALTER TABLE `debtors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_fields`
--
ALTER TABLE `directions_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_allowed_countries`
--
ALTER TABLE `directions_has_allowed_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_cities`
--
ALTER TABLE `directions_has_cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_fields`
--
ALTER TABLE `directions_has_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_forbidden_countries`
--
ALTER TABLE `directions_has_forbidden_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_modes`
--
ALTER TABLE `directions_has_modes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_networks`
--
ALTER TABLE `directions_has_networks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `directions_has_requisites`
--
ALTER TABLE `directions_has_requisites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_day`
--
ALTER TABLE `direction_day`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_exchange`
--
ALTER TABLE `direction_exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_exchange_cities`
--
ALTER TABLE `direction_exchange_cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_exchange_error_log`
--
ALTER TABLE `direction_exchange_error_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_exchange_group`
--
ALTER TABLE `direction_exchange_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `direction_exchange_merchants`
--
ALTER TABLE `direction_exchange_merchants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_exchange_modes`
--
ALTER TABLE `direction_exchange_modes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_notification`
--
ALTER TABLE `direction_notification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `direction_requisites`
--
ALTER TABLE `direction_requisites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `docs_category`
--
ALTER TABLE `docs_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `docs_category_type`
--
ALTER TABLE `docs_category_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `docs_items`
--
ALTER TABLE `docs_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `event_employees`
--
ALTER TABLE `event_employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `event_reserve`
--
ALTER TABLE `event_reserve`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `export_data`
--
ALTER TABLE `export_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `e_voucher_codes`
--
ALTER TABLE `e_voucher_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `favorites_links`
--
ALTER TABLE `favorites_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `file_parser_groups`
--
ALTER TABLE `file_parser_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `file_parser_rates`
--
ALTER TABLE `file_parser_rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `filter_currency`
--
ALTER TABLE `filter_currency`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `fine_employees`
--
ALTER TABLE `fine_employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `firewall`
--
ALTER TABLE `firewall`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `fund`
--
ALTER TABLE `fund`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `gateways_merchants`
--
ALTER TABLE `gateways_merchants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gateways_payments`
--
ALTER TABLE `gateways_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `geo_country_list`
--
ALTER TABLE `geo_country_list`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT для таблицы `getblockbot_histories`
--
ALTER TABLE `getblockbot_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `group_parser_exchange`
--
ALTER TABLE `group_parser_exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `health_checks`
--
ALTER TABLE `health_checks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `histories_codes`
--
ALTER TABLE `histories_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `history_excode`
--
ALTER TABLE `history_excode`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `history_fields`
--
ALTER TABLE `history_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `history_internal_accounts`
--
ALTER TABLE `history_internal_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `history_payment_transactions`
--
ALTER TABLE `history_payment_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `history_recalculation`
--
ALTER TABLE `history_recalculation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `hosts`
--
ALTER TABLE `hosts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `iex_settings`
--
ALTER TABLE `iex_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `info_statistics`
--
ALTER TABLE `info_statistics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `internal_accounts`
--
ALTER TABLE `internal_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `job_settings`
--
ALTER TABLE `job_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `links_footers`
--
ALTER TABLE `links_footers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `links_footer_groups`
--
ALTER TABLE `links_footer_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `links_reviews`
--
ALTER TABLE `links_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `links_review_groups`
--
ALTER TABLE `links_review_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `live_notification`
--
ALTER TABLE `live_notification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `logs_email`
--
ALTER TABLE `logs_email`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `log_autopayments`
--
ALTER TABLE `log_autopayments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `log_check_pay`
--
ALTER TABLE `log_check_pay`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `log_drain`
--
ALTER TABLE `log_drain`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `log_error_merchants`
--
ALTER TABLE `log_error_merchants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `log_merchants`
--
ALTER TABLE `log_merchants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `main_event_logs`
--
ALTER TABLE `main_event_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `merchant_account`
--
ALTER TABLE `merchant_account`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `merchant_transaction_hash`
--
ALTER TABLE `merchant_transaction_hash`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `merchant_transaction_ids`
--
ALTER TABLE `merchant_transaction_ids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=940;

--
-- AUTO_INCREMENT для таблицы `monitors`
--
ALTER TABLE `monitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `notices_exchange`
--
ALTER TABLE `notices_exchange`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `operation_levels`
--
ALTER TABLE `operation_levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `operation_level_groups`
--
ALTER TABLE `operation_level_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `pages_static`
--
ALTER TABLE `pages_static`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `parser_api_keys`
--
ALTER TABLE `parser_api_keys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `parser_exchange`
--
ALTER TABLE `parser_exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `parser_exchange_log`
--
ALTER TABLE `parser_exchange_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `parser_formula_coefficient`
--
ALTER TABLE `parser_formula_coefficient`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `parser_formula_rates`
--
ALTER TABLE `parser_formula_rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `parser_type`
--
ALTER TABLE `parser_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `partner_parser_groups`
--
ALTER TABLE `partner_parser_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `partner_parser_rates`
--
ALTER TABLE `partner_parser_rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `password_histories`
--
ALTER TABLE `password_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `payment_explorer`
--
ALTER TABLE `payment_explorer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `payout_address`
--
ALTER TABLE `payout_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pay_transaction_hash`
--
ALTER TABLE `pay_transaction_hash`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `pending_order_status`
--
ALTER TABLE `pending_order_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT для таблицы `permissions_group`
--
ALTER TABLE `permissions_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `proxies_payment`
--
ALTER TABLE `proxies_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `proxies_qiwi`
--
ALTER TABLE `proxies_qiwi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `qiwi_currencies`
--
ALTER TABLE `qiwi_currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `referrals_info_logs`
--
ALTER TABLE `referrals_info_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `referral_links`
--
ALTER TABLE `referral_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `referral_log`
--
ALTER TABLE `referral_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `referral_programs`
--
ALTER TABLE `referral_programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `referral_relationships`
--
ALTER TABLE `referral_relationships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `referral_statistics`
--
ALTER TABLE `referral_statistics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requirements_verification`
--
ALTER TABLE `requirements_verification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites`
--
ALTER TABLE `requisites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites_blacklist`
--
ALTER TABLE `requisites_blacklist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites_fields`
--
ALTER TABLE `requisites_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites_group`
--
ALTER TABLE `requisites_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `requisites_has_fields`
--
ALTER TABLE `requisites_has_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites_has_info_fields`
--
ALTER TABLE `requisites_has_info_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites_info_fields`
--
ALTER TABLE `requisites_info_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites_list`
--
ALTER TABLE `requisites_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserves`
--
ALTER TABLE `reserves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserves_alerts`
--
ALTER TABLE `reserves_alerts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserves_files`
--
ALTER TABLE `reserves_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserves_files_groups`
--
ALTER TABLE `reserves_files_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserve_group`
--
ALTER TABLE `reserve_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `reserve_log`
--
ALTER TABLE `reserve_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserve_log_profit`
--
ALTER TABLE `reserve_log_profit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reserve_request`
--
ALTER TABLE `reserve_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reward`
--
ALTER TABLE `reward`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `reward_log`
--
ALTER TABLE `reward_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reward_programs`
--
ALTER TABLE `reward_programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `selected_courses`
--
ALTER TABLE `selected_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `settings_referrals`
--
ALTER TABLE `settings_referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `settings_reward`
--
ALTER TABLE `settings_reward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `settings_theme`
--
ALTER TABLE `settings_theme`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `social_auth_system`
--
ALTER TABLE `social_auth_system`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `social_reviews`
--
ALTER TABLE `social_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_card_details`
--
ALTER TABLE `tasks_card_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_chat`
--
ALTER TABLE `tasks_chat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_comments`
--
ALTER TABLE `tasks_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_convert_log`
--
ALTER TABLE `tasks_convert_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_fields`
--
ALTER TABLE `tasks_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_history_operators`
--
ALTER TABLE `tasks_history_operators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_info`
--
ALTER TABLE `tasks_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_profits`
--
ALTER TABLE `tasks_profits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_rates_data`
--
ALTER TABLE `tasks_rates_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_rejection_status`
--
ALTER TABLE `tasks_rejection_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `tasks_requisites`
--
ALTER TABLE `tasks_requisites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_shots`
--
ALTER TABLE `tasks_shots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_status`
--
ALTER TABLE `tasks_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `tasks_status_log`
--
ALTER TABLE `tasks_status_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks_user`
--
ALTER TABLE `tasks_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `task_log`
--
ALTER TABLE `task_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `task_log_confirmation`
--
ALTER TABLE `task_log_confirmation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `task_private_hash`
--
ALTER TABLE `task_private_hash`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `task_reports`
--
ALTER TABLE `task_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `task_single_log_confirm`
--
ALTER TABLE `task_single_log_confirm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `telescope_entries`
--
ALTER TABLE `telescope_entries`
  MODIFY `sequence` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `template_type_events`
--
ALTER TABLE `template_type_events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `unpaid_items`
--
ALTER TABLE `unpaid_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `update_systems`
--
ALTER TABLE `update_systems`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users_history_profiles`
--
ALTER TABLE `users_history_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user_auth`
--
ALTER TABLE `user_auth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user_balance`
--
ALTER TABLE `user_balance`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `user_balance_log`
--
ALTER TABLE `user_balance_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user_wallet_stories`
--
ALTER TABLE `user_wallet_stories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `verification_card`
--
ALTER TABLE `verification_card`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `verification_card_category`
--
ALTER TABLE `verification_card_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `verification_card_instructions`
--
ALTER TABLE `verification_card_instructions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wallets_addresses`
--
ALTER TABLE `wallets_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wallets_history`
--
ALTER TABLE `wallets_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wallet_transactions`
--
ALTER TABLE `wallet_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `whitebit_withdraw`
--
ALTER TABLE `whitebit_withdraw`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `whitelist_order`
--
ALTER TABLE `whitelist_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `withdrawal_request`
--
ALTER TABLE `withdrawal_request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `withdrawal_request_log`
--
ALTER TABLE `withdrawal_request_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `yandex_currencies`
--
ALTER TABLE `yandex_currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `your_exchange`
--
ALTER TABLE `your_exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `checks`
--
ALTER TABLE `checks`
  ADD CONSTRAINT `checks_host_id_foreign` FOREIGN KEY (`host_id`) REFERENCES `hosts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `contests_has_contests_users`
--
ALTER TABLE `contests_has_contests_users`
  ADD CONSTRAINT `contests_has_contests_users_contests_user_id_foreign` FOREIGN KEY (`contests_user_id`) REFERENCES `contests_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contests_has_contests_users_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `contests` (`id`);

--
-- Ограничения внешнего ключа таблицы `currency_in_has_fields`
--
ALTER TABLE `currency_in_has_fields`
  ADD CONSTRAINT `currency_in_has_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `currency_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `currency_in_has_fields_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `currencies` (`id`);

--
-- Ограничения внешнего ключа таблицы `currency_merchants`
--
ALTER TABLE `currency_merchants`
  ADD CONSTRAINT `currency_merchants_gateway_merchant_id_foreign` FOREIGN KEY (`gateway_merchant_id`) REFERENCES `gateways_merchants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `currency_merchants_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `currencies` (`id`);

--
-- Ограничения внешнего ключа таблицы `currency_out_has_fields`
--
ALTER TABLE `currency_out_has_fields`
  ADD CONSTRAINT `currency_out_has_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `currency_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `currency_out_has_fields_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `currencies` (`id`);

--
-- Ограничения внешнего ключа таблицы `currency_requisites_has_fields`
--
ALTER TABLE `currency_requisites_has_fields`
  ADD CONSTRAINT `currency_requisites_has_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `requisites_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `currency_requisites_has_fields_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `currencies` (`id`);

--
-- Ограничения внешнего ключа таблицы `directions_has_allowed_countries`
--
ALTER TABLE `directions_has_allowed_countries`
  ADD CONSTRAINT `directions_has_allowed_countries_geo_country_list_id_foreign` FOREIGN KEY (`geo_country_list_id`) REFERENCES `geo_country_list` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directions_has_allowed_countries_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `directions_has_cities`
--
ALTER TABLE `directions_has_cities`
  ADD CONSTRAINT `directions_has_cities_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directions_has_cities_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `directions_has_fields`
--
ALTER TABLE `directions_has_fields`
  ADD CONSTRAINT `directions_has_fields_direction_field_id_foreign` FOREIGN KEY (`direction_field_id`) REFERENCES `directions_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directions_has_fields_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `directions_has_forbidden_countries`
--
ALTER TABLE `directions_has_forbidden_countries`
  ADD CONSTRAINT `directions_has_forbidden_countries_geo_country_list_id_foreign` FOREIGN KEY (`geo_country_list_id`) REFERENCES `geo_country_list` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directions_has_forbidden_countries_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `directions_has_modes`
--
ALTER TABLE `directions_has_modes`
  ADD CONSTRAINT `directions_has_modes_direction_exchange_mode_id_foreign` FOREIGN KEY (`direction_exchange_mode_id`) REFERENCES `direction_exchange_modes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directions_has_modes_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `directions_has_networks`
--
ALTER TABLE `directions_has_networks`
  ADD CONSTRAINT `directions_has_networks_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`),
  ADD CONSTRAINT `directions_has_networks_network_id_foreign` FOREIGN KEY (`network_id`) REFERENCES `currencies_networks` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `directions_has_requisites`
--
ALTER TABLE `directions_has_requisites`
  ADD CONSTRAINT `directions_has_requisites_direction_requisite_id_foreign` FOREIGN KEY (`direction_requisite_id`) REFERENCES `direction_requisites` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `directions_has_requisites_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `direction_exchange_merchants`
--
ALTER TABLE `direction_exchange_merchants`
  ADD CONSTRAINT `direction_exchange_merchants_gateway_merchant_id_foreign` FOREIGN KEY (`gateway_merchant_id`) REFERENCES `gateways_merchants` (`id`),
  ADD CONSTRAINT `direction_exchange_merchants_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `direction_exchange` (`id`);

--
-- Ограничения внешнего ключа таблицы `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `parser_exchange_log`
--
ALTER TABLE `parser_exchange_log`
  ADD CONSTRAINT `parser_exchange_log_id_parser_exchange_foreign` FOREIGN KEY (`id_parser_exchange`) REFERENCES `parser_exchange` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `password_histories`
--
ALTER TABLE `password_histories`
  ADD CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `referral_links`
--
ALTER TABLE `referral_links`
  ADD CONSTRAINT `referral_links_referral_program_id_foreign` FOREIGN KEY (`referral_program_id`) REFERENCES `referral_programs` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `referral_relationships`
--
ALTER TABLE `referral_relationships`
  ADD CONSTRAINT `referral_relationships_referral_link_id_foreign` FOREIGN KEY (`referral_link_id`) REFERENCES `referral_links` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `requisites_has_fields`
--
ALTER TABLE `requisites_has_fields`
  ADD CONSTRAINT `requisites_has_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `requisites_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `requisites_has_fields_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `requisites` (`id`);

--
-- Ограничения внешнего ключа таблицы `requisites_has_info_fields`
--
ALTER TABLE `requisites_has_info_fields`
  ADD CONSTRAINT `requisites_has_info_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `requisites_info_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `requisites_has_info_fields_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `requisites` (`id`);

--
-- Ограничения внешнего ключа таблицы `reserves_alerts`
--
ALTER TABLE `reserves_alerts`
  ADD CONSTRAINT `reserves_alerts_id_reserve_foreign` FOREIGN KEY (`id_reserve`) REFERENCES `reserves` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `task_log_confirmation`
--
ALTER TABLE `task_log_confirmation`
  ADD CONSTRAINT `task_log_confirmation_id_task_foreign` FOREIGN KEY (`id_task`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `task_reports`
--
ALTER TABLE `task_reports`
  ADD CONSTRAINT `task_reports_id_task_foreign` FOREIGN KEY (`id_task`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_referral_program_id_foreign` FOREIGN KEY (`referral_program_id`) REFERENCES `referral_programs` (`id`) ON DELETE SET NULL;
COMMIT;
