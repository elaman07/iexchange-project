$( function()
{
    var ul_sortable = $('#sortable');

    ul_sortable.sortable({
        revert: 100,
        placeholder: 'placeholder'
    });

    ul_sortable.disableSelection();

    var btn_save = $('button.save'), div_response = $('#response');

    btn_save.on('click', function(e) {
        e.preventDefault();
        var sortable_data = ul_sortable.sortable('serialize');
        console.log('Сохранение позиций...');
        $.ajax({
            data: sortable_data,
            type: 'POST',
            url: '/admin/private/update_position',
            success: function (result) {
                console.log(result)
            }
        });
    });
} );