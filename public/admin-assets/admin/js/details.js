function pageTransitions() {
    var a = [".pt-page-moveFromLeft", "pt-page-moveFromRight", "pt-page-moveFromTop", "pt-page-moveFromBottom", "pt-page-fade", "pt-page-moveFromLeftFade", "pt-page-moveFromRightFade", "pt-page-moveFromTopFade", "pt-page-moveFromBottomFade", "pt-page-scaleUp", "pt-page-scaleUpCenter", "pt-page-flipInLeft", "pt-page-flipInRight", "pt-page-flipInBottom", "pt-page-flipInTop", "pt-page-rotatePullRight", "pt-page-rotatePullLeft", "pt-page-rotatePullTop", "pt-page-rotatePullBottom", "pt-page-rotateUnfoldLeft", "pt-page-rotateUnfoldRight", "pt-page-rotateUnfoldTop", "pt-page-rotateUnfoldBottom"];
    for (var b in a) {
        var c = a[b];
        if ($(".add-transition").hasClass(c)) return $(".add-transition").addClass(c + "-init page-transition"), void setTimeout(function() {
            $(".add-transition").removeClass(c + " " + c + "-init page-transition")
        }, 1200)
    }
}

$(document).ready(function() {
    pageTransitions(), $(".dropdown").on("show.bs.dropdown", function(a) {
        $(this).find(".dropdown-menu").first().stop(!0, !0).slideDown()
    }), $(".dropdown").on("hide.bs.dropdown", function(a) {
        $(this).find(".dropdown-menu").first().stop(!0, !0).slideUp()
    })
});

// document.getElementById('fullscreen-btn').addEventListener('click', () => {
//     screenfull.toggle($('html')[0]);
// });

$('#fullscreen-btn2').on('click',function(e)
{
    e.preventDefault();
    screenfull.toggle();

    var $this = $(this);
    if ($this.children('.md-font').hasClass('fa-expand'))
    {
        console.log('123')

        $this.children('.md-font').removeClass('fa-expand');
        $this.children('.md-font').addClass('fa-compress');
    }
    else if ($this.children('md-icon').hasClass('fa-compress'))
    {
        $this.children('md-icon').removeClass('fa-compress');
        $this.children('md-icon').addClass('fa-expand');
    }

})