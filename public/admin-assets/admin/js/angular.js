(function(){
    'use strict';

    var admin = angular.module('adminPanel',
    [
        'ngMaterial',
        'ui.bootstrap',
        'dataGrid',
        'pagination',
        'ngAnimate',
        'ngAria',
        'ngSlimScroll',
        'ngclipboard',
        'ui.sortable',
        'ion.rangeslider'
])    .config(['$compileProvider','$interpolateProvider','$httpProvider','$mdIconProvider','$mdThemingProvider','$mdAriaProvider',
        '$logProvider','$qProvider',
        function($compileProvider, $interpolateProvider,$httpProvider, $mdIconProvider,$mdThemingProvider,$mdAriaProvider,$logProvider,$qProvider)
{
    $logProvider.debugEnabled(true);
    $compileProvider.debugInfoEnabled(false);
    $compileProvider.commentDirectivesEnabled(false);
    $compileProvider.cssClassDirectivesEnabled(false);
    $compileProvider.preAssignBindingsEnabled(true);
    $mdAriaProvider.disableWarnings();
    $qProvider.errorOnUnhandledRejections(false);


    //Новые параметры для определение angular
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    //Фиксируем тему для шаблона
    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('blue');

    // $mdThemingProvider.theme('green')
    //     .primaryPalette('blue')
    //     .accentPalette('blue');
    //
    //
    // $mdThemingProvider.theme('default')
    //     .primaryPalette('blue')
    //     .accentPalette('blue');

    $mdIconProvider
        .defaultFontSet('FontAwesome')
        .fontSet('fa', 'FontAwesome');

    // var csrf_app     =  $('meta[name=csrf-token]').attr('content');
    // $httpProvider.defaults.headers.common['x-csrf-token'] = csrf_app;
    // $httpProvider.defaults.headers.post['x-csrf-token'] = csrf_app;
}]);

admin.controller('BaseController', ['$scope','$http','$uibModal',
    '$httpParamSerializer','$timeout','$mdSidenav','$mdUtil','$log','$mdToast',
    function($scope, $http,$uibModal, $httpParamSerializer,$timeout,$mdSidenav,$mdUtil,$log, $mdToast)
{
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.lockLeft = true;

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildToggler(navID) {
        var debounceFn =  $mdUtil.debounce(function(){
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    $log.debug("toggle " + navID + " is done");
                });
        },300);

        return debounceFn;
    }

    $scope.onEventClipboardSuccess = function(e) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(e+' Copied')
                .position('top right')
                .hideDelay(1000)
        );
    }

    $scope.new_postponed = 0;
    $scope.new_task = 0;
    $scope.new_payment_bonuses = 0;
    $scope.reserve_request = 0;
    $scope.verification_card = 0;
    $scope.reviews_count = 0;
    $scope.live = {
        data: {}
    };

    $scope.reload = function () {

        $http({
            method: 'GET',
            url: '/admin/ajax/order'
        }).then(function successCallback(response) {

            $scope.new_task = response.data.attributes.counts.count;
            $scope.new_payment_bonuses = response.data.attributes.counts.payment_bonuses;
            $scope.reserve_request = response.data.attributes.counts.reserve_request;
            $scope.new_postponed = response.data.attributes.counts.new_postponed;
            $scope.verification_card = response.data.attributes.counts.verification_card;
            $scope.reviews_count = response.data.attributes.counts.reviews_count;

            $scope.live.data = response.data.attributes.live_orders;

        }, function errorCallback(response)
        {
            if(response.status == 503) {
                window.location.href = '/';
            }
        });


        $timeout(function(){
            $scope.reload();
        }, 30000);
    };
    $scope.reload();


    $scope.listReceivingRepeat = {};
    $scope.giveRate = function(id_currency)
    {
        $http({
            method: 'POST',
            url: '/admin/private/list_receiving',
            data: {
                'id_currency': id_currency
            }
        }).then(function successCallback(response) {
            $scope.listReceivingRepeat = response.data;
        }, function errorCallback(response)
        {
            if(response.status == 503) {
                window.location.href = '/';
            }
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

}])


    .controller('SettingsSidenav', function ($scope, $timeout, $mdSidenav, $log, $mdMedia)
    {
        $scope.isMobile = $mdMedia('sm');

        $scope.openLeftMenu = function() {
            $mdSidenav('left').toggle();
        };

        $scope.toggleSettings = buildDelayedToggler('settings');

        $scope.close = function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('left').close()
                .then(function () {
                    $log.debug("close LEFT is done");
                });
        };

        /**
         * Supplies a function that will continue to operate until the
         * time is up.
         */
        function debounce(func, wait, context) {
            var timer;

            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }

        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildDelayedToggler(navID) {
            return debounce(function() {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }

        function buildToggler(navID) {
            return function() {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            };
        }


        $scope.close = function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('settings').close()
                .then(function () {
                    $log.debug("close RIGHT is done");
                });
        };
    })
    .controller('TickerController',['$scope','$http','$mdToast','$mdDialog','$window','$interval', function($scope,$http,$mdToast, $mdDialog,$window,$interval)
    {




        $scope.confirmations = null;
        $scope.isButton = false;
        $scope.isButtonDisabled = false;

        $scope.disableClick = function() {
            $scope.isButton = true;

            $interval(function() {
                $scope.isButtonDisabled = true;
            }, 1000);
        };

        //Передать заявку другому оператору
        $scope.transferToAnother = function($event, $id)
        {
            $scope.listOperators = {};
            $scope.selectOperator = null;

            $http({
                method: 'GET',
                url: '/admin/private/list_operators?id='+$id,
            })
                .then(function successCallback(response) {
                    $scope.listOperators = response.data;
                }, function errorCallback(response) {
                    //
                });


            $mdDialog.show({
                controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window) {
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };

                    $scope.selectedOperator = function($event, $id_user)
                    {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Пожалуйста, подождите...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/change_operators',
                            data: {
                                'id': $id,
                                'id_operator': $id_user,
                                'api_panel': 'v1'
                            }
                        })
                            .then(function successCallback(response)
                            {
                                if(response.data.status == 0) {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent('Оператор успешно изменен...')
                                            .position('top right')
                                            .hideDelay(3000)
                                    );
                                    $window.location.href = response.data.redirect;
                                }
                            }, function errorCallback(response) {
                                //
                            });
                    }
                }],
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/transfer_to_operator.html',
                targetEvent: $event,
            });
        };

        $scope.successPayment = function($event, $paymentName, $merchant, $amount, $id)
        {
            $scope.payment_name = $paymentName;
            $scope.type = $merchant;
            $scope.amount = $amount;

            $mdDialog.show({
                controller: ['$scope', '$mdDialog','$window','$interval', function($scope, $mdDialog, $window,$interval) {

                    $scope.feeAmount = false;
                    $scope.notPartner = false;
                    $scope.notCashback = false;
                    $scope.idTransaction = null;
                    $scope.hasBalance = 0;
                    $scope.isButtonDisabled = false;
                    $scope.toolbarCustom = '';
                    $scope.isError = false;
                    $scope.ErrorMessage = null;

                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                    $scope.success = function($event)
                    {
                        $scope.toolbarCustom = '';
                        $scope.isButtonDisabled = true;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Пожалуйста, подождите...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $scope.isError = false;
                        $scope.ErrorMessage = null;

                        $http({
                            method: 'POST',
                            url: '/admin/private/success_operator_payment',
                            data: {
                                'id': $id,
                                'type': $scope.type,
                                'actions': 'success',
                                'payment': $scope.payment_name,
                                'id_transaction': $scope.idTransaction,
                                'manual_fee': $scope.amount,
                                'fee':  $scope.feeAmount,
                                'not_partner':  $scope.notPartner,
                                'not_cashback':  $scope.notCashback,
                                'api_panel': 'v1'
                            }
                        }).then(function successCallback(response)
                        {
                            if(response.data.status == 1)
                            {
                                $scope.toolbarCustom = 'toolbar-error';
                                $mdToast.show({
                                    template: '<md-toast class="md-toast md-toast-error">Ошибка: ' + response.data.message + '</md-toast>',
                                    hideDelay: 6000,
                                    position: 'top right'
                                });

                                $scope.isError = true;
                                $scope.ErrorMessage  = response.data.message;

                                $interval(function() {
                                    $scope.isButtonDisabled = false;
                                }, 1000);

                            } else {
                                $mdToast.show({
                                    template: '<md-toast class="md-toast md-toast-success">Заявка успешно выполнена</md-toast>',
                                    hideDelay: 3000,
                                    position: 'top right'
                                });

                                $window.location.href = response.data.redirect;
                            }

                        }, function errorCallback(response) {
                            //
                        });
                    }

                }],
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/success-task-2.html',
                targetEvent: $event,
            });

        };

        //Отложить
        $scope.postpone = function($event, $id, $statusses)
        {
            $scope.list_statusses = JSON.parse($statusses);
            $scope.deferType = 1;

            $mdDialog.show({
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/postpone-task.html',
                targetEvent: $event,
                controller: ['$scope', '$mdDialog','$window','$interval', function($scope, $mdDialog, $window,$interval) {

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.confirmDefer = function($event)
                    {
                        $scope.isButtonDisabled = true;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Процесс отложения заявки...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $scope.isError = false;
                        $scope.ErrorMessage = null;

                        $http({
                            method: 'POST',
                            url: '/admin/private/success_operator_payment',
                            data: {
                                'id': $id,
                                'actions': 'defer',
                                'defer_type': $scope.deferType,
                                'api_panel':    'v1'
                            }
                        }).then(function successCallback(response)
                        {
                            if(response.data.status == 1)
                            {
                                $scope.toolbarCustom = 'toolbar-error';
                                $mdToast.show({
                                    template: '<md-toast class="md-toast md-toast-error">Ошибка: ' + response.data.message + '</md-toast>',
                                    hideDelay: 6000,
                                    position: 'top right'
                                });

                                $scope.isError = true;
                                $scope.ErrorMessage  = response.data.message;

                                $interval(function() {
                                    $scope.isButtonDisabled = false;
                                }, 1000);

                            } else {
                                $mdToast.show({
                                    template: '<md-toast class="md-toast md-toast-success">Заявка успешно отложена</md-toast>',
                                    hideDelay: 3000,
                                    position: 'top right'
                                });

                                $window.location.href = response.data.redirect;
                            }

                        }, function errorCallback(response) {
                            //
                        });
                    }
                }],
            });
        }

        //Отклонить заявку
        $scope.reject = function($event, $id, $statusses)
        {
            $scope.list_statusses = JSON.parse($statusses);
            $scope.categoryReject = 1;

            $mdDialog.show({
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/reject-task.html',
                targetEvent: $event,
                controller: ['$scope', '$mdDialog','$window','$interval', function($scope, $mdDialog, $window,$interval) {

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.confirmReject = function($event)
                    {
                        $scope.isButtonDisabled = true;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Процесс отклонения заявки...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $scope.isError = false;
                        $scope.ErrorMessage = null;

                        $http({
                            method: 'POST',
                            url: '/admin/private/success_operator_payment',
                            data: {
                                'id': $id,
                                'actions': 'failed',
                                'rejection_status': $scope.categoryReject,
                                'api_panel':    'v1'
                            }
                        }).then(function successCallback(response)
                        {
                            if(response.data.status == 1)
                            {
                                $scope.toolbarCustom = 'toolbar-error';
                                $mdToast.show({
                                    template: '<md-toast class="md-toast md-toast-error">Ошибка: ' + response.data.message + '</md-toast>',
                                    hideDelay: 6000,
                                    position: 'top right'
                                });

                                $scope.isError = true;
                                $scope.ErrorMessage  = response.data.message;

                                $interval(function() {
                                    $scope.isButtonDisabled = false;
                                }, 1000);

                            } else {
                                $mdToast.show({
                                    template: '<md-toast class="md-toast md-toast-success">Заявка успешно отклонена</md-toast>',
                                    hideDelay: 3000,
                                    position: 'top right'
                                });

                                $window.location.href = response.data.redirect;
                            }

                        }, function errorCallback(response) {
                            //
                        });
                    }
                }],
            });
        };

        $scope.checkPayment = function($code, $id)
        {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Обновление данных...')
                    .position('top right')
                    .hideDelay(3000)
            );

            $http.post('/admin/private/check_receipts', {
                'id':   $id,
                'payment': $code,
                'api_panel': 'v1'
            }).then(function(response)
            {
                if(response.data.status == 1)
                {
                    if(response.data.message) {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(response.data.message)
                                .position('top right')
                                .hideDelay(3000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Средства не поступили')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    }
                } else {

                    if(response.data.redirect != null) {
                        $window.location.href = response.data.redirect;
                    }

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Данные успешно обновлены')
                            .position('top right')
                            .hideDelay(3000)
                    );
                }


                $scope.confirmations = response.data.items.confirmations;
            });
        }
    }])

    .controller('userController', ['$http','$scope','$uibModal','userFactory', function ($http,$scope,$uibModal,userFactory)
    {
        $scope.gridOptions = {
            data: [],
            getData: userFactory.getData(),
            sort: {
                predicate: 'orderNo',
                direction: 'asc'
            }
        };

        userFactory.getData().then(function (responseData) {
            $scope.gridOptions.data = responseData.data;
        });
    }])

    .controller('LiveTaskController', ['$scope', '$http','$uibModal','$timeout', function ($scope, $http,$uibModal,$timeout)
    {

        $scope.live = {
            data: {}
        };

        $scope.reload = function ()
        {
            $http({
                method: 'GET',
                url: '/admin/json/live_task'
            }).then(function successCallback(response) {
                $scope.live.data = response.data;
            }, function errorCallback(response) {
                if(response.status == 503) {
                    window.location.href = '/';
                }
            });


            $timeout(function(){
                $scope.reload();
            }, 15000)
        };

        $scope.reload();


        $scope.moreInfo = function ($index_item, id)
        {
            $window.location.href= "/"


            // $http({
            //     method: 'POST',
            //     url: '/admin/private/info_task/'+id
            // }).then(function successCallback(response) {
            //
            //     $uibModal.open({
            //         animation: true,
            //         ariaLabelledBy: 'modal-title-top',
            //         ariaDescribedBy: 'modal-body-top',
            //         templateUrl: '/partials/admin/info_task.html',
            //         size: 'lg',
            //         backdrop: 'static',
            //         scope: $scope,
            //         controller: ['$scope','$uibModalInstance','$window', function($scope,$uibModalInstance,$window) {
            //
            //             $scope.item = response.data;
            //
            //             $scope.close = function() {
            //                 $uibModalInstance.close();
            //             }
            //
            //             $scope.deleteTask = function (id)
            //             {
            //
            //                 $http({
            //                     method: 'GET',
            //                     url: '/admin/private/task_status/'+id+'?action=reject',
            //                     headers: {
            //                         'Content-type': 'application/json;charset=utf-8'
            //                     }
            //                 }).then(function successCallback(response) {
            //                     $window.location.href = '/admin/applications/live';
            //
            //                 }, function errorCallback(response) {
            //                     console.log(response)
            //                 });
            //             }
            //
            //             $scope.successHandler = function(id) {
            //                 $http({
            //                     method: 'GET',
            //                     url: '/admin/private/task_status/'+id+'?action=success',
            //                     headers: {
            //                         'Content-type': 'application/json;charset=utf-8'
            //                     }
            //                 }).then(function successCallback(response) {
            //                     $window.location.href = '/admin/applications/live';
            //
            //                 }, function errorCallback(response) {
            //                 });
            //             }
            //
            //         }]
            //     });
            //
            // }, function errorCallback(response) {
            //     // called asynchronously if an error occurs
            //     // or server returns response with an error status.
            // });


        }
    }])


    .controller('ApplicationDetails', ['$scope','$mdDialog','$http', function($scope, $mdDialog, $http)
    {
        $scope.disableClick = function() {
            $scope.isDisabled = true;
            return false;
        }

        $scope.AppClick = function($event, $payment_name, $merchant = 'default', $amount)
        {
            $scope.payment_name = $payment_name;
            $scope.merchant = $merchant;

            console.log($payment_name);

            if($payment_name == 'sberbank') {
                $scope.amount = ($amount * 1 / 100).toFixed(2);
            } else if($payment_name == 'zcash') {
                $scope.amount = (0.0002);
            } else if($payment_name == 'nem') {
                $scope.amount = (0.05);
            } else if($payment_name == 'cardano') {
                $scope.amount = (1);
            } else if($payment_name == 'bitcoin-cash') {
                $scope.amount = (0.001);
            } else {
                $scope.amount = 0;
            }


            $mdDialog.show({
                controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window) {
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                }],
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/success-task.html',
                targetEvent: $event,
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        }


    }])
    .controller('taskController', ['$scope', '$http','taskFactory','$uibModal', function ($scope, $http, taskFactory,$uibModal)
    {
        $scope.gridOptions = {
            data: [],
            getData: taskFactory.getData(),
            sort: {
                predicate: 'orderNo',
                direction: 'asc'
            }
        };

        taskFactory.getData().then(function (responseData) {
            $scope.gridOptions.data = responseData.data;
        });

        $scope.UI = {};
        $scope.gridActions = {};
        taskFactory.getStatuses().then(function (resp) {
            $scope.UI.statusOptions = resp.data;
        });


        $scope.moreInfo = function ($index_item, id) {

            $http({
                method: 'POST',
                url: '/admin/private/info_task/'+id
            }).then(function successCallback(response) {

                $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title-top',
                    ariaDescribedBy: 'modal-body-top',
                    templateUrl: '/partials/admin/info_task.html',
                    size: 'lg',
                    backdrop: 'static',
                    scope: $scope,
                    controller: ['$scope','$uibModalInstance','$window', function($scope,$uibModalInstance,$window) {

                        $scope.item = response.data;

                        $scope.close = function() {
                            $uibModalInstance.close();
                        }

                        $scope.deleteTask = function (id) {

                            $http({
                                method: 'GET',
                                url: '/admin/private/task_status/'+id+'?action=reject',
                                headers: {
                                    'Content-type': 'application/json;charset=utf-8'
                                }
                            }).then(function successCallback(response) {
                                $window.location.href = '/admin/applications';

                            }, function errorCallback(response) {
                                console.log(response)
                            });

                            // $http({
                            //     method: 'DELETE',
                            //     url: '/admin/private/delete_task/'+id,
                            //     headers: {
                            //         'Content-type': 'application/json;charset=utf-8'
                            //     }
                            // }).then(function successCallback(response) {
                            //     console.log(response.data)
                            //     $window.location.href = '/admin/applications';
                            //
                            // }, function errorCallback(response) {
                            //     console.log(response.data)
                            // });
                        }

                        $scope.successHandler = function(id) {
                            $http({
                                method: 'GET',
                                url: '/admin/private/task_status/'+id+'?action=success',
                                headers: {
                                    'Content-type': 'application/json;charset=utf-8'
                                }
                            }).then(function successCallback(response) {
                                $window.location.href = '/admin/applications';

                            }, function errorCallback(response) {
                            });
                        }

                    }]
                });

            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });


        }
    }])

    .controller('RolesController', ['$mdDialog','$mdMenu', function($mdDialog,$mdMenu) {

        var originatorEv;

        this.openMenu = function($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };

    }])

    .controller('CommonController', ['$mdDialog','$mdMenu', function($mdDialog,$mdMenu) {

        var originatorEv;

        this.openMenu = function($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };

    }])

    .factory('taskFactory',['$http', function ($http)
    {
        return {
            getData: function () {
                return $http({
                    method: 'GET',
                    url: '/admin/json/list_task'
                });
            },
            getStatuses: function() {
                return $http.get('/admin/json/list_task/statuses');
            }
        }
    }])

    .factory('userFactory',['$http', function ($http)
    {
        return {
            getData: function () {
                return $http({
                    method: 'GET',
                    url: '/admin/json/users'
                });
            }
        }
    }])




    .directive('htmlcompile', ['$compile', function ($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    return scope.$eval(attrs.htmlcompile);
                },
                function(value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                }
            );
        };
    }])

    .directive('compile', ['$compile', function ($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        };
    }]).directive('htmlcompile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                return scope.$eval(attrs.htmlcompile);
            },
            function(value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        );
    };
}])

    .directive('compile', ['$compile', function ($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        };
    }])

    .directive('uiSwitch', ['$window', '$timeout','$log', '$parse', function($window, $timeout, $log, $parse) {

        /**
         * Initializes the HTML element as a Switchery switch.
         *
         * $timeout is in place as a workaround to work within angular-ui tabs.
         *
         * @param scope
         * @param elem
         * @param attrs
         * @param ngModel
         */
        function linkSwitchery(scope, elem, attrs, ngModel) {
            if(!ngModel) return false;
            var options = {};
            try {
                options = $parse(attrs.uiSwitch)(scope);
            } catch (e) {}

            var switcher;

            attrs.$observe('disabled', function(value) {
                if (!switcher) {
                    return;
                }

                if (value) {
                    switcher.disable();
                } else {
                    switcher.enable();
                }
            });

            // Watch changes
            scope.$watch(function () {
                return ngModel.$modelValue;
            }, function(newValue,oldValue) {
                initializeSwitch()
            });

            function initializeSwitch() {
                $timeout(function() {
                    // Remove any old switcher
                    if (switcher) {
                        angular.element(switcher.switcher).remove();
                    }
                    // (re)create switcher to reflect latest state of the checkbox element
                    switcher = new $window.Switchery(elem[0], {
                        'color': '#4CAF50',
                        'speed': '0.6s',
                        'secondaryColor': '#dfdfdf',
                        'jackColor': '#fff',
                        'jackSecondaryColor': null,
                        'disabledOpacity': '0.5',
                        'size' : 'default'
                    });
                    var element = switcher.element;
                    element.checked = scope.initValue;
                    if (attrs.disabled) {
                        switcher.disable();
                    }

                    switcher.setPosition(false);
                    element.addEventListener('change',function(evt) {
                        scope.$apply(function() {
                            ngModel.$setViewValue(element.checked);
                        })
                    });
                    scope.$watch('initValue', function(newValue, oldValue) {
                        switcher.setPosition(false);
                    });
                }, 0);
            }
            initializeSwitch();
        }

        return {
            require: 'ngModel',
            restrict: 'AE',
            scope : {
                initValue : '=ngModel'
            },
            link: linkSwitchery
        }
    }])

    .filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }

})

}());
