$(function()
{
    $('[data-toggle="tooltip"]').tooltip();
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $(".panel-fullscreen").on('click',function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.children('i').hasClass('fa-expand'))
        {
            $this.children('i').removeClass('fa-expand');
            $this.children('i').addClass('fa-compress');
        }
        else if ($this.children('i').hasClass('fa-compress'))
        {
            $this.children('i').removeClass('fa-compress');
            $this.children('i').addClass('fa-expand');
        }

        $(this).closest('.x_panel').toggleClass('panel-to-fullscreen');
    });

    $('#fullscreen-btn').on('click',function(e) {
        e.preventDefault();
        screenfull.toggle();

        var $this = $(this);

        if ($this.children('md-icon').hasClass('fa-expand'))
        {
            $this.children('md-icon').removeClass('fa-expand');
            $this.children('md-icon').addClass('fa-compress');
        }
        else if ($this.children('md-icon').hasClass('fa-compress'))
        {
            $this.children('md-icon').removeClass('fa-compress');
            $this.children('md-icon').addClass('fa-expand');
        }

    })

    $('a[data-custom-tab="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("data-tab-title"); // activated tab
        $(".setting-sidenav-title h2").text(target)
    });


    $('.datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    $('.timepicker').datetimepicker({
        locale: 'ru',
        format: 'HH:mm'
    });



    $('table.manager-table').DataTable({
        "language": {
            "url": "/admin-assets/admin/components/datatable/i18n/Russian.json"
        },
        "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "Все"]]
    });


    $('#default').DataTable({
        "language": {
            "url": "/admin-assets/admin/components/datatable/i18n/Russian.json"
        },
        "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "Все"]],
        columnDefs: [
            { targets: 'not-sortable', orderable: false }
        ]
    });

    $('#default2').DataTable({
        "language": {
            "url": "/admin-assets/admin/components/datatable/i18n/Russian.json"
        },
        "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "Все"]]
    });

    $('#default-table').DataTable({
        "language": {
            "url": "/admin-assets/admin/components/datatable/i18n/Russian.json"
        },
        "lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "Все"]],
        columnDefs: [
            { targets: 'not-sortable', orderable: false }
        ],

        "initComplete": function(settings, json) {
            $('div#default-table_wrapper select').addClass('selectpicker show-tick')
                .attr('data-width', 'fit').attr('data-size', 5);
        }
    });



    $('#cp1').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    });

    $('#cp2').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    });

    $('#cp3').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    });

    $('.iex-color-picker').each(function ()
    {
        $('#'+$(this).prop('id')).colorpicker({
            customClass: 'colorpicker-2x',
            sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                },
                alpha: {
                    maxTop: 200
                }
            }
        });
    });

    $('.selectpicker').selectpicker({
        iconBase: 'fas', // the font family for the checkmark
        tickIcon: 'fa-check-circle', // classname for the checkmark
    });



    $('.ckeditor-multieditor-for').each(function ()
    {

        CKEDITOR.replace($(this).prop('id'),
            {
            height: 200,
            filebrowserBrowseUrl: '/admin-assets/admin/components/ckeditor/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/admin-assets/admin/components/ckeditor/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '/in-assets/admin/components/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/admin-assets/admin/components/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            codeSnippet_theme: 'monokai_sublime',

            on: {
                instanceReady: function () {
                    this.dataProcessor.htmlFilter.addRules({
                        elements: {
                            img: function (el) {
                                // Add an attribute.
                                if (!el.attributes.alt)
                                    el.attributes.alt = $('#title_news').val();

                                //el.addClass('responsive-img preview--img__default');
                            }
                        }
                    });
                }
            }
        });
    });



    CKEDITOR.replace( 'ckeditor', {
            height: 200,
            filebrowserBrowseUrl: '/admin-assets/admin/components/ckeditor/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/admin-assets/admin/components/ckeditor/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '/admin-assets/admin/components/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/admin-assets/admin/components/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            codeSnippet_theme: 'monokai_sublime',

            on: {
                instanceReady: function () {
                    this.dataProcessor.htmlFilter.addRules({
                        elements: {
                            img: function (el) {
                                // Add an attribute.
                                if (!el.attributes.alt)
                                    el.attributes.alt = $('#title_news').val();

                                //el.addClass('responsive-img preview--img__default');
                            }
                        }
                    });
                }
            }
        }
    );

    CKEDITOR.replace( 'ckeditor2', {
            height: 200,
            filebrowserBrowseUrl: '/admin-assets/admin/components/ckeditor/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/admin-assets/admin/components/ckeditor/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '/in-assets/admin/components/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/admin-assets/admin/components/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            codeSnippet_theme: 'monokai_sublime',

            on: {
                instanceReady: function () {
                    this.dataProcessor.htmlFilter.addRules({
                        elements: {
                            img: function (el) {
                                // Add an attribute.
                                if (!el.attributes.alt)
                                    el.attributes.alt = $('#title_news').val();

                                //el.addClass('responsive-img preview--img__default');
                            }
                        }
                    });
                }
            }
        }
    );
});

$(function() {

    var $theme_switcher = $('#theme_switcher'),
        $body = $('body');

    $theme_switcher.children('li').click(function(e) {
        e.preventDefault();
        var $this = $(this);

        iex_theme = $this.attr('data-app-theme');

        $theme_switcher.children('li').removeClass('active_theme');
        $(this).addClass('active_theme');

        $body.removeClass('iex-black-theme').addClass(iex_theme);
        if(iex_theme == '') {
            localStorage.removeItem('iex-theme');
        } else {
            localStorage.setItem('iex-theme', iex_theme);
        }
    });


    var selectedTab = localStorage.getItem('iex-theme');
    if (selectedTab) {
        $body.addClass(selectedTab);
        $('li[data-app-theme='+selectedTab+']').addClass('active_theme');
    }
});
