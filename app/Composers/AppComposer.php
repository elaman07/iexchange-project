<?php
namespace App\Composers;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\View\View;


class AppComposer
{
    /**
     * The illuminate config instance.
     *
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * Список доступных типов для сортировки
     *
     * @var array
     */
    protected $selectTypesSorting = [
        ''      => '-- Не выбрано --',
        'asc'   =>  'По возрастанию',
        'desc'  =>  'По убыванию'
    ];

    protected $metaLinkIcons = [
        [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-57x57',
            'size' => '57x57',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-60x60',
            'size' => '60x60',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-72x72',
            'size' => '72x72',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-76x76',
            'size' => '76x76',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-114x114',
            'size' => '114x114',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-120x120',
            'size' => '120x120',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-144x144',
            'size' => '144x144',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-152x152',
            'size' => '152x152',
        ], [
            'rel' => 'apple-touch-icon',
            'filename' => 'apple-icon-180x180',
            'size' => '180x180',
        ], [
            'rel' => 'icon',
            'filename' => 'android-icon-192x192',
            'size' => '192x192',
            'type' => 'image/png'
        ], [
            'rel' => 'icon',
            'filename' => 'favicon-32x32',
            'size' => '32x32',
        ], [
            'rel' => 'icon',
            'filename' => 'favicon-96x96',
            'size' => '96x96',
        ]
    ];

    /**
     * Create a new app composer instance.
     *
     * @param \Illuminate\Contracts\Config\Repository $config
     *
     * @return void
     */
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * Index page view composer.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withSelectTypesSorting($this->selectTypesSorting);
        $view->withAppDomain($this->config->get('app.url'));
        $view->withProductLicence(iex_licence());
        $view->withAppSiteTitle(iEXContentLanguage('sitename'). ' - '. iEXContentLanguage('sitename_desc'));
        $view->with('metaLinkIcons', $this->metaLinkIcons);
    }

}
