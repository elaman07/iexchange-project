<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.10.2019
 * Time: 8:01
 */

namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class StatisticsComposer
{
    protected $listStatisticsTypes = [
        'perfect_money' =>  'PerfectMoney',
        'users_today'  =>  'Пользователи в сутки',
        'orders_today'    =>  'Обменов за сутки',
        'users'  =>  'Всего пользователей',
        'orders'    =>  'Всего обменов',
        'average_order'    =>  'Среднее время обработки заявок',
        'daily_turnover_rub'    =>  'Оборот за сутки в (Р)',
        'daily_turnover_usd'    =>  'Оборот за сутки в ($)',
        'reserve_usd'       =>  'Резервы в ($)',
        'reserve_rub'       =>  'Резервы в (Р)',
        'bestchange_comment'    =>  'Счетчик отзывов из BestChange',
        'other' => 'Другие'
    ];

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withListStatisticsTypes($this->listStatisticsTypes);
    }
}
