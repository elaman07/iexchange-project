<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class SettingComposer
{
    protected $typeDefineBeginner = [
        0 => 'Счет Отдаете',
        1 => 'Счет Получаете',
        2 => 'IP Адрес',
        3 => 'Email адрес'
    ];

    protected $typePhoneNumbers = [
        'RU'    => 'Россия',
        'UA'    =>  'Украина',
        'UZ'    =>  'Узбекистан',
    ];

    protected $typeFormatOrderCheck = [
        0 => 'Отображать полностью',
        1 => 'Скрыть номер счета',
        2 => 'Не скрывать посл. 3-символа',
        3 => 'Не скрывать первые 3-символа',
        4 => 'Не скрывать первые и посл. 3-символа',
        5 => 'Не скрывать посл. 4-символа',
        6 => 'Не скрывать первые 4-символа',
        7 => 'Не скрывать первые и посл. 4-символа'
    ];

    protected $max_time_task = [
        10 => '10 секунд',
        300 => '5 минут',
        600 => '10 минут',
        900 => '15 минут',
        1020 => '17 минут',
        1200 => '20 минут',
        1800 => '30 минут',
        3600 => '1 час',
        7200 => '2 часа'
    ];

    protected $lead_time_task = [
        10 => '10 минут',
        20 => '20 минут',
        30 => '30 минут',
        50 => '50 минут',
        60 => '1 час'
    ];

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withTypeDefineBeginner($this->typeDefineBeginner);

        $view->with('lead_time_task', $this->lead_time_task);
        $view->with('max_time_task', $this->max_time_task);
        $view->with('iex_settings', iEXSetting());
        $view->with('typeFormatOrderCheck', $this->typeFormatOrderCheck);
    }
}