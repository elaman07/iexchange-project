<?php
namespace App\Composers\Admin;


use Illuminate\Contracts\View\View;
use Spatie\Permission\Models\Role;

class UserComposer
{
    /**
     * Для фильтрации пользователей
     *
     * @var array
    */
    protected $filters = [
         ''   => '-- Не выбрано --',
        'id' => 'ID',
        'referral' => 'Реферальным',
        'reward'   => 'Вознаграждениям',
        'created'   =>  'Дате регистрации',
        'last_activity' => 'Посл. посещениям'
    ];

    /**
     * Доступные сортировки
     *
     * @var array
    */
    protected $selectSorting = [
        ''      => '-- Не выбрано --',
        'asc'   =>  'По возрастанию',
        'desc'  =>  'По убыванию'
    ];

    /**
     * Список ролей
     *
     * @var array
    */
    protected $filterAllRoles = [];

    /**
     * Все роли
     *
     * @return void
    */
    public function __construct()
    {
        $this->filterAllRoles = Role::all()->pluck('name', 'name');
    }

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withFilterAllRoles($this->filterAllRoles);
        $view->withUserFilterBy($this->filters);
        $view->withUserSelectSorting($this->selectSorting);
    }
}