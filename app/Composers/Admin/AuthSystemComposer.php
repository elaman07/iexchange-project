<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class AuthSystemComposer
{
    protected $listAuthSystems = [
        'vkontakte' =>  'В контакте',
        'facebook'  =>  'Facebook',
        'yandex'    =>  'Яндекс',
        'coinbase'  =>  'Coinbase',
        'google'    =>  'Google',
        'github'    =>  'GitHub',
        'yahoo'     =>  'Yahoo',
        'tumblr'    =>  'Tumblr',
        'medium'    =>  'Medium',
        'twitter'   =>  'Twitter',
        'paypal'    =>  'PayPal',
        'pinterest' =>  'Pinterest',
        'discord'   =>  'Discord',
        'dribbble'  =>  'Dribbble',
        'linkedin'  =>  'LinkedIn',
        'stripe'    =>  'Stripe',
        'stackexchange' => 'StackExchange',
        'slack'     =>  'Slack',
        'odnoklassniki' => 'OK',
        'gitlab' => 'GitLab',
        'dropbox' => 'Dropbox',
        'bitbucket' => 'Bitbucket',
        'wordpress' => 'Wordpress',
        'zendesk' => 'Zendesk'
    ];

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withListAuthSystems($this->listAuthSystems);
    }
}