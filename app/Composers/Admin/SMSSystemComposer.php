<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class SMSSystemComposer
{
    protected $listSystems = [
        'nexmo' =>  'Nexmo',
        'smscru' => 'SMSC',
        'twilio' => 'Twilio',
        'plivo' => 'Plivo'
    ];

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('listSMSSystems', $this->listSystems);
    }
}