<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class DirectionExchangeComposer
{
    protected $exchangeExportLabels = [
        'manual'      =>  'Manual',
        'juridical'   =>  'Juridical',
        'verifying'   =>  'Verifying',
        'cardverify'  =>  'Cardverify',
        'floating'    =>  'Floating',
        'otherin'     =>  'Otherin',
        'otherout'    =>  'Otherout'
    ];

    protected $exchangeLanguage = [
        'ru' => 'Русский',
        'en' => 'English'
    ];

    protected $exchangeDevice = [
        'desktop'   => 'Компьютерная версия',
        'mobile'    => 'Мобильная версия',
        'tablet'    =>  'Планшетная версия'
    ];

    /**
     * Список статусов
     *
     * @var array
    */
    protected $exchangeStatusses = [];

    /**
     * Сортировка
     *
     * @var array
    */
    protected $exchangeSorting = [];

    /**
     * Конструктор направлений
     *
     * @return void
    */
    public function __construct()
    {
        $this->exchangeStatusses = collect([
            ['id' => '1', 'name' => 'Включенные'],
            ['id' => '0', 'name' => 'Отключенные'],
        ])->pluck('name', 'id');

        $this->exchangeSorting = collect([
            ['id' => 'id-desc', 'name' => 'ID (По убыванию)'],
            ['id' => 'id-asc', 'name' => 'ID (По возрастанию)'],
            ['id' => 'created_at-desc', 'name' => 'Дата добавления (По убыванию)'],
            ['id' => 'created_at-asc', 'name' => 'Дата добавления (По возрастанию)'],
            ['id' => 'updated_at-desc', 'name' => 'Дата обновления (По убыванию)'],
            ['id' => 'updated_at-asc', 'name' => 'Дата обновления (По возрастанию)'],
        ])->pluck('name', 'id');
    }

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withExchangeExportLabels($this->exchangeExportLabels);
        $view->with('exchangeStatusses', $this->exchangeStatusses);
        $view->with('exchangeSorting', $this->exchangeSorting);
        $view->with('exchangeLanguage', $this->exchangeLanguage);
        $view->with('exchangeDevice', $this->exchangeDevice);
    }
}