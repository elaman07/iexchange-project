<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class SocialReviewComposer
{
    protected $listSocialReviews = [
        'vk' =>  'В контакте',
        'facebook'  =>  'Facebook',
        'telegram'    =>  'Telegram',
        'whatsapp'  =>  'WhatsApp',
        'twitter'    =>  'Twitter',
        'instagram'    =>  'Instagram'
    ];

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withListSocialReviews($this->listSocialReviews);
    }
}