<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;

class ContactComposer
{
    protected $listContactsTypes = [
        'email'         =>  'E-mail',
        'telegram'      =>  'Telegram',
        'job'           =>  'Время работы',
        'chat'   =>  'Онлайн чат',
        'button' => 'Кнопка'
    ];

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withListContactsTypes($this->listContactsTypes);
    }
}