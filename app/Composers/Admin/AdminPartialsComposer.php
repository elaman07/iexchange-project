<?php
namespace App\Composers\Admin;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;

class AdminPartialsComposer
{
    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $error_model = [ ];
        if(!empty(Cache::get('c-parser-exchange')))
        {
            $error_model['parser-exchange'] = \Cache::get('c-parser-exchange');
        }

        $view->with('withUserInfo', auth()->user());
        $view->with('iexErrorModel', $error_model);
    }
}
