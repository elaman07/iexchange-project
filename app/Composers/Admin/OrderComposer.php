<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 25.03.2019
 * Time: 6:15
 */

namespace App\Composers\Admin;

use App\Models\Task;
use App\Models\TaskInfo;
use App\Models\TaskStatus;
use Illuminate\Contracts\View\View;
use Spatie\Permission\Models\Role;

class OrderComposer
{

    /**
     * Статусы заявок
     *
     * @var array
     */
    protected $filteredStatuses = [];

    /**
     * Сумма платежа через мерчант
     *
     * @var array
     */
    protected $filterPaymentMerchants = [
        0   =>  'Точная сумма',
        1   =>  'Переплата',
        2   =>  'Недоплата'
    ];

    /**
     * Список всех стран
     *
     * @var array
    */
    protected $filteredCountries = [];

    /**
     * Список менеджеров
     *
     * @var array
    */
    protected $filteredManagers = [];

    /**
     * Список доступных девайсов
     *
     * @var array
    */
    protected $filteredDevices = [
        'desktop' => 'Основная версия',
        'mobile' => 'Мобильная версия',
        'tablet' => 'Планшетная версия'
    ];

    /**
     * Список доступных типов для сортировки
     *
     * @var array
     */
    protected $allTypesSorting = [
        ''      => '-- Не выбрано --',
        'asc'   =>  'По возрастанию',
        'desc'  =>  'По убыванию'
    ];

    /**
     * Конструктор
     *
     * @return void
     */
    public function __construct()
    {
        $this->filteredStatuses = TaskStatus::orderBy('id')->pluck('name', 'id');
        $this->filteredCountries = TaskInfo::groupBy('country')->pluck('country', 'country');
        $this->filteredManagers = Task::where('id_manager','>',0)->groupBy('id_manager')
            ->get()->map(function($item) {
                return ['id' => $item->id_manager, 'value' => $item->manager->name];
            })->pluck('value', 'id');
    }

    /**
     * Привязать данные к представлению.
     *
     * @param \Illuminate\Contracts\View\View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->withPaymentMerchantStatusses($this->filterPaymentMerchants);
        $view->withAllOrderStatuses($this->filteredStatuses);
        $view->withAllOrderCountries($this->filteredCountries);
        $view->withAllOrderManagers($this->filteredManagers);
        $view->withAllDevices($this->filteredDevices);
        $view->withAllTypesSorting($this->allTypesSorting);
    }
}
