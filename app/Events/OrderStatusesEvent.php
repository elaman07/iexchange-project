<?php

namespace App\Events;

use App\Models\Task;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderStatusesEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Детали заявки
     *
     * @var Task
    */
    protected $order;

    /**
     * Create a new event instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
    }

    public function broadcastWith()
    {

        $client_order_id = (iEXSetting('client_id_type_for_order') == 1) ? $this->order->public_id : $this->order->id;

        $options = [];
        $attributes = [];
        if($this->order->status == 3) {
            $options = [
                'message' => sprintf('Заявка №%s принят на обработку', $client_order_id),
                'className' => 'handler-notification-bg',
                'type'  =>  'wait'
            ];
        }elseif($this->order->status == 4) {

            if((int)iEXSetting('iex_modal_success_order') == 1)
            {
                // Название платежной системы (Для мультиязычности)
                $inPaymentName = $this->order->direction_exchange->currency1->payment->name;
                $outPaymentName = $this->order->direction_exchange->currency2->payment->name;

                if($this->order->direction_exchange->currency1->visible_code_currency == 0) {
                    $in_format_name = sprintf("%s %s", $inPaymentName, $this->order->direction_exchange->currency1->code_currency->name);
                }else {
                    $in_format_name = sprintf("%s", $inPaymentName);
                }
                if($this->order->direction_exchange->currency2->visible_code_currency == 0) {
                    $out_format_name = sprintf("%s %s", $outPaymentName, $this->order->direction_exchange->currency2->code_currency->name);
                }else {
                    $out_format_name = sprintf("%s", $outPaymentName);
                }



                $in_icon_url = '/storage/payment_systems/'.$this->order->direction_exchange->currency1->payment->logo;
                if(!empty(iEXSetting('storage_disk_payment_system')) and $this->order->direction_exchange->currency1->payment->is_local_image == 1) {
                    $in_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$this->order->direction_exchange->currency1->payment->logo, false);
                }

                $out_icon_url = '/storage/payment_systems/'.$this->order->direction_exchange->currency2->payment->logo;
                if(!empty(iEXSetting('storage_disk_payment_system')) and $this->order->direction_exchange->currency2->payment->is_local_image == 1) {
                    $out_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$this->order->direction_exchange->currency2->payment->logo, false);
                }


                $attributes = [
                    'attributes'    =>  [
                        'income_amount' => [
                            'amount'    =>  (float)$this->order->give_price,
                            'currency'  =>  $this->order->direction_exchange->currency1->code_currency->name
                        ],

                        'outcome_amount' => [
                            'amount'        =>  (float)$this->order->receiving_price,
                            'currency'      =>  $this->order->direction_exchange->currency2->code_currency->name
                        ],
                    ],
                    'relationships' => [
                        'income_payment_system' => [
                            'name' => $in_format_name,
                            'color' =>  $this->order->direction_exchange->currency1->border_color,
                            'image'         =>  $in_icon_url
                        ],

                        'outcome_payment_system' => [
                            'name' => $out_format_name,
                            'color' =>  $this->order->direction_exchange->currency2->border_color,
                            'image'         =>  $out_icon_url
                        ],
                    ]
                ];
            }

            $options = [
                'order_id' => iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id,
                'message' => sprintf('Заявка №%s успешно выполнено', $client_order_id),
                'className' => 'successful-notification-bg',
                'type'  =>  'success',
                'is_modal' => (int)iEXSetting('iex_modal_success_order'),
                'is_snackbar' => (int)iEXSetting('iex_snackbar_success_order')
            ];

            if((int)iEXSetting('iex_modal_success_order') == 1)
            {
                if(isset($this->order->wallet_history) and isset($this->order->direction_exchange->currency2->payment->explorer))
                {
                    $options['blockchain'] = sprintf('%s%s',
                        $this->order->direction_exchange->currency2->payment->explorer->link,
                        $this->order->wallet_history->txid
                    );

                    if(!empty($this->order->direction_exchange->currency2->payment->explorer->text)) {
                        $options['blockchain_text'] = $this->order->direction_exchange->currency2->payment->explorer->text;
                    }
                }

                $options['title'] = 'Обмен №'.$client_order_id.' завершен';
                $options['text'] =  iEXSetting('s_order_notify_text');
                $options['attributes'] = $attributes;
            }

        }elseif($this->order->status == 5) {
            $options = [
                'message' => sprintf('Заявка №%s удалена', $client_order_id),
                'className' => 'failed-notification-bg',
                'type'  =>  'failed'
            ];
        }elseif($this->order->status == 8) {
            $options = [
                'message' => sprintf('Заявка №%s заморожено', $client_order_id),
                'className' => 'defer-notification-bg',
                'type'  =>  'defer'
            ];
        }

        return $options;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('order.user.'.$this->order->id_user);
    }

    public function broadcastAs()
    {
        return 'order.statusses';
    }
}
