<?php

namespace App\Events;

use App\Models\LiveNotification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LiveNotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Детали уведомлений
     *
     * @var LiveNotification
     */
    protected $notification;

    /**
     * Create a new event instance.
     *
     * @param LiveNotification $liveNotification
     */
    public function __construct(LiveNotification $liveNotification)
    {
        $this->notification = $liveNotification;
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->notification->text,
            'className' => $this->notification->color,
            'timeout'   =>  $this->notification->timeout * 60 * 1000
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('live.notification');
    }

    public function broadcastAs()
    {
        return 'live.notification.cast';
    }
}
