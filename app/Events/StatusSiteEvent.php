<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StatusSiteEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Текст письма
     *
     * @var string
    */
    protected $message;

    /**
     * Тип письма
     *
     * @var string
     */
    protected $type;

    /**
     * Create a new event instance.
     *
     * @param string $message
     * @param int $type
     */
    public function __construct(string $message, int $type)
    {
        $this->message = $message;
        $this->type = $type;
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->message,
            'type' => $this->type,
            'status'   => isJobOffline()
        ];
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('status.site');
    }

    public function broadcastAs()
    {
        return 'status.site.cast';
    }
}
