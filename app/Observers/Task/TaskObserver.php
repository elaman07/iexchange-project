<?php
namespace App\Observers\Task;

use App\Jobs\TelegramOrderPostponedJob;
use App\Models\Task;
use App\Models\TaskStatusLog;
use Carbon\Carbon;

/**
 * Class TaskObserver.
 */
class TaskObserver
{
    /**
     * Слушаем созданное пользователем событие.
     *
     * @param  \App\Models\Task  $task
     */
    public function created(Task $task) : void
    {
        // Время создания заявки (для автообмена)
        $task->next_checkout_at = Carbon::now()->addMinutes(1)->toDateTimeString();
        $task->save();

        // Записываем лог статусов
        if(iEXSetting('is_enabled_log_status') == 1)
        {
            TaskStatusLog::create([
                'id_task'       =>  $task->id,
                'user_id'       =>  $task->id_user,
                'old_status'    =>  0,
                'new_status'    =>  $task->status,
                'in_price'      =>  $task->give_price,
                'out_price'     =>  $task->receiving_price,
                'place_change'  =>  0,
                'course_display'    => $task->course_display
            ]);
        }
    }

    /**
     * Слушаем обновленное пользователем событие.
     *
     * @param  \App\Models\Task  $task
     */
    public function updated(Task $task) : void
    {
        // В случае, если заявка отложено
        if($task->isDirty('status') and $task->status == 8 and (int)iEXSetting('is_notify_order_telegram_postponed') == 1) {
            $delay = now()->addSeconds(30);
            dispatch(new TelegramOrderPostponedJob($task))->delay($delay)->onQueue('low');
        }
    }
}
