<?php
namespace App\Observers\User;

use App\Models\User;


/**
 * Class UserObserver.
 */
class UserObserver
{
    /**
     * Слушаем созданное пользователем событие.
     *
     * @param  \App\Models\User  $user
     */
    public function created(User $user) : void
    {
        $this->logPasswordHistory($user);
    }

    /**
     * Слушаем обновленное пользователем событие.
     *
     * @param  \App\Models\User  $user
     */
    public function updated(User $user) : void
    {
        // Регистрировать историю паролей только при обновлении, если пароль действительно изменился
        if ($user->isDirty('password')) {
            $this->logPasswordHistory($user);
        }
    }

    /**
     * @param User $user
     */
    private function logPasswordHistory(User $user) : void
    {
        if(iEXSetting('is_user_password_history'))
        $user->passwordHistories()->create([
            'password' => $user->password, // Пароль уже хеширован и сохранен, поэтому просто берите из модели
        ]);
    }
}
