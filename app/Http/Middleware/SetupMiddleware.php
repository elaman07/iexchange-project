<?php

namespace App\Http\Middleware;

use Closure;

class SetupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(config('setup.enabled') == true)
            return redirect('/setup');

        return $next($request);
    }
}
