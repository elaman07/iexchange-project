<?php
namespace App\Http\Middleware;

use App\Models\Settings;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\IpUtils;

class WhitelistMiddleware
{
    public function handle(Request $request, Closure $next, $group)
    {
        $admin_allowed_ip = preg_replace('~[\r\n]+~', '', iEXSetting('admin_allowed_ip'));
        $array = explode(',', $admin_allowed_ip);
        $filter_ip = IpUtils::checkIp($request->ip(), $array);


        if($filter_ip == 1 or (int)iEXSetting('is_firewall_enabled') == 0) {
            return $next($request);
        }

        // Проверяем права (Если разработчик, то пускаем в любые лимиты)
        $authGuard = auth()->user();
        if($authGuard and $authGuard->hasRole(config('app.developer_role'))) {
            return $next($request);
        }

        if (is_array($array)) {
            foreach ($array as $ip) {
                if (Str::is($ip, $request->ip())) {
                    return $next($request);
                }
            }
        }

        return abort(403);
    }
}
