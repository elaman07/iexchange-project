<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OptimizeMiddleware
{
    /**
     * Исключение некоторых путей
     *
     * @var array
     */
    protected $except = [
        'robots.txt',
        'robots'
    ];

    /**
     * Handle an incoming request.
     *
     * @param
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(in_array(\Arr::last(explode('/', $request->path())), $this->except)) {
            return $next($request);
        }

        // Отключаем сжатие для админ-панели
        if($request->is(mb_substr(admin_path(), 1).'/*') and iEXSetting('disabled_admin_allow_gzip')) {
            return $next($request);
        }


        // Если отключено, дальше не выполняем
        if(!config('cache.allow_gzip')) return $next($request);

        $response = $next($request);
        $buffer = $response->getContent();

        $replace = [
            '/<!--[^\[](.*?)[^\]]-->/s' => '',
            "/<\?php/"                  => '<?php ',
            "/\n([\S])/"                => '$1',
            "/\n/"                      => ' ',
            "/\t/"                      => '',
            "/ +/"                      => ' '
        ];

        $buffer = preg_replace(array_keys($replace), array_values($replace), $buffer);
        $response->setContent($buffer);
        //ini_set('zlib.output_compression', 'On'); // If you like to enable GZip, too!
        return $response;
    }
}