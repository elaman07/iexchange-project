<?php


namespace App\Http\Middleware;

use Closure;

class DemoRouteMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // В случае неудачи, переадресовываем
        if(config('admin.is_demo_mode') == true) {
            return abort(403,'Это действие отключено в Demo версии');
        }
        return $next($request);
    }
}
