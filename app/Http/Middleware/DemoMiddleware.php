<?php
namespace App\Http\Middleware;

use Closure;

class DemoMiddleware
{
    protected $methods = [
        'POST',
        'PUT',
        'DELETE'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // В случае неудачи, переадресовываем
        if(!in_array($request->getMethod(), $this->methods))
            return $next($request);
        return abort(403,'Это действие отключено в Demo версии');
    }
}
