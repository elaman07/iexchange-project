<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Cog\Contracts\Ban\Bannable as BannableContract;
use Illuminate\Contracts\Auth\Guard;

class ForbidBannedUser
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && $user instanceof BannableContract && $user->isBanned()) {
            return redirect('/banned');
        }

        return $next($request);
    }
}
