<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminRoles
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @param null $permission
     * @param null $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission = null, $guard = null): mixed
    {
        $authGuard = app('auth')->guard($guard);
        if ($authGuard->guest()) {
            return redirect()->to('/404');
        }

        // В случае неудачи, переадресовываем
        if ($authGuard->user()->can('allow_admin')) {
            return $next($request);
        }

        return redirect()->to('/404');
    }
}
