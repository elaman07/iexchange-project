<?php
namespace App\Http\Middleware;


use App\Models\Banned;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Spatie\Permission\Guard;

class BlacklistMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $banned = Banned::where('filter_name', $request->ip())
            ->where('expired_at', '>=', Carbon::now());

        if($banned->exists()) {
            if(auth()->check()) {
                return redirect('/banned/ip');
            }
            return abort(403, $banned->first()->description);
        };

        return $next($request);
    }
}