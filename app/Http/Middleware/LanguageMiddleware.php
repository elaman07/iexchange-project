<?php
namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Closure;

class LanguageMiddleware
{
    /**
     * Ручка входящий запрос.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(config('setup.enabled') == 1)
            return $next($request);

        // Отключить возможность автоматического изменения языка
        if(iEXSetting('is_language_deactivation') == 1) {
            return $next($request);
        }

        if($request->has('locale')) {
            $locale = $request->get('locale');
        } else {

            // Отключение автоматического определения языка
            if(!iEXSetting('is_language_detection')) {
                $locale = $request->session()->has('language') ? $request->session()->get('language') : 'ru';
            } else {
                $locale = !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',') : '';
                $locale = substr($locale , 0,2);
            }
        }

        //Список доступных языков
        $language = collect(config('app.all_locale'))->keys()->toArray();

        //Если пользователь авторизован, то проверяем у него, локализацию.
        //Если она установлена то записываем ее в сессию а иначе продолжаем по стандарту
        if((int)iEXSetting('is_language_selected_user') == 1 and Auth::check()) {
            $user_locale = User::select('language')->where('id', Auth::id())->first();
            if(isset($user_locale->language) and strlen($user_locale->language) > 1) {
                $request->session()->put('language', $user_locale->language);
            }
        }

        if(!$request->session()->has('language'))
        {
            if(in_array($locale, $language) == true) {
                $request->session()->put('language', $locale);
            }else {
                $request->session()->put('language', config('app.locale'));
            }
        } else {
            if($request->has('locale') and in_array($locale, $language) == true) {
                $request->session()->put('language', $locale);
            }
        }

        if(!in_array(config('admin.route_path'), explode('/',request()->path()))) {
            app()->setLocale($request->session()->get('language'));
        }
        return $next($request);
    }
}
