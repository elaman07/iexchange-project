<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 20.10.2017
 * Time: 23:55
 */

namespace App\Http\Middleware;


use Mockery\Matcher\Closure;

class AffiliateTrackingMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($request->has('affiliate_id')) {
            $response->withCookie(cookie('affiliate_id', $request->get('affiliate_id'), 30));
        }

        return $response;
    }

}