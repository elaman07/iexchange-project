<?php
namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class AdminPhoneMiddleware
{
    /**
     * Ключ безопасности
     *
     * @var string
     */
    protected $key = 'iex-phone-hash';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Если отключено, пропускаем
        if(!iEXSetting('enable_admin_sms_notification'))
            return $next($request);

        // Получаем ID пользователя в ключем для того чтобы проверить данные
        // В текущих сессиях
        $key = $this->key.$request->user()->id;

        // Если секретный ключ ранее не был введен, дальше авторизации не пускаем
        if(!\Session::has($key))
            return redirect()->to('/2faPhone');

        // Сбрасываем ключ, если пользователь не заходил больше 5 часов
        if($request->user()->last_activity_at <= Carbon::now()->subHours(5)) {
            \Session::forget($key);
            return redirect()->to('/2faPhone');
        }

        // Если проверки прошли успешно, авторизуемся
        if(\Session::get($key) == $request->user()->phone_hash) {
            return $next($request);
        }

        return redirect()->to('/');
    }
}
