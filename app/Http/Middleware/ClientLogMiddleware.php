<?php
namespace App\Http\Middleware;


use Closure;

class ClientLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Записываем IP адреса клиентов которые находили на сайт
        if(iEXSetting('is_write_client_log') and $request->path() == '/')
        {
            \Log::info('Информация о клиенте: ', [
                'ip' => $request->ip(),
                'user_agent' => $request->userAgent()
            ]);
        }


        return $next($request);
    }
}
