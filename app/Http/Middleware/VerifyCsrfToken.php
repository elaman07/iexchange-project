<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    ];

    /***
     * Конструктор
     *
     * @param Application $app
     * @param Encrypter $encrypter
     */
    public function __construct(Application $app, Encrypter $encrypter)
    {
        parent::__construct($app, $encrypter);



        $this->except = [
            'iexbot/*',
            'broadcasting/*',
            'payment_status/*',
            'file-upload/*',
            'callbacks/*',
            'ticketsproc/*',
            'admin/*',
            'oauth/*',
            'api/*',
            'private/*',
            'api/v1/*',
            'api/v3/*',
            'api/2.0/*',
            'api/windows/*',
            'events/broadcasting/*',
            config('admin.directory').'/*'
        ];
    }
}
