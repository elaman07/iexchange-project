<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 03.04.2019
 * Time: 21:44
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;

class MonitoringClientMiddleware
{
    /**
     * Обработчик входных запросов
     *
     * @param Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $auth_user = $request->user();
        // Сбросить авторизацию, случае изменения IP Адреса
        if(iEXSetting('ip_change_control') == 1 and $request->getMethod() == 'GET' and $auth_user->ip_changed)
        {
            auth()->logout();
            return redirect()->to('/');
        }

        return $next($request);
    }
}
