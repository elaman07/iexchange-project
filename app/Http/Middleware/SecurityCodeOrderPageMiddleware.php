<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SecurityCodeOrderPageMiddleware
{
    /**
     * Ключ безопасности
     *
     * @var string
     */
    protected string $key = 'iex-security-code-order-page';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty(config('security-codes.order-page'))) {
            return $next($request);
        }

        // Получаем ID пользователя в ключем для того чтобы проверить данные
        // В текущих сессиях
        $key = $this->key.$request->user()->id;

        // Если секретный ключ ранее не был введен, дальше авторизации не пускаем
        if(!\Session::has($key))
            return redirect()->to('/confirmAuthOrderPage');

        // Сбрасываем ключ, если пользователь не заходил больше 5 часов
        if($request->user()->last_activity_at <= Carbon::now()->subDays(1)) {
            \Session::forget($key);
            return redirect()->to('/confirmAuthOrderPage');
        }

        // Если проверки прошли успешно, авторизуемся
        if(\Session::get($key) == $request->user()->security_order_page_code) {
            return $next($request);
        }

        return redirect()->to('/');
    }
}
