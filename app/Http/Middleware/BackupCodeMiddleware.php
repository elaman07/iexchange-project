<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 23.10.2019
 * Time: 22:00
 */

namespace App\Http\Middleware;


use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class BackupCodeMiddleware
{
    /**
     * Ключ безопасности
     *
     * @var string
    */
    protected $key = 'iex-backup-secret';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Получаем ID пользователя в ключем для того чтобы проверить данные
        // В текущих сессиях
        $key = $this->key.$request->user()->id;

        // Если секретный ключ ранее не был введен, дальше авторизации не пускаем
        if(!\Session::has($key))
            return redirect()->to('/2faBackupCode');

        // Сбрасываем ключ, если пользователь не заходил больше 5 часов
        if($request->user()->last_activity_at <= Carbon::now()->subHours(5)) {
            \Session::forget($key);
            return redirect()->to('/2faBackupCode');
        }

        // Если проверки прошли успешно, авторизуемся
        if(\Session::get($key) == $request->user()->backup_code_secret) {
            return $next($request);
        }

        return redirect()->to('/');
    }
}