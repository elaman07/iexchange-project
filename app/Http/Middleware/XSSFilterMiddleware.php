<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class XSSFilterMiddleware extends TransformsRequest
{
    /**
     * Атрибуты, которые не следует редактировать.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Иницилизация модуля Security XSS
     *
     * @var \GrahamCampbell\SecurityCore\Security
     */
    protected $security;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->security = app('security');
    }

    /**
     * Transform the given value.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return mixed
     */
    protected function transform($key, $value)
    {
        if ($this->shouldIgnore($key, $value)) {
            return $value;
        }
        return $this->security->clean($value);
    }

    /**
     * determine if should ignore the field.
     *
     * @param $key
     * @param $value
     * @return bool
     */
    protected function shouldIgnore($key, $value): bool
    {
        return ! is_string($value) || in_array($key, $this->except, true);
    }
}
