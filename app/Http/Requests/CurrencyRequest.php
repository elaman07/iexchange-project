<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Если настройки, пропускаем
        $extends = ['currency_checkbox', 'action', 'actions'];
        foreach ($extends as $extend) {
            if($this->has($extend))
                return [];
        }

        $rules = [
            'id_filter_currency' => 'required',
            'convert_by' =>  'required|numeric',
            'number_format' => 'required|numeric',
            'account_number_field' => 'required|string',
            'day_limit_give' => 'required|numeric',
            'day_limit_receive' => 'required|numeric',
            'week_limit_in' => 'required|numeric',
            'week_limit_out' => 'required|numeric',
            'month_limit_in' => 'required|numeric',
            'month_limit_out' => 'required|numeric',
            'min_char' => 'required|numeric',
            'max_char' => 'required|numeric',
            'status' => 'required|numeric',
            'text_color' => 'required|string',
            'border_color' => 'required|string',
            'reserve'       =>  'required|numeric'
        ];

        // Если необходимо добавить новую платежную систему
        if($this->get('id_payment') == 0) {
            $rules['payment_system_name'] = 'required|max:150';
        } else {
            $rules['id_payment'] = 'required';
        }

        // Если необходимо добавить новы код валюты
        if($this->get('id_code_currency') == 0) {
            $rules['code_currency_name'] = 'required|max:20';
            $rules['code_currency_sign'] = 'required|max:20';
            $rules['code_currency_number_format'] = 'required|numeric';
        } else {
            $rules['id_code_currency'] = 'required';
        }

        return $rules;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        flash($validator->messages()->first(), ['alert alert-danger']);
        return parent::failedValidation($validator);
    }
}
