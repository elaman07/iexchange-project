<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 28.06.2018
 * Time: 21:30
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

class BaseController extends Controller
{
    /***
     * Глобальный массив для сбора результатов
     *
     * @return array
     */
    protected $result = [];

    /**
     * @param null $key
     * @return array
     */
    public function getItem($key = null)
    {
        if($key == null) {
            return $this->result;
        }else {
            return Arr::get($this->result, $key);
        }
    }


    public function setItem($key, $value)
    {
        Arr::set($this->result, $key, $value);
        return $this;
    }
}