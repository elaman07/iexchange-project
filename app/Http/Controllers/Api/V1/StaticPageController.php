<?php
namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ReferralSystem\Models\ReferralProgram;
use App\Http\Controllers\AbstractController;
use App\Models\CollaborationPrModel;
use App\Models\Contact;
use App\Models\FaqCategory;
use App\Models\LinksReview;
use App\Models\LinksReviewGroup;
use App\Models\RewardProgram;
use App\Models\SettingsReferrals;
use App\Models\SettingsReward;
use App\Models\SocialReview;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class StaticPageController extends AbstractController
{
    public function static_page($type)
    {
        if($type == 'referral_and_cashback') {
            return $this->referralAndCashBack();
        } elseif($type == 'contacts')
        {
            $contracts_items = Contact::orderBy('sorting')->get()->map(function($item)
            {
                return [
                    'name' => $item->name,
                    'type' => $item->type,
                    'value' => $item->value,
                    'url' => $item->url,
                    'block_size' => $item->block_size,
                    'sorting' => $item->sorting,
                    'is_home' => $item->is_home,
                    'is_button' => $item->is_button,
                    'icon_url'  => !empty($item->icon) ? '/storage/contact/'.$item->icon : null
                ];
            })->toArray();
            $links_reviews = LinksReviewGroup::with(['links_review_large', 'links_review_url'])->select('id','name')->orderBy('sorting')
                ->get()->toArray();

            $item = [];
            $item['description'] = [
                'contact' => iEXContentLanguage('description_contact'),
                'pr'    =>  iEXContentLanguage('description_pr'),
                'review' =>  iEXContentLanguage('description_review')
            ];
            $item['contacts'] = $contracts_items;
            $item['pr'] = CollaborationPrModel::where('status', '=', 1)->orderBy('sorting')->get()->toArray();
            $item['links'] = $links_reviews;

            return $item;
        }
    }


    private function referralAndCashBack(): \Illuminate\Http\JsonResponse
    {
        $arrays = [
            'referral'  =>  [
                'list' => [],
                'description' => [],
                'description_account' => [],
            ],
            'cashback'  =>  [
                'list' => [],
                'description' => [],
                'description_account' => [],
            ],

            'monitoring' => [
                'description' => iEXContentLanguage('monitoring_text')
            ]
        ];

        $responseProgram = \cache()->remember('static-page-referral', Carbon::now()->addHours(), function()
        {
            $referrals = ReferralProgram::all();
            $cashbacks = RewardProgram::all();
            return [
                'referral' => $referrals,
                'cashback' => $cashbacks
            ];
        });

        foreach ($responseProgram['referral'] as $item) {
            $arrays['referral']['list'][] = [
                'label' => $item->title,
                'name' => $item->name,
                'is_reg' => (bool)$item->is_reg,
                'value' => $item->percent,
                'style_width' => $item->style_width
            ];
        }

        foreach ($responseProgram['cashback'] as $item) {
            $arrays['cashback']['list'][] = [
                'label' => $item->title,
                'name' => $item->name,
                'amount' => $item->amount,
                'is_reg' => (bool)$item->is_reg,
                'value' => $item->percent
            ];
        }

        $arrays['referral']['description'] = iEXContentLanguage('referral_system_text');
        $arrays['referral']['description_account'] = iEXContentLanguage('referral_system_text_footer');
        $arrays['cashback']['description'] = iEXContentLanguage('cashback_text');
        $arrays['cashback']['description_account'] = iEXContentLanguage('cashback_footer');

        return response()->json($arrays);
    }
}
