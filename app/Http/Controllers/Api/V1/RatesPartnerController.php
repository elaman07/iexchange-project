<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\ParserExchange;
use Illuminate\Support\Str;

class RatesPartnerController extends Controller
{
    /**
     * Данные курсов
     *
     * @param null $from
     * @param null $to
     * @return mixed
     */
    public function rates($from = null, $to = null)
    {
        if((int)iEXSetting('is_crypto_partner_parser_api') == 0) {
            return [];
        }

        $parser = ParserExchange::where('status', 1)->cursor();


        $glob = $parser->reject(function($item) {
            return !isset($item->group_parse_exchange);
        })->map(function($item)
        {
            return [
                'name' => Str::upper($item->name),
                'group' => [
                    'id' => $item->id_group,
                    'name' => $item->group_parse_exchange->name
                ],
                'value' => $item->summa_default,
                'number_format' => $item->number_format,
                'last_updated_at' => $item->updated_at->format('c')
            ];

        });
        return $glob->toArray();
    }
}
