<?php

namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ReferralSystem\Models\ReferralProgram;
use App\Http\Controllers\Controller;
use App\Models\RewardProgram;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function index(Request $request)
    {
        $arrays = [
            'referral'  =>  [
                'list' => [],
                'description' => [],
                'description_account' => [],
            ],
            'cashback'  =>  [
                'list' => [],
                'description' => [],
                'description_account' => [],
            ],

            'monitoring' => [
                'description' => iEXContentLanguage('monitoring_text')
            ],

            'files' => []
        ];

        $responseProgram = \cache()->remember('static-page-referral', Carbon::now()->addHours(), function()
        {
            $referrals = ReferralProgram::all();
            $cashbacks = RewardProgram::all();
            return [
                'referral' => $referrals,
                'cashback' => $cashbacks
            ];
        });

        foreach ($responseProgram['referral'] as $item) {
            $arrays['referral']['list'][] = [
                'label' => $item->title,
                'name' => $item->name,
                'is_reg' => (bool)$item->is_reg,
                'value' => $item->percent,
                'style_width' => $item->style_width
            ];
        }

        foreach ($responseProgram['cashback'] as $item) {
            $arrays['cashback']['list'][] = [
                'label' => $item->title,
                'name' => $item->name,
                'amount' => $item->amount,
                'is_reg' => (bool)$item->is_reg,
                'value' => $item->percent,
                'style_width' => $item->style_width
            ];
        }

        $arrays['referral']['description'] = iEXContentLanguage('referral_system_text');
        $arrays['referral']['description_account'] = iEXContentLanguage('referral_system_text_footer');
        $arrays['cashback']['description'] = iEXContentLanguage('cashback_text');
        $arrays['cashback']['description_account'] = iEXContentLanguage('cashback_footer');
        $arrays['files'] = $this->rates();
        return response()->json($arrays);
    }

    public function rates()
    {
        return \Cache::remember('courses_config', \Illuminate\Support\Carbon::now()->addHour(),  function() {
            $array = [];
            if(iEXSetting('grates_format_xml') == 1) {
                $array[] = [
                    'name' => iEXSetting('grates_filename') . '.xml',
                    'format' => 'XML'
                ];
            }

            if(iEXSetting('grates_format_txt') == 1) {
                $array[] = [
                    'name' => iEXSetting('grates_filename') . '.txt',
                    'format' => 'TXT'
                ];
            }

            if(iEXSetting('grates_format_json') == 1) {
                $array[] = [
                    'name' => iEXSetting('grates_filename') . '.json',
                    'format' => 'JSON'
                ];
            }

            return $array;
        });
    }
}
