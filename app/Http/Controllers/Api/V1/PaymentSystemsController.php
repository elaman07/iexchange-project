<?php
namespace App\Http\Controllers\Api\V1;


use App\Http\Resources\Angular\UserWalletStoryResource;
use App\Http\Resources\Rates\PaymentSystemsResource;
use App\Models\Currency;
use App\Models\UserWalletStories;
use Illuminate\Http\Request;

class PaymentSystemsController
{
    /**
     * Список платежных систем
    */
    public function index()
    {
        $currencies = Currency::with([
            'currency_in_fields', 'payment', 'code_currency', 'commands', 'currency_out_fields'
        ])->where('status', '=', 0)->get();

        return new PaymentSystemsResource($currencies);
    }

    /**
     * Список платежных систем
     */
    public function user_currencies()
    {
        $currencies = Currency::with([
            'currency_in_fields', 'payment', 'code_currency', 'commands', 'currency_out_fields'
        ])->where('status', '=', 0)->get()->filter(function($item) {
            return !in_array($item->id, explode(',',iEXSetting('ids_currencies_account_my_wallets')));
        })->all();


        return new PaymentSystemsResource($currencies);
    }

    /**
     * Информация по счетам
     *
     * @param Request $request
     * @return UserWalletStoryResource
     */
    public function user_wallets(Request $request)
    {
        $info = UserWalletStories::where([
            'id_user' => $request->user()->id
        ])->get();


        return new UserWalletStoryResource($info);

    }

    /**
     * Информация по счетам
     *
     * @param Request $request
     * @return UserWalletStoryResource
     */
    public function delete_wallet(int $id)
    {
        UserWalletStories::where([
            ['id_user', auth()->id()],
            ['id', $id]
        ])->delete();

        $info = UserWalletStories::where([
            'id_user' => auth()->id()
        ])->get();

        return new UserWalletStoryResource($info);

    }
}
