<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.02.2020
 * Time: 10:51
 */

namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\Pages\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class PageController extends Controller
{
    /**
     * Получить детали страницы
     *
     * @param string $page_slug
     * @return \Illuminate\Http\JsonResponse| string[]
     */
    public function getPageId(string $page_slug)
    {
        // Если страница не найдена
        if(!Pages::pageExists($page_slug)) {
            return ['error' => 'no data'];
        }

        $array_search = ['{sitename}', '{sitename_desc}', '{url}'];
        $array_value = [iEXContentLanguage('sitename'), iEXContentLanguage('sitename_desc'), config('app.url')];

        $pageData = Pages::getPage($page_slug);
        // Найти и заменить
        $content = str_replace($array_search, $array_value, $pageData->page_content);

        return response()->json([
            'page_title' => $pageData->page_title,
            'page_content' => $content
        ]);
    }
}
