<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Requisites;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostRequisitesController extends Controller
{
    /**
     * Данные курсов
     *
     * @param null $from
     * @param null $to
     * @return mixed
     */
    public function index()
    {
        if((int)iEXSetting('is_iexrequisites_enabled') == 0) {
            return [];
        }
        $getRequisites = Requisites::activeWallet()->cursor();

        foreach ($getRequisites as $requisite) {
            // Если добавлены несколько счетов, разделяем
            $receive_money = collect(explode(',', $requisite->account_number))->random();
        }

        return $getRequisites->reject(function($item) {
            return !isset($item->currency);
        })->map(function($item)
        {
            return [
                'code' =>  Str::upper($item->currency->designation_xml),
                'value' => $item->account_number
            ];

        })->toArray();
    }
}
