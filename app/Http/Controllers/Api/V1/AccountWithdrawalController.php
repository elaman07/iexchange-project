<?php
namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\AbstractController;
use App\Http\Resources\V2\Account\Partners\WithdrawalHistoriesResponse;

use App\Jobs\{
    EmailPayoutsJob,
    TelegramPayoutsJob,
    VerifiedPayoutsJob
};

use App\Models\{Currency, User, WithdrawalRequest, WithdrawalWallets};
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JetBrains\PhpStorm\ArrayShape;

class AccountWithdrawalController extends AbstractController
{
    /**
     * Информация о выводе средств
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function withdrawalInfo(Request $request): JsonResponse
    {
        // Информация о пользователе
        $user = $request->user();

        if($user->is_unique_user == 1) {
            $currency = Currency::where('is_payment_unique', '=', 1)->where('status', 0)->get();
        } else {
            $currency = Currency::where('is_payment_default', '=', 1)->where('status', 0)->get();
        }

        $array = [];
        foreach ($currency as $item) {

            // Список
            $autoCompleteWallets = WithdrawalWallets::where('id_currency', $item->id)->where('id_user', \auth()->id())->pluck('wallet');

            $icon_url = '/storage/payment_systems/'.$item->payment->logo;
            if(!empty(iEXSetting('storage_disk_payment_system')) and $item->payment->is_local_image == 1) {
                $icon_url = iex_dynamic_read_view_image('/payment_systems/'.$item->payment->logo, false);
            }

            $array[] = [
                'name' => sprintf("%s %s", $item->payment->name, $item->code_currency->name),
                'currency' => $item->id,
                'autoCompleteWallets' => $autoCompleteWallets,
                'mask' => $item->mask_input,
                'icon_url' => $icon_url,
                'form' => [
                    'placeholder' => $item->first_char,
                    'default_value' => $item->char_default,
                    'min_char' => $item->min_char,
                    'max_char' => $item->max_char
                ],
                'fees' => [
                    'fee_percent' => $item->payout_commission,
                    'fee_currency' => $item->payout_commission_amount
                ]
            ];
        }

        // Список выводов
        $withdrawalRequest = WithdrawalRequest::where([
            ['id_user', $user->id],
        ])->orderBy('id', 'desc');

        $withdrawal_histories = new WithdrawalHistoriesResponse(
            $withdrawalRequest->simplePaginate(10)
        );
        $minimum_bonus_payout = iEXSetting('minimum_bonus_payout');

        return response()->json([
                'payments' => $array,
                'withdrawal_histories' => $withdrawal_histories,
                'user' => [
                    'balance'   => ($user->user_balance->balance + $user->user_balance->balance_reward),
                    'code_sign' => config('crypto.currency_payout_sign'),
                    'position'  => config('crypto.currency_payout_position'),
                    'min_withdrawal' => iex_number_format($minimum_bonus_payout, 0, true),
                    'is_withdrawal' => (bool)($user->user_balance->balance > $minimum_bonus_payout || $user->user_balance->balance_reward > $minimum_bonus_payout)
                ]
            ]
        );
    }

    /**
     * История вывода бонусов
     *
     * @param Request $request
     * @return WithdrawalHistoriesResponse
     */
    public function histories(Request $request)
    {
        // Список выводов
        $withdrawalRequest = WithdrawalRequest::where([
            ['id_user', $request->user()->id],
        ])->orderBy('id', 'desc');

        return new WithdrawalHistoriesResponse(
            $withdrawalRequest->simplePaginate(10)
        );
    }

    /**
     * Создаем заявку на вывод средств
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $array = [];
        $validator = Validator::make($request->all(), [
            'id_currency' => 'required|numeric',
            'score'     =>  'required|min:2'
        ]);

        // Если нет ошибок
        if (!$validator->fails())
        {
            // Информация по валюте
            $currency = Currency::find(intval($request->get('id_currency')));
            // Информация о балансах
            $user_balance = auth()->user()->user_balance;
            // Получаем итоговую сумму выплат
            $final_balance = ($user_balance->balance + $user_balance->balance_reward);
            // Минимальная сумма выплаты
            $minimum_bonus_payout = iEXSetting('minimum_bonus_payout');


            $client_wallet = security_xss($request->get('score'));
            $user_id = \auth()->id();

            $withdrawalRequest = WithdrawalRequest::where([
                ['id_user', $user_id],
                ['status', 0]
            ])->count();

            $array['multi_message'] = [];

            // Проверяем, сумму выплаты
            if($final_balance < $minimum_bonus_payout)
            {
                $array['status'] = 1;
                $array['send'] = 'push';
                $array['multi_message'][] = [
                    'message' => 'Минимальная сумма выплаты '. $minimum_bonus_payout.' '.config('crypto.currency_payout_sign')
                ];
            }

            // Обработчик счета (с валидатором)
            if($currency->is_valid_account and !wallet_validator($currency->is_valid_account, $client_wallet))
            {
                $array['status'] = 1;
                $array['multi_message'][] = [
                    'message' => 'Указан неправильный номер кошелька ' . $currency->payment->name
                ];
            }

            //Обработчик счета (без валидатора)
            if(empty($currency->is_valid_account) and $currency->min_char > 0)
            {
                if (strlen($request->score) < $currency->min_char or strlen($request->score) > $currency->max_char)
                {
                    $array['status'] = 1;
                    $array['multi_message'][] = [
                        'message' => 'Указан неправильный номер кошелька ' . $currency->payment->name
                    ];
                }
            }

            //Удаляем пустые массивы
            $array = array_filter($array, function($element) {
                return !empty($element);
            });

            //Если нет больше никаких ошибок то создаем заявку
            if (empty($array))
            {
                $array['status'] = 0;
                // Выводим нулевые балансы
                $array['is_withdrawal'] = false;

                $user_balance = auth()->user()->user_balance;
                // Получаем партнерский расчет
                $partner_balance = $this->convertTo($currency, $user_balance->balance);
                // Cashback баланс
                $cashback_balance = $this->convertTo($currency, $user_balance->balance_reward);

                $withdrawal_request = WithdrawalRequest::create([
                    'id_user'               => Auth::id(),
                    'id_currency'           => $request->get('id_currency'),
                    'score'                 => $client_wallet,
                    'balance_referral'      => $partner_balance,
                    'balance_reward'        => $cashback_balance,
                    'view_balance_referral' => $partner_balance,
                    'view_balance_reward'   => $cashback_balance,
                    // В случае возврата
                    'base_referral'         =>  (float)$user_balance->balance,
                    'base_reward'           =>  (float)$user_balance->balance_reward,
                    'ip'                    =>  $request->ip(),
                    'tx_id'                 => \Str::uuid()
                ]);

                // Уведомлять о новых вознаграждениях (Telegram)
                if(iEXSetting('telegram_order_withdrawal')) {
                    $delay = now()->addMinutes(1);
                    dispatch(new TelegramPayoutsJob($withdrawal_request))->delay($delay)->onQueue('low');
                }

                // Уведомлять о новых вознаграждения на (E-mail)
                if(iEXSetting('mail_order_withdrawal')) {
                    $delay = now()->addMinutes(2);
                    dispatch(new EmailPayoutsJob($withdrawal_request))->delay($delay)->onQueue('low');
                }

                // Отправить клиенту уведомление для подтверждения вывода средств
                if(iEXSetting('is_verified_payouts_bonus') == 1) {
                    $delay = now()->addSeconds(20);
                    dispatch(new VerifiedPayoutsJob($withdrawal_request))->delay($delay)->onQueue('high');
                }

                //Обновление цен
                \DB::table('user_balance')->where('id_user', $user_id)->update([
                    'balance' => 0,
                    'balance_reward' => 0
                ]);
            }

        }else {
            $array['status'] = 'error';
        }

        return response()->json($array);
    }


    /**
     * Конвертируем базовую суму
     *
     * @param $currency
     * @param $amount
     * @return float
     */
    private function convertTo($currency, $amount): float
    {
        $final_balance = $amount;
        if($currency->payout_commission > 0) {
            $final_balance = ($amount - $amount / 100) * $currency->payout_commission;
        }

        // Код конвертации
        $convert_code = \Str::upper(config('crypto.currency_payout'));
        // Код выбранной валюты
        $selected_code = \Str::upper($currency->code_currency->name);

        // Конвертировать в основную валюту
        $converter = convert_to_currency($convert_code, $selected_code, $amount);
        return (float)iex_number_format($converter - $currency->payout_commission_amount, $currency->number_format);
    }
}
