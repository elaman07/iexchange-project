<?php
namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Http\Controllers\Controller;
use App\Http\Resources\Angular\ReferralIncomeResource;
use App\Http\Resources\Angular\ReferralResource;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;

class ReferralIncomesController extends Controller
{
    /**
     * Список рефераллов
     *
     * @param Request $request
     * @return ReferralResource
     */
    public function items(Request $request)
    {
        // force current page to 5
        Paginator::currentPageResolver(function() use($request) {
            return $request->query('offset') ?? $request->query('page') ?? 0;
        });

        $response = ReferralLog::with(['tasks' => function($q) {
            $q->select('id', 'public_id', 'id_direction_exchange');
        },'tasks.direction_exchange' => function($q) {
            $q->select('id', 'id_currency1', 'id_currency2');
        },'tasks.direction_exchange.currency1' => function($q) {
            $q->select('id', 'id_payment', 'id_code_currency');
        },'tasks.direction_exchange.currency1.payment' => function($q) {
            $q->select('id', 'name');
        },'tasks.direction_exchange.currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        },'tasks.direction_exchange.currency2' => function($q) {
            $q->select('id', 'id_payment','id_code_currency');
        },'tasks.direction_exchange.currency2.payment' => function($q) {
            $q->select('id', 'name');
        },'tasks.direction_exchange.currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'user' => function($q) {
            $q->select('id', 'name');
        }])->where('id_user', $request->user()->id)
            ->where('bonus_number', '>', 0)->orderBy('id_task', 'desc');

        $count = $response->count();

        return (new ReferralResource(
            $response->limit(10)->offset($request->query('offset') * 10 ?? 0)->get())
        )->additional([
            'meta' => [
                'total' => $count,
                'per_page' => 10
            ]
        ]);
    }

    public function user(Request $request) {
        return new ReferralIncomeResource($request->user());
    }

    public function settings(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username' => 'required|string|max:255|unique:referral_links,code|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        ]);

        $user = $request->user();
        if($validate->fails()) {
            $array['status']    = 1;
            $array['message']   = $validate->errors()->first();
        }else {
            $event_value = 'Имя пользователя: <span style="color:red">'.$user->username.'</span> => <span style="color:green">'.$request->username.'</span><br />';
            event_users_history('Изменение', $event_value, $user->id);

            $user->update([
                'username' => security_xss($request->get('username'))
            ]);

            ReferralLink::where('user_id', $user->id)->update([
                'code' => security_xss($request->get('username'))
            ]);

            $array['status']    = 0;
            $array['message']   = 'Данные обновлены';
        }


        return $array;
    }
}
