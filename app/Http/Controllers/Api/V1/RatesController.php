<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 08.04.2020
 * Time: 15:41
 */

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Models\ParserExchange;

class RatesController extends Controller
{
    /**
     * Данные курсов
     *
     * @param null $from
     * @param null $to
     * @return mixed
     */
    public function rates($from = null, $to = null)
    {
        $parser = ParserExchange::where('name', '=', $from.' - '.$to)->first();
        return $parser->summa;
    }
}