<?php
namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ImageSystem\Facades\ImageSystemFacade;
use App\Http\Controllers\Controller;
use App\Jobs\TelegramVerificationCardJob;
use App\Models\Currency;
use App\Models\Task;
use App\Models\VerificationCard;
use App\Models\VerificationCardCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class VerificationController extends Controller
{
    public function index()
    {
        $currencies = Currency::where('status', 0)->where('is_enabled_verification', '=', 1)->where('is_verified_cabinet', '=', 0)->get();

        $verifications = VerificationCard::where('id_user', auth()->id())->orderBy('id', 'desc')
            ->get()->map(function($item) {
            return [

                'id' => $item->id,
                'type' => 'card_verification',
                'attributes' => [
                    'public_id' => $item->id,
                    'created_at' => $item->created_at->format('c'),
                    'name'      => \Str::upper($item->name),
                    'card_number' => preg_replace('/^(.{4}).*(.{4})$/u', '$1 **** **** $2', remove_all_spaces($item->card_number)),
                    'status'   => $item->status,
                    'id_bank'   => $item->id_currency
                ],

                'relationships' => [
                    'issuing_bank' => [
                        'name' => sprintf("%s %s", $item->currency->payment->name, $item->currency->code_currency->name),
                        'icon_url_path' => '/storage/payment_systems/'.$item->currency->payment->logo,
                    ]
                ]
            ];
        });

        $categories = VerificationCardCategory::where('status', '=', 1)->orderBy('sorting')->get();

        $data = [];
        foreach ($categories as $category)
        {
            $data[] = [
                'name' => $category->name,
                'details' => $category->instructions->toArray()
            ];
        }

        $array = [];
        foreach ($currencies as $item)
        {
            $array[] = [
                'name' => sprintf("%s %s", $item->payment->name, $item->code_currency->name),
                'currency' => $item->id,
                'mask' => $item->mask_input,
                'icon_url_path' => '/storage/payment_systems/'.$item->payment->logo,
                'form' => [
                    'placeholder' => $item->first_char,
                    'default_value' => $item->char_default,
                    'min_char' => $item->min_char,
                    'max_char' => $item->max_char
                ],
            ];
        }
        return response()->json([
                'payments' => $array,
                'cards' => $verifications,
                'instructions' => $data
            ]
        );
    }

    /**
     * Добавить и отправить на проверку
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        // Стандартная система верификации карты
        if($request->has('order_id'))
        {
            $validator = Validator::make($request->all(), [
                'image' => 'required|image',
                'order_id' => 'required|numeric|exists:tasks,id'
            ]);

            if($validator->fails()) {
                return response()->json([
                    'status' => 1,
                    'message' => $validator->messages()->first()
                ]);
            } else {

                $card_exists = VerificationCard::where('id_order', (int)$request->order_id)->where('status', '=', 0)->count();
                if($card_exists > 0)
                {
                    return response()->json([
                        'status' => 1,
                        'message' => 'Вы уже подавали заявку на верификацию карты'
                    ]);
                }

                $order = Task::find($request->order_id);


                // Загружаем фотографию
                $is_local_image = 0;
                if($request->hasFile('image'))
                {
                    $responseImage = ImageSystemFacade::make($request->file('image'))
                        ->setDisk('verification_card')
                        ->put('images/verifications');

                    $filename = $responseImage['filename'];
                    $is_local_image = $responseImage['is_local_image'];
                }

                // Номер счета
                $from_shot = preg_replace('/\s/', '', $order->from_shot);
                $item = VerificationCard::create([
                    'id_user'       =>  $order->user->id,
                    'id_order'      =>  $order->id,
                    'email'         =>  $order->email,
                    'id_currency'   =>  $order->direction_exchange->currency1->id,
                    'card_number'   =>  $from_shot,
                    'card_number_string' => $order->from_shot,
                    'name'          =>  $order->sender_fullname,
                    'image'         =>  $filename ?? null,
                    'status'        =>  0,
                    'ip_address'    =>  $request->ip(),
                    'user_agent'    =>  $request->userAgent(),
                    'is_local_image'    =>  $is_local_image
                ]);

                $item->hash_id = $order->public_id;
                $item->save();

                // Уведомлять о новых заявках на верификацию счетов (Telegram)
                if(iEXSetting('telegram_order_verification_card') == 1) {
                    $delay = now()->addSeconds(5);
                    dispatch(new TelegramVerificationCardJob($item))
                        ->delay($delay)->onQueue('high');
                }

                return response()->json([
                    'status' => 0,
                    'verification_id' => $item->hash_id
                ]);
            }
        } elseif($request->has('account_verification'))
        {
            $validator = Validator::make($request->all(), [
                'image' => 'required|image',
                'card_number' => 'required',
                'fio_account' => 'required|string',
                'payment_system' => 'required|numeric',
            ]);


            if($validator->fails()) {
                return response()->json([
                    'status' => 1,
                    'message' => $validator->messages()->first()
                ]);
            } else {

                $card_number = remove_all_spaces($request->get('card_number'));

                $card_exists = VerificationCard::where([
                    ['card_number', $card_number],
                    ['id_user', auth()->id()]
                ])->where('status', '=', 0)->count();

                if($card_exists > 0)
                {
                    return response()->json([
                        'status' => 1,
                        'message' => 'Вы уже подавали заявку на верификацию карты'
                    ]);
                }

                // Загружаем фотографию
                $is_local_image = 0;
                if($request->hasFile('image'))
                {
                    $responseImage = ImageSystemFacade::make($request->file('image'))
                        ->setDisk('verification_card')
                        ->put('images/verifications');

                    $filename = $responseImage['filename'];
                    $is_local_image = $responseImage['is_local_image'];
                }

                $item = VerificationCard::create([
                    'id_user'       =>  auth()->id(),
                    'id_currency'   =>  $request->get('payment_system'),
                    'card_number'   =>  $card_number,
                    'card_number_string' => $card_number,
                    'name'          =>  $request->get('fio_account'),
                    'image'         =>  $filename ?? null,
                    'status'        =>  0,
                    'ip_address'    =>  $request->ip(),
                    'user_agent'    =>  $request->userAgent(),
                    'is_local_image'    => $is_local_image
                ]);

                $item->hash_id = \Hashids::connection('verification')->encode($item->id);
                $item->save();

                // Уведомлять о новых заявках на верификацию счетов (Telegram)
                if(iEXSetting('telegram_order_verification_card') == 1) {
                    $delay = now()->addSeconds(5);
                    dispatch(new TelegramVerificationCardJob($item))
                        ->delay($delay)->onQueue('high');
                }

                return response()->json([
                    'status' => 0,
                    'verification_id' => $item->hash_id
                ]);
            }
        }
    }

    /**
     * Статус верификации карты
     *
     * @param $hash_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($order_id)
    {
        $card_find = VerificationCard::where('hash_id', (int)$order_id)->orderBy('id', 'desc')->first();
        if(!$card_find) {
            return response()->json([
                'status' => 2
            ]);
        }

        // Если включено, то автоматически верифицируем клиента
        if((int)iEXSetting('enabled_auto_verification_card') == 1) {
            $card_find->update(['status' => 1]);
        }

        if($card_find['status'] == 3) {
            return response()->json([
                'status' => 2,
                'text_message' => $card_find['text_message']
            ]);
        }

        // Отправляем уведомление клиенту о новой заявке
        if($card_find->status == 1) {
            iex_email_notify_new_order($card_find->tasks, 3);
        }

        return response()->json([
            'status' => $card_find->status,
            'text_message' => $card_find->text_message
        ]);
    }
}
