<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\AbstractController;
use Illuminate\Support\Carbon;

class CourseController extends AbstractController
{
    /**
     * Получение файлы курсов
     *
     * @return array
    */
    public function configuration()
    {
        return \Cache::remember('courses_config', Carbon::now()->addHour(),  function() {
            $array = [];
            if(iEXSetting('grates_format_xml') == 1) {
                $array[] = [
                    'name' => iEXSetting('grates_filename') . '.xml',
                    'format' => 'XML'
                ];
            }

            if(iEXSetting('grates_format_txt') == 1) {
                $array[] = [
                    'name' => iEXSetting('grates_filename') . '.txt',
                    'format' => 'TXT'
                ];
            }

            if(iEXSetting('grates_format_json') == 1) {
                $array[] = [
                    'name' => iEXSetting('grates_filename') . '.json',
                    'format' => 'JSON'
                ];
            }

            return $array;
        });
    }
}
