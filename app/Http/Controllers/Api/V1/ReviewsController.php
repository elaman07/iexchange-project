<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.10.2019
 * Time: 21:14
 */

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Resources\V2\Review\ReviewResponse;
use App\Models\LinksReview;
use App\Models\Review;
use App\Models\Task;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    /**
     * Получаем список отзывов
     *
     * @return ReviewResponse
     */
    public function index()
    {
        //Поиск по запросу
        $items = Review::with(['tasks' => function($q) {
            $q->select('id', 'public_id', 'id_direction_exchange', 'give_price', 'receiving_price');
        }, 'tasks.direction_exchange' => function($q) {
            $q->select('id', 'id_currency1', 'id_currency2');
        }, 'tasks.direction_exchange.currency1' => function($q) {
            $q->select('id', 'id_payment', 'id_code_currency');
        }, 'tasks.direction_exchange.currency2' => function($q) {
            $q->select('id', 'id_payment', 'id_code_currency');
        }, 'tasks.direction_exchange.currency1.payment' => function($q) {
            $q->select('id', 'name', 'logo');
        }, 'tasks.direction_exchange.currency2.payment' => function($q) {
            $q->select('id', 'name', 'logo');
        }, 'tasks.direction_exchange.currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'tasks.direction_exchange.currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }])->where('version', '=', 1)->where('status', '=', 1)->orderByDesc('id');


        return new ReviewResponse($items->simplePaginate(10));
    }

    public function reviewInfo()
    {
        $link_reviews = LinksReview::where('is_review', '=', 1)->get()->map(function($item) {
            return [
                'id' => $item->id,
                'icon' => '/storage/links/'.$item->icon,
                'url' => $item->url,
                'count' => $item->count_review
            ];
        });


        return [
            'links' => $link_reviews
        ];
    }

    /**
     * Публикация нового отзыва
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addReview(Request $request)
    {

        if(!iEXSetting('is_enabled_reviews'))
        {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Администратор отключил, публикацию отзывов'
            ]);
        }

        if(iEXSetting('is_auth_type_reviews') == 1 and !auth()->check()) {
            return response()->json([
                'status'    =>  1,
                'message'   =>  'Отзывы могут оставить только авторизованные клиенты'
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'name'      =>  ['required', 'max:40'],
            'text'      =>  ['required', 'max:200']
        ]);

        if ($validator->fails())
        {
            return response()->json([
                'status'    =>  1,
                'message'   =>  $validator->messages()->first()
            ]);
        } else {

            $task = Task::where('public_id', (int)$request->get('order_id'))->first();

            // Получить название
            Review::create([
                'version'               =>  1,
                'id_task'               =>  $task->id,
                'id_direction_exchange' =>  $task->id_direction_exchange,
                'ip_address'            =>  $request->ip(),
                'user_agent'            =>  $request->userAgent(),
                'id_user'               =>  $task->id_user,
                'type'                  =>  ($request->has('type') ? $request->get('type') : 0),
                'name'                  =>  security_xss($request->get('name')),
                'text'                  =>  security_xss($request->get('text')),
                'rate_speed'            =>  (int)$request->get('rate_speed'),
                'status'                =>  0 // Отправляем на проверку
            ]);

            return response()->json([
                'status'    =>  0,
                'message'   =>  'Отзыв отправлен на модерацию'
            ]);
        }
    }

    /**
     * Доступные параметры
     *
     * @param $item
     * @param array $exclude
     * @return array
     */
    protected function json($item, $exclude = [])
    {
        return array_diff_key([
            'id'        =>  $item->id,
            'user'  =>  [
                'name'  =>  $item->name,
                'email' =>  $item->email,
            ],
            'rate_full' => $item->rate_full,
            'rate_speed' => $item->rate_speed,
            'description'      =>  $item->text,
            'created_at'    =>  $item->created_at->translatedFormat('j F Y')
        ], array_flip($exclude));
    }
}
