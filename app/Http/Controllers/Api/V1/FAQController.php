<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\FaqCategory;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;

class FAQController
{

    /**
     * Вопросы и Ответы
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): \Illuminate\Http\JsonResponse
    {
        if((int)config('cache.enable_cache') and (int)iEXSetting('enabled_cache_day_faq'))
        {
            $value = Cache::remember('iexexchanger-faq-'.app()->getLocale(), Carbon::now()->addDays((int)iEXSetting('clear_cache_day_faq')), function () {
                return $this->pageFaqDefault();
            });
            return response()->json($value);
        }
        return response()->json($this->pageFaqDefault());
    }

    /**
     * Кэширование данных Вопросов и Ответов
     *
     * @return array
     */
    private function pageFaqDefault(): array
    {
        $data =  FaqCategory::with(['faq' => function($query) {
            $query->where('status', '=', 1)->orderBy('sorting');
        }])->select('id','name')->orderBy('sorting')
            ->get();

        $array = [];
        foreach ($data as $item) {
            Arr::set($array, $item->id, $item);
        }

        return $array;
    }
}
