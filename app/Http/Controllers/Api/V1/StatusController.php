<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\AbstractController;
use Illuminate\Support\Carbon;

class StatusController extends AbstractController
{
    /**
     * Информация о статусе работы сайта
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
//        // Сайт не отключаем, если клиент оплачивает заявку
//        if(iEXSetting('service_break') == 1)
//        {
//            $start_time = Carbon::now();
//            $lead_time = Carbon::parse(iEXSetting('service_break_interval'));
//            if($lead_time->gt($start_time))
//                $service_break = $lead_time->diffInSeconds($start_time);
//            else
//                iEXSetting(['service_break' =>  0]);
//
//            $site_technical_break = (isset($service_break) ? $service_break : 0);
//        }else {
//            $site_technical_break = 0;
//        }

        return response()->json([
            'status'    =>  isJobOffline(),
        ]);
    }
}
