<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 06.07.2018
 * Time: 15:15
 */

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\AbstractController;

class LanguageController extends AbstractController
{
    /**
     * Получение списка доступных языков
    */
    public function language()
    {
        $array['results'] = [];
        foreach (config('app.all_locale') as $key => $value) {
            $return                 =   [];
            $return['name']         =   $value;
            $return['alias']        =   $key;

            \array_push($array['results'], $return);
        }

        return response()->json($array['results']);
    }
}