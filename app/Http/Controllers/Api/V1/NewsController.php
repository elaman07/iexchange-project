<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 30.09.2019
 * Time: 8:11
 */

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\AbstractController;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class NewsController extends AbstractController
{

    public function index(Request $request)
    {
        //Поиск по запросу
        $items = News::orderBy('id', 'desc')->paginate(20);
        $array = [
            'count'     =>  $items->total(),
            'next'      =>  $items->nextPageUrl(),
            'previous'  =>  $items->previousPageUrl()
        ];

        $array['results'] = [];

        foreach ($items as $item) {
            array_push($array['results'], $this->json($item, [
                'text'
            ]));
        }

        return response()->json($array);
    }

    /**
     * Получение записей по Parent_url
     *
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id = null)
    {
        $id_int = explode('-', $id)[0];



        $item = News::findOrFail($id_int);

//        $item->views = $item->views+1;
//        $item->save();

        $news_image = '/storage/news/'.$item->image;
        if(!empty(iEXSetting('storage_disk_news')) and $item->is_local_image == 1) {
            $news_image = iex_dynamic_read_view_image('/news/'.$item->image, false);
        }

        return \response()->json([
            'results'   =>  [
                'title'     =>  $item->name,
                'body'      =>  $item->text,
                'slug'      =>  $item->id.'-'.$item->parent_url,
                'short_text'=>  $item->cr_text,
                'parent_url'=>  config('app.url').'/news/'.$item->id,
                'image'     =>  [
                    'big'   =>  $news_image,
                ],
                'created_at'    =>  Carbon::parse($item->created_at)->translatedFormat('d M Y H:i'),
            ]
        ]);
    }

    /**
     * Доступные параметры
     *
     * @param $item
     * @param array $exclude
     * @return array
     */
    protected function json($item, $exclude = [])
    {
        $news_image = '/storage/news/'.$item->image;
        if(!empty(iEXSetting('storage_disk_news')) and $item->is_local_image == 1) {
            $news_image = iex_dynamic_read_view_image('/news/'.$item->image, false);
        }

        return array_diff_key([
            'id'        =>  $item->id,
            'title'     =>  $item->name,
            'slug'      =>  $item->id.'-'.$item->parent_url,
            'body'      =>  $item->text,
            'short_text'=>  '',
            'parent_url'=>  $item->parent_url,
            'image'     =>  [
                'middle'   =>  $news_image,
            ],
            'created_at'    =>  Carbon::parse($item->created_at)->translatedFormat('d M Y H:i'),
        ], array_flip($exclude));
    }
}
