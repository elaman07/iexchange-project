<?php
namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Common\Packages\ReferralSystem\Models\ReferralRelationship;
use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use App\Models\ReferralStatistics;
use App\Models\RewardLog;
use App\Models\Task;
use App\Models\WithdrawalRequest;
use Carbon\CarbonInterval;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class StatisticController extends Controller
{
    /**
     * Реферальные начисления
    */
    public function referral_order()
    {
        $accruals = WithdrawalRequest::where('id_user', \auth()->id());

        $array = [];
        foreach ($accruals->get() as $item) {
            $array[] = [
                'name' => $item->id
            ];
        }

        return response()->json($array);
    }

    /**
     * Графический список популярный транзакций
     *
     * @throws \Exception
     */
    public function get_popular_tx()
    {
        if(config('cache.enable_cache') and config('cache.user_stat')) {
            $result = cache()->remember('user-'.\auth()->id().'-stat', Carbon::now()->addMinutes(120), function() {
                return $this->unCachePopularTX()->getContent();
            });
            return $result;
        }

        return $this->unCachePopularTX()->getContent();
    }

    /**
     * Статистика рефералов
     *
     * @throws \Exception
     */
    public function referral_statistics()
    {
        return $this->unCacheReferralStatistics()->getContent();
    }


    /**
     * Некэшированный пользовательская статистика
     *
    */
    protected function unCachePopularTX()
    {
        $task = Task::where('status', 4)->where('id_user', Auth::id())->get()
            ->groupBy('id_direction_exchange')
            ->map(function($item) {
                return $item->count();
            })->sortByDesc(function($key, $value) {
                return $key;
            });

        //Для графики
        $array['name'] = 'Обменов';
        $array['data'] = [];
        $detail = [];
        $reward = [];
        $reward_graphic['name'] = 'Бонус в (руб.)';
        $reward_graphic['data'] = [];
        $amounts = null;

        if($task->count() > 0)
        {
            foreach ($task as $key => $value) {
                $exchange = DirectionExchange::find($key);
                $array['data'][] = [
                    'name'  =>  direction_name($exchange),
                    'y'     =>  $value
                ];
            }
            //Подробнее
            foreach ($task->take(5) as $key => $value) {
                $exchange = DirectionExchange::find($key);
                array_push($detail, [
                    'name'              =>  direction_name($exchange),
                    'average_time'      =>  order_average_time($key),
                    'count'             =>  $value
                ]);
            }

            //Статистика бонусных начислений
            $rewardLog = RewardLog::where('id_user', Auth::id());
            foreach ($rewardLog->orderByDesc('bonus')->get()->take(5) as $item) {

                array_push($reward, [
                    'name'  =>  direction_name($item->task, true),
                    'bonus' =>  $item->bonus
                ]);
            }

            //Статистика для графики
            foreach ($rewardLog->orderBy('id')->get() as $item)
            {
                $reward_graphic['data'][] = [
                    'name'  =>  Carbon::parse($item->created_at)->timestamp * 1000,
                    'y'     =>  (float)$item->bonus
                ];
            }

            $common_amounts = Task::where('status', 4)->where('id_user', Auth::id());
            foreach ($common_amounts->get() as $item) {

                if(!isset($item->task_convert_log['to_rub']))
                    continue;

                $amounts += is_null($item->task_convert_log['to_rub']) ? 0 : $item->task_convert_log['to_rub'];
            }
        }

        return response()->json([
            'count'     =>  $task->count(),
            'count_order'   =>  Task::where('status', 4)->count(),
            'detail'    =>  $detail,
            'graphic'   =>  $array,
            'reward'    =>  $reward,
            'reward_graphic'    =>  $reward_graphic,
            'amounts'           =>  $amounts
        ]);
    }

    /**
     * Некешированная статистика рефералов
     */
    protected function unCacheReferralStatistics()
    {
        $start_year = Carbon::now()->startOfYear();
        $end_year = Carbon::now()->endOfYear();

        $interval = new \DatePeriod(
            Carbon::parse($start_year),
            CarbonInterval::month(),
            Carbon::parse($end_year)
        );

        $referral_link = ReferralLink::where('user_id', \auth()->id())->first();
        $referral_ship = \Cache::remember('user-referral__chips__'.\auth()->id(), Carbon::now()->addDay(), function() use ($referral_link, $start_year, $end_year)
        {
            return ReferralRelationship::where([
                ['referral_link_id', $referral_link->id]
            ])->whereBetween('created_at', [$start_year, $end_year])
                ->get()->groupBy(function($item) {
                    return $item->created_at->format('Y-m');
                })->map(function ($month) {
                    return $month->count();
                });
        });

        //Заработок с рефералов (Получение графики)
        $referral_log_amount = \Cache::remember('user-referral__summa__'.\auth()->id(), Carbon::now()->addDay(), function() use ($start_year, $end_year)
        {
            return ReferralLog::where('id_user', \auth()->id())
                ->whereBetween('created_at', [$start_year, $end_year])
                ->get()
                ->groupBy(function($item) {
                    return $item->created_at->format('Y-m');
                })->map(function ($month) {
                    return $month->sum('bonus_number');
                })->toArray();
        });

        $array_amount = [];
        $referral_user = [];
        foreach ($interval as $date) {
            $day = $date->format('Y-m');
            $array_amount[] = [Carbon::parse($day)->addDay(1)->timestamp * 1000, ($referral_log_amount[$day] ?? 0)];
            $referral_user[] = [Carbon::parse($day)->addDay(1)->timestamp * 1000, ($referral_ship[$day] ?? 0)];
        }

        //Общая сумма заработка за рефералы
        $sum_amount = ReferralLog::where('id_user', \auth()->id())->sum('bonus_number');

        $withdrawal = WithdrawalRequest::with(['currency' => function($q) {
            $q->select('id', 'id_payment', 'id_code_currency');
        },'currency.payment' => function($q) {
            $q->select('id', 'name');
        },'currency.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'withdrawal_request_log',
            'user' => function($q) {
                $q->select('id');
            },
            'user.user_balance'])->where([
            ['id_user', \auth()->id()],
            ['referral', 1]
        ]);
        $accruals = [];
        foreach ($withdrawal->get() as $item)
        {
            $accruals[] = [
                'currency' => $item->currency->payment->name . ' ' . $item->currency->code_currency->name,
                'shot' => $item->score,
                'status' => $item->status,
                'amount' => (isset($item->withdrawal_request_log) ? $item->withdrawal_request_log->amount : $item->user->user_balance->balance . ' руб.')
            ];
        }

        // Статистика реферальная
        $transition = ReferralStatistics::whereRaw("BINARY `ref_hash` = ?",[$referral_link->code])
            ->whereDate('created_at', Carbon::today())
            ->where('is_archive', '=', 0)->count();

        $referral_count = ReferralRelationship::where('referral_link_id', '=', $referral_link->id)->count();
        $referrals_logs = ReferralLog::with(['user_admin','user_admin.user_balance'])
            ->select('id_user')
            ->where('referral_log.id_user', \auth()->id())
            ->selectRaw("count(*) as count_num")
            ->selectRaw("sum(`bonus_number`) as total_amount")
            ->first();


        // Статистика и реферальный процент
        $info = [
            'percent' => $referral_link->program->percent,
            'count_transitions' => iex_number_format($transition, 0, true),
            'count_referral' => iex_number_format($referral_count, 0, true),
            'balance' => iex_number_format($referrals_logs['total_amount'], 2, true),
            'code_sign' => config('crypto.currency_payout_sign'),
            'position' => config('crypto.currency_payout_position'),
            'enabled_referral_system_space_indent' => iEXSetting('enabled_referral_system_space_indent')
        ];

        return response()->json([
            'accruals'      =>  $accruals,
            'info'          =>  $info,
            'amount'        =>  $array_amount,
            'attracted'     =>  $referral_user,
            'statistics'    =>  [
                'earned'        =>  number_format($sum_amount,2,'.',' '),
                'attracted'     =>  $referral_link->relationships()->count()
            ],
            'settings' => [
                'is_referral_disable_history_orders' => (int)iEXSetting('is_referral_disable_history_orders', 0),
                'is_referral_disable_profit_ref' => (int)iEXSetting('is_referral_disable_profit_ref', 0),
                'is_referral_disable_involved_clients' => (int)iEXSetting('is_referral_disable_involved_clients', 0),
                'is_referral_disable_profit_money' => (int)iEXSetting('is_referral_disable_profit_money', 0)
            ]
        ]);
    }
}
