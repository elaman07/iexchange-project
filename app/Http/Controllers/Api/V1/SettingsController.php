<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\AbstractController;
use App\Models\Task;
use Illuminate\Support\Facades\Cache;

class SettingsController extends AbstractController
{
    /**
     * Получить настройки
     */
    public function index() {
        return response()->json($this->handlerSettings());
    }

    /**
     * Обработчик
    */
    private function handlerSettings()
    {
        $array = [
            'block_visible_reserve' =>  iEXSetting('block_visible_reserve'),
            'visible_last_exchange' =>  iEXSetting('visible_last_exchange'),
            'visible_description'   =>  iEXSetting('visible_description'),
            'visible_my_order'      =>  iEXSetting('visible_my_order'),
            'allow_reserve'         =>  iEXSetting('allow_reserve'),
            'hide_exchange_guest'   =>  iEXSetting('hide_exchange_guest'),
            'order_count'           =>  0
        ];

        if(iEXSetting('visible_last_exchange')) {
            $array['last_items'] = $this->cacheLastExchange();
        }

        if(iEXSetting('visible_description')) {
            $array['description_home'] = iEXSetting('description_home');
        }
        return $array;
    }


    /**
     * Получить последние обмены
    */
    protected function cacheLastExchange()
    {
        if(config('cache.enable_cache') and config('cache.enable_cache_last_exchange')) {
            $value = Cache::remember('exchange', config('cache.clear_cache'), function () {
                return $this->queryLastExchange();
            });
            return $value;
        }

        return $this->queryLastExchange();
    }

    /**
     * Запрос на получение последних обменов
    */
    protected function queryLastExchange()
    {
        //last items
        $lastItems = Task::where('status', 4)->orderByDesc('id')
            ->limit(iEXSetting('count_last_exchange'))->get();

        $array = [];
        foreach ($lastItems as $lastItem)
        {
            array_push($array, [
                'in'    =>  currency_in($lastItem, true),
                'out'    =>  currency_out($lastItem, true),
                'in_price'  =>  $lastItem->give_price,
                'out_price' => $lastItem->receiving_price,
                'updated_at' => $lastItem->updated_at->format('d.m.Y H:i')
            ]);
        }

        return $array;
    }
}