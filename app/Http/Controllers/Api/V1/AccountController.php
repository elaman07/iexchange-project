<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\AbstractController;
use App\Http\Resources\Sessions\CurrentUserResource;
use App\Models\UserAuth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AccountController extends AbstractController
{
    /**
     * Статус авторизации пользователя
     *
     * @return JsonResponse
     */
    public function isAuth(): JsonResponse
    {
        return response()->json(Auth::check());
    }

    public function user(Request $request): CurrentUserResource
    {
        return new CurrentUserResource($request->user());
    }

    public function update_restapi(Request $request)
    {
        if(config('admin.is_demo_mode')) {
            return [
                'status' => 1
            ];
        }

        $user = $request->user();

        if($user->is_enabled_restapi == 1)
        {
            $user->tokens()->delete();
            $user->update([
                'restapi_key' => $user->createToken('user-'.$user->id)->plainTextToken
            ]);

            return response()->json(['status' => 0, 'token' => $user->restapi_key]);
        } else {
            return response()->json(['status' => 0, 'token' => 'error api']);
        }
    }

    /**
     * Смена интерфейса для клиентов
     * @param Request $request
     * @return array
     */
    public function user_style(Request $request): array
    {
        if(config('admin.is_demo_mode')) {
            return [
                'status' => 1
            ];
        }

        $style = security_xss($request->has('style') ? $request->get('style') : (int)iEXSetting('interface_exchange'));
        $user = $request->user();

        $user->update([
            'user_style' => $style
        ]);

        $array['status'] = 0;
        return $array;
    }


    /**
     * Сохраняем информацию о пользователе
     *
     * @param Request $request
     * @return array
     */
    public function user_update(Request $request): array
    {
        if(config('admin.is_demo_mode')) {
            return [
                'status' => 1
            ];
        }

        $user = $request->user();
        $filterName = 'required|string|max:50';
        if(iEXSetting('is_filter_username')) {
            $filterName .= '|regex:/[A-Za-zА-Яа-яЁё -]$/u';
        }

        $validate = Validator::make($request->all(), [
            'name' => $filterName,
        ]);

        if($validate->fails()) {
            $array['status']  = 1;
            $array['message'] = $validate->errors()->first();
        }else {
            $event_value = 'Редактирование личных данных.<br />';
            $event_value .= 'Имя: <span style="color:red">'.$user->name.'</span> => <span style="color:green">'.$request->name.'</span><br />';
            $event_value .= 'Номер: <span style="color:red">'.$user->phone.'</span> => <span style="color:green">'.$request->phone.'</span>';
            if(!empty($user->telegram)) {
                $event_value .= 'Telegram: <span style="color:red">'.$user->telegram.'</span> => <span style="color:green">'.$request->telegram.'</span>';
            }

            event_users_history(__('Изменение'), $event_value, $user->id);

            $user->name = $request->has('name') ? security_xss($request->get('name')) : 'nonname';
            $user->phone = $request->has('phone') ? $request->get('phone') : null;
            $user->telegram = $request->has('telegram') ? $request->get('telegram') : null;
            $user->save();

            // Записываем в лог информацию об редактировании личных данных
            if(iEXSetting('event_log_is_user_edit')) {
                $event_log = sprintf(__('Пользователь %s изменил личные данные'), $request->user()->name);
                main_event_log('event_log_is_user_edit', $event_log);
            }

            $array['status'] = 0;
            $array['message'] = __('Данные успешно изменены');
            $array['user'] = new CurrentUserResource($user);
        }

        return $array;
    }

    /**
     * Обновление пароля пользователя
     *
     * @param Request $request
     * @return array
     */
    public function update_password(Request $request): array
    {
        if(config('admin.is_demo_mode')) {
            return [
                'status' => 1
            ];
        }

        $user = $request->user();

        $validate = Validator::make($request->all(), [
            'current_password'  => 'required',
            'password'          => 'required|string|min:6|confirmed'
        ], [
            'password.confirmed'    =>  'Пароли не совпадают'
        ]);

        if($validate->fails()) {
            $array['status']  = 1;
            $array['message'] = $validate->errors()->first();
        }else
        {
            $current_password = null;
            if(!Hash::check($request->get('current_password'), $user->password)) {
                $current_password = __('Неверный старый пароль');
            }

            if($current_password == null) {
                event_users_history(__('Изменение'), __('Изменение пароля'), $user->id);
                $user->password = bcrypt($request->get('password'));
                $user->save();

                $array['status'] = 0;
                $array['message'] = __('Данные успешно изменены');
            }else {
                $array['status'] = 1;
                $array['message'] = $current_password;
            }
        }

        return $array;
    }

    /**
     * Получаем лог авторизаций пользователя
    */
    protected function userLogsAuth()
    {
        $agent = UserAuth::where('id_user', \auth()->id())
            ->orderBy('id', 'desc')->limit(20)->get();

        $array = [];
        foreach ($agent as $item)
        {
            $array[] = [
                'browser' => $item->browser,
                'os' => $item->os,
                'ip' => $item->ip,
                'created_at' => Carbon::parse($item->created_at)->translatedFormat('d M Y, H:i'),
                'status' => $item->status,
                'country' => $item->country,
                'city' => $item->city
            ];
        }

        return $array;
    }
}
