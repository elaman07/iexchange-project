<?php

namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Common\Packages\ReferralSystem\Models\ReferralRelationship;
use App\Http\Controllers\Controller;
use App\Http\Resources\Angular\ReferralResource;
use App\Models\ReferralStatistics;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class AccountPartnerController extends Controller
{
    public function info()
    {
        $user = User::find(\auth()->id());
        $referral_link = ReferralLink::where('user_id', \auth()->id())->first();
        $referral_count = ReferralRelationship::where('referral_link_id', '=', $referral_link->id)->count();

        $response = ReferralLog::with(['tasks' => function($q) {
            $q->select('id', 'public_id', 'id_direction_exchange');
        }, 'user' => function($q) {
            $q->select('id', 'name');
        }])->where('id_user', auth()->id())
            ->where('bonus_number', '>', 0)->orderBy('id_task', 'desc');

        $histories = [
            'items' => (new ReferralResource($response->simplePaginate(10))),
            'count' => $response->count()
        ];

        //Общая сумма заработка за рефералы
        $sum_amount = ReferralLog::where('id_user', \auth()->id())->sum('bonus_number');

        // Статистика и реферальный процент
        $info = [
            'percent' => $referral_link->program->percent,
            'count_referral' => iex_number_format($referral_count, 0, true),
            'total_balance' => $user->user_balance->referral_total_profit,
            'total_withdrawal' => $user->user_balance->referral_total_withdrawal,
            'code_sign' => config('crypto.currency_payout_sign'),
            'position' => config('crypto.currency_payout_position'),
            'enabled_referral_system_space_indent' => iEXSetting('enabled_referral_system_space_indent')
        ];

        return response()->json([
            'info'          =>  $info,
            'user'          =>  [
                'referral_hash'     => $user->referral_links->getHashAttribute(),
                'referral_hash_url' => config('app.url').'/?ref='.$user->referral_links->getHashAttribute(),
                'balance'           => $user->user_balance->balance+$user->user_balance->balance_reward
            ],
            'histories' => $histories,
            'statistics'    =>  [
                'earned'        =>  number_format($sum_amount,2,'.',' '),
                'attracted'     =>  $referral_link->relationships()->count()
            ],
            'settings' => [
                'is_referral_disable_history_orders' => (int)iEXSetting('is_referral_disable_history_orders', 0),
                'is_referral_disable_profit_ref' => (int)iEXSetting('is_referral_disable_profit_ref', 0),
                'is_referral_disable_involved_clients' => (int)iEXSetting('is_referral_disable_involved_clients', 0),
                'is_referral_disable_profit_money' => (int)iEXSetting('is_referral_disable_profit_money', 0)
            ]
        ]);
    }


    /**
     * Обновляем реферальную ссылку
     *
     * @param Request $request
     * @return array
     */
    public function updateUsername(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username' => 'required|string|max:255|unique:referral_links,code|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
        ]);

        $user = $request->user();
        if($validate->fails()) {
            $array['status'] = 1;
            $array['message'] = $validate->errors()->first();
        }else {
            $event_value = 'Имя пользователя: <span style="color:red">'.$user->username.'</span> => <span style="color:green">'.$request->username.'</span><br />';
            event_users_history('Изменение', $event_value, $user->id);

            $user->update([
                'username' => security_xss($request->get('username'))
            ]);

            ReferralLink::where('user_id', $user->id)->update([
                'code' => security_xss($request->get('username'))
            ]);

            $array['status']    = 0;
            $array['message']   = 'Данные обновлены';
        }

        return $array;
    }

    public function histories(Request $request)
    {
        $response = ReferralLog::with(['tasks' => function($q) {
            $q->select('id', 'public_id', 'id_direction_exchange');
        }, 'user' => function($q) {
            $q->select('id', 'name');
        }])->where('id_user', $request->user()->id)
            ->where('bonus_number', '>', 0)->orderBy('id_task', 'desc');


        return (new ReferralResource($response->simplePaginate(10)));
    }
}
