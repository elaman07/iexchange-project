<?php

namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\ImageSystem\Facades\ImageSystemFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Events\OrderStatusesEvent;
use App\Http\Controllers\AbstractController;
use App\Http\Resources\V2\Order\{OrderAccountResource, OrderProcessResource, OrderStatusResource, OrdersUserResource};
use App\Jobs\{AdminNewOrderJob, NexmoNewOrder, TelegramOrderJob};
use App\Models\{Reserve, Task, TaskInfo, TasksCheckImage, TaskStatusLog};
use Illuminate\Http\{JsonResponse, Request};
use Illuminate\Support\{Carbon, Facades\Validator};


class AccountOrderController extends AbstractController
{
    /**
     * Список всех заявок клиента
     *
     * @param Request $request
     * @return OrdersUserResource
     */
    public function orders(Request $request): OrdersUserResource
    {
        $items = Task::with(['task_info' => function($q) {
            $q->select('id', 'currency_sign_payout', 'currency_position_payout');
        }, 'tasks_rejection_status' => function($q) {
            $q->select('id', 'name');
        },'direction_exchange' => function($q) {
            $q->select('id', 'id_currency1', 'id_currency2');
        },'direction_exchange.currency1' => function($q) {
            $q->select('id', 'number_format', 'id_payment', 'id_code_currency');
        },'direction_exchange.currency1.payment' => function($q) {
            $q->select('id', 'name', 'logo', 'is_local_image');
        },'direction_exchange.currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        },'direction_exchange.currency2' => function($q) {
            $q->select('id', 'number_format', 'id_payment','id_code_currency');
        },'direction_exchange.currency2.payment' => function($q) {
            $q->select('id', 'name', 'logo', 'is_local_image');
        },'direction_exchange.currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }])->select('id', 'id_direction_exchange', 'started_at', 'status', 'updated_at', 'public_id', 'id_rejection_status', 'give_price', 'receiving_price', 'from_shot', 'to_shot', 'id_user', 'created_at')
            ->where('id_user', '=', $request->user()->id)
            ->whereIn('status', explode(',', iEXSetting('displayed_statuses')))
            ->orderBy('id','desc');

        return new OrdersUserResource($items->simplePaginate(10));
    }

    /**
     * Полная информация о заявке
     *
     * @param int $public_id
     * @return OrderProcessResource
     */
    public function process(int $public_id): OrderProcessResource
    {
        $order = Task::where('public_id', '=', $public_id)->firstOrFail();
        return new OrderProcessResource($order);
    }

    /**
     * Статус заявки
     *
     * @param int $public_id
     * @return OrderStatusResource| JsonResponse
     */
    public function status(int $public_id): OrderStatusResource | JsonResponse
    {
        $order = Task::select('id', 'status', 'public_id', 'created_at', 'id_payment_requisites', 'is_request_payment_type', 'requisites_receive', 'requisites_description')
            ->where('public_id', '=', $public_id)->first();

        if(empty($order)) {
            return response()->json([]);
        }

        return new OrderStatusResource($order);
    }

    /**
     * Информация о заявки в личном кабинете
     *
     * @param Request $request
     * @param int $public_id
     * @return OrderAccountResource|JsonResponse
     */
    public function account(Request $request, int $public_id)
    {
        $order = Task::where('public_id', '=', $public_id)->firstOrFail();

        // Если клиент не авторизован, проверяем заявку по IP Адресу
        if(!auth()->check() and (int)iEXSetting('disable_check_display') == 1) {
            if($request->ip() != $order->ip) {
                return response()->json([
                    'is_error_ip' => 1
                ]);
            }
        }

        return new OrderAccountResource($order);
    }

    /**
     * Создание новой заявки
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(Request $request): JsonResponse
    {
        $errors = collect();
        if(isJobOffline() == 1)
        {
            $errors->push([
                'field' => 'buy',
                'message' => 'Нет связи с сервером.',
                'modal'     =>  false
            ]);
            return response()->json($errors->filter()->all());
        }

        $validator = Validator::make($request->all(), [
            'income_amount'             =>   'required|numeric|min:0',
            'outcome_amount'            =>   'required|numeric|min:0',
            'income_payment_system'     =>   'required|numeric|exists:currencies,id|regex:/^[0-9]+$/',
            'outcome_payment_system'     =>  'required|numeric|exists:currencies,id|regex:/^[0-9]+$/',
        ]);
        //unique:users
        $validator->sometimes('email', 'email|required|max:150', function () {
            return \Auth::guest();
        });
        $validator->setAttributeNames([
            'email' => 'Email адрес', 'income_amount' => 'Отдаю', 'outcome_amount' => 'Получаю', 'phone' => 'Ваш номер'
        ]);

        // Валидация полученной формы
        if($validator->fails())
        {
            $fields = ['email', 'income_amount', 'outcome_amount', 'phone'];
            foreach ($fields as $field)
            {
                if($validator->errors()->has($field))
                {
                    $this->setItems([
                        'field'     =>  $field,
                        'modal'     =>  false,
                        'message'   =>  $validator->errors()->first($field)
                    ]);
                }
            }
            return response()->json($this->getItem());
        } else {
            $order = \Order::request($request);
            return response()->json($order->created());
        }
    }

    /**
     * Отклонение заявки
     *
     * @param int $public_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function cancel(int $public_id): JsonResponse
    {
        $order = Task::select('id', 'public_id', 'status', 'is_bot', 'id_user', 'give_price', 'receiving_price')->where('public_id', $public_id)->first();
        if($order->status == 6) {
            return response()->json(['status' => 0]);
        }

        // Страховка, в случае если клиент попытается обмануть.
        if(!in_array($order->status, [2, 9])) {
            throw new \Exception('Ошибка при создании заявки №'.$order->id);
        }

        // Записываем лог статус
        TaskStatusLog::create([
            'id_task'       => $order->id,
            'user_id'       => $order->id_user,
            'old_status'    => $order->status,
            'new_status'    => 6,
            'in_price'      =>  $order->give_price,
            'out_price'     =>  $order->receiving_price,
            'course_display'  => $order->course_display
        ]);

        $order->update([
            'status' => 6,
            'is_bot' => 0
        ]);

        return response()->json([
            'status' => 0
        ]);
    }

    /**
     * Подтверждение о создании заявки
     *
     * @param Request $request
     * @param int $public_id
     * @return JsonResponse
     * @throws \Exception
     */
    public function confirm(int $public_id, Request $request): JsonResponse
    {
        $validate = validator($request->all(), [
            'type'      =>  'required'
        ], [
            'income_code.min'    =>  'Укажите правильный "Номер чека" '
        ]);

        //Проверяем EXCode на доступность
        $validate->sometimes('income_code', 'required|min:15', function () use($request) {
            return $request->has('income_code');
        });

        if($validate->fails()) {
            if($validate->errors()->has('income_code')) {
                $this->setItem('status', 1);
                $this->setItem('message', $validate->errors()->first('income_code'));
            }
        } else
        {
            $task = Task::where('public_id', $public_id)->first();

            // Страховка, в случае если клиент попытается обмануть.
            if($task->status != 2) {
                throw new \Exception('Ошибка при создании заявки №'.$task->id);
            }

            // Записываем лог статус
            TaskStatusLog::create([
                'id_task'       => $task->id,
                'user_id'       => $task->id_user,
                'old_status'    => $task->status,
                'new_status'    => ($request->type == 'paid' ? 3 : 1),
                'in_price'      =>  $task->give_price,
                'out_price'     =>  $task->receiving_price,
                'place_change'  =>  0,
                'course_display'  => $task->course_display
            ]);

            $task->status           =   ($request->type == 'paid' ? 3 : 1);
            $task->lead_time        =   Carbon::now()->addMinutes((int)iEXSetting('lead_time_task'));
            $task->started_at       =   Carbon::now()->toDateTimeString();

            if($request->has('income_code')) {
                $task->income_code  = $request->income_code;
            }

            $task->save();

            // Проверяем Номер перевода
            if($request->has('num_tx')) {
                TaskInfo::where('id_task', '=', $task->id)->update([
                    'num_transaction' =>  $request->get('num_tx')
                ]);
            }

            // Проверяем Номер перевода
            if($request->has('note_tx')) {
                TaskInfo::where('id_task', '=', $task->id)->update([
                    'note_tx' =>  $request->get('note_tx')
                ]);
            }

            if($request->type == 'paid')
            {
                $tx = TransactionFacade::call($task->id);
                // Изменить резерв
                $tx->changeReserves();

                // Если заявка мошенническая то морозим
                if($task->task_info->is_freeze_scam == 1) {
                    $tx->setDeferType(5);
                    $tx->defer();
                } else {

                    if(iEXSetting('is_enabled_module_socket') == 1) {
                        broadcast(new OrderStatusesEvent($task));
                    }

                    // Уведомляем оператора о новой заявки
                    if((int)iEXSetting('is_mail_notify_order_manager') == 1) {
                        // Уведомляем оператора о новой заявки
                        dispatch(new AdminNewOrderJob($task))->delay(
                            now()->addSeconds(30)
                        )->onQueue('low');
                    }

                    // SMS Уведомление для оператора о новой заявке
                    if (iEXSetting('enable_sms_notification') and iEXSetting('sms_new_order')) {
                        $delay = now()->addMinute();
                        dispatch(new NexmoNewOrder($task))->delay($delay)->onQueue('low');
                    }

                    // Уведомлять о новых заявках в Telegram (Для операторов)
                    if((int)iEXSetting('enable_tg_notify_operator', 0) == 1) {
                        $delay = now()->addSeconds(15);
                        dispatch(new TelegramOrderJob($task))->delay($delay)->onQueue('high');
                    }

                    // Отправляем уведомление клиенту о подтверждении заявки
                    iex_email_notify_new_order($task, 2);
                }
            }

            $this->setItem('result', 'ok');
        }

        return response()->json($this->getItem());
    }

    public function addImage(Request $request)
    {
        // Стандартная система верификации карты
        if($request->has('order_id'))
        {
            $validator = Validator::make($request->all(), [
                'image' => 'required|image',
                'order_id' => 'required|numeric|exists:tasks,public_id'
            ]);

            if($validator->fails()) {
                return response()->json([
                    'status' => 1,
                    'message' => $validator->messages()->first()
                ]);
            } else {

                $order = Task::where('public_id', (int)$request->order_id)->first();

                // Загружаем фотографию
                $is_local_image = 0;
                if($request->hasFile('image'))
                {
                    $responseImage = ImageSystemFacade::make($request->file('image'))
                        ->setDisk('checks')
                        ->put('images/checks');

                    $filename = $responseImage['filename'];
                    $is_local_image = $responseImage['is_local_image'];
                }

                $item = TasksCheckImage::create([
                    'id_order'      =>  (int)$order->id,
                    'image'         =>  $filename ?? null,
                    'ip_address'    =>  $request->ip(),
                    'user_agent'    =>  $request->userAgent(),
                ]);

                $order->update([
                    'is_file_check' => 1
                ]);

                return response()->json([
                    'status' => 0
                ]);
            }
        }
    }

    /**
     * Проверка резерва
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function hasReserve(Request $request): JsonResponse
    {
        $item = Reserve::find(intval($request->id));
        $available = iex_reserve_amount($item);
        return response()->json([
            'result' => $available < $request->amount,
            'amount' => $available.' '.$item->currency->code_currency->name
        ]);
    }
}
