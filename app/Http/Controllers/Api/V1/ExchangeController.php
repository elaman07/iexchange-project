<?php
namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\Crypto\Facades\CompilerFacade;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class ExchangeController extends Controller
{
    /**
     * Получение сформированного файла курсов (первоначально).
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCompiler()
    {
        // Стандартное кэширование
        if(config('cache.enable_cache') and config('cache.enable_cache_exchange'))
        {
            $value = Cache::remember('exchange', now()->addMinutes(config('cache.clear_cache_exchange')), function () {
                return CompilerFacade::compile()->toArray();
            });
            return response()->json($value);
        }

        return response()->json(CompilerFacade::compile()->toArray());
    }

    /**
     * Обновление файла курсов на новые данные
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getUpdateCompiler()
    {
        // Получение курсов из redis Multi
        if((int)iEXSetting('type_cached_rates') == 1) {
            return CompilerFacade::getCSRedisMulti();
        }

        return response()->json(CompilerFacade::update()->toArray());
    }
}
