<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Sessions\CurrentUserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    /**
     * Информация о пользователе
     *
     * @param Request $request
     * @return CurrentUserResource|array
     */
    public function current(Request $request)
    {
        // Если пользователь не авторизован
        if(Auth::check() == false) {
            return [
                'errors' => [
                    'code' => 'api_error/not_authenticated',
                    'detail' => 'Пользователь не авторизован'
                ]
            ];
        }
        
        return new CurrentUserResource($request->user());
    }
}