<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\UserVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UserVerifyController extends Controller
{
    public function index(Request $request)
    {
        $is_check = UserVerification::where('user_id', auth()->id())->where('status', 0)->exists();
        $is_verified = $request->user()->is_verify_account === 1;
        $histories = UserVerification::where('user_id', (int)auth()->id())->get()->map(function($item) {
            return [
                'created_at' => Carbon::parse($item->created_at)->diffForHumans(),
                'status' => $item->status
            ];
        });

        return response()->json([
            'histories' => $histories,
            'is_check' => $is_check,
            'is_verified'   => $is_verified
        ]);
    }

    /**
     * Добавить и отправить на проверку
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file_one' => 'required|image',
            'file_two' => 'required|image',
            'fio_user'  => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'status' => 1,
                'message' => $validator->messages()->first()
            ]);
        } else {

            $user_exists = UserVerification::where('user_id', (int)auth()->id())->where('status', '=', 0)->count();
            if($user_exists > 0)
            {
                return response()->json([
                    'status' => 1,
                    'message' => 'Вы уже подавали заявку на верификацию'
                ]);
            }

            $filename_one = Str::uuid()->toString().'.jpg';
            $filename_two = Str::uuid()->toString().'.jpg';

            // Загружаем фотографию
            if($request->hasFile('file_one')) {
                $img = Image::make($request->file('file_one')->getRealPath());
                $img->save(public_path('storage/user_verify/'.$filename_one));
            }

            // Загружаем фотографию
            if($request->hasFile('file_two')) {
                $img = Image::make($request->file('file_two')->getRealPath());
                $img->save(public_path('storage/user_verify/'.$filename_two));
            }

            $item = UserVerification::create([
                'user_id'   =>  auth()->id(),
                'file_one'  =>  $filename_one,
                'file_two'  => $filename_two,
                'fio_user'  =>  $request->get('fio_user'),
                'ip_address'    =>  $request->ip(),
                'user_agent'    =>  $request->userAgent(),
                'status'    =>  0,
            ]);
            $item->hash_id = \Hashids::connection('user_verify')->encode($item->id);
            $item->save();

            return response()->json([
                'status' => 0,
                'verify_id' => $item->hash_id
            ]);
        }
    }
}
