<?php

namespace App\Http\Controllers\Api\V1;

use App\Common\Packages\Pages\Pages;
use App\Http\Controllers\Controller;
use App\Models\RulesPage;

class RulesPageController extends Controller
{

    public function categories(): \Illuminate\Http\JsonResponse
    {
        $rules = RulesPage::where('status', '=', 1)->orderBy('sorting')->get()->map(function($item) {
            return [
                'id' => $item->page->page_slug,
                'title' => $item->title
            ];
        })->toArray();

        return response()->json([
            'title' => iEXContentLanguage('title_rules_page'),
            'text' => iEXContentLanguage('description_rules_page'),
            'categories' => $rules
        ]);
    }

    public function view(string $id)
    {
        $view = Pages::getPage(security_xss($id));


        return response()->json([
            'title' => $view->page_title,
            'content' => $view->page_content
        ]);
    }
}
