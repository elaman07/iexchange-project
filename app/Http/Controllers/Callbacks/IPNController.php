<?php


namespace App\Http\Controllers\Callbacks;


use App\Common\Packages\Payment\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Controllers\AbstractController;
use App\Models\HistoryPaymentTransaction;
use App\Models\LogMerchant;
use App\Models\MerchantTransactionHash;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class IPNController extends AbstractController
{

    /**
     *  Обработка платежа (Callback)
     *
     * @param Request $request
     * @param string $payment_system
     * @param string|null $security_hash
     * @throws \Exception
     */
    public function status(Request $request, string $payment_system, string $security_hash = null)
    {
        // Получаем форматированную наименование платежной системы
        $payment_system = security_xss($payment_system);

        // Получаем ID адрес сервера платежной системы
        $ip_address = $request->ip();

        // Получаем ID заявки
        $column_order = Config::get('payment.callback_columns.'.$payment_system);
        $order_id = $request->has($column_order) ? (int)$request->get($column_order) : 0;

        // Данные по заявке
        $order = Task::whereId($order_id)->whereStatus(3)->first();

        // Проверяем есть ли информация по заявке
        if(empty($order)) {
            return;
        }

        // Данные о полученном платеже
        $driver = PaymentFacade::merchant($payment_system, $order->id_merchant);

        try {
            $response = $driver->completePurchase($request->all())
                ->setOrderData($order)
                ->send();

            $order->lead_time = Carbon::now()->addMinutes((int)iEXSetting('lead_time_task'));
            $order->started_at = Carbon::now()->toDateTimeString();
            $order->save();

            // Получение всей информации
            $transaction = TransactionFacade::init($order);

            // Получаем информацию о мерчанте
            $merchant = $transaction->getMerchant();
            if(is_null($merchant)) {
                throw new \Exception('Мерчант не определен');
            }

            // Alias мерчанта
            $alias = Str::lower($merchant->gateway->alias);

            // Проверяем провайдера в логах мерчантов
            $merchant_log_count = LogMerchant::whereIdOrder($order->id)->where('provider','=', $payment_system)->count();

            // Заморозка заявки, в случае если указанный hash не соответствует введенной
            if(!empty($merchant->security_hash) and $merchant->security_hash != $security_hash)
            {
                $transaction
                    ->addEventMerchantLog('security_hash', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // Проверка ID Range
            if(!empty($merchant->allow_ip_address) and !$transaction->isMerchantValidIpRange($ip_address))
            {
                $transaction
                    ->addEventMerchantLog('allow_ip_address', $payment_system, $ip_address)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            if($merchant->is_merchant_log == 1 and $merchant_log_count == 0)
            {
                $transaction
                    ->addEventMerchantLog('merchant_log_checked', $alias)
                    ->failedBanMessage('merchant:scam')
                    ->setCategoryReject(4)
                    ->failed(['allow_user_ban' => true]);
                return;
            }

            // Дальше не пускаем, если оплачено с другого провайдера
            if(Str::lower($payment_system) != $alias) {
                $transaction
                    ->addEventMerchantLog('provider_alias', $payment_system)
                    ->failedBanMessage('merchant:scam')
                    ->setCategoryReject(4)
                    ->failed(['allow_user_ban' => true]);
                return;
            }

            $network_code = $transaction->getCurrencyIn()->network_code;
            $response_currency = $transaction->getCodeIn()->name;
            if(!empty($network_code)) {
                $response_currency = $network_code;
            }

            // Дальше не пропускаем если оплачено с другого кода валюты
            if(\Str::upper($response->getCurrency()) != \Str::upper($response_currency))
            {
                $transaction->addEventMerchantLog('check_currency', $payment_system)
                    ->failedBanMessage('merchant:scam')
                    ->setCategoryReject(4)
                    ->failed(['allow_user_ban' => true]);
                return;
            }

            // Переводим заявку в ручной режим,
            // если номер счета не соотвествует тому что указано в заявке
            if($merchant->is_check_from_shot == 1 and Str::upper($response->getFromAccount()) != Str::upper($transaction->getFromShot()))
            {
                $transaction->addEventMerchantLog('verify_wallet', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // Если номер счета находится в черном списке, отложить создание заявок
            if(is_local_black_list($transaction->getItem()) == true)
            {
                $transaction->addEventMerchantLog('check_local_black_list', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // В случае успешной платы
            if($response->isPending() or $response->isSuccessful())
            {
                if($response->isPending())
                {
                    // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
                    // записываем в событие и регистрируем транзакцию
                    if($alias == 'alfabitpay') {
                        if($order->register_tx == 0) {
                            $order->update(['register_tx' => 1]);
                        }
                    } else {
                        if($order->register_tx == 0 and method_exists($response, 'getBlockchainHash')) {
                            $transaction->addEventRegisterTransaction($response->getBlockchainHash(), $response->getBlockchainConfirm(), $response->getAmount(), $alias);
                            $order->update(['register_tx' => 1]);
                        }
                    }

                    $tx_hash = method_exists($response, 'getBlockchainHash') ? $response->getBlockchainHash() : null;

                    // Переводим заявку в режим ожидания
                    $order->task_info->update([
                        'is_pending' => 1,
                        'id_transaction_merchant' => $tx_hash
                    ]);

                    // Также записываем в отдельную базу
                    if(!empty($order->task_info->id_transaction_merchant))
                    {
                        MerchantTransactionHash::updateOrCreate([
                            'id_task' => $order->id
                        ], [
                            'provider' => $alias,
                            'id_currency' => $order->direction_exchange->id_currency1,
                            'id_task' => $order->id,
                            'transaction_hash' => $tx_hash
                        ]);
                    }

                } elseif($response->isSuccessful()) {
                    // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
                    // записываем в событие и регистрируем транзакцию
                    if($alias == 'alfabitpay') {
                        if($order->register_tx == 0) {
                            $order->update(['register_tx' => 1]);
                        }
                    } else {
                        if($order->register_tx == 0 and method_exists($response, 'getBlockchainHash')) {
                            $transaction->addEventRegisterTransaction($response->getBlockchainHash(), $response->getBlockchainConfirm(), $response->getAmount(), $alias);
                            $order->update(['register_tx' => 1]);
                        }
                    }

                    $tx_hash = method_exists($response, 'getBlockchainHash') ? $response->getBlockchainHash() : null;

                    // Переводим заявку в режим ожидания
                    $order->task_info->update([
                        'is_pending' => 0,
                        'id_transaction_merchant' => $tx_hash
                    ]);


                    // Также записываем в отдельную базу
                    if(!empty($order->task_info->id_transaction_merchant))
                    {
                        MerchantTransactionHash::updateOrCreate([
                            'id_task' => $order->id
                        ], [
                            'provider' => $alias,
                            'id_currency' => $order->direction_exchange->id_currency1,
                            'id_task' => $order->id,
                            'transaction_hash' => $tx_hash
                        ]);
                    }


                } elseif($response->isCancelled()) {
                    $order->task_info->update(['is_pending' => 0]);
                    $transaction->failed();
                }

                HistoryPaymentTransaction::firstOrCreate([
                    'id_task'   =>  $response->getTransactionId(),
                    'payment'   =>  $order->merchant_provider,
                    'transfer'  =>  $response->getTransferId()
                ]);

            }
        }catch (\Exception $e) {
            \Log::error('IPController error: '. $e->getMessage());
        }
    }

}
