<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 30.07.2020
 * Time: 20:05
 */

namespace App\Http\Controllers\Callbacks;


use App\Common\Packages\MultiGateway\MultiGatewayFacade;
use App\Common\Packages\Payment\PaymentFacade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PrivateVerifyController extends Controller
{
    /**
     * Ссылка на регистрацию ключа
     * @param string $account
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function yandexLink(string $account)
    {
        $gateway = PaymentFacade::pay('yoomoney', $account)->api();
        \Cache::set('yoomoney-callback', $account);
        $url = $gateway->buildObtainTokenUrl();
        return redirect()->to($url);
    }

    /**
     *
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public function yandexCallback(Request $request)
    {
        $account = \Cache::get('yoomoney-callback');
        $gateway = PaymentFacade::pay('yoomoney', $account)->api();
        $response = $gateway->getAccessToken($request->get('code'));
        if(isset($response['access_token']) and !empty($response['access_token']))
        {
            // Получаем параметры по умолчанию
            $defaultParameters = \Config::get('gateways.default_parameters.yoomoney.pay');
            $mergeParams = array_merge($defaultParameters, [
                'access_token' => $response['access_token']
            ]);

            $filename = storage_path('/gateways/pay/yoomoney/id'.$account).'.php';
            if(File::isFile($filename)) {
                File::put($filename, encrypt_protect_gateway_keys($mergeParams));
            }

            echo '<div style="color: green; text-align: center;width: 100%; margin: 0 auto;">Ключ успешно записан</div>';
        } else {
            echo '<div style="color: red; text-align: center;width: 100%; margin: 0 auto;">Доступ запрещен</div>';
        }
    }


    public function qiwiLink(string $account, Request $request)
    {
        $type = $request->has('type') ? $request->get('type') : 'qiwi';

        $gateway = PaymentFacade::driver($type)->getFactory($account);

        try {
            $register = $gateway->registerHook();
            if(isset($register['hookId']))
            {
                $hookId = $register['hookId'];
                $secret_key = $gateway->getSecretKey($hookId);

                if(isset($secret_key['key']))
                {
                    MultiGatewayFacade::driver($type)->update([
                        'account' => $account,
                        'hook_id' => $hookId,
                        'webhook_key' => $secret_key['key']
                    ]);

                    echo '<div style="color: green; text-align: center;width: 100%; margin: 0 auto;">Ключ успешно записан</div>';
                }else {
                    echo '<div style="color: red; text-align: center;width: 100%; margin: 0 auto;">Доступ запрещен</div>';
                }
            }else {
                echo '<div style="color: red; text-align: center;width: 100%; margin: 0 auto;">Доступ запрещен</div>';
            }

        }catch (\Exception $exception) {

            if($exception->getCode() == '422') {
                $statusHook = $gateway->statusHook();
                if(isset($statusHook['hookId'])) {
                    $gateway->deleteHook($statusHook['hookId']);
                }
            } else {
                die($exception->getMessage());
            }
        }

    }
}
