<?php

namespace App\Http\Controllers\Callbacks;

use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Controllers\Controller;
use App\Jobs\AdminNewOrderJob;
use App\Jobs\NewOrderJob;
use App\Jobs\TelegramOrderJob;
use App\Models\InternalAccount;
use App\Models\LogAutoPayment;
use App\Models\LogMerchant;
use App\Models\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class InternalAccountController extends Controller
{
    /**
     * Проверка внутреннего счета
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function status(Request $request, string $security_hash = null)
    {
        if(config('payment.enable_internal_account') == false) {
            return redirect()->to('/');
        }

        // Получаем форматированную наименование платежной системы
        $payment_system = 'internalaccount';

        $validator = Validator::make($request->route()->parameters(), []);

        //  Если в валидаторе нет ошибок, продолжаем обработку статусного файла
        if(!$validator->fails())
        {
            // Получаем форматированную наименование платежной системы
            $gateway_name = $payment_system;
            $order = Task::where('public_id', $request->get('hash_id'))->whereStatus(9);

            // Если найдена необработанная заявка по ID
            if($order->exists())
            {
                $item = $order->first();

                $item->lead_time = Carbon::now()->addMinutes((int)iEXSetting('lead_time_task'));
                $item->started_at = Carbon::now()->toDateTimeString();
                $item->save();

                //Получаем транзакционные данные
                $transaction = TransactionFacade::init($item);

                // Получение баланс
                $account = InternalAccount::where([
                    ['id_user', '=', auth()->id()],
                    ['id_code_currency', '=', $transaction->getCodeIn()->id]
                ])->first();

                if(isset($account) and $account->balance >= $transaction->getAmountIn())
                {
                    // Получение оплаченной суммы
                    $balance = (float)$transaction->getAmountIn();
                    $account->update([
                        'balance' => $account->balance - $transaction->getAmountIn()
                    ]);

                    // Получаем информацию о мерчанте
                    $merchant = $transaction->getMerchant();
                    if(!is_null($merchant) and $merchant->gateway->alias == 'internalaccount')
                    {
                        $alias = $merchant->gateway->alias;

                        // Заморозка заявки, в случае если указанный hash не соответствует введенной
                        if (!empty($merchant->security_hash) and $merchant->security_hash != $request->route('security_hash')) {
                            iex_order_merchant_log($item->id, 1, 2, 'Секретный хэш мерчанта не соответствует указанной', $gateway_name);
                            $transaction->setDeferType(0);
                            $transaction->defer();
                            return;
                        }

                        // Проверяем провайдера в логах мерчантов
                        $merchant_log = LogMerchant::whereIdOrder($item->id)->get()
                            ->filter(function ($item) use ($gateway_name) {
                                return $item->provider == $gateway_name;
                            });

                        if ($merchant->is_disable_log == 0 and $merchant_log->count() == 0) {
                            iex_order_merchant_log($item->id, 1, 4, 'Внезапно изменился провайдер мерчанта', $gateway_name);
                            $transaction->failedBanMessage('merchant:scam');
                            $transaction->setCategoryReject(4);
                            $transaction->failed();

                            // Если данные не подходят, отправляем в бан
                            add_user_ban($transaction->getClient()->id, null, 'Нарушение правилы №1');
                            return;
                        }

                        // Дальше не пускаем, если оплачено с другого провайдера
                        if (mb_strtolower($gateway_name) != mb_strtolower($alias)) {
                            iex_order_merchant_log($item->id, 1, 4, 'Оплачено с другого провайдера, актуальный ' . $alias . '.', $gateway_name);
                            $transaction->failedBanMessage('merchant:scam');
                            $transaction->setCategoryReject(4);
                            $transaction->failed();

                            // Если данные не подходят, отправляем в бан
                            add_user_ban($transaction->getClient()->id, null, 'Нарушение правил №2');
                            return;
                        }

                        // Дальше не пропускаем если оплачено с другого кода валюты
                        if (mb_strtoupper($account->code_currency->name) != mb_strtoupper($transaction->getCodeIn()->name)) {
                            iex_order_merchant_log($item->id, 1, 4, 'Оплачено другим кодом валюты', $gateway_name);
                            $transaction->setCategoryReject(4);
                            $transaction->failed();
                            return;
                        }

                        // Основное условие: Проверяем если клиент не успел оплатить заявку в течении,
                        // установленного времени пересчитываем заявку.
                        if (iEXSetting('is_recount_to_merchant')) {
                            $expired_at = Carbon::parse($item->created_at)->addSeconds(iEXSetting('max_time_task'));
                            if (Carbon::now() > $expired_at) {
                                $transaction->recount(1, $item->give_price);
                                // Отправить уведомление в случае пересчета заявки
                                if (iEXSetting('is_recount_to_merchant_notify')) {
                                    $transaction->notifyRecount();
                                }
                            }
                        }

                        $transaction->totalChangeReserve();
                        iex_order_status_log($item, 7, 2);

                        // Если определен как мошенник, дальше не пускаем
                        if ($item->task_info->is_freeze_scam == 1) {
                            $transaction->setDeferType(5);
                            $transaction->defer();
                            return;
                        }

                        // В случае переплаты
                        if ($balance > (float)$item->give_price) {
                            $item->update(['merchant_overpayment' => 1]);
                        }

                        $item->update([
                            'status' => 7,
                            'lead_time' => Carbon::now()->addMinutes(iEXSetting('lead_time_task')),
                            'started_at' => Carbon::now()->toDateTimeString()
                        ]);

                        // Уведомляем оператора о новой заявки
                        // Уведомляем оператора о новой заявки
                        if (config('crypto.order_mail')) {
                            // Уведомляем оператора о новой заявки
                            dispatch(new AdminNewOrderJob($item))->delay(
                                now()->addSeconds(30)
                            )->onQueue('low');
                        }

                        // Отправляем клиенту уведомление об успешной оплате
                        if (iEXSetting('notify_change_status') == 1) {
                            $delay = now()->addSeconds(20);
                            dispatch(new NewOrderJob($item))->delay($delay)
                                ->onQueue('high');
                        }

                        // Уведомлять о новых заявках в Telegram (Для операторов)
                        if ((int)iEXSetting('enable_tg_notify_operator') == 1) {
                            $delay = now()->addSeconds(30);
                            dispatch(new TelegramOrderJob($item))->delay($delay)->onQueue('low');
                        }
                    }
                    return redirect()->to('/');
                }

                return 'Недостаточно средств на счету';
            }
        }

        return redirect()->to('/');
    }
}
