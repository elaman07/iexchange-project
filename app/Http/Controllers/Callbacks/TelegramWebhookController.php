<?php

namespace App\Http\Controllers\Callbacks;

use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Telegram\Facades\{AdminUser, TelegramAccountFacade, User};
use Illuminate\Support\Facades\{Lang, Log, View};
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramWebhookController extends Controller
{
    /**
     * Webhook для клиентского бота
     *
     */
    public function user_webhook()
    {
        try {
            $update = Telegram::bot('iex')->getWebhookUpdate();

            $message = $update->getMessage();
            if (is_null($message) or ! $message->has('text')) {
                return false;
            }

            Log::channel('telegram-bot')->info(implode(':', [$message->getChat()->getId(), $message->getChat()->getUsername(), $message->getText()]));

            // Init user
            TelegramAccountFacade::init('account-panel-'.$message->getChat()->getId());
            User::init($message->getChat()->getId(), $message->getFrom()->getLanguageCode());
            $user = User::getData();
            Lang::setLocale($user['locale']);


            $type_banned = $message->getChat()->getId();
            if(iEXSetting('telegram_bot_banned_type') == 'username') {
                $type_banned = $message->getChat()->getUsername();
            }

            if(!empty(iEXSetting('telegram_bot_banned_ids')))
            {
                if (in_array($type_banned, explode(',', iEXSetting('telegram_bot_banned_ids')))) {
                    if (!$user['banned']) {
                        $user = User::banUser();
                    }
                } else {
                    if ($user['banned']) {
                        $user = User::unbanUser();
                    }
                }
            }

            if ($user['banned']) {
                return Telegram::bot('iex')->sendMessage([
                    'chat_id' => $message->getChat()->getId(),
                    'text' => __('telegram-bot.messages.error_access_denied')
                ]);
            }

            // Если обменник отключен, отключаем бота
            if(isJobOffline()) {
                return Telegram::bot('iex')->sendMessage([
                    'chat_id' => $message->getChat()->getId(),
                    'parse_mode' => 'HTML',
                    'text' => \view('_parent.bot.offline')->render()
                ]);
            }

            // Execute command
            $commands = Telegram::bot('iex')->getCommands();
            $command = array_search($message->getText(), __('telegram-bot.start_commands')) ?: substr($message->getText(), 1);

            if (isset($commands[$command])) {
                User::forgetConversation();
                $user = User::setConversationCommand($command);
            }

            Telegram::bot('iex')->triggerCommand(is_null($user['conversation']['command']) ? 'help' : $user['conversation']['command'], $update);

        }catch (\Exception $e) {
            Log::channel('telegram-bot')->error('Bot Exception: ' . $e);
            return true;
        }
    }

    /**
     * Webhook для админ бота
     *
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function admin_webhook()
    {
        $update = \Telegram::bot('admin')->getWebhookUpdate();
        $message = $update->getMessage();
        if (is_null($message) or ! $message->has('text')) {
            return false;
        }


        $admin_ids = explode(',', iEXSetting('telegram_chat_id'));
        if (!in_array($message->getChat()->getId(), $admin_ids))
        {
            return Telegram::bot('admin')->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __('telegram-bot.messages.error_access_denied')
            ]);
        }

        if ($update->isType('callback_query'))
        {
            $query = $update->getCallbackQuery();
            $data  = $query->getData();
            $chat_id = $query->getFrom()->getId();

            // Разделитель
            $callback_explode = explode('_admindata_', $data);

            switch ($callback_explode[0])
            {
                case 'order-id':
                    //Получаем подробную информацию о заявке
                    $order = Task::find((int)$callback_explode[1]);

                    if($order->scans > 0)
                    {
                        return Telegram::bot('admin')->editMessageText([
                            'chat_id'           => $chat_id,
                            'text'              => '⚠️С заявкой №'.$order->id.' уже работает оператор '.$order->operator->name,
                            'parse_mode'        => 'HTML',
                            'message_id'        =>  $query->getMessage()->getMessageId()
                        ]);
                    }

                    //Получаем информацию о заявке
                    $html = View::make('telegram.message', ['detail' => $order]);

                    $keyboard = Keyboard::make()
                        ->inline()
                        ->row(
                            Keyboard::inlineButton(['text' => '✅ Заявка исполнено', 'callback_data' => 'completed-order_admindata_'.$order->id]),
                            Keyboard::inlineButton(['text' => '❌ Отклонить заявку', 'callback_data' => 'cancel-order_admindata_'.$order->id])
                        );

                    $data11 = [
                        'reply_markup'  => $keyboard,
                        'chat_id'           => $chat_id,
                        'text'              => $html->render(),
                        'parse_mode'        => 'HTML',
                        'message_id'        =>  $query->getMessage()->getMessageId()
                    ];

                    return Telegram::bot('admin')->editMessageText($data11);
                    break;

                case 'completed-order':
                    $transaction = TransactionFacade::call($callback_explode[1]);

                    $message = '<b>Статус заявки:</b> '.$transaction->getStatusName();
                    if($transaction->getStatus() == 3) {
                        $transaction->success();
                        $message = '✅ Заявка '.$callback_explode[1].' успешно исполнено';
                    }
                    $data11 = [
                        'chat_id'           => $chat_id,
                        'text'              =>  $message,
                        'parse_mode'        => 'HTML',
                        'message_id'        =>  $query->getMessage()->getMessageId()
                    ];

                    return Telegram::bot('admin')->editMessageText($data11);

                    break;

                case 'cancel-order':
                    $transaction = TransactionFacade::call($callback_explode[1]);
                    $message = '<b>Статус заявки:</b> '.$transaction->getStatusName();
                    if($transaction->getStatus() == 3) {
                        $transaction->failed();
                        $message = '❌ Заявка '.$callback_explode[1].' успешно отклонена';
                    }


                    $data11 = [
                        'chat_id'           => $chat_id,
                        'text'              =>  $message,
                        'parse_mode'        => 'HTML',
                        'message_id'        =>  $query->getMessage()->getMessageId()
                    ];
                    return Telegram::bot('admin')->editMessageText($data11);
                    break;
            }
        }

        AdminUser::init('admin-user-'.$message->getChat()->getId(), $message->getFrom()->getLanguageCode());
        $user = AdminUser::getData();
        Lang::setLocale($user['locale']);


        // Execute command
        $commands = Telegram::bot('admin')->getCommands();
        $command = array_search($message->getText(), __('telegram-admin.start_commands')) ?: substr($message->getText(), 1);

        if (isset($commands[$command])) {
            AdminUser::forgetConversation();
            $user = AdminUser::setConversationCommand($command);
        }

        Telegram::bot('admin')->triggerCommand(is_null($user['conversation']['command']) ? 'help' : $user['conversation']['command'], $update);
    }
}
