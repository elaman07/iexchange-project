<?php


namespace App\Http\Controllers\Callbacks;


use App\Common\Packages\Payment\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Common\Packages\Transaction\Transaction;
use App\Http\Controllers\AbstractController;
use App\Jobs\AdminNewOrderJob;
use App\Jobs\NewOrderJob;
use App\Jobs\TelegramOrderJob;
use App\Models\HistoryPaymentTransaction;
use App\Models\LogAutoPayment;
use App\Models\LogMerchant;
use App\Models\MerchantTransactionId;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class ReceiveMoneyController extends AbstractController
{
    /**
     * @return TransactionFacade
    */
    protected $transaction;

    /**
     *  Обработка платежа (Callback)
     *
     * @param Request $request
     * @param string $payment_system
     * @param string|null $security_hash
     * @throws \Exception
     * @throws \Throwable
     */
    public function status(Request $request, string $payment_system, string $security_hash = null)
    {
        // Получаем форматированную наименование платежной системы
        $payment_system = security_xss($payment_system);

        // Получаем ID адрес сервера платежной системы
        $ip_address = $request->ip();

        // Получаем ID заявки
        $column_order = Config::get('payment.callback_columns.'.$payment_system);
        $order_id = $request->has($column_order) ? (int)$request->get($column_order) : 0;
        $is_merchant_transaction = false;

        if(Str::lower($payment_system) == 'kunapay') {
            $order_id = $request->has('data') ? $request->get('data')['attributes']['reference_id'] : 0;
        }

        if(Str::lower($payment_system) == 'adgroup')
        {
            if($request->has('responseData') and !empty(Arr::first($request->get('responseData')['transactions']))) {
                $tx_arr = Arr::first($request->get('responseData')['transactions']);
                $order_id = MerchantTransactionId::where('transaction_id', '=', $tx_arr['_id'])->first()->id_task;
            }
        }

        // Данные по заявке
        $order = Task::whereId($order_id)->whereIn('status', [3, 9])->first();


        // Проверяем есть ли информация по заявке
        if(empty($order)) {
            if(\Str::lower($payment_system) == 'firekassa') {
                return response('OK', 200)
                    ->header('Content-Type', 'text/html');
            } else {
                return;
            }
        }

        // Данные о полученном платеже
        $driver = PaymentFacade::merchant($payment_system, $order->id_merchant);

        try {

            // Получение результатов
            $response = $driver->completePurchase($request->all())->setOrderData($order)->send();

            $order->lead_time = Carbon::now()->addMinutes((int)iEXSetting('lead_time_task'));
            $order->started_at = Carbon::now()->toDateTimeString();
            $order->save();

            // Получение всей информации
            $transaction = TransactionFacade::init($order);

            // Получаем информацию о мерчанте
            $merchant = $transaction->getMerchant();
            if(is_null($merchant)) {
                throw new \Exception('Мерчант не определен');
            }

            // Alias мерчанта
            $alias = Str::lower($merchant->gateway->alias);

            // Проверяем провайдера в логах мерчантов
            $merchant_log_count = LogMerchant::whereIdOrder($order->id)->where('provider','=', $payment_system)->count();

            // Заморозка заявки, в случае если указанный hash не соответствует введенной
            if(!empty($merchant->security_hash) and $merchant->security_hash != $security_hash)
            {
                $transaction
                    ->addEventMerchantLog('security_hash', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // Проверка ID Range
            if(!empty($merchant->allow_ip_address) and !$transaction->isMerchantValidIpRange($ip_address))
            {
                $transaction
                    ->addEventMerchantLog('allow_ip_address', $payment_system, $ip_address)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            if($merchant->is_merchant_log == 1 and $merchant_log_count == 0)
            {
                $transaction
                    ->addEventMerchantLog('merchant_log_checked', $alias)
                    ->failedBanMessage('merchant:scam')
                    ->setCategoryReject(4)
                    ->failed(['allow_user_ban' => true]);
                return;
            }

            // Дальше не пускаем, если оплачено с другого провайдера
            if(Str::lower($payment_system) != $alias) {
                $transaction
                    ->addEventMerchantLog('provider_alias', $payment_system)
                    ->failedBanMessage('merchant:scam')
                    ->setCategoryReject(4)
                    ->failed(['allow_user_ban' => true]);
                return;
            }

            // Дальше не пропускаем если оплачено с другого кода валюты
            if(Str::upper($response->getCurrency()) != Str::upper($transaction->getCodeIn()->name))
            {
                $transaction->addEventMerchantLog('check_currency', $payment_system)
                    ->failedBanMessage('merchant:scam')
                    ->setCategoryReject(4)
                    ->failed(['allow_user_ban' => true]);
                return;
            }

            // Переводим заявку в ручной режим,
            // если номер счета не соотвествует тому что указано в заявке
            if($merchant->is_check_from_shot == 1 and Str::upper($response->getFromAccount()) != Str::upper($transaction->getFromShot()))
            {
                $transaction->addEventMerchantLog('verify_wallet', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // Если номер счета находится в черном списке, отложить создание заявок
            if(is_local_black_list($transaction->getItem()) == true)
            {
                $transaction->addEventMerchantLog('check_local_black_list', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // Проверяем, существует ли транзакция на сервере Платежной системы
            if($merchant->is_check_api == 1 and $transaction->checkedMerchantOrder() == 0) {

                $transaction->addEventMerchantLog('is_disable_payment_server', $payment_system)
                    ->setDeferType(0)
                    ->defer();
                return;
            }

            // Если транзакция в ожидании подтверждения
            if($response->isPending())
            {
                // Переводим заявку в режим ожидания
                $order->task_info->update(['is_pending' => 1]);
            }
            // Если транзакция успешно выполнена
            elseif($response->isSuccessful())
            {
                $order->task_info->update(['is_pending' => 0]); // Убираем из режима, в ожидание

                // Сумма, которая должна поступить на счет
                $credit_amount = $order->give_price;
                if($merchant->credit_amount == 1) {
                    $credit_amount = $order->give_price_with_comm_pay;
                } elseif($merchant->credit_amount == 2) {
                    $credit_amount = $order->give_price_default;
                }

                // Сумма которую отправил мерчант
                $balance = (float)$response->getAmount();


                // Фиксируем сумму, которую отправил клиент
                if(!empty($balance) and empty($order->in_amount_merchant))
                {
                    $order->update([
                        'in_amount_merchant' => $balance
                    ]);
                }

                HistoryPaymentTransaction::create([
                    'id_task'   =>  $response->getTransactionId(),
                    'payment'   =>  $order->merchant_provider,
                    'transfer'  =>  $response->getTransferId()
                ]);

                // Основное условие: Проверяем если клиент не успел оплатить заявку в течении,
                // установленного времени пересчитываем заявку.
                if(iEXSetting('is_recount_to_merchant'))
                {
                    $expired_at = Carbon::parse($order->created_at)->addSeconds(iEXSetting('max_time_task'));
                    if(Carbon::now() > $expired_at)
                    {
                        $transaction->recount(1, $order->give_price_with_comm);
                        // Отправить уведомление в случае пересчета заявки
                        if(iEXSetting('is_recount_to_merchant_notify')) {
                            $transaction->notifyRecount();
                        }
                    }
                }

                $transaction->totalChangeReserve();
                iex_order_status_log($order, 7,2);

                // Если определен как мошенник, дальше не пускаем
                if($order->task_info->is_freeze_scam == 1) {
                    $transaction->setDeferType(5)
                        ->defer();
                    return;
                }

                // В случае переплаты
                if($balance > (float)$credit_amount) {
                    $order->update(['merchant_overpayment' => 1]);
                }

                // Получаем погрешность
                $amount_fault = $credit_amount - (!empty($merchant->amount_fault) ? $merchant->amount_fault : 0);

                // В случае неполной оплаты (merchant_incomplete_payment)
                if($balance < (float)$amount_fault)
                {
                    $order->update([
                        'is_bot' => 0,
                        'merchant_incomplete_payment' => 1,
                        'merchant_overpayment' => 2
                    ]);

                    $transaction->addEventMerchantLog('verify_income_balance', $payment_system, null, [
                        'amount'    => $balance,
                        'currency'  => $response->getCurrency(),
                        'id'        => $response->getTransferId()
                    ]);

                    if (iEXSetting('is_disabled_order_merchant') == 1) {
                        $transaction->failedBanMessage('merchant:scam')
                            ->setCategoryReject(4)
                            ->failed();
                    }

                    return;
                }

                $order->update([
                    'status'        =>  7,
                    'lead_time'     =>  Carbon::now()->addMinutes(iEXSetting('lead_time_task')),
                    'started_at'    =>  Carbon::now()->toDateTimeString()
                ]);

                $transaction->addEventMerchantLog('income_balance_done', $payment_system, null, [
                    'amount'    => $balance,
                    'currency'  => $response->getCurrency(),
                    'id'        => $response->getTransferId()
                ]);

                // Автовыплата
                if(iEXSetting('is_enabled_autopayment') == 1)
                {
                    try {
                        $transaction->paymentRelatedInstanceFor();
                    } catch (\Exception $e) {
                        $transaction->disableIsBot();
                        LogAutoPayment::create([
                            'id_task' => $order->id,
                            'status' => 1,
                            'event_value' => $e->getMessage()
                        ]);
                    }
                }

                // Уведомляем оператора о новой заявки
                if(config('crypto.order_mail')) {
                    // Уведомляем оператора о новой заявки
                    dispatch(new AdminNewOrderJob($order))->delay(
                        now()->addSeconds(30)
                    )->onQueue('low');
                }

                // Отправляем клиенту уведомление об успешной оплате
                if(iEXSetting('notify_change_status') == 1) {
                    $delay = now()->addSeconds(20);
                    dispatch(new NewOrderJob($order))->delay($delay)
                        ->onQueue('high');
                }

                // Уведомлять о новых заявках в Telegram (Для операторов)
                if((int)iEXSetting('enable_tg_notify_operator') == 1) {
                    $delay = now()->addSeconds(30);
                    dispatch(new TelegramOrderJob($order))->delay($delay)->onQueue('low');
                }

                // Записываем в мерчант
                //$total_usd = (is_null($merchant->total_usd) ? 0 : $merchant->total_usd) + convert_to_usd($transaction->getCodeIn()->name, $response->getAmount());
                $merchant->update([
                    //'total_usd' => iex_number_format($total_usd),
                    'last_order_id' => $response->getTransferId()
                ]);

            }
            // Если транзакция отменена
            elseif($response->isCancelled()) {
                $order->task_info->update(['is_pending' => 0]); // Убираем из режима, в ожидание

            }
        }catch (\Exception $e) {
            \Log::error('Merchant Error: '.$e->getMessage());
        }

    }


    protected function messages(Transaction $transaction, Task $task, $name)
    {

    }
}
