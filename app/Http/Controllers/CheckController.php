<?php
namespace App\Http\Controllers;


use App\Common\Packages\Payment\Facades\PaymentFacade;
use App\Models\Task;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\LinksReview;
use Illuminate\Support\Carbon;

class CheckController extends Controller
{
    /**
     * Получение статуса транзакции
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handler(Request $request)
    {
        if(is_numeric($request->id)) {
            $transaction = Task::where('public_id', security_xss($request->id))->first();

            // Если не найдена транзакция
            if(!$transaction)
                return abort(404);
        } else {
            $tx = Transaction::where('transaction', (string)$request->id);
            // Если не найдена транзакция
            if(!$tx->exists())
                return abort(404);

            $transaction = $tx->first()->tasks;
        }


        if(iEXSetting('disable_check_display') == 1 and $request->ip() != $transaction->ip) {
            abort(403);
        }

        $start = Carbon::parse($transaction->started_at);
        $lead_time = Carbon::parse($transaction->updated_at);
        $getSecond = $start->diffForHumans($lead_time, true);

        $links = LinksReview::orderBy('sorting')->get();
        return view('check', [
            'links'     =>  $links,
            'detail'    =>  $transaction,
            'leadTime'  =>  $getSecond
        ]);
    }
}
