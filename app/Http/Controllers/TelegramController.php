<?php
namespace App\Http\Controllers;

use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class TelegramController extends Controller
{
    /***
     * Полный путь к командным файлам
     *
     * @return array
    */
    protected $commands_path = [

    ];

    /***
     * Параметры для соединения с базой данных
     *
     * @return array
    */
    protected $mysql_credentials = [];

    /***
     * Определите URL-адрес для файла "hook"
     *
     * @return string
    */
    protected $hook_url = null;

    /***
     * Экземпляр базового класса Telegram
     *
     * @return \Longman\TelegramBot\Telegram
    */
    protected $telegram = [];

    /***
     * Конструктор
     *
     * @throws \Longman\TelegramBot\Exception\TelegramException
     * @return mixed
     */
    public function __construct()
    {
        //Определите все пути для ваших пользовательских команд в этом массиве
        // (оставьте в качестве пустого массива, если они не используются)
        $this->commands_path = base_path('app/Console/Telegram');

        // Введите свои учетные данные базы данных MySQL
        $this->mysql_credentials = [
            'host'     => config('telegram.connections.host'),
            'user'     => config('telegram.connections.username'),
            'password' => config('telegram.connections.password'),
            'database' => config('telegram.connections.database'),
        ];

        $bot_username = config('telegram.bots.iex.username');

        // Logging (Error, Debug and Raw Updates)
        TelegramLog::initialize(
            new Logger('telegram_bot', [
                (new StreamHandler(storage_path($bot_username.'_debug.log'), Logger::DEBUG))->setFormatter(new LineFormatter(null, null, true)),
                (new StreamHandler(storage_path($bot_username.'_error.log'), Logger::ERROR))->setFormatter(new LineFormatter(null, null, true)),
            ]),
            new Logger('telegram_bot_updates', [
                (new StreamHandler(storage_path($bot_username.'_update.log'), Logger::INFO))->setFormatter(new LineFormatter('%message%'. PHP_EOL))
            ])
        );

        //url адрес hook
        $this->hook_url = url(config('telegram.prefix').'/bot/hook');
        $this->telegram = new Telegram(config('telegram.bots.iex.token'), $bot_username);

        //ID Администраторов
        $ids_admins = collect(iEXSetting('telegram_chat_id'))->map(function($item) {
            return (int)$item;
        })->filter()->all();
        $this->telegram->enableAdmins($ids_admins);

        $this->telegram->addCommandsPath($this->commands_path);
        $this->telegram->enableMySql($this->mysql_credentials);
        $this->telegram->enableLimiter();
    }

    /**
     * Этот файл конфигурации предназначен для запуска бота с помощью метода getUpdates
     *
     * # Незарегистрированные параметры должны быть заполнены
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function getUpdates()
    {
        $server_response = $this->telegram->handleGetUpdates();

        if ($server_response->isOk()) {
            $update_count = count($server_response->getResult());
            echo date('Y-m-d H:i:s', time()) . ' - Processed ' . $update_count . ' updates';
        } else {
            echo date('Y-m-d H:i:s', time()) . ' - Не удалось получить обновления' . PHP_EOL;
            echo $server_response->printError();
        }
    }

    /**
     * Этот файл конфигурации предназначен для запуска бота с помощью метода webhook.
     *   * Незарегистрированные параметры должны быть заполнены
     *   *
     *   * Обратите внимание, что если вы откроете этот файл в своем браузере, вы получите «Ввод пуст!». Исключение.
     *   * Это нормальное поведение, потому что этот адрес должен быть достигнут только серверами Telegram.
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function hook() {
        $this->telegram->handle();
    }

    /***
     * Этот файл предназначен для установки webhook.
     *
     *   * Незарегистрированные параметры должны быть заполнены
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function set()
    {
        // Установить webhook
        $result = $this->telegram->setWebhook($this->hook_url, [
            ['certificate' => storage_path('app/cert.cert')]
        ]);

        if ($result->isOk()) {
            echo $result->getDescription();
        }
    }

    /**
     * Этот файл предназначен для отключения hook.
     *
     *   * Незарегистрированные параметры должны быть заполнены
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function uninstall()
    {
        //Удалить webhook
        $result = $this->telegram->deleteWebhook();

        if ($result->isOk()) {
            echo $result->getDescription();
        }
    }

    public function getTelegram() {
        return $this->telegram;
    }
}
