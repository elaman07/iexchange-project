<?php
namespace App\Http\Controllers;
use App\Models\WithdrawalRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Контроллер для подтверждения различных событий
*/
class ConfirmController extends Controller
{
    /**
     * Подтверждение вывода средств
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmPayouts(Request $request)
    {
        $payouts = WithdrawalRequest::where('tx_id', $request->id);

        // Если не найдена транзакция
        if(!$payouts->exists())
            abort(404);

        $item = $payouts->first();
        if(is_null($item->verified_at)) {
            $payouts->update([
                'verified_at' => Carbon::now()->toDateTimeString()
            ]);
        }

        return view('confirm.payouts', [
            'payout' => $item
        ]);
    }
}