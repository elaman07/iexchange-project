<?php

namespace App\Http\Controllers;

use App\Common\Packages\Angular\AngularFacade;
use App\Models\Banned;
use App\Models\DirectionExchange;
use App\Models\EventReserve;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Показать панель приложения.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        return view('home', [
            'build_config' => AngularFacade::buildConfig()
        ]);
    }

    /**
     * Robots.txt
     *
     * @throws \Throwable
     */
    public function robots()
    {
        $robots = view('_parent.default_robots')->render();
        // output the entire robots.txt
        return \Response::make($robots, 200, [
            'content-type' => 'text/plain'
        ]);
    }

    /**
     * Страница где находятся забанненые клиены
     */
    public function banned()
    {
        if(auth()->user()->isBanned()) {
            return view('banned');
        } else {
            return redirect('/');
        }
    }

    /**
     * Страница где находятся забанненые клиены по IP
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function bannedIp(Request $request)
    {
        $banned = Banned::where('filter_name', $request->ip())
            ->where('expired_at', '>=', Carbon::now());

        if($banned->exists()) {
            return view('banned_ip', [
                'banned' => $banned->first()
            ]);
        };

        return redirect('/');
    }


    /**
     * Смена локализации сайта
     *
     * GET: /lang/{language}
     * HttpPost : AllowAnonymous
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function language(Request $request)
    {
        $lang =  $request->language;
        //Список доступных языков
        $language = collect(config('app.all_locale'))->keys()->toArray();

        if(\strlen($lang) > 0 and \strlen($lang) < 4) {
            if(in_array($lang, $language) == true)
            {
                //Теперь проверяем, если пользователя авторизован то
                //сохраняем локализацию в базе
                if(Auth::check() > 0) {
                    User::where('id', Auth::id())->update(['language' => $lang]);
                }
                $request->session()->put('language', $lang);
            }
        }

        \app()->setLocale($request->session()->get('language'));

        return redirect('/');
    }

    /**
     * Направлении для SEO
     * @param $in
     * @param $out
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function exchange($parent_url)
    {
        $find = DirectionExchange::where('parent_url', $parent_url);
        if($find->count() == 0) abort(404);



        return view('exchange_preview' , [
            'exchanger' => $find->first()
        ]);
    }
}
