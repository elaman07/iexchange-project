<?php

namespace App\Http\Controllers\Broadcasting;

use App\Events\OrderChatMessagesEvent;
use App\Http\Controllers\Controller;
use App\Jobs\TelegramNewChatJob;
use App\Models\Task;
use App\Models\TaskMessage;
use App\Notifications\TelegramNewChatNotification;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class OrderChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        if((int)iEXSetting('is_enable_order_online_chat') == 0) {
            return abort(404);
        }
    }

    /**
     * Список всех сообщений в чате
     *
     * @param Request $request
     * @return array
     */
    public function fetchMessages(Request $request): array
    {
        $order_id = $request->input('order_id');
        $task = Task::where([
            ['public_id', $order_id],
            ['id_user', auth()->id()]
        ])->first();

        return TaskMessage::where([
            ['id_task', $task->id],
            ['user_id', $task->id_user]
        ])->get()->map(function($item)
        {
            return [
                'message' => $item->message,
                'date' => Carbon::parse($item->created_at)->diffForHumans(),
                'type_user' => $item->type_user
            ];
        })->all();
    }

    /**
     * Добавление нового сообщения
     *
     * @param  Request $request
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();
        $order_id = $request->input('order_id');
        $task = Task::where([
            ['public_id', $order_id],
            ['id_user', $user->id]
        ])->first();

        if(!isset($task)) {
            return;
        }

        if($task->task_info->is_blocked_chat == 1)
        {
            return [
                'status' => 1,
                'message' => 'Error Message'
            ];
        }

        $message = $user->messages()->create([
            'id_task' => $task->id,
            'type_user' => 0,
            'message' => strip_tags(security_xss($request->input('message')))
        ]);

        broadcast(new OrderChatMessagesEvent($task->public_id, $task->id_user, [
            'message' => $message->message,
            'date' => Carbon::parse($message->created_at)->diffForHumans(),
            'type_user' => $message->type_user
        ]))->toOthers();

        // Включить уведомление операторам в Telegram
        if((int)iEXSetting('is_enabled_online_chat_tg_notify') == 1)
        {
            $delay = now()->addSeconds(5);
            dispatch(new TelegramNewChatJob($task, $message->message))->delay($delay)->onQueue('high');
        }

        return [
            'status' => 0
        ];
    }
}
