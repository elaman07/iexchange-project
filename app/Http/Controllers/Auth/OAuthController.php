<?php
namespace App\Http\Controllers\Auth;

use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Http\Controllers\Controller;
use App\Jobs\RegisterClientJob;
use App\Models\SocialAuthSystem;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class OAuthController extends Controller
{
    /**
     * Переадресация на авторизацию в соц. сети
     *
     * @param $provider
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider($provider)
    {
        $verify = $this->initialConfig($provider);
        if(isset($verify['status']) and $verify['status'] == 1)
            return \redirect()->to('/');

        try {
            // Отдельные права для Coinbase
            if($provider == 'coinbase') {
                $scopes = explode(' ', 'wallet:user:read wallet:user:email wallet:contacts:read wallet:accounts:read');
                return Socialite::driver($provider)->scopes($scopes)->redirect();
            }

            return Socialite::driver($provider)->redirect();
        }catch (\Exception $exception) {
            return Redirect::to('/');
        }
    }

    /**
     * Получаем информацию от сервиса
     *
     * @param $provider
     * @return User|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function callback($provider)
    {
        $this->initialConfig($provider);
        try {
            $response = Socialite::with($provider)->user();

            $authUser = $this->findOrCreateUser($response, $provider);

            if(isset($authUser['iex-error']) and $authUser['iex-error'] == true) {
                $find = SocialAuthSystem::where('alias', '=', $provider)->first();
                return view('auth.failed-callback', [
                    'status' => $authUser['type'],
                    'provider'  =>  $find
                ]);
            }

            //Если пользователь в процессе создания заявки не прошел авторизацию то авторизуем его
            $remember = (iEXSetting('is_remember_login') == 1 ? true : false);
            Auth::login($authUser, $remember);

            return redirect()->to('/');
        }catch (\Exception $exception) {
            return \Redirect::to('/');
        }
    }


    /**
     * Вернуть пользователя, если существует; создать и вернуть
     *
     * @param $response
     * @param $provider
     * @return User | array
     */
    private function findOrCreateUser($response, $provider)
    {
        $email = $this->getUserEmail($response, $provider);

        if(empty($email)) {
            return [
                'iex-error' => true,
                'type' => 'empty'
            ];
        }

        if($authUser = User::where([['email', $email], ['provider', $provider]])->first()) {
            return $authUser;
        }

        // Проверяем, найден ли e-mail
        if(User::where([['email', $email], ['provider', null]])->exists()) {
            return [
                'iex-error' => true,
                'type' => 'exists'
            ];
        }

        // Создание пароля в случае если клиента нет в базе
        $password = Str::random(10);
        $user = User::create([
            'name'              => (!empty($response->name) ? $response->name : 'NoSocialName'),
            'email'             =>  $email,
            'password'          =>  Hash::make($password),
            'provider'          =>  $provider,
            'provider_id'       =>  $response->id,
            'last_login'        =>  Carbon::now(),
            'last_activity'     =>  Carbon::now(),
        ]);

        //Регистрация реферала
        ReferralSystemFacade::register($user, null);
        //Регистрируем бонусную программу
        CashbackFacade::register($user->id);

        // Отправляем уведомление о регистрационных данных
        if(iEXSetting('is_email_auto_user')) {
            $delay = now()->addSeconds(5);
            dispatch(new RegisterClientJob($user, $password))->delay($delay)->onQueue('low');
        }
        // Отправляем уведомление на почту
        $user->sendEmailVerificationNotification();

        return $user;
    }

    /**
     * Получение E-mail
     *
     * @param $user
     * @param $provider
     * @return null
     */
    public static function getUserEmail($user, $provider)
    {
        if($provider == 'vkontakte') {
            return $user->accessTokenResponseBody['email'] ?? null;
       }
        return $user->email;
    }

    /**
     * Инциализации конфигурации выбранного провайдера
     *
     * @param $provider
     * @return array
     */
    private function initialConfig($provider)
    {
        // Если авторизован, не пускаем
        if(\auth()->check())
            return redirect('/');

        $auth = SocialAuthSystem::where([
            ['alias', '=', security_xss($provider)],
            ['status', '=', 1]
        ]);

        // Если ничего не найдено, переадресация на главную
        if(!$auth->exists())
            return ['status' => 1];

        $service = $auth->first();

        \Config::set("services.{$provider}", [
            'client_id' => $service->client_id,
            'client_secret' => $service->client_secret,
            'redirect' => url("/oauth/{$service->alias}/callback")
        ]);
    }
}
