<?php

namespace App\Http\Controllers\Auth;

use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Common\Support\Traits\RegistersUsers;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Куда перенаправлять пользователей после проверки.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $filterName = 'required|string|max:50';
        if(iEXSetting('is_filter_username'))
            $filterName .= '|regex:/[A-Za-zА-Яа-яЁё -]$/u';

        $unique = null;
        if(iEXSetting('is_user_email_unique')) {
            $unique = '|unique:users';
        }

        return Validator::make($data, [
            'name' => (string)$filterName,
            'email' => 'required|string|email|max:255'.$unique,
            'password' => 'required|string|min:6'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name'              =>  strip_tags($data['name']),
            'email'             =>  $data['email'],
            'password'          =>  Hash::make($data['password']),
            'ip_address'        =>  request()->ip(),
            'last_login'        =>  Carbon::now(),
            'last_activity'     =>  Carbon::now(),
            'language'          =>  app()->getLocale()
        ]);

        // Записываем в лог регистрацию нового пользователя
        if(iEXSetting('event_log_is_register')) {
            $event_log = sprintf('Пользователь %s успешно зарегистрировался', $user->name);
            main_event_log('event_log_login_fail', $event_log, $user->id);
        }

        //Регистрация рефералла
        ReferralSystemFacade::register($user, request()->cookie('ref'));
        CashbackFacade::register($user->id);

        return $user;
    }
}
