<?php

namespace App\Http\Controllers\Auth;

use App\Common\Support\Traits\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Куда перенаправлять пользователей после проверки.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, User $user)
    {
        // Если разрешена одна сессия на пользователя
        if(config('auth.one_session_user') == true)
        {
            $last_session = ($user->session_id != null) ? \Session::getHandler()->read($user->session_id) : null;
            if ($last_session) {
                \Session::getHandler()->destroy($user->session_id);
            }
            $user->session_id = \Session::getId();
            $user->save();
        }
    }

    /**
     * Get the throttle key for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function throttleKey(Request $request)
    {
        return '2fa:'.$request->ip();
    }

}
