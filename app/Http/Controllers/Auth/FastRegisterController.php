<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\RegisterClientJob;
use App\Common\Packages\{Cashback\Facades\CashbackFacade, ReferralSystem\ReferralSystemFacade};

use Illuminate\Support\{Carbon, Facades\Hash, Facades\Validator as ValidatorFacade, Str};

use App\Common\Support\Traits\FastRegistersUsers;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;

class FastRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use FastRegistersUsers;

    /**
     * Куда перенаправлять пользователей после проверки.
     *
     * @var string
     */
    protected string $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return Validator
     */
    protected function validator(array $data): Validator
    {
        $unique = null;
        if(iEXSetting('is_user_email_unique')) {
            $unique = '|unique:users';
        }

        return ValidatorFacade::make($data, [
            'email' => 'required|string|email|max:255'.$unique,
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data): User
    {
        // Создание пароля в случае если клиента нет в базе
        $password = Str::random(10);

        $newUser = User::create([
            'name'              =>  'iEXDefault',
            'ip_address'        =>  request()->ip(),
            'last_login'        =>  Carbon::now(),
            'last_activity'     =>  Carbon::now(),
            'email'             =>  Str::lower($data['email']),
            'password'          =>  Hash::make($password)
        ]);

        // Регистрируем имя
        if(!empty(iEXSetting('username_new_user'))) {
            $name = str_replace(
                [':id:', ':random:'], [$newUser->id, Str::random(7)], iEXSetting('username_new_user')
            );
            $newUser->update(['name' => $name]);
        }


        // Записываем в лог регистрацию нового пользователя
        if(iEXSetting('event_log_is_register')) {
            $event_log = sprintf('Пользователь %s успешно зарегистировался', $newUser->name);
            main_event_log('event_log_login_fail', $event_log, $newUser->id);
        }

        //Регистрация реферала
        ReferralSystemFacade::register($newUser, request()->cookie('ref'));
        //Регистрируем бонусную программу
        CashbackFacade::register($newUser->id);

        // Отправляем уведомление о регистрационных данных
        if(iEXSetting('is_email_auto_user'))
        {
            $delay = now()->addSeconds(2);
            dispatch(new RegisterClientJob($newUser, $password))->delay($delay)->onQueue('low');
        }

        return $newUser;
    }
}
