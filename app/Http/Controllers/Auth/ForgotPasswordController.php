<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Rules\ValidHCaptcha;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $validate['email'] = 'required|email';
//        if(iEXSetting('is_security_captcha_type') == 0 and !empty(config('captcha.sitekey')) and !empty(config('captcha.secret'))) {
//            $validate['g-recaptcha-response'] = 'required|captcha';
//        }

        if(iEXSetting('is_security_captcha_type') == 1 and !empty(config('captcha.sitekey')) and !empty(config('captcha.secret'))) {
            $validate['h-captcha-response'] = 'required|HCaptcha';
        }

        $request->validate($validate);
    }
}
