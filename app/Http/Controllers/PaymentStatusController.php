<?php
namespace App\Http\Controllers;

use App\Common\Packages\Editor\EditorFacade;
use App\Common\Packages\Payment\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Models\MerchantTransactionId;
use App\Models\Task;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PaymentStatusController extends Controller
{
    /**
     * Обработка и передача запроса в мерчант
     *
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|RedirectResponse
     * @throws \Throwable
     */
    public function checkout(Request $request)
    {
        $validator = Validator::make($request->route()->parameters(), [
            'hash' => ['required', 'exists:transactions,transaction']
        ]);

        // Если в валидаторе есть ошибки, блокируем клиента
        if($validator->fails()) {
            // Бан за нарушение
            if(\Auth::check()) {
                add_user_ban(\Auth::id(), Carbon::now()->addDay(), 'Нарушение правил: Предупреждение');
            }
            abort(403, 'Доступ запрещен');
        } else {
            // Делаем запрос к заявкам, для получения данных.
            $order_data = Task::whereHas('transaction', function(Builder $query) use($request) {
                $query->where('transaction', '=', (string)security_xss($request->route('hash')));
            })->where([
                ['status', 2], ['merchant_status', 0]
            ]);

            // Если существует информация по заявке, разбираем
            if($order_data->exists()) {
                $order = $order_data->first();

                //Получаем подробную информацию о транзакции
                $transaction = TransactionFacade::init($order);

                // Отклоняем заявку, если клиент не перешел в установленное время
                $expired_at = Carbon::parse($order->created_at)->addSeconds(intval(iEXSetting('max_time_task')));
                if(Carbon::now() > $expired_at) {
                    $transaction->failed(['fixed_manager' => false]);
                    return redirect()->to('/');
                }

                // Получаем детали мерчанта
                $merchant = $transaction->getMerchant();
                if(!is_null($merchant) and $order->merchant_provider == $merchant->gateway->alias)
                {
                    // Запрещаем управлять заявкой с другого IP Адреса
                    if($merchant->is_deny_ip_address == 1 and $request->ip() != $order->ip) {
                        return redirect()->to('/');
                    }

                    // Если примечание заполнено, заменяем базовый текст на новый.
                    if(!empty($merchant->comment)) {
                        $description = EditorFacade::driver('bbcode')->order($merchant->comment, $order);
                    } else {
                        $description = $transaction->merchantDescription($request->getHttpHost(), $merchant->gateway->alias);
                    }

                    // Записываем в лог мерчанта
                    iex_order_merchant_log($order->id, 0, 1, 'Переход на страницу оплаты', $merchant->gateway->alias);


                    $cancelUrl = url('/payment_status/fail');
                    $returnUrl = url('/payment_status/success');


                    // Опция для суммы отдаю
                    $in_price = $order->give_price;
                    if($merchant->pay_amount == 1) {
                        $in_price = $order->give_price_with_comm_pay;
                    } elseif($merchant->pay_amount == 2) {
                        $in_price = $order->give_price_default;
                    }

                    //Формирование html документа для переадресации
                    $response = PaymentFacade::merchant($merchant->gateway->alias, $order->merchant->id)
                        ->purchase([
                            'notifyUrl' =>  route('merchant.receive_money', [$merchant->gateway->alias, $merchant->security_hash]), // receive_money
                            'cancelUrl' =>  $cancelUrl,
                            'returnUrl' =>  $returnUrl
                        ])
                        ->setClientIp($request->getClientIp())
                        ->setAmount($in_price)
                        ->setCustomAmount($in_price)
                        ->setCoinCurrency(Str::upper($transaction->getCodeIn()->name))
                        ->setCurrency(Str::upper($transaction->getCodeIn()->name))
                        ->setDescription($description)
                        ->setTransactionId($order->id)
                        ->setOrderData($order)
                        ->setPaymentExtraFields($transaction)
                        ->send();

                    // Подгружаем конфигурацию
                    $configProvider = PaymentFacade::configProvider($merchant->gateway->alias);

                    // Исключенные callbacks вызовы
                    if(isset($configProvider) and isset($configProvider['except_callbacks']) and $configProvider['except_callbacks'])
                    {
                        // записываем обновление статуса в лог
                        iex_order_status_log($order, 3);

                        $order->update([
                            'lead_time'   =>        Carbon::now()->addMinutes((int)iEXSetting('lead_time_task')),
                            'started_at'  =>        Carbon::now()->toDateTimeString(),
                            'status'      =>        3,
                            'is_bot'      =>        1,
                            'is_auto_check_pay' =>  1
                        ]);

                    } else {
                        // записываем обновление статуса в лог
                        iex_order_status_log($order, 9);
                        $order->update([
                            'merchant_status'   =>  1,
                            'status'            =>  9
                        ]);
                    }

                    // Если это мерчант, сразу переадресовываем

                    if(isset($configProvider) and isset($configProvider['payment_status_checkout_redirected']) and $configProvider['payment_status_checkout_redirected'])
                    {
                        if(method_exists($response, 'getLabel'))
                        {
                            MerchantTransactionId::create([
                                'id_task' => $order->id,
                                'transaction_id' => $response->getLabel(),
                                'provider' => $merchant->gateway->alias
                            ]);
                        }

                        return $response->redirect();
                    }

                    return view('payment.redirect', [
                        'item'  =>  $order,
                        'html'  =>  $response->render()
                    ]);
                }
            } else {
                // Если данные не подходят, отправляем в бан
                //add_user_ban($request->user()->id, null, 'Нарушение правил при переходе в Платежную систему');
            }
        }

        return redirect()->to('/');
    }
}
