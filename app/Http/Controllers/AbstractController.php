<?php
namespace App\Http\Controllers;

use Illuminate\Support\Arr;

class AbstractController extends Controller
{
    /***
     * Глобальный массив для сбора результатов
     *
     * @return array
     */
    protected $result_data = [];


    public function __construct()
    {
        $this->result_data['items'] = collect();
    }

    /**
     * @param null $key
     * @return array
     */
    public function getItem($key = null)
    {
        if($key == null) {
            return $this->result_data['items']->all();
        }else {
            return Arr::get($this->result_data['items'], $key);
        }
    }

    public function setItem($key, $value)
    {
        $this->result_data['items']->put($key, $value);
    }

    public function setItems($value)
    {
        $this->result_data['items']->push($value);
    }
}
