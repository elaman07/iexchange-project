<?php
namespace App\Http\Controllers\Administrator;


use App\Http\Controllers\Controller;
use App\Models\AdminDesktop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Куда перенаправлять пользователей после проверки.
     *
     * @var string
     */
    protected string $redirectTo = '/';

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_admin_favorites_direction',
        'is_admin_top_partners',
        'is_admin_user_admin_home'
    ];

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $configStatusOperator = [
        'is_user_status_operator',
    ];

    /**
     * Рабочий стол с виджетами
     *
    */
    public function index(Request $request)
    {
        $desktop = AdminDesktop::with(['gadgets' => function ($query) {
            $query->orderBy('sorting');
        }])->where('id_user', '=', auth()->id())->orderBy('sorting');


        if($desktop->count() == 0)
        {
            $next_id = ($desktop->count() + 1);
            AdminDesktop::create([
                'name' => 'Рабочий стол '.$next_id,
                'id_user' => auth()->id(),
                'columns' => 2,
                'sorting' => 0,
                'flex_num1' => 50,
                'flex_num2' => 50,
                'flex_num3' => 0,
                'flex_num4' => 0
            ]);

            return redirect()->to(
                admin_base_path('/')
            );
        }

        // Для админов у которых есть права для настройки
        if($request->user()->can('admin_settings'))
        {
            if($request->has('widget') and $request->has('cache_clear'))
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }
                switch ($request->get('widget')) {
                    case 'order_id':
                        Cache::forget('order_id_thumb');
                        break;
                }

                flash(__('Кэш успешно очищен'), ['alert alert-success']);
                return redirect()->to(
                    admin_base_path('/')
                );
            }
        }


        if($request->has('page_id')) {
            $desktop->where('id', '=', $request->get('page_id'));
        }

        if($desktop->count() == 0) {
            flash(__('Рабочий стол не найден'), ['alert alert-danger']);
            return redirect()->to(admin_base_path('/'));
        }

        $desktops = AdminDesktop::with(['gadgets' => function ($query) {
            $query->orderBy('sorting');
        }])->where('id_user', '=', auth()->id())->get();


        // Кэш виджетов
        if($request->has('widget_form'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            if($request->user()->can('admin_cache') and $request->get('widget_form') == 'cache_control')
            {
                iex_demo_version_limitation();

                if($request->has('wid_cache_control')) {
                    iex_clear_cache_control($request->get('wid_cache_control'));
                }


                flash('Кэш успешно очищен', ['alert alert-success']);
                return redirect()->to(admin_base_path('/'));

            } elseif($request->has('widget_form') == 'status_operator')
            {
                // Обновление конфига
                $array = [];
                foreach ($this->configStatusOperator as $item) {
                    if($request->has($item) and is_array($request->get($item))) {
                        $array[$item] = implode(',', $request->get($item));
                    } else {
                        $array[$item] = ($request->has($item) ? $request->get($item): null);
                    }
                }
                iEXSetting($array);
                flash(__('Настройки успешно сохранены'), ['alert alert-success']);
                return redirect()->to(admin_base_path('/'));
            }
        }


        return view('admin.index', [
            'desktop' => $desktop->first(),
            'desktops' => $desktops
        ]);
    }

}
