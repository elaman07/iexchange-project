<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 25.04.2021
 * Time: 0:59
 */

namespace App\Http\Controllers\Administrator;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Nwidart\Modules\Facades\Module;

class ModuleController extends Controller
{

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'module_status_bestchange',
        'currency_is_recount_default',
        'currency_recount_percent',
        'currency_recount_time_minutes',
        'currency_recount_time_hours',
        'is_currency_disabled_admin_filter',
        'is_currencies_fire_angular'
    ];


    public function index(Request $request)
    {
        if($request->has('action'))
        {

            if(config('admin.is_demo_mode') == true) {
                flash('В демо версии, это действие отключено', ['alert alert-danger']);
                return redirect()->to(admin_base_path('/modules'));
            }

            if($request->has('type') and $request->get('type') == 'local') {
                // Обновление конфига
                $array = [];
                if($request->has('module') and $request->get('module') == 'module_status_bestchange' and $request->get('action') == 'enable') {
                    $array['module_status_bestchange'] = 0;
                }elseif($request->has('module') and $request->get('module') == 'module_status_bestchange' and $request->get('action') == 'disable') {
                    $array['module_status_bestchange'] = 1;
                }
                iEXSetting($array);
                return redirect()->back();

            } else {


                if($request->get('action') == 'enable') {
                    Artisan::call('module:enable', ['module' => trim($request->get('module'))]);
                } elseif($request->get('action') == 'disable') {
                    Artisan::call('module:disable', ['module' => [trim($request->get('module'))]]);
                }
            }


            return redirect()->to(
                admin_base_path('/modules')
            );
        }


        return view('admin.module', [
            'modules' => Module::all(),
            'modules_count' => Module::count()
        ]);
    }
}
