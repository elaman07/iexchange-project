<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\CodeCurrency;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class APIController extends Controller
{

    public function index(Request $request)
    {
        $users = User::has('tokens')->paginate(20);


        $abilities = collect([
            ['id' => 'account', 'name' => 'Доступ к учетной записи'],
            ['id' => 'exchange', 'name' => 'Доступ к валютам и направлениям'],
            ['id' => 'create-order', 'name' => 'Доступ к создании заявки'],
            ['id' => 'confirm-order', 'name' => 'Доступ к подтверждении заявки'],
            ['id' => 'status-order', 'name' => 'Доступ к статусу заявки'],
            ['id' => 'cancel-order', 'name' => 'Доступ к отклонении заявки'],
            ['id' => 'partner', 'name' => 'Доступ к партнерской программе']
        ])->pluck('name', 'id')->toArray();

        return view('admin.api.index', [
            'users' => $users,
            'filter' => $request->all(),
            'abilities' => $abilities
        ]);
    }

    /**
     * Обработать и добавить код валюты
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if (config('admin.is_demo_mode'))
        {
            flash('Данная функция недоступа в демо версии', ['alert alert-danger']);
            return redirect()->back();
        }

        if ($request->get('actions') == 'save')
        {
            foreach ($request->get('item_id') as $key => $value)
            {
                $find = User::find($key);
                foreach ($find->tokens as $token)
                {
                    $token->update([
                        'abilities' => $request->get('abilities')[$key][$token->id] ?? []
                    ]);
                }
            }

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();

        } elseif($request->get('actions') == 'trashed')
        {

            foreach ($request->get('item_id') as $key => $value)
            {
                $find = User::find($key);
                $find->tokens()->whereIn('id', $request->get('check')[$key])->delete();
            }

            flash('Выбранный элементы удалены', ['alert alert-success']);
            return redirect()->back();
        }
    }
}
