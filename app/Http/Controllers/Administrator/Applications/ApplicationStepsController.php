<?php

namespace App\Http\Controllers\Administrator\Applications;

use App\Http\Controllers\Controller;
use App\Models\OrderStep;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class ApplicationStepsController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_enabled_module_order_step'
    ];

    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $lists = OrderStep::orderByDesc('id')->paginate(20);
        return view('admin.applications.steps.index', [
            'items' =>  $lists
        ]);
    }

    /**
     * Форма добавления записи
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('admin.applications.steps.create');
    }

    /**
     * Обработка и добавление записи
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $options = [
                'id_manager'    => auth()->id(),
                'status'            => $request->has('status') ? 1 : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            if($request->hasFile('icon'))
            {
                $logo_icon = $request->file('icon');
                $filename = sprintf('step-%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

                $destinationPath = public_path('/storage');
                $logo_icon->move($destinationPath, $filename);
                $options['icon'] = $filename;
            }

            $item = OrderStep::create($options);

            flash("Этап {$item->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('application-steps.index');
        }
    }

    /**
     * Форма редактирования записи
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $order_step = OrderStep::findOrFail($id);

        return view('admin.applications.steps.edit',compact('order_step'));
    }

    /**
     * Обработка и обновления записи
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $order_step = OrderStep::findOrFail($id);
        $filename = $order_step->icon;
        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $options = [
                'id_manager'    => auth()->id(),
                'status'            => $request->has('status') ? 1 : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            if($request->hasFile('icon'))
            {
                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/'.$filename));

                $logo_icon = $request->file('icon');
                $filename = sprintf('step-%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

                $destinationPath = public_path('/storage');
                $logo_icon->move($destinationPath, $filename);
                $options['icon'] = $filename;
            }

            $order_step->update($options);

            flash("Этап {$order_step->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('application-steps.index');
        }
    }

    /**
     * Удаление записи
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $order_step = OrderStep::findOrFail($id);
        flash("Этап {$order_step->name} успешно удален", ['alert alert-success']);
        $order_step->delete();

        return redirect()->back();
    }

    /**
     * Сортировка преимуществ
     */
    public function sorting()
    {
        $items = OrderStep::orderBy('sorting')->get();
        return view('admin.applications.steps.sorting', [
            'items' => $items
        ]);
    }
}

