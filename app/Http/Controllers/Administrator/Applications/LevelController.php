<?php
namespace App\Http\Controllers\Administrator\Applications;

use App\Http\Controllers\Controller;
use App\Models\OperationLevel;
use App\Models\OperatorLevelGroup;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class LevelController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $lists = OperationLevel::orderByDesc('id')->paginate(20);
        return view('admin.applications.level.index', [
            'lists' =>  $lists
        ]);
    }

    /**
     * Форма добавления записи
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        $levels = OperatorLevelGroup::orderByDesc('id')->get();
        $users = User::role(Role::all())->get();

        return view('admin.applications.level.create', compact('levels', 'users'));
    }

    /**
     * Обработка и добавление записи
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id_level_group' => ['required'],
            'id_operator'   =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $first = OperationLevel::create([
                'id_level_group'    => $request->has('id_level_group') ? $request->get('id_level_group') : 0,
                'id_operator'       => $request->has('id_operator') ? $request->get('id_operator') : 0,
                'status'            => $request->has('status') ? 1 : 0
            ]);

            flash("Запись {$first->name} успешно добавлена", ['alert alert-success']);
            return redirect()->route('levels.index');
        }
    }

    /**
     * Форма редактирования записи
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = OperationLevel::findOrFail($id);
        $levels = OperatorLevelGroup::orderByDesc('id')->get();
        $users = User::role(Role::all())->get();
        return view('admin.applications.level.edit',compact('item', 'levels', 'users'));
    }

    /**
     * Обработка и обновления записи
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $link = OperationLevel::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_level_group' => ['required'],
            'id_operator'   =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link->update([
                'id_level_group'    => $request->has('id_level_group') ? $request->get('id_level_group') : 0,
                'id_operator'       => $request->has('id_operator') ? $request->get('id_operator') : 0,
                'status'            => $request->has('status') ? 1 : 0
            ]);

            flash("Запись {$link->name} успешно обновлена", ['alert alert-success']);
            return redirect()->route('levels.index');
        }
    }

    /**
     * Удаление записи
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $link = OperationLevel::findOrFail($id);
        flash("Запись {$link->name} успешно удалена", ['alert alert-success']);
        $link->delete();

        return redirect()->back();
    }
}
