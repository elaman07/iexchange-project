<?php
namespace App\Http\Controllers\Administrator\Applications;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Controllers\Controller;
use App\Models\AdminFilterHeader;
use App\Models\AdminFilterUser;
use App\Models\CodeCurrency;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\FilterCurrency;
use App\Models\Filters\OrderLiteFilter;
use App\Models\Payment;
use App\Models\PendingOrderStatus;
use App\Models\Requisites;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\TaskInfo;
use App\Models\TaskLogConfirmation;
use App\Models\TaskRejectionStatus;
use App\Models\TaskStatus;
use App\Models\TaskStatusLog;
use App\Models\UnpaidItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class ApplicationController extends Controller
{
    /**
     * Сортировки которые буду записываться в локальное храналище
     *
     * @var array
    */
    protected $allowSorting = [
        'sorting_order_id',
        'sorting_order_status',
        'sorting_order_created_at',
        'sorting_order_updated_at',
        'sorting_order_amount',
    ];

    /**
     * Дополнительные фильтры
     *
     * @var array
    */
    protected $allowFiltered = [
        'is_allow_order_filter',
        'show_task_page',
//        'order_priority',
        'is_order_all_filters',
        'order_identify_newbie',
        'max_time_task',
        'give_transfer_reserve',
        'order_is_allow_spam',
        'is_enabled_favorites_order',
        'is_enabled_spam_order',
        'is_enabled_delete_order',
        'count_change_operator',
        'is_enabled_add_comment_task',
        'is_enabled_last_comment_task',
        'is_currencies_fire_angular'
    ];

    /***
     * Список всех заявок
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function index(Request $request){
        return redirect()->route('admin.application.tx', $request->all());
    }


    /**
     * Архивированные заявки
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function archive(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'cancel_archiving')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            if(count($request->get('archives')) > 0)
            {
                foreach ($request->get('archives') as $item) {
                    Task::find($item)->update(['is_archive' => 0]);
                }
                flash('Архивированные заявки успешно возвращены', ['alert alert-success']);
            }

            return redirect()->back();
        }

        $orders = Task::where('is_archive', '=',1)->orderByDesc('id')->paginate(20);
        return view('admin.applications.archive', [
            'count' => $orders->total(),
            'orders' => $orders
        ]);
    }

    /**
     * Избранные заявки
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function favorites(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'remove_favorites')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            if(count($request->get('favorites')) > 0)
            {
                foreach ($request->get('favorites') as $item) {
                    Task::find($item)->update(['is_favorites' => 0]);
                }
                flash('Избранные заявки успешно возвращены', ['alert alert-success']);
            }

            return redirect()->back();
        }

        $orders = Task::where('is_favorites', '=',1)->orderByDesc('id')->paginate(20);
        return view('admin.applications.favorites', [
            'count' => $orders->total(),
            'orders' => $orders
        ]);
    }

    /**
     * Спам заявки
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function spam(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'remove_spam')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            if(count($request->get('spam')) > 0)
            {
                foreach ($request->get('spam') as $item) {
                    Task::find($item)->update(['is_spam' => 0]);
                }
                flash('Выбранные заявки успешно возвращены', ['alert alert-success']);
            }

            return redirect()->back();
        }

        $orders = Task::where('is_spam', '=',1)->orderByDesc('id')->paginate(20);
        return view('admin.applications.spam', [
            'count' => $orders->total(),
            'orders' => $orders
        ]);
    }

    /**
     * Получить все удаленные заявки
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function trashed(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'cancel')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            if(count($request->get('deleted_at')) > 0)
            {
                foreach ($request->get('deleted_at') as $item) {
                    Task::withTrashed()->find($item)->restore();
                    TransactionFacade::call($item)->restoreOrder();
                }
                flash('Выбранные заявки успешно восстановлены', ['alert alert-success']);
            }

            return redirect()->back();
        }

        $orders = Task::onlyTrashed()->orderByDesc('id')->paginate(20);
        return view('admin.applications.trashed', [
            'count' => $orders->total(),
            'orders' => $orders
        ]);
    }

    /**
     * Лог подтверждений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmationLog(Request $request)
    {
        $logs = TaskLogConfirmation::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action'))
        {
            $conditions = Arr::except($request->all(), ['send_action']);
            return \redirect()->route('admin.application.confirmation-log', $conditions);
        }

        return view('admin.applications.confirmation-log', [
            'logs' => $logs,
            'filter' => $request->all()
        ]);
    }

    /**
     * Отложенные заявки
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postponed(Request $request)
    {
        if($request->actions == 'reject') {
            TransactionFacade::call($request->id)->failedNotReserves();
            return \redirect()->back();
        }

        if(isset($request->id))
        {
            $find = Task::find($request->id);

            // записываем обновление статуса в лог
            $this->writeStatusLog($find->id, $find->status, 3);

            if($request->is_reserve == 1) {
                $find->is_reserve = 1;
            } else {
                $find->is_reserve = 0;
                TransactionFacade::call($request->id)->changeReserves();
            }
            $find->is_frozen = 0;
            $find->status = 3;
            $find->save();

            \redirect()->back();
        }

        $task = Task::orderBy('id', 'desc')->where('status', 8)->get();

       return view('admin.applications.postponed', [
           'items'  =>  $task
       ]);
    }

    /**
     * Настройки статусов заявок
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function settings(Request $request)
    {
        $statuses = TaskStatus::all();

        if($request->getMethod() == 'POST')
        {
            if($request->has('item_id'))
            {
                foreach ($request->item_id as $value_id)
                {
                    $update = TaskStatus::find($value_id);
                    $update_multi_language = [];
                    foreach (config('app.form_lang') as $key => $item) {
                        $update_multi_language['name'][$key] = $request->get('name'.$item['field'])[$value_id];
                    }

                    $update->update(array_merge($update_multi_language, [
                        'is_export' => $request->is_export[$value_id],
                        'class'     =>  $request->class[$value_id],
                        'color'     =>  $request->color[$value_id]
                    ]));
                }
            }

            return \redirect()->back();
        }

        return view('admin.applications.settings', [
            'statuses' =>  $statuses
        ]);
    }

    /**
     * Подробная информация о заявки
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Throwable
     */
    public function details(Request $request)
    {
        $transaction = TransactionFacade::find($request->id, [
            'preview'       =>  ($request->actions == 'arhive'),
            'commission'    =>  ($request->reserve_commission ?? 0),
            'type'          =>  ($request->type ?? 'default'),
            'payment'       =>  ($request->payment ?? null),
            'actions'       =>  ($request->actions ?? null),
            'message'       =>  ($request->message_success ?? null),
            'type_message'  =>  ($request->type_send_message ?? null),
            'feeAmount'     =>  ($request->subtractfeefromamount ?? false)
        ]);

        // Блокировка клиента
        if($request->has('ban_order'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $transaction->clientOrderDataBan();
            return redirect()->to(admin_base_path('/applications/details/'.$request->id.'/?actions=arhive'));
        }

        // Не даем заходить, если не проходит по лимиту
        if(!$transaction->hasPreview() and !$transaction->limit_operator_view()) {
            flash('У вас нет доступа к заявке', ['alert alert-danger']);
            return redirect()->to(admin_base_path('/applications/details/'.$request->id.'/?actions=arhive'));
        }


        //Для отложенных заявок
        if($request->st == 'postponed')
        {
            if($request->act == 'reject') {

                TransactionFacade::call($request->id)->failedNotReserves();
                return redirect(config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');

            } elseif($request->act == 'expectation')
            {
                $find = Task::find($request->id);

                // записываем обновление статуса в лог
                $this->writeStatusLog($find->id, $find->status, 3);

                if($request->is_reserve == 1) {
                    $find->is_reserve = 1;
                } else {
                    $find->is_reserve = 0;
                    TransactionFacade::call($request->id)->changeReserves();
                }
                $find->is_frozen = 0;
                $find->status = 3;
                $find->save();

                return redirect(config('admin.directory').'/applications/details/'.$request->id);
            }
        }

        // Изменения статуса транзакции у которого статус "Ожидается оплата"
        if($transaction->hasPreview() and isset($request->change_status))
        {
            if($request->change_status == 3) {
                $transaction->restoreOrder();
            } else {
                $transaction->setStatus($request->change_status);
            }

            // Переадресация транзакций со статусами
            if($transaction->isToArchive()) {
                return redirect()->to(
                    admin_base_path('/tx/'.$request->id.'?actions=arhive')
                );
            }


            if($request->change_status == 3) {
                return redirect(config('admin.directory').'/applications/details/'.$request->id);
            } else {
                return redirect(config('admin.directory').'/applications');
            }
        }

        if(!$transaction->hasPreview())
        {
            //Проверяем запущен ли процесс обработки заявки
            if($transaction->isStart() == false) {
                $transaction->start();
            }

            // Переадресация транзакций со статусами (Оплаченная)
            if($transaction->isToArchiveNotPreview()) {
                return redirect(config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');
            }

            // Недопускаем неактивные транзакции и недаем возможность обрабатывать транзакцию
            // нескольким операратора
            if(!in_array($request->user()->id, config('admin.ids_admins_orders')))
            {
                // Если в заявке оператор, то мы записываем всех, то обрабатывает заявку
                if($transaction->hasNotActive() or $transaction->hasOperator())
                {
                    if($transaction->maxCountChangeOperator()) {
                        flash('Превышен лимит смены операторов', ['alert alert-info']);
                        return redirect()->to(admin_base_path('/applications'));
                    }


                    $transaction->addOperatorToHistory(Auth::id());
                    $transaction->setOperator(Auth::id());

                    return redirect(config('admin.directory').'/applications/details/'.$request->id);
                }
            }


            //Мгновенная обработка транзакции по выбранным действиям
            if($transaction->getStatus() != 4) {
                $transaction->handlerAction();
            }
        }

        if($transaction->hasAction() == true and !$transaction->hasPreview())
        {
            if($transaction->hasAction() == 'logout') {
                return redirect(config('admin.directory').'/applications');
            }elseif($transaction->rulesRedirect() == 0) {
                return redirect(config('admin.directory').'/applications/details/'.$request->id);
            } else {
                return redirect(config('admin.directory').'/applications');
            }
        }

        $walletInfoIn = null;
        if($transaction->isStart() and $transaction->getWalletTransaction() != null) {
            $walletInfoIn = $transaction->getWalletTransaction();
        } elseif($transaction->isStart() and $transaction->getHistoryCode() != null) {
            $walletInfoIn = $transaction->getHistoryCode();
        }

        $taskInfo = $transaction->getTaskInfo();
        $taskReport = $transaction->getTaskReport();

        if($transaction->getStatus() != 10)
        {
            if($transaction->isValidityOrder() != null and $transaction->isValidityOrder()['level'] == 3) {
                $transaction->failedInvalidBid();

                return redirect(config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');
            }
        }

        // Причины отклонения заявок
        $reasonRejection = TaskRejectionStatus::all()->pluck('name', 'id');
        // Причина отложения заявки
        $pendingOrderStatus = PendingOrderStatus::all()->pluck('name', 'id');


        $recalc = $transaction->getHistoryRecalculation();

        $display_in_price = $transaction->getDisplayInPrice(true);
        $display_out_price = $transaction->getDisplayOutPrice(true);

        // Новый курс
        $new_rates = ConvertFacade::call($transaction->getDirectionExchange())->all();

        $percent_diff = 0;

        try {
            $percent_diff = iex_number_format(($display_out_price / ($new_rates['amount'] * $display_in_price) - 1) * 100, 2);
        }catch (\Exception $exception) {
            //
        }

        $new_out_amount = 0;
        if($percent_diff > 0) {
            $new_out_amount = iex_number_format($display_out_price - ($display_in_price * $new_rates['amount']), $transaction->getCurrencyOut()->number_format);
        }


        $detail = $transaction->getItem();
        // Лог резерва
        $reserves_log = \App\Models\ReserveLog::whereIdTask($detail->id)->get();


        return view('admin.applications.details', [
            'reserves_log'          =>  $reserves_log,
            'disable_manual_button' =>  $transaction->isDisabledManualButton(),
            'new_rates'             =>  $new_rates,
            'percent_diff'          =>      $percent_diff,
            'new_out_amount'        =>      $new_out_amount,
            'reasonRejection'       =>      $reasonRejection,
            'pendingOrderStatus'    =>      $pendingOrderStatus,
            'validityOrder'         =>      $transaction->isValidityOrder(),
            'historyRecalculation'  =>      $recalc,
            'historyOperators'      =>      $transaction->getHistoryOperators(),
            'lastRecalcDate'         =>      ($recalc->count() > 0 ? $recalc->last()->created_at : null),
            'historyChat'           =>      $transaction->getHistoryChat(),
            'taskInfo'              =>      (isset($taskInfo) ? $transaction->getTaskInfo() : null),
            'taskReport'            =>      ($taskReport ?? null),
            'totalProfit'           =>      (isset($taskReport) ? $transaction->getTotalProfit() : 0),
            'txReceiving'           =>      ($history ?? []),
            'paymentIn'             =>      $transaction->getPaymentIn(),
            'codeIn'                =>      $transaction->getCodeIn(),
            'paymentOut'            =>      $transaction->getPaymentOut(),
            'codeOut'               =>      $transaction->getCodeOut(),
            'tasks_fields'          =>      $transaction->getTasksFields(),
            'transactions'          =>      $transaction->getTransactionById(),
            'bonus'                 =>      $transaction->rewardBonusString(),
            'referral'              =>      $transaction->referralBonus(),
            'referral_bonus'        =>      $transaction->referralBonusString(),
            'detail'                =>      $detail,
            'hasBalance'            =>      (!$transaction->hasPreview() ? $transaction->hasBalance() : null),
            'apiBalance'            =>      (!$transaction->hasPreview() ? $transaction->getAPIBalance() : null),
            'preview'               =>      $transaction->hasPreview(),
            'receive_wallet'        =>      $transaction->getAddresses(),
            'privateKeyAddress'     =>      $transaction->getPrivateKeyForAddress(),
            'account_number_field'  =>      $transaction->getFieldAccountNumber(),
            'checkPayment'          =>      $transaction->checkPayment(),
            'payOut'                =>      $transaction->getPay() ?: null,
            'walletInfoIn'          =>      $walletInfoIn,
            'leadTime'              =>      $transaction->getLeadTime(),
            'isAllowTransferReserve' =>     $transaction->isAllowTransferReserve(),
            'transferCommission'    =>      $transaction->getTransferCommission(),
            'is_hold'               =>      $transaction->isHold(),
            'uniqueToShot'          =>      $transaction->uniqueToShot(),
            'hold_timeout'          =>      $transaction->holdTimeOut(),
            'isVerifiedCard'        =>      $transaction->isVerifiedCard(),
            'display_in_price'      =>      $transaction->getDisplayInPrice(),
            'display_out_price'      =>      $transaction->getDisplayOutPrice(),
            // 'display_out_price_fee' =>      $transaction->getDisplayOutPriceFee(),
            'cardDetails'           =>      $transaction->getCardDetails()
        ]);
    }

    /**
     * Список покупателей
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buyers(Request $request)
    {
        $buyers = Task::with('user')->where('status', '=','4')
            ->select('id_user', \DB::raw('count(*) as total_orders'))
            ->groupBy('id_user')->orderByDesc('total_orders')->paginate(20);

        return view('admin.applications.buyers', compact('buyers'));
    }

    /**
     * Записываем статус в лог
     *
     * @param $old
     * @param $new
     */
    protected function writeStatusLog($id_task, $old, $new)
    {
        TaskStatusLog::create([
            'user_id'   =>  \auth()->id(),
            'id_task'   =>  $id_task,
            'old_status'    =>  $old,
            'new_status'    =>  $new
        ]);
    }
}
