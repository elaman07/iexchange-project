<?php
namespace App\Http\Controllers\Administrator\Applications;


use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Controllers\Controller;
use App\Models\AdminFilterHeader;
use App\Models\AdminFilterUser;
use App\Models\CodeCurrency;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\FilterCurrency;
use App\Models\Filters\OrderLiteFilter;
use App\Models\OrderStep;
use App\Models\Payment;
use App\Models\PendingOrderStatus;
use App\Models\Task;
use App\Models\TaskComment;
use App\Models\TaskCommentUser;
use App\Models\TaskConvertLog;
use App\Models\TaskFile;
use App\Models\TaskHistoryOperator;
use App\Models\TaskInfo;
use App\Models\TaskManagerStyle;
use App\Models\TaskMessage;
use App\Models\TaskRejectionStatus;
use App\Models\TaskStatus;
use App\Models\TaskStatusLog;
use App\Models\TransitRequisite;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MTApplicationController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_allow_order_filter',
        'show_task_page',
//        'order_priority',
        'is_order_all_filters',
        'order_identify_newbie',
        'max_time_task',
        'give_transfer_reserve',
        'order_is_allow_spam',
        'is_enabled_favorites_order',
        'is_enabled_spam_order',
        'is_enabled_delete_order',
        'count_change_operator',
        'is_enabled_add_comment_task',
        'is_enabled_last_comment_task',
        'is_currencies_fire_angular',
        'is_currencies_order_hide_cashback_bonus',
        'operator_panel_handler_orders',
        'is_allow_panel_operation_verification'
    ];


    /**
     * Список заявок клиентов
     *
     * @param Request $request
     * @return View|Factory|RedirectResponse|Application
     */
    public function index(Request $request): View|Factory|RedirectResponse|Application
    {
        $user_info = $request->user();

        if((int)iEXSetting('is_enable_order_online_chat') == 1and $request->has('clear_messages'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            \DB::table('tasks_messages')->update(['is_view' => 1]);
            return redirect()->to(
                admin_base_path('/tx')
            );
        }


        // Добавляем комментарий к заявке
        if($request->has('actions'))
        {
            if($request->get('actions') == 'custom_style')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                // Обновление конфига
                $array = [];
                foreach ($this->allowFiltered as $item) {
                    if($request->has($item) and is_array($request->get($item))) {
                        $array[$item] = implode(',', $request->get($item));
                    } else {
                        $array[$item] = ($request->has($item) ? $request->get($item): null);
                    }
                }
                iEXSetting($array);

                $style = TaskManagerStyle::where([
                    ['user_id', \auth()->id()]
                ])->update([
                    'is_hide_block_give_service' => $request->has('is_hide_block_give_service') ? $request->get('is_hide_block_give_service') : 0,
                    'is_hide_block_receive_service' => $request->has('is_hide_block_receive_service') ? $request->get('is_hide_block_receive_service') : 0,
                    'is_hide_block_client' => $request->has('is_hide_block_client') ? $request->get('is_hide_block_client') : 0,
                    'count_block_home' => $request->has('count_block_home') ? $request->get('count_block_home') : 0,
                ]);


                return redirect()->to(
                    admin_base_path('/tx')
                );
            }
            // Обновление фильтров
            if($request->get('actions') == 'admin_filter')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                $id_filter = $request->get('id_filter');
                $adminFilterUser = AdminFilterUser::where('id_filter_header', '=', $id_filter)->first();

                if(empty($request->get('adminFilters'))) {
                    $adminFilterUser->options = config('admin-filters.control_panel.default_fields');
                } else {
                    $adminFilterUser->options = $request->get('adminFilters');
                }
                $adminFilterUser->save();

                flash('Фильтр успешно обновлен', ['alert alert-success']);
                return redirect()->to(
                    admin_base_path('/tx')
                );
            }

            // Добавляем фильтр
            if($request->get('actions') == 'create_filter')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                $filter_options = [
                    'id_user' => $request->user()->id,
                    'name' => security_xss($request->get('name_filter')),
                    'type_filter' => 'control_panel'
                ];
                if($request->user()->can('admin_access_global_control')) {
                    $filter_options['is_common'] = $request->has('is_common') ? $request->get('is_common') : 0;
                }

                $response_header = AdminFilterHeader::create($filter_options);

                AdminFilterUser::create([
                    'id_filter_header' => $response_header->id,
                    'options' => config('admin-filters.orders.default_fields'),
                    'type_filter' => 'control_panel',
                    'id_user' => $response_header->id_user
                ]);

                flash('Фильтр успешно создан', ['alert alert-success']);
                return redirect()->to(
                    admin_base_path('/tx')
                );
            }

            if($request->get('actions') == 'delete_filter')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                $delete_filter = AdminFilterHeader::where('id_user', '=', $user_info->id)
                    ->where('id', '=', $request->get('id'));

                if($delete_filter->exists())
                {
                    $delete_filter->delete();
                    // Удаляем все связанные фильтры
                    DB::table('admin_filters_user')->where('id_filter_header', $request->get('id'))->delete();
                    flash('Фильтр успешно удален', ['alert alert-success']);
                } else {
                    flash('У вас нет прав для удаления фильтров', ['alert alert-danger']);
                }

                return redirect()->to(
                    admin_base_path('/tx')
                );
            }

            if($request->get('actions') == 'delete_filter_key')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                $key_filter = AdminFilterUser::where('id_user', '=', $request->user()->id)->where('id_filter_header', '=', $request->get('id_filter'))->first();
                $array = collect($key_filter->options)->reject(function($key, $value) use ($request){
                    return $key == $request->get('key_id');
                })->values()->toArray();


                $key_filter->options = $array;
                $key_filter->save();

                return redirect()->to(
                    admin_base_path('/tx')
                );
            }


            // Добавить файл к заявке
            if($request->get('actions') == 'add_attach_file')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                $validator = Validator::make($request->all(), [
                    'file_order' => 'required|image'
                ]);

                if($validator->fails()) {
                    flash($validator->messages()->first(), ['alert alert-danger']);
                    return redirect()->to(
                        admin_base_path('/tx')
                    );
                } else {
                    $file_value = $request->file('file_order');
                    $filename = $request->get('id').'__'.Str::uuid()->toString().'.'.$file_value->getClientOriginalExtension();

                    // Загружаем фотографию
                    if($request->hasFile('file_order')) {
                        $img = Image::make($request->file('file_order')->getRealPath());
                        $img->save(public_path('storage/orders/'.$filename));
                    }

                    TaskFile::create([
                        'id_task' => $request->get('id'),
                        'id_manager' => \auth()->id(),
                        'text' => $request->has('file_message') ? $request->get('file_message') : null,
                        'file' => $filename
                    ]);

                    flash('Файл успешно загружен', ['alert alert-success']);
                    return redirect()->to(
                        admin_base_path('/tx')
                    );
                }
            }

            if($request->get('actions') == 'add_comment')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                if(!$request->has('message')) {
                    return redirect()->back();
                }

                TaskComment::create([
                    'id_manager' => $request->user()->id,
                    'id_task' => intval($request->get('id')),
                    'message' => security_xss($request->has('message') ? $request->get('message') : null),
                    'class_style'   => $request->has('class_style') ? $request->get('class_style') : null
                ]);

                return redirect()->to(
                    admin_base_path('/tx')
                );
            }

            if($request->get('actions') == 'add_comment_user')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                if(!$request->has('message')) {
                    return redirect()->back();
                }

                TaskCommentUser::create([
                    'id_manager' => $request->user()->id,
                    'id_task' => intval($request->get('id')),
                    'message' => security_xss($request->has('message') ? $request->get('message') : null)
                ]);

                flash('Комментарий для пользователя добавлен', ['alert alert-success']);
                return redirect()->to(
                    admin_base_path('/tx')
                );
            }
        }

        $task = Task::has('task_info')
            ->withSum('task_user', 'convert_to_usd')
            ->with([
                'user',
                'merchant',
                'direction_exchange' => function($q) {
                    $q->select('id', 'id_currency1', 'id_currency2');
                },
                'direction_exchange.currency1' => function($q) {
                    $q->select('id', 'id_code_currency', 'id_payment', 'aml_service', 'number_format');
                },
                'direction_exchange.currency1.code_currency' => function($q) {
                    $q->select('id', 'name');
                },
                'direction_exchange.currency1.payment' => function($q) {
                    $q->select('id', 'name');
                },
                'direction_exchange.currency1.payment.explorer' => function($q) {
                    $q->select('id', 'id_payment', 'link');
                },
                'direction_exchange.currency1.aml_service_model' => function($q) {
                    $q->select('id', 'aml_name', 'aml_type');
                },

                'direction_exchange.currency2' => function($q) {
                    $q->select('id', 'id_code_currency', 'id_payment', 'aml_service', 'number_format');
                },
                'direction_exchange.currency2.code_currency' => function($q) {
                    $q->select('id', 'name');
                },
                'direction_exchange.currency2.payment' => function($q) {
                    $q->select('id', 'name');
                },
                'direction_exchange.currency2.payment.explorer' => function($q) {
                    $q->select('id', 'id_payment', 'link');
                },
                'direction_exchange.currency2.aml_service_model' => function($q) {
                    $q->select('id', 'aml_name', 'aml_type');
                },

                'merchant_transaction_hash',
                'history_recalculation',
                'referral_link',
                'referral_link.user' => function($q) {
                    $q->select('id', 'name', 'email', 'id');
                },
                'pending_order_status',
                'task_single_log_confirm',
                'task_status',
                'operator',
                'history_recalculation' => function($q) {
                    $q->select('id_task', 'course', 'type', 'created_at')->where('type', '=', 1);
                },
                'verification_card',
                'verification_card.currency' => function($q) {
                    $q->select('id', 'id_code_currency', 'id_payment');
                },
                'verification_card.currency.payment' => function($q) {
                    $q->select('id', 'name');
                },
                'verification_card.currency.code_currency' => function($q) {
                    $q->select('id', 'name');
                },
                'task_info' => function($q) {
                    $q->select('id', 'id_task', 'is_freeze_scam', 'is_freeze_local', 'is_aml_analysis', 'aml_riskscore', 'country_name', 'city_name', 'city_id', 'country_name',
                        'recalculated_at', 'network_name', 'is_wait_hash_pay', 'code_country', 'country', 'device', 'newbie', 'is_scam', 'is_local_scam', 'aml_result_value', 'is_aml_high_risk');
                },
                'task_info.cities',
                'aml_analytics_log_getblock_success',
                'referral_log',
                'task_messages',
                'edit_data_manager',
                'task_comment',
                'task_file',
                'task_comment_user',
                'wallet_transaction',
                'pay_transaction_hash',
                'tasks_fields_base',
                'tasks_fields_in_out',
                'payment_requisites',
                'payment_requisites.requisites_info_fields',
                'verification_card_active_user',
                'tasks_rejection_status'])
            ->filter($request->all())->where('is_archive', '=', 0);


        // Не отображать спам заявки
        if(iEXSetting('order_is_allow_spam') == 0) {
            $task->where('is_spam', '=', 0);
        }

        $conditions = null;
        // Удаляем ненужные ключи из массива
        if($request->has('send_action'))
        {
            $options = [];
            $options['send_action'] = 'on';

            $filter_search = $request->q;
            if (filter_var($request->q, FILTER_VALIDATE_EMAIL)) {
                $filter_search = 'emailAddress '.$filter_search;
            }

            if(is_numeric($filter_search) and \Str::length($filter_search) > 12) {
                $filter_search = 'idPublic '.$filter_search;
            }

            $filter = explode(' ', $filter_search);

            if(isset($filter[1]))
                $options[$filter[0]] = $filter[1];
            else
                $options['id'] = $filter_search;
            $options['q'] = $filter_search;

            $conditions = Arr::except($options, ['send_action']);
            flash('Результаты по запросу '.$options['q'], ['alert text-center alert-success alert-no-margin']);

            if(!empty($filter_search)) {
                return \redirect()->to(
                    admin_base_path('/tx?'. http_build_query(Arr::except($options, ['send_action'])))
                );
            }
        }

        // Удаляем ненужные ключи из массива
        if($request->has('lite_send_action'))
        {
            $conditions = Arr::except($request->all(), ['lite_send_action']);
            if($request->has('price') and $request->get('price') == '0;0')
                $conditions = Arr::except($request->all(), ['price']);

            return \redirect()->to(
                admin_base_path('/tx?'. http_build_query($conditions))
            );
        }

        //Order_id
        $items = $task->orderBy('id', 'desc');
        if($user_info->is_enable_order_paginate == 0) {
            $items = $items->simplePaginate((int)iEXSetting('show_task_page', 20));
        } else {
            $items = $items->paginate((int)iEXSetting('show_task_page', 20));
        }
        $order_id = '0';


//        ->simplePaginate(20);
//        $order_id = '';
        $adminFilterCommon = AdminFilterHeader::with(['admin_filters_user'])->where([
            ['is_common', '=', 1],
            ['type_filter', '=', 'control_panel']
        ])->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
                'admin_filters_user' => $item->admin_filters_user
            ];
        })->toArray();

        $adminFiltersHeaders = AdminFilterHeader::with(['admin_filters_user'])->where([
            ['id_user', '=', $request->user()->id],
            ['is_common', '=', 0],
            ['type_filter', '=', 'control_panel']
        ])->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
                'admin_filters_user' => $item->admin_filters_user
            ];
        })->toArray();

        $arrayFilter = array_merge($adminFilterCommon, $adminFiltersHeaders);

        $style = TaskManagerStyle::where([[
            'user_id', \auth()->id()
        ]])->first();

        if(!isset($style)) {
            $style = TaskManagerStyle::create([
                'user_id' => \auth()->id(),
                'count_block_home' => 4,
                'is_hide_block_client' => 0,
                'is_hide_block_receive_service' => 0,
                'is_hide_block_give_service' => 0
            ]);
        }

        return view('admin.applications.tx.index', [
            'style'                 => $style,
            'adminFiltersHeaders'   =>  $arrayFilter,
            'direction'             =>  ($response_filters['direction'] ?? []),
            'currencies'           =>   ($response_filters['currencies'] ?? []),
            'conditions'            =>  $request->all(),
            'count'                 =>  $order_id,
            'items'                 =>  $items,
            'search'                =>  ($request->q ?? null),
            'error'                 =>  $request->error,
            'allOrderStatuses'      =>  ($response_filters['allOrderStatuses'] ?? []),
            'filter'                =>  $request->all()
        ]);
    }

    /**
     * Детали по заявке №
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     * @throws \Throwable
     */
    public function detail(Request $request)
    {
        $transaction = TransactionFacade::find($request->id, [
            'preview'       =>  ($request->actions == 'arhive'),
            'type'          =>  ($request->type ?? 'default'),
            'payment'       =>  ($request->payment ?? null),
            'actions'       =>  ($request->actions ?? null),
            'message'       =>  ($request->message_success ?? null),
            'type_message'  =>  ($request->type_send_message ?? null),
            'feeAmount'     =>  ($request->subtractfeefromamount ?? false)
        ]);


      //  $transaction->generalRecountOrder();

        if((int)iEXSetting('is_enable_order_online_chat') == 1)
        {
            // Отметка как прочитанное все записи в чате
            TaskMessage::where([
                ['id_task', $request->id]
            ])->update(['is_view' => 1]);
        }

        // Удаляем транзакцию если отдается 0
        if((float)$transaction->getAmountIn() <= 0) {
            $transaction->failed();
            return redirect()->to(admin_base_path('/tx/'));
        }


        // Блокировка клиента
        if($request->has('ban_order'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $transaction->clientOrderDataBan();
            return redirect()->to(admin_base_path('/tx/'.$request->id.'/?actions=arhive'));
        }

        // Смена этапа
        if($request->has('order_step')) {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $transaction->changeOrderStep($request->get('order_step'));
            return redirect()->back();
        }


        if((int)iEXSetting('is_enable_order_online_chat') == 1 and $request->has('is_blocked_chat'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $find = TaskInfo::where('id_task', $request->id)->first();
            $find->update([
                'is_blocked_chat' => 1
            ]);
            return redirect()->back();
        }
        if((int)iEXSetting('is_enable_order_online_chat') == 1 and $request->has('is_unblocked_chat'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $find = TaskInfo::where('id_task', $request->id)->first();
            $find->update([
                'is_blocked_chat' => 0
            ]);
            return redirect()->back();
        }

        // Не даем заходить, если не проходит по лимиту
        if(!$transaction->hasPreview() and !$transaction->limit_operator_view()) {
            flash('У вас нет доступа к заявке', ['alert alert-danger']);
            return redirect()->to(admin_base_path('/tx/'.$request->id.'/?actions=arhive'));
        }


        //Для отложенных заявок
        if($request->st == 'postponed')
        {
            if($request->act == 'reject') {

                TransactionFacade::call($request->id)->failedNotReserves();
                return redirect(config('admin.directory').'/tx/'.$request->id.'?actions=arhive');

            } elseif($request->act == 'expectation')
            {
                $find = Task::find($request->id);

                // записываем обновление статуса в лог
                $this->writeStatusLog($find->id, $find->status, 3, $find->course_display);

                if($request->is_reserve == 1) {
                    $find->is_reserve = 1;
                } else {
                    $find->is_reserve = 0;
                    TransactionFacade::call($request->id)->changeReserves();
                }
                $find->is_frozen = 0;
                $find->status = 3;
                $find->save();

                return redirect(config('admin.directory').'/tx/'.$request->id);
            }
        }

        if($request->has('actions') and $request->get('actions') == 'add_requisites_payment')
        {
            // Берем из списка транзитные реквизитов
            $id_request_payments = $request->id_request_payments;

            $account = !empty($request->requisites_field) ? $request->requisites_field : null;
            $requisites_description = !empty($request->requisites_description) ? $request->requisites_description : null;

            // Если выбрано из транзитных реквизитов, начинаем обработку
            if((int)$id_request_payments > 0) {
                // Получение реквизита
                $transit_view = TransitRequisite::find($id_request_payments);
                $account = $transit_view->account;
                $transit_view->update([
                    'view' => $transit_view->view+1
                ]);
            }

            if($account == null) {
                flash('Реквизит не найден, попробуйте снова', ['alert alert-danger']);
                return redirect()->back();
            }
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $transaction->updateAddRequisitesPayment($account, $requisites_description);
            flash('Данные обновлены', ['alert alert-success']);

            // Переадресация транзакций со статусами
            if($transaction->isToArchive()) {
                return redirect()->to(
                    admin_base_path('/tx/'.$request->id.'?actions=arhive')
                );
            }
        }

        // Изменения статуса транзакции у которого статус "Ожидается оплата"
        if($transaction->hasPreview() and isset($request->change_status))
        {
            if($request->change_status == 3) {
                $transaction->restoreOrder();
            } else {
                $transaction->setStatus($request->change_status);
            }

            // Переадресация транзакций со статусами
            if($transaction->isToArchive()) {
                return redirect()->to(
                    admin_base_path('/tx/'.$request->id.'?actions=arhive')
                );
            }


            if($request->change_status == 3) {
                return redirect(config('admin.directory').'/tx/'.$request->id);
            } else {
                return redirect(config('admin.directory').'/tx');
            }
        }

        if(!$transaction->hasPreview())
        {
            //Проверяем запущен ли процесс обработки заявки
            if($transaction->isStart() == false) {
                $transaction->start();
            }

            // Переадресация транзакций со статусами (Оплаченная)
            if($transaction->isToArchiveNotPreview()) {
                return redirect(config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
            }

            // Недопускаем неактивные транзакции и недаем возможность обрабатывать транзакцию
            // нескольким операратора
            // Если в заявке оператор, то мы записываем всех, то обрабатывает заявку
            if($transaction->hasNotActive() or $transaction->hasOperator())
            {
                if((int)iEXSetting('operator_panel_handler_orders') == 1 and $transaction->hasOperator()) {
                    return redirect(config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
                }

                if((int)iEXSetting('operator_panel_handler_orders') == 2 and (int)iEXSetting('count_change_operator') > 0 and $transaction->maxCountChangeOperator()) {
                    flash('Превышен лимит смены операторов', ['alert alert-info']);
                    return redirect()->to(admin_base_path('/tx'));
                }

                $transaction->addOperatorToHistory(Auth::id());
                $transaction->setOperator(Auth::id());

                return redirect(config('admin.directory').'/tx/'.$request->id);
            }


            //Мгновенная обработка транзакции по выбранным действиям
            if($transaction->getStatus() != 4) {
                $transaction->handlerAction();
            }
        }

        if($transaction->hasAction() and !$transaction->hasPreview())
        {
            if($transaction->hasAction() == 'logout') {
                return redirect(config('admin.directory').'/tx');
            }elseif($transaction->rulesRedirect() == 0) {
                return redirect(config('admin.directory').'/tx/'.$request->id);
            } else {
                return redirect(config('admin.directory').'/tx');
            }
        }

        $walletInfoIn = null;
        if($transaction->isStart() and $transaction->getWalletTransaction() != null) {
            $walletInfoIn = $transaction->getWalletTransaction();
        } elseif($transaction->isStart() and $transaction->getHistoryCode() != null) {
            $walletInfoIn = $transaction->getHistoryCode();
        }

        $taskInfo = $transaction->getTaskInfo();
        $taskReport = $transaction->getTaskReport();

        if($transaction->getStatus() != 10)
        {
            if($transaction->isValidityOrder() != null and $transaction->isValidityOrder()['level'] == 3) {
                $transaction->failedInvalidBid();

                return redirect(config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
            }
        }

        // Причины отклонения заявок
        $reasonRejection = TaskRejectionStatus::all()->pluck('name', 'id');
        // Причина отложения заявки
        $pendingOrderStatus = PendingOrderStatus::all()->pluck('name', 'id');


        $recalc = $transaction->getHistoryRecalculation();

        $display_in_price = $transaction->getDisplayInPrice(true);
        $display_out_price = $transaction->getDisplayOutPrice(true);


        // Новый курс
        $direction_exchange = $transaction->getDirectionExchange();

        $newCourse = [
            'amount' => $direction_exchange->course_value,
            'curs'  => $direction_exchange->exchange_rate
        ];
        if($taskInfo->city_id > 0)
        {
            $city_id = $direction_exchange->direction_exchange_cities->filter(function($item) use($taskInfo) {
                return $item['city_id'] == $taskInfo->city_id;
            })->first()->toArray();

            $convertFacade = ConvertFacade::call($direction_exchange)->all();

            if(isset($convertFacade['cities'])) {
                // Если используется работа с наличными
                $newCourse = [
                    'amount' => $convertFacade['cities'][$city_id['id']]['amount'],
                    'curs'  =>  $convertFacade['cities'][$city_id['id']]['curs'],
                ];
            }
        }

        $percent_diff = 0;

        try {
            $percent_diff = iex_number_format(($display_out_price / ($newCourse['amount'] * $display_in_price) - 1) * 100, 2);
        }catch (\Exception $exception) {
            //
        }

        $new_out_amount = 0;
        if($percent_diff > 0) {
            $new_out_amount = iex_number_format($display_out_price - ($display_in_price * $newCourse['amount']), $transaction->getCurrencyOut()->number_format);
        }


        $detail = $transaction->getItem();

        // Лог резерва
        $reserves_log = \App\Models\ReserveLog::whereIdTask($detail->id)->get();

        // Получение списка этапов
        if((int)iEXSetting('is_enabled_module_order_step')) {
            $order_steps = OrderStep::pluck('name', 'id');
        }

        // Список реквизитов
        $transit_requisites = TransitRequisite::where('id_currency', $detail->direction_exchange->id_currency1)->get();

        return view('admin.applications.tx.detail', [
            'transit_requisites'    =>      $transit_requisites ?? [],
            'order_steps'           =>      $order_steps ?? [],
            'reserves_log'          =>      $reserves_log,
            'disable_manual_button' =>      $transaction->isDisabledManualButton(),
            'direction_exchange'    =>      $direction_exchange,
            'newCourse'             =>      $newCourse,
            'percent_diff'          =>      $percent_diff,
            'new_out_amount'        =>      $new_out_amount,
            'reasonRejection'       =>      $reasonRejection,
            'pendingOrderStatus'    =>      $pendingOrderStatus,
            'validityOrder'         =>      $transaction->isValidityOrder(),
            'historyRecalculation'  =>      $recalc,
            'historyOperators'      =>      $transaction->getHistoryOperators(),
            'lastRecalcDate'         =>      ($recalc->count() > 0 ? $recalc->last()->created_at : null),
            'historyChat'           =>      $transaction->getHistoryChat(),
            'taskInfo'              =>      (isset($taskInfo) ? $transaction->getTaskInfo() : null),
            'taskReport'            =>      ($taskReport ?? null),
            'totalProfit'           =>      (isset($taskReport) ? $transaction->getTotalProfit() : 0),
            'txReceiving'           =>      ($history ?? []),
            'paymentIn'             =>      $transaction->getPaymentIn(),
            'codeIn'                =>      $transaction->getCodeIn(),
            'paymentOut'            =>      $transaction->getPaymentOut(),
            'codeOut'               =>      $transaction->getCodeOut(),
            'tasks_fields'          =>      $transaction->getTasksFields(),
            'transactions'          =>      $transaction->getTransactionById(),
            'bonus'                 =>      $transaction->rewardBonusString(),
            'referral'              =>      $transaction->referralBonus(),
            'referral_bonus'        =>      $transaction->referralBonusString(),
            'detail'                =>      $detail,
            'hasBalance'            =>      (!$transaction->hasPreview() ? $transaction->hasBalance() : null),
            'apiBalance'            =>      (!$transaction->hasPreview() ? $transaction->getAPIBalance() : null),
            'preview'               =>      $transaction->hasPreview(),
            'receive_wallet'        =>      $transaction->getAddresses(),
            'privateKeyAddress'     =>      $transaction->getPrivateKeyForAddress(),
            'account_number_field'  =>      $transaction->getFieldAccountNumber(),
            'checkPayment'          =>      $transaction->checkPayment(),
            'payOut'                =>      $transaction->getPay() ?: null,
            'walletInfoIn'          =>      $walletInfoIn,
            'leadTime'              =>      $transaction->getLeadTime(),
            'isAllowTransferReserve' =>     $transaction->isAllowTransferReserve(),
            'transferCommission'    =>      $transaction->getTransferCommission(),
            'is_hold'               =>      $transaction->isHold(),
            'uniqueToShot'          =>      $transaction->uniqueToShot(),
            'hold_timeout'          =>      $transaction->holdTimeOut(),
            'isVerifiedCard'        =>      $transaction->isVerifiedCard(),
            'display_in_price'      =>      $transaction->getDisplayInPrice(),
            'display_out_price'      =>      $transaction->getDisplayOutPrice(),
           // 'display_out_price_fee' =>      $transaction->getDisplayOutPriceFee(),
            'cardDetails'           =>      $transaction->getCardDetails()
        ]);
    }

    /**
     * Записываем статус в лог
     *
     * @param $id_task
     * @param $old
     * @param $new
     */
    protected function writeStatusLog($id_task, $old, $new, $course_display = null)
    {
        TaskStatusLog::create([
            'user_id'   =>  \auth()->id(),
            'id_task'   =>  $id_task,
            'old_status'    =>  $old,
            'new_status'    =>  $new,
            'course_display'  => $course_display ?? null
        ]);
    }
}
