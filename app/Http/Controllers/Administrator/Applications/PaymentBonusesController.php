<?php
namespace App\Http\Controllers\Administrator\Applications;


use App\Common\Packages\Payout\Facades\PayoutFacade;
use App\Http\Controllers\Controller;
use App\Jobs\SMSPayoutJob;
use App\Jobs\WithdrawalRequestJob;
use App\Mail\WithdrawalRequestMail;
use App\Models\Reserve;
use App\Models\ReserveLog;
use App\Models\UserBalance;
use App\Models\WithdrawalRequest;
use App\Models\WithdrawalRequestLog;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PaymentBonusesController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'payment_bonuses_paginate'
    ];

    /**
     * Список выплат
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {

        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $withdrawal = WithdrawalRequest::filter($request->all())->orderByDesc('id')
            ->paginate((int)iEXSetting('payment_bonuses_paginate'));

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('admin.application.payment_bonuses', $conditions);
        }



        $currencies = WithdrawalRequest::groupBy('id_currency')->get()->map(function($item) {
            return [
                'id'    =>  $item->id_currency,
                'value' => $item->currency->payment->name.' '.$item->currency->code_currency->name
            ];
        })->pluck('value', 'id');

        $statuses = [
            0 => 'Ожидает обработки',
            1 => 'Выполнено'
        ];

        return view('admin.applications.payment_bonuses.index', [
            'withdrawal'    =>  $withdrawal,
            'filter'        =>  $request->all(),
            'currencies'    =>  $currencies,
            'statuses'      =>  $statuses
        ]);
    }

    /**
     * Подробнее о выплате
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function details(Request $request)
    {
        // Детали выплаты
        $payout = PayoutFacade::call($request->id, [
            'actions'       =>  ($request->actions ?? null),
            'type'          =>  ($request->type ?? 'default'),
        ], $request);


        if(!empty($payout->hasAction()))
        {
            try {
                $payout->handlerAction();

                if($payout->hasAction() == 'failed') {
                    return redirect()->to(admin_base_path('/applications/payment_bonuses'));
                }elseif(in_array($payout->hasAction(), ['success', 'handler'])) {
                    return redirect()->to(admin_base_path('/applications/payment_bonuses/'.$request->id));
                }

            } catch (\Exception $exception) {
                flash($exception->getMessage(), ['alert alert-danger']);
                return redirect()->back();
            }
        }

        $histories = WithdrawalRequestLog::where('id_withdrawal_request', $request->id)->get();

       return view('admin.applications.payment_bonuses.details',  [
           'item'       =>  $payout->getPayouts(),
           'histories'  =>  $histories,
           'hasBalance' =>  $payout->hasBalance(),
           'apiBalance' => $payout->getAPIBalance()
       ]);
    }

    public function complete(Request $request)
    {
        $complete = WithdrawalRequest::find($request->id);
        $price_referral = 0;
        $price_reward = 0;

        if($complete->referral == 1) {
            $price_referral = $complete->user->user_balance->balance;
        }
        if($complete->rewards == 1) {
            $price_reward = $complete->user->user_balance->balance_reward;
        }

        $price = $price_referral + $price_reward;

        $int_price = $price;
        //$price = $price.' '. $complete->currency->code_currency->sign;

        //Лог выплаты
        WithdrawalRequestLog::create([
            'id_withdrawal_request' =>  $request->id,
            'text'                  =>  'Средства переведены на '
                .$complete->currency->payment->name.' '.$complete->currency->code_currency->name.', сумма выплаты '. $price,
            'amount'                =>  $price
        ]);

        $balance_array = [];
        if($complete->referral == 1) {
            $balance_array['balance'] = 0;
        }
        if($complete->rewards == 1) {
            $balance_array['balance_reward'] = 0;
        }

        //Обновление цен
        UserBalance::where('id_user', $complete->id_user)->update($balance_array);

        $complete->id_handler = Auth::id(); // Обработчик
        $complete->status = 1;
        $complete->save();

        //Снимаем деньги с резерва
        $reserve = Reserve::where('id_currency', $complete->id_currency);
        //Получаем текущую сумму
        $summa = $reserve->first()->summa;

        $result_price = ($summa - $int_price);
        $reserve->update([
            'summa' => iex_number_format($result_price, $reserve->first()->currency->number_format)
        ]);


        //В процессе успешно обработки заявки, мы меняем резервную цену
        ReserveLog::create([
            'id_currency'       => $complete->id_currency,
            'id_reserve'        => $reserve->first()->id,
            'summa'             => $int_price,
            'commission'        =>  0,
            'type'              => 'minus'
        ]);

        // Отложенная отправки писем
        if(iEXSetting('is_on_notify_referral_system') == 0) {
            dispatch(new WithdrawalRequestJob($complete, $price))
                ->delay(now()->addMinutes(10))->onQueue('low');
        }


        if($complete->user->is_report_referral_data == 1) {
            dispatch(new \App\Jobs\PartnerStatJob($complete->id_user))->onQueue('low')->delay(now()->addMinutes(5));
        }
        return redirect(config('admin.directory').'/applications/payment_bonuses');
    }

    /**
     * Удалить заявку на выплату
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $item = WithdrawalRequest::find($request->id);

        $user_balance = UserBalance::find($item->id_user);
        $balance_reward = $item->base_reward;
        $balance_referral = $item->base_referral;

        $user_balance->update([
            'balance'   =>  number_format(
                $user_balance->balance + $balance_referral, 2,'.',''
            ),
            'balance_reward'   =>  number_format(
                $user_balance->balance_reward + $balance_reward, 2,'.',''
            ),
        ]);
        $item->delete();
        return redirect(config('admin.directory').'/applications/payment_bonuses');
    }
}
