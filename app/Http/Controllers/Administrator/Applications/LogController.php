<?php
namespace App\Http\Controllers\Administrator\Applications;

use App\Http\Controllers\Controller;
use App\Models\AmlAnalysisLog;
use App\Models\LogAutoPayment;
use App\Models\LogCheckPay;
use App\Models\LogMerchant;
use App\Models\TaskStatusLog;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LogController extends Controller
{
    /**
     * Ключи для настроек (Лог мерчантов)
     *
     * @var array
     */
    protected $attributes_merchant = [
        'is_enabled_log_merchant'
    ];

    /**
     * Ключи для настроек (Лог статусов)
     *
     * @var array
     */
    protected $attributes_status = [
        'is_enabled_log_status'
    ];

    /**
     * Лог статусов заявок
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function statusLog(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            // Обновление конфига
            $array = [];
            foreach ($this->attributes_status as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $statusLog = TaskStatusLog::filter($request->all())->orderBy('id', 'desc')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action'))
        {
            $conditions = Arr::except($request->all(), ['send_action']);
            return \redirect()->route('admin.application.status-log', $conditions);
        }

        return view('admin.applications.logs.status', [
            'status_logs'   =>  $statusLog,
            'filter'    =>  $request->all()
        ]);
    }

    /**
     * Лог мерчантов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function merchantLog(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            // Обновление конфига
            $array = [];
            foreach ($this->attributes_merchant as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('admin.application.merchant-log', $conditions);
        }


        $logs = LogMerchant::filter($request->all())->orderByDesc('id')->paginate(20);

        return view('admin.applications.logs.merchant', [
            'logs' => $logs,
            'filter' => $request->all()
        ]);
    }

    /**
     * Лог автовыплат
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function autoPaymentLog(Request $request)
    {
        $logs = LogAutoPayment::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('admin.application.autopayment-log', $conditions);
        }

        return view('admin.applications.logs.autopayment', [
            'logs' => $logs,
            'filter'    =>  $request->all()
        ]);
    }

    /**
     * Лог проверки оплаты
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkpayLog(Request $request)
    {
        $logs = LogCheckPay::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('admin.application.checkpay-log', $conditions);
        }

        return view('admin.applications.logs.checkpay', [
            'logs' => $logs,
            'filter' => $request->all()
        ]);
    }

    /**
     * Лог AML проверок
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function amlLog(Request $request)
    {
        $logs = AmlAnalysisLog::orderByDesc('id')->paginate(20);

        return view('admin.applications.logs.aml', [
            'logs' => $logs
        ]);
    }
}
