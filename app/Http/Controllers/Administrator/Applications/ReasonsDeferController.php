<?php
namespace App\Http\Controllers\Administrator\Applications;

use App\Http\Controllers\Controller;
use App\Models\PendingOrderStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReasonsDeferController extends Controller
{

    public function index()
    {
        $items = PendingOrderStatus::orderBy('id')->get();

        return view('admin.applications.reasons-defer.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        return view('admin.applications.reasons-defer.create');
    }

    /**
     * Обработка и добавление новой причины
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            PendingOrderStatus::create($options);

            flash('Запись успешно добавлена', ['alert alert-success']);
        }

        return redirect()->to(
            admin_base_path('applications/reasons-defer')
        );
    }

    public function edit($id)
    {
        $item = PendingOrderStatus::find($id);

        return view('admin.applications.reasons-defer.edit', [
            'item' => $item
        ]);
    }

    /**
     * Обновление причины
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $item = PendingOrderStatus::find($id);

        $validator = Validator::make($request->all(), [
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $form_lang) {
                $options['name'][$key] = $request->get('name'.$form_lang['field']);
            }

            $item->update($options);

            flash('Данные успешно обновлены', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удаление причины
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = PendingOrderStatus::find($id);

        if(in_array($item->id, [1,2,3,4,5])) {
            flash('Нельзя удалять эту причину', ['alert alert-danger']);

            return redirect()->to(
                admin_base_path('applications/reasons-defer')
            );
        }

        if($item->is_not_delete == 1) {
            flash('Нельзя удалять эту причину', ['alert alert-danger']);
        } else {
            flash('Выбранная причина успешно удалена', ['alert alert-success']);
            $item->delete();
        }

        return redirect()->to(
            admin_base_path('applications/reasons-defer')
        );
    }
}
