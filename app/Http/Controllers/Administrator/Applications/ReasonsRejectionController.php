<?php
namespace App\Http\Controllers\Administrator\Applications;

use App\Http\Controllers\Controller;
use App\Models\TaskRejectionStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReasonsRejectionController extends Controller
{

    public function index()
    {
        $items = TaskRejectionStatus::all();

        return view('admin.applications.reasons-rejection.index', [
            'items' => $items
        ]);
    }

    /**
     * Обработка и добавление новой причины
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {


            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            TaskRejectionStatus::create($options);
        }

        return redirect()->back();
    }

    public function edit($id)
    {
        $item = TaskRejectionStatus::find($id);

        return view('admin.applications.reasons-rejection.edit', [
            'item' => $item
        ]);
    }

    /**
     * Обновление причины
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $item = TaskRejectionStatus::find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:200'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $form_lang) {
                $options['name'][$key] = $request->get('name'.$form_lang['field']);
            }

            $item->update($options);

            flash('Данные успешно обновлены', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удаление причины
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $item = TaskRejectionStatus::find($id);
        if($item->is_not_delete == 1) {
            flash('Нельзя удалять эту причину', ['alert alert-danger']);
        } else {
            flash('Выбранная причина успешно удалена', ['alert alert-success']);
            $item->delete();
        }

        return redirect()->back();
    }
}
