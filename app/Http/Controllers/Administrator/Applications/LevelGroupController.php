<?php
namespace App\Http\Controllers\Administrator\Applications;

use App\Http\Controllers\Controller;
use App\Models\OperatorLevelGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LevelGroupController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Создаем дубликат
        if($request->has('duplicate') and $request->get('duplicate') == true)
        {
            $item = OperatorLevelGroup::find($request->get('id'));
            $newParser = $item->replicate();
            $newParser->save();

            flash('Дубликат '.$item->name.' успешно создан', ['alert alert-success']);
            return redirect()->back();
        }

        $groups = OperatorLevelGroup::orderByDesc('id')->paginate(20);
        return view('admin.applications.level.groups.index', [
            'groups' =>  $groups
        ]);
    }

    /**
     * Форма добавления ссылок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.applications.level.groups.create');
    }

    /**
     * Обработка и добавление групп
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link = OperatorLevelGroup::create([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'from_limit'        => $request->has('from_limit') ? $request->get('from_limit') : null,
                'to_limit'          => $request->has('to_limit') ? $request->get('to_limit') : null,
                'status'            => $request->has('status') ? 1 : 0
            ]);

            flash("Группа {$link->name} успешно добавлена", ['alert alert-success']);
            return redirect()->route('levels-group.index');
        }
    }

    /**
     * Форма редактирования групп
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = OperatorLevelGroup::findOrFail($id);
        return view('admin.applications.level.groups.edit',compact('item'));
    }

    /**
     * Обработка и обновления групп
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $link = OperatorLevelGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link->update([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'from_limit'        => $request->has('from_limit') ? $request->get('from_limit') : null,
                'to_limit'          => $request->has('to_limit') ? $request->get('to_limit') : null,
                'status'            => $request->has('status') ? 1 : 0
            ]);

            flash("Группа {$link->name} успешно обновлена", ['alert alert-success']);
            return redirect()->route('levels-group.index');
        }
    }

    /**
     * Удаление ссылок
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = OperatorLevelGroup::findOrFail($id);

        if($item->operation_level->count() > 0) {
            flash('Выбранную группу удалить невозможно, К нему привязаны лимиты', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Группа {$item->name} успешно удалена", ['alert alert-success']);
        $item->delete();

        return redirect()->back();
    }
}
