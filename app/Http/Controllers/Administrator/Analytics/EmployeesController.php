<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.06.2018
 * Time: 18:42
 */

namespace App\Http\Controllers\Administrator\Analytics;


use App\Http\Controllers\Controller;
use App\Models\Employees;
use App\Models\EventEmployees;
use App\Models\FineEmployees;
use App\Models\Requisites;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class EmployeesController extends Controller
{
    /**
     * Список сотрудников
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $employees = Employees::orderBy('id', 'desc')->get();
        $admins = User::role(Role::all())->get();

        if($request->getMethod() == 'POST')
        {
            if($request->actions == 'fine' and $request->employees != 0)
            {
                $number = (isset($request->number) ? $request->number : 0);
                $amount = (isset($request->amount) ? $request->amount : 0);

                //Добавляем результат в базу
                $fine = new FineEmployees();
                $fine->id_employees = $request->employees;

                if($request->amount_task == 1 and $request->number)
                {
                    $task  =Task::find($request->number);
                    $rub = convert_to_rub($task->direction_exchange->currency1->code_currency->name, $task->give_price);

                    $fine->amount = $amount + $rub;
                } else {
                    $fine->amount = $amount;
                }
                $fine->number = $number;
                $fine->text = (isset($request->text) ? $request->text : null);
                $fine->save();
            }

            return redirect()->back();
        }


        return view('admin.analytics.employees.index',[
            'employees'     =>  $employees,
            'admins'        =>  $admins,
            'last_month'    =>  (isset($request->last_month) ? true : false)
        ]);
    }

    /**
     * Изменить данные сотрудника
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $item = Employees::find($request->id);

        if($request->getMethod() == 'POST')
        {
            $item->update([
                'started_work'      =>  $request->started_work,
                'job_from'          =>  $request->job_from,
                'job_to'            =>  $request->job_to,
                'salary'            =>  $request->salary,
                'percent'           =>  $request->percent,
                'bonus_rub'         =>  $request->bonus_rub,
                'status'            =>  $request->status,
            ]);

            return redirect()->back();
        }

        return view('admin.analytics.employees.edit',[
            'item'  =>  $item
        ]);
    }

    /**
     * Событии менеджеров
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function event(Request $request)
    {
        if(isset($request->delete)) {
            EventEmployees::find($request->delete)->delete();
            return redirect()->back();
        }

        $month = Carbon::today()->startOfMonth();
        $month_end = Carbon::today()->endOfMonth();

        if($request->has('last_month')) {
            $month = Carbon::today()->subMonth(1)->startOfMonth();
            $month_end = Carbon::today()->subMonth(1)->endOfMonth();
        }

        $event = EventEmployees::where('id_employees', $request->id)
            ->whereBetween('created_at', [$month, $month_end])->orderBy('id','desc')->get();
        $user = Employees::find($request->id)->user;

        return view('admin.analytics.employees.event',[
            'events'    =>  $event,
            'user'      =>  $user
        ]);
    }

    /**
     * Список штрафов оператора
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fine(Request $request)
    {
        if(isset($request->delete)) {
            FineEmployees::find($request->delete)->delete();
            return redirect()->back();
        }

        $month = Carbon::today()->startOfMonth();
        $month_end = Carbon::today()->endOfMonth();

        if($request->has('last_month')) {
            $month = Carbon::today()->subMonth(1)->startOfMonth();
            $month_end = Carbon::today()->subMonth(1)->endOfMonth();
        }


        $fines = FineEmployees::where('id_employees', $request->id)
            ->whereBetween('created_at', [$month, $month_end])->orderBy('id','desc')->get();
        $user = Employees::find($request->id)->user;

        return view('admin.analytics.employees.fine',[
            'fines' =>  $fines,
            'user'  =>  $user
        ]);
    }

    /**
     * Банковские карты сотрудника
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cards(Request $request)
    {
        $user = Employees::find($request->id)->user;
        $items = Requisites::where('id_user', $user->id)->whereDate('created_at', '>=', Carbon::today()->startOfMonth())->cursor();

        return view('admin.analytics.employees.cards', [
            'items' =>  $items,
            'user'  =>  $user
        ]);
    }

    /**
     * Добавить нового сотрудника
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $admins = User::role(Role::all())->get();

        if($request->getMethod() == 'POST')
        {
            Employees::create([
                'id_user'       =>  $request->manager,
                'started_work'  =>  (isset($request->started_work) ? $request->started_work : null),
                'job_from'      =>  $request->job_from,
                'job_to'        =>  $request->job_to,
                'salary'        =>  $request->salary,
                'percent'       =>  $request->percent,
                'bonus_rub'     =>  $request->bonus_rub,
                'status'        =>  $request->status,
            ]);

            return redirect()->back();
        }


        return view('admin.analytics.employees.create', [
            'admins'    =>  $admins
        ]);
    }

    /**
     * Отчет по сотрудникам
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report(Request $request)
    {
        $admins = User::role(Role::all())->get();

        return view('admin.analytics.employees.report',[
            'admins' =>  $admins
        ]);
    }
}
