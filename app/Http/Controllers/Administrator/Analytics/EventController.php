<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 14.06.2018
 * Time: 0:33
 */

namespace App\Http\Controllers\Administrator\Analytics;


use App\Http\Controllers\Controller;
use App\Models\EventReserve;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EventController extends Controller
{
    /**
     * Событии по резервам
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reserve(Request $request)
    {
        $events = EventReserve::filter($request->all())->orderBy('id', 'desc')->paginate();

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('event_reserve.index', $conditions);
        }

        // Тип события
        $types = collect([
               ['key' => 1, 'value' => 'Пополнен'],
               ['key' => 0, 'value' => 'Уменьшен']
           ])->pluck('value', 'key');

        // Резервы
        $currencies = EventReserve::groupBy('id_currency')->get()->map(function ($item) {
            return [
                'value' => sprintf('%s %s', $item->currency->payment->name, $item->currency->code_currency->name),
                'id' => $item->currency->id
            ];
        })->pluck('value', 'id');


        return view('admin.analytics.event_reserve',[
            'items' =>  $events,
            'types' => $types,
            'filter' => $request->all(),
            'currencies' => $currencies
        ]);
    }
}