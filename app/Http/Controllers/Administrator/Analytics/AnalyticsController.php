<?php
namespace App\Http\Controllers\Administrator\Analytics;

use App\Http\Controllers\Controller;
use App\Models\ArchiveReport;
use App\Models\DirectionExchange;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Adapter\Local;
use Spatie\Analytics\Period;
use Spatie\Permission\Models\Role;

class AnalyticsController extends Controller
{
    /**
     * Общие суммы обменов в (USD or RUB)
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function exchange(Request $request)
    {
        $commonAmount = \DB::table("tasks_convert_log")
            ->orderBy('created_at', 'desc')
            ->groupBy(\DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->selectRaw('*, sum(to_usd) as usd, sum(to_rub) as rub')
            ->paginate(20);


//        $commonAmount = TaskConvertLog::select('id', 'to_usd', 'to_rub','created_at')->paginate(5)
//            ->groupBy(function($date) {
//                return Carbon::parse($date->created_at)->format('d.m.Y'); // grouping by years
//            })->paginate();

        return view('admin.analytics.exchange',[
            'items' =>  $commonAmount
        ]);
    }

    /**
     * Сводный отчет
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function summary(Request $request)
    {
        $archive = ArchiveReport::orderBy('id', 'desc')->paginate(20);
        $storage = Storage::disk('archive');

        // Очистить архив отчетов
        if($request->has('clear') and $request->get('clear') == 'archive_reserve')
        {
            $list = ArchiveReport::all();
            foreach ($list as $item) {
                // Удаляем файл если найден
                if ($storage->exists($item->name))
                    $storage->delete($item->name);
                $item->delete();
            }

            flash('Архив отчетов успешно очищен', ['alert alert-success']);
            return redirect()->back();
        }

        //Скачивание файла
        if(isset($request->download)) {
            if($storage->exists($request->download))
                return $storage->download($request->download)->send();
        }

        //Открываем документ
        if(isset($request->open)) {
            if ($storage->exists($request->open)) {
                return \response()->file(storage_path('app/archive/'.$request->open), [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' =>  'inline; filename="'.$request->open.'"'
                ]);
            } else {
                return redirect(config('admin.directory') . '/analytics/summary');
            }
        }

        //Обновление настроек
        if($request->action == 'settings')
        {
            iEXSetting([
                'is_auto_reserves_report'   =>  $request->has('is_auto_reserves_report') ? $request->get('is_auto_reserves_report') : 0,
                'report_analytics_status'   =>  $request->has('report_analytics_status') ? $request->get('report_analytics_status') : 0,
                'report_analytics_email'  =>  $request->has('report_analytics_email') ? $request->get('report_analytics_email') : null
            ]);

            return redirect()->back();
        }


        $stat_google = null;
        if(isset($request->stat_google)) {
            $stat_google = $request->stat_google;
        }

        if($stat_google == '30d') {
            //Какие страницы просматривают пользователи
            $fetchMostVisitedPages = $this->jsonToArray('fetchMostVisitedPages_30d.json');
            //С каких сайтов идут переходы
            $fetchTopReferrers     =  $this->jsonToArray('fetchTopReferrers_30d.json');
            //Посещаемость
            $fetchUserTypes       =   $this->jsonToArray('fetchUserTypes_30d.json');
        }else {
            $fetchMostVisitedPages  =  $this->jsonToArray('fetchMostVisitedPages_7d.json');
            $fetchTopReferrers      =  $this->jsonToArray('fetchTopReferrers_7d.json');
            $fetchUserTypes         =  $this->jsonToArray('fetchUserTypes_7d.json');
        }

        // Обзор аудитории
        $audienceReview = [];
        if(iEXSetting('is_enabled_google_analytics') == true)
        {
            $audienceReview = Cache::remember('audience_review_v2', Carbon::now()->endOfDay()->addSecond(), function () {
                $startDate = Carbon::now()->subWeek();
                $endDate = Carbon::now();
                $analyticsData = \Analytics::performQuery(Period::create($startDate, $endDate),
                    'ga:', [
                        'metrics' => 'ga:users,ga:newUsers,ga:sessions,ga:sessionsPerUser,ga:pageviews,ga:pageviewsPerSession,ga:avgSessionDuration,ga:bounceRate',
                        'dimensions' => 'ga:pageTitle'
                    ]);
                return $analyticsData;
            });
        }

        return view('admin.analytics.summary',[
            'audience_review'           =>      $audienceReview,
            'popular_direction'         =>      [],
            'popular_user'              =>      [],
            'items'                     =>      $archive,
            'settings'                  =>      iEXSetting(),
            'stat_google'               =>      $stat_google,
            'fetchMostVisitedPages'     =>      $fetchMostVisitedPages,
            'fetchTopReferrers'         =>      $fetchTopReferrers,
            'fetchUserTypes'            =>      $fetchUserTypes
        ]);
    }

    protected function jsonToArray($path) {
        return json_decode(\File::get(public_path('admin-assets/statistics/'.$path)), true);
    }
}