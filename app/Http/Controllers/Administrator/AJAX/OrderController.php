<?php
namespace App\Http\Controllers\Administrator\AJAX;

use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Models\Task;
use App\Models\VerificationCard;
use App\Models\WithdrawalRequest;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{
    /**
     * Получение списка заявок
    */
    public function index()
    {
        // Список счетчиков для панели управления
        $count = [
            'count'             =>  Task::whereIn('status', [3,7])->where('is_frozen', 0)->count(),
            'payment_bonuses'   =>  WithdrawalRequest::where('status','=',0)->count(),
            'new_postponed'     =>  Task::where('status', 8)->count(),
            'reserve_request'   =>  '',
            'verification_card' =>  VerificationCard::where('status', 0)->count(),
            'reviews_count'     =>  Review::where('status', '=', 0)->count()
        ];

        $order = Task::with(['direction_exchange' => function($q) {
            $q->select('id','id_currency1', 'id_currency2');
        }, 'direction_exchange.currency1' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'direction_exchange.currency1.payment' => function($q) {
            $q->select('id','name');
        }, 'direction_exchange.currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        },'direction_exchange.currency2' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'direction_exchange.currency2.payment' => function($q) {
            $q->select('id','name');
        }, 'direction_exchange.currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }])->select('id', 'id_direction_exchange', 'give_price', 'receiving_price', 'created_at', 'is_frozen')->orderByDesc('id')->where('is_frozen', '=', 0)
            ->limit(20)
            ->whereIn('status', [3, 7]);


        $array = collect();

        $order->chunk(100, function($items) use($array){
            foreach ($items as $value) {
                $array->add([
                    'id' => $value->id,
                    'amount' => $value->give_price . ' ' . $value->direction_exchange->currency1->code_currency->name . ' → ' . $value->receiving_price . ' ' . $value->direction_exchange->currency2->code_currency->name,
                    'name' => direction_name($value, true),
                    'created_at' => Carbon::parse($value->created_at)->diffForHumans(),
                    'status_int' => $value->status
                ]);
            }
        });

        return response()->json([
            'attributes' => [
                'counts' => $count,
                'live_orders' => $array
            ]
        ]);
    }
}
