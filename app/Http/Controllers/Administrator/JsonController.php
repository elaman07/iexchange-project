<?php
namespace App\Http\Controllers\Administrator;


use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use App\Models\Task;
use App\Models\TaskMessage;
use App\Models\TaskStatus;
use App\Models\User;
use App\Models\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class JsonController extends Controller
{

    public function list_task(Request $request)
    {
        $list = Task::orderBy('id', 'desc')->where('is_frozen', '=', 0);

        if(iEXSetting('task_time_over') != 1) {
            $list->where('status', '!=', 1);
        }

        if(iEXSetting('task_denied') != 1) {
            $list->where('status', '!=', 5);
        }

        if(iEXSetting('task_success') != 1) {
            $list->where('status', '!=', 4);
        }

        if(iEXSetting('task_time') != 1) {
            $list->where('status','!=',2);
        }

        if(isset($request->status)) {
            $list->where('status', $request->status);
        }


        $array = [];
        foreach ($list->get() as $value)
        {
            $carbon = new Carbon($value['created_at']);
            $in = in_array(Str::studly($value->direction_exchange->currency1->payment->name), config('transaction.check_payment'));

            $return             =   [];
            $return['status']   =   $value->task_status->name;
            $return['status_int']   = $value->status;
            $return['public_id']    =   $value->public_id > 0 ? $value->public_id : $value->id;
            $return['auto_id']  =   $value->id;
            $return['id']       =   $value->id;
            $return['name']     =   $value->direction_exchange->currency1->payment->name.' '.$value->direction_exchange->currency1->code_currency->name.' → '.
                $value->direction_exchange->currency2->payment->name.' '.$value->direction_exchange->currency2->code_currency->name;
            $return['created']     =   Carbon::parse($value->created_at)->diffForHumans();
            $return['button']       =  '<a href="'.config('admin.directory').'/applications/details/'.$value->id.'?actions=arhive" class="btn btn-sm btn-info">Детали заявки</a>';
            $return['hidden']       =   false;
            $return['is_scam']      =   (isset($value->task_info) and $value->task_info->is_scam == 1);

            $return['check_payment']    =   ($in and $value->wallet_transaction == null);
            $return['payment_in_name']  =   $value->direction_exchange->currency1->payment->name;

            array_push($array, $return);
        }


        return response()->json($array);
    }

    public function list_task_statuses()
    {
        $data = TaskStatus::all();

        $array  =   [];
        foreach ($data as $item) {
            $list = [];
            $list['value']  = $item->id;
            $list['text']   =   $item->name;

            array_push($array, $list);
        }

        return response()->json($array);
    }

    public function users() {

        $all = User::orderBy('id', 'desc')->get();

        $array = [];
        foreach ($all as $item)
        {
            $user = [];
            $user['id']             =       $item->id;
            $user['name']           =       $item->name;
            $user['email']          =       $item->email;
            $user['last_login']     =       ($item->last_login != null ? Carbon::parse($item->last_login)->diffForHumans() : 'Не определен');
            $user['last_activity']  =       ($item->last_activity != null ? Carbon::parse($item->last_activity)->diffForHumans() : 'Не определен');
            $user['created']        =       Carbon::parse($item->created_at)->translatedFormat('d M Y H:i');
            $user['roles']          =       $item->roles()->pluck('name')->implode(', ');
            $user['action']         =   '';
            if(Auth::user()->hasRole('Разработчик')) {
                $user['action']     =   '

                <md-button class="md-icon-button" ng-href="'.config('admin.directory').'/account/users/'.$item->id.'/edit">
                        <i class="fa fa-fw fa-pencil"></i>
                    </md-button>


                ';
            }

            array_push($array, $user);
        }



        return response()->json($array);
    }


    public function live_tx(Request $request)
    {
        $list = Task::query()->with(['direction_exchange' => function ($q) {
            $q->select('id', 'id_currency1', 'id_currency2');
        }, 'direction_exchange.currency1' => function ($q) {
            $q->select('id', 'hold_in_hours', 'id_code_currency', 'id_payment');
        },'direction_exchange.currency1.code_currency' => function ($q) {
            $q->select('id', 'name');
        },'direction_exchange.currency1.payment' => function ($q) {
            $q->select('id', 'name');
        }, 'direction_exchange.currency2' => function ($q) {
            $q->select('id', 'hold_in_hours', 'id_code_currency', 'id_payment');
        },'direction_exchange.currency2.code_currency' => function ($q) {
            $q->select('id', 'name');
        },'direction_exchange.currency2.payment' => function ($q) {
            $q->select('id', 'name');
        }, 'task_status' => function ($q) {
            $q->select('id', 'name');
        }, 'task_info' => function ($q) {
            $q->select('id', 'is_scam');
        }, 'task_messages' => function ($q) {
            $q->select('id', 'is_view')->where('is_view', '=', 0);
        }])
            ->select('id', 'public_id', 'scans', 'created_at', 'is_frozen', 'status', 'id_direction_exchange', 'in_flow_funds', 'is_hold', 'give_price', 'started_at','scan_name', 'receiving_price')
            ->orderBy('id', 'desc')->where('is_frozen', 0)
            ->whereIn('status', [3, 7])->limit(30)->get();

        $user_id = Auth::id();
        $array['task'] = [];
        foreach ($list as $value)
        {
            $carbon = new Carbon($value['created_at']);

            // Проверяем замороженные транзакции
            $timeout = Carbon::parse($value['started_at'])->addHours($value['direction_exchange']['currency1']['hold_in_hours']);
            $now = Carbon::now();

            $scans = $value['scan_name'] ?? 'Нет операторов';

            $hold_timeout = null;
            if($timeout->gt($now)) {
                $hold_timeout = 'Заморожено на '.($timeout->diffInMinutes($now) <= 60 ? $timeout->diffInMinutes($now). ' м.' : $timeout->diffInHours($now). ' ч.');
            }

            $return                 =   [];
            $return['auth_user_validate'] = $user_id == $value['scans'];
            $return['auth_id']      =   $user_id;
            $return['in_flow_funds']       =   $value['in_flow_funds'];
            $return['is_hold']      = ($timeout->gt($now) and $value['is_hold']);
            $return['hold_timeout'] =   $hold_timeout;
            $return['scans_name']   =   $scans;
            $return['amount']       =   $value['give_price'].' '.$value['direction_exchange']['currency1']['code_currency']['name'].' → '.
                $value['receiving_price'].' '.$value['direction_exchange']['currency2']['code_currency']['name'];
            $return['scans']        =   ($value['scans'] > 0) ? $value['scans'] : 0;
            $return['status']       =   $value['task_status']['name'];
            $return['status_int']   =   $value['status'];
            $return['id']           =   $value['id'];
            $return['public_id']    =   $value['public_id'] > 0 ? $value['public_id'] : $value['id'];
            $return['name']         =   $value['direction_exchange']['currency1']['payment']['name'].' '.$value['direction_exchange']['currency1']['code_currency']['name'].' → '.
                $value['direction_exchange']['currency2']['payment']['name'].' '.$value['direction_exchange']['currency2']['code_currency']['name'];
            $return['created']      =   Carbon::parse($value['created_at'])->diffForHumans();
            $return['hidden']       =   false;
            $return['is_scam']      =   (isset($value['task_info']) and $value['task_info']['is_scam'] == 1);
            $return['payment_in_name']  =   $value['direction_exchange']['currency1']['payment']['name'];
            if((int)iEXSetting('is_enable_order_online_chat') == 1) {
                $return['new_message']    = $value->task_messages->count();
            }


            array_push($array['task'], $return);
        }

      //  $task = Task::orderBy('id', 'desc')->where('status', 8)->limit(10);

//        $array['deferred'] = [];
//        foreach ($task->get() as $value)
//        {
//            $carbon = new Carbon($value['created_at']);
//
//            $scans = 'Нет операторов';
//            if($value->scans > 0) {
//                $user = User::find($value->scans);
//                $scans = $user->name;
//            }
//
//            $return                 =   [];
//            $return['auth_id']      =   Auth::id();
//            $return['scans_name']   =   $scans;
//            $return['amount']       =   $value->give_price.' '.$value->direction_exchange->currency1->code_currency->name.' → '.
//                $value->receiving_price.' '.$value->direction_exchange->currency2->code_currency->name;
//            $return['scans']        =   ($value->scans > 0) ? $value->scans : 0;
//            $return['status']       =   $value->task_status->name;
//            $return['status_int']   =   $value->status;
//            $return['id']           =   $value->id;
//            $return['public_id']    =   $value->public_id > 0 ? $value->public_id : $value->id;
//            $return['name']         =   $value->direction_exchange->currency1->payment->name.' '.$value->direction_exchange->currency1->code_currency->name.' → '.
//                $value->direction_exchange->currency2->payment->name.' '.$value->direction_exchange->currency2->code_currency->name;
//            $return['created']      =   Carbon::parse($value->created_at)->diffForHumans();
//
//            array_push($array['deferred'], $return);
//        }


//        $withdrawal = WithdrawalRequest::where('status', 0)->orderBy('id', 'desc')->get();
//
//        $array['payment_bonuses'] = [];
//        foreach ($withdrawal as $value)
//        {
//            $return                 =   [];
//            $return['id']           =   $value->id;
//            $return['name']         =   $value->user->name;
//            $return['created']      =   Carbon::parse($value->created_at)->diffForHumans();
//
//            array_push($array['payment_bonuses'], $return);
//        }


        $array['status'] = isJobOffline() == 0;
        $array['other'] = [
            'count'             =>  Task::whereIn('status', [3, 7])->where('is_frozen', 0)->count(),
            'payment_bonuses'   =>  WithdrawalRequest::where('status','=',0)->count(),
            'new_postponed'     =>  Task::where('status', 8)->count(),
            'user_messages'     => TaskMessage::where('is_view', '=', 0)->count(),
        ];

        return response()->json($array);
    }


    public function deferred_task(Request $request)
    {
        $task = Task::orderBy('id', 'desc')->where('status', 8)->limit(10);

        $array = [];
        foreach ($task->get() as $value)
        {
            $carbon = new Carbon($value['created_at']);

            $scans = 'Нет операторов';
            if($value->scans > 0) {
                $user = User::find($value->scans);
                $scans = $user->name;
            }

            $return                 =   [];
            $return['auth_id']      =   Auth::id();
            $return['scans_name']   =   $scans;
            $return['amount']       =   $value->give_price.' '.$value->direction_exchange->currency1->code_currency->name.' → '.
                $value->receiving_price.' '.$value->direction_exchange->currency2->code_currency->name;
            $return['scans']        =   ($value->scans > 0) ? $value->scans : 0;
            $return['status']       =   $value->task_status->name;
            $return['status_int']   =   $value->status;
            $return['public_id']    =   $value->public_id > 0 ? $value->public_id : $value->id;
            $return['id']           =   $value->id;
            $return['name']         =   $value->direction_exchange->currency1->payment->name.' '.$value->direction_exchange->currency1->code_currency->name.' → '.
                $value->direction_exchange->currency2->payment->name.' '.$value->direction_exchange->currency2->code_currency->name;
            $return['created']      =   Carbon::parse($value->created_at)->diffForHumans();

            array_push($array, $return);
        }


        return response()->json($array);
    }
}
