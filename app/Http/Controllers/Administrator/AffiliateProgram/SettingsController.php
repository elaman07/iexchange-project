<?php
namespace App\Http\Controllers\Administrator\AffiliateProgram;

use App\Http\Controllers\Controller;
use App\Jobs\BalanceUpdateJob;
use App\Models\CodeCurrency;
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'enabled_referral_system',
        'referral_storage_periods',
        'referral_lifetime_day',
        'minimum_bonus_payout',
        'is_enabled_not_referral_bonus',
        'is_on_notify_referral_system',
        'referral_min_bonus',
        'referral_max_bonus',
        'referral_bonus_limit_day',
        'referral_bonus_limit_month',
        'is_enabled_referral_logs',
        'referral_bonus_from',
        'referral_bonus_to',
        'referral_bonus_range',
        'type_partner_deductions',
        'referral_system_type_exchange',
        'enabled_referral_system_space_indent'
    ];

    /**
     * Настройка реферальной системы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function index(Request $request)
    {
        if($request->method() == 'POST')
        {
            $name = null;
            $item = CodeCurrency::find($request->get('currency_id'));

            $env = new DotenvEditor();
            $env->changeEnv([
                'PAYOUT_CURRENCY_ID'         =>  $item->id,
                'PAYOUT_CURRENCY_NAME'       =>  remove_all_spaces($item->name),
                'PAYOUT_CURRENCY_SIGN'       =>  remove_all_spaces($request->get('currency_payout_sign')),
                'PAYOUT_CURRENCY_SIGN_ALT'   =>  remove_all_spaces($request->get('currency_payout_sign_alt')),
                'PAYOUT_CURRENCY_POSITION'   =>  remove_all_spaces($request->get('currency_payout_position'))
            ]);

            // Обновление цен
//            dispatch(
//                (new BalanceUpdateJob())
//            )->delay(now()->addMinute())->onQueue('low');


            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item)
            {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);


            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $codes = CodeCurrency::pluck('name', 'id');

        return view('admin.affiliate.settings', [
            'codes' => $codes
        ]);
    }
}
