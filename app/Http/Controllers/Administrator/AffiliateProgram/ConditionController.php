<?php
namespace App\Http\Controllers\Administrator\AffiliateProgram;

use App\Http\Controllers\Controller;
use App\Models\SettingsReferrals;
use App\Models\SettingsReward;
use Illuminate\Http\Request;

class ConditionController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowLocaleOptionsReferral = [
        'referral_system_text',
        'referral_system_text_footer'
    ];

    protected array $allowLocaleOptionsCashback = [
        'cashback_text',
        'cashback_footer'
    ];

    protected array $allowLocaleOptionsMonitoring = [
        'monitoring_text'
    ];


    /**
     * Условия партнерской программы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function referral(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptionsReferral as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }
            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptionsReferral)->all();
            iEXContentLanguage($locale_data);

            return redirect()->back();
        }


        return view('admin.affiliate.condition.referral');
    }

    /**
     * Условия кэшбэка
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function cashback(Request $request)
    {
        if($request->getMethod() == 'POST')
        {

            $options_locale = [];
            foreach ($this->allowLocaleOptionsCashback as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptionsCashback)->all();
            iEXContentLanguage($locale_data);

            return redirect()->back();
        }


        return view('admin.affiliate.condition.cashback');
    }

    /**
     * Условия для мониторингов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function monitoring(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptionsMonitoring as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }
            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptionsMonitoring)->all();
            iEXContentLanguage($locale_data);

            return redirect()->back();
        }

        return view('admin.affiliate.condition.monitoring');
    }
}
