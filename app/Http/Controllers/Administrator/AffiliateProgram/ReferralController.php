<?php
namespace App\Http\Controllers\Administrator\AffiliateProgram;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Common\Packages\ReferralSystem\Models\ReferralProgram;
use App\Common\Packages\ReferralSystem\Models\ReferralRelationship;
use App\Http\Controllers\Controller;
use App\Models\ReferralInfoLog;
use App\Models\ReferralStatistics;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ReferralController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_referral_disable_history_orders',
        'is_referral_disable_profit_ref',
        'is_referral_disable_involved_clients',
        'is_referral_disable_profit_money'
    ];

    /**
     * Список реферальных программ
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $referrals = ReferralProgram::orderBy('id', 'asc')->get();
        return view('admin.affiliate.referral.index', compact('referrals'));
    }

    /**
     * Форма добавления новой программы
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create()
    {
        return view('admin.affiliate.referral.create');
    }

    /**
     * Обработка и добавление новой программы
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:referral_programs|max:100|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'type' => 'required',
            'is_reg' => 'required',
            'percent' => 'required|numeric',
            'count' => 'required|numeric'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {
            $options = [
                'name' => $request->get('name'),
                'type' => $request->get('type'),
                'is_reg' => $request->get('is_reg'),
                'percent' => $request->get('percent'),
                'count' => $request->get('count')
            ];
            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
            }

            // Удаляем кэш
            cache()->forget('static-page-referral');

            ReferralProgram::create($options);
            flash(sprintf(__('Программа %s успешно добавлено'), $request->get('name')), ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Форма редактирования программы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $referral = ReferralProgram::findOrFail($id);
        return view('admin.affiliate.referral.edit', compact('referral'));
    }

    /**
     * Обработка и обновление программы
     *
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update($id, Request $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'type' => 'required',
            'is_reg' => 'required',
            'percent' => 'required|numeric',
            'count' => 'required|numeric'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            $options = [
                'name' => $request->get('name'),
                'type' => $request->get('type'),
                'is_reg' => $request->get('is_reg'),
                'percent' => $request->get('percent'),
                'count' => $request->get('count'),
                'style_width'   => $request->has('style_width') ? $request->get('style_width') : null
            ];
            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
            }

            // Удаляем кэш
            cache()->forget('static-page-referral');

            ReferralProgram::findOrFail($id)->update($options);
            flash(sprintf(__('Программа %s успешно обновлена'), $request->get('name')), ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удалить программу
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

       $item = ReferralProgram::findOrFail($id);
       $exists = ReferralLink::whereReferralProgramId($id)->exists();

       if($exists) {
           flash(__('Невозможно удалить выбранную программу'), ['alert alert-danger']);
       } else {
           flash(__('Программа успешно удалена'), ['alert alert-success']);
           $item->delete();
       }

       return redirect()->route('referral.index');
    }

    /**
     * Реферальная статистика
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistics()
    {
        // Партнерских переходов
        $transitions = ReferralStatistics::where('is_archive', '=', 0)->count();
        $transitions_today = ReferralStatistics::where('is_archive', '=', 0)->whereDate('created_at', Carbon::today())->count();
        $transitions_week = ReferralStatistics::where('is_archive', '=', 0)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $transitions_month = ReferralStatistics::where('is_archive', '=', 0)->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();

        // Партнерских регистраций
        $register = ReferralLink::count();
        $register_today = ReferralLink::whereDate('created_at', Carbon::today())->count();
        $register_week = ReferralLink::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $register_month = ReferralLink::whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();

        // Партнерских обменов
        $exchange = ReferralLog::count();
        $exchange_today = ReferralLog::whereDate('created_at', Carbon::today())->count();
        $exchange_week = ReferralLog::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $exchange_month = ReferralLog::whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();


        return view('admin.affiliate.referral.statistics', [
            'transitions' => $transitions,
            'transitions_today' => $transitions_today,
            'transitions_week' => $transitions_week,
            'transitions_month' => $transitions_month,
            'register'  =>  $register,
            'register_today' => $register_today,
            'register_week' => $register_week,
            'register_month' => $register_month,
            'exchange'  =>  $exchange,
            'exchange_today' => $exchange_today,
            'exchange_week' => $exchange_week,
            'exchange_month' => $exchange_month
        ]);
    }

    /**
     * Реферальные переходы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function transitions(Request $request)
    {
        // Очистить реферальные переходы
        if($request->has('clear')) {
            ReferralStatistics::query()->delete();
            ReferralStatistics::truncate();

            flash(__('История переходов успешно очищена'), ['alert alert-success']);
            return redirect()->back();
        }


        $transitions = ReferralStatistics::with(['user'])->filter($request->all())
            ->leftJoin('referral_links', 'referral_statistics.ref_hash', '=', \DB::raw('referral_links.code'))
            ->leftJoin('users as referral_user', 'referral_links.user_id', '=', 'referral_user.id')
            ->select(
                'referral_statistics.ref_hash',
                'referral_statistics.user_agent',
                \DB::raw('count(*) as total'),
                'referral_statistics.created_at',
                'referral_statistics.id_user',
                'referral_statistics.ip_address',
                'referral_user.id as referral_user_id',
                'referral_user.name as referral_user_name',
                'referral_user.email as referral_user_email')
            ->groupBy('ref_hash')
            ->orderByDesc('total')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('affiliate.referral.transitions', $conditions);
        }

        return view('admin.affiliate.referral.transitions', [
            'transitions'   =>  $transitions,
            'filter' => $request->all()
        ]);
    }

    /**
     * Реферальные направления
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transitionExchange(Request $request)
    {
        $transitions = ReferralStatistics::with(['user'])->filter($request->all())
            ->leftJoin('referral_links', 'referral_statistics.ref_hash', '=', \DB::raw('referral_links.code'))
            ->leftJoin('users as referral_user', 'referral_links.user_id', '=', 'referral_user.id')
            ->select(
                'referral_statistics.ref_hash',
                'referral_statistics.user_agent',
                \DB::raw('count(*) as total'),
                'referral_statistics.created_at',
                'referral_statistics.from_and_to',
                'referral_statistics.cur_from',
                'referral_statistics.cur_to',
                'referral_statistics.id_user',
                'referral_statistics.ip_address',
                'referral_user.id as referral_user_id',
                'referral_user.name as referral_user_name',
                'referral_user.email as referral_user_email')
            ->groupBy('from_and_to')
            ->whereNotNull('from_and_to')
            ->orderByDesc('total')->paginate(20);

        return view('admin.affiliate.referral.transition_exchange', [
            'transitions'   =>  $transitions,
            'filter' => $request->all()
        ]);
    }

    /**
     * Список рефералов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function referrals()
    {
        $referrals = ReferralRelationship::orderByDesc('id')->paginate(20);
        return view('admin.affiliate.referral.referrals', [
            'referrals'   =>  $referrals
        ]);
    }

    /**
     * Партнерские обмены
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function exchanges(Request $request)
    {
        $logs = ReferralLog::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('affiliate.referral.exchanges', $conditions);
        }

        return view('admin.affiliate.referral.exchanges', [
            'logs'   =>  $logs,
            'filter' => $request->all()
        ]);
    }

    /**
     * Сгрупированные партнерские обмены
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function exchanges_group(Request $request)
    {
        $logs = ReferralLog::select('id_user', \DB::raw('count(*) as count_num'), \DB::raw('sum(`bonus_number`) as total_amount'))
            ->groupBy('id_user')->orderByDesc('count_num')->paginate(20);

        return view('admin.affiliate.referral.exchanges_group', [
            'logs'   =>  $logs
        ]);
    }

    /**
     * Лог реферальной системы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function logs(Request $request)
    {
        $logs = ReferralInfoLog::orderByDesc('id')->paginate(20);

        return view('admin.affiliate.referral.logs', [
            'logs'   =>  $logs
        ]);
    }
}
