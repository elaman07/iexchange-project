<?php
namespace App\Http\Controllers\Administrator\AffiliateProgram;

use App\Http\Controllers\Controller;
use App\Models\CashbackErrorLog;
use App\Models\Reward;
use App\Models\RewardLog;
use App\Models\RewardProgram;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BonusController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_cashback_disabled',
    ];

    /**
     * Список бонусных программ
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $bonus = RewardProgram::all();
        return view('admin.affiliate.bonus.index', compact('bonus'));
    }

    /**
     * Форма добавления новой программы
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.affiliate.bonus.create');
    }

    /**
     * Обработка и добавление новой программы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        if($request->has('action') and $request->get('action') == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item)
            {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);


            flash(__('Данные успешно обновлены'), ['alert alert-success']);
            return redirect()->back();
        }

        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|unique:reward_programs|max:100|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'is_reg' => 'required',
            'amount' => 'required|numeric',
            'percent' => 'required|numeric',
        ]);

        // Перед добавлением проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {
            flash(sprintf('Программа %s успешно добавлено', $request->get('name')), ['alert alert-success']);

            $options = [
                'name' => $request->get('name'),
                'is_reg' => $request->get('is_reg'),
                'percent' => $request->get('percent'),
                'amount' => $request->get('amount'),
                'style_width'   => $request->has('style_width') ? $request->get('style_width') : null
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
            }
            // Удаляем кэш
            cache()->forget('static-page-referral');
            RewardProgram::create($options);
        }

        return redirect()->back();
    }

    /**
     * Форма редактирования программы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $bonus = RewardProgram::findOrFail($id);
        return view('admin.affiliate.bonus.edit', compact('bonus'));
    }

    /**
     * Обработка и обновление программы
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:100|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'is_reg' => 'required',
            'amount' => 'required|numeric',
            'percent' => 'required|numeric',
        ]);

        // Перед обновлением проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            flash(sprintf('Программа %s успешно изменена', $request->get('name')), ['alert alert-success']);

            $options = [
                'name' => $request->get('name'),
                'is_reg' => $request->get('is_reg'),
                'percent' => $request->get('percent'),
                'amount' => $request->get('amount'),
                'style_width'   => $request->has('style_width') ? $request->get('style_width') : null
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
            }
            // Удаляем кэш
            cache()->forget('static-page-referral');
            RewardProgram::findOrFail($id)->update($options);
        }

        return redirect()->back();
    }

    /**
     * Удалить программу
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = RewardProgram::findOrFail($id);
        $exists = Reward::whereRewardProgramId($id)->exists();

        if($exists) {
            flash('Невозможно удалить выбранную программу', ['alert alert-danger']);
        } else {
            flash('Программа успешно удалена', ['alert alert-success']);
            $item->delete();
        }

        return redirect()->route('bonus.index');
    }

    /**
     * Бонусные обмены
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exchanges(Request $request)
    {
        $logs = RewardLog::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('affiliate.bonus.exchanges', $conditions);
        }

        return view('admin.affiliate.bonus.exchanges', [
            'logs'   =>  $logs,
            'filter' => $request->all()
        ]);
    }

    /**
     * Лог ошибок
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logs(Request $request)
    {
        $logs = CashbackErrorLog::orderByDesc('id')->paginate(20);

        return view('admin.affiliate.bonus.logs', [
            'logs'   =>  $logs
        ]);
    }
}
