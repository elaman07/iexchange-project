<?php
namespace App\Http\Controllers\Administrator\Verifications;


use App\Models\RequirementVerification;
use App\Http\Controllers\Controller;
use App\Models\VerificationCardCategory;
use App\Models\VerificationCardInstruction;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class VerificationCardInstructionController extends Controller
{

    /**
     * Список требований
     *
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        $items = VerificationCardInstruction::orderBy('sorting')->get();
        return view('admin.verifications.cards.instruction.index', compact('items'));
    }

    /**
     * Добавить требование
     *
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function create()
    {
        $categories = VerificationCardCategory::orderBy('id', 'desc')->get();
        return view('admin.verifications.cards.instruction.create', compact('categories'));
    }

    /**
     * Обработка и добавление требований
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_category' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $filename = Str::random(13).'.png';

            Image::make($request->file('image')->getRealPath())
                ->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save(public_path('storage/card_inst_'.$filename));

            $options = [
                'id_category' => $request->get('id_category'),
                'image' => 'card_inst_'.$filename,
                'status' => $request->has('status') ? $request->get('status') : 0,
                'sorting' => $request->has('sorting') ? $request->get('sorting') : 0
            ];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
                $options['notice_text'][$key] = $request->get('notice_text'.$item['field']);
            }

            VerificationCardInstruction::create($options);

            flash(__('Инструкция успешно добавлена'), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(int $id)
    {
        $item = VerificationCardInstruction::findOrFail($id);
        $categories = VerificationCardCategory::orderBy('id', 'desc')->get();
        return view('admin.verifications.cards.instruction.edit', compact('item','categories'));
    }

    /**
     * Обработка и обновление
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = VerificationCardInstruction::findOrFail($id);

            $options = [
                'id_category' => $request->get('id_category'),
                'status' => $request->has('status') ? $request->get('status') : 0,
                'sorting' => $request->has('sorting') ? $request->get('sorting') : 0
            ];

            if($request->file('image') != null)
            {
                $filename = Str::random(13).'.png';

                ///Удаление предыдущих фото
                iex_file_delete(public_path('storage/'.$item->image));

                Image::make($request->file('image')->getRealPath())
                    ->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(public_path('storage/card_inst_'.$filename));

                $options['image'] = 'card_inst_'.$filename;
            }


            foreach (config('app.form_lang') as $key_update => $item_update) {
                $options['name'][$key_update] = $request->get('name'.$item_update['field']);
                $options['text'][$key_update] = $request->get('text'.$item_update['field']);
                $options['notice_text'][$key_update] = $request->get('notice_text'.$item_update['field']);
            }

            $item->update($options);

            flash(__('Инструкция успешно обновлено'), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = VerificationCardInstruction::findOrFail($id);
        $item->delete();

        flash(__('Инструкция успешно удалена'), ['alert alert-success']);
        return redirect()->back();
    }
}
