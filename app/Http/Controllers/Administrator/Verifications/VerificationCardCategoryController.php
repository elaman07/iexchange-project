<?php
namespace App\Http\Controllers\Administrator\Verifications;


use App\Models\RequirementVerification;
use App\Http\Controllers\Controller;
use App\Models\VerificationCardCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationCardCategoryController extends Controller
{

    /**
     * Список требований
     *
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        $items = VerificationCardCategory::orderBy('sorting')->get();
        return view('admin.verifications.cards.category.index', compact('items'));
    }

    /**
     * Добавить требование
     *
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function create()
    {
        return view('admin.verifications.cards.category.create');
    }

    /**
     * Обработка и добавление требований
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sorting' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'status' => $request->get('status'),
                'sorting' => $request->get('sorting')
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            VerificationCardCategory::create($options);

            flash(__('Категория успешно добавлена'), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(int $id)
    {
        $item = VerificationCardCategory::findOrFail($id);
        return view('admin.verifications.cards.category.edit', compact('item'));
    }

    /**
     * Обработка и обновление
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sorting' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = VerificationCardCategory::findOrFail($id);

            $options = [
                'status' => $request->get('status'),
                'sorting' => $request->get('sorting')
            ];

            foreach (config('app.form_lang') as $key_update => $item_update) {
                $options['name'][$key_update] = $request->get('name'.$item_update['field']);
            }
            $item->update($options);

            flash(__('Категория успешно обновлено'), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление категорий
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash(__('В demo версии данная функция недоступна'), ['alert alert-danger']);
            return redirect()->back();
        }

        $item = VerificationCardCategory::findOrFail($id);
        if(isset($item->instructions) and $item->instructions->count() > 0) {
            foreach ($item->instructions as $instruction) {
                $instruction->delete();
            }
        }
        $item->delete();

        flash(__('Категория и связанные записи успешно удалена'), ['alert alert-success']);
        return redirect()->back();
    }
}
