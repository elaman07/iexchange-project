<?php
namespace App\Http\Controllers\Administrator\Verifications;

use App\Http\Controllers\Controller;

use Illuminate\Contracts\{
    Foundation\Application,
    View\Factory,
    View\View
};
use App\Models\{
    User,
    UserVerification,
};

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class VerificationAccountController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array$allowFiltered = [
        'is_enabled_exchange_verify_account'
    ];

    /**
     * Список записей на верификацию счета
     *
     * @param Request $request
     *
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index(Request $request)
    {
        $cards = UserVerification::orderBy('id', 'desc')->paginate(20);

        $statuses = [
            0 => __('В обработке'),
            1 => __('Верифицирован'),
            2 => __('Отклонена')
        ];

        return view('admin.verifications.accounts.index', compact('cards', 'statuses'));
    }

    /**
     * Обновление данные
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
        }

        if($request->has('actions'))
        {
            if($request->actions == 'save')
            {
                foreach ($request->status as $key => $status) {
                    $verification = UserVerification::find($key);

                    $verification->update([
                        'status' => $status ?? 0
                    ]);

                    $user = User::find($verification->user_id);
                    $user->update([
                        'is_verify_account' => ($status == 1) ?? 1
                    ]);
                }
                flash(__('Данные успешно обновлены'), ['alert alert-success']);
            }elseif($request->actions == 'delete')
            {
                if(config('admin.is_demo_mode')) {
                    flash(__('В demo версии данная функция недоступна'), ['alert alert-danger']);
                    return redirect()->back();
                }

                foreach ($request->check as $value) {
                    UserVerification::find($value)->delete();
                }
                flash(__('Выбранные элементы успешно удалены'), ['alert alert-success']);
            }
        }

        return redirect()->back();
    }
}
