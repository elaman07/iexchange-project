<?php
namespace App\Http\Controllers\Administrator\Verifications;

use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Controllers\Controller;
use App\Jobs\Verification\Fail;
use App\Models\Currency;
use App\Models\VerificationCard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class VerificationCardController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'num_count_failed_verification',
        'telegram_order_verification_card',
        'enabled_auto_verification_card',
        'other_percent_user_verification',
        'email_verification_card',
        'reject_failed_verification_card'
    ];


    protected array $allowLocaleOptions = [
        'description_verification_card',
        'error_verification_card',
    ];

    /**
     * Список карт
     *
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|RedirectResponse
     */
    public function index(Request $request)
    {
        $cards = VerificationCard::filter($request->all())->orderBy('id', 'desc')->paginate(20);
        $currencies = Currency::where('status', '=', 0)->get()->map(function ($item) {
            return [
                'value' => sprintf('%s %s', $item->payment->name, $item->code_currency->name),
                'id' => $item->id
            ];
        })->pluck('value', 'id');

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('verifications-card.index', $conditions);
        }

        $statuses = [
            0 => __('В обработке'),
            1 => __('Верифицирован'),
            3 => __('Отклонена')
        ];

        return view('admin.verifications.cards.index', [
            'cards' =>  $cards,
            'statuses' => $statuses,
            'filter' => $request->all(),
            'currencies'  => $currencies
        ]);
    }

    /**
     * Обновление данные
     *
     * @param Request $request
     * @return RedirectResponse|void
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            // For multi-languages
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->except($this->allowFiltered)->all();
            iEXContentLanguage($locale_data);

            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions'))
        {
            if($request->actions == 'save')
            {
                // Для одинарных
                if($request->has('verification_id')) {

                    if($request->get('status') == 0) {
                        return redirect()->to(
                            admin_base_path('/tx')
                        );
                    }

                    $card = VerificationCard::with('tasks')->find($request->verification_id);

                    $card->update([
                        'status' => $request->status,
                        'text_message' => $request->has('verification_message') ? $request->get('verification_message') : null
                    ]);

                    if(isset($card->tasks) and in_array($request->status, [1, 3])) {

                        if($request->status == 1) {
                            $card->tasks->update(['int_status_verification_card' => 2]);
                        }

                        // Отклоняем заявку
                        if($request->status == 3 and (int)iEXSetting('reject_failed_verification_card') == 1) {
                            $tx = TransactionFacade::init($card->tasks);
                            $tx->failed();
                            $card->tasks->update(['int_status_verification_card' => 2]);
                        }
                    }

                    if($request->has('redirect') and $request->get('redirect') == 'tx')
                    {
                        flash('Статус верификации успешно обновлен', ['alert alert-success']);
                        return redirect()->to(
                            admin_base_path('/tx')
                        );
                    }
                } else {

                    foreach ($request->status as $key => $status) {

                        $verification = VerificationCard::find($key);

                        // Отправить уведомление в случае отказа верификации
                        if(in_array($status, [2, 3]) and (int)iEXSetting('email_verification_card', 0) == 1) {
                            $delay = now()->addMinutes(1);
                            dispatch(new Fail($verification))->delay($delay)->onQueue('low');
                        }

                        $verification->update([
                            'status' => $status ?? 0
                        ]);
                    }
                    flash(__('Данные успешно обновлены'), ['alert alert-success']);

                }
            }elseif($request->actions == 'delete') {
                foreach ($request->check as $value) {
                    VerificationCard::find($value)->delete();
                }
                flash(__('Выбранные счета успешно удалены'), ['alert alert-success']);
            }

            return redirect()->back();
        }
    }

    /**
     * Детали карты
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        $card = VerificationCard::with('tasks')->find($id);

        if($request->has('status'))
        {
            $card->update(['status' => $request->status]);
            if(isset($card->tasks) and in_array($request->status, [1, 3])) {
                $card->tasks->update(['int_status_verification_card' => 2]);

                // Отклоняем заявку
                if($request->status == 3 and (int)iEXSetting('reject_failed_verification_card') == 1) {
                    $tx = TransactionFacade::init($card->tasks);
                    $tx->failed();
                }
            }

            if($request->has('redirect') and $request->get('redirect') == 'tx')
            {
                flash('Статус верификации успешно обновлен', ['alert alert-success']);
                return redirect()->to(
                    admin_base_path('/tx')
                );
            } else {
                flash('Статус успешно обновлен', ['alert alert-success']);
                return redirect()->back();
            }
        }

        return view('admin.tools.verification_card.show', [
            'card' =>  $card
        ]);
    }
}
