<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class APILogsController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.api.api_logs', [
            'logs' => []
        ]);
    }
}
