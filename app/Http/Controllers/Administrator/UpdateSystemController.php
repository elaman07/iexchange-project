<?php
namespace App\Http\Controllers\Administrator;

use App\Common\Packages\Update\Facades\UpdateClientFacade;
use App\Common\Packages\Update\UpdateClient;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\UpdateSystem;
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class UpdateSystemController extends Controller
{
    public function __construct()
    {
        if(config('admin.is_demo_mode')) {
            flash('Данные недоступны в Demo версии', ['alert alert-danger']);
            return redirect()->to(admin_base_path('/'));
        }
    }

    /**
     * Обновление системы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function index(Request $request)
    {
        $server_version = Cache::get('iex_new_version');
        UpdateClientFacade::checkUpdates();

        if($request->has('actions') and $request->get('actions') == 'upgrade')
        {
            $version = $request->get('v');
            $step = $request->get('step');

            if($version == config('version.current'))
            {
                if($step == 1)
                {
                    \Artisan::call('iex:update');
                    echo '<pre>';
                    print_r(\Artisan::output());
                    echo '</pre>';
                }

                exit();
            }

            if($version == '6.5')
            {
                if($step == 1)
                {
                    \Artisan::call('iex:update');
                    echo '<pre>';
                    print_r(\Artisan::output());
                    echo '</pre>';
                    exit;
                }

                if($step == 2)
                {
                    // Кэшируем настройки
                    file_put_contents(storage_path('/settings-'.time().'.json'), json_encode(iEXSetting()->all()));
                    $options = [
                        'description_contact',
                        'interface_regauth_text1',
                        'interface_regauth_text2',
                        'telegram_bot_name_button',
                        'tech_breach_title',
                        'tech_breach_text',
                        'input_footer_title',
                        'description_footer_text',
                        'welcome_title',
                        'welcome_description',
                        'telegram_block_title',
                        'telegram_block_description',
                        'telegram_block_button',
                        'description_verification_card',
                        'error_verification_card',
                        'sitename',
                        'sitename_desc',
                        'description',
                        'keywords',
                        'username_new_user',
                        'main_title_header',
                        'main_value_header',
                        'chat_app_id',
                        'working_online_text',
                        'working_offline_text',
                        'referral_system_text',
                        'referral_system_text_footer',
                        'cashback_text',
                        'cashback_footer',
                        'monitoring_text',
                        'description_pr',
                        'description_review',
                        'working_offline_notify'
                    ];

                    $data = [];
                    foreach ($options as $value) {
                        if(!empty(iEXSetting($value))) {
                            $data[$value] = iEXSetting($value);
                            iEXSetting()->forget($value);
                        }
                    }
                    iEXContentLanguage($data);
                    iEXSetting()->save();
                }


                if($step == 3)
                {
                    Task::chunk(100, function($tasks)
                    {
                        foreach ($tasks as $task)
                        {
                            $task->give_price_with_comm = $task->give_price;
                            $task->receiving_price_with_comm = $task->receiving_price;
                            $task->receiving_price_with_comm_pay = $task->receiving_price;
                            $task->save();
                        }
                    });
                }


                return redirect()->back();
            }


            if($version == '6.1')
            {
                if($step == 1) {
                    \Artisan::call('iex:update');
                    echo '<pre>';
                    print_r(\Artisan::output());
                    echo '</pre>';
                }

                if($step == 2) {
                    \Artisan::call('wallet:clear_stories');
                    echo '<pre>';
                    print_r(\Artisan::output());
                    echo '</pre>';
                    exit;
                }

                return redirect()->back();
            }

        }


        return view('admin.update_system.index', [
            'server_version' => $server_version
        ]);
    }

    /**
     * Установка лицензии
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'activation_code' => ['required']
        ]);

        return redirect()->back();
    }

    /**
     * Обновление настроен
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function settings(Request $request)
    {
        $env = new DotenvEditor();
        $env->changeEnv([
            'LICENSE_KEY' => trim($request->get('license_key')),
            'UPDATE_SITE_PROXY_ADDR' => trim($request->get('update_site_proxy_addr')),
            'UPDATE_SITE_PROXY_PORT' => trim($request->get('update_site_proxy_port')),
            'UPDATE_SITE_PROXY_USER' => trim($request->get('update_site_proxy_user')),
            'UPDATE_SITE_PROXY_PASS' => trim($request->get('update_site_proxy_pass')),
            'STABLE_VERSIONS_ONLY' => ($request->has('stable_versions_only') ? $request->get('stable_versions_only') : 0),
            'UPDATE_AUTOCHECK' => ($request->has('update_autocheck') ? $request->get('update_autocheck') : 0),
            'UPDATE_STOP_AUTOCHECK' => ($request->has('update_stop_autocheck') ? $request->get('update_stop_autocheck') : 0),
        ]);
        flash(__('Данные успешно обновлены'), ['alert alert-success']);
        return redirect()->back();
    }
}
