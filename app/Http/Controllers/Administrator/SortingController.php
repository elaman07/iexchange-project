<?php
namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Advantage;
use App\Models\Banner;
use App\Models\CollaborationPrModel;
use App\Models\CompetitorLink;
use App\Models\Contact;
use App\Models\Currency;
use App\Models\CurrencyFields;
use App\Models\CurrencyGroup;
use App\Models\CurrencyNotification;
use App\Models\DirectionExchange;
use App\Models\DirectionField;
use App\Models\DirectionNotification;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\Favorites;
use App\Models\FileParserGroup;
use App\Models\FilterCurrency;
use App\Models\GroupParserExchange;
use App\Models\InfoStatistic;
use App\Models\LinksReview;
use App\Models\Menu;
use App\Models\NoticeExchange;
use App\Models\OrderStep;
use App\Models\Partner;
use App\Models\RequirementVerification;
use App\Models\RequisitesGroup;
use App\Models\Reserve;
use App\Models\RulesPage;
use App\Models\SelectedCourse;
use App\Models\SocialAuthSystem;
use App\Models\SocialReview;
use App\Models\YourExchangeGroup;
use Illuminate\Http\Request;

class SortingController extends Controller
{
    public function cryptoBanners(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Banner::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingRulesPages(Request $request)
    {
        foreach ($request->item as $key => $item) {
            RulesPage::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }


    public function sortingApplicationStep(Request $request)
    {
        foreach ($request->item as $key => $item) {
            OrderStep::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingCurrencyField(Request $request)
    {
        foreach ($request->item as $key => $item) {
            CurrencyFields::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingCurrencyFieldOut(Request $request)
    {
        foreach ($request->item as $key => $item) {
            CurrencyFields::find($item)->update([
                'sorting_out'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingCurrencyAdmin(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Currency::find($item)->update([
                'sorting_admin'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function currencyReserves(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Currency::find($item)->update([
                'sorting_reserve'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function currencyNotification(Request $request)
    {
        foreach ($request->item as $key => $item) {
            CurrencyNotification::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function socialAuthSystem(Request $request)
    {
        foreach ($request->item as $key => $item) {
            SocialAuthSystem::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingYourCourseGroup(Request $request)
    {
        foreach ($request->item as $key => $item) {
            YourExchangeGroup::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingDirectionAdmin(Request $request)
    {
        foreach ($request->item as $key => $item) {
            DirectionExchange::find($item)->update([
                'sorting_admin'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingDirectionField(Request $request)
    {
        foreach ($request->item as $key => $item) {
            DirectionField::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingTariffs(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Currency::find($item)->update([
                'sorting_tariffs'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingTariffId(Request $request)
    {
        foreach ($request->item as $key => $item) {
            DirectionExchange::find($item)->update([
                'sorting_tariffs'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }


    public function sortingDirectionCurrency(Request $request)
    {
        foreach ($request->item as $key => $item) {
            DirectionExchange::find($item)->update([
                'sorting_2'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function statistics(Request $request)
    {
        foreach ($request->item as $key => $item) {
            InfoStatistic::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function socialReview(Request $request)
    {
        foreach ($request->item as $key => $item) {
            SocialReview::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function sortingContacts(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Contact::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function collaborationPR(Request $request)
    {
        foreach ($request->item as $key => $item) {
            CollaborationPrModel::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function favoriteSorting(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Favorites::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка источников курсов
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cryptoParser(Request $request)
    {
        foreach ($request->item as $key => $item) {
            GroupParserExchange::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }


    /**
     * Сортировка источников курсов
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function competitors_link(Request $request)
    {
        foreach ($request->item as $key => $item) {
            CompetitorLink::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка групп
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function file_parser_group(Request $request)
    {
        foreach ($request->item as $key => $item) {
            FileParserGroup::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка резервов для админ панели
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function basicReserves(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Reserve::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка уведомлений
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function notification(Request $request)
    {
        foreach ($request->item as $key => $item) {
            NoticeExchange::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка преимущест
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function advantage(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Advantage::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка партнеров
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function partners(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Partner::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка ссылок на отзывы
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function links_reviews(Request $request)
    {
        foreach ($request->item as $key => $item) {
            LinksReview::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка групп
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function requisites_group(Request $request)
    {
        foreach ($request->item as $key => $item) {
            RequisitesGroup::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка Требовании к верификации карт
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verificationRequirement(Request $request)
    {
        foreach ($request->item as $key => $item) {
            RequirementVerification::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка меню
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function menu(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Menu::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка фильтров
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter_currency(Request $request)
    {
        foreach ($request->item as $key => $item) {
            FilterCurrency::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка групп
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function group_currency(Request $request)
    {
        foreach ($request->item as $key => $item) {
            CurrencyGroup::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка вопросов и ответов
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function faq(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Faq::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка категорий
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function faq_category(Request $request)
    {
        foreach ($request->item as $key => $item) {
            FaqCategory::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Сортировка избранных курсов
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selected_course(Request $request)
    {
        foreach ($request->item as $key => $item) {
            SelectedCourse::find($item)->update([
                'sorting'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }
}
