<?php
namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\AdminDesktop;
use App\Models\AdminDesktopGadget;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class WidgetController extends Controller
{
    /**
     * Добавление нового виджета
     *
     * @param int $id_desktop
     * @param string $id
     * @return RedirectResponse
     */
    public function add(int $id_desktop, string $id): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $detail = config('widgets.available')[$id];
        if(isset($detail)) {
            AdminDesktopGadget::create([
                'id_user' => auth()->id(),
                'id_desktop' => $id_desktop,
                'hash_id' => hash('sha1', $id.time()),
                'column_id' => 'column0',
                'name' => $detail['name'],
                'alias' => $detail['alias'],
                'sorting' => 0
            ]);

            flash(__('Виджет успешно добавлен'), ['alert alert-success']);
        } else {
            flash(__('Виджет не найден'), ['alert alert-danger']);
        }

        return redirect()->to(admin_base_path('/?page_id='.$id_desktop));
    }

    /**
     * Форма Редактирование виджета
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|Factory|\Illuminate\Contracts\View\View|RedirectResponse
     */
    public function edit(Request $request)
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $request->validate($request->route()->parameters(), [
            'hash_id' => ['required', 'string']
        ]);


        $item = AdminDesktopGadget::where([
            ['hash_id', $request->route('hash_id')],
            ['id_user', auth()->id()]
        ])->first();

        if(!isset($item)) {
            flash(__('Ничего не найдено'), ['alert alert-danger']);
        }


        $forms = collect(
            config('widgets.available')
        )->filter(function($filter) use ($item) {
            return $filter['alias'] == $item['alias'];
        })->first();


        return view('admin.widgets.edit', compact('item', 'forms'));
    }

    /**
     * Обновление виджета
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $this->validate($request, [
            'name' => ['required']
        ]);

        $item = AdminDesktopGadget::where([
                ['hash_id', $request->route('hash_id')],
                ['id_user', auth()->id()]
            ])->first();

        $forms = collect(
            config('widgets.available')
        )->filter(function($filter) use ($item) {
            return $filter['alias'] == $item['alias'];
        })->first();

        $responseData = [];
        if(isset($forms['inputs']) and count($forms['inputs']) > 0)
        {
            $json = [];
            foreach ($forms['params'] as $form_item)
            {
                if($request->has($form_item) and is_array($request->get($form_item))) {
                    $json[$form_item] = implode(',', $request->get($form_item));
                } else {
                    $json[$form_item] = ($request->has($form_item) ? $request->get($form_item): null);
                }
            }

            iEXSetting(
                array_merge($json, $responseData)
            );
        }

        $item->update([
            'name' => $request->get('name')
        ]);

        flash(__('Виджет успешно обновлен'), ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/widgets/'.$request->route('hash_id').'/edit')
        );
    }


    /**
     * Удаление виджета
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $validator = Validator::make($request->route()->parameters(), [
            'hash_id' => ['required', 'string']
        ]);

        //  Если в валидаторе ошибки
        if($validator->fails()) {
            flash(__('Виджет не удален'), ['alert alert-danger']);
            return redirect()->back();
        }

       $response = AdminDesktopGadget::where([
           'hash_id' => $request->route('hash_id'),
           'id_user' => auth()->id(),
       ])->first();
       $response->delete();

       flash(__('Виджет успешно удален'), ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Сортировка виджетов
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sorting(Request $request): JsonResponse
    {
        foreach ($request->item as $key => $item)
        {
            AdminDesktopGadget::where([
                ['id_desktop', $request->id_desktop],
                ['id_user', auth()->id()]
            ])->where('id', $item)->update([
                'sorting'    =>  $key,
                'column_id' => $request->column
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Создание нового рабочего стола
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function create_desktop(Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $this->validate($request, [
            'columns' => ['required', 'numeric']
        ]);

        $name = $request->get('name');
        if(empty($request->name))
        {
            $desktop = AdminDesktop::with(['gadgets' => function ($query) {
                $query->orderBy('sorting');
            }])->where('id_user', '=', auth()->id())->orderBy('sorting');
            $name = 'Рабочий стол '.($desktop->count() + 1);
        }

        $item = AdminDesktop::create([
            'id_user' => $request->user()->id,
            'name' => $name,
            'columns' => $request->get('columns'),
            'sorting' => 0,
            'flex_nums' => ($request->has('flex_nums') ? $request->get('flex_nums') : null)
        ]);

        flash(__('Рабочий стол :name успешно создан', ['name' => $item->name]),['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Обновить настройки текущего рабочего стола
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update_desktop(int $id, Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $this->validate($request, [
            'name' => ['required'],
            'columns' => ['required', 'numeric']
        ]);

        $item = AdminDesktop::where([
            ['id_user', '=', auth()->id()],
            ['id', '=', $id]
        ])->first();

        if(isset($item)) {
            $item->update([
                'name' => $request->get('name'),
                'columns' => $request->get('columns'),
                'flex_nums' => ($request->has('flex_nums') ? $request->get('flex_nums') : null)
            ]);
        }

        flash(__('Рабочий стол :name успешно обновлен', ['name' => $item->name]),['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Форма редактирования рабочего стола
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|Factory|\Illuminate\Contracts\View\View|RedirectResponse
     */
    public function edit_desktop(int $id)
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = AdminDesktop::where([
            ['id', $id],
            ['id_user', auth()->id()]
        ])->first();

        if(!isset($item)) {
            flash(__('Ничего не найдено'), ['alert alert-danger']);
        }

        return view('admin.widgets.edit_desktop', compact('item'));
    }


    /**
     * Настройка рабочих столов
    */
    public function config_desktops()
    {
        $desktops = AdminDesktop::where('id_user', auth()->id())->orderBy('sorting')->get();
        return view('admin.widgets.config_desktops', compact('desktops'));
    }

    /**
     * Сортировка рабочего стола
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sorting_desktops(Request $request): JsonResponse
    {
        foreach ($request->item as $key => $item)
        {
            AdminDesktop::where([
                ['id_user', auth()->id()]
            ])->where('id', $item)->update([
                'sorting'    =>  $key,
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Удаление рабочего стола
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function desktop_destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $response = AdminDesktop::where([
            'id' => $id,
            'id_user' => auth()->id(),
        ])->first();

        // Если есть виджеты, удаляем с ними
        if($response->gadgets->count() > 0) {
            $response->gadgets()->delete();
        }

        $response->delete();

        flash(__('Рабочий стол успешно удален'), ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Сбросить все настройки
    */
    public function reset(): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $items = AdminDesktop::where('id_user', auth()->id())->get();
        foreach ($items as $item) {
            if($item->gadgets->count() > 0)
                $item->gadgets()->delete();
            $item->delete();
        }

        flash(__('Все настройки успешно сброшены'), ['alert alert-success']);
        return redirect()->to(admin_base_path('/'));
    }
}
