<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.10.2019
 * Time: 20:55
 */

namespace App\Http\Controllers\Administrator;


use App\Http\Controllers\Controller;
use App\Jobs\TelegramAdminAuthJob;
use App\Jobs\TelegramAdminUnAuthorizationJob;
use App\Jobs\TelegramBlockedUserJob;
use App\Notifications\SMSAdminCodeNotification;
use Illuminate\Cache\RateLimiter;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\View\View;
use NotificationChannels\SmscRu\SmscRuChannel;

class AdminPhoneController extends Controller
{
    /**
     * Ключ сессии
     *
     * @var string
     */
    protected string $key = 'iex-phone-hash';

    /**
     * Код для проверки неудачных попыток
     *
     * @var string
     */
    protected string $throttleSuffix = ':code2faPhone';


    /**
     * Главная страница для ввода номера телефона
     *
     * @param Request $request
     * @return Factory|View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        // Временно кэшируем код, чтобы зафиксировать №
        $code = $this->getCurrentCode($request);
        return view('admin.phone_codes', compact('code'));
    }

    /**
     * Обработка введенного резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return RedirectResponse
     */
    public function handle(Request $request, RateLimiter $limiter): RedirectResponse
    {
        $validator = \Validator::make($request->all(), [
            'code' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            // Для начала проверяем установленный лимит
            if ($this->hasTooManyRecoveryAttempts($request, $limiter)) {
                $text = $this->sendLockoutResponse($request, $limiter);

                if(iEXSetting('is_notify_telegram_blocked_client')) {
                    $delay = now()->addMinutes(1);
                    dispatch(new TelegramBlockedUserJob($request->user(), "{$request->user()->name}-SMS Код: {$text}"))
                        ->delay($delay)
                        ->onQueue('low');
                }

                flash($text, ['alert alert-danger']);
                return back();
            }

            // Увеличиваем кол-во попыток
            $this->incrementAttempts($request, $limiter);

            if($this->attemptRecovery($request)) {
                $this->sendRecoveryResponse($request, $limiter);

                return redirect()->to(admin_base_path('/'));
            } else {
                if(iEXSetting('is_failed_admin_auth')) {
                    $delay = now()->addMinutes(1);
                    dispatch(new TelegramAdminAuthJob($request->user(), false, 'SMS Код'))
                        ->delay($delay)
                        ->onQueue('low');
                }
                flash('Неверный код', ['alert alert-danger']);
            }

            return redirect()->back();
        }
    }

    /**
     * В случае удачного ввода резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return void
     */
    protected function sendRecoveryResponse(Request $request, RateLimiter $limiter)
    {
        $this->clearAttempts($request, $limiter);

        // Записываем код в базу
        $secret = hash('sha256', $request->get('code').'-'.$request->user()->id);
        // Обновляем ключ в базе
        $request->user()->update(['phone_hash' => $secret]);

        // После успешной авторизации, очищаем кэш
        \Cache::tags('phone-code'.$request->user()->id)->flush();

        // Записываем в сессию
        \Session::put($this->key.$request->user()->id, $secret);
    }

    /**
     * Очищаем попытки ввода кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     */
    protected function clearAttempts(Request $request, RateLimiter $limiter)
    {
        $limiter->clear(
            $this->throttleKey($request)
        );
    }

    /**
     * Ключ, для проверки кол-во неудачно введенных кодов
     *
     * @param Request $request
     * @return string
     */
    private function throttleKey(Request $request): string
    {
        return $request->ip() . '|' . $request->user()->id . $this->throttleSuffix;
    }

    /**
     * Проверка кода
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptRecovery(Request $request): bool
    {
        $code = \Cache::tags('phone-code'.$request->user()->id)->get('phone-user-'.$request->user()->id);

        if(!$code) return false;
        return $code == $request->get('code');
    }

    /**
     * Получаем детали кэшированного кода
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getCurrentCode(Request $request)
    {
        $code = random_int(111111, 999999);

        // Если кэш не найден, то отправляем уведомление
        if(!\Cache::tags('phone-code'.$request->user()->id)->has('phone-user-'.$request->user()->id))
        {
            $channel = 'nexmo';
            if(config('crypto.sms_driver') == 'smscru')
                $channel = 'smscru';
            elseif(config('crypto.sms_driver') == 'twilio')
                $channel = 'twilio';
            elseif(config('crypto.sms_driver') == 'plivo')
                $channel = 'plivo';
            elseif(config('crypto.sms_driver') == 'jusibe')
                $channel = 'jusibe';

            Notification::route($channel, $request->user()->admin_phone)
                ->notify(new SMSAdminCodeNotification($code));
        }

        return \Cache::tags('phone-code'.$request->user()->id)
            ->remember('phone-user-'.$request->user()->id, Carbon::now()->addMinutes(20), function() use($code) {
                return $code;
            });
    }

    /**
     * Определяем, есть ли у пользователя слишком много неудачных попыток ввода резервного кода.
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return bool
     */
    private function hasTooManyRecoveryAttempts(Request $request, RateLimiter $limiter): bool
    {
        return $limiter->tooManyAttempts(
            $this->throttleKey($request), $this->maxAttempts()
        );
    }

    /**
     * В случае множественных неудачных вводов, блокируем все действия
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return string
     */
    protected function sendLockoutResponse(Request $request, RateLimiter $limiter): string
    {
        $availableAt = now()->addSeconds(
            $limiter->availableIn($this->throttleKey($request))
        )->ago();
        return sprintf("Слишком много попыток ввода. Пожалуйста, попробуйте еще раз %s ", $availableAt);
    }

    /**
     * Увеличиваем количество неудачных попыток ввода кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     */
    protected function incrementAttempts(Request $request, RateLimiter $limiter) {
        $limiter->hit(
            $this->throttleKey($request), 320 * 60
        );
    }

    /**
     * Макс. количество неудачных попыток ввода кода
     *
     * @return integer
     */
    protected function maxAttempts(): int
    {
        return 5;
    }
}
