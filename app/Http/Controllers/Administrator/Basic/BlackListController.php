<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\BlacklistOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BlackListController extends Controller
{
    /**
     * Ключи для настроек
     *
     * @var array
     */
    protected $attributes = [
        'is_local_blacklist',
        'is_bs_unified_base_iex'
    ];

    /**
     * Доступные типы
     *
     * @var array
    */
    protected $types = [
        0   =>  'счет',
        1   =>  'email',
        2   =>  'ip'
    ];

    public function index(Request $request)
    {
        // Очистить весь список
        if($request->has('clear_all')) {
            BlacklistOrder::query()->delete();
            flash('Черный список успешно очищен', ['alert alert-success']);

            return redirect()->back();
        }

        $lists = BlacklistOrder::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('blacklist.index', $conditions);
        }

        return view('admin.basic.blacklist.index', [
            'lists' =>  $lists,
            'types' =>  $this->types,
            'filter' => $request->all(),
        ]);
    }

    public function create(Request $request)
    {
        return view('admin.basic.blacklist.create',[
            'types' =>  $this->types,
            'query' =>  $request->all()
        ]);
    }

    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->attributes as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $uuid = \Str::uuid();
        BlacklistOrder::create([
            'hash_id' => $uuid,
            'type'  =>  3,
            'value' =>  ($request->has('value') ? $request->get('value') : ''),
            'text' =>  ($request->has('text') ? $request->get('text') : ''),
            'is_bestchange' =>  ($request->has('is_bestchange') ? $request->get('is_bestchange') : 0),
            'is_iex'    => iEXSetting('is_bs_unified_base_iex') == 0 ? 1 : 0
        ]);


        if(iEXSetting('is_bs_unified_base_iex') == 0)
        {
            \Http::post('https://iexexchanger.com/api/v1/add_record_blacklist', [
                'id_license' => config('license.license_key'),
                'exchange_name' => iEXSetting('sitename'),
                'exchange_url'  => config('app.url'),
                'type'  =>  3,
                'value' =>  ($request->has('value') ? $request->get('value') : ''),
                'text' =>  ($request->has('text') ? $request->get('text') : ''),
                'hash_id'   => $uuid
            ]);
        }

        if($request->has('is_bestchange')) {
            addToBestchangeList($request->get('value'), $request->get('text'));
        }

        return redirect(admin_path('basic/blacklist'));
    }

    public function edit($id) {
        $item = BlacklistOrder::findOrFail($id);
        return view('admin.basic.blacklist.edit', [
            'item'  =>  $item,
            'types' =>  $this->types
        ]);
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $blacklist = BlacklistOrder::findOrFail($id);

        if($request->has('is_bestchange')) {
            addToBestchangeList($request->get('value'), $request->get('text'));
        }

        $blacklist->update([
            'value' =>  ($request->has('value') ? $request->get('value') : ''),
            'text' =>  ($request->has('text') ? $request->get('text') : ''),
            'is_bestchange' =>  ($request->has('is_bestchange') ? $request->get('is_bestchange') : 0),
        ]);


        flash('Запись успешно добавлена', ['alert alert-success']);
        return redirect()->route('blacklist.index')
            ->with('flash_message',
                'Page'. $blacklist->name.' updated!');
    }

    public function destroy($id)
    {
        $role = BlacklistOrder::findOrFail($id);
        if(iEXSetting('is_bs_unified_base_iex') == 0)
        {
            \Http::post('https://iexexchanger.com/api/v1/delete_record_blacklist', [
                'id_license' => config('license.license_key'),
                'hash_id'   => $role->hash_id
            ]);
        }


        $role->delete();

        return redirect()->route('blacklist.index');
    }
}
