<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\CurrencyGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyGroupController extends Controller
{
    /**
     * Список групп валют
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Обновление позиций
        if($request->has('action') and $request->get('action') == 'update')
        {
            foreach ($request->sorting as $key => $item) {
                CurrencyGroup::find($key)->update([
                    'sorting' => $item
                ]);
            }

            return redirect()->back();
        }


        $groups = CurrencyGroup::orderBy('sorting')->get();
        return view('admin.basic.currency.groups.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления группы
     */
    public function create()
    {
        return view('admin.basic.currency.groups.create');
    }

    /**
     * Обработка и добавления новой группы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = CurrencyGroup::create([
                'name'  =>  $request->name,
                'id_user' => auth()->id()
            ]);
            $item->save();

            flash('Новая группа успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = CurrencyGroup::findOrFail($id);
        return view('admin.basic.currency.groups.edit',compact('item'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = CurrencyGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $group->update([
                'name'  =>  ($request->has('name') ? $request->get('name') : null)
            ]);

            flash('Группа '.$group->name.' успешно обновлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить группу
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        // Делаем доп. проверку в списке направлений
        $currency = Currency::where('id_group', '=', $id);

        if($currency->count() > 0) {
            flash('Выбранную группу удалить невозможно, К группе привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = CurrencyGroup::findOrFail($id);

        flash(
            sprintf('Группа %s успешно удалена', $item->name),
            ['alert alert-success']
        );

        $item->delete();
        return redirect()->back();
    }

    /**
     * Сортировка фильтров
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sorting()
    {
        $groups = CurrencyGroup::orderBy('sorting')->get();
        return view('admin.basic.currency.groups.sorting', compact('groups'));
    }
}
