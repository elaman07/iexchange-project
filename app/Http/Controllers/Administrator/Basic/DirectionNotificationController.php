<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use App\Models\DirectionNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectionNotificationController extends Controller
{
    /**
     * Список уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $notifications = DirectionNotification::orderBy('sorting')->get();
        return view('admin.basic.direction_exchange.notification.index', compact('notifications'));
    }

    /**
     * Форма добавления уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        $direction = DirectionExchange::isEnabled()->get();
        return view('admin.basic.direction_exchange.notification.create', [
            'directions' => $direction
        ]);
    }

    /**
     * Обработка и добавление уведомлений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_direction_exchange' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $options = [
                'id_direction_exchange' => $request->get('id_direction_exchange'),
                'bg_color' => $request->has('bg_color') ? $request->get('bg_color') : null,
                'text_color' => $request->has('text_color') ? $request->get('text_color') : null,
                'status' => $request->has('status') ? $request->get('status') : 0,
                'is_enabled_schedule' => $request->has('is_enabled_schedule') ? $request->get('is_enabled_schedule') : 0,
                'from_time' => $request->has('from_time') ? $request->get('from_time') : null,
                'to_time' => $request->has('to_time') ? $request->get('to_time') : null,
                'is_order_detail' => $request->has('is_order_detail') ? $request->get('is_order_detail') : 0,
            ];

            foreach (config('app.form_lang') as $key_update => $item_update) {
                $options['description'][$key_update] = $request->get('description'.$item_update['field']);
                $options['title'][$key_update] = $request->get('title'.$item_update['field']);
            }



            DirectionNotification::create($options);
            flash(__('Уведомление успешно добавлена'), ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Форма редактирования уведомлений
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = DirectionNotification::findOrFail($id);
        $directions = DirectionExchange::isEnabled()->get();
        return view('admin.basic.direction_exchange.notification.edit',compact('item', 'directions'));
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $notification = DirectionNotification::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_direction_exchange' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [
                'id_direction_exchange' => $request->get('id_direction_exchange'),
                'bg_color' => $request->has('bg_color') ? $request->get('bg_color') : null,
                'text_color' => $request->has('text_color') ? $request->get('text_color') : null,
                'status' => $request->has('status') ? $request->get('status') : 0,
                'is_enabled_schedule' => $request->has('is_enabled_schedule') ? $request->get('is_enabled_schedule') : 0,
                'from_time' => $request->has('from_time') ? $request->get('from_time') : null,
                'to_time' => $request->has('to_time') ? $request->get('to_time') : null,
                'is_order_detail' => $request->has('is_order_detail') ? $request->get('is_order_detail') : 0,
            ];

            foreach (config('app.form_lang') as $key_update => $item_update) {
                $options['description'][$key_update] = $request->get('description'.$item_update['field']);
                $options['title'][$key_update] = $request->get('title'.$item_update['field']);
            }


            $notification->update($options);
        }

        flash(__('Данные успешно обновлены'), ['alert alert-success']);
        return redirect()->route('direction-exchange-notification.edit', $notification->id);
    }

    /**
     * Удаление уведомлений
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $notify = DirectionNotification::findOrFail($id);
        $notify->delete();

        flash(__('Уведомление успешно удалена'), ['alert alert-success']);

        return redirect()->route('direction-exchange-notification.index');
    }
}
