<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\CurrencyCommand;
use App\Models\CurrencyGroup;
use App\Models\CurrencyNetwork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyNetworkController extends Controller
{
    /**
     * Список команд
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $groups = CurrencyNetwork::all();
        return view('admin.basic.currency.network.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления
     */
    public function create()
    {
        return view('admin.basic.currency.network.create');
    }

    /**
     * Обработка и добавления
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = CurrencyNetwork::create([
                'name' => ($request->has('name') ? $request->get('name') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);
            $item->save();

            // Записываем в лог
            add_to_currencies_log($item->id_currency, 'Добавлена новая сеть '.$item->name.' ', 0,6);

            flash('Новая сеть успешно добавлена', ['alert alert-success']);
            return redirect()->back();

        }
    }

    /**
     * Форма изменения
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = CurrencyNetwork::findOrFail($id);
        return view('admin.basic.currency.network.edit',compact('item'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = CurrencyNetwork::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $group->update([
                'name' => ($request->has('name') ? $request->get('name') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            // Записываем в лог
            add_to_currencies_log($group->id_currency, 'Обновление сети '.$group->name.' ', 1,6);
            flash('Сеть '.$group->name.' успешно обновлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить сеть
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = CurrencyNetwork::findOrFail($id);

        flash(
            sprintf('Сеть %s успешно удалена', $item->name),
            ['alert alert-success']
        );

        // Записываем в лог
        add_to_currencies_log($item->id_currency, 'Удаление сети '.$item->name.' ', 3,6);

        $item->delete();
        return redirect()->back();
    }
}
