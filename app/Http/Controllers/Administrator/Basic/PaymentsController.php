<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Common\Packages\ImageSystem\Facades\ImageSystemFacade;
use App\Common\Packages\ImageSystem\ImageSystem;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PaymentsController extends Controller
{
    /**
     * Список платежных систем
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $payments = Payment::where('is_delete','=', 0)->get();
        $imports = file_get_contents(storage_path('/templates/payments.json'));

        return view('admin.basic.payments.index', [
            'payments' => $payments,
            'imports'  => collect(
                json_decode($imports, true)
            )->pluck('name', 'name')
        ]);
    }

    /**
     * Форма добавления новой платежной системы
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('admin.basic.payments.create');
    }

    /**
     * Обработка и добавление новой платежной системы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
            return redirect()->back();
        }


        // Импорт данных
        if($request->has('import_payments'))
        {
            $imports = json_decode(file_get_contents(storage_path('/templates/payments.json')), true);

            foreach ($request->get('imports_data') as $key => $value)
            {
                $item = $imports[$value];
                Payment::usingLocale('ru')->updateOrCreate([
                    'name' => $item['name'] ?? null,
                ], [
                    'name->ru' => $item['name'] ?? null,
                    'logo' => $item['logo'] ?? null,
                    'is_import' => 1
                ]);
            }

            flash('Импортирование данных завершено', ['alert alert-success']);
            return  redirect()->to(
                admin_base_path('/basic/payments')
            );
        }


        // Обновление данные прямо из таблицы валюты
        if($request->has('item_id'))
        {
            foreach ($request->item_id as $value_id)
            {
                $update = Payment::find($value_id);
                $update_multi_language = [];
                foreach (config('app.form_lang') as $key => $item) {
                    $update_multi_language['name'][$key] = $request->get('name'.$item['field'])[$value_id];
                }
                $update->update($update_multi_language);
            }

            if($request->has('payment_checkbox'))
            {
                foreach ($request->get('payment_checkbox') as $value)
                {
                    $payment = Payment::findOrFail($value);
                    // Визуальное удаление платежной системы
                    if($request->get('actions') == 'trashed') {
                        $payment->update(['is_delete' => 1]);
                    } elseif($request->get('actions') == 'deleteglobal') {
                        $this->fullDestroy($value);
                    }
                }
            }
            return redirect()->back();
        }

        // Проверка входных параметров
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            // Создаем новое имя для логотипа
            $filename = sprintf('iex-%s.png', Str::random(20));

            $options = [
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }
            $payment = Payment::create($options);


            // Загружаем изображение из готового списка
            if($request->has('logo_code') and !empty($request->get('logo_code')))
            {
                $logo_url = sprintf('https://raw.githubusercontent.com/spothq/cryptocurrency-icons/master/128/color/%s.png', Str::lower($request->get('logo_code')));

                try{
                    $responseImage = ImageSystemFacade::make($logo_url)
                        ->setDisk('payment_systems')
                        ->put(public_path('storage/payment_systems'), 'payment_systems');

                    $payment->update([
                        'logo' => $responseImage['filename'],
                        'is_local_image' => $responseImage['is_local_image']
                    ]);

                }catch (\Exception $exception) {
                    flash('Иконка не найдена, попробуйте другую или добавьте вручную', ['alert alert-danger']);
                }

            } else {
                if($request->hasFile('logo'))
                {
                    $responseImage = ImageSystemFacade::make($request->file('logo'))
                        ->setDisk('payment_systems')
                        ->put(public_path('storage/payment_systems'), $payment->logo);

                    $payment->update([
                        'logo' => $responseImage['filename'],
                        'is_local_image' => $responseImage['is_local_image']
                    ]);
                }
            }

            return redirect()->to(admin_base_path('/basic/payments/'));
        }

    }

    /**
     * Изменить платежную систему
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Payment::findOrFail($id);
        return view('admin.basic.payments.edit', compact('item'));
    }

    /**
     * Обработка и обновление данных платежной системы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);

        // Проверка входных параметров
        $validator =  Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }


            $payment->update($options);

            if($request->has('logo_code') and !empty($request->get('logo_code')))
            {
                $logo_url = sprintf('https://raw.githubusercontent.com/spothq/cryptocurrency-icons/master/128/color/%s.png', Str::lower($request->get('logo_code')));

                try{
                    $responseImage = ImageSystemFacade::make($logo_url)
                        ->setDisk('payment_systems')
                        ->put(public_path('storage/payment_systems'), $payment->logo);

                    $payment->update(['logo' => $responseImage['filename'], 'is_local_image' => $responseImage['is_local_image']]);

                }catch (\Exception $exception) {
                    flash('Иконка не найдена, попробуйте другую или добавьте вручную', ['alert alert-danger']);
                }

            } else {
                if($request->hasFile('logo'))
                {
                    $responseImage = ImageSystemFacade::make($request->file('logo'))
                        ->setDisk('payment_systems')
                        ->put(public_path('storage/payment_systems'), $payment->logo);

                    $payment->update(['logo' => $responseImage['filename'], 'is_local_image' => $responseImage['is_local_image']]);
                }
            }

            return redirect()->to(admin_base_path('/basic/payments/'));
        }
    }

    /**
     * Визуальное удаление платежной системы
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $payment = Payment::findOrFail($id);
        $payment->update(['is_delete' => 1]);

        return redirect()->route('payments.index');
    }

    /**
     * Полное удаление Платежной системы
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function fullDestroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $payment = Payment::findOrFail($id);
        // Делаем доп. проверку в списке направлений
        $currency = Currency::where('id_payment', '=', $id);

        if($currency->count() > 0) {
            flash($payment->name. ' удалить невозможно, К платежной системе привязана валюта', ['alert alert-danger']);
            return redirect()->back();
        }
        $payment->delete();
    }

    public function delete_url(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $payment = Payment::findOrFail($id);
        // Делаем доп. проверку в списке направлений
        $currency = Currency::where('id_payment', '=', $id);

        if($currency->count() > 0) {
            flash($payment->name. ' удалить невозможно, К платежной системе привязана валюта', ['alert alert-danger']);
            return redirect()->back();
        }

        flash($payment->name.' успешна удален', ['alert alert-success']);
        $payment->delete();
        return redirect()->route('payments.index');
    }
}
