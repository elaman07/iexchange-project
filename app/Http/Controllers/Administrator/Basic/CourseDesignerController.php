<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Common\Packages\Crypto\Facades\BestChange;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\Favorites;
use App\Models\GroupCommision;
use Illuminate\Http\Request;

class CourseDesignerController extends Controller
{
    protected $allowFiltered = [
        'is_enabled_base_main_direction'
    ];

    /**
     * Список валют
    */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item)
            {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);

            if($request->has('id_direction_exchange_main') and $request->get('id_direction_exchange_main') > 0)
            {
                \DB::table('direction_exchange')->update(['is_main' => 0]);
                DirectionExchange::findOrFail($request->get('id_direction_exchange_main'))->update([
                    'is_main' => 1
                ]);
            }

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        // Фильтр валют получаю
        $exchange_main_relation = DirectionExchange::with(['currency1' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'currency2' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'currency1.payment' => function($q) {
            $q->select('id', 'name');
        }, 'currency2.payment' => function($q) {
            $q->select('id', 'name');
        }, 'currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }])->select('id','id_currency1', 'id_currency2', 'is_main', 'status')->groupBy('id_currency2');
        $exchange_main = collect();
        $exchange_main_relation->chunk(100, function($items) use($exchange_main)
        {
            foreach ($items as $item)
            {
                 $exchange_main->add([
                     'id' => $item->id,
                     'is_main' => $item->is_main,
                     'name' => direction_name($item),
                     'status' => $item->status
                 ]);
            }
        });


        // Список направлений активных
        $exchange_main_data = DirectionExchange::select('id','tech_name', 'is_main', 'status')->get()->map(function($item) {
            return [
                'id' => $item->id,
                'is_main' => $item->is_main,
                'name' => $item->tech_name,
                'status' => $item->status
            ];
        });

        $is_main = [];

        // Список валют
        $currencies = Currency::with(['payment' => function($q) {
            $q->select('id', 'name');
        }, 'code_currency' => function($q) {
            $q->select('id','name','sign');
        },'currency_analytics'])
            ->addSelect('id', 'status', 'id_payment','id_code_currency', 'number_format')
            ->withCount(['direction_exchange_in','direction_exchange_out'])->active()->hideDisabled()
            ->orderBy('id', 'asc')->get();


        return view('admin.basic.course_designer.index', [
            'currencies'    =>  $currencies,
            'exchange_main' => $exchange_main,
            'exchange_main_data' => $exchange_main_data
        ]);
    }


    /**
     * Детали валюты
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function detail(Request $request)
    {
        $item = Currency::find($request->id);

        //Валют в блоке отдаю
        $in = DirectionExchange::with(['currency1' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'currency2' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'currency1.payment' => function($q) {
            $q->select('id', 'name');
        }, 'currency2.payment' => function($q) {
            $q->select('id', 'name');
        }, 'currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'bestchange_rates' => function($q) {
            $q->select('id', 'summa');
        }, 'parser_exchange' => function($q) {
            $q->select('id', 'id_group', 'summa');
        }, 'parser_exchange.group_parse_exchange' => function($q) {
            $q->select('id', 'name');
        }])->select('id','id_currency1', 'id_currency2', 'status',
            'is_main', 'id_bestchange_rates', 'enable_bestchange', 'is_disable_bs_error', 'is_error_rate', 'exchange_rate', 'parser_source_name', 'bestchange_position', 'add_course1', 'id_group_commission', 'id_crypto_parser',
            'profit', 'profit_s', 'is_not_partner')->active()->whereHas('currency2', function($query) {
            $query->hideDisabled();
        })->where('id_currency1', $item->id)->get();

        //Валют в блоке получаю
        $out = DirectionExchange::with(['currency1' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'currency2' => function($q) {
            $q->select('id','id_payment','id_code_currency');
        }, 'currency1.payment' => function($q) {
            $q->select('id', 'name');
        }, 'currency2.payment' => function($q) {
            $q->select('id', 'name');
        }, 'currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'bestchange_rates' => function($q) {
            $q->select('id', 'summa');
        }, 'parser_exchange' => function($q) {
            $q->select('id', 'id_group', 'summa');
        }, 'parser_exchange.group_parse_exchange' => function($q) {
            $q->select('id', 'name');
        }])->select('id','id_currency1', 'id_currency2', 'status',
            'is_main', 'id_bestchange_rates', 'enable_bestchange', 'is_disable_bs_error', 'is_error_rate', 'exchange_rate', 'parser_source_name', 'bestchange_position', 'add_course1', 'id_group_commission', 'id_crypto_parser',
            'profit', 'profit_s', 'is_not_partner')->active()->whereHas('currency1', function($query) {
            $query->hideDisabled();
        })->where('id_currency2', $item->id)->get();

        // Сделать главным направлением
        if($request->has('up_item')) {
            \DB::table('direction_exchange')->update(['is_main' => 0]);

            DirectionExchange::findOrFail($request->get('up_item'))->update([
                'is_main' => 1
            ]);

            return redirect()->back();
        }

        // Групповые обновления
        if($request->has('item_id_in') and is_array($request->get('item_id_in')))
        {
            foreach ($request->get('item_id_in') as $item_in)
            {
                $direction_find = DirectionExchange::find($item_in);

                if(!empty($request->profit_in) and $request->profit_in > 0) {
                    $direction_find->profit = $request->profit_in;
                }

                if(!empty($request->bestchange_position_in) and $request->bestchange_position_in > 0) {
                    $direction_find->bestchange_position = $request->bestchange_position_in;
                }
                $direction_find->save();
            }

            return redirect()->back();
        }

        // Групповые обновления
        if($request->has('item_id_out') and is_array($request->get('item_id_out')))
        {
            foreach ($request->get('item_id_out') as $item_out)
            {
                $direction_find = DirectionExchange::find($item_out);

                if(!empty($request->profit_out) and $request->profit_out > 0) {
                    $direction_find->profit = $request->profit_out;
                }

                if(!empty($request->position_bestchange_out) and $request->position_bestchange_out > 0) {
                    $direction_find->bestchange_position = $request->position_bestchange_out;
                }
                $direction_find->save();
            }

            return redirect()->back();
        }


        if($request->status_in == 'allow') {
            foreach ($in as $value) {
                DirectionExchange::find($value->id)->update([
                    'status' => 1
                ]);
            }
            return redirect()->back();
        }

        if($request->status_in == 'disabled') {
            foreach ($in as $value) {
                DirectionExchange::find($value->id)->update([
                    'status' => 0
                ]);
            }

            return redirect()->back();
        }

        if($request->status_out == 'allow')
        {
            foreach ($out as $value) {
                DirectionExchange::find($value->id)->update([
                    'status' => 1
                ]);
            }

            return redirect()->back();
        }

        if($request->status_out == 'disabled')
        {
            foreach ($out as $value) {
                DirectionExchange::find($value->id)->update([
                    'status' => 0
                ]);
            }

            return redirect()->back();
        }

        if($request->getMethod() == 'POST')
        {
            // Запрещаем некоторым пользователям редактировать направление
            if($request->user()->hasAnyPermission('admin_limit_editing_directions')) {
                flash('Ошибка при редактировании направления', ['alert alert-danger']);
                return redirect()->back();
            }

            if($request->form == 'in' or $request->form == 'out')
            {
                foreach ($request->get('item_id') as $key => $value)
                {
                    $findCurrency = DirectionExchange::find($key);
                    $findCurrency->update([
                        'profit'        =>   $request->profit[$key] ?? 0,
                        'profit_s'        =>  $request->profit_s[$key] ?? 0,
                        'is_not_bonus'  =>  (isset($request->is_not_bonus[$key]) ? 0 : 1),
                        'is_not_partner'  =>  (isset($request->is_not_partner[$key]) ? 0 : 1),
                    ]);

                    if(isset($request->bestchange_position[$key])) {
                        $findCurrency->bestchange_position = $request->bestchange_position[$key];
                    }

                    if($request->has('is_type_parser'))
                    {
                        if($request->get('is_type_parser') == 1) {
                            $findCurrency->enable_bestchange = 1;
                        } elseif($request->get('is_type_parser') == 2) {
                            $findCurrency->enable_bestchange = 0;
                        }

                        $findCurrency->add_course1 = $request->add_course1[$key] ?? 0;
                        $findCurrency->save();
                    }
                }

                if(isset($request->check))
                {
                    foreach ($request->check as $value)
                    {
                        if ($request->actions == 'activation')
                        {
                            $direction = DirectionExchange::find($value);
                            $direction->update([
                                'status' => 1
                            ]);

                        } elseif ($request->actions == 'deactivation') {
                            $direction = DirectionExchange::find($value);
                            $direction->update([
                                'status' => 0
                            ]);
                        }

                        $favorite = Favorites::where('id_direction_exchange', $value);
                        if($request->actions == 'add_favorite')
                        {
                            $direction = DirectionExchange::find($value);
                            if(!$favorite->exists())
                            {
                                Favorites::create([
                                    'id_direction_exchange' => $value,
                                    'type' => 'direction',
                                    'id_currency1'  =>  $direction->id_currency1,
                                    'id_currency2'  =>  $direction->id_currency2
                                ]);
                            }
                        } elseif($request->actions == 'delete_favorite') {
                            if($favorite->exists()) {
                                $favorite->delete();
                            }
                        }
                    }
                }

                return redirect()->back();
            }
        }

        // Групповая комиссия
        $group_commission = GroupCommision::orderByDesc('id')->get();

        return view('admin.basic.course_designer.detail', [
            'item'  =>  $item,
            'in'    =>  $in,
            'out'   =>  $out,
            'group_commission' => $group_commission
        ]);
    }

    /**
     * Типы курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function types(Request $request)
    {
        $directions = DirectionExchange::isEnabled()->get();

        if($request->getMethod() == 'POST')
        {
            foreach ($request->get('directions') as $value)
            {
                $export_label_param = null;
                if(isset($request->export_label_param[$value]))
                    $export_label_param = implode(',', $request->export_label_param[$value]);

                DirectionExchange::find($value)->update([
                    'export_label_param'    =>  $export_label_param,
                    'hidden_export_label_param' => (isset($request->hidden_export_label_param[$value]) ? 1 : 0)
                ]);
            }

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        return view('admin.basic.course_designer.types', [
            'directions'    =>  $directions,
            'export_labels' => [
                'manual'      =>  'Manual',
                'juridical'   =>  'Juridical',
                'verifying'   =>  'Verifying',
                'cardverify'  =>  'Cardverify',
                'floating'    =>  'Floating',
                'otherin'     =>  'Otherin',
                'otherout'    =>  'Otherout'
            ]
        ]);
    }

    public function competitors(Request $request)
    {
        $exchange = DirectionExchange::find($request->id);

        if(!($exchange->bestchange_rates)) {
            throw new \Exception('Ошибка при получение данных конкурентов');
        }

        $rates = $exchange->bestchange_rates;
        $bestchange = BestChange::getRatesFilter($rates->exchanger_id1, $rates->exchanger_id2);
        $exchangers = BestChange::getExchangers();

        $collect = collect($bestchange)->map(function($item)  use($exchangers) {
           $item['name'] = $exchangers[$item['exchanger_id']];
           return $item;
        })->toArray();

        return view('admin.basic.course_designer.competitors', [
            'competitors'     =>  $collect,
            'direction'       =>    $exchange
        ]);
    }

    /**
     * Избранные направления
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function favorites(Request $request)
    {
        $favorites = Favorites::orderBy('sorting')->get();

        if($request->getMethod() == 'POST')
        {
            // Запрещаем некоторым пользователям редактировать направление
            if($request->user()->hasAnyPermission('admin_limit_editing_directions')) {
                flash('Ошибка при редактировании направления', ['alert alert-danger']);
                return redirect()->back();
            }

            if($request->form == 'in')
            {
                foreach ($request->profit as $key => $value)
                {
                    DirectionExchange::find($key)->update([
                        'profit'  =>  $value,
                        'bestchange_position' => $request->bestchange_position[$key],
                        'status'    =>  (isset($request->status[$key]) ? 1 : 0),
                        'is_not_bonus'  =>  (isset($request->is_not_bonus[$key]) ? 0 : 1),
                        'is_not_partner'  =>  (isset($request->is_not_partner[$key]) ? 0 : 1),
                    ]);
                }

                if(isset($request->check))
                {
                    foreach ($request->check as $value)
                    {
                        if ($request->actions == 'activation')
                        {
                            $direction = DirectionExchange::find($value);
                            $direction->update([
                                'status' => 1
                            ]);

                        } elseif ($request->actions == 'deactivation') {
                            $direction = DirectionExchange::find($value);
                            $direction->update([
                                'status' => 0
                            ]);
                        }
                    }
                }
                return redirect()->back();
            }
        }

        return view('admin.basic.course_designer.favorites', [
            'favorites'     =>  $favorites
        ]);
    }

    public function favoriteSorting(Request $request)
    {
        $favorites = Favorites::orderBy('sorting')->get();

        return view('admin.basic.course_designer.favorites_sorting', [
            'favorites'     =>  $favorites
        ]);
    }

    /**
     * Подробнее о направлении
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function direction(Request $request)
    {
        //Детали направления
        $item = DirectionExchange::find($request->item_id);
        //Валюта
        $currency = Currency::find($request->id);

        $favorite = Favorites::where('id_direction_exchange', $request->item_id);

        if($request->actions == 'add_favorites')
        {
            if(!$favorite->exists()) {
                Favorites::create([
                    'id_direction_exchange' => $request->item_id,
                    'type' => 'direction',
                    'id_currency1'  =>  $item->id_currency1,
                    'id_currency2'  =>  $item->id_currency2
                ]);
            }

            return redirect()->back();
        } elseif($request->actions == 'remove_favorites')
        {
            if($favorite->exists()) {
                $favorite->delete();
            }
            return redirect()->back();
        }

        //Действие при сохранении
        if($request->getMethod() == 'POST')
        {
            // Запрещаем некоторым пользователям редактировать направление
            if($request->user()->hasAnyPermission('admin_limit_editing_directions')) {
                flash('Ошибка при редактировании направления', ['alert alert-danger']);
                return redirect()->back();
            }

            if($request->parser == 2) {
                $item->update([
                    'enable_bestchange'         =>  0,
                    'enable_competitors'        =>  1,
                    'status'                    =>  $request->status,
                    'bestchange_position'           =>  ($request->has('bestchange_position') ? $request->get('bestchange_position') : 0),
                    'bestchange_step'           =>  ($request->has('bestchange_step') ? $request->get('bestchange_step') : 0),
                    'enable_group_commission'   => ($request->enable_group_commission ?? 0),
                    'profit'                    => ($request->profit ?? 0),
                ]);
            } else {
                $item->update([
                    'enable_bestchange'         =>  $request->parser,
                    'enable_competitors'        =>  0,
                    'status'                    =>  $request->status,
                    'bestchange_position'       =>  ($request->has('bestchange_position') ? $request->get('bestchange_position') : 0),
                    'bestchange_step'           =>  ($request->has('bestchange_step') ? $request->get('bestchange_step') : 0),
                    'enable_group_commission'   => ($request->enable_group_commission ?? 0),
                    'profit'               => ($request->profit ?? 0)
                ]);
            }

            return redirect()->back();
        }

        //Групповая комиссия
        $group_commission = GroupCommision::all();

        // Alt direction
        $alt = DirectionExchange::where('id_currency1', $item->id_currency2)
            ->where('id_currency2', $item->id_currency1);


        return view('admin.basic.course_designer.direction', [
            'alt_count'         =>  $alt->count(),
            'alt_data'          =>  $alt->first(),
            'data'              =>  $item,
            'currency'          =>  $currency,
            'group_commission'  =>  $group_commission,
            'favorite'          =>  $favorite->exists()
        ]);
    }

    public function commission(Request $request)
    {
        $directions = DirectionExchange::isEnabled()->get();

        // Сохранение данные
        if($request->getMethod() == 'POST')
        {
            // Запрещаем некоторым пользователям редактировать направление
            if($request->user()->hasAnyPermission('admin_limit_editing_directions')) {
                flash('Ошибка при редактировании направления', ['alert alert-danger']);
                return redirect()->back();
            }

            foreach ($request->get('item_id') as $key => $value)
            {
                DirectionExchange::findOrFail($key)->update([
                    'profit' => ($request->profit[$key] ?? 0),
                    'profit_s' => ($request->profit_s[$key] ?? 0),
                ]);
            }

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        return view('admin.basic.course_designer.commission', [
            'directions'    =>  $directions
        ]);
    }
}
