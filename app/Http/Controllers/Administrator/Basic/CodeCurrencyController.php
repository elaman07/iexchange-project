<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Models\{CodeCurrency, Currency, GroupParserExchange, ParserExchange, ParserFormulaRates, Reserve};
use App\Http\Controllers\Controller;
use Illuminate\Http\{
    RedirectResponse, Request
};
use Illuminate\Support\Facades\Validator;

class CodeCurrencyController extends Controller
{

    protected array $allowFilteredPage = [
        'admin_code_pagination',
        'admin_code_hidden_columns'
    ];

    public function index()
    {
        $code_currencies = CodeCurrency::with(['parser_exchange', 'currency', 'currency.payment'])->active()
            ->paginate(iEXSetting('admin_code_pagination', 20));
        $imports = file_get_contents(storage_path('/templates/codes.json'));

        // Парсинг из остальных источников
        $group_rates = GroupParserExchange::select('id','name', 'status')->where('status','=', 1)->get();
        $parser_exchange_allowed = ParserExchange::select('id','summa','name','value','id_group','status')->where('status','=',1)
            ->cursor();

        $parser_exchange = [];
        foreach ($parser_exchange_allowed as $parser_value)
        {
            $parser_exchange[$parser_value->id_group][] = [
                'id' => $parser_value->id,
                'summa' => $parser_value->summa,
                'name' => $parser_value->name,
                'value' => $parser_value->value,
            ];
        }
        $admin_code_hidden_columns = explode(',', iEXSetting('admin_code_hidden_columns'));

        // Получение курсов по формуле
        $rates_formula = ParserFormulaRates::select('id', 'summa', 'title', 'status')->where('status', '=', 1)->get();

        return view('admin.basic.code_currency.index', [
            'codes' => $code_currencies,
            'group_rates' => $group_rates,
            'parser_exchange'  =>   $parser_exchange,
            'imports'  => collect(json_decode($imports, true))->pluck('name', 'name'),
            'admin_code_hidden_columns' => $admin_code_hidden_columns,
            'rates_formula' =>  $rates_formula
        ]);
    }

    /**
     * Форма добавления кода валют
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $group_parsers = GroupParserExchange::where('status','=', 1)->get();
        // Получение курсов по формуле
        $rates_formula = ParserFormulaRates::select('id', 'summa', 'title', 'status')->where('status', '=', 1)->get();
        return view('admin.basic.code_currency.create', compact('group_parsers', 'rates_formula'));
    }

    /**
     * Обработать и добавить код валюты
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request): RedirectResponse
    {
        $admin_code_hidden_columns = explode(',', iEXSetting('admin_code_hidden_columns'));

        if($request->has('action') and $request->get('action') == 'settings_columns')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFilteredPage as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        // Импорт данных
        if($request->has('import_payments'))
        {
            if(config('admin.is_demo_mode')) {
                flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
                return redirect()->back();
            }


            $imports = json_decode(file_get_contents(storage_path('/templates/codes.json')), true);

            foreach ($request->get('imports_data') as $key => $value)
            {
                $item = $imports[$value];
                CodeCurrency::updateOrCreate([
                    'name' => $item['name'] ?? null,
                ], [
                    'name' => $item['name'] ?? null,
                    'sign' => $item['sign'] ?? null,
                    'id_parser_exchange' => 0,
                    'add_to_course' => 0,
                    'id_parser_formula' => 0,
                    'add_to_course_formula' => 0,
                    'is_import' => 1
                ]);
            }

            flash('Импортирование данных завершено', ['alert alert-success']);
            return  redirect()->to(
                admin_base_path('/basic/code_currency')
            );
        }

        if($request->get('actions') == 'save')
        {
            if(config('admin.is_demo_mode')) {
                flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
                return redirect()->back();
            }

            foreach ($request->get('item_id') as $key => $value)
            {
                $findCurrency = CodeCurrency::find($key);
                $update_data = [];

                if(in_array('auto_currect', $admin_code_hidden_columns)) {
                    $update_data['add_to_course'] = ($request->add_to_course[$key] ?? 0);
                    $update_data['id_parser_exchange'] = ($request->id_parser_exchange[$key] ?? 0);
                }

                if(in_array('auto_currect_formula', $admin_code_hidden_columns)) {
                    $update_data['add_to_course_formula'] = ($request->add_to_course_formula[$key] ?? 0);
                    $update_data['id_parser_formula'] = ($request->id_parser_formula[$key] ?? 0);
                }

                if(in_array('course', $admin_code_hidden_columns)) {
                    $update_data['internal_rate'] = ($request->internal_rate[$key] ?? 0);
                }

                $findCurrency->update($update_data);

            }

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();

        }

        if($request->has('actions') and $request->has('check') and count($request->get('check')) > 0)
        {
            if(config('admin.is_demo_mode')) {
                flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
                return redirect()->back();
            }


            foreach ($request->get('check') as $item)
            {
                $code_currency = CodeCurrency::find($item);
                if($request->get('actions') == 'trashed') {
                    $code_currency->update(['is_trashed' => 1]);
                } elseif($request->get('actions') == 'deleteglobal') {
                    $code_currency->delete();
                }
            }

            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'code' =>  'required',
            'sign' => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            $item = CodeCurrency::create([
                'name'                      => $request->get('code'),
                'sign'                      => $request->get('sign'),
                'add_to_course'             => $request->has('add_to_course') ? $request->get('add_to_course') : 0,
                'id_parser_exchange'        => $request->has('id_parser_exchange') ? $request->get('id_parser_exchange') : 0,
                'add_to_course_formula'     => $request->has('add_to_course_formula') ? $request->get('add_to_course_formula') : 0,
                'id_parser_formula'         => $request->has('id_parser_formula') ? $request->get('id_parser_formula') : 0
            ]);
            flash("Код {$item->name} успешно добавлен", ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Форма редактирования кода валюты
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = CodeCurrency::findOrFail($id);
        $group_parsers = GroupParserExchange::where('status','=', 1)->get();
        // Получение курсов по формуле
        $rates_formula = ParserFormulaRates::select('id', 'summa', 'title', 'status')->where('status', '=', 1)->get();

        return view('admin.basic.code_currency.change', compact('item','group_parsers', 'rates_formula'));
    }

    /**
     * Обработать и обновить код валюты
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'code' =>  'required',
            'sign' => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            CodeCurrency::findOrFail($id)->update([
                'name'               => $request->get('code'),
                'sign'               => $request->get('sign'),
                'add_to_course'         => $request->has('add_to_course') ? $request->get('add_to_course') : 0,
                'id_parser_exchange' => $request->has('id_parser_exchange') ? $request->get('id_parser_exchange') : 0,
                'add_to_course_formula'     => $request->has('add_to_course_formula') ? $request->get('add_to_course_formula') : 0,
                'id_parser_formula'         => $request->has('id_parser_formula') ? $request->get('id_parser_formula') : 0
            ]);

            flash('Код валюты успешно обновлен', ['alert alert-success']);
        }

        return redirect()->back();
    }
}
