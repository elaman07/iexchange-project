<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\FileParserGroup;
use App\Models\ReserveFileGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReserveFileGroupController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $links = ReserveFileGroup::paginate(20);
        return view('admin.basic.reserves.files.groups.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.basic.reserves.files.groups.create');
    }

    /**
     * Обработка и добавление
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->get('link') as $key => $value) {
                ReserveFileGroup::find($key)->update([
                    'link'          =>  $value,
                    'status'        =>   (isset($request->status[$key]) ? $request->status[$key] : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link = ReserveFileGroup::create([
                'name'      => $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Файл {$link->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('reserves-files-group.index');
        }
    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ReserveFileGroup::findOrFail($id);
        return view('admin.basic.reserves.files.groups.edit',compact('item'));
    }

    /**
     * Обработка и обновления
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $link = ReserveFileGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link->update([
                'name'      =>  $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Файл {$link->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('reserves-files-group.index');
        }
    }

    /**
     * Удаление ссылок
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $link = ReserveFileGroup::findOrFail($id);

        if($link->reserves_files->count() > 0) {
            flash('Выбранный файл удалить невозможно, К нему привязаны резервы', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Файл {$link->name} успешно удален", ['alert alert-success']);
        $link->delete();

        return redirect()->route('file-parser-group.index');
    }

    /**
     * Сортировка
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting()
    {
        $links = FileParserGroup::where('status', '=', 1)->orderBy('sorting')->get();
        return view('admin.basic.reserves.files.groups.sorting', [
            'links'    =>  $links
        ]);
    }
}
