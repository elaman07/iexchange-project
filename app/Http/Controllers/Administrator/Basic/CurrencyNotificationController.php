<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 06.11.2019
 * Time: 20:23
 */

namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\CurrencyNotification as CurrencyNotificationModel;


class CurrencyNotificationController extends Controller
{
    /**
     * Список уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $notifications = CurrencyNotificationModel::orderBy('id')->get();
        return view('admin.basic.currency.notification.index', compact('notifications'));
    }

    /**
     * Форма добавления уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        $currencies = Currency::enabledCurrency()->get();
        return view('admin.basic.currency.notification.create', [
            'currencies' => $currencies
        ]);
    }

    /**
     * Обработка и добавление уведомлений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_currency' => 'required',
            'status' => 'required',
            'description' => 'required',
            'css_class' =>  'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            $item = CurrencyNotificationModel::create($request->all());

            // Записываем в лог
            add_to_currencies_log($item->id_currency, 'Уведомление добавлено: '.$item->description.' ', 0,2);
            flash('Уведомление успешно добавлено', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Форма редактирования уведомлений
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = CurrencyNotificationModel::findOrFail($id);
        $currencies = Currency::active()->get();
        return view('admin.basic.currency.notification.edit',compact('item', 'currencies'));
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $notification = CurrencyNotificationModel::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_currency' => 'required',
            'status' => 'required',
            'description' => 'required',
            'css_class' =>  'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $notification->update($request->all());

            // Записываем в лог
            add_to_currencies_log($notification->id_currency, 'Уведомление обновлена: '.$notification->description.' ', 1,2);
        }

        flash('Уведомление успешно обновлена', ['alert alert-success']);
        return redirect()->route('currency-notification.index');
    }

    /**
     * Удаление уведомлений
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $notify = CurrencyNotificationModel::findOrFail($id);
        // Записываем в лог
        add_to_currencies_log($notify->id_currency, 'Уведомление удалена: '.$notify->description.' ', 3,2);

        $notify->delete();

        flash('Уведомление успешно удалено', ['alert alert-success']);

        return redirect()->route('currency-notification.index');
    }

    /**
     * Сортировка уведомлений
     */
    public function sorting()
    {
        $currencies = CurrencyNotificationModel::orderBy('sorting')->get();
        return view('admin.basic.currency.notification.sorting', [
            'currencies' => $currencies
        ]);
    }
}
