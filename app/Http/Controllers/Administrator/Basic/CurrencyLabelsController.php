<?php

namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\CurrencyGroup;
use App\Models\CurrencyLabel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CurrencyLabelsController extends Controller
{
    /**
     * Список меток
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $labels = CurrencyLabel::orderBy('id', 'desc')->get();
        return view('admin.basic.currency.labels.index', [
            'labels'    =>  $labels
        ]);
    }

    /**
     * Форма добавления метки
     */
    public function create()
    {
        return view('admin.basic.currency.labels.create');
    }

    /**
     * Обработка и добавления новой метки
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [];
            if($request->hasFile('image'))
            {
                if(!\File::isDirectory(public_path('storage/currency-labels/'))) {
                    \File::makeDirectory(public_path('storage/currency-labels/'), 0777, true, true);
                }

                $logo_icon = $request->file('image');
                $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());



                $destinationPath = public_path('/storage/currency-labels');
                $logo_icon->move($destinationPath, $filename);
                $options['image'] = $filename;
            }

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
            }

            $options['bg_color'] = $request->has('bg_color') ? $request->get('bg_color') : null;
            $options['text_color'] = $request->has('text_color') ? $request->get('text_color') : null;


            $item = CurrencyLabel::create($options);
            $item->save();

            flash(__('Метка успешно добавлена'), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения метки
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|RedirectResponse
     */
    public function edit(Request $request, int $id) {

        $item = CurrencyLabel::findOrFail($id);

        // Удаление фото
        if($request->get('type') == 'delete')
        {
            // Удаляем актуальное фото
            iex_file_delete(public_path('storage/currency-labels/'.$item->image));
            $item->image = null;
            $item->save();
            return redirect()->back();
        }

        return view('admin.basic.currency.labels.edit',compact('item'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $label = CurrencyLabel::findOrFail($id);

        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [];
            if($request->hasFile('image'))
            {
                if(!\File::isDirectory(public_path('storage/currency-labels/'))) {
                    \File::makeDirectory(public_path('storage/currency-labels/'), 0777, true, true);
                }

                $logo_icon = $request->file('image');
                $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());


                $destinationPath = public_path('/storage/currency-labels');
                $logo_icon->move($destinationPath, $filename);
                $options['image'] = $filename;
            }

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
            }

            $options['bg_color'] = $request->has('bg_color') ? $request->get('bg_color') : null;
            $options['text_color'] = $request->has('text_color') ? $request->get('text_color') : null;


            $label->update($options);

            flash('Метка '.$label->title.' успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удалить метку
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        // Делаем доп. проверку в списке направлений
        $currency = Currency::where('id_label', '=', $id);

        if($currency->count() > 0) {
            flash('Выбранную метку удалить невозможно, К меткам привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = CurrencyLabel::findOrFail($id);

        flash(
            sprintf('Метка %s успешно удалена', $item->name),
            ['alert alert-success']
        );

        $item->delete();
        return redirect()->back();
    }
}
