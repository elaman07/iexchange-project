<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UserWalletController extends Controller
{
    /**
     * Получить доступ к счетам пользователей
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $wallets = Task::filter($request->all())->where('status', '=', 4)
            ->whereNotNull('to_shot')
            ->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('admin.basic.user_wallets', $conditions);
        }


        // Резервы
        $currencies = Currency::where('status', '=', 0)->get()->map(function ($item) {
            return [
                'value' => sprintf('%s %s', $item->payment->name, $item->code_currency->name),
                'id' => $item->id
            ];
        })->pluck('value', 'id');


        return view('admin.basic.user_wallet.index', [
            'wallets'   =>  $wallets,
            'currencies' => $currencies,
            'filter' => $request->all()
        ]);
    }
}