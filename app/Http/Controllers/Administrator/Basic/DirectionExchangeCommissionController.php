<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\DirectionExchangeGroup;
use App\Models\GroupCommision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectionExchangeCommissionController extends Controller
{
    /**
     * Список групповых комиссий
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $groups = GroupCommision::all();

        return view('admin.basic.direction_exchange.commission.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления группы
     */
    public function create()
    {
        return view('admin.basic.direction_exchange.commission.create');
    }

    /**
     * Обработка и добавления новой групповой комиссии
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        // Общая обновление
        if($request->has('group_out'))
        {
            foreach ($request->get('group_out') as $key => $value) {
                GroupCommision::findOrFail($key)->update([
                    'receiving' => ($request->group_out[$key] ?? 0),
                ]);
            }

            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'receiving' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = GroupCommision::create([
                'name'  =>  ($request->has('name') ? $request->get('name') : null),
                'receiving'  =>  ($request->has('receiving') ? $request->get('receiving') : 0),
                'id_user' => $request->user()->id
            ]);

            flash("Групповая комиссия {$item->name} успешно добавлена", ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = GroupCommision::findOrFail($id);
        return view('admin.basic.direction_exchange.commission.edit',compact('item'));
    }

    /**
     * Обработчик обновления групповой комиссии
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = GroupCommision::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'receiving' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $group->update([
                'name'  =>  ($request->has('name') ? $request->get('name') : null),
                'receiving'  =>  ($request->has('receiving') ? $request->get('receiving') : 0)
            ]);

            flash('Групповая комиссия '.$group->name.' успешно обновлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить групповую комиссию
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = GroupCommision::findOrFail($id);

        if($item->direction_exchange->count() > 0) {
            flash('Это группу нельзя удалить, к ней привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Групповая комиссия {$item->name} успешно удалена", ['alert alert-success']);
        $item->delete();

        return redirect()->back();
    }
}
