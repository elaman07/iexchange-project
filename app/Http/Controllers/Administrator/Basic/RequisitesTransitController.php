<?php

namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\TransitRequisite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RequisitesTransitController extends Controller
{
    /**
     * Список групп резервов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $items = TransitRequisite::all();
        return view('admin.basic.requisites.transit.index', [
            'items'    =>  $items
        ]);
    }

    /**
     * Форма добавления группы
     */
    public function create()
    {
        $currencies = Currency::all()->reject(function($item) {
            return !isset($item->payment);
        })->reject(function($item) {
            return !isset($item->code_currency);
        });
        return view('admin.basic.requisites.transit.create', compact('currencies'));
    }

    /**
     * Обработка и добавления новой группы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_currency' => 'required',
            'account' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = TransitRequisite::create([
                'id_currency'  =>  ($request->has('id_currency') ? $request->get('id_currency') : 0),
                'account'  =>  ($request->has('account') ? $request->get('account') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            $item->save();

            flash('Реквизит успешно добавлен', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = TransitRequisite::findOrFail($id);
        $currencies = Currency::all()->reject(function($item) {
            return !isset($item->payment);
        })->reject(function($item) {
            return !isset($item->code_currency);
        });
        return view('admin.basic.requisites.transit.edit',compact('item', 'currencies'));
    }

    /**
     * Обработчик обновления реквизитов
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $transit = TransitRequisite::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_currency' => 'required',
            'account' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $transit->update([
                'id_currency'  =>  ($request->has('id_currency') ? $request->get('id_currency') : 0),
                'account'  =>  ($request->has('account') ? $request->get('account') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash('Реквизит успешно обновлен', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удалить реквизит
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = TransitRequisite::findOrFail($id);
        $item->delete();

        return redirect()->back();
    }
}
