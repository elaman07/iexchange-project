<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Models\Discount;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    public function index()
    {
        $discounts = Discount::all();

        return view('admin.basic.discounts.index', [
            'discounts' => $discounts
        ]);
    }

    public function create(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            Discount::create($request->all());
            return redirect(config('admin.directory').'/basic/discounts');
        }

        return view('admin.basic.discounts.create');
    }

    public function change(Request $request)
    {
        $data = Discount::find($request->id);

        if($request->getMethod() == 'POST') {
            $data->amount   = $request->amount;
            $data->discount = $request->discount;
            $data->save();

            return redirect(config('admin.directory').'/basic/discounts');
        }

        return view('admin.basic.discounts.change', [
            'data' => $data
        ]);
    }

    public function delete(Request $request)
    {
        $delete = Discount::find($request->id);
        $delete->delete();

        return redirect(config('admin.directory').'/basic/discounts');
    }
}