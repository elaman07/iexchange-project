<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\WhitelistOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class WhiteListController extends Controller
{
    /**
     * Ключи для настроек
     *
     * @var array
     */
    protected $attributes = [
        'is_local_whitelist'
    ];

    /**
     * Доступные типы
     *
     * @var array
    */
    protected $types = [
        0   =>  'счет',
        1   =>  'email',
        2   =>  'ip'
    ];

    public function index(Request $request)
    {
        // Очистить весь список
        if($request->has('clear_all')) {
            WhitelistOrder::query()->delete();
            flash('Белый список успешно очищен', ['alert alert-success']);

            return redirect()->back();
        }

        $lists = WhitelistOrder::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('whitelist.index', $conditions);
        }

        return view('admin.basic.whitelist.index', [
            'lists' =>  $lists,
            'types' =>  $this->types,
            'filter' => $request->all(),
        ]);
    }

    public function create(Request $request)
    {
        return view('admin.basic.whitelist.create',[
            'types' =>  $this->types,
            'query' =>  $request->all()
        ]);
    }

    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->attributes as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $list = preg_split('/\r\n|\r|\n/', $request->value);
        foreach ($list as $item)
        {
            WhitelistOrder::create([
                'value' =>  $item,
                'type'  =>  $request->type,
                'text'  =>  $request->text
            ]);
        }

        return redirect(admin_path('basic/whitelist'));
    }

    public function edit($id) {
        $item = WhitelistOrder::findOrFail($id);
        return view('admin.basic.whitelist.edit', [
            'item'  =>  $item,
            'types' =>  $this->types
        ]);
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $blacklist = WhitelistOrder::findOrFail($id);
        $blacklist->update($request->except('_token'));

        return redirect()->route('whitelist.index')
            ->with('flash_message',
                'Page'. $blacklist->name.' updated!');
    }

    public function destroy($id)
    {
        $role = WhitelistOrder::findOrFail($id);
        $role->delete();

        return redirect()->route('whitelist.index');
    }

    /**
     * Настройки белого списка
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings(Request $request)
    {
        if($request->method() == 'POST')
        {
            $options = [];
            foreach ($this->attributes as $item)
            {
                if($request->has($item) and is_array($request->get($item))) {
                    $options[$item] = implode(',', $request->get($item));
                } else {
                    $options[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($options);
            flash('Настройки успешно сохранены', 'alert alert-success');

            return redirect()->back();
        }

        return view('admin.basic.whitelist.settings');
    }
}