<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Models\Currency;
use App\Models\CurrencyFields;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyFieldsController extends Controller
{

    /**
     * Список дополнительных полей
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin.basic.currency_fields.index',[
            'fields' => CurrencyFields::orderBy('id')->get()
        ]);
    }

    /**
     * Форма добавления доп. поля
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');

        return view('admin.basic.currency_fields.create', [
            'currencies' => $currencies
        ]);
    }

    /**
     * Обработка и добавление полей + настройки
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            if($request->has('item_id'))
            {
                foreach ($request->item_id as $key => $item)
                {
                    CurrencyFields::findOrFail($key)->update([
                        'min_char'          =>  $request->min_char[$key],
                        'max_char'          =>  $request->max_char[$key],
                        'obligatory_field'  =>  $request->obligatory_field[$key],
                        'field_type'        =>  $request->field_type[$key],
                        'status'            =>  ($request->status[$key] ?? 1)
                    ]);
                }
                flash('Данные успешно обновлены', ['alert alert-success']);
            }
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'key_id' => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $field = CurrencyFields::create($request->except('currencies_in', 'currencies_out', 'name'));

            $options = [];
            foreach (config('app.form_lang') as $key => $item)
            {
                $name_filter = str_replace(['"', "'"], '', $request->get('name'.$item['field']));
                $options['name'][$key] = $name_filter;
                $options['first_char'][$key] = $request->get('first_char'.$item['field']);
                $options['checkbox_title'][$key] = $request->get('checkbox_title'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
                $options['list_text'][$key] = $request->get('list_text'.$item['field']);
            }

            $field->update($options);
            $field->currencies_in()->sync($request->currencies_in ?? []);
            $field->currencies_out()->sync($request->currencies_out ?? []);

            flash('Доп. поле успешно добавлена', ['alert alert-success']);
        }

        return redirect(admin_base_path('/basic/currency_fields'));

    }

    /**
     * Форма редактирование доп. полей для валют
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $field = CurrencyFields::findOrFail($id);

        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');

        return view('admin.basic.currency_fields.change', [
            'field'  =>  $field,
            'currencies' => $currencies
        ]);
    }


    /**
     * Обработка и обновление данных
    */
    public function update(int $id, Request $request)
    {
        $field = CurrencyFields::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'key_id' => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $name_filter = str_replace(['"', "'"], '', $request->get('name'.$item['field']));
                $options['name'][$key] = $name_filter;
                $options['first_char'][$key] = $request->get('first_char'.$item['field']);
                $options['checkbox_title'][$key] = $request->get('checkbox_title'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
                $options['list_text'][$key] = $request->get('list_text'.$item['field']);
            }

            $field->update(array_merge(
                $request->except('currencies_in', 'currencies_out', 'name'), $options
            ));
            $field->currencies_in()->sync($request->currencies_in ?? []);
            $field->currencies_out()->sync($request->currencies_out ?? []);

            flash('Данные успешно обновлены', ['alert alert-success']);
        }

        return redirect(admin_base_path('/basic/currency_fields/'.$field->id.'/edit'));
    }

    /**
     * Удаляем доп. поле
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        $field = CurrencyFields::findOrFail($id);
        if($field->currencies_in->count() > 0 or $field->currencies_out->count() > 0) {
            flash('Выбранное поле удалить невозможно, К этому поле привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        $field->delete();

        return redirect(admin_base_path('/basic/currency_fields/'));
    }

    /**
     * Сортировка полей Отдаю
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting(): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $fields = CurrencyFields::orderBy('sorting')->orderBy('id', 'desc')->get();

        return view('admin.basic.currency_fields.sorting', [
            'fields'  =>  $fields
        ]);
    }

    /**
     * Сортировка полей Получаю
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting_out(): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $fields = CurrencyFields::orderBy('sorting_out')->orderBy('id', 'desc')->get();

        return view('admin.basic.currency_fields.sorting_out', [
            'fields'  =>  $fields
        ]);
    }
}
