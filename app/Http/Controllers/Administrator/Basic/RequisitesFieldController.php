<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\RequisiteField;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RequisitesFieldController extends Controller
{
    /**
     * Список групп платежных реквизитов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $fields = RequisiteField::orderByDesc('id')->get();
        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');

        return view('admin.basic.requisites.fields.index', [
            'fields'    =>  $fields,
            'currencies' => $currencies
        ]);
    }

    /**
     * Форма добавления поля
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');
        return view('admin.basic.requisites.fields.create', compact('currencies'));
    }

    /**
     * Обработка и добавления нового поля
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->item_id as $key => $item) {
                $item = RequisiteField::findOrFail($key);
                $item->update([
                    'status' =>  ($request->status[$key] ?? 0)
                ]);

                $item->currencies()->sync($request->currencies[$key] ?? []);
            }

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'value' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {


            $options = [
                'value'  =>  ($request->has('value') ? $request->get('value') : null),
                'prefix'  =>  ($request->has('prefix') ? $request->get('prefix') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0),
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['comment'][$key] = $request->get('comment'.$item['field']);
            }

            $item = RequisiteField::create($options);
            $item->currencies()->sync($request->currencies ?? []);

            flash('Новое поле успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = RequisiteField::findOrFail($id);
        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');
        return view('admin.basic.requisites.fields.edit', compact('item', 'currencies'));
    }

    /**
     * Обработчик обновления поля
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $field = RequisiteField::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'value' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'value'  =>  ($request->has('value') ? $request->get('value') : null),
                'prefix'  =>  ($request->has('prefix') ? $request->get('prefix') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0),
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['comment'][$key] = $request->get('comment'.$item['field']);
            }


            $field->update($options);
            $field->currencies()->sync($request->currencies ?? []);

            flash('Поле '.$field->name.' успешно добавлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить поле
     *
     * @param $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id): RedirectResponse
    {
        $field = RequisiteField::findOrFail($id);
        if($field->currencies->count() > 0) {
            flash('Выбранное поле удалить невозможно, К этому поле привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Поле {$field->name} успешно удалена", ['alert alert-success']);
        $field->delete();

        return redirect()->back();
    }

    /**
     * Сортировка групп
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sorting()
    {
        $groups = RequisiteField::orderBy('sorting')->get();
        return view('admin.basic.requisites.fields.sorting', compact('groups'));
    }
}
