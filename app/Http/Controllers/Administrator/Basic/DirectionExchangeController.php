<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Common\Packages\Crypto\BestchangeHandler;
use App\Models\{
    CompetitorLink, Currency, CurrencyNetwork, DirectionDay,
    DirectionExchange, DirectionExchangeCity, DirectionExchangeErrorLog,
    DirectionExchangeGroup, DirectionExchangeMode, DirectionExchangePercentAmount,
    DirectionField, DirectionNotification, DirectionRequisite, FileParserGroup,
    GatewayMerchant, GeoCountryList, GroupCommision, GroupParserExchange,
    ParserExchange, ParserFormulaRates, Task, TaskStatus, UnpaidItem, YourExchange, BestChangeRatesModel
};
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\{
    Http\Request,
    Support\Arr,
    Support\Facades\Validator
};
use Modules\Cities\Entities\CitiesModel;

class DirectionExchangeController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_only_first_direction',
        'is_enabled_possible_directions',
        'generate_min_price',
        'generate_max_price',
        'is_direction_exchange_selected_all_courses',
        'is_direction_exchange_update_minmax',
        'is_enabled_uncreated_directions',
        'is_enabled_geo_direction_country'
    ];

    protected $allowFilteredPage = [
        'num_direction_paginate',
        'admin_direction_hidden_columns'
    ];

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFilteredPrice = [
        'price_adjustment_pagination',
    ];


    protected BestchangeHandler $bestchange;

    /**
     * Конструктор направлений
    */
    public function __construct()
    {
        $this->bestchange = new BestchangeHandler();
    }

    /**
     * Список направлений
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Routing\Redirector|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
    {
        if($request->has('update_rates'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            try {
                \Artisan::call('compiler:courses');

                // Активация режима тестирования
                if(iEXSetting('is_crypto_parser_test', 0) == 1) {
                    echo '<pre>';
                    print_r(\Artisan::output());
                    echo '</pre>';
                    exit;
                }
                flash(__('Курсы обновлены'), ['alert alert-success']);
            }catch (\Exception $e) {
                flash($e->getMessage() , ['alert alert-danger']);
            }
            return  redirect()->to(admin_base_path('/basic/direction_exchange'));
        }

        // Фильтры
        $exchanges = DirectionExchange::with([
            'currency2',
            'currency1',
            'currency2.payment',
            'currency1.payment',
            'currency2.code_currency',
            'currency1.code_currency',
            'bs_alt_parser',
            'bs_alt_parser.group_parse_exchange',
            'partner_parser_rates',
            'partner_parser_rates.partner_parser_group',
            'parser_exchange',
            'parser_exchange.group_parse_exchange',
            'competitor_rates',
            'competitor_rates.competitor_link',
            'group_commission',
            'bestchange_rates'
        ])->active()->orderBy('sorting_admin')->orderBy('id', 'desc')->filter($request->all());

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('direction_exchange.index', $conditions);
        }


        // Валюты отдаю
        $currencies_filter = \Cache::remember('1admin-direction-exchange__currency', \Illuminate\Support\Carbon::now()->addMinutes(1), function()
        {
            $currencies_filter_cursor = Currency::with(['payment' => function($q)
            {
                $q->select('id','name');
            }, 'code_currency' => function($q) {
                $q->select('id', 'name');
            }])->active()->select('id', 'status', 'id_payment', 'id_code_currency');

            $currencies_filter = collect();
            $currencies_filter_cursor->chunk(100, function($items) use($currencies_filter) {
                foreach ($items as $item)
                {
                    $status_name = __('включена');
                    if($item->status == 1)
                        $status_name = __('отключена');
                    elseif($item->status == 2)
                        $status_name = __('в архиве');

                    $currencies_filter->add([
                        'id' => $item->id,
                        'name'  =>  sprintf('%s %s (%s)', $item->payment->name, $item->code_currency->name, $status_name)
                    ]);
                }
            });

            return $currencies_filter;
        });

        // Группы направлений
        $groups_direction = DirectionExchangeGroup::pluck('name', 'id');

        $exchange_main = [];

        // Парсинг из остальных источников
        $group_rates = GroupParserExchange::select('id','name', 'status')->where('status','=', 1)->get();
        $parser_exchange_allowed = ParserExchange::select('id','summa','name','value','id_group','status')->where('status','=',1)->cursor();

        $parser_exchange = [];
        foreach ($parser_exchange_allowed as $item)
        {
            $parser_exchange[$item->id_group][] = [
                'id' => $item->id,
                'summa' => $item->summa,
                'name' => $item->name,
                'value' => $item->value,
            ];
        }

        $admin_direction_hidden_columns = explode(',', iEXSetting('admin_direction_hidden_columns'));
        return view('admin.basic.direction_exchange.index', [
            'uncreated_directions'  =>  $this->uncreatedDirections(),
            'groups_direction'      =>  $groups_direction,
            'exchange_main'         =>  $exchange_main,
            'exchanges'             =>  $exchanges->paginate((int)iEXSetting('num_direction_paginate', 20)),
            'currencies_filter'     =>  $currencies_filter->pluck('name', 'id'),
            'filter'                =>  $request->all(),
            'group_rates'           =>  $group_rates,
            'parser_exchange'       =>  $parser_exchange,
            'admin_direction_hidden_columns' => $admin_direction_hidden_columns
        ]);
    }

    /**
     * Обработка и добавление направлений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $admin_direction_hidden_columns = explode(',', iEXSetting('admin_direction_hidden_columns'));

        // Обновление данные прямо из таблицы валюты
        if($request->get('actions') == 'save')
        {
            foreach ($request->get('item_id') as $key => $value)
            {
                $findCurrency = DirectionExchange::find($key);
                $options_data1 = [];

                if(in_array('profit', $admin_direction_hidden_columns)) {
                    $options_data1['profit'] = $request->profit[$key] ?? 0;
                    $options_data1['profit_s'] = $request->profit_s[$key] ?? 0;
                }

                if(in_array('auto_currect', $admin_direction_hidden_columns)) {
                    $options_data1['id_crypto_parser'] = $request->id_crypto_parser[$key] ?? 0;
                }

                $findCurrency->update($options_data1);
            }

            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('action') and $request->get('action') == 'settings_columns')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFilteredPage as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);

            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions'))
        {
            if($request->has('check') and count($request->check) > 0)
            {
                foreach ($request->check as $item)
                {
                    if($request->actions == 'activation') {
                        $direction = DirectionExchange::find($item);
                        $direction->update([
                            'status' => 1
                        ]);

                    }elseif($request->actions == 'deactivation') {
                        $direction = DirectionExchange::find($item);
                        $direction->update([
                            'status' => 0
                        ]);
                    } elseif($request->actions == 'delete') {
                        $this->disabledExchange($item, false);
                    } elseif($request->actions == 'duplicate') {
                        return $this->duplicate($item);
                    } elseif($request->actions == 'deleteglobal') {
                        $this->destroyGlobal($item);
                    }
                }

                return redirect()->back();
            } else {
                return redirect()->back();
            }
        }

        $validator = Validator::make($request->all(), [
            'id_currency1' => ['required', 'exists:currencies,id'],
            'id_currency2' => ['required', 'exists:currencies,id'],
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            if($request->get('id_currency1') == $request->get('id_currency2')) {
                flash(__('Запрещено создавать направления с одинаковыми валютами'), ['alert alert-danger']);
                return redirect()->back();
            }

            $find = DirectionExchange::active()->where([
                ['id_currency1', '=', $request->get('id_currency1')],
                ['id_currency2', '=', $request->get('id_currency2')],
            ]);

            if($find->exists()) {
                flash(__('Такое направление уже существует'), ['alert alert-danger']);
                return redirect()->back();
            }

            $response = DirectionExchange::create([
                'id_currency1' => $request->get('id_currency1'),
                'id_currency2' => $request->get('id_currency2'),
            ]);

            $name = direction_name($response);
            $response->update([
                    'tech_name' =>  $request->get('tech_name') ?? $name
                ]
            );

            // Записываем в лог
            add_to_currencies_log($response->id_currency1, __('Направление :name добавлено', ['name' => $name]), 0,4);
            add_to_currencies_log($response->id_currency2, __('Направление :name добавлено', ['name' => $name]), 0,4);

            flash(__('Направление :name успешно добавлено', ['name' => $name]), ['alert alert-success']);
            return redirect()->to(
                admin_base_path('/basic/direction_exchange/'.$response->id.'/edit')
            );
        }
    }

    /**
     * Форма редактирования направлений
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(int $id, Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
    {

        if($request->has('actions') and $request->get('actions') == 'exchange_amount')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            for($i = 1; $i <= (int)$request->get('elem'); $i++) {
                DirectionExchangePercentAmount::create([
                    'id_direction_exchange' => $id,
                    'percentage' => 0,
                    'amount' => 0
                ]);
            }
            return redirect()->to(
                admin_base_path('/basic/direction_exchange/'.$id.'/edit')
            );
        }

        // Детали направления
        $item = DirectionExchange::with([
            'direction_exchange_cities',
            'group_commission',
            'direction_modes',
            'direction_exchange_cities.city',
            'direction_forbidden_countries',
            'direction_allowed_countries'
        ])->findOrFail($id);

        // Парсинг курсов BestChange
        $bestchange_rates = BestChangeRatesModel::cursor()->sortBy('name', SORT_ASC, true)->reverse();

        // Парсинг из остальных источников
        $group_rates = GroupParserExchange::select('id','name', 'status')->where('status','=', 1)->get();

        // Получение списка активных валют
        $currencies = Currency::with(['payment' => function($q) {
            $q->select('id','name');
        },'code_currency' => function($q) {
            $q->select('id','name');
        }])->select('id','status','id_payment','id_code_currency')
            ->active()->get()->sortBy('id_payment', SORT_ASC, true)->reverse();

        // Групповая комиссия
        $group_commission = GroupCommision::orderByDesc('id')->cursor();

        // Группы направлений
        $groups_direction = DirectionExchangeGroup::orderByDesc('id')->get();

        // Курсы конрурентов
        $competitors = CompetitorLink::where('status', '=', 1)->get();

        // Файл курсов
        $file_parser_groups = FileParserGroup::where('status', '=', 1)->get();

        // Курсы из формулы
        $parser_formula = ParserFormulaRates::where('status', '=', 1)->get();

        try {
            $bs_exchangers = \Cache::remember('bs-exchangers-list', Carbon::now()->addDay(), function() {
                return $this->bestchange->getExchangers();
            });

            $bs_cities = \Cache::remember('bs-cities-list5', Carbon::now()->addDay(), function() {
                return collect($this->bestchange->getCities())->map(function($value, $key) {
                   return [
                       'id' => $key,
                       'name' => $value
                   ];
                })->whereIn('id', (array)explode(',', iEXSetting('bestchange_cities_parsers')))->pluck('name', 'id')->toArray();

            });
        }catch (\Exception $exception) {
            $bs_exchangers = [];
            $bs_cities = [];
        }


        //Статусы заявок
        $auto_del_order_status = TaskStatus::whereIn('id', [1, 2, 6, 10, 11])->pluck('name','id');

        $custom_fields = DirectionField::whereStatus(0)->pluck('name', 'id');
        $requisites = DirectionRequisite::whereStatus(1)->pluck('name', 'id');

        $cities_data = [];
        if(\Module::find('Cities')->isEnabled() == 1) {
            $cities_data = CitiesModel::whereStatus(1)->pluck('name', 'id');
        }


        $networks = CurrencyNetwork::pluck('name', 'id');


        // Список мерчантов
        $merchants = GatewayMerchant::where('status', '=', 1)->get()->pluck('name', 'id');

        // Список стран
        $geo_countries = iEXSetting('is_enabled_geo_direction_country') == 1 ? GeoCountryList::pluck('value', 'id') : [];

        // Режимы работ
        $direction_modes = DirectionExchangeMode::pluck('name','id');


        $parser_exchange_allowed = ParserExchange::select('id','summa','name','value','id_group','status')
            ->where('status','=',1)->cursor();
        $parser_exchange = [];
        foreach ($parser_exchange_allowed as $parser_value)
        {
            $parser_exchange[$parser_value->id_group][] = [
                'id' => $parser_value->id,
                'summa' => $parser_value->summa,
                'name' => $parser_value->name,
                'value' => $parser_value->value,
            ];
        }

        try {
            $bs_exchangers = \Cache::remember('bs-exchangers-list', Carbon::now()->addDay(), function() {
                return $this->bestchange->getExchangers();
            });
            $bestchange_currencies_get = json_decode(file_get_contents(config('bestchange.bestchange_json')), true);
            $bestchange_currencies = collect($bestchange_currencies_get)->whereIn('id', explode(',',iEXSetting('bestchange_currencies_parsers')))->pluck('name', 'id');


        }catch (\Exception $exception) {
            $bs_exchangers = [];
            $bestchange_currencies = [];
        }

        return view('admin.basic.direction_exchange.edit', [
            'bestchange_currencies'    =>  $bestchange_currencies,
            'parser_exchange'           =>  $parser_exchange,
            'direction_modes'           =>  $direction_modes,
            'geo_countries'             =>  $geo_countries,
            'requisites'                =>  $requisites,
            'merchants'                 =>  $merchants,
            'cities_data'               =>  $cities_data,
            'auto_del_order_status'     =>  $auto_del_order_status,
            'custom_fields'             =>  $custom_fields,
            'bs_exchangers'             =>  $bs_exchangers,
            'bs_cities'                 =>  $bs_cities,
            'groups_direction'          =>  $groups_direction,
            'group_rates'               =>  $group_rates,
            'file_parser_groups'        =>  $file_parser_groups,
            'competitors_links'         =>  $competitors,
            'currencies'                =>  $currencies,
            'your_exchange'             =>  YourExchange::all(),
            'bestchange_rates'          =>  $bestchange_rates,
            'group_commission'          =>  $group_commission,
            'data'                      =>  $item,
            'parser_formula'            => $parser_formula,
            'networks'                  =>  $networks
        ]);
    }

    /**
     * Обработка и обновление направлений
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request): \Illuminate\Http\RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'id_currency1' => ['required', 'exists:currencies,id'],
            'id_currency2' => ['required', 'exists:currencies,id'],
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            // Детали направления
            $item = DirectionExchange::findOrFail($id);


            // Перед созданием проверяем, не создает ли клиент новую групповую комиссию
            $id_group_commission = ($request->has('id_group_commission') ? $request->get('id_group_commission') : 0);
            if($request->has('id_group_commission') and $request->get('id_group_commission') == 'add')
            {
                $group_comm_data = GroupCommision::create([
                    'name'  =>  ($request->has('group_commission_name') ? $request->get('group_commission_name') : null),
                    'give'  =>  ($request->has('group_commission_value') ? $request->get('group_commission_value') : 0),
                    'receiving'  =>  ($request->has('group_commission_value') ? $request->get('group_commission_value') : 0),
                    'id_user' => $request->user()->id
                ]);
                $id_group_commission = $group_comm_data->id;
            }

            // Добавление нового Bestchange парсинга
            $id_bestchange_rates = ($request->has('id_bestchange_rates') ? $request->get('id_bestchange_rates') : 0);
            if($request->has('id_bestchange_rates') and $request->get('id_bestchange_rates') == 'add')
            {
                if($request->get('bestchange_rates_exchanger_id1') == $request->get('bestchange_rates_exchanger_id2')) {
                    flash(__('Запрещено создавать одинаковые пары для Bestchange парсера'), ['alert alert-danger']);
                    return redirect()->back();
                }

                // Проверяем, если пара существует то не пускаем дальше
                $check_bestchange_rate = BestChangeRatesModel::where([
                    ['exchanger_id1', '=', $request->get('bestchange_rates_exchanger_id1')],
                    ['exchanger_id2', '=', $request->get('bestchange_rates_exchanger_id2')],
                    ['status', '=', 1]
                ])->first();

                // Если пара найдена
                if(isset($check_bestchange_rate) and !empty($check_bestchange_rate)) {
                    flash(__('Пара :name для BestChange парсера уже существует', ['name' => $check_bestchange_rate->name]), ['alert alert-danger']);
                    return redirect()->back();
                }

                $bs_currency = $this->bestchange->getCachedCurrencies()->all();
                // Получаем итоговое название курса
                $bestchange_rates_name = sprintf('%s -> %s',
                    $bs_currency[$request->get('bestchange_rates_exchanger_id1')],
                    $bs_currency[$request->get('bestchange_rates_exchanger_id2')]
                );

                if(empty($bestchange_rates_name)) {
                    flash(__('Название пары не найдено'), ['alert alert-danger']);
                    return redirect()->back();
                }

                $bestchange_rates_data = BestChangeRatesModel::create([
                    'name'          => $bestchange_rates_name,
                    'exchanger_id1'  =>  ($request->has('bestchange_rates_exchanger_id1') ? $request->get('bestchange_rates_exchanger_id1') : 0),
                    'exchanger_id2'  =>  ($request->has('bestchange_rates_exchanger_id2') ? $request->get('bestchange_rates_exchanger_id2') : null),
                    'is_automatic_type'  =>  1,
                    'is_automatic_sort_by'  =>  1,
                    'value' =>  1,
                    'summa' =>  0,
                    'status' =>  1,
                    'position' => ($request->has('bestchange_rates_position') ? $request->get('bestchange_rates_position') : 0),
                    'number_format' => ($request->has('bestchange_rates_number_format') ? $request->get('bestchange_rates_number_format') : 0)
                ]);

                $id_bestchange_rates = $bestchange_rates_data->id;
            }

//            try {
//                $call = \Converter::call($item)->in();
//                $update['course_value'] = $call->getAmount();
//                $update['exchange_rate'] = $call->getCourse();
//                $update['exchange_rate_str'] = $call->getCourseStr();
//                $update['is_error_rate'] = 0;
//            }catch (\Exception $exception) {
//                $update['is_error_rate'] = 1;
//                $update['error_rate_text'] = $exception->getMessage();
//            }


            $item->direction_field()->sync($request->ids_custom_fields);
            $item->direction_requisites()->sync($request->ids_requisites);
            $item->direction_forbidden_countries()->sync($request->ids_geo_forbidden_countries);
            $item->direction_allowed_countries()->sync($request->ids_geo_allowed_countries);
            $item->direction_modes()->sync($request->ids_direction_modes);

            if(\Module::find('Cities')->isEnabled() == 1 and !empty($request->ids_cities))
            {
                // Проверка в базе городов
                $check_city = DirectionExchangeCity::where('id_direction_exchange', '=', $item->id)->count();
                foreach ($request->ids_cities as $city_value)
                {
                    $user = DirectionExchangeCity::updateOrCreate(
                        [
                            'id_direction_exchange' =>  $item->id,
                            'city_id' => $city_value,
                        ], [
                            'id_direction_exchange' =>  $item->id,
                            'city_id' => $city_value,
                        ]
                    );
                }
            }

            foreach (config('app.form_lang') as $currency_key => $currency_item) {
                $update['desc_exchange'][$currency_key] = $request->get('desc_exchange'.$currency_item['field']);
                $update['desc_exchange_dop'][$currency_key] = $request->get('desc_exchange_dop'.$currency_item['field']);
                $update['other_docs'][$currency_key] = $request->get('other_docs'.$currency_item['field']);
                $update['order_button_i_pay'][$currency_key] = $request->get('order_button_i_pay'.$currency_item['field']);
                $update['order_button_i_pay_text'][$currency_key] = $request->get('order_button_i_pay_text'.$currency_item['field']);
                $update['formalization_text'][$currency_key] = $request->get('formalization_text'.$currency_item['field']);
                $update['instructions'][$currency_key] = $request->get('instructions'.$currency_item['field']);
                $update['text_order_success'][$currency_key] = $request->get('text_order_success'.$currency_item['field']);
                $update['text_order_failed'][$currency_key] = $request->get('text_order_failed'.$currency_item['field']);
                $update['text_order_handler'][$currency_key] = $request->get('text_order_handler'.$currency_item['field']);
                $update['text_order_confirm'][$currency_key] = $request->get('text_order_confirm'.$currency_item['field']);
                $update['order_button_i_confirm'][$currency_key] = $request->get('order_button_i_confirm'.$currency_item['field']);
                $update['notice_process_desc'][$currency_key] = $request->get('notice_process_desc'.$currency_item['field']);
                $update['multiplicity_comment'][$currency_key] = $request->get('multiplicity_comment'.$currency_item['field']);
            }


            $item->merchants()->sync($request->ids_merchant);
            $item->update(
                array_merge($update, [
                    'id_currency1'              =>  $request->get('id_currency1'),
                    'id_currency2'              =>  $request->get('id_currency2'),
                    'parent_url'                =>  ($request->has('parent_url') ? $request->get('parent_url') : null),
                    'profit'                    =>  ($request->has('profit') ? $request->get('profit') : 0),
                    'profit_s'                  =>  ($request->has('profit_s') ? $request->get('profit_s') : 0),
                    'profit_partner'            =>  ($request->has('profit_partner') ? $request->get('profit_partner') : 0),
                    'commission2'               =>  ($request->has('commission2') ? $request->get('commission2') : 0),
                    'commission_s2'             =>  ($request->has('commission_s2') ? $request->get('commission_s2') : 0),
                    'min_price1'                =>  ($request->has('min_price1') ? $request->get('min_price1') : 0),
                    'min_price2'                =>  ($request->has('min_price2') ? $request->get('min_price2') : 0),
                    'max_price1'                =>  ($request->has('max_price1') ? $request->get('max_price1') : 0),
                    'max_price2'                =>  ($request->has('max_price2') ? $request->get('max_price2') : 0),
                    'is_manual_min_price1'      =>  ($request->has('is_manual_min_price1') ? $request->get('is_manual_min_price1') : 0),
                    'is_manual_min_price2'      =>  ($request->has('is_manual_min_price2') ? $request->get('is_manual_min_price2') : 0),
                    'is_manual_max_price1'      =>  ($request->has('is_manual_max_price1') ? $request->get('is_manual_max_price1') : 0),
                    'is_manual_max_price2'      =>  ($request->has('is_manual_max_price2') ? $request->get('is_manual_max_price2') : 0),
                    'id_crypto_parser'          =>  ($request->has('id_crypto_parser') ? $request->get('id_crypto_parser') : 0),
                    'id_your_exchange'          =>  ($request->has('id_your_exchange') ? $request->get('id_your_exchange') : 0),
                    'your_add_course1'          =>  ($request->has('your_add_course1') ? $request->get('your_add_course1') : 0),
                    'your_add_course1_s'          =>  ($request->has('your_add_course1_s') ? $request->get('your_add_course1_s') : 0),
                    'add_course1'               =>  ($request->has('add_course1') ? $request->get('add_course1') : 0),
                    'add_course1_s'               =>  ($request->has('add_course1_s') ? $request->get('add_course1_s') : 0),
                    'status'                    =>  ($request->has('status') ? $request->get('status') : 0),
                    'seo_title'                 =>  ($request->has('seo_title') ? $request->get('seo_title') : null),
                    'seo_description'           =>  ($request->has('seo_description') ? $request->get('seo_description') : null),
                    'seo_keywords'              =>  ($request->has('seo_keywords') ? $request->get('seo_keywords') : null),
                    'deadline'                  =>  ($request->has('deadline') ? $request->get('deadline') : null),
                    'enable_bestchange'         =>  ($request->has('enable_bestchange') ? $request->get('enable_bestchange') : 0),
                    'x19_mode'                  =>  ($request->has('x19_mode') ? $request->get('x19_mode') : 0),
                    'id_bestchange_rates'       =>  $id_bestchange_rates,
                    'bestchange_position'       =>  ($request->has('bestchange_position') ? $request->get('bestchange_position') : 0),
                    'bestchange_bl'             =>  ($request->has('bestchange_bl') ? implode(',', $request->get('bestchange_bl')) : null),
                    'bestchange_wl'             =>  ($request->has('bestchange_wl') ? implode(',', $request->get('bestchange_wl')) : null),
                    'bestchange_min_reserve'    =>  ($request->has('bestchange_min_reserve') ? $request->get('bestchange_min_reserve') : 0),
                    'bestchange_max_reserve'    =>  ($request->has('bestchange_max_reserve') ? $request->get('bestchange_max_reserve') : 0),
                    'bc_enable_your_course'     =>   ($request->has('bc_enable_your_course') ? $request->get('bc_enable_your_course') : 0),
                    'bc_id_your_exchange'       =>  ($request->has('bc_id_your_exchange') ? $request->get('bc_id_your_exchange') : 0),
                    'bc_your_add_course'        =>  ($request->has('bc_your_add_course') ? $request->get('bc_your_add_course') : 0),
                    'bc_min_sum'                =>  ($request->has('bc_min_sum') ? $request->get('bc_min_sum') : 0),
                    'bc_max_sum'                =>  ($request->has('bc_max_sum') ? $request->get('bc_max_sum') : 0),
                    'bc_new_commission'         =>  ($request->has('bc_new_commission') ? $request->get('bc_new_commission') : 0),
                    'bc_id_new_rate'            =>  ($request->has('bc_id_new_rate') ? $request->get('bc_id_new_rate') : 0),
                    'bc_add_course'             =>  ($request->has('bc_add_course') ? $request->get('bc_add_course') : 0),
                    'id_competitor'             =>  ($request->has('id_competitor') ? $request->get('id_competitor') : 0),
                    'id_bs_alt_parser'             =>  ($request->has('id_bs_alt_parser') ? $request->get('id_bs_alt_parser') : 0),
                    'bs_alt_parser_course'      =>  ($request->has('bs_alt_parser_course') ? $request->get('bs_alt_parser_course') : 0),
                    'id_file_parser_rate'       =>  ($request->has('id_file_parser_rate') ? $request->get('id_file_parser_rate') : 0),
                    'id_group_commission'       =>  ($request->has('id_group_commission') ? $request->get('id_group_commission') : 0),
                    'is_not_bonus'              =>  $request->has('is_not_bonus') ? 0 : 1,
                    'is_not_partner'            =>  $request->is_not_partner,
                    'is_disable_auto_reg'       =>  $request->has('is_disable_auto_reg') ? $request->get('is_disable_auto_reg') : 0,
                    'individual_percentage'     =>  ($request->has('individual_percentage') ? $request->get('individual_percentage') : 0),
                    'is_restrict_editing'       =>  ($request->has('is_restrict_editing') ? $request->get('is_restrict_editing') : 0),
                    'fixed_payout'              =>  ($request->has('fixed_payout') ? $request->get('fixed_payout') : 0),
                    'allow_export'              =>  ($request->has('allow_export') ? $request->get('allow_export') : 0),
                    'allow_export_from'         =>  ($request->has('allow_export_from') ? $request->get('allow_export_from') : null),
                    'allow_export_to'           =>  ($request->has('allow_export_to') ? $request->get('allow_export_to') : null),
                    'export_label_param'        =>  ($request->has('export_label_param') ? implode(',', $request->get('export_label_param')) : null),
                    'hidden_export_label_param' =>  ($request->has('hidden_export_label_param') ? $request->get('hidden_export_label_param') : 0),
                    'label_floating'         =>  ($request->has('label_floating') ? $request->get('label_floating') : null),
                    'label_delay'         =>  ($request->has('label_delay') ? $request->get('label_delay') : null),
                    'export_label_minamount'    =>  ($request->has('export_label_minamount') ? $request->get('export_label_minamount') : 0),
                    'export_label_maxamount'    =>  ($request->has('export_label_maxamount') ? $request->get('export_label_maxamount') : 0),
                    'minimum_payout'            =>  ($request->has('minimum_payout') ? $request->get('minimum_payout') : 0),
                    'maximum_payout'            =>  ($request->has('maximum_payout') ? $request->get('maximum_payout') : 0),
                    'id_group_direction'        =>  ($request->has('id_group_direction') ? $request->get('id_group_direction') : 0),
                    'is_enabled_exchange'       =>  ($request->has('is_enabled_exchange') ? $request->get('is_enabled_exchange') : 0),
                    'from_on_time'              =>  ($request->has('from_on_time') ? $request->get('from_on_time') : null),
                    'to_on_time'                =>  ($request->has('to_on_time') ? $request->get('to_on_time') : null),
                    'is_verified_account'       =>  ($request->has('is_verified_account') ? $request->get('is_verified_account') : 0),
                    'max_order_one_ip'          =>  ($request->has('max_order_one_ip') ? $request->get('max_order_one_ip') : 0),
                    'max_order_one_user'        =>  ($request->has('max_order_one_user') ? $request->get('max_order_one_user') : 0),
                    'max_order_one_email'       =>  ($request->has('max_order_one_email') ? $request->get('max_order_one_email') : 0),
                    'max_order_one_account1'    =>  ($request->has('max_order_one_account1') ? $request->get('max_order_one_account1') : 0),
                    'max_order_one_account2'    =>  ($request->has('max_order_one_account2') ? $request->get('max_order_one_account2') : 0),

                    'is_notify_exchange_amount' =>  $request->has('is_notify_exchange_amount') ? $request->get('is_notify_exchange_amount') : 0,

                    'max_order_one_ip_day'          =>  ($request->has('max_order_one_ip_day') ? $request->get('max_order_one_ip_day') : 0),
                    'max_order_one_user_day'        =>  ($request->has('max_order_one_user_day') ? $request->get('max_order_one_user_day') : 0),
                    'max_order_one_email_day'       =>  ($request->has('max_order_one_email_day') ? $request->get('max_order_one_email_day') : 0),
                    'max_order_one_account1_day'    =>  ($request->has('max_order_one_account1_day') ? $request->get('max_order_one_account1_day') : 0),
                    'max_order_one_account2_day'    =>  ($request->has('max_order_one_account2_day') ? $request->get('max_order_one_account2_day') : 0),

                    'max_amount_newbie'                    =>  ($request->has('max_amount_newbie') ? $request->get('max_amount_newbie') : 0),
                    'not_ip'                    =>  ($request->has('not_ip') ? $request->get('not_ip') : null),
                    'cr_min_sum'                =>  ($request->has('cr_min_sum') ? $request->get('cr_min_sum') : 0),
                    'cr_max_sum'                =>  ($request->has('cr_max_sum') ? $request->get('cr_max_sum') : 0),
                    'cr_add_course'             =>  ($request->has('cr_add_course') ? $request->get('cr_add_course') : 0),
                    'cr_id_new_rate'            =>  ($request->has('cr_id_new_rate') ? $request->get('cr_id_new_rate') : 0),
                    'max_percent_partner'       =>  ($request->has('max_percent_partner') ? $request->get('max_percent_partner') : 0),
                    'xml_juridical'             =>  ($request->has('xml_juridical') ? $request->get('xml_juridical') : 0),
                    'languages'                 =>  ($request->has('languages') ? implode(',', $request->get('languages')) : null),
                    'is_hidden_tariffs'         =>  ($request->has('is_hidden_tariffs') ? $request->get('is_hidden_tariffs') : 0),
                    'is_holding_direction'      =>  ($request->has('is_holding_direction') ? $request->get('is_holding_direction') : 0),
                    'is_hidden_not_locale'     =>   ($request->has('is_hidden_not_locale') ? $request->get('is_hidden_not_locale') : 0),
                    'is_hidden_not_device'      =>  ($request->has('is_hidden_not_device') ? $request->get('is_hidden_not_device') : 0),
                    'device'                    =>  ($request->has('device') ? implode(',', $request->get('device')) : null),
                    'is_allow_telegram_bot'     =>  ($request->has('is_allow_telegram_bot') ? $request->get('is_allow_telegram_bot') : 0),
                    'rl_min2_course'            =>  ($request->has('rl_min2_course') ? $request->get('rl_min2_course') : 0),
                    'rl_max2_course'            =>  ($request->has('rl_max2_course') ? $request->get('rl_max2_course') : 0),
                    'rl_id_parser_exchange'     =>  ($request->has('rl_id_parser_exchange') ? $request->get('rl_id_parser_exchange') : 0),
                    'rl_add_course'             =>  ($request->has('rl_add_course') ? $request->get('rl_add_course') : 0),
                    'manual_rate_value'     =>  ($request->has('manual_rate_value') ? $request->get('manual_rate_value') : 0),

                    'rl_id_your_exchange'       =>  ($request->has('rl_id_your_exchange') ? $request->get('rl_id_your_exchange') : 0),
                    'rl_your_add_course'        =>  ($request->has('rl_your_add_course') ? $request->get('rl_your_add_course') : 0),

                    'reserve_max_limit'     =>  ($request->has('reserve_max_limit') ? $request->get('reserve_max_limit') : 0),
                    'reserve_limit_day'     =>  ($request->has('reserve_limit_day') ? $request->get('reserve_limit_day') : 0),
                    'reserve_limit_month'   =>  ($request->has('reserve_limit_month') ? $request->get('reserve_limit_month') : 0),

                    'is_num_transaction'     =>  ($request->has('is_num_transaction') ? $request->get('is_num_transaction') : 0),
                    'num_transaction_label'     =>  ($request->has('num_transaction_label') ? $request->get('num_transaction_label') : null),

                    'is_note_tx'     =>  ($request->has('is_note_tx') ? $request->get('is_note_tx') : 0),
                    'note_tx_label'     =>  ($request->has('note_tx_label') ? $request->get('note_tx_label') : null),

                    'is_change_bestchange_range'=>  ($request->has('is_change_bestchange_range') ? $request->get('is_change_bestchange_range') : 0),
                    'bestchange_range'          =>  ($request->has('bestchange_range') ? $request->get('bestchange_range') : null),
                    'bestchange_range_from'     =>  ($request->has('bestchange_range_from') ? $request->get('bestchange_range_from') : null),
                    'bestchange_range_to'     =>  ($request->has('bestchange_range_to') ? $request->get('bestchange_range_to') : null),
                    'bestchange_step'           =>  ($request->has('bestchange_step') ? $request->get('bestchange_step') : 0),
                    'bestchange_city'           =>  ($request->has('bestchange_city') ? $request->get('bestchange_city') : 0),
                    'oth_comm_percent'          =>  ($request->has('oth_comm_percent') ? $request->get('oth_comm_percent') : null),
                    'oth_comm_currency'         =>  ($request->has('oth_comm_currency') ? $request->get('oth_comm_currency') : null),
                    'oth_comm2_percent'          =>  ($request->has('oth_comm2_percent') ? $request->get('oth_comm2_percent') : null),
                    'oth_comm2_currency'         =>  ($request->has('oth_comm2_currency') ? $request->get('oth_comm2_currency') : null),
                    'oth_min_comm'              =>  ($request->has('oth_min_comm') ? $request->get('oth_min_comm') : null),
                    'oth_min2_comm'              =>  ($request->has('oth_min2_comm') ? $request->get('oth_min2_comm') : null),

                    'auto_del_order_status'     =>  ($request->has('auto_del_order_status') ? implode(',', $request->get('auto_del_order_status')) : null),
                    'auto_del_order_day'        =>  ($request->has('auto_del_order_day') ? $request->get('auto_del_order_day') : 0),
                    'auto_del_order_hour'        =>  ($request->has('auto_del_order_hour') ? $request->get('auto_del_order_hour') : 0),
                    'auto_del_order_minute'        =>  ($request->has('auto_del_order_minute') ? $request->get('auto_del_order_minute') : 0),
                    'tech_name'                 =>  $request->get('tech_name') ?? currency_in($item, false).' -> '.currency_out($item, false),
                    'id_partner_parser_rate'    => $request->has('id_partner_parser_rate') ? $request->get('id_partner_parser_rate') : 0,
                    'id_parser_formula_rate'    => $request->has('id_parser_formula_rate') ? $request->get('id_parser_formula_rate') : 0,
                    'type_output_requisites'    => $request->has('type_output_requisites') ? $request->get('type_output_requisites') : 0,
                    'min_count_exchanges_client'    => $request->has('min_count_exchanges_client') ? $request->get('min_count_exchanges_client') : 0,

                    'pay_comm_percent'          =>  ($request->has('pay_comm_percent') ? $request->get('pay_comm_percent') : null),
                    'pay_comm_currency'         =>  ($request->has('pay_comm_currency') ? $request->get('pay_comm_currency') : null),
                    'pay_comm2_percent'          =>  ($request->has('pay_comm2_percent') ? $request->get('pay_comm2_percent') : null),
                    'pay_comm2_currency'         =>  ($request->has('pay_comm2_currency') ? $request->get('pay_comm2_currency') : null),
                    'pay_min_comm'              =>  ($request->has('pay_min_comm') ? $request->get('pay_min_comm') : null),
                    'pay_min2_comm'              =>  ($request->has('pay_min2_comm') ? $request->get('pay_min2_comm') : null),
                    'is_enable_user_discount'   =>  $request->has('is_enable_user_discount') ? $request->get('is_enable_user_discount') : 0,
                    'type_profit_field'         =>  $request->has('type_profit_field') ? $request->get('type_profit_field') : 0,
                    'is_email_instructions'     =>  $request->has('is_email_instructions') ? $request->get('is_email_instructions') : 0,
                    'is_email_desc_exchange_dop'     =>  $request->has('is_email_desc_exchange_dop') ? $request->get('is_email_desc_exchange_dop') : 0,
                    'is_email_other_docs'     =>  $request->has('is_email_other_docs') ? $request->get('is_email_other_docs') : 0,
                    'multiplicity_type'         =>  $request->has('multiplicity_type') ? $request->get('multiplicity_type') : 0,
                    'multiplicity_amount'         =>  $request->has('multiplicity_amount') ? $request->get('multiplicity_amount') : 0
                ])
            );



            $name = direction_name($item);

            // Записываем в лог
            add_to_currencies_log($item->id_currency1, __('Направление :name обновлена', ['name' => $name]), 1,4);
            add_to_currencies_log($item->id_currency2, __('Направление :name обновлена', ['name' => $name]), 1,4);

            flash(__('Направление :name успешно обновлена', ['name' => $name]), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Полное удаление направления
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $direction = DirectionExchange::find($id);
        // Делаем доп. проверку в списке заявок
        $order = Task::where('id_direction_exchange', '=', $id);
        if($order->count() > 0) {
            flash(__('Выбранное направление удалить невозможно, У направлений найдены заявки'), ['alert alert-danger']);
            return redirect()->back();
        }

        $name = direction_name($direction);
        // Записываем в лог
        add_to_currencies_log($direction->id_currency1, __('Направление :name полностью удалена', ['name' => $name]), 3,4);
        add_to_currencies_log($direction->id_currency2, __('Направление :name полностью удалена', ['name' => $name]), 3,4);

        flash(__('Направление :name успешно удалена', ['name' => $name]), ['alert alert-success']);

        // Удаляем связанные уведомления
        DirectionNotification::where('id_direction_exchange', '=', $id)->delete();
        DirectionExchangeErrorLog::where('id_direction_exchange', '=', $id)->delete();

        DirectionDay::where('id_direction_exchange', '=', $id)->delete();
        $direction->delete();

        return redirect()->to(
            admin_base_path('/basic/direction_exchange')
        );
    }

    public function destroyGlobal(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $direction = DirectionExchange::find($id);
        // Делаем доп. проверку в списке заявок
        $order = Task::where('id_direction_exchange', '=', $id);
        if($order->count() > 0) {
            flash(__('Выбранное направление удалить невозможно, У направлений найдены заявки'), ['alert alert-danger']);
            return redirect()->back();
        }

        $name = direction_name($direction);
        // Записываем в лог
        add_to_currencies_log($direction->id_currency1, __('Направление :name полностью удалена', ['name' => $name]), 3,4);
        add_to_currencies_log($direction->id_currency2, __('Направление :name полностью удалена', ['name' => $name]), 3,4);

        flash(__('Направление :name успешно удалена', ['name' => $name]), ['alert alert-success']);

        // Удаляем связанные уведомления
        DirectionNotification::where('id_direction_exchange', '=', $id)->delete();
        DirectionExchangeErrorLog::where('id_direction_exchange', '=', $id)->delete();

        DirectionDay::where('id_direction_exchange', '=', $id)->delete();
        $direction->delete();
    }

    /**
     * Деактивируем направление
     * @param int $id
     * @param bool $redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function disabledExchange(int $id, bool $redirect = true): \Illuminate\Http\RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }
        $direction = DirectionExchange::find($id);
        $direction->update([
            'status' => 2
        ]);

        $name = direction_name($direction);
        // Записываем в лог
        add_to_currencies_log($direction->id_currency1, __('Направление :name удалена', ['name' => $name]), 3,4);
        add_to_currencies_log($direction->id_currency2, __('Направление :name удалена', ['name' => $name]), 3,4);

        flash(__('Направление :name перемещено в корзину', ['name' => $name]), ['alert alert-success']);
        if($redirect) {
            return redirect()->back();
        }
    }

    /**
     * Автоматическое удаление неоплаченных заявок
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function unpaid_item(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
    {
        $unpaid = UnpaidItem::find(1);
        $statuses = TaskStatus::where('allow_delete', '=',' 1')->get()->pluck('name', 'id');

        // Обработка и выполнения действий
        if($request->getMethod() == 'POST')
        {
            $unpaid->update([
                'auto_delete' => ($request->has('auto_delete') ? $request->get('auto_delete') : 0),
                'order_status' => ($request->has('order_status') ? implode(',', $request->get('order_status')) : 0),
                'removal_time' => ($request->has('removal_time') ? $request->get('removal_time') : 0),
                'removal_minute' => ($request->has('removal_minute') ? $request->get('removal_minute') : 0),
                'rules_cron' => ($request->has('auto_delete') ? $request->get('rules_cron') : 0),
            ]);

            flash(__('Данные успешно обновлены'), ['alert alert-success']);
            return redirect()->to(admin_base_path('/basic/direction_exchange/unpaid_item'));
        }

        return view('admin.basic.direction_exchange.unpaid_item', [
            'item' => $unpaid,
            'statuses' => $statuses
        ]);
    }

    /**
     * Сортировка направлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting_admin(): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $all = DirectionExchange::active()->orderBy('sorting_admin')->orderBy('id', 'desc')->get();

        return view('admin.basic.direction_exchange.sorting_admin', [
            'direction_exchange'  =>  $all
        ]);
    }

    /**
     * Сортировка направлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function sorting(): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $all = Currency::active()->where('status', 0)->orderBy('sorting_1')->get();

        return view('admin.basic.direction_exchange.sorting', [
            'currency'  =>  $all
        ]);
    }

    /**
     * Сортировка по ID Валюты
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sortingId(int $id): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $currency = Currency::findOrFail($id);
        $direction_exchange = DirectionExchange::active()->where('id_currency1', $id)->orderBy('sorting_2')->cursor();

        return view('admin.basic.direction_exchange.sorting_id', [
            'items'  =>  $direction_exchange,
            'currency' => $currency
        ]);
    }

    /**
     * Сортировка валют для тарифов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting_tariffs(): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $all = Currency::active()->orderBy('sorting_tariffs')->get();

        return view('admin.basic.direction_exchange.sorting_tariffs', [
            'currency'  =>  $all
        ]);
    }

    /**
     * Сортировка направлений для тарифов (По валютам)
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting_tariff_id(int $id): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $currency = Currency::findOrFail($id);
        $all = DirectionExchange::active()->where('id_currency1' ,'=', $id)->orderBy('sorting_tariffs')->get();

        return view('admin.basic.direction_exchange.sorting_tariffs_id', [
            'items'  =>  $all,
            'currency' => $currency,
        ]);
    }

    /**
     * Групповая корректировка минимальных и макс. цен
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function min_price(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Routing\Redirector|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
    {
        $exchange1 = DirectionExchange::with('currency1')->active()->whereHas('currency1', function($query) {
            return $query->where('status', '!=', 2);
        })->where([
            ['is_manual_min_price1', '=', 0], ['is_manual_max_price1', '=', 0],
            ['is_manual_min_price2', '=', 0], ['is_manual_max_price2', '=', 0]
        ])->groupBy('id_currency1')->get();

        $exchange2 = DirectionExchange::with('currency2')->active()->whereHas('currency2', function($query) {
            return $query->where('status', '!=', 2);
        })->where([
            ['is_manual_min_price1', '=', 0], ['is_manual_max_price1', '=', 0],
            ['is_manual_min_price2', '=', 0], ['is_manual_max_price2', '=', 0]
        ])->groupBy('id_currency2')->get();

        //Автоматического обновление минимальных цен
        if($request->has('generate_min') and $request->get('generate_min') == true)
        {
            foreach (DirectionExchange::active()->where('is_manual_min_price1', '=', 0)->get() as $item)
            {
                $generate = direction_filter_amount($item, $item->min_price2 / \Converter::call($item)->out()->getAmount());
                DirectionExchange::active()->where('id', $item->id)->update([
                    'min_price1' =>  $generate
                ]);
            }

            flash(__('Данные успешно обновлен'), ['alert alert-success']);
            return redirect()->back();
        }

        //Автоматического обновление макс. цен
        if($request->has('generate_max') and $request->get('generate_max') == true)
        {
            foreach (DirectionExchange::active()->where('is_manual_max_price1', '=', 0)->get() as $item)
            {
                $generate = direction_filter_amount($item, $item->max_price2 / \Converter::call($item)->out()->getAmount());
                DirectionExchange::active()->where('id', $item->id)->update([
                    'max_price1' =>  $generate
                ]);
            }
            flash(__('Данные успешно обновлен'), ['alert alert-success']);
            return redirect()->back();
        }

        // Обработка полученных данных
        if($request->getMethod() == 'POST')
        {
            if($request->form == 'min_price1' and $request->actions == 'save')
            {
                foreach ($request->get('min_price') as $key => $value)
                {
                    DirectionExchange::active()->where('is_manual_min_price1', '=', 0)->where('id_currency1', $key)->update([
                        'min_price1' =>  $value
                    ]);
                }

                foreach ($request->get('max_price') as $key => $value)
                {
                    DirectionExchange::active()->where('is_manual_max_price1', '=', 0)->where('id_currency1', $key)->update([
                        'max_price1' =>  $value
                    ]);
                }
            }elseif($request->form == 'min_price2' and $request->actions == 'save')
            {
                foreach ($request->get('min_price') as $key => $value)
                {
                    DirectionExchange::active()->where('id_currency2', $key)->where('is_manual_min_price2', '=', 0)->update([
                        'min_price2' =>  $value
                    ]);
                }

                foreach ($request->max_price as $key => $value)
                {
                    DirectionExchange::active()->where('id_currency2', $key)->where('is_manual_max_price2', '=', 0)->update([
                        'max_price2' =>  $value
                    ]);
                }
            }

            flash(__('Данные успешно обновлен'), ['alert alert-success']);
            return redirect()->back();
        }

        return view('admin.basic.direction_exchange.min_price', [
            'exchange1' =>  $exchange1,
            'exchange2'    =>  $exchange2
        ]);
    }

    /**
     * Общая корректировка всех направлений
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function price_adjustment(Request $request): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $directions = DirectionExchange::select('id', 'status', 'tech_name', 'min_price1', 'min_price2', 'max_price1', 'max_price2', 'is_manual_min_price1', 'is_manual_min_price2', 'is_manual_max_price1', 'is_manual_max_price2')
            ->where('status', '=', 1)->paginate((int)iEXSetting('price_adjustment_pagination', 200));

        // Сохранение данные
        if($request->getMethod() == 'POST')
        {

            if($request->has('action') and $request->action == 'settings')
            {
                // Обновление конфига
                $array = [];
                foreach ($this->allowFilteredPrice as $item) {
                    if($request->has($item) and is_array($request->get($item))) {
                        $array[$item] = implode(',', $request->get($item));
                    } else {
                        $array[$item] = ($request->has($item) ? $request->get($item): null);
                    }
                }
                iEXSetting($array);

                flash(__('Настройки успешно сохранены'), ['alert alert-success']);
                return redirect()->back();
            }

            // Запрещаем некоторым пользователям редактировать направление
            if($request->user()->hasAnyPermission('admin_limit_editing_directions'))
            {
                flash(__('Ошибка при редактировании направления'), ['alert alert-danger']);
                return redirect()->back();
            }

            foreach ($request->get('item_id') as $value)
            {
                DirectionExchange::findOrFail($value)->update([
                    'min_price1' => $request->min_price1[$value] ?? 0,
                    'max_price1' => $request->max_price1[$value] ?? 0,
                    'min_price2' => $request->min_price2[$value] ?? 0,
                    'max_price2' => $request->max_price2[$value] ?? 0,

                    'is_manual_min_price1' => ($request->is_manual_min_price1[$value] ?? 0),
                    'is_manual_min_price2' => ($request->is_manual_min_price2[$value] ?? 0),
                    'is_manual_max_price1' => ($request->is_manual_max_price1[$value] ?? 0),
                    'is_manual_max_price2' => ($request->is_manual_max_price2[$value] ?? 0)
                ]);
            }

            flash(__('Данные успешно обновлены'), ['alert alert-success']);
            return redirect()->back();
        }

        return view('admin.basic.direction_exchange.price_adjustment', [
            'directions'    =>  $directions
        ]);
    }

    /**
     * Создаем дубликат направления
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate(int $id): \Illuminate\Http\RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $direction = DirectionExchange::find($id);
        $newExchange = $direction->replicate();
        $newExchange->save();

        return redirect()->back();
    }

    /**
     * Лог направлений обменов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function logs(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
    {
        if($request->has('clear_logs')) {
            DirectionExchangeErrorLog::query()->delete();

            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return  redirect()->to(admin_base_path('/basic/direction_exchange/logs'));
        }


        $logs = DirectionExchangeErrorLog::orderByDesc('id')->paginate(20);
        return view('admin.basic.direction_exchange.logs', [
            'logs'    =>  $logs
        ]);
    }

    /**
     * Удаленные направления
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function trashed(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
    {

        if(mb_strtoupper($request->getMethod()) == 'POST')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            foreach ($request->get('check') as $key => $value)
            {
                $direction = DirectionExchange::find($value);
                $direction->update([
                    'status' => 0
                ]);

                $name = direction_name($direction);
                // Записываем в лог
                add_to_currencies_log($direction->id_currency1, __('Направление :name восстановлена', ['name' => $name]), 1,4);
                add_to_currencies_log($direction->id_currency2, __('Направление :name восстановлена', ['name' => $name]), 1,4);
            }


            flash(__('Направления восстановлены'), ['alert alert-success']);
            return redirect()->back();
        }

        $exchanges = DirectionExchange::where('status', '=', 2)->orderByDesc('id')->paginate(20);
        return view('admin.basic.direction_exchange.trashed', [
            'exchanges' => $exchanges
        ]);
    }

    /**
     * Недостающие направления
     *
     * @return array
     */
    public function uncreatedDirections(): array
    {
        if((int)iEXSetting('is_enabled_uncreated_directions') == 0) {
            return [];
        }

        $currencies = Currency::where('status', 0)->get();

        $currency_1 = [];
        foreach ($currencies as $currency)
        {
            $currencies2 = Currency::where('status', 0)->where('id', '!=', $currency->id)->get();
            $currency_1_2 = [];
            foreach ($currencies2 as $item2) {
                $currency_1_2[$item2->id] = $item2->payment->name.' '.$item2->code_currency->name;
            }

            $currency_1[$currency->id] = [
                'name'  =>  $currency->payment->name.' '.$currency->code_currency->name,
                'relationship' => $currency_1_2
            ];
        }

        $direction_exists = [];

        foreach ($currency_1 as $key => $value)
        {
            foreach ($value['relationship'] as $key2 => $value2)
            {
                $exists = DirectionExchange::where([
                    ['id_currency1', $key],
                    ['id_currency2', $key2]
                ])->exists();

                if($exists) {
                    continue;
                }

                $direction_exists[] = [
                    'currency1' => [
                        'id' => $key,
                        'name' => $value['name']
                    ],
                    'currency2' => [
                        'id' => $key2,
                        'name' => $value2
                    ],
                ];
            }
        }

        return $direction_exists;
    }
}
