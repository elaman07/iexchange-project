<?php

namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use App\Models\DirectionRequisite;
use App\Models\RequisitesBlacklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectionRequisitesController extends Controller
{
    /**
     * Список уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $items = DirectionRequisite::orderBy('id')->get();

        return view('admin.basic.direction_exchange.requisites.index', [
            'items'        =>  $items
        ]);
    }

    /**
     * Форма добавления уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $directions_exchanges = DirectionExchange::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => direction_name($item)
            ];
        })->pluck('name', 'id');
        return view('admin.basic.direction_exchange.requisites.create', compact('directions_exchanges'));
    }

    /**
     * Обработка и добавление уведомлений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'account_number' => 'required',
            'limit_day'  => 'required|numeric',
            'limit_month' => 'required|numeric',
            'limit_views'   =>  'required|integer'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            // Если все удачно, добавляем
            $account_numbers = preg_split('/\r\n|\r|\n/', $request->get('account_number'));
            foreach ($account_numbers as $account_number)
            {
                // Проверяем реквизиты в черном списке
                $result = RequisitesBlacklist::all()->filter(function($item) use($account_number) {
                    $score = preg_replace('/\s+/', '', $account_number);
                    $db_score = preg_replace('/\s+/', '', $item->score);
                    return $db_score == $score;
                });

                if(count($result) > 0) {
                    flash('Счет '.$account_number.' находится в черном списке', ['alert alert-danger']);
                    continue;
                }

                $options = [
                    'account_number'        =>  ($account_number ?? null),
                    'name'                  =>  ($request->has('name') ? $request->get('name') : null),
                    'limit_day'             =>  ($request->has('limit_day') ? $request->get('limit_day') : 0),
                    'limit_month'           =>  ($request->has('limit_month') ? $request->get('limit_month') : 0),
                    'status'                =>  ($request->has('status') ? $request->get('status') : 0),
                    'limit_views'           =>  ($request->has('limit_views') ? $request->get('limit_views'): 0)
                ];

                $requisite = DirectionRequisite::create($options);
                $requisite->direction_exchange()->sync($request->directions_exchanges ?? []);

                flash('Реквизит для '.$request->get('name').' успешно добавлен', ['alert alert-success']);
            }
        }

        return redirect()->back();
    }

    /**
     * Форма редактирования уведомлений
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = DirectionRequisite::findOrFail($id);
        $directions_exchanges = DirectionExchange::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => direction_name($item)
            ];
        })->pluck('name', 'id');
        return view('admin.basic.direction_exchange.requisites.edit',compact('item', 'directions_exchanges'));
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $requisite = DirectionRequisite::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'account_number' => 'required',
            'limit_day'  => 'required|numeric',
            'limit_month' => 'required|numeric',
            'limit_views'   =>  'required|integer'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            // Если все удачно, добавляем
            $account_numbers = preg_split('/\r\n|\r|\n/', $request->get('account_number'));
            foreach ($account_numbers as $account_number)
            {
                // Проверяем реквизиты в черном списке
                $result = RequisitesBlacklist::all()->filter(function($item) use($account_number) {
                    $score = preg_replace('/\s+/', '', $account_number);
                    $db_score = preg_replace('/\s+/', '', $item->score);
                    return $db_score == $score;
                });

                if(count($result) > 0) {
                    flash('Счет '.$account_number.' находится в черном списке', ['alert alert-danger']);
                    continue;
                }

                $options = [
                    'account_number'        =>  ($account_number ?? null),
                    'name'                  =>  ($request->has('name') ? $request->get('name') : null),
                    'limit_day'             =>  ($request->has('limit_day') ? $request->get('limit_day') : 0),
                    'limit_month'           =>  ($request->has('limit_month') ? $request->get('limit_month') : 0),
                    'status'                =>  ($request->has('status') ? $request->get('status') : 0),
                    'limit_views'           =>  ($request->has('limit_views') ? $request->get('limit_views'): 0)
                ];

                $requisite->update($options);
                $requisite->direction_exchange()->sync($request->directions_exchanges ?? []);
            }

        }

        flash('Данные успешно обновлены', ['alert alert-success']);
        return redirect()->route('direction-requisites.index')
            ->with('flash_message',
                'Page'. $requisite->name.' updated!');
    }

    /**
     * Удаление уведомлений
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $field = DirectionRequisite::findOrFail($id);

        if($field->direction_exchange->count() > 0) {
            flash('Выбранный реквизит удалить невозможно, К реквизиту привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        $field->delete();
        flash('Поле успешно удалено', ['alert alert-success']);
        return redirect(admin_base_path('/basic/direction-requisites'));
    }
}
