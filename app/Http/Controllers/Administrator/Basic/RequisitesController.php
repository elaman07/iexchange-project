<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\RequisiteInfoField;
use App\Models\Requisites;
use App\Models\RequisitesBlacklist;
use App\Models\RequisitesGroup;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class RequisitesController extends Controller
{
    protected array $allowLocaleOptions = [
        'description_request_payment',
    ];


    /**
     * Ключи для настроек
     *
     * @var array
     */
    protected $attributes = [
        'is_iexrequisites_enabled',
        'admin_requisites_pagination'
    ];

    /**
     * Список реквизитов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Удаление счета
        if($request->has('delete'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            return $this->destroy($request->get('id'));
        }

        // Группа реквизитов
        $groups = RequisitesGroup::with([
            'requisites',
            'requisites.currency',
            'requisites.currency.payment',
            'requisites.currency.code_currency'
        ])->where('status', '=', 1)
            ->orderBy('sorting', 'asc')->get();

        $request_all = $request->except('page');

        // Не отфильтрованные реквизиты
        $baseGroups = Requisites::with('currency', 'currency.payment', 'currency.code_currency')->where('id_group', '=', 0)->get();
        if(!empty($request_all)) {
            $baseGroups = Requisites::isNotHistory()->filter($request->all())->get();
        }

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('requisites.index', $conditions);
        }

        // Резервы
        $currencies = \cache()->remember('admin-requisites-page__currencies', \Illuminate\Support\Carbon::now()->addHours(2), function()
        {
            return Requisites::with(['currency', 'currency.payment', 'currency.code_currency'])->groupBy('id_currency')->get()->map(function ($item) {
                    return [
                        'value' => sprintf('%s %s', !isset($item->currency->payment->name) ? 'undefined' : $item->currency->payment->name, !isset($item->currency->code_currency->name) ? 'undefined' : $item->currency->code_currency->name),
                        'id' => $item->id
                    ];
                })->pluck('value', 'id');
        });


        $statuses = [
            1   => __('Активен'),
            0   =>  __('Не активен')
        ];

        return view('admin.basic.requisites.index', [
            'groups'        =>  $groups,
            'bases_groups'  =>  $baseGroups,
            'statuses'      =>  $statuses,
            'filter'        =>  $request_all,
            'currencies'    =>  $currencies
        ]);
    }

    /**
     * Форма добавления реквизитов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        // Список пользователей с ролями
        $users = User::role(Role::all())->get();
        $is_list = false;
        $currencies = Currency::enabledCurrency()->cursor();
        $groups = RequisitesGroup::all();

        // Доп. поля
        $info_fields = RequisiteInfoField::whereStatus(1)->get()->map(function($item)  {
            $item['key_value'] = $item['key_name'].' ['. $item['value_name'].']';
            return $item;
        })->pluck('key_value', 'id');


        return view('admin.basic.requisites.create', compact('users', 'is_list', 'currencies', 'groups', 'info_fields'));
    }

    /**
     * Обработчик добавления реквизитов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            // Обновление конфига
            $array = [];
            foreach ($this->attributes as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions'))
        {
            // Сохранение данных
            if($request->get('actions') == 'save') {
                foreach ($request->get('requisites_data') as $key => $value)
                {
                    $update = Requisites::findOrFail($key);
                    if(isset($request->account_number[$key]))
                        $update->account_number = $request->account_number[$key];
                    $update->status = (isset($request->status[$key]) ? 1 : 0);
                    $update->save();
                }
            }
            return redirect()->route('requisites.index');
        }


        $validator = Validator::make($request->all(), [
            'id_currency' => 'required|integer|exists:currencies,id',
            'account_number' => 'required',
            'id_group' => 'required|integer',
            'limit_day'  => 'required|numeric',
            'limit_month' => 'required|numeric',
            'limit_views'   =>  'required|integer'
        ]);

        // Перед добавлением резквизитов проверяем
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            // Если все удачно, добавляем
            $account_numbers = preg_split('/\r\n|\r|\n/', $request->get('account_number'));
            foreach ($account_numbers as $account_number)
            {
                // Проверяем реквизиты в черном списке
                $result = RequisitesBlacklist::all()->filter(function($item) use($account_number) {
                    $score = preg_replace('/\s+/', '', $account_number);
                    $db_score = preg_replace('/\s+/', '', $item->score);
                    return $db_score == $score;
                });

                if(count($result) > 0) {
                    flash('Счет '.$account_number.' находится в черном списке', ['alert alert-danger']);
                   continue;
                }

                $item = Requisites::create([
                    'account_number'        =>  ($account_number ?? null),
                    'id_currency'           =>  ($request->has('id_currency') ? $request->get('id_currency') : 0),
                    'limit_day'             =>  ($request->has('limit_day') ? $request->get('limit_day') : 0),
                   // 'limit_week'            =>  ($request->has('limit_week') ? $request->get('limit_week') : 0),
                    'is_unique_shot'        =>  $request->has('is_unique_shot') ? $request->get('is_unique_shot') : 0,
                    'limit_month'           =>  ($request->has('limit_month') ? $request->get('limit_month') : 0),
                    'status'                =>  ($request->has('status') ? $request->get('status') : 0),
                    'id_user'               =>  ($request->has('id_user') ? $request->get('id_user') : 0),
                    'id_group'              =>  ($request->has('id_group') ? $request->get('id_group') : 0),
                    'limit_views'           =>  ($request->has('limit_views') ? $request->get('limit_views'): 0),
                    'type_shot'             =>  ($request->has('type_shot') ? $request->get('type_shot'): 0),
                    'photo_status'          =>  $request->has('photo_status') ? $request->get('photo_status') : 0
                ]);


                if($request->hasFile('photo_name'))
                {
                    $logo = $request->file('photo_name');
                    $filename = sprintf('iex-requisites-%s.%s', Str::random(10), $logo->getClientOriginalExtension());

                    $destinationPath = public_path('/storage');
                    $logo->move($destinationPath, $filename);
                    $item->update(['photo_name' => $filename]);
                }

                $item->requisites_info_fields()->sync($request->info_fields);

                $name = $item->currency->payment->name.' '.$item->currency->code_currency->name;
                // Записываем в лог
                add_to_currencies_log($item->id_currency, "Реквизит {$item->account_number} добавлен", 0,5);

                flash('Реквизит для '.$name.' успешно добавлен', ['alert alert-success']);
            }
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования реквизитов
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $requisite = Requisites::findOrFail($id);
        // Список пользователей с ролями
        $users = User::role(Role::all())->get();
        $currencies = Currency::active()->cursor();
        $groups = RequisitesGroup::all();


        // Доп. поля
        $info_fields = RequisiteInfoField::whereStatus(1)->get()->map(function($item)  {
            $item['key_value'] = $item['key_name'].' ['. $item['value_name'].']';
            return $item;
        })->pluck('key_value', 'id');


        return view('admin.basic.requisites.change', compact('requisite', 'users', 'currencies', 'groups', 'info_fields'));
    }

    /**
     * Обработчик обновления реквизитов
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $item = Requisites::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_currency' => 'required|integer|exists:currencies,id',
            'account_number' => 'required',
            'id_group' => 'required|integer',
            'limit_day'  => 'required|numeric',
            'limit_month' => 'required|numeric',
           // 'limit_week'  => 'required|numeric',
            'limit_views' => 'required|integer',
        ]);

        // Перед добавлением резквизитов проверяем
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            // Проверяем реквизиты в черном списке
            $account_number = $request->has('account_number');
            $result = RequisitesBlacklist::all()->filter(function($item) use($account_number) {
                $score = preg_replace('/\s+/', '', $account_number);
                $db_score = preg_replace('/\s+/', '', $item->score);
                return $db_score == $score;
            });

            if(count($result) > 0) {
                flash('Счет '.$account_number.' находится в черном списке', ['alert alert-danger']);
                redirect()->back();
            }


            if($request->hasFile('photo_name'))
            {
                $logo = $request->file('photo_name');
                $filename = sprintf('iex-requisites-%s.%s', Str::random(10), $logo->getClientOriginalExtension());

                // Удаление предыдущих фото
                iex_file_delete(public_path('storage/'.$item->photo_name));

                $destinationPath = public_path('/storage');
                $logo->move($destinationPath, $filename);
                $item->update(['photo_name' => $filename]);
            }


            $options = [
                'account_number'        =>  ($request->has('account_number') ? $request->get('account_number') : null),
                'id_currency'           =>  ($request->has('id_currency') ? $request->get('id_currency') : 0),
                'limit_day'             =>  ($request->has('limit_day') ? $request->get('limit_day') : 0),
                //'limit_week'            =>  ($request->has('limit_week') ? $request->get('limit_week') : 0),
                'limit_month'           =>  ($request->has('limit_month') ? $request->get('limit_month') : 0),
                'status'                =>  ($request->has('status') ? $request->get('status') : 0),
                'id_user'               =>  ($request->has('id_user') ? $request->get('id_user') : 0),
                'id_group'              =>  ($request->has('id_group') ? $request->get('id_group') : 0),
                'type_shot'             =>  ($request->has('type_shot') ? $request->get('type_shot'): 0),
                'limit_views'           =>  ($request->has('limit_views') ? $request->get('limit_views'): 0),
                'is_unique_shot'        =>  $request->has('is_unique_shot') ? $request->get('is_unique_shot') : 0,
                'photo_status'          =>  $request->has('photo_status') ? $request->get('photo_status') : 0
            ];

            foreach (config('app.form_lang') as $r_key => $r_item) {
                $options['text_request_payment'][$r_key] = $request->get('text_request_payment'.$r_item['field']);
            }



            $item->update($options);

            $item->requisites_info_fields()->sync($request->info_fields);

            // Записываем в лог
            add_to_currencies_log($item->id_currency, "Реквизит {$item->account_number} обновлен", 1,5);

            flash('Реквизиты для '.$item->currency->payment->name.' успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удалить реквизиты (Но в историях оставляем)
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Requisites::find($id);
        $item->update([
            'is_history'    => 1,
            'history_at'    =>  Carbon::now()->toDateTimeString()
        ]);

        // Записываем в лог
        add_to_currencies_log($item->id_currency, "Реквизит {$item->account_number} добавлен в архив", 3,5);

        flash('Реквизит успешно добавлен в архив', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Архив реквизитов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archive(Request $request)
    {
        $archives = Requisites::isHistory()->orderByDesc('history_at')->paginate(20);

        // Восстанавливаем архивированных счет
        if($request->has('restore')) {
            $update = Requisites::find($request->get('id'));
            $update->is_history = 0;
            $update->history_at = null;
            $update->save();

            flash(sprintf('Номер счета %s успешно восстановлен', $update->account_number), ['alert alert-success']);
            return redirect()->back();
        }


        return view('admin.basic.requisites.archive', compact('archives'));
    }

    /**
     * Создать дубликат реквизитов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Requisites::find($id);
        $newParser = $item->replicate();
        $newParser->save();

        // Записываем в лог
        add_to_currencies_log($item->id_currency, "Для реквизита {$item->account_number} создан дубликат", 2,5);


        flash('Дубликат '.$item->currency->payment->name.' '. $item->currency->code_currency->name .' успешно создан', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Аннулировать посещаемость
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function zero_out(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Requisites::find($id);
        $item->update([
            'view' => 0
        ]);

        flash('Посещаемость '.$item->currency->payment->name.' '. $item->currency->code_currency->name .' успешно аннулирован', ['alert alert-success']);
        return redirect()->back();
    }
}
