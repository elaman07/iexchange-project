<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use App\Models\DirectionField;
use Illuminate\Http\Request;

class DirectionFieldsController extends Controller
{
    /**
     * Список всех доп. полей
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin.basic.direction_exchange.fields.index',[
            'fields' => DirectionField::all()
        ]);
    }

    /**
     * Форма добавления нового доп. поля
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $directions_exchanges = DirectionExchange::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => direction_name($item)
            ];
        })->pluck('name', 'id');

        return view('admin.basic.direction_exchange.fields.create', compact('directions_exchanges'));
    }

    /**
     * Обработка и добавления нового поля
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            if($request->has('item_id'))
            {
                foreach ($request->item_id as $key => $item)
                {
                    DirectionField::findOrFail($key)->update([
                        'min_char'          =>  $request->min_char[$key],
                        'max_char'          =>  $request->max_char[$key],
                        'obligatory_field'  =>  $request->obligatory_field[$key],
                        'field_type'        =>  $request->field_type[$key],
                        'status'            =>  ($request->status[$key] ?? 1)
                    ]);
                }
            }

            return redirect()->back();
        }


        $field = DirectionField::create($request->except('name', 'description', 'directions_exchanges'));
        $options = [];
        foreach (config('app.form_lang') as $key => $item) {
            $name_filter = str_replace(['"', "'"], '', $request->get('name'.$item['field']));
            $options['name'][$key] = $name_filter;
            $options['description'][$key] = $request->get('description'.$item['field']);
        }

        $field->direction_exchange()->sync($request->directions_exchanges ?? []);
        $field->update($options);


        flash(sprintf('Поле %s успешно добавлена', $field->name), ['alert alert-success']);
        return redirect(admin_base_path('/basic/direction_exchange/fields'));
    }

    /**
     * Форма, редактирование доп. полей
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $field = DirectionField::find($id);
        $directions_exchanges = DirectionExchange::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => direction_name($item)
            ];
        })->pluck('name', 'id');

        return view('admin.basic.direction_exchange.fields.edit', [
            'field'  =>  $field,
            'directions_exchanges' => $directions_exchanges
        ]);
    }

    /**
     * Обработка и обновления полей
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $id, Request $request)
    {
        $field = DirectionField::find($id);

        $options = [];
        foreach (config('app.form_lang') as $key => $item) {
            $name_filter = str_replace(['"', "'"], '', $request->get('name'.$item['field']));
            $options['name'][$key] = $name_filter;
            $options['description'][$key] = $request->get('description'.$item['field']);
        }
        $field->update(array_merge(
            $request->except('name', 'directions_exchanges', 'description'), $options
        ));
        $field->direction_exchange()->sync($request->directions_exchanges ?? []);

        flash('Данные успешно обновлены', ['alert alert-success']);
        return redirect(admin_base_path('/basic/direction_exchange/fields'));
    }

    /**
     * Удаление доп. полей для направлений
    */
    public function delete(int $id)
    {
        $field = DirectionField::findOrFail($id);

        if($field->direction_exchange->count() > 0) {
            flash('Выбранное поле удалить невозможно, К поле привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        $field->delete();
        flash('Поле успешно удалено', ['alert alert-success']);
        return redirect(admin_base_path('/basic/direction_exchange/fields'));
    }

    /**
     * Сортировка полей
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting(): \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    {
        $fields = DirectionField::orderBy('sorting')->orderBy('id', 'desc')->get();

        return view('admin.basic.direction_exchange.fields.sorting', [
            'fields'  =>  $fields
        ]);
    }
}
