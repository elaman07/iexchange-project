<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\YourExchange;
use Illuminate\Http\{RedirectResponse, Request};

class YourCourseController extends Controller
{
    /**
     * Список записей своего курса
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.basic.your_course.index', [
            'yours' =>  YourExchange::orderByDesc('id')->cursor()
        ]);
    }

    /**
     * Форма добавления пар
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create()
    {
        return view('admin.basic.your_course.create');
    }

    /**
     * Обработка и добавление записи
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $validator = \Validator::make($request->all(), [
            'name'  =>  'required',
            'exchange_rate' => 'required',
            'number_format'    =>  'required|numeric'
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        }

        $response = YourExchange::create([
            'name'             =>  ($request->has('name') ? $request->get('name') : null),
            'exchange_rate'    =>  ($request->has('exchange_rate') ? $request->get('exchange_rate') : null),
            'number_format'    =>  ($request->has('number_format') ? $request->get('number_format') : null),
        ]);

        flash("Пара {$response->name} успешно добавлена", ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Форма обновления пар
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = YourExchange::find($id);
        return view('admin.basic.your_course.change', [
            'item' => $item
        ]);
    }

    /**
     * Обработка и обновление записей
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        $validator = \Validator::make($request->all(), [
            'name'  =>  'required',
            'exchange_rate' => 'required',
            'number_format'    =>  'required|numeric',
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        }

        $find = YourExchange::find($id);
        $find->update([
            'name'              =>  ($request->has('name') ? $request->get('name') : null),
            'exchange_rate'    =>  ($request->has('exchange_rate') ? $request->get('exchange_rate') : null),
            'number_format'    =>  ($request->has('number_format') ? $request->get('number_format') : null),
        ]);

        flash("Пара {$find->name} успешно обновлена", ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Удалить пару
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        $your = YourExchange::findOrFail($id);

        if($your->direction_exchange->count() > 0) {
            flash("Пару {$your->name} невозможно удалить, к ней привязаны направления", ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Пара {$your->name} успешно удалена", ['alert alert-success']);
        $your->delete();
        return redirect()->back();
    }
}
