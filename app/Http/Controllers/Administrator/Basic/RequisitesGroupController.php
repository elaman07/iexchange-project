<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\RequisitesGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RequisitesGroupController extends Controller
{
    /**
     * Список групп платежных реквизитов
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Обновление позиций
        if($request->has('action') and $request->get('action') == 'update')
        {
            foreach ($request->sorting as $key => $item) {
                RequisitesGroup::find($key)->update([
                    'sorting' => $item
                ]);
            }

            return redirect()->back();
        }


        $groups = RequisitesGroup::all();
        return view('admin.basic.requisites.groups.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления группы
    */
    public function create()
    {
        return view('admin.basic.requisites.groups.create');
    }

    /**
     * Обработка и добавления новой группы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = RequisitesGroup::create([
                'name'  =>  $request->name,
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            $item->sorting = $item->id;
            $item->save();

            flash('Новая группа успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = RequisitesGroup::findOrFail($id);
        return view('admin.basic.requisites.groups.edit',compact('item'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = RequisitesGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $group->update([
                'name'  =>  ($request->has('name') ? $request->get('name') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash('Группа '.$group->name.' успешно добавлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить группу
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = RequisitesGroup::findOrFail($id);

        if($item->requisites_all->count() > 0) {
            flash('Удаление невозможно, к группе привязаны реквизиты', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Группа {$item->name} успешно удалена", ['alert alert-success']);
        $item->delete();

        return redirect()->back();
    }

    /**
     * Сортировка групп
     */
    public function sorting()
    {
        $groups = RequisitesGroup::orderBy('sorting')->get();

        return view('admin.basic.requisites.groups.sorting', compact('groups'));
    }
}