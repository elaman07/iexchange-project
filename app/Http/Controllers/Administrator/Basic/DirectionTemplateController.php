<?php

namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\DirectionTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectionTemplateController extends Controller
{

    protected array $templates = [];

    public function __construct()
    {
        $this->templates = [
            0 => 'Инструкция по оплате',
            1 => 'Описание обмена',
            2 => 'Дополнительное описание обмена',
            3 => 'Формальное описание перед вводом данных',
            4 => 'Дополнительный текст в процессе оплаты (внизу)',
            5 => 'Текст для статуса "Заявка выполнена"',
            6 => 'Текст для статуса "Заявка отклонена"'
        ];
    }


    public function index()
    {
        $templates = DirectionTemplate::orderBy('id' ,'desc')->get();

        return view('admin.basic.direction_exchange.templates.index', [
            'templates' => $templates,
            'types' => $this->templates
        ]);
    }

    /**
     * Добавление нового шаблона
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_type_template' =>  'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        }

        $find = DirectionTemplate::where([
            ['id_type', '=', $request->get('id_type_template')],
        ]);

        if($find->exists()) {
            flash(__('Такой шаблон уже существует'), ['alert alert-danger']);
            return redirect()->back();
        }


        $currency = DirectionTemplate::create([
            'id_type' =>  ($request->has('id_type_template') ? $request->get('id_type_template') : 0)
        ]);

        flash('Шаблон успешно добавлен', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/basic/direction_exchange/templates/'.$currency->id.'/edit')
        );
    }

    /**
     * Форма редактирования
     */
    public function edit(int $id, Request $request)
    {
        $item = DirectionTemplate::find($id);
        $template_bb_code = [];

        if($item->id_type == 0) {
            //$template_bb_code = config('admin-settings.currencies.instruction');
        }

        return view('admin.basic.direction_exchange.templates.edit', [
            'item' => $item,
            'template_name' => $this->templates[$item->id_type] ?? 0,
            'template_bb_code' => $template_bb_code,
            'templates' => $this->templates
        ]);
    }


    public function update(int $id, Request $request)
    {
        $template = DirectionTemplate::find($id);
        $update_options = [
            'type_view_info' => $request->has('type_view_info') ? $request->get('type_view_info') : 0
        ];

        $update_multi_language = [];
        foreach (config('app.form_lang') as $currency_key => $currency_item) {
            $update_multi_language['text'][$currency_key] = $request->get('text'.$currency_item['field']);
        }

        $template->update(array_merge($update_options, $update_multi_language));

        flash('Данные успешно сохранены', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/basic/direction_exchange/templates/'.$template->id.'/edit')
        );
    }

    public function destroy(int $id)
    {
        DirectionTemplate::find($id)->delete();
        flash('Шаблон успешно удален', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/basic/direction_exchange/templates/')
        );
    }
}
