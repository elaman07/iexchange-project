<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\ReserveGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservesGroupController extends Controller
{
    /**
     * Список групп резервов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Обновление позиций
        if($request->has('action') and $request->get('action') == 'update')
        {
            foreach ($request->sorting as $key => $item) {
                ReserveGroup::find($key)->update([
                    'sorting' => $item
                ]);
            }

            return redirect()->back();
        }


        $groups = ReserveGroup::all();
        return view('admin.basic.reserves.groups.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления группы
    */
    public function create()
    {
        return view('admin.basic.reserves.groups.create');
    }

    /**
     * Обработка и добавления новой группы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = ReserveGroup::create([
                'name'  =>  $request->name,
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            $item->sorting = $item->id;
            $item->save();

            flash('Новая группа успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = ReserveGroup::findOrFail($id);
        return view('admin.basic.reserves.groups.edit',compact('item'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = ReserveGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $group->update([
                'name'  =>  ($request->has('name') ? $request->get('name') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash('Группа '.$group->name.' успешно добавлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить группу
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = ReserveGroup::findOrFail($id);
        $item->delete();

        return redirect()->back();
    }
}