<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Reserve;
use App\Models\ReserveAlert;
use App\Models\ReserveGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservesAlertController extends Controller
{
    /**
     * Список групп резервов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $alerts = ReserveAlert::all();
        return view('admin.basic.reserves.alerts.index', compact('alerts'));
    }

    /**
     * Форма добавления группы
    */
    public function create()
    {
        $reserves = Reserve::where([
            ['status', '=', 0],
            ['id_main', '=', 0]
        ])->get();

        return view('admin.basic.reserves.alerts.create', compact('reserves'));
    }

    /**
     * Обработка и добавления новой группы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_reserve' => 'required|unique:reserves_alerts',
            'threshold' => 'required|numeric'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            ReserveAlert::create([
                'id_reserve'  =>  $request->id_reserve,
                'threshold' => ($request->has('threshold') ? $request->get('threshold') : 0),
                'is_telegram' => ($request->has('is_telegram') ? $request->get('is_telegram') : 0),
                'is_email' => ($request->has('is_email') ? $request->get('is_email') : 0)
            ]);

            flash('Оповещение успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = ReserveAlert::findOrFail($id);
        $reserves = Reserve::where([
            ['status', '=', 0],
            ['id_main', '=', 0]
        ])->get();

        return view('admin.basic.reserves.alerts.edit',compact('item', 'reserves'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = ReserveAlert::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'threshold' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $group->update([
                'threshold' => ($request->has('threshold') ? $request->get('threshold') : 0),
                'is_telegram' => ($request->has('is_telegram') ? $request->get('is_telegram') : 0),
                'is_email' => ($request->has('is_email') ? $request->get('is_email') : 0)
            ]);

            flash('Оповещение успешно обновлено', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить группу
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = ReserveAlert::findOrFail($id);
        $item->delete();

        return redirect()->back();
    }
}