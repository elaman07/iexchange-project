<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\RequisiteField;
use App\Models\RequisiteInfoField;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RequisitesInfoFieldController extends Controller
{
    /**
     * Список групп платежных реквизитов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $fields = RequisiteInfoField::orderByDesc('id')->get();
        return view('admin.basic.requisites.info-fields.index', [
            'fields'    =>  $fields
        ]);
    }

    /**
     * Форма добавления поля
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.basic.requisites.info-fields.create');
    }

    /**
     * Обработка и добавления нового поля
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->item_id as $key => $item) {
                $item = RequisiteInfoField::findOrFail($key);
                $item->update([
                    'status' =>  ($request->status[$key] ?? 0)
                ]);
            }

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'key_name' => 'required',
            'value_name' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {


            $options = [
                'status' => ($request->has('status') ? $request->get('status') : 0),
                'user_id'   => $request->user()->id
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['key_name'][$key] = $request->get('key_name'.$item['field']);
                $options['value_name'][$key] = $request->get('value_name'.$item['field']);
            }

            $item = RequisiteInfoField::create($options);

            flash('Новое поле успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = RequisiteInfoField::findOrFail($id);
        return view('admin.basic.requisites.info-fields.edit', compact('item'));
    }

    /**
     * Обработчик обновления поля
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $field = RequisiteInfoField::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'key_name' => 'required',
            'value_name' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'status' => ($request->has('status') ? $request->get('status') : 0),
                'user_id'   => $request->user()->id
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['key_name'][$key] = $request->get('key_name'.$item['field']);
                $options['value_name'][$key] = $request->get('value_name'.$item['field']);
            }

            $field->update($options);

            flash('Поле '.$field->key_name.' успешно добавлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить поле
     *
     * @param $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id): RedirectResponse
    {
        $field = RequisiteInfoField::findOrFail($id);
        if($field->requisites->count() > 0) {
            flash('Выбранное поле удалить невозможно, К этому поле привязаны реквизиты', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Поле {$field->key_name} успешно удалена", ['alert alert-success']);
        $field->delete();

        return redirect()->back();
    }
}
