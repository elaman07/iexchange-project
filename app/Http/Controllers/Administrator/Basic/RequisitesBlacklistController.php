<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\RequisitesBlacklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RequisitesBlacklistController extends Controller
{
    public function index()
    {
        $requisites = RequisitesBlacklist::orderByDesc('id')->get();
        return view('admin.basic.requisites.blacklist.index', compact('requisites'));
    }

    /**
     * Обработчик перед добавлением записи
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'score' => 'required',
            'text'  => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            RequisitesBlacklist::create([
                'score' => $request->get('score'),
                'text' => ($request->get('text') ? $request->get('text') : null),
                'id_user' => $request->user()->id
            ]);

            flash('Реквизит успешно добавлен в черный список', ['alert alert-success']);
        }

        return redirect()->back();
    }

    public function edit($id)
    {
        $item = RequisitesBlacklist::find($id);
        return view('admin.basic.requisites.blacklist.edit', compact('item'));
    }

    /**
     * Обновление реквизита в черном списке
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'score' => 'required',
            'text'  => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {
            RequisitesBlacklist::find($id)->update([
                'score'     => $request->get('score'),
                'text'      => ($request->get('text') ? $request->get('text') : null),
                'id_user'   => $request->user()->id
            ]);

            flash('Реквизит в черном списке успешно обновлен', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удаление реквизита из черного списка
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        RequisitesBlacklist::find($id)->delete();
        flash('Реквизит успешно удален из черного списка', ['alert alert-success']);
        return redirect()->back();
    }
}