<?php

namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Requests\CurrencyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\{Arr, Carbon};
use Illuminate\Support\Facades\Validator;
use Modules\AMLServices\Entities\AMLService;
use Illuminate\Http\{
    RedirectResponse,
    Request
};
use App\Models\{Currency,
    CodeCurrency,
    CurrencyAnalytics,
    CurrencyCommand,
    CurrencyFields,
    CurrencyGroup,
    CurrencyLabel,
    CurrencyLog,
    CurrencyNotification,
    DirectionExchange,
    EventReserve,
    FilterCurrency,
    GatewayMerchant,
    GatewayPayment,
    Payment,
    Requisites,
    Reserve,
    ReserveLogProfit};

class CurrencyController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_currency_enabled_log',
        'currency_is_recount_default',
        'currency_recount_percent',
        'currency_recount_time_minutes',
        'currency_recount_time_hours',
        'is_currency_disabled_admin_filter',
        'is_currencies_fire_angular',
    ];

    protected $allowFilteredPage = [
        'admin_currency_pagination',
        'admin_currency_hidden_columns'
    ];

    /**
     * Список валют
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        // Если данные еще сохранились, делаем чистку
        if(\Session::has('_old_input'))
            \Session::forget('_old_input');

        // Фильтры
        $valuta = Currency::with(['payment' => function($q) {
            $q->select('id','name','logo', 'is_local_image');
        }, 'code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'merchants', 'reserve', 'reserve.main_reserve', 'currency_analytics', 'updated_user' => function($q) {
            $q->select('id' ,'name');
        }, 'filter_currency'])
            ->withCount(['direction_exchange_in','direction_exchange_out'])->active()->where('is_archive', '=', 0)->filter($request->all());

        $valuta->orderBy('sorting_admin')->orderByDesc('id');
        // Статусы
        $statuses = collect([
            ['id' => '0', 'name' => 'Включенные'],
            ['id' => '1', 'name' => 'Отключенные'],
        ])->pluck('name', 'id');

        // Сортировка
        $sorting = collect([
            ['id' => 'default', 'name' => 'По умолчанию'],
            ['id' => 'id-desc', 'name' => 'ID (По убыванию)'],
            ['id' => 'id-asc', 'name' => 'ID (По возрастанию)'],
            ['id' => 'created_at-desc', 'name' => 'Дата добавления (По убыванию)'],
            ['id' => 'created_at-asc', 'name' => 'Дата добавления (По возрастанию)'],
            ['id' => 'updated_at-desc', 'name' => 'Дата обновления (По убыванию)'],
            ['id' => 'updated_at-asc', 'name' => 'Дата обновления (По возрастанию)'],
        ])->pluck('name', 'id');

        // Список платежных систем
        $payments = Payment::active()->pluck('name','id');
        // Список кодов валют
        $code_currencies = CodeCurrency::pluck('name','id');
        // Фильтры валют
        $filter_currencies = FilterCurrency::pluck('name', 'id');

        // Список доступных валидаторов
        $validators = collect(config('crypto.validators_support'))->sortBy(function($item) {
            return $item[0];
        });

        // Список групп валют
        $groups = CurrencyGroup::pluck('name','id');
        $imports = file_get_contents(storage_path('/templates/currencies.json'));


        $gateways_merchants = GatewayMerchant::select('id','name','status')->get()->map(function($item) {
            $status = 'не активна';
            if($item->status == 1) {
                $status = 'активна';
            }
            return [
                'id' => $item->id,
                'name' => sprintf('%s  (%s)', $item->name, $status)
            ];
        })->pluck('name','id');

        // Список доступных валюты для выплат
        $pays = GatewayPayment::where('status', '=', 1)->get();

        // Коды валюты из списка Bestchange
        $bestchange_codes_get = json_decode(file_get_contents(config('bestchange.bestchange_json')), true);
        $bestchange_codes = collect($bestchange_codes_get)->pluck('code', 'code');


        $admin_currency_hidden_columns = explode(',', iEXSetting('admin_currency_hidden_columns'));
        return view('admin.basic.currency.index', [
            'pays'          =>  $pays,
            'bestchange_codes' => $bestchange_codes,
            'valuta'        => $valuta->paginate(iEXSetting('admin_currency_pagination', 20)),
            'gateways_merchants'    =>  $gateways_merchants,
            'statuses'      =>  $statuses,
            'groups'        =>  $groups,
            'payments'      =>  $payments,
            'code_currencies'   =>  $code_currencies,
            'filter_currencies' =>  $filter_currencies,
            'sorting'           =>  $sorting,
            'filter'        =>  $request->all(),
            'validators'    =>  $validators,
            'imports'       => json_decode($imports, true),
            'admin_currency_hidden_columns' => $admin_currency_hidden_columns
        ]);
    }

    /**
     * Обработка и добавление новой валюты
     *
     * @param CurrencyRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request): RedirectResponse
    {
        $admin_currency_hidden_columns = explode(',', iEXSetting('admin_currency_hidden_columns'));

        if($request->has('action'))
        {
            if($request->get('action') == 'settings')
            {
                // Обновление конфига
                $array = [];
                foreach ($this->allowFiltered as $item) {
                    if($request->has($item) and is_array($request->get($item))) {
                        $array[$item] = implode(',', $request->get($item));
                    } else {
                        $array[$item] = ($request->has($item) ? $request->get($item): null);
                    }
                }
                iEXSetting($array);
                flash('Настройки успешно сохранены', ['alert alert-success']);
                return redirect()->back();
            }

            if($request->get('action') == 'settings_columns')
            {
                // Обновление конфига
                $array = [];
                foreach ($this->allowFilteredPage as $item) {
                    if($request->has($item) and is_array($request->get($item))) {
                        $array[$item] = implode(',', $request->get($item));
                    } else {
                        $array[$item] = ($request->has($item) ? $request->get($item): null);
                    }
                }
                iEXSetting($array);
                flash('Настройки успешно сохранены', ['alert alert-success']);
                return redirect()->back();
            }
        }

        // Обновление данные прямо из таблицы валюты
        if($request->get('actions') == 'save')
        {
            // Создание резерва
            if($request->has('reserve_value')) {
                foreach ($request->get('reserve_value') as $key => $value)
                {
                    // Добавляем в список резервов
                    $reserve = Reserve::updateOrCreate([
                        'id_currency'   =>  $key,
                    ], [
                        'id_currency'   =>  $key,
                        'id_group'      =>  0,
                        'summa'         =>  (float)$value,
                        'id_user'       =>  $request->user()->id,
                    ]);

                    // Создаем событие для резерва
                    EventReserve::updateOrCreate([
                        'id_currency'       =>  $reserve->id_currency,
                        'id_reserve'        =>  $reserve->id,
                    ], [
                        'id_user'           =>  $reserve->id_user,
                        'id_currency'       =>  $reserve->id_currency,
                        'id_reserve'        =>  $reserve->id,
                        'type'              =>  1,
                        'text'              =>  'Создание резерва. Сумма: '.$reserve->summa,
                        'value_before'      =>  0,
                        'value_after'       =>  (float)$reserve->summa,
                        'comment'           =>  'Создание резерва для валюты'
                    ]);
                }
            }

            foreach ($request->item_id as $value)
            {
                $update = Currency::find($value);
                $update_data = [
                    'updated_user_id'       =>  auth()->user()->id,
                ];

                if(in_array('number_format', $admin_currency_hidden_columns)) {
                    $update_data['number_format'] = ($request->number_format[$value] ?? 0);
                }

                if(in_array('payment', $admin_currency_hidden_columns)) {
                    $update_data['id_pay'] = ($request->id_pay[$value] ?? 0);
                }

                $update->update($update_data);

                if(in_array('merchant', $admin_currency_hidden_columns)) {
                    $update->merchants()->sync($request->gateways_merchants[$value] ?? []);
                }
            }

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();

        }elseif($request->has('currency_checkbox'))
        {
            foreach ($request->get('currency_checkbox') as $value) {
                $currency = Currency::findOrFail($value);
                if($request->get('actions') == 'activation') {
                    // Включение валюты
                    $currency->update(['status' => 0]);
                } elseif($request->get('actions') == 'deactivation') {
                    // Отключение валют
                    $currency->update(['status' => 1]);

                    // Отключаем все направления
                    DirectionExchange::where('id_currency1', $currency->id)
                        ->orWhere('id_currency2', $currency->id)->update(['status' => 0]);

                } elseif($request->get('actions') == 'trashed') {
                    // Визуальное удаление
                    $currency->update(['status' => 2]);
                } elseif($request->get('actions') == 'archive') {
                    // Архивирование валют
                    $currency->update(['is_archive' => 1]);
                }
            }
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        if($request->has('action') and $request->get('action') == 'create')
        {
            $validator = Validator::make($request->all(), [
                'id_payment' =>  'required',
                'id_code_currency' => 'required',
            ]);

            // Перед добавлением новой валюты проверяем на ошибки
            if ($validator->fails()) {
                flash($validator->messages()->first(), ['alert alert-danger']);
                return redirect()->back();
            }

            $find = Currency::active()->where([
                ['id_payment', '=', $request->get('id_payment')],
                ['id_code_currency', '=', $request->get('id_code_currency')],
            ]);

            if($find->exists()) {
                flash(__('Такая валюта уже существует'), ['alert alert-danger']);
                return redirect()->back();
            }


            $currency = Currency::create([
                'id_payment'            =>  ($request->has('id_payment') ? $request->get('id_payment') : 0),
                'id_code_currency'      =>  ($request->has('id_code_currency') ? $request->get('id_code_currency') : 0),
            ]);

            // Добавляем запись в лог
            add_to_currencies_log($currency->id, 'Добавление новой валюты', 0,0);
            flash(sprintf('Валюты %s %s успешно добавлена', $currency->payment->name, $currency->code_currency->name), ['alert alert-success']);
            return redirect()->to(
                admin_base_path('/basic/currency/'.$currency->id.'/edit')
            );
        }

    }

    /**
     * Форма редактирования валюты
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request, int $id)
    {
        // Информация о валюте
        $info = Currency::findOrFail($id);

        if($request->has('actions') and $request->get('actions') == 'return_archive') {
            $info->update(['is_archive' => 0]);
            return redirect()->back();
        }

        // Список платежных систем
        $payments = Payment::pluck('name', 'id');
        // Список кодов валют
        $code_currencies = CodeCurrency::pluck('name','id');
        // Список фильтров валют
        $filter_currencies = FilterCurrency::pluck('name','id');
        // Список меток
        $labels = CurrencyLabel::pluck('title', 'id');
        // Список доступных валюты для выплат
        $pays = GatewayPayment::where('status', '=', 1)->get();


        // Коды валюты из списка Bestchange
        try {
            $bestchange_codes_get = json_decode(file_get_contents(config('bestchange.bestchange_json')), true);
            $bestchange_codes = collect($bestchange_codes_get);

        }catch (\Exception $exception) {
            $bestchange_codes = [];
        }

        // Список доступных валидаторов
        $validators = collect(config('crypto.validators_support'))->sortBy(function($item) {
            return $item[0];
        });

        // Список групп валют
        $groups = CurrencyGroup::pluck('name','id');

        // Доп. поля
        $in_custom_fields = CurrencyFields::whereStatus(0)->where('when_print', '=', 0)->orderBy('sorting')->orderBy('id')->pluck('name', 'id');
        $out_custom_fields = CurrencyFields::whereStatus(0)->where('when_print', '=', 1)->orderBy('sorting_out')->orderBy('id')->pluck('name', 'id');

        // Список мерчантов
        $merchants = GatewayMerchant::select('id','name', 'status')
            ->get()->map(function($item) {
            $status = __('не активна');
            if($item->status == 1) {
                $status = __('активна');
            }
            return [
                'id' => $item->id,
                'name' => sprintf('%s  (%s)', $item->name, $status)
            ];
        })->pluck('name','id');


        // Список AML сервисов
        $aml_services = AMLService::orderBy('id')->get();

        return view('admin.basic.currency.change', compact(
            'info',
            'payments',
            'in_custom_fields',
            'out_custom_fields',
            'code_currencies',
            'filter_currencies',
            'merchants',
            'pays',
            'bestchange_codes',
            'validators',
            'groups',
            'aml_services',
            'labels'
        ));
    }

    /**
     * Обновление данные
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $item = Currency::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_payment' =>  'required',
            'id_code_currency' => 'required',
            'id_filter_currency' => 'required',
            'convert_by' =>  'required|numeric',
            'number_format' => 'required|numeric',
            'min_char' => 'required|numeric',
            'max_char' => 'required|numeric',
            'status' => 'required|numeric'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            //Выбираем "Обозначение для Сайта" из списка
            $designation_xml = $request->get('designation_xml');
            if(!is_null($request->get('designation_xml_select'))) {
                $designation_xml = $request->get('designation_xml_select');
            }

            if($request->get('text_color') != $item->text_color)
            {
                \Artisan::call('generator:css');
                iEXSetting([
                    'color_style_hash' =>  Carbon::now()->format('c')
                ])->save();
            }



            $item->merchants()->sync($request->ids_merchant);
            $item->currency_in_fields()->sync($request->in_custom_fields);
            $item->currency_out_fields()->sync($request->out_custom_fields);

            $update_options = [
                'id_payment'            =>  ($request->has('id_payment') ? $request->get('id_payment') : 0),
                'id_code_currency'      =>  ($request->has('id_code_currency') ? $request->get('id_code_currency') : 0),
                'id_filter_currency'    =>  ($request->has('id_filter_currency') ? $request->get('id_filter_currency') : 0),
                'id_label'    =>  ($request->has('id_label') ? $request->get('id_label') : 0),
                'designation_xml'       =>  $designation_xml,
                'is_hold'               =>  ($request->has('is_hold') ? $request->get('is_hold') : 0),
                'is_valid_account'      =>  ($request->has('is_valid_account') ? $request->get('is_valid_account') : null),
                'visible_give'          =>  ($request->has('visible_give') ? $request->get('visible_give') : 0),
                'visible_receiving'     =>  ($request->has('visible_receiving') ? $request->get('visible_receiving') : 0),
                'max_limit_in_reserve' =>  ($request->has('max_limit_in_reserve') ? $request->get('max_limit_in_reserve') : 0),
                'updated_user_id'       =>  auth()->user()->id,

                'is_unique_recount_order'           =>  ($request->has('is_unique_recount_order') ? $request->get('is_unique_recount_order') : 0),
                'is_enable_auto_recount_order'           =>  ($request->has('is_enable_auto_recount_order') ? $request->get('is_enable_auto_recount_order') : 0),
                'unique_recount_percent'           =>  ($request->has('unique_recount_percent') ? $request->get('unique_recount_percent') : 0),
                'recount_time_hours'           =>  ($request->has('recount_time_hours') ? $request->get('recount_time_hours') : 0),
                'recount_time_minutes'           =>  ($request->has('recount_time_minutes') ? $request->get('recount_time_minutes') : 0),

                'is_aml_check_cost_reserve' => $request->has('is_aml_check_cost_reserve') ? $request->get('is_aml_check_cost_reserve') : 0,
                'aml_check_cost'            =>  $request->has('aml_check_cost') ? $request->get('aml_check_cost') : 0,

//                'is_recount_default'           =>  ($request->has('is_recount_default') ? $request->get('is_recount_default') : 0),
//                'recount_percent'           =>  ($request->has('recount_percent') ? $request->get('recount_percent') : 0),
                'is_payment_default'    =>  ($request->has('is_payment_default') ? $request->get('is_payment_default') : 0),
                'is_payment_unique'     =>  ($request->has('is_payment_unique') ? $request->get('is_payment_unique') : 0),
                'convert_by'            =>  ($request->has('convert_by') ? $request->get('convert_by') : 0),
                'number_format'         =>  ($request->has('number_format') ? $request->get('number_format') : 0),
                'number_format_xml'     =>  ($request->has('number_format_xml') ? $request->get('number_format_xml') : 0),
                'min_char'              =>  ($request->has('min_char') ? $request->get('min_char') : 0),
                'max_char'              =>  ($request->has('max_char') ? $request->get('max_char') : 0),
                'mask_input'            =>  ($request->has('mask_input') ? $request->get('mask_input') : null),
                'char_default'          =>  ($request->has('char_default') ? $request->get('char_default') : null),
                'allowed_char'          =>  ($request->has('allowed_char') ? $request->get('allowed_char') : 0),
                'id_pay'                =>  ($request->has('id_pay') ? $request->get('id_pay') : 0),
                'is_allow_order'        =>  ($request->has('is_allow_order') ? $request->get('is_allow_order') : 0),

                'status'                =>  ($request->has('status') ? $request->get('status') : 0),
                'visible_code_currency' =>  ($request->has('visible_code_currency') ? $request->get('visible_code_currency') : 1),
                'text_color'            =>  ($request->has('text_color') ? $request->get('text_color') : '#000000'),
                'is_qrcode'             =>  ($request->has('is_qrcode') ? $request->get('is_qrcode') : 0),
                'prefix_qrcode'         =>  ($request->has('prefix_qrcode') ? $request->get('prefix_qrcode') : null),
                'is_qrcode_amount'      =>  ($request->has('is_qrcode_amount') ? $request->get('is_qrcode_amount') : 0),
                'day_limit_give'        =>  ($request->has('day_limit_give') ? $request->get('day_limit_give') : 0),
                'day_limit_receive'     =>  ($request->has('day_limit_receive') ? $request->get('day_limit_receive') : 0),

                'month_limit_in'        =>  ($request->has('month_limit_in') ? $request->get('month_limit_in') : 0),
                'month_limit_out'        =>  ($request->has('month_limit_out') ? $request->get('month_limit_out') : 0),
                'is_income_banner'      =>  ($request->has('is_income_banner') ? $request->get('is_income_banner') : 0),
                'is_kyc_checkbox'      =>  ($request->has('is_kyc_checkbox') ? $request->get('is_kyc_checkbox') : 0),
                'kyc_enabled'      =>  ($request->has('kyc_enabled') ? $request->get('kyc_enabled') : 0),
                'is_email_verification_modal' => ($request->has('is_email_verification_modal') ? $request->get('is_email_verification_modal') : 0),
                'transfer_percent_reserve' => ($request->has('transfer_percent_reserve') ? $request->get('transfer_percent_reserve') : 0),
                'transfer_amount_reserve' => ($request->has('transfer_amount_reserve') ? $request->get('transfer_amount_reserve') : 0),
                'remove_spaces_requisite'   => ($request->has('remove_spaces_requisite') ? $request->get('remove_spaces_requisite') : 0),
                'payout_commission'         =>  ($request->has('payout_commission') ? $request->get('payout_commission') : 0),
                'payout_commission_amount'  =>  ($request->has('payout_commission_amount') ? $request->get('payout_commission_amount') : 0),


                'commission_payment_currency' => ($request->has('commission_payment_currency') ? $request->get('commission_payment_currency') : 0),

                'id_group'            =>  ($request->has('id_group') ? $request->get('id_group') : 0),
                'profit_percent_reserve'    =>  ($request->has('profit_percent_reserve') ? $request->get('profit_percent_reserve') : 0),
                'hold_in_hours'             =>  ($request->has('hold_in_hours') ? $request->get('hold_in_hours') : 0),
                'hold_delay'                =>  ($request->has('hold_delay') ? $request->get('hold_delay') : 0),
                'max_display_reserve'       =>  ($request->has('max_display_reserve') ? $request->get('max_display_reserve') : null),
                'hour_limit_order_pending'  =>  ($request->has('hour_limit_order_pending') ? $request->get('hour_limit_order_pending') : 0),
                'hour_limit_order_process'  =>  ($request->has('hour_limit_order_process') ? $request->get('hour_limit_order_process') : 0),
                'is_enabled_verification'   =>  ($request->has('is_enabled_verification') ? $request->get('is_enabled_verification') : 0),
                'is_out_enabled_verification'   =>  ($request->has('is_out_enabled_verification') ? $request->get('is_out_enabled_verification') : 0),
                'min_amount_verification'   =>  ($request->has('min_amount_verification') ? (float)$request->get('min_amount_verification') : 0),
                'min_out_amount_verification'   =>  ($request->has('min_out_amount_verification') ? $request->get('min_out_amount_verification') : 0),
                'is_user_verification'      =>  ($request->has('is_user_verification') ? $request->get('is_user_verification') : 0),
                'commission_merchant_percent'   =>  ($request->has('commission_merchant_percent') ? $request->get('commission_merchant_percent') : 0),
                'commission_merchant_currency' => ($request->has('commission_merchant_currency') ? $request->get('commission_merchant_currency') : 0),
                'fee_no_verified_merchant'  =>  ($request->has('fee_no_verified_merchant') ? $request->get('fee_no_verified_merchant') : 0),

                'out_pay_min_amount'       =>  $request->has('out_pay_min_amount') ? $request->get('out_pay_min_amount') : 0,
                'out_pay_max_amount'       =>  $request->has('out_pay_max_amount') ? $request->get('out_pay_max_amount') : 0,
                'out_pay_day_limit'       =>  $request->has('out_pay_day_limit') ? $request->get('out_pay_day_limit') : 0,
                'out_pay_month_limit'       =>  $request->has('out_pay_month_limit') ? $request->get('out_pay_month_limit') : 0,


                'is_allow_amount_space'   =>  ($request->has('is_allow_amount_space') ? $request->get('is_allow_amount_space') : 0),
                'network_code'           => $request->has('network_code') ? $request->get('network_code') : null,
                'network_code_out'           => $request->has('network_code_out') ? $request->get('network_code_out') : null,
                'aml_day_limit_count'   =>  $request->has('aml_day_limit_count') ? $request->get('aml_day_limit_count') : 0,


                'blockchain_network_congestion' => $request->has('blockchain_network_congestion') ? $request->get('blockchain_network_congestion') : 0,
                'is_verified_cabinet'       =>  $request->has('is_verified_cabinet') ? $request->get('is_verified_cabinet') : 0,
                'first_value'               =>  $request->has('first_value') ? $request->get('first_value') : null,
                'type_output_requisites'    =>  $request->has('type_output_requisites') ? $request->get('type_output_requisites') : 0,
                'auto_pay_order_pay'        =>  $request->has('auto_pay_order_pay') ? $request->get('auto_pay_order_pay') : 0,
                'is_allow_file'             =>  $request->has('is_allow_file') ? $request->get('is_allow_file') : 0,
                'is_enabled_step_order'     =>  $request->has('is_enabled_step_order') ? $request->get('is_enabled_step_order') : 0,

                'aml_service'     =>  $request->has('aml_service') ? $request->get('aml_service') : null,
                'is_in_aml_check_wallet'     =>  $request->has('is_in_aml_check_wallet') ? $request->get('is_in_aml_check_wallet') : 0,
                'is_out_aml_check_wallet'     =>  $request->has('is_out_aml_check_wallet') ? $request->get('is_out_aml_check_wallet') : 0,
                'is_in_aml_check_tx'     =>  $request->has('is_in_aml_check_tx') ? $request->get('is_in_aml_check_tx') : 0,
                'in_aml_tx_amount'     =>  $request->has('in_aml_tx_amount') ? $request->get('in_aml_tx_amount') : 0,
            ];

            $update_multi_language = [];
            foreach (config('app.form_lang') as $currency_key => $currency_item) {
                $update_multi_language['tech_currency_name'][$currency_key] = $request->get('tech_currency_name'.$currency_item['field']);
                $update_multi_language['desc_exchange'][$currency_key] = $request->get('desc_exchange'.$currency_item['field']);
                $update_multi_language['instruction_exchange'][$currency_key] = $request->get('instruction_exchange'.$currency_item['field']);
                $update_multi_language['notice_in'][$currency_key] = $request->get('notice_in'.$currency_item['field']);;
                $update_multi_language['notice_out'][$currency_key] = $request->get('notice_out'.$currency_item['field']);
                $update_multi_language['field_name_from'][$currency_key] = $request->get('field_name_from'.$currency_item['field']);
                $update_multi_language['field_name_to'][$currency_key] = $request->get('field_name_to'.$currency_item['field']);
                $update_multi_language['field_comment_from'][$currency_key] = $request->get('field_comment_from'.$currency_item['field']);
                $update_multi_language['field_comment_to'][$currency_key] = $request->get('field_comment_to'.$currency_item['field']);
                $update_multi_language['account_number_field'][$currency_key] = $request->get('account_number_field'.$currency_item['field']);
                $update_multi_language['button_create_order'][$currency_key] = $request->get('button_create_order'.$currency_item['field']);
                $update_multi_language['button_create_order_text'][$currency_key] = $request->get('button_create_order_text'.$currency_item['field']);
                $update_multi_language['formalization_text'][$currency_key] = $request->get('formalization_text'.$currency_item['field']);
                $update_multi_language['other_docs_in'][$currency_key] = $request->get('other_docs_in'.$currency_item['field']);
                $update_multi_language['other_docs_out'][$currency_key] = $request->get('other_docs_out'.$currency_item['field']);
                $update_multi_language['aml_text_in'][$currency_key] = $request->get('aml_text_in'.$currency_item['field']);
                $update_multi_language['aml_text_out'][$currency_key] = $request->get('aml_text_out'.$currency_item['field']);
                $update_multi_language['verification_info'][$currency_key] = $request->get('verification_info'.$currency_item['field']);
                $update_multi_language['verification_text'][$currency_key] = $request->get('verification_text'.$currency_item['field']);
                $update_multi_language['recount_course_text'][$currency_key] = $request->get('recount_course_text'.$currency_item['field']);
                $update_multi_language['valid_account_error'][$currency_key] = $request->get('valid_account_error'.$currency_item['field']);
                $update_multi_language['min_max_error_message'][$currency_key] = $request->get('min_max_error_message'.$currency_item['field']);
                $update_multi_language['account_number_field_text'][$currency_key] = $request->get('account_number_field_text'.$currency_item['field']);
            }

            $item->update(array_merge($update_options, $update_multi_language));
            $item->update([
                'tech_name' => $item->payment->name.' '.$item->code_currency->name
            ]);
            // Записываем в лог
            add_to_currencies_log($item->id, 'Обновление валюты', 1,0);

            flash(sprintf('Валюты %s %s успешно обновлена', $item->payment->name, $item->code_currency->name), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Сделать дубликат валюты
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function duplicate(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }


        $currency = Currency::find($id);
        $newCurrency = $currency->replicate();
        $newCurrency->save();

        // Записываем в лог
        add_to_currencies_log($currency->id, 'Создание дубликата валюты', 2,0);
        flash('Дубликат успешно создан', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Удалить валюту
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $currency = Currency::findOrFail($id);

        // Делаем доп. проверку в списке направлений
        $direction = DirectionExchange::where('id_currency1', '=', $id)
            ->orWhere('id_currency2', '=', $id);

        if($direction->count() > 0) {
            flash('Выбранную валюту удалить невозможно, К валюте привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        // Удаляем еще связанные (Резервы и события резервов)
        Reserve::where('id_currency', $id)->delete();
        EventReserve::where('id_currency', $id)->delete();
        CurrencyNotification::where('id_currency', $id)->delete();
        CurrencyLog::where('id_currency', $id)->delete();
        CurrencyFields::where('id_currency', $id)->delete();
        CurrencyCommand::where('id_currency', $id)->delete();
        Requisites::where('id_currency', $id)->delete();
        CurrencyAnalytics::where('id_currency', $id)->delete();

        // Записываем в лог
        add_to_currencies_log($currency->id, 'Удаление валюты', 3,0);

        flash(
            sprintf('Валюта %s %s успешно удалена', $currency->payment->name, $currency->code_currency->name),
            ['alert alert-success']
        );
        $currency->delete();

        return redirect()->to(
            admin_base_path('/basic/currency')
        );
    }


    /**
     * Сортировка для админ панели
     */
    public function sortingAdmin()
    {
        $currencies = Currency::active()->orderBy('sorting_admin')->get();
        return view('admin.basic.currency.sorting_admin', [
            'currencies' => $currencies
        ]);
    }


    /**
     * Сортировка резервов
    */
    public function sortingReserves()
    {
        $currencies = Currency::active()->orderBy('sorting_reserve')->get();
        return view('admin.basic.currency.sorting', [
            'currencies' => $currencies
        ]);
    }

    /**
     * Лог валют
    */
    public function log()
    {
        $logs = CurrencyLog::orderByDesc('id')->paginate(20);
        return view('admin.basic.currency.log', compact('logs'));
    }

    /**
     * Лог прибыли
    */
    public function logProfit()
    {
        $logs = ReserveLogProfit::orderByDesc('id')->paginate(20);
        return view('admin.basic.currency.log_profit', compact('logs'));
    }


    /**
     * Архивные валюты
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archive(Request $request)
    {
        if($request->has('currency_checkbox'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            foreach ($request->get('currency_checkbox') as $value) {
                $currency = Currency::findOrFail($value);
                $currency->update(['is_archive' => 0]);
            }

            flash('Выбранные валюты успешно возвращены в общий список', ['alert alert-success']);
            return redirect()->back();
        }

        $currencies = Currency::where('is_archive', '=', 1)->orderByDesc('id')->get();
        return view('admin.basic.currency.archive', compact('currencies'));
    }
}
