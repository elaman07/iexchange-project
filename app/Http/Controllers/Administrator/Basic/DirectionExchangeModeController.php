<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use App\Models\DirectionExchangeMode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectionExchangeModeController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_enabled_module_direction_mode'
    ];

    /**
     * Список групп направлений для режимов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if($request->has('status_mode'))
        {
            if($request->get('status_mode') == 'enable') {
                \DB::table('direction_exchange_modes')->update(['status' => 0]);
                DirectionExchangeMode::find($request->id)->update(['status' => 1]);
            }

            if($request->get('status_mode') == 'disable') {
                \DB::table('direction_exchange_modes')->update(['status' => 0]);
            }

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $modes = DirectionExchangeMode::all();

        return view('admin.basic.direction_exchange.mode.index', [
            'modes'    =>  $modes
        ]);
    }

    /**
     * Форма добавления режима
     */
    public function create()
    {
        // Фильтры
        $exchanges = DirectionExchange::active()->filter([])->cursor()->map(function($item) {
           return [
               'id' => $item->id,
               'name' => direction_name($item)
           ];
        })->pluck('name', 'id');

        return view('admin.basic.direction_exchange.mode.create', [
            'exchanges' => $exchanges
        ]);
    }

    /**
     * Обработка и добавления нового режима
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = DirectionExchangeMode::create([
                'name'  =>  $request->name
            ]);
            $item->direction_exchange()->sync($request->ids_direction_exchange);
            $item->save();

            flash('Новый режим успешно создан', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения режима
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        // Фильтры
        $exchanges = DirectionExchange::active()->filter([])->cursor()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => direction_name($item)
            ];
        })->pluck('name', 'id');


        $item = DirectionExchangeMode::findOrFail($id);
        return view('admin.basic.direction_exchange.mode.edit',compact('item', 'exchanges'));
    }

    /**
     * Обработчик обновления режима
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = DirectionExchangeMode::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $group->update([
                'name'  =>  ($request->has('name') ? $request->get('name') : null)
            ]);

            $group->direction_exchange()->sync($request->ids_direction_exchange);

            flash('Режим '.$group->name.' успешно обновлен', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удалить режим
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = DirectionExchangeMode::findOrFail($id);
        if(isset($item->direction_exchange)) {
            $item->direction_exchange()->sync([]);
        }

        flash("Режим с привязанными направлениями {$item->name} успешно удалена", ['alert alert-success']);
        $item->delete();

        return redirect()->back();
    }
}
