<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\PaymentExplorer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentExplorerController extends Controller
{
    public function index()
    {
        $explorers = PaymentExplorer::all();
        // Платежные системы
        $payments = Payment::all()->pluck('name', 'id');
        return view('admin.basic.payments.explorer.index', compact('explorers', 'payments'));
    }

    /**
     * Обработчик и добавление нового Explorer
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment' =>  'required',
            'link' => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            PaymentExplorer::create([
                'id_payment' => $request->get('payment'),
                'link' => $request->get('link'),
                'text' => ($request->has('text') ? $request->get('text') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash('Ссылка успешно добавлено', ['alert alert-success']);
        }

        return redirect()->back();
    }


    public function edit($id) {

        $explorer = PaymentExplorer::find($id);
        $payments = Payment::all()->pluck('name', 'id');

        return view('admin.basic.payments.explorer.edit', compact('explorer', 'payments'));
    }

    /**
     * Обновить blockchain explorer
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment' =>  'required',
            'link' => 'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            PaymentExplorer::find($id)->update([
                'id_payment' => $request->get('payment'),
                'link' => $request->get('link'),
                'text' => ($request->has('text') ? $request->get('text') : null),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash('Ссылка успешно обновлена', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удаление ссылки
    */
    public function destroy($id) {
        PaymentExplorer::find($id)->delete();
        flash('Ссылка успешно удалена', ['alert alert-success']);

        return redirect()->back();
    }
}