<?php

namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\CurrencyTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyTemplateController extends Controller
{

    protected array $templates = [];

    public function __construct()
    {
        $this->templates = [
            0 => 'Инструкция к оплате',
            1 => 'Описание обмена',
            2 => 'Дополнительный текст в процессе оплаты (внизу) (Для отдаю)',
            3 => 'Дополнительный текст в процессе оплаты (внизу) (Для получаю)'
        ];
    }


    public function index()
    {
        $templates = CurrencyTemplate::orderBy('id' ,'desc')->get();
        return view('admin.basic.currency.templates.index', [
            'templates' => $templates,
            'types' => $this->templates
        ]);
    }

    /**
     * Добавление нового шаблона
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_type_template' =>  'required'
        ]);

        // Перед добавлением новой валюты проверяем на ошибки
        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        }

        $find = CurrencyTemplate::where([
            ['id_type', '=', $request->get('id_type_template')],
        ]);

        if($find->exists()) {
            flash(__('Такой шаблон уже существует'), ['alert alert-danger']);
            return redirect()->back();
        }


        $currency = CurrencyTemplate::create([
            'id_type' =>  ($request->has('id_type_template') ? $request->get('id_type_template') : 0)
        ]);

        flash('Шаблон успешно добавлен', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/basic/currency-templates/'.$currency->id.'/edit')
        );
    }

    /**
     * Форма редактирования
    */
    public function edit(int $id, Request $request)
    {
        $item = CurrencyTemplate::find($id);
        $template_bb_code = [];

        if($item->id_type == 0) {
            $template_bb_code = config('admin-settings.currencies.instruction');
        }

        return view('admin.basic.currency.templates.edit', [
            'item' => $item,
            'template_name' => $this->templates[$item->id_type] ?? 0,
            'template_bb_code' => $template_bb_code
        ]);
    }


    public function update(int $id, Request $request)
    {
        $template = CurrencyTemplate::find($id);
        $update_options = [
            'type_view_info' => $request->has('type_view_info') ? $request->get('type_view_info') : 0
        ];

        $update_multi_language = [];
        foreach (config('app.form_lang') as $currency_key => $currency_item) {
            $update_multi_language['text'][$currency_key] = $request->get('text'.$currency_item['field']);
        }

        $template->update(array_merge($update_options, $update_multi_language));

        flash('Данные успешно сохранены', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/basic/currency-templates/'.$template->id.'/edit')
        );
    }

    public function destroy(int $id)
    {
        CurrencyTemplate::find($id)->delete();
        flash('Шаблон успешно удален', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/basic/currency-templates/')
        );
    }
}
