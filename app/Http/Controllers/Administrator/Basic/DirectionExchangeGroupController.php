<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\DirectionExchangeGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DirectionExchangeGroupController extends Controller
{
    /**
     * Список групп направлений обменов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Обновление позиций
        if($request->has('action') and $request->get('action') == 'update')
        {
            foreach ($request->sorting as $key => $item) {
                DirectionExchangeGroup::find($key)->update([
                    'sorting' => $item
                ]);
            }

            return redirect()->back();
        }


        $groups = DirectionExchangeGroup::all();
        return view('admin.basic.direction_exchange.groups.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления группы
     */
    public function create()
    {
        return view('admin.basic.direction_exchange.groups.create');
    }

    /**
     * Обработка и добавления новой группы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = DirectionExchangeGroup::create([
                'name'  =>  $request->name,
                'id_user' => auth()->id()
            ]);
            $item->save();

            flash('Новая группа успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма изменения группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = DirectionExchangeGroup::findOrFail($id);
        return view('admin.basic.direction_exchange.groups.edit',compact('item'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = DirectionExchangeGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $group->update([
                'name'  =>  ($request->has('name') ? $request->get('name') : null)
            ]);

            flash('Группа '.$group->name.' успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удалить группу
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = DirectionExchangeGroup::findOrFail($id);
        if($item->direction_exchange->count() > 0) {
            flash('Это группу нельзя удалить, к ней привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Группа {$item->name} успешно удалена", ['alert alert-success']);
        $item->delete();

        return redirect()->back();
    }
}
