<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Http\Controllers\Controller;
use App\Models\ReserveFile;
use App\Models\ReserveFileGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservesFilesController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $links = ReserveFileGroup::where('status', '=', 1)->get();
        return view('admin.basic.reserves.files.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $links = ReserveFileGroup::where('status', '=', 1)->get();
        return view('admin.basic.reserves.files.create', compact('links'));
    }

    /**
     * Обработка и добавление
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_group' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $rate = ReserveFile::create([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'id_group'          => $request->has('id_group') ? $request->get('id_group') : 0,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'     => $request->has('number_format') ? $request->get('number_format') : 0,
            ]);

            flash("Резерв  {$rate->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('reserves-files.index');
        }

    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ReserveFile::findOrFail($id);
        $links = ReserveFileGroup::where('status', '=', 1)->get();

        return view('admin.basic.reserves.files.edit',compact('item', 'links'));
    }

    /**
     * Обработка и обновление
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $reserve = ReserveFile::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_group' => ['required']
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {
            $reserve->update([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'id_group'          => $request->has('id_group') ? $request->get('id_group') : 0,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'          => $request->has('number_format') ? $request->get('number_format') : 0,
            ]);

            flash("Резерв {$reserve->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('reserves-files.index');
        }
    }

    /**
     * Удаление
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $reserve = ReserveFile::findOrFail($id);

        if($reserve->reserves->count() > 0) {
            flash('Выбранный резерв удалить невозможно, К нему привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Резерв {$reserve->name} успешно удален", ['alert alert-success']);
        $reserve->delete();

        return redirect()->route('reserves-files.index');
    }
}
