<?php
namespace App\Http\Controllers\Administrator\Basic;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\CurrencyCommand;
use App\Models\CurrencyGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyCommandController extends Controller
{
    /**
     * Список команд
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $groups = CurrencyCommand::orderBy('sorting')->paginate(20);
        return view('admin.basic.currency.commands.index', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * Форма добавления группы
     */
    public function create()
    {
        $currencies = Currency::active()
            ->where([
                ['is_archive', '=', 0]
            ])->get();

        return view('admin.basic.currency.commands.create', compact('currencies'));
    }

    /**
     * Обработка и добавления новой команды
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Обновление позиций
        if($request->has('action') and $request->get('action') == 'update')
        {
            foreach ($request->sorting as $key => $item) {
                CurrencyCommand::find($key)->update([
                    'sorting' => $item
                ]);
            }
            return redirect()->back();
        } else {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'id_currency' => ['required', 'exists:currencies,id'],
                'amount' => ['required', 'numeric']
            ]);

            if ($validator->fails())
            {
                flash($validator->messages()->first(), ['alert alert-danger']);
                return redirect()->back()->withInput();
            } else {

                $item = CurrencyCommand::create([
                    'id_currency' => $request->id_currency,
                    'name'  =>  $request->name,
                    'amount' => ($request->has('amount') ? $request->get('amount') : 0),
                    'sorting' => ($request->has('sorting') ? $request->get('sorting') : 0)
                ]);
                $item->save();

                // Записываем в лог
                add_to_currencies_log($item->id_currency, 'Добавлена новая команда '.$item->name.' ', 0,1);

                flash('Новая команда успешно добавлена', ['alert alert-success']);
                return redirect()->back();
            }

        }
    }

    /**
     * Форма изменения команды
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id) {
        $item = CurrencyCommand::findOrFail($id);
        $currencies = Currency::active()
            ->where([
                ['is_archive', '=', 0]
            ])->get();

        return view('admin.basic.currency.commands.edit',compact('item','currencies'));
    }

    /**
     * Обработчик обновления группы
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = CurrencyCommand::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_currency' => ['required', 'exists:currencies,id'],
            'name' => 'required',
            'amount' => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $group->update([
                'id_currency' => $request->id_currency,
                'name'  =>  ($request->has('name') ? $request->get('name') : null),
                'amount' => ($request->has('amount') ? $request->get('amount') : 0),
                'sorting' => ($request->has('sorting') ? $request->get('sorting') : 0)
            ]);

            // Записываем в лог
            add_to_currencies_log($group->id_currency, 'Обновление команды '.$group->name.' ', 1,1);
            flash('Команда '.$group->name.' успешно обновлена', ['alert alert-success']);

            return redirect()->back();
        }
    }

    /**
     * Удалить команду
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $item = CurrencyCommand::findOrFail($id);

        flash(
            sprintf('Команды %s успешно удалена', $item->name),
            ['alert alert-success']
        );

        // Записываем в лог
        add_to_currencies_log($item->id_currency, 'Удаление команды '.$item->name.' ', 3,1);

        $item->delete();
        return redirect()->back();
    }
}