<?php
namespace App\Http\Controllers\Administrator\Basic;


use App\Common\Packages\Payment\Facades\PaymentFacade;
use App\Http\Controllers\Controller;
use App\Jobs\NotifyChangeReserveJob;
use App\Mail\NotifyChangeReserve;
use App\Models\Currency;
use App\Models\EventReserve;
use App\Models\GatewayPayment;
use App\Models\Reserve;
use App\Models\ReserveAlert;
use App\Models\ReserveFile;
use App\Models\ReserveFileGroup;
use App\Models\ReserveGroup;
use App\Models\ReserveLog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class ReservesController extends Controller
{

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_enabled_reserves_from_server',
        'is_enabled_reserves_from_file',
        'max_number_format_reserve'
    ];

    /***
     * Список резервов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        // Добавить в избранное
        if($request->has('star_status'))
        {
            if($request->get('star_status') == 'disabled') {
                Reserve::find($request->get('id_reserve'))->update(['is_star' => 0]);
            }

            if($request->get('star_status') == 'enabled') {
                Reserve::find($request->get('id_reserve'))->update(['is_star' => 1]);
            }

            return redirect()->to(admin_base_path('/basic/reserves'));
        }

        // Распределить по группам
        if($request->has('transferred_to_group'))
        {
            // Получаем группу первую в списке категорий
            $default_group = ReserveGroup::where('status', '=', 1)->first();
            // Получаем резервы, которые находятся вне категорий
            if(!empty($default_group)) {
                Reserve::where(['id_group' => 0])->update([
                    'id_group' => $default_group->id
                ]);

                flash('Резервы успешно распределены по группам', ['alert alert-success']);
            } else {
                flash('Ошибка при распределении, создайте группу в резервах', ['alert alert-danger']);
            }

            return redirect()->back();
        }


        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->getMethod() == 'POST')
        {
            if($request->actions == 'save')
            {
                foreach ($request->reserve as $key => $item)
                {
                    $data = Reserve::find($key);

                    //Создаем событие
                    if($item < $data->summa)
                    {
                        $this->event([
                            'id_reserve'        =>  $key,
                            'id_currency'       =>  $data->id_currency,
                            'value_before'      =>  $data->summa,
                            'value_after'       =>  $item,
                            'text'              =>  'Резерв уменьшен с '.$data->summa.' на '.$item,
                        ], 'minus');

                        Reserve::find($key)->update([
                            'summa'         =>  $item,
                            'id_user'       =>  auth()->id()
                        ]);

                    } elseif($item > $data->summa)
                    {
                        $this->event([
                            'id_reserve'        =>  $key,
                            'id_currency'       =>  $data->id_currency,
                            'value_before'      =>  $data->summa,
                            'value_after'      =>  $item,
                            'text'              =>  'Резерв увеличен с '.$data->summa.' на '.$item
                        ], 'plus');

                        Reserve::find($key)->update([
                            'summa'         =>  $item,
                            'id_user'       => auth()->id()
                        ]);
                    }
                }
            }
            return redirect()->back();
        }

        // Получение общей суммы резервов в USD
        $reserves = Reserve::whereHas('currency', function($query) {
            $query->where('status', '=', 0);
        })->where('id_main', '=', 0)->orderBy('sorting')->get();

        $convert_reserve = \Cache::remember('total_reserves', Carbon::now()->addMinutes(20), function() use($reserves) {
            return $reserves->map(function($item) {
                try {
                    return (float)convert_to_usd($item->currency->code_currency->name, $item->summa);
                }catch (\Exception $exception) {
                    return 0;
                }
            })->sum();
        });

        // Группа резервов
        $groups = ReserveGroup::with(['reserve' => function($q){
            $q->where('id_main', '=', 0)->orderBy('sorting');
        }])->where('status', '=', 1)->get();


        // Получаем резервы, которые находятся вне категорий
        $default_reserves = Reserve::where(['id_group' => 0])->get();


        return view('admin.basic.reserves.index', [
            'groups' => $groups,
            'default_reserves' => $default_reserves,
            'categories' => $groups->pluck('name', 'id'),
            'convert_reserve' => $convert_reserve
        ]);
    }

    public function show(Request $request)
    {
        $reserve = Reserve::find($request->id);
        if($reserve->main_reserves->count() > 0) {
            $items = ReserveLog::whereIn('id_currency', [$reserve->id_currency, $reserve->main_reserves->implode('id_currency', ',')])
                ->orderBy('id_task')->paginate(20);
        } else {
            $items = ReserveLog::where('id_currency', $reserve->id_currency)->orderByDesc('created_at')->paginate(20);
        }

        return view('admin.basic.reserves.show', [
            'id'        =>  $request->id,
            'items'      =>  $items,
            'reserv'    =>  $reserve
        ]);
    }

    /***
     * Создание нового резерва
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            $find = Reserve::where('id_currency', $request->id_currency);
            if($find->exists()) {
                $old_price = $find->first()->summa;
            } else {
                $old_price = 0;
            }

            //Если найден данный резерв то обновляем сумму
            if($find->count() > 0) {
                $price = ($find->first()->summa + $request->summa);
            }else {
                $price = $request->summa;
            }

            $last_id = Reserve::updateOrCreate([
                'id_currency'   =>  $request->id_currency,
            ],[
                'id_currency'   =>  $request->id_currency,
                'id_group'      =>  ($request->has('id_group') ? $request->get('id_group') : 0),
                'summa'         =>  $price,
                'id_user'       =>  auth()->id(),
            ]);

            //Добавлякм новый резерв в лог
//            ReserveLog::create([
//                'id_reserve'        => $last_id->id,
//                'id_currency'       => $request->id_currency,
//                'summa'             => $request->summa,
//                'black_amount'      => (isset($request->black_amount) ? $request->black_amount: 0),
//                'type'              => 'plus'
//            ]);

            //Добавляем в событие
            $this->event([
                'id_reserve'        =>  $last_id->id,
                'id_currency'       =>  $request->id_currency,
                'value_before'      =>  $old_price,
                'value_after'       =>  $price,
                'text'              =>  'Резерв увеличен с '.$old_price.' на '.$price,
                'comment'           =>  ($request->has('comment') ? $request->get('comment') : null)
            ], 'plus');

            return redirect(config('admin.directory').'/basic/reserves');
        }

        $groups =  ReserveGroup::where('status', '=', 1)->get();
        $currencies = Currency::active()->get();

        return view('admin.basic.reserves.create', [
            'groups' => $groups,
            'currencies' => $currencies
        ]);
    }

    /***
     * Уменьшить резерв
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function reduce(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            $find = Reserve::where('id_currency', $request->id_currency);
            $old_price = $find->first()->summa;

            //Если найден данный резерв то обновляем сумму
            if($find->count() > 0) {
                $price = ($find->first()->summa - $request->summa);
            }else {
                $price = $request->summa;
            }

            $last_id = Reserve::updateOrCreate([
                'id_currency'   =>  $request->id_currency,
            ],[
                'id_currency'   =>  $request->id_currency,
                'id_user'       =>  auth()->id(),
                'summa'         =>  $price
            ]);

            //Добавлякм новый резерв в лог
//            ReserveLog::create([
//                'id_reserve'        => $last_id->id,
//                'id_currency'       => $request->id_currency,
//                'summa'             => $request->summa,
//                'type'              => 'minus'
//            ]);

            $this->event([
                'id_reserve'        =>  $last_id->id,
                'id_currency'       =>  $request->id_currency,
                'value_before'      =>  $old_price,
                'value_after'       =>  $price,
                'text'              =>  'Резерв уменьшен с '.$old_price.' на '.$price,
                'comment'           =>  ($request->has('comment') ? $request->get('comment') : null)
            ], 'minus');

            return redirect(config('admin.directory').'/basic/reserves');
        }


        $popular_reserves = Reserve::where('is_star', '=', 1)->get();

        $currencies = Currency::active()->get();
        return view('admin.basic.reserves.reduce', compact('currencies', 'popular_reserves'));
    }

    /**
     * Изменить резерв
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function change(Request $request)
    {
        $item = Reserve::find($request->id);

        if($request->getMethod() == 'POST')
        {
//            if($request->has('id_main') and $item->main_reserves->count() > 0) {
//                flash('К выбранному резерву нельзя привязать другой резерв', ['alert alert-danger']);
//                return redirect()->back();
//            }

            //Создаем события для уменьшения резерва
            if($request->summa < $item->summa)
            {
                $this->event([
                    'id_reserve'        =>  $item->id,
                    'id_currency'       =>  $item->id_currency,
                    'value_before'      =>  $item->summa,
                    'value_after'       =>  $request->get('summa'),
                    'text'              =>  'Резерв уменьшен с '.$item->summa.' на '.$request->get('summa'),
                    'comment'           =>  ($request->has('comment') ? $request->get('comment') : null)
                ], 'minus');

            } elseif($request->summa > $item->summa)
            {
                // События для увеличения резерва
                $this->event([
                    'id_reserve'        =>  $item->id,
                    'id_currency'       =>  $item->id_currency,
                    'value_before'      =>  $item->summa,
                    'value_after'       =>  $request->get('summa'),
                    'text'              =>  'Резерв увеличен с '.$item->summa.' на '.$request->get('summa'),
                    'comment'           =>  ($request->has('comment') ? $request->get('comment') : null)
                ], 'plus');
            }

            $item->summa = $request->get('summa');

            // Нельзя привязать однин резерв к другой идентичной
            if($request->has('id_main') and $item->id == $request->get('id_main')) {
                $item->id_main = 0;
            } else {
                $item->id_main =  ($request->has('id_main') ? $request->get('id_main') : 0);
            }

            $item->id_file_reserve = ($request->has('id_file_reserve') ? $request->get('id_file_reserve') : 0);
            $item->id_server_reserve = ($request->has('id_server_reserve') ? $request->get('id_server_reserve') : null);
            $item->is_fixed_reserve = ($request->has('is_fixed_reserve') ? $request->get('is_fixed_reserve') : 0);
            $item->save();

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->to(admin_base_path('/basic/reserves'));
        }

        // Список валют
        $currencies = Currency::active()->where('id', '!=', $item->id)->get();
        $currencies_all = Currency::active()->get();
        $groups =  ReserveGroup::where('status', '=', 1)->get();
        $reserves = Reserve::where('status', '=', 0)->get();


        $filter = $request->all();
        $analytics = EventReserve::whereIdCurrency($item->id_currency)->orderBy('id', 'desc')->paginate();

        // Резерв из файла
        $reserve_files_groups = [];
        if(iEXSetting('is_enabled_reserves_from_file') == 1) {
            $reserve_files_groups = ReserveFileGroup::orderBy('id')->get();
        }

        $online_balance = [];
        if(iEXSetting('is_enabled_reserves_from_server') == 1)
        {
            $gateways = GatewayPayment::all();
            $online_balance = $gateways->reject(function($item) {
                return !isset($item->gateway);
            })->map(function ($item)
            {
                $balance = collect(json_decode(config('balances.api.' . $item->gateway->alias), true));
                return [
                    'id' => $item->id,
                    'alias' => $item->gateway->alias,
                    'name' => $item->name,
                    'balances' => $balance->map(function ($response) use ($item) {
                        return [
                            'id' => \Str::lower($item->gateway->alias . '_' . $item->id . '_' . $response),
                            'name' => $item->gateway->name . ' [' . $item->gateway->alias . '] (' . $item->name . ') - ' . $response
                        ];
                    })->pluck('name', 'id')->toArray()
                ];
            })->reject(function ($item) {
                return count($item['balances']) == 0;
            })->toArray();
        }

        return view('admin.basic.reserves.change', compact('item', 'currencies', 'groups', 'currencies_all', 'reserves', 'filter', 'analytics', 'reserve_files_groups','online_balance'));
    }

    /**
     * Удаление резерва
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $reserve = Reserve::find($request->id);
        if(isset($reserve->reserve_alerts))
            ReserveAlert::where('id_reserve', $reserve->id)->delete();
        if(isset($reserve->event_reserve))
            EventReserve::where('id_reserve', $reserve->id)->delete();
        $reserve->delete();

        return redirect()->back();
    }

    /**
     * Сортировка резервов
     */
    public function sortingReserves()
    {
        $reserves = Reserve::where('id_main', '=', 0)->orderBy('sorting')->get();
        return view('admin.basic.reserves.sorting', [
            'reserves' => $reserves
        ]);
    }

    /**
     * Обновление резервов с сервера платежной системы
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function money(Request $request)
    {
        $reserve = Reserve::findOrFail($request->id);

        if(isset($reserve->currency->auto_reserve->gateway)) {
            $money = PaymentFacade::reserves(
                $reserve->currency->auto_reserve->gateway->alias,
                $reserve->currency->code_currency->name
            );

            //Создаем событие
            if($money < $reserve->summa)
            {
                $this->event([
                    'id_reserve'        =>  $request->id,
                    'id_currency'       =>  $reserve->id_currency,
                    'value_before'      =>  $reserve->summa,
                    'value_after'       =>  $money,
                    'text'              =>  'Резерв уменьшен с '.$reserve->summa.' на '.$money,
                ], 'minus');

                Reserve::find($request->id)->update([
                    'summa'         =>  $money,
                    'id_user'       =>  auth()->id()
                ]);

            } elseif($money > $reserve->summa)
            {
                $this->event([
                    'id_reserve'        =>  $request->id,
                    'id_currency'       =>  $reserve->id_currency,
                    'value_before'      =>  $reserve->summa,
                    'value_after'       =>  $money,
                    'text'              =>  'Резерв увеличен с '.$reserve->summa.' на '.$money
                ], 'plus');

                Reserve::find($request->id)->update([
                    'summa'         =>  $money,
                    'id_user'       => auth()->id()
                ]);
            }
            $payment = sprintf('%s %s', $reserve->currency->payment->name, $reserve->currency->code_currency->name);
            flash("Резерв валюты {$payment} успешно обновлен", ['alert alert-success']);
        } else {
            $payment = sprintf('%s %s', $reserve->currency->payment->name, $reserve->currency->code_currency->name);
            flash("Резерв валюты {$payment} не обновлен", ['alert alert-danger']);
        }

        return redirect()->back();
    }

    /**
     * Добавляем действие с резервами в событие
     *
     * @param array $data
     * @param string $type
     */
    protected function event(array $data, string $type)
    {
        $event = EventReserve::create([
            'id_user'           =>  auth()->id(),
            'id_currency'       =>  $data['id_currency'],
            'id_reserve'        =>  $data['id_reserve'],
            'type'              =>  ($type == 'minus' ? 0 : 1),
            'text'              =>  $data['text'],
            'value_before'      =>  (float)$data['value_before'],
            'value_after'       =>  (float)$data['value_after'],
            'comment'           =>  (isset($data['comment']) ? $data['comment'] : null)
        ]);

        // Уведомлять о ручной корректировки резерва
        if(iEXSetting('is_notify_change_reserve')) {
            $delay = now()->addMinutes(20);
            dispatch(new NotifyChangeReserveJob($event))->delay($delay)->onQueue('low');
        }

    }
}
