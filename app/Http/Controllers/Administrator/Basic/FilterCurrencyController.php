<?php

namespace App\Http\Controllers\Administrator\Basic;

use App\Models\{Currency, FilterCurrency};
use App\Http\Controllers\Controller;
use Illuminate\Http\{RedirectResponse, Request};
use Illuminate\Support\Facades\Validator;

class FilterCurrencyController extends Controller
{
    /**
     * Список фильтров
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $filters = FilterCurrency::orderBy('sorting')->get();
        return view('admin.basic.filter_currency.index',compact('filters'));
    }

    /**
     * Форма добавление фильтров
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.basic.filter_currency.create');
    }

    /**
     * Обработка и добавление фильтра
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            $item = FilterCurrency::create($options);
            flash('Фильтр ' . $item->name . ' успешно добавлен', ['alert alert-success']);
        }
        return redirect()->back();
    }

    /**
     * Форма редактирования фильтров
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $filter = FilterCurrency::find($id);
        return view('admin.basic.filter_currency.edit', compact('filter'));
    }

    /**
     * Обработка и обновление фильтров
     *
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update($id, Request $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            $item_view = FilterCurrency::find($id);
            $item_view->update($options);

            flash('Фильтр '.$item_view->name.' успешно обновлен', ['alert alert-success']);
        }
        return redirect()->back();
    }

    /**
     * Удаление фильтра
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        $has = Currency::whereIdFilterCurrency($id);

        if($has->exists()) {
            flash('Выбранный фильтр удалить невозможно, К фильтру привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        FilterCurrency::findOrFail($id)->delete();
        flash('Фильтр успешно удален', ['alert alert-success']);

        return redirect()->back();
    }

    /**
     * Сортировка фильтров
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sorting()
    {
        $filters = FilterCurrency::orderBy('sorting')->get();
        return view('admin.basic.filter_currency.sorting', compact('filters'));
    }
}
