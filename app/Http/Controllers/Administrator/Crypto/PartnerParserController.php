<?php
namespace App\Http\Controllers\Administrator\Crypto;


use App\Http\Controllers\Controller;
use App\Models\CompetitorLink;
use App\Models\CompetitorRates;
use App\Models\CompetitorRatesLog;
use App\Models\FileParserGroup;
use App\Models\FileParserRates;
use App\Models\PartnerParserGroup;
use App\Models\PartnerParserRates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PartnerParserController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $links = PartnerParserGroup::with(['rates' => function($query) {
            $query->orderBy('last_updated_at', 'desc');
        }])->where('status', '=', 1)->orderBy('sorting')->get();


        return view('admin.crypto.partner_parser.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления курсов конкурентов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $links = FileParserGroup::where('status', '=', 1)->get();
        return view('admin.crypto.partner_parser.create', compact('links'));
    }

    /**
     * Обработка и добавление курсов конкурентов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_group' => ['required', 'exists:file_parser_groups,id']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $explode = explode('-', $request->get('name'));

            $rate = PartnerParserRates::create([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'exchange_in'       => $explode[0],
                'exchange_out'      => $explode[1],
                'id_group'          => $request->has('id_group') ? $request->get('id_group') : 0,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'     => $request->has('number_format') ? $request->get('number_format') : 0,
                'type'              => $request->has('type') ? $request->get('type') : 0,
            ]);

            flash("Парсер {$rate->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('file-parser.index');
        }

    }

    /**
     * Удаление курсов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $rate = PartnerParserRates::findOrFail($id);

        if($rate->direction_exchange->count() > 0) {
            flash('Выбранный курс удалить невозможно, К нему привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Курс {$rate->name} успешно удален", ['alert alert-success']);
        $rate->delete();

        return redirect()->route('partner-parser.index');
    }
}
