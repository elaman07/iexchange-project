<?php
namespace App\Http\Controllers\Administrator\Crypto;

use App\Models\GroupParserExchange;
use App\Http\Controllers\Controller;
use App\Models\ParserExchange;
use App\Models\ParserExchangeHttpLog;
use App\Models\ParserExchangeLog;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\ProxyManager\Entities\ProxyModel;
use Nwidart\Modules\Facades\Module;

class ParseController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'record_course_history',
        'crypto_parser_sorting',
        'is_crypto_parser_test',
        'max_number_format_parser',
        'is_crypto_partner_parser_api',
        'is_record_parser_http_log'
    ];

    protected array $allowFilteredCron = [
        'cron_format_update_rates',
        'cron_interval_minutes_update_rates',
        'cron_interval_second_update_rates'
    ];

    public function index(Request $request)
    {
        // Отключаем парсинг
        if($request->has('actions'))
        {
            if(config('admin.is_demo_mode') == true) {
                flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
                return redirect()->back();
            }

            if($request->get('actions') == 'disabled') {
                $item = GroupParserExchange::find($request->get('id'));
                $item->update(['status' => 0]);


                flash(sprintf('Источник %s успешно отключен', $item->name), 'alert alert-success');
                return redirect()->to(admin_base_path('/crypto/parser'));
            }

            if($request->get('actions') == 'enabled') {
                $item = GroupParserExchange::find($request->get('id'));
                $item->update(['status' => 1]);
                flash(sprintf('Источник %s успешно активирован', $item->name), 'alert alert-success');
                return redirect()->to(admin_base_path('/crypto/parser'));
            }
        }

        if($request->has('import_rates'))
        {
            if(config('admin.is_demo_mode') == true) {
                flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
                return redirect()->back();
            }

            flash('Курсы успешно загружены', ['alert alert-success']);
            return  redirect()->to(
                admin_base_path('/crypto/parser')
            );
        }

//        // Удаление курсов курсов
//        if($request->has('delete_rates'))
//        {
//            if(config('admin.is_demo_mode') == true) {
//                flash('Данная функция недоступа в демо версии', ['alert alert-danger']);
//                return redirect()->back();
//            }
//
//            $delete_id = $request->get('delete_rates');
//
//            $rates = ParserExchange::whereIdGroup($delete_id)->cursor();
//
//            foreach ($rates as $rate) {
//                if($rate->direction_exchange->count() == 0) {
//                    $rate->selected_courses_many()->delete();
//                    $rate->parser_exchange_log()->delete();
//                    $rate->delete();
//                }
//            }
//
//            flash('Курсы успешно удалены', ['alert alert-success']);
//            return redirect()->back();
//        }



        if($request->section == 'action')
        {
            if(config('admin.is_demo_mode') == true) {
                flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
                return redirect()->back();
            }

            $update = ParserExchange::find($request->id);
            $update->status = $request->status;
            $update->save();

            return redirect(admin_base_path('/crypto/parser'));
        }


        $group_parser_v2_count = GroupParserExchange::with('parser_exchange')
            ->orderBy('status', 'desc')
            ->count();


        $group_parser_v2_on = GroupParserExchange::with('parser_exchange')
            ->where('status', 1)
            ->orderBy('status', 'desc')
            ->get();

        $group_parser_v2_off = GroupParserExchange::with('parser_exchange')
            ->where('status', 0)
            ->orderBy('status', 'desc')
            ->get();

        $parser = ParserExchange::orderByDesc('id')->get();

        return view('admin.crypto.parser.index', [
            'group_parser_v2_on'        =>  $group_parser_v2_on,
            'group_parser_v2_off'       =>  $group_parser_v2_off,
            'group_parser_v2_count'           =>  $group_parser_v2_count,
            'all_groups'                =>  GroupParserExchange::orderBy('sorting')->pluck('name', 'id')
        ]);
    }

    public function show(Request $request, int $id)
    {
        // Отключаем парсинг
        if($request->has('actions'))
        {
            if(config('admin.is_demo_mode') == true) {
                flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
                return redirect()->back();
            }


            if($request->get('actions') == 'disabled') {
                $item = GroupParserExchange::find($request->get('id'));
                $item->update(['status' => 0]);


                flash(sprintf('Источник %s успешно отключен', $item->name), 'alert alert-success');
                return redirect()->to(admin_base_path('/crypto/parser/'.$item->id));
            }

            if($request->get('actions') == 'enabled') {
                $item = GroupParserExchange::find($request->get('id'));
                $item->update(['status' => 1]);
                flash(sprintf('Источник %s успешно активирован', $item->name), 'alert alert-success');
                return redirect()->to(admin_base_path('/crypto/parser/'.$item->id));
            }


            // Загрузить курсы
            if($request->get('actions') == 'import_trashed')
            {
                if(config('admin.is_demo_mode') == true) {
                    flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
                    return redirect()->back();
                }

                $item = GroupParserExchange::find($request->get('id'));

                foreach ($item->parserExchange as $parser)
                {
                    if($parser->direction_exchange->count() > 0) {
                        continue;
                    }
                    $parser->selected_courses_many()->delete();
                    $parser->parser_exchange_log()->delete();
                    $parser->delete();
                }

                $item->update([
                    'is_import_rates'   => 0
                ]);

                flash(sprintf('Курсы из источника %s удалены', $item->name), 'alert alert-success');
                return redirect()->to(admin_base_path('/crypto/parser/'.$item->id));
            }


            if($request->get('actions') == 'import')
            {
                if(config('admin.is_demo_mode') == true) {
                    flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
                    return redirect()->back();
                }


                $item = GroupParserExchange::find($request->get('id'));

                if(file_exists( storage_path('templates/rates/'. $item->alias.'.json'))) {
                    $get_file = json_decode(
                        file_get_contents(
                            storage_path('templates/rates/'. $item->alias.'.json')
                        ), true
                    );
                } else {
                    flash('Файл для импорта курсов не найден', ['alert alert-danger']);
                    return redirect()->to(admin_base_path('/crypto/parser/'.$item->id));
                }



                foreach ($get_file as $import)
                {
                    $get_name = explode(' - ', $import['name']);

                    $parser_update = ParserExchange::updateOrCreate([
                        'name'  => $import['name'],
                        'id_group' => $item->id,
                        'type'  => $import['type'],
                        'type_price' => $import['type_amount'] ?? null
                    ], [
                        'name'  => $import['name'],
                        'id_group' => $item->id,
                        'type'  => $import['type'],
                        'code_in' => trim($get_name[0]),
                        'code_out' => trim($get_name[1]),
                        'value' => 1,
                        'value_default' => 1,
                        'number_format' => $import['number_format'],
                        'summa_default' => $import['amount'],
                        'summa' => $import['amount'],
                        'type_price' => $import['type_amount'] ?? null
                    ]);

                    $code_view = '['.\Str::lower($parser_update->group_parse_exchange->alias).'_'.\Str::lower($parser_update->code_in.'-'.$parser_update->code_out).']';
                    if(!empty($parser_update->type_price)) {
                        $code_view = '['.\Str::lower($parser_update->group_parse_exchange->alias).'_'.\Str::lower($parser_update->code_in.'-'.$parser_update->code_out).'_'.\Str::lower($parser_update->type_price).']';
                    }

                    $parser_update->update([
                        'code' => $code_view
                    ]);
                }

                $item->update([
                    'is_import_rates'   => 1,
                    'last_imported_at'  => Carbon::now()->format('c'),
                ]);


                flash(sprintf('Курсы из истоничка %s выгружены', $item->name), 'alert alert-success');
                return redirect()->to(admin_base_path('/crypto/parser/'.$item->id));
            }

        }


        $group_parser = GroupParserExchange::find($id);

        $parser_exchange__count = ParserExchange::where('id_group', $group_parser->id)->count();
        $parser_exchange = ParserExchange::filter($request->all())->where('id_group', $group_parser->id)->orderByRaw("status DESC, name ASC")->paginate(150);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);

            return \redirect()->to(admin_base_path(
                '/crypto/parser/'.$group_parser->id.'?'.http_build_query($conditions)
            ));
        }

        return view('admin.crypto.parser.show', [
            'parser_exchange'   => $parser_exchange,
            'parser_exchange__count' => $parser_exchange__count,
            'group_parser' => $group_parser,
            'filter'     => $request->all()
        ]);
    }

    /**
     * Форма, добавления пары
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $id_group = 0;
        if($request->has('id_group')) {
            $id_group = $request->get('id_group');
        }

        $group_parser = GroupParserExchange::with('parser_exchange')->where('status','=',1)->orderBy('sorting')->get();
        return view('admin.crypto.parser.create', compact('group_parser', 'id_group'));
    }

    /**
     * Обработка и добавление новой пары
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
            return redirect()->back();
        }


        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('action') and $request->action == 'settings_cron')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFilteredCron as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('form') and $request->get('form') == 'update')
        {
            // Обновление данные прямо из таблицы валюты
            if($request->get('actions') == 'save')
            {
                foreach ($request->get('item_id') as $key => $item)
                {
                    $direction = ParserExchange::find($key);
                    $direction->update([
                        'type' => ($request->type[$key] ?? 0),
                        'status'  =>   ($request->status[$key] ?? 0),
                    ]);
                }


                flash('Данные успешно обновлены', ['alert alert-success']);
                return redirect()->to(
                    admin_base_path('/crypto/parser/'. $request->get('id_group'))
                );
            }


            if(isset($request->check) and count($request->check) > 0)
            {
                foreach ($request->check as $item)
                {
                    $direction = ParserExchange::find($item);

                    if($request->actions == 'activation') {
                        $direction->update([
                            'status' => 1
                        ]);

                    }elseif($request->actions == 'deactivation') {
                        $direction->update([
                            'status' => 0
                        ]);
                    }elseif($request->actions == 'delete') {
                        $direction->delete();
                    }
                }
            }

            return redirect()->to(
                admin_base_path('/crypto/parser')
            );
        }

        $validator = Validator::make($request->all(), [
            'code_in' => 'required',
            'code_out'  => 'required',
            'id_group' => 'required',
            'number_format' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $filter_name = sprintf('%s - %s', $request->get('code_in'), $request->get('code_out'));

            $main_parser = ParserExchange::create([
                'name' => $filter_name,
                'code_in' => $request->get('code_in'),
                'code_out' => $request->get('code_out'),
                'id_group' => $request->get('id_group'),
                'number_format' => $request->get('number_format'),
                'type'  =>  $request->has('type') ? $request->get('type') : 0,
                'status'    => $request->get('status') ? $request->get('status') : 0,
                'value' => 1
            ]);

            $main_parser->update([
                'code' => '['.\Str::lower($main_parser->group_parse_exchange->alias).'_'.\Str::lower($main_parser->code_in.'-'.$main_parser->code_out).']'
            ]);

            if($request->has('is_allow_to_backspace') and $request->get('is_allow_to_backspace') == 1)
            {
                $filter_name = sprintf('%s - %s', $request->get('code_out'), $request->get('code_in'));
                $type_parser = ($main_parser->type == 0 ? 1 : 0);
                $parser_backspace = ParserExchange::create([
                    'name' => $filter_name,
                    'code_in' => $request->get('code_out'),
                    'code_out' => $request->get('code_in'),
                    'id_group'  =>  $main_parser->id_group,
                    'number_format' => $main_parser->number_format,
                    'type'  =>  $type_parser,
                    'status'    => $main_parser->status,
                    'value' => 1
                ]);

                $parser_backspace->update([
                    'code' => '['.\Str::lower($parser_backspace->group_parse_exchange->alias).'_'.\Str::lower($parser_backspace->code_in.'-'.$parser_backspace->code_out).']'
                ]);
            }


            flash(sprintf('Пара %s успешно добавлена', $request->get('name')), ['alert alert-success']);
            return redirect()->to(
                admin_base_path('/crypto/parser/create')
            );
        }
    }

    /**
     * Форма, редактирования пары
    */
    public function edit(int $id)
    {
        $find = ParserExchange::find($id);
        $group_parser = GroupParserExchange::with('parser_exchange')->where('status','=',1)->orderBy('sorting')->get();

        return view('admin.crypto.parser.edit', [
            'item'  =>  $find,
            'group_parser'      =>  $group_parser
        ]);
    }

    /**
     * Обработка и обновление пары
     *
     * @param int $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request  $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'code_in' => 'required',
            'code_out'  => 'required',
            'id_group' => 'required',
            'number_format' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $item = ParserExchange::find($id);

            $filter_name = sprintf('%s - %s', $request->get('code_in'), $request->get('code_out'));


            $item->update([
                'name' => $filter_name,
                'code_in' => $request->get('code_in'),
                'code_out' => $request->get('code_out'),
                'id_group' => $request->get('id_group'),
                'number_format' => $request->get('number_format'),
                'type'  =>  $request->has('type') ? $request->get('type') : 0,
                'status'    => $request->get('status') ? $request->get('status') : 0,
                'value' => 1
            ]);

            $item->update([
                'code' => '['.\Str::lower($item->group_parse_exchange->alias).'_'.\Str::lower($item->code_in.'-'.$item->code_out).']'
            ]);

            flash(sprintf('Пара %s успешно обновлена', $item->name), ['alert alert-success']);
            return redirect()->to(
                admin_base_path('/crypto/parser/'.$id.'/edit')
            );
        }
    }


    /**
     * Получаем список всех курсов из всех источников
    */
    public function items(Request $request){
        $rates = ParserExchange::filter($request->all())->orderBy('id')->paginate(20);
        $groups = GroupParserExchange::all();
        return view('admin.crypto.parser.all', [
            'rates' => $rates,
            'filter' => $request->all(),
            'groups' => $groups
        ]);
    }


    public function logs(Request $request)
    {
        if($request->has('clear_logs')) {
            DB::table('parser_exchange_http_logs')->delete();
            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return redirect()->back();
        }

        $logs = ParserExchangeHttpLog::paginate(20);
        return view('admin.crypto.parser.logs', compact('logs'));
    }

    /**
     * Удаление курсов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }


        $item = ParserExchange::find($id);

        if($item->direction_exchange->count() > 0) {
            flash("Пару {$item->name} удалить невозможно, к ней привязаны направления", ['alert alert-danger']);
            return redirect()->to(admin_base_path('/crypto/parser/'));
        }

        flash('Пара '.$item->name.' успешно удалена', ['alert alert-success']);
        $item->selected_courses_many()->delete();
        $item->parser_exchange_log()->delete();
        $item->delete();

        return redirect()->to(
            admin_base_path('/crypto/parser/'.$item->id_group)
        );
    }

    /**
     * Сортировка источников
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting()
    {
        $groups = GroupParserExchange::where('status', '=', 1)->orderBy('sorting')->get();
        return view('admin.crypto.parser.sorting', [
            'groups'    =>  $groups
        ]);
    }

    /**
     * История курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function history(Request $request)
    {
        // Очищаем историю курсов
        if($request->has('clear'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }


            ParserExchangeLog::query()->delete();
            ParserExchangeLog::truncate();

            flash('История курсов успешно очищена', ['alert alert-success']);
            return redirect()->back();
        }


        $logs = ParserExchangeLog::orderByDesc('id')->paginate(20);
        return view('admin.crypto.parser.history', [
            'logs'  =>  $logs,
            'item' => null
        ]);
    }

    /**
     * История курсов для определенного направления
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyPair(int $id)
    {
        $logs = ParserExchangeLog::where('id_parser_exchange', '=', $id)->orderByDesc('id')->paginate(20);
        $item = ParserExchange::find($id);

        return view('admin.crypto.parser.history', [
            'logs'  =>  $logs,
            'item' => $item
        ]);
    }
}
