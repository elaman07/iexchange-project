<?php
namespace App\Http\Controllers\Administrator\Crypto;


use App\Http\Controllers\Controller;
use App\Models\CompetitorLink;
use App\Models\CompetitorRates;
use App\Models\CompetitorRatesLog;
use App\Models\FileParserGroup;
use App\Models\FileParserRates;
use App\Models\ParserFormulaLog;
use App\Models\ParserFormulaRates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ParserFormulaController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'record_course_formula_history'
    ];


    protected array $allowFilteredPage = [
        'admin_parser_formula_paginate',
        'admin_parser_formula_hidden_columns'
    ];

    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $parsers = ParserFormulaRates::filter($request->all())->paginate((int)iEXSetting('admin_parser_formula_paginate', 20));
        $admin_parser_formula_hidden_columns = explode(',', iEXSetting('admin_parser_formula_hidden_columns'));

        return view('admin.crypto.parser_formula.index', [
            'parsers' =>  $parsers,
            'admin_parser_formula_hidden_columns' => $admin_parser_formula_hidden_columns,
            'filter' => $request->all(),
            'status' => [
                0 => 'Не активен',
                1 => 'Активен'
            ]
        ]);
    }

    /**
     * Форма добавления курсов конкурентов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.crypto.parser_formula.create');
    }

    /**
     * Обработка и добавление курсов конкурентов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $admin_parser_formula_hidden_columns = explode(',', iEXSetting('admin_parser_formula_hidden_columns'));

        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }

        if($request->get('actions') == 'save')
        {
            if(config('admin.is_demo_mode')) {
                flash('Данная функция недоступна в демо версии', ['alert alert-danger']);
                return redirect()->back();
            }

            foreach ($request->item_id as $value)
            {
                $find = ParserFormulaRates::find($value);
                $update_data = [];

                if(in_array('formula', $admin_parser_formula_hidden_columns))
                {
                    $update_data = [
                        'name' =>   ($request->formula[$value] ?? 0),
                    ];
                }

                $find->update($update_data);
            }

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();

        }

        if($request->has('action') and $request->get('action') == 'settings_columns')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFilteredPage as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $options = [
                'title'              => $request->has('title') ? $request->get('title') : null,
                'name'              => $request->has('name') ? $request->get('name') : null,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'     => $request->has('number_format') ? $request->get('number_format') : 0,
            ];

            $rate = ParserFormulaRates::create($options);

            flash("Формула {$rate->name} успешно добавлена", ['alert alert-success']);
            return redirect()->route('parser-formula.index');
        }

    }

    /**
     * Форма редактирования курсов конкурентов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ParserFormulaRates::findOrFail($id);

        return view('admin.crypto.parser_formula.edit',compact('item'));
    }

    /**
     * Обработка и обновление курсов конкурентов
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $rates = ParserFormulaRates::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $options = [
                'title'              => $request->has('title') ? $request->get('title') : null,
                'name'              => $request->has('name') ? $request->get('name') : null,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'     => $request->has('number_format') ? $request->get('number_format') : 0,
            ];

            $rates->update($options);

            flash("Парсер {$rates->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('parser-formula.index');
        }
    }

    /**
     * Удаление курсов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }
        $rate = ParserFormulaRates::findOrFail($id);

        if($rate->direction_exchange->count() > 0) {
            flash('Выбранный курс удалить невозможно, К нему привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Курс {$rate->name} успешно удален", ['alert alert-success']);
        $rate->delete();

        return redirect()->route('parser-formula.index');
    }

    /**
     * История курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function history(Request $request)
    {
        // Очищаем историю курсов
        if($request->has('clear'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            ParserFormulaLog::query()->delete();
            ParserFormulaLog::truncate();

            flash('История курсов успешно очищена', ['alert alert-success']);
            return redirect()->back();
        }


        $logs = ParserFormulaLog::orderByDesc('id')->paginate(20);
        return view('admin.crypto.parser_formula.history', [
            'logs'  =>  $logs,
            'item' => null
        ]);
    }
}
