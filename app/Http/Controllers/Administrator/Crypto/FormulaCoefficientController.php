<?php

namespace App\Http\Controllers\Administrator\Crypto;

use App\Http\Controllers\Controller;
use App\Models\ParserFormulaCoefficient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class FormulaCoefficientController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $parsers = ParserFormulaCoefficient::all();
        return view('admin.crypto.formula_coefficient.index', [
            'parsers' =>  $parsers
        ]);
    }

    /**
     * Форма добавления курсов конкурентов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.crypto.formula_coefficient.create');
    }

    /**
     * Обработка и добавление курсов конкурентов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'summa' => ['required', 'numeric']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $custom_name = '[index_'.Str::lower(Str::slug($request->get('name'))).']';

            $rate = ParserFormulaCoefficient::create([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'alias'             =>  $custom_name,
                'summa'             => $request->has('summa') ? $request->get('summa') : 0,
            ]);

            flash("Коэффициент {$rate->name} успешно добавлена", ['alert alert-success']);
            return redirect()->route('formula-coefficient.index');
        }

    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ParserFormulaCoefficient::findOrFail($id);
        return view('admin.crypto.formula_coefficient.edit',compact('item'));
    }

    /**
     * Обработка и обновление
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $rates = ParserFormulaCoefficient::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'summa' => ['required', 'numeric']
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $rates->update([
                'summa'     => $request->has('summa') ? $request->get('summa') : 0,
            ]);

            flash("Коэффициент {$rates->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('formula-coefficient.index');
        }
    }

    /**
     * Удаление курсов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $rate = ParserFormulaCoefficient::findOrFail($id);

        flash("Коэффициент {$rate->name} успешно удален", ['alert alert-success']);
        $rate->delete();

        return redirect()->route('formula-coefficient.index');
    }
}
