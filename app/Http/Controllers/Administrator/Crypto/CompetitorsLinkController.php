<?php
namespace App\Http\Controllers\Administrator\Crypto;


use App\Http\Controllers\Controller;
use App\Models\CompetitorLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompetitorsLinkController extends Controller
{
    /**
     * Главная страница
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Создаем дубликат
        if($request->has('duplicate') and $request->get('duplicate') == true)
        {
            $item = CompetitorLink::find($request->get('id'));
            $newParser = $item->replicate();
            $newParser->save();

            flash('Дубликат '.$item->name.' успешно создан', ['alert alert-success']);
            return redirect()->back();
        }

        $links = CompetitorLink::orderBy('sorting')->paginate(20);
        return view('admin.crypto.competitors.links.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления ссылок конкуректов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.crypto.competitors.links.create');
    }

    /**
     * Обработка и добавление ссылок конкуректов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->get('link') as $key => $value) {
                CompetitorLink::find($key)->update([
                    'link'          =>  $value,
                    'status'        =>   (isset($request->status[$key]) ? $request->status[$key] : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            try {
                $check_file = json_encode(simplexml_load_file($request->link));
            }catch (\Exception $exception) {
                flash('Файл курсов недоступен для парсинга', ['alert alert-danger']);
                return redirect()->back();
            }

            $link = CompetitorLink::create([
                'name'      => $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Ссылка конкурента {$link->name} успешно добавлена", ['alert alert-success']);
            return redirect()->route('competitors-link.index');
        }
    }

    /**
     * Форма редактирования ссылок конкуректов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = CompetitorLink::findOrFail($id);
        return view('admin.crypto.competitors.links.edit',compact('item'));
    }

    /**
     * Обработка и обновления ссылок конкуректов
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $link = CompetitorLink::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            try {
                $check_file = json_encode(simplexml_load_file($request->link));
            }catch (\Exception $exception) {
                flash('Файл курсов недоступен для парсинга', ['alert alert-danger']);
                return redirect()->back();
            }

            $link->update([
                'name'      =>  $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Ссылка конкурента {$link->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('competitors-link.index');
        }
    }

    /**
     * Удаление ссылок конкуректов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $link = CompetitorLink::findOrFail($id);

        if($link->rates->count() > 0) {
            flash('Выбранного конкурента удалить невозможно, К нему привязаны курсы', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Конкурент {$link->name} успешно удален", ['alert alert-success']);
        $link->delete();

        return redirect()->route('competitors-link.index');
    }

    /**
     * Сортировка источников
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting()
    {
        $links = CompetitorLink::where('status', '=', 1)->orderBy('sorting')->get();
        return view('admin.crypto.competitors.links.sorting', [
            'links'    =>  $links
        ]);
    }
}