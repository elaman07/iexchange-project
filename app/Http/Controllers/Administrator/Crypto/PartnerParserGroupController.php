<?php
namespace App\Http\Controllers\Administrator\Crypto;


use App\Http\Controllers\Controller;
use App\Models\PartnerParserGroup;
use App\Models\PartnerParserRates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

/**
 * @deprecated
*/
class PartnerParserGroupController extends Controller
{

    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'import_rates')
        {
            $id = $request->get('id');
            $find = PartnerParserGroup::find($id);
            $data_url = config('crypto.partner_parsers')[$find->name];


            $response  = Http::get($data_url.'/api/v1/rates_partner')->json();

            if(count($response) > 0)
            {
                foreach ($response as $item)
                {
                    PartnerParserRates::updateOrCreate([
                        'name' => $item['name'],
                        'partner_id' => $id,
                        'id_group' => $item['group']['id'],
                    ], [
                        'name' => $item['name'],
                        'partner_id' => $id,
                        'id_group' => $item['group']['id'],
                        'group_name' => $item['group']['name'],
                        'rate' => $item['value'],
                        'partner_type' => 'default',
                        'number_format' => $item['number_format'],
                        'last_updated_at'   => $item['last_updated_at']
                    ]);
                }

                flash('Данные обновлены', ['alert alert-success']);
            } else {
                flash('Ошибка парсинга курсов, владелец обменника отключил API', ['alert alert-danger']);
            }
            return redirect()->to(
                admin_base_path('/crypto/partner-parser-group')
            );
        }

        if($request->has('actions') and $request->get('actions') == 'trash_rates')
        {
            $id = $request->get('id');
            $find = PartnerParserGroup::find($id);
            $data_url = config('crypto.partner_parsers')[$find->name];

            $rate = PartnerParserRates::withCount('direction_exchange')->where('partner_id', $id)->having('direction_exchange_count', '=', 0)->get();
            foreach ($rate as $item) {
                $item->delete();
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->to(
                admin_base_path('/crypto/partner-parser-group')
            );
        }

        // Создаем дубликат
        if($request->has('duplicate') and $request->get('duplicate') == true)
        {
            $item = PartnerParserGroup::find($request->get('id'));
            $newParser = $item->replicate();
            $newParser->save();

            flash('Дубликат '.$item->name.' успешно создан', ['alert alert-success']);
            return redirect()->back();
        }

        $links = PartnerParserGroup::orderBy('sorting')->paginate(20);
        return view('admin.crypto.partner_parser.groups.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления ссылок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.crypto.partner_parser.groups.create');
    }

    /**
     * Обработка и добавление ссылок
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->get('link') as $key => $value)
            {
                PartnerParserGroup::find($key)->update([
                    'link'          =>  $value,
                    'status'        =>   ($request->status[$key] ?? 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {


            $data_url = $this->partners[$request->get('name')];



            $link = PartnerParserGroup::create([
                'name'      => $request->has('name') ? $request->get('name') : null,
                'link'      => $data_url,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Партнер {$link->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('partner-parser-group.index');
        }
    }

    /**
     * Форма редактирования ссылок
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = PartnerParserGroup::findOrFail($id);
        return view('admin.crypto.partner_parser.groups.edit',compact('item'));
    }

    /**
     * Обработка и обновления ссылок конкуректов
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $link = PartnerParserGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link->update([
                'name'      =>  $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Файл {$link->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('partner-parser-group.index');
        }
    }

    /**
     * Удаление ссылок
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $link = PartnerParserGroup::findOrFail($id);

        if($link->rates->count() > 0) {
            flash('Выбранного партнера удалить невозможно, К нему привязаны курсы', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Партнер {$link->name} успешно удален", ['alert alert-success']);
        $link->delete();

        return redirect()->route('partner-parser-group.index');
    }

    /**
     * Сортировка
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting()
    {
        $links = PartnerParserGroup::where('status', '=', 1)->orderBy('sorting')->get();
        return view('admin.crypto.partner_parser.groups.sorting', [
            'links'    =>  $links
        ]);
    }
}
