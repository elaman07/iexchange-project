<?php
namespace App\Http\Controllers\Administrator\Crypto;


use App\Http\Controllers\Controller;
use App\Models\FileParserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FileParserGroupController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Создаем дубликат
        if($request->has('duplicate') and $request->get('duplicate') == true)
        {
            $item = FileParserGroup::find($request->get('id'));
            $newParser = $item->replicate();
            $newParser->save();

            flash('Дубликат '.$item->name.' успешно создан', ['alert alert-success']);
            return redirect()->back();
        }

        $links = FileParserGroup::orderBy('sorting')->paginate(20);
        return view('admin.crypto.file_parser.groups.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления ссылок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.crypto.file_parser.groups.create');
    }

    /**
     * Обработка и добавление ссылок
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->get('link') as $key => $value) {
                FileParserGroup::find($key)->update([
                    'link'          =>  $value,
                    'status'        =>   (isset($request->status[$key]) ? $request->status[$key] : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link = FileParserGroup::create([
                'name'      => $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Файл {$link->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('file-parser-group.index');
        }
    }

    /**
     * Форма редактирования ссылок
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = FileParserGroup::findOrFail($id);
        return view('admin.crypto.file_parser.groups.edit',compact('item'));
    }

    /**
     * Обработка и обновления ссылок конкуректов
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $link = FileParserGroup::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'link' => ['required', 'min:3']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $link->update([
                'name'      =>  $request->has('name') ? $request->get('name') : null,
                'link'      => $request->has('link') ? $request->get('link') : null,
                'status'    => $request->has('status') ? $request->get('status') : 0
            ]);

            flash("Файл {$link->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('file-parser-group.index');
        }
    }

    /**
     * Удаление ссылок
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $link = FileParserGroup::findOrFail($id);

        if($link->rates->count() > 0) {
            flash('Выбранный файл удалить невозможно, К нему привязаны курсы', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Файл {$link->name} успешно удален", ['alert alert-success']);
        $link->delete();

        return redirect()->route('file-parser-group.index');
    }

    /**
     * Сортировка
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sorting()
    {
        $links = FileParserGroup::where('status', '=', 1)->orderBy('sorting')->get();
        return view('admin.crypto.file_parser.groups.sorting', [
            'links'    =>  $links
        ]);
    }
}