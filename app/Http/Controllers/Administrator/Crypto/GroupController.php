<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 21.09.2017
 * Time: 0:17
 */

namespace App\Http\Controllers\Administrator\Crypto;

use App\Models\GroupParserExchange;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        if($request->getMethod() == 'POST' and $request->form == 'home')
        {
            foreach ($request->check as $item)
            {
                if($request->actions == 'activation')
                {
                    $direction = GroupParserExchange::find($item);
                    $direction->update([
                        'status' => 1
                    ]);

                }elseif($request->actions == 'deactivation')
                {
                    $direction = GroupParserExchange::find($item);
                    $direction->update([
                        'status' => 0
                    ]);
                }
            }

            return redirect(config('admin.directory').'/crypto/group');

        }elseif($request->getMethod() == 'POST' and $request->form == 'add')
        {
            GroupParserExchange::create($request->all());
            return redirect(config('admin.directory').'/crypto/group');

        }elseif($request->getMethod() == 'POST' and $request->form == 'change')
        {
            $group = GroupParserExchange::find($request->id);
            $group->name = $request->name;
            $group->status = $request->status;
            $group->type_amount = $request->type_amount;
            $group->save();


            return redirect(config('admin.directory').'/crypto/group');
        }


        return view('admin.crypto.group.index',[
            'group' =>  GroupParserExchange::all()
        ]);
    }
}