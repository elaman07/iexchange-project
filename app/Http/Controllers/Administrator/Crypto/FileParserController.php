<?php
namespace App\Http\Controllers\Administrator\Crypto;


use App\Http\Controllers\Controller;
use App\Models\CompetitorLink;
use App\Models\CompetitorRates;
use App\Models\CompetitorRatesLog;
use App\Models\FileParserGroup;
use App\Models\FileParserRates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class FileParserController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        // Создаем дубликат
        if($request->has('duplicate'))
        {
            $item = FileParserRates::find($request->get('id'));
            $newParser = $item->replicate();
            $newParser->save();

            flash('Дубликат '.$item->name.' успешно создан', ['alert alert-success']);
            return redirect()->back();
        }


        $links = FileParserGroup::where('status', '=', 1)->orderBy('sorting')->get();

        return view('admin.crypto.file_parser.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления курсов конкурентов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $links = FileParserGroup::where('status', '=', 1)->get();
        return view('admin.crypto.file_parser.create', compact('links'));
    }

    /**
     * Обработка и добавление курсов конкурентов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_group' => ['required', 'exists:file_parser_groups,id']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $explode = explode('-', $request->get('name'));

            $rate = FileParserRates::create([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'exchange_in'       => $explode[0],
                'exchange_out'      => $explode[1],
                'id_group'          => $request->has('id_group') ? $request->get('id_group') : 0,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'     => $request->has('number_format') ? $request->get('number_format') : 0,
                'type'              => $request->has('type') ? $request->get('type') : 0,
            ]);

            $rate->update([
                'code' => '[fileparser_'.\Str::lower(Str::studly($rate->file_parser_group->name)).'_'.\Str::lower($rate->exchange_in.'-'.$rate->exchange_out).']'
            ]);

            flash("Парсер {$rate->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('file-parser.index');
        }

    }

    /**
     * Форма редактирования курсов конкурентов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = FileParserRates::findOrFail($id);
        $links = FileParserGroup::where('status', '=', 1)->get();

        return view('admin.crypto.file_parser.edit',compact('item', 'links'));
    }

    /**
     * Обработка и обновление курсов конкурентов
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $rates = FileParserRates::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_group' => ['required', 'exists:file_parser_groups,id']
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {
            $explode = explode('-', $request->get('name'));

            $rates->update([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'exchange_in'       => $explode[0],
                'exchange_out'      => $explode[1],
                'id_group'          => $request->has('id_group') ? $request->get('id_group') : 0,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'number_format'          => $request->has('number_format') ? $request->get('number_format') : 0,
                'type'            => $request->has('type') ? $request->get('type') : 0,
            ]);

            $rates->update([
                'code' => '[fileparser_'.\Str::lower(Str::studly($rates->file_parser_group->name)).'_'.\Str::lower($rates->exchange_in.'-'.$rates->exchange_out).']'
            ]);

            flash("Парсер {$rates->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('file-parser.index');
        }
    }

    /**
     * Удаление курсов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $rate = FileParserRates::findOrFail($id);

        if($rate->direction_exchange->count() > 0) {
            flash('Выбранный курс удалить невозможно, К нему привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Курс {$rate->name} успешно удален", ['alert alert-success']);
        $rate->delete();

        return redirect()->route('file-parser.index');
    }
}
