<?php

namespace App\Http\Controllers\Administrator\Crypto;

use App\Http\Controllers\Controller;
use App\Models\CourseUpdateTimeLog;
use Illuminate\Http\Request;

class ParserLogController extends Controller
{
    /**
     * Лог обновлении курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse
     */
    public function log_update_courses(Request $request)
    {
        if($request->has('clear_logs'))
        {
            \DB::table('course_update_time_logs')->delete();
            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return redirect()->back();
        }

        $logs = CourseUpdateTimeLog::orderBy('id', 'desc')->paginate(20);
        return view('admin.crypto.log_update_courses', compact('logs'));
    }
}
