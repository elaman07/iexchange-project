<?php
namespace App\Http\Controllers\Administrator\Crypto;

use App\Http\Controllers\Controller;
use App\Models\ParserApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class APIKeyController extends Controller
{

    /**
     * Список API Ключей
    */
    public function index()
    {
        $items = ParserApiKey::orderByDesc('id')->paginate(20);
        return view('admin.crypto.keys.index', compact('items'));
    }

    /**
     * Форма добавления нового ключа
    */
    public function create()
    {
        return view('admin.crypto.keys.create');
    }

    /**
     * Обработка и добавление нового ключа
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'save')
        {

            foreach ($request->key_id as $value)
            {
                ParserApiKey::find($value)->update([
                    'status' =>   (isset($request->status[$value]) ? 1 : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'keys' => ['required']
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $array = explode(PHP_EOL, $request->get('keys'));

            foreach ($array as $item)
            {
                ParserApiKey::create([
                    'provider_id' => $request->get('provider_id'),
                    'api_key' => trim($item),
                    'status'  => $request->has('status') ? 1 : 0
                ]);
            }

            flash('Ключи добавлены', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Форма обновления ключей
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ParserApiKey::find($id);

        return view('admin.crypto.keys.edit', compact('item'));
    }

    /**
     * Обработка и обновление ключа
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => ['required']
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            ParserApiKey::find($id)->update([
                'provider_id' => $request->get('provider_id'),
                'api_key' => trim($request->get('key')) ?? null,
                'status'  => $request->has('status') ? 1 : 0
            ]);

            flash('Ключ '.$request->get('key').' обновлен', ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удаление ключа
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        $key = ParserApiKey::find($id);
        flash("Ключ {$key->api_key} удален", ['alert alert-success']);
        $key->delete();

        return redirect()->back();
    }

    /**
     * Удаление всех ключей
    */
    public function delete_all()
    {
        ParserApiKey::query()->delete();
        flash("Ключи успешно удалены", ['alert alert-success']);
        return redirect()->back();
    }
}
