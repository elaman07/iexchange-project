<?php
namespace App\Http\Controllers\Administrator\Crypto;

use App\Http\Controllers\Controller;
use App\Models\CompetitorLink;
use App\Models\CompetitorRates;
use App\Models\CompetitorRatesLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompetitorsParserController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'record_course_history_competitors',
        'is_enabled_competitors_rounding'
    ];

    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Создаем дубликат
        if($request->has('duplicate') and $request->get('duplicate') == true)
        {
            $item = CompetitorRates::find($request->get('id'));
            $newParser = $item->replicate();
            $newParser->save();

            flash('Дубликат '.$item->name.' успешно создан', ['alert alert-success']);
            return redirect()->back();
        }


        $links = CompetitorLink::where('status', '=', 1)->orderBy('sorting')->get();

        return view('admin.crypto.competitors.index', [
            'links' =>  $links
        ]);
    }

    /**
     * Форма добавления курсов конкурентов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $links = CompetitorLink::where('status', '=', 1)->get();

        return view('admin.crypto.competitors.create', compact('links'));
    }

    /**
     * Обработка и добавление курсов конкурентов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // Обработка доп. настроек
        if($request->has('actions') or $request->has('action')) {
            return $this->handlerOptions($request);
        }

        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_competitor' => ['required', 'exists:competitor_links,id'],
            'type' => ['required'],
            'number_format' => ['required', 'numeric'],
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {

            $explode = explode(' - ', $request->get('name'));

            $rate = CompetitorRates::create([
                'name'    =>  $request->has('name') ? $request->get('name') : null,
                'exchange_in' => $explode[0],
                'exchange_out' => $explode[1],
                'id_competitor'  => $request->has('id_competitor') ? $request->get('id_competitor') : 0,
                'status'  => $request->has('status') ? $request->get('status') : 0,
                'type'  => $request->has('type') ? $request->get('type') : 0,
                'number_format' => $request->has('number_format') ? $request->get('number_format') : 0,
            ]);

            $rate->update([
                'code' => '[competitors_'.\Str::lower(\Str::studly($rate->competitor_link->name)).'_'.\Str::lower($rate->exchange_in.'-'.$rate->exchange_out).']'
            ]);

            flash("Парсер {$rate->name} успешно добавлен", ['alert alert-success']);
            return redirect()->route('competitors-parser.index');
        }

    }

    /**
     * Форма редактирования курсов конкурентов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = CompetitorRates::findOrFail($id);
        $links = CompetitorLink::where('status', '=', 1)->get();

        return view('admin.crypto.competitors.edit',compact('item', 'links'));
    }

    /**
     * Обработка и обновление курсов конкурентов
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $rates = CompetitorRates::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'id_competitor' => ['required', 'exists:competitor_links,id'],
            'type' => ['required'],
            'number_format' => ['required', 'numeric'],
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput($request->all());
        } else {
            $explode = explode(' - ', $request->get('name'));

            $rates->update([
                'name'              => $request->has('name') ? $request->get('name') : null,
                'exchange_in'       => $explode[0],
                'exchange_out'      => $explode[1],
                'id_competitor'     => $request->has('id_competitor') ? $request->get('id_competitor') : 0,
                'status'            => $request->has('status') ? $request->get('status') : 0,
                'type'              => $request->has('type') ? $request->get('type') : 0,
                'number_format'     => $request->has('number_format') ? $request->get('number_format') : 0,
            ]);

            $rates->update([
                'code' => '[competitors_'.\Str::lower(\Str::studly($rates->competitor_link->name)).'_'.\Str::lower($rates->exchange_in.'-'.$rates->exchange_out).']'
            ]);

            flash("Парсер {$rates->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('competitors-parser.index');
        }
    }

    /**
     * Удаление курсов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        $rate = CompetitorRates::findOrFail($id);

        if($rate->direction_exchange->count() > 0) {
            flash('Выбранный курс удалить невозможно, К нему привязаны направления', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Курс {$rate->name} успешно удален", ['alert alert-success']);
        $rate->delete();

        return redirect()->route('competitors-parser.index');
    }

    /**
     * История курсов для определенного направления
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyPair(int $id)
    {
        $logs = CompetitorRatesLog::where('id_competitors', '=', $id)->orderByDesc('id')->paginate(20);
        $item = CompetitorRates::find($id);

        return view('admin.crypto.competitors.history', [
            'logs'  =>  $logs,
            'item' => $item
        ]);
    }

    /**
     * История курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history(Request $request)
    {
        // Очищаем историю курсов
        if($request->has('clear')) {
            CompetitorRatesLog::query()->delete();
            CompetitorRatesLog::truncate();

            flash('История курсов успешно очищена', ['alert alert-success']);
            return redirect()->back();
        }


        $logs = CompetitorRatesLog::orderByDesc('id')->paginate(20);
        return view('admin.crypto.competitors.history', [
            'logs'  =>  $logs,
            'item' => null
        ]);
    }

    /**
     * Обработка и настройка курсов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    private function handlerOptions(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions'))
        {
            if($request->get('actions') == 'change_type')
            {
                foreach ($request->get('type') as $key => $item)
                {
                    $bestchange = CompetitorRates::findOrFail($key);
                    $bestchange->type = $item;
                    $bestchange->save();
                }
            }elseif($request->has('exchange_checkbox'))
            {
                foreach ($request->get('exchange_checkbox') as $key => $item)
                {
                    $find = CompetitorRates::findOrFail($key);
                    if($request->get('actions') == 'activation') {
                        $find->status = 1;
                        $find->save();
                    } elseif($request->get('actions') == 'deactivation') {
                        $find->status = 0;
                        $find->save();
                    } elseif($request->get('actions') == 'delete')
                        return $this->destroy($key);
                }
            }

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }
    }
}
