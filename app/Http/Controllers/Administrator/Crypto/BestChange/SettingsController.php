<?php

namespace App\Http\Controllers\Administrator\Crypto\BestChange;

use App\Common\Packages\Crypto\BestchangeHandler;
use App\Common\Packages\Crypto\Facades\BestChange;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_enable_parser_bs_console',
        'public_server_bestchange',
        'is_crypto_parser_test',
        'bestchange_exchange_black_list',
        'bestchange_exc_scam',
        'bestchange_position',
        'bestchange_currencies_parsers',
        'is_enabled_bestchange_log',
        'is_enable_bs_log_error',
        'record_course_history_bestchange',
        'is_enabled_bestchange_rounding',
        'is_enable_bs_undefined_pair',
        'bestchange_timeout',
        'bestchange_cities_parsers'
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Очистка кэша
        if($request->has('wid_cache_control')) {
            iex_clear_cache_control($request->get('wid_cache_control'));
            flash('Кэш успешно обновлен', ['alert alert-success']);

            return redirect()->back();
        }


        if($request->method() == 'POST')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);

            flash('Настройки успешно сохранены', ['alert alert-success']);

            return redirect()->back();
        }

        $bestchange = new BestchangeHandler();


        try {
            $exchangers = $bestchange->getExchangers();
            $cities = $bestchange->getCities();
            $cached = $bestchange->cache('currency');
        }catch (\Exception $e) {
            $exchangers = [];
            $cached = [];
            $cities = [];
        }

        $array = [];
        foreach ($exchangers as $exchanger) {
            $array[$exchanger] = $exchanger;
        }

        return view('admin.crypto.bestchange.settings', [
            'exchangers' => $exchangers,
            'exchangers_2' => $array,
            'allowed_currencies' => $cached,
            'cities'    =>  $cities
        ]);
    }
}
