<?php
namespace App\Http\Controllers\Administrator\Crypto\BestChange;


use App\Common\Packages\Crypto\BestchangeHandler;
use App\Models\BestchangeDataLog;
use App\Models\BestchangeParserError;
use App\Models\BestchangeRatesLog;
use App\Models\BestChangeRatesModel;
use App\Http\Controllers\Controller;
use App\Models\DirectionExchange;
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BestChangeController extends Controller
{
    /**
     * Список курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if($request->has('delete')) {
            return $this->destroy($request->get('id'));
        }

        if($request->has('update_rates'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            try {
                \Artisan::call('compiler:bestchange');

                // Активация режима тестирования
                if(iEXSetting('is_crypto_parser_test', 0) == 1) {
                    echo '<pre>';
                    print_r(\Artisan::output());
                    echo '</pre>';
                    exit;
                }
                flash(__('Курсы обновлены'), ['alert alert-success']);
            }catch (\Exception $e) {
                flash($e->getMessage() , ['alert alert-danger']);
            }
            return  redirect()->to(admin_base_path('/crypto/bestchange'));
        }

        if($request->has('actions'))
        {
            if($request->get('actions') == 'delete') {
                $dx = DirectionExchange::find($request->get('id_direction'));
                $dx->update([
                    'enable_bestchange' => 0,
                    'id_bestchange_rates' => 0
                ]);

                flash('Курс отвязан от направления', ['alert alert-success']);
                return redirect()->route('bestchange.index');
            }

        }

        $rates = BestChangeRatesModel::orderByDesc('id')->get();

        $is_automatic = BestChangeRatesModel::count();
        return view('admin.crypto.bestchange.index', [
            'rates' => $rates,
            'is_automatic' => $is_automatic
        ]);
    }

    /**
     * Форма добавления нового курса
     *
     * @throws \Exception
     */
    public function create()
    {
        $bestchange_currencies_get = json_decode(file_get_contents(config('bestchange.bestchange_json')), true);
        $bestchange_currencies = collect($bestchange_currencies_get)->whereIn('id', explode(',',iEXSetting('bestchange_currencies_parsers')))->pluck('name', 'id');

        return view('admin.crypto.bestchange.create',[
            'rates'    =>  $bestchange_currencies
        ]);
    }

    /**
     * Обработка и добавление курса
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('bestchange_checkbox'))
        {
            foreach ($request->get('bestchange_checkbox') as $key => $item)
            {
                $bestchange = BestChangeRatesModel::findOrFail($key);
                if($request->get('actions') == 'activation') {
                    $bestchange->update(['status' => 1]);
                }
                if($request->get('actions') == 'deactivation') {
                    $bestchange->update(['status' => 0]);
                }
                if($request->get('actions') == 'delete') {
                    $bestchange->delete();
                }
            }

            flash('Пары успешно удалены', ['alert alert-success']);
            return redirect()->route('bestchange.index');
        }


        $validator = Validator::make($request->all(), [
            'give'          =>  'required|numeric',
            'receiving'     =>  'required|numeric',
            'status'        =>  'required',
            'number_format' =>  'required|numeric'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $bestchange = new BestchangeHandler();
            $currency = $bestchange->getCurrencies();

            // Получаем итоговое название курса
            $name = sprintf('%s → %s',
                $currency[$request->get('give')]['name'],
                $currency[$request->get('receiving')]['name']
            );

            try {
                collect($bestchange->setRatesPair([$request->get('give'),  $request->get('receiving')]))
                    ->reject(function($item) {
                        return empty($item);
                    })->filter(function($item) {
                        $black_list = explode(',', config('crypto.bestchange_black_list'));
                        return !in_array($item['exchanger_id'], $black_list);
                    });

                //Добавляем в базу
                BestChangeRatesModel::create([
                    'name'              =>  $name,
                    'exchanger_id1'     =>  $request->get('give'),
                    'exchanger_id2'     =>  $request->get('receiving'),
                    'value'             =>  1,
                    'summa'             =>  0,
                    'status'            =>  $request->get('status'),
                    'number_format'     =>  $request->get('number_format'),
                ]);

                flash("Пара {$name} успешно создана", ['alert alert-success']);

            } catch (\Exception $exception) {
                flash("Пара {$name} не добавлена", ['alert alert-danger']);
            }

            return redirect()->to(
                admin_base_path(($request->has('success_and_create')) ? '/crypto/bestchange/create' : '/crypto/bestchange')
            );
        }
    }

    /**
     * Форма редактирования валюты
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit(int $id)
    {
        $item = BestChangeRatesModel::findOrFail($id);
        // Список валюты
        $bestchange_currencies_get = json_decode(file_get_contents(config('bestchange.bestchange_json')), true);
        $rates = collect($bestchange_currencies_get)->whereIn('id', explode(',',iEXSetting('bestchange_currencies_parsers')))->pluck('name', 'id');


        return view('admin.crypto.bestchange.edit', compact('item', 'rates'));
    }

    /**
     * Обработка и обновление курса
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, int $id)
    {
        $item = BestChangeRatesModel::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'give'          =>  'required|numeric',
            'receiving'     =>  'required|numeric',
            'status'        =>  'required',
            'number_format' =>  'required|numeric'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $bestchange = new BestchangeHandler();
            $currency = $bestchange->getCurrencies();

            // Получаем итоговое название курса
            $name = sprintf('%s → %s',
                $currency[$request->get('give')]['name'],
                $currency[$request->get('receiving')]['name']
            );

            //Добавляем в базу
            $item->update([
                'name'                  =>  $name,
                'exchanger_id1'         =>  $request->get('give'),
                'exchanger_id2'         =>  $request->get('receiving'),
                'value'                 =>  1,
                'summa'                 =>  0,
                'status'                =>  $request->get('status'),
                'number_format'         =>  $request->get('number_format')
            ]);

            flash("Пара {$name} успешно обновлена", ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление пары
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = BestChangeRatesModel::findOrFail($id);

        if($item->direction_exchange->count() > 0) {
            flash("Пару {$item->name} удалить невозможно, к ней привязаны направления", ['alert alert-danger']);
            return redirect()->route('bestchange.index');
        }

        flash("Пара {$item->name} удалена", ['alert alert-success']);
        $item->delete();

        return redirect()->route('bestchange.index');
    }

    /**
     * Обновить коды валюты
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    protected function update_codes()
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        try {
            (new BestchangeHandler())->refreshCode();
            flash('Коды валют успешно обновлены', ['alert alert-success']);
        } catch (\Exception $exception) {
            flash('Курсы не обновлены', ['alert alert-danger']);
        }
        return redirect()->route('bestchange.index');
    }


    /**
     * История курсов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history(Request $request)
    {
        // Очищаем историю курсов
        if($request->has('clear')) {
            BestchangeRatesLog::query()->delete();
            BestchangeRatesLog::truncate();

            flash('История курсов успешно очищена', ['alert alert-success']);
            return redirect()->back();
        }

        $logs = BestchangeRatesLog::orderByDesc('id')->paginate(20);
        return view('admin.crypto.bestchange.history', [
            'logs'  =>  $logs,
            'item' => null
        ]);
    }

    /**
     * История курсов для определенного направления
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyPair(int $id)
    {
        $logs = BestchangeRatesLog::where('id_bestchange_rates', '=', $id)->orderByDesc('id')->paginate(20);
        $item = BestChangeRatesModel::find($id);

        return view('admin.crypto.bestchange.history', [
            'logs'  =>  $logs,
            'item' => $item
        ]);
    }

    /**
     * Получение лога ошибка из bestchange
    */
    public function logData()
    {
        $logs = BestchangeDataLog::orderByDesc('id')->paginate(20);
        return view('admin.crypto.bestchange.log', compact('logs'));
    }

    /**
     * Получение лог ошибок по ID пары из bestchange
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function errorLogId(int $id, Request $request)
    {
        $item = BestChangeRatesModel::find($id);

        // Очищаем лог ошибок
        if($request->has('clear'))
        {
            BestchangeParserError::where('id_bestchange', $id)->delete();
            flash('Лог ошибок для '.$item->name.' успешно очищен', ['alert alert-success']);
            return redirect()->back();
        }

        $logs = BestchangeParserError::where('id_bestchange', $id)->orderByDesc('id')->paginate(20);
        return view('admin.crypto.bestchange.error_log', compact('logs', 'item'));
    }
}
