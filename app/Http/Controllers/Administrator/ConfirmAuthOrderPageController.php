<?php

namespace App\Http\Controllers\Administrator;

use App\Common\Support\ConfirmAuthOrderPageHandler;
use App\Http\Controllers\Controller;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;

class ConfirmAuthOrderPageController extends Controller
{
    use ConfirmAuthOrderPageHandler;


    /**
     * Главная страница для ввода резервного кода
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Временно кэшируем код, чтобы зафиксировать №
        $code = $this->getCurrentCode();
        return view('admin.confirm-auth-order-page', compact('code'));
    }

    /**
     * Обработка введенного резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, RateLimiter $limiter)
    {
        $validator = \Validator::make($request->all(), [
            'code' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            // Для начала проверяем установленный лимит
            if ($this->hasTooManyRecoveryAttempts($request, $limiter)) {
                $text = $this->sendLockoutResponse($request, $limiter);

                flash($text, ['alert alert-danger']);
                return back();
            }

            // Увеличиваем кол-во попыток
            $this->incrementAttempts($request, $limiter);

            if($this->attemptRecovery($request))
            {
                $this->sendRecoveryResponse($request, $limiter);

                return redirect()->to(admin_base_path('/'));
            } else {
                flash('Неверный код', ['alert alert-danger']);
            }


            return redirect()->back();
        }
    }

    /**
     * В случае удачного ввода резервного кода
     *
     * @param  \Illuminate\Http\Request $request
     * @param RateLimiter $limiter
     * @return void
     */
    protected function sendRecoveryResponse(Request $request, RateLimiter $limiter)
    {
        $this->clearAttempts($request, $limiter);
        $this->insertCode($request);
    }

    /**
     * Проверка кода
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptRecovery(Request $request)
    {
        return $this->checkedCode($request);
    }
}
