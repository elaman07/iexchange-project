<?php

namespace App\Http\Controllers\Administrator\Gateways;

use App\Http\Controllers\Controller;
use App\Models\LogAutoPaymentEvent;
use App\Models\LogAutoPaymentOrderEvent;
use App\Models\LogMerchantEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LogController extends Controller
{

    /**
     * Запросы к мерчантам
     */
    public function merchant_event(Request $request)
    {
        if($request->has('clear_logs'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            \DB::table('log_merchants_events')->delete();
            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return redirect()->back();
        }

        // Удаляем ненужные ключи из массива
        if($request->has('send_action'))
        {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('event-merchant-log', $conditions);
        }


        $events = LogMerchantEvent::filter($request->all())->orderBy('id', 'desc')->paginate(20);
        return view('admin.gateways.logs.merchant_event', [
            'logs' => $events,
            'filter' => $request->all()
        ]);
    }

    /**
     * Запросы к выплатам
     */
    public function autopayment_event(Request $request)
    {
        if($request->has('clear_logs'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            \DB::table('logs_autopayment_events')->delete();
            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return redirect()->back();
        }


        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('event-autopayment-log', $conditions);
        }
        $events = LogAutoPaymentEvent::filter($request->all())->orderBy('id', 'desc')->paginate(20);

        return view('admin.gateways.logs.autopayment_event', [
            'logs' => $events,
            'filter' => $request->all()
        ]);
    }

    public function orders_event(Request $request)
    {
        if($request->has('clear_logs'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            \DB::table('logs_autopayment_orders_events')->delete();
            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return redirect()->back();
        }


//        // Удаляем ненужные ключи из массива
//        if($request->has('send_action')) {
//            $conditions = Arr::except($request->all(),['send_action']);
//            return \redirect()->route('event-orders-log', $conditions);
//        }
        $events = LogAutoPaymentOrderEvent::orderBy('id', 'desc')->paginate(20);

        return view('admin.gateways.logs.autopayment_orders_event', [
            'logs' => $events,
            'filter' => $request->all()
        ]);
    }
}
