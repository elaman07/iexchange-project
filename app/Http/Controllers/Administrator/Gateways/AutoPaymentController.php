<?php
namespace App\Http\Controllers\Administrator\Gateways;

use App\Http\Controllers\Controller;
use App\Models\Gateway;
use App\Models\GatewayPayment;
use App\Models\Merchant;
use Brotzka\DotenvEditor\DotenvEditor;
use Defuse\Crypto\Crypto;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Modules\ProxyManager\Entities\ProxyModel;
use Nwidart\Modules\Facades\Module;

class AutoPaymentController extends Controller
{
    /**
     * Переадресация
     *
     * @var string
     */
    protected string $redirectPath = '/gateways';

    /**
     * Список автовыплат
    */
    public function index()
    {
        $payments = GatewayPayment::orderByDesc('id')->get();
        return view('admin.gateways.autopayment.index', compact('payments'));
    }

    /**
     * Добавление автовыплаты
     */
    public function create()
    {
        $gateways = Gateway::whereIsPay(1)->get()->pluck('name', 'id');
        return view('admin.gateways.autopayment.create', compact('gateways'));
    }

    /**
     * Обработка и добавление автовыплаты
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public function store(Request $request): RedirectResponse
    {
        // Обновленять действие мгновенно для неогр. кол-во платежных шлюзов на выплат
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->key_id as $value) {
                GatewayPayment::find($value)->update([
                    'status' =>   (isset($request->status[$value]) ? 1 : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }

        // Проверка входных параметров
        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        // Если при создании найдены ошибки, дальше не пускаем
        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $item = GatewayPayment::create([
                'name'          =>  $request->get('name'),
                'id_gateways'   => $request->get('id_gateways'),
                'status'        => $request->has('status') ? 1 : 0
            ]);


            // Параметры, для создании конфигруационного файла для мерчанта
            $dir = storage_path('/gateways/pay/'.$item->gateway->alias);
            $filename = storage_path('/gateways/pay/'.$item->gateway->alias.'/id'.$item->id).'.php';

            if(!File::isDirectory($dir)) {
                File::makeDirectory($dir, 0775);
            }

            // Получение параметров, по умолчанию
            $defaultParameters = \Config::get('gateways.default_parameters.'.$item->gateway->alias.'.pay');


            if(!File::exists($filename)) {
                File::put($filename, encrypt_protect_gateway_keys($defaultParameters));
            }

            // Получаем дополнительные позиция.
            $options = \Config::get('gateways.default_parameters.'.$item->gateway->alias.'.options');

            if(!empty($options)) {
                $item->update($options);
            }

            flash("Автовыплата {$item->name} успешно добавлена", ['alert alert-success']);
            return redirect()->to(
                admin_base_path(sprintf('%s/autopayment/%s/edit',$this->redirectPath, $item->id))
            );
        }
    }

    /**
     * Редактирование автовыплты
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function edit(int $id)
    {
        $item = GatewayPayment::find($id);
        $gateways = Gateway::whereIsPay(1)->get()->pluck('name', 'id');

        $data = protect_gateway_keys($item->gateway->alias, $item->id, 'pay');

        // Proxy
        $proxies = [];
        if(Module::find('ProxyManager')->isEnabled()) {
            $proxies = ProxyModel::orderByDesc('id')->get();
        }

        return view('admin.gateways.autopayment.edit', compact('item', 'gateways', 'proxies', 'data'));
    }

    /**
     * Обработка и обновление автовыплаты
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $item = GatewayPayment::find($id);
            $config = protect_gateway_keys($item->gateway->alias, $item->id, 'pay');

            // Получаем параметры по умолчанию
            $defaultParameters = \Config::get('gateways.default_parameters.'.$item->gateway->alias.'.pay');
            $only_keys = collect($defaultParameters)->keys()->toArray();

            $env_data = collect($request->all())->filter(function ($value) {
                return !empty($value);
            })->only($only_keys)->toArray();

            if(empty($defaultParameters)) {
                $mergeParams = $config;
            } else {
                $mergeParams = array_merge($defaultParameters, $config, $env_data);
            }

            $filename = storage_path('/gateways/pay/'.$item->gateway->alias.'/id'.$item->id).'.php';
            if(File::isFile($filename)) {
                File::put($filename,  encrypt_protect_gateway_keys($mergeParams));
            }

            // Обновление настроек в базе
            $options = [
                'name'                      =>  $request->get('name'),
                'status'                    =>  ($request->has('status') ? 1 : 0),
                'id_proxy'                  =>  ($request->has('id_proxy') ? $request->get('id_proxy') : 0),
                'min_confirm'               =>  ($request->has('min_confirm') ? $request->get('min_confirm') : 0),
                'max_register_blockchain'   =>  ($request->has('max_register_blockchain') ? $request->get('max_register_blockchain') : 0),
                'max_first_confirm_blockchain'   =>  ($request->has('max_first_confirm_blockchain') ? $request->get('max_first_confirm_blockchain') : 0),
                'manual_pay_order'               =>  ($request->has('manual_pay_order') ? $request->get('manual_pay_order') : 0),
                'is_mass_payouts'                =>  ($request->has('is_mass_payouts') ? $request->get('is_mass_payouts') : 0),
                'hide_check_balance'             =>  $request->has('hide_check_balance') ? $request->get('hide_check_balance') : 0,
                'is_subtract'                    =>  ($request->has('is_subtract') ? $request->get('is_subtract') : 0),
                'direction'                    =>  ($request->has('direction') ? $request->get('direction') : 0),
                'currency_code'                  =>  ($request->has('currency_code') ? $request->get('currency_code') : null),
                'site_account'  =>  $request->has('site_account') ? $request->get('site_account'): null,
                'method_pay'   =>  ($request->has('method_pay') ? $request->get('method_pay') : 0),
                'country_code'   =>  ($request->has('country_code') ? $request->get('country_code') : null),
                'num_request'   =>  ($request->has('num_request') ? $request->get('num_request') : null),
                'priority_fee' => ($request->has('priority_fee') ? $request->get('priority_fee') : null),
                'code_currency' => ($request->has('code_currency') ? $request->get('code_currency') : null),

                'exchange_buy' => ($request->has('exchange_buy') ? $request->get('exchange_buy') : 0),
                'exchange_fee' => ($request->has('exchange_fee') ? $request->get('exchange_fee') : 0),
                'exchange_buy_type' => ($request->has('exchange_buy_type') ? $request->get('exchange_buy_type') : 0),
                'time_in_force' => ($request->has('time_in_force') ? $request->get('time_in_force') : null),
                'exchange_buy_curr' => ($request->has('exchange_buy_curr') ? $request->get('exchange_buy_curr') : null),
                'is_auto_take_fee' => ($request->has('is_auto_take_fee') ? $request->get('is_auto_take_fee') : 0),
                'pay_amount_type'   => ($request->has('pay_amount_type') ? $request->get('pay_amount_type') : 0),
                'bank_name'     =>  $request->has('bank_name') ? $request->get('bank_name') : null
            ];

            foreach (config('app.form_lang') as $m_key => $m_item) {
                $options['comment'][$m_key] = $request->get('comment'.$m_item['field']);
            }

            $item->update($options);

            flash("Данные {$item->name} успешно обновлен", ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаляем автовыплату
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $merchant = GatewayPayment::find($id);

        if($merchant->currency->count() > 0) {
            flash('Невозможно удалить автовыплату, к ней привязаны валюты', ['alert alert-danger']);
            return redirect()->back();
        }

        flash("Автовыплата {$merchant->name} успешно удалена", ['alert alert-success']);
        $merchant->delete();

        return redirect()->back();
    }
}
