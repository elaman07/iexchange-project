<?php
namespace App\Http\Controllers\Administrator\Gateways;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Gateway;
use App\Models\GatewayMerchant;
use Defuse\Crypto\Crypto;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use XRPL_PHP\Wallet\Wallet;

/**
 * Класс для работы с мерчантами
*/
class MerchantController extends Controller
{
    protected array $allowFilteredPage = [
        'admin_merchant_pagination',
        'admin_merchant_hidden_columns'
    ];

    /**
     * Переадресация
     *
     * @var string
    */
    protected string $redirectPath = '/gateways/merchant';


    public function index(Request $request)
    {
        $merchants = GatewayMerchant::filter($request->all())->orderByDesc('id')->paginate(iEXSetting('admin_merchant_pagination', 20));

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('merchant.index', $conditions);
        }

        $merchant_data = config('gateways.default_parameters');
        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');

        $admin_merchant_hidden_columns = explode(',', iEXSetting('admin_merchant_hidden_columns'));

        return view('admin.gateways.merchant.index', [
            'merchants' => $merchants,
            'currencies' => $currencies,
            'filter' => $request->all(),
            'merchant_data' => $merchant_data,
            'admin_merchant_hidden_columns' => $admin_merchant_hidden_columns,
            'status' => [
                0 => 'Не активен',
                1 => 'Активен'
            ]
        ]);
    }

    /**
     * Добавление мерчанта
    */
    public function create()
    {
        $gateways = Gateway::whereIsMerchant(1)->get()->pluck('name', 'id');
        $roles = Role::get();
        return view('admin.gateways.merchant.create', compact('gateways', 'roles'));
    }

    /**
     * Обработка и добавление мерчанта
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('action') and $request->get('action') == 'settings_columns')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFilteredPage as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        // Дополнительные настройки для мгновенной отключении и включении мерчантов
        if($request->has('actions') and $request->get('actions') == 'save')
        {
            foreach ($request->key_id as $value) {
                $update = GatewayMerchant::find($value);
                $update->update([
                    'status' =>  (isset($request->status[$value]) ? 1 : 0)
                ]);
                $update->currencies()->sync($request->currencies[$value] ?? []);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->to(
                admin_base_path($this->redirectPath)
            );
        }

        // Проверка входных параметров
        $validator = Validator::make($request->all(), [
            'name' => ['required']
        ]);

        // Если при создании найдены ошибки, дальше не пускаем
        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            // Добавление нового мерчанта
            $item = GatewayMerchant::create([
                'name'          => $request->get('name'),
                'id_gateways'   => $request->get('id_gateways'),
                'status'        => $request->has('status') ? 1 : 0
            ]);

            // Параметры, для создании конфигруационного файла для мерчанта
            $dir = storage_path('/gateways/merchant/'.$item->gateway->alias);
            $filename = storage_path('/gateways/merchant/'.$item->gateway->alias.'/id'.$item->id).'.php';

            if(!File::isDirectory($dir)) {
                File::makeDirectory($dir, 0775);
            }

            // Получение параметров, по умолчанию
            $defaultParameters = \Config::get('gateways.default_parameters.'.$item->gateway->alias.'.merchant');
            if(!File::exists($filename)) {
                File::put($filename,  encrypt_protect_gateway_keys($defaultParameters));
            }

            flash("Мерчант {$item->name} успешно добавлен", ['alert alert-success']);
            return redirect()->to(
                admin_base_path(sprintf('%s/%s/edit', $this->redirectPath, $item->id))
            );
        }
    }

    /**
     * Редактирование мерчанта
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function edit(int $id, Request $request)
    {
        if($request->has('random_secret')) {
            $update_key = GatewayMerchant::findOrFail($id)->update([
                'security_hash' => \Str::random(12)
            ]);

            return redirect()->to(
                admin_base_path('/gateways/merchant/'.$id.'/edit')
            );
        }

        $item = GatewayMerchant::find($id);

        $options_render = [];
        // Для индивидуальных провайдеров
        if($request->has('create_wallet'))
        {
            switch ($item->gateway->alias)
            {
                /**
                 * Для Ripple
                */
                case 'ripple':
                    $wallet = Wallet::generate();
                    $options_render = [
                        'address' => $wallet->getClassicAddress(),
                        'private_key' => $wallet->getSeed()
                    ];
                    break;
            }
        }

        $data = protect_gateway_keys($item->gateway->alias, $item->id);
        $gateways = Gateway::whereIsMerchant(1)->get()->pluck('name', 'id');

        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name. ' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');

        return view('admin.gateways.merchant.edit', compact('item', 'data', 'gateways','currencies', 'options_render'));
    }

    /**
     * Обработка и обновление мерчанта
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function update(int $id, Request $request): RedirectResponse
    {

        $item = GatewayMerchant::find($id);

        $validator = Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            if(isset($item->gateway))
            {
                $config = protect_gateway_keys($item->gateway->alias, $item->id);

                // Получаем параметры по умолчанию
                $defaultParameters = \Config::get('gateways.default_parameters.'.$item->gateway->alias.'.merchant');
                $only_keys = collect($defaultParameters)->keys()->toArray();

                $env_data = collect($request->all())->filter(function ($value) {
                    return !empty($value);
                })->only($only_keys)->toArray();

                if(empty($defaultParameters)) {
                    $mergeParams = $config;
                } else {
                    $mergeParams = array_merge($defaultParameters, $config, $env_data);
                }
                $array = var_export($mergeParams, true);

                $is_config_done = 0;
                if(count(array_filter($mergeParams)) == count($mergeParams)) {
                    $is_config_done = 1;
                }

                $filename = storage_path('/gateways/merchant/'.$item->gateway->alias.'/id'.$item->id).'.php';
                if(File::isFile($filename)) {
                    File::put($filename,  encrypt_protect_gateway_keys($mergeParams));
                }
            }


            $options = [
                'name'                          =>  $request->get('name'),
                'status'                        =>  ($request->has('status') ? 1 : 0),
                'allow_ip_address'              =>  ($request->has('allow_ip_address') ? $request->get('allow_ip_address') : null),
                'is_check_from_shot'            =>  ($request->has('is_check_from_shot') ? $request->get('is_check_from_shot') : 0),
                'is_merchant_log'                =>  ($request->has('is_merchant_log') ? $request->get('is_merchant_log') : 0),
                'is_check_api'     =>  ($request->has('is_check_api') ? $request->get('is_check_api') : 0),
                'is_deny_ip_address'            =>  ($request->has('is_deny_ip_address') ? $request->get('is_deny_ip_address') : 0),
                'is_pay_commission'             =>  ($request->has('is_pay_commission') ? $request->get('is_pay_commission') : 0),
                'day_limit_merchant'            =>  ($request->has('day_limit_merchant') ? $request->get('day_limit_merchant') : 0),
                'max_limit_amount_order'        =>  $request->has('max_limit_amount_order') ? $request->get('max_limit_amount_order') : 0,
                'amount_fault'                  =>  $request->has('amount_fault') ? $request->get('amount_fault') : 0,
                'day_limit_amount_merchant'     =>  $request->has('day_limit_amount_merchant') ? $request->get('day_limit_amount_merchant') : 0,
                'is_enable_merchant_button'     =>  $request->has('is_enable_merchant_button') ? $request->get('is_enable_merchant_button') : 0,
                'method_pay'     =>  $request->has('method_pay') ? $request->get('method_pay') : 0,
                'fixed_fee'     =>  $request->has('fixed_fee') ? $request->get('fixed_fee') : 0,
                'is_card_found'     =>  $request->has('is_card_found') ? $request->get('is_card_found') : 0,
                'pay_amount'     =>  $request->has('pay_amount') ? $request->get('pay_amount') : 0,
                'credit_amount'     =>  $request->has('credit_amount') ? $request->get('credit_amount') : 0,
                'bank_name'     =>  $request->has('bank_name') ? $request->get('bank_name') : 0,
                'site_account'  =>  $request->has('site_account') ? $request->get('site_account'): null,
                'code_currency'  =>  $request->has('code_currency') ? $request->get('code_currency'): null,
                'min_confirm'               =>  ($request->has('min_confirm') ? $request->get('min_confirm') : 0),
                'max_register_blockchain'   =>  ($request->has('max_register_blockchain') ? $request->get('max_register_blockchain') : 0),
                'max_first_confirm_blockchain'   =>  ($request->has('max_first_confirm_blockchain') ? $request->get('max_first_confirm_blockchain') : 0),
                'exchange_fee'              =>  $request->has('exchange_fee') ? $request->get('exchange_fee') : 0,
                'is_config_done'                =>  (isset($is_config_done)) ? $is_config_done : 0,
                'type_pay'                  =>  $request->has('type_pay') ? $request->get('type_pay') : 0
            ];

            foreach (config('app.form_lang') as $m_key => $m_item) {
                $options['instruction_payment'][$m_key] = $request->get('instruction_payment'.$m_item['field']);
                $options['comment'][$m_key] = $request->get('comment'.$m_item['field']);
            }


            // Обновление настроек в базе
            $item->update($options);
            $item->currencies()->sync($request->currencies);

            flash("Данные {$item->name} успешно обновлен", ['alert alert-success']);
        }

        return redirect()->to(
            admin_base_path('gateways/merchant/'.$item->id.'/edit')
        );
    }

    /**
     * Удаляем мерчант
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = GatewayMerchant::find($id);

        if($item->currencies->count() > 0) {
            flash('Невозможно удалить мерчант, к ней привязаны валюты', ['alert alert-danger']);
            return redirect()->to(
                admin_base_path($this->redirectPath)
            );
        }

        // Удаляем конфигурационный файл
        if(isset($item->gateway)) {
            $filename = config_path('/gateways/merchant/'.$item->gateway->alias.'/id'.$item->id).'.php';
            iex_file_delete($filename);
        }

        flash("Мерчант {$item->name} успешно удален", ['alert alert-success']);
        $item->delete();


        //  admin_base_path(sprintf('%s/merchant/%s/edit',$this->redirectPath, $item->id))
        return redirect()->to(
            admin_base_path($this->redirectPath)
        );
    }
}
