<?php
namespace App\Http\Controllers\Administrator\Account;


use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Common\Packages\ReferralSystem\Models\ReferralProgram;
use App\Common\Packages\ReferralSystem\Models\ReferralRelationship;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Exports\OrderUserExport;
use App\Http\Controllers\Controller;
use App\Jobs\RegisterClientJob;
use App\Models\ActiveUserModel;
use App\Models\BackupCodeModel;
use App\Models\BannedUser;
use App\Models\Task;
use App\Models\User;
use App\Models\UserBalanceLog;
use App\Models\UsersHistoryProfile;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'enable_user_auth_log',
        'default_user_allow_filter'
    ];

    protected $allowFilteredPage = [
        'admin_user_hidden_columns'
    ];


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $users = User::with('user_balance');
        $users->filter($request->all());


        // По умолчанию, сортировать по ID
        $users->orderByDesc('id');
        $items = $users->paginate(config('exchange.pagination_limit.users'));

        // Отправляем повторно сообщение о верификации пользователя
        if($request->has('verify_email'))
        {
            $user = User::find($request->id);
            if(is_null($user->email_verified_at)) {
                $user->sendEmailVerificationNotification();
                flash('Сообщение успешно отправлено на почту', ['alert alert-success']);
            } else{
                flash('E-mail уже был подтвержден ранее', ['alert alert-danger']);
            }


            return redirect()->back();
        }

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('users.index', $conditions);
        }


        $programs = ReferralProgram::all()->pluck('name', 'id');
        $roles = Role::get();
        $admin_user_hidden_columns = explode(',', iEXSetting('admin_user_hidden_columns'));
        return view('admin.account.users.index', [
            'programs'          =>  $programs,
            'items'             =>  $items,
            'filter'            =>  $request->all(),
            'roles'             =>  $roles,
            'conditions'        =>  [],
            'user_count'        =>  User::count(),
            'pagination_limit'  =>  [
                20,
                30,
                40,
                50,
                100
            ],
            'admin_user_hidden_columns' => $admin_user_hidden_columns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('admin.account.users.create', ['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $conditions = null;
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return \redirect()->route('users.index');
        }

        if($request->has('action') and $request->get('action') == 'settings_columns')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFilteredPage as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        // Экпортирование данных
        if($request->has('export_users') and $request->get('export_users') == 'enable')
        {
            $validate = Validator::make($request->all(), [
                'export_type' => 'required',
                'columns' => 'required'
            ]);

            if($validate->fails()) {
                flash($validate->errors()->first(), ['alert alert-danger']);
                return redirect()->back();
            }

            $class = '\\App\\Exports\\UsersFilterExport';
            $first = Carbon::now()->format('Y_m_d').'_'. random_int(0, 999999);
            $ext = $first.'_users_.'.mb_strtolower($request->get('export_type'));
            Excel::download(new $class($request), $ext, excel_type($request->get('export_type')))->send();

            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6'
        ]);

        if($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->route('users.index');

        } else {
            $user = User::create([
                'name'      =>  $request->get('name'),
                'ip_address'=>  $request->ip(),
                'email'     =>  mb_strtolower($request->get('email')),
                'password'  =>  Hash::make($request->get('password'))
            ]);

            ReferralSystemFacade::register($user, null);
            CashbackFacade::register($user->id);

            if(\auth()->user()->can('admin_roles'))
            {
                $roles = $request['roles'];
                if (isset($roles)) {
                    foreach ($roles as $role) {
                        $role_r = Role::where('id', '=', $role)->firstOrFail();
                        $user->assignRole($role_r);
                    }
                }
            }

            // Отправляем уведомление о регистрационных данных
            if(iEXSetting('is_email_auto_user')) {
                $delay = now()->addSeconds(5);
                dispatch(new RegisterClientJob($user, $request->get('password')))->delay($delay)->onQueue('low');
            }

            return redirect()->route('users.index')
                ->with('flash_message',
                    'User successfully added.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $roles = Role::get();
        $programs = ReferralProgram::all();
        $link = ReferralLink::where('user_id', $id)->first();

        if($request->has('verified') and is_null($user->email_verified_at))
        {
            $user->email_verified_at = Carbon::now();
            $user->save();

            flash($user->email.' успешно верифицирован', ['alert alert-success']);
            return redirect()->back();
        }

        // Провеяем, получены ли ранее резервные ключи
        $checkRecovery = BackupCodeModel::where('id_user', $user->id)->exists();

        return view('admin.account.users.edit',
            compact('user', 'roles','programs', 'link', 'checkRecovery')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if($request->has('edit_referral_hash') and $request->get('edit_referral_hash') == 'enable')
        {
            $validator = Validator::make($request->all(), [
                'referral_name' => 'required|string|max:255|unique:referral_links,code|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            ]);

            if ($validator->fails()) {
                flash($validator->messages()->first(), ['alert alert-danger']);
                return redirect()->back();
            } else {

                $event_value = 'Имя пользователя: <span style="color:red">'.$user->username.'</span> => <span style="color:green">'.$request->referral_name.'</span><br />';
                event_users_history('Изменение', $event_value, $user->id);


                $user->update([
                    'username' => $request->referral_name
                ]);

                ReferralLink::where('user_id', $user->id)->update([
                    'code' => $request->referral_name
                ]);

                return redirect()->back();
            }
        }

        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            'referral'  =>  'required',
            'restriction'  =>  'required'
            //'password'=>'required|min:6'
        ]);

        $event_value = 'Редактирование личных данных.<br />';
        $event_value .= 'Имя: <span style="color:red">'.$user->name.'</span> => <span style="color:green">'.$request->name.'</span><br />';
        $event_value .= 'Email: <span style="color:red">'.$user->email.'</span> => <span style="color:green">'.$request->email.'</span><br />';
        $event_value .= 'Номер: <span style="color:red">'.$user->phone.'</span> => <span style="color:green">'.$request->phone.'</span><br />';


        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->has('phone') ? $request->get('phone') : null;
        $user->admin_phone = $request->has('admin_phone') ? $request->get('admin_phone') : null;
        $user->max_ref_discount = $request->has('max_ref_discount') ? $request->get('max_ref_discount') : 0;
        $user->is_unique_user   =   $request->is_unique_user;
        $user->restriction = $request->restriction;
        $user->is_report_referral_data = ($request->has('is_report_referral_data') ? 1 : 0);
        $user->is_notify_email  =   $request->has('is_notify_email') ? $request->get('is_notify_email') : 0;
        $user->is_vip_client    =   ($request->has('is_vip_client') ? $request->get('is_vip_client') : 0);
        $user->is_enabled_restapi    =   ($request->has('is_enabled_restapi') ? $request->get('is_enabled_restapi') : 0);
        $user->is_password_reset    =   $request->has('is_password_reset') ? $request->get('is_password_reset'): 0;
        $user->is_pay_referral      =   $request->has('is_pay_referral') ? $request->get('is_pay_referral') : 0;
        $user->is_pay_cashback      =   $request->has('is_pay_cashback') ? $request->get('is_pay_cashback') : 0;
        $user->is_verification      =   $request->has('is_verification') ? $request->get('is_verification') : 0;
        $user->deactivation =   $request->deactivation;
        $user->role_expired_at = ($request->has('role_expired_at') ? $request->get('role_expired_at') :  null);
        $user->is_horizon = ($request->has('is_horizon') ? $request->get('is_horizon') : 0);
        $user->is_backup = ($request->has('is_backup') ? $request->get('is_backup') : 0);
        $user->is_order     =   $request->is_order;
        $user->personal_discount = $request->has('personal_discount') ? $request->get('personal_discount') : 0;
        $user->personal_ref_discount = $request->has('personal_ref_discount') ? $request->get('personal_ref_discount') : 0;
        $user->is_follow_referral   =   ($request->has('is_follow_referral') ? $request->get('is_follow_referral') : false);
        $user->is_enable_order_paginate   =   ($request->has('is_enable_order_paginate') ? $request->get('is_enable_order_paginate') : 0);
        $user->is_verify_account        =  ($request->has('is_verify_account') ? $request->get('is_verify_account') : 0);
        $user->is_hidden_ip_address        =  ($request->has('is_hidden_ip_address') ? $request->get('is_hidden_ip_address') : 0);

        if($request->password) {
            $user->password =  Hash::make($request->password);
            $event_value .= 'Пароль: <span style="color:red">***</span> => <span style="color:green">***</span>';
        }

        ReferralLink::where('user_id', $id)->update([
            'referral_program_id'   =>  $request->referral
        ]);

        $roles = $request['roles'];
        $user->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);

            $role_names = Role::whereIn('id', $roles)->get()->map(function($item) {
                return $item['name'];
            })->implode(',');
            $role_expired_at = ($request->has('role_expired_at') ? $request->get('role_expired_at') :  'Не ограничено');
            $event_value .= 'Выдано права: <b>'.$role_names.'</b> до '.$role_expired_at;
            // Записываем в лог информацию об редактировании личных данных
            if(iEXSetting('event_log_is_user_groups')) {
                $event_log = sprintf(
                    'Пользователь который имеет доступ в админпанель выдал права "%s" пользователю %s. Срок действия прав %s',
                    $role_names, $user->name, $role_expired_at
                );
                main_event_log('event_log_is_user_groups', $event_log, $user->id, \auth()->id());
            }
        } else {
            $user->roles()->detach();
        }

        // Записываем в лог информацию об редактировании личных данных
        if(iEXSetting('event_log_is_user_edit')) {
            $event_log = sprintf('Пользователь который имеет доступ в админпанель изменил личные данные %s', $user->name);
            main_event_log('event_log_is_user_edit', $event_log, $user->id, \auth()->id());
        }

        event_users_history('Изменение', $event_value, $user->id);


        flash('Данные успешно обновлены', ['alert alert-success']);

        return redirect(config('admin.directory').'/account/users/'.$id.'/edit')->with('flash_message',
            'User successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with('flash_message',
                'User successfully deleted.');
    }

    /**
     * Список пользовательских заявок
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function application(Request $request)
    {
        $task = Task::limitOutput()->orderBy('id', 'desc');
        $task->where('id_user', $request->id);
        $items = $task->paginate(20);

        return view('admin.account.users.application',[
            'items'         =>  $items,
            'count'         =>  $task->count(),
            'id'            =>  $request->id
        ]);
    }

    /**
     * Заблокировать клиента
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ban(Request $request)
    {
        $user = User::find($request->id);

        if($request->getMethod() == 'POST')
        {
            $user->update([
                'banned_at' =>  $request->expired_at
            ]);

            $user->ban([
                'expired_at' => $request->expired_at,
                'comment'   =>  $request->comment
            ]);

            return redirect()->back();
        }

        return view('admin.account.users.ban',[
            'user'  =>  $user
        ]);
    }

    /**
     * Управление балансом
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function balances(Request $request)
    {
        $user = User::find($request->id);

        if($request->getMethod() == 'POST')
        {
            //Создаем события для уменьшения резерва
            if($request->get('cashback') < $user->user_balance->balance_reward)
            {
                UserBalanceLog::create([
                    'id_manager' => $request->user()->id,
                    'id_user' => $user->id,
                    'route_type' => 0,
                    'type' => 0,
                    'text' =>  'Cashback уменьшен с '.$user->user_balance->balance_reward.' на '.$request->get('cashback'),
                    'from_balance' => $user->user_balance->balance_reward,
                    'to_balance' => $request->get('cashback')
                ]);
            }

            if($request->get('cashback') > $user->user_balance->balance_reward)
            {
                UserBalanceLog::create([
                    'id_manager' => $request->user()->id,
                    'id_user' => $user->id,
                    'route_type' => 0,
                    'type' => 1,
                    'text' =>  'Cashback увеличен с '.$user->user_balance->balance_reward.' на '.$request->get('cashback'),
                    'from_balance' => $user->user_balance->balance_reward,
                    'to_balance' => $request->get('cashback')
                ]);
            }


            if($request->get('referral') < $user->user_balance->balance)
            {
                UserBalanceLog::create([
                    'id_manager' => $request->user()->id,
                    'id_user' => $user->id,
                    'route_type' => 1,
                    'type' => 0,
                    'text' =>  'Реферальный бонус уменьшен с '.$user->user_balance->balance.' на '.$request->get('referral'),
                    'from_balance' => $user->user_balance->balance,
                    'to_balance' => $request->get('referral')
                ]);
            }

            if($request->get('referral') > $user->user_balance->balance)
            {
                UserBalanceLog::create([
                    'id_manager' => $request->user()->id,
                    'id_user' => $user->id,
                    'route_type' => 1,
                    'type' => 1,
                    'text' =>  'Реферальный бонус увеличен с '.$user->user_balance->balance.' на '.$request->get('referral'),
                    'from_balance' => $user->user_balance->balance,
                    'to_balance' => $request->get('referral')
                ]);
            }

            $user->user_balance->update([
                'balance_reward'   =>  ($request->has('cashback') ? $request->get('cashback') : $user->user_balance->balance_reward),
                'balance'           =>  ($request->has('referral') ? $request->get('referral') : $user->user_balance->balance),
            ]);

            return redirect()->back();
        }

        $logs = UserBalanceLog::where([
            ['id_user', '=', $request->id]
        ])->orderByDesc('id')->paginate(20);

        return view('admin.account.users.balances', [
            'user'  =>  $user,
            'logs' => $logs
        ]);
    }

    /**
     * Разблокировать клиента
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function revokeUser(Request $request)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $user = User::find($request->id);
        $user->unban();

        return redirect()->back();
    }

    /**
     * Рефералы пользователя
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function referral($id, Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            foreach ($request->redirects as $key => $value)
            {
                if($value > 0)
                {
                    $r_referral = ReferralLink::where('user_id', $value)->first();
                    ReferralRelationship::find($key)->update([
                        'referral_link_id'  =>  $r_referral->id
                    ]);

                    ReferralLog::where('id_user', $id)->update([
                        'id_referral_link' => $r_referral->id
                    ]);
                }
            }

            return redirect()->back();
        }

        $referral_link = ReferralLink::where('user_id', $request->id)->first();
        $items = ReferralRelationship::select(['user_id','id'])->whereHas('user')
            ->where('referral_link_id', $referral_link->id)->orderBy('id', 'desc')
            ->paginate(20);


        // Список избранных рефералов
        $follow_referrals = User::where('is_follow_referral', '=', 1)->get();

        return view('admin.account.users.referral', [
            'items' =>  $items,
            'id_user'   =>  $request->id,
            'follow_referrals' => $follow_referrals
        ]);

    }

    /**
     * Подробная статистика пользователя
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function statistic(Request $request)
    {
        return view('admin.account.users.statistic');
    }

    public function google(Request $request)
    {
        if(config('admin.is_demo_mode') == true) {
            return redirect()->back();
        }

        $user = User::find($request->id);
        $google2fa = app('pragmarx.google2fa');
        $user->google2fa_secret = $google2fa->generateSecretKey();
        $user->save();

        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $user->google2fa_secret
        );

        return view('admin.account.users.google',[
            'user'      =>  $user,
            'QR_Image'  =>  (isset($QR_Image) ? $QR_Image : null),
            'secret'    =>  (isset($user->google2fa_secret) ? $user->google2fa_secret : null),
        ]);
    }

    /**
     * Реферальная статистика
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function referralStatistics(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $start_year = Carbon::now()->startOfYear();
        $end_year = Carbon::now()->endOfYear();
        $interval = new \DatePeriod(
            $start_year,
            CarbonInterval::month(),
            $end_year
        );


        $referral_link = ReferralLink::where('user_id', $id)->first();
        $referral_ship = ReferralRelationship::where([
            ['referral_link_id', $referral_link->id]
        ])->whereBetween('created_at', [$start_year, $end_year])
            ->get()->groupBy(function($item) {
                return $item->created_at->format('Y-m');
            })->map(function ($item) {
                $last_item = Arr::last($item->toArray());

                $minBonus = ReferralLog::whereBetween('created_at', [
                    Carbon::parse($last_item['created_at'])->startOfMonth(),
                    Carbon::parse($last_item['created_at'])->endOfMonth()
                ])->where('id_referral_link', $last_item['referral_link_id'])
                    ->orderBy('bonus_number', 'asc')->first();

                $maxBonus = ReferralLog::whereBetween('created_at', [
                    Carbon::parse($last_item['created_at'])->startOfMonth(),
                    Carbon::parse($last_item['created_at'])->endOfMonth()
                ])->where('id_referral_link', $last_item['referral_link_id'])
                    ->orderBy('bonus_number', 'desc')->first();

                // Последняя заявка
                $last_order = Task::where('id_user','=', $last_item['user_id'])
                    ->where('status', '=', 4)->orderByDesc('id')->first();

                return [
                    'count' => $item->count(),
                    'last_order'    => isset($last_order) ? $last_order : null,
                    'min_bonus'     =>  $minBonus,
                    'max_bonus'     =>  $maxBonus
                ];
            });

        //Заработок с рефералов (Получение графики)
        $referral_log_amount = ReferralLog::where('id_user', $id)
            ->whereBetween('created_at', [$start_year, $end_year])
            ->get()
            ->groupBy(function($item) {
                return $item->created_at->format('Y-m');
            })->map(function ($month) {
                return $month->sum('bonus_number');
            })->toArray();


        $array_amount = [];
        $referral_user = [];
        foreach ($interval as $date) {
            $day = $date->format('Y-m');
            $array_amount[] = [
                'snapshot' => Carbon::parse($day)->translatedFormat('F Y'),
                'earned' => (isset($referral_log_amount[$day]) ? $referral_log_amount[$day] : 0),
                'attracted'  => (isset($referral_ship[$day]['count']) ? $referral_ship[$day]['count'] : 0),
                'last_order' => (isset($referral_ship[$day]['last_order']) ? $referral_ship[$day]['last_order'] : null),
                'min_bonus' =>  (isset($referral_ship[$day]['min_bonus']) ? $referral_ship[$day]['min_bonus'] : null),
                'max_bonus' =>  (isset($referral_ship[$day]['max_bonus']) ? $referral_ship[$day]['max_bonus'] : null),
            ];
        }

        return view('admin.account.users.referral_statistics', [
            'user' => $user,
            'array_amount' => $array_amount,
            'referral_user' =>  $referral_user
        ]);
    }

    /**
     * История блокировок
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bannedHistories($id)
    {
        $user = User::findOrFail($id);
        $histories = $user->bans()->paginate(20);

       return view('admin.account.users.banned_histories', compact('user', 'histories'));
    }

    /**
     * История профилей
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history_profiles()
    {
        $histories = UsersHistoryProfile::orderByDesc('id')->paginate(20);
        return view('admin.account.users.history_profiles', compact('histories'));
    }

    /**
     * Загружаем заявки клиентов
     *
     * @param int $id
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function download_order(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $first = Carbon::now()->format('Y_m_d').'_'. random_int(0, 999999);
        return Excel::download(new OrderUserExport($id), $first.'.xls', excel_type('XLS'))->send();
    }
}
