<?php
namespace App\Http\Controllers\Administrator\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            if(count($request->type_balance) > 0)
            {
                DB::table('code_currency')->whereIn('id', $request->type_balance)->update(['balance' => 1]);
                DB::table('code_currency')->whereNotIn('id', $request->type_balance)->update(['balance' => 0]);
            }else {
                DB::table('code_currency')->update(['balance' => 0]);
            }

            return redirect(config('admin.directory').'/account/settings');
        }

        return view('admin.account.settings');
    }
}