<?php
namespace App\Http\Controllers\Administrator\Account;

use App\Http\Controllers\Controller;
use App\Models\CodeCurrency;
use App\Models\User;

class InternalAccountController extends Controller
{

    public function index(int $id)
    {
        $user = User::findOrFail($id);
        $codes = CodeCurrency::all();

        return view('admin.account.internal_account.index', [
            'codes' => $codes,
            'user' => $user
        ]);
    }
}
