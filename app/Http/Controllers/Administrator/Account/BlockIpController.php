<?php
namespace App\Http\Controllers\Administrator\Account;

use App\Http\Controllers\Controller;
use App\Models\Banned;
use Illuminate\Http\Request;

class BlockIpController extends Controller
{
    /**
     * Фильтр по IP, Email
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        // Список заблокированных пользователей
        $users = Banned::paginate(20);
        $findById  = ['filter_name' => '', 'expired_at' => '', 'description' => null];

        if(isset($request->id)) {
            $findById = Banned::find($request->id);
        }

        if(isset($request->delete_id)) {
            $exists = Banned::find($request->delete_id);
            if(!empty($exists)) $exists->delete();

            return redirect()->back();
        }

        if($request->getMethod() == 'POST')
        {
            $filter_name = trim(mb_strtolower($request->get('filter_name')));
            $filterExists = Banned::where('filter_name', $filter_name);


            if($request->edit == true) {
                Banned::find($request->id)->update([
                    'description'   => $request->get('description'),
                    'expired_at'    => $request->get('expired_at'),
                    'filter_name'   => $filter_name
                ]);
            } else {
                if(!$filterExists->exists()) {
                    Banned::create([
                        'description'   => $request->get('description'),
                        'expired_at'    => $request->get('expired_at'),
                        'filter_name'   => $filter_name
                    ]);
                } else {
                    $filterExists->update([
                        'description'   => $request->get('description'),
                        'expired_at'    => $request->get('expired_at'),
                    ]);
                }
            }

            return redirect(config('admin.directory').'/account/block_ip');
        }

       return view('admin.account.block_ip.index', [
           'banned_list'  =>  $users,
           'editData'   =>  $findById
       ]);
    }
}