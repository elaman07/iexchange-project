<?php
namespace App\Http\Controllers\Administrator\Account;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PayoutsController extends Controller
{
    /**
     * Главная страница
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Список выплат
        $payouts = WithdrawalRequest::where('id_user', $request->id)->orderByDesc('id');
        $user = User::find($request->id);


        return view('admin.account.users.payouts.index', [
            'payouts'   =>  $payouts->paginate(20),
            'user'      =>  $user,
            'is_payouts'    =>  $payouts->where('status', '=',0)->exists(),
            'currencies' => Currency::whereIn('id', $this->getAvailableIdsCurrency($request->id))->get()
        ]);
    }

    /**
     * Создаем заявку на выплату
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_currency' => 'required',
            'score' => 'required|max:1000',
            'purpose' => 'required|string|max:100'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $user = User::find($request->id);
            $purpose = $this->getPurpose($request->purpose);
            $balance = $this->getBalance($purpose, $request->id_currency, $user);

            WithdrawalRequest::create([
                'id_user'               => $request->id,
                'id_currency'           => $request->id_currency,
                'referral'              => $purpose['referral'],
                'reward'                => $purpose['cashback'],
                'score'                 => $request->score,
                'balance_referral'      => $balance['referral'],
                'balance_reward'        => $balance['reward'],
                'base_referral'         =>  $balance['base_referral'],
                'base_reward'           =>  $balance['base_reward'],
                'ip'                    =>  $request->ip()
            ]);

            $this->cleanBalance($purpose, $request->id);
            return redirect()->back();
        }
    }

    /**
     * Обнуляем баланс
     *
     * @param $purpose
     */
    protected function cleanBalance($purpose, $id)
    {
        $balance_array = [];
        if($purpose['referral'] == 1) {
            $balance_array['balance'] = 0;
        }

        if($purpose['cashback'] == 1) {
            $balance_array['balance_reward'] = 0;
        }

        //Обновление цен
        UserBalance::where('id_user', $id)
            ->update($balance_array);
    }

    /**
     * Получить тип запроса
     *
     * @param $purpose
     * @return array
     */
    protected function getPurpose($purpose)
    {
        $referral = ($purpose == 'referral' ? 1 : 0);
        $cashback = ($purpose == 'cashback' ? 1 : 0);

        if(in_array($purpose, ['referral_and_cashback', 'cashback_and_referral'])) {
            $referral = 1;
            $cashback = 1;
        }

        return [
            'referral'  => $referral,
            'cashback'  =>  $cashback
        ];
    }

    /**
     * Получение баланса
     *
     * @param $purpose
     * @param $id
     * @param User $user
     * @return array
     */
    protected function getBalance($purpose, $id, User $user)
    {
        $currency = Currency::find($id);

        $price_referral = 0;
        $price_reward = 0;
        $base_referral = 0;
        $base_reward = 0;

        // Выплата реферальных бонусов
        if($purpose['referral'] == 1) {
            if(mb_strtolower($currency->code_currency->name) == 'usd') {
                $price_referral = (float)convert_to_usd('RUB', $user->user_balance->balance);
            } else {
                $price_referral = (float)$user->user_balance->balance;
            }
            $base_referral = (float)$user->user_balance->balance;
        }

        // Выплата вознаграждений
        if($purpose['cashback'] == 1) {
            if(mb_strtolower($currency->code_currency->name) == 'usd') {
                $price_reward = (float)convert_to_usd('RUB', $user->user_balance->balance_reward);
            } else {
                $price_reward = (float)$user->user_balance->balance_reward;
            }
            $base_reward = (float)$user->user_balance->balance_reward;
        }

        return [
            'referral'      =>  $price_referral,
            'reward'      =>  $price_reward,
            'combine'       =>  ($price_referral + $price_reward),
            'base_referral' =>  $base_referral,
            'base_reward'   =>  $base_reward
        ];
    }

    /**
     * Список доступных платежных систем
     *
     * @param $id
     * @return array
     */
    protected function getAvailableIdsCurrency($id)
    {
        $users = User::where('is_unique_user', '=', 1)->pluck('id')->toArray();
        if(in_array($id, $users)) {
            return Currency::where('is_payment_unique', '=',1)->pluck('id')->toArray();
        } else {
            return Currency::where('is_payment_default', '=',1)->pluck('id')->toArray();
        }
    }
}