<?php
namespace App\Http\Controllers\Administrator\Account;

use App\Common\Packages\BackupCode\BackupCode;
use App\Exports\BackupCodesExport;
use App\Http\Controllers\Controller;
use App\Models\BackupCodeModel;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BackupCodesController extends Controller
{

    /**
     * Конструктор
    */
    public function __construct()
    {
        if(config('admin.is_demo_mode') == true) {
            return redirect()->back();
        }
    }

    public function codes(int $id, Request $request)
    {
        $user = User::findOrFail($id);
        $codes = BackupCodeModel::where('id_user', $user->id)->count();

        // Если кодов нет, создаем
        if($codes == 0) {
            $list = new BackupCode();
            $array = $list->setLength(9)->setCount(25)->generate()->toArray();

            foreach ($array as $key => $value) {
                BackupCodeModel::create([
                    'id_user' => $user->id,
                    'num'   =>  $key,
                    'code'  =>  $value
                ]);
            }

            return redirect()->to(admin_base_path('/account/users/'.$id.'/backup_codes'));
        }


        // Разрешаем скачивать резервные коды один раз
        if($request->has('download')) {

            if($user->is_download_codes == 1) {
                flash('Невозможно скачать коды', ['alert alert-danger']);
                return redirect()->back();
            }

            Excel::download(new BackupCodesExport($id),'backup-codes-'.time().'.xls')->send();
            $user->update(['is_download_codes' => 1]);
        }

        return view('admin.account.users.backup_codes.codes', [
            'user' => $user,
            'codes' => $codes
        ]);
    }

    /**
     * Скачать полученные коды в PDF формате
     *
     * @param int $id
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function download(int $id) {

        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }


        return (new BackupCodesExport($id))->download('backup-codes-'.time().'.pdf', \Maatwebsite\Excel\Excel::MPDF);
    }
}
