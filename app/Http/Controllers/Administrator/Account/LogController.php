<?php
namespace App\Http\Controllers\Administrator\Account;

use App\Http\Controllers\Controller;
use App\Models\AdminAuthLog;
use App\Models\UserAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LogController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('clear_logs'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            \DB::table('user_auth')->delete();
            flash(__('Лог успешно очищен'), ['alert alert-success']);
            return redirect()->back();
        }


        $logs = UserAuth::filter($request->all())->orderBy('id', 'desc')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(), ['send_action']);
            return \redirect()->route('admin.account.logs_auth', $conditions);
        }

        $statusses = [
            0 => 'Успешная авторизация',
            1 => 'Неудачная авторизация'
        ];


        return view('admin.account.log.index', [
            'logs' => $logs,
            'filter' => $request->all(),
            'statusses' => $statusses
        ]);
    }

    /**
     * Лог авторизаций в админпанели
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function admin_logs(Request $request)
    {
        $logs = AdminAuthLog::filter($request->all())->orderByDesc('id')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(), ['send_action']);
            return \redirect()->route('admin.account.admin_logs_auth', $conditions);
        }

        return view('admin.account.log.admin_log', [
            'logs' => $logs,
            'filter' => $request->all()
        ]);
    }
}
