<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 21.10.2017
 * Time: 21:31
 */

namespace App\Http\Controllers\Administrator;


use App\Common\Support\Traits\AdminAuthenticatesUsers;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AdminAuthenticatesUsers;


    /**
     * Создайте новый экземпляр контроллера.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function redirectTo() {
        return config('admin.directory');
    }

    public function index()
    {
        return view('admin.login');
    }
}