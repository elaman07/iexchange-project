<?php
namespace App\Http\Controllers\Administrator\Settings;

use App\Common\Packages\ProxiesFilter\ProxiesFilter;
use App\Http\Controllers\Controller;
use App\Mail\TestMailRequest;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\FileStorageModel;
use App\Models\Reserve;
use App\Models\TaskStatus;
use App\Services\Settings\SettingsService;
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Nwidart\Modules\Facades\Module;
use Telegram\Bot\Laravel\Facades\Telegram;

class SettingController extends Controller
{
    /***
     * Настройки системы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     * @throws \Exception
     */
    public function index(Request $request)
    {

        if($request->has('reload_ip_address'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            $loader = new ProxiesFilter();
            Cache::forever('proxyfilter.allowed', $loader->load());
            flash('IP Адреса обновлены', ['alert alert-success']);
            return redirect()->to(
                admin_base_path('/settings')
            );
        }

        if($request->has('service_api') and $request->get('service_api') == 'telegram')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            // Получение ID Канала
            if($request->has('type_api_service') and $request->get('type_api_service') == 'get_id_channel')
            {
                $api = json_decode(file_get_contents('https://api.telegram.org/bot'.config('telegram.bots.notice.token').'/getUpdates'), true);

                if(!empty($api['result']))
                {
                    $api_result = collect($api['result'])->filter(function ($item) {
                        return isset($item['channel_post']);
                    })->first();

                    if(!empty($api_result)) {
                        flash('Ваш ID Канала: '.$api_result['channel_post']['chat']['id'], ['alert alert-success']);
                    } else {
                        flash('ID Канала не найден', ['alert alert-danger']);
                    }
                } else {
                    flash('ID Канала не найден', ['alert alert-danger']);
                }

                return redirect()->to(admin_base_path('/settings?mid=telegramservice'));
            }

            // Регистрация клиентского бота
            if($request->has('type_api_service') and $request->get('type_api_service') == 'register_client_bot')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                iEXSetting(['is_user_telegram_status' => 1])->save();
                try {
                    $url = config('app.url').'/telegram-bot/'.config('telegram.prefix').'/client';
                    $response = Telegram::bot('iex')->setWebhook([
                        'url' => $url
                    ]);
                    flash('Клиентский бот зарегистрирован', ['alert alert-success']);
                }catch (\Exception $exception) {
                    flash('Ошибка: '.$exception->getMessage(), ['alert alert-danger']);
                }


                return redirect()->to(admin_base_path('/settings?mid=telegramservice'));
            }

            // Регистрация администраторского бота
            if($request->has('type_api_service') and $request->get('type_api_service') == 'register_admin_bot')
            {
                if(config('admin.is_demo_mode')) {
                    flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                    return redirect()->back();
                }

                iEXSetting(['is_user_telegram_status' => 1])->save();
                try {
                    $url = config('app.url').'/telegram-bot/'.config('telegram.prefix').'/admin';
                    $response = Telegram::bot('admin')->setWebhook([
                        'url' => $url
                    ]);
                    flash('Администраторский бот зарегистрирован', ['alert alert-success']);
                }catch (\Exception $exception) {
                    flash('Ошибка: '.$exception->getMessage(), ['alert alert-danger']);
                }


                return redirect()->to(admin_base_path('/settings?mid=telegramservice'));
            }

        }


        // Отправить тестовую sms сообщение
        if($request->has('test') and $request->test == 'send_mail')
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            try {
                Mail::to(iEXSetting('project_email_support'))
                    ->send(new TestMailRequest());

                flash('Сообщение успешно отправлено', ['alert alert-success']);
            }catch (\Exception $exception) {
                flash($exception->getMessage(), ['alert alert-danger']);
            }
            return redirect()->to(admin_base_path('/settings'));
        }

        // Расширяем основную конфигурацию
        foreach(Module::getCached() as $value)
        {
            $c_data = config($value['alias'].'.settings');
            if(isset($c_data['category']))
            {
                Config::set('admin-settings.categories.'.$value['alias'], [
                    'route' => $value['alias'],
                    'icon' => $c_data['category']['icon'],
                    'title' => $c_data['category']['title'],
                    'is_module' => true
                ]);
            }

            if(isset($c_data['updates'])) {
                Config::set('admin-settings.updates.'.$value['alias'], $c_data['updates']);
            }
        }


        if($request->has('visible_menu'))
        {
            if(config('admin.is_demo_mode')) {
                flash('В demo версии данная функция недоступна', ['alert alert-danger']);
                return redirect()->back();
            }

            if ($request->get('visible_menu') == 'enable') {
                iEXSetting(['admin_settings_is_visible_menu' => 1])->save();
            } else {
                iEXSetting(['admin_settings_is_visible_menu' => 0])->save();
            }

            flash('Настройки были успешно сохранены', 'alert alert-success');
            return redirect()->to(admin_base_path('/settings'));
        }

        if($request->getMethod() == 'POST')
        {
            $mid = $request->has('mid') ? $request->get('mid') : 'main';
            $service = new SettingsService($mid, $request);
            $response = $service->getUpdate();


            if(isset($response['status'])) {
                flash($response['message'], 'alert alert-danger');
            } else {
                flash('Настройки были успешно сохранены', 'alert alert-success');
            }
            return redirect()->to(admin_base_path('/settings?mid='.$mid));
        }

        //Статусы заявок
        $statuses = TaskStatus::all()->pluck('name','id');

        $settings_label = config('admin-settings.categories');


        $selected_setting = $settings_label['main'];
        $config = [];
        $storages_files = [];
        if($request->has('mid'))
        {
            $selected_setting = (isset($settings_label[$request->get('mid')]) ? $settings_label[$request->get('mid')] : $settings_label['main']);

            if($request->get('mid') == 'bsblacklist')
            {
                $config = [
                    'categories' => [
                        'ip' => 'По IP',
                        'email' => 'По E-mail',
                        'wallet_from' => 'По счету (Отдаю)',
                        'wallet_to' => 'По счету (Получаю)',
                    ],
                    'types' => [
                        'sc' => 'По всем типам',
                        's' => 'Среди мошенников',
                        'c' => 'Среди неадекватов'
                    ],
                    'columns' => [
                        'cd' => 'По всем параметрам',
                        'c' => 'В контактах',
                        'd' => 'В описании'
                    ]
                ];
            }

            if($request->get('mid') == 'images') {
                $storages_files = FileStorageModel::all();
            }
        }

        $exchange_main_data = [];
        if($request->has('mid') and $request->get('mid') == 'currencyreserve')
        {
            // Список направлений активных
            $exchange_main_data = DirectionExchange::select('id','tech_name', 'is_main', 'status')->get()->map(function($item) {
                return [
                    'id' => $item->id,
                    'is_main' => $item->is_main,
                    'name' => $item->tech_name,
                    'status' => $item->status
                ];
            });

        }


        $currencies = Currency::active()->get();

        return view('admin.settings.index', [
            'config' => $config,
            'storages_files'    =>  $storages_files,
            'selected_setting' => $selected_setting,
            'setting'   =>  iEXSetting(),
            'statuses' =>  $statuses,
            'reserve'   =>  Reserve::all(),
            'currencies'  =>  Currency::where('status', 0)->get(),
            'currencies_reserves' => collect($currencies)->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->payment->name.' '.$item->code_currency->name
                ];
            })->pluck('name','id'),
            'exchange_main' => $exchange_main_data
        ]);
    }

    /***
     * Настройка SMS сервисоам
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function sms(Request $request)
    {
        if($request->method() == 'POST') {
            $env = new DotenvEditor();
            $env->changeEnv([
                'NEXMO_API_KEY'    =>  ($request->has('nexmo_api_sms_key') ? $request->get('nexmo_api_sms_key') : null),
                'NEXMO_API_SECRET'    =>  ($request->has('nexmo_api_sms_secret') ? $request->get('nexmo_api_sms_secret') : null),
                'NEXMO_API_SMS_FROM'    =>  ($request->has('nexmo_api_sms_from') ? $request->get('nexmo_api_sms_from') : null),
                'SMSC_LOGIN'    =>  ($request->has('smsc_sms_login') ? $request->get('smsc_sms_login') : null),
                'SMSC_SECRET'    =>  ($request->has('smsc_sms_password') ? $request->get('smsc_sms_password') : null),
                'SMSC_SENDER'    =>  ($request->has('smsc_sms_from') ? $request->get('smsc_sms_from') : null)
                ]);

            flash('Настройки SMS успешно сохранены', 'alert alert-success');
            return redirect()->back();
        }

        return view('admin.settings.sms');
    }

    /**
     * SMS Шаблоны
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function smsTemplate(Request $request)
    {
        if($request->method() == 'POST')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->attributes_sms as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки шаблонов успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        return view('admin.settings.sms_template');
    }
}
