<?php
namespace App\Http\Controllers\Administrator\Settings;

use App\Http\Controllers\Controller;
use App\Models\MainEventLog;
use Illuminate\Http\Request;

class EventController extends Controller
{
    protected $attributes = [
        'event_log_is_enabled',
        'event_log_cleanup_days',
        'event_log_is_logout',
        'event_log_is_login_success',
        'event_log_is_login_fail',
        'event_log_is_register',
        'event_log_is_password_request',
        'event_log_is_password_change',
        'event_log_is_user_edit',
        'event_log_is_user_groups',
        'event_log_user_profile_history'
    ];

    /**
     * Получаем список всех событий
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function event_logs(Request $request)
    {
        if($request->method() == 'POST')
        {
            $options = [];
            foreach ($this->attributes as $item)
            {
                if($request->has($item) and is_array($request->get($item))) {
                    $options[$item] = implode(',', $request->get($item));
                } else {
                    $options[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($options);
            flash('Настройки журнала успешно сохранены', 'alert alert-success');

            return redirect()->back();
        }

        $events = MainEventLog::orderByDesc('id')->paginate(20);

        return view('admin.settings.instruments.event_logs', [
            'events' => $events
        ]);
    }
}