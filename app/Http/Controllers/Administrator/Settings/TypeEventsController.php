<?php
namespace App\Http\Controllers\Administrator\Settings;

use App\Http\Controllers\Controller;
use App\Models\TemplateTypeEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypeEventsController extends Controller
{

    public function index()
    {
        $events = TemplateTypeEvent::orderBy('id')->get();
        return view('admin.settings.menu-templates.type-events.index', compact('events'));
    }

    public function create()
    {
        return view('admin.settings.menu-templates.type-events.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_name' => ['required', 'unique:template_type_events,event_name'],
            'name'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        }

        TemplateTypeEvent::create([
            'event_name' => $request->get('event_name'),
            'event_type' => $request->has('event_type') ? $request->get('event_type') : 0,
            'name'  =>  $request->get('name'),
            'description' => $request->has('description') ? $request->get('description') : null,
            'id_user' => $request->user()->id
        ]);

        flash('Тип события успешно добавлен', ['alert alert-success']);
        return redirect()->back()->withInput();
    }

    /**
     * Интерфейс типов событий
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $event = TemplateTypeEvent::find($id);
        return view('admin.settings.menu-templates.type-events.edit', compact('event'));
    }

    /**
     * Обновление типа событий
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        }

        TemplateTypeEvent::find($id)->update([
            'name' => $request->get('name'),
            'event_type' => $request->has('event_type') ? $request->get('event_type') : 0,
            'description' => $request->has('description') ? $request->get('description') : null,
        ]);

        flash('Тип события успешно обновлен', ['alert alert-success']);
        return redirect()->back()->withInput();
    }

    /**
     * Удаление событий
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        $item = TemplateTypeEvent::find($id);

        if($item->email_template->count() > 0) {
            flash('Ошибка при удаление событий, к событию привязаны шаблоны', ['alert alert-danger']);
            return redirect()->back();
        }

        $item->delete();
        flash('Тип события успешно удален', ['alert alert-success']);
        return redirect()->back()->withInput();
    }
}