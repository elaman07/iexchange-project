<?php
namespace App\Http\Controllers\Administrator\Settings;

use App\Http\Controllers\Controller;
use App\Models\{EmailTemplate, TemplateTypeEvent};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmailTemplatesController extends Controller
{

    public function index()
    {
        $templates = EmailTemplate::orderBy('id')->get();
        return view('admin.settings.menu-templates.email.index', compact('templates'));
    }

    public function create()
    {
        $events = TemplateTypeEvent::where('event_type', 0)->get();

        return view('admin.settings.menu-templates.email.create', compact('events'));
    }

    /**
     * Сохранение шаблона
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_event_type' => ['required', 'unique:email_templates,id_event_type'],
            'subject'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        }

        EmailTemplate::create([
            'id_event_type' => $request->get('id_event_type'),
            'subject' => $request->has('subject') ? $request->get('subject') : null,
            'email_to' => $request->has('email_to') ? $request->get('email_to') : null,
            'status' => $request->has('status') ? $request->get('status') : 0,
            'id_user' => $request->user()->id
        ]);

        flash('Шаблон успешно создан', ['alert alert-success']);
        return redirect()->back();
    }

    public function edit(int $id)
    {
        $template = EmailTemplate::findOrFail($id);

        return view('admin.settings.menu-templates.email.edit', compact('template'));
    }

    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        }


        EmailTemplate::find($id)->update([
            'subject' => $request->get('subject'),
            'text_template' => $request->has('text_template') ? $request->get('text_template') : null,
            'status' => $request->has('status') ? 1 : 0,
            'email_to' => $request->has('email_to') ? $request->get('email_to') : null,
            'template' => $request->has('template') ? $request->get('template') : 0
        ]);

        flash('Шаблон успешно обновлен', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Удаление шаблона
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id): \Illuminate\Http\RedirectResponse
    {
        $item = EmailTemplate::find($id);

        $item->delete();
        flash('Шаблон успешно удален', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('/settings/menu-templates/email-template')
        );
    }
}
