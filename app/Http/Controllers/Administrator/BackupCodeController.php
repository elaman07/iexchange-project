<?php

namespace App\Http\Controllers\Administrator;

use App\Common\Packages\BackupCode\BackupCodeHandler;
use App\Http\Controllers\Controller;
use App\Jobs\TelegramAdminAuthJob;
use App\Jobs\TelegramBlockedUserJob;
use App\Models\BackupCodeModel;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BackupCodeController extends Controller
{
    use BackupCodeHandler;


    /**
     * Главная страница для ввода резервного кода
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Временно кэшируем код, чтобы зафиксировать №
        $code = $this->getCurrentCode($request);
        return view('admin.backup_codes', compact('code'));
    }

    /**
     * Обработка введенного резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, RateLimiter $limiter)
    {
        $validator = \Validator::make($request->all(), [
            'code' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            // Для начала проверяем установленный лимит
            if ($this->hasTooManyRecoveryAttempts($request, $limiter))
            {
                $text = $this->sendLockoutResponse($request, $limiter);

                if(iEXSetting('is_notify_telegram_blocked_client')) {
                    $delay = now()->addMinutes(1);
                    dispatch(new TelegramBlockedUserJob($request->user(),"{$request->user()->name}-Резервный Код: {$text}"))
                        ->delay($delay)
                        ->onQueue('low');
                }

                flash($text, ['alert alert-danger']);
                return back();
            }

            // Увеличиваем кол-во попыток
           $this->incrementAttempts($request, $limiter);

            if($this->attemptRecovery($request)) {
                $this->sendRecoveryResponse($request, $limiter);

                return redirect()->to(admin_base_path('/'));
            } else {
                if(iEXSetting('is_failed_admin_auth')) {
                    $delay = now()->addMinutes(1);
                    dispatch(new TelegramAdminAuthJob($request->user(), false, 'Резервный код'))
                        ->delay($delay)
                        ->onQueue('low');
                }
                flash('Неверный код', ['alert alert-danger']);
            }


            return redirect()->back();
        }
    }

    /**
     * В случае удачного ввода резервного кода
     *
     * @param  \Illuminate\Http\Request $request
     * @param RateLimiter $limiter
     * @return void
     */
    protected function sendRecoveryResponse(Request $request, RateLimiter $limiter)
    {
        $this->clearAttempts($request, $limiter);
        $this->insertCode($request);
    }

    /**
     * Проверка кода
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptRecovery(Request $request)
    {
        return $this->checkedCode($request);
    }
}