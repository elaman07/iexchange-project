<?php
namespace App\Http\Controllers\Administrator;

use App\Common\Packages\Payment\Facades\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Events\CoursesFileEvent;
use App\Http\Controllers\AbstractController;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\DirectionExchangeCity;
use App\Models\DirectionExchangePercentAmount;
use App\Models\Requisites;
use App\Models\Reserve;
use App\Models\ReserveLog;
use App\Models\Review;
use App\Models\Task;
use App\Models\User;
use App\Models\VerificationCard;
use App\Models\WalletTransaction;
use App\Models\WithdrawalRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Throwable;

class PrivateController extends AbstractController
{
    public function directionExchangePercentAmount(Request $request)
    {
        $actions = $request->has('actions') ? $request->get('actions') : 0;


        if($actions == 'update') {
            $percentage_amount_id = security_xss((int)$request->get('percentage_amount_id'));
            $direction_id = security_xss((int)$request->get('direction_id'));
            $exchange_amount = security_xss((int)$request->get('exchange_amount'));
            $exchange_percentage = security_xss($request->get('exchange_percentage'));

            $direction_exchange = DirectionExchangePercentAmount::where([
                ['id_direction_exchange', $direction_id],
                ['id', $percentage_amount_id]
            ])->update([
                'percentage' => (string)$exchange_percentage,
                'amount' => $exchange_amount,
            ]);
        }elseif($actions == 'trash')
        {
            $direction_id = security_xss((int)$request->get('direction_id'));
            $percentage_amount_id = security_xss((int)$request->get('percentage_amount_id'));

            \Log::debug(json_encode($direction_id));

            $direction_exchange = DirectionExchangePercentAmount::where([
                ['id_direction_exchange', $direction_id],
                ['id', $percentage_amount_id]
            ])->delete();
        }

        return response()->json([
            'status' => 0
        ]);
    }


    public function directionExchangeCities(Request $request)
    {
        $actions = $request->has('actions') ? $request->get('actions') : 0;


        if($actions == 'update') {
            $direction_id = security_xss((int)$request->get('direction_id'));
            $city_id = security_xss((int)$request->get('city_id'));
            $add_comm = security_xss($request->get('add_comm'));
            $param = security_xss($request->get('param'));
            $min_price = security_xss($request->get('min_price'));
            $max_price = security_xss($request->get('max_price'));
            $id_country = security_xss($request->get('id_country'));

            $direction_exchange = DirectionExchangeCity::where([
                ['id_direction_exchange', $direction_id],
                ['id', $city_id]
            ])->update([
                'add_comm' => (string)$add_comm,
                'param' => $param,
                'min_price' => $min_price,
                'max_price' => $max_price,
                'id_country'    => $id_country
            ]);
        }elseif($actions == 'trash')
        {
            $direction_id = security_xss((int)$request->get('direction_id'));
            $city_id = security_xss((int)$request->get('city_id'));
            $direction_exchange = DirectionExchangeCity::where([
                ['id_direction_exchange', $direction_id],
                ['id', $city_id]
            ])->delete();
        }



        return response()->json([
            'status' => 0
        ]);
    }

    /**
     * Получение списка необходимых настроек для операторов
    */
    public function getOperatorSettings()
    {
        $arrays = [];
        $array['requisites'] = [];

        $requisites = Requisites::with(['currency'])
            ->whereHas('currency', function($query) {
            $query->whereIn('status', [0, 1]);
        })->where([
            ['generate_address', 0],
            ['is_history','=', 0],
            ['id_currency', config('payment.id_sberbank')],
            ['status', 1]
        ])->first();

        if(!empty($requisites)) {
            $list = Requisites::with(['currency'])->whereHas('currency', function($query) {
                $query->whereIn('status', [0, 1]);
            })->where([
                ['generate_address',0],
                ['is_history','=',0],
                ['id_currency', $requisites->id_currency],
            ])->orderBy('id_currency')->get();

            foreach ($list as $item) {
                array_push($arrays, [
                    'id'    =>  $item->id,
                    'address'   =>  $item->account_number,
                    'status'    =>  $item->status
                ]);
            }

            array_push($array['requisites'], [
                'name'  =>  $requisites->currency->payment->name.' '.$requisites->currency->code_currency->name,
                'id_currency'   =>  $requisites->id_currency,
                'wallets'  =>  $arrays,
            ]);
        }

        //Получение списка резервов
        $array['reserves'] = [];
        foreach (Reserve::all() as $item)
        {
            array_push($array['reserves'], [
                'id'    =>  $item->id,
                'name'  =>  $item->currency->payment->name.' '. $item->currency->code_currency->name,
                'summa' =>  number_format($item->summa, $item->currency->number_format,'.','')
            ]);
        }

        $array['unlimited_reserve'] = [];
        foreach (Currency::where('status', 0)->get() as $item)
        {
            array_push($array['unlimited_reserve'], [
                'id'    =>  $item->id,
                'name'  =>  $item->payment->name.' '. $item->code_currency->name,
                'status' => $item->unlimited_reserve
            ]);
        }

        $array['direction_exchange'] = [];

        foreach (Currency::active()->get() as $item)
        {
            array_push($array['direction_exchange'], [
                'id'    =>  $item->id,
                'name'  =>  $item->payment->name.' '. $item->code_currency->name,
                'status' => ($item->status == 0) ? 1: 0
            ]);
        }

        $array['unlimited_base'] = iEXSetting('unlimited_reserve');


        return response()->json($array);
    }

    /**
     * Получение списка необходимых операторов
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postOperatorSettings(Request $request)
    {
        $detail = $request->detail;
        $type = $request->type;

        $this->setItem('status', 1);

        if($type == 'direction_exchange')
        {
            $currency_status = ($detail == 1 ? 0 : 0);

            Currency::active()->where('id', $request->id_currency)->update(['status' => $currency_status]);
            DirectionExchange::active()->where('id_currency1', $request->id_currency)->update(['status' => $detail]);
            DirectionExchange::active()->where('id_currency2', $request->id_currency)->update(['status' => $detail]);

            $this->setItem('status', 0);

        }elseif($type == 'unlimited_base')
        {
            iEXSetting(['unlimited_reserve' => $detail]);
            $this->setItem('status', 0);
        }elseif($type == 'unlimited')
        {
            Currency::find($request->id_currency)->update(['unlimited_reserve' => $detail]);
            $this->setItem('status', 0);

        }elseif($type == 'requisites')
        {
            foreach ($detail as $key => $value)
            {
                //Используем только существующие элементы
                if($value != null) {
                    Requisites::where([
                        ['id_currency', $key],
                    ])->update(['status' => 0]);

                    Requisites::find($value['id'])->update(['status' => 1]);

                    $this->setItem('status', 0);
                }
            }
        } elseif($type == 'reserves') {
            foreach ($detail as $key => $value)
            {
                //Используем только существующие элементы
                if($value != null) {
                    Reserve::find($key)->update(['summa' => $value]);

                    $this->setItem('status', 0);
                }
            }
        }

        return response()->json($this->getItem());
    }

    /**
     * Список операторов
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listOperators(Request $request)
    {
        $task = Task::find($request->id);

        $user = User::role(Role::all())->get();

        $array = [];
        foreach ($user as $item) {
            if($task->scans != $item->id) {
                array_push($array, [
                    'id'    =>  $item->id,
                    'email' => $item->email
                ]);
            }
        }

        return response()->json($array);
    }


    public function ajaxOperatorPayment(Request $request)
    {
        $transaction = TransactionFacade::find($request->id, [
            'preview'       =>  false,
            'commission'    =>  0,
            'type'          =>  'default',
            'payment'       =>  null,
            'actions'       =>  null,
            'message'       =>  null,
            'feeAmount'     =>  false
        ]);

        return response()->json([
            'auto_payment'  =>  (int)$transaction->hasBalance()
        ]);
    }

    /**
     * Перед подтверждением заявки, добавляем дополнительные параметры
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function successOperatorPayment(Request $request): JsonResponse
    {
        $idTransaction = (isset($request->id_transaction) ? $request->get('id_transaction') : null);
        $text_message_order = (isset($request->text_message_order) ? $request->get('text_message_order') : null);

        $transaction = TransactionFacade::find($request->id, [
            'preview'           =>  false,
            'commission'        =>  ($request->manual_fee ?? 0),
            'type'              =>  ($request->type ?? 'default'),
            'actions'           =>  ($request->actions ?? null),
            'message'           =>  ($request->message_success ?? null),
            'notPartner'        =>  $request->has('not_partner') ? $request->get('not_partner') : 0,
            'notCashback'        =>  $request->has('not_cashback') ? $request->get('not_cashback') : 0,
            'idTransaction'     =>  $idTransaction,
            'textMessageOrder'  =>  $text_message_order,
            'rejection_status'  =>  ($request->rejection_status ?? 0),
            'pin_code'          =>   $request->has('pin_code') ? $request->get('pin_code') : null
        ]);

        if($transaction->hasAction() == 'success')
        {
            try {
                $transaction->success();

                $this->setItem('status', 0);
                if(isset($request->api_panel) and $request->api_panel == 'v1') {
                    $this->setItem('redirect', config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');
                } else {
                    $this->setItem('redirect', config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
                }

            }catch (\Exception $exception) {
                iex_order_auto_payment_log($request->id,1,$exception->getMessage(),$request->payment);

                $this->setItem('status', 1);
                $this->setItem('message', $exception->getMessage());
            }
        } elseif($transaction->hasAction() == 'failed')
        {
            try {
                $transaction->failed();

                $this->setItem('status', 0);
                if(isset($request->api_panel) and $request->api_panel == 'v1') {
                    $this->setItem('redirect', config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');
                } else {
                    $this->setItem('redirect', config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
                }

            }catch (\Exception $exception) {
                $this->setItem('status', 1);
                $this->setItem('message', $exception->getMessage());
            }
        } elseif($transaction->hasAction() == 'defer')
        {
            try {
                $transaction->setDeferType($request->defer_type);
                $transaction->defer();
                $this->setItem('status', 0);

                if(isset($request->api_panel) and $request->api_panel == 'v1') {
                    $this->setItem('redirect', config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');
                } else {
                    $this->setItem('redirect', config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
                }
            }catch (\Exception $exception) {
                $this->setItem('status', 1);
                $this->setItem('message', $exception->getMessage());
            }
        }


        return response()->json($this->getItem());
    }

    /**
     * Сменить оператора
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changeOperators(Request $request) {

        Task::find($request->id)->update([
            'scans' =>  $request->id_operator
        ]);

        $this->setItem('status', 0);
        if(isset($request->api_panel) and $request->api_panel == 'v1') {
            $this->setItem('redirect', config('admin.directory').'/applications/details/'.$request->id.'?actions=arhive');
        } else {
            $this->setItem('redirect', config('admin.directory').'/tx/'.$request->id.'?actions=arhive');
        }

        return response()->json($this->getItem());
    }

    /**
     * Проверяем поступление средств
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function check_receipts(Request $request)
    {
        return response()->json(
            TransactionFacade::call($request->get('id'))->checkInPayment($request->all())
        );
    }

    /**
     * Статус сайта (для операторов)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function status_site(Request $request)
    {
        $status = 0;
        if($request->active) {
            $status = isJobOffline() == 0;
        }elseif(!$request->status) {
            Artisan::call('operation:stop');
            $status = true;
        } else {
            Artisan::call('operation:start');
            $status = false;
        }

        return response()->json([
            'status' =>  $status
        ]);
    }

    public function listReceiving(Request $request)
    {
        $receiving_all = DirectionExchange::with([
            'currency1',
            'currency2',
            'currency2.payment',
            'currency2.code_currency',
            'currency2.reserve',
            'currency2.reserve.code_currency'])
            ->where('status','=',1)
        ->where('id_currency1', $request->id_currency)->get();

        $array = [];

        foreach ($receiving_all as $item) {
            array_push($array, [
                'id'    =>  $item->id,
                'id_currency1'  =>  $item->id_currency1,
                'id_currency2'  =>  $item->id_currency2,
                'name'          =>  $item->currency2->payment->name.' '. $item->currency2->code_currency->name
            ]);
        }

        return response()->json($array);

    }

    public function update_position(Request $request)
    {
        foreach ($request->item as $key => $item) {
            Currency::find($item)->update([
                'sorting_1'    =>  $key
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    public function info_task(Request $request)
    {
        $item = Task::with(['task_status'])->find($request->id);

        $data = [];
        $data['base'] = $item;


        if($item->direction_exchange->id_your_exchange == 0) {
            $course = $item->direction_exchange->parser_exchange->value.' '.
                $item->direction_exchange->currency1->code_currency->sign. ' = '.
                $item->direction_exchange->parser_exchange->summa.' '.
                $item->direction_exchange->currency2->code_currency->sign;
        }else {
            $course = $item->direction_exchange->your_exchange->name.' ('.
                $item->direction_exchange->your_exchange->exchange_rate1.' = '. $item->direction_exchange->your_exchange->exchange_rate2
            .')';
            ;
        }


        $data['custom'] = [
            'course'    =>  $course,

            'date_created' => Carbon::parse($item->created_at)->translatedFormat("d F Y H:i:s"),
            'date_updated' => Carbon::parse($item->updated_at)->translatedFormat("d F Y H:i:s"),
            'give_name'     =>  $item->direction_exchange->currency1->payment->name.' '.$item->direction_exchange->currency1->code_currency->name,
            'give_price'    =>  $item->give_price.' '.$item->direction_exchange->currency1->code_currency->name,
            'receive_name'     =>  $item->direction_exchange->currency2->payment->name.' '.$item->direction_exchange->currency2->code_currency->name,
            'receive_price'    =>  $item->receiving_price.' '.$item->direction_exchange->currency2->code_currency->name,
            'user'              =>  ($item->id_user > 0 ? $item->id_user : 'Неизвестно'),

        ];

        return response()->json($data);
    }

    //Удаление заявки
    public function delete_task(Request $request)
    {
        $task = Task::find($request->id);
        $task->delete();

        return response()->json([
            'status'    =>  0
        ]);
    }
}
