<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 24.10.2017
 * Time: 23:38
 */

namespace App\Http\Controllers\Administrator\Tools;


use App\Common\Packages\Pages\Models\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PagesController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'exchange_rules_link',
        'exchange_kyc_link'
    ];


    /**
     * Список страниц
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $pages = Page::orderByDesc('page_id')->paginate(20);
        return view('admin.tools.pages.index', compact('pages'));
    }

    /**
     * Форма создании страниц
    */
    public function create()
    {
        return view('admin.tools.pages.create');
    }

    /**
     * Обработка и создании страниц
     *
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'page_title' => 'required|max:255',
            'page_content' => 'required',
            'page_slug' => 'required',
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['page_title'][$key] = $request->get('page_title'.$item['field']);
                $options['page_content'][$key] = $request->get('page_content'.$item['field']);
            }

            $options['page_slug'] = $request->has('page_slug') ? $request->get('page_slug') : Str::slug($request->get('page_title'));
            $options['user_id'] = $request->user()->id;
            Page::create($options);
         //   Artisan::call('generator:pages');

            flash('Страница успешно добавлена', ['alert alert-success']);
            return redirect(admin_base_path('/tools/pages'));
        }
    }

    /**
     * Форма обновления страницы
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('admin.tools.pages.edit',compact('page'));
    }

    /**
     * Обработка и обновление данных
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'page_title' => 'required|max:255',
            'page_content' => 'required',
            'page_slug' => 'required',
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {
            $page = Page::findOrFail($id);

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['page_title'][$key] = $request->get('page_title'.$item['field']);
                $options['page_content'][$key] = $request->get('page_content'.$item['field']);
            }


            if($request->has('page_slug') and $request->get('page_slug') != 'pravila-obmena') {
                $options['page_slug'] = $request->has('page_slug') ? $request->get('page_slug') : Str::slug($request->get('page_title'));
            }

            $page->update($options);
          //  Artisan::call('generator:pages');

            flash('Страница успешно обновлена', ['alert alert-success']);
            return redirect()->route('pages.index')
                ->with('flash_message',
                    'Page'. $page->name.' updated!');
        }
    }

    /**
     * Удаление страницы
     *
     * @param $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $role = Page::findOrFail($id);
        $role->delete();
        flash('Страница успешно удалена', ['alert alert-success']);
        return redirect()->route('pages.index');
    }
}
