<?php
namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use App\Models\CollaborationPrModel;
use Illuminate\Http\Request;

class CollaborationPRController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowLocaleOptions = [
        'description_pr',
    ];


    /**
     * Список Сотрудичество и PR
     */
    public function index()
    {
        $contacts = CollaborationPrModel::orderBy('sorting')->get();
        return view('admin.tools.collaboration-pr.index', [
            'contacts' => $contacts
        ]);
    }

    /**
     * Добавить новый Сотрудичество и PR
     */
    public function create()
    {
        return view('admin.tools.collaboration-pr.create');
    }

    /**
     * Обработка и добавление нового Сотрудичество и PR
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption) {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions'))
        {
            foreach ($request->get('item_id') as $value) {
                CollaborationPrModel::find($value)->update([
                    'status'   =>  ($request->status[$value] ?? 0),
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('collaboration-pr.index');
        }


        $validator = \Validator::make($request->all(), [
            'name' => ['required'],
            'type' => ['required'],
            'value' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = CollaborationPrModel::create([
                'name' => $request->get('name'),
                'type' => $request->get('type'),
                'value' => $request->get('value'),
                'url'   =>  ($request->has('url') ? $request->get('url') : null),
                'is_button'  =>  ($request->has('is_button') ? $request->get('is_button') : null)
            ]);

            flash($item->name.' успешно добавлен', ['alert alert-success']);
            return redirect()->route('collaboration-pr.index');
        }
    }

    /**
     * Форма редактирования Сотрудичество и PR
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = CollaborationPrModel::find($id);
        return view('admin.tools.collaboration-pr.edit', compact('item'));
    }

    /**
     * Обработка и обновление Сотрудичество и PR
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $item = CollaborationPrModel::find($id);

        $validator = \Validator::make($request->all(), [
            'name' => ['required'],
            'type' => ['required'],
            'value' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item->update([
                'name' => $request->get('name'),
                'type' => $request->get('type'),
                'value' => $request->get('value'),
                'url'   =>  ($request->has('url') ? $request->get('url') : null),
                'is_button'  =>  ($request->has('is_button') ? $request->get('is_button') : null)
            ]);

            flash("{$item->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('collaboration-pr.index');
        }
    }

    /**
     * Удаление Сотрудичество и PR
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = CollaborationPrModel::find($id);
        flash("{$item->name} успешно удален", ['alert alert-success']);
        $item->delete();

        return redirect()->route('collaboration-pr.index');
    }

    /**
     * Сортировка Сотрудичество и PR
     */
    public function sorting()
    {
        $links = CollaborationPrModel::orderBy('sorting')->get();
        return view('admin.tools.collaboration-pr.sorting', compact('links'));
    }
}
