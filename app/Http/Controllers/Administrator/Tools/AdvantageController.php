<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Advantage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdvantageController extends Controller
{
    /**
     * Список преимуществ
    */
    public function index()
    {
        $advantages = Advantage::orderBy('sorting')->get();
        return view('admin.tools.advantage.index', compact('advantages'));
    }

    public function create() {
        return view('admin.tools.advantage.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'icon' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $logo_icon = $request->file('icon');
            $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

            if(!\File::isDirectory(public_path('storage/advantage/'))) {
                \File::makeDirectory(public_path('storage/advantage/'), 0777, true, true);
            }

            $destinationPath = public_path('/storage/advantage');
            $logo_icon->move($destinationPath, $filename);

            $options = [
                'link' => ($request->has('link') ? $request->get('link') : null),
                'is_target' => ($request->has('is_target') ? $request->get('is_target') : 0),
                'status'    =>  ($request->has('status') ? $request->get('status') : 0),
                'icon' => $filename,
                'id_user'   =>  auth()->id()
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['content'][$key] = $request->get('content'.$item['field']);
            }

            Advantage::create($options);

            flash('Новое преимущество успешно добавлено', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Изменить преимущество
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Advantage::findOrFail($id);
        return view('admin.tools.advantage.edit',compact('item'));
    }

    /**
     * Обновление преимущества
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $advantage = Advantage::findOrFail($id);
        $filename = $advantage->icon;

        $this->validate($request, [
            'status' => 'required'
        ]);

        if($request->hasFile('icon')) {
            // Удаляем актуальное фото
            iex_file_delete(public_path('storage/advantage/'.$filename));


            $logo_icon = $request->file('icon');
            $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

            $destinationPath = public_path('/storage/advantage');
            $logo_icon->move($destinationPath, $filename);
        }

        $options = [
            'link' => ($request->has('link') ? $request->get('link') : null),
            'is_target' => ($request->has('is_target') ? $request->get('is_target') : 0),
            'status'    =>  ($request->has('status') ? $request->get('status') : 0),
            'id_user'   =>  auth()->id(),
            'icon' => $filename
        ];

        foreach (config('app.form_lang') as $key => $item) {
            $options['title'][$key] = $request->get('title'.$item['field']);
            $options['content'][$key] = $request->get('content'.$item['field']);
        }

        $advantage->update($options);

        flash(sprintf('Преимущество %s успешно обновлен', $advantage->name), ['alert alert-success']);
        return redirect()->route('advantage.index');
    }

    /**
     * Удалить преимущство
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Advantage::findOrFail($id);
        iex_file_delete(public_path('storage/advantage/'.$item->icon));
        $item->delete();

        return redirect()->route('advantage.index');
    }


    /**
     * Сортировка преимуществ
     */
    public function sorting()
    {
        $advantages = Advantage::orderBy('sorting')->get();
        return view('admin.tools.advantage.sorting', [
            'advantages' => $advantages
        ]);
    }
}
