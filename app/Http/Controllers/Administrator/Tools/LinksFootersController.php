<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Models\{LinksFooter, LinksFooterGroup};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LinksFootersController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowLocaleOptions = [
        'input_footer_title',
        'description_footer_text',
    ];

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowOptions = [
        'input_footer_image',
        'input_footer_select'
    ];


    /**
     * Список ссылок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function index()
    {
        $links = LinksFooter::orderBy('sorting')->get();

        return view('admin.tools.links_footers.index', [
            'links' => $links
        ]);
    }

    /**
     * Добавить ссылку
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create()
    {
        // Список групп
        $groups = LinksFooterGroup::get()->pluck('name','id');
        return view('admin.tools.links_footers.create', compact('groups'));
    }

    /**
     * Обработка и добавление ссылки
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            // Обновление конфига
            $array_options = [];
            foreach ($this->allowOptions as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array_options[$item] = implode(',', $request->get($item));
                } else {
                    $array_options[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            if($request->hasFile('footer_image'))
            {
                $logo = $request->file('footer_image');
                $filename = sprintf('iex-footer-%s.%s', Str::random(10), $logo->getClientOriginalExtension());

                // Удаление предыдущих фото
                if(!empty(iEXSetting('input_footer_image'))) {
                    iex_file_delete(public_path('storage/'.iEXSetting('input_footer_image')));
                }

                $destinationPath = public_path('/storage');
                $logo->move($destinationPath, $filename);
                $array_options['input_footer_image'] = $filename;
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);
            iEXSetting($array_options);

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'id_group' => ($request->has('id_group') ? $request->get('id_group') : 0),
                'is_blank'  =>  $request->has('is_blank') ? $request->get('is_blank') : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['url'][$key] = $request->get('url'.$item['field']);
            }

            LinksFooter::create($options);

            flash('Ссылка успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования ссылки
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $link = LinksFooter::findOrFail($id);

        // Список групп
        $groups = LinksFooterGroup::get()->pluck('name','id');

        return view('admin.tools.links_footers.edit', compact('link', 'groups'));
    }

    /**
     * Обработка и обновление ссылки
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $link_review = LinksFooter::findOrFail($id);
            $options = [
                'id_group' => ($request->has('id_group') ? $request->get('id_group') : 0),
                'is_blank'  =>  $request->has('is_blank') ? $request->get('is_blank') : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['url'][$key] = $request->get('url'.$item['field']);
            }
            $link_review->update($options);

            flash('Ссылка успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление ссылки
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }
        $item = LinksFooter::findOrFail($id);
        $item->delete();

        flash('Ссылка успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Сортировка ссылок
     */
    public function sorting()
    {
        $links = LinksFooter::orderBy('sorting')->get();
        return view('admin.tools.links_footers.sorting', compact('links'));
    }
}
