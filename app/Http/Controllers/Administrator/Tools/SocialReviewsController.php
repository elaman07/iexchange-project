<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\SocialReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SocialReviewsController extends Controller
{
    /**
     * Список ссылок на соц.сети
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $links = SocialReview::orderBy('sorting')->get();

        return view('admin.tools.social-reviews.index', [
            'links' => $links
        ]);
    }

    /**
     * Добавить ссылку на соц.сети
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.tools.social-reviews.create');
    }

    /**
     * Обработка и добавление ссылки на соц.сети
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        if($request->has('actions'))
        {
            foreach ($request->get('item_id') as $value) {
                SocialReview::find($value)->update([
                    'status' =>  (isset($request->status[$value]) ? $request->status[$value] : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('social-reviews.index');
        }


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'link' => 'required',
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            SocialReview::create([
                'name' =>  $request->get('name'),
                'link' => ($request->has('link') ? $request->get('link') : null),
                'type' => ($request->has('type') ? $request->get('type') : null),
            ]);

            flash('Ссылка успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования ссылки на соц.сети
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $link = SocialReview::findOrFail($id);

        return view('admin.tools.social-reviews.edit', compact('link'));
    }

    /**
     * Обработка и обновление ссылки на соц.сети
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'link' => 'required',
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = SocialReview::findOrFail($id);
            $item->update([
                'name' =>  $request->get('name'),
                'link' => ($request->has('link') ? $request->get('link') : null),
                'type' => ($request->has('type') ? $request->get('type') : null),
            ]);

            flash('Ссылка успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление ссылки на соц.сети
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }
        $item = SocialReview::findOrFail($id);
        $item->delete();

        flash('Ссылка успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Сортировка ссылок на соц. сети
     */
    public function sorting()
    {
        $links = SocialReview::orderBy('sorting')->get();
        return view('admin.tools.social-reviews.sorting', compact('links'));
    }
}
