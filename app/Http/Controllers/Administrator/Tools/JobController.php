<?php
namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowLocaleOptions = [
        'working_online_text',
        'working_offline_text',
        //'working_offline_notify'
    ];

    protected array $allowFiltered = [
        'is_working_manual',
        'type_working_mode',
        'is_working_view_header'
    ];

    public function index(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }


            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption)
            {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);
            iEXSetting($array);

            return redirect(config('admin.directory').'/tools/job');
        }

        return view('admin.tools.job.index');
    }
}
