<?php

namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\{
    RedirectResponse,
    Request
};
use Illuminate\Support\{
    Facades\Validator,
    Str
};

class MenuController extends Controller
{
    /**
     * Список меню
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $menus = Menu::orderBy('sorting')->get();
        return view('admin.tools.menu.index', [
            'menus'         =>  $menus,
        ]);
    }

    /**
     * Форма добавления меню
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.tools.menu.create');
    }

    /**
     * Обработка и добавления меню
     *
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'slug' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }
            $options['slug'] = $request->has('slug') ? $request->get('slug') : Str::slug($request->get('name'));
            $options['status'] = $request->has('status') ? $request->get('status') : 0;
            $options['sorting'] = $request->has('sorting') ? $request->get('sorting') : 0;
            $options['text_color'] = $request->has('text_color') ? $request->get('text_color') : 0;

            Menu::create($options);

            flash('Меню успешно добавлена', ['alert alert-success']);
            return redirect(admin_base_path('/tools/menu'));
        }
    }

    /**
     * Форма обновления меню
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = Menu::findOrFail($id);
        return view('admin.tools.menu.edit', compact('item'));
    }

    /**
     * Обработка и обновление данных
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $menu = Menu::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'slug' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }
            $options['slug'] = $request->has('slug') ? $request->get('slug') : Str::slug($request->get('name'));
            $options['status'] = $request->has('status') ? $request->get('status') : 0;
            $options['sorting'] = $request->has('sorting') ? $request->get('sorting') : 0;
            $options['text_color'] = $request->has('text_color') ? $request->get('text_color') : 0;

            $menu->update($options);

            flash('Меню успешно обновлена', ['alert alert-success']);
            return redirect(admin_base_path('/tools/menu'));
        }
    }

    /**
     * Удаление меню
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $menu = Menu::findOrFail($id);
        flash('Меню '.$menu->name.' успешно удалена', ['alert alert-success']);
        $menu->delete();

        return redirect()->route('menu.index');
    }

    /**
     * Сортировка меню
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sorting()
    {
        $menus = Menu::where('status', '=', 1)->orderBy('sorting')->get();

        return view('admin.tools.menu.sorting', compact('menus'));
    }
}
