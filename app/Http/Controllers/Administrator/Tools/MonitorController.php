<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 12.02.2019
 * Time: 14:32
 */

namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;

class MonitorController extends Controller
{

    public function index()
    {
        return view('admin.tools.monitor');
    }
}