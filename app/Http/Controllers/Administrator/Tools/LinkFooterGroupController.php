<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\LinksFooterGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LinkFooterGroupController extends Controller
{
    /**
     * Список ссылок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function index()
    {
        $links = LinksFooterGroup::orderBy('sorting')->get();

        return view('admin.tools.links_footers.groups.index', [
            'links' => $links
        ]);
    }

    /**
     * Добавить группу
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create()
    {
        return view('admin.tools.links_footers.groups.create');
    }

    /**
     * Обработка и добавление ссылки
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'sorting' => $request->has('sorting') ? $request->get('sorting') : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            LinksFooterGroup::create($options);

            flash('Группа успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования группы
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $link = LinksFooterGroup::findOrFail($id);
        return view('admin.tools.links_footers.groups.edit', compact('link'));
    }

    /**
     * Обработка и обновление группы
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $group = LinksFooterGroup::findOrFail($id);


            $options = [
                'sorting' => $request->has('sorting') ? $request->get('sorting') : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            $group->update($options);

            flash('Группа успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление группы
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = LinksFooterGroup::findOrFail($id);

        if($item->links_footer()->count() > 0) {
            flash('К группе привязаны ссылки, удаление невозможно', ['alert alert-danger']);
            return redirect()->back();
        }

        $item->delete();

        flash('Группа успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }
}
