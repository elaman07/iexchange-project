<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Models\{LinksReview, LinksReviewGroup};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class LinksReviewsController extends Controller
{
    protected array $allowFiltered = [
        'is_disabled_writing_links_reviews'
    ];

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowLocaleOptions = [
        'description_review',
    ];

    /**
     * Список ссылок
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function index()
    {
        $links = LinksReview::orderBy('sorting')->get();

        return view('admin.tools.links_reviews.index', [
            'links' => $links
        ]);
    }

    /**
     * Добавить ссылку
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create()
    {
        // Список групп
        $groups = LinksReviewGroup::get()->pluck('name','id');
        return view('admin.tools.links_reviews.create', compact('groups'));
    }

    /**
     * Обработка и добавление ссылки
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);


            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption) {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'url' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $filename = null;
            if($request->hasFile('icon')) {
                // Загружаем фото
                $filename = sprintf('%s.png', Str::random(10));
                $img = Image::make(
                    $request->file('icon')->getRealPath()
                );

                $img->save(public_path('storage/links/'.$filename));
            }

            $options = [
                'id_group' => ($request->has('id_group') ? $request->get('id_group') : 0),
                'icon' => $filename,
                'is_review' => ($request->has('is_review') ? $request->get('is_review') : 0),
                'is_bot' => ($request->has('is_bot') ? $request->get('is_bot') : 0),
                'count_review'  =>  $request->has('count_review') ? $request->get('count_review') : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['url'][$key] = $request->get('url'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
            }

            LinksReview::create($options);

            flash('Ссылка успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования ссылки
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $link = LinksReview::findOrFail($id);

        // Список групп
        $groups = LinksReviewGroup::get()->pluck('name','id');

        return view('admin.tools.links_reviews.edit', compact('link', 'groups'));
    }

    /**
     * Обработка и обновление ссылки
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $link_review = LinksReview::findOrFail($id);
            $filename = $link_review->icon;

            if($request->hasFile('icon')) {
                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/links/'.$filename));

                // Загружаем фото
                $filename = sprintf('%s.png', Str::random(10));
                $img = Image::make(
                    $request->file('icon')->getRealPath()
                );
                $img->save(public_path('storage/links/'.$filename));
            }

            $options = [
                'id_group' => ($request->has('id_group') ? $request->get('id_group') : 0),
                'icon' => $filename,
                'is_review' => ($request->has('is_review') ? $request->get('is_review') : 0),
                'is_bot' => ($request->has('is_bot') ? $request->get('is_bot') : 0),
                'count_review'  =>  $request->has('count_review') ? $request->get('count_review') : 0
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['url'][$key] = $request->get('url'.$item['field']);
                $options['description'][$key] = $request->get('description'.$item['field']);
            }


            $link_review->update($options);

            flash('Ссылка успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление ссылки
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = LinksReview::findOrFail($id);
        $item->delete();

        flash('Ссылка успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }

    /**
     * Сортировка ссылок
     */
    public function sorting()
    {
        $links = LinksReview::orderBy('sorting')->get();
        return view('admin.tools.links_reviews.sorting', compact('links'));
    }
}
