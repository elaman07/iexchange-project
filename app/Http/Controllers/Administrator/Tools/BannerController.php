<?php

namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Advantage;
use App\Models\Banner;
use App\Models\BannerButton;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowOptions = [
        'is_banner_autoplay',
        'number_banner_autoplay_timeout',
        'is_banner_hidden_nav'
    ];

    public function index()
    {
        $banners = Banner::all();
        return view('admin.tools.banners.index', [
            'banners' => $banners
        ]);
    }


    public function create()
    {
        $buttons = BannerButton::select('id','name')->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        })->pluck('name', 'id');


        return view('admin.tools.banners.create',[
            'buttons' => $buttons
        ]);
    }

    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array_options = [];
            foreach ($this->allowOptions as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array_options[$item] = implode(',', $request->get($item));
                } else {
                    $array_options[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array_options);

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), []);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $logo_icon = $request->file('images');
            $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

            if(!\File::isDirectory(public_path('storage/banners/'))) {
                \File::makeDirectory(public_path('storage/banners/'), 0777, true, true);
            }

            $destinationPath = public_path('/storage/banners');
            $logo_icon->move($destinationPath, $filename);

            $options = [
                'images' => $filename,
            ];

            if($request->hasFile('images_banner'))
            {
                $images_banner = $request->file('images_banner');
                $filename_image = sprintf('banner-image-%s.%s', Str::random(10), $images_banner->getClientOriginalExtension());
                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/banners/'.$filename_image));

                $destinationPath = public_path('/storage/banners');
                $images_banner->move($destinationPath, $filename_image);
                $options['images_banner'] = $filename_image;
            }

            $options['color_title'] = $request->has('color_title') ? $request->get('color_title') : null;
            $options['color_text'] = $request->has('color_text') ? $request->get('color_text') : null;
            $options['status'] = $request->has('status') ? $request->get('status') : 0;
            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
            }

            $banner = Banner::create($options);
            $banner->buttons()->sync($request->ids_buttons);

            flash('Баннер успешно создан', ['alert alert-success']);
            return redirect()->back();
        }
    }


    public function edit(int $id, Request $request)
    {
        if($request->has('actions') and $request->get('actions') == 'banner_image')
        {
            if($request->get('type') == 'delete')
            {
                $find_banner = Banner::find($request->get('item_id'));
                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/banners/'.$find_banner->images_banner));
                $find_banner->images_banner = '';
                $find_banner->save();
                return redirect()->back();
            }
        }

        $item = Banner::find($id);
        $buttons = BannerButton::select('id','name')
            ->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        })->pluck('name', 'id');



        return view('admin.tools.banners.edit', [
            'item' => $item,
            'buttons' => $buttons
        ]);
    }

    public function update(int $id, Request $request)
    {
        $banner = Banner::findOrFail($id);
        $filename = $banner->images;

        $validator = Validator::make($request->all(), []);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            if($request->hasFile('images')) {
                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/banners/'.$filename));

                $logo_icon = $request->file('images');
                $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

                $destinationPath = public_path('/storage/banners');
                $logo_icon->move($destinationPath, $filename);
            }

            $options = [
                'images' => $filename,
            ];


            if($request->hasFile('images_banner'))
            {
                $images_banner = $request->file('images_banner');
                $filename_image = sprintf('banner-image-%s.%s', Str::random(10), $images_banner->getClientOriginalExtension());
                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/banners/'.$filename_image));

                $destinationPath = public_path('/storage/banners');
                $images_banner->move($destinationPath, $filename_image);
                $options['images_banner'] = $filename_image;
            }


            $options['color_title'] = $request->has('color_title') ? $request->get('color_title') : null;
            $options['color_text'] = $request->has('color_text') ? $request->get('color_text') : null;
            $options['status'] = $request->has('status') ? $request->get('status') : 0;
            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
            }

            $banner->update($options);
            $banner->buttons()->sync($request->ids_buttons);
            flash(sprintf('Баннер %s успешно обновлен', $banner->title), ['alert alert-success']);
            return redirect()->back();
        }
    }


    /**
     * Удалить
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Banner::findOrFail($id);
        iex_file_delete(public_path('storage/banners/'.$item->images));
        $item->buttons()->sync([]);
        $item->delete();

        return redirect()->route('banners.index');
    }

    /**
     * Сортировка преимуществ
     */
    public function sorting()
    {
        $banners = Banner::orderBy('sorting')->get();
        return view('admin.tools.banners.sorting', [
            'banners' => $banners
        ]);
    }
}
