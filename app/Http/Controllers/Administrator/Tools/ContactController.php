<?php
namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\LanguageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ContactController extends Controller
{
    protected array $allowLocaleOptions = [
        'description_contact',
    ];

    /**
     * Список контактов
    */
    public function index()
    {
        $contacts = Contact::orderBy('sorting')->get();
        return view('admin.tools.contact.index', [
            'contacts' => $contacts
        ]);
    }

    /**
     * Добавить новый контакта
     */
    public function create()
    {
        return view('admin.tools.contact.create');
    }

    /**
     * Обработка и добавление нового контакта
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption) {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions'))
        {
            foreach ($request->get('item_id') as $value) {
                Contact::find($value)->update([
                    'is_home'   =>  ($request->is_home[$value] ?? 0),
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('contacts.index');
        }


        $validator = \Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'is_home' => ($request->has('is_home') ? $request->get('is_home') : 0),
                'is_button'  =>  ($request->has('is_button') ? $request->get('is_button') : 0),
                'type'  =>  ($request->has('type') ? $request->get('type') : 0),
                'text_color'    => $request->has('text_color') ? $request->get('text_color') : null
            ];


            if($request->hasFile('icon'))
            {
                if(!\File::isDirectory(public_path('storage/contact/'))) {
                    \File::makeDirectory(public_path('storage/contact/'), 0777, true, true);
                }

                $logo_icon = $request->file('icon');
                $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());



                $destinationPath = public_path('/storage/contact');
                $logo_icon->move($destinationPath, $filename);
                $options['icon'] = $filename;
            }

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['value'][$key] = $request->get('value'.$item['field']);
                $options['url'][$key] = $request->get('url'.$item['field']);
            }


            Contact::create($options);

            flash('Контакт успешно добавлен', ['alert alert-success']);
            return redirect()->route('contacts.index');
        }
    }

    /**
     * Форма редактирования контактов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = Contact::find($id);
        return view('admin.tools.contact.edit', compact('item'));
    }

    /**
     * Обработка и обновление контактов
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $contact = Contact::find($id);
        $filename = $contact->icon;

        $validator = \Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'is_home' => ($request->has('is_home') ? $request->get('is_home') : 0),
                'is_button'  =>  ($request->has('is_button') ? $request->get('is_button') : 0),
                'type'  =>  ($request->has('type') ? $request->get('type') : 0),
                'text_color'    => $request->has('text_color') ? $request->get('text_color') : null
            ];
            if($request->hasFile('icon'))
            {
                if(!\File::isDirectory(public_path('storage/contact/'))) {
                    \File::makeDirectory(public_path('storage/contact/'), 0777, true, true);
                }

                // Удаляем актуальное фото
                iex_file_delete(public_path('storage/contact/'.$filename));

                $logo_icon = $request->file('icon');
                $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

                $destinationPath = public_path('/storage/contact');
                $logo_icon->move($destinationPath, $filename);
                $options['icon'] = $filename;
            }

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['value'][$key] = $request->get('value'.$item['field']);
                $options['url'][$key] = $request->get('url'.$item['field']);
            }

            $contact->update($options);

            flash("Контакт {$contact->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('contacts.edit', [$contact->id]);
        }
    }

    /**
     * Удаление контактов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Contact::find($id);
        flash("Контакт {$item->name} успешно удален", ['alert alert-success']);
        $item->delete();

        return redirect()->route('contacts.index');
    }

    /**
     * Сортировка контактов
     */
    public function sorting()
    {
        $links = Contact::orderBy('sorting')->get();
        return view('admin.tools.contact.sorting', compact('links'));
    }
}
