<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\NoticeExchange;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NotificationController extends Controller
{
    /**
     * Список уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function index() {
        $notifications = NoticeExchange::orderBy('sorting')->get();
        return view('admin.tools.notification.index', compact('notifications'));
    }

    /**
     * Форма добавления уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create() {
        return view('admin.tools.notification.create');
    }

    /**
     * Обработка и добавление уведомлений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $options = [
            'status' => $request->status,
            'link'  =>  ($request->has('link') ? $request->get('link') : null),
            'is_enabled_schedule' => ($request->has('is_enabled_schedule') ? $request->get('is_enabled_schedule') : 0),
            'from_time' => ($request->has('from_time') ? $request->get('from_time') : null),
            'to_time' => ($request->has('to_time') ? $request->get('to_time') : 0),
            'is_blank' => ($request->has('is_blank') ? $request->get('is_blank') : 0),
            'bg_color'  => $request->has('bg_color') ? $request->get('bg_color') : null,
            'text_color'  => $request->has('text_color') ? $request->get('text_color') : null,
            'text_size'  => $request->has('text_size') ? $request->get('text_size') : null,
        ];

        if($request->hasFile('icon_notice'))
        {
            if(!\File::isDirectory(public_path('storage/notices/'))) {
                \File::makeDirectory(public_path('storage/notices/'), 0777, true, true);
            }

            $logo_icon = $request->file('icon_notice');
            $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());


            $destinationPath = public_path('/storage/notices');
            $logo_icon->move($destinationPath, $filename);
            $options['icon_notice'] = $filename;
        }


        foreach (config('app.form_lang') as $key => $item) {
            $options['text'][$key] = $request->get('text'.$item['field']) ?? null;
        }

        NoticeExchange::create($options);
        flash('Уведомление успешно добавлено', ['alert alert-success']);

        return redirect()->to(
            admin_base_path('/tools/notification')
        );
    }

    /**
     * Форма редактирования уведомлений
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request, int $id)
    {
        $item = NoticeExchange::findOrFail($id);

        // Удаление фото
        if($request->get('type') == 'delete')
        {
            // Удаляем актуальное фото
            iex_file_delete(public_path('storage/notices/'.$item->icon_notice));
            $item->icon_notice = null;
            $item->save();
            return redirect()->back();
        }

        return view('admin.tools.notification.edit',compact('item'));
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $notification = NoticeExchange::findOrFail($id);
        $filename = $notification->icon_notice;

        $options = [
            'status' => $request->status,
            'link'  =>  ($request->has('link') ? $request->get('link') : null),
            'is_enabled_schedule' => ($request->has('is_enabled_schedule') ? $request->get('is_enabled_schedule') : 0),
            'from_time' => ($request->has('from_time') ? $request->get('from_time') : null),
            'to_time' => ($request->has('to_time') ? $request->get('to_time') : 0),
            'is_blank' => ($request->has('is_blank') ? $request->get('is_blank') : 0),
            'bg_color'  => $request->has('bg_color') ? $request->get('bg_color') : null,
            'text_color'  => $request->has('text_color') ? $request->get('text_color') : null,
            'text_size'  => $request->has('text_size') ? $request->get('text_size') : null,
        ];

        if($request->hasFile('icon_notice'))
        {
            if(!\File::isDirectory(public_path('storage/notices/'))) {
                \File::makeDirectory(public_path('storage/notices/'), 0777, true, true);
            }

            // Удаляем актуальное фото
            iex_file_delete(public_path('storage/notices/'.$filename));

            $logo_icon = $request->file('icon_notice');
            $filename = sprintf('%s.%s', Str::random(10), $logo_icon->getClientOriginalExtension());

            $destinationPath = public_path('/storage/notices');
            $logo_icon->move($destinationPath, $filename);
            $options['icon_notice'] = $filename;
        }

        foreach (config('app.form_lang') as $key => $item) {
            $options['text'][$key] = $request->get('text'.$item['field']) ?? null;
        }


        $notification->update($options);

        flash('Уведомление успешно обновлена', ['alert alert-success']);
        return redirect()->to(
            admin_base_path('tools/notification/'.$notification->id.'/edit')
        );
    }

    /**
     * Удаление уведомлений
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $role = NoticeExchange::findOrFail($id);
        $role->delete();

        return redirect()->route('notification.index');
    }

    /**
     * Включаем уведомление
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function on($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = NoticeExchange::findOrFail($id);
        $item->update(['status' => 1]);

        flash('Уведомление включено', ['alert alert-success']);
        return redirect()->route('notification.index');
    }

    /**
     * Отключаем уведомление
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function off($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = NoticeExchange::findOrFail($id);
        $item->update(['status' => 0]);

        flash('Уведомление отключено', ['alert alert-success']);
        return redirect()->route('notification.index');
    }

    /**
     * Сортировка уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function sorting()
    {
        $notices = NoticeExchange::orderBy('sorting')->get();

        return view('admin.tools.notification.sorting', [
            'notices' => $notices
        ]);
    }
}
