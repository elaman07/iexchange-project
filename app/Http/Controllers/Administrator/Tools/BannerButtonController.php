<?php

namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Advantage;
use App\Models\Banner;
use App\Models\BannerButton;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BannerButtonController extends Controller
{

    public function index()
    {
        $banners = BannerButton::all();
        return view('admin.tools.banners.button.index', [
            'banners' => $banners
        ]);
    }


    public function create() {
        return view('admin.tools.banners.button.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [];

            $options['color_text_button'] = $request->has('color_text_button') ? $request->get('color_text_button') : null;
            $options['color_bg_button'] = $request->has('color_bg_button') ? $request->get('color_bg_button') : null;

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['link'][$key] = $request->get('link'.$item['field']);
            }

            BannerButton::create($options);

            flash('Кнопка успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }


    public function edit(int $id)
    {
        $item = BannerButton::find($id);
        return view('admin.tools.banners.button.edit', [
            'item' => $item
        ]);
    }

    public function update(int $id, Request $request)
    {
        $banner = BannerButton::findOrFail($id);
        $filename = $banner->images;

        $validator = Validator::make($request->all(), []);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {


            $options = [];
            $options['color_text_button'] = $request->has('color_text_button') ? $request->get('color_text_button') : null;
            $options['color_bg_button'] = $request->has('color_bg_button') ? $request->get('color_bg_button') : null;
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['link'][$key] = $request->get('link'.$item['field']);
            }

            $banner->update($options);
            flash(sprintf('Кнопка %s успешно обновлена', $banner->title), ['alert alert-success']);
            return redirect()->back();
        }
    }


    /**
     * Удалить
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = BannerButton::findOrFail($id);
        $item->delete();

        return redirect()->route('banners-button.index');
    }

}
