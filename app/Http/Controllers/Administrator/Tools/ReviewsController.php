<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class ReviewsController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_enabled_reviews',
        'checked_ip_address_reviews',
        'input_num_reviews',
        'is_auth_type_reviews',
        'count_reviews_page'
    ];

    /***
     * Главная страница отзывов
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $reviews = Review::filter($request->all())->where('version', '=', 1)->orderBy('id', 'desc')->paginate(20);

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('reviews.index', $conditions);
        }

        $statuses = [
            0 => 'На проверке',
            1 => 'Опубликовано',
            2 => 'Заблокирован'
        ];


        return view('admin.tools.reviews.index', [
            'filter' => $request->all(),
            'reviews'   =>  $reviews,
            'statuses'  =>  $statuses
        ]);
    }

    /**
     * Форма добавления отзыва
    */
    public function create()
    {
        return view('admin.tools.reviews.create');
    }

    /**
     * Обработка и публикация отзыва
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Доп. настройки
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        if($request->has('actions')) {

            foreach ($request->get('item_id') as $value) {
                if(isset($request->status[$value]) and $request->status[$value] == 1) {
                    $this->checked($value);




                } elseif(isset($request->status[$value]) and $request->status[$value] == 2) {
                    $this->blocked($value);
                }
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('reviews.index');
        }

        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required'],
            'email' => ['required'],
            'text'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            Review::create([
                'id_user'       =>  $request->user()->id,
                'id_admin'      =>  $request->user()->id,
                'user_agent'    =>  $request->userAgent(),
                'type'          =>  ($request->has('type') ? $request->get('type') : 0),
                'name'          =>  $request->get('name'),
                'email'         =>  $request->get('email'),
                'text'          =>  $request->get('text'),
                'ip_address'    =>  $request->ip(),
                'status'        =>  1,
            ]);

            flash('Отзыв успешно добавлен', ['alert alert-success']);
            return redirect()->route('reviews.index');
        }
    }

    /**
     * Форма редактирования отзыва
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = Review::find($id);
        return view('admin.tools.reviews.edit', compact('item'));
    }

    /**
     * Обработка и обновление отзыва
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $item = Review::find($id);
        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required'],
            'email' =>  ['required'],
            'text'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item->update([
                'name'          =>  $request->get('name'),
                'email'         =>  $request->get('email'),
                'text'          =>  $request->get('text'),
                'type'          =>  ($request->has('type') ? $request->get('type') : 0),
                'status'        =>  ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash('Отзыв успешно обновлен', ['alert alert-success']);
            return redirect()->route('reviews.index');
        }
    }

    /**
     * Удаление отзыва
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Review::find($id);
        $item->delete();

        flash('Отзыв успешно удален', ['alert alert-success']);
        return redirect()->route('reviews.index');
    }


    /**
     * Подтвердить отзыв
     *
     * @param int $id
     * @throws \Throwable
     */
    public function checked(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Review::find($id);
        $item->update([
            'status'    =>  1
        ]);

        // Если заявка создана из Telegram, клиенту отправляем сообщение
        if(!empty($item->telegram_id))
        {
            $telegram_message = view('_parent.bot.website.success_review', ['item' => $item])->render();
            Telegram::sendMessage([
                'chat_id'           => $item->telegram_id,
                'text'              => $telegram_message,
                'parse_mode'        => 'HTML',
            ]);
        }
    }

    /**
     * Заблокировать отзыв
     *
     * @param int $id
     */
    public function blocked(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Review::find($id);
        $item->update([
            'status'    =>  2
        ]);
    }
}
