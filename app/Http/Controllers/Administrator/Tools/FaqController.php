<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Models\Faq;
use App\Models\FaqCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class FaqController extends Controller
{
    /**
     * Список вопросов и ответов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $faq = Faq::filter($request->all())->orderBy('sorting')->paginate(20);

        $categories = FaqCategory::all()->map(function ($item) {
            return [
                'value' => $item->name,
                'id' => $item->id
            ];
        })->pluck('value', 'id');

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('faq.index', $conditions);
        }

        $statusses = [
            0 => 'Отключен',
            1 => 'Включен'
        ];

        return view('admin.tools.faq.index', [
            'faq'   =>  $faq,
            'categories' => $categories,
            'filter'    =>  $request->all(),
            'statusses' => $statusses
        ]);
    }

    /**
     * Форма добавленя вопросов и ответов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create() {
        $category = FaqCategory::orderBy('sorting')->get();
        return view('admin.tools.faq.create', compact('category'));
    }

    /**
     * Обработка и добавление вопросов и ответов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // Доп. настройки
        if($request->has('actions'))
        {
            foreach ($request->get('item_id') as $value) {
                Faq::find($value)->update([
                    'status'    =>  (isset($request->status[$value]) ? $request->status[$value] : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }

        $validator = \Validator::make($request->all(), [
            'id_group' => ['required', 'exists:faq_category,id'],
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'id_group' =>  $request->get('id_group'),
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
            }

            Faq::create($options);

            flash('Запись успешно добавлена', ['alert alert-success']);
            return redirect()->route('faq.index');
        }
    }

    /**
     * Форма редактирования вопросов и ответов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function edit(int $id)
    {
        $faq = Faq::findOrFail($id);
        $category = FaqCategory::orderBy('sorting')->get();

        return view('admin.tools.faq.edit', compact('faq', 'category'));
    }

    /**
     * Обработка и редактирование вопросов и ответов
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $faq = Faq::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'id_group' => ['required', 'exists:faq_category,id'],
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'id_group' =>  $request->get('id_group'),
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
            }


            $faq->update($options);

            flash('Запись успешно обновлена', ['alert alert-success']);
            return redirect()->route('faq.index');
        }
    }

    /**
     * Удаление вопросов и ответов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $faq = Faq::findOrFail($id);

        flash("Запись {$faq->name} успешно удалена", ['alert alert-success']);

        $faq->delete();

        return redirect()->route('faq.index');
    }

    /**
     * Сортировка вопросов и ответов
     */
    public function sorting()
    {
        $faq = Faq::orderBy('sorting')->get();

        return view('admin.tools.faq.sorting', [
            'faqs' => $faq
        ]);
    }
}
