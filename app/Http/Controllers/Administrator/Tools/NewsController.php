<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Common\Packages\ImageSystem\Facades\ImageSystemFacade;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    /**
     * Список новостей
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = News::orderBy('id','desc')->paginate(20);
        return view('admin.tools.news.index', [
            'news'  =>  $news
        ]);
    }

    /**
     * Форма добавление новостей
    */
    public function create()
    {
        return view('admin.tools.news.create');
    }

    /**
     * Обработка и добавление новостей
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $filename = null;
            $is_local_image = 0;
            if($request->hasFile('image'))
            {
                $responseImage = ImageSystemFacade::make($request->file('image'))
                    ->setDisk('news')
                    ->put(public_path('storage/news'));


                $filename = $responseImage['filename'];
                $is_local_image = $responseImage['is_local_image'];
            }


            $options = [
                'id_user'           =>  $request->user()->id,
                'image'             =>  $filename,
                'parent_url'        =>  Str::slug($request->name.'-'.time()),
                'is_local_image'    => $is_local_image
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
            }

            News::create($options);



            flash('Новость успешно опубликовано', ['alert alert-success']);
            return redirect()->back();
        }
    }


    /**
     * Форма обновления новостей
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $news = News::find($id);
        return view('admin.tools.news.edit', [
            'news' => $news
        ]);
    }


    /**
     * Обработка и обновление новостей
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $news = News::find($id);
            $filename = $news->image;
            $is_local_image = 0;
            if($request->hasFile('image'))
            {
                $responseImage = ImageSystemFacade::make($request->file('image'))
                    ->setDisk('news')
                    ->put(public_path('storage/news'), $filename);

                $filename = $responseImage['filename'];
                $is_local_image = $responseImage['is_local_image'];
            }

            $options = [
                'image'             =>  $filename,
                'parent_url'        =>  Str::slug($request->get('name')),
                'is_local_image'    =>  $is_local_image
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['text'][$key] = $request->get('text'.$item['field']);
            }

            $news->update($options);

            flash('Новость №'.$news->id.' успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление новостей
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $news = News::findOrFail($id);
        $news->delete();

        flash('Новость успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }
}
