<?php

namespace App\Http\Controllers\Administrator\Tools;

use App\Common\Packages\Pages\Models\Page;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\RulesPage;
use Illuminate\Http\{
    RedirectResponse,
    Request
};
use Illuminate\Support\{
    Facades\Validator,
    Str
};

class RulesController extends Controller
{
    protected array $allowLocaleOptions = [
        'title_rules_page',
        'description_rules_page'
    ];


    /**
     * Список записей
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $items = RulesPage::orderBy('sorting')->get();
        return view('admin.tools.rules_page.index', [
            'items'         =>  $items,
        ]);
    }

    /**
     * Форма добавления меню
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $pages = Page::all();
        return view('admin.tools.rules_page.create', [
            'pages' => $pages
        ]);
    }

    /**
     * Обработка и добавления меню
     *
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $options_locale = [];
            foreach ($this->allowLocaleOptions as $allowLocaleOption) {
                foreach (config('app.form_lang') as $language_key => $language_value) {
                    $options_locale[$allowLocaleOption][$language_key] = $request->get($allowLocaleOption.$language_value['field']);
                }
            }

            //Для мультиязычности
            $locale_data = collect($options_locale)->only($this->allowLocaleOptions)->all();
            iEXContentLanguage($locale_data);

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'id_page' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
            }
            $options['id_page'] = $request->has('id_page') ? $request->get('id_page') : Str::slug($request->get('id_page'));
            $options['status'] = $request->has('status') ? $request->get('status') : 0;

            RulesPage::create($options);

            flash('Запись успешно добавлена', ['alert alert-success']);
            return redirect(admin_base_path('/tools/rules_pages'));
        }
    }

    /**
     * Форма обновления меню
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $pages = Page::all();
        $item = RulesPage::findOrFail($id);
        return view('admin.tools.rules_page.edit', compact('item', 'pages'));
    }

    /**
     * Обработка и обновление данных
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $menu = RulesPage::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_page' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
            }
            $options['id_page'] = $request->has('id_page') ? $request->get('id_page') : Str::slug($request->get('id_page'));
            $options['status'] = $request->has('status') ? $request->get('status') : 0;


            $menu->update($options);

            flash('Запись успешно обновлена', ['alert alert-success']);
            return redirect(admin_base_path('/tools/rules_pages'));
        }
    }

    /**
     * Удаление меню
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $rules = RulesPage::findOrFail($id);
        flash('Запись '.$rules->name.' успешно удалена', ['alert alert-success']);
        $rules->delete();

        return redirect()->route('rules_pages.index');
    }

    /**
     * Сортировка меню
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sorting()
    {
        $items = RulesPage::where('status', '=', 1)->orderBy('sorting')->get();

        return view('admin.tools.rules_page.sorting', compact('items'));
    }
}
