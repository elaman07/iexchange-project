<?php

namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\JobSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JobScheduleController extends Controller
{

    public function index()
    {
        $items = JobSchedule::all();
        return view('admin.tools.job.schedule.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        $dayNames = [1 => 'Понедельник', 2 => 'Вторник', 3 => 'Среда', 4 => 'Четверг', 5 => 'Пятница', 6 => 'Суббота', 7 => 'Воскресенье'];
        return view('admin.tools.job.schedule.create', [
            'dayNames' => $dayNames
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
            'from_time' => 'required',
            'to_time' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [
                'status' => ($request->has('status') ? $request->get('status') : 0),
                'name' => ($request->has('name') ? $request->get('name') : null),
                'from_time'    =>  ($request->has('from_time') ? $request->get('from_time') : null),
                'to_time'    =>  ($request->has('to_time') ? $request->get('to_time') : null),
                'work_days'    =>  ($request->has('work_days') ? implode(',', $request->get('work_days')) : null),
                'id_user'   =>  auth()->id()
            ];

            JobSchedule::create($options);

            flash('Расписание успешно добавлено', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Изменить расписание
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $item = JobSchedule::findOrFail($id);
        $dayNames = [1 => 'Понедельник', 2 => 'Вторник', 3 => 'Среда', 4 => 'Четверг', 5 => 'Пятница', 6 => 'Суббота', 7 => 'Воскресенье'];
        return view('admin.tools.job.schedule.edit',compact('item', 'dayNames'));
    }

    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
            'from_time' => 'required',
            'to_time' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $find = JobSchedule::find($id);


            $options = [
                'status' => ($request->has('status') ? $request->get('status') : 0),
                'name' => ($request->has('name') ? $request->get('name') : null),
                'from_time'    =>  ($request->has('from_time') ? $request->get('from_time') : null),
                'to_time'    =>  ($request->has('to_time') ? $request->get('to_time') : null),
                'work_days'    =>  ($request->has('work_days') ? implode(',', $request->get('work_days')) : null),
            ];

            $find->update($options);

            flash('Расписание успешно обновлено', ['alert alert-success']);
            return redirect()->back();
        }
    }

    public function destroy(int $id, Request $request)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $job = JobSchedule::findOrFail($id);

        flash("Запись {$job->name} успешно удалена", ['alert alert-success']);
        $job->delete();

        return redirect()->route('job-schedule.index');
    }
}
