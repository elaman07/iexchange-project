<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\GeoCountryList;
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Http\Request;

class GEOIPController extends Controller
{
    /**
     * Список стран
    */
    public function countries(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            foreach ($request->item_id as $value_id)
            {
                $update = GeoCountryList::find($value_id);
                $update_multi_language = [];
                foreach (config('app.form_lang') as $key => $item) {
                    $update_multi_language['value'][$key] = $request->get('value'.$item['field'])[$value_id];
                }
                $update->update($update_multi_language);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        $countries = GeoCountryList::all();

        return view('admin.tools.geoip.countries', [
            'countries' => $countries
        ]);
    }

    /**
     * Настройки GeoIp
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function settings(Request $request)
    {
        if($request->method() == 'POST')
        {
            $env = new DotenvEditor();
            $env->changeEnv([
                'GEOIP_DRIVER'          =>  ($request->has('GEOIP_DRIVER') ? $request->get('GEOIP_DRIVER') : null),
                'MAXMIND_USER_ID'       =>  ($request->has('MAXMIND_USER_ID') ? $request->get('MAXMIND_USER_ID') : null),
                'MAXMIND_LICENSE_KEY'   =>  ($request->has('MAXMIND_LICENSE_KEY') ? $request->get('MAXMIND_LICENSE_KEY') : null),
                'IPAPI_KEY'             =>  ($request->has('IPAPI_KEY') ? $request->get('IPAPI_KEY') : null),
                'IPGEOLOCATION_KEY'     =>  ($request->has('IPGEOLOCATION_KEY') ? $request->get('IPGEOLOCATION_KEY') : null),
                'IPDATA_API_KEY'        =>  ($request->has('IPDATA_API_KEY') ? $request->get('IPDATA_API_KEY') : null),
                ]);

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }


        return view('admin.tools.geoip.settings');
    }
}
