<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.10.2019
 * Time: 15:20
 */

namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use App\Models\FaqCategory;
use Illuminate\Http\Request;

class FaqCategoryController extends Controller
{
    /**
     * Список категорий вопросов и ответов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $categories = FaqCategory::orderBy('sorting')->get();
        return view('admin.tools.faq.category.index', compact('categories'));
    }

    /**
     * Форма добавления категорий
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.tools.faq.category.create');
    }

    /**
     * Обработка и добавление категорий
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            FaqCategory::create($options);

            flash('Категория успешно добавлена', ['alert alert-success']);
            return redirect()->route('faq-category.index');
        }
    }

    /**
     * Форма редактирования категорий
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = FaqCategory::findOrFail($id);
        return view('admin.tools.faq.category.edit',compact('item'));
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $group = FaqCategory::findOrFail($id);

        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [];
            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }

            $group->update($options);

            flash("Категория {$group->name} успешно обновлена", ['alert alert-success']);
            return redirect()->route('faq-category.index');
        }
    }

    /**
     * Удаление уведомлений
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }
        $item = FaqCategory::findOrFail($id);

        if($item->faq->count() > 0) {
            flash('Категорию невозможно удалить, к ней привязаны вопросы и ответы', ['alert alert-danger']);
            return redirect()->back();
        }

        $item->delete();

        return redirect()->route('faq-category.index');
    }

    /**
     * Сортировка вопросов и ответов
     */
    public function sorting()
    {
        $categories = FaqCategory::orderBy('sorting')->get();

        return view('admin.tools.faq.category.sorting', [
            'categories' => $categories
        ]);
    }
}
