<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 13.11.2019
 * Time: 13:01
 */

namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use App\Models\LogMail;

class LogEmailController extends Controller
{
    public function index()
    {
        $logs = LogMail::orderByDesc('id')->paginate(20);

        return view('admin.tools.logs_email', [
            'logs'  => $logs
        ]);
    }
}