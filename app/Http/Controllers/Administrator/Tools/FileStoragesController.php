<?php

namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\FileStorageModel;
use App\Models\Gateway;
use App\Models\GatewayMerchant;
use Defuse\Crypto\Crypto;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

/**
 * Класс для работы с хранилищем
 */
class FileStoragesController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_module_storage_disk'
    ];

    protected array $storages = [
        'ftp'   =>  'Внешний FTP Сервер',
        'yandex' => 'Яндекс Облако (Object Storage)',
        's3' => 'Amazon Cloud (S3)',
        's3_all' => 'Любое S3 совместимое хранилище'
    ];


    /**
     * Переадресация
     *
     * @var string
     */
    protected string $redirectPath = '/tools/file-storages';


    public function index(Request $request)
    {
        $items = FileStorageModel::orderByDesc('id')->get();
        return view('admin.tools.file_storages.index', [
            'items' => $items,
            'storages' => $this->storages
        ]);
    }

    /**
     * Обработка и добавление мерчанта
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->has('action'))
        {
            if ($request->get('action') == 'settings') {
                // Обновление конфига
                $array = [];
                foreach ($this->allowFiltered as $item) {
                    if ($request->has($item) and is_array($request->get($item))) {
                        $array[$item] = implode(',', $request->get($item));
                    } else {
                        $array[$item] = ($request->has($item) ? $request->get($item) : null);
                    }
                }
                iEXSetting($array);
                flash('Настройки успешно сохранены', ['alert alert-success']);
                return redirect()->back();
            }
        }
        // Проверка входных параметров
        $validator = Validator::make($request->all(), [
            'disk' => ['required']
        ]);

        // Если при создании найдены ошибки, дальше не пускаем
        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            // Добавление нового мерчанта
            $item = FileStorageModel::create([
                'disk_type'  => $request->get('disk')
            ]);

            $item->update([
                'disk' => $request->get('disk').'_'.$item->id
            ]);

            // Параметры, для создании конфигруационного файла для мерчанта
            $dir = storage_path('/file_storages/');
            $filename = storage_path('/file_storages/'.$item->disk).'.php';

            if(!File::isDirectory($dir)) {
                File::makeDirectory($dir, 0775);
            }

            // Получение параметров, по умолчанию
            $defaultParameters = \Config::get('storages.default_parameters.'.$item->disk_type);


            if(!File::exists($filename)) {
                File::put($filename,  encrypt_protect_gateway_keys($defaultParameters));
            }

            flash("Хранилище {$item->name} успешно добавлено", ['alert alert-success']);
            return redirect()->to(
                admin_base_path(sprintf('%s/%s/edit', $this->redirectPath, $item->id))
            );
        }
    }

    /**
     * Редактирование
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function edit(int $id, Request $request)
    {
        $item = FileStorageModel::find($id);
        $data = protect_storages_keys($item->disk);
        $storages = $this->storages;

        return view('admin.tools.file_storages.edit', compact('item', 'data', 'storages'));
    }

    /**
     * Обработка и обновление
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        $item = FileStorageModel::find($id);

        $validator = Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $config = protect_storages_keys($item->disk);

            // Получение параметров, по умолчанию
            $defaultParameters = \Config::get('storages.default_parameters.'.$item->disk_type);
            $only_keys = collect($defaultParameters)->keys()->toArray();

            $env_data = collect($request->all())->filter(function ($value) {
                return !empty($value);
            })->only($only_keys)->toArray();

            if(empty($defaultParameters)) {
                $mergeParams = $config;
            } else {
                $mergeParams = array_merge($defaultParameters, $config, $env_data);
            }
            if($item->disk_type == 'ftp') {
                $mergeParams['driver'] = 'ftp';
            } else {
                $mergeParams['driver'] = 's3';
            }

            $filename = storage_path('/file_storages/'.$item->disk).'.php';
            if(File::isFile($filename)) {
                File::put($filename,  encrypt_protect_gateway_keys($mergeParams));
            }

            $options = [
                'name'                          =>  $request->get('name'),
                'status'                        =>  ($request->has('status') ? 1 : 0)
            ];

            // Обновление настроек в базе
            $item->update($options);

            flash("Данные {$item->name} успешно обновлен", ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Удаляем мерчант
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = FileStorageModel::find($id);

        // Удаляем конфигурационный файл
        $filename = storage_path('/file_storages/'.$item->disk).'.php';
        iex_file_delete($filename);

        flash("Хранилище {$item->name} успешно удален", ['alert alert-success']);
        $item->delete();

        return redirect()->to(
            admin_base_path($this->redirectPath)
        );
    }
}
