<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Events\LiveNotificationEvent;
use App\Http\Controllers\Controller;
use App\Models\LiveNotification;
use Illuminate\Http\Request;

class LiveNotificationController extends Controller
{
    /**
     * Список уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $notifications = LiveNotification::orderByDesc('id')->get();
        return view('admin.tools.live-notification.index', compact('notifications'));
    }

    /**
     * Форма добавления уведомлений
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.tools.live-notification.create');
    }

    /**
     * Обработка и добавление уведомлений
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $event = LiveNotification::create([
            'text'    =>  $request->text,
            'text_alt'  => $request->has('text_alt') ? $request->get('text_alt') : null,
            'color'  =>  $request->color,
            'timeout' => ($request->has('timeout') ? $request->get('timeout') : 0),
        ]);

        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new LiveNotificationEvent($event));
        }

        return redirect(config('admin.directory').'/tools/live-notification');
    }

    /**
     * Форма редактирования уведомлений
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = LiveNotification::findOrFail($id);
        return view('admin.tools.live-notification.edit',compact('item'));
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $notification = LiveNotification::findOrFail($id);
        $this->validate($request, [
            'text' => 'required|max:1000',
            'color' => 'required',
        ]);

        $notification->update([
            'text'    =>  $request->text,
            'text_alt'  => $request->has('text_alt') ? $request->get('text_alt') : null,
            'color'  =>  $request->color,
            'timeout' => ($request->has('timeout') ? $request->get('timeout') : 0),
        ]);

        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new LiveNotificationEvent($notification));
        }

        return redirect()->route('live-notification.index');
    }

    /**
     * Удаление уведомлений
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $role = LiveNotification::findOrFail($id);
        $role->delete();

        return redirect()->route('live-notification.index');
    }
}
