<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 12.10.2017
 * Time: 9:10
 */

namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ToolsController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'grates_filename',
        'grates_inactive_currencies',
        'grates_number_format',
        'grates_format_xml',
        'grates_format_json',
        'grates_format_txt',
        'grates_is_minamount',
        'grates_is_maxamount',
        'grates_type_number_format',
        'is_in_type_tofee',
        'is_in_type_fromfee',
        'grates_inactive_min_reserve',
        'grates_static',
        'grates_is_disabled_export_file'
    ];


    public function generator_currency(Request $request)
    {
        if($request->getMethod() == 'POST')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }

        return view('admin.tools.generator_currency');
    }
}
