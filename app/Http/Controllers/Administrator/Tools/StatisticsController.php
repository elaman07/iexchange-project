<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.10.2019
 * Time: 7:42
 */

namespace App\Http\Controllers\Administrator\Tools;


use App\Http\Controllers\Controller;
use App\Models\InfoStatistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StatisticsController extends Controller
{
    /**
     * Список статистики
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $statistics = InfoStatistic::orderBy('sorting')->get();

        return view('admin.tools.statistics.index', [
            'statistics' => $statistics
        ]);
    }

    /**
     * Добавить статистику
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.tools.statistics.create');
    }

    /**
     * Обработка и добавление статистики
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        if($request->has('actions'))
        {
            foreach ($request->get('item_id') as $value) {
                InfoStatistic::find($value)->update([
                    'status' =>  (isset($request->status[$value]) ? $request->status[$value] : 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('statistics.index');
        }


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required|numeric',
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {
            $item = InfoStatistic::create([
                'name'          =>  $request->get('name'),
                'value'         => ($request->has('value') ? $request->get('value') : null),
                'account_number'         => ($request->has('account_number') ? $request->get('account_number') : null),
                'link'         => ($request->has('link') ? $request->get('link') : null),
                'type'          => ($request->has('type') ? $request->get('type') : null),
                'is_automatic'  => ($request->has('is_automatic') ? $request->get('is_automatic') : 0),
            ]);

            flash($item->name.' успешно добавлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования статистики
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = InfoStatistic::findOrFail($id);

        return view('admin.tools.statistics.edit', compact('item'));
    }

    /**
     * Обработка и обновление статистики
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'value' => 'required',
            'type' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = InfoStatistic::findOrFail($id);
            $item->update([
                'name' =>  $request->get('name'),
                'value' => ($request->has('value') ? $request->get('value') : null),
                'account_number'         => ($request->has('account_number') ? $request->get('account_number') : null),
                'link'         => ($request->has('link') ? $request->get('link') : null),
                'type' => ($request->has('type') ? $request->get('type') : null),
                'is_automatic' => ($request->has('is_automatic') ? $request->get('is_automatic') : 0),
            ]);

            flash($item->name.' успешно обновлена', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление статистики
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): \Illuminate\Http\RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }
        $item = InfoStatistic::findOrFail($id);
        flash($item->name .' успешно удалена', ['alert alert-success']);
        $item->delete();

        return redirect()->back();
    }

    /**
     * Сортировка статистики
     */
    public function sorting()
    {
        $links = InfoStatistic::orderBy('sorting')->get();
        return view('admin.tools.statistics.sorting', compact('links'));
    }
}
