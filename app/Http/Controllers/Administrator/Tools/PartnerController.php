<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PartnerController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_view_logotype_partner',
    ];


    /**
     * Список партнеров
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $partners = Partner::orderBy('sorting')->get();

        return view('admin.tools.partner.index', [
            'partners'  =>  $partners
        ]);
    }

    /**
     * Форма добавления нового партнера
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request)
    {
        return view('admin.tools.partner.create');
    }

    /**
     * Обработка и добавление партнера
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
       
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'logo' => 'required|image|mimes:jpg,jpeg,png,gif,svg'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $logo = $request->file('logo');
            $filename = sprintf('%s.%s', Str::random(13), $logo->getClientOriginalExtension());

            $destinationPath = public_path('/storage');
            $logo->move($destinationPath, $filename);

            $options = [
                'logo' => $filename
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
                $options['link'][$key] = $request->get('link'.$item['field']);
            }

            Partner::create($options);

            flash(sprintf('Партнер %s успешно добавлен', $request->get('name')), ['alert alert-success']);
            return redirect(admin_base_path('/tools/partners/'));
        }
    }

    /**
     * Форма, редактирования партнера
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $item = Partner::find($id);
        return view('admin.tools.partner.edit', [
            'item' => $item
        ]);
    }

    /**
     * Обработка и обновление партнера
     *
     * @param int $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'logo' => 'image|mimes:jpg,jpeg,png,svg,gif'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = Partner::find($id);

            $options = [];
            if($request->file('logo') != null)
            {
                ///Удаление предыдущих фото
                File::delete(public_path('storage/'.$item->logo));

                $logo = $request->file('logo');
                $filename = sprintf('%s.%s', Str::random(13), $logo->getClientOriginalExtension());

                $destinationPath = public_path('/storage');
                $logo->move($destinationPath, $filename);

                $options['logo'] = $filename;
            }

            foreach (config('app.form_lang') as $key_update => $item_update) {
                $options['name'][$key_update] = $request->get('name'.$item_update['field']);
                $options['link'][$key_update] = $request->get('link'.$item_update['field']);
            }

            $item->update($options);

            flash('Данные партнера успешно обновлены', ['alert alert-success']);
            return redirect(admin_base_path('/tools/partners/'));
        }
    }

    /**
     * Удаление партнера
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = Partner::find($id);

        ///Удаление предыдущих фото
        File::delete(public_path('storage/'.$item->logo));
        $item->delete();

        flash('Партнер успешно удален', ['alert alert-success']);
        return redirect(admin_base_path('/tools/partners/'));
    }

    /**
     * Сортировка партнеров
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sorting()
    {
        $partners = Partner::orderBy('sorting')->get();
        return view('admin.tools.partner.sorting', [
            'partners' => $partners
        ]);
    }
}
