<?php
namespace App\Http\Controllers\Administrator\Tools;

use App\Http\Controllers\Controller;
use App\Models\SocialAuthSystem;
use Illuminate\Http\Request;

class AuthSystemController extends Controller
{

    public function index()
    {
        $authSystem = SocialAuthSystem::orderBy('sorting')->get();
        return view('admin.tools.auth-system.index', [
            'auth_systems'    =>  $authSystem
        ]);
    }

    /**
     * Добавить нового сервиса
    */
    public function create()
    {
        return view('admin.tools.auth-system.create');
    }

    /**
     * Обработка и добавление нового сервиса
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->has('actions')) {

            foreach ($request->get('item_id') as $value) {
                SocialAuthSystem::find($value)->update([
                    'status'            =>  (isset($request->status[$value]) ? $request->status[$value] : 0),
                    'client_id'         =>  (isset($request->client_id[$value]) ? $request->client_id[$value] : 0),
                    'client_secret'     =>  (isset($request->client_secret[$value]) ? $request->client_secret[$value] : 0),
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('auth-system.index');
        }


        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required'],
            'alias' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = SocialAuthSystem::create([
                'name'  =>  $request->get('name'),
                'alias' =>  $request->get('alias'),
                'status' => ($request->has('status') ? $request->get('status') : 0),
                'client_id' =>  'YOUR_API_KEY',
                'client_secret' => 'YOUR_API_SECRET'
            ]);

            flash('Сервис '.$item->name.' успешно добавлен', ['alert alert-success']);
            return redirect()->route('auth-system.create');
        }
    }

    /**
     * Форма редактирования сервиса
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = SocialAuthSystem::find($id);
        return view('admin.tools.auth-system.edit', compact('item'));
    }

    /**
     * Обработка и обновление сервиса
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $item = SocialAuthSystem::find($id);

        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required'],
            'alias' => ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item->update([
                'name'  =>  $request->get('name'),
                'alias' =>  $request->get('alias'),
                'status' => ($request->has('status') ? $request->get('status') : 0)
            ]);

            flash("Сервис {$item->name} успешно обновлен", ['alert alert-success']);
            return redirect()->route('auth-system.index');
        }
    }

    /**
     * Удаление сервиса
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $item = SocialAuthSystem::find($id);
        flash("Сервис {$item->name} успешно удален", ['alert alert-success']);
        $item->delete();

        return redirect()->route('auth-system.index');
    }

    /**
     * Сортировка сервисов
     */
    public function sorting()
    {
        $links = SocialAuthSystem::orderBy('sorting')->get();
        return view('admin.tools.auth-system.sorting', compact('links'));
    }
}