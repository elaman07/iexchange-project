<?php
namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\FavoriteLink;
use App\Models\GroupParserExchange;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FavoritesController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected array $allowFiltered = [
        'is_not_favorites_left_menu'
    ];

    /**
     * Управления избранными страницами
     *
     * @return Factory|View
     */
    public function control(Request $request)
    {
        $favorites = FavoriteLink::where('id_user', auth()->id())->orderBy('sorting')->get();
        return view('admin.favorites', compact('favorites'));
    }

    public function controlPost(Request $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }

            iEXSetting($array);
            flash(__('Настройки успешно сохранены'), ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Сортировка разделов
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sorting(Request $request): JsonResponse
    {
        foreach ($request->item as $key => $item)
        {
            FavoriteLink::where([
                ['id_user', auth()->id()]
            ])->where('id', $item)->update([
                'sorting'    =>  $key,
            ]);
        }

        return response()->json([
            'status'    =>  0
        ]);
    }

    /**
     * Добавить страницу в избранное
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function add(Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $request->validate([
            'url' => ['required'],
            'name' => ['required', 'string']
        ]);

        $link = explode(config('admin.route_path'), $request->get('url'));

        if(!is_null($link[1])) {
            FavoriteLink::updateOrCreate([
                'link' => $link[1],
                'id_user' => $request->user()->id
            ], [
                'name' => $request->get('name'),
                'link' => $link[1],
                'id_user' => $request->user()->id
            ]);

            flash(__('Ссылка успешно добавлено в избранное'), ['alert alert-success']);
            return redirect()->to($request->get('url'));
        }
        return redirect()->to(admin_base_path('/'));
    }

    /**
     * Удаляем из избранных
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(int $id, Request $request): RedirectResponse
    {
        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        FavoriteLink::where([
            ['id', $id],
            ['id_user', $request->user()->id]
        ])->delete();
        flash(__('Страница успешно удалена из избранных'), ['alert alert-success']);

        if($request->has('redirect_page')) {
            return redirect()->to($request->redirect_page);
        }

        return redirect()->back();
    }
}
