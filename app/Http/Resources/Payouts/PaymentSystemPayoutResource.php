<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 09.03.2020
 * Time: 12:04
 */

namespace App\Http\Resources\Payouts;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentSystemPayoutResource extends JsonResource
{
    /**
     * Преобразуйте коллекцию ресурсов в массив.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
            'type' => 'payment_system'
        ];
    }
}