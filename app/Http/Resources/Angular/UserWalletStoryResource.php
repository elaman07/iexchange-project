<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 08.03.2021
 * Time: 16:36
 */

namespace App\Http\Resources\Angular;

use App\Http\Resources\Rates\FormFieldsResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserWalletStoryResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->collection->mapWithKeys(function($item, $key)
        {
            return [
                $item->id => [
                    'id' => $item->id,
                    'type' => 'payment_system',
                    'attributes' => [
                        'payment_system_id' => $item->id_currency,
                        'account' => $item->wallet,
                        'fio' => null,
                        'unk' => null
                    ]]
            ];
        });


        return [
            'data' => $data
        ];
    }
}
