<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 09.03.2020
 * Time: 10:23
 */

namespace App\Http\Resources\Angular;

use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Models\Reward;
use App\Models\UserAuth;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class ReferralIncomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $referral = ReferralSystemFacade::partner($this);

        return [
            'id' => $this->id,
            'type' => 'user',
            'attributes' => [
                'referral_hash' => $referral->referral_hash(),
                'referral_balance' => $referral->balance(),
                'referral_num'      =>  $referral->num(),
            ]
        ];
    }
}
