<?php
namespace App\Http\Resources\Angular;


use App\Http\Resources\CurrencyResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;

class ReferralResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->map(function ($item) {
            return [
                'id' => $item->id,
                'type' => 'referral_charge',
                'attributes' => [
                    'status' => 'referral',
                    'created_at' => Carbon::parse($item->created_at)->diffForHumans(),
                    'amount' => $item->bonus_number,
                    'currency' => config('crypto.currency_payout'),
                    'order_id' => iEXSetting('client_id_type_for_order') == 1 ?  $item->tasks->public_id : $item->tasks->id,
                ]
            ];
        });
    }
}
