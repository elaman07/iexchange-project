<?php

namespace App\Http\Resources\V2\Order;

use App\Common\Packages\Editor\EditorFacade;
use App\Common\Packages\Order\Facades\OrderWalletFacade;
use App\Common\Packages\Order\Wallet\PaymentWallet;
use App\Common\Packages\Payment\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Models\Currency;
use App\Models\CurrencyNotification;
use App\Models\CurrencyTemplate;
use App\Models\DirectionNotification;
use App\Models\DirectionTemplate;
use App\Models\Reserve;
use App\Models\TaskField;
use App\Models\User;
use App\Models\VerificationCardCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

class OrderProcessResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string|null
     */
    public static $wrap = 'data';

    /**
     * Название статусов в тест формате
     *
     * @var array
     */
    protected array $statusText = [
        2 => 'pending',
        9 => 'merchant',
        3 => 'process',
        7 => 'pay',
        4 => 'success',
        8 => 'frozen'
    ];

    #[ArrayShape(['id' => "mixed", 'order_id' => "mixed", 'type' => "string", 'attributes' => "array", 'relationships' => "array"])]
    public function toArray(Request $request): array
    {
            // Запрос на получение кодов валют
        $currency = Currency::with(['payment' => function($q) {
            $q->select('id', 'name', 'enable_svg', 'logo', 'is_local_image');
        }, 'code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'reserve' => function($q) {
            $q->select('id', 'id_currency', 'summa');
        }, 'requisites_fields' => function ($q) {
            $q->select('*');
        }])->whereIn('id', [$this->direction_exchange->id_currency1, $this->direction_exchange->id_currency2])->get()->keyBy('id');

        $user_info = User::select('id', 'email', 'name')->where('id', '=', $this->id_user)->first();

        // Информация по валюте (Отдаю)
        $in_currency = $currency[$this->direction_exchange->id_currency1];
        // Информация по валюте (Получаю)
        $out_currency = $currency[$this->direction_exchange->id_currency2];

        // Получаем всю необходимую информацию по выбранной заявке
        $detail = TransactionFacade::init($this->resource);

        // Статус верификации карты
        $status_verify = $detail->isVerificationCard($in_currency);
        // Если верификация пройдена то, генерируем реквизиты
        if($status_verify == 1 and $this->is_wallet_issued == 0) {
            $this->resource->update(['is_wallet_issued' => 1]);
            OrderWalletFacade::call($this->resource)->toArray();
        }

        // Получаем резерв (Получаю)
        $out_reserve = Reserve::select('id', 'id_currency')->where('id_currency', $this->direction_exchange->id_currency2)->first();

        $public_id = $this->public_id;
        // Получаем номер счета куда клиент отправил средства
        $receive_wallet = $detail->getAddresses();
        $address = $receive_wallet;
        $address_tag = null;
        if(is_array($receive_wallet)) {
            $address = $receive_wallet['address'] ?? null;
            $address_tag = $receive_wallet['memo_id'] ?? null;
        }


        // Проверяем, есть ли валюты с кодами
        $is_income_code = false;
        if(isset($this->merchant) and in_array($this->merchant->gateway->alias, ['exmogiftcard', 'whitebitcodes', 'kunacodes'])) {
            $is_income_code = true;
        }

        // Время обработки заявки
        $start_time = Carbon::now();
        $duration = Carbon::parse($this->created_at)->addSeconds((int)iEXSetting('max_time_task'));
        $lead_time = 0;
        if($duration->gt($start_time)) {
            $lead_time = $duration->diffInSeconds($start_time);
        }
        // Получаем статус заявки
        $status = $this->statusText[$this->status] ?? 'cancel';

        // Получение информационных полей (Кэшируем)
        $payment_field_fields = Cache::remember('payment_field_fields_'.$public_id, Carbon::now()->addMinutes(10), function() {
            $cache_fields = [];
            if(isset($this->payment_requisites->requisites_info_fields)) {
                $cache_fields = $this->payment_requisites->requisites_info_fields->pluck('value_name', 'key_name');
            }
            return $cache_fields;
        });

        $in_icon_url = '/storage/payment_systems/'.$in_currency['payment']['logo'];
        if(!empty(iEXSetting('storage_disk_payment_system')) and $in_currency['payment']['is_local_image'] == 1) {
            $in_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$in_currency['payment']['logo'], false);
        }

        $out_icon_url = '/storage/payment_systems/'.$out_currency['payment']['logo'];
        if(!empty(iEXSetting('storage_disk_payment_system')) and $out_currency['payment']['is_local_image'] == 1) {
            $out_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$out_currency['payment']['logo'], false);
        }

        $response = [
            'id'   =>  (string)iEXSetting('client_id_type_for_order') == 1 ?  $public_id : $this->id,
            'order_id' => $this->id,
            'type' => 'order',
            'attributes' => [
                'url_pay'   =>  url('/order-pay/'. $this->public_id),
                'public_id' =>  $public_id,
                'created_at'    =>  $this->created_at->translatedFormat('d F Y, H:i'),
                'created_date'    =>  $this->created_at->translatedFormat('d M Y'),
                'is_allow_file'     =>  (int)$in_currency->is_allow_file,
                'payment_system_from_id' => $this->direction_exchange->id_currency1,
                'payment_system_to_id' => $this->direction_exchange->id_currency2,
                'fio_in'            =>  $this->sender_fullname,
                'fio_out'           =>  $this->recipient_fullname,
                'is_new_user'       =>  $this->is_new_user,
                'income_account'    =>  $this->from_shot,
                'outcome_account'   =>  $this->to_shot,
                'is_income_code'     =>  $is_income_code,
                'income_amount' => [
                    //'amount'    =>  (float)$this->give_price,
                    'currency'  =>  $in_currency['code_currency']['name'],
                    'name'      =>  $in_currency['payment']['name'],
                    'image'      =>  $in_icon_url,
                    'number_format' => $in_currency['number_format'],
                ],
                'outcome_amount' => [
                    //'amount'   =>  (float)$this->receiving_price,
                    'currency' =>  $out_currency['code_currency']['name'],
                    'name'      => $out_currency['payment']['name'],
                    'image'      => $out_icon_url,
                    'number_format' => $out_currency['number_format'],
                ],
                'income_qrcode' =>  [
                    'is_qrcode' =>  (bool)$in_currency['is_qrcode'],
                    'prefix'    =>  (string)$in_currency['prefix_qrcode']
                ],
                'income_payment_timeout' =>  $lead_time,
                'pay_invoice_url'   =>  null,
                'status' =>  $status,
                'buttons' => [
                    'i_pay' => $this->direction_exchange->order_button_i_pay,
                    'i_pay_text' => $this->direction_exchange->order_button_i_pay_text,
                ],
            ],
            'relationships' => [
                'user' => [
                    'type' => 'user',
                    'attributes' => [
                        'is_auth'   => (bool)auth()->check(),
                        'email' =>  $user_info->email,
                        'name' => $user_info->name
                    ],
                ],
                'income_payment_system' => [
                    'data' => [
                        'id' =>  $this->direction_exchange->id_currency1,
                        'type' => 'payment_system',
                        'attributes' => [
                            'name'                  => ($in_currency['visible_code_currency'] == 0) ? $in_currency['payment']['name'].' '.$in_currency['code_currency']['name'] : $in_currency['payment']['name'],
                            'payment_system'        => $in_currency['payment']['name'],
                            'letter_cod'            => $in_currency['designation_xml'],
                            'currency_iso_code'     => $in_currency['code_currency']['name'],
                            'icon_url'              => $in_icon_url,
                            'color'         => (bool)iEXSetting('is_gradient_text_color') ? $in_currency['text_color'] : '',
                            'field_name_from'   =>  $in_currency['field_name_from']
                        ]
                    ]
                ],
                'outcome_payment_system' => [
                    'data' => [
                        'id' =>  $this->direction_exchange->id_currency2,
                        'type' => 'payment_system',
                        'attributes' => [
                            'name'                  => ($out_currency['visible_code_currency'] == 0) ? $out_currency['payment']['name'].' '.$out_currency['code_currency']['name'] : $out_currency['payment']['name'],
                            'payment_system'        => $out_currency['payment']['name'],
                            'letter_cod'            => $out_currency['designation_xml'],
                            'currency_iso_code'     => $out_currency['code_currency']['name'],
                            'icon_url'              => $out_icon_url,
                            'color'                 => (bool)iEXSetting('is_gradient_text_color') ? $out_currency['text_color'] : '',
                            'field_name_to'         =>  $out_currency['field_name_to']
                        ]
                    ]
                ],

                'reserve' => [
                    'id' =>  $out_reserve->id,
                    'type' => 'reserve'
                ]
            ]
        ];

        // Если есть промо-код
        if($this->id_promo_code > 0)
        {
            $response['attributes']['promo_codes'] = [
                'name' => $this->promo_code->name,
                'percent' => $this->promo_code->discount_percent
            ];
        }

        // Вывод информационных полей
        $response['attributes']['payment_field'] = [
            'name' => ($in_currency['account_number_field'] ?? null),
            'comment' => ($in_currency['account_number_field_text'] ?? null),
            'value' => $address,
            'fields' => $payment_field_fields,
            'request_text' => ($address == '[request_payment]' ? $this->payment_requisites->text_request_payment : '')
        ];


        if(isset($this->payment_requisites) and $this->payment_requisites->photo_status == 1 and !empty($this->payment_requisites->photo_name)) {
            $response['attributes']['payment_field']['image'] = '/storage/'.$this->payment_requisites->photo_name;
        }

        // Получение данных по QR Code
        if($in_currency['is_qrcode'])
        {
            // income_qrcode
            $value = (!empty($in_currency['prefix_qrcode']) ? $in_currency['prefix_qrcode'].$address : $address);
            // Привязываем сумму к QRCode
            if($in_currency['is_qrcode_amount'])
                $value .= '?amount='.$this->give_price;

            $response['attributes']['income_qrcode']['value'] = $value;
        }



        if($in_currency['is_email_verification_modal'] == 0) {
            $verifyEmail = true;
        } else {
            $verifyEmail = !is_null($this->user->email_verified_at);
        }

        $response['attributes']['is_email_verification'] = $verifyEmail;
        $response['attributes']['is_verification'] = true;
        $response['attributes']['verification_status'] = $status_verify;


        if(in_array($status_verify, [-1, 0]))
        {
            $data_verification = \Cache::remember('order-verification-cat', Carbon::now()->addMinutes(20), function() {
                $categories_verification = VerificationCardCategory::where('status', '=', 1)->orderBy('sorting')->get();
                $data_verification = [];
                foreach ($categories_verification as $category)
                {
                    $data_verification[] = [
                        'name' => $category->name,
                        'details' => $category->instructions->toArray()
                    ];
                }

                return $data_verification;
            });

            $description_verification_card = iEXContentLanguage('description_verification_card');
            if(!empty($in_currency['verification_info'])) {
                $description_verification_card = $in_currency['verification_info'];
            }

            if((int)iEXSetting('is_allow_panel_operation_verification') == 1) {
                $this->update(['int_status_verification_card' => 1]);
            }
            $response['attributes']['verifications'] = [
                'text'  =>  $description_verification_card,
                'qrcode'  =>  url('/order/'.$this->public_id),
                'error' => iEXContentLanguage('error_verification_card'),
                'items' =>  $data_verification,
            ];
        }


        // Номер денежного перевода
        if($this->direction_exchange->is_num_transaction)
        {
            $response['attributes']['num_tx'] = [
                'label' => (empty($this->direction_exchange->num_transaction_label) ? 'NoName' : $this->direction_exchange->num_transaction_label)
            ];
        }

        // Примечание к транзакции
        if($this->direction_exchange->is_note_tx) {
            $response['attributes']['note_tx'] = [
                'label' => (empty($this->direction_exchange->note_tx_label) ? 'NoName' : $this->direction_exchange->note_tx_label)
            ];
        }

        // Черный список мошенников
        $response['attributes']['is_black_list'] = ((int)iEXSetting('bestchange_allow_order') ? false : $this->task_info->is_scam);
        $payMerchant = false;
        $merchant_pay = $detail->getMerchant();

        // Цена отдаю
        $in_price = $this->give_price;
        $out_price = $this->receiving_price;

        $currency_in_name = sprintf('%s %s',
            $in_currency['payment']['name'],
            $in_currency['code_currency']['name'],
        );

        $currency_out_name = sprintf('%s %s',
            $out_currency['payment']['name'],
            $out_currency['code_currency']['name'],
        );


        // Получаем шаблоны валют
        $templates_currencies = CurrencyTemplate::all();
        // Получаем шаблоны направлений
        $templates_directions = DirectionTemplate::all();

        // Инструкция к оплате
        $templates_instruction_currency_in = $templates_currencies->first(function($item) {
            return $item->id_type == 0;
        });
        // Инструкция по оплате
        $templates_instruction_direction = $templates_directions->first(function($item) {
            return $item->id_type == 0;
        });
        // Инструкция по оплате
        $templates_other_docs = $templates_directions->first(function($item) {
            return $item->id_type == 4;
        });



        // Инструкция к оплате
        $bbcodes_instructions = [
            '[direction]', '[created_at]', '[rate]', '[city]','[country]','[in_amount]','[out_amount]','[in_code]',
            '[out_code]', '[in_currency]', '[out_currency]', '[public_id]', '[order_id]', '[profit_percent]',
            '[account]', '[to_account]'];
        $bbcodes_instructions_replace = [
            direction_name($this->direction_exchange),
            $this->created_at->translatedFormat('d M Y H:i'),
            $this->course_display,
            $this->task_info->city_name,
            $this->task_info->country_name,
            $this->give_price,
            $this->receiving_price,
            $in_currency['code_currency']['name'],
            $out_currency['code_currency']['name'],
            $currency_in_name,
            $currency_out_name,
            $this->public_id,
            $this->id,
            $this->direction_exchange->profit,
            $address,
            $this->to_shot
        ];


        // Инструкция к оплате
        $instruction_currency_in = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $in_currency['instruction_exchange']);
        if(!empty($templates_instruction_currency_in))
        {
            if($templates_instruction_currency_in->type_view_info == 1) {
                $instruction_currency_in = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $templates_instruction_currency_in->text);
            }elseif($templates_instruction_currency_in->type_view_info == 2 and empty($in_currency['instruction_exchange'])) {
                $instruction_currency_in = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $templates_instruction_currency_in->text);
            }
        }

        // Инструкция по оплате
        $instruction_direction = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $this->direction_exchange->instructions);
        if(!empty($templates_instruction_direction))
        {
            if($templates_instruction_direction->type_view_info == 1) {
                $instruction_direction = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $templates_instruction_direction->text);
            }elseif($templates_instruction_direction->type_view_info == 2 and empty($this->direction_exchange->instructions)) {
                $instruction_direction = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $templates_instruction_direction->text);
            }
        }

        // Дополнительный текст в процессе оплаты (внизу)
        $other_docs_direction = $this->direction_exchange->other_docs;
        if(!empty($templates_other_docs))
        {
            if($templates_other_docs->type_view_info == 1) {
                $other_docs_direction = $templates_other_docs->text;
            }elseif($templates_other_docs->type_view_info == 2 and empty($this->direction_exchange->other_docs)) {
                $other_docs_direction = $templates_other_docs->text;
            }
        }


        $response['attributes']['other_docs'] = $other_docs_direction;
        $response['attributes']['instructions'] = $instruction_direction;
        $response['attributes']['notice_process_desc'] = $this->direction_exchange->notice_process_desc;
        // Инструкция к оплате
        $response['attributes']['instruction_currency_in'] = $instruction_currency_in;

        if(isset($merchant_pay))
        {
            // Подгружаем конфигурацию
            $configProvider = PaymentFacade::configProvider($merchant_pay->gateway->alias);

            // Выдача реквизитов на отдельной странице
            if(isset($configProvider))
            {

                // Выдача реквизитов на отдельной странице
                if(isset($configProvider['is_checkout']) and $configProvider['is_checkout'])
                {
                    $payMerchant = true;
                    // Адрес для оплаты через мерчант
                    $response['attributes']['pay_invoice_url'] = route('merchant.checkout', [
                        (string)$this->transaction->transaction
                    ]);
                } else {

                    \Log::debug('---------22');
                    $isCheckoutRequisites = (isset($configProvider['receiving_requisites_default']) and $configProvider['receiving_requisites_default']);

                    if($isCheckoutRequisites) {
                        $payMerchant = true;
                        // Адрес для оплаты через мерчант
                        $response['attributes']['pay_invoice_url'] = route('merchant.checkout', [
                            (string)$this->transaction->transaction
                        ]);
                    }
                }


                $response['attributes']['is_enable_merchant_button'] = $merchant_pay->is_enable_merchant_button;
                // Если переключаемая система (из настроек мерчанта в поле "Тип оплаты")
                if(isset($configProvider['receiving_requisites_types']) and $configProvider['receiving_requisites_types'] and $merchant_pay->type_pay == 1) {
                    $payMerchant = false;
                    $response['attributes']['pay_invoice_url'] = null;
                }
            }

            // max количество заявок в день
            if($merchant_pay->day_limit_merchant > 0)
            {
                $count_order_day = $detail->getCountDayOrderIn();
                if($count_order_day > $merchant_pay->day_limit_merchant) {
                    $payMerchant = false;
                }
            }

            // макс. сумма обмена
            if($merchant_pay->max_limit_amount_order > 0 and $detail->getAmountIn() > $merchant_pay->max_limit_amount_order) {
                $payMerchant = false;
            }

            // Суточный лимит для мерчанта
            if($merchant_pay->day_limit_amount_merchant > 0)
            {
                if($detail->dayLimitAmountIn() > $merchant_pay->day_limit_amount_merchant) {
                    $payMerchant = false;
                }
            }

            if(in_array($merchant_pay->gateway->alias, ['exmogiftcard', 'whitebitcodes', 'kunacodes'])) {
                $payMerchant = false;
            }


            // Меняем сумму отдаю для мерчантов
            if($payMerchant)
            {
                if($merchant_pay->pay_amount == 1) {
                    $in_price = $this->give_price_with_comm_pay;
                } elseif($merchant_pay->pay_amount == 2) {
                    $in_price = $this->give_price_default;
                }
            }


            // Выводим инструкцию к оплате
            if(empty($merchant_pay->instruction_payment))
            {
                if(iEXSetting('type_instruction_merchant') == 0) { // Ничего не выводим
                    $response['attributes']['instructions'] = null;
                }
            } else {
                $response['attributes']['instructions'] = EditorFacade::driver('bbcode')->order($merchant_pay->instruction_payment, $this);
            }
        }

        $response['attributes']['income_amount']['amount'] = $in_price;
        $response['attributes']['outcome_amount']['amount'] = $out_price;


        // Проверяем доступен ли мерчант для метода
        $response['attributes']['pay_merchant'] = $payMerchant;

        // Шаблон Дополнительный текст в процессе оплаты (внизу) (Для отдаю)
        $template_other_docs_in = $templates_currencies->first(function($item) {
            return $item->id_type == 2;
        });


        $other_docs_in = $in_currency['other_docs_in'];
        if(!empty($template_other_docs_in))
        {
            if($template_other_docs_in->type_view_info == 1) {
                $other_docs_in = $template_other_docs_in->text;
            }elseif($template_other_docs_in->type_view_info == 2 and empty($in_currency['other_docs_out'])) {
                $other_docs_in = $template_other_docs_in->text;
            }
        }

        // Шаблон Дополнительный текст в процессе оплаты (внизу) (Для получаю)
        $template_other_docs_out = $templates_currencies->first(function($item) {
            return $item->id_type == 3;
        });



        $other_docs_out = $out_currency['other_docs_in'];
        if(!empty($template_other_docs_out))
        {
            if($template_other_docs_out->type_view_info == 1) {
                $other_docs_out = $template_other_docs_out->text;
            }elseif($template_other_docs_out->type_view_info == 2 and empty($out_currency['other_docs_out'])) {
                $other_docs_out = $template_other_docs_out->text;
            }
        }

        $response['attributes']['other_docs_in'] = $other_docs_in;
        $response['attributes']['other_docs_out'] = $other_docs_out;

        // Дополнительные поля
        $currency_fields = $this->tasks_fields_base ?? [];

        if(count($currency_fields) > 0)
        {
            $pay_fields = [];
            foreach ($currency_fields as $currency_field)
            {
                $pay_fields[] = [
                    'name' => $currency_field->field_name,
                    'value' => $currency_field->field_value,
                    'comment' => ($currency_field->requisites_fields) ? $currency_field->requisites_fields->comment : null
                ];
            }

            $response['relationships']['pay_fields'] = $pay_fields;
        } else {

            // Дополнительные поля
            $currency_fields_db = Currency::find($in_currency['id'])->requisites_fields->where('status', '=', 1);

            if(count($currency_fields_db) > 0)
            {
                $task_fields = [];
                foreach ($currency_fields_db as $currency_field)
                {
                    $value = str_replace(['[auto]', '[order_id]', '[public_id]'], [$address_tag, $this->id, $this->public_id], $currency_field->value);
                    $task_fields[] = [
                        'id_task' => $this->id,
                        'field_name' => $currency_field->name,
                        'field_value' => (!empty($currency_field->prefix) ? $currency_field->prefix : '') . !is_null($value) ? $value : $currency_field->value,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'id_field' => $currency_field->id,
                        'alias' => 'requisites'
                    ];
                }
                TaskField::insert($task_fields);

                $pay_fields = [];
                // Дополнительные поля
                $currency_fields = $this->tasks_fields_base ?? [];
                foreach ($currency_fields as $currency_field)
                {
                    $pay_fields[] = [
                        'name' => $currency_field->field_name,
                        'value' => $currency_field->field_value,
                        'comment' => ($currency_field->requisites_fields) ? $currency_field->requisites_fields->comment : null
                    ];
                }

                $response['relationships']['pay_fields'] = $pay_fields;
            }
        }


        $notification = Cache::remember('notification_direction_'.$this->public_id, Carbon::now()->addMinutes(20), function()
        {
            $notification =  DirectionNotification::where([
                ['id_direction_exchange','=', $this->id_direction_exchange],
                ['status', '=', 1]
            ])->orderBy('sorting')->get()->filter(function($filter) {
                if($filter->is_enabled_schedule == 0) return $filter;

                // Отображаем по расписанию
                if($filter->is_enabled_schedule == 1 and
                    Carbon::now()->format('H:i') > $filter->from_time and
                    Carbon::now()->format('H:i') < $filter->to_time)
                    return $filter;
            });

            return $notification->map(function($item) {
                return [
                    'text_color' => $item->text_color,
                    'bg_color' => $item->bg_color,
                    'title'     =>  $item->title,
                    'description' => $item->description,
                    'is_order_detail' => $item->is_order_detail
                ];
            })->toArray();
        });

        if(!empty($notification)) {
            $response['relationships']['notifications'] = $notification;
        }


        $currency_notification = Cache::remember('currency-notification-'.$this->direction_exchange->id_currency1, Carbon::now()->addMinutes(20), function() {
            $view = CurrencyNotification::where([
                ['id_currency', '=', $this->direction_exchange->id_currency1],
                ['status', '=', 1]
            ])->orderBy('sorting')->pluck('description','css_class');
            return $view->toArray();
        });

        $response['relationships']['income_payment_systems']['notifications'] = $currency_notification;


        // Если заявка выполнена
        if($this->status == 4)
        {
            $response['attributes']['events']['message'] = iEXContentLanguage('s_order_notify_text');
        }

        // Доп. настройки для текста (при выдаче реквизитов)
        $request_payment_text = null;
        if($address == '[request_payment]') {
            $request_payment_text = iEXContentLanguage('description_request_payment');
        }

        $response['attributes']['settings'] = [
            'request_payment_text' => $request_payment_text
        ];


        $array['attributes']['course_display'] = $this->course_display;
        // Получаем данные по наличным
        if(isset($this->task_info) and isset($this->task_info->cities) and \Module::find('Cities')->isEnabled()) {
            $array['attributes']['cities_value'] = sprintf('%s - %s', $this->task_info->country_name, $this->task_info->city_name);
        }

        return $response;
    }
}
