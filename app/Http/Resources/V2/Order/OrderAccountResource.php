<?php

namespace App\Http\Resources\V2\Order;

use App\Models\Currency;
use App\Models\DirectionTemplate;
use App\Models\LinksReview;
use App\Models\OrderStep;
use App\Models\PaymentExplorer;
use App\Models\PayTransactionHash;
use App\Models\Review;
use App\Models\TaskInfo;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

class OrderAccountResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string|null
     */
    public static $wrap = 'data';

    /**
     * Название статусов в тест формате
     *
     * @var array
     */
    protected array $statusText = [
        2 => 'pending',
        9 => 'merchant',
        3 => 'process',
        7 => 'pay',
        4 => 'success',
        8 => 'frozen'
    ];

    public function toArray(Request $request): array
    {
        $public_id = $this->public_id;

        // Запрос на получение кодов валют
        $currency = \Cache::remember('v1-currency-account-order-id-cache__'.$public_id, Carbon::now()->addMinutes(10), function() {
            return Currency::with(['payment' => function($q) {
                $q->select('id', 'name', 'enable_svg', 'logo', 'is_local_image');
            }, 'payment.explorer' => function($q) {
                $q->select('id', 'id_payment', 'link');
            }, 'code_currency' => function($q) {
                $q->select('id', 'name');
            }, 'reserve' => function($q) {
                $q->select('id', 'id_currency', 'summa');
            }])->whereIn('id', [$this->direction_exchange->id_currency1, $this->direction_exchange->id_currency2])->get()->keyBy('id');
        });

        $task_info = TaskInfo::select('id', 'id_task', 'is_aml_analysis', 'aml_riskscore', 'is_blocked_chat')->where('id_task', $this->id)->first();

        // Информация по валюте (Отдаю)
        $in_currency = $currency[$this->direction_exchange->id_currency1];
        // Информация по валюте (Получаю)
        $out_currency = $currency[$this->direction_exchange->id_currency2];
        // Получаем статус заявки
        $status = $this->statusText[$this->status] ?? 'cancel';

        // Время обработки заявки
//        $start_time = Carbon::now();
//        $duration = Carbon::parse($this->created_at)->addSeconds((int)iEXSetting('max_time_task'));
//        $lead_time = 0;
//        if($duration->gt($start_time)) {
//            $lead_time = $duration->diffInSeconds($start_time);
//        }

        $in_icon_url = '/storage/payment_systems/'.$in_currency['payment']['logo'];
        if(!empty(iEXSetting('storage_disk_payment_system')) and $in_currency['payment']['is_local_image'] == 1) {
            $in_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$in_currency['payment']['logo'], false);
        }

        $out_icon_url = '/storage/payment_systems/'.$out_currency['payment']['logo'];
        if(!empty(iEXSetting('storage_disk_payment_system')) and $out_currency['payment']['is_local_image'] == 1) {
            $out_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$out_currency['payment']['logo'], false);
        }


        $response = [
            'id'   =>  (string)iEXSetting('client_id_type_for_order') == 1 ?  $public_id : $this->id,
            'order_id' => $this->id,
            'type' => 'order',
            'attributes' => [
                'public_id' =>  $public_id,
                'created_at'        =>  $this->created_at->translatedFormat('d M Y, H:i'),
                'created_date'      =>  $this->created_at->translatedFormat('d M Y'),
                'course_display'    =>  $this->course_display,
                'income_account'    =>  $this->from_shot,
                'outcome_account'   =>  $this->to_shot,

                'income_amount' => [
                    'amount'    =>  (float)$this->give_price,
                    'currency'  =>  $in_currency['code_currency']['name'],
                    'name'      =>  $in_currency['payment']['name'],
                    'image'     => $in_icon_url,
                    'number_format'   =>  $in_currency['number_format']
                ],
                'outcome_amount' => [
                    'amount'    =>  (float)$this->receiving_price,
                    'currency'  =>  $out_currency['code_currency']['name'],
                    'name'      =>  $out_currency['payment']['name'],
                    'image'     =>  $out_icon_url,
                    'number_format'   =>  $out_currency['number_format']
                ],
                'status' =>  $status,
                'status_int' => $this->status,
                'status_string' => $this->task_status->name
            ]
        ];

        // Получаем шаблоны направлений
        $templates_directions = DirectionTemplate::all();

        // Текст для статуса "Заявка выполнена"
        $templates_status_order_success = $templates_directions->first(function($item)
        {
            return $item->id_type == 5;
        });

        // Текст для статуса "Заявка отклонена"
        $templates_status_order_failed = $templates_directions->first(function($item) {
            return $item->id_type == 6;
        });


        // Текст для статуса "Заявка выполнена"
        $status_success_direction = $this->direction_exchange->text_order_success;
        if(!empty($templates_status_order_success))
        {
            if($templates_status_order_success->type_view_info == 1) {
                $status_success_direction = $templates_status_order_success->text;
            }elseif($templates_status_order_success->type_view_info == 2 and empty($this->direction_exchange->text_order_success)) {
                $status_success_direction = $templates_status_order_success->text;
            }
        }

        // Текст для статуса "Заявка отклонена"
        $status_failed_direction = $this->direction_exchange->text_order_failed;
        if(!empty($templates_status_order_failed))
        {
            if($templates_status_order_failed->type_view_info == 1) {
                $status_failed_direction = $templates_status_order_failed->text;
            }elseif($templates_status_order_failed->type_view_info == 2 and empty($this->direction_exchange->text_order_failed)) {
                $status_failed_direction = $templates_status_order_failed->text;
            }
        }

        $blockchain_link = null;
        if(isset($in_currency['payment']['explorer']) and !empty(isset($in_currency['payment']['explorer'])) and isset($this->merchant_transaction_hash) and !empty($this->merchant_transaction_hash->transaction_hash)) {
            $blockchain_link = str_replace('{hash}', $this->merchant_transaction_hash->transaction_hash, $in_currency['payment']['explorer']['link']);
        }

        $response['attributes']['text_status'] = [
            'success' => $status_success_direction,
            'failed' => $status_failed_direction
        ];

        // Получение входящей транзакции из Blockchain
        $response['attributes']['income_transaction'] = [
            'is_statusbar'  => $this->id_merchant > 0 && $status != 'cancel',
            'is_found_tx' =>  $this->register_tx == 1,
            'blockchain_link' => $blockchain_link,
        ];

        if($this->id_merchant > 0 and $this->status == 3 and $this->task_single_log_confirm and !empty($this->task_single_log_confirm))
        {
            $confirm_data = [
                'received_confirm' => $this->task_single_log_confirm->received_confirm,
                'needed_confirm' => $this->task_single_log_confirm->needed_confirm
            ];
            $response['attributes']['income_transaction']['confirm_data'] = $confirm_data;
        }


        // Получение ссылки на Blockchain
        if(isset($out_currency['payment']['explorer']) and !empty(isset($out_currency['payment']['explorer'])) and isset($this->pay_transaction_hash) and !empty($this->pay_transaction_hash->transaction_hash)) {
            $response['attributes']['outcome_transaction'] = str_replace('{hash}', $this->pay_transaction_hash->transaction_hash, $out_currency['payment']['explorer']['link']);
        }

        if(isset($this->tasks_rejection_status)) {
            $response['attributes']['rejection'] = $this->tasks_rejection_status->name;
        }

        if(isset($this->pending_order_status) and $this->status == 8) {
            $response['attributes']['frozen_pending'] = $this->pending_order_status->name;
        }


        if($this->status == 4 and !empty(iEXContentLanguage('s_order_notify_text')))  {
            $response['attributes']['success_message'] = iEXContentLanguage('s_order_notify_text');
        }

        // Комментарий для клиента
        if($this->task_comment_user->count() > 0) {
            $response['attributes']['comment'] = $this->task_comment_user->pluck('message');
        }

        // Фото для прикрепления
        if($this->task_file->count() > 0) {
            $response['attributes']['files'] = $this->task_file->pluck('file');
        }


        // Проверка AML
        $aml_status = 0;
        if($task_info->is_aml_analysis == 1)
        {
            if($task_info->aml_riskscore > 0 and $task_info->aml_riskscore <= 30) {
                $aml_status = 1;
            } elseif($task_info->aml_riskscore >= 31 and $task_info->aml_riskscore <= 69) {
                $aml_status = 2;
            }elseif($task_info->aml_riskscore >= 70 and $task_info->aml_riskscore <= 100) {
                $aml_status = 3;
            }
        }

        $response['attributes']['aml_check_tx'] = [
            'status'    =>  $task_info->is_aml_analysis == 1,
            'result'    =>  $task_info->aml_riskscore,
            'type'      =>  $aml_status
        ];

        $response['attributes']['online_chat'] = [
            'status' => (int)iEXSetting('is_enable_order_online_chat'),
            'is_blocked' => (int)$task_info->is_blocked_chat
        ];

        // Получаем список этапов
        if($in_currency['is_enabled_step_order'] == 1 and (int)iEXSetting('is_enabled_module_order_step') == 1)
        {
            $order_steps = OrderStep::where('status', '=', 1)->orderBy('sorting')->get()->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'icon' => '/storage/'.$item->icon
                ];
            });
            $response['attributes']['steps'] = [
                'list' => $order_steps->toArray(),
                'active' => $this->id_order_step,
            ];
        }

        $response['attributes']['fields'] = \Cache::remember('currency_field_order_id__'.$public_id, Carbon::now()->addMinutes(10), function()
        {
            return [
                'in' => $this->tasks_fields_in_out->where('type_field', 'in') ?? [],
                'out' => $this->tasks_fields_in_out->where('type_field', 'out') ?? [],
            ];
        });


        // Проверяем, оставлялся ли отзыв по заявке
        $is_review = Review::where('id_task', $this->id)->exists();
        $response['attributes']['is_review'] = (int)$is_review;

        // Ссылки на отзывы
        $linksReviews = LinksReview::whereNotNull('icon')->orderBy('sorting')->get()->map(function($item) {
            return [
                'link' => $item->url,
                'icon' => '/storage/links/'.$item->icon
            ];
        });
        $response['attributes']['link_reviews'] = $linksReviews;

        return $response;
    }

}
