<?php

namespace App\Http\Resources\V2\Review;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ReviewResponse extends ResourceCollection
{
    /**
     * Преобразуйте коллекцию ресурсов в массив.
     *
     * @param Request $request
     * @return Collection
     */
    public function toArray(Request $request): Collection
    {
        return $this->collection->map(function($item) {

            // Получаем данные по

            return [
                'id' => $item->id,
                'type' => 'review_item',
                'attributes' => [
                    'created_at' =>  Carbon::parse($item->created_at)->translatedFormat('d M Y H:i'),
                    'text'          => $item->text,
                    'rate_speed'    => $item->rate_speed,
                    'name'          =>  $item->name,
                    'income_payment' => [
                        'amount' => $item['tasks']['give_price'],
                        'currency' => $item['tasks']['direction_exchange']['currency1']['payment']['name'].' '.$item['tasks']['direction_exchange']['currency1']['code_currency']['name'],
                        'logo' =>   '/storage/payment_systems/'. $item['tasks']['direction_exchange']['currency1']['payment']['logo']
                    ],
                    'outcome_payment' => [
                        'amount' => $item['tasks']['receiving_price'],
                        'currency' => $item['tasks']['direction_exchange']['currency2']['payment']['name'].' '.$item['tasks']['direction_exchange']['currency2']['code_currency']['name'],
                        'logo' =>   '/storage/payment_systems/'. $item['tasks']['direction_exchange']['currency2']['payment']['logo']
                    ]
                ],
            ];
        });
    }
}
