<?php

namespace App\Http\Resources\Sessions;

use App\Models\Reward;
use App\Models\UserAuth;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class CurrentUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $current_percent =  Cache::remember('reward_program_'.$this->id, now()->addHours(), function () {
            return Reward::where('user_id', $this->id)->first()->program->percent;
        });

        $successOrderSum = Cache::remember('order_sum_user_'.$this->id, now()->addHours(), function () {
            return iex_number_format($this->successOrderSum(), 2, true);
        });

        // Получение Токена
        $rest_api = null;
        if($this->is_enabled_restapi == 1)
        {
            if(isset(auth()->user()->tokens) and auth()->user()->tokens->count() > 0) {
                $first_api = auth()->user()->tokens->first();
                \Log::debug(json_encode($first_api));
                $rest_api = sprintf('%s|%s', $first_api->id, $first_api->token_code);
            }
        }

        $minFill = 4;
        return [
            'id' => $this->id,
            'type' => 'user',
            'attributes' => [
                'is_auth'   => (bool)auth()->check(),
                'is_verify_account' => $this->is_verify_account,
                'restapi'   => $this->restapi_key,
                'is_restapi' => $this->is_enabled_restapi,
                'name' => $this->name,
                'email' => $this->email,
                'email_security' => preg_replace_callback(
                    '/^(.)(.*?)([^@]?)(?=@[^@]+$)/u',
                    function ($m) use ($minFill) {
                        return $m[1]
                            . str_repeat("*", max($minFill, mb_strlen($m[2], 'UTF-8')))
                            . ($m[3] ?: $m[1]);
                    },
                    $this->email
                ),
                'phone' => $this->phone,
                'telegram' => $this->telegram,
                'created_at' => Carbon::parse($this->created_at)->diffForHumans(),
                'is_email_confirmed' => (boolean)!is_null($this->email_verified_at),
                'cashback_percent'  =>  $current_percent,
                'referral_balance' => $this->user_balance->balance,
                'cashback_balance' => $this->user_balance->balance_reward,
                'referral_hash' => $this->referral_links->getHashAttribute(),
                'referral_hash_url' => config('app.url').'/?ref='.$this->referral_links->getHashAttribute(),
                'personal_discount' => $this->personal_discount,
                'is_checkbox_rules' =>  $this->is_checkbox_rules,
                'is_checkbox_aml'   =>  $this->is_checkbox_aml,
                'order' => [
                    'count' => $this->order_num,
                    'sum' => $successOrderSum
                ],
            ],
            'meta' => [
                'remote_ip' => $request->ip()
            ]
        ];
    }

    public function with($request): array
    {
        $response = [];
        if($request->has('allow_log'))
        {
            $data = \Cache::remember('user-auth-log__s'.$this->id, Carbon::now()->addHours(5), function()
            {
                return UserAuth::where('id_user', $this->id)
                    ->orderBy('id', 'desc')->limit(10)->get()->map(function($item) {
                        return [
                            'browser'       =>  $item->browser,
                            'os'            =>  $item->os,
                            'ip'            =>  $item->ip,
                            'created_at'    =>  Carbon::parse($item->created_at)->translatedFormat('d M Y, H:i'),
                            'status'        =>  $item->status,
                            'country'       =>  $item->country,
                            'city'          =>  $item->city
                        ];
                    });
            });


            $response['included']['logs'] = $data;
        }

        return $response;
    }
}
