<?php
namespace App\Http\Resources\API;


use Illuminate\Http\Resources\Json\JsonResource;

class ReferralClientsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->map(function ($item) {
            return [
                'id' => $item->user_id,
                'type' => 'referral_user',
                'attributes' => [
                    'email' => $item->user->email,
                    'created_at' => $item->user->created_at->toDateTimeString(),
                ]
            ];
        });
    }
}
