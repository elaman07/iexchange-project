<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 23.02.2020
 * Time: 23:27
 */

namespace App\Http\Resources\API;


use Illuminate\Http\Resources\Json\JsonResource;

class ReferralRelationshipResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'payment_system' => [
                'data' => [
                    'id' => $this->resource->direction_exchange->id_currency1,
                    'type' => 'payment_system'
                ]
            ]
        ];
    }
}