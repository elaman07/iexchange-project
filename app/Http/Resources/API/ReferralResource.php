<?php
namespace App\Http\Resources\API;


use App\Http\Resources\CurrencyResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ReferralResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->map(function ($item) {
            return [
                'id' => $item->id,
                'type' => 'referral_charge',
                'attributes' => [
                    'status' => 'referral',
                    'charged_amount' => [
                        'original' => [
                            'amount' => $item->bonus_number,
                            'currency' => config('crypto.currency_payout')
                        ]
                    ],

                    'relationships' => new ReferralRelationshipResource($item->tasks)
                ]
            ];
        });
    }

    public function with($request)
    {
        return [
            'included' => $this->collection->pluck('tasks.direction_exchange.currency1')->unique()->values()->map(function ($item) {
                return new CurrencyResource($item);
        })
        ];
    }
}