<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'  => (string)$this->id,
            'type' => 'order',
            'attributes' => [
                'public_id'     => $this->transaction->transaction,
                'hash_id'       => (string)$this->transaction->transaction,
                'created_at'    => $this->created_at,
                'payment_system_from_id' => $this->direction_exchange->id_currency1,
                'payment_system_to_id' => $this->direction_exchange->id_currency2,
                'fio_in'        =>  $this->sender_fullname,
                'fio_out'       =>  $this->recipient_fullname,
                'income_account' => $this->from_shot,
                'outcome_account' => $this->to_shot,
                'income_amount' => [
                    'amount'    =>  (float)$this->give_price,
                    'currency'  =>  $this->direction_exchange->currency1->code_currency->name
                ],
                'outcome_amount' => [
                    'amount'    =>  (float)$this->give_price,
                    'currency'  =>  $this->direction_exchange->currency2->code_currency->name
                ],
                'income_qrcode' => [
                    'is_qrcode'   =>  (bool)$this->direction_exchange->currency1->is_qrcode,
                    'prefix'    =>  (string)$this->direction_exchange->currency1->prefix_qrcode
                ],
                'income_payment_timeout' =>  (int)iEXSetting('max_time_task'),
                'pay_invoice_url'   =>  null,
                'status'            =>  ($this->status != 4 ? 'waiting' : 'success'),
                'payment_field'     =>  [

                ],
                'relationships'     =>  new RelationshipResource($this)
            ]
        ];
    }
}
