<?php
namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class RelationshipResource extends JsonResource
{
    /**
     * Преобразуйте коллекцию ресурсов в массив.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => [
                'data' => new UserResource($this->user)
            ],
            'income_payment_system' => [
                'data' => new IncomePaymentSystemResource($this->direction_exchange)
            ],
            'outcome_payment_system' => [
                'data' => new OutcomePaymentSystemResource($this->direction_exchange)
            ],
        ];
    }
}