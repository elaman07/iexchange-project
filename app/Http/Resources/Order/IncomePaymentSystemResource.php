<?php
namespace App\Http\Resources\Order;


use Illuminate\Http\Resources\Json\JsonResource;

class IncomePaymentSystemResource extends JsonResource
{
    /**
     * Преобразуйте коллекцию ресурсов в массив.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id_currency1,
            'type' => 'payment_system'
        ];
    }
}