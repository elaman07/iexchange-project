<?php

namespace App\Http\Resources\ReserveRequest;

use Illuminate\Http\Resources\Json\JsonResource;

class ReserveRequestResource extends JsonResource
{
    /**
     * Преобразуйте коллекцию ресурсов в массив.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string)$this->id,
            'type' => 'reserve_request',
            'attributes' => [
                'email' => $this->email,
                'phone' => $this->phone,
                'amount' => [
                    'amount' => (float)$this->price,
                    'currency' => $this->currency->code_currency->name
                ]
            ],
            'relationships' => new RelationshipResource($this)
        ];
    }
}
