<?php

namespace App\Http\Resources\Rates;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommandResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection->map(function($item) {
            return [
                'label' => (!is_null($item->name) ? $item->name : 'NoName'),
                'value' => $item->amount
            ];
        });
    }
}
