<?php

namespace App\Http\Resources\Rates;

use App\Http\Resources\Rates\FormFieldsResource;
use App\Models\CurrencyAnalytics;
use App\Models\CurrencyFields;
use App\Models\CurrencyTemplate;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PaymentSystemsResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fire_in = [];
        $fire_out = [];
        if(iEXSetting('is_currencies_fire_angular') == 1) {
            $fire_in = CurrencyAnalytics::orderBy('in_amount_usd', 'desc')->limit(2)->get()->pluck('id_currency', 'id_currency')->toArray();
            $fire_out = CurrencyAnalytics::orderBy('out_amount_usd', 'desc')->limit(2)->get()->pluck('id_currency', 'id_currency')->toArray();
        }


        // Получаем шаблоны валют
        $templates_currencies = CurrencyTemplate::all();

        $payment_system = $this->collection->mapWithKeys(function($item, $key) use ($fire_in, $fire_out, $templates_currencies)
        {
            // Описание
            $templates_desc_exchange = $templates_currencies->first(function($item) {
                return $item->id_type == 1;
            });

            $desc_exchange = $item->desc_exchange;

            if(!empty($templates_desc_exchange))
            {
                if($templates_desc_exchange->type_view_info == 1) {
                    $desc_exchange = $item->text;
                }elseif($templates_desc_exchange->type_view_info == 2 and empty($desc_exchange)) {
                    $desc_exchange = $item->text;
                }
            }

            $icon_url = '/storage/payment_systems/'.$item->payment->logo;
            if(!empty(iEXSetting('storage_disk_payment_system')) and $item->payment->is_local_image == 1) {
                $icon_url = iex_dynamic_read_view_image('/payment_systems/'.$item->payment->logo, false);
            }


            $labels = [];
            if($item->id_label > 0) {
                $labels = [
                    'title' => $item->currency_labels->title,
                    'text_color' => $item->currency_labels->text_color,
                    'bg_color' => $item->currency_labels->bg_color,
                ];
            }

            return [$item->id => [
                'id' => $item->id,
                'type' => 'payment_system',
                'attributes' => [
                    'first_char'            =>  $item->first_value ?? null,
                    'is_fire_in'            => isset($fire_in[$item->id]) ? 1 : 0,
                    'is_fire_out'            => isset($fire_out[$item->id]) ? 1 : 0,
                    'name'                  => ($item->visible_code_currency == 0) ? $item->payment->name.' '.$item->code_currency->name : $item->payment->name,
                    'tech_name'             =>  $item->tech_currency_name,
                    'payment_system'        => $item->payment->name,
                    'letter_cod'            => $item->designation_xml,
                    'currency_iso_code'     => $item->code_currency->name,
                    'is_iso_code'           => $item->visible_code_currency,
                    'default_value'         => $item->convert_by,
                    'income_enabled'        => $item->visible_give,
                    'outcome_enabled'       => $item->visible_receiving,
                    'income_fee'            => (float)$item->commission_merchant_percent,
                    'income_fee_amount'     => (float)$item->commission_merchant_currency,

                    'sorting_reserve'       =>  $item->sorting_reserve,
                    'outcome_fee'           =>  0,
                    'outcome_fee_amount'    => (float)$item->commission_payment_currency,

                    // Верификация
                    'income_verify'         =>  (int)$item->is_enabled_verification,
                    'description'           => $desc_exchange,
                    'formalization_text'    => $item->formalization_text,

                    'amount_decimal'        => $item->number_format,

                    'unverified_income_fee' => (float)$item->fee_no_verified_merchant,
                    'income_notice'         => $item->notice_in,
                    'outcome_notice'        => $item->notice_out,
                    'banner_enabled'         =>  $item->is_income_banner,
                    'kyc_enabled'         =>  $item->kyc_enabled,
                    'is_kyc_checkbox'   => $item->is_kyc_checkbox,
                    'icon_url'      => $item->payment->logo,
                    'icon_url_path' => $icon_url,
                    'color'         => (bool)iEXSetting('is_gradient_text_color') ?? $item->text_color,
                    'filter' => [
                        'id' => $item->id_filter_currency,
                    ],
                    'group' => [
                        'id' => $item->id_group,
                    ],
                    'labels' => [
                        'id' => $item->id_label,
                        'item' => $labels
                    ],
                    'currency' => [
                        'symbol' => $item->code_currency->sign,
                        'sorting' => $item->sorting_1,
                    ],
                    'order_form_fields' => [
                        'field_type' => 'string',
                        'label_in' => $item->field_name_from,
                        'label_out' => $item->field_name_to,
                        'mask' => $item->mask_input
                    ],
                    'field_comment_in' => $item->field_comment_from,
                    'field_comment_out' => $item->field_comment_to,
                    'button_create_order'   => $item->button_create_order,
                    'button_create_order_text'   => $item->button_create_order_text,
                    'is_blockchain_network'  =>  $item->blockchain_network_congestion,
                    'verification_text'     =>  $item->verification_text,
                    'recount_course_text'   =>  $item->recount_course_text,

                    'income_form_fields' => new FormFieldsResource($item->currency_in_fields->where('status', '=', 0)->sortBy('sorting')),
                    'outcome_form_fields' => new FormFieldsResource($item->currency_out_fields->where('status', '=', 0)->sortBy('sorting_out')),
                    'commands'  =>  new CommandResource($item->commands)
                ]
            ]];
        });


        return [
            'data' => $payment_system
        ];
    }
}
