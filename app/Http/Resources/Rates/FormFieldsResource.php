<?php

namespace App\Http\Resources\Rates;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FormFieldsResource extends ResourceCollection
{
    /**
     * Default параметры для массива
     *
     * @var array
    */
    protected array $form_fields = [
        'main'          => [],
        'additional'    => []
    ];

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        $this->collection->map(function($item)
        {
            if(in_array($item->key_id, ['sender_fullname', 'income_unk', 'recipient_fullname', 'outcome_unk']))
            {
                $this->form_fields['main'][] = [
                    'field_type' => $item->field_type,
                    'is_required' => (bool)$item->obligatory_field == 0,
                    'name' => $item->key_id,
                    'label' => $item->name,
                    'checkbox_title' => ($item->checkbox_title ?: null),
                    'notice' => ($item->description ?: null),
                ];
            } else {

                if($item->type_field == 0) {
                    $this->form_fields['additional'][] = [
                        'key' => $item->key_id,
                        'type' => 'input',
                        'templateOptions' => [
                            'label' => $item->name,
                            'placeholder' => $item->name,
                            'required' => (bool)$item->obligatory_field == 0,
                            'appearance' => 'fill'
                        ],
                    ];
                } else {

                    $list_text = preg_replace('~[\r\n]+~', '', $item->list_text);
                    $explode_list = explode(',', $list_text);

                    $list_options = [];
                    foreach ($explode_list as $key => $value) {
                        $list_options[] = [
                            'value' => $value,
                            'label' => $value
                        ];
                    }

                    $this->form_fields['additional'][] = [
                        'key' => $item->key_id,
                        'type' => ($item->type_field == 1) ? 'select' : 'radio',
                        'templateOptions' => [
                            'label' => $item->name,
                            'options' => $list_options,
                            'required' => (bool)$item->obligatory_field == 0,
                            'appearance' => 'fill'
                        ],
                    ];
                }
            }
        });

        return collect($this->form_fields);
    }
}
