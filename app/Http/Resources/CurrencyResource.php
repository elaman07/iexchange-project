<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 23.02.2020
 * Time: 23:41
 */

namespace App\Http\Resources;


use App\Http\Resources\Rates\CommandResource;
use App\Http\Resources\Rates\FormFieldsResource;
use App\Models\CurrencyFields;
use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        // Название платежной системы
        if($this->visible_code_currency == 0) {
            $formatted_name = sprintf("%s %s", $this->payment->name, $this->code_currency->name);
        }else {
            $formatted_name = sprintf("%s", $this->payment->name);
        }

        return [
            'id' => $this->id,
            'type' => 'payment_system',
            'attributes' => [
                'name'                  => $formatted_name,
                'letter_cod'            => $this->designation_site,
                'currency_iso_code'     => $this->code_currency->name,
                'default_value'         => $this->convert_by,
                'income_enabled'        => $this->visible_give,
                'outcome_enabled'       => $this->visible_receiving,
                'income_fee'            => (float)$this->commission_merchant_percent,
                'unverified_income_fee' => (float)$this->fee_no_verified_merchant,
                'income_notice'         => $this->notice_in,
                'icon_svg' => [
                    'enable'    =>  $this->payment->enable_svg,
                    'name'      =>  $this->payment->svg_name,
                    'class'     =>  $this->payment->svg_class
                ],
                'icon_url'      => $this->payment->logo,
                'color'         => $this->border_color,
                'filter' => [
                    'id' => $this->id_filter_currency,
                ],
                'currency' => [
                    'symbol' => $this->code_currency->sign,
                    'sorting' => $this->sorting_1,
                ],
                'order_form_fields' => [
                    'field_type' => 'string',
                    'label' => $this->first_char,
                    'mask' => $this->mask_input
                ],
                'income_form_fields' => new FormFieldsResource($this->currency_in_fields),
                'outcome_form_fields' => new FormFieldsResource($this->currency_out_fields),
                'commands'  =>  new CommandResource($this->commands)
            ]
        ];
    }
}
