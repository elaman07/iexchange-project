<?php

namespace App\Widgets;

use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;

class AdminTopPartners extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        if(\Request::has('actions') and \Request::get('actions') == 'top_partners')
        {
            if(\Request::get('type') == 'off') {
                User::find(\Request::get('user_id'))->update([
                    'is_pay_referral' => 1
                ]);
            }

            if(\Request::get('type') == 'on') {
                User::find(\Request::get('user_id'))->update([
                    'is_pay_referral' => 0
                ]);
            }
        }



        $start_day = Carbon::today()->startOfDay()->toDateTimeString();
        $end_day = Carbon::today()->endOfDay()->toDateTimeString();

        $referrals_logs = ReferralLog::with(['user_admin','user_admin.user_balance'])->select('id_user')
            ->selectRaw("count(*) as count_num")
            ->selectRaw("sum(`bonus_number`) as total_amount")
            ->selectRaw("count(case when created_at BETWEEN '{$start_day}' AND '{$end_day}' then 1 end) as today")
            ->groupBy('id_user')
            ->orderByDesc('count_num')
            ->limit(3)
            ->get();

        return view('widgets.admin_top_partners', [
            'config' => $this->config,
            'referrals_logs' => $referrals_logs
        ]);
    }
}
