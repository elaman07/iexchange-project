<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 17.08.2020
 * Time: 10:10
 */

namespace App\Widgets;


use App\Models\GroupParserExchange;
use App\Models\ParserExchange;
use App\Models\TaskConvertLog;
use App\Models\TaskProfit;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;
use Spatie\Permission\Models\Role;

class OrderProfitWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $action = '';
        $avg_date = false;

        if(request()->method() == 'GET' and request()->has('widget') and request()->has('widget') == 'order_profit')
        {
            if(request()->has('action') and request()->get('action') == 'sum_month') {
                $action = 'sum_month';
                $order_profit = TaskProfit::selectRaw('created_at, sum(profit_usd) as usd')
                    ->orderBy('id','desc')->groupBy(\DB::raw("DATE_FORMAT(created_at, '%m-%Y')"))->limit(20)
                    ->cursor();
            } elseif(request()->has('action') and request()->get('action') == 'sum_year') {
                $action = 'sum_year';
                $order_profit = TaskProfit::selectRaw('created_at, sum(profit_usd) as usd')
                    ->orderBy('id','desc')->groupBy(\DB::raw("DATE_FORMAT(created_at, '%Y')"))->limit(20)
                    ->cursor();
            } elseif(request()->has('action') and request()->get('action') == 'avg_date_profit')
            {
                $avg_date = true;
                $from_date = Carbon::parse(Request::get('wid_op_duration_from'))->toDateTimeString();
                $to_date = Carbon::parse(Request::get('wid_op_duration_to'))->toDateTimeString();

                $order_profit = TaskProfit::selectRaw('created_at, sum(profit_usd) as usd')
                    ->whereBetween('created_at', [$from_date, $to_date])->sum('profit_usd');
            }

        } else {
            $order_profit = TaskProfit::selectRaw('created_at, sum(profit_usd) as usd')
                ->orderBy('id','desc')->groupBy(\DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))->limit(20)
                ->cursor();
        }


        return view('widgets.order_profit', [
            'avg_date' => $avg_date,
            'action' => $action,
            'config' => $this->config,
            'order_profit' => $order_profit,
            'request_all' => \request()->all()
        ]);
    }
}
