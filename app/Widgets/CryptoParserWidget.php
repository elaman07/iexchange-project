<?php


namespace App\Widgets;

use App\Models\GroupParserExchange;
use App\Models\ParserExchange;
use App\Models\TaskConvertLog;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Spatie\Permission\Models\Role;

class CryptoParserWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $active_count = ParserExchange::whereHas('group_parse_exchange', function($query) {
            $query->where('status', '=','1');
        })->where('status', '=', 1)->count();

        $parser = ParserExchange::orderByDesc('id')->get();

        $parser_cron = [];
        if(\Cache::has('cron-parser-last-update')) {
            $parser_cron = \Cache::get('cron-parser-last-update');
        }

        $bestchange_cron = [];
        if(\Cache::has('cron-bestchange-last-update')) {
            $bestchange_cron = \Cache::get('cron-bestchange-last-update');
        }


        return view('widgets.crypto_parser', [
            'config' => $this->config,
            'parser'            =>  $parser,
            'parser_cron'      =>  $parser_cron,
            'bestchange_cron'   =>  $bestchange_cron,
            'all_count'         =>  ParserExchange::count(),
            'active_count'      =>  $active_count,
            'all_groups'        =>  GroupParserExchange::orderBy('sorting')->get(),
            'active_source'     =>  GroupParserExchange::where('status', '=','1')->count()
        ]);
    }
}
