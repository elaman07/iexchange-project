<?php

namespace App\Widgets;

use App\Models\TaskConvertLog;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Spatie\Permission\Models\Role;

class AdminTotalExchange extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        // Суммы обменов за 3 дня
        $commonAmount = TaskConvertLog::selectRaw('created_at, sum(to_usd) as usd')
            ->orderByDesc('id')
            ->groupBy(\DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
            ->limit(3)
            ->get();

        // Список ролей
        $list_roles = User::role(Role::all())->get();


        return view('widgets.admin_total_exchange', [
            'config' => $this->config,
            'list_roles' => $list_roles,
            'common_amount' => $commonAmount
        ]);
    }
}
