<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class AboutProduct extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.about_product', [
            'config' => $this->config,
            'productLicence' => iex_licence(),
            'ioncube_version' => $this->ioncube_version()
        ]);
    }

    public function ioncube_version()
    {
        ob_start();
        phpinfo(INFO_GENERAL);
        $aux = str_replace('&nbsp;', ' ', ob_get_clean());
        if($aux !== false)
        {
            $pos = mb_stripos($aux, 'ionCube PHP Loader');
            if($pos !== false)
            {
                $aux = mb_substr($aux, $pos + 18);
                $aux = mb_substr($aux, mb_stripos($aux, ' v') + 2);

                $version = '';
                $c = 0;
                $char = mb_substr($aux, $c++, 1);
                while(mb_strpos('0123456789.', $char) !== false)
                {
                    $version .= $char;
                    $char = mb_substr($aux, $c++, 1);
                }

                return $version;
            }
        }

        return false;
    }
}
