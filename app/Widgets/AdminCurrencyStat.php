<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 13:43
 */

namespace App\Widgets;


use App\Models\Currency;
use Arrilot\Widgets\AbstractWidget;

class AdminCurrencyStat extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $currencies = Currency::active()->hideDisabled()->cursor();

        return view('widgets.currencies', [
            'config' => $this->config,
            'currencies' => $currencies
        ]);
    }
}