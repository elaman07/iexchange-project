<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 25.04.2021
 * Time: 0:00
 */

namespace App\Widgets\Main;

use App\Models\FavoriteLink;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class AdminSidenavWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $route_modules = [];

        // Список модулей
        $route_modules[] = [
            'rule' => 'admin_merchant',
            'url' => admin_base_path('/modules'),
            'name' => 'Список плагинов'
        ];


        $insert_categories = [];


        if(Config::has('modules-config'))
        {
            foreach (Config::get('modules-config') as $key => $value)
            {
                if(isset($value['existing-category'])) {
                    Config::push($value['existing-category']['category_name'], $value['existing-category']['category_value']);
                }

                if(!isset($value['admin-menu']))
                    continue;


                $route_modules[] = $value['admin-menu'];

//                $route_modules[] = [
//                    'rule' => 'admin_merchant',
//                    'url' => $url,
//                    'name' => $value['admin-menu']['title']
//                ];
            }
        }

        Config::set('admin-menu.categories.admin-layout__sections__modules', [
            'icon' => 'far fa-layer-group fa-custom-theme',
            'rules' => ['admin_merchant'],
            'categories' => $route_modules
        ]);


        $favorites = FavoriteLink::where('id_user', auth()->id())->orderBy('sorting')->get();

        return view('widgets.main.sidenav', [
            'favorites' => $favorites,
            'currentUser' => Auth::user(),
            'adminMenuSidenav' => config('admin-menu.categories'),
            'config' => $this->config,
        ]);
    }
}
