<?php

namespace App\Widgets;

use App\Models\DirectionExchange;
use App\Models\Task;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AdminStatistics extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        // Кэширование статистики на 1 час
        $stat_result = Cache::remember('admin-statistic-order', Carbon::now()->addHour(), function()
        {
            $response = collect();
            foreach (iex_order_statistics() as $item)
            {
                if(is_array($item['created_at'])) {
                    $order = Task::whereBetween('created_at', $item['created_at']);
                } else {
                    $order = Task::whereDate('created_at', $item['created_at']);
                }

                $totals = $order->selectRaw('count(*) as total')
                    ->selectRaw("count(case when status = 4 then 1 end) as success_orders")
                    ->selectRaw("count(case when status = 5 then 1 end) as failed_orders")
                    ->first();

                $response->push([
                    'n' => $item['name'],
                    'a' => $totals->total,
                    's' => $totals->success_orders,
                    'f' => $totals->failed_orders,
                    'u' => $item['created_at']

                ]);
            }
            return $response->all();
        });

        $last_orders = Task::where('is_archive', '=', 0)->orderBy('id','desc')->limit(7);
        // Не отображать спам заявки
        if(iEXSetting('order_is_allow_spam') == 0) {
            $last_orders->where('is_spam', '=', 0);
        }

        // Список посл. пользователей
        $last_users = User::orderBy('id', 'desc')->limit(6)->get();
        // Список пользователей с посл. активностью
        $last_active_users = User::orderByDesc('last_activity_at')->limit(6)->get();

        // Самые популярные направления
        $popular_exchange = Cache::remember('admin-popular-exchange', Carbon::now()->addDay(), function() {
           return DirectionExchange::leftJoin('tasks', 'id_direction_exchange', '=', 'direction_exchange.id')
               ->select('*', DB::raw("count('tasks.id') as orders_count"))
               ->where('tasks.status', '=', 4)
               ->groupBy('direction_exchange.id')
               ->orderBy('orders_count', 'desc')
               ->limit(8)
               ->get();
        });

        return view('widgets.admin_statistics', [
            'config' => $this->config,
            'stat_result' => $stat_result,
            'last_orders' => $last_orders->get(),
            'last_users' => $last_users,
            'last_active_users' => $last_active_users,
            'popular_exchange' => $popular_exchange
        ]);
    }
}
