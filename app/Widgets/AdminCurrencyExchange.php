<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 9:14
 */

namespace App\Widgets;


use App\Models\Currency;
use App\Models\Reserve;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;

class
AdminCurrencyExchange extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $cache_id = (iEXSetting('wa_currency_exchange_type') == 0 ? 'currency-exchange-widget-current' : 'currency-exchange-widget-fixed');
        $response = \Cache::remember($cache_id, Carbon::now()->addMinutes(20), function()
        {
            $currencies = Currency::active()->get();

            $in_amount = 0;
            $out_amount = 0;

            if(iEXSetting('wa_currency_exchange_type') == 0)
            {
                foreach ($currencies as $currency) {
                    if(isset($currency->currency_analytics) and isset($currency->code_currency)) {
                        if(!empty($currency->currency_analytics->in_amount)) {
                            $in_amount += convert_to_usd($currency->code_currency->name, $currency->currency_analytics->in_amount);
                        }

                        if(!empty($currency->currency_analytics->out_amount)) {
                            $out_amount += convert_to_usd($currency->code_currency->name, $currency->currency_analytics->out_amount);
                        }
                    }
                }
            } else {
                foreach ($currencies as $currency) {
                    if(isset($currency->currency_analytics) and isset($currency->code_currency)) {
                        $in_amount += $currency->currency_analytics->in_amount_usd;
                        $out_amount += $currency->currency_analytics->out_amount_usd;
                    }
                }
            }


            return [
                'in' => $in_amount,
                'out' => $out_amount
            ];
        });

        return view('widgets.common_currency_exchange', [
            'config' => $this->config,
            'in_amount' => $response['in'],
            'out_amount' => $response['out']
        ]);
    }
}
