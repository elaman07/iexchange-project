<?php

namespace App\Widgets;

use App\Models\ActiveUserModel;
use Arrilot\Widgets\AbstractWidget;

class ActivityClient extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.activity_users', [
            'config' => $this->config,
            'active_count_10_min' => ActiveUserModel::users(20)->count(),
            'active_guest_10_min' => ActiveUserModel::guests(20)->count()

        ]);
    }
}
