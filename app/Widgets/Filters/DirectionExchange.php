<?php

namespace App\Widgets\Filters;

use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class DirectionExchange extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $direction = \App\Models\DirectionExchange::select('id', 'tech_name', 'status')->active()->pluck('tech_name', 'id');
        return view('widgets.filters.direction_exchange', [
            'direction' => $direction,
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
