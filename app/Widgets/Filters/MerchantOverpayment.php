<?php

namespace App\Widgets\Filters;

use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class MerchantOverpayment extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $paymentMerchantStatusses = [
            0   =>  'Точная сумма',
            1   =>  'Переплата',
            2   =>  'Недоплата'
        ];

        return view('widgets.filters.merchant_overpayment', [
            'config' => $this->config,
            'paymentMerchantStatusses' => $paymentMerchantStatusses,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
