<?php

namespace App\Widgets\Filters;

use App\Models\CodeCurrency;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class CodeCurrencyOut extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $code_currencies = CodeCurrency::orderByDesc('id')->get()->pluck('name', 'id');

        return view('widgets.filters.code_currency_out', [
            'config' => $this->config,
            'code_currencies' => $code_currencies,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
