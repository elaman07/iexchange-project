<?php

namespace App\Widgets\Filters;

use App\Models\Currency;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class CurrenciesOut extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $currencies = Currency::select('id', 'tech_name', 'status')->active()->orderByDesc('id')->pluck('tech_name', 'id');

        return view('widgets.filters.currencies_out', [
            'currencies' => $currencies,
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
