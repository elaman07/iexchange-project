<?php

namespace App\Widgets\Filters;

use App\Models\Payment;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class PaymentOut extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $payments = Payment::active()->orderByDesc('id')->get()->pluck('name', 'id');

        return view('widgets.filters.payment_out', [
            'payments' => $payments,
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
