<?php

namespace App\Widgets\Filters;

use App\Models\TaskInfo;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class UserCountry extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $allOrderCountries = TaskInfo::groupBy('country')->pluck('country', 'country');
        return view('widgets.filters.user_country', [
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? [],
            'allOrderCountries' => $allOrderCountries
        ]);
    }
}
