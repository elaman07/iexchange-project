<?php

namespace App\Widgets\Filters;

use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class Status extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $allOrderStatuses = TaskStatus::orderBy('id')->pluck('name', 'id');

        //
        return view('widgets.filters.status', [
            'config' => $this->config,
            'allOrderStatuses'  => $allOrderStatuses,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
