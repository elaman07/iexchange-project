<?php

namespace App\Widgets\Filters;

use App\Models\FilterCurrency;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class FilterCurrencyIn extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        // Фильтры для валюты (Отдаю)
        $filter_currency = FilterCurrency::cursor()->pluck('name', 'id');

        return view('widgets.filters.filter_currency_in', [
            'config' => $this->config,
            'filter_currency' => $filter_currency,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
