<?php

namespace App\Widgets\Filters;

use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class Devices extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $filteredDevices = [
            'desktop' => 'Основная версия',
            'mobile' => 'Мобильная версия',
            'tablet' => 'Планшетная версия'
        ];

        return view('widgets.filters.devices', [
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? [],
            'allDevices' => $filteredDevices
        ]);
    }
}
