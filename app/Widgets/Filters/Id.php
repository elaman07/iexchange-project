<?php

namespace App\Widgets\Filters;

use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class Id extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.filters.id', [
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
