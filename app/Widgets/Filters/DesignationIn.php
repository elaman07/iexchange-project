<?php

namespace App\Widgets\Filters;

use App\Models\Currency;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class DesignationIn extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        // Обозначении
        $designation_code = Currency::orderByDesc('id')->cursor()->map(function($item) {
            return ['value' => $item->designation_xml];
        })->pluck('value', 'value');

        return view('widgets.filters.designation_in', [
            'config' => $this->config,
            'designation_code' => $designation_code,
            'filter'   => $this->config['request'] ?? []
        ]);
    }
}
