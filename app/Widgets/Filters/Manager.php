<?php

namespace App\Widgets\Filters;

use App\Models\Task;
use App\Models\TaskStatus;
use Arrilot\Widgets\AbstractWidget;

class Manager extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $allOrderManagers = Task::where('id_manager','>',0)->groupBy('id_manager')
            ->get()->map(function($item) {
                return ['id' => $item->id_manager, 'value' => $item->manager->name];
            })->pluck('value', 'id');

        return view('widgets.filters.manager', [
            'config' => $this->config,
            'filter'   => $this->config['request'] ?? [],
            'allOrderManagers' => $allOrderManagers
        ]);
    }
}
