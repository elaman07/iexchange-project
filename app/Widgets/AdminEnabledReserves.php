<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 9:14
 */

namespace App\Widgets;


use App\Models\Reserve;
use Arrilot\Widgets\AbstractWidget;

class AdminEnabledReserves extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $reserves = Reserve::whereHas('currency', function($query) {
            $query->where('status', '=', 0);
        })->where('id_main', '=', 0)->orderBy('sorting')->get();

        return view('widgets.enabled_reserves', [
            'config' => $this->config,
            'reserves' => $reserves
        ]);
    }
}
