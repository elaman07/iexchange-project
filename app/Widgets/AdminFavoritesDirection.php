<?php

namespace App\Widgets;

use App\Models\Favorites;
use Arrilot\Widgets\AbstractWidget;

class AdminFavoritesDirection extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $favorites = Favorites::orderBy('id', 'desc')->get();

        return view('widgets.admin_favorites_direction', [
            'config' => $this->config,
            'favorites' => $favorites
        ]);
    }
}
