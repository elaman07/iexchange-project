<?php

namespace App\Widgets;

use App\Models\EventReserve;
use Arrilot\Widgets\AbstractWidget;

class AdminEventReserve extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //Событие по резервам
        $events = EventReserve::with(['currency', 'currency.code_currency', 'currency.payment'])
            ->orderBy('id', 'desc')->limit(20)->cursor();

        return view('widgets.admin_event_reserve', [
            'events' => $events,
            'config' => $this->config,
        ]);
    }
}
