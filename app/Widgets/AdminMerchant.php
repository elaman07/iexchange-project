<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 17:23
 */

namespace App\Widgets;

use App\Models\GatewayMerchant;
use App\Models\Task;
use Arrilot\Widgets\AbstractWidget;

class AdminMerchant extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        $merchants = GatewayMerchant::orderByDesc('id')->cursor();

        return view('widgets.admin_merchant', [
            'config' => $this->config,
            'merchants' => $merchants
        ]);
    }
}
