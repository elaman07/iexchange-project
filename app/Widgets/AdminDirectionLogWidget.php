<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 13:43
 */

namespace App\Widgets;


use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\DirectionExchangeErrorLog;
use Arrilot\Widgets\AbstractWidget;

class AdminDirectionLogWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $logs = DirectionExchangeErrorLog::orderByDesc('id')->limit(10)->cursor();
        //Активные проблемы с направлениями
        $active_errors_count = DirectionExchange::where('status', 1)->where('is_error_rate', '=', 1)->count();

        return view('widgets.direction_logs', [
            'active_errors_count' => $active_errors_count,
            'config' => $this->config,
            'logs' => $logs
        ]);
    }
}
