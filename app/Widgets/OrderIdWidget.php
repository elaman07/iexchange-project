<?php

namespace App\Widgets;

use App\Models\ActiveUserModel;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Cache;

class OrderIdWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.order_id', [
            'config' => $this->config,
            'logs' => collect(Cache::get('order_id_thumb'))->sortByDesc('created_at')->take(20)->toArray()
        ]);
    }
}
