<?php

namespace App\Widgets;

use App\Models\ActiveUserModel;
use App\Models\DirectionExchange;
use App\Models\Task;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AdminHeaderStat extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $start_day = Carbon::today()->startOfDay()->toDateTimeString();
        $end_day = Carbon::today()->endOfDay()->toDateTimeString();
        $direction_count = DirectionExchange::selectRaw('count(*) as total')
            ->selectRaw("count(case when status = 1 then 1 end) as active")
            ->first();

        $order_count = Task::selectRaw('count(*) as total')
            ->selectRaw("count(case when status = 4 and updated_at BETWEEN '{$start_day}' AND '{$end_day}' then 1 when created_at BETWEEN '{$start_day}' AND '{$end_day}' then 1 end) as success")
            ->first();

        $users_count = User::selectRaw('count(*) as total')
            ->selectRaw("count(case when created_at BETWEEN '{$start_day}' AND '{$end_day}' then 1 end) as today")
            ->selectRaw("count(case when email_verified_at IS NOT NULL then 1 end) as verified")
            ->first();

        return view('widgets.admin_header_stat', [
            'active_count_10_min' => ActiveUserModel::users(20)->count(),

            'direction_count' => $direction_count,
            'order_count'   =>  $order_count,
            'users_count'   => $users_count,
            'config' => $this->config,
        ]);
    }
}
