<?php

namespace App\Widgets;


use App\Models\ActiveUserModel;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;

class AdminUserActivity extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.admin_user_activity', [
            'config' => $this->config,
            'user_online'       =>  ActiveUserModel::users(10)->groupBy('ip_address')->get(),
        ]);
    }
}
