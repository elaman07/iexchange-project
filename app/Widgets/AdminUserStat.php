<?php

namespace App\Widgets;

use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class AdminUserStat extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $verified = User::whereNotNull('email_verified_at')->count();
        $banned = User::onlyBanned()->count();
        $roles_count = User::permission('allow_admin')->count();
        $provider_count = User::whereNotNull('provider')->count();

        return view('widgets.admin_user_stat', [
            'config' => $this->config,
            'user_count'        =>  User::count(),
            'verified_count'    =>  $verified,
            'provider_count'    =>  $provider_count,
            'banned_count'      =>  $banned,
            'roles_count'       =>  $roles_count,
        ]);
    }
}
