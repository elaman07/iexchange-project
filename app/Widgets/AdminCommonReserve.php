<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 9:14
 */

namespace App\Widgets;


use App\Models\Reserve;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;

class AdminCommonReserve extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $convert_reserve = 0;
        try {
            $reserves = Reserve::whereHas('currency', function($query) {
                $query->where('status', '=', 0);
            })->where('id_main', '=', 0)->orderBy('sorting')->get();

            $convert_reserve = \Cache::remember('total_reserves', Carbon::now()->addMinutes(20), function() use($reserves) {
                return $reserves->map(function($item) {
                    return (float)convert_to_usd($item->currency->code_currency->name, $item->summa);
                })->sum();
            });
        } catch (\Exception $exception) {

        }

        return view('widgets.common_reserve', [
            'config' => $this->config,
            'amount_reserve' => $convert_reserve
        ]);
    }
}
