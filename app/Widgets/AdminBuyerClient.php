<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 17:23
 */

namespace App\Widgets;

use App\Models\Task;
use Arrilot\Widgets\AbstractWidget;

class AdminBuyerClient extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $buyers = Task::with('user')->where('status', '=',4)
            ->select('id_user', \DB::raw('count(*) as total_orders'))
            ->groupBy('id_user')->orderByDesc('total_orders')->limit(20)->cursor();

        return view('widgets.buyer_client', [
            'config' => $this->config,
            'buyers' => $buyers
        ]);
    }
}
