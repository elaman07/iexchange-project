<?php

namespace App\Widgets;


use App\Models\DirectionExchange;
use App\Models\GroupParserExchange;
use App\Models\ParserExchange;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;
use Spatie\Permission\Models\Role;

class AvgDirectionCourse extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $direction_exchange_result = 0;
        if(Request::has('wid_adc_direction'))
        {
            $from_date = Carbon::parse(Request::get('wid_adc_duration_from'))->toDateTimeString();
            $to_date = Carbon::parse(Request::get('wid_adc_duration_to'))->toDateTimeString();


            $direction_exchange_first = DirectionExchange::find(Request::get('wid_adc_direction'));
            $direction_exchange_result = Task::where('id_direction_exchange', Request::get('wid_adc_direction'))->whereBetween('created_at', [$from_date, $to_date])->avg('course_float');


        }


        $direction_exchange = DirectionExchange::where('status', 1)->get();


        return view('widgets.avg_direction_course', [
            'config' => $this->config,
            'direction_exchange' => $direction_exchange,
            'direction_exchange_result' => $direction_exchange_result
        ]);
    }
}
