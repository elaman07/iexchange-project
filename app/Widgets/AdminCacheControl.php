<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 17:23
 */

namespace App\Widgets;

use App\Models\Task;
use Arrilot\Widgets\AbstractWidget;

class AdminCacheControl extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.cache_control', [
            'config' => $this->config
        ]);
    }
}
