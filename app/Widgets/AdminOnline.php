<?php

namespace App\Widgets;

use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class AdminOnline extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $admin_users = User::where('last_activity_at','>=', Carbon::now()->subMinutes(5))
            ->get()->filter(function($item) {
                $page = explode('/', $item->current_page);
                return Arr::first($page) == config('admin.route_path');
            });

        return view('widgets.admin_online', [
            'config' => $this->config,
            'admin_users' => $admin_users
        ]);
    }
}
