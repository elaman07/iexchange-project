<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 17.08.2020
 * Time: 10:10
 */

namespace App\Widgets;


use App\Models\GroupParserExchange;
use App\Models\ParserExchange;
use App\Models\TaskConvertLog;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Spatie\Permission\Models\Role;

class IEXNewsWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $response = \Cache::remember('iexexchanger-news', Carbon::now()->addHours(5), function() {
            try {
                return Http::get('https://exchanger.iexbase.com/api/v2/news')->json();
            }catch (\Exception $exception) {
                return [];
            }
        });


        return view('widgets.iexexchanger', [
            'config' => $this->config,
            'news' => $response['data'] ?? [],
        ]);
    }
}
