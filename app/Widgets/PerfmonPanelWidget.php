<?php

namespace App\Widgets;

use App\Models\Task;
use App\Models\TaskInfo;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;

class PerfmonPanelWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $memory_limit = ini_get('memory_limit');
        if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches))
        {
            if ($matches[2] == 'M') {
                $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
            } else if ($matches[2] == 'K') {
                $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
            }
        }
        return view('widgets.perfmon_panel', [
            'config' => $this->config,
            'memory_limit' => $memory_limit
        ]);
    }
}
