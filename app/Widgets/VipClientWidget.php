<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.04.2020
 * Time: 17:23
 */

namespace App\Widgets;

use App\Models\Task;
use App\Models\TaskInfo;
use App\Models\User;
use Arrilot\Widgets\AbstractWidget;

class VipClientWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $users = User::where('is_vip_client', '=', 1)->orderBy('id')->cursor();

        return view('widgets.vip_clients', [
            'config' => $this->config,
            'users' => $users
        ]);
    }
}
