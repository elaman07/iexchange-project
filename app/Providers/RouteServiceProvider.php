<?php

namespace App\Providers;

use App\Common\Packages\SecureHeaders\SecureHeadersMiddleware;
use App\Http\Controllers\Callbacks\TelegramWebhookController;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function()
        {
            // Api router
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api.php'));

            // Telegram Bot
            Route::prefix(sprintf('telegram-bot/%s', config('telegram.prefix')))
                ->namespace($this->namespace)
                ->group(function() {
                    Route::post('/client', [TelegramWebhookController::class, 'user_webhook']);
                    Route::post('/admin', [TelegramWebhookController::class, 'admin_webhook']);
                });

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));

            Route::prefix('api/v1')
                ->middleware(['web', SecureHeadersMiddleware::class, 'xss-filter'])
                ->namespace($this->namespace.'\\Api\\V1')
                ->group(base_path('routes/frontend.php'));
        });

    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
