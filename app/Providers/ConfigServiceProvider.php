<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.04.2019
 * Time: 8:27
 */

namespace App\Providers;

use App\Models\FileStorageModel;
use App\Models\LanguageContent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
    }

    public function register()
    {
        $this->app->singleton('language_content', function ($app)
        {
            if(Schema::hasTable('language_contents'))
            {
                $response = LanguageContent::find(1);
                if(empty($response)) {
                    $response = LanguageContent::create();
                }
                return $response;
            } else {
                return [];
            }
        });
    }
}
