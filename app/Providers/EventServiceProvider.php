<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
            'SocialiteProviders\\Yandex\\YandexExtendSocialite@handle',
            'SocialiteProviders\\Google\\GoogleExtendSocialite@handle',
            'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
            'SocialiteProviders\\Tumblr\\TumblrExtendSocialite@handle',
            'SocialiteProviders\\Coinbase\\CoinbaseExtendSocialite@handle',
            'SocialiteProviders\\GitHub\\GitHubExtendSocialite@handle',
            'SocialiteProviders\\Yahoo\\YahooExtendSocialite@handle',
            'SocialiteProviders\\Twitter\\TwitterExtendSocialite@handle',
            'SocialiteProviders\\Medium\\MediumExtendSocialite@handle',
            'SocialiteProviders\\PayPal\\PayPalExtendSocialite@handle',
            'SocialiteProviders\\Pinterest\\PinterestExtendSocialite@handle',
            'SocialiteProviders\\Discord\\DiscordExtendSocialite@handle',
            'SocialiteProviders\\Dribbble\\DribbbleExtendSocialite@handle',
            'SocialiteProviders\\LinkedIn\\LinkedInExtendSocialite@handle',
            'SocialiteProviders\\Stripe\\StripeExtendSocialite@handle',
            'SocialiteProviders\\StackExchange\\StackExchangeExtendSocialite@handle',
            'SocialiteProviders\\Slack\\SlackExtendSocialite@handle',
            'SocialiteProviders\\Odnoklassniki\\OdnoklassnikiExtendSocialite@handle',
            'SocialiteProviders\\GitLab\\GitLabExtendSocialite@handle',
            'SocialiteProviders\\Dropbox\\DropboxExtendSocialite@handle',
           // 'SocialiteProviders\\Bitbucket\\BitbucketExtendSocialite@handle',
            'SocialiteProviders\\Twitch\\TwitchExtendSocialite@handle',
            'SocialiteProviders\\WordPress\\WordPressExtendSocialite@handle',
            'SocialiteProviders\\Zendesk\\ZendeskExtendSocialite@handle',
        ],

        'Illuminate\Mail\Events\MessageSending' => [
            'App\Listeners\AuditLog\EmailLogger',
        ],

        // Лог успешной авторизации в админпанели
        'PragmaRX\Google2FALaravel\Events\LoginSucceeded' => [
            'App\Listeners\AdminLogs\LogLoginSucceeded',
        ],

        // Лог неудачной авторизации в админпанели
        'PragmaRX\Google2FALaravel\Events\LoginFailed' => [
            'App\Listeners\AdminLogs\LogLoginFailed',
        ],

        'Illuminate\Auth\Events\Lockout' => [
            'App\Listeners\LogLockout',
        ],

        // Неудачная авторизация
        'Illuminate\Auth\Events\Failed' => [
            'App\Listeners\LogFailedAuthenticationAttempt',
        ],


        //Последний вход
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\LogSuccessLogin',
        ],

        // Восстановление пароля
        'Illuminate\Auth\Events\PasswordReset' => [
            'App\Listeners\LogPasswordReset',
        ],

        //Последний выход
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\LogSuccessLogout',
        ],

        //Последняя активность
        'Illuminate\Auth\Events\Authenticated' => [
            'App\Listeners\LogUserAuthenticated',
        ],

        'App\Events\AutoPaymentSuccessful' => [
            'App\Listeners\AutoPaymentSuccessfulListener',
        ],

        'App\Events\CoursesFileEvent' => [
            'App\Listeners\CoursesFileListener',
        ],

        Registered::class => [
            SendEmailVerificationNotification::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
