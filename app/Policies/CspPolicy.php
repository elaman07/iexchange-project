<?php
namespace App\Policies;

use Illuminate\Support\Str;
use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic as BasicPolicy;

class CspPolicy extends BasicPolicy
{
    /**
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     * @throws \Spatie\Csp\Exceptions\InvalidValueSet
     */
    public function configure()
    {
        $this->addDirective(Directive::BASE, [
                'self',
            ]
        );
        $this->addDirective(Directive::CONNECT, [
            'self',
            'https://*.yandex.ru',
            'http://*.yandex.ru',
            'https://*.google-analytics.com',
            'wss://*.pusher.com',
            'wss://*.sochain.com',
            'wss://*.chain.so',
            'https://*.pusher.com',
            'http://*.pusher.com',
            'https://*.sochain.com',
            'http://*.sochain.com',
            'https://*.chain.so',
            'http://*.chain.so',
            '*.lottiefiles.com'
        ]);

        $this->addDirective(Directive::FONT, [
            'self',
            'data:',
            'fonts.gstatic.com',
            '*.bootstrapcdn.com'
        ]);

        $this->addDirective(Directive::STYLE, [
            'self',
            'fonts.googleapis.com',
            'fonts.gstatic.com',
            '*.bootstrapcdn.com',
            'unsafe-inline'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            'self',
            'unsafe-inline',
            'unsafe-eval',
            '*.googletagmanager.com',
            '*.google-analytics.com',
            '*.google.com',
            '*.gstatic.com',
            '*.yandex.ru',
            '*.pusher.com',
            'unpkg.com',

        ]);

        $this->addDirective(Directive::FORM_ACTION, [
            'self',
            '*.advcash.com',
            'payeer.com',
            'perfectmoney.is',
            'qiwi.com',
            '*.yandex.ru'
        ]);

        $this->addDirective(Directive::IMG, [
            '*',
            '*.yandex.ru',
            'unsafe-inline',
            'data:',
        ]);
        $this->addDirective(Directive::OBJECT, 'none');
        //$this->addNonceForDirective(Directive::SCRIPT);

        $method = 'chat'.Str::studly(iEXSetting('chat_type'));
        if(method_exists($this, $method))
            $this->{$method}();

        if(iEXSetting('is_enabled_module_socket')) {
            $this->socketPusher();
        }

    }

    /**
     * Настройка сокета (Pusher)
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
    */
    private function socketPusher()
    {
        $this->addDirective(Directive::CONNECT, [
            'wss://*.pusher.com',
            'https://*.pusher.com',
            'http://*.pusher.com'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            '*.pusher.com'
        ]);
    }

    /**
     * Настройка Intercom
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     * @throws \Spatie\Csp\Exceptions\InvalidValueSet
     */
    protected function chatIntercom()
    {
        $this->addDirective(Directive::CONNECT, [
            'self',
            'https://*.intercom.io',
            'wss://*.intercom.io'
        ]);

        $this->addDirective(Directive::FONT, [
            '*.intercomcdn.com'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            '*.intercom.io',
            '*.intercomcdn.com'
        ]);
    }

    /**
     * Настройка JivoSite
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     */
    protected function chatJivosite()
    {
        $this->addDirective(Directive::CONNECT, [
            'wss://*.jivosite.com',
            'https://*.jivosite.com',
            'http://*.jivosite.com',
            'wss://*.sochain.com',
            'wss://*.chain.so',
            'https://*.jivo.ru',
            'wss://*.jivo.ru',
            'http://*.jivo.ru',
        ]);

        $this->addDirective(Directive::STYLE, [
            '*.jivosite.com',
            '*.jivo.ru'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            '*.jivosite.com',
            '*.jivo.ru'
        ]);
    }

    /**
     * Настройки для Replain чата
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     */
    protected function chatReplain()
    {
        $this->addDirective(Directive::CONNECT, [
            'wss://*.replain.cc',
            'https://*.replain.cc',
            'http://*.replain.cc'
        ]);

        $this->addDirective(Directive::STYLE, [
            'widget.replain.cc'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            'https://*.replain.cc'
        ]);
    }

    /**
     * Настройки для Replain чата
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     * @throws \Spatie\Csp\Exceptions\InvalidValueSet
     */
    protected function chatTalkme()
    {
        $this->addDirective(Directive::CONNECT, [
            'wss://*.me-talk.ru',
            'https://*.me-talk.ru',
            'http://*.me-talk.ru',
            'wss://*.apibcknd.com',
            'https://*.apibcknd.com',
            'http://*.apibcknd.com',
        ]);

        $this->addDirective(Directive::STYLE, [
            '*.me-talk.ru',
            '*.me-talk.ru',
            '*.site-chat.me'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            'https://*.talk-me.ru',
            'https://*.me-talk.ru',
            'https://*.site-chat.me'
        ]);
    }

    /**
     * Настройки для Tawk чата
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     * @throws \Spatie\Csp\Exceptions\InvalidValueSet
     */
    protected function chatTawk()
    {
        $this->addDirective(Directive::FONT, ['*.tawk.to', 'fonts.gstatic.com', '*.bootstrapcdn.com']);
        $this->addDirective(Directive::FORM_ACTION, ['*.tawk.to']);
        $this->addDirective(Directive::CONNECT, [
            'self',
            'https://*.tawk.to',
            'http://*.tawk.to',
            'wss://*.tawk.to'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            '*.tawk.to',
            '*.jsdelivr.net'
        ]);

        $this->addDirective(Directive::STYLE, [
            'cdn.jsdelivr.net',
            '*.tawk.to'
        ]);
    }

    /**
     * Настройки для Tawk чата
     *
     * @throws \Spatie\Csp\Exceptions\InvalidDirective
     * @throws \Spatie\Csp\Exceptions\InvalidValueSet
     */
    protected function chatSmartsupp()
    {
       $this->addDirective(Directive::FONT, ['*.cdn77.org']);
        //$this->addDirective(Directive::FORM_ACTION, ['*.tawk.to']);
        $this->addDirective(Directive::CONNECT, [
            'self',
            'https://*.smartsupp.com',
            'wss://*.smartsupp.com'
        ]);

        $this->addDirective(Directive::SCRIPT, [
            '*.smartsuppchat.com',
            '*.cdn77.org'
        ]);

        $this->addDirective(Directive::STYLE, [
            '*.cdn77.org'
        ]);
    }
}
