<?php

namespace App\Mail;

use App\Common\Packages\Editor\EditorFacade;
use App\Models\EmailTemplate;
use App\Models\EventReserve;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyChangeReserve extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
    */
    protected $rules = ['[manager_name]', '[created_at]', '[type]', '[event]', '[comment]','[currency]'];

    /**
     * События резервов
     *
     * @var \App\Models\EventReserve
    */
    protected $eventReserve = [];

    /**
     * Create a new message instance.
     *
     * @param EventReserve $eventReserve
     */
    public function __construct(EventReserve $eventReserve)
    {
        $this->eventReserve = $eventReserve;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = $this->buildTemplate();

        if(empty($build)) {
            $this->subject(
                sprintf('Корректировка резерва для валюты %s %s', $this->eventReserve->currency->payment->name, $this->eventReserve->currency->code_currency->name)
            );
        } else {
            $this->subject($build['title']);
        }


        return $this->markdown('emails.notification.change_reserve', [
            'reserve' => $this->eventReserve,
            'data' => $build
        ]);
    }

    /**
     * Получение шаблона
    */
    protected function buildTemplate()
    {
        $response = EmailTemplate::whereHas('template_type_event', function($item) {
            $item->where('event_name', '=', 'CHANGE_RESERVE_NOTIFY');
        })->first();

        $content = null;
        if(!empty($response))
        {
            // Стандартный шаблон
            if($response->template == 0)
                return [];

            $replace = [
                $this->eventReserve->user->name,
                $this->eventReserve->created_at,
                $this->eventReserve->type,
                $this->eventReserve->text,
                $this->eventReserve->comment,
                sprintf('%s %s', $this->eventReserve->currency->payment->name, $this->eventReserve->currency->code_currency->name)
            ];

            $content = EditorFacade::driver('mail_code');

            return [
                'title' => $content->build($response->subject, $this->rules, $replace),
                'content' => $content->build($response->text_template, $this->rules, $replace),
                'email_to' => $response->email_to,
            ];
        }

        return [];
    }
}
