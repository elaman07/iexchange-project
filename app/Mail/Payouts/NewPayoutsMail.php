<?php
namespace App\Mail\Payouts;

use App\Common\Packages\Editor\EditorFacade;
use App\Models\EmailTemplate;
use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPayoutsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[id]', '[currency]', '[user_id]', '[user_name]', '[user_email]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
    */
    protected $templateReplace = [];

    /**
     * Модель WithdrawalRequest
     *
     * @var \App\Models\WithdrawalRequest
    */
    protected $withdrawalRequest;

    /**
     * Create a new message instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
        $this->templateReplace = [
            $this->withdrawalRequest->id,
            sprintf('%s %s', $this->withdrawalRequest->currency->payment->name, $this->withdrawalRequest->currency->code_currency->name),
            $this->withdrawalRequest->user->id,
            $this->withdrawalRequest->user->name,
            $this->withdrawalRequest->user->email,
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('NEW_BONUS_WITHDRAWAL', $this->rules, $this->templateReplace);

        if(empty($build)) {
            $this->subject('Заявка №'.$this->withdrawalRequest->id.' на выплату бонусов');
        } else {
            $this->subject($build['title']);
        }

        return $this->markdown('emails.custom_template', [
            'subject' => $this->subject,
            'default_template' => 'payouts.new_payouts',
            'item'  =>  $this->withdrawalRequest,
            'data'  =>  $build
        ]);
    }
}
