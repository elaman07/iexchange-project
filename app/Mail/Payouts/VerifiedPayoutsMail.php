<?php
namespace App\Mail\Payouts;


use App\Common\Packages\Editor\EditorFacade;
use App\Models\EmailTemplate;
use App\Models\Task;
use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifiedPayoutsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[balance]', '[currency]', '[code_currency]', '[user_id]', '[user_email]', '[user_name]', '[button]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];

    /**
     * Модель WithdrawalRequest
     *
     * @var \App\Models\WithdrawalRequest
    */
    protected $withdrawalRequest;

    /**
     * Create a new message instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
        $this->templateReplace = [
            iex_number_format($this->withdrawalRequest->balance_referral + $this->withdrawalRequest->balance_reward, 2),
            sprintf('%s %s', $this->withdrawalRequest->currency->payment->name, $this->withdrawalRequest->currency->code_currency->name),
            $this->withdrawalRequest->currency->code_currency->name,
            $this->withdrawalRequest->user->id,
            $this->withdrawalRequest->user->email,
            $this->withdrawalRequest->user->name,
            url('/confirm/'.$this->withdrawalRequest->tx_id.'/payouts')
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('CONFIRM_WITHDRAWAL_BONUSES', $this->rules, $this->templateReplace);

        if(empty($build)) {
            $this->subject('Подтвердите вывод средств');
        } else {
            $this->subject($build['title']);
        }

        $button  = url('/confirm/'.$this->withdrawalRequest->tx_id.'/payouts');

        return $this->markdown('emails.custom_template', [
            'subject' => $this->subject,
            'default_template' => 'payouts.verified_payouts',
            'item'  =>  $this->withdrawalRequest,
            'confirm' => $button,
            'data'   => $build
        ]);
    }
}
