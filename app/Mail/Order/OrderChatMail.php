<?php
namespace App\Mail\Order;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/***
 * Дополнителнье уведомления для клиента
 *
 * @class OrderChatMail
 */
class OrderChatMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[order_id]', '[message]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];

    /***
     * Детали транзакции
     *
     * @return Task
     */
    protected $order;

    /**
     * Дополнительные опции
     *
     * @var array
     */
    protected $options = [];

    /**
     * Create a new message instance.
     *
     * @param Task $order
     * @param array $options
     */
    public function __construct(Task $order, $options = [])
    {
        $this->order = $order;
        $this->options = $options;
        $this->templateReplace = [
            $this->order->id,
            (isset($options['message_success']) ? $options['message_success'] : '')
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('ORDER_CHAT_NOTIFY', $this->rules, $this->templateReplace);

        if(empty($build)) {
            $this->subject('Уведомление по заявке №'.iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id);
        } else {
            $this->subject($build['title']);
        }

        return $this->markdown('emails.custom_template', [
            'subject' => $this->subject,
            'default_template' => 'order.order_chat',
            'order'  =>  $this->order,
            'options'  =>  $this->options
        ]);
    }
}
