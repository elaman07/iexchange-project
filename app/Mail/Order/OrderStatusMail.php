<?php
namespace App\Mail\Order;


use App\Models\EVoucherCode;
use App\Models\LinksReview;
use App\Models\SocialReview;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderStatusMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
    */
    protected $order;

    /**
     * Дополнительные параметры
     *
     * @var array
    */
    protected $options = [];

    /**
     * Create a new message instance.
     *
     * @param Task $order
     * @param array $options
     */
    public function __construct(Task $order, $options = [])
    {
        $this->order = $order;
        $this->options = $order;
        $this->locale = $this->order->task_info->language ?? app()->getLocale();

        // Название темы
        $title = null;
        if($order->status == 4) {
            $title = __('email.order_success', [], $this->locale);
        } elseif($order->status == 8) {
            if(isset($order->pending_order_status) and $order->pending_order_status->id == 5) {
                $title = __('email.order_ares', [], $this->locale);
            } else {
                $title = __('email.order_postponed', [], $this->locale);
            }
        } elseif($order->status == 5) {
            $title = __('email.order_delete', [], $this->locale);
        }

        // Записываем в название заголовок темы
        $this->subject = iEXContentLanguage('sitename').' - '.$title;
        $this->options['title'] = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $links = LinksReview::orderBy('sorting')->get();
        $socials = SocialReview::orderBy('sorting')->get();

        // Для e-Voucher кодов
        $voucher = [];
        if($this->order->status == 4) {
            $voucher = EVoucherCode::where('id_task', $this->order->id)->first();
        }

        $content_link = '<ul>';
        foreach ($links as $item) {
            $content_link .= '<li><a target="_blank" href="'.$item->url.'">'.$item->name.'</a></li><br />';
        }
        $content_link .='</ul>';

        $content_social = '<ul>';
        foreach ($socials as $item) {
            $content_social .= '<li><a target="_blank" href="'.$item->link.'">'.$item->name.'</a></li><br />';
        }
        $content_social .='</ul>';

        return $this->markdown('emails.order.order_status', [
            'options' => $this->options,
            'content_link' => $content_link,
            'content_social'    =>  $content_social,
            'order' => $this->order,
            'links' => $links,
            'socials' => $socials,
            'voucher'   =>  $voucher
        ]);
    }
}
