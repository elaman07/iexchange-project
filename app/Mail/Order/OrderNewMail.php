<?php
namespace App\Mail\Order;

use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use s9e\RegexpBuilder\Input\Utf8;

class OrderNewMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = [
        '[rate]', '[city]', '[country]', '[code_in]', '[code_out]', '[public_id]','[profit_percent]', '[id]',
        '[currency_in]', '[currency_out]', '[status]', '[direction]', '[from_shot]',
        '[to_shot]', '[created_at]', '[amount_in]', '[amount_out]', '[user_id]',
        '[user_email]', '[user_name]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new message instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
        $this->templateReplace = [
            $this->order->course_display,
            $this->order->task_info->city_name,
            $this->order->task_info->country_name,
            $this->order->direction_exchange->currency1->code_currency->name,
            $this->order->direction_exchange->currency2->code_currency->name,
            $this->order->public_id,
            $this->order->direction_exchange->profit,
            iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id,
            currency_in($this->order, true),
            currency_out($this->order, true),
            $this->order->task_status->name,
            direction_name($this->order, true),
            $this->order->from_shot,
            $this->order->to_shot,
            $this->order->created_at,
            $this->order->give_price,
            $this->order->receiving_price,
            $this->order->user->id,
            $this->order->user->email,
            $this->order->user->name
        ];

        $this->locale = $this->order->task_info->language ?? app()->getLocale();
    }

    public function content(): Content
    {
        $build = iex_mail_custom_template('ORDER_STATUS_PENDING_PROCESS', $this->rules, $this->templateReplace);

        if(empty($build)) {
            $this->subject(iEXContentLanguage('sitename').' - '.__('email.application_accepted.subject'));
        } else {
            $this->subject($build['title']);
        }

        $transaction = TransactionFacade::call($this->order->id);

        // Инструкция к оплате
        $bbcodes_instructions = [
            '[direction]', '[created_at]', '[rate]', '[city]','[country]','[in_amount]','[out_amount]','[in_code]',
            '[out_code]', '[in_currency]', '[out_currency]', '[public_id]', '[order_id]', '[profit_percent]',
            '[account]', '[to_account]'
        ];

        $bbcodes_instructions_replace = [
            direction_name($this->order, true),
            $this->order->created_at->translatedFormat('d M Y H:i'),
            $this->order->course_display,
            $this->order->task_info->city_name,
            $this->order->task_info->country_name,
            $this->order->give_price,
            $this->order->receiving_price,
            $this->order->direction_exchange->currency1->code_currency->name,
            $this->order->direction_exchange->currency2->code_currency->name,
            currency_in($this->order, true),
            currency_out($this->order, true),
            $this->order->public_id,
            $this->order->id,
            $this->order->direction_exchange->profit,
            $this->order->transfer_to_account,
            $this->order->to_shot
        ];

        // Инструкция к валюте Отдаю
        $instruction_currency_in = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $this->order->direction_exchange->currency1->instruction_exchange);
        $instruction_direction = null;
        if($this->order->direction_exchange->is_email_instructions == 1) {
            $instruction_direction = str_replace($bbcodes_instructions, $bbcodes_instructions_replace, $this->order->direction_exchange->instructions);
        }

        return new Content(
            markdown: 'emails.custom_template',
            with: [
                'subject' => $this->subject,
                'default_template' => 'order.order_new',
                'order' =>  $this->order,
                'transaction' => $transaction,
                'receive_wallet' => $transaction->getAddresses(),
                'tasks_fields'  => $transaction->getTasksFields(),
                'data' => $build,
                'instruction_currency_in' => $instruction_currency_in,
                'instruction_direction' => $instruction_direction
            ]
        );
    }
}
