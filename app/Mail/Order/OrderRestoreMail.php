<?php

namespace App\Mail\Order;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderRestoreMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[order_id]', '[status]', '[user_name]', '[user_email]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new message instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
        $this->templateReplace = [
            iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id,
            $this->order->task_status->name,
            $this->order->user->name,
            $this->order->user->email
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('ORDER_RESTORE', $this->rules, $this->templateReplace);

        if(empty($build)) {
            $this->subject(iEXContentLanguage('sitename').' - Восстановление заявки');
        } else {
            $this->subject($build['title']);
        }


        return $this->markdown('emails.custom_template', [
            'subject' => $this->subject,
            'default_template' => 'order.order_restore',
            'order' => $this->order,
            'data'   => $build
        ]);
    }
}
