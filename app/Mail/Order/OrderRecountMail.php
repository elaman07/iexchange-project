<?php
namespace App\Mail\Order;

use App\Models\HistoryRecalculation;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderRecountMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[order_id]', '[new_course]', '[amount_in]', '[amount_out]', '[code_currency_in]', '[code_currency_out]', '[currency_in]', '[currency_out]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * История пересчетов
     *
     * @var HistoryRecalculation
    */
    protected $history_recalculation;

    /**
     * Create a new message instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
        $this->history_recalculation = HistoryRecalculation::where('id_task', '=', $this->order->id)
            ->orderByDesc('id')->first();

        $this->templateReplace = [
            iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id,
            (isset($this->history_recalculation) ? $this->history_recalculation->course : ''),
            $this->order->give_price,
            $this->order->receiving_price,
            $this->order->direction_exchange->currency1->code_currency->name,
            $this->order->direction_exchange->currency2->code_currency->name,
            currency_in($this->order, true),
            currency_out($this->order, true)
        ];
        $this->locale = $this->order->task_info->language ?? app()->getLocale();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('ORDER_RECALCULATION', $this->rules, $this->templateReplace);
        if(empty($build)) {
            $this->subject(
                sprintf('%s - %s', iEXContentLanguage('sitename'), __('email.recount_order', ['id' => iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id]))
            );
        } else {
            $this->subject($build['title']);
        }

        return $this->markdown('emails.custom_template', [
            'subject' => $this->subject,
            'default_template' => 'order.order_recount',
            'order' =>  $this->order,
            'recount' => $this->history_recalculation,
            'data'   => $build
        ]);
    }
}
