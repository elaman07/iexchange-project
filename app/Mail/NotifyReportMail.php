<?php

namespace App\Mail;

use App\Models\ArchiveReport;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;

class NotifyReportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * ID Отчета
    */
    protected $id = 0;

    /**
     * Create a new message instance.
     *
     * @param int $id
     */
    public function __construct($id = 0)
    {
        $this->id = $id;
        $this->subject(config('app.name').' - отчет за '.Carbon::now()->format('d.m.Y'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $item = ArchiveReport::find($this->id);
        $filename = storage_path('app/archive/'.$item->name);

        return $this->markdown('emails.notification.report')->attach($filename, [
            'as' => $item->name,
            'mime' => 'application/pdf',
        ]);
    }
}
