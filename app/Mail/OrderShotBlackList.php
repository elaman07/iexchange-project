<?php

namespace App\Mail;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShotBlackList extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[order_id]', '[message]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];

    /***
     * Детали транзакции
     *
     * @return Task
     */
    protected $order;

    /**
     * Текст сообщения
     *
     * @var string
     */
    protected $text = null;

    /**
     * Create a new message instance.
     *
     * @param Task $task
     * @param string $text
     */
    public function __construct(Task $task, string $text)
    {
        $this->order = $task;
        $this->text = $text;
        $this->templateReplace = [
            iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id,
            $text
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('ORDER_SHOT_BLACK_LIST_NOTIFY', $this->rules, $this->templateReplace);

        if(empty($build)) {
            $this->subject('Ваши данные занесены в черный список');
        } else {
            $this->subject($build['title']);
        }

        return $this->markdown('emails.custom_template', [
            'subject' => $this->subject,
            'default_template' => 'order.black_list',
            'order'  =>  $this->order,
            'text'  =>  $this->text
        ]);
    }
}
