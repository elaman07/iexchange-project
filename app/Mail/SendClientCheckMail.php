<?php

namespace App\Mail;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/***
 * Уведомление для клиента об успешном заверщении заявки
 *
 * @class SendClientCheckMail
 */
class SendClientCheckMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Правила для шаблона
     *
     * @var array
     */
    protected $rules = ['[order_id]', '[message]'];

    /**
     * Параметры для замены шаблонных тегов
     *
     * @var array
     */
    protected $templateReplace = [];


    /***
     * Детали транзакции
     *
     * @return Task
     */
    protected $order;

    /**
     * Дополнительные опции
     *
     * @var array
    */
    protected $options = [];

    /**
     * Create a new message instance.
     *
     * @param Task $order
     * @param array $options
     */
    public function __construct(Task $order, $options = [])
    {
        $this->order = $order;
        $this->options = $options;
        $this->templateReplace = [
            $this->order->id,
            isset($options['message_success']) ? $options['message_success'] : ''
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $build = iex_mail_custom_template('ORDER_CHECK_CREATED', $this->rules, $this->templateReplace);

        $title = null;
        if(empty($build)) {
            $this->subject('По Вашей заявке создан номер чека!');
            $title = 'Ваш номер чека!';
        } else {
            $this->subject($build['title']);
            $title = $build['title'];
        }

        return $this->markdown('emails.custom_template', [
            'subject' => $title,
            'default_template' => 'send_client_check',
            'order'  =>  $this->order,
            'options'  =>  $this->options,
            'data'   => $build
        ]);
    }
}
