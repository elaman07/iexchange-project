<?php

namespace App\Mail;

use App\Models\VerificationCard;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationFailMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Информация по заявки на верификацию
     *
     * @var VerificationCard
    */
    protected $verification;

    /**
     * Create a new message instance.
     *
     * @param VerificationCard $verificationCard
     */
    public function __construct(VerificationCard $verificationCard)
    {
        $this->verification = $verificationCard;
        $this->subject(iEXContentLanguage('sitename').' - Отказ в верификации');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.verification_fail', [
            'item' => $this->verification
        ]);
    }
}
