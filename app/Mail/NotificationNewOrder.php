<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/***
 * Уведомление для оператора (Создание новой заявки)
 *
 * @class NotificationClient
 */
class NotificationNewOrder extends Mailable
{
    use Queueable, SerializesModels;

    /***
     * Массив где будет собрана вся необходимая информация
     *
     * @return array
     */
    protected $order = [];

    /**
     * Create a new message instance.
     *
     * @param array $order
     */
    public function __construct($order = [])
    {
        $this->order = $order;
        $this->subject('Поступила новая заявка #'.$this->order['id']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.notification_new_order',[
            'item'  =>  $this->order
        ]);
    }
}
