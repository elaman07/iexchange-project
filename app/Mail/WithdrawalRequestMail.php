<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 20.12.2017
 * Time: 19:29
 */

namespace App\Mail;


use App\Models\WithdrawalRequest;
use App\Models\WithdrawalRequestLog;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WithdrawalRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * ID заявки
    */
    protected $id = 0;

    /**
     * Тип выплаты
     *
     * @var boolean
    */
    protected $part = false;

    /**
     * Create a new message instance.
     *
     * @param $id
     * @param bool $part
     */
    public function __construct($id, bool $part = false)
    {
        $this->id = $id;
        $this->part = $part;

        if(!$part) {
            $this->subject(config('app.name').' - Заявка на выплату выполнено');
        } else {
            $this->subject(config('app.name').' - Заявка на выплату выполнено частична');
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $complete = WithdrawalRequest::find($this->id);
        $log = WithdrawalRequestLog::where('id_withdrawal_request', $this->id)->orderBy('id', 'desc')->first();

        return $this->markdown('emails.withdrawal_request',[
            'item'  =>  $complete,
            'log'   =>  $log,
            'part'  =>  $this->part
        ]);
    }
}
