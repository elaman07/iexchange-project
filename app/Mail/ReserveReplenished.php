<?php

namespace App\Mail;

use App\Models\Currency;
use App\Models\DirectionExchange;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReserveReplenished extends Mailable
{
    use Queueable, SerializesModels;

    protected $array = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($array = [])
    {
        $this->array = $array;

        $this->subject(config('app.name').' - Заявка на пополнение резерва');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $item = Currency::find($this->array['id_currency']);

        if($item->visible_code_currency == 0) {
            $format_name = sprintf("%s %s", $item->payment->name, $item->code_currency->name);
        }else {
            $format_name = sprintf("%s", $item->payment->name);
        }

        return $this->markdown('emails.reserve_replenished',[
            'data'  =>  $this->array,
            'name'  =>  $format_name
        ]);
    }
}
