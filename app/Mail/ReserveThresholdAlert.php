<?php
namespace App\Mail;

use App\Models\ReserveAlert;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReserveThresholdAlert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Детали оповещений для резерва
     *
     * @var ReserveAlert
     */
    protected $alert;

    /**
     * Create a new message instance.
     *
     * @param ReserveAlert $alert
     */
    public function __construct(ReserveAlert $alert)
    {
        $currency = sprintf('%s %s', $alert->reserves->currency->payment->name, $alert->reserves->currency->code_currency->name);
        $this->subject = 'Оповещение об окончании резерва для '.$currency;
        $this->alert = $alert;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.notification.reserve_alert', [
            'alert'  =>  $this->alert
        ]);
    }
}
