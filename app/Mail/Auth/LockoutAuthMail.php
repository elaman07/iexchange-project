<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.04.2019
 * Time: 7:44
 */

namespace App\Mail\Auth;


use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LockoutAuthMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Информация о пользователе
     *
     * @var \App\Models\User
    */
    protected $user;

    /**
     * Дополнительная информация
     *
     * @var array
     */
    protected $details;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param array $details
     */
    public function __construct(User $user, array $details)
    {
        $this->user = $user;
        $this->details = $details;

        // Получение версии языка
        $this->locale = !is_null($this->user->language) ? $this->user->language : app()->getLocale();
        $this->subject(
            sprintf('%s - %s', config('app.name'), __('email.failed_step_auth'))
        );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.auth.lockout', [
            'user'  =>  $this->user,
            'details' => $this->details
        ]);
    }
}
