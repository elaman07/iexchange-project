<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.10.2019
 * Time: 20:59
 */

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\SmscRu\SmscRuChannel;
use NotificationChannels\SmscRu\SmscRuMessage;
use NotificationChannels\SmsRu\SmsRuChannel;
use NotificationChannels\SmsRu\SmsRuMessage;

class SMSAdminCodeNotification extends Notification
{
    use Queueable;

    /**
     * Код
     *
     * @var string
     */
    protected $code;

    /**
     * Create a new notification instance.
     *
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if(config('crypto.sms_driver') == 'smscru')
            return [SmscRuChannel::class];
        elseif(config('crypto.sms_driver') == 'smsru')
            return [SmsRuChannel::class];
        return ['nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        $prefix = (iEXSetting('sms_message_prefix_website') ? iEXContentLanguage('sitename').': ' : null);
        $message = sprintf('AuthCode: %s', $this->code);
        return (new NexmoMessage)
            ->content($prefix.$message)
            ->unicode();
    }

    /**
     * Отправка через SMSCRu драйвер
     *
     * @param $notifiable
     * @return SmscRuMessage
     */
    public function toSmscRu($notifiable)
    {
        $prefix = (iEXSetting('sms_message_prefix_website') ? iEXContentLanguage('sitename').': ' : null);
        $message = sprintf('AuthCode: %s', $this->code);
        return SmscRuMessage::create($prefix.$message);
    }

    /**
     * Отправка через SMSRu драйвер
     *
     * @param $notifiable
     * @return SmsRuMessage
     */
    public function toSmsRu($notifiable)
    {
        $prefix = (iEXSetting('sms_message_prefix_website') ? iEXContentLanguage('sitename').': ' : null);
        $message = sprintf('AuthCode: %s', $this->code);

        return (new SmsRuMessage())->content($prefix.$message)
            ->translit(false);
    }
}
