<?php

namespace App\Notifications;

use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\SmscRu\SmscRuChannel;
use NotificationChannels\SmscRu\SmscRuMessage;
use NotificationChannels\SmsRu\SmsRuChannel;
use NotificationChannels\SmsRu\SmsRuMessage;

class SMSPayoutNotification extends Notification
{
    use Queueable;

    /**
     * Детали бонусных вознаграждений
     *
     * @var \App\Models\WithdrawalRequest
     */
    protected $withdrawalRequest;

    /**
     * Create a new notification instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if(config('crypto.sms_driver') == 'smscru')
            return [SmscRuChannel::class];
        elseif(config('crypto.sms_driver') == 'smsru')
            return [SmsRuChannel::class];
        return ['nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        $prefix = (iEXSetting('sms_message_prefix_website') ? iEXContentLanguage('sitename').': ' : null);
        $message = str_replace([':id'], [$this->withdrawalRequest->id], iEXSetting('sms_message_success_payouts'));
        return (new NexmoMessage)
            ->content($prefix.$message)
            ->unicode();
    }

    /**
     * Отправка через SMSCRu драйвер
     *
     * @param $notifiable
     * @return SmscRuMessage
     */
    public function toSmscRu($notifiable)
    {
        $prefix = (iEXSetting('sms_message_prefix_website') ? iEXContentLanguage('sitename').': ' : null);
        $message = str_replace([':id'], [$this->withdrawalRequest->id], iEXSetting('sms_message_success_payouts'));
        return SmscRuMessage::create($prefix.$message);
    }

    /**
     * Отправка через SMSRu драйвер
     *
     * @param $notifiable
     * @return SmsRuMessage
     */
    public function toSmsRu($notifiable)
    {
        $prefix = (iEXSetting('sms_message_prefix_website') ? iEXContentLanguage('sitename').': ' : null);
        $message = str_replace([':id'], [$this->withdrawalRequest->id], iEXSetting('sms_message_success_payouts'));

        return (new SmsRuMessage())->content($prefix.$message)
            ->translit(false);
    }
}
