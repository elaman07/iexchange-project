<?php
namespace App\Notifications;

use App\Models\UserAuth;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class NewDeviceNotification extends Notification
{
    use Queueable;

    /**
     * The authentication log.
     *
     * @var \App\Models\UserAuth
     */
    public $authenticationLog;

    /**
     * Create a new notification instance.
     *
     *
     * @param UserAuth $authenticationLog
     */
    public function __construct(UserAuth $authenticationLog)
    {
        $this->authenticationLog = $authenticationLog;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->notifyAuthenticationLogVia();
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('email.sign_new_device'))
            ->markdown('emails.auth.new_device', [
                'account' => $notifiable,
                'log' => $this->authenticationLog,
                'time' => $this->authenticationLog->created_at,
                'ipAddress' => $this->authenticationLog->ip,
                'browser' => $this->authenticationLog->user_agent,
            ]);
    }

//    /**
//     * Get the Nexmo / SMS representation of the notification.
//     *
//     * @param  mixed  $notifiable
//     * @return \Illuminate\Notifications\Messages\NexmoMessage
//     */
//    public function toNexmo($notifiable)
//    {
//        return (new NexmoMessage)
//            ->content('Your '.iEXContentLanguage('sitename').' account logged in from a new device');
//    }
}
