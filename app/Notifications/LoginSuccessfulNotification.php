<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LoginSuccessfulNotification extends Notification
{
    use Queueable;

    /**
     * Обратный вызов, который должен использоваться для создания почтового сообщения.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Данные пользователя
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    protected $user;

    /**
     * Местоположение
     *
     * @var string
    */
    protected $location;

    /**
     * Create a new notification instance.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $location
     */
    public function __construct(Authenticatable $user, string $location = null)
    {
        $this->user = $user;
        $this->location = $location;
    }

    /**
     * Получите каналы уведомления.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Постройте почтовое представление уведомления.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        $build = iex_mail_custom_template('USER_LOGIN_SUCCESSFUL', ['[user_id]', '[user_name]', '[user_email]', '[user_username]', '[city_country]'], [
            $this->user->id,
            $this->user->name,
            $this->user->email,
            $this->user->username,
            $this->location
        ]);

        if(empty($build)) {
            $subject = iEXContentLanguage('sitename'). ' - Успешная авторизация!';
        } else {
            $subject = $build['title'];
        }

        return (new MailMessage)->subject($subject)
            ->markdown(
                'emails.custom_template', [
                    'subject' => $subject,
                    'default_template' => 'notification.login_successful',
                    'user' => $this->user,
                    'geo_string' => $this->location,
                    'data' => $build
                ]
            );
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
