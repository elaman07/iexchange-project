<?php
namespace App\Notifications;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramPaymentFailed extends Notification
{
    use Queueable;

    /**
     * Текст ошибки
     *
     * @var string
     */
    protected $error_text;

    /**
     * Create a new notification instance.
     *
     * @param $error
     */
    public function __construct($error)
    {
        $this->error_text = $error;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return [TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param Task $item
     * @return TelegramMessage
     * @throws \Throwable
     */
    public function toTelegram(Task $item)
    {
        $id = iEXSetting('client_id_type_for_order') == 1 ? $item->public_id : $item->id;
        $content = "При автовыплате по заявке №{$id} возникли ошибки. ".PHP_EOL;
        $content .= 'Текст ошибки: '.$this->error_text;
        return TelegramMessage::create()
            ->token(config('telegram.bots.notice.token'))
            ->to(config('telegram.channel_id'))
            ->content($content)
            ->options([
                'parse_mode' => 'HTML',
            ]);
    }
}
