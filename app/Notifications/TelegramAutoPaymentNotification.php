<?php

namespace App\Notifications;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramAutoPaymentNotification extends Notification
{
    use Queueable;

    /**
     * Провайдер
     *
     * @var string
     */
    protected $provider;

    /**
     * Create a new notification instance.
     *
     * @param $provider
     */
    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return [TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param Task $item
     * @return TelegramMessage
     * @throws \Throwable
     */
    public function toTelegram(Task $item)
    {
        $price = $item->receiving_price.' '.$item->direction_exchange->currency2->code_currency->name;
        $content = '['.$this->provider.'] - автовыплата по заявке №'.$item->id.' на сумму '.$price.' успешно завершена.';
        return TelegramMessage::create()
            ->token(config('telegram.bots.notice.token'))
            ->to(config('telegram.channel_id'))
            ->content($content)
            ->options([
                'parse_mode' => 'HTML',
            ]);
    }
}
