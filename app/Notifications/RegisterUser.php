<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RegisterUser extends Notification
{
    use Queueable;

    /**
     * Открытый пароль
     *
     * @var array
    */
    protected $open_password;

    /**
     * Create a new notification instance.
     *
     * @param string $password
     */
    public function __construct(string $password)
    {
        $this->open_password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $build = iex_mail_custom_template('NEW_USER', ['[id]', '[name]', '[email]', '[username]','[password]', '[user_ip]', '[user_host]'], [
            $notifiable->id,
            $notifiable->name,
            $notifiable->email,
            $notifiable->username,
            $this->open_password,
            $notifiable->ip_address,
            $notifiable->user_agent
        ]);

        if(empty($build)) {
            $subject = __('email.register_on_site', [], $this->locale).' '.iEXContentLanguage('sitename');
        } else {
            $subject = $build['title'];
        }


        return (new MailMessage)
            ->subject($subject)
            ->markdown(
                'emails.custom_template', [
                    'subject' => $subject,
                    'default_template' => 'notification.new_user',
                    'user' => $notifiable,
                    'password' => $this->open_password,
                    'data' => $build
                ]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
