<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramVerificationCard extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return [TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return TelegramMessage
     */
    public function toTelegram($notifiable)
    {
        $content = '<b>Поступила заявка №'.$notifiable->id.' на верификацию карты '.$notifiable->currency->payment->name.'</b>';
        return TelegramMessage::create()
            ->token(config('telegram.bots.notice.token'))
            ->to(config('telegram.channel_id'))
            ->content($content)
            ->options([
                'parse_mode' => 'HTML',
            ]);
    }
}
