<?php

namespace App\Notifications;

use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramPayouts extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return [TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param WithdrawalRequest $item
     * @return TelegramMessage
     * @throws \Throwable
     */
    public function toTelegram(WithdrawalRequest $item)
    {
        $content = '<b>Поступила новая заявка №'.$item->id.' на выплату вознаграждений</b>';
        if(iEXSetting('telegram_view_notification') == 1) {
            $content = view('telegram.payouts.message', [
                'detail' => $item
            ])->render();
        }

        return TelegramMessage::create()
            ->token(config('telegram.bots.notice.token'))
            ->to(config('telegram.channel_id'))
            ->content($content)
            ->options([
                'parse_mode' => 'HTML',
            ]);
    }
}
