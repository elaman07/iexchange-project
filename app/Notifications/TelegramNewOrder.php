<?php

namespace App\Notifications;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramNewOrder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return [TelegramChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param Task $item
     * @return TelegramMessage
     * @throws \Throwable
     */
    public function toTelegram(Task $item)
    {
        $telegram = TelegramMessage::create()
            ->token(config('telegram.bots.notice.token'))
            ->to(config('telegram.channel_id'));

        if((int)iEXSetting('telegram_view_notification', 0) == 0) {
            $telegram->content('<b>Поступила новая заявка №'.iEXSetting('client_id_type_for_order') == 1 ? $item->public_id : $item->id.'</b>');
        }else {
            $telegram->content(view("telegram.message", [
                'detail' => $item
            ])->render());
        }

        return $telegram->options([
            'parse_mode' => 'HTML'
        ]);
    }
}
