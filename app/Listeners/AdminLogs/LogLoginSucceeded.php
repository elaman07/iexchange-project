<?php
namespace App\Listeners\AdminLogs;


use App\Jobs\TelegramAdminAuthJob;
use App\Models\AdminAuthLog;
use Illuminate\Http\Request;
use PragmaRX\Google2FALaravel\Events\LoginSucceeded;

class LogLoginSucceeded
{
    /**
     * Request
     *
     * @var Request
    */
    protected $request;

    /**
     * Create the event listener.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  LoginSucceeded  $event
     * @return void
     */
    public function handle(LoginSucceeded $event)
    {
        $prev_ip = AdminAuthLog::where('id_user', '=', $event->user->id)->orderByDesc('id')->first();
        AdminAuthLog::create([
            'id_user'           =>  $event->user->id,
            'prev_ip_address'   =>  (isset($prev_ip) ? $prev_ip->ip_address : null),
            'ip_address'        =>   $this->request->ip(),
            'is_successful'     =>  1
        ]);

        // Отправляем в Telegram уведомление об авторизации
        if(iEXSetting('is_successful_admin_auth') == 1)
        {
            $delay = now()->addMinutes(1);
            dispatch(new TelegramAdminAuthJob($event->user, true))
                ->delay($delay)
                ->onQueue('low');
        }

    }
}
