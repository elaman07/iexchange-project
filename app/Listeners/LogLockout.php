<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.04.2019
 * Time: 7:42
 */

namespace App\Listeners;


use App\Jobs\LockoutAuthJob;
use App\Models\User;
use Illuminate\Auth\Events\Lockout;

class LogLockout
{
    /**
     * Create the event listener.
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param  Lockout  $event
     * @return void
     */
    public function handle(Lockout $event)
    {
        if(!iEXSetting('is_lockout_auth'))
            return;

        $user = User::where('email', '=', $event->request->email)->first();
        dispatch(new LockoutAuthJob($user, $event->request->ip()))->delay(
            now()->addMinutes(5)
        )->onQueue('low');
    }
}
