<?php

namespace App\Listeners;

use App\Jobs\EmailLoginSuccessfulJob;
use App\Jobs\NewDeviceJob;
use App\Notifications\NewDeviceNotification;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use hisorange\BrowserDetect\Parser as Browser;


class LogSuccessLogin
{
    /**
     * Request
     *
     * @var \Illuminate\Http\Request
    */
    protected $request;

    /**
     * Create the event listener.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Login $event
     * @return void
     * @throws \Exception
     */
    public function handle(Login $event)
    {
        $user = $event->user;
        $ip = $this->request->ip();
        $userAgent = $this->request->userAgent();
        $known = $user->user_auth()->whereIp($ip)->whereUserAgent($userAgent)->first();

        // Записываем успешный вход в систему
        if(iEXSetting('event_log_is_login_success')) {
            $log = sprintf('Пользователь %s успешно авторизовался', $user->name);
            main_event_log('event_log_login_success', $log);
        }

        //Записываем в лог авторизации
        $userAuth = user_auth_log($user->id, 0, $user->ip_address, $userAgent);

        // Счетчик входов
        $user->num_auth = ($user->num_auth + 1);

        // Обновление данныех при авторизации
        $user->ip_address = $this->request->getClientIp();
        $user->user_agent = $this->request->userAgent();
        $user->user_browser = Browser::browserFamily() ?? '';
        //$user->user_device = Browser::deviceType() ?? '';
        $user->last_activity_at = Carbon::now();
        $user->last_login_at = Carbon::now();
        $user->language = app()->getLocale();
        $user->save();


        // Отправить уведомление об успешной авторизации
        if(iEXSetting('is_user_successful_login') == 1) {
            $delay = now()->addMinutes(2);
            dispatch(
                (new EmailLoginSuccessfulJob($user))->delay($delay)->onQueue('low')
            );
        }

        // Уведомлять о входах с новых устройств
        if (! $known && (int)iEXSetting('is_notify_new_device') == 1 and isset($userAuth)) {
            dispatch((new NewDeviceJob($user, $userAuth)))
                ->delay(Carbon::now()->addMinute())
                ->onQueue('low');
        }
    }
}
