<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;

class LogSuccessLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Logout $event
     * @return void
     */
    public function handle(Logout $event)
    {
        // Записываем выход из системы
        if(iEXSetting('event_log_is_logout')) {
            $log = sprintf('Пользователь %s вышел из учетной записи', $event->user->name);
            main_event_log('event_log_logout', $log);
        }

        $event->user->last_logout_at = Carbon::now();
        $event->user->save();
    }
}
