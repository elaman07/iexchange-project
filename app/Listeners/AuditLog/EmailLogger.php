<?php
namespace App\Listeners\AuditLog;


use App\Models\LogMail;
use Carbon\Carbon;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Mime\Email;

class EmailLogger
{
    /**
     * Handle the event.
     *
     * @param MessageSending $event
     */
    public function handle(MessageSending $event)
    {
        if(!iEXSetting('is_enable_email_notification'))
            return;

        $message = $event->message;
        DB::table('logs_email')->insert([
            'created_at' => Carbon::now()->format('c'),
            'from_mail' => $this->formatAddressField($message, 'From'),
            'to_mail' => $this->formatAddressField($message, 'To'),
            'subject' => $message->getSubject(),
            'body'  =>  $message->getBody()->bodyToString(),
            'headers' => $message->getHeaders()->toString(),
        ]);
    }

    function formatAddressField(Email $message, string $field): ?string
    {
        $headers = $message->getHeaders();
        return $headers->get($field)?->getBodyAsString();
    }
}
