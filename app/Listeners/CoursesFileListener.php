<?php

namespace App\Listeners;

use App\Events\CoursesFileEvent;

class CoursesFileListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        // ...
    }


    public function handle(CoursesFileEvent $event)
    {


        if((int)iEXSetting('grates_static') == 1) {
            \Crypto::setSchema('xml')->delete();
        }

        if(isJobOffline())
        {
            if((int)iEXSetting('grates_static') == 0) {
                \Crypto::setSchema('xml')->empty();
            }
            \Crypto::setSchema('txt')->empty();
            \Crypto::setSchema('json')->delete();

        } else {

            if((int)iEXSetting('grates_static') == 0) {
                \Crypto::setSchema('xml')->build();
            }
            \Crypto::setSchema('txt')->build();
            \Crypto::setSchema('json')->build();
        }

    }
}
