<?php
namespace App\Listeners;

use Illuminate\Auth\Events\Authenticated;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;

class LogUserAuthenticated
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * The URIs that should be excluded
     *
     * @var array
     */
    protected $except = [

    ];

    /**
     * Create the event listener.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->except = [
            admin_base_path('/json/*'),
            admin_base_path('/private/*'),
            'api/*'
        ];
    }

    /**
     * Handle the event.
     *
     * @param Authenticated $event
     * @return void
     */
    public function handle(Authenticated $event)
    {
        // исключаем некоторые страницы
        if($this->inExceptArray($this->request)) return;

        if(\Auth::check()) {
            $event->user->ip_changed = ($this->request->ip() != $event->user->ip_address);
            $event->user->last_activity_at = Carbon::now();
            $event->user->current_page = $this->request->path();
            $event->user->ip_address = $this->request->getClientIp();

            $event->user->save();
        }
    }

    /**
     * Determine if the request has a URI that should pass through CSRF verification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }
            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }
        return false;
    }
}
