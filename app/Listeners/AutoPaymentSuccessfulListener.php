<?php
namespace App\Listeners;

use App\Events\AutoPaymentSuccessful;
use App\Jobs\TelegramAutoPaymentJob;

class AutoPaymentSuccessfulListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AutoPaymentSuccessful $event
     * @return void
     */
    public function handle(AutoPaymentSuccessful $event)
    {
        if(iEXSetting('is_order_notify_auto_payment') == 1) {
            $delay = now()->addMinutes(1);
            dispatch(new TelegramAutoPaymentJob($event->order, $event->provider))
                ->delay($delay)->onQueue('low');
        }
    }
}