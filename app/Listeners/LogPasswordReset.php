<?php
namespace App\Listeners;


use Illuminate\Auth\Events\PasswordReset;

class LogPasswordReset
{
    /**
     * Create the event listener.
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param  PasswordReset  $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        // Записываем в лог информацию об успешном изменении пароля
        if(iEXSetting('event_log_is_password_change')) {
            $event_log = sprintf('Пользователь %s успешно изменить пароль', $event->user->name);
            main_event_log('event_log_password_change', $event_log, $event->user->id);
        }
    }
}