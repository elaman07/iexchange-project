<?php

namespace App\View\Components\Admin;

use App\Models\Review;
use App\Models\WithdrawalRequest;
use Illuminate\View\Component;

class Toolbar extends Component
{
    /**
     * The alert message.
     *
     * @var string
     */
    public $message;

    /**
     * Create a new component instance.
     *
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        $this->message = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('admin.components.toolbar');
    }
}
