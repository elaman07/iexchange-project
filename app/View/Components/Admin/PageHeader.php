<?php

namespace App\View\Components\Admin;

use App\Models\FavoriteLink;
use Illuminate\View\Component;

class PageHeader extends Component
{
    protected $selected_url;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $link = explode(config('admin.route_path'), \Request::getRequestUri());
        if(isset($link[1]))
            $this->selected_url = $link[1];


        $selected_favorites = FavoriteLink::where([
            ['link', $this->selected_url],
            ['id_user', auth()->id()]
        ])->first();

        return view('admin.components.page-header', [
            'current_url' => $selected_favorites
        ]);
    }
}
