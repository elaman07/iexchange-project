<?php

namespace App\View\Components\Admin;

use App\Models\FavoriteLink;
use Illuminate\View\Component;

class BreadcrumbElements extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $user = auth()->user();

        $links = FavoriteLink::where('id_user', '=', $user->id)->orderBy('sorting')->get();

        return view('admin.components.breadcrumb-elements', [
            'links' => $links,
            'user' => $user
        ]);
    }
}
