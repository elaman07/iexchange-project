<?php

namespace App\View\Components\Forms\Language;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class TextareaCkeditor extends Component
{
    /**
     * Create the component instance.
     */
    public function __construct(
        public string $tabName,
        public string $keyName,
        public string $ckeditorId,
        public ?string $label = null,
    ) {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.forms.language.textarea-ckeditor', [
            'label' => $this->label,
            'tab_name' => $this->tabName,
            'key_name' => $this->keyName,
            'ckeditor_id' => $this->ckeditorId
        ]);
    }
}
