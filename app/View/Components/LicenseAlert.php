<?php

namespace App\View\Components;

use App\Common\Packages\Update\Facades\UpdateClientFacade;
use Illuminate\View\Component;

class LicenseAlert extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.license-alert', [
            'is_active' => UpdateClientFacade::isEnable()
        ]);
    }
}
