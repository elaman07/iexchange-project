<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 11.03.2020
 * Time: 18:08
 */

namespace App\Common\Packages\X19;


use App\Common\Packages\Update\Console\AutoCheckUpdateConsole;
use App\Common\Packages\Update\Console\UpdateConsole;
use Illuminate\Support\ServiceProvider;

class X19ServiceProvider extends ServiceProvider
{
    public function boot() {
        //
    }

    public function register()
    {
        $this->app->singleton('webmoney-x19', function() {
            return new X19();
        });
    }
}