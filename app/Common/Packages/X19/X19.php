<?php
namespace App\Common\Packages\X19;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\DirectionExchange;
use App\Models\Task;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use baibaratsky\WebMoney\{
    Request\Requester\CurlRequester,
    Signer,
    WebMoney,
    Api\X\X19\Request as X19Request
};
use Carbon\Carbon;
use DiDom\Document;
use Illuminate\Support\Facades\Config;
use baibaratsky\WebMoney\Api\X\X8\Request as X8Request;

class X19
{
    /**
     * Webmoney Client
     *
     * @var WebMoney
     */
    protected WebMoney $webmoney;

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $config;


    protected $pay_data;

    /**
     * Конструктор X19 Интерфейса Webmoney
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->webmoney = new WebMoney(new CurlRequester);
    }

    /**
     * Проверка введенных данных
     *
     * @param array $options
     * @param int $selected
     * @param $pay_data
     * @return string
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     * @throws \baibaratsky\WebMoney\Exception\CoreException
     */
    public function check(array $options, int $selected, $pay_data)
    {
        $this->pay_data = $pay_data;

        if(isset($pay_data) and !empty($pay_data)) {
            $this->config = protect_gateway_keys('webmoney', $pay_data->id, 'pay');
        }

        $passport = null;
        if (isset($options['direction_fields']['passport'])) {
            $passport = $options['direction_fields']['passport'];
        }

        $last_name = null;
        if (isset($options['direction_fields']['lastname'])) {
            $last_name = $options['direction_fields']['lastname'];
        }

        $first_name = null;
        if (isset($options['direction_fields']['firstname'])) {
            $first_name = $options['direction_fields']['firstname'];
        }

        $bank_name = 'Sberbank RF';
        if (isset($options['direction_fields']['bank_name'])) {
            $bank_name = $options['direction_fields']['bank_name'];
        }


        $income_account = (isset($options['income_account']) ? preg_replace('/\s/', '', $options['income_account']) : null);
        $outcome_account = isset($options['outcome_account']) ? preg_replace('/\s/', '', $options['outcome_account']) : null;

        // Прием Webmoney
        $firsts_wm = [3, 4, 8, 9, 10];

        if(in_array($selected, $firsts_wm)) {
            $in_account = $income_account;
            $out_account = $outcome_account;
        } else {
            $in_account = $outcome_account;
            $out_account = $income_account;
        }

        $x19mods = [
            1 => [ // QIWI Кошелёк -> WM
                'type' => 5,
                'direction' => 2,
                'emoney_name' => 'qiwi.ru',
                'emoney_id' => $this->validate('phone', $income_account)
            ],

            2 => [ // Яндекс.Деньги -> WM
                'type' => 5,
                'direction' => 2,
                'emoney_name' => 'money.yandex.ru',
                'emoney_id' => $income_account
            ],

            3 => [ // WM -> QIWI Кошелёк
                'type' => 5,
                'direction' => 1,
                'emoney_name' => 'qiwi.ru',
                'emoney_id' => $this->validate('phone', $outcome_account)
            ],

            4 => [ // WM -> Яндекс.Деньги
                'type' => 5,
                'direction' => 1,
                'emoney_name' => 'money.yandex.ru',
                'emoney_id' => $outcome_account
            ],

            5 => [ // Наличные в офисе -> WM
                'type' => 1,
                'direction' => 2,
                'pnomer' => (string)$passport
            ],

            6 => [ // Банковский счет -> WM
                'type' => 3,
                'direction' => 2,
                'bank_name' => $bank_name,
                'bank_account' => $income_account
            ],

            7 => [ // Банковская карта -> WM
                'type' => 4,
                'direction' => 2,
                'bank_name' => $bank_name,
                'card_number' => $income_account
            ],

            8 => [ // WM -> Наличные в офисе
                'type' => 1,
                'direction' => 1,
                'pnomer' => $passport
            ],

            9 => [ // WM -> Банковский счет
                'type' => 3,
                'direction' => 1,
                'bank_name' => $bank_name,
                'bank_account' => $outcome_account
            ],

            10 => [ // WM -> Банковская карта
                'type' => 4,
                'direction' => 1,
                'bank_name' => $bank_name,
                'card_number' => $outcome_account
            ],

            // 11 - Webmoney - Webmoney

            12 => [ // WM -> Bitcoin
                'type' => 8,
                'direction' => 1,
                'crypto_name' => 'bitcoin',
                'crypto_address' => $outcome_account
            ],

            13 => [ // WM -> Bitcoin Cash
                'type' => 8,
                'direction' => 1,
                'crypto_name' => 'bitcoincash',
                'crypto_address' => $outcome_account
            ],

            14 => [ // WM -> PayPal
                'type' => 5,
                'direction' => 1,
                'emoney_name' => 'paypal.com',
                'emoney_id' => $outcome_account
            ],

            15 => [ // WM -> Skrill (Moneybookers)
                'type' => 5,
                'direction' => 1,
                'emoney_name' => 'moneybookers.com',
                'emoney_id' => $outcome_account
            ],

            16 => [ // Skrill (Moneybookers) -> WM
                'type' => 5,
                'direction' => 2,
                'emoney_name' => 'moneybookers.com',
                'emoney_id' => $income_account
            ],

            17 => [ // PayPal -> WM
                'type' => 5,
                'direction' => 2,
                'emoney_name' => 'paypal.com',
                'emoney_id' => $income_account
            ],

            18 => [ // EasyPay -> WM
                'type' => 5,
                'direction' => 2,
                'emoney_name' => 'easypay.by',
                'emoney_id' => $income_account
            ],

            19 => [ // WM -> EasyPay
                'type' => 5,
                'direction' => 1,
                'emoney_name' => 'easypay.by',
                'emoney_id' => $income_account
            ]
        ];


        $responseMod = $x19mods[$selected] ?? null;
        $purse_type = 'WM' . Str::upper(mb_substr($in_account, 0, 1));


        $get_wmid = $this->getWMIDFromAccount($in_account);

        if(is_bool($get_wmid)) {
            return 'wrong account number (wmid)';
        }

        // Webmoney -> Webmoney
        if($selected == 11) {
            $get_wmid2 = $this->getWMIDFromAccount($out_account);

            if(is_bool($get_wmid2)) {
                return 'Script is unable to define Wmid 2';
            }

            if($get_wmid != $get_wmid2) {
                return 'Owner own several accounts';
            }

            return 'OK';
        }

        $request = new X19Request;
        $request->setSignerWmid($this->config['wmid']); // admin wmid
        $request->setOperationType($responseMod['type']);
        $request->setOperationDirection($responseMod['direction']);
        $request->setOperationPurseType($purse_type);
        $request->setOperationAmount($options['outcome_amount'] ?? 1000);

        if ($responseMod['type'] == 5) {
            $request->setUserEMoneyId($responseMod['emoney_id']);
            $request->setUserEMoneyName($responseMod['emoney_name']);
        }

        // Для 1 типа
        if ($responseMod['type'] == 1) {
            $request->setUserLastName($last_name);
            $request->setUserFirstName($first_name);
            $request->setUserPassportNum($responseMod['pnomer']);
        }

        if ($responseMod['type'] == 3) {
            $request->setUserLastName($last_name);
            $request->setUserFirstName($first_name);
            $request->setUserBankName($responseMod['bank_name']);
            $request->setUserBankAccount($responseMod['bank_account']);
        }

        if ($responseMod['type'] == 4) {
            $request->setUserLastName($last_name);
            $request->setUserFirstName($first_name);
            $request->setUserBankName($responseMod['bank_name']);
            $request->setUserCardNumber($responseMod['card_number']);
        }

        if($responseMod['type'] == 8) {
            $request->setUserCryptoName($responseMod['crypto_name']);
            $request->setUserCryptoAddress($responseMod['crypto_address']);
        }


        $request->setUserWmid($get_wmid); // user wmid
        $request->sign($this->sign());

        // Создаем файлы
        if(!\File::isDirectory(storage_path('/x19'))) {
            \File::makeDirectory(storage_path('/x19'), 0777, true);
        }

        if ($request->validate())
        {
            $response = $this->webmoney->request($request);
            if ($response->getReturnCode() === 0) {

                // записываем лог
                $is_file = storage_path('x19').'/'.$this->config['wmid'].'-'.Carbon::now()->unix().'.xml';
                if(!\File::exists($is_file)) {
                    \File::put($is_file, '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.$request->getData());
                }

                return 'OK';
            } else {
                return 'Error: ' . $response->getReturnDescription();
            }
        }
        return 'Webmoney Verify errors';
    }

    /**
     * Валидатор счетов
     *
     * @param string $type
     * @param string $value
     * @return mixed|string
     */
    private function validate(string $type, string $value = null)
    {
        switch ($type) {
            case 'phone':
                $tel = str_replace(['(', ')', '-', ' '], '', $value);
                if (strstr($tel, '+'))
                    return mb_substr($tel, 2, mb_strlen($tel));
                return $tel;
        }
    }


    /**
     * Получаем WMID по номеру счета
     *
     * @param string|null $account
     */
    protected function getWMIDFromAccount(string $account = null)
    {
        try {
            $curl = new CurlRequester();
            $curl->setSslDisabled(true);


            $webmoney = new WebMoney($curl);

            $request = new X8Request;
            $request->setSignerWmid($this->config['wmid']);
            $request->setPurse($account);
            $request->sign($this->sign());

            if ($request->validate()) {
                $response = $this->webmoney->request($request);

                if ($response->getReturnCode() === 1) {
                    return (string)$response->getWmid();
                } else {
                    throw new \Exception( $response->getReturnDescription());
                }
            }
        }catch (\Exception $exception) {
            return false;
        }


//        try {
//            return PaymentFacade::pay('webmoney', $this->pay_data->id)->api()->x8(mb_strtoupper($account));
//        }catch (\Exception $exception) {
//            return false;
//        }

//        if((int)iEXSetting('is_webmoney_x19_server', 0) == 0) {
//            return PaymentFacade::driver('webmoney')->getFactory()->x8(mb_strtoupper($account));
//        }
////
//        $curl = curl_init('https://passport.webmoney.ru/asp/CertView.asp?purse=' . $account);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
//        $page = curl_exec($curl);
//
//        $parser = new Document($page);
//
//        $response = $parser->xml();
//        preg_match("/\<title.*\>(.*)\<\/title\>/isU", $response, $matches);
//
//
//
//        if(isset($matches[1]) and is_numeric(mb_substr($matches[1], 5))) {
//            return mb_substr($matches[1], 5);
//        }
//
//        return false;
    }

    /**
     * Подпись транзакции
     *
     * @throws \Exception
     */
    private function sign() {
        return new Signer($this->config['wmid'], storage_path($this->config['wmid'].'.kwm'), $this->config['key_pass']);
    }
}
