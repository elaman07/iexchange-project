<?php
namespace App\Common\Packages\X19\Facades;


use App\Common\Packages\Transaction\Transaction;
use App\Common\Packages\X19\X19;
use App\Models\Task;
use Illuminate\Support\Facades\Facade;


/**
 * @method static X19 check(array $options, int $selected, $merchant_pay)
 */
class X19Facade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'webmoney-x19';
    }
}
