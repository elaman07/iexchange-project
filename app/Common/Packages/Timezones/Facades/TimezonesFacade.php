<?php
namespace App\Common\Packages\Timezones\Facades;

use Illuminate\Support\Facades\Facade;

class TimezonesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'timezones';
    }
}
