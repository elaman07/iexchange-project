<?php

namespace App\Common\Packages\Crypto\Validators;

class LuhnValidator
{
    public function isValid($value = null): bool
    {
        if(empty($value)) return false;

        $value = str_replace(' ','', $value);
        return \Luhn::isValid(trim($value));
    }
}
