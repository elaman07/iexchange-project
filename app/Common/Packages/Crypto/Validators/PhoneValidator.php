<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 20.09.2019
 * Time: 18:58
 */

namespace App\Common\Packages\Crypto\Validators;


class PhoneValidator
{
    /**
     * Валидация QIWI Номеров
     *
     * @param null $value
     * @return bool
     */
    public function isValid($value = null)
    {
        if(empty($value)) return false;
        return $this->validation($value);
    }

    /**
     * Валидация номеров
     *
     * @param $value
     * @return false|int
     */
    protected function validation($value) {
        return preg_match('/^(\+)?(\(\d{2,3}\) ?\d|\d)(([ \-]?\d)|( ?\(\d{2,3}\) ?)){5,12}\d$/', $value);
    }
}
