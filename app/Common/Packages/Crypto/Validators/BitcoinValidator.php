<?php

namespace App\Common\Packages\Crypto\Validators;

class BitcoinValidator
{
    private $regex = "/^([13][a-km-zA-HJ-NP-Z1-9]{25,34})|^(bc1([qpzry9x8gf2tvdw0s3jn54khce6mua7l]{39}|[qpzry9x8gf2tvdw0s3jn54khce6mua7l]{59}))$/";


    public function isValid($value = null): bool
    {
        if(empty($value)) return false;

        $value = str_replace(' ','', $value);
        return (bool)preg_match($this->regex, $value);
    }
}
