<?php
namespace App\Common\Packages\Crypto\Compilers;

use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use Illuminate\Filesystem\Filesystem;

abstract class AbstractCompiler
{
    /**
     * Конфигурационный файл
     *
     * @var array
     */
    protected $config;

    /**
     * Экземпляр файловой системы.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Массив для сбора данных
     *
     * @var array
     */
    protected $document = [];

    /**
     * Создайте новый экземпляр компилятора.
     *
     * @param  \Illuminate\Filesystem\Filesystem $files
     * @param Repository $repository
     */
    public function __construct(Filesystem $files, $repository)
    {
        $this->files = $files;
        $this->config = $repository;
        $this->document = collect();

        if(empty($repository)) {
            throw new InvalidArgumentException('Не найдено конфигурация компилятора');
        }

        $this->registerCompiler();
    }

    /**
     * Добавить новые значения к массиву
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        Arr::set($this->document, $key, $value);
        return $this;
    }

    /**
     * Получить необходимые элементы из массива
     *
     * @param $key
     * @return array|\Illuminate\Support\Collection|mixed
     */
    public function get($key)
    {
        if($key == null) {
            return $this->document;
        }else {
            return Arr::get($this->document, $key);
        }
    }

    /**
     * Получаем версию модуля
     *
     * @return float
    */
    protected function getVersion()
    {
        return $this->config->get('version');
    }

    /**
     * Получаем время генерации документа
     *
     * @return string
    */
    public function getCreatedAt()
    {
        return Carbon::now()->toDateTimeString();
    }

    /**
     * Получаем путь к файлу
     *
     * @return string
    */
    public function getPath()
    {
        return $this->config->get('path');
    }

    /***/
    public function registerCompiler()
    {

    }
}