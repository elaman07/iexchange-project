<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.05.2018
 * Time: 17:40
 */

namespace App\Common\Packages\Crypto\Compilers\Concerns;


use App\Models\Currency;
use App\Models\CurrencyFields;
use App\Models\DirectionExchange;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

trait CompilersManager
{
    /**
     * Список валют "Отдаю"
     *
     * @return \Illuminate\Support\LazyCollection
    */
    public function getListIn()
    {
        // Получение статусов режимов работ
        if((int)iEXSetting('is_enabled_module_direction_mode') == 0)
        {
            $in = Currency::whereStatus(0)->whereHas('direction_exchange_in', function(Builder $query) {
                if (iEXSetting('is_visible_disabled_directions')) {
                    $query->where([
                        ['status', '!=', 2],
                        ['is_error_rate', '=', 0]
                    ]);
                } else {
                    $query->where([
                        ['status', '=', 1],
                        ['is_error_rate', '=', 0]
                    ]);
                }
            });
        } else {
            $modes_count = \App\Models\DirectionExchangeMode::where('status', 1)->first();
            $in = Currency::whereStatus(0)->whereHas('direction_exchange_in', function(Builder $query) use ($modes_count)
            {
                if(isset($modes_count->direction_exchange)) {
                    $response = $query->whereHas('direction_modes', function ($query) {
                        return $query->where('status', 1);
                    })->cursor();
                }  else {
                    if (iEXSetting('is_visible_disabled_directions')) {
                        $query->where([
                            ['status', '!=', 2],
                            ['is_error_rate', '=', 0]
                        ]);
                    } else {
                        $query->where([
                            ['status', '=', 1],
                            ['is_error_rate', '=', 0]
                        ]);
                    }
                }
            });
        }
        return $in->orderBy('sorting_1')->get();
    }

    /**
     * Список валют "Получаю"
     *
     * @param int $in
     * @return \Generator|\Illuminate\Support\LazyCollection
     */
    public function getListOut(int $in = 0)
    {
        $direction_exchange = DirectionExchange::with([
            'direction_exchange_percentage_amount',
            'direction_exchange_cities',
            'direction_field_sorting'
        ])->active();

        if(isset($modes_count->direction_exchange) and (int)iEXSetting('is_enabled_module_direction_mode') == 1)
        {
            $response = $direction_exchange->whereHas('direction_modes', function ($query) {
                return $query->where('status', 1);
            })->cursor();
        } else {
            if (iEXSetting('is_visible_disabled_directions')) {
                $response = $direction_exchange->get();
            } else {
                $response = $direction_exchange->where('status', '=', 1)->get();
            }
        }

        $response_callback = $response->filter(function($filter) {
            if($filter->is_enabled_exchange == 0) return $filter;

            // Отображаем направелния по расписанию
            if($filter->is_enabled_exchange == 1 and
                Carbon::now()->format('H:i') > $filter->from_on_time and
                Carbon::now()->format('H:i') < $filter->to_on_time)
                return $filter;
        });

        if($this->is_telegram_bot == 1)
        {
            $response_callback = $response_callback->filter(function($item) {
                if($this->is_telegram_bot == 1)
                    return $item->is_allow_telegram_bot == 0;
                return $item;
            });
        }

        return $response_callback->reject(function($item) {
            return $item->is_hidden_not_locale == 1 and !empty($item->languages) and !in_array(app()->getLocale(), explode(',', $item->languages));
        })->reject(function($item) {
            return $item->is_hidden_not_device == 1 and !empty($item->device) and !in_array($this->getUserDevice(), explode(',', $item->device));
        })->reject(function($item) {
            return $item->is_hidden_noauth_user == 1 and !auth()->check();
        });
    }

    /**
     * Добавить новые значения к массиву
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function add($key, $value)
    {
        Arr::add($this->document, $key, $value);
        return $this;
    }

    /**
     * Добавить новые значения к массиву
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        Arr::set($this->document, $key, $value);
        return $this;
    }

    /**
     * Получить необходимые элементы из массива
     *
     * @param $key
     * @return array|\Illuminate\Support\Collection|mixed
     */
    public function get($key)
    {
        if($key == null) {
            return $this->document;
        }else {
            return Arr::get($this->document, $key);
        }
    }
}
