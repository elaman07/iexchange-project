<?php
namespace App\Common\Packages\Crypto\Compilers\Concerns;

use App\Models\GroupParserExchange;
use App\Models\ParserApiKey;
use App\Models\ParserExchange;
use App\Models\ParserExchangeHttpLog;
use App\Models\ParserExchangeLog;
use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleHttp;
use GuzzleHttp\Promise\EachPromise;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use function Symfony\Component\String\b;


trait CompilersCrypto
{
    /**
     * Драйвер парсинга
    */
    protected $driver;

    /**
     * Массив лога
     *
     * @var array
    */
    protected $logUpdate;

    /**
     * Скомпилировать документ курсов из Других источников
     *
     * @return void
     * @throws \Exception
     */
    public function compileCrypto()
    {
        $client = new GuzzleHttp();

        $groups = GroupParserExchange::whereStatus(1)
            ->withCount('parser_exchange_enabled')
            ->orderBy('priority', 'desc')->get();

        $promise = (function () use ($groups, $client)
        {
            foreach ($groups as $group)
            {
                $provider_url = $group->provider_url;
                $options = [];

                // Fixer API
                if(Str::lower($group->provider_id) == 'fixerapi') {
                    $keys = ParserApiKey::where([['status', 1], ['provider_id', 'fixerapi']])->inRandomOrder()->first();
                    $options = [
                        'headers' => ['apikey' => trim($keys->api_key)]
                    ];
                }

                if(Str::lower($group->provider_id) == 'coinmarketcap') {
                    $keys = ParserApiKey::where([['status', 1], ['provider_id', 'coinmarketcap']])->inRandomOrder()->first();
                    $options = [
                        'query' =>  [
                            'start' =>  1,
                            'limit' =>  100
                        ],
                        'headers' => ['X-CMC_PRO_API_KEY' => trim($keys->api_key)]
                    ];
                }

                if(Str::lower($group->alias) == Str::lower('NationalBankOfMoldova')) {
                    $provider_url = str_replace('{date_now}', date('d.m.Y'), $provider_url);
                }

                yield $client->requestAsync('GET', $provider_url, $options);
            }
        })();

        $pool = (new EachPromise($promise, [
            'concurrency' => 10,
            'fulfilled'   => function (ResponseInterface $response, int $index) use($groups)
            {
                $group = $groups[$index];

                // Получаем итоговый результат запроса
                $data = $response->getBody()->getContents();

                // Записываем данные в лог
                if((int)iEXSetting('is_record_parser_http_log') == 1) {
                    ParserExchangeHttpLog::create([
                        'source' => $group->alias,
                        'status' => $response->getStatusCode(),
                        'data' => json_encode([
                            'url' => $group->provider_url,
                            'headers' => $response->getHeaders(),
                            'status_code' => $response->getStatusCode(),
                            'protocol_version' => $response->getProtocolVersion(),
                            'size' => $response->getBody()->getSize(),
                            'contents' => $data,
                        ]),
                    ]);
                }


                if($group->parser_exchange_enabled->count() == 0) {
                    $this->command->error('* Не найдены пары в источнике : '. $group->name);
                    return;
                }
                $this->command->line('* Обновление курсов из источника: '. $group->name);

                $this->driver = \Crypto::driver($group->alias);
                $this->driver->loadRates($data);


                $parser_exchange = ParserExchange::where([['id_group', '=', $group->id], ['status', '=', 1]])
                    ->orderBy('type', 'asc')->cursor();

                foreach ($parser_exchange as $parser)
                {
                    $this->command->info($group->name.' обновление пары '.$parser->name. '(тип '.$parser->type.')');

                    // Обновление курсов с сервера источника
                    if($parser->type == 0 and $parser->status == 1)
                    {
                        try {
                            $result_amount = iex_number_format(
                                $this->driver->setPair($parser->name)->setTickerType($parser->type_price)->get(), $parser->number_format);

                            //Обновление данных в базе
                            $parser->update([
                                'summa' => $result_amount,
                                'value' => $parser->value,
                                'value_default' => 1,
                                'summa_default' => $result_amount,
                                'is_not_update' => 0
                            ]);
                        }catch (\Exception $exception) {
                            $this->command->error(sprintf('Ошибка обновления пары %s(%s). Текст ошибки: %s', $group->name, $parser->name, $exception->getMessage()));
                            $parser->update(['is_not_update' => 1]);
                            $this->driver->failedUpdateCourses();
                            \Log::warning(sprintf('Курсы из источника %s не обновлены (Текст ошибки: %s)', $group->name, $exception->getMessage()));
                        }
                    }

                    // Обновление курсов локально (/)
                    if($parser->type == 1 and $parser->status == 1)
                    {
                        try {
                            //Разбираем название
                            $pair = \explode(' - ', $parser->name);
                            $data = $parser_exchange->first(function($item) use($pair) {
                                return $item['name'] == sprintf('%s - %s', $pair[1], $pair[0]);
                            });

                            try {
                                $result_amount = iex_number_format(
                                    1 / (float)$data->summa, $parser->number_format
                                );
                            }catch (\DivisionByZeroError) {

                            }

                            $parser->update([
                                'summa' => $result_amount ?? 0,
                                'value' => $parser->value,
                                'value_default' => 1,
                                'summa_default' => $result_amount ?? 0,
                                'is_not_update' => 0
                            ]);

                        }catch (\Exception $exception) {
                            $this->command->error(sprintf('Ошибка обновления пары %s(%s). Текст ошибки: %s', $group->name, $parser->name, $exception->getMessage()));
                            $parser->update(['is_not_update' => 1]);
                            $this->driver->failedUpdateCourses();
                            \Log::warning(sprintf('Курсы из источника %s не обновлены (Текст ошибки: %s)', $group->name, $exception->getMessage()));
                        }
                    }

                    // Для лога
                    $this->logUpdate[] = [
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'id_parser_exchange' => $parser->id,
                        'in_price' => $parser->value,
                        'out_price' => ($result_amount ?? 0)
                    ];

                }

                GroupParserExchange::find($group->id)->update(['last_updated_at' => \Illuminate\Support\Carbon::now()->format('c')]);
            },
            'rejected'    => function ($reason, $index) {
                $this->command->error('ERROR => ' . strtok($reason->getMessage(), "\n") . PHP_EOL);
            },
        ])
        )->promise()->wait();
        $this->writeToLog();
    }

    /**
     * Серверное обновление данных
     *
     * @param ParserExchange $item
     * @return string
     */
    private function serverUpdate(ParserExchange $item)
    {
        $amount = iex_number_format($this->driver->setPair($item->name)->get(), $item->number_format);

        //Обновление данных в базе
        ParserExchange::where('id', '=', $item->id)->update([
            'summa' => $amount,
            'value' => $item->value,
            'value_default' => 1,
            'summa_default' => $amount
        ]);

        return $amount;
    }

    /**
     * Внутренее обновление данных
     *
     * @param ParserExchange $item
     * @return string
     */
    private function localUpdate(ParserExchange $item)
    {
        //Разбираем название
        $pair = \explode(' - ', $item->name);
        $data = ParserExchange::where([
            ['name', '=', $pair[1].' - '.$pair[0]],
            ['id_group', $item->id_group]
        ])->first();

        $amount_default = iex_number_format(
            1 / (float)$data->summa, $item->number_format
        );

        //Обновление данных в базе
        ParserExchange::where('id', '=', $item->id)->update([
            'summa' => $amount_default,
            'value' => $item->value,
            'value_default' => 1,
            'summa_default' => $amount_default
        ]);

        return $amount_default;
    }

    /**
     * Записываем в лог курсы
     *
     */
    protected function writeToLog()
    {
        // Статус записи лога
        if(!iEXSetting('record_course_history'))
            return;

        ParserExchangeLog::insert($this->logUpdate);
    }
}
