<?php
namespace App\Common\Packages\Crypto\Compilers\Concerns;


use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\Reserve;
use Illuminate\Support\Arr;

trait CompilersReserves
{
    /**
     * Компиляция резервов
     *
     * @return array
     */
    public function compileReserve(): array
    {
        $response = Currency::select('id', 'status', 'max_display_reserve', 'number_format')
            ->where('status', '=', 0)->get();
        $reserves = Reserve::with(['main_reserve' => function($q) {
            $q->select('id', 'summa', 'id_currency');
        }])->get()->keyBy('id_currency')->toArray();

        $array = [];
        foreach ($response as $item)
        {
            $reserve_id = $reserves[$item->id] ?? '';
            if(empty($reserve_id)) {
                Arr::set($array, $item->id, 0);
                continue;
            }

            Arr::set($array, $item->id, (float)(float)$this->getReserveAmount($reserve_id, $item));
        }

        return $array;
    }

    /**
     * Компиляция и получение больших резервов
     *
     * @return array
     */
    public function compileBigReserve(): array
    {
        //array_chunk($reserves, (ceil(count($reserves)/3)))
        $response = Currency::select('id', 'status', 'max_display_reserve', 'number_format')
            ->where('status', '=', 0)->get();
        $reserves = Reserve::with(['main_reserve' => function($q) {
            $q->select('id', 'summa', 'id_currency');
        }])->get()->keyBy('id_currency')->toArray();

        $array = collect();
        foreach ($response as $item)
        {
            $reserve_id = $reserves[$item->id] ?? '';
            if(empty($reserve_id)) {
                $array->push([
                    'id' => $item->id,
                    'value' => 0
                ]);
                continue;
            }

            $array->push([
                'id' => $item->id,
                'value' => (float)$this->getReserveAmount($reserve_id, $item),
            ]);
        }

        return $array->all();
    }

    /**
     * Получение суммы резерва
     *
     * @param array $item
     * @param Currency|null $currency
     * @return float | integer
     */
    private function getReserveAmount(array $item, Currency $currency = null): float|int
    {
        // Если не создан резерв
        if(empty($item['main_reserve']) and empty($item['summa']))
            return 0;

        if(!is_null($currency['max_display_reserve']) and $currency['max_display_reserve'] > 0) {
            $summa = (!empty($item['main_reserve']) ? $item['main_reserve']['summa'] : $item['summa']);
            if($summa > $currency['max_display_reserve'])
                return $currency['max_display_reserve'];
        }

        return (!empty($item['main_reserve']) ? $item['main_reserve']['summa'] : $item['summa']);
    }
}
