<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 04.01.2020
 * Time: 17:51
 */

namespace App\Common\Packages\Crypto\Compilers\Concerns;


use App\Models\FileParserGroup;
use App\Models\FileParserRates;
use App\Models\PartnerParserGroup;
use App\Models\PartnerParserRates;
use Illuminate\Support\Facades\Http;

trait CompilersPartnerParser
{
    /**
     * Скомпилировать файл курсов
     *
     * @throws \Exception
     */
    protected function compilePartnerParser()
    {
        $this->partnerParserCourses();
    }

    /**
     * Курсы
     */
    private function partnerParserCourses()
    {
        PartnerParserGroup::where('status', '=', 1)->get()->map(function($item)
        {
            $data_url = config('crypto.partner_parsers')[$item->name];
            $response  = Http::get($data_url.'/api/v1/rates_partner')->json();


            if(count($response) > 0)
            {
                $this->command->line('* Обновление курсов партнера: '. $item->name);

                foreach ($response as $rates)
                {
                    $this->command->info('Партнер ['.$item->name.'] обновление пары '.$rates['name']);

                    PartnerParserRates::updateOrCreate([
                        'name' => $rates['name'],
                        'partner_id' => $item->id,
                        'id_group' => $rates['group']['id'],
                    ], [
                        'name' => $rates['name'],
                        'partner_id' => $item->id,
                        'id_group' => $rates['group']['id'],
                        'group_name' => $rates['group']['name'],
                        'rate' => $rates['value'],
                        'partner_type' => 'default',
                        'number_format' => $rates['number_format'],
                        'last_updated_at'   => $rates['last_updated_at']
                    ]);
                }
            }
        });
    }
}
