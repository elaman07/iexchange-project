<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.05.2018
 * Time: 16:27
 */

namespace App\Common\Packages\Crypto\Compilers\Concerns;


use Illuminate\Support\Carbon;

trait CompilersJson
{
    /**
     * Параметры JSON по умолчанию.
     *
     * @var int
     */
    private $encodingOptions = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT;

    /**
     * Скомпилировать JSON документ курсов
     *
     * @param null $custom
     * @return string
     */
    protected function compileJson()
    {
        return json_encode($this->document, $this->encodingOptions);
    }

    protected function compileJsonDymanic($data)
    {
        return json_encode($data, $this->encodingOptions);
    }
}
