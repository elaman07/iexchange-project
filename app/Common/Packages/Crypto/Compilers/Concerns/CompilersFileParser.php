<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 04.01.2020
 * Time: 17:51
 */

namespace App\Common\Packages\Crypto\Compilers\Concerns;


use App\Models\FileParserGroup;
use App\Models\FileParserRates;

trait CompilersFileParser
{
    /**
     * Скомпилировать файл курсов
     *
     * @throws \Exception
     */
    protected function compileFileParser()
    {
        $this->fileParserCourses();
    }

    /**
     * Курсы
     */
    private function fileParserCourses()
    {
        FileParserGroup::where('status', '=', 1)->get()->map(function($item)
        {
            $handle = trim(file_get_contents($item->link));
            $yield = collect(
                preg_split("/\r\n|\n|\r/", $handle)
            )->map(function($filter) {
                $filter_value = explode(':', $filter);
                return [
                    'name' => trim($filter_value[0]),
                    'rate' => trim($filter_value[1])
                ];
            })->pluck('rate','name')->toArray();

            $this->command->line('* Обновление курсов из файла: '. $item->name);

            $parsers = FileParserRates::where([
                ['id_group', '=', $item->id],
                ['status', '=', 1]
            ])->orderBy('type', 'asc')->get();


            foreach ($parsers as $parser) {
                $this->command->info('Файл '.$item->name.' обновление пары '.$parser->name);
                $find_parser = $yield[\Str::upper($parser->name)];

                // Если внутри файла есть разделение
                $spacing = preg_match('#\((.*?)\)#', $find_parser, $match);
                if(!empty($match) and isset($match[1])) {
                    $find_parser = iex_number_format((1 / $yield[\Str::upper($match[1])]), (int)iEXSetting('max_number_format_parser'));
                }

                if(empty($find_parser))
                    throw new \Exception('В файле '.$item->name.' не найдена пара '.$parser->name);

                //Обновление данных в базе
                FileParserRates::where('id', '=', $parser->id)
                    ->update([
                        'value' => 1,
                        'summa' => $find_parser
                    ]);
            }

        });
    }
}
