<?php
namespace App\Common\Packages\Crypto\Compilers\Concerns;


use App\Models\CompetitorLink;
use App\Models\CompetitorRates;
use App\Models\CompetitorRatesLog;
use Illuminate\Support\LazyCollection;

trait ComposersExchanger
{
    /**
     * Скомпилировать документ из курсов Контурентов
     * @throws \Exception
     */
    protected function compileExchanger()
    {
        $this->competitorCourses();
    }

    /**
     * Курсы конкурентов
    */
    private function competitorCourses()
    {
        $links = CompetitorLink::where('status', '=', 1)->get();

        foreach ($links as $link)
        {
            $handle = json_encode(simplexml_load_file($link->link));
            $yield = collect(json_decode($handle, true)['item']);

            $this->command->line('* Обновление курсов из источника конкурента: '. $link->name);

            $parsers = CompetitorRates::where([
                ['id_competitor', '=', $link->id],
                ['status', '=', 1]
            ])->orderBy('type', 'asc')->get();

            foreach ($parsers as $parser) {
                $this->command->info('Конкурент '.$link->name.' обновление пары '.$parser->name. '(тип '.$parser->type.')');

                $explode = explode('-', $parser->name);

                $find = $yield->filter(function($filter) use($explode) {
                    return $filter['from'] == trim($explode[0]) and $filter['to'] == trim($explode[1]);
                })->first();

                try {
                    if(iEXSetting('is_enabled_competitors_rounding')) {
                        $amount = (float)iex_number_format(
                            $parser->type == 0 ? 1 / $find['in'] : 1 * $find['out'], $parser->number_format
                        );
                    } else {
                        $amount = (float)($parser->type == 0 ? 1 / $find['in'] : iex_number_format(1 * $find['out'], $parser->number_format));
                    }

                    //Обновление данных в базе
                    $parser->update([
                        'value' => 1,
                        'summa' => (float)$amount
                    ]);

                    $this->writeToCompetitorLog($parser->id, 1, (float)$amount);
                }catch (\Exception $exception) {
                    $this->command->error('Name: '.$parser->name.' Error:'.$exception->getMessage().'---'. $exception->getLine());
                }
            }
        }
    }

    /**
     * Записываем в лог курсы
     *
     * @param $id
     * @param int $in
     * @param int $out
     */
    protected function writeToCompetitorLog($id, $in = 0, $out = 0)
    {
        if(!iEXSetting('record_course_history_competitors')) {
            return;
        }

        CompetitorRatesLog::create([
            'id_competitors' => $id,
            'in_price' => $in,
            'out_price' => $out
        ]);
    }
}
