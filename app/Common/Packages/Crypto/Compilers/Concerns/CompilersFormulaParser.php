<?php
namespace App\Common\Packages\Crypto\Compilers\Concerns;


use Carbon\Carbon;
use App\Models\{CompetitorRates,
    FileParserRates,
    ParserExchange,
    ParserFormulaCoefficient,
    ParserFormulaLog,
    ParserFormulaRates};

trait CompilersFormulaParser
{
    /**
     * Скомпилировать файл курсов
     *
     * @throws \Exception
     */
    protected function compileFormulaParser(): void
    {
        $this->formulaParserCourses();
    }

    /**
     * Курсы
     * @throws \Exception
     */
    private function formulaParserCourses(): void
    {
        /// Получение всех курсов из общего источника
        $default_parser_exchange = ParserExchange::whereNotNull('code')->pluck('summa_default', 'code')->toArray();
        // Получение курсов из типа "Курсы конкурентов"
        $default_competitor = CompetitorRates::whereNotNull('code')->pluck('summa', 'code')->toArray();
        // Получение курсов из типа "Курсы обмена из файлов"
        $default_file_rates = FileParserRates::whereNotNull('code')->pluck('summa', 'code')->toArray();
        /// Получение доп. коэффициентов
        $default_coefficients = ParserFormulaCoefficient::whereNotNull('alias')->pluck('summa', 'alias')->toArray();

        // Объединение массивов
        $default_rates = array_merge($default_parser_exchange, $default_competitor, $default_file_rates, $default_coefficients);
        $rates = ParserFormulaRates::where('status', '=', 1)->get();


        $this->logUpdate = [];
        foreach ($rates as $item) {
            $this->command->line('* Обновление курсов через формулу: '. $item->name);
            $result_summa = get_parser_formula_calculate($item->name, $default_rates);

            //Обновление данных в базе
            $item->update([
                'value' => 1,
                'summa' => (float)iex_number_format($result_summa, $item->number_format)
            ]);

            // Для лога
            $this->logUpdate[] = [
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                'id_parser_formula' => $item->id,
                'amount' => $item->summa,
                'formula' => $item->name,
                'name' => $item->title
            ];
        }
        if((int)iEXSetting('record_course_formula_history') == 1)
        {
            ParserFormulaLog::insert($this->logUpdate);
        }
    }
}
