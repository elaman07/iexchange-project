<?php
namespace App\Common\Packages\Crypto\Compilers\Concerns;


use App\Common\Packages\Crypto\BestchangeHandler;
use App\Jobs\TelegramErrorBCPairJob;
use App\Models\BestchangeParserError;
use App\Models\BestchangeRatesLog;
use App\Models\BestChangeRatesModel;
use App\Models\DirectionExchange;
use GuzzleHttp\{
    Client as GuzzleClient,
};
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Psr\Http\Message\ResponseInterface;
use ZipArchive;

trait CompilersBestChange
{
    /**
     * Обработчик Bestchange
     *
     * @var BestchangeHandler
    */
    protected $bestchange;

    /**
     * Скомпилировать документ курсов из BestChange
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function compileBestChange()
    {
        $this->download();
        $this->unZip();
        $this->updateCourses();
    }

    /**
     * Обновление курсов
     * @throws \Exception
     */
    public function updateCourses()
    {
        $this->bestchange = new BestchangeHandler();
        $exchangers = $this->bestchange->getExchangers();

        $parsers = BestChangeRatesModel::with(['direction_first'])
            ->whereHas('direction_first', function ($query) {
                return $query->where('status', '=', 1);
            })
            ->where('status', '=', 1)
            ->cursor();

        foreach ($parsers as $item)
        {
            // Если нет направлений, пропускаем
            if(!isset($item->direction_first))
                continue;

            try {
                $filtered = $this->filtered($item, $item->direction_first);

                // Получаем позицию
                $position = $this->getPosition($item->direction_first);

                // Получаем последнюю позицию
                $response = Arr::last($filtered);
                // Если в списке больше данных, получаем их
                if(count($filtered) > 0 and !empty($filtered[$position]))
                    $response = $filtered[$position];

                //Результат внутреннего парсинга
                if((int)iEXSetting('is_enabled_bestchange_rounding') == 1)
                {
                    // Автоматическая корректировка
                    $amount = iex_number_format(
                        ($response['rate_give'] == 1) ? (1 * $response['rate_receiver']) : 1 / $response['rate_give'],
                        $item->number_format
                    );
                } else {
                    $amount = ($response['rate_give'] == 1 ? iex_number_format(1 * $response['rate_receiver'], $item->number_format) : 1 / $response['rate_give']);
                }

                //Обновление данных в базе
                $item->update([
                    'value' => 1,
                    'summa' => $amount,
                    'source_name' => $exchangers[$response['exchanger_id']] ?? null
                ]);

                // Если найден парсер у которого был сбой, сбрасываем
                $item->direction_exchange()->where('is_disable_bs_error', 1)
                    ->orWhere('is_enable_alt_bs_parser', 1)->update([
                        'is_disable_bs_error' => 0,
                        'is_enable_alt_bs_parser' => 0
                    ]);
                $item->update(['is_not_pair' => 0]);

                $this->writeToBestchangeLog($item->id, 1, $amount);
                $this->command->info('Bestchange: Успешное обновление пары '.$item->name. '(курс 1 - '.iex_number_format($item->summa, $item->number_format, true).')');

            }catch (\Exception $exception)
            {
                $item->direction_first->update([
                    'is_enable_alt_bs_parser' => 1,
                    'is_disable_bs_error' => 1
                ]);
                $this->command->error('Bestchange: Не удалось обновить пару '.$item->name);
                $item->update(['is_not_pair' => 1]);
                if((int)iEXSetting('is_enable_bs_log_error') == 1)
                {
                    BestchangeParserError::create([
                        'id_direction_exchange' =>  $item->direction_first->id,
                        'id_bestchange' => $item->id,
                        'status' => 1,
                        'description' => 'Не найден курс по направлению '.$item->name
                    ]);
                }

                // Отправляем уведомление, о недоступности курса по направлению
                if((int)iEXSetting('is_enable_bs_undefined_pair') == 1)
                {
                    $delay = now()->addSeconds(10);
                    dispatch(new TelegramErrorBCPairJob(
                        direction_name($item->direction_first)
                    ))->delay($delay)->onQueue('low');
                }
            }
        }

       $this->command->info('Bestchange: Обновление пар успешно завершено');
       $this->command->info('');
    }

    /**
     * Получаем отфильтрованные данные из bestchange
     *
     * @param $item
     * @param DirectionExchange $exchange
     * @return array
     * @throws \Exception
     */
    protected function filtered($item, DirectionExchange $exchange)
    {
        $collect = collect($this->bestchange->setRatesPair([$item['exchanger_id1'], $item['exchanger_id2']]))
            ->reject(function($item) {
                return empty($item);
            })->filter(function($item) use($exchange) {
                return $item['city_id'] == $exchange->bestchange_city;
            })->filter(function($item) use($exchange) {
                return $item['reserve'] >= (empty($exchange->bestchange_min_reserve) ? 0 : $exchange->bestchange_min_reserve);
            })->filter(function($item) {
                return !in_array($item['exchanger_id'], $this->getBestChangeBlackList());
            })->filter(function($item) use($exchange) {
                return !in_array($item['exchanger_id'], explode(',', $exchange->bestchange_bl));
            })->reject(function($item) use($exchange) {
                return !empty($exchange->bestchange_wl) and !in_array($item['exchanger_id'], explode(',', $exchange->bestchange_wl));
            })->reject(function($item) use($exchange) {
                return !empty($exchange->bestchange_max_reserve) and $item['reserve'] > $exchange->bestchange_max_reserve;
            });

        // Автоматическое определение сортировки
        $sorting = $collect->sortBy('rate'); // Сортировка по убыванию

        $response = [];
        $key = 1;
        foreach ($sorting->values() as $value) {
            $response[$key++] = $value;
        }

        return $response;
    }

    /**
     * Получение черного списка обменников из BestChange
     *
     * @return array
    */
    public function getBestChangeBlackList(): array
    {
        return explode(',', iEXSetting('bestchange_exchange_black_list'));
    }

    /**
     * Получаем позицию из которого необходимо парсить данные
     *
     * @param DirectionExchange|null $exchange
     * @return integer
     */
    protected function getPosition(DirectionExchange $exchange = null): int
    {
        if(!isset($exchange))
            return iEXSetting('bestchange_position', 10);

        // Включаем поиск по диапазон
        if($exchange->is_change_bestchange_range == 1 and !empty($exchange->bestchange_range))
        {
            //  Если пусто все
            if(empty($exchange->bestchange_range_from) and empty($exchange->bestchange_range_to))
            {
                $range = explode('-', $exchange->bestchange_range);
                $exchange->update([
                    'bestchange_position' => rand($range[0], $range[1])
                ]);
            } elseif (Carbon::now()->format('H:i') > $exchange->bestchange_range_from and Carbon::now()->format('H:i') < $exchange->bestchange_range_to)
            {
                $range = explode('-', $exchange->bestchange_range);
                $exchange->update([
                    'bestchange_position' => rand($range[0], $range[1])
                ]);
            }
        }

        if($exchange->bestchange_position > 0)
            return $exchange->bestchange_position;

        return iEXSetting('bestchange_position', 10);
    }

    /**
     * Распаковка скачанных файлов bestchange
     *
     * @throws \Exception
     */
    private function unZip(): void
    {
        $zip = new \ZipArchive();
        if ($zip->open(config('bestchange.bestchange_zip'), ) === true) {
            $zip->extractTo(config('bestchange.bestchange_repository'));
            $zip->close();
        } else {
            throw new \Exception('Курсы не найдены');
        }
    }


    /**
     * Скачивание курсов
     *
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function download(): ResponseInterface
    {
        $server = 'http://api.bestchange.ru/info.zip';
        if(iEXSetting('public_server_bestchange') == 1)
            $server = 'http://api.bestchange.net/info.zip';

        $client = new GuzzleClient();
        return $client->get($server, [
            'sink' => config('bestchange.bestchange_zip'),
            'timeout' => iEXSetting('bestchange_timeout', 30), // Response timeout
            'connect_timeout' => iEXSetting('bestchange_timeout', 30), // Connection timeout
        ]);
    }

    /**
     * Записываем в лог курсы
     *
     * @param int $id
     * @param int $in
     * @param int $out
     */
    protected function writeToBestchangeLog(int $id, int $in = 0, int $out = 0): void
    {
        if(!iEXSetting('record_course_history_bestchange')) {
            return;
        }

        BestchangeRatesLog::create([
            'id_bestchange_rates' => $id,
            'in_price' => $in,
            'out_price' => $out
        ]);
    }
}
