<?php
namespace App\Common\Packages\Crypto\Compilers\Concerns;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Http\Resources\Rates\PaymentSystemsResource;
use App\Models\{Currency,
    CurrencyGroup,
    CurrencyNetwork,
    DirectionExchange,
    DirectionExchangeCity,
    DirectionField,
    DirectionTemplate,
    FilterCurrency,
    ParserExchange};
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use JetBrains\PhpStorm\ArrayShape;
use Modules\Cities\Entities\CitiesModel;
use function Sodium\add;

trait CompilersConditionals
{
    /**
     * Основная валюта "Отдаю"
     *
     * @var integer
     */
    protected int $incomePaymentSystemId = 0;

    /**
     * Основная валюта "Получаю"
     *
     * @var integer
     */
    protected int $outcomePaymentSystemId = 0;

    /**
     * Компиляция списка валют
     *
     * @return PaymentSystemsResource
    */
    public function compilerCurrency(): PaymentSystemsResource
    {
        $currencies = Currency::with(['payment', 'code_currency', 'commands', 'currency_in_fields', 'currency_out_fields', 'currency_labels'
        ])->where('status', '=', 0)->get();

        $this->currency_data = $currencies;

        return new PaymentSystemsResource($currencies);
    }

    /**
     * Компилятор курсов для создания
     *
     * @return array
    */
    #[ArrayShape(['app_version' => "float", 'created_at' => "string", 'dr_snapshot_id' => "mixed", 'direction_rates' => "array", 'reserves' => "array", 'reserves_view' => "array", 'filters' => "array", 'groups' => "array", "reserves_big" => "array"])]
    public function compileCsCreated(): array
    {
        $reserves = $this->compileReserve();
        $reservesView = $reserves;
        if(!empty(iEXSetting('ids_currencies_reserves_home_view')))
        {
            $reservesView = collect($reserves)->reject(function($value, $key) {
                return in_array($key, explode(',', iEXSetting('ids_currencies_reserves_home_view')));
            })->toArray();
        }

        if(iEXSetting('template_block_reserve') == 2)
        {
            $reservesBigView = $this->compileBigReserve();

            if(!empty(iEXSetting('ids_currencies_reserves_home_view')))
            {
                $reservesBigView = collect($reservesBigView)->reject(function($item) {
                    return in_array($item['id'], explode(',', iEXSetting('ids_currencies_reserves_home_view')));
                })->toArray();
            }

            $reservesBigView = array_chunk($reservesBigView, (ceil(count($reservesBigView) / (int)iEXSetting('count_template_block_reserve_2', 3))));
        }


        return [
            'app_version' => $this->getVersion(),
            'created_at' => $this->getCreatedAt(),
            'dr_snapshot_id' => \Cache::get('dr_snapshot_id'),
            'direction_rates' => $this->compileDirectionRates(),
            'reserves' => $reserves,
            'reserves_view' => $reservesView,
            'reserves_big' => $reservesBigView ?? [],
            'filters' => $this->compileFilters(),
            'groups' => $this->compileGroups()
        ];
    }

    /**
     * Компилятор направлений
    */
    protected function compileDirectionRates(): array
    {
        // Получаем шаблоны направлений
        $templates_directions = \Cache::remember('compiler-conditionals-directioin-template', Carbon::now()->addSeconds(30), function() {
            return DirectionTemplate::all();
        });

        $default_parser_courses_usd = [];
        if(iEXSetting('is_enabled_convert_exchange_to_usd') == 1) {
            $default_parser_courses_usd = ParserExchange::where('status', '=', 1)
                ->orderBy('updated_at', 'asc')->pluck('summa', 'name')->reject(function($item) {
                    return $item <= 0;
                })->toArray();
        }

        $currencies_data = \Cache::remember('compiler-conditionals-currency-data', Carbon::now()->addSeconds(15), function() {
            return Currency::with(['payment', 'code_currency'])->get();
        });

        $lists_out = \Cache::remember('compiler-conditionals-direction-data',  Carbon::now()->addSeconds(15), function() {
            // Получение статусов режимов работ
            $modes_count = [];
            if((int)iEXSetting('is_enabled_module_direction_mode') == 1) {
                $modes_count = \App\Models\DirectionExchangeMode::where('status', 1)->first();
            }

            $direction_exchange = DirectionExchange::with([
                'direction_exchange_percentage_amount',
                'direction_exchange_cities',
                'direction_field_sorting'
            ])->active();

            if(isset($modes_count->direction_exchange) and (int)iEXSetting('is_enabled_module_direction_mode') == 1)
            {
                $response = $direction_exchange->whereHas('direction_modes', function ($query) {
                    return $query->where('status', 1);
                })->cursor();
            } else {
                if (iEXSetting('is_visible_disabled_directions')) {
                    $response = $direction_exchange->get();
                } else {
                    $response = $direction_exchange->where('status', '=', 1)->get();
                }
            }

            $response_callback = $response->filter(function($filter)
            {
                if($filter->is_enabled_exchange == 0) return $filter;

                // Отображаем направелния по расписанию
                if($filter->is_enabled_exchange == 1 and
                    Carbon::now()->format('H:i') > $filter->from_on_time and
                    Carbon::now()->format('H:i') < $filter->to_on_time)
                    return $filter;
            });

            if($this->is_telegram_bot == 1)
            {
                $response_callback = $response_callback->filter(function($item) {
                    if($this->is_telegram_bot == 1)
                        return $item->is_allow_telegram_bot == 0;
                    return $item;
                });
            }

            return $response_callback->reject(function($item) {
                return $item->is_hidden_not_locale == 1 and !empty($item->languages) and !in_array(app()->getLocale(), explode(',', $item->languages));
            })->reject(function($item) {
                return $item->is_hidden_not_device == 1 and !empty($item->device) and !in_array($this->getUserDevice(), explode(',', $item->device));
            })->reject(function($item) {
                return $item->is_hidden_noauth_user == 1 and !auth()->check();
            });
        });

        $list_in = \Cache::remember('compiler-conditionals-direction-in', Carbon::now()->addSeconds(15), function() {
            return $this->getListIn();
        });

        $income_data = [];
        foreach ($list_in as $in)
        {
            $outcome_data = [];
            $filter_out = $lists_out->filter(function($item) use($in){
                return ($item->id_currency1 == $in->id) and $item->is_error_rate == 0;
            });

            foreach ($filter_out as $item)
            {
                // In Currency
                $in_currency = $currencies_data->filter(fn($fn) => $fn->id == $item->id_currency1)->first();
                // Out Currency
                $out_currency = $currencies_data->filter(fn($fn) => $fn->id == $item->id_currency2)->first();

                if(empty($in_currency) or empty($out_currency)) {
                    continue;
                }

                $status = $item->status;
                // Ограничиваем активность валюты, в зависимости от резерва
                if($in_currency->max_limit_in_reserve > 0 and iex_reserve_amount($in_currency->reserve) > $in_currency->max_limit_in_reserve)
                    $status = 0;

                // Вытаскиваем основные направления
                if($item->is_main == 1) {
                    $this->setIncomePaymentSystemId($item->id_currency1);
                    $this->setOutcomePaymentSystemId($item->id_currency2);
                }

                //$custom_fields = $item->direction_field->where('status', '=', 0)->sortBy('sorting');
                $custom_fields = $item->direction_field_sorting;

                $direction_fields = [];
                if(count($custom_fields) > 0) {
                    foreach ($custom_fields as $relationship)
                    {
                        $direction_fields[] = [
                            'key' => $relationship->key_id,
                            'type' => 'input',
                            'templateOptions' => [
                                'label' => $relationship->name,
                                'placeholder' => $relationship->name,
                                'required' => (bool)$relationship->obligatory_field == 0,
                                'appearance' => 'fill'
                            ],
                        ];
                    }
                }

                // Если есть курсы с городами, сделаем серверным
                $r = [
                    'amount' => $item->course_value,
                    'curs'   => $item->exchange_rate,
                    'cur_str' => $item->exchange_rate_str
                ];

                $cities_count = 0;
                if(\Module::find('Cities')->isEnabled() == 1) {
                    $cities_count = $item->direction_exchange_cities->count();
                }

                if(\Module::find('Cities')->isEnabled() == 1 and $cities_count > 0) {
                    $r = ConvertFacade::call($item)->setCurrencyIn($in_currency)->setCurrencyOut($out_currency)->all();
                }

                if($item->direction_exchange_percentage_amount->count() > 0) {
                    $r = ConvertFacade::call($item)->setCurrencyIn($in_currency)->setCurrencyOut($out_currency)->all();
                }

                $arr_direction = [
                    'id' => $item->id,
                    // 'net' => $item->id_network,
                    'in_id' => $item->id_currency1,
                    'out_id' => $item->id_currency2,
                    's' => $status,
                    'r' =>  $r,
                    'min_in' => $item->min_price1,
                    'max_in' => $item->max_price1,
                    'min_out' => $item->min_price2,
                    'max_out' => $item->max_price2,
                ];
                if($item->is_enable_user_discount == 1) {
                    $arr_direction['is_user_discount'] = 1;
                }
                if($item->oth_comm_percent > 0) {
                    $arr_direction['oth_comm1_percent'] = $item->oth_comm_percent;
                }
                if($item->oth_comm_currency > 0) {
                    $arr_direction['oth_comm1_currency'] = $item->oth_comm_currency;
                }

                if($item->oth_min_comm > 0) {
                    $arr_direction['oth_min1_comm'] = $item->oth_min_comm;
                }

                if($item->oth_min2_comm > 0) {
                    $arr_direction['oth_min2_comm'] = $item->oth_min2_comm;
                }

                if($item->oth_comm2_percent > 0) {
                    $arr_direction['oth_comm2_percent'] = $item->oth_comm2_percent;
                }
                if($item->oth_comm2_currency > 0) {
                    $arr_direction['oth_comm2_currency'] = $item->oth_comm2_currency;
                }

                //
                if($item->pay_comm_percent > 0) {
                    $arr_direction['pay_comm1_percent'] = $item->pay_comm_percent;
                }
                if($item->pay_comm_currency > 0) {
                    $arr_direction['pay_comm1_currency'] = $item->pay_comm_currency;
                }

                if($item->pay_min_comm > 0) {
                    $arr_direction['pay_min1_comm'] = $item->pay_min_comm;
                }

                if($item->pay_comm2_percent > 0) {
                    $arr_direction['pay_comm2_percent'] = $item->pay_comm2_percent;
                }

                if($item->pay_comm2_currency > 0) {
                    $arr_direction['pay_comm2_currency'] = $item->pay_comm2_currency;
                }
                if($item->pay_min2_comm > 0) {
                    $arr_direction['pay_min2_comm'] = $item->pay_min2_comm;
                }

                if(\Module::find('Cities')->isEnabled() == 1 and $cities_count > 0)
                {
                    $arr_direction['prices'] = [];
                    foreach ($item->direction_exchange_cities as $city_price) {
                        $arr_direction['prices'][$city_price['id']] = [
                            'min_price' => $city_price['min_price'],
                            'max_price' => $city_price['max_price']
                        ];
                    }
                }

                if(iEXSetting('is_enabled_convert_exchange_to_usd') == 1)
                {
                    $rate_in_name = sprintf('%s - USD', $in_currency->code_currency->name);
                    $rate_out_name = sprintf('%s - USD', $out_currency->code_currency->name);

                    $arr_direction['riu'] = $default_parser_courses_usd[$rate_in_name] ?? 0;
                    $arr_direction['rou'] = $default_parser_courses_usd[$rate_out_name] ?? 0;
                }


                // Показывать дату последнего обмена
                if((int)iEXSetting('is_visible_direction_order_at') == 1) {
                    $arr_direction['last_order_at'] = !is_null($item->last_order_at) ? Carbon::parse($item->last_order_at)->diffForHumans() : '';
                }

                if(count($direction_fields) > 0) {
                    $arr_direction['fields'] = $direction_fields;
                }

                // Описание обмена
                $templates_desc_exchange = $templates_directions->first(function($item) {
                    return $item->id_type == 1;
                });

                // Дополнительное описание обмена
                $templates_desc_exchange_dop = $templates_directions->first(function($item) {
                    return $item->id_type == 2;
                });

                // Формальное описание перед вводом данных
                $templates_formalization_text = $templates_directions->first(function($item) {
                    return $item->id_type == 3;
                });

                // Инструкция по оплате
                $desc_exchange = $item->desc_exchange;
                if(!empty($templates_desc_exchange))
                {
                    if($templates_desc_exchange->type_view_info == 1) {
                        $desc_exchange = $templates_desc_exchange->text;
                    }elseif($templates_desc_exchange->type_view_info == 2 and empty($item->desc_exchange)) {
                        $desc_exchange = $templates_desc_exchange->text;
                    }
                }

                // Инструкция по оплате
                $desc_exchange_dop = $item->desc_exchange_dop;
                if(!empty($templates_desc_exchange_dop))
                {
                    if($templates_desc_exchange_dop->type_view_info == 1) {
                        $desc_exchange_dop = $templates_desc_exchange_dop->text;
                    }elseif($templates_desc_exchange_dop->type_view_info == 2 and empty($item->desc_exchange_dop)) {
                        $desc_exchange_dop = $templates_desc_exchange_dop->text;
                    }
                }

                // Формальное описание перед вводом данных
                $formalization_text = $item->formalization_text;
                if(!empty($templates_formalization_text))
                {
                    if($templates_formalization_text->type_view_info == 1) {
                        $formalization_text = $templates_formalization_text->text;
                    }elseif($templates_formalization_text->type_view_info == 2 and empty($item->formalization_text)) {
                        $formalization_text = $templates_formalization_text->text;
                    }
                }


                if(!empty($desc_exchange)) {
                    $arr_direction['desc_exchange'] = $desc_exchange;
                }
                if(!empty($desc_exchange_dop)) {
                    $arr_direction['desc_exchange_dop'] = $desc_exchange_dop;
                }
                if(!empty($formalization_text)) {
                    $arr_direction['formalization_text'] = $formalization_text;
                }


                // Кратность суммы обмена
                if($item->multiplicity_type > 0) {
                    $arr_direction['multiplicity'] = [
                        'multiplicity_type' => $item->multiplicity_type,
                        'multiplicity_amount'   => $item->multiplicity_amount,
                        'multiplicity_comment'  => $item->multiplicity_comment
                    ];
                }

//                $networks = [];
//                if($item->direction_network->count() > 0)
//                {
//                    $networks[] = [
//                        'key' => 'network_id',
//                        'type' => 'radio',
//                        'templateOptions' => [
//                            'label' => __('ticker.network.input'),
//                            'options' => $item->direction_network->map(function ($item) {
//                                return [
//                                    'label' => $item->name,
//                                    'value' => $item->id
//                                ];
//                            })
//                        ],
//                    ];
//                }
//
//                $arr_direction['network'] = $networks;


                $direction_exchange_amount = [];
                if($item->direction_exchange_percentage_amount->count() > 0)
                {
                    foreach ($item->direction_exchange_percentage_amount as $percentage_amount)
                    {
                        // Не пропускаем 0 значения
                        if((float)$percentage_amount->amount == 0)
                            continue;

                        $direction_exchange_amount[] = [
                            'value' => $percentage_amount->id,
                            'label' => (float)$percentage_amount->amount
                        ];
                    }

                    $arr_direction['is_notify_exchange_amount'] = $item->is_notify_exchange_amount;
                    $arr_direction['exchange_amount'] = $direction_exchange_amount;
                }

                if(\Module::find('Cities')->isEnabled() == 1)
                {
                    $direction_countries = [];
                    $direction_cities = [];
                    $direction_country_default = 0;
                    if($cities_count > 0)
                    {
                        $group_countries = DirectionExchangeCity::with('geo_country')->where('id_direction_exchange', $item->id)->groupBy('id_country')
                            ->get()->reject(function($item) {
                                return $item->id_country == 0;
                            });

                        $country_options = [];
                        foreach ($group_countries as $country) {
                            $country_options[] = [
                                'value' => $country->id_country,
                                'label' => isset($country->geo_country) ? $country->geo_country->value: ''
                            ];
                        }

                        $direction_country_default = $country_options[0]['value'];

                        $direction_countries[] = [
                            'key' => 'country_id',
                            'type' => 'select',
                            'templateOptions' => [
                                'label' => __('Страна'),
                                'options' => $country_options,
                                'appearance' => 'fill'
                            ],
                        ];

                        $cities_list = $item->direction_exchange_cities->reject(function($value) {
                            return $value->id_country == 0;
                        });

                        foreach ($cities_list as $city) {
                            $direction_cities[] = [
                                'id_country' => $city->id_country,
                                'value' => $city->id,
                                'label' => $city->city->name
                            ];
                        }
                    }

                    $arr_direction['country_default'] = $direction_country_default;
                    $arr_direction['countries'] = $direction_countries;
                    $arr_direction['cities'] = $direction_cities;
                }

                Arr::set($outcome_data, $item->id_currency2, $arr_direction);
            }

            Arr::set($income_data, $in->id, $outcome_data);
        }

        return $income_data;
    }

    /**
     * Получение списка фильтров
     *
     * @return array
     */
    protected function compileFilters(): array
    {
        $filters = FilterCurrency::orderBy('sorting')->cursor()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        });
        return $filters->all();
    }

    /**
     * Получение списка групп
     *
     * @return array
     */
    protected function compileGroups(): array
    {
        $filters = CurrencyGroup::orderBy('sorting')->cursor()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        });
        return $filters->all();
    }

    /**
     * Получение списка сетей
     *
     * @return array
     */
    protected function compileNetworks(): array
    {
        $filters = CurrencyNetwork::orderBy('id')->cursor()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->name
            ];
        });
        return $filters->all();
    }

    /**
     * Установка нового ID для валюты "Отдаю"
     *
     * @param int $value
     * @return void
     */
    public function setIncomePaymentSystemId(int $value = 0): void
    {
        $this->incomePaymentSystemId = $value;
    }

    /**
     * Установка нового ID для валюты "Получаю"
     *
     * @param int $value
     * @return void
     */
    public function setOutcomePaymentSystemId(int $value = 0): void
    {
        $this->outcomePaymentSystemId = $value;
    }
}
