<?php
namespace App\Common\Packages\Crypto\Compilers;

use App\Models\Currency;
use App\Models\DirectionExchange;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redis;
use InvalidArgumentException;
use Jenssegers\Agent\Facades\Agent;

class Compiler
{
    use Concerns\CompilersManager,
        Concerns\CompilersJson,
        Concerns\CompilersReserves,
        Concerns\CompilersBestChange,
        Concerns\ComposersExchanger,
        Concerns\CompilersCrypto,
        Concerns\CompilersConditionals,
        Concerns\CompilersFileParser,
        Concerns\CompilersPartnerParser,
        Concerns\CompilersFormulaParser;


    protected string $redis_tag_add = 'cs-initial';
    protected string $redis_tag_update = 'cs-update';
    /**
     * Конфигурационный файл
     *
     * @var array
     */
    protected $config;

    /**
     * Экземпляр файловой системы.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Массив для сбора данных
     *
     * @var Collection
     */
    protected $document = [];

    /**
     * Массив для сбора данных для обновления
     *
     * @var Collection
     */
    protected $document_update = [];

    /**
     * Консольная команда
     *
     * @return Command
    */
    public $command;

    /**
     * Все доступные функции компилятора.
     *
     * @var array
     */
    protected $compilers = [
        'In',
        'Out'
    ];

    protected int $is_telegram_bot = 0;

    protected string $locale = 'ru';

    protected $currency_data;

    /**
     * Создайте новый экземпляр компилятора.
     *
     * @param  \Illuminate\Filesystem\Filesystem $files
     * @param $repository
     */
    public function __construct(Filesystem $files, $repository)
    {
        $this->files = $files;
        $this->config = $repository;
        $this->document = collect();

        if(empty($repository)) {
            throw new InvalidArgumentException('Не найдено конфигурация компилятора');
        }
    }

    public function setLocale(string $locale): string
    {
        $this->locale = \Str::lower($locale);
        return $this->locale;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setCurrencyData($data)
    {
        $this->currency_data = $data;
        return $this;
    }

    /**
     * Полная компиляция документа
     *
     * @return Compiler
     * @throws \Exception
     */
    public function compile(string $locale = null)
    {
        $currencies = $this->compilerCurrency();
        $cs = $this->compileCsCreated();

        $this->document = collect();
        $this->set('locale', app()->getLocale());
        $this->set('layout', 'default');
        $this->set('cs', $cs);

        //  Если не выбрано направление по умолчанию
        if($this->incomePaymentSystemId == 0)
        {
            $find = DirectionExchange::select('id_currency1', 'id_currency2')->where('status', '=', 1)->where('is_main', '=', 1)->first();
            if(!empty($find)) {
                $this->incomePaymentSystemId = $find->id_currency1;
                $this->outcomePaymentSystemId = $find->id_currency2;
            } else {
                $find_one = DirectionExchange::select('id_currency1', 'id_currency2')->where('status', '=', 1)->first();
                $this->incomePaymentSystemId = $find_one->id_currency1;
                $this->outcomePaymentSystemId = $find_one->id_currency2;
            }
        }

        $this->set('initialDirection', [
            'incomePaymentSystemId' => $this->incomePaymentSystemId,
            'outcomePaymentSystemId' => $this->outcomePaymentSystemId
        ]);
        $this->set('paymentSystems', $currencies);
        return $this;
    }

    /**
     * Визуальное обновление документа
     *
     * @return Compiler
     * @throws \Exception
     */
    public function update(string $locale = null)
    {
        if(!is_null($locale)) {
            $this->setLocale($locale);
            app()->setLocale($this->getLocale());
        }

        // Сайт не отключаем, если клиент оплачивает заявку
        if(iEXSetting('service_break') == 1)
        {
            $start_time = Carbon::now();
            $lead_time = Carbon::parse(iEXSetting('service_break_interval'));
            if($lead_time->gt($start_time))
                $service_break = $lead_time->diffInSeconds($start_time);
            else
                iEXSetting(['service_break' =>  0]);

            $site_technical_break = ($service_break ?? 0);
        }else {
            $site_technical_break = 0;
        }

        $reserves = $this->compileReserve();
        $reservesView = $reserves;
        if(!empty(iEXSetting('ids_currencies_reserves_home_view'))) {
            $reservesView = collect($this->compileReserve())->reject(function($value, $key) {
                return in_array($value, explode(',', iEXSetting('ids_currencies_reserves_home_view')));
            });
        }


        $this->document = collect();
        $this->set('app_version', $this->getVersion());
        $this->set('created_at', $this->getCreatedAt());
        $this->set('dr_snapshot_id', \Cache::get('dr_snapshot_id'));
        $this->set('site_technical_break', $site_technical_break);
        $this->set('site_showdown', isJobOffline());
        $this->set('direction_rates', $this->compileDirectionRates());
        $this->set('reserves', $reserves);
        $this->set('reserves_view', $reservesView);

        return $this;
    }

    /**
     * Мультиязычность
     *
     * @param string $type_saved
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *
     * @return Compiler
     */
    public function compileLang(string $type_saved = 'redis')
    {
        $langs = ['ru', 'en'];

        $currencies = $this->compilerCurrency();

        //  Если не выбрано направление по умолчанию
        if($this->incomePaymentSystemId == 0)
        {
            $find = DirectionExchange::select('id_currency1', 'id_currency2')->where('status', '=', 1)->where('is_main', '=', 1)->first();
            if(!empty($find)) {
                $this->incomePaymentSystemId = $find->id_currency1;
                $this->outcomePaymentSystemId = $find->id_currency2;
            } else {
                $find_one = DirectionExchange::select('id_currency1', 'id_currency2')->where('status', '=', 1)->first();
                $this->incomePaymentSystemId = $find_one->id_currency1;
                $this->outcomePaymentSystemId = $find_one->id_currency2;
            }
        }

        foreach ($langs as $lang)
        {
            $this->document = collect();
            if(!is_null($lang)) {
                $this->setLocale($lang);
                app()->setLocale($lang);
            }

            $cs = $this->compileCsCreated();

            $this->set('locale', $lang);
            $this->set('layout', 'default');
            $this->set('cs', $cs);
            $this->set('paymentSystems', $currencies);

            $this->set('initialDirection', [
                'incomePaymentSystemId' => $this->incomePaymentSystemId,
                'outcomePaymentSystemId' => $this->outcomePaymentSystemId
            ]);

            if($type_saved == 'redis') {
                $this->addToRedisMulti($lang);
            } elseif($type_saved == 'file') {
                $this->toFileMulti($lang);
            }

            // IS Update
            $this->updateLang($lang, $cs, $type_saved);

        }

        return $this;
    }

    /**
     * Визуальное обновление документа
     *
     * @param $lang
     * @param $cs
     * @param $type_saved
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function updateLang($lang, $cs, $type_saved): void
    {
        // Сайт не отключаем, если клиент оплачивает заявку
        if(iEXSetting('service_break') == 1)
        {
            $start_time = Carbon::now();
            $lead_time = Carbon::parse(iEXSetting('service_break_interval'));
            if($lead_time->gt($start_time))
                $service_break = $lead_time->diffInSeconds($start_time);
            else
                iEXSetting(['service_break' =>  0]);

            $site_technical_break = ($service_break ?? 0);
        }else {
            $site_technical_break = 0;
        }

        $this->document = collect();
        $this->set('component', 'ExchangePage');
        $this->set('created_at', $this->getCreatedAt());
        $this->set('dr_snapshot_id', \Cache::get('dr_snapshot_id'));
        $this->set('site_technical_break', $site_technical_break);
        $this->set('site_showdown', isJobOffline());
        $this->set('direction_rates', $cs['direction_rates']);
        $this->set('reserves', $cs['reserves']);
        $this->set('reserves_view', $cs['reserves_view']);
        $this->set('reserves_big', $cs['reserves_big']);

        if($type_saved == 'redis') {
            $this->addToCacheRedisMulti($lang);
        }elseif($type_saved == 'file') {
            $this->toCacheFileMulti($lang);
        }
    }

    public function consoleBestChange(Command $command)
    {
        $this->command = $command;
        if((int)iEXSetting('is_enable_parser_bs_console') == 1)
        {
            $this->command->line('* Обновление курсов из источника: BestChange');
            $this->compileBestChange();
        }
    }

    /**
     * Запуск общего компилятора курсов
     *
     * @param Command $command
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function console(Command $command)
    {
        $this->command = $command;
        $this->compileCrypto();
        $this->compileExchanger();
        $this->compileFileParser();
        $this->compilePartnerParser();
        $this->compileFormulaParser();


        // Проверяем лог ошибок
        \DB::table('direction_exchange_error_log')->truncate();
        // Обновление курсов
        DirectionExchange::active()->where('status', 1)->chunk(100, function($exchange)
        {
                $exchange->each(function($item)
                {
                    $update = [];
                    try {
                        $this->command->info(direction_name($item));
                        $call = \Converter::call($item)->in();
                        $update['course_value'] = $call->getAmount();
                        $update['exchange_rate'] = $call->getCourse();
                        $update['exchange_rate_str'] = $call->getCourseStr();
                        $update['is_error_rate'] = 0;
                    }catch (\Exception $exception) {
                        $update['is_error_rate'] = 1;
                        $update['error_rate_text'] = $exception->getMessage();
                        $this->command->error(direction_name($item).' Type: Exception, Message: '. $exception->getMessage());
                    }catch (\DivisionByZeroError|\Error $exception) {
                        $text_error = direction_name($item).' Type: Exception, Message: '. $exception->getMessage();
                        $update['is_error_rate'] = 1;
                        $update['error_rate_text'] = $exception->getMessage();
                        $this->command->error($text_error);
                    }

                    $update['parser_source_name'] = $this->getParserSourceName($item);
                    $item->update($update);
                });
            });

    }

    private function getParserSourceName(DirectionExchange $item)
    {
        // BestChange
        if(isset($item->id_bestchange_rates) and $item->enable_bestchange == 1 and $item->is_disable_bs_error == 0)
        {
            if($item->is_enable_alt_bs_parser == 1 and isset($item->bs_alt_parser->group_parse_exchange)) {
                return '(BestChange) - '.$item->bs_alt_parser->group_parse_exchange->name;
            }

            return 'BestChange';
        }

        if($item->id_file_parser_rate > 0) {
            return $item->file_parser_rates->file_parser_group->name;
        }

        if($item->id_competitor > 0) {
            return $item->competitor_rates->competitor_link->name;
        }

        if($item->id_parser_formula_rate > 0) {
            return 'Парсинг по формуле';
        }

        if($item->id_your_exchange > 0) {
            return 'Свой курс';
        }

        if($item->manual_rate_value > 0) {
            return 'Ручной курс';
        }

        if(isset($item->id_crypto_parser) and $item->id_crypto_parser > 0 and $item->enable_bestchange == 0)
        {
            if(isset($item->parser_exchange) and isset($item->parser_exchange->group_parse_exchange)) {
                return $item->parser_exchange->group_parse_exchange->name;
            }

            return 'Error parser';
        }


        return 'No name';
    }

    /**
     * Отладка результатов массива
    */
    public function toDebug()
    {
        echo '<pre>';
            print_r($this->document->all());
        echo '</pre>';
    }

    /**
     * Записываем сформированный документ в файл
     *
     * @return void
     */
    public function toFile()
    {
        $this->files->put($this->getPath(), $this->compileJson());
    }

    /**
     * Записываем сформированный документ в файл
     *
     * @return void
     */
    public function toFileMulti(string $locale = null)
    {
        $this->files->put($this->getPathMulti($locale), $this->compileJson());
    }

    /**
     * Записываем обновленный файл в Redis
     *
     * @return void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function addToRedis()
    {
        Redis::set('exchange-iex-initial-rates', $this->compileJson());
    }


    /**
     * Записываем обновленный файл в Redis (multi)
     *
     * @return void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function addToRedisMulti(string $locale = null)
    {
        Redis::set('exchange-iex-initial-rates_'.$locale, $this->compileJson());
    }


    /**
     * Временное кэширование данных
     */
    public function temporaryCachingCourses()
    {
        return \Cache::remember('temp-cache-rates', Carbon::now()->addMinutes(3), function() {
            return $this->compileJson();
        });
    }

    /**
     * Временное кэширование данных
     */
    public function temporaryCachingCoursesTelegram()
    {
        $this->is_telegram_bot = true;
        return \Cache::remember('temp-cache-rates', Carbon::now()->addMinutes(3), function() {
            return $this->compileJson();
        });
    }

    /**
     * Записываем обновленный файл в Redis
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getRatesRedis()
    {
        if(empty(Redis::get('exchange-iex-initial-rates')))
            $this->addToRedis();
        return Redis::get('exchange-iex-initial-rates');
    }

    /**
     * Записываем обновленный файл в Redis
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getRatesRedisMulti(string $locale = null)
    {
        if(empty(Redis::get('exchange-iex-initial-rates_'.$locale)))
            $this->addToRedisMulti($locale);
        return Redis::get('exchange-iex-initial-rates_'.$locale);
    }

    /**
     * Записываем обновленный файл в документ
     *
     * @return void
     */
    public function toCacheFile()
    {
        $this->files->put($this->getCachePath(), $this->compileJson());
    }

    /**
     * Записываем обновленный файл в документ
     *
     * @return void
     */
    public function toCacheFileMulti(string $locale = null)
    {
        $this->files->put($this->getCachePathMulti($locale), $this->compileJson());
    }

    /**
     * Записываем обновленный файл в Redis
     *
     * @param bool $is_dynamic
     * @param array $data
     * @return void
     */
    public function addToCacheRedis(bool $is_dynamic = false, array $data = []): void
    {
        if($is_dynamic) {
            Redis::set('exchange-'.$this->redis_tag_update, $this->compileJsonDymanic($data));
        } else {
            Redis::set('exchange-'.$this->redis_tag_update, $this->compileJson());
        }
    }

    /**
     * Получаем обновленный файл из Redis
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCSRedis()
    {
        if(empty(Redis::get('exchange-'.$this->redis_tag_update)))
            $this->addToCacheRedis();
        return Redis::get('exchange-'.$this->redis_tag_update);
    }

    /**
     * Записываем обновленный файл в Redis Multi
     *
     * @return void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function addToCacheRedisMulti(string $locale = null)
    {
        Redis::set('exchange-'.$this->redis_tag_update.'-'.$locale, $this->compileJson());
    }

    /**
     * Получаем обновленный файл из Redis Multi
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCSRedisMulti()
    {
        $locale = \Str::lower(\App::getLocale());
        return Redis::get('exchange-'.$this->redis_tag_update.'-'.$locale);
    }

    /**
     * Отображаем массив
     *
     * @return array
    */
    public function toArray()
    {
        return $this->document->all();
    }

    public function toJson()
    {
        return $this->compileJson();
    }

    /**
     * Получаем версию модуля
     *
     * @return float
     */
    protected function getVersion()
    {
        return $this->config['version'];
    }

    /**
     * Получаем время генерации документа
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return Carbon::now()->format('Y-m-d H:i:s O');
    }

    /**
     * Получаем путь к файлу
     *
     * @return string
     */
    public function getPath()
    {
        return $this->config['path'];
    }

    public function getPathMulti($locale) {
        return public_path('/uploads/rates_'.$locale.'.json');
    }

    /**
     * Путь к документу обновлений
     *
     * @return string
    */
    public function getCachePath()
    {
        return $this->config['cache_path'];
    }

    public function getCachePathMulti($locale)
    {
        return public_path('/uploads/cs_'.$locale.'.json');
    }

    public function toNumber($value, $decimal = 10) {
        return number_format($value, $decimal, '.','');
    }

    /**
     * Определяем устройство с которого была создана заявка
     *
     * @return string
     */
    public function getUserDevice()
    {
        if(Agent::isDesktop())
            return 'desktop';
        elseif(Agent::isMobile())
            return 'mobile';
        elseif(Agent::isTablet())
            return 'tablet';

        return 'desktop';
    }
}
