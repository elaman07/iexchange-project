<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto;

use App\Common\Packages\Crypto\Adapters\{AlcorExchange\AlcorExchangeAdapter,
    Binance\BinanceAdapter,
    BinanceDEX\BinanceDEXAdapter,
    BinanceTicker\BinanceTickerAdapter,
    Bitfinex\BitfinexAdapter,
    Bitmart\BitmartAdapter,
    BitPay\BitPayAdapter,
    Blockchain\BlockchainAdapter,
    ByBit\ByBitAdapter,
    Coinbase\CoinbaseAdapter,
    CoinMarketCap\CoinMarketCapAdapter,
    CoinStats\CoinStatsAdapter,
    CoinStatsFiat\CoinStatsFiatAdapter,
    Commex\CommexAdapter,
    CryptoCom\CryptoComAdapter,
    Cryptonex\CryptonexAdapter,
    EuropeanCentralBank\EuropeanCentralBankAdapter,
    FixerApi\FixerApiAdapter,
    FloatRates\FloatRatesAdapter,
    GarantexOrder\GarantexOrderAdapter,
    GateIo\GateIoAdapter,
    Kraken\KrakenAdapter,
    MexcExchange\MexcExchangeAdapter,
    Moex\MoexAdapter,
    PancakeSwap\PancakeSwapAdapter,
    Garantex\GarantexAdapter,
    HitBtc\HitBtcAdapter,
    KuCoin\KuCoinAdapter,
    Kuna\KunaAdapter,
    NationalBankOfBelarus\NationalBankOfBelarusAdapter,
    NationalBankOfIsrael\NationalBankOfIsraelAdapter,
    NationalBankOfKazakhstan\NationalBankOfKazakhstanAdapter,
    NationalBankOfKyrgyz\NationalBankOfKyrgyzAdapter,
    NationalBankOfMoldova\NationalBankOfMoldovaAdapter,
    NationalBankOfRomania\NationalBankOfRomaniaAdapter,
    Okex\OkexAdapter,
    Poloniex\PoloniexAdapter,
    Rapira\RapiraAdapter,
    RussianCentralBank\RussianCentralBankAdapter,
    Exmo\ExmoAdapter,
    UzbekistanCentralBank\UzbekistanCentralBankAdapter,
    WhiteBit\WhiteBitAdapter,
    WMExchanger\WMExchangerAdapter};

use Illuminate\Foundation\Application;
use Illuminate\Support\Str;
use InvalidArgumentException;

class CryptoManager
{
    /**
     * Список адаптеров доступных
     *
     * @var array
    */
    protected array $adapters = [
        'russiancentralbank' => RussianCentralBankAdapter::class,
        'exmo' => ExmoAdapter::class,
        'coinmarketcap' => CoinMarketCapAdapter::class,
        'europeancentralbank' => EuropeanCentralBankAdapter::class,
        'nationalbankofromania' => NationalBankOfRomaniaAdapter::class,
        'nationalbankofkyrgyz' => NationalBankOfKyrgyzAdapter::class,
        'nationalbankofbelarus' => NationalBankOfBelarusAdapter::class,
        'nationalbankofkazakhstan' => NationalBankOfKazakhstanAdapter::class,
        'nationalbankofmoldova' => NationalBankOfMoldovaAdapter::class,
        'uzbekistancentralbank' => UzbekistanCentralBankAdapter::class,
        'nationalbankofisrael' => NationalBankOfIsraelAdapter::class,
        'binance' => BinanceAdapter::class,
        'binanceticker' => BinanceTickerAdapter::class,
        'blockchain' => BlockchainAdapter::class,
        'poloniex' => PoloniexAdapter::class,
        'bitfinex' => BitfinexAdapter::class,
        'fixerapiusd' => FixerApiAdapter::class,
        'fixerapieur' => FixerApiAdapter::class,
        'fixerapiuah' => FixerApiAdapter::class,
        'hitbtc' => HitBtcAdapter::class,
        'kucoin' => KuCoinAdapter::class,
        'binancedex' => BinanceDEXAdapter::class,
        'okex' => OkexAdapter::class,
        'bitpay' => BitPayAdapter::class,
        'cryptonex' => CryptonexAdapter::class,
        'bitmart' => BitmartAdapter::class,
        'floatrates' => FloatRatesAdapter::class,
        'floatratescny' => FloatRatesAdapter::class,
        'floatratesinr' => FloatRatesAdapter::class,
        'floatrateseur' => FloatRatesAdapter::class,
        'floatratesuah' => FloatRatesAdapter::class,
        'whitebit' => WhiteBitAdapter::class,
        'wmexchanger' => WMExchangerAdapter::class,
        'kuna' => KunaAdapter::class,
        'garantex' => GarantexAdapter::class,
        'garantexorder' => GarantexOrderAdapter::class,
        'pancakeswap' => PancakeSwapAdapter::class,
        'coinstats' => CoinStatsAdapter::class,
        'coinstatsfiat' => CoinStatsFiatAdapter::class,
        'cryptocom' => CryptoComAdapter::class,
        'gateio' => GateIoAdapter::class,
        'coinbase' => CoinbaseAdapter::class,
        'alcorexchange' => AlcorExchangeAdapter::class,
        'mexcexchange' => MexcExchangeAdapter::class,
        'kraken'    => KrakenAdapter::class,
        'moex'  =>   MoexAdapter::class,
        'commex' => CommexAdapter::class,
        'bybit' => ByBitAdapter::class,
        'rapira' => RapiraAdapter::class
    ];

    /**
     * Экземпляр приложения.
     *
     * @var Application
     */
    protected Application $app;

    /**
     * Массив разрешенных драйверов.
     *
     * @var array
     */
    protected array $parsers = [];

    /**
     * Создайте новый экземпляр менеджера.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Получите экземпляр мерчанта.
     *
     * @param string|null $name
     * @return mixed
     */
    public function driver(string $name = null)
    {
        return $this->parser($name);
    }

    /**
     * Получите драйвер парсинга.
     *
     * @param string|null $name
     * @return mixed
     */
    public function parser(string $name = null)
    {
        $name = mb_strtolower(Str::studly($name)) ?: $this->getDefaultDriver();
        return $this->parsers[$name] = $this->get($name);
    }

    /**
     * Попытайтесь получить платеж из локального кеша.
     *
     * @param string $name
     * @return mixed
     */
    protected function get(string $name)
    {
        return $this->parsers[$name] ?? $this->resolve($name);
    }

    /**
     * Разрешите данный платеж.
     *
     * @param string $name
     *
     * @return mixed
     */
    protected function resolve(string $name)
    {
        $lower_name = Str::lower($name);
        if(isset($this->adapters[$lower_name])) {
            return new $this->adapters[$lower_name]();
        } else {
            throw new InvalidArgumentException("Driver [{$lower_name}] не поддерживается.");
        }
    }

    /**
     * Адаптер BestChange
     *
     * @return BestchangeHandler
     */
    public function bestchange(): BestchangeHandler
    {
        return new BestchangeHandler();
    }

    /**
     * Файлы курсов
     *
     * @param $type
     * @return mixed
     * @throws \Exception
     */
    public function setSchema($type)
    {
        $type = mb_strtoupper($type);
        $class = "\\App\\Common\Packages\\Crypto\\Schema\\{$type}Builder";

        if(class_exists($class))
            return new $class();

        throw new \Exception('Не определен тип курсов');
    }

    /**
     * Получите экземпляр драйвера.
     *
     * @param string $driver
     * @return mixed
     */
    public function with(string $driver)
    {
        return $this->driver($driver);
    }

    /**
     * Получить имя драйвера по умолчанию.
     *
     * @return string
     */
    public function getDefaultDriver(): string
    {
        throw new InvalidArgumentException('Не указан драйвер Crypto.');
    }

    /**
     * Динамически вызывать экземпляр драйвера по умолчанию.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->driver()->$method(...$parameters);
    }
}
