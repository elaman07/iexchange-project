<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 21.12.2017
 * Time: 16:36
 */

namespace App\Common\Packages\Crypto;


use App\Common\Packages\Crypto\Compilers\Compiler;
use App\Common\Packages\Crypto\Contracts\CompilerFactory;
use App\Common\Packages\Crypto\Contracts\Factory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Client\Factory as HttpFactory;

class CryptoServiceProvider extends ServiceProvider
{
    public function boot() {

    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Factory::class, function ($app) {
            return new CryptoManager($app);
        });

        $this->app->singleton(CompilerFactory::class, function ($app) {
            return new Compiler($this->app['files'], $this->app['config']['crypto.compiler']);
        });

        $this->app->singleton('crypto.validator', function($app) {
            return new ValidatorRequest($app[HttpFactory::class]);
        });


        $this->commands([
            Console\CompilerCoursesConsole::class,
            Console\CompilerBestChangeConsole::class
        ]);
    }
}
