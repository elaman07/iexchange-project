<?php
namespace App\Common\Packages\Crypto\Schema;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\Reserve;

class JSONBuilder extends Builder
{
    /**
     * Название схемы
     *
     * @var string
     */
    protected $scheme = 'json';

    protected $data = [];

    /**
     * Обработчик данных для файла JSON
     *
     * @return array|bool
     * @throws \Exception
     */
    protected function handle()
    {
        if(iEXSetting('grates_format_json') != 1)
            return false;

        // Исключаем обменные пункты у которых нет резерва
        $this->except = Reserve::where('summa','<=', 0)->get()->map(function($item) {
            return $item->id_main > 0 ? $item->main_reserve->id_currency : $item->id_currency;
        })->toArray();

        $this->data = Currency::where('status', '=', 0)
            ->whereHas('direction_exchange_in', function($q) {
                $q->where('status', '=', 1);
            })->orWhereHas('direction_exchange_out', function($q) {
                $q->where('status', '=', 1);
            })->cursor();

        $this->setArray([
            'version' => '1.3',
            'currencies'    =>  [
                'list'   =>  $this->currencies(),
                'amounts'   =>  $this->amounts(),
            ],
            'exchange'  =>  $this->exchange()
        ]);

        return json_encode($this->array->first(), JSON_NUMERIC_CHECK);
    }

    /**
     * Список направлений "Отдаю"
     *
     * @return array
     * @throws \Exception
     */
    protected function exchange(): array
    {
        $array  = [];
        foreach ($this->getRates() as $item)
        {
            $array[$item->currency1->id] = [
                'to'    =>  $this->to($item)
            ];
        }

        return $array;
    }

    /**
     * Список направлений "Получаю"
     *
     * @param DirectionExchange $exchange
     * @return array
     * @throws \Exception
     */
    protected function to(DirectionExchange $exchange): array
    {
        $array = [];

        //Получение списка активных направлений
        $direction_exchange = DirectionExchange::where([
            ['id_currency1', '=', $exchange->id_currency1],
            ['status', '=', 1]])->whereNotIn('id_currency2', $this->except)->cursor();

        foreach ($direction_exchange as $item)
        {
            $in_format = ($item->currency2->number_format > 6 ? 6 : $item->currency2->number_format);

            if($this->fetchSymbol($item) == '0') {
                $xr =  iex_number_format($this->fetchIn($item), $in_format);
            }else {
                if($this->fetchOut($item) == 1)
                    $xr = 1;
                else
                    $xr = -iex_number_format($this->fetchOut($item), $in_format);
            }

            $array[$item->currency2->id] = [
                'xr'    => $xr,
                'min'   =>  $item->min_price1,
                'max'   =>  $item->max_price1
            ];

            if($item->min_price1 <= 0)
                unset($array[$item->currency2->id]['min']);

            if($item->max_price1 <= 0)
                unset($array[$item->currency2->id]['max']);

            if($item->min_price1 <= 0 and $item->max_price1 <= 0)
                $array[$item->currency2->id] =  ($xr <= 1) ?  1: $xr;
        }

        return $array;
    }

    /**
     * Получаем список валют
     *
     * @return array
     */
    protected function currencies(): array
    {
        $array = [];

        foreach ($this->data as $item) {
            $array[$item->id] = (string)$item->designation_xml;
        }

        return $array;
    }

    /**
     * Получение списка резервов
     *
     * @return array
     */
    protected function amounts(): array
    {
        $array = [];
        foreach ($this->data as $item) {

            $amount_base = (float)isset($item->reserve) ? iex_reserve_amount($item->reserve) : 0;
            if($amount_base < 0)
                continue;
            $amount = ($amount_base < 0 ? 0.01 : $amount_base);

            if($amount <= 0 or $item->direction_exchange_out->where('status', '=', 1)->count() == 0)
                continue;

            if($item->id_filter_currency == 3) {
                $array[$item->id] = (float)number_format($amount, 6,'.',''
                );
            }else {
                $array[$item->id] = (float)number_format($amount,2,'.','');
            }
        }

        return $array;
    }
}
