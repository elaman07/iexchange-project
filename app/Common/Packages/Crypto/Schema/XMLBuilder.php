<?php
namespace App\Common\Packages\Crypto\Schema;

use Nwidart\Modules\Facades\Module;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Spatie\ArrayToXml\ArrayToXml;

class XMLBuilder extends Builder
{
    /**
     * Название схемы
     *
     * @return string
     */
    protected $scheme = 'xml';

    /**
     * Обработчик данных для файла JSON
     *
     * @return false|string
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function handle()
    {
        if(iEXSetting('grates_format_xml') != 1)
            return false;

        $rates1 = $this->getRates()->map(function($rate)
        {
            if($this->fetchSymbol($rate) == '0') {
                $in = $this->fetchIn($rate);
                $out = 1;
            }else {
                $in =  1;
                $out = $this->fetchOut($rate);
            }

            if($rate->direction_exchange_cities->count() == 0) {
                // Управление параметрами
                $item['from'] = $rate->currency1->designation_xml;
                $item['to'] = $rate->currency2->designation_xml;
                $item['in'] = $in;
                $item['out'] = $out;
                $item['amount'] = (float)(isset($rate->currency2->reserve) ? iex_reserve_amount($rate->currency2->reserve) : 0);

                if(!empty($rate->label_floating)) {
                    $item['floating'] = $rate->label_floating;
                }

                if(!empty($rate->label_delay)) {
                    $item['delay'] = $rate->label_delay;
                }

                // Отображать или скрывать минимальную сумму
                if($rate->export_label_minamount == 0 and iEXSetting('grates_is_minamount')) {
                    $item['minamount'] = sprintf('%s %s', $rate->min_price1, $rate->currency1->code_currency->name);
                }elseif($rate->export_label_minamount == 1) {
                    $item['minamount'] = sprintf('%s %s', $rate->min_price1, $rate->currency1->code_currency->name);
                }

                // Отображать или скрывать максимальную сумму
                if($rate->export_label_maxamount == 0 and iEXSetting('grates_is_maxamount')) {
                    $item['maxamount'] = sprintf('%s %s', $rate->max_price1, $rate->currency1->code_currency->name);
                }elseif($rate->export_label_maxamount == 1) {
                    $item['maxamount'] = sprintf('%s %s', $rate->max_price1, $rate->currency1->code_currency->name);
                }

                // Доп. комиссия  (Отдаю)
                if((int)iEXSetting('is_in_type_fromfee') == 0) // Передаем доп. комиссию
                {
                    if($rate->oth_comm_percent > 0 or $rate->oth_comm_currency > 0) {
                        $item['fromfee'] = implode(', ', array_filter([
                            ($rate->oth_comm_percent > 0) ? $rate->oth_comm_percent.'%' : '',
                            ($rate->oth_comm_currency > 0) ?$rate->oth_comm_currency.' '. $rate->currency1->code_currency->name : ''
                        ]));
                    }
                }

                if((int)iEXSetting('is_in_type_fromfee') == 1) // Передаем комиссию платежной системы
                {
                    if($rate->pay_comm_percent > 0 or $rate->pay_comm_currency > 0) {
                        $item['fromfee'] = implode(', ', array_filter([
                            ($rate->pay_comm_percent > 0) ? $rate->pay_comm_percent.'%' : '',
                            ($rate->pay_comm_currency > 0) ? $rate->pay_comm_currency.' '. $rate->currency1->code_currency->name : ''
                        ]));
                    }
                }

                // Доп. комиссия  (Получаю)
                if((int)iEXSetting('is_in_type_tofee') == 0) // Передаем доп. комиссию
                {
                    if($rate->oth_comm2_percent > 0 or $rate->oth_comm2_currency > 0) {
                        $item['tofee'] = implode(', ', array_filter([
                            ($rate->oth_comm2_percent > 0) ? $rate->oth_comm2_percent.'%' : '',
                            ($rate->oth_comm2_currency > 0) ? $rate->oth_comm2_currency.' '. $rate->currency2->code_currency->name : ''
                        ]));
                    }
                }

                if((int)iEXSetting('is_in_type_tofee') == 1) // Передаем комиссию платежной системы
                {
                    if($rate->pay_comm2_percent > 0 or $rate->pay_comm2_currency > 0)
                    {
                        $item['tofee'] = implode(', ', array_filter([
                            ($rate->pay_comm2_percent > 0) ? $rate->pay_comm2_percent.'%' : '',
                            ($rate->pay_comm2_currency > 0) ? $rate->pay_comm2_currency.' '. $rate->currency2->code_currency->name : ''
                        ]));
                    }
                }


                $options = [];
                if($rate->hidden_export_label_param == 0) {
                    $options[] = $this->getParam($rate);
                }

                if($rate->hidden_export_label_param == 2) {
                    $options[] = 'manual';
                }

                if($rate->xml_juridical == 1) {
                    $options[] = 'juridical';
                }

                if(count($options) > 0) {
                    $item['param'] = implode(', ', $options);
                }
            }

            if($rate->direction_exchange_cities->count() > 0)
            {
                $item['parent'] = [];
                foreach ($rate->direction_exchange_cities as $direction_exchange_city)
                {
                    if($this->fetchSymbol($rate) == '0') {
                        $in =  $this->fetchIn($rate, $direction_exchange_city->id);
                        $out = 1;
                    }else {
                        $in =  1;
                        $out = $this->fetchOut($rate, $direction_exchange_city->id);
                    }

                    // Управление параметрами
                    $item['parent'][$direction_exchange_city->id]['from'] = $rate->currency1->designation_xml;
                    $item['parent'][$direction_exchange_city->id]['to'] = $rate->currency2->designation_xml;
                    $item['parent'][$direction_exchange_city->id]['in'] = $in;
                    $item['parent'][$direction_exchange_city->id]['out'] = $out;


                    // Отображать или скрывать минимальную сумму
                    if($rate->export_label_minamount == 0 and iEXSetting('grates_is_minamount')) {
                        $item['parent'][$direction_exchange_city->id]['minamount'] = sprintf('%s %s', ($direction_exchange_city->min_price > 0 ? $direction_exchange_city->min_price : $rate->min_price1), $rate->currency1->code_currency->name);
                    }elseif($rate->export_label_minamount == 1) {
                        $item['parent'][$direction_exchange_city->id]['minamount'] = sprintf('%s %s', $direction_exchange_city->min_price > 0 ? $direction_exchange_city->min_price : $rate->min_price1, $rate->currency1->code_currency->name);
                    }

                    // Отображать или скрывать максимальную сумму
                    if($rate->export_label_maxamount == 0 and iEXSetting('grates_is_maxamount')) {
                        $item['parent'][$direction_exchange_city->id]['maxamount'] = sprintf('%s %s', $direction_exchange_city->max_price > 0 ? $direction_exchange_city->min_price : $rate->max_price1, $rate->currency1->code_currency->name);
                    }elseif($rate->export_label_maxamount == 1) {
                        $item['parent'][$direction_exchange_city->id]['maxamount'] = sprintf('%s %s', $direction_exchange_city->max_price > 0 ? $direction_exchange_city->min_price : $rate->max_price1, $rate->currency1->code_currency->name);
                    }

                    $item['parent'][$direction_exchange_city->id]['amount'] = (float)(isset($rate->currency2->reserve) ? iex_reserve_amount($rate->currency2->reserve) : 0);


                    if(!empty($direction_exchange_city->param)) {
                        $item['parent'][$direction_exchange_city->id]['param'] = $direction_exchange_city->param;
                    }else {
                        $item['parent'][$direction_exchange_city->id]['param'] = 'manual';
                    }

                    if(!empty($direction_exchange_city->city->designation_xml)) {
                        $item['parent'][$direction_exchange_city->id]['city'] = $direction_exchange_city->city->designation_xml;
                    }
                }
            }


            return $item;
        });


        $result = $rates1->toArray();

        $parent_array = [];
        $new_array = [];
        foreach ($result as $item)
        {
            if(isset($item['parent'])) {
                $parent_array += $item['parent'];
                unset($item['parent']);
            }
            $new_array[] = $item;
        }

        $this->arrayView = array_filter(array_merge($new_array, $parent_array));


        return ArrayToXml::convert([
            'item' => $this->arrayView
        ], [
            'rootElementName' => 'rates',
        ]);
    }

    /**
     * Получение параметра
     *
     * @param $item
     * @return string
     */
    protected function getParam($item) {
        return ($item->export_label_param ?? 'manual');
    }
}
