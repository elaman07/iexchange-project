<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 14.02.2018, 11:33
 */

namespace App\Common\Packages\Crypto\Schema;


class TXTBuilder extends Builder
{
    /**
     * Название схемы
     *
     * @return string
     */
    protected $scheme = 'txt';

    /**
     * Обработчик данных
     *
     * @return array|string
     * @throws \Exception
     */
    protected function handle()
    {
        if(iEXSetting('grates_format_txt') != 1)
            return false;

        $write = null;
        foreach ($this->getRates() as $item)
        {
            if($this->fetchSymbol($item) == '0') {
                $in     =   $this->fetchIn($item);
                $out    =   1;
            }else {
                $in     =   1;
                $out    =   $this->fetchOut($item);
            }

            $write .= sprintf('%s;%s;%s;%s;%s',
                    $item->currency1->designation_xml,
                    $item->currency2->designation_xml,
                    $in,
                    $out,
                    iex_reserve_amount($item->currency2->reserve)
                ).PHP_EOL;
        }

        return $write;
    }
}
