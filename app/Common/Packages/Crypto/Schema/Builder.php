<?php
namespace App\Common\Packages\Crypto\Schema;


use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Models\DirectionExchange;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Traits\Macroable;

abstract class Builder
{
    use Macroable {
        __call as macroCall;
    }

    /**
     * Название схемы
     *
     * @var  string
    */
    protected $scheme = null;

    /**
     * Полное имя файла
     *
     * @var string
    */
    protected $filename = null;

    /**
     * Массив для результатов
     *
     * @var Collection
     */
    protected Collection $array;

    protected array $arrayView = [];

    /**
     * Валюты которые исключены из списка
     *
     * @var array
     */
    protected $except = [];

    /**
     * Создаем новый диспетчер схем.
     *
     * @return void
     */
    public function __construct()
    {
        $this->array = new Collection;
        $this->filename = public_path(
            sprintf('%s.%s', iEXSetting('grates_filename', 'filename'), $this->scheme)
        );
    }

    /**
     * Выполнить действие, чтобы построить / изменить данные.
     *
     * @return void
     */
    public function build()
    {
        //Перед созданием нового документа, удаляем старый
        //iex_file_delete($this->filename);
        ///Записываем результат в файл
        File::put($this->filename, $this->handle(), FILE_APPEND);
    }

    /**
     * Создаем пустые файлы
     *
     * @return mixed
    */
    public function empty() {
        File::put($this->filename, null, FILE_APPEND);
    }

    /**
     * Удаление файла курсов
     *
     * @return mixed
    */
    public function delete() {
        iex_file_delete($this->filename);
    }

    /**
     * Просмотр сформированного файла
     *
     * @return string
    */
    public function view() {
        echo $this->handle();
    }

    public function getViewArray(): array
    {
        $this->handle();
        return $this->arrayView;
    }

    /***
     * Записываем новые элементы в массив
     *
     * @param $value
     */
    protected function setArray($value)
    {
        $this->array->push($value);
    }

    /**
     * Получение списка направлений
     *
     */
    protected function getRates()
    {
//        return cache()->remember('builder-scheme-rates', \Illuminate\Support\Carbon::now()->addSeconds(5), function()
//        {
//
//        });

        $exchange = DirectionExchange::isEnabled()->with([
            'currency1' => function($q) {
                $q->select('id', 'id_code_currency', 'number_format', 'status', 'designation_xml');
            },
            'currency2' => function($q) {
                $q->select('id', 'id_code_currency', 'number_format', 'status', 'designation_xml');
            },
            'currency1.code_currency',
            'currency2.code_currency',
            'currency1.reserve',
            'currency2.reserve',
            'currency1.reserve.main_reserve',
            'currency2.reserve.main_reserve',
            'your_exchange',
            'rl_your_exchange',
            'bestchange_rates',
            'direction_exchange_cities',
            'direction_exchange_cities.city',
            'group_commission',
            'parser_exchange',
            'rl_parser_exchange',
            'bc_your_rate',
            'bc_new_rate',
            'parser_formula_rates',
            'file_parser_rates',
            'bs_alt_parser',
            'competitor_rates',
            'cr_new_rate',
            'direction_exchange_percentage_amount'
        ])->whereNotIn('id_currency2', $this->except)->where('is_error_rate', '=', 0);

        // Отключение неактивных валют
        if(iEXSetting('grates_inactive_currencies') == 0)
            $exchange->where('status', '=', 1);

        return $exchange->get()->reject(function($filter) {
            // Пропускаем те направления, резерв которых больше чем мин. сумма обмена
            if((int)iEXSetting('grates_inactive_min_reserve') == 1) {
                $reserve = (float)(isset($filter->currency2->reserve) ? iex_reserve_amount($filter->currency2->reserve) : 0);;
                if($filter->min_price2 > $reserve) {
                    return $filter;
                }
            }
        })->filter(function($filter) {
            // Включенные направления
            if($filter->allow_export == 0) return $filter;

            // Отображаем направелния по расписанию
            if($filter->allow_export == 1 and
                Carbon::now()->format('H:i') > $filter->allow_export_from and
                Carbon::now()->format('H:i') < $filter->allow_export_to)
                return $filter;
        });

        return $exchange;
    }

    /**
     * Получаем первый символ и проверяем ее
     *
     * @param DirectionExchange $item
     * @param int $number
     * @return string
     */
    protected function fetchSymbol(DirectionExchange $item, int $number = 2): string
    {
        return \mb_substr(ConvertFacade::call($item)->in()->getResultAmount(), 0, $number);
    }

    /**
     * Получить конверитированную цену "В"
     *
     * @param DirectionExchange $item
     * @param int $is_city
     * @return float
     */
    protected function fetchIn(DirectionExchange $item, int $is_city = 0): float
    {
        if($is_city > 0)
        {
            // По настройкам из валюты
            if(iEXSetting('grates_type_number_format') == 0) {
                return iex_number_format(1 / ConvertFacade::call($item)->in_city($is_city)->getAmount(),
                    $item->currency1->number_format
                );
            }elseif((int)iEXSetting('grates_type_number_format') == 2)
            {
                return iex_number_format(1 / ConvertFacade::call($item)->in_city($is_city)->getAmount(),
                    $item->currency1->number_format_xml
                );
            }

            return iex_number_format(1 / ConvertFacade::call($item)->in_city($is_city)->getAmount(), iEXSetting('grates_number_format'));
        }


        // По настройкам из валюты
        if(iEXSetting('grates_type_number_format') == 0) {
            return iex_number_format(1 / ConvertFacade::call($item)->in()->getAmount(),
                $item->currency1->number_format
            );
        } elseif((int)iEXSetting('grates_type_number_format') == 2)
        {
            return iex_number_format(1 / ConvertFacade::call($item)->in()->getAmount(),
                $item->currency1->number_format_xml
            );
        }

        return iex_number_format(1 / ConvertFacade::call($item)->in()->getAmount(), iEXSetting('grates_number_format'));
    }

    /**
     * Получить конверитированную цену "На"
     *
     * @param DirectionExchange $item
     * @param int $is_city
     * @return float
     */
    protected function fetchOut(DirectionExchange $item, int $is_city = 0): float
    {
        if($is_city > 0) {
            // По настройкам из валюты
            if(iEXSetting('grates_type_number_format') == 0) {
                return iex_number_format(
                    ConvertFacade::call($item)->in_city($is_city)->getAmount(),
                    $item->currency2->number_format
                );
            } elseif((int)iEXSetting('grates_type_number_format') == 2)
            {
                return iex_number_format(
                    ConvertFacade::call($item)->in_city($is_city)->getAmount(),
                    $item->currency2->number_format_xml
                );
            }

            return iex_number_format(ConvertFacade::call($item)->in_city($is_city)->getAmount(),  iEXSetting('grates_number_format'));
        }

        // По настройкам из валюты
        if(iEXSetting('grates_type_number_format') == 0) {
            return iex_number_format(
                ConvertFacade::call($item)->in()->getAmount(),
                $item->currency2->number_format
            );
        } elseif((int)iEXSetting('grates_type_number_format') == 2)
        {
            return iex_number_format(
                ConvertFacade::call($item)->in()->getAmount(),
                $item->currency2->number_format_xml
            );
        }

        return iex_number_format(ConvertFacade::call($item)->in()->getAmount(),  iEXSetting('grates_number_format'));
    }


    /**
     * Обработчик данных
     *
     * @return array | string
     */
    protected abstract function handle();
}
