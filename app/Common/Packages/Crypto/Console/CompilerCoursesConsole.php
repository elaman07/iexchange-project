<?php
namespace App\Common\Packages\Crypto\Console;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Common\Packages\Crypto\BestchangeHandler;
use App\Models\CourseUpdateTimeLog;
use App\Models\DirectionExchange;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CompilerCoursesConsole extends Command
{
    /**
     * Имя и подпись команды консоли.
     *
     * @var string
     */
    protected $signature = 'compiler:courses';

    /**
     * Описание команды консоли.
     *
     * @var string
     */
    protected $description = 'Полное обновление курсов и файлов';

    /**
     * Выполните команду консоли.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle()
    {
        $start = microtime(true);

        $this->comment('+ Запуск обновления курсов');
        \CryptoCompiler::console($this);
        $this->comment('+ Курсы успешно обновлены');
        $this->comment('+ Обновление JSON документов');

        if((int)iEXSetting('type_cached_rates') == 1) { // re
            \CryptoCompiler::compileLang('redis');
        } elseif((int)iEXSetting('type_cached_rates') == 2) {
            \CryptoCompiler::compileLang('file');
        }

        $this->comment('+ Документы успешно обновлены');

        // Скриншот счетчика
        \Cache::increment('dr_snapshot_id');

        if(iEXSetting('is_direction_exchange_update_minmax') == 0)
        {
            if((int)iEXSetting('generate_min_price') == 1 or (int)iEXSetting('generate_max_price') == 1)
            {
                DirectionExchange::active()->where('status', '=', 1)
                    ->chunk(100, function($exchange) {
                        $exchange->each(function($item) {
                            $update = [];
                            // Обновление мин. цены
                            if(iEXSetting('generate_min_price') and $item->is_manual_min_price1 == 0) {
                                $update['min_price1'] = iex_number_format($item->min_price2 / \Converter::call($item)->in()->getAmount(), $item->currency1->number_format);
                            }
                            // Обновление макс. цены
                            if(iEXSetting('generate_max_price') and $item->is_manual_max_price1 == 0)
                                $update['max_price1'] = iex_number_format($item->max_price2 / \Converter::call($item)->in()->getAmount(), $item->currency1->number_format);

                            $item->update($update);
                        });
                    });
            }

        }elseif(iEXSetting('is_direction_exchange_update_minmax') == 1)
        {
            $this->comment('Обновление мин/макс. суммы через биржу Whitebit .....');
            $dx = DirectionExchange::active()->where('status', 1)->cursor();
            $getWhiteBitFee = json_decode(
                file_get_contents('https://whitebit.com/api/v4/public/assets')
                ,true);

            foreach ($dx as $item)
            {
                $update = [];
                // Обновляем только в случае если подключен WhiteBit
                if(isset($getWhiteBitFee[$item->currency1->designation_xml])) {
                    $update['min_price1'] = $getWhiteBitFee[$item->currency1->designation_xml]['min_deposit'];
                    $update['max_price1'] = $getWhiteBitFee[$item->currency1->designation_xml]['max_deposit'];
                }

                // Обновляем только в случае если подключен WhiteBit
                if(isset($getWhiteBitFee[$item->currency2->designation_xml])) {
                    $update['min_price2'] = $getWhiteBitFee[$item->currency2->designation_xml]['min_withdraw'];
                    $update['max_price2'] = $getWhiteBitFee[$item->currency2->designation_xml]['max_withdraw'];
                }

                $item->update($update);
            }
        }

        $time = round(microtime(true) - $start, 4);
        $this->info('Время обновления: '.$time.' сек.');

        // Записываем лог в базу
        CourseUpdateTimeLog::create([
            'time' => $time,
            'type_rate' => 'default',
            'source' => 'CompilerCoursesConsole'
        ]);

        cache()->set('cron-parser-last-update', [
            'last-update' => Carbon::now()->toDateTimeString(),
            'time' => $time
        ]);
    }
}
