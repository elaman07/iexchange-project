<?php
namespace App\Common\Packages\Crypto\Console;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Common\Packages\Crypto\BestchangeHandler;
use App\Models\CourseUpdateTimeLog;
use App\Models\DirectionExchange;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CompilerBestChangeConsole extends Command
{
    /**
     * Имя и подпись команды консоли.
     *
     * @var string
     */
    protected $signature = 'compiler:bestchange';

    /**
     * Описание команды консоли.
     *
     * @var string
     */
    protected $description = 'Полное обновление курсов и файлов';

    /**
     * Выполните команду консоли.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle()
    {
        $start = microtime(true);
        \CryptoCompiler::consoleBestChange($this);
        $time = round(microtime(true) - $start, 4);

        // Записываем лог в базу
        CourseUpdateTimeLog::create([
            'time' => $time,
            'type_rate' => 'bestchange',
            'source' => 'CompilerBestChangeConsole'
        ]);

        cache()->set('cron-bestchange-last-update', [
            'last-update' => Carbon::now()->toDateTimeString(),
            'time' => $time
        ]);
    }
}
