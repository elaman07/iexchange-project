<?php


namespace App\Common\Packages\Crypto\Facades;


use App\Common\Packages\Crypto\Contracts\Factory;
use Illuminate\Support\Facades\Facade;

class CryptoValidator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'crypto.validator';
    }
}
