<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.05.2018
 * Time: 16:55
 */

namespace App\Common\Packages\Crypto\Facades;

use App\Common\Packages\Crypto\Compilers\Compiler;
use App\Common\Packages\Crypto\Contracts\CompilerFactory;
use Illuminate\Support\Facades\Facade;


/**
 * @method static Compiler compile()
 * @method static Compiler update()
 */
class CompilerFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CompilerFactory::class;
    }
}