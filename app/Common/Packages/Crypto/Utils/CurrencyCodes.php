<?php
namespace App\Common\Packages\Crypto\Utils;

use DiDom\Document;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Коды взяты отсюда: https://www.bestchange.ru/wiki/rates.html
 * нет API для получения кода валют
 */

class CurrencyCodes
{
    const PAGE_URL = 'https://www.bestchange.ru/wiki/rates.html';
    const TIMEOUT = 20;
    private $pathfile;
    private $data;
    private $currencies;

    /**
     * ECurrencyCodes constructor.
     * @param Currencies $currencies
     * @throws \Exception
     */
    public function __construct(Currencies $currencies)
    {
        $this->currencies = $currencies;
        $this->pathfile = storage_path('app/bestchange/codes.json');
        if (file_exists($this->pathfile)) {
            $this->data = json_decode(file_get_contents($this->pathfile), true);
            return;
        }

        //$this->refreshCurrenciesCodes();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function getByID($id)
    {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }

        return;
    }

    public function get()
    {
        return $this->data;
    }

    /**
     * @throws \Exception
     */
    public function refreshCurrenciesCodes()
    {
        $codes = $this->getCodes();
        $currencies = $this->currencies->get();
        foreach ($currencies as $currency) {
            if (empty($codes[$currency['name']])) {
                throw new \Exception('Несоответствие данных таблицы «Коды электронных валют» и bm_cy.dat');
            }
            $this->data[$currency['id']] = [
                'id' => $currency['id'],
                'code' => $codes[$currency['name']],
                'name' => $currency['name'],
            ];
        }

        file_put_contents($this->pathfile, json_encode($this->data, JSON_UNESCAPED_UNICODE));
        return $this;
    }

    /**
     * @throws \Exception
     */
    private function getCodes()
    {
        $page = $this->getPage();
        $doc = new Document($page);
        if (!$doc->has('h2')) {
            throw new \Exception('Коды электронных валют не получены. Проверьте доступность ' . self::PAGE_URL);
        }
        foreach ($doc->find('h2') as $title) {
            if (preg_match('/Коды\s+электронных\s+валют/sui', $title->text())) {
                $table = $title->nextSibling('table.codetable');
                break;
            }
        }
        if (empty($table)) {
            throw new \Exception(
                'Коды электронных валют не получены. Проверьте наличие таблицы с кодами на странице ' . self::PAGE_URL
            );
        }
        $codes = [];
        foreach ($table->find('tr') as $row) {
            if (!$row->has('i')) {
                continue;
            }
            $cols = $row->find('td');
            if (count($cols) != 2) {
                throw new \Exception(
                    'Ошибка парсинга таблицы «Коды электронных валют». Таблица с кодами должна содержать 2 колонки (' . self::PAGE_URL . ')'
                );
            }
            $codes[trim($cols[1]->first('i')->text())] = trim($cols[0]->text());
        }
        return $codes;
    }

    private function getPage()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::PAGE_URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return iconv('CP1251', 'UTF-8', $data);
    }
}
