<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.01.2018
 * Time: 21:53
 */

namespace App\Common\Packages\Crypto\Utils;

class Currencies
{
    private $data = [];

    public function __construct($data)
    {
        $data = explode("\n", $data);
        foreach ($data as $row) {
            $row = iconv('CP1251', 'UTF-8', $row);
            $data = explode(';', $row);
            $this->data[$data[0]] = [
                'id' => $data[0],
                'name' => $data[2],
            ];
        }
        uasort($this->data, function ($a, $b) {
            return strcmp(
                mb_strtolower($a['name'], 'UTF-8'),
                mb_strtolower($b['name'], 'UTF-8')
            );
        });

        $ecc = new CurrencyCodes($this);
        foreach ($this->data as $id => &$item) {
            $res = $ecc->getByID($id);
            if(empty($res)) {
                continue;
            }
            $item['code'] = $res['code'];
        }
    }

    public function get()
    {
        return $this->data;
    }

    public function getByID($id)
    {
        return empty($this->data[$id]) ? false : $this->data[$id];
    }
}
