<?php
namespace App\Common\Packages\Crypto\Utils;


class Rates
{
    private array $data = [];

    public function __construct($data)
    {
        $data = explode("\n", $data);

        foreach ($data as $row)
        {
            $row = iconv('CP1251', 'UTF-8', $row);
            $data = explode(';', $row);

            $rateGive = (float)$data[3];
            $rateReceiver = (float)$data[4];

            $rate = $rateReceiver ? $rateGive / $rateReceiver : 0;
            $this->data[$data[0]][$data[1]][$data[2]] = [
                'give_id'           =>  $data[0],
                'get_id'            =>  $data[1],
                'exchanger_id'      =>  (int)$data[2],
                'rate_give'         =>  $rateGive,
                'rate_receiver'     =>  $rateReceiver,
                'rate'              =>  (float)$rate,
                'reserve'           =>  (float)$data[5],
                'reviews'           =>  (int)explode('.', $data[6])[1] ?? 0,
                'min_sum'           =>  (float)$data[8],
                'max_sum'           =>  (float)$data[9],
                'city_id'           =>  (int)$data[10]
            ];
        }

        $this->sortRateAscAll();
    }

    public function get()
    {
        return $this->data;
    }

    public function filter($currencyReceiveID = 0, $currencyGiveID = 0)
    {
        if (empty($this->data[$currencyReceiveID][$currencyGiveID])) {
            throw new \Exception('Нет направления обмена');
        }
        return $this->data[$currencyReceiveID][$currencyGiveID];
    }

    private function sortRateAsc($a, $b)
    {
        if ($a['rate'] == $b['rate']) {
            return 0;
        }
        return ($a['rate'] < $b['rate']) ? -1 : 1;
    }

    /**
     * Отсортируем все по rate ASC
     * @return $this
     */
    private function sortRateAscAll()
    {
        foreach ($this->data as $currencyReceiveID => $currencyIn) {
            foreach ($currencyIn as $currencyGiveID => $item) {
                uasort($item, [$this, 'sortRateAsc']);
                $this->data[$currencyReceiveID][$currencyGiveID] = $item;
            }
        }
        return $this;
    }
}
