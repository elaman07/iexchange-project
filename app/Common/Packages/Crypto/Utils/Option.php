<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 09.02.2018, 19:33
 */

namespace App\Common\Packages\Crypto\Utils;


class Option
{
    public static function toFormat($number,$decimal) {
        return number_format($number, $decimal,'.','');
    }
}