<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 10.02.2018, 9:04
 */

namespace App\Common\Packages\Crypto\Utils;



/**
 * Utility class to manipulate strings.
 *
 * @author GuzzleHttp
 */
final class StringUtil
{
    /**
     * Transforms an XML string to an element.
     *
     * @param string $string
     *
     * @throws \RuntimeException
     *
     * @return \SimpleXMLElement
     */
    public static function xmlToElement($string)
    {

        $internalErrors = libxml_use_internal_errors(true);

        try {
            // Allow XML to be retrieved even if there is no response body
            $xml = new \SimpleXMLElement($string ?: '<root />', LIBXML_NONET);
            libxml_use_internal_errors($internalErrors);
        } catch (\Exception $e) {
            libxml_use_internal_errors($internalErrors);

            throw new \RuntimeException('Unable to parse XML data: '.$e->getMessage());
        }

        return $xml;
    }

    /**
     * Transforms a JSON string to an array.
     *
     * @param string $string
     *
     * @throws \RuntimeException
     *
     * @return array
     */
    public static function jsonToArray($string)
    {
        static $jsonErrors = [
            JSON_ERROR_DEPTH => 'JSON_ERROR_DEPTH - Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'JSON_ERROR_STATE_MISMATCH - Underflow or the modes mismatch',
            JSON_ERROR_CTRL_CHAR => 'JSON_ERROR_CTRL_CHAR - Unexpected control character found',
            JSON_ERROR_SYNTAX => 'JSON_ERROR_SYNTAX - Syntax error, malformed JSON',
            JSON_ERROR_UTF8 => 'JSON_ERROR_UTF8 - Malformed UTF-8 characters, possibly incorrectly encoded',
        ];

        $data = json_decode($string, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            $last = json_last_error();
            throw new \RuntimeException(
                'Unable to parse JSON data: '
                .(isset($jsonErrors[$last]) ? $jsonErrors[$last] : 'Unknown error')
            );
        }

        return $data;
    }

    public static function removeNamespaceFromXML( $xml )
    {
        // Because I know all of the the namespaces that will possibly appear in
        // in the XML string I can just hard code them and check for
        // them to remove them
        $toRemove = ['rap', 'turss', 'crim', 'cred', 'j', 'rap-code', 'evic'];
        // This is part of a regex I will use to remove the namespace declaration from string
        $nameSpaceDefRegEx = '(\S+)=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?';

        // Cycle through each namespace and remove it from the XML string
        foreach( $toRemove as $remove ) {
            // First remove the namespace from the opening of the tag
            $xml = str_replace('<' . $remove . ':', '<', $xml);
            // Now remove the namespace from the closing of the tag
            $xml = str_replace('</' . $remove . ':', '</', $xml);
            // This XML uses the name space with CommentText, so remove that too
            $xml = str_replace($remove . ':commentText', 'commentText', $xml);
            // Complete the pattern for RegEx to remove this namespace declaration
            $pattern = "/xmlns:{$remove}{$nameSpaceDefRegEx}/";
            // Remove the actual namespace declaration using the Pattern
            $xml = preg_replace($pattern, '', $xml, 1);
        }

        // Return sanitized and cleaned up XML with no namespaces
        return $xml;
    }

    public static function XMLToArray($xml)
    {
        // One function to both clean the XML string and return an array
        return json_decode(json_encode(simplexml_load_string(static::removeNamespaceFromXML($xml))), true);
    }
}
