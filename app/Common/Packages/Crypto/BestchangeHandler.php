<?php
namespace App\Common\Packages\Crypto;

use App\Common\Packages\Crypto\Utils\Cities;
use App\Common\Packages\Crypto\Utils\Currencies;
use App\Common\Packages\Crypto\Utils\CurrencyCodes;
use App\Common\Packages\Crypto\Utils\Exchangers;
use App\Common\Packages\Crypto\Utils\Rates;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class BestchangeHandler
{
    /**
     * Версия обновления
     */
    private $version = null;

    /**
     * Последнее обновление
     *
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * Массив валют
     *
     * @var Utils\Currencies
     */
    private $currencies;

    /**
     * Массив обменников
     *
     * @var Utils\Exchangers
     */
    private $exchangers;

    /**
     * Массив курсов
     *
     * @var Utils\Rates
     */
    private $rates;

    /**
     * Массив курсов
     *
     * @var Utils\Rates
     */
    private $cities;


    /**
     * Создайте новый экземпляр Bestchange.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $currencies = config('bestchange.bestchange_currencies');
        $exchangers = config('bestchange.bestchange_exchangers');
        $rates      = config('bestchange.bestchange_rates');
        $cities      = config('bestchange.bestchange_cities');

        if(file_exists($currencies)) {
            $this->currencies = new Currencies(
                file_get_contents($currencies)
            );
        }

        if(file_exists($exchangers)) {
            $this->exchangers = new Exchangers(
                file_get_contents($exchangers)
            );
        }

        if(file_exists($rates)) {
            $this->rates = new Rates(
                file_get_contents($rates)
            );
        }

        if(file_exists($cities)) {
            $this->cities = new Cities(
                file_get_contents($cities)
            );
        }
    }

    /**
     * Обновление кодов валют
     *
     * @throws \Exception
     */
    public function refreshCode(): void
    {
        $code_currency = new CurrencyCodes($this->currencies);
        $code_currency->refreshCurrenciesCodes();
    }

    public function getCurrencies()
    {
        return $this->currencies->get();
    }

    public function getAvailableCurrencies()
    {
        return collect($this->currencies->get())->whereIn('id', explode(',',iEXSetting('bestchange_currencies_parsers')))->pluck('name', 'id');
    }

    public function getExchangers()
    {
        return $this->exchangers->get();
    }

    public function getRates()
    {
        return $this->rates->get();
    }

    public function getCities()
    {
        return $this->cities->get();
    }

    /**
     * @param int $currencyGiveID
     * @param int $currencyReceiveID
     * @return mixed
     * @throws \Exception
     */
    public function getRatesFilter(int $currencyGiveID = 0, int $currencyReceiveID = 0): mixed
    {
        return $this->rates->filter($currencyGiveID, $currencyReceiveID);
    }

    /**
     *
     * @param $currency
     * @return mixed
     * @throws \Exception
     */
    public function setRatesPair($currency): mixed
    {
        return $this->rates->filter($currency[0], $currency[1]);
    }

    /**
     *
     * @param $currency
     * @return mixed
     * @throws \Exception
     */
    public function setSearchPair($currency): mixed
    {
        $in = collect($this->getCurrencies())->filter(function($item) use ($currency) {
            return $item['code'] == $currency[0] or $item['code'] == $currency[1];
        })->toArray();


        return $this->rates->filter(Arr::first($in)['id'], Arr::last($in)['id']);
    }

    /**
     * После заверщения всех действий, очищаем временное хранилище
     *
     * @return void
    */
    public function close()
    {
        File::cleanDirectory(storage_path('app/bestchange'));
    }

    public function cache($name = 'currency') {

        switch ($name) {
            case 'currency':
                return json_decode(
                    file_get_contents(
                        storage_path('app/bestchange/currencies.json')
                    ), true
                );
                break;
            case 'codes':
                return json_decode(
                    file_get_contents(
                        storage_path('app/bestchange/codes.json')
                    ), true
                );
                break;
        }
    }

    public function getCachedCurrencies()
    {
        return Cache::remember('bs-currencies', Carbon::now()->addDay(), function() {
            return collect($this->currencies->get())->pluck('name', 'id');
        });
    }

    public function getAvailableCachedCurrencies()
    {
        return Cache::remember('bs-currencies-panel', Carbon::now()->addMinutes(30), function() {
            return collect($this->currencies->get())->whereIn('id', explode(',',iEXSetting('bestchange_currencies_parsers')))->pluck('name', 'id');
        });
    }

    public function getCachedCurrencyData()
    {
        return Cache::remember('bs-currencies-codes', Carbon::now()->addHours(5), function() {
            return collect($this->currencies->get())->whereIn('id', explode(',',iEXSetting('bestchange_currencies_parsers')));
        });
    }

}
