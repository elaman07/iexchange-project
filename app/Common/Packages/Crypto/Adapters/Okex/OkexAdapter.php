<?php
namespace App\Common\Packages\Crypto\Adapters\Okex;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class OkexAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'OKEx';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('-', [$this->baseCurrency, $this->quoteCurrency]);

        $response = $this->cachedParserData->filter(function($item) use($key) {
            return $item['instrument_id'] == $key;
        })->first();

        if(empty($response)) {
            throw new \Exception('Возникли ошибки');
        }

        return $response['last'];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
