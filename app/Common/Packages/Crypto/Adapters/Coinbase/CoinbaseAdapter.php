<?php
namespace App\Common\Packages\Crypto\Adapters\Coinbase;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class CoinbaseAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Coinbase';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromJsonToArray($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = Str::lower(implode('_', [$this->baseCurrency, $this->quoteCurrency]));

        if(isset($this->cachedParserData['data']['rates'][Str::upper($this->quoteCurrency)])) {
            return $this->cachedParserData['data']['rates'][Str::upper($this->quoteCurrency)];
        }

        throw new \Exception('Не найдена пара '. $key);
    }
}
