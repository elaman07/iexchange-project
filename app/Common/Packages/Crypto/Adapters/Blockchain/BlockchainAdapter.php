<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\Blockchain;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class BlockchainAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Blockchain';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        if($this->cachedParserData->has($this->quoteCurrency)) {
            return $this->cachedParserData[$this->quoteCurrency]['last'];
        }

        throw new \Exception('Не найдена пара '. $this->baseCurrency);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
