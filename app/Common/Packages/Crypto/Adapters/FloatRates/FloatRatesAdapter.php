<?php
namespace App\Common\Packages\Crypto\Adapters\FloatRates;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class FloatRatesAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'FloatRates';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./item[targetCurrency="'.$to.'"]');
        $item = $elements[0];
        if (empty($item)) {
            throw new \Exception('Ошибка при получение курса');
        }

        return (float) str_replace(',', '.', (string) $item->exchangeRate);
    }
}
