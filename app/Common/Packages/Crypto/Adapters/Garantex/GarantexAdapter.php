<?php
namespace App\Common\Packages\Crypto\Adapters\Garantex;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class GarantexAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Garantex';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        /// codes
        $codes = [];

        $codes_json = \cache()->remember('garantex-api-stage-codes', Carbon::now()->addWeek(), function() {
            return file_get_contents('https://stage.garantex.biz/api/v2/fees/trading');
        });
        foreach (json_decode($codes_json, true) as $item) {
            $codes[] = $item['market'];
        }


        $array = [];
        foreach ($codes as $item) {
            $array[$item] = Arr::first($this->fromJsonToArray(file_get_contents('https://garantex.org/api/v2/trades?market='.Str::lower($item))));
        }

        $this->cachedParserData = $array;
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $data = $this->cachedParserData[Str::lower($from.$to)];

        if(isset($data['price'])) {
            return $data['price'];
        }

        throw new \Exception('Не найдена пара '. $this->quoteCurrency);
    }
}
