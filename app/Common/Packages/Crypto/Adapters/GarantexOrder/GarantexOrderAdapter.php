<?php
namespace App\Common\Packages\Crypto\Adapters\GarantexOrder;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class GarantexOrderAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'GarantexOrder';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        /// codes
        $codes = [];
        $codes_json = \cache()->remember('garantex-order-api-stage-codes', Carbon::now()->addWeek(), function() {
            return file_get_contents('https://garantex.org/api/v2/markets');
        });

        foreach (json_decode($codes_json, true) as $item) {
            $codes[] = $item['name'];
        }

        $array = [];
        foreach ($codes as $item)
        {
            $implode = explode('/', $item);
            $name = Str::lower(sprintf('%s%s', $implode[0], $implode[1]));
            $depth = $this->fromJsonToArray(file_get_contents('https://garantex.org/api/v2/depth?market='.$name));
            $array[$name] = [
                'sell' => Arr::first($depth['asks'])['price'],
                'buy' => Arr::first($depth['bids'])['price']
            ];
        }

        $this->cachedParserData = $array;
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $data = $this->cachedParserData[Str::lower($from.$to)];

        if(isset($data)) {
            return $data[$this->typePrice];
        }

        throw new \Exception('Не найдена пара '. $this->quoteCurrency);
    }
}
