<?php
namespace App\Common\Packages\Crypto\Adapters\Poloniex;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class PoloniexAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Poloniex';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('_', [$this->baseCurrency, $this->quoteCurrency]);
        if($this->cachedParserData->has($key)) {
            return $this->cachedParserData[$key]['last'];
        }

        throw new \Exception('Не найдена пара '. $this->baseCurrency);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
