<?php
namespace App\Common\Packages\Crypto\Adapters\NationalBankOfRomania;


use App\Common\Packages\Crypto\Adapters\AbstractAdapter;


class NationalBankOfRomaniaAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'NationalBankOfRomania';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
        $this->cachedParserData->registerXPathNamespace('xmlns', 'http://www.bnr.ro/xsd');
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $date = new \DateTime((string) $this->cachedParserData->xpath('//xmlns:PublishingDate')[0]);
        $elements = $this->cachedParserData->xpath('//xmlns:Rate[@currency="'.$from.'"]');
        if (empty($elements) || !$date) {
            throw new \Exception('Ошибка при получение курса');
        }

        $element = $elements[0];
        $rate = (string) $element;
        $rateValue = (!empty($element['multiplier'])) ? $rate / (int) $element['multiplier'] : $rate;

        return (float)$rateValue;
    }
}
