<?php
namespace App\Common\Packages\Crypto\Adapters\WhiteBit;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class WhiteBitAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'WhiteBit';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        if($this->cachedParserData['success'] != true) {
            throw new \Exception('Ошибка при парсинге');
        }

        $pair =  implode('_', [mb_strtoupper($this->baseCurrency), mb_strtoupper($this->quoteCurrency)]);
        $response = $this->cachedParserData['result'][$pair];

        if(isset($response)) {
            $data = $response['ticker']['bid'];
            return $data;
        }
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromJsonToArray($data);
    }
}
