<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\Rapira;

use App\Common\Packages\Crypto\{
    Adapters\AbstractAdapter,
    Utils\StringUtil
};

class RapiraAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'RapiraAdapter';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromJsonToArray($data);
    }

    /**
     * Получаем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = collect($this->cachedParserData['data'])->first(function($item) use($from, $to) {
            return $item['quoteCurrency'] == \Str::upper($from) && $item['baseCurrency'] == \Str::upper($to);
        });

        if (empty($elements)) {
            throw new \Exception('Ошибка при получение курса');
        }

        return (float) $elements['close'];
    }
}
