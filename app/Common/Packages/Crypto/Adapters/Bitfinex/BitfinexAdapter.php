<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\Bitfinex;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class BitfinexAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Bitfinex';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = 't'.implode('', [$this->baseCurrency, $this->quoteCurrency]);

        $response = $this->cachedParserData->filter(function($item) use($key) {
            return $item[0] == $key;
        })->first();

        $column = 'LAST_PRICE';
        if(mb_strtoupper($column) == 'BID') {
            return $response[1];
        } elseif(mb_strtoupper($column) == 'ASK') {
            return $response[3];
        } elseif(mb_strtoupper($column) == 'LAST_PRICE') {
            return $response[7];
        }

        return $response[1];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
