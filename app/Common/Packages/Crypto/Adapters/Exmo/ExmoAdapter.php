<?php
namespace App\Common\Packages\Crypto\Adapters\Exmo;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class ExmoAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Exmo';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('_', [$this->baseCurrency, $this->quoteCurrency]);
        if($this->cachedParserData->has($key)) {
            return $this->cachedParserData[$key]['buy_price'];
        }

        throw new \Exception('Не найдена пара '. $key);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
