<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\RussianCentralBank;

use App\Common\Packages\Crypto\{
    Adapters\AbstractAdapter,
    Utils\StringUtil
};

class RussianCentralBankAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'RussianCentralBank';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./Valute[CharCode="'.$from.'"]');
        $date = \DateTime::createFromFormat('!d.m.Y', (string) $this->cachedParserData['Date']);

        if (empty($elements) || !$date) {
            throw new \Exception('Ошибка при получение курса');
        }

        $rate = str_replace(',', '.', (string) $elements['0']->Value);
        $nominal = str_replace(',', '.', (string) $elements['0']->Nominal);
        return (float) $rate / $nominal;
    }
}
