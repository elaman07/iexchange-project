<?php
namespace App\Common\Packages\Crypto\Adapters\Cryptonex;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class CryptonexAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Cryptonex';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = mb_strtoupper(implode('/', [$this->baseCurrency, $this->quoteCurrency]));
        $value = $this->cachedParserData->filter(function($item) use($key) {
            return $item['alias'] == $key;
        })->first();

        // Если ничего не найдено
        if(empty($value)) {
            throw new \Exception('Не найдена пара '. $key);
        }

        return $value['ask'];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
