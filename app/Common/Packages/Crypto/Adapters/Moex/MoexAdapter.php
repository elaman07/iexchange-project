<?php
namespace App\Common\Packages\Crypto\Adapters\Moex;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class MoexAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Moex';


    protected array $rates = [
        'USDRUB_TOM'    => 'USD-RUB_TOM',
        'EURRUB_TOM'    => 'EUR-RUB_TOM',
        'CNYRUB_TOM'    => 'CNY-RUB_TOM'
    ];

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->priceData($this->fromJsonToArray($data));
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = Str::upper(implode('', [$this->baseCurrency, $this->quoteCurrency]).'_TOM');

        if(isset($this->cachedParserData[$key])) {
            return $this->cachedParserData[$key];
        }

        throw new \Exception('Не найдена пара '. $key);
    }

    /**
     * priceData Converts Price Data into an easy key/value array
     *
     * $array = $this->priceData($array);
     *
     * @param $array array of prices
     * @return array of key/value pairs
     */
    private function priceData(array $array): array
    {
        $prices = [];
        foreach ($array['wap_rates']['data'] as $obj) {
            $prices[$obj[3]] = $obj[4];
        }
        return $prices;
    }
}
