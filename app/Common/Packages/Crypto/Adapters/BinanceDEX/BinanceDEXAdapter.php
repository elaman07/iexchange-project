<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\BinanceDEX;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class BinanceDEXAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'BinanceDEX';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = $this->baseCurrency;
        $value = $this->cachedParserData->filter(function($item) use($key) {
            $symbol = explode('-', $item['baseAssetName']);
            return $symbol[0] == $key;
        })->first();

        // Если ничего не найдено
        if(empty($value) or !isset($value['symbol'])) {
            throw new \Exception('Не найдена пара '. $key);
        }

        return $value['lastPrice'];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
