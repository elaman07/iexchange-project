<?php
namespace App\Common\Packages\Crypto\Adapters\UzbekistanCentralBank;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class UzbekistanCentralBankAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'UzbekistanCentralBank';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./CcyNtry[Ccy="'.$from.'"]');

        if (empty($elements)) {
            throw new \Exception('Ошибка при получение курса');
        }

        $rate = str_replace(',', '.', (string) $elements['0']->Rate);
        $nominal = str_replace(',', '.', (string) $elements['0']->Nominal);
        return (float) $rate / $nominal;
    }
}
