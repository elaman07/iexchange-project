<?php
namespace App\Common\Packages\Crypto\Adapters\CoinStatsFiat;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class CoinStatsFiatAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'CoinStatsFiat';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('', [$this->baseCurrency, $this->quoteCurrency]);

        $response = $this->cachedParserData->filter(function($item) {
            return $item['name'] == $this->quoteCurrency;
        })->first();

        return $response['rate'];
    }
}
