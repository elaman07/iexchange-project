<?php
namespace App\Common\Packages\Crypto\Adapters\Kuna;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class KunaAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Kuna';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = mb_strtolower(implode('', [$this->baseCurrency, $this->quoteCurrency]));
        if($this->cachedParserData->has($key)) {
            return $this->cachedParserData[$key]['ticker']['buy'];
        }

        throw new \Exception('Не найдена пара '. $key);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromJsonToArray($data);
    }
}
