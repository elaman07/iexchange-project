<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\BinanceTicker;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class BinanceTickerAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'BinanceTicker';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('', [$this->baseCurrency, $this->quoteCurrency]);

        if(isset($this->cachedParserData[$key][$this->typePrice])) {
            return $this->cachedParserData[$key][$this->typePrice];
        }

        throw new \Exception('Не найдена пара '. $key);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data, $priceData = true)
    {
        if($priceData) {
            $this->cachedParserData = $this->priceData($this->fromJsonToArray($data));
        } else {
            $this->cachedParserData = $this->fromJsonToArray($data)[$this->typePrice];
        }
    }

    /**
     * priceData Converts Price Data into an easy key/value array
     *
     * $array = $this->priceData($array);
     *
     * @param $array array of prices
     * @return array of key/value pairs
     */
    private function priceData(array $array): array
    {
        $prices = [];
        foreach ($array as $obj) {
            $prices[$obj['symbol']] = [
                'bidPrice' => $obj['bidPrice'],
                'askPrice' => $obj['askPrice'],
            ];
        }
        return $prices;
    }
}
