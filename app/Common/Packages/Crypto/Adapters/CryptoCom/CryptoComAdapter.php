<?php
namespace App\Common\Packages\Crypto\Adapters\CryptoCom;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class CryptoComAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'CryptoCom';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = Str::lower(implode('', [$this->baseCurrency, $this->quoteCurrency]));

        if(isset($this->cachedParserData['data'][$key])) {
            return $this->cachedParserData['data'][$key];
        }

        throw new \Exception('Не найдена пара '. $key);
    }
}
