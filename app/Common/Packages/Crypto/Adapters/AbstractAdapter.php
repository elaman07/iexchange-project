<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters;

use App\Common\Packages\Crypto\Utils\StringUtil;
use App\Jobs\TelegramFailedUpdateCoursesJob;
use GuzzleHttp\Client as GuzzleHttp;
use Illuminate\Config\Repository;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Request as HttpRequest;

/**
 * Abstract class: AbstractAdapter
 *
 * @version 1.0
 * @author iEXExchanger
 */
abstract class AbstractAdapter implements AbstractInterface
{
    /**
     * Название адаптера
     *
     * @var string
    */
    protected string $name;

    /**
     * Массив кэшированных данные
     *
     * @var Collection | array
     */
    protected $cachedParserData;

    /**
     * Массив пар
     *
     * @var array
    */
    protected array $pairs = [];

    /**
     * Базовая валюта.
     *
     * @var string
     */
    protected string $baseCurrency;

    /**
     * Валюта котировки.
     *
     * @var string
     */
    protected string $quoteCurrency;

    /**
     * Значение цены
    */
    protected float $amount;

    protected string|null $typePrice = null;

    /**
     * Конструктор основного класса
     */
    public function __construct()
    {
        // Если в драйвере найден метод, initial, объявлем его
        if(method_exists($this, 'initial')) {
            $this->initial();
        }
    }

    /**
     * Создает валютные пары
     *
     * @param mixed $value
     * @return $this
     */
    public function setPair($value): AbstractAdapter
    {
        if(is_string($value))
            $value = explode(' - ', $value);

        if(count($value) != 2)
            throw new \InvalidArgumentException('Ошибка, пары должны быть из двух значений');

        $this->baseCurrency = $value[0];
        $this->quoteCurrency = $value[1];

        $this->pairs = $value;
        return $this;
    }

    public function setTickerType($value = null): static
    {
        $this->typePrice = $value;
        return $this;
    }

    /**
     * Проверяем, идентичны ли пары
     *
     * @return bool
     */
    public function isIdentical(): bool
    {
        return $this->baseCurrency === $this->quoteCurrency;
    }

    /**
     * Установить новое значение перед конвертацией
     *
     * @param float | integer $amount
     * @return AbstractAdapter
     */
    public function setAmount($amount): AbstractAdapter
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Преобразовываем из JSON в Array
     *
     * @param string $content
     * @return mixed
     */
    public function fromJsonToArray(string $content)
    {
        return StringUtil::jsonToArray($content);
    }

    /**
     * Преобразовываем из HTML в XML
     *
     * @param string $content
     * @return \SimpleXMLElement
     */
    public function fromXmlToElement(string $content): \SimpleXMLElement
    {
        return StringUtil::xmlToElement($content);
    }

    /**
     * Получение результата
     *
     * @return float | integer
     * @throws \Exception
     */
    public function get() {
        return $this->set(...$this->pairs);
    }

    /**
     * Получаем список курсов
     *
     * @return array|Collection
     */
    public function getParsingData()
    {
        return $this->cachedParserData;
    }



    /**
     * Уведомлять о сбоях при обновлении курсов
    */
    public function failedUpdateCourses()
    {
        if(!iEXSetting('telegram_failed_update_courses'))
            return;

        $now = now()->addSeconds(10);
        dispatch(
            (new TelegramFailedUpdateCoursesJob($this->name))->delay($now)->onQueue('low')
        );
    }
}
