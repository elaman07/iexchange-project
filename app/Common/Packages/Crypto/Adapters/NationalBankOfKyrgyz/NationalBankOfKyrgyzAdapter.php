<?php
namespace App\Common\Packages\Crypto\Adapters\NationalBankOfKyrgyz;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class NationalBankOfKyrgyzAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'NationalBankOfKyrgyz';
    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('Currency[@ISOCode="'.$from.'"]');
        $rate = str_replace(',', '.', (string) $elements['0']->Value);
        $nominal = str_replace(',', '.', (string) $elements['0']->Nominal);
        return (float) $rate / $nominal;
    }
}
