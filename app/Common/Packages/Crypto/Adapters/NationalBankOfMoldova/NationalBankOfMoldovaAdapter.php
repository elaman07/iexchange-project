<?php
namespace App\Common\Packages\Crypto\Adapters\NationalBankOfMoldova;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class NationalBankOfMoldovaAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'NationalBankOfMoldova';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./Valute[CharCode="'.$from.'"]');

        if (empty($elements)) {
            throw new \Exception('Ошибка при получение курса');
        }

        $rate = str_replace(',', '.', (string) $elements['0']->Value);
        $nominal = str_replace(',', '.', (string) $elements['0']->Nominal);
        return (float) $rate / $nominal;
    }
}
