<?php
namespace App\Common\Packages\Crypto\Adapters\EuropeanCentralBank;

use App\Common\Packages\Crypto\{
    Adapters\AbstractAdapter
};

class EuropeanCentralBankAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'EuropeanCentralBank';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
        $this->cachedParserData->registerXPathNamespace('xmlns', 'http://www.ecb.int/vocabulary/2002-08-01/eurofxref');
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('//xmlns:Cube[@currency="'.$to.'"]/@rate');
        $date = new \DateTime((string) $this->cachedParserData->xpath('//xmlns:Cube[@time]/@time')[0]);
        if (empty($elements) || !$date) {
            throw new \Exception('Ошибка при получение курса');
        }

        return (string) $elements[0]['rate'];
    }
}
