<?php
namespace App\Common\Packages\Crypto\Adapters\NationalBankOfIsrael;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class NationalBankOfIsraelAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'NationalBankOfIsrael';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./CURRENCY[CURRENCYCODE="'.$from.'"]');
        if (empty($elements)) {
            throw new \Exception('Ошибка при получение курса');
        }

        $rate = str_replace(',', '.', (string) $elements['0']->RATE);
        $nominal = str_replace(',', '.', (string) $elements['0']->UNIT);
        return (float) $rate / $nominal;
    }
}
