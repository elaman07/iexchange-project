<?php
namespace App\Common\Packages\Crypto\Adapters\MexcExchange;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class MexcExchangeAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'MexcExchange';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data)['data']);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = Str::upper(implode('_', [$this->baseCurrency, $this->quoteCurrency]));

        $value = $this->cachedParserData->filter(function($item) use($key) {
            return $item['symbol'] == $key;
        })->first();

        if(empty($value)) {
            throw new \Exception('Не найдена пара '. $key);
        }

        return $value['ask'];
    }

    /**
     * priceData Converts Price Data into an easy key/value array
     *
     * $array = $this->priceData($array);
     *
     * @param $array array of prices
     * @return array of key/value pairs
     */
    private function priceData(array $array): array
    {
        $prices = [];
        foreach ($array as $obj) {
            $prices[$obj['base_token']['symbol']['name'].$obj['quote_token']['symbol']['name']] = $obj['last_price'];
        }
        return $prices;
    }
}
