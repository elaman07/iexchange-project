<?php
namespace App\Common\Packages\Crypto\Adapters;


interface AbstractInterface
{
    public function set($from, $to = null);

    public function get();
}