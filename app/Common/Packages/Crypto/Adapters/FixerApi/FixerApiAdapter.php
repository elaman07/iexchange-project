<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\FixerApi;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class FixerApiAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'FixedApi';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        return $this->cachedParserData['rates'][Str::upper($this->quoteCurrency)];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
