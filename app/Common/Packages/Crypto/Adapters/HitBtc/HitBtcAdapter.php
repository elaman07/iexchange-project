<?php
namespace App\Common\Packages\Crypto\Adapters\HitBtc;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class HitBtcAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'HitBtc';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('', [$this->baseCurrency, $this->quoteCurrency]);
        $value = $this->cachedParserData->filter(function($item) use($key) {
            return $item['symbol'] == $key;
        })->first();

        // Если ничего не найдено
        if(empty($value) or !isset($value['timestamp'])) {
            throw new \Exception('Не найдена пара '. $key);
        }

        return $value['ask'];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
