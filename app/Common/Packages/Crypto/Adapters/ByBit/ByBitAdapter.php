<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\ByBit;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class ByBitAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'ByBit';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('', [$this->baseCurrency, $this->quoteCurrency]);

        if(isset($this->cachedParserData[$key])) {
            return $this->cachedParserData[$key];
        }

        throw new \Exception('Не найдена пара '. $key);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data, $priceData = true)
    {
        if($priceData) {
            $this->cachedParserData = $this->priceData($this->fromJsonToArray($data));
        } else {
            $this->cachedParserData = $this->fromJsonToArray($data);
        }
    }

    /**
     * priceData Converts Price Data into an easy key/value array
     *
     * $array = $this->priceData($array);
     *
     * @param $array array of prices
     * @return array of key/value pairs
     */
    private function priceData(array $array): array
    {
        $array = $array['result']['list'];

        $prices = [];
        foreach ($array as $obj) {
            $prices[$obj['symbol']] = $obj['lastPrice'];
        }
        return $prices;
    }
}
