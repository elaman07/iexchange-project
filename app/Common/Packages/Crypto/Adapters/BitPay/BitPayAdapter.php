<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\BitPay;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class BitPayAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'BitPay';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $data = $this->cachedParserData->filter(function($item) use($to) {
           return $item['code'] == $to;
        })->first();

        if(isset($data['rate'])) {
            return $data['rate'];
        }
        throw new \Exception('Не найдена пара '. $this->quoteCurrency);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data));
    }
}
