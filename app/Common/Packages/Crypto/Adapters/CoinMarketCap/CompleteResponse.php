<?php
namespace App\Common\Packages\Crypto\Adapters\CoinMarketCap;


use App\Models\ParserExchange;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CompleteResponse
{
    /**
     * Фиат
     *
     * @var array
    */
    protected array $fiat = [
        'rub', 'usd', 'uah','cny', 'eur', 'inr', 'kzt', 'pln', 'thb', 'byn', 'aed'
    ];

    /***
     * Собираем данные
     *
     * @return array
     */
    protected $array;

    /**
     * Какую цену высчитываем
     *
     * @return string
     */
    protected $amount = null;

    /***
     * Код валюты
     */
    protected $code = null;

    /**
     * Валюта которую отдает клиент
     *
     * @var array
     */
    protected $in = [];

    /**
     * Валюта в которую будет конвертироваться
     *
     * @var array
     */
    protected $out = [];


    protected $default_parser;

    /***
     * Конструктор
     *
     * @param array $array
     * @param null $parser
     */
    public function __construct(array $array = [], $parser = null, $default_parser = [])
    {
        //Если конвертатор состоит из 2 результатов
        $this->in = Arr::first($array);
        $this->out = Arr::last($array);
        $this->default_parser = $default_parser;

        $this->array = $array;
        $this->code = $parser;
    }

    /***
     * Получение ID курса
     *
     * @return string
     */
    public function getId()
    {
        return $this->in['id'];
    }

    /***
     * Получение Наименовании курса
     *
     * @return string
     */
    public function getName()
    {
        return  $this->in['name'];
    }

    /***
     * Получение Кода курса
     *
     * @return string
     */
    public function getSymbol()
    {
        return $this->in['symbol'];
    }

    /***
     * Получение цены в долларах
     *
     * @return string
     */
    public function getPriceUsd()
    {
        return $this->in['quote']['USD']['price'];
    }

    /***
     * Получение цены в рублях
     *
     * @return string
     */
    public function getPriceRub()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - RUB']);
    }

    /***
     * Получение цены в рублях
     *
     * @return string
     */
    public function getPriceEur()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - EUR']);
    }

    /***
     * Получение цены в CNY
     *
     * @return string
     */
    public function getPriceCny()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - CNY']);
    }

    /***
     * Получение цены в KZT
     *
     * @return string
     */
    public function getPriceKzt()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - KZT']);
    }


    /***
     * Получение цены в INR
     *
     * @return string
     */
    public function getPriceInr()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - INR']);
    }

    /***
     * Получение цены в гривнах
     *
     * @return string
     */
    public function getPriceUah()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - UAH']);
    }

    /***
     * Получение цены в pln
     *
     * @return string
     */
    public function getPricePln()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - PLN']);
    }

    /***
     * Получение цены в thb
     *
     * @return string
     */
    public function getPriceThb()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - THB']);
    }


    /***
     * Получение цены в byn
     *
     * @return string
     */
    public function getPriceByn()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - BYN']);
    }


    /***
     * Получение цены в aed
     *
     * @return string
     */
    public function getPriceAed()
    {
        return ($this->getPriceUsd() * $this->default_parser['USD - AED']);
    }

    /**
     * Получение цены по курсу
     *
     * @param bool $coin
     * @param int $float
     *
     * @return string
     */
    public function getPrice($coin = false, $float = 0)
    {
        if($coin == true) {
            $first = Arr::first($this->array);
            return ($float * $first['quote'][mb_strtoupper($this->code)]['price']);
        }
        else {
            if(in_array(mb_strtolower($this->code), $this->fiat)) {
                $method = 'getPrice'.Str::studly($this->code);
                return number_format($this->{$method}(), 10,'.','');
            }else {
                return number_format($this->getPriceUsd() / $this->out['quote']['USD']['price'], 10,'.','');
            }
        }
    }

    /**
     * Показать весь массив
     *
     * @return array
     */
    public function all() {
        return Arr::first($this->array);
    }
}
