<?php
namespace App\Common\Packages\Crypto\Adapters\CoinMarketCap;


use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use App\Models\ParserApiKey;
use App\Models\ParserExchange;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Psr\Http\Message\ResponseInterface;

class CoinMarketCapAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'CoinMarketCap';

    /**
     * Массив кодов валют с ID
     *
     * @var Collection
     */
    protected Collection $cachedCodes;

    /**
     * Информация о парсеров
     *
     * @var array
    */
    protected array $default_parser;

    /**
     * Инициализация адаптера
     * @throws \Exception
     */
    public function initial()
    {
        // Парсинг кодов
        $codes = File::get(storage_path('app/coinmarketcap.json'));
        $this->cachedCodes = collect($this->fromJsonToArray($codes));
        $this->default_parser = cache()->remember('coinmarketcap-default-rates', Carbon::now()->addSeconds(10), function() {
            return ParserExchange::where('status', '=', 1)
                ->whereNotIn('id_group', [3])->get()->pluck('summa', 'name')->toArray();
        });
    }

    /**
     *
     * @param $from
     * @param $to
     * @return CompleteResponse
     * @throws \Exception
     */
    public function set($from, $to = null): CompleteResponse
    {
        $result = collect();
        $result->push($this->cachedParserData->get($this->filterCodeCurrency($from)));
        $result->push($this->cachedParserData->get($this->filterCodeCurrency($to)));
        return new CompleteResponse($result->all(), $to, $this->default_parser);
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $data = $this->fromJsonToArray($data);

        $result = [];
        foreach ($data['data'] as $value) {
            $result[$value['id']] = [
                'id' => $value['id'],
                'name' => $value['name'],
                'symbol' => $value['symbol'],
                'quote' => $value['quote']
            ];
        }

        $this->cachedParserData = collect($result);

//        $data = collect($this->cachedParserData)->pluck('id', 'symbol')->toArray();
//        file_put_contents(storage_path('app/coinmarketcap.json'), json_encode($data));
    }

    /**
     * Получение ID валют по коду
     *
     * @param $code
     * @return mixed
     */
    protected function filterCodeCurrency($code) {
        return $this->cachedCodes->has($code) ? $this->cachedCodes->get($code) : null;
    }

    /**
     * Получение результата
     *
     * @return float | integer
     * @throws \Exception
     */
    public function get()
    {
        return $this->set(...$this->pairs)->getPrice();
    }
}
