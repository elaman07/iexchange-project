<?php
namespace App\Common\Packages\Crypto\Adapters\NationalBankOfKazakhstan;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class NationalBankOfKazakhstanAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'NationalBankOfKazakhstan';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./channel/item[title="'.$from.'"]');
        if (empty($elements)) {
            throw new \Exception('Ошибка при получение курса');
        }

        $rate = str_replace(',', '.', (string) $elements['0']->description);
        return (float) $rate;
    }
}
