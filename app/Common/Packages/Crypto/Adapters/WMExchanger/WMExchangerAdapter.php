<?php
namespace App\Common\Packages\Crypto\Adapters\WMExchanger;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class WMExchangerAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'WmExchanger';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float
     * @throws \Exception
     */
    public function set($from, $to = null): float
    {
        $response = collect($this->cachedParserData['response'])
            ->filter(function($item) {
                return $item['Direct'] == implode(' - ', [$this->baseCurrency, $this->quoteCurrency]);
            })->first();

        $BaseRate = mb_substr($response['BaseRate'], 1);

        if(empty($BaseRate))
            throw new \Exception('Пара не найдена');
        return $BaseRate;
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromJsonToArray($data);
    }
}
