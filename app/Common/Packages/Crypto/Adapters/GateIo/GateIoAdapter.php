<?php
namespace App\Common\Packages\Crypto\Adapters\GateIo;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class GateIoAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'GateIo';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromJsonToArray($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = Str::lower(implode('_', [$this->baseCurrency, $this->quoteCurrency]));

        if(isset($this->cachedParserData[$key])) {
            return $this->cachedParserData[$key]['last'];
        }

        throw new \Exception('Не найдена пара '. $key);
    }
}
