<?php
namespace App\Common\Packages\Crypto\Adapters\CoinStats;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;
use Illuminate\Support\Str;

class CoinStatsAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'CoinStats';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->priceData($this->fromJsonToArray($data));
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = Str::upper(implode('', [$this->baseCurrency, $this->quoteCurrency]));

        if(isset($this->cachedParserData[$key])) {
            return $this->cachedParserData[$key];
        }

        throw new \Exception('Не найдена пара '. $key);
    }

    /**
     * priceData Converts Price Data into an easy key/value array
     *
     * $array = $this->priceData($array);
     *
     * @param $array array of prices
     * @return array of key/value pairs
     */
    private function priceData(array $array): array
    {
        $prices = [];
        foreach ($array['coins'] as $obj) {
            $prices[$obj['symbol'].'USD'] = $obj['price'];
        }
        return $prices;
    }
}
