<?php
declare(strict_types=1);

namespace App\Common\Packages\Crypto\Adapters\Bitmart;


use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class BitmartAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'Bitmart';

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param $to
     * @return float | string
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $key = implode('_', [$this->baseCurrency, $this->quoteCurrency]);
        $response = $this->cachedParserData->filter(function($item) use($key) {
            return $item['symbol'] == $key;
        })->first();

        if(empty($response)) {
            throw new \Exception('Возникли ошибки');
        }
        return $response['last_price'];
    }

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = collect($this->fromJsonToArray($data)['data']['tickers']);
    }
}
