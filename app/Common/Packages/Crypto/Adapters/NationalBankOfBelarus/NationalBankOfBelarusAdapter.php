<?php
namespace App\Common\Packages\Crypto\Adapters\NationalBankOfBelarus;

use App\Common\Packages\Crypto\Adapters\AbstractAdapter;

class NationalBankOfBelarusAdapter extends AbstractAdapter
{
    /**
     * Название адаптера
     *
     * @var string
     */
    protected string $name = 'NationalBankOfBelarus';

    /**
     * Загрузка данных в хранилище
     *
     * @return void
     * @throws \Exception
     */
    public function loadRates($data)
    {
        $this->cachedParserData = $this->fromXmlToElement($data);
    }

    /**
     * Получем необходимые ответы
     *
     * @param $from
     * @param null $to
     * @return mixed
     * @throws \Exception
     */
    public function set($from, $to = null)
    {
        $elements = $this->cachedParserData->xpath('./Currency[CharCode="'.$from.'"]');

        if (empty($elements)) {
            throw new \Exception('Ошибка при получение курса');
        }

        $rate = str_replace(',', '.', (string) $elements['0']->Rate);
        return (float) $rate;
    }
}
