<?php
namespace App\Common\Packages\BackupCode;


use App\Models\BackupCodeModel;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

trait BackupCodeHandler
{
    /**
     * Ключ сессии
     *
     * @var string
    */
    protected $key = 'iex-backup-secret';

    /**
     * Код для проверки неудачных попыток
     *
     * @var string
    */
    protected $throttleSuffix = '::code2fa';

    /**
     * Получаем детали кэшированных резервных кодов
     *
     * @param Request $request
     * @return mixed
     */
    public function getCurrentCode(Request $request)
    {
        return \Cache::tags('backup-code'.$request->user()->id)
            ->remember('code-user-'.$request->user()->id, Carbon::now()->addMinutes(20), function() use($request) {
            return BackupCodeModel::where('id_user', $request->user()->id)->inRandomOrder()->first();
        });
    }

    /**
     * Записываем данные в базу и в сессиюю
     *
     * @param Request $request
     */
    public function insertCode(Request $request)
    {
        // Записываем код в базу
        $secret = hash('sha256', $request->get('code').'-'.$request->user()->id);
        // Обновляем ключ в базе
        $request->user()->update(['backup_code_secret' => $secret]);

        // После успешной авторизации, очищаем кэш
        \Cache::tags('backup-code'.$request->user()->id)->flush();

        // Записываем в сессию
        \Session::put($this->key.$request->user()->id, $secret);
    }

    /**
     * Проверка полученного кода
     *
     * @param Request $request
     * @return bool
     */
    public function checkedCode(Request $request)
    {
        $code = \Cache::tags('backup-code'.$request->user()->id)->get('code-user-'.$request->user()->id);

        if(!$code) return false;
        return $code->code == $request->get('code');
    }

    /**
     * Определяем, есть ли у пользователя слишком много неудачных попыток ввода резервного кода.
     *
     * @param  \Illuminate\Http\Request $request
     * @param RateLimiter $limiter
     * @return bool
     */
    private function hasTooManyRecoveryAttempts(Request $request, RateLimiter $limiter)
    {
        return $limiter->tooManyAttempts(
            $this->throttleKey($request), $this->maxAttempts()
        );
    }

    /**
     * В случае множественных неудачных вводов, блокируем все действия
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return string
     */
    protected function sendLockoutResponse(Request $request, RateLimiter $limiter)
    {
        $availableAt = now()->addSeconds(
            $limiter->availableIn($this->throttleKey($request))
        )->ago();
        return sprintf("Слишком много попыток ввода. Пожалуйста, попробуйте еще раз %s ", $availableAt);
    }

    /**
     * Увеличиваем количество неудачных попыток ввода резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     */
    protected function incrementAttempts(Request $request, RateLimiter $limiter) {
        $limiter->hit(
            $this->throttleKey($request), 320 * 60
        );
    }

    /**
     * Очищаем попытки ввода резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     */
    protected function clearAttempts(Request $request, RateLimiter $limiter)
    {
        $limiter->clear(
            $this->throttleKey($request)
        );
    }

    /**
     * Ключ, для проверки кол-во неудачно введенных кодов
     *
     * @param Request $request
     * @return string
     */
    private function throttleKey(Request $request)
    {
        return $request->ip() . '|' . $request->user()->id . $this->throttleSuffix;
    }

    /**
     * Макс. количество неудачных попыток ввода резервного кода
     *
     * @return integer
     */
    protected function maxAttempts()
    {
        return 5;
    }
}