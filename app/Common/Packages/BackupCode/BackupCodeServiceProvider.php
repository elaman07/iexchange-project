<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 23.10.2019
 * Time: 19:00
 */

namespace App\Common\Packages\BackupCode;


use Illuminate\Support\ServiceProvider;

class BackupCodeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('backup-codes', function() {
            return new BackupCode();
        });
    }
}