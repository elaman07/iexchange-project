<?php
namespace App\Common\Packages\BackupCode;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BackupCode implements BackupCodeInterface
{
    /**
     * Список созданных кодов
     *
     * @var array
    */
    protected $codes = [];

    /**
     * Длина кодов
     *
     * @var integer
    */
    protected $length = 8;

    /**
     * Количество кодов
     *
     * @var integer
    */
    protected $count = 5;

    /**
     * Получение префикса для кодов
     *
     * @var string
    */
    protected $prefix = null;

    /**
     * Конструктор резервных кодов
     *
     * @return mixed
    */
    public function __construct()
    {
        $this->codes = collect();
    }

    /**
     * Создаем коды
     *
     * @return BackupCodeInterface
    */
    public function generate(): BackupCodeInterface
    {
        for($i = 1; $i <= $this->count; $i++) {
            $this->codes->put($i, mb_strtoupper($this->prefix.Str::random($this->length)));
        }

        return $this;
    }

    /**
     * Указываем количество отображаемых кодов
     *
     * @param int $num
     * @return BackupCodeInterface
     */
    public function setCount(int $num): BackupCodeInterface
    {
        $this->count = $num;
        return $this;
    }

    /**
     * Указываем длину кодов
     *
     * @param int $num
     * @return BackupCodeInterface
     */
    public function setLength(int $num): BackupCodeInterface
    {
        $this->length = $num;
        return $this;
    }

    /**
     * Указываем префикс для кодов
     *
     * @param string $str
     * @return BackupCodeInterface
     */
    public function setPrefix(string $str): BackupCodeInterface
    {
        $this->prefix = $str;
        return $this;
    }

    /**
     * Получить коды в виде массива
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->codes->toArray();
    }

    /**
     * Получить коды в виде JSON
     *
     * @return array
     */
    public function toJson()
    {
        return $this->codes->toJson();
    }
}
