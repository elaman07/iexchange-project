<?php
namespace App\Common\Packages\BackupCode\Facades;

use Illuminate\Support\Facades\Facade;

class BackupCodeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'backup-codes';
    }
}
