<?php
namespace App\Common\Packages\BackupCode;


interface BackupCodeInterface
{
    /**
     * Создаем коды
     *
     * @return BackupCodeInterface
     */
    public function generate();

    /**
     * Указываем количество отображаемых кодов
     *
     * @param int $num
     * @return BackupCodeInterface
     */
    public function setCount(int $num);

    /**
     * Указываем длину кодов
     *
     * @param int $num
     * @return BackupCodeInterface
     */
    public function setLength(int $num);

    /**
     * Указываем префикс для кодов
     *
     * @param string $str
     * @return BackupCodeInterface
     */
    public function setPrefix(string $str): BackupCodeInterface;

    /**
     * Получить коды в виде массива
     *
     * @return array
     */
    public function toArray();

    /**
     * Получить коды в виде JSON
     *
     * @return string
     */
    public function toJson();
}