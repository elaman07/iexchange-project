<?php

namespace App\Common\Packages\ReferralSystem;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralProgram;
use App\Models\Task;
use App\Models\User;
use App\Models\UserBalance;

class ReferralSystem
{
    /**
     * Регистрация нового клиента в реферальной программе
     *
     * @param User $user
     * @param string|null $referral_hash
     */
    public function register(User $user, string $referral_hash = null): void
    {

        if($referral_hash != null)
        {
            // Данные по реферальной ссылке
            $link = ReferralLink::whereRaw("BINARY `code` = ?", [security_xss($referral_hash)])->first();
            if (!empty($link))
            {
                $link->relationships()->create(['user_id' => $user->id]);

                // Записываем лог нового реферала
                $text = 'Новый клиент через партнера '.$link->user->name;
                add_to_referral_log($text, 1, $link->id, $user->id);
            }
        }


        // Получаем реферальную программу (по умолчанию для новых клиентов)
        $referral = ReferralProgram::whereIsReg(1)->first();
        if(!empty($referral))
        {
            ReferralLink::create([
                'user_id' => $user->id,
                'referral_program_id' => $referral->id
            ]);
        }

        //Создаем баланс для нового клиента
        $exists = UserBalance::whereIdUser($user->id)->exists();
        if(!$exists) {
            UserBalance::create([
                'id_user' => $user->id,
                'id_code_currency' => config('crypto.currency_id')
            ]);
        }
    }

    /**
     * Обработка и получение бонусов
    */
    public function builder(Task $task): ReferralSystemBuilder
    {
        return new ReferralSystemBuilder($task);
    }
}
