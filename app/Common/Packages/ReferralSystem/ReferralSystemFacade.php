<?php

namespace App\Common\Packages\ReferralSystem;

use Illuminate\Support\Facades\Facade;

class ReferralSystemFacade extends Facade
{
    /**
     * Получить зарегистрированное имя компонента.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'referral_system';
    }
}
