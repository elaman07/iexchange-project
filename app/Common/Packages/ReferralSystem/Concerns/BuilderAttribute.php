<?php

namespace App\Common\Packages\ReferralSystem\Concerns;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Models\User;
use Illuminate\Support\Str;

trait BuilderAttribute
{
    private User $client;
    private ?User $partner_client;

    /**
     * Бонусный процент
     *
     * @var float
     */
    private float $currentPercent = 0;

    /**
     * Фиксированная выплата
     *
     * @var float
     */
    private float $fixedBonus = 0;

    /**
     * Калькулятор для расчета бонусов
     *
     * @param string|null $convert_code
     * @return float
     */
    protected function calculator(string $convert_code = null): float
    {
        if(is_null($convert_code)) {
            $convert_code = $this->getCurrencyBonus();
        }

        // Определение "Суммы обмена"
        $price = (float)$this->task->receiving_price_default;
        $parser_summa = $this->getOutCodeCurrency()->internal_rate;
        $parser_exchange = $this->getOutCodeCurrency()->parser_exchange;
        $add_to_course = $this->getOutCodeCurrency()->add_to_course;

        if($this->getOutCodeCurrency()->id_parser_formula > 0)
        {
            $parser_exchange = $this->getOutCodeCurrency()->parser_formula; // Курсы по формуле
            $add_to_course = $this->getOutCodeCurrency()->add_to_course_formula;
        }
        $code_name = $this->getOutCodeCurrency()->name;

        if((int)iEXSetting('referral_system_type_exchange') == 0)
        {
            $price = $this->task->give_price_with_comm;
            $parser_summa = $this->getInCodeCurrency()->internal_rate;

            $parser_exchange = $this->getInCodeCurrency()->parser_exchange;
            $add_to_course = $this->getInCodeCurrency()->add_to_course;
            if($this->getInCodeCurrency()->id_parser_exchange == 0) {
                $parser_exchange = $this->getInCodeCurrency()->parser_formula;
                $add_to_course = $this->getInCodeCurrency()->add_to_course_formula;
            }

            $code_name = $this->getInCodeCurrency()->name;
            // Партнерам будет начисляться бонусы в зависимости от суммы "Получаю"
            // Берем курс из источников + добавляем к курсу
            if(isset($parser_exchange)) {
                $parser_summa = $this->addCommissionDynamicAuto($add_to_course, $parser_exchange->summa, $this->getInCurrency()->number_format);
            }

        } elseif((int)iEXSetting('referral_system_type_exchange') == 1) {
            $price = $this->task->give_price_with_comm_pay;
            $parser_summa = $this->getInCodeCurrency()->internal_rate;

            $parser_exchange = $this->getInCodeCurrency()->parser_exchange;
            $add_to_course = $this->getInCodeCurrency()->add_to_course;
            if($this->getInCodeCurrency()->id_parser_formula > 0) {
                $parser_exchange = $this->getInCodeCurrency()->parser_formula;
                $add_to_course = $this->getInCodeCurrency()->add_to_course_formula;
            }

            $code_name = $this->getInCodeCurrency()->name;

            // Партнерам будет начисляться бонусы в зависимости от суммы "Получаю"
            // Берем курс из источников + добавляем к курсу
            if(isset($parser_exchange)) {
                $parser_summa = $this->addCommissionDynamicAuto($add_to_course, $parser_exchange->summa, $this->getInCurrency()->number_format);
            }

        } elseif((int)iEXSetting('referral_system_type_exchange') == 2) {
            $price = $this->task->give_price_default;
            $parser_summa = $this->getInCodeCurrency()->internal_rate;

            $parser_exchange = $this->getInCodeCurrency()->parser_exchange;
            $add_to_course = $this->getInCodeCurrency()->add_to_course;
            if($this->getInCodeCurrency()->id_parser_formula > 0) {
                $parser_exchange = $this->getInCodeCurrency()->parser_formula;
                $add_to_course = $this->getInCodeCurrency()->add_to_course_formula;
            }

            $code_name = $this->getInCodeCurrency()->name;

            // Партнерам будет начисляться бонусы в зависимости от суммы "Получаю"
            // Берем курс из источников + добавляем к курсу
            if(isset($parser_exchange)) {
                $parser_summa = $this->addCommissionDynamicAuto($add_to_course, $parser_exchange->summa, $this->getInCurrency()->number_format);
            }

        }elseif((int)iEXSetting('referral_system_type_exchange') == 3)
        {
            $price = $this->task->receiving_price_with_comm;
            $parser_summa = $this->getOutCodeCurrency()->internal_rate;

            $parser_exchange = $this->getOutCodeCurrency()->parser_exchange;
            $add_to_course = $this->getOutCodeCurrency()->add_to_course;
            if($this->getOutCodeCurrency()->id_parser_formula > 0) {
                $parser_exchange = $this->getOutCodeCurrency()->parser_formula;
                $add_to_course = $this->getOutCodeCurrency()->add_to_course_formula;
            }

            $code_name = $this->getOutCodeCurrency()->name;

            // Партнерам будет начисляться бонусы в зависимости от суммы "Получаю"
            // Берем курс из источников + добавляем к курсу
            if(isset($parser_exchange)) {
                $parser_summa = $this->addCommissionDynamicAuto($add_to_course, $parser_exchange->summa, $this->getOutCurrency()->number_format);
            }

        } elseif((int)iEXSetting('referral_system_type_exchange') == 4)
        {
            $price = $this->task->receiving_price_with_comm_pay;
            $parser_summa = $this->getOutCodeCurrency()->internal_rate;

            $parser_exchange = $this->getOutCodeCurrency()->parser_exchange;
            $add_to_course = $this->getOutCodeCurrency()->add_to_course;
            if($this->getOutCodeCurrency()->id_parser_formula > 0) {
                $parser_exchange = $this->getOutCodeCurrency()->parser_formula;
                $add_to_course = $this->getOutCodeCurrency()->add_to_course_formula;
            }

            $code_name = $this->getOutCodeCurrency()->name;

            // Партнерам будет начисляться бонусы в зависимости от суммы "Получаю"
            // Берем курс из источников + добавляем к курсу
            if(isset($parser_exchange)) {
                $parser_summa = $this->addCommissionDynamicAuto($add_to_course, $parser_exchange->summa, $this->getOutCurrency()->number_format);
            }

        } elseif((int)iEXSetting('referral_system_type_exchange') == 5) {
            $price = $this->task->receiving_price_default;
            $parser_summa = $this->getOutCodeCurrency()->internal_rate;

            $parser_exchange = $this->getOutCodeCurrency()->parser_exchange;
            $add_to_course = $this->getOutCodeCurrency()->add_to_course;
            if($this->getOutCodeCurrency()->id_parser_formula > 0) {
                $parser_exchange = $this->getOutCodeCurrency()->parser_formula;
                $add_to_course = $this->getOutCodeCurrency()->add_to_course_formula;
            }

            $code_name = $this->getOutCodeCurrency()->name;

            // Партнерам будет начисляться бонусы в зависимости от суммы "Получаю"
            // Берем курс из источников + добавляем к курсу
            if(isset($parser_exchange)) {
                $parser_summa = $this->addCommissionDynamicAuto($add_to_course, $parser_exchange->summa, $this->getOutCurrency()->number_format);
            }
        }

        // Если в дополнительных возможностях установлено 0 или отключена,
        // в этом случае система будет рассчитывать бонусы автоматически
        if($parser_summa == 0) {
            return (float)convert_to_currency(Str::upper($code_name), Str::upper($convert_code), (float)$price);
        }

        return (float)($parser_summa * $price);
    }

    /**
     * Записываем данные клиента
     *
     * @param User $user
     * @return static
     */
    protected function setClient(User $user): static
    {
        $this->client = $user;
        return $this;
    }

    /**
     * Получаем данные клиента
     *
     * @return User
     */
    protected function getClient(): User
    {
        return $this->client;
    }

    /**
     * Записываем данные пользователя (партнера)
     *
     * @param User|null $user
     * @return static
     */
    protected function setPartnerClient(?User $user): static
    {
        $this->partner_client = $user;
        return $this;
    }

    /**
     * Получаем данные пользователя (партнера)
     *
     * @return User
     */
    protected function getPartnerClient(): User
    {
        return $this->partner_client;
    }

    /**
     * Получаем информацию по партнерской ссылке (партнера)
     *
     * @return ReferralLink|null
     */
    public function getReferralPartner(): ReferralLink|null
    {
        return ReferralLink::whereCode($this->task->referral_hash)->first();
    }


    /**
     * Записываем процент по выплате
     *
     * @param float $percent
     * @return static
     */
    private function setCurrentPercent(float $percent = 0): static
    {
        $this->currentPercent = $percent;
        return $this;
    }

    /**
     * Получаем текущий процент по выплате
     *
     * @return float
     */
    public function getCurrentPercent(): float
    {
        return $this->currentPercent;
    }

    /**
     * Устанавливаем фиксированный бонус
     *
     * @param float $value
     * @return static
     */
    private function setFixedBonus(float $value = 0): static
    {
        $this->fixedBonus = $value;
        return $this;
    }

    /**
     * Получаем фиксированный бонус
     *
     * @return float
     */
    public function getFixedBonus(): float
    {
        return $this->fixedBonus;
    }

    /**
     * Получение процента на текущий тарифный план
     *
     * @return float
     */
    public function getPercent(): float
    {
        $link = $this->getReferralPartner();

        if($this->directionExchange->max_percent_partner > 0 and $link->program->percent > $this->directionExchange->max_percent_partner)
            return $this->directionExchange->max_percent_partner;

        return $link->program->percent;
    }

    /**
     * Получаем итоговый бонус, которую получит партнер за обмен
     *
     * @return float
     */
    protected function getAmount(): float
    {
        // Проверяем, доступны ли выплаты бонусов по обменному направлению
        if($this->isNotPartner())
            return floor(0);

        // Если указана фиксированная сумма, исключаем проценты
        if($this->getFixedPayout() > 0) {
            $amount = iex_number_format($this->getFixedPayout(), 2);
            $this->setFixedBonus($this->getFixedPayout());
        } else
        {
            if(!empty($this->getPartnerClient()) and (float)$this->getPartnerClient()->personal_ref_discount > 0) { // Персональный партнерский процент
                $percent = (float)$this->getPartnerClient()->personal_ref_discount;
            } else {
                $percent = ($this->getIndividualPercentage() > 0 ? $this->getIndividualPercentage() : $this->getPercent());
            }

            if($this->getPartnerClient()->max_ref_discount > 0 and $percent > $this->getPartnerClient()->max_ref_discount) {
                $this->setCurrentPercent($this->getPartnerClient()->max_ref_discount);
            }


            // Записываем процент выплаты
            $this->setCurrentPercent($percent);

            if((int)iEXSetting('type_partner_deductions') == 1) {
                $amount = iex_number_format(($this->calculator() * $this->getProfitPartner() / 100) * ($percent / 100), 2);
            } else {
                $amount = iex_number_format($this->calculator() * ($percent / 100), 2);
            }
        }

        // Минимальная и максимальная сумма выплат
        if($this->getMinimumPayout() > 0 and $this->getMinimumPayout() >= $amount or $this->getMaximumPayout() > 0 and $this->getMaximumPayout() < $amount) {
            // Если превышает максимальную сумму.
            if($this->getMaximumPayout() > 0 and $this->getMaximumPayout() < $amount) {
                return $this->getMaximumPayout();
            }

            return 0;
        }

        return (float)$amount;
    }

    /**
     * Получение сумму бонуса в строковом формате
     *
     * @return string
     */
    public function getAmountString(): string
    {
        // Отображения бонуса
        $view_bonus = sprintf('%s %s', $this->getAmount(), $this->getCurrencyBonus(true));
        if(config('crypto.currency_payout_position') == 'left')
            $view_bonus = sprintf('%s %s',$this->getCurrencyBonus(true), $this->getAmount());
        return $view_bonus;
    }
}
