<?php

namespace App\Common\Packages\ReferralSystem\Concerns;

use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Models\UserBalance;
use Illuminate\Support\Carbon;

trait BuilderDetails
{
    public function getInCurrency()
    {
        return $this->directionExchange->currency1;
    }

    public function getOutCurrency()
    {
        return $this->directionExchange->currency2;
    }

    public function getInPayment()
    {
        return $this->getInCurrency()->payment;
    }

    public function getOutPayment()
    {
        return $this->getOutCurrency()->payment;
    }

    public function getInCodeCurrency()
    {
        return $this->getInCurrency()->code_currency;
    }

    public function getOutCodeCurrency()
    {
        return $this->getOutCurrency()->code_currency;
    }


    /**
     * Разбираем и обрабатываем значение с авто определением
     *
     * @param string $value
     * @param $summa
     * @param int $number_format
     * @return float
     */
    public function addCommissionDynamicAuto(string $value, $summa, $number_format = 8): float
    {
        $value_number = preg_replace('/[^\d.]/', '', $value); // Получаем число
        $char_first = mb_substr($value, 0, 1); // Получение первого символа
        $char_last = mb_substr($value, -1); // Получение последнего символа

        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        $first_summa = $summa;
        if(\mb_substr(iex_number_format($first_summa, $number_format), 0, 2) == '0') {
            $summa = 1 / $summa;
        }

        $value_n = $value_number;
        if($char_last == '%') {
            $value_n = ($value_number > 0) ? ($value_number / 100) * $summa : 0;
        }


        // Умножение
        if($char_first == '*') {
            $summa = $summa * $value_n;
        }

        // Деление
        if($char_first == '/') {
            $summa = $summa / $value_n;
        }

        // Вычитание
        if($char_first == '-') {
            $summa = $summa - $value_n;
        }

        if(!in_array($char_first, ['*', '/', '-'])) {
            $summa = $summa + $value_n;
        }

        if(\mb_substr(iex_number_format($first_summa, $number_format), 0, 2) == '0') {
            $summa = (1 / $summa);
        }

        return $summa;
    }

    /**
     * Минимальная сумма бонусов
     *
     * @return bool
     */
    public function hasNotMinAndMaxBonus()
    {
        $min = (float)iEXSetting('referral_min_bonus');
        $max = (float)iEXSetting('referral_max_bonus');

        if($min > 0 and $this->getAmount() > $min or $max > 0 and $max < $this->getAmount())
            return false;
        return true;
    }

    /**
     * Выплата бонусов по расписанию
     */
    public function hasRangeBonus() {

        $from = iEXSetting('referral_bonus_from');
        $to = iEXSetting('referral_bonus_to');

        if(!iEXSetting('referral_bonus_range'))
            return true;

        if(Carbon::now()->format('H:i') > $from and Carbon::now()->format('H:i') < $to)
            return true;

        return false;
    }

    /**
     * Суточный лимит бонусов
     *
     * @return bool
     */
    public function hasBonusLimitDay()
    {
        $log = ReferralLog::where('id_user', '=', $this->getReferralPartner()->id)
            ->whereDate('created_at', Carbon::today())->sum('bonus_number');
        $amount = $log + $this->getAmount();
        return (float)iEXSetting('referral_bonus_limit_day') > 0 and $amount > (float)iEXSetting('referral_bonus_limit_day');
    }

    /**
     * Месячный лимит бонусов
     *
     * @return bool
     */
    public function hasBonusLimitMonth()
    {
        $log = ReferralLog::where('id_user', '=', $this->getReferralPartner()->id)
            ->whereBetween('updated_at', [
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
            ])->sum('bonus_number');
        $amount = $log + $this->getAmount();
        return (float)iEXSetting('referral_bonus_limit_day') > 0 and $amount > (float)iEXSetting('referral_bonus_limit_month');
    }

    /**
     * Получаем фиксированную сумму выплаты в валюте
     *
     * @return float
     */
    public function getFixedPayout(): float  {
        return ($this->directionExchange->fixed_payout ?? 0);
    }

    /**
     * Выплачивать партнерские бонусы
     *
     * @return int
     */
    public function isNotPartner() : int {
        return $this->directionExchange->is_not_partner;
    }

    /**
     * Получение индивидуального процента
     *
     * @return float
     */
    public function getIndividualPercentage(): float {
        return ($this->directionExchange->individual_percentage ?? 0);
    }

    /**
     * Получаем % от прибыли обменника
     *
     * @return float
     */
    public function getProfitPartner(): float  {
        return ($this->directionExchange->profit_partner ?? 0);
    }

    /**
     * Минимальная сумма выплаты
     *
     * @return float
     */
    public function getMinimumPayout(): float  {
        return ($this->directionExchange->minimum_payout ?? 0);
    }

    /**
     * Максимальная сумма выплаты
     *
     * @return float
     */
    public function getMaximumPayout(): float  {
        return ($this->directionExchange->maximum_payout ?? 0);
    }

    /**
     * Получаем код валюты, в которую будут конвертироваться бонусы
     *
     * @param bool $is_read
     * @return string
     */
    public function getCurrencyBonus(bool $is_read = false): string
    {
        if($is_read)
            return config('crypto.currency_payout_sign');
        return config('crypto.currency_payout');
    }

    /**
     * Получение баланса рефералла
     *
     * @return float
     */
    public function getReferralBalance(): float
    {
        $balance = UserBalance::where('id_user', $this->getPartnerClient()->id)->first();
        return $balance->balance;
    }

    /**
     * Обновление баланса рефералла
     *
     * @return void
     */
    public function updateReferralBalance()
    {
        UserBalance::where('id_user', $this->getPartnerClient()->id)
            ->update(['balance' => iex_number_format($this->getReferralBalance() + $this->getAmount())]);
    }
}
