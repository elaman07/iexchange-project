<?php
namespace App\Common\Packages\ReferralSystem\Models;

use App\Models\Filters\ReferralLogFilter;
use App\Models\Task;
use App\Models\User;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ReferralLog extends Model
{
    use Filterable;

    /**
     * Название таблицы
     *
     * @var string
    */
    protected $table = 'referral_log';

    /**
     * Определение свойств колонок
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'id_referral',
        'id_referral_link',
        'text',
        'bonus',
        'bonus_number',
        'id_task',
        'fixed_bonus',
        'current_percent'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(ReferralLogFilter::class);
    }

    /**
     * Информация о пользователе
    */
    public function user() : HasOne {
        return $this->hasOne(User::class,'id','id_referral');
    }

    public function user_admin() : HasOne {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id', 'id_task');
    }
}
