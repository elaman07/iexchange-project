<?php
namespace App\Common\Packages\ReferralSystem\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;


class ReferralProgram extends Model
{
    use SoftDeletes, HasTranslations;

    /**
     * Определение свойств колонок
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lifetime_minutes',
        'title',
        'description',
        'count',
        'percent',
        'is_reg',
        'style_width'
    ];

    protected $translatable = [
        'title',
        'description'
    ];

    /**
     * Колонка с датой удаленных таблиц
     *
     * @return array
    */
    protected $dates = ['deleted_at'];

    /**
     * Ссылается на реферальную программу
     *
     * @return HasMany
    */
    public function links()
    {
        return $this->hasMany(ReferralLink::class, 'referral_program_id', 'id');
    }
}
