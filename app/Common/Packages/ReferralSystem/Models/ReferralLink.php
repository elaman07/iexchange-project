<?php
namespace App\Common\Packages\ReferralSystem\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Ramsey\Uuid\Uuid;

class ReferralLink extends Model
{
    protected $table = 'referral_links';

    /**
     * Определение свойств колонок
     *
     * @var array
    */
    protected $fillable = [
        'user_id',
        'referral_program_id',
        'code'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function (ReferralLink $model) {
            $model->generateCode();
        });
    }

    private function generateCode() {
        $this->code = \Hashids::encode($this->getAttribute('user_id'));
    }

    public static function getReferral($user, $program)
    {
        return static::where([
            'user_id' => $user->id,
            'referral_program_id' => $program->id
        ])->first();
    }

    public function getLinkAttribute()
    {
        return url(config('app.url')) . '/?ref=' . $this->code;
    }

    public function getHashAttribute()
    {
        return $this->code;
    }

    /**
     * Информация о пользователе
    */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Детали реферальной программы
    */
    public function program() : BelongsTo
    {
        return $this->belongsTo(ReferralProgram::class, 'referral_program_id');
    }

    /**
     * Список рефералов
    */
    public function relationships() : HasMany
    {
        return $this->hasMany(ReferralRelationship::class);
    }
}
