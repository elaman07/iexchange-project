<?php
namespace App\Common\Packages\ReferralSystem\Models;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ReferralRelationship extends Model
{
    protected $table = 'referral_relationships';

    /**
     * Определение свойств колонок
     *
     * @var array
     */
    protected $fillable = [
        'referral_link_id',
        'user_id'
    ];

    public function tasks() {
        return $this->hasMany(Task::class,'id_user', 'user_id');
    }

    public function lastOrder() {
        return $this->hasOne(Task::class,'id_user', 'user_id')
            ->where('status', '=', 4)
            ->orderByDesc('id');
    }

    public function taskss()
    {
        return $this->hasMany(Task::class,'id_user', 'user_id')
            ->where('status', 4);
        // replace module_id with appropriate foreign key if needed
    }


    /**
     * Ссылается на реферальную программу
     *
     * @return HasOne
    */
    public function referral_link() : HasOne {
        return $this->hasOne(ReferralLink::class,'id','referral_link_id');
    }

    /**
     * Информация о пользователе
     *
     * @return HasOne
    */
    public function user() : HasOne {
        return $this->hasOne(User::class,'id','user_id');
    }
}
