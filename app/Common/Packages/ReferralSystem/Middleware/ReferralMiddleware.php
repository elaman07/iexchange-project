<?php
namespace App\Common\Packages\ReferralSystem\Middleware;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Models\ReferralStatistics;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ReferralMiddleware
{
    /**
     * Обрабатывать входящий запрос.
     *
     * @param Request $request
     * @param \Closure $next
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(config('setup.enabled') == 1)
            return $next($request);


        // Для параметра partnerId
        if($request->has('partnerId') and (int)iEXSetting('enabled_referral_system') == 0)
        {
            $partnerId = security_xss($request->get('partnerId'));
            $referral = ReferralLink::where('user_id', $partnerId)->first();


            if(!empty($referral) and isset($referral->user) and $referral->user->is_pay_referral == 0)
            {
                // Записываем в лог для статистики
                $item = ReferralStatistics::create([
                    'ref_hash'      => (string)$referral->code,
                    'ip_address'    => $request->ip(),
                    'id_user'       => (\auth()->check() ? auth()->id() : 0),
                    'user_agent'    => $request->userAgent()
                ]);

                if(iEXSetting('referral_storage_periods') == 0) {
                    $cookie = \Cookie::forever('ref', $referral->code);
                } else {
                    $minutes = (integer)iEXSetting('referral_lifetime_day', 7) * 1400;
                    $cookie = \Cookie::make('ref', $referral->code, $minutes);
                }

                $redirect_url = $request->url();
                if($request->has('locale'))
                {
                    $question = $request->getBaseUrl().$request->getPathInfo() === '/' ? '/?' : '?';
                    $redirect_url = $redirect_url.$question.Arr::query($request->all());
                }

                // Записываем в историю данные (cur_from and cur_to)
                if($request->has('cur_from') and $request->has('cur_to'))
                {
                    // Записываем еще доп. параметры
                    $item->cur_from = $request->get('cur_from');
                    $item->cur_to = $request->get('cur_to');
                    $item->from_and_to = sprintf('%s - %s', $request->get('cur_from'), $request->get('cur_to'));
                    $item->save();

                    $question = $request->getBaseUrl().$request->getPathInfo() === '/' ? '/?' : '?';
                    $redirect_url = $request->url().$question.Arr::query($request->all());
                }


                return redirect($redirect_url)->withCookie($cookie);
            }


        }



        if($request->has('ref') and (int)iEXSetting('enabled_referral_system') == 0)
        {
            $referral = ReferralLink::whereRaw("BINARY `code` = ?",[security_xss($request->get('ref'))])->first();

            if(!empty($referral) and isset($referral->user) and $referral->user->is_pay_referral == 0)
            {
                // Записываем в лог для статистики
                $item = ReferralStatistics::create([
                    'ref_hash'      => (string)$referral->code,
                    'ip_address'    => $request->ip(),
                    'id_user'       => (\auth()->check() ? auth()->id() : 0),
                    'user_agent'    => $request->userAgent()
                ]);

                if(iEXSetting('referral_storage_periods') == 0) {
                    $cookie = \Cookie::forever('ref', $referral->code);
                } else {
                    $minutes = (integer)iEXSetting('referral_lifetime_day', 7) * 1400;
                    $cookie = \Cookie::make('ref', $referral->code, $minutes);
                }

                $redirect_url = $request->url();
                if($request->has('locale'))
                {
                    $question = $request->getBaseUrl().$request->getPathInfo() === '/' ? '/?' : '?';
                    $redirect_url = $redirect_url.$question.Arr::query($request->except('ref'));
                }

                // Записываем в историю данные (cur_from and cur_to)
                if($request->has('cur_from') and $request->has('cur_to'))
                {
                    // Записываем еще доп. параметры
                    $item->cur_from = $request->get('cur_from');
                    $item->cur_to = $request->get('cur_to');
                    $item->from_and_to = sprintf('%s - %s', $request->get('cur_from'), $request->get('cur_to'));
                    $item->save();

                    $question = $request->getBaseUrl().$request->getPathInfo() === '/' ? '/?' : '?';
                    $redirect_url = $request->url().$question.Arr::query($request->except('ref'));
                }

                return redirect($redirect_url)->withCookie($cookie);
            }
        }

        return $next($request);
    }
}
