<?php

namespace App\Common\Packages\ReferralSystem;

use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Models\DirectionExchange;
use App\Models\Task;
use App\Models\UserBalance;

class ReferralSystemBuilder
{
    use Concerns\BuilderAttribute,
        Concerns\BuilderDetails;

    protected Task $task;

    protected DirectionExchange $directionExchange;

    /**
     * Создание нового обработчика
    */
    public function __construct(Task $task)
    {
        $this->task = $task;
        $this->directionExchange = $task->direction_exchange;
        $this->setClient($this->task->user);

        $referralPartner = $this->getReferralPartner();
        if(empty($referralPartner)) {
            return;
        }

        $this->setPartnerClient($referralPartner->user);
    }

    /**
     * Обработчик
    */
    public function handle()
    {
        if(empty($this->partner_client)) {
            return;
        }

        // Выплачивать бонусы
        if((int)iEXSetting('is_enabled_not_referral_bonus') == 1)
        {
            $text = 'Партнер не получил бонус по заявке №'.$this->task->id;
            add_to_referral_log($text, 2, $this->getPartnerClient()->id, $this->getClient()->id);
            return;
        }

        // Если не верифицирован не начисляем реферальный бонус партнеру
        if (is_null($this->getPartnerClient()->email_verified_at) and (int)iEXSetting('is_verified_referral')) {
            $text = 'Партнер не верифицировал e-mail адрес';
            add_to_referral_log($text, 2, $this->getPartnerClient()->id, $this->getClient()->id);
            return;
        }

        // Не выплачиваем партнерские бонусы
        if($this->getPartnerClient()->is_pay_referral == 1) {
            $text = 'Администратор отключил партнеру выплату бонусов от заявок';
            add_to_referral_log($text, 2, $this->getPartnerClient()->id, $this->getClient()->id);
            return;
        }

        // Проверяем, доступны ли выплаты бонусов по обменному направлению
        if($this->isNotPartner()) {
            $text = 'По направлению '.$this->directionExchange->tech_name.' отключены выплаты бонусов';
            add_to_referral_log($text, 2, $this->getPartnerClient()->id, $this->getClient()->id);
            return;
        }

        // Проверяем, не отключена ли возможность выплаты бонусов через заявку
        if($this->task->task_info->is_switch_not_partner == 1) {
            $text = 'Оператор '. auth()->check() ? auth()->user()->name : 'Бот'.' не выплатил бонус партнеру';
            add_to_referral_log($text, 2, $this->getPartnerClient()->id, $this->getClient()->id);
            return;
        }

        // Проверяем установленную макс. и минимальную цену
        if(!$this->hasNotMinAndMaxBonus()) {
            $text = 'Не пройден этап установленного мин. или макс. порога получения бонусов';
            add_to_referral_log($text, 2, $this->getPartnerClient()->id, $this->getClient()->id);
            return;
        }

        // Выплата бонусов по расписанию
        if(!$this->hasRangeBonus()) {
            $text = 'Истек установленных дневной период выплат бонусов';
            return;
        }

        // Проверяем суточные и месячные лимиты
        if($this->hasBonusLimitDay() or $this->hasBonusLimitMonth()) {
            $text = 'Превышен суточный или месячный лимит получения бонусов';
            return;
        }

        $referral_amount = (float)$this->getAmount();

        //Создаем лог собыий
        ReferralLog::create([
            'id_referral_link'  =>  $this->getReferralPartner()->referral_program_id,
            'id_task'           =>  $this->task->id,
            'id_referral'       =>  $this->getClient()->id,
            'id_user'           =>  $this->getPartnerClient()->id,
            'bonus'             =>  $this->getAmountString(),
            'bonus_number'      =>  $referral_amount,
            'text'              =>  sprintf('Бонус за обмен (%s)', $this->directionExchange->tech_name),
            'fixed_bonus'       =>  $this->getFixedPayout(),
            'current_percent'   =>  $this->getCurrentPercent()
        ]);

        // Записываем в общий баланс партнера
        $user_balance = UserBalance::where('id_user', $this->getPartnerClient()->id)->first();
        $user_balance->update([
            'referral_total_profit' => $user_balance->referral_total_profit + $referral_amount
        ]);

        // Прикрепляем реферала к заявке
        $this->task->update([
            'id_referral_link'  =>  $this->getReferralPartner()->id
        ]);

        $text = 'По направлению '.$this->directionExchange->tech_name.' получен бонус в размере '.$this->getAmountString();
        add_to_referral_log($text, 3, $this->getPartnerClient()->id, $this->getClient()->id);

        $this->updateReferralBalance();
    }
}
