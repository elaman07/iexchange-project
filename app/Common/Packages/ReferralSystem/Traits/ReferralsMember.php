<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 25.02.2018, 20:18
 */

namespace App\Common\Packages\ReferralSystem\Traits;


use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralProgram;

trait ReferralsMember
{
    public function getReferrals()
    {
        return ReferralProgram::all()->map(function ($program) {
            return ReferralLink::getReferral($this, $program);
        })->filter();
    }

    public function referral_links() {
        return $this->hasOne(ReferralLink::class,'user_id','id');
    }

    public function referralProgram()
    {
        return $this->hasOne(ReferralProgram::class, 'id', 'referral_program_id');
    }
}
