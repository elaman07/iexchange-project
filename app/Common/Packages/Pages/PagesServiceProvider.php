<?php
namespace App\Common\Packages\Pages;

use Illuminate\Support\ServiceProvider;

class PagesServiceProvider extends ServiceProvider
{
    /**
     * Указывает, откладывается ли загрузка поставщика.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Загрузите события приложения.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('pages', function ($app) {
            return new SectionPages();
        });
    }

    /**
     * Получите услуги, предоставляемые поставщиком.
     *
     * @return array
     */
    public function provides()
    {
        return ['pages'];
    }
}