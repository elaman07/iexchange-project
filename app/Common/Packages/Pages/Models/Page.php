<?php
namespace App\Common\Packages\Pages\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;


/**
 * App\Common\Packages\Pages\Models\Page
 *
 * @property int $page_id
 * @property string $page_title
 * @property string $page_content
 * @property string $page_slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Common\Packages\Pages\Models\Page onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page wherePageContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page wherePageSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page wherePageTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Common\Packages\Pages\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Common\Packages\Pages\Models\Page withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Common\Packages\Pages\Models\Page withoutTrashed()
 * @mixin \Eloquent
 * @property-read mixed $translations
 */
class Page extends Model
{
    use SoftDeletes, HasTranslations;

    /**
     * Таблица базы данных, используемая моделью.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * Первичный ключ.
     *
     * @var string
     **/
    protected $primaryKey = 'page_id';

    protected $fillable = [
        'page_title', 'page_content', 'page_slug', 'user_id'
    ];

    /**
     * Это для функциональности SoftDeleting.
     *
     * @var bool
     */
    protected $dates = ['deleted_at'];

    /**
     * Для мультиязычности
    */
    public $translatable = ['page_title', 'page_content'];


    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }
}
