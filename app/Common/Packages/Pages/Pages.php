<?php
namespace App\Common\Packages\Pages;

use Illuminate\Support\Facades\Facade;

class Pages extends Facade
{
    /**
     * Получить зарегистрированное имя компонента.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pages';
    }
}