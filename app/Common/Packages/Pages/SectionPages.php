<?php
namespace App\Common\Packages\Pages;

use App\Common\Packages\Pages\Models\Page;
use Illuminate\Support\Str;

class SectionPages
{
    /**
     * Проверяет, существует ли страница в базе данных.
     *
     * @param string $slug
     * @return bool
     **/
    public function pageExists($slug)
    {
        $count = Page::where('page_slug', '=', $slug)->count();

        if ($count == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Получает все данные страницы из базы данных на основе "slug".
     *
     * @param string $slug
     * @param bool $trashed
     * @return array
     **/
    public function getPage($slug, $trashed = false)
    {
        if ($trashed) {
            return Page::withTrashed()->where('page_slug', '=', $slug)->first();
        }

        return Page::where('page_slug', '=', $slug)->first();
    }

    /**
     * Получает все данные страницы из базы данных на основе идентификатора.
     *
     * @param string $id
     * @param bool $trashed
     * @return array
     **/
    public function getPageById($id, $trashed = false)
    {
        if ($trashed) {
            return Page::withTrashed()->find($id);
        }

        return Page::find($id);
    }

    /**
     * Получает только идентификатор страницы, принадлежащей данному "slug".
     *
     * @param string $slug
     * @return int
     **/
    public function getPageId($slug)
    {
        $query = Page::where('page_slug', '=', $slug)->withTrashed()->select('page_id')->first();

        return $query->page_id;
    }

    /**
     * Создаем "slug"
     *
     * @param string $slugify_this
     * @return string
     **/
    public function createSlug($slugify_this)
    {
        return Str::slug($slugify_this);
    }

    /**
     * Добавляет страницу в базу данных.
     *
     * @param string $page_title
     * @param string $page_content
     * @param string|null $custom_slug
     * @return boolean
     **/
    public function addPage($page_title, $page_content, $custom_slug = null)
    {
        $newPage = new Page;
        $newPage->page_title = $page_title;
        $newPage->page_content = $page_content;

        if (is_null($custom_slug) or empty($custom_slug)) {
            $newPage->page_slug = $this->createSlug($page_title);
        } else {
            $newPage->page_slug = $this->createSlug($custom_slug);
        }

        return $newPage->save();
    }

    /**
     * Обновляет существующую страницу.
     *
     * @param int $page_id
     * @param string $page_title
     * @param string $page_content
     * @param string $page_slug
     * @return boolean
     **/
    public function updatePage($page_id, $page_title, $page_content, $page_slug)
    {
        $page = Page::find($page_id);
        $page->page_title = $page_title;
        $page->page_content = $page_content;
        $page->page_slug = $this->createSlug($page_slug);
        $page->touch();

        return $page->save();
    }

    /**
     * Удаляет страницу, возможно, даже с силой.
     *
     * Если $forceDelete установлено значение true, страница будет удалена из базы данных (навсегда).
     * Если значение false, страница получит значение «deleted_at» и не будет показываться до восстановления.
     *
     * @param int $page_id
     * @param bool $forceDelete
     * @return boolean
     **/
    public function deletePage($page_id, $forceDelete = false)
    {
        $page = Page::withTrashed()->find($page_id);

        if ($forceDelete) {
            return $page->forceDelete($page_id);
        } else {
            return $page->delete($page_id);
        }
    }

    /**
     * Восстанавливает ранее удаленную страницу.
     *
     * Идентификатор страницы можно получить с помощью функции getPageId().
     *
     * @param int $page_id
     * @return boolean
     **/
    public function restorePage($page_id)
    {
        $page = Page::withTrashed()->find($page_id);
        return $page->restore($page_id);
    }
}