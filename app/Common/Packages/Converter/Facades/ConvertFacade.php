<?php
namespace App\Common\Packages\Converter\Facades;

use App\Common\Packages\Converter\Converter;
use App\Models\DirectionExchange;
use Illuminate\Support\Facades\Facade;

/**
 * @method static array all();
 * @method static Converter call(DirectionExchange $exchange);
 * @method static Converter in();
 * @method static Converter out();
 *
 */
class ConvertFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'converter';
    }
}
