<?php
namespace App\Common\Packages\Converter;

use App\Jobs\TelegramCoursesFailJob;
use App\Models\DirectionExchange;

class Converter implements ConverterContract
{
    use Bindings\ManagerQueries,
        Bindings\ManagerDetails,
        Bindings\ManagerCompiler,
        Bindings\ManagerRule;

    /**
     * Массив результатов
     *
     * @var array
    */
    protected $collect = [];

    /**
     * Детали направления
     *
     * @var \App\Models\DirectionExchange
    */
    protected $exchange;


    protected $currency1;
    protected $currency2;

    /**
     * Главный вызов
     *
     * @param DirectionExchange $exchange
     * @return Converter
     */
    public function call(DirectionExchange $exchange)
    {
        $this->exchange = $exchange;
        return $this;
    }

    public function setCurrencyIn($item)
    {
        $this->currency1 = $item;
        return $this;
    }

    public function setCurrencyOut($item)
    {
        $this->currency2 = $item;
        return $this;
    }

    /**
     * Объединение обоих результатов
     *
     * @throws \Exception
     */
    public function all()
    {
        return array_merge(
            $this->in()->toArray(),
            $this->out()->toArray()
        );
    }

    /**
     * Converter amount from > to <
     *
     * @return ConverterResponse
     * @throws \Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function in()
    {
        $text = null;
        try {
            if($this->exchange->enable_bestchange == 1) {
                return new ConverterResponse($this->compilerBestchange(), 'out');
            } elseif(!empty($this->exchange->id_competitor)) {
                return new ConverterResponse($this->compilerCompetitors(), 'out');
            } elseif(!empty($this->exchange->id_file_parser_rate)) {
                return new ConverterResponse($this->compilerFileParser(), 'out');
            }  elseif(!empty($this->exchange->id_partner_parser_rate)) {
                return new ConverterResponse($this->compilerPartnerParser(), 'out');
            } elseif(!empty($this->exchange->id_parser_formula_rate)) {
                return new ConverterResponse($this->compilerFormulaParser(), 'out');
            } elseif($this->exchange->id_crypto_parser > 0) {
                return new ConverterResponse($this->compilerCryptoParser(), 'out');
            } elseif(!empty($this->exchange->id_your_exchange)) {
                return new ConverterResponse($this->compilerYourParser(), 'out');
            } elseif($this->exchange->manual_rate_value > 0) {
                return new ConverterResponse($this->compilerManualRate(), 'out');
            }

        } catch (\Exception $exception) {
            $text = 'Не выбран тип парсинга курсов '. direction_name($this->exchange). 'Text: Error: '.$exception->getMessage();
            add_to_direction_exchange_error($text, $this->exchange->id, 1, 1, 1);
        }

        // Если не записан в лог
        if(empty($text)) {
            $text = 'Не выбран курс обмена для направления '. direction_name($this->exchange);
            add_to_direction_exchange_error($text, $this->exchange->id, 1, 1, 1);
        }

        // Telegram уведомление о проблеме с парсинго
        if (iEXSetting('enable_telegram_courses_fail')) {
            $delay = now()->addSeconds(5);
            dispatch(new TelegramCoursesFailJob($text))->delay($delay)->onQueue('low');
        }
    }

    /**
     * Конвертатор цены с < на >
     *
     * @return ConverterResponse
     * @throws \Exception
     */
    public function out() {
        return $this->in();
    }


    public function in_city($id_city = 0)
    {
        $response = $this->in()->toArray();
        if($id_city > 0 and isset($response['cities'])) {
            $response = $response['cities'][$id_city];
        }
        return new ConverterResponse($response,'out');
    }


    public function out_city($id_city = 0)
    {
        $response = $this->out()->toArray();
        if($id_city > 0 and isset($response['cities'])) {
            $response = $response['cities'][$id_city];
        }

        return new ConverterResponse($response,'in');
    }
}
