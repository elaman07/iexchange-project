<?php
namespace App\Common\Packages\Converter;

use App\Models\DirectionExchange;

interface ConverterContract
{
    /**
     * Конвертатор цены с > на <
     *
     * @return array
     */
    public function in();

    /**
     * Конвертатор цены с < на >
     *
     * @return array
     */
    public function out();
}