<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 07.02.2018, 19:54
 */

namespace App\Common\Packages\Converter;

use Illuminate\Support\ServiceProvider;

class ConverterServiceProvider extends ServiceProvider
{
    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('converter', function() {
            return new Converter();
        });
    }
}