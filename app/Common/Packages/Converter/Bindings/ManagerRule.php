<?php
namespace App\Common\Packages\Converter\Bindings;


trait ManagerRule
{
    public function rate_limiters($summa)
    {
        return $this->exchange->rl_min2_course > 0 and $this->exchange->rl_min2_course > $summa or
            $this->exchange->rl_max2_course > 0 and $this->exchange->rl_max2_course < $summa;
    }
}