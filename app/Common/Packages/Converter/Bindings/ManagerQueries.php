<?php
namespace App\Common\Packages\Converter\Bindings;

trait ManagerQueries
{
    /**
     * Выполнение вычитаний
     *
     * @param float $summa
     * @param string $type
     * @param string|null $add_custom_course
     * @param string $city_rate
     * @return float
     */
    protected function subtraction(float $summa, string $type, string $add_custom_course = null, string $city_rate = ''): float
    {
        $summa = $this->addCommissionDynamicCurrency($this->exchange->commission_s1, $summa);

        if($add_custom_course == 'rl_add_course' and !empty($this->exchange->rl_add_course)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->rl_add_course, $summa);
        }elseif($add_custom_course == 'bc_new' and !empty($this->exchange->bc_add_course)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->bc_add_course, $summa);
        } elseif($add_custom_course == 'bc_your_new' and !empty($this->exchange->bc_your_add_course)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->bc_your_add_course, $summa);
        } elseif($add_custom_course == 'bc_add_comm' and !empty($this->exchange->bc_new_commission)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->bc_new_commission, $summa);
        } elseif($add_custom_course == 'cr_new' and !empty($this->exchange->cr_add_course)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->cr_add_course, $summa);
        }

        // Проверяем, включена ли групповая комиссия
        if($this->exchange->id_group_commission > 0 and !empty($this->getGroupCommission()->receiving)) {
            $summa = $this->addCommissionDynamicAuto($this->getGroupCommission()->receiving, $summa);
        } else {
            $summa = $this->addCommissionDynamicPercent($this->exchange->commission1, $summa);
        }


        // Доп. процент для своего курса
        if(isset($this->exchange->your_exchange) and !empty($this->exchange->your_add_course1)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->your_add_course1, $summa);
        }
        if(isset($this->exchange->your_exchange) and !empty($this->exchange->your_add_course1_s)) {
            $summa = $this->addCommissionDynamicCurrency($this->exchange->your_add_course1_s, $summa);
        }

        // Доп. процент для авто-корректировки курса
        if($this->exchange->id_crypto_parser > 0 and !empty($this->exchange->add_course1)) {
            $summa = $this->addCommissionDynamicPercent($this->exchange->add_course1, $summa);
        }

        // Учитываем прибыль
        if($this->exchange->id_crypto_parser > 0 and !empty($this->exchange->add_course1_s)) {
            $summa = $this->addCommissionDynamicCurrency($this->exchange->add_course1_s, $summa);
        }


        // Учитываем прибыль
        if(!empty($this->exchange->profit))
        {
            if($this->exchange->type_profit_field == 0) {
                $summa = $this->profitPercent($this->exchange->profit, $summa);
            }else {
                $summa = $this->addCommissionDynamicPercent($this->exchange->profit, $summa);
            }
        }

        // Учитываем прибыль
        if(!empty($this->exchange->profit_s))
        {
            if($this->exchange->type_profit_field == 0) {
                $summa = $this->profitCurrency($this->exchange->profit_s, $summa);
            } else {
                $summa = $this->addCommissionDynamicCurrency($this->exchange->profit_s, $summa);
            }
        }

        //$oth_min_comm = (!empty($this->exchange->oth_min_comm) ? $this->exchange->oth_min_comm : 0);

        if(!empty($city_rate)) {
            $summa = $this->addCommissionDynamicAuto($city_rate, $summa);
        }

        // Итоговая сумма
        $bestchange_step = $this->exchange->bestchange_step;
        if($this->exchange->enable_bestchange == 1 and !empty($bestchange_step)) {
            $result_summa = $this->addCommissionDynamicAuto($bestchange_step, $summa);
        } else {
            $result_summa = $summa;
        }

        return $result_summa;
    }

    /**
     * Разбираем и обрабатываем значение с авто определением
     *
     * @param string $value
     * @param $summa
     * @return float
     */
    protected function addCommissionDynamicAuto(string $value, $summa): float
    {
        $value_number = $this->getOnlyNumber($value); // Получаем число
        $char_first = mb_substr($value, 0, 1); // Получение первого символа
        $char_last = mb_substr($value, -1); // Получение последнего символа

        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        $first_summa = $summa;
        if(\mb_substr(iex_number_format($first_summa, $this->getOutCurrency()->number_format), 0, 2) == '0') {
            $summa = 1 / $summa;
        }

        $value_n = $value_number;
        if($char_last == '%') {
            $value_n = ($value_number > 0) ? ($value_number / 100) * $summa : 0;
        }


        // Умножение
        if($char_first == '*') {
            $summa = $summa * $value_n;
        }

        // Деление
        if($char_first == '/') {
            $summa = $summa / $value_n;
        }

        // Вычитание
        if($char_first == '-') {
            $summa = $summa - $value_n;
        }

        if(!in_array($char_first, ['*', '/', '-'])) {
            $summa = $summa + $value_n;
        }

        if(\mb_substr(iex_number_format($first_summa, $this->getOutCurrency()->number_format), 0, 2) == '0') {
           $summa = (1 / $summa);
        }

        return $summa;
    }


    public function profitPercent(string $value_number, $summa)
    {
        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        $value_n = ($value_number > 0) ? ($value_number / 100) * $summa : 0;
        return $summa - $value_n;
    }

    public function profitCurrency(string $value_number, $summa)
    {
        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        return $summa - $value_number;
    }

    /**
     * Разбираем и обрабатываем значение с процентом
     *
     * @param string $value
     * @param $summa
     * @return float
     */
    protected function addCommissionDynamicPercent(string $value, $summa): float
    {
        $value_number = $this->getOnlyNumber($value); // Получаем число
        $char_first = mb_substr($value, 0, 1); // Получение первого символа

        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        $value_n = $value_number;
        $value_n = ($value_number > 0) ? ($value_number / 100) * $summa : 0;

        $result_summa_v = $summa;
        // Умножение
        if($char_first == '*') {
            $result_summa_v = $result_summa_v * $value_n;
        }

        // Деление
        if($char_first == '/') {
            $result_summa_v = $result_summa_v / $value_n;
        }

        // Вычитание
        if($char_first == '-') {
            $result_summa_v = $result_summa_v - $value_n;
        }

        if(!in_array($char_first, ['*', '/', '-'])) {
            $result_summa = $result_summa_v + $value_n;
        } else {
            $result_summa = $result_summa_v;
        }

        return $result_summa;
    }


    /**
     * Разбираем и обрабатываем значение с валютой
     *
     * @param string $value
     * @param $summa
     * @param float $other_comm
     * @return float
     */
    protected function addCommissionDynamicCurrency(string $value, $summa, float $other_comm = 0): float
    {
        $value_number = $this->getOnlyNumber($value); // Получаем число
        $char_first = mb_substr($value, 0, 1); // Получение первого символа

        if($other_comm > 0 and $other_comm < $value_number) {
            return ($summa - $other_comm);
        }

        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        $value_n = $value_number;
        $result_summa_v = $summa;
        // Умножение
        if($char_first == '*') {
            $result_summa_v = $result_summa_v * $value_n;
        }

        // Деление
        if($char_first == '/') {
            $result_summa_v = $result_summa_v / $value_n;
        }

        // Вычитание
        if($char_first == '-') {
            $result_summa_v = $result_summa_v - $value_n;
        }

        if(!in_array($char_first, ['*', '/', '-'])) {
            $result_summa = $result_summa_v + $value_n;
        } else {
            $result_summa = $result_summa_v;
        }

        return $result_summa;
    }

    /**
     * В конечной суммы после запятой оставляем определенное количество знаков
     *
     * @param int $amount
     * @param int $decimals
     * @return string
     */
    protected function toFormat($amount = 0, $decimals = 2)
    {
        return iex_number_format($amount, (float)$decimals);
    }

    protected function toCustomFormat($amount = 0, $decimals = 2) {

        if($decimals == 'auto') {
            $price = preg_replace("/\.$/", "", $amount);
            // Убираем ненужные дробные числа
            $decimal = (int)strlen(substr(strrchr($price, '.'), 1));

            return number_format(
                $price, ($decimal > 4 ? 4 : $decimal), '.',' '
            );
        }

        return number_format($amount, $decimals, '.',' ');
    }

    /**
     * Получение результата обменного курса
     *
     * @param int $summa
     * @param $value
     * @param int $summaString
     * @param bool $isString
     * @return null|string
     */
    protected function cursCompiler($summa = 0, $value = 0, $summaString = 0, $isString = false)
    {
        $first_char = mb_substr($summa, 0, 1);
        $result_curs = null;

            if($first_char == 0)
            {
                $resultString = $this->toFormat(1 / $summaString, $this->getInCurrency()->number_format);
                if($isString) {
                    $result_curs = $this->toCustomFormat($resultString, 'auto');
                } else {

                    $format_price = preg_replace("/\.?0*$/",'', iex_number_format($resultString, $this->getOutCurrency()->number_format, true));


                    $result_curs = $this->toCourseFormat([
                        1, $this->getOutCodeCurrency()->sign, $format_price, $this->getInCodeCurrency()->sign
                    ]);
                }
            }else {
                if($isString) {
                    $result_curs = $this->toCustomFormat($summa, 'auto');
                } else {

                    $format_price = preg_replace("/\.?0*$/",'', iex_number_format($summa, $this->getOutCurrency()->number_format, true));

                    $result_curs = $this->toCourseFormat([
                        1,  $this->getInCodeCurrency()->sign, $format_price,  $this->getOutCodeCurrency()->sign
                    ]);
                }
            }

        return $result_curs;
    }

    /**
     * Получаем строковый курсы
     *
     * @param array $args
     * @return string
     */
    protected function toCourseFormat(array $args = []): string
    {
        return vsprintf('%s %s = %s %s', $args);
    }

    protected function getOnlyNumber($value)
    {
        return preg_replace('/[^\d.]/','', $value);
    }
}
