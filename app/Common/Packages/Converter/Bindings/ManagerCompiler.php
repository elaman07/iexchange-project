<?php
namespace App\Common\Packages\Converter\Bindings;


trait ManagerCompiler
{
    /**
     * Если включен парсинг из Bestchange
     *
     * @return array
     * @throws \Exception
     */
    public function compilerBestchange(): array
    {
        // Если, была ошибка в курсе, переключаем парсер
        if($this->exchange->is_enable_alt_bs_parser == 1) {
            return $this->compilerBSAltParser();
        }

        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Чтобы курс не превышал больше установленной
        $summa = $this->getBestchangeRates()->summa;
        $course = null;
        if($this->exchange->bc_max_sum > 0 and $summa > $this->exchange->bc_max_sum)
        {
            // Если комиссия не затронута то сбрасываем
            if($this->exchange->bc_enable_your_course == 1) {
                $summa = $this->getBcYourRates()->exchange_rate;
                $course = 'bc_your_new';
            } elseif($this->exchange->bc_new_commission == 0) {
                $summa = $this->getBCNewRate()->summa;
                $course = 'bc_new';
            } else {
                $summa = $this->getBestchangeRates()->summa;
                $course = 'bc_add_comm';
            }
        }

        // Независимая функция для настройки мин. макс курса
        if($this->getRlYourExchange() > 0 and $this->rate_limiters($this->getBestchangeRates()->summa)) {
            $summa = $this->getRlYourExchange()->exchange_rate;
            $course = 'rl_your_add_course';
        } else {
            if($this->rate_limiters($this->getBestchangeRates()->summa)) {
                $summa = $this->getRlParserExchange()->summa_default;
                $course = 'rl_add_course';
            }
        }

        $value = $this->subtraction($summa, 'out', $course);
        $numberFormat = $this->toFormat($value * $this->getBestchangeRates()->value, $decimal);

        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($numberFormat, $this->getBestchangeRates()->value, $value),
            'cur_str'   => $this->cursCompiler($numberFormat, $this->getBestchangeRates()->value, $value, true),
        ];
    }


    /**
     * Этот парсер будет доступен только, если Bestchange не работает
    */
    protected function compilerBSAltParser()
    {
        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Чтобы курс не превышал меньше установленной минимальной
        $summa = $this->getBSAltParserRates()->summa;
        $course = null;

        if($this->getRlYourExchange() > 0 and $this->rate_limiters($this->getBSAltParserRates()->summa_default)) {
            $summa = $this->getRlYourExchange()->exchange_rate;
            $course = 'rl_your_add_course';
        } else {
            if($this->rate_limiters($this->getBSAltParserRates()->summa_default)) {
                $summa = $this->getRlParserExchange()->summa_default;
                $course = 'rl_add_course';
            }
        }

        $value = $this->subtraction($summa, 'out', $course);
        $toFormat = $this->toFormat($value * $this->getBSAltParserRates()->value_default, $decimal);


        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($toFormat, $this->getBSAltParserRates()->value_default, $value),
            'cur_str'   => $this->cursCompiler($toFormat, $this->getBSAltParserRates()->value_default, $value, true),
        ];
    }

    /**
     * Если включен парсинг курсов Конкурентов
     *
     * @return array
     */
    public function compilerCompetitors()
    {
        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Чтобы курс не превышал меньше установленной минимальной
        $summa = $this->getCompetitorRates()->summa;
        $course = null;
        if($this->exchange->cr_max_sum > 0 and $summa > $this->exchange->cr_max_sum) {
            $summa = $this->getCRNewRate()->summa;
            $course = 'cr_new';
        }

        // Независимая функция для настройки мин. макс курса
        if($this->getRlYourExchange() > 0 and $this->rate_limiters($this->getCompetitorRates()->summa)) {
            $summa = $this->getRlYourExchange()->exchange_rate;
            $course = 'rl_your_add_course';
        } else {
            if($this->rate_limiters($this->getCompetitorRates()->summa)) {
                $summa = $this->getRlParserExchange()->summa_default;
                $course = 'rl_add_course';
            }
        }

        $value = $this->subtraction($summa, 'out', $course);
        $numberFormat = $this->toFormat($value * $this->getCompetitorRates()->value, $decimal);

        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($numberFormat, $this->getCompetitorRates()->value, $value),
            'cur_str'   => $this->cursCompiler($numberFormat, $this->getCompetitorRates()->value, $value, true),
        ];
    }

    /**
     * Если включен парсинг курсов Из файла
     *
     * @return array
     */
    public function compilerFileParser()
    {
        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Чтобы курс не превышал меньше установленной минимальной
        $summa = (float)$this->getFileParserRates()->summa;

        $value = $this->subtraction($summa, 'out', null);
        $numberFormat = $this->toFormat($value * $this->getFileParserRates()->value, $decimal);

        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($numberFormat, $this->getFileParserRates()->value, $value),
            'cur_str'   => $this->cursCompiler($numberFormat, $this->getFileParserRates()->value, $value, true),
        ];
    }

    /**
     * Если включен парсинг курсов Из файла
     *
     * @return array
     */
    public function compilerPartnerParser()
    {
        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Чтобы курс не превышал меньше установленной минимальной
        $summa = (float)$this->getPartnerParserRates()->rate;

        $value_default = (int)1;

        $value = $this->subtraction($summa, 'out', null);
        $numberFormat = $this->toFormat($value * $value_default, $decimal);

        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($numberFormat, $value_default, $value),
            'cur_str'   => $this->cursCompiler($numberFormat, $value_default, $value, true),
        ];
    }


    /**
     * Если включен парсинг курсов Из формулы
     *
     * @return array
     */
    public function compilerFormulaParser()
    {
        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Чтобы курс не превышал меньше установленной минимальной
        $summa = (float)$this->getFormulaParserRates()->summa;

        $value = $this->subtraction($summa, 'out', null);
        $numberFormat = $this->toFormat($value * $this->getFormulaParserRates()->value, $decimal);

        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($numberFormat, $this->getFormulaParserRates()->value, $value),
            'cur_str'   => $this->cursCompiler($numberFormat, $this->getFormulaParserRates()->value, $value, true),
        ];
    }

    /**
     * Если включен парсинг из остальных источников
     *
     * @return array
    */
    public function compilerCryptoParser()
    {

        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');


        // Независимая функция для настройки мин. макс курса
        $summa = $this->getParserExchange()->summa_default;
        $course = null;

        if($this->getRlYourExchange() > 0 and $this->rate_limiters($this->getParserExchange()->summa_default)) {
            $summa = $this->getRlYourExchange()->exchange_rate;
            $course = 'rl_your_add_course';
        } else {
            if($this->rate_limiters($this->getParserExchange()->summa_default)) {
                $summa = $this->getRlParserExchange()->summa_default;
                $course = 'rl_add_course';
            }
        }

        $value = $this->subtraction($summa, 'out', $course);


        $toFormat = $this->toFormat($value * $this->getParserExchange()->value_default, $decimal);


        $response_view = [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($toFormat, $this->exchange->parser_exchange->value_default, $value),
            'cur_str'   => $this->cursCompiler($toFormat, $this->exchange->parser_exchange->value_default, $value, true),
        ];

        if($this->exchange->direction_exchange_cities->count() > 0)
        {
            foreach ($this->exchange->direction_exchange_cities as $city_rate)
            {
                $result_rate_value = (float)$this->subtraction($summa, 'out', $course, $city_rate->add_comm);
                $toFormat_city = $this->toFormat($result_rate_value * $this->getParserExchange()->value_default, $decimal);
                $response_view['cities'][$city_rate->id] = [
                    'a_decimal' => $decimal,
                    'amount'    => $result_rate_value,
                    'curs'      => $this->cursCompiler($toFormat_city, $this->exchange->parser_exchange->value_default, $result_rate_value),
                    'cur_str'   => $this->cursCompiler($toFormat_city, $this->exchange->parser_exchange->value_default, $result_rate_value, true)
                ];
            }
        }


        if($this->exchange->direction_exchange_percentage_amount->count() > 0)
        {
            foreach ($this->exchange->direction_exchange_percentage_amount as $percentage_amount)
            {
                $result_rate_value = (float)$this->subtraction($summa, 'out', $course, $percentage_amount->percentage);
                $toFormat_city = $this->toFormat($result_rate_value * $this->getParserExchange()->value_default, $decimal);
                $response_view['exchange_amount'][$percentage_amount->id] = [
                    'a_decimal' => $decimal,
                    'amount'    => $result_rate_value,
                    'curs'      => $this->cursCompiler($toFormat_city, $this->exchange->parser_exchange->value_default, $result_rate_value),
                    'cur_str'   => $this->cursCompiler($toFormat_city, $this->exchange->parser_exchange->value_default, $result_rate_value, true)
                ];
            }
        }


        return $response_view;
    }

    /**
     * Если включен парсинг из своих курсов
     *
     * @return array
     */
    public function compilerYourParser()
    {
        $decimal = $this->getYourExchange()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Независимая функция для настройки мин. макс курса
        $summa = $this->getYourExchange()->exchange_rate;
        $course = null;
        if($this->getRlYourExchange() > 0 and $this->rate_limiters($this->getYourExchange()->exchange_rate)) {
            $summa = $this->getRlYourExchange()->exchange_rate;
            $course = 'rl_your_add_course';
        } else {
            if($this->rate_limiters($this->getYourExchange()->exchange_rate)) {
                $summa = $this->getRlParserExchange()->summa_default;
                $course = 'rl_add_course';
            }
        }


        $value = $this->subtraction($summa, 'out', $course);
        $toFormat = $this->toFormat($value * 1, $decimal);

        return [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($toFormat, 1, $value),
            'cur_str'   => $this->cursCompiler($toFormat, 1, $value, true),
        ];
    }


    /**
     * Если включен парсинг из ручного курса
     *
     * @return array
     */
    public function compilerManualRate()
    {
        $decimal = $this->getOutCurrency()->number_format;
        if($decimal > (integer)iEXSetting('max_number_format_parser'))
            $decimal = iEXSetting('max_number_format_parser');

        // Независимая функция для настройки мин. макс курса
        $summa = $this->exchange->manual_rate_value;
        $course = null;


        $value = $this->subtraction($summa, 'out');
        $toFormat = $this->toFormat($value * 1, $decimal);


        $response_view = [
            'amount'    => $value,
            'a_decimal' => $decimal,
            'curs'      => $this->cursCompiler($toFormat, 1, $value),
            'cur_str'   => $this->cursCompiler($toFormat, 1, $value, true),
        ];

        if($this->exchange->direction_exchange_cities->count() > 0)
        {
            foreach ($this->exchange->direction_exchange_cities as $city_rate)
            {
                $result_rate_value = (float)$this->subtraction($summa, 'out', null, $city_rate->add_comm);
                $toFormat_city = $this->toFormat($result_rate_value * 1, $decimal);
                $response_view['cities'][$city_rate->id] = [
                    'a_decimal' => $decimal,
                    'amount'    => $result_rate_value,
                    'curs'      => $this->cursCompiler($toFormat_city, 1, $result_rate_value),
                    'cur_str'   => $this->cursCompiler($toFormat_city, 1, $result_rate_value, true)
                ];
            }
        }


        return $response_view;
    }
}
