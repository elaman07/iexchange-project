<?php
namespace App\Common\Packages\Converter\Bindings;


trait ManagerDetails
{
    /**
     * Получение деталей валюты (Отдаю)
     *
     * @return \App\Models\Currency
     */
    public function getInCurrency()
    {
        if(empty($this->currency1)) {
            return $this->exchange->currency1;
        }

        return $this->currency1;
    }

    /**
     * Получение деталей валюты (Получаю)
     *
     * @return \App\Models\Currency
     */
    public function getOutCurrency()
    {
        if(empty($this->currency2)) {
            return $this->exchange->currency2;
        }

        return $this->currency2;
    }

    /**
     * Получение деталей групповой комиссии
     *
     * @return \App\Models\GroupCommision
    */
    public function getGroupCommission()
    {
        return $this->exchange->group_commission;
    }

    /**
     *  Получить детали кодов валют (Отдаю)
     *
     * @return \App\Models\CodeCurrency
     */
    public function getInCodeCurrency()
    {
        return $this->getInCurrency()->code_currency;
    }

    /**
     *  Получить детали кодов валют (Получаю)
     *
     * @return \App\Models\CodeCurrency
     */
    public function getOutCodeCurrency()
    {
        return $this->getOutCurrency()->code_currency;
    }

    /**
     * Получение деталей курсов из Bestchange модели
     *
     * @return \App\Models\BestChangeRatesModel
     */
    public function getBestchangeRates()
    {
        return $this->exchange->bestchange_rates;
    }

    /**
     * Получение деталей из курсов конкурентов
     *
     * @return \App\Models\CompetitorRates
     */
    public function getCompetitorRates()
    {
        return $this->exchange->competitor_rates;
    }

    /**
     * Получение деталей из альтернативного курса (С Включенным Bestchange)
     *
     * @return \App\Models\ParserExchange
     */
    public function getBSAltParserRates()
    {
        return $this->exchange->bs_alt_parser;
    }

    /**
     * Получение деталей из файла курсов
     *
     * @return \App\Models\FileParserRates
     */
    public function getFileParserRates()
    {
        return $this->exchange->file_parser_rates;
    }

    /**
     * Получение деталей из файла курсов
     *
     * @return \App\Models\PartnerParserRates
     */
    public function getPartnerParserRates()
    {
        return $this->exchange->partner_parser_rates;
    }

    /**
     * Получение деталей из формулы курсов
     *
     * @return \App\Models\ParserFormulaRates
     */
    public function getFormulaParserRates()
    {
        return $this->exchange->parser_formula_rates;
    }


    /**
     * Получение деталей курсов из остальных источников
     *
     * @return \App\Models\ParserExchange
     */
    public function getParserExchange()
    {
        return $this->exchange->parser_exchange;
    }

    /**
     * Получение деталей курсов из остальных источников (Альтернатива Bestchange)
     *
     * @return \App\Models\ParserExchange
     */
    public function getBCNewRate()
    {
        return $this->exchange->bc_new_rate;
    }

    /**
     * Получение деталей курсов из своего курса (Альтернатива Bestchange)
     *
     * @return \App\Models\YourExchange
     */
    public function getBcYourRates()
    {
        return $this->exchange->bc_your_rate;
    }

    /**
     * Получение деталей курсов из остальных источников (Альтернатива Bestchange)
     *
     * @return \App\Models\ParserExchange
     */
    public function getRlParserExchange()
    {
        return $this->exchange->rl_parser_exchange;
    }

    /**
     * Получение деталей курсов из своего курса (Альтернатива)
     *
     * @return \App\Models\YourExchange
     */
    public function getRlYourExchange()
    {
        return $this->exchange->rl_your_exchange;
    }

    /**
     * Получение деталей курсов из остальных источников (Альтернатива Автоброкера)
     *
     * @return \App\Models\ParserExchange
     */
    public function getCRNewRate()
    {
        return $this->exchange->cr_new_rate;
    }

    /**
     * Получение деталей курсов из своих курсов
     *
     * @return \App\Models\YourExchange
     */
    public function getYourExchange()
    {
        return $this->exchange->your_exchange;
    }
}
