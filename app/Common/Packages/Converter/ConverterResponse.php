<?php
namespace App\Common\Packages\Converter;


use Illuminate\Support\Collection;

class ConverterResponse
{
    /**
     * @var Collection
    */
    protected $collect;

    /**
     * @var string
    */
    protected string $type;

    /**
     * Конструктор
     *
     * @param array $collection
     * @param string $type
     */
    public function __construct(array $collection, string $type)
    {
        $this->collect = collect($collection);
        $this->type = $type;
    }

    public function getAmount()
    {
        return $this->collect->get('amount');
    }

    public function getResultAmount(): string
    {
        return iex_number_format($this->collect->get('amount'),10);
    }

    public function getDecimal()
    {
        return ($this->type == 'in' ? $this->collect->get('a_decimal') : $this->collect->get('b_decimal'));
    }

    /***
     * Получаем общий курс
     *
     * @return string
     */
    public function getCourse(): string
    {
        return $this->collect->get('curs');
    }

    /***
     * Получаем общий курс
     *
     * @return string
     */
    public function getCourseStr(): string
    {
        return $this->collect->get('cur_str');
    }

    public function toArray(): array
    {
        return $this->collect->all();
    }
}
