<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.04.2020
 * Time: 9:08
 */

namespace App\Common\Packages\IEXConfig\Adapters;

use Illuminate\Contracts\Redis\Factory;

class RedisAdapter
{
    /**
     * The Redis factory implementation.
     *
     * @var Factory
     */
    protected $redis;

    /**
     * The Redis connection that should be used.
     *
     * @var string
     */
    protected $connection;

    /**
     * A string that should be prepended to keys.
     *
     * @var string
     */
    protected $prefix;

    /**
     * Create a new Redis store.
     *
     * @param $redis
     * @param string $prefix
     * @param int $connection
     */
    public function __construct(Factory $redis, string $prefix, $connection = 0)
    {
        $this->redis = $redis;
        $this->prefix = $prefix;
        $this->setConnection($connection);
    }

    /**
     * Set the connection name to be used.
     *
     * @param  string  $connection
     * @return void
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get the Redis connection instance.
     *
     * @return \Illuminate\Redis\Connections\Connection
     */
    public function connection() {
        return $this->redis->connection($this->connection);
    }

    /**
     * Retrieve an item from the cache by key.
     *
     * @param  string|array  $key
     * @return mixed
     */
    public function get($key) {
        return $this->connection()->get($this->prefix.$key);
    }

    /**
     * Store an item in the cache for a given number of seconds.
     *
     * @param  string $key
     * @param  mixed $value
     * @return bool
     */
    public function set($key, $value)
    {
        return (bool) $this->connection()->set($this->prefix.$key, $value);
    }

    /**
     * Get the Redis database instance.
     *
     * @return Factory
     */
    public function getRedis() {
        return $this->redis;
    }

    /**
     * Serialize the value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    protected function serialize($value)
    {
        return is_numeric($value) && ! in_array($value, [INF, -INF]) && ! is_nan($value) ? $value : serialize($value);
    }

    /**
     * Unserialize the value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    protected function unserialize($value)
    {
        return is_numeric($value) ? $value : unserialize($value);
    }
}