<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.04.2020
 * Time: 9:10
 */

namespace App\Common\Packages\IEXConfig;

use App\Common\Packages\Crypto\Compilers\Compiler;
use App\Common\Packages\Crypto\Contracts\CompilerFactory;
use Illuminate\Support\Facades\Facade;


class IEXConfigFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'iexconfig';
    }
}