<?php
namespace App\Common\Packages\IEXConfig;

use Illuminate\Support\ServiceProvider;

class IEXConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('iexconfig', function ($app) {
            return new IEXConfigManager($app);
        });


        $this->app->singleton('iexconfig.store', function($app) {
            return $app['iexconfig']->driver();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'iexconfig',
            'iexconfig.manager'
        ];
    }
}