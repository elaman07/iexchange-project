<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.04.2020
 * Time: 9:04
 */

namespace App\Common\Packages\IEXConfig;


use App\Common\Packages\IEXConfig\Adapters\RedisAdapter;
use Illuminate\Support\Manager;
use InvalidArgumentException;

class IEXConfigManager extends Manager
{

    /**
     * Get a driver instance.
     *
     * @param  string|null  $name
     * @return mixed
     */
    public function store($name = null) {
        return $this->driver($name);
    }

    /**
     * Redis База
    */
    public function createRedisDriver()
    {
        $redis = $this->container['redis'];
        return new RedisAdapter($redis, 'iex-config', 'config');
    }

    /**
     * Get the default SMS driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return 'redis';
    }
}
