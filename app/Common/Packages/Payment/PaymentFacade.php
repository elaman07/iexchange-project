<?php


namespace App\Common\Packages\Payment;

use App\Common\Packages\Payment\Engines\Interfaces\GatewayInterface;
use Illuminate\Support\Facades\Facade;


class PaymentFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'gateways';
    }
}
