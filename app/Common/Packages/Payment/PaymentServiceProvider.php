<?php

namespace App\Common\Packages\Payment;


use App\Common\Packages\Payment\Engines\GatewayFactory;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('gateways', function ($app) {
            return new PaymentManager($app, new GatewayFactory);
        });
    }

    /**
     * Загрузка пакетов.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
