<?php

namespace App\Common\Packages\Payment\Gateways\WhiteBitCodes;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     *
     * @var string
     */
    protected $api = 'https://whitebit.com';

    /**
     * Конфигурация
     */
    protected $parameters = [];

    /**
     * Создаем констркутор Exmo
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return int
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->call('/api/v4/main-account/balance', [
            'ticker' => $currency
        ]);

        return ($response['main_balance'] ?? 0);
    }

    /**
     * Создаем код
     *
     * @param $currency
     * @param $amount
     * @param null $comment
     * @return mixed
     * @throws Exception
     */
    public function createCode($currency, $amount, $comment = null)
    {
        $response = $this->call('/api/v4/main-account/codes', array_filter([
            'amount' => (string)$amount,
            'ticker' => mb_strtoupper($currency),
            'description' => Str::limit($comment, 70)
        ]));

        if(isset($response['code'])) {
            return $response;
        }

        throw new \Exception('Средства не отправлены');
    }

    /**
     * Активация кода
     *
     * @param string $code
     * @return mixed
     * @throws Exception
     */
    public function activateCode(string $code)
    {
        $response = $this->call('/api/v4/main-account/codes/apply', [
            'code' => $code
        ]);

        if(!isset($response['amount'])) {
            throw new \Exception('Код не активирован');
        }

        return $response;
    }

    /**
     * Вызов определенных действий для "Exmo"
     *
     * @param $method
     * @param $path
     * @param array $options
     * @return mixed
     *
     */
    public function call($path, $options = [])
    {
        $nonce = (string) (int) (microtime(true) * 10000);

        $data = array_merge($options, [
            'request' => $path,
            'nonce' => $nonce,
        ]);

        $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $this->parameters['secret_key']);


        $http = new Client([
            'base_uri' => $this->api,
            'headers' => [
                'accept' => 'application/json',
                'Content-type' => 'application/json',
                'X-TXC-APIKEY' => $this->parameters['public_key'],
                'X-TXC-PAYLOAD' => $payload,
                'X-TXC-SIGNATURE' => $signature
            ]
        ]);
        $response = $http->post($path, ['body' => $dataJsonStr]);
        return json_decode($response->getBody()->getContents(), true);
    }
}
