<?php

namespace App\Common\Packages\Payment\Gateways\WhiteBitCodes;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'WhiteBitCodes';

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setPublicKey(string $value): Gateway
    {
        return $this->setParameter('public_key', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
     */
    public function getDefaultParameters(): array
    {
        return [
            'public_key' => '',
            'secret_key' => ''
        ];
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
