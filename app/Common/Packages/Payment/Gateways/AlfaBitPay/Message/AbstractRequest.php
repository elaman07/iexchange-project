<?php

namespace App\Common\Packages\Payment\Gateways\AlfaBitPay\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKey(string $value): AbstractRequest
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить Приватный ключ
     *
     * @return string
     */
    public function getMasterKey(): string
    {
        return $this->getParameter('master_key');
    }

    /**
     * Установить Приватный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setMasterKey(string $value): AbstractRequest
    {
        return $this->setParameter('master_key', $value);
    }

}
