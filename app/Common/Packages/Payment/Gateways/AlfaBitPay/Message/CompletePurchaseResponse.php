<?php


namespace App\Common\Packages\Payment\Gateways\AlfaBitPay\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    public function getSign()
    {
        return $this->data['signature'];
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['status'] == 'success';
    }

    /**
     * Если транзакция в ожидании
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        return in_array($this->data['status'], ['pending', 'waitConfirmation']);
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return !in_array($this->data['status'], ['unknown', 'canceled', 'fail']);
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['comment'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['tokenCode'];
    }

    public function getStatus() {
        return $this->data['status'];
    }

    /**
     * Счетчик подтверждений
     */
    public function getBlockchainHash() {
        return $this->data['txHashIn'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        return hash('sha256', implode('', [
            $this->data['id'],
            $this->data['comment'],
            $this->data['tokenCode'],
            (string)$this->data['amount'],
            $this->data['status'],
            $this->request->getMasterKey()
        ]));
    }
}
