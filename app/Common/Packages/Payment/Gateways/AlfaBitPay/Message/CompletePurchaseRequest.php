<?php

namespace App\Common\Packages\Payment\Gateways\AlfaBitPay\Message;

use App\Common\Packages\Payment\Exception\{
    InvalidRequestException
};

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $response = $this->httpRequest->request->all();
        $this->parameters->add(
            array_intersect_key($response, array_flip((array)['signature', 'comment', 'status']))
        );

        $this->validate(
            'signature', 'comment', 'status'
        );

        return $this->httpRequest->request->all();
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
