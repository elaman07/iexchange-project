<?php


namespace App\Common\Packages\Payment\Gateways\AlfaBitPay\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        $currency = $this->getCoinCurrency();
        if(!empty($network_code)) {
            $currency = $network_code;
        }

        return [
            'code'      =>  $currency,
            'comment'   =>  (string)$this->getTransactionId(),
            'cbUrl'     =>  $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $timestamp = time();
        $body = (!empty($data) ? json_encode($data, JSON_UNESCAPED_SLASHES) : null);

        $headers = [
            'x-access-apikey' =>  $this->getApiKey()
        ];
        $headers['Content-Type'] = 'application/json';
        $http_response = \Http::baseUrl('https://pay.alfabit.org')->asJson()->withHeaders($headers)->post('/api/user/createPayment', $data)->body();

        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'AlfaBitPay',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => 'https://pay.alfabit.org/api/user/createPayment',
            'headers' => json_encode($headers),
            'content' => json_encode($data),
            'response' => $http_response
        ]);

        return $this->response = new AddressResponse($this, json_decode($http_response, true));
    }
}
