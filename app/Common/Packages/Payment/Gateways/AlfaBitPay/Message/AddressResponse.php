<?php


namespace App\Common\Packages\Payment\Gateways\AlfaBitPay\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['success']) && $this->data['success'] == true;
    }

    public function isRedirect(): bool
    {
        return false;
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->data['result']['address'];
    }

    /**
     * Получаем валюту
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['result']['tokenCode'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return (string)$this->data['result']['payRecipientMemoTag'];
    }

    /**
     * Получаем описание
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->data['result']['comment'];
    }

    public function getUniqueOrderHashId()
    {
        return $this->data['result']['uid'];
    }

    public function getUniqueServiceId()
    {
        return $this->data['result']['id'];
    }
}
