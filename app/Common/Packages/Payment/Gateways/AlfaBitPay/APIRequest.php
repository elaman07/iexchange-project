<?php

namespace App\Common\Packages\Payment\Gateways\AlfaBitPay;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];


    /**
     * URL AlfaBitPay API
     *
     * @var string
     */
    const API_URL = 'https://pay.alfabit.org';

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response =  $this->request('GET', '/api/user/getMerchantBalances', [
            'currency' => $currency
        ])['result'][\Str::upper($currency)];

        // Получение баланса
        if(isset($response['balance']))
            return $response['balance'];

        throw new \Exception('API Не настроено');

    }

    /**
     * Отправка средств
     *
     * @param array $options
     * @return array|bool
     * @throws Exception
     */
    public function transfer(array $options = [])
    {
        $params = [
            'code' => Str::upper($options['currency']),
            'amount'    => (float)$options['amount'],
            'comment' => (string)$options['description'],
            'addressTo' => (string)$options['address']
        ];

        if(isset($options['dest_tag'])) {
            $params['memoTag'] = (string)$options['dest_tag'];
        }

        return $this->request('POST', '/api/user/createWithdraw', $params);
    }


    /**
     * Детали транзакции
     *
     * @param string $id
     * @return array|bool
     * @throws \Exception
     */
    public function findTransaction(string $id)
    {
        return $this->request('GET', '/api/public/getTransactionByUid', [
            'uid' => (string)$id
        ]);
    }

    /**
     * Запросы WestWallet
     *
     * @param string $method
     * @param $url
     * @param array $params
     * @return array|bool
     * @throws \Exception
     */
    public function request(string $method, $url, array $params = [])
    {
        $request = Http::baseUrl(self::API_URL)->withHeaders([
            'x-access-apikey' =>  $this->parameters['api_key']
        ])->{Str::lower($method)}($url, $params);

        $httpResponse =  $request->json();


        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'AlfaBitPay',
            'url' => self::API_URL.$url,
            'headers' => json_encode([
                'x-access-apikey' =>  $this->parameters['api_key']
            ]),
            'content' => json_encode($params),
            'response' => json_encode($httpResponse)
        ]);

        if(isset($httpResponse['success']) and $httpResponse['success'] == 1) {
            return $httpResponse;
        }
        throw new \Exception($httpResponse['msg']);
    }
}
