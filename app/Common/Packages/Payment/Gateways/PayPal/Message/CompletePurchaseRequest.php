<?php

namespace App\Common\Packages\Payment\Gateways\PayPal\Message;

use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Facades\Http;

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate('login_account');

        return array_merge([
            'cmd' => '_notify-validate',
        ], $this->httpRequest->request->all());
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        $httpResponse = Http::post('https://ipnpb.paypal.com/cgi-bin/webscr', $data);
        $data['_result'] = $httpResponse->json();

        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
