<?php


namespace App\Common\Packages\Payment\Gateways\PayPal\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        if ($this->getResult() !== 'VERIFIED') {
            throw new InvalidResponseException('Not verified');
        }

        if ($this->getTestMode() == true) {
            throw new InvalidResponseException('Invalid test mode');
        }

        if ($this->getTransactionStatus() !== 'Completed') {
            throw new InvalidResponseException('Invalid payment status');
        }
    }

    /**
     * Whether the payment is successful.
     * @return boolean
     */
    public function isSuccessful()
    {
        return true;
    }

    /**
     * Whether the payment is test.
     * @return boolean
     */
    public function getTestMode()
    {
        return (bool) $this->data['test_ipn'];
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTransactionId()
    {
        return $this->data['item_number'];
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getTransferId()
    {
        return $this->data['txn_id'];
    }

    /**
     * Returns the transaction status.
     * @return string
     */
    public function getTransactionStatus()
    {
        return $this->data['payment_status'];
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getAmount()
    {
        return $this->data['payment_gross'] ? : $this->data['mc_gross'];
    }

    /**
     * Returns the result, injected by [[CompletePurchaseRequest::sendData()]].
     * @return mixed
     */
    public function getResult()
    {
        return $this->data['_result'];
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getFee()
    {
        return $this->data['payment_fee'] ? : $this->data['mc_fee'];
    }

    /**
     * Returns the currency.
     * @return string
     */
    public function getCurrency()
    {
        return strtoupper($this->data['mc_currency']);
    }
}
