<?php

namespace App\Common\Packages\Payment\Gateways\PayPal\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Логин безнес аккаунта
     *
     * @return string
     */
    public function getLoginAccount(): string
    {
        return $this->getParameter('login_account');
    }

    /**
     * Установить Логин безнес аккаунта
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setLoginAccount(string $value): AbstractRequest
    {
        return $this->setParameter('login_account', $value);
    }
}
