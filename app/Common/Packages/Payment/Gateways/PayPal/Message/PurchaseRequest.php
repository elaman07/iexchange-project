<?php

namespace App\Common\Packages\Payment\Gateways\PayPal\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate('amount', 'currency', 'description');

        return [
            'cmd'           => '_xclick',
            'bn'            => 'PP-BuyNowBF:btn_paynowCC_LG.gif:NonHostedGuest',
            'item_name'     => $this->getDescription(),
            'amount'        => $this->getAmount(),
            'currency_code' => strtoupper($this->getCurrency()),
            'business'      => $this->getLoginAccount(),
            'notify_url'    => $this->getNotifyUrl(),
            'return'        => $this->getReturnUrl(),
            'cancel_return' => $this->getCancelUrl(),
            'item_number'   => $this->getTransactionId(),
            'charset'       => 'utf-8',
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
