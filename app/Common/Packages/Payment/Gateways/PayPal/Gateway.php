<?php

namespace App\Common\Packages\Payment\Gateways\PayPal;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'PayPal';

    /**
     * Получить Логин безнес аккаунта
     *
     * @return string
     */
    public function getLoginAccount(): string
    {
        return $this->getParameter('login_account');
    }

    /**
     * Установить Логин безнес аккаунта
     *
     * @param string $value
     * @return Gateway
     */
    public function setLoginAccount(string $value): Gateway
    {
        return $this->setParameter('login_account', $value);
    }
    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'login_account' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }
}
