<?php

namespace App\Common\Packages\Payment\Gateways\Merchant001\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить API Key
     *
     * @return string
     */
    public function getApiToken(): string
    {
        return $this->getParameter('api_token');
    }

    /**
     * Установить API Key
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiToken(string $value): AbstractRequest
    {
        return $this->setParameter('api_token', $value);
    }
}
