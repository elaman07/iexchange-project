<?php

namespace App\Common\Packages\Payment\Gateways\Merchant001\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'api_token', 'amount', 'currency', 'transactionId'
        );

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (int)round($amount + $amount * $commission / 100, 0);
        }

        $codes_options = json_decode(file_get_contents(storage_path('/gateways/options/merchant001_codes.json')), true);
        $code_find = collect($codes_options)->first(function($query) use ($order_data) {
            return $query['id'] == $order_data->merchant->method_pay;
        });

        $options = [
            'isPartnerFee' => true,
            'pricing' => [
                'local' => [
                    'amount' => (float)$total_amount,
                    'currency' => $this->getCurrency()
                ]
            ],
            'selectedProvider' => [
                'type' => $code_find['type'],
                'method' => $code_find['method']
            ],

            "invoiceId" => $this->getTransactionId()
        ];


        return $options;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $order_data = $this->getOrderData();
        $httpRequest = Http::baseUrl('https://api.merchant001.io')
            ->withToken($this->getApiToken());


        $httpResponse = $httpRequest->post('/v1/transaction/merchant', $data)->json();

        if(isset($httpResponse['id'])) {
            if($order_data->merchant->type_pay == 0) {
                return $this->response = new PurchaseResponse($this, $httpResponse);
            }

            $httpIdResponse = $httpRequest->get('/v1/transaction/merchant/requisite/'.$httpResponse['id'])->json();
            return $this->response = new PurchaseH2HResponse($this, $httpIdResponse);
        }

        throw new \Exception(json_encode($httpResponse));
    }
}
