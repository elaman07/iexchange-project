<?php

namespace App\Common\Packages\Payment\Gateways\Merchant001\Message;


use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RedirectResponseInterface,
    RequestInterface
};


class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['paymentUrl'])) {
            return $this->data['paymentUrl'];
        }
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    public function getRedirectData()
    {
        return $this->data;
    }

    public function getLabel()
    {
        return $this->data['invoiceId'];
    }
}


