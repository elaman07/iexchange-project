<?php

namespace App\Common\Packages\Payment\Gateways\Merchant001;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance($currency = null)
    {
        $response = $this->call('get', '/account/balance');
        $balance = collect($response)->first(function($query) use ($currency) {
           return $query['currencyName'] == $currency;
        });

        return $balance['balance'];
    }

    /**
     * Баланс кошелька
     *
     * @param null $order_id
     * @return mixed
     * @throws Exception
     */
    public function findTransaction($order_id = null)
    {
        return $this->call('get', '/v1/transaction/merchant/'.$order_id);
    }


    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @param $order_id
     * @return mixed
     * @throws Exception
     */
    public function transfer($options = []) {
        return $this->call('post', '/payout/process', $options);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $method
     * @param string $path
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    private function call(string $method, string $path, array $options = [])
    {
        $httpRequest = Http::baseUrl('https://api.merchant001.io')
            ->withToken($this->parameters['api_token']);

        $httpResponse = $httpRequest->{$method}($path, $options)->json();

        \Log::debug(json_encode($httpResponse));
        return $httpResponse;
    }
}
