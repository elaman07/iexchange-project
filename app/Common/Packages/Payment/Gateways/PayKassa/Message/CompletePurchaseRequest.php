<?php

namespace App\Common\Packages\Payment\Gateways\PayKassa\Message;

use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $response = $this->httpRequest->request->all();
        $this->parameters->add(
            array_intersect_key($response, array_flip((array)['private_hash', 'system', 'order_id']))
        );

        $this->validate(
            'private_hash', 'system', 'order_id', 'id_shop', 'secret_key'
        );

        return $response;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
