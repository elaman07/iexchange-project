<?php


namespace App\Common\Packages\Payment\Gateways\PayKassa\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Facades\Http;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @var
     */
    protected $transaction;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        $this->transaction = $this->findTransaction();
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return isset($this->data['private_hash']);
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return !isset($this->data['private_hash']);
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['private_hash'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->transaction['data']['order_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->transaction['data']['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->transaction['data']['currency'];
    }


    /**
     * Получаем детали транзакции
     *
     * @throws InvalidResponseException|\Exception
     */
    protected function findTransaction()
    {
        $response = Http::asForm()->post('https://paykassa.pro/sci/0.4/index.php', [
            'func' => 'sci_confirm_order',
            'sci_id' => $this->request->getIdShop(),
            'sci_key' => $this->request->getSecretKey(),
            'private_hash' => $this->data['private_hash'],
            'test'  =>  0
        ])->json();


        if($response['error'] == true) {
            throw new \Exception($response['message']);
        }

        return $response;
    }
}
