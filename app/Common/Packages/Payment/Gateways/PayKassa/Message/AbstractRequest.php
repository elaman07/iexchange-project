<?php

namespace App\Common\Packages\Payment\Gateways\PayKassa\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить ID магазина
     *
     * @return string
     */
    public function getIdShop() : string
    {
        return $this->getParameter('id_shop');
    }

    /**
     * Установка ID магазина
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setIdShop(string $value): AbstractRequest
    {
        return $this->setParameter('id_shop', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey() : string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установка Секретного ключа
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }
}
