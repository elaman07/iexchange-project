<?php

namespace App\Common\Packages\Payment\Gateways\PayKassa\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Поддерживающие системы
     *
     * @var array
     */
    private array $supportedSystems = [
        'btc'   => 11,
        'eth'   => 12,
        'ltc'   => 14,
        'doge'  => 15,
        'dash'  => 16,
        'bch'   => 18,
        'zec'   => 19,
        'trx'   => 27,
        'xlm'   => 28,
        'bnb'   => 29,
        'xrp'   => 22
    ];

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'coinAmount', 'transactionId');

        return [
            'currency'       =>  $this->getCoinCurrency(),
            'order_id'       =>  (string)$this->getTransactionId(),
            'amount'         =>  floatval($this->getCoinAmount()),
            'comment'        =>  $this->getDescription(),
            'system'         =>  (int)$this->supportedSystems[Str::lower(Str::studly($this->getCoinCurrency()))]
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $body = $data ? http_build_query(array_merge([
            'func' => 'sci_create_order_get_data',
            'sci_id' => $this->getIdShop(),
            'sci_key' => $this->getSecretKey()
        ], $data)) : null;
        $httpResponse = $this->httpClient->request('POST', 'https://paykassa.pro/sci/0.4/index.php', [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ], $body);

        $response =  json_decode($httpResponse->getBody()->getContents(), true);
        if($response['error'] == 'true') {
            throw new \Exception($response['message']);
        }

        return $this->response = new AddressResponse($this, $response);
    }
}
