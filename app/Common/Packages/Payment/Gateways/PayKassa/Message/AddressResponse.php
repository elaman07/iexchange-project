<?php


namespace App\Common\Packages\Payment\Gateways\PayKassa\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['error']) && $this->data['error'] == '';
    }

    public function isRedirect(): bool
    {
        return false;
    }

    public function getAddress(): string
    {
        if(isset($this->data['data']))
            return $this->data['data']['wallet'];
    }

    /**
     * Получаем валюту
     *
     * @return string
     */
    public function getCurrency(): string
    {
        if(isset($this->data['data']))
            return $this->data['data']['currency'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->data['data']['tag'];
    }
    /**
     * Получаем описание
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->data['data']['order_id'];
    }
}
