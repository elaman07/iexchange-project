<?php

namespace App\Common\Packages\Payment\Gateways\PayKassa;

use Illuminate\Support\Facades\Http;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Поддерживающие системы
     *
     * @var array
     */
    private array $supportedSystems = [
        'btc'   => 11,
        'eth'   => 12,
        'ltc'   => 14,
        'doge'  => 15,
        'dash'  => 16,
        'bch'   => 18,
        'zec'   => 19,
        'trx'   => 27,
        'xlm'   => 28,
        'bnb'   => 29,
        'xrp'   => 22
    ];

    /**
     * URL Paykassa API
     *
     * @var string
     */
    const API_URL = 'https://paykassa.pro';

    /**
     * Конструктор
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получить баланс
     *
     * @param null $currency
     * @return array|bool
     * @throws \Exception
     */
    public function getBalance($currency = null)
    {
        $response = $this->request('/api/0.5/index.php', 'api_get_shop_balance');
        if(!empty($response['error']))
            throw new \Exception($response['message']);

        return collect($response['data'])->first(function($key, $value) use($currency) {
            if(explode('_', $value)[1] == mb_strtolower($currency))
                return $key;
            return 0;
        });
    }

    /**
     * Получение деталей транзакции
     *
     * @param string $private_hash
     * @return mixed
     * @throws \Exception
     */
    public function findTransaction(string $private_hash)
    {
        $response = $this->request('/sci/0.4/index.php', 'sci_confirm_transaction_notification', [
            'private_hash' => $private_hash,
            'test'  =>  0
        ]);
        if(($response['error'])) {
            throw new \Exception($response['message']);
        }
        return $response['data'];
    }

    /**
     * Отправляем средства клиенту на счет
     *
     * @param array $options
     * @return mixed
     * @throws \Exception
     */
    public function transfer(array $options = [])
    {
        $data = [
            'system' => (int)$this->supportedSystems[mb_strtolower($options['currency'])],
            'amount' => (float)$options['amount'],
            'currency' => (string)$options['currency'],
            'number' => (string)$options['address']
        ];

        if(isset($options['tag'])) {
            $data['tag'] = $options['tag'];
        }

        $response = $this->request('/api/0.5/index.php', 'api_payment', $data);

        if(empty($response['error']))
            return $response;

        throw new \Exception($response['message']);
    }

    /**
     * Запросы Paykassa
     *
     * @param $url
     * @param string $method
     * @param array $params
     * @return array|bool
     * @throws \Exception
     */
    public function request($url, string $method, array $params = [])
    {
        if($url == '/sci/0.4/index.php')
        {
            $data = array_merge([
                'func'  =>  $method,
                'sci_id' => $this->parameters['id_shop'],
                'sci_key' => $this->parameters['secret_key']
            ], $params);
        } else {
            $data = array_merge([
                'func'  =>  $method,
                'api_id' => $this->parameters['api_id'],
                'api_key' => $this->parameters['api_secret'],
                'shop' => $this->parameters['id_shop'],
            ], $params);
        }

        $response = Http::baseUrl(self::API_URL)->asForm()->post($url, $data)->json();
        if($response['error'] == true) {
            throw new \Exception($response['message']);
        }

        return $response;
    }
}
