<?php

namespace App\Common\Packages\Payment\Gateways\PayKassa;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\PayKassa\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'PayKassa';

    /**
     * Получить ID магазина
     *
     * @return string
     */
    public function getIdShop() : string
    {
        return $this->getParameter('id_shop');
    }

    /**
     * Установка ID магазина
     *
     * @param string $value
     * @return Gateway
     */
    public function setIdShop(string $value): Gateway
    {
        return $this->setParameter('id_shop', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey() : string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установка Секретного ключа
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Получить API ID
     *
     * @return string
     */
    public function getApiId() : string
    {
        return $this->getParameter('api_id');
    }

    /**
     * Установка API ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiId(string $value): Gateway
    {
        return $this->setParameter('api_id', $value);
    }

    /**
     * Получить API пароль
     *
     * @return string
     */
    public function getApiSecret() : string
    {
        return $this->getParameter('api_secret');
    }

    /**
     * Установка API пароль
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiSecret(string $value): Gateway
    {
        return $this->setParameter('api_secret', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'id_shop' => '',
            'secret_key' => '',
            'api_id' => '',
            'api_secret' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
