<?php

namespace App\Common\Packages\Payment\Gateways\Billline\Message;

use App\Common\Packages\Payment\Exception\{
    InvalidRequestException,
    InvalidResponseException
};

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $response = $this->httpRequest->request->all();
        $this->parameters->add(
            array_intersect_key($response, array_flip((array)['co_order_no', 'co_sign', 'co_merchant_id', 'co_merchant_uuid', 'co_inv_st']))
        );

        $this->validate(
            'merchant_id', 'co_order_no', 'co_sign', 'co_merchant_id', 'co_merchant_uuid', 'co_inv_st'
        );

        return $response;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
