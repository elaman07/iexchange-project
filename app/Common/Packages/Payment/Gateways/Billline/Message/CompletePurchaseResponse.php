<?php


namespace App\Common\Packages\Payment\Gateways\Billline\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        if ($this->data['co_cur'] != $this->request->getCurrency()) {
            throw new InvalidResponseException("Invalid co_cur: {$this->data['co_cur']}");
        }

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful()
    {
        return $this->data['co_inv_st'] == 'success';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->data['co_inv_st'] != 'success';
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['co_inv_id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['co_order_no'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['co_amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['co_cur'];
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['co_sign'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        $dataSet = $this->getData();
        ksort($dataSet, SORT_STRING);
        array_push($dataSet, $this->request->getSecretKey());
        unset($dataSet['co_sign']);


        $signString = implode(':', $dataSet);
        return base64_encode(md5($signString, true));
    }
}
