<?php

namespace App\Common\Packages\Payment\Gateways\Billline\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'merchant_id', 'currency', 'amount', 'transactionId'
        );

        return [
            'merchant'      =>  $this->getMerchantId(),
            'item_name'     =>  $this->getDescription(),
            'order'         =>  $this->getTransactionId(),
            'amount'        =>  $this->getAmount(),
            'currency'      =>  $this->getCurrency(),
            'ip'            =>  Request::getClientIp()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
