<?php

namespace App\Common\Packages\Payment\Gateways\Billline;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Billline';

    /**
     * ID мерчанта
     *
     * @return string
    */
    public function getMerchantId() : string
    {
        return $this->getParameter('merchant_id');
    }

    /**
     * Установка ID мерчанта
     *
     * @param string $value
     * @return Gateway
     */
    public function setMerchantId(string $value): Gateway
    {
        return $this->setParameter('merchant_id', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установка Секретного ключа
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'merchant_id' => '',
            'secret_key' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
