<?php

namespace App\Common\Packages\Payment\Gateways\Billline;

use Illuminate\Support\Facades\Http;

class APIRequest
{
    /**
     * URL Payeer API
     *
     * @var string
     */
    const API_URL = 'https://api.billline.net';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Создаем констркутор Payeer
     *
     * @param $config
     */
    public function __construct($config)
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса по кошелькам пользователя.
     *
     * @param null $currency
     * @return array | float
     */
    public function getBalance($currency = null)
    {
        $response = (array)$this->call('payment/balance', [
            'currency' => $currency
        ]);

        if(mb_strtolower($response['status']) == 'success')
            return $response['balance'];

        return 0;
    }

    /**
     * Отправляем средства клиенту
     *
    */
    public function transfer(array $params = []): array
    {
        $options = [
            'UAH' => 1,
            'RUB' => 3,
            'USD' => 8,
            'EUR' => 9
        ];

        return (array)$this->call('merchant/api/payout_send', [
            'method' => $options[mb_strtoupper($params['currency'])],
            'payout_id' => $params['id'],
            'account' => $params['account'],
            'amount' => $params['amount'],
            'currency' => $params['currency']
        ]);
    }

    /**
     * Вызов определенных действий для "PayeerAPI"
     *
     * @param string $url
     * @param array $params
     * @return array|mixed
     */
    protected function call(string $url, array $params = [])
    {
        $data = array_merge([
            'merchant'  =>  $this->parameters['merchant_id'],
        ], $params);

        $dataSet = $data;
        ksort($dataSet, SORT_STRING);
        array_push($dataSet, $this->parameters['secret_key']);
        $signString = implode(':', $dataSet);
        $calc_sign = base64_encode(md5($signString, true));
        $data['sign'] = $calc_sign;


        return Http::baseUrl(static::API_URL)->post($url, $data)->json();
    }
}
