<?php

namespace App\Common\Packages\Payment\Gateways\CryptoProcessing;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\CryptoProcessing\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'CryptoProcessing';

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiKey(string $value): Gateway
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить Приватный ключ
     *
     * @return string
     */
    public function getApiSecret(): string
    {
        return $this->getParameter('api_secret');
    }

    /**
     * Установить Приватный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiSecret(string $value): Gateway
    {
        return $this->setParameter('api_secret', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'api_key' => '',
            'api_secret' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }


    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
