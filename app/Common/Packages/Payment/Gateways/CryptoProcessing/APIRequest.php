<?php

namespace App\Common\Packages\Payment\Gateways\CryptoProcessing;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string|null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response =  $this->request('POST', 'accounts/list', [
            'currency' => $currency
        ]);

        $data = collect($response['data'])->filter(function($item) use($currency){
            return $item['currency'] == mb_strtoupper($currency);
        })->first();

        // Получение баланса
        if(isset($data['balance']))
            return $data['balance'];

        throw new \Exception('API Не настроено');
    }

    /**
     * Проверка адреса
     *
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function checkAddress(array $options = []): bool
    {
        $response = $this->request('POST', 'addresses/check', $options);

        if(isset($response['errors'])) {
            return 0;
        }

        if(!isset($response['data']))
            throw new \Exception('Ошибка проверки адреса');

        return $response['data']['internal'];
    }

    /**
     * Отправка средств
     *
     * @param array $options
     * @return array|bool
     * @throws \Exception
     */
    public function transfer(array $options = [])
    {

        $paramsArray = [
            'foreign_id' => 'order:'.$options['order_id'],
            'amount' => (string)$options['amount'],
            'currency' => mb_strtoupper($options['currency']),
            'address' => (string)$options['address']
        ];

        if(isset($options['tag']))
            $paramsArray['tag'] = (string)$options['tag'];

        return $this->request('POST', 'withdrawal/crypto', $paramsArray);
    }

    /**
     * Запросы WestWallet
     *
     * @param string $method
     * @param $url
     * @param array $paramsArray
     * @return array|bool
     * @throws \Exception
     */
    public function request(string $method, $url, array $paramsArray = [])
    {
        $requestBody = json_encode($paramsArray);
        $signature   = hash_hmac('sha512', $requestBody, $this->parameters['api_secret']);

        $client = Http::baseUrl('https://app.sandbox.cryptoprocessing.com');

        $http = $client->withHeaders([
            'X-Processing-Key' =>  $this->parameters['api_key'],
            'X-Processing-Signature' => $signature,
            'Content-Type' => 'application/json'
        ]);

        if($method == 'POST') {
            $response = $http->post('/api/v2/'.$url, $paramsArray)->json();
        } else {
            $response = $http->get('/api/v2/'.$url, $paramsArray)->json();
        }

        if(isset($response['errors']))
            throw new \Exception(json_encode($response));

        return $response;
    }
}
