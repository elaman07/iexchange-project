<?php

namespace App\Common\Packages\Payment\Gateways\CryptoProcessing\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKey(string $value): AbstractRequest
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить Приватный ключ
     *
     * @return string
     */
    public function getApiSecret(): string
    {
        return $this->getParameter('api_secret');
    }

    /**
     * Установить Приватный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiSecret(string $value): AbstractRequest
    {
        return $this->setParameter('api_secret', $value);
    }

}
