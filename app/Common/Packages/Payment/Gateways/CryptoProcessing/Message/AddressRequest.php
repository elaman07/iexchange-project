<?php


namespace App\Common\Packages\Payment\Gateways\CryptoProcessing\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [
            'currency'  =>  $this->getCoinCurrency(),
            'foreign_id'  => (string)$this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $signature   = hash_hmac('sha512', json_encode($data), $this->getApiSecret());

        $url =  'https://app.sandbox.cryptoprocessing.com';
        $response = Http::baseUrl($url)->withHeaders([
            'X-Processing-Key' => $this->getApiKey(),
            'X-Processing-Signature' => $signature,
            'Content-Type' => 'application/json'
        ])->post('/api/v2/addresses/take', $data)->json();

        if(isset($response['errors'])) {
            throw new \Exception($response['errors']);
        }

        return $this->response = new AddressResponse($this, $response);
    }
}
