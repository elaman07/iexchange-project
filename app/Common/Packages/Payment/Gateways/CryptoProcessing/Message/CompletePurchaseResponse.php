<?php


namespace App\Common\Packages\Payment\Gateways\CryptoProcessing\Message;

use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RequestInterface
};
use Illuminate\Support\Arr;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['status'] == 'confirmed';
    }

    /**
     * Если транзакция в ожидании
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        return in_array($this->data['status'], ['not_confirmed', 'pending']);
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->data['status'] == 'cancelled';
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['crypto_address']['foreign_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['currency_received']['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['currency_received']['currency'];
    }

    /**
     * Получение ID в blockchain
     *
     * @return string
     */
    public function getBlockchainId(): string
    {
        $data = Arr::first($this->data['transactions']);
        return $data['txid'];
    }

    /**
     * Получение счетчика подтверждений
     *
     * @return mixed
     */
    public function getBlockchainConfirmation()
    {
        $data = Arr::first($this->data['transactions']);
        return $data['confirmations'];
    }
}
