<?php


namespace App\Common\Packages\Payment\Gateways\CryptoProcessing\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['data']) && (int)$this->data['data']['id'] > 0;
    }

    public function isRedirect(): bool
    {
        return false;
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->data['data']['address'];
    }

    /**
     * Получаем валюту
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['data']['currency'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->data['data']['tag'] ?? '';
    }

    /**
     * Получаем описание
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->data['data']['foreign_id'];
    }
}
