<?php

namespace App\Common\Packages\Payment\Gateways\CryptoSwap\Message;

use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $response = $this->httpRequest->request->all();
        $this->parameters->add(
            array_intersect_key($response, array_flip((array)['merchant_id', 'order_id', 'status', 'amount_currency', 'invoice_id', 'merchant_amount', 'signature', 'fiat_currency']))
        );

        $this->validate(
            'merchant_id', 'order_id', 'status', 'amount_currency', 'invoice_id', 'merchant_amount', 'signature', 'fiat_currency'
        );

        return $response;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
