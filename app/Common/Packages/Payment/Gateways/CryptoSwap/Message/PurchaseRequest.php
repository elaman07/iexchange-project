<?php

namespace App\Common\Packages\Payment\Gateways\CryptoSwap\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'token_key'
        );

        // Итоговая сумма
        $amount = (float)$this->getCustomAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getCustomAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (float)iex_number_format( $amount + $amount * $commission / 100, 2);
        }

        return [
            'order_id'  => (string)$this->getTransactionId(),
            'amount' => iex_number_format($total_amount, 2),
            'email' => $order_data->user->email,
            'currency_xml'  =>  $order_data->direction_exchange->currency1->designation_xml,
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $httpResponse = Http::baseUrl('https://crypto-swap.ru/api')
            ->withToken($this->getTokenKey());


        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        $data_response = [];
        if(!empty($network_code))
        {
            $data_response = [
                'get_code' => $network_code,
                'get_sum' => $data['amount'],
                'email' => $data['email']
            ];
        } else {
            $data_response = [
                'get_code' => $data['currency_xml'],
                'get_sum' => $data['amount'],
                'email' => $data['email']
            ];
        }


        $options = $data_response;
        $responseInvoiceData = $httpResponse->post('/bid/create', $options)->json();

        if($responseInvoiceData['code'] != 200) {
            throw new \Exception(json_encode($responseInvoiceData));
        }

        return $this->response = new PurchaseResponse($this, $responseInvoiceData);
    }
}
