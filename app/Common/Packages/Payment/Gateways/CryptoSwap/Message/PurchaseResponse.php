<?php

namespace App\Common\Packages\Payment\Gateways\CryptoSwap\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['link'])) {
            return $this->data['link'];
        }
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    public function getRedirectData()
    {
        return '';
    }


    public function getLabel()
    {
        return $this->data['id'];
    }
}
