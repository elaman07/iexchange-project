<?php

namespace App\Common\Packages\Payment\Gateways\CryptoSwap\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить
     *
     * @return string
     */
    public function getTokenKey(): string
    {
        return $this->getParameter('token_key');
    }

    /**
     * Установить
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setTokenKey(string $value): AbstractRequest
    {
        return $this->setParameter('token_key', $value);
    }

}
