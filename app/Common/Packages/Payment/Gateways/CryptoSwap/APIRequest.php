<?php

namespace App\Common\Packages\Payment\Gateways\CryptoSwap;


use DateTimeImmutable;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string|null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        return 0;
    }

    public function findTransaction(string $id)
    {
        $request = $this->request('post', '/bid', [
            'id' => $id,
        ]);

        return $request;
    }


    /**
     * Запросы
     * @param $method
     * @param $path
     * @param $options
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function request($method, $path, $options = [])
    {
        $httpResponse = Http::baseUrl('https://crypto-swap.ru/api')
            ->withToken($this->parameters['token_key']);

        $httpResponse = $httpResponse->{$method}($path, $options)->json();

        if($httpResponse['code'] != 200) {
            throw new \Exception('Токен Rapira не создан');
        }


        return $httpResponse;
    }
}
