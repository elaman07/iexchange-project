<?php

namespace App\Common\Packages\Payment\Gateways\CryptoSwap;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\Rapira\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'CryptoSwap';

    /**
     * Получить
     *
     * @return string
     */
    public function getTokenKey(): string
    {
        return $this->getParameter('token_key');
    }

    /**
     * Установить
     *
     * @param string $value
     * @return Gateway
     */
    public function setTokenKey(string $value): Gateway
    {
        return $this->setParameter('token_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'token_key' => '',
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
     */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
