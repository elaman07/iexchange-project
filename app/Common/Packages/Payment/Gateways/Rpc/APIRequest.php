<?php

namespace App\Common\Packages\Payment\Gateways\Rpc;

use App\Common\Packages\Payment\Gateways\Rpc\Includes\ConnectionClient;
use Exception;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получить баланс
     *
     * @param string $currency
     * @return array|bool
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        return $this->rpcClient()->getBalance()->get();
    }

    /**
     * Возвращает подробную информацию о транзакции
     *
     * @param $txid string
     * @return array
     * @throws \Exception
     */
    public function getTransaction(string $txid): array
    {
        $transaction = $this->rpcClient()->gettransaction($txid);
        return $transaction->get();
    }

    /**
     * Получаем список транзакций
     *
     * @param null $account
     * @param int $count
     * @param int $from
     * @return mixed
     */
    public function listTransactions($account = null, int $count = 10, int $from = 0)
    {
        return $this->rpcClient()->listtransactions($account, $count, $from)->get();
    }

    /**
     *  Перевод средств на адрес Криптовалюты
     *
     * @param string $address
     * @param float|string $amount
     * @param array $options
     * @return string
     */
    public function transfer(string $address, $amount, array $options = []): string
    {
        return $this->rpcClient()->sendtoaddress($address, $amount, $options['comment'] ?? null)->get();
    }

    /**
     * Устанавливаем соединение с Bitcoin
     *
     */
    protected function rpcClient(): ConnectionClient
    {
        return new ConnectionClient([
            'scheme'    =>  'http',
            'host'      =>  $this->parameters['rpc_host'],
            'port'      =>  $this->parameters['rpc_port'],
            'user'      =>  $this->parameters['rpc_username'],
            'pass'      =>  $this->parameters['rpc_password']
        ]);
    }

}
