<?php

namespace App\Common\Packages\Payment\Gateways\Rpc\Includes\Exceptions;

use RuntimeException;

class ClientException extends RuntimeException
{
}
