<?php

namespace App\Common\Packages\Payment\Gateways\Rpc\Includes\Exceptions;

use RuntimeException;

class CoinException extends RuntimeException
{
    /**
     * Construct new bitcoind exception.
     *
     * @param object $error
     *
     * @return void
     */
    public function __construct($error)
    {
        parent::__construct($error['message'], $error['code']);
    }
}
