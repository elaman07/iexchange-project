<?php


namespace App\Common\Packages\Payment\Gateways\Rpc\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return !empty($this->data['address']);
    }

    public function isRedirect(): bool
    {
        return false;
    }

    public function getAddress(): string
    {
        return $this->data['address'];
    }
}
