<?php

namespace App\Common\Packages\Payment\Gateways\Rpc\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Gateways\Rpc\Includes\ConnectionClient;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('rpc_host', 'rpc_port', 'rpc_username', 'rpc_password');

        return [
            'label'  =>  $this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $client = new ConnectionClient([
            'scheme'    =>  'http',
            'host'      =>  $this->getRpcHost(),
            'port'      =>  $this->getRpcPort(),
            'user'      =>  $this->getRpcUsername(),
            'pass'      =>  $this->getRpcPassword()
        ]);

        $address = $client->getnewaddress('№'.$data['label'])->get();

        return $this->response = new AddressResponse($this, [
            'address' => $address
        ]);
    }
}
