<?php

namespace App\Common\Packages\Payment\Gateways\Rpc\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Host
     *
     * @return string
     */
    public function getRpcHost() : string
    {
        return $this->getParameter('rpc_host');
    }

    /**
     * Установка Host
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setRpcHost(string $value): AbstractRequest
    {
        return $this->setParameter('rpc_host', $value);
    }

    /**
     * Получить RPC Port
     *
     * @return string
     */
    public function getRpcPort() : string
    {
        return $this->getParameter('rpc_port');
    }

    /**
     * Установка RPC Port
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setRpcPort(string $value): AbstractRequest
    {
        return $this->setParameter('rpc_port', $value);
    }

    /**
     * Получить RPC Username
     *
     * @return string
     */
    public function getRpcUsername() : string
    {
        return $this->getParameter('rpc_username');
    }

    /**
     * Установка RPC Username
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setRpcUsername(string $value): AbstractRequest
    {
        return $this->setParameter('rpc_username', $value);
    }

    /**
     * Получить RPC Password
     *
     * @return string
     */
    public function getRpcPassword() : string
    {
        return $this->getParameter('rpc_password');
    }

    /**
     * Установка RPC Password
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setRpcPassword(string $value): AbstractRequest
    {
        return $this->setParameter('rpc_password', $value);
    }
}
