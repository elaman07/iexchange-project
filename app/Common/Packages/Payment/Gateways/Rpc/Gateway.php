<?php

namespace App\Common\Packages\Payment\Gateways\Rpc;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\Rpc\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Rpc';

    /**
     * Получить Host
     *
     * @return string
     */
    public function getRpcHost() : string
    {
        return $this->getParameter('rpc_host');
    }

    /**
     * Установка Host
     *
     * @param string $value
     * @return Gateway
     */
    public function setRpcHost(string $value): Gateway
    {
        return $this->setParameter('rpc_host', $value);
    }

    /**
     * Получить RPC Port
     *
     * @return string
     */
    public function getRpcPort() : string
    {
        return $this->getParameter('rpc_port');
    }

    /**
     * Установка RPC Port
     *
     * @param string $value
     * @return Gateway
     */
    public function setRpcPort(string $value): Gateway
    {
        return $this->setParameter('rpc_port', $value);
    }

    /**
     * Получить RPC Username
     *
     * @return string
     */
    public function getRpcUsername() : string
    {
        return $this->getParameter('rpc_username');
    }

    /**
     * Установка RPC Username
     *
     * @param string $value
     * @return Gateway
     */
    public function setRpcUsername(string $value): Gateway
    {
        return $this->setParameter('rpc_username', $value);
    }

    /**
     * Получить RPC Password
     *
     * @return string
     */
    public function getRpcPassword() : string
    {
        return $this->getParameter('rpc_password');
    }

    /**
     * Установка RPC Password
     *
     * @param string $value
     * @return Gateway
     */
    public function setRpcPassword(string $value): Gateway
    {
        return $this->setParameter('rpc_password', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'rpc_host' => '',
            'rpc_port' => '',
            'rpc_username' => '',
            'rpc_password' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
