<?php


namespace App\Common\Packages\Payment\Gateways\Tron\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['private_key']);
    }

    public function isRedirect(): bool
    {
        return false;
    }

    public function getAddress(): string
    {
        return $this->data['address_base58'];
    }


    public function getLabel()
    {
        return null;
    }

    public function getPrivateKey()
    {
        return $this->data['private_key'];
    }
}
