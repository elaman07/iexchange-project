<?php

namespace App\Common\Packages\Payment\Gateways\Tron;


use Exception;
use GuzzleHttp\Client;
use IEXBase\TronAPI\Exception\TronException;
use IEXBase\TronAPI\Provider\HttpProvider;
use IEXBase\TronAPI\Tron;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];


    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return float
     * @throws Exception
     */
    public function getBalance(string $currency): float
    {
        if(Str::upper($currency) != 'TRX') {
            throw new \Exception('Неверный код валюты');
        }
        return $this->request()->getBalance($this->parameters['main_address'], true);
    }

    /**
     * Получение баланса по адресу
     *
     * @param string $address
     * @return float
     * @throws TronException
     */
    public function getBalanceFromAddress(string $address): float
    {
        return $this->request()->getBalance($address, true);
    }

    /**
     * Получение транзакций по адресу
     *
     * @param string $address
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransactionByAddress(string $address)
    {
        $custom_client = new Client([
            'base_uri' => 'https://apilist.tronscan.org',
            'defaults' => [
                'headers'  => ['content-type'  => 'application/json']
            ],
        ]);

        $response = $custom_client->get('/api/transaction', [
            'query' => [
                'sort'      =>  '-timestamp',
                'limit'     =>  1,
                'start'     =>  0,
                'address'   =>  $address
            ]
        ]);
        return json_decode($response->getBody()->getContents(), true);
    }


    /**
     * Отправляем на базовый адрес
     *
     * @param $from
     * @param $private_key
     * @param $amount
     * @return mixed
     * @throws \Exception
     */
    public function sendTransactionToBase($from, $private_key, $amount)
    {
        $http = $this->request();
        $http->setAddress($from);
        $http->setPrivateKey($private_key);

        try {
            $response = $http->sendTransaction($this->parameters['main_address'], $amount);
            if(isset($response['result']) and $response['result'] == 1) {
                return [
                    'status'    =>  0,
                    'hash'      =>  $response['txID']
                ];
            }
        }catch (\Exception $e) {
            return [
                'status'    =>  1,
                'error'     =>  $e->getMessage()
            ];
        }

        return [
            'status'    =>  1,
            'error'     =>  'Error'
        ];
    }

    /**
     * Отправляем средства клиенту
     *
     * @param $to
     * @param $amount
     * @return array
     * @throws TronException
     */
    public function transfer($to, $amount): array
    {
        $http = $this->request();
        $http->setAddress($this->parameters['main_address']);
        $http->setPrivateKey($this->parameters['private_key']);


        try {
            $response = $http->sendTransaction($to, $amount);
            if(isset($response['result']) and $response['result'] == 1) {
                return [
                    'status'    =>  0,
                    'hash'      =>  $response['txID']
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status'    =>  1,
                'error'     =>  $exception->getMessage()
            ];
        }
    }

    /**
     * Запросы
     *
     * @return Tron
     * @throws TronException
     */
    public function request(): Tron
    {
        $fullNode = new HttpProvider('https://api.trongrid.io');
        $solidityNode = new HttpProvider('https://api.trongrid.io');

        $tron = new Tron($fullNode, $solidityNode);
        if(!empty($this->config['private_key'])) {
            $tron->setPrivateKey($this->parameters['private_key']);
            $tron->setAddress($this->parameters['main_address']);
        }
        return $tron;
    }
}
