<?php

namespace App\Common\Packages\Payment\Gateways\Tron;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\Tron\Message\AddressRequest;


class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Tron';


    /**
     * Получить основной адрес
     *
     * @return string
     */
    public function getMainAddress() : string
    {
        return $this->getParameter('main_address');
    }

    /**
     * Установка основного адреса
     *
     * @param string $value
     * @return Gateway
     */
    public function setMainAddress(string $value): Gateway
    {
        return $this->setParameter('main_address', $value);
    }

    /**
     * Получить приватный ключ
     *
     * @return string
     */
    public function getPrivateKey() : string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установка приватного ключа
     *
     * @param string $value
     * @return Gateway
     */
    public function setPrivateKey(string $value): Gateway
    {
        return $this->setParameter('private_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'private_key' => '',
            'main_address' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
