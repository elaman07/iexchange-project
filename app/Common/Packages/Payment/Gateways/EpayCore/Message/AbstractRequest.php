<?php


namespace App\Common\Packages\Payment\Gateways\EpayCore\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Sci id
     *
     * @return string
     */
    public function getSciId(): string
    {
        return $this->getParameter('sci_id');
    }

    /**
     * Установить SCI id
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSciId(string $value): AbstractRequest
    {
        return $this->setParameter('sci_id', $value);
    }

    /**
     * Получить Sci Password
     *
     * @return string
     */
    public function getSciPassword(): string
    {
        return $this->getParameter('sci_password');
    }

    /**
     * Установить SCI Password
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSciPassword(string $value): AbstractRequest
    {
        return $this->setParameter('sci_password', $value);
    }

    /**
     * Подписать транзакцию
     * @throws InvalidRequestException
     */
    public function signatureTx()
    {
        return hash('sha256', implode(':', [
            $this->getSciId(),
            $this->getAmount(),
            $this->getCurrency(),
            $this->getTransactionId(),
            $this->getSciPassword()
        ]));
    }
}
