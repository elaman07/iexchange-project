<?php

namespace App\Common\Packages\Payment\Gateways\EpayCore\Message;

use App\Common\Packages\Payment\Exception\{
    InvalidRequestException,
    InvalidResponseException
};

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $response = $this->httpRequest->request->all();
        $this->parameters->add(
            array_intersect_key($response, array_flip((array)['epc_order_id', 'epc_src_account', 'epc_amount', 'epc_currency_code', 'epc_batch']))
        );

        $this->validate(
            'sci_id', 'sci_password', 'epc_order_id', 'epc_src_account', 'epc_amount', 'epc_currency_code', 'epc_batch'
        );

        return $response;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
