<?php


namespace App\Common\Packages\Payment\Gateways\EpayCore\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->getSign() == $this->signatureResponse();
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->getSign() != $this->signatureResponse();
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['epc_batch'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['epc_order_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['epc_amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string|integer
     */
    public function getCurrency()
    {
        return $this->data['epc_currency_code'];
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return $this->data['epc_src_account'];
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['epc_sign'];
    }


    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        return hash('sha256', implode(':', [
            $this->data['epc_merchant_id'],
            $this->data['epc_order_id'],
            $this->data['epc_created_at'],
            $this->data['epc_amount'],
            $this->data['epc_currency_code'],
            $this->data['epc_dst_account'],
            $this->data['epc_src_account'],
            $this->data['epc_batch'],
            $this->request->getSciPassword()
        ]));
    }
}
