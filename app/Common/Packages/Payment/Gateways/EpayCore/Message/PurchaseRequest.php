<?php

namespace App\Common\Packages\Payment\Gateways\EpayCore\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     * @throws \Exception
     */
    public function getData()
    {
        $this->validate(
            'sci_id', 'sci_password', 'amount', 'currency', 'transactionId'
        );

        return [
            'epc_merchant_id'       =>  $this->getSciId(),
            'epc_amount'            =>  $this->getAmount(),
            'epc_currency_code'     =>  $this->getCurrency(),
            'epc_order_id'          =>  $this->getTransactionId(),
            'epc_sign'              =>  $this->signatureTx(),
            'epc_success_url'       =>  $this->getReturnUrl(),
            'epc_cancel_url'        =>  $this->getCancelUrl(),
            'epc_status_url'        =>  $this->getNotifyUrl(),
            'epc_descr'             =>  $this->getDescription()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
