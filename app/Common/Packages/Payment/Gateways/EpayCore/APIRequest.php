<?php

namespace App\Common\Packages\Payment\Gateways\EpayCore;

use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * URL API
     *
     * @var string
     */
    const API_URL = 'https://wallet.epaycore.com';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->request('/v1/api/balance', [
            'account' => $this->parameters['account_'.Str::lower($currency)]
        ]);

        if(isset($response['account']))
            return $response['account']['balance'];

        throw new \Exception('ePayCore: Баланс не определен');
    }

    /**
     * История транзакций
     *
     * @param array $options
     * @return array
     */
    public function getHistory(array $options = [])
    {
        return $this->request('/v1/api/history', $options);
    }

    /**
     * Информация о транзакции
     *
     * @param $id
     * @return array
     */
    public function findTransaction($id)
    {
        return $this->request('/v1/api/info', [
            'batch' => $id
        ]);
    }

    /**
     * Отправляем средства на адрес
     *
     * @param $amount
     * @param $currency
     * @param $to
     * @param null $comment
     * @return array
     */
    public function sendMoney($amount, $currency, $to, $comment = null)
    {
        return $this->request('/v1/api/transfer', [
            'src_account'   =>  $this->parameters['account_'.Str::lower($currency)],
            'dst_account'   =>  $to,
            'amount'        =>  $amount,
            'descr'         =>  $comment
        ]);
    }


    /**
     * Запрос к PerfectMoney api
     *
     * @param string $path
     * @param array $params
     * @return array|bool
     */
    public function request(string $path, array $params = [])
    {
        $data = array_merge([
            'api_id' => $this->parameters['api_id'],
            'api_secret' => $this->parameters['api_secret'],
        ], $params);


        $client = Http::baseUrl(self::API_URL)->asForm()->post($path, $data);
        return $client->json();
    }
}
