<?php

namespace App\Common\Packages\Payment\Gateways\EpayCore;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'EpayCore';


    /**
     * Получить Sci id
     *
     * @return string
     */
    public function getSciId(): string
    {
        return $this->getParameter('sci_id');
    }

    /**
     * Установить SCI id
     *
     * @param string $value
     * @return Gateway
     */
    public function setSciId(string $value): Gateway
    {
        return $this->setParameter('sci_id', $value);
    }

    /**
     * Получить Sci Password
     *
     * @return string
     */
    public function getSciPassword(): string
    {
        return $this->getParameter('sci_password');
    }

    /**
     * Установить SCI Password
     *
     * @param string $value
     * @return Gateway
     */
    public function setSciPassword(string $value): Gateway
    {
        return $this->setParameter('sci_password', $value);
    }


    /**
     * Получить Api id
     *
     * @return string
     */
    public function getApiId(): string
    {
        return $this->getParameter('api_id');
    }

    /**
     * Установить Api id
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiId(string $value): Gateway
    {
        return $this->setParameter('api_id', $value);
    }

    /**
     * Получить Api secret
     *
     * @return string
     */
    public function getApiSecret(): string
    {
        return $this->getParameter('api_secret');
    }

    /**
     * Установить Api secret
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiSecret(string $value): Gateway
    {
        return $this->setParameter('api_secret', $value);
    }

    /**
     * Получить Номер счета (USD)
     *
     * @return string
     */
    public function getAccountUsd(): string
    {
        return $this->getParameter('account_usd');
    }

    /**
     * Установить Номер счета (USD)
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountUsd(string $value): Gateway
    {
        return $this->setParameter('account_usd', $value);
    }

    /**
     * Получить Номер счета (UAH)
     *
     * @return string
     */
    public function getAccountUah(): string
    {
        return $this->getParameter('account_uah');
    }

    /**
     * Установить Номер счета (UAH)
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountUah(string $value): Gateway
    {
        return $this->setParameter('account_uah', $value);
    }

    /**
     * Получить Номер счета (UAH)
     *
     * @return string
     */
    public function getAccountRub(): string
    {
        return $this->getParameter('account_rub');
    }

    /**
     * Установить Номер счета (RUB)
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountRub(string $value): Gateway
    {
        return $this->setParameter('account_rub', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'api_id' => '',
            'api_secret' => '',
            'account_usd' => '',
            'account_uah' => '',
            'account_rub' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
