<?php

namespace App\Common\Packages\Payment\Gateways\Bitconce;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Bitconce';


    public function getTokenMerchant(): string
    {
        return $this->getParameter('token_merchant');
    }


    public function setTokenMerchant(string $value): Gateway
    {
        return $this->setParameter('token_merchant', $value);
    }


    public function getTokenPay(): string
    {
        return $this->getParameter('token_pay');
    }


    public function setTokenPay(string $value): Gateway
    {
        return $this->setParameter('token_pay', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'token_merchant' => '',
            'token_pay' => '',
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
