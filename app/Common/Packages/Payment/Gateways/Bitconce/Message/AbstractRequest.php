<?php

namespace App\Common\Packages\Payment\Gateways\Bitconce\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getTokenMerchant(): string
    {
        return $this->getParameter('token_merchant');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setTokenMerchant($value): AbstractRequest
    {
        return $this->setParameter('token_merchant', $value);
    }
}
