<?php

namespace App\Common\Packages\Payment\Gateways\Bitconce\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'token_merchant'
        );

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (float)iex_number_format( $amount + $amount * $commission / 100, 2);
        }

        $bank_name = match ($order_data->merchant->bank_name) {
            0 => 'default',
            1 => 'visa',
            2 => 'mastercard',
            3 => 'maestro',
            4 => 'mir',
            5 => 'sberbank',
            6 => 'otkritie',
            7 => 'tinkoff',
            8 => 'raiffeisenbank',
            9 => 'alfa',
            10 => 'vtb',
            11 => 'mkb',
            12 => 'sovcombank',
            13 => 'kaspi',
            14 => 'jusan',
            15 => 'eurasian'
        };

        $options =  [
            'fiat_amount'   => (float)$total_amount,
            'custom_id'     => (string)$this->getTransactionId(),
            'client_email'  => $order_data->email,
        ];
        if($bank_name != 'default') {
            $options['bank_name'] = $bank_name;
        }

        return $options;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $httpResponse = Http::baseUrl('https://bitconce.top')->withToken($this->getTokenMerchant());

        $result = $httpResponse->asForm()->post('/api/createOrder/', $data)->json();
        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'bitconce',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => 'https://bitconce.top/api/createOrder',
            'headers' => isset($httpResponse->getOptions()['headers']) ? json_encode($httpResponse->getOptions()['headers']) : null,
            'content' => json_encode($data),
            'response' => json_encode($result)
        ]);

        if($result['status'] != 'success') {
            throw new \Exception(json_encode($result));
        }

        return $this->response = new PurchaseResponse($this, $result);
    }
}
