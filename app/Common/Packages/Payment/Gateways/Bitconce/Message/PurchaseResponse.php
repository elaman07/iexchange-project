<?php

namespace App\Common\Packages\Payment\Gateways\Bitconce\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;


class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return isset($this->data['status']);
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['data'])) {
            return $this->data['data']['requisities'];
        }
    }


    /**
     * Получаем валюту
     *
     * @return string
     */
    public function getCurrency(): ?string
    {
        return null;
    }

    /**
     * Получаем описание
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->data['data']['id'];
    }
}
