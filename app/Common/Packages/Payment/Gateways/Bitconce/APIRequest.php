<?php

namespace App\Common\Packages\Payment\Gateways\Bitconce;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return float
     * @throws Exception
     */
    public function getBalance($currency): float
    {
        $response = $this->call('get', '/api/getAccountInfo/',[], 'merchant');
        return remove_all_spaces($response['balance_fiat']);
    }


    public function findTransaction(int $id)
    {
        return $this->call('get', '/api/getOrderById/', [
            'order_id' => (int)$id
        ], 'merchant');
    }

    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $direction
     * @param int $order_id
     * @return mixed
     * @throws Exception
     */
    public function transfer($amount, string $to, string $direction, int $order_id)
    {
        return $this->call('post', '/api/createExOrder/', [
            'custom_id'  => (string)$order_id,
            'rub_amount' => (int)$amount,
            'requisites' => (string)$to,
            'live_time' => 35,
            'direction' => (string)$direction
        ]);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $method
     * @param string $path
     * @param array $options
     * @param string $type
     * @return mixed
     * @throws Exception
     */
    private function call(string $method, string $path, array $options = [], string $type = 'pay')
    {
        $token = $this->parameters['token_pay'];
        if($type == 'merchant') {
            $token = $this->parameters['token_merchant'];
        }

        $response = Http::withToken($token)
            ->baseUrl('https://bitconce.top')
            ->asForm()->{\Str::lower($method)}($path, $options)->json();

        $order_id = 0;
        if(isset($options['custom_id'])) {
            $order_id = $options['custom_id'];
        }
        if(isset($options['order_id'])) {
            $order_id = $options['order_id'];
        }


        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'Bitconce',
            'id_order' => $order_id,
            'url' => 'https://bitconce.top'.$path,
            'content' => json_encode($options),
            'response' => json_encode($response)
        ]);

        if($response['status'] == 'success') {
            return $response['data'] ?? '';
        }

        throw new \Exception('Bitconce Message: '.json_encode($response));
    }
}
