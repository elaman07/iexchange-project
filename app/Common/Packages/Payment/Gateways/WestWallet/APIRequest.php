<?php

namespace App\Common\Packages\Payment\Gateways\WestWallet;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected $parameters = [];


    /**
     * URL Paykassa API
     *
     * @var string
     */
    const API_URL = 'https://api.westwallet.info';

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function getBalance(string $currency)
    {
        $response =  $this->request('GET', '/wallet/balance', [
            'currency' => $currency
        ]);

        // Получение баланса
        if(isset($response['balance']))
            return $response['balance'];

        throw new \Exception('API Не настроено');
    }

    public function getAllBalances()
    {
        $response =  $this->request('GET', '/wallet/balances');

        if($response['error'] == 'ok') {
            return $response;
        }
        return [];
    }

    /**
     * Отправка средств
     *
     * @param array $options
     * @return array|bool
     * @throws \Exception
     */
    public function transfer(array $options = [])
    {
        $params = [
            'currency'      => Str::upper($options['currency']),
            'amount'        => (string)$options['amount'],
            'address'       => (string)$options['address'],
            'description'   => (string)$options['description'],
        ];

        if(isset($options['dest_tag'])) {
            $params['dest_tag'] = (string)$options['dest_tag'];
        }

        return $this->request('POST', '/wallet/create_withdrawal', $params);
    }



    /**
     * Детали транзакции
     *
     * @param int $id
     * @return array|bool
     * @throws \Exception
     */
    public function findTransaction(int $id)
    {
        return $this->request('POST', '/wallet/transaction', [
            'id' => (int)$id
        ]);
    }


    /**
     * Детали транзакций
     *
     * @param string $currency
     * @param int $id
     * @return array|bool
     * @throws Exception
     */
    public function findTransactions(string $currency, int $id)
    {
        $response = $this->request('POST', '/wallet/transactions', [
            'currency' => (string)$currency,
            'type' => 'receive',
            'order' => 'desc'
        ]);
        if(isset($response['error']) and $response['error'] == 'ok')
        {
            if(isset($response['result']) and  count($response['result']) > 0) {
                return collect($response['result'])->reject(function($item) {
                    return !isset($item['label']);
                })->filter(function($item) use($id) {
                    return (int)$item['label'] == $id;
                })->first();
            }

            return [];
        }

        return [];
    }

    /**
     * Запросы WestWallet
     *
     * @param string $method
     * @param $url
     * @param array $params
     * @return array|bool
     * @throws \Exception
     */
    public function request(string $method, $url, array $params = [])
    {
        $timestamp = time();
        $body = (!empty($params) ? json_encode($params, JSON_UNESCAPED_SLASHES) : null);
        $signature = hash_hmac("sha256", $timestamp.$body, $this->parameters['private_key']);

        $headers = [
            'X-API-KEY' =>  $this->parameters['public_key'],
            'X-ACCESS-SIGN' => $signature,
            'X-ACCESS-TIMESTAMP' => $timestamp
        ];

        $request = Http::baseUrl(self::API_URL)->withHeaders($headers)->{Str::lower($method)}($url, $params);
        $httpResponse =  $request->json();

        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'WestWallet',
            'id_order' => $options['order_id'] ?? 0,
            'url' => self::API_URL.$url,
            'headers' => json_encode($headers),
            'content' => json_encode($params),
            'response' => json_encode($httpResponse)
        ]);

        if(isset($httpResponse['error']) and $httpResponse['error'] != 'ok')
            throw new \Exception($httpResponse['error']);

        return $httpResponse;
    }
}
