<?php


namespace App\Common\Packages\Payment\Gateways\WestWallet\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['status'] == 'completed';
    }

    /**
     * Если транзакция в ожидании
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        return $this->data['status'] == 'pending';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return !in_array($this->data['status'], ['sending', 'network_error']);
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['label'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['currency'];
    }

    /**
     * Счетчик подтвержедний
     *
     * @return string|integer
     */
    public function getBlockchainConfirm()
    {
        return $this->data['blockchain_confirmations'];
    }

    /**
     * Счетчик подтвержедний
     *
     * @return string|integer
     */
    public function getBlockchainHash()
    {
        return $this->data['blockchain_hash'];
    }
}
