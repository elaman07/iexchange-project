<?php


namespace App\Common\Packages\Payment\Gateways\WestWallet\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        $currency = $this->getCoinCurrency();
        if(!empty($network_code)) {
            $currency = $network_code;
        }

        return [
            'currency'  =>  $currency,
            'label'     =>  (string)$this->getTransactionId(),
            'ipn_url'   =>  $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $timestamp = time();
        $body = (!empty($data) ? json_encode($data, JSON_UNESCAPED_SLASHES) : null);
        $signature = hash_hmac("sha256", (string)$timestamp.$body, $this->getPrivateKey());


        $headers = [
            'X-API-KEY' =>  $this->getPublicKey(),
            'X-ACCESS-SIGN' => $signature,
            'X-ACCESS-TIMESTAMP' => $timestamp
        ];
        $headers['Content-Type'] = 'application/json';
        $http_response = \Http::baseUrl('https://api.westwallet.info')->asJson()->withHeaders($headers)->post('/address/generate', $data)->body();

        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'westwallet',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => 'https://api.westwallet.info/address/generate',
            'headers' => json_encode($headers),
            'content' => json_encode($data),
            'response' => $http_response
        ]);

        return $this->response = new AddressResponse($this, json_decode($http_response, true));
    }
}
