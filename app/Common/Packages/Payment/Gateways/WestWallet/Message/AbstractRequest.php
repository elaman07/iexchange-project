<?php

namespace App\Common\Packages\Payment\Gateways\WestWallet\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPublicKey(string $value): AbstractRequest
    {
        return $this->setParameter('public_key', $value);
    }

    /**
     * Получить Приватный ключ
     *
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установить Приватный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPrivateKey(string $value): AbstractRequest
    {
        return $this->setParameter('private_key', $value);
    }

}
