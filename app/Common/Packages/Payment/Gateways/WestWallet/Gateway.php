<?php

namespace App\Common\Packages\Payment\Gateways\WestWallet;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\WestWallet\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'WestWallet';

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setPublicKey(string $value): Gateway
    {
        return $this->setParameter('public_key', $value);
    }

    /**
     * Получить Приватный ключ
     *
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установить Приватный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setPrivateKey(string $value): Gateway
    {
        return $this->setParameter('private_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'public_key' => '',
            'private_key' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
