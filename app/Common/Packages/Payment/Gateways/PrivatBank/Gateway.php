<?php

namespace App\Common\Packages\Payment\Gateways\PrivatBank;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\Ripple\Message\AddressRequest;


class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'PrivatBank';

    /**
     * Получить
     *
     * @return string
     */
    public function getMerchantId() : string
    {
        return $this->getParameter('merchant_id');
    }

    /**
     * Установка
     *
     * @param string $value
     * @return Gateway
     */
    public function setMerchantId(string $value): Gateway
    {
        return $this->setParameter('merchant_id', $value);
    }

    /**
     * Получить
     *
     * @return string
     */
    public function getPassword() : string
    {
        return $this->getParameter('password');
    }

    /**
     * Установка
     *
     * @param string $value
     * @return Gateway
     */
    public function setPassword(string $value): Gateway
    {
        return $this->setParameter('password', $value);
    }

    /**
     * Получить
     *
     * @return string
     */
    public function getCard() : string
    {
        return $this->getParameter('card');
    }

    /**
     * Установка
     *
     * @param string $value
     * @return Gateway
     */
    public function setCard(string $value): Gateway
    {
        return $this->setParameter('card', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'merchant_id' => '',
            'password' => ''
        ];
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
