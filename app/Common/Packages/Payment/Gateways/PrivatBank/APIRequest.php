<?php

namespace App\Common\Packages\Payment\Gateways\PrivatBank;


use Exception;
use IEXBase\RippleAPI\Ripple;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use GuzzleHttp\Client;

class APIRequest
{
    /**
     *
     * @var string
     */
    const API_URL = 'https://api.privatbank.ua/';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Экземпляр GuzzleHttp
     *
     * @var Client
     */
    protected $client;

    /**
     * Proxy
     */
    protected $proxy = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;

        $this->client = new Client([
            'base_uri' => self::API_URL,
            'defaults' => [
                'headers'  => ['content-type'  => 'text/xml; charset=UTF8']
            ],
        ]);
    }

    /**
     * Установка proxy
     *
     * @param $options
     * @return APIRequest
     */
    public function setProxy($options): APIRequest
    {
        if(empty($options) or is_null($options))
            return $this;

        $url = [];
        if(!is_null($options['auth']))
            $url[0] = $options['auth'];

        if(!is_null($options['address']))
            $url[1] = $options['address'];

        $this->proxy['proxy'] = 'http://'.implode('@', $url);

        if(!empty($options['user_agent'])) {
            $this->proxy['headers']['User-Agent'] = $options['user_agent'];
        }

        return $this;
    }

    /**
     * Получение баланса
     *
     * @throws Exception
     */
    public function getBalance()
    {
        $data = '<oper>cmt</oper><wait>0</wait><test>0</test><payment id="1"><prop name="cardnum" value="'. $this->parameters['card'] .'" /><prop name="country" value="UA" /></payment>';
        $res = $this->call('balance', $data);

        if(isset($res->data->info->cardbalance->av_balance)) {
            return $res->data->info->cardbalance->av_balance;
        }

        return 0;
    }

    /**
     * Отправить на карту приват банка
     *
     * @param $order_id
     * @param $to_card
     * @param $amount
     * @param $description
     * @param string $currency
     * @return int
     * @throws Exception
     */
    public function transfer($order_id, $to_card, $amount, $description, $currency = 'UAH')
    {
        $data = '<oper>cmt</oper><wait>0</wait><test>'. $this->parameters['is_test'] .'</test><payment id="'. $order_id .'"><prop name="b_card_or_acc" value="'. $to_card .'" /><prop name="amt" value="'. $amount .'" /><prop name="ccy" value="'. $currency .'" /><prop name="details" value="'. $description .'" /></payment>';

        $res = $this->call('pay_pb', $data);

        if(isset($res->data->payment->attributes()->ref[0]) and isset($res->data->payment->attributes()->state[0]))
        {
            $state = (string)$res->data->payment->attributes()->state[0];
            $ref = (string)$res->data->payment->attributes()->ref[0];

            if($state == 1) {
                return $ref;
            }
            throw new Exception($res->data->payment->attributes()->message);
        }

        throw new Exception('Средства не отправлены');
    }

    public function call($path, $params = [])
    {
        $sign=sha1(md5($params.$this->parameters['password']));

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
		<request version="1.0">
		  <merchant>
			<id>'. $this->parameters['merchant_id'] .'</id>
			<signature>'. $sign .'</signature>
		  </merchant>
		  <data>'. $params .'</data>
		</request>';

        $request = $this->client->post('/p24api/'.$path, [
            'body' => $xml
        ]);

        $res = @simplexml_load_string($request->getBody()->getContents());
        if(!is_object($res)) {
            throw new Exception(empty($res) ? 'Данные не получены' : $res);
        }

        return $res;
    }

    private function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;

        return $out;
    }
}
