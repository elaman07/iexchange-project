<?php

namespace App\Common\Packages\Payment\Gateways\KunaCodes;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     *
     * @var string
     */
    protected $api = 'https://api.kuna.io';

    /**
     * Конфигурация
     */
    protected $parameters = [];

    /**
     * Создаем констркутор Exmo
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return int
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->call('POST', '/v3/auth/r/wallets', []);
        $result = collect($response)->first(function($item) use($currency){
            return mb_strtoupper($item[1]) == mb_strtoupper($currency);
        });

        return (!empty($result) ? $result[4] : 0);
    }

    /**
     * Создаем код
     *
     * @param $currency
     * @param $amount
     * @param null $comment
     * @return mixed
     * @throws Exception
     */
    public function createCode($currency, $amount, $comment = null)
    {
        $response = $this->call('POST', '/v3/auth/kuna_codes', array_filter([
            'amount' => (float)$amount,
            'currency' => mb_strtolower($currency),
            'comment' => $comment
        ]));

        if(isset($response['status']) and $response['status'] == 'processing') {
            return $response;
        }

        throw new \Exception('Средства не отправлены');
    }

    /**
     * Активация кода
     *
     * @param string $code
     * @return mixed
     * @throws Exception
     */
    public function activateCode(string $code)
    {
        $explode = explode('-', $code);
        $check = $this->call('GET', '/v3/kuna_codes/'.$explode[0].'/check', []);

        // Если все нормально с кодом
        if(isset($check['status']) and $check['status'] == 'active') {

            $response = $this->call('PUT', '/v3/auth/kuna_codes/redeem', [
                'code' => $code
            ]);
            if($response['status'] != 'redeeming') {
                throw new \Exception('Код не активирован');
            }

            return $response;
        }

        throw new \Exception('[Ошибка] - Код не активирован');
    }

    /**
     * Вызов определенных действий для "Exmo"
     *
     * @param $method
     * @param $path
     * @param array $options
     * @return mixed
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call($method, $path, array $options = [])
    {
        $nonce = (int)(microtime(true) * 1000);
        $json_body = json_encode($options);


        $signatureString = "{$path}{$nonce}{$json_body}";
        $sign = hash_hmac('sha384', $signatureString, $this->parameters['secret_key']);
        $http = new Client([
            'base_uri' => $this->api,
            'headers' => [
                'accept' => 'application/json',
                'content-type' => 'application/json',
                'kun-nonce' => $nonce,
                'kun-apikey' => $this->parameters['public_key'],
                'kun-signature' => $sign
            ]
        ]);
        $response = $http->request(mb_strtoupper($method), $path, ['body' => $json_body]);
        return json_decode($response->getBody()->getContents(), true);
    }
}
