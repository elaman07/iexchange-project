<?php

namespace App\Common\Packages\Payment\Gateways\Rapira;


use DateTimeImmutable;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string|null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        return 0;
    }

    public function findTransaction(string $currency)
    {
        $request = $this->request('post', '/open/deposit/records', [
            'pageNo' => 0,
            'pageSize' => 100,
            'unit' => \Str::lower($currency)
        ]);

        $trans = [];
        if(is_array($request) and !isset($request['error']) and $request['code'] == 0) {
            $trans = $request['data']['content'];
        }

        return $trans;
    }


    /**
     * Запросы
     * @param $method
     * @param $path
     * @param $options
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function request($method, $path, $options = [])
    {
        $key = InMemory::plainText(base64_decode($this->parameters['private_key'], true));
        $config = Configuration::forSymmetricSigner(new Sha256(), $key);
        $now   = new DateTimeImmutable();

        $builder = $config->builder()
            ->identifiedBy(bin2hex(random_bytes(12)))
            ->expiresAt($now->modify('+1 month'))
            ->getToken($config->signer(), $config->signingKey());


        $token_data = Http::asJson()
            ->post('https://api.rapira.net/open/generate_jwt', [
                'kid' => $this->parameters['uuid'],
                'jwt_token' => strval($builder->toString())
            ])->json();

        if(!isset($token_data['token'])) {
            throw new \Exception('Токен Rapira не создан');
        }

        $token_id = $token_data['token'];


        if(\Str::lower($method) == 'post') {
            return  Http::baseUrl('https://api.rapira.net')
                ->withToken($token_id)->asForm()->{$method}($path, $options)->json();
        }
        return  Http::baseUrl('https://api.rapira.net')
            ->withToken($token_id)->asJson()->{$method}($path, $options)->json();
    }
}
