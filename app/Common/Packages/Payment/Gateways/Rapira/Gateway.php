<?php

namespace App\Common\Packages\Payment\Gateways\Rapira;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\Rapira\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Rapira';

    /**
     * Получить private_key
     *
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установить private_key
     *
     * @param string $value
     * @return Gateway
     */
    public function setPrivateKey(string $value): Gateway
    {
        return $this->setParameter('private_key', $value);
    }

    /**
     * Получить uuid
     *
     * @return string
     */
    public function getUuid(): string
    {
        return $this->getParameter('uuid');
    }

    /**
     * Установить uuid
     *
     * @param string $value
     * @return Gateway
     */
    public function setUuid(string $value): Gateway
    {
        return $this->setParameter('uuid', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'private_key' => '',
            'uuid' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
