<?php


namespace App\Common\Packages\Payment\Gateways\Rapira\Message;

use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Lcobucci\JWT\{
    Configuration,
    Signer\Rsa\Sha256,
    Signer\Key\InMemory
};
use DateTimeImmutable;
use Illuminate\Support\{
    Facades\Http, Str
};



class AddressRequest extends AbstractRequest
{
    /**
     * Ссылка на API
     *
     * @var string
    */
    protected string $endpoint_url = 'https://api.rapira.net';


    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [
            'currency'  =>  $this->getCoinCurrency()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $key = InMemory::plainText(base64_decode($this->getPrivateKey(), true));
        $config = Configuration::forSymmetricSigner(new Sha256(), $key);
        $now   = new DateTimeImmutable();


        $builder = $config->builder()
            ->identifiedBy(bin2hex(random_bytes(12)))
            ->expiresAt($now->modify('+1 month'))
            ->getToken($config->signer(), $config->signingKey());


        $token_data = Http::asJson()
            ->post('https://api.rapira.net/open/generate_jwt', [
                'kid' => $this->getUuid(),
                'jwt_token' => strval($builder->toString())
            ])->json();

        if(!isset($token_data['token'])) {
            throw new \Exception('Токен Rapira не создан');
        }

        $token_id = $token_data['token'];


        $http_request = Http::baseUrl($this->endpoint_url)
            ->withToken($token_id)
            ->asJson();

        $options_currency = $data['currency'];
        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        if(!empty($network_code)) {
            $options_currency = $network_code;
        }

        $httpResponse = $http_request->asForm()->post('/open/deposit_address', [
            'currency' => trim(Str::lower($options_currency))
        ])->json();


        return $this->response = new AddressResponse($this, $httpResponse);
    }
}
