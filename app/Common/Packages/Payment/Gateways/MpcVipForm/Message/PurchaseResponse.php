<?php

namespace App\Common\Packages\Payment\Gateways\MpcVipForm\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;
use Illuminate\Support\Arr;


class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl(){
        if(isset($this->data['paymentURL'])) {
            return $this->data['paymentURL'];
        }
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    public function getRedirectData()
    {
        return $this->data;
    }

    public function getLabel()
    {
        return $this->data['externalID'];
    }
}

