<?php


namespace App\Common\Packages\Payment\Gateways\MpcVipForm\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use App\Models\MerchantTransactionId;
use App\Models\Task;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @return Task
    */
    protected $transaction;


    protected $billId;


    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        $merchant_id = MerchantTransactionId::where('id_task', $this->data['order_id'])->first();
        $this->billId = $merchant_id->transaction_id;

        $options = [
            'externalID' => $this->billId
        ];


        // Данные по транзакции
        $signature = hash_hmac('sha512', json_encode($options), $this->request->getApiSecret());
        $httpResponse = Http::withHeaders([
            'API-Key' => $this->request->getApiKey(),
            'Signature' => $signature
        ])->asJson()->baseUrl('https://masterprocessingvip.ru')
            ->post('/api/payment/get_invoice_order_info', $options)->json();

        $this->data = $httpResponse;
        $this->transaction = $merchant_id->tasks;
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['statusName'] == 'payed';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->data['statusName'] != 'payed';
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->billId;
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->transaction->id;
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['successPayed'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return 'RUB';
    }


    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return '';
    }
}
