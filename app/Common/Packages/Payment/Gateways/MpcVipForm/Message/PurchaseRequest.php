<?php

namespace App\Common\Packages\Payment\Gateways\MpcVipForm\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'api_key', 'api_secret', 'amount', 'currency', 'transactionId'
        );

        // (float)$this->getOrderData()->direction_exchange->currency1->merchant->fixed_fee

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (int)round($amount + $amount * $commission / 100, 0);
        }


        $from_shot = preg_replace('/\s+/', '', $this->getOrderData()->from_shot);


        return [
            'amount' => (int)$total_amount,
            'paySource' => 1,
            'successRedirectURL' => $this->getReturnUrl(),
            'failedRedirectURL' => $this->getCancelUrl(),
            'callbackURL' => $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $signature = hash_hmac('sha512', json_encode($data), $this->getApiSecret());
        $httpResponse = Http::withHeaders([
            'API-Key' => $this->getApiKey(),
            'Signature' => $signature
        ])->asJson()->baseUrl('https://masterprocessingvip.ru')
            ->post('/api/payment/create_p2p_form', $data)->json();


        if(!$httpResponse['success']) {
            throw new \Exception(json_encode($httpResponse));
        }

        return $this->response = new PurchaseResponse($this, $httpResponse);
    }
}
