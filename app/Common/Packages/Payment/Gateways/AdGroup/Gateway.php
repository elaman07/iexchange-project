<?php

namespace App\Common\Packages\Payment\Gateways\AdGroup;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'AdGroup';

    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->getParameter('client_id');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setClientId(string $value): Gateway
    {
        return $this->setParameter('client_id', $value);
    }

    /**
     * Получить Client Secret
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->getParameter('client_secret');
    }

    /**
     * Установить Client Secret
     *
     * @param string $value
     * @return Gateway
     */
    public function setClientSecret(string $value): Gateway
    {
        return $this->setParameter('client_secret', $value);
    }

    /**
     * Получить Пароль от аккаунта
     *
     * @return string
     */
    public function getPin(): string
    {
        return $this->getParameter('pin');
    }

    /**
     * Установить Пароль от аккаунта
     *
     * @param string $value
     * @return Gateway
     */
    public function setPin(string $value): Gateway
    {
        return $this->setParameter('pin', $value);
    }

    /**
     * Установить User ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setUserId(string $value): Gateway
    {
        return $this->setParameter('user_id', $value);
    }

    /**
     * Получить User ID
     *
     * @return string
     */
    public function getUserId(): string
    {
        return $this->getParameter('user_id');
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'client_id' => '',
            'client_secret' => '',
            'pin' => '',
            'user_id' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
