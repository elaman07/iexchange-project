<?php


namespace App\Common\Packages\Payment\Gateways\AdGroup\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Arr;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;


    protected $transaction;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        if(isset($this->data['result']) and !$this->data['result']['status']) {
            throw new InvalidResponseException('Возникли непредвиденные ошибки');
        }

        $this->transaction = Arr::first($this->data['responseData']['transactions']);

        // Проверяем верификацию
        if(isset($this->data['responseData']) and $this->data['responseData']['total'] == 0 and isset($this->data['responseData']['verify_url']) and $this->data['responseData']['verify_url']) {
            exit;
        }

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->transaction['tx_status'] == 'APPROVED';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->transaction['tx_status'] != 'APPROVED';
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->transaction['_id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->transaction['extra_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->transaction['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return str_replace('RUR', 'RUB', $this->transaction['currency']);
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['responseData']['signature'];
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return $this->transaction['source_address'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        return hash_hmac('sha256', implode('|', [
            (string)$this->transaction['_id'],
            (string)$this->transaction['ref_id'],
            (string)$this->transaction['currency'],
            (string)$this->transaction['amount'],
        ]), $this->request->getPassword());
    }
}
