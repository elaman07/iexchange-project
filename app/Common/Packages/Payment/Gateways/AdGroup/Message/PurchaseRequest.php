<?php

namespace App\Common\Packages\Payment\Gateways\AdGroup\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'client_id', 'client_secret', 'amount', 'currency', 'transactionId'
        );

        // (float)$this->getOrderData()->direction_exchange->currency1->merchant->fixed_fee

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (float)iex_number_format( $amount + $amount * $commission / 100, 2);
        }

        switch ($order_data->merchant->method_pay)
        {
            case '1':
                $method = 'card';
                $platform = 'QIWI';
                break;
            case '2':
                $method = 'qw';
                $platform = 'YANDEX';
                break;
            default:
                $method = 'qw';
                $platform = 'QIWI';
        }


        $from_shot = preg_replace('/\s+/', '', $this->getOrderData()->from_shot);

        $array = [
            'amount' => $total_amount,
            'platform' => $platform,
            'currency' => mb_strtoupper($this->getCurrency()),
            'ip' => Request::getClientIp(),
            'recipient' => preg_replace('/\s+/', '', $this->getOrderData()->to_shot),
            'c_from' => $order_data->direction_exchange->currency1->designation_xml,
            'c_to' => $order_data->direction_exchange->currency2->designation_xml,
            'email' => $order_data->user->email,
            'client' => $from_shot,
            'txn' => (string)$this->getTransactionId(),
            'date' => explode('+', date(DATE_ATOM))[0],
            'user_agent' => Request::userAgent(),
            'cid' => (string)$order_data->user->id,
            'extra' => [
                ['_id' => $this->getTransactionId()]
            ],
            'paySource' => $method
        ];

        if($method == 'card') {
            $array['address'] = (string)mb_substr($from_shot, -4);
        }


        return $array;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {

        $httpResponse = Http::withBasicAuth($this->getClientId(), $this->getClientSecret())->baseUrl('https://api.payassist.io')
            ->asJson()
            ->post('/p2p/bill-payment/invoice/create', ['reqData' => $data, 'header' => ['txName' => 'CreateBillInvoice']])->json();


        if(!$httpResponse['result']['status']) {
            throw new \Exception(json_encode($httpResponse));
        }

        if(isset($httpResponse['errors'])) {
            throw new \Exception(json_encode($httpResponse['errors']));
        }

        return $this->response = new PurchaseResponse($this, $httpResponse['responseData']);
    }
}
