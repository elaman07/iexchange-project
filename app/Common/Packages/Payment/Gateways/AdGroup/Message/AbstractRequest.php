<?php

namespace App\Common\Packages\Payment\Gateways\AdGroup\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->getParameter('client_id');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setClientId(string $value): AbstractRequest
    {
        return $this->setParameter('client_id', $value);
    }

    /**
     * Получить Client Secret
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->getParameter('client_secret');
    }

    /**
     * Установить Client Secret
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setClientSecret(string $value): AbstractRequest
    {
        return $this->setParameter('client_secret', $value);
    }

    /**
     * Получить Пароль от аккаунта
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->getParameter('password');
    }

    /**
     * Установить Пароль от аккаунта
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPassword(string $value): AbstractRequest
    {
        return $this->setParameter('password', $value);
    }
}
