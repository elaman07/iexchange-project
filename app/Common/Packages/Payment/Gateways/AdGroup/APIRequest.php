<?php

namespace App\Common\Packages\Payment\Gateways\AdGroup;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance($currency)
    {
        $response = $this->call('/accounts/fetch-balance', 'accountsBalance', [
            'provider' => [Str::upper($this->methodPay)],
            'currency' => [Str::upper($currency)]
        ]);

        return Arr::first($response['accounts'])['sum'];
    }

    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function sendToAccount($amount, string $to, string $currency)
    {
        return $this->call('/transfer/send-wallet-external', 'QiwiPayout', [
            'amount' => $amount,
            'address' => $to,
            'platform' => 'QIWI',
            'currency' => Str::upper($currency)
        ]);
    }

    /**
     * Отправить на карту
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function sendToCard($amount, string $to, string $currency)
    {
        return $this->call('/transfer/send-card-external', 'P2Card', [
            'amount'    => $amount,
            'address'   => $to,
            'platform' => 'QIWI',
            'currency'  => mb_strtoupper($currency)
        ]);
    }

    /**
     * Отправить на Yandex
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function sendToYandex($amount, string $to, string $currency)
    {
        return $this->call('/transfer/send-yandex-external', 'QiwiPayout', [
            'amount'    => $amount,
            'address'   => $to,
            'platform' => 'QIWI',
            'currency'  => mb_strtoupper($currency)
        ]);
    }

    /**
     * Отправить на WebMoney
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function sendToWebMoney($amount, string $to, string $currency)
    {
        return $this->call('/transfer/send-webmoney-external', 'QiwiPayout', [
            'amount'    => $amount,
            'address'   => $to,
            'platform' => 'QIWI',
            'currency'  => mb_strtoupper($currency)
        ]);
    }

    /**
     * Отправить на Номер телефона
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @param string $country_code
     * @return mixed
     * @throws Exception
     */
    public function sendToMobile($amount, string $to, string $currency, string $country_code = "7")
    {
        return $this->call('/transfer/send-mobile-external', 'QiwiPayout', [
            'amount'    => $amount,
            'address'   => $to,
            'platform'  => 'QIWI',
            'currency'  => mb_strtoupper($currency),
            'country_code' => (string)$country_code
        ]);
    }

    /**
     * Список доступных провайдеров
     *
     * @param int $method_pay
     * @return APIRequest
     */
    public function setMethodPay(int $method_pay): APIRequest
    {
        // Тип транзакции
        switch ($method_pay) {
            case 2:
                $type = 'YANDEX';
                break;
            // Стандартная выплата
            default:
                $type = 'QIWI';
        }

        $this->methodPay = $type;
        return $this;
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $path
     * @param string $name
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    private function call(string $path, string $name, array $options = [])
    {
        $response = Http::withBasicAuth($this->parameters['client_id'], $this->parameters['client_secret'])
            ->baseUrl('https://api.payassist.io')
            ->asJson()
            ->post('/p2p'.$path, ['reqData' => $options, 'header' => ['txName' => $name]])->json();


        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'ADGroup',
            'url' => 'https://api.payassist.io/p2p'.$path,
            'headers' => json_encode(['txName' => $name]),
            'content' => json_encode($options),
            'response' => json_encode($response)
        ]);


        if(empty($response)) {
            throw new \Exception('ADGroup: API не настроен');
        }

        // Если возникли ошибки
        if($response['result']['status'] != 1) {
            throw new Exception(
                json_encode($response['errors'])
            );
        }

        return $response['responseData'];
    }
}
