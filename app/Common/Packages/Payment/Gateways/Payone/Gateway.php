<?php

namespace App\Common\Packages\Payment\Gateways\Payone;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Payone';

    /**
     * Получить Api Key
     *
     * @return string
     */
    public function getApiHost(): string
    {
        return $this->getParameter('api_host');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiHost(string $value): Gateway
    {
        return $this->setParameter('api_host', $value);
    }

    /**
     * Получить Api Key
     *
     * @return string
     */
    public function getApiIp(): string
    {
        return $this->getParameter('api_ip');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiIp(string $value): Gateway
    {
        return $this->setParameter('api_ip', $value);
    }

    /**
     * Получить Api Key
     *
     * @return string
     */
    public function getApiPort(): string
    {
        return $this->getParameter('api_port');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiPort(string $value): Gateway
    {
        return $this->setParameter('api_port', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'api_host' => '',
            'api_ip' => '',
            'api_port' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
