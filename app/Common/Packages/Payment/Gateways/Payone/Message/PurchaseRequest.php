<?php

namespace App\Common\Packages\Payment\Gateways\Payone\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'api_host', 'amount', 'currency', 'transactionId'
        );

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (int)round($amount + $amount * $commission / 100, 0);
        }



        return [
            'method' => "client/order/deposit/create",
            'params' => [
                'client_tx_id' => (string)$this->getTransactionId(),
                'currency_uuid' => (string)$order_data->merchant->method_pay,
                'sum' => (float)$total_amount,
            ]
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $order_data = $this->getOrderData();

        $_caFile = storage_path('/ssls/payone/server_public.pem');
        $_certFile = storage_path('/ssls/payone/client_public.pem');
        $_keyFile = storage_path('/ssls/payone/client_private.pem');

        $_apiUri = '/rpc';
        $url = 'https://'.$this->getApiHost().":".$this->getApiPort().$_apiUri;
        $data = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RESOLVE, [$this->getApiHost().":".$this->getApiPort().":".$this->getApiIp()]);

        curl_setopt($ch, CURLOPT_SSLKEY, $_keyFile);
        curl_setopt($ch, CURLOPT_CAINFO, $_caFile);
        curl_setopt($ch, CURLOPT_SSLCERT, $_certFile);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($ch);

        $httResponse = json_decode($result, true);

        if(isset($httResponse['error'])) {
            throw new \Exception(json_encode($httResponse['error']));
        }

        return $this->response = new PurchaseResponse($this, $httResponse);
    }
}
