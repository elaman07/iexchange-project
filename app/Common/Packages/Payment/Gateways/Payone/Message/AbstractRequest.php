<?php

namespace App\Common\Packages\Payment\Gateways\Payone\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Api Key
     *
     * @return string
     */
    public function getApiHost(): string
    {
        return $this->getParameter('api_host');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiHost(string $value): AbstractRequest
    {
        return $this->setParameter('api_host', $value);
    }

    /**
     * Получить Api Key
     *
     * @return string
     */
    public function getApiIp(): string
    {
        return $this->getParameter('api_ip');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiIp(string $value): AbstractRequest
    {
        return $this->setParameter('api_ip', $value);
    }

    /**
     * Получить Api Key
     *
     * @return string
     */
    public function getApiPort(): string
    {
        return $this->getParameter('api_port');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiPort(string $value): AbstractRequest
    {
        return $this->setParameter('api_port', $value);
    }
}
