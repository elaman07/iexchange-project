<?php

namespace App\Common\Packages\Payment\Gateways\Payone\Message;


use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RedirectResponseInterface,
    RequestInterface
};


class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['result']) and isset($this->data['result']['uuid'])) {
            return sprintf('https://p1.payone.tech/%s', $this->data['result']['uuid']);
        }
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    public function getRedirectData()
    {
        return $this->data;
    }

    public function getLabel()
    {
        return $this->data['result']['uuid'];
    }
}


