<?php

namespace App\Common\Packages\Payment\Gateways\Payone;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance($currency = null)
    {
        return 1000000;
    }

    /**
     * Баланс кошелька
     *
     * @param null $order_id
     * @return array
     * @throws Exception
     */
    public function findTransaction($order_id = null): array
    {
        $dataPost = [
            'method' => "client/order/status",
            'params' => [
                'uuid' => (string)$order_id
            ]
        ];

        return $this->call($dataPost);
    }


    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @param $order_id
     * @return mixed
     * @throws Exception
     */
    public function transfer($options = []) {
        return $this->call($options);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $path
     * @param array $options
     * @return array
     * @throws Exception
     */
    private function call(array $options = [])
    {

        $_caFile = storage_path('/ssls/payone/server_public.pem');
        $_certFile = storage_path('/ssls/payone/client_public.pem');
        $_keyFile = storage_path('/ssls/payone/client_private.pem');

        $_apiUri = '/rpc';
        $url = 'https://'.$this->parameters['api_host'].":".$this->parameters['api_port'].$_apiUri;
        $data = json_encode($options);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RESOLVE, [$this->parameters['api_host'].":".$this->parameters['api_port'].":".$this->parameters['api_ip']]);

        curl_setopt($ch, CURLOPT_SSLKEY, $_keyFile);
        curl_setopt($ch, CURLOPT_CAINFO, $_caFile);
        curl_setopt($ch, CURLOPT_SSLCERT, $_certFile);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($ch);

        $httResponse =  json_decode($result, true);

        if(isset($httResponse['error'])) {
            throw new \Exception(json_encode($httResponse['error']));
        }

        return $httResponse;
    }
}
