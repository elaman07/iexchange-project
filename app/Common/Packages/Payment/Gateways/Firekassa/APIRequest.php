<?php

namespace App\Common\Packages\Payment\Gateways\Firekassa;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return float
     * @throws Exception
     */
    public function getBalance($currency): float
    {
        $response = $this->call('get', '/api/v2/balance');
        return $response['balance'];
    }

    public function findTransaction(int $id) {
        $response =  $this->call('get', '/api/v2/transactions/'.$id);
        return $response;
    }

    /**
     * Отправить на счет
     *
     * @param $method
     * @param $amount
     * @param string $to
     * @param string $comment
     * @param int $order_id
     * @param Task $task
     * @throws Exception
     */
    public function transfer($method, $amount, string $to, string $comment, int $order_id, Task $task, $site_account = 0)
    {
        $from_shot = preg_replace('/\s+/', '', $task->from_shot);
        $options = [
            'account' => (string)$to,
            'amount' => (string)$amount,
            'order_id' => (string)$order_id,
            'comment' => (string)$comment,
            'ext_txn' => (string)'OUT_'.$task->id,
            'ext_date' => Carbon::now()->format('c'),
            'ext_client' => $task->user->email,
            'ext_email' => $task->user->email,
            'ext_ip' => $task->ip,
            'ext_user_agent' => $task->user->user_agent,
            'ext_sender_system' => currency_in($task, true),
            'ext_sender' => !empty($from_shot) ? $from_shot : 'null',
            'ext_c_from' => $task->direction_exchange->currency1->designation_xml,
            'ext_partner' => $this->parameters['site_name']
        ];

        if($site_account == 1) {
            $options['method'] = 'card';
            $options['site_account'] = 'sber';
        }elseif($site_account == 2) {
            $options['method'] = 'card';
            $options['site_account'] = 'tinkoff';
        }elseif($site_account == 3) {
            $options['method'] = 'card';
            $options['site_account'] = 'vmm';
        }elseif($site_account == 4) {
            $options['method'] = 'card';
            $options['site_account'] = 'privat';
        }elseif($site_account == 5) {
            $options['method'] = 'card';
            $options['site_account'] = 'mono';
        }elseif($site_account == 6) {
            $options['method'] = 'card';
            $options['site_account'] = 'vmc';
        } else {
            $options['method'] = $method;
        }

        return $this->call('post', '/api/v2/withdrawal', $options);
    }

    public function sendCheck(int $id, string $email)
    {
        return $this->call('post', '/api/v2/transactions/'.$id.'/send-receipt', [
            'email' => \Str::lower($email),
            'format' => 'pdf'
        ]);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $method
     * @param string $path
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    private function call(string $method, string $path, array $options = [])
    {
        $response = Http::withToken($this->parameters['secret_key'])
            ->baseUrl($this->parameters['site_url'])
            ->asJson()->{\Str::lower($method)}($path, $options)->json();

        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'Firekassa',
            'id_order' => $options['order_id'] ?? 0,
            'url' => $this->parameters['site_url'].$path,
            'content' => json_encode($options),
            'response' => json_encode($response)
        ]);

        if(isset($response['message'])) {
            throw new \Exception('Firekassa Message: '.json_encode($response));
        }

        if(isset($response['error'])) {
            throw new \Exception('Firekassa Error: '.json_encode($response));
        }

        if(empty($response)) {
            throw new \Exception('Firekassa: API не настроен');
        }

        return $response;
    }
}
