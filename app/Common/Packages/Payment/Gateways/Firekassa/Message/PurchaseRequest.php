<?php

namespace App\Common\Packages\Payment\Gateways\Firekassa\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'secret_key'
        );

        // (float)$this->getOrderData()->direction_exchange->currency1->merchant->fixed_fee

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (float)iex_number_format( $amount + $amount * $commission / 100, 2);
        }

        $from_shot = preg_replace('/\s+/', '', $order_data->from_shot);


        $options = [];
        if($order_data->merchant->type_pay == 0) {
            $method = match ($order_data->merchant->method_pay) {
                1 => 'invoice-card',
                2 => 'yandex',
                default => 'invoice-qw'
            };

            $options = [
                'order_id' => (string)$this->getTransactionId(),
                'method' => $method,
                'amount' => (string)$total_amount,
                'account' => empty($from_shot) ? 'null' : $from_shot,
                'notification_url' => $this->getNotifyUrl(),
                'success_url' => $this->getReturnUrl(),
                'fail_url' => $this->getCancelUrl(),
                'ext_txn' => (string)'IN_'.$this->getTransactionId(),
                'ext_date' => Carbon::now()->format('c'),
                'ext_client' => $order_data->user->email,
                'ext_email' => $order_data->user->email,
                'ext_ip' => $order_data->ip,
                'ext_user_agent' => $order_data->user->user_agent,
                'ext_partner' => $this->getSiteName(),
                'ext_recipient_system' => currency_out($order_data, true),
                'ext_recipient' => preg_replace('/\s+/', '', $order_data->to_shot),
                'ext_c_to' => $order_data->direction_exchange->currency2->designation_xml,
            ];
        } else {
            // Выдача реквизитов
            $options =  [
                'order_id' => (string)$this->getTransactionId(),
                'amount' => (string)$total_amount,
                'account' => empty($from_shot) ? 'null' : $from_shot,
                'notification_url' => $this->getNotifyUrl(),
                'success_url' => $this->getReturnUrl(),
                'fail_url' => $this->getCancelUrl(),
                'ext_txn' => (string)'IN_'.$this->getTransactionId(),
                'ext_date' => Carbon::now()->format('c'),
                'ext_client' => $order_data->user->email,
                'ext_email' => $order_data->user->email,
                'ext_ip' => $order_data->ip,
                'ext_user_agent' => $order_data->user->user_agent,
                'ext_partner' => $this->getSiteName(),
                'ext_recipient_system' => currency_out($order_data, true),
                'ext_recipient' => preg_replace('/\s+/', '', $order_data->to_shot),
                'ext_c_to' => $order_data->direction_exchange->currency2->designation_xml,
            ];

            if($order_data->merchant->site_account == 1) {
                $options['method'] = 'card';
                $options['site_account'] = 'sber';
            }elseif($order_data->merchant->site_account == 2) {
                $options['method'] = 'card';
                $options['site_account'] = 'tinkoff';
            }elseif($order_data->merchant->site_account == 3) {
                $options['method'] = 'card';
                $options['site_account'] = 'vmm';
            }elseif($order_data->merchant->site_account == 4) {
                $options['method'] = 'card';
                $options['site_account'] = 'privat';
            }elseif($order_data->merchant->site_account == 5) {
                $options['method'] = 'card';
                $options['site_account'] = 'mono';
            }elseif($order_data->merchant->site_account == 6) {
                $options['method'] = 'card';
                $options['site_account'] = 'vmc';
            } else {
                $options['method'] = 'wallet-card';
            }
        }

        return $options;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $httpResponse = Http::withToken($this->getSecretKey())
            ->baseUrl($this->getSiteUrl())
            ->asJson();

        $order_data = $this->getOrderData();


        $result = $httpResponse->post('/api/v2/deposit', $data)->json();
        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'firekassa',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => $this->getSiteUrl().'api/v2/deposit',
            'headers' => isset($httpResponse->getOptions()['headers']) ? json_encode($httpResponse->getOptions()['headers']) : null,
            'content' => json_encode($data),
            'response' => json_encode($result)
        ]);

        if(!$result['id'] and !is_int($result['id'])) {
            throw new \Exception(json_encode($result));
        }

        if($order_data->merchant->type_pay == 0) {
            return $this->response = new PurchaseResponse($this, $result);
        } else {
            return $this->response = new PurchaseH2HResponse($this, $result);
        }
    }
}
