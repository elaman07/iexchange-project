<?php

namespace App\Common\Packages\Payment\Gateways\Firekassa\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Получить Site name
     *
     * @return string
     */
    public function getSiteName(): string
    {
        return $this->getParameter('site_name');
    }

    /**
     * Установить Site name
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSiteName(string $value): AbstractRequest
    {
        return $this->setParameter('site_name', $value);
    }

    /**
     * Получить Site name
     *
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->getParameter('site_url');
    }

    /**
     * Установить Site name
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSiteUrl(string $value): AbstractRequest
    {
        return $this->setParameter('site_url', $value);
    }
}
