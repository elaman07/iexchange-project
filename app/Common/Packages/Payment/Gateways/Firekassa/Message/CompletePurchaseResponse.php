<?php


namespace App\Common\Packages\Payment\Gateways\Firekassa\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Arr;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['status'] == 'paid';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return in_array($this->data['status'], ['error', 'cancel']);
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        return $this->data['status'] == 'process';
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['order_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return 'RUB';
    }


    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return '';
    }
}
