<?php

namespace App\Common\Packages\Payment\Gateways\Firekassa\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;


class PurchaseH2HResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return isset($this->data['card_number']) && !empty($this->data['card_number']);
    }

    public function isRedirect(): bool {
        return false;
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress(): string
    {
        return mask_formatting_bank_card($this->data['card_number']);
    }

    /**
     * Получаем валюту
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->data['currency'];
    }

    /**
     * Получаем описание
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->data['id'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        if(isset($this->data['last_name']) and !empty($this->data['last_name'])) {
            return sprintf('%s %s %s', $this->data['last_name'] ?? '', $this->data['first_name'] ?? '', $this->data['middle_name'] ?? '');
        }

        return '';
    }
}
