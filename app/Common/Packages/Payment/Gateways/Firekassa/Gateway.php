<?php

namespace App\Common\Packages\Payment\Gateways\Firekassa;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Firekassa';

    /**
     * Получить Client Secret
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Client Secret
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Получить Site name
     *
     * @return string
     */
    public function getSiteName(): string
    {
        return $this->getParameter('site_name');
    }

    /**
     * Установить Site name
     *
     * @param string $value
     * @return Gateway
     */
    public function setSiteName(string $value): Gateway
    {
        return $this->setParameter('site_name', $value);
    }

    /**
     * Получить Site name
     *
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->getParameter('site_url');
    }

    /**
     * Установить Site name
     *
     * @param string $value
     * @return Gateway
     */
    public function setSiteUrl(string $value): Gateway
    {
        return $this->setParameter('site_url', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'secret_key' => '',
            'site_name' => '',
            'site_url' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
