<?php

namespace App\Common\Packages\Payment\Gateways\FourBill;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'FourBill';

    /**
     * Получить Point ID
     *
     * @return string
     */
    public function getPointId(): string
    {
        return $this->getParameter('point_id');
    }

    /**
     * Установить Point ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setPointId(string $value): Gateway
    {
        return $this->setParameter('point_id', $value);
    }

    /**
     * Получить Acc ID
     *
     * @return string
     */
    public function getAccId(): string
    {
        return $this->getParameter('acc_id');
    }

    /**
     * Установить Acc ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccId(string $value): Gateway
    {
        return $this->setParameter('acc_id', $value);
    }

    /**
     * Получить Wallet ID
     *
     * @return string
     */
    public function getWalletId(): string
    {
        return $this->getParameter('wallet_id');
    }

    /**
     * Установить Wallet ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setWalletId(string $value): Gateway
    {
        return $this->setParameter('wallet_id', $value);
    }

    /**
     * Получить Service  ID
     *
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->getParameter('service_id');
    }

    /**
     * Установить Service  ID
     *
     * @param string $value
     * @return Gateway
     */
    public function setServiceId(string $value): Gateway
    {
        return $this->setParameter('service_id', $value);
    }

    /**
     * Получить Token
     *
     * @return string
     */
    public function getTokenId(): string
    {
        return $this->getParameter('token_id');
    }

    /**
     * Установить Token
     *
     * @param string $value
     * @return Gateway
     */
    public function setTokenId(string $value): Gateway
    {
        return $this->setParameter('token_id', $value);
    }


    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'point_id' => '',
            'acc_id' => '',
            'wallet_id' => '',
            'service_id' => '',
            'token_id' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
