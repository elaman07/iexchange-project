<?php

namespace App\Common\Packages\Payment\Gateways\FourBill;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * URL 4Bill API
     *
     * @var string
     */
    const API_URL = 'https://api.settlepay.net';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return int|mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->call('/account/info');
        $data = collect($response['response']['wallets'])->filter(function($item) use ($currency){
            return $item['currency'] == $currency and $item['id'] == $this->parameters['wallet_id'];
        })->first();

        return $data['status'] == 1 ? $data['balance'] : 0;
    }

    /**
     * Получение деталей транзакции
     *
     * @param string $id
     * @return int|mixed
     * @throws Exception
     */
    public function find(string $id)
    {
        $response = $this->call('/transaction/find', [
            'id' => $id
        ]);

        if($response['error']['code'] != 0) {
            throw new \Exception(json_encode($response['error']));
        }
        return $response;
    }

    /**
     * Отправим средства клиенту на счет
     *
     * @param string $to
     * @param string $currency
     * @param $amount
     * @param int $order
     * @param null $description
     * @return mixed
     * @throws \Exception
     */
    public function transfer(string $to, string $currency, $amount, int $order, $description = null)
    {
        $result_amount = str_replace('.', '', sprintf('%01.2f', $amount));


        $response = $this->call('/transaction/pay', [
            'locale'                => 'ru',
            'account_id'            => $this->parameters['acc_id'],
            'wallet_id'             => $this->parameters['wallet_id'],
            'service_id'            => $this->parameters['service_id'],
            'amount'                => (int)$result_amount,
            'amount_currency'       => Str::upper($currency),
            'external_order_id'     => (string)$order,
            'customer_ip_address'   => '0.0.0.0',
            'fields' => [
                'card_number' => preg_replace('/\s+/', '', $to)
            ],
            'description' => (empty($description) ? '' : $description)
        ]);

        if($response['error']['code'] != 0) {
            throw new \Exception(json_encode($response['error']));
        }
        return $response;
    }

    /**
     * Проверка карты
     *
     * @param string|null $card
     * @return bool
     */
    public function checkCard(string $card = null)
    {
        try {
            $response = $this->call('/cards/whitelist/check', [
                'card_number' => $card
            ]);
            return $response['response']['in_whitelist'] == 1;
        }catch (\Exception $exception) {
            return 0;
        }
    }

    /**
     * Добавление в белый список
     *
     * @param string $card
     * @return int
     */
    public function addWhitelist(string $card)
    {
        try {
            $this->call('/cards/whitelist/add', [
                'card_number' => $card
            ]);
        }catch (\Exception $exception) {
            return 0;
        }
    }

    /**
     * Запрос на получение данных
     *
     * @param string $path
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    protected function call(string $path, array $params = [])
    {
        $timestamp = time();
        $response = Http::baseUrl(self::API_URL)->asJson()->post($path,
            array_merge([
                'auth' => [
                    'debug' => true,
                    'point' => $this->parameters['point_id'],
                    "key" => $timestamp,
                    'hash' => md5($this->parameters['point_id'].$this->parameters['token_id'].$timestamp)
                ]
            ], $params))->json();


        if(isset($response['error']) and $response['error']['code'] != 0) {
            throw new \Exception($response['error']['title']);
        }

        return $response;
    }
}
