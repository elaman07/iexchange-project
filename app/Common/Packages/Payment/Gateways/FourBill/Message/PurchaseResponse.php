<?php

namespace App\Common\Packages\Payment\Gateways\FourBill\Message;


use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RedirectResponseInterface,
    RequestInterface
};

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful()
    {
        return false;
    }

    public function isRedirect()
    {
        return true;
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['response']) and isset($this->data['response']['result']['pay_url'])) {
            return $this->data['response']['result']['pay_url'];
        }
    }

    public function getRedirectMethod()
    {
        return 'GET';
    }

    /**
     * Получить данные ответа.
     *
     * @return mixed
     */
    public function getRedirectData()
    {
        return $this->data;
    }

    public function getLabel()
    {
        return $this->data['response']['id'];
    }
}

