<?php

namespace App\Common\Packages\Payment\Gateways\FourBill\Message;

use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $response = $this->httpRequest->query->all();
        $this->parameters->add(
            array_intersect_key($response, array_flip((array)['order_id', 'external_transaction_id']))
        );

        $this->validate('order_id', 'external_transaction_id');

        return $response;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
