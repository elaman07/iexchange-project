<?php


namespace App\Common\Packages\Payment\Gateways\FourBill\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use App\Models\LogErrorMerchant;
use Illuminate\Support\Facades\Http;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @var
     */
    protected $transaction;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        $this->transaction = $this->findTransaction();
        if(!isset($this->transaction) or !isset($this->transaction['response'])) {
            \Log::error('4Bill (callback): Invalid Transaction ID');
            throw new InvalidResponseException('4bill: Invalid Transaction ID');
        }

        // Не пропускаем, если платеж тестовый
        if($this->transaction['response']['is_test'] == true) {
            \Log::error('4Bill (callback): In the test version, transaction processing is prohibited');
            throw new InvalidResponseException('4bill: Test Transaction');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->transaction['response']['status'] == 1;
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->transaction['response']['status'] != 1;
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->transaction['response']['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->transaction['response']['external_order_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        $int_amount = $this->transaction['response']['amount'];

        // Если комиссию платит обменник
        $str_amount = implode('.', preg_split('~(?=\d{2}$)~', (string)$int_amount));
        if($this->request->getOrderData()->merchant->is_pay_commission == 0) {
            return iex_number_format(($str_amount * 1.0125) + 5);
        }

        return iex_number_format($str_amount);
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return str_replace('RUR', 'RUB', $this->transaction['response']['amount_currency']);
    }

    /**
     * Получаем детали транзакции
     *
     * @throws InvalidResponseException
     */
    protected function findTransaction()
    {
        $httpresponse = Http::baseUrl('https://api.settlepay.net')->asJson()->post('/transaction/find', [
            'auth' => $this->request->getAuthHeader(),
            'external_transaction_id' => $this->data['external_transaction_id']
        ])->json();

        if($httpresponse['error']['code'] != 0) {
            throw new InvalidResponseException('4bill: Error Find Transaction, Code: '.$httpresponse['error']['code']);
        }

        return $httpresponse;
    }
}
