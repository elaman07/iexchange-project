<?php

namespace App\Common\Packages\Payment\Gateways\FourBill\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Facades\Http;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate('wallet_id',
            'amount', 'currency', 'transactionId',
            'service_id'
        );

        // Если комиссию платит обменник
        if($this->getOrderData()->merchant->is_pay_commission == 0) {
            $amount = str_replace('.', '', sprintf('%01.2f', ($this->getAmount() - 5) / (float)$this->getOrderData()->merchant->fixed_fee));
        } else {
            $amount = str_replace('.', '', sprintf('%01.2f', $this->getAmount()));
        }


        return [
            'locale' => app()->getLocale(),
            'account_id' => $this->getAccId(),
            'wallet_id' => $this->getWalletId(),
            'service_id' => $this->getServiceId(),
            'amount' => (int)$amount,
            'amount_currency' => $this->getCurrency(),
            'external_order_id' => $this->getTransactionId(),
            'external_transaction_id' => $this->getOrderData()->unique_security_code,
            'customer_ip_address' => (string)$this->getClientIp(),
            'external_customer_id'  => $this->getOrderData()->id_user,
            'point' => [
                'success_url' => $this->getReturnUrl(),
                'fail_url' => $this->getCancelUrl(),
                'callback_url' => $this->getNotifyUrl().'?order_id='.$this->getTransactionId().'&external_transaction_id='.$this->getOrderData()->unique_security_code,
            ],
            'description' => $this->getDescription()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $response = Http::asJson()->post('https://api.settlepay.net/transaction/create',
            array_merge([
                'auth' => $this->getAuthHeader()
            ], $data))->json();

        if(isset($response['error']) and $response['error']['code'] != 0)
        {
            \Log::error('4bill: '.$response['error']['title']);
            echo 'Возникли ошибки при оплате, свяжитесь с администратором';
            exit;
        }

        return $this->response = new PurchaseResponse($this, $response);
    }
}
