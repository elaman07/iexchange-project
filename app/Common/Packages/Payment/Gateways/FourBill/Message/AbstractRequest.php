<?php

namespace App\Common\Packages\Payment\Gateways\FourBill\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Point ID
     *
     * @return string
     */
    public function getPointId(): string
    {
        return $this->getParameter('point_id');
    }

    /**
     * Установить Point ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPointId(string $value): AbstractRequest
    {
        return $this->setParameter('point_id', $value);
    }

    /**
     * Получить Acc ID
     *
     * @return string
     */
    public function getAccId(): string
    {
        return $this->getParameter('acc_id');
    }

    /**
     * Установить Acc ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccId(string $value): AbstractRequest
    {
        return $this->setParameter('acc_id', $value);
    }

    /**
     * Получить Wallet ID
     *
     * @return string
     */
    public function getWalletId(): string
    {
        return $this->getParameter('wallet_id');
    }

    /**
     * Установить Wallet ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWalletId(string $value): AbstractRequest
    {
        return $this->setParameter('wallet_id', $value);
    }

    /**
     * Получить Service  ID
     *
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->getParameter('service_id');
    }

    /**
     * Установить Service  ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setServiceId(string $value): AbstractRequest
    {
        return $this->setParameter('service_id', $value);
    }

    /**
     * Получить Token
     *
     * @return string
     */
    public function getTokenId(): string
    {
        return $this->getParameter('token_id');
    }

    /**
     * Установить Token
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setTokenId(string $value): AbstractRequest
    {
        return $this->setParameter('token_id', $value);
    }

    /**
     * Авторизационные параметры 4Bill
     *
     * @return array
    */
    public function getAuthHeader(): array
    {
        $timestamp = time();

        return [
            'debug' => false,
            'point' => $this->getPointId(),
            "key" => $timestamp,
            'hash' => md5($this->getPointId().$this->getTokenId().$timestamp)
        ];
    }

}
