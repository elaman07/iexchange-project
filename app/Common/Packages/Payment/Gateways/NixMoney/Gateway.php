<?php

namespace App\Common\Packages\Payment\Gateways\NixMoney;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'NixMoney';


    /**
     * Получить E-mail от аккаунта NixMoney
     *
     * @return string
     */
    public function getAccountEmail(): string
    {
        return $this->getParameter('account_email');
    }

    /**
     * Установить E-mail от аккаунта NixMoney
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountEmail(string $value): Gateway
    {
        return $this->setParameter('account_email', $value);
    }

    /**
     * Получить Пароль от аккаунта NixMoney
     *
     * @return string
     */
    public function getAccountPassword(): string
    {
        return $this->getParameter('account_password');
    }

    /**
     * Установить Пароль от аккаунта NixMoney
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountPassword(string $value): Gateway
    {
        return $this->setParameter('account_password', $value);
    }

    /**
     * Получить USD номер счета
     *
     * @return string
     */
    public function getAccountUsd(): string
    {
        return $this->getParameter('account_usd');
    }

    /**
     * Установить USD номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountUsd(string $value): Gateway
    {
        return $this->setParameter('account_usd', $value);
    }

    /**
     * Получить EUR номер счета
     *
     * @return string
     */
    public function getAccountEur(): string
    {
        return $this->getParameter('account_eur');
    }

    /**
     * Установить EUR номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountEur(string $value): Gateway
    {
        return $this->setParameter('account_eur', $value);
    }

    /**
     * Получить BTC номер счета
     *
     * @return string
     */
    public function getAccountBtc(): string
    {
        return $this->getParameter('account_btc');
    }

    /**
     * Установить BTC номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountBtc(string $value): Gateway
    {
        return $this->setParameter('account_btc', $value);
    }

    /**
     * Получить LTC номер счета
     *
     * @return string
     */
    public function getAccountLtc(): string
    {
        return $this->getParameter('account_ltc');
    }

    /**
     * Установить LTC номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountLtc(string $value): Gateway
    {
        return $this->setParameter('account_ltc', $value);
    }

    /**
     * Получить PPC номер счета
     *
     * @return string
     */
    public function getAccountPpc(): string
    {
        return $this->getParameter('account_ppc');
    }

    /**
     * Установить PPC номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountPpc(string $value): Gateway
    {
        return $this->setParameter('account_ppc', $value);
    }

    /**
     * Получить FTC номер счета
     *
     * @return string
     */
    public function getAccountFtc(): string
    {
        return $this->getParameter('account_ftc');
    }

    /**
     * Установить FTC номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountFtc(string $value): Gateway
    {
        return $this->setParameter('account_ftc', $value);
    }

    /**
     * Получить CRT номер счета
     *
     * @return string
     */
    public function getAccountCrt(): string
    {
        return $this->getParameter('account_crt');
    }

    /**
     * Установить CRT номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountCrt(string $value): Gateway
    {
        return $this->setParameter('account_crt', $value);
    }

    /**
     * Получить GBC номер счета
     *
     * @return string
     */
    public function getAccountGbc(): string
    {
        return $this->getParameter('account_gbc');
    }

    /**
     * Установить GBC номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountGbc(string $value): Gateway
    {
        return $this->setParameter('account_gbc', $value);
    }

    /**
     * Получить DOGE номер счета
     *
     * @return string
     */
    public function getAccountDoge(): string
    {
        return $this->getParameter('account_doge');
    }

    /**
     * Установить DOGE номер счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountDoge(string $value): Gateway
    {
        return $this->setParameter('account_doge', $value);
    }

    /**
     * Получить Имя продавца
     *
     * @return string
     */
    public function getAccountName() : string
    {
        return $this->getParameter('account_name');
    }

    /**
     * Установка Имя продавца
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountName(string $value): Gateway
    {
        return $this->setParameter('account_name', $value);
    }


    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'account_password' => '',
            'account_email' => '',
            'account_name' => '',
            'account_usd' => '',
            'account_eur' => '',
            'account_btc' => '',
            'account_ltc' => '',
            'account_ppc' => '',
            'account_ftc' => '',
            'account_crt' => '',
            'account_gbc' => '',
            'account_doge' => '',
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
