<?php

namespace App\Common\Packages\Payment\Gateways\NixMoney;


use DiDom\Document;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * URL NixMoney API
     *
     * @var string
     */
    const API_URL = 'https://www.nixmoney.com';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Отправка средств на счет
     *
     * @param array $options
     * @return array
     * @throws Exception
     */
    public function transfer(array $options = []): array
    {
        $array = [
            'PAYER_ACCOUNT' =>  $this->parameters['account_'.Str::lower($options['currency'])],
            'PAYEE_ACCOUNT' =>  $options['to'],
            'AMOUNT'        =>  $options['amount']
        ];
        if(isset($options['comment'])) $array['MEMO'] = $options['comment'];
        $response = $this->call('send', $array);

        // Разбор HTML Ответа
        $parser = new Document($response);
        if(!$parser->has('input')) {
            throw new \Exception('Транзакция не прошла');
        }

        $data = [];
        foreach ($parser->find('input') as $input) {
            if($input->getAttribute('name') == 'ERROR') {
                throw new \Exception($input->getAttribute('value'));
            }

            $data[$input->getAttribute('name')] = $input->getAttribute('value');
        }

        return $data;
    }

    /**
     * Получение баланса
     *
     * @param null $currency
     * @return array|mixed
     * @throws Exception
     */
    public function getBalance($currency)
    {
        $response = $this->call('balance', []);

        $parser = new Document($response);
        if (!$parser->has('input')) {
            throw new Exception('Баланс не определен');
        }

        $array = [];
        foreach ($parser->find('input') as $input)
        {
            if($input->getAttribute('name') == 'ERROR') {
                throw new Exception($input->getAttribute('value'));
            }
            $array[$input->getAttribute('name')] = $input->getAttribute('value');
        }

        return $array[$this->parameters['account_'.Str::lower($currency)]];
    }

    /**
     * Запрос к  api
     *
     * @param string $url
     * @param array $params
     * @return string
     */
    public function call(string $url, array $params = []): string
    {
        $data = array_merge([
            'ACCOUNTID'  =>  $this->parameters['account_email'],
            'PASSPHRASE'  =>  $this->parameters['account_password'],
        ], $params);

        $request = Http::baseUrl(self::API_URL)->asForm()->post($url, $data);
        return $request->body();
    }
}
