<?php

namespace App\Common\Packages\Payment\Gateways\NixMoney\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Str;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     * @throws \Exception
     */
    public function getData(): array
    {
        $this->validate('transactionId', 'currency', 'amount');

        $data['PAYEE_ACCOUNT'] = $this->getAccount();
        $data['PAYEE_NAME'] = $this->getAccountName();
        $data['PAYMENT_AMOUNT'] = $this->getAmount();
        $data['PAYMENT_URL'] = $this->getReturnUrl();
        $data['NOPAYMENT_URL'] = $this->getCancelUrl();
        $data['PAYMENT_ID'] = $this->getTransactionId();
        $data['STATUS_URL'] = $this->getNotifyUrl();
        $data['SUGGESTED_MEMO'] = $this->getDescription();

        return $data;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
