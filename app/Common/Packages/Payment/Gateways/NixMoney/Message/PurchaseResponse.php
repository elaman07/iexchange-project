<?php

namespace App\Common\Packages\Payment\Gateways\NixMoney\Message;


use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RedirectResponseInterface,
    RequestInterface
};

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful()
    {
        return false;
    }

    public function isRedirect()
    {
        return true;
    }

    public function getRedirectUrl()
    {
        return 'https://www.nixmoney.com/merchant.jsp';
    }

    public function getRedirectMethod()
    {
        return 'POST';
    }

    public function getRedirectData()
    {
        return [
            'order_id'  =>  $this->request->getTransactionId()
        ];
    }
}

