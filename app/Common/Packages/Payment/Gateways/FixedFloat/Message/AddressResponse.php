<?php


namespace App\Common\Packages\Payment\Gateways\FixedFloat\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['msg']) && $this->data['msg'] == 'OK';
    }

    public function isRedirect(): bool
    {
        return false;
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->data['data']['from']['address'];
    }

    public function getUniqueOrderHashId()
    {
        return $this->data['data']['id'];
    }

    public function getUniqueToAddress()
    {
        return $this->data['data']['to']['address'];
    }

    public function getTokenAddress()
    {
        return $this->data['data']['token'];
    }
    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return '';
    }
}
