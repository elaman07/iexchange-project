<?php


namespace App\Common\Packages\Payment\Gateways\FixedFloat\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $from_shot = preg_replace('/\s+/', '', $this->getOrderData()->from_shot);
        $to_shot = preg_replace('/\s+/', '', $this->getOrderData()->to_shot);

        $from_network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        $from_code = $this->getOrderData()->direction_exchange->currency1->code_currency->name;
        if(!empty($from_network_code)) {
            $from_code = $from_network_code;
        }

        $to_network_code = $this->getOrderData()->direction_exchange->currency2->network_code;
        $to_code = $this->getOrderData()->direction_exchange->currency2->code_currency->name;
        if(!empty($to_network_code)) {
            $to_code = $to_network_code;
        }

        $options = [];
        $options['type'] = 'fixed';
        $options['toAddress'] = $to_shot;
        $options['fromCcy'] = $from_code;
        $options['toCcy'] = $to_code;
        $options['afftax'] = $this->getOrderData()->direction_exchange->currency1->commission_merchant_percent;
        $options['amount'] = $this->getOrderData()->give_price;
        $options['direction'] = "from";

        $signature = hash_hmac('sha256', json_encode($options), $this->getSecretKey());

        $http_response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'X-API-KEY' => $this->getApiKey(),
            'X-API-SIGN' => $signature
        ])->asJson()->post('https://ff.io/api/v2/create', $options)->json();

        return $this->response = new AddressResponse($this, $http_response);
    }
}
