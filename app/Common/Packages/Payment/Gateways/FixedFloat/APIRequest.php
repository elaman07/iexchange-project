<?php

namespace App\Common\Packages\Payment\Gateways\FixedFloat;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * URL WhiteBit API
     *
     * @var string
     */
    const API_URL = 'https://ff.io';

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        return 0;
    }

    /**
     * Детали транзакции
     *
     * @param string $hash_id
     * @param string $to_address
     * @return array|bool
     */
    public function findTransaction(string $hash_id, string $token)
    {
        $response =  $this->call('post', '/api/v2/order', [
            'id' => $hash_id,
            'token' => $token
        ]);

        return $response;
    }

    /**
     * Запросы К API
     *
     * @param string $url
     * @param array $params
     * @return array|bool
     */
    public function call(string $method, string $url, array $params = [])
    {
        $data = $params;
        $signature = hash_hmac('sha256', json_encode($data), $this->parameters['secret_key']);

        return Http::withHeaders([
            'Content-Type' => 'application/json',
            'X-API-KEY' => $this->parameters['api_key'],
            'X-API-SIGN' => $signature
        ])->asJson()->{$method}(self::API_URL.$url, $data)->json();
    }
}
