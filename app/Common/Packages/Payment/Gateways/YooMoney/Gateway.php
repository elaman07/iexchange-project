<?php

namespace App\Common\Packages\Payment\Gateways\YooMoney;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'YooMoney';

    /**
     * Получить номер счета
     *
     * @return string
    */
    public function getAccountId() : string
    {
        return $this->getParameter('account_id');
    }

    /**
     * Установка нового номера счета
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountId(string $value): Gateway
    {
        return $this->setParameter('account_id', $value);
    }

    /**
     * Получить пароль
     *
     * @return string
     */
    public function getPassword() : string
    {
        return $this->getParameter('password');
    }

    /**
     * Установка нового пароля
     *
     * @param string $value
     * @return Gateway
     */
    public function setPassword(string $value): Gateway
    {
        return $this->setParameter('password', $value);
    }

    /**
     * Получить Client Id
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->getParameter('client_id');
    }

    /**
     * Установка нового Client Id
     *
     * @param string $value
     * @return Gateway
     */
    public function setClientId(string $value): Gateway
    {
        return $this->setParameter('client_id', $value);
    }

    /**
     * Получить  Client Secret
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->getParameter('client_secret');
    }

    /**
     * Установка нового значения для Client Secret
     *
     * @param string $value
     * @return Gateway
     */
    public function setClientSecret(string $value): Gateway
    {
        return $this->setParameter('client_secret', $value);
    }

    /**
     * Получить Access Token
     *
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->getParameter('access_token');
    }

    /**
     * Установка нового значения для Access Token
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccessToken(string $value): Gateway
    {
        return $this->setParameter('access_token', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'access_token' => '',
            'account_id' => '',
            'client_id' => '',
            'client_secret' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
