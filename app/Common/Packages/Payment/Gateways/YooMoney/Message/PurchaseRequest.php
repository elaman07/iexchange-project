<?php

namespace App\Common\Packages\Payment\Gateways\YooMoney\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'account_id', 'transactionId', 'amount'
        );

        // Детали заявки
        $order = $this->getOrderData();
        // Комиссия мерчанта
        $commission = (float)$this->getOrderData()->direction_exchange->currency1->commission_merchant_percen;
        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $total_amount = $amount;
        if($commission > 0) {
            $total_amount = (float)iex_number_format( $amount + $amount * $commission / 100, 2);
        }

        return [
            'receiver'          =>  $this->getAccountId(),
            'formcomment'       =>  $this->getDescription(),
            'short-dest'        =>  $this->getDescription(),
            'label'             =>  $this->getTransactionId(),
            'quickpay-form'     => 'shop',
            'targets'           =>  $this->getDescription(),
            'sum'               =>  (float)$total_amount,
            'paymentType'       =>  $this->getParameter('paymentType')
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
