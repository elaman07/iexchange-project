<?php

namespace App\Common\Packages\Payment\Gateways\YooMoney\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить номер счета
     *
     * @return string
     */
    public function getAccountId() : string
    {
        return $this->getParameter('account_id');
    }

    /**
     * Установка нового номера счета
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountId(string $value): AbstractRequest
    {
        return $this->setParameter('account_id', $value);
    }

    /**
     * Получить пароль
     *
     * @return string
     */
    public function getPassword() : string
    {
        return $this->getParameter('password');
    }

    /**
     * Установка нового пароля
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPassword(string $value): AbstractRequest
    {
        return $this->setParameter('password', $value);
    }

    /**
     * Получить Client Id
     *
     * @return string
     */
    public function getClientId(): string
    {
        return $this->getParameter('client_id');
    }

    /**
     * Установка нового Client Id
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setClientId(string $value): AbstractRequest
    {
        return $this->setParameter('client_id', $value);
    }

    /**
     * Получить  Client Secret
     *
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->getParameter('client_secret');
    }

    /**
     * Установка нового значения для Client Secret
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setClientSecret(string $value): AbstractRequest
    {
        return $this->setParameter('client_secret', $value);
    }
}
