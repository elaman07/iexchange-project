<?php


namespace App\Common\Packages\Payment\Gateways\YooMoney\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }

        // Если поле в Label нет данных
        if(is_null($this->getTransactionId()) or empty($this->getTransactionId())) {
            throw new  InvalidResponseException('Поле label пуста');
        }

        // перевод еще не зачислен на счет получателя.
        if ($this->getUnaccepted() !== 'false') {
            throw new InvalidResponseException('Платеж не был успешным');
        }

        // перевод защищен кодом протекции
        if($this->getCodePro() !== 'false') {
            throw new InvalidResponseException('Перевод защищен кодом протекции');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->getSign() == $this->signatureResponse();
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->getUnaccepted() !== 'false';
    }

    public function getUnaccepted()
    {
        return $this->data['unaccepted'];
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['operation_id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['label'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['amount'];
    }

    /**
     * @return string Always "RUB".
     *
     * Yandex always returns currency=643 (mapped to currencyId), so currency is always RUB.
     * @see https://tech.yandex.ru/money/doc/dg/reference/notification-p2p-incoming-docpage/
     */
    public function getCurrency(): string
    {
        return ($this->data['currency'] == '643' ? 'RUB' : 'None');
    }

    /**
     * Код протекции
     *
     * @return boolean
     */
    public function getCodePro(): bool
    {
        return $this->data['codepro'];
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['sha1_hash'];
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return $this->data['sender'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        return hash('sha1', implode('&', [
            $this->data['notification_type'],
            $this->data['operation_id'],
            $this->data['amount'],
            $this->data['currency'],
            $this->data['datetime'],
            $this->data['sender'],
            $this->data['codepro'],
            $this->request->getPassword(),
            $this->data['label']
        ]));
    }
}
