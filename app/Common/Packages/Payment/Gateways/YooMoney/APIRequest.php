<?php

namespace App\Common\Packages\Payment\Gateways\YooMoney;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class APIRequest
{
    /**
     * API Сервиса
     *
     * @var string
     */
    protected string $api = 'https://yoomoney.ru';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * GuzzleHttp клиент
     *
     * @var Client
     */
    protected Client $client;

    /**
     * Создаем констркутор YandexMoney API
     *
     * @param $config
     */
    public function __construct($config)
    {
        $this->parameters = $config;
    }

    /**
     * Получаем баланс
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance()
    {
        $response = $this->sendRequest('/api/account-info', 'GET');
        return $response['balance'];
    }

    /**
     * Позволяет получить детальную информацию об операции из истории.
     *
     * @param $operationId
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransaction($operationId) {
        return $this->sendRequest('/api/operation-details', 'POST', [
            'form_params' => ['operation_id' => (string)$operationId]
        ]);
    }

    /**
     * История транзакций
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHistoryTx($options)
    {
        return $this->sendRequest('/api/operation-history', 'GET', $options);
    }

    /**
     * Запросить платеж
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function requestPayment($options)
    {
        return $this->sendRequest('/api/request-payment', 'POST', [
            'form_params' => $options
        ]);
    }

    /**
     * Подтверждение платежа
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function processPayment($options)
    {
        return $this->sendRequest('/api/process-payment', 'POST', [
            'form_params' => $options
        ]);
    }

    /**
     * Получаем ссылку для авторизации на YandexMoney
     *
     * @return string
     */
    public function buildObtainTokenUrl(): string
    {
        $params = sprintf(
            "client_id=%s&client_secret=%s&response_type=%s&redirect_uri=%s&scope=%s",
            $this->parameters['client_id'],
            $this->parameters['client_secret'],
            "code", url('merchant-verify/yoomoney/callback'),
            implode(' ', ['account-info', 'operation-history', 'operation-details', 'payment-p2p'])
        );

        return sprintf("%s/oauth/authorize?%s", $this->api, $params);
    }

    /**
     * Получаем AccessToken
     *
     * @param $code
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAccessToken($code)
    {
        return Http::baseUrl($this->api)->post('/oauth/token', [
            'code'          => $code,
            'client_id'     => $this->parameters['client_id'],
            'client_secret' => $this->parameters['client_secret'],
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => url('merchant-verify/yandexmoney/callback')
        ])->json();
    }

    /**
     * Вызов для получения данных из Yoomoney
     *
     * @param string $method
     * @param $path
     * @param array $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendRequest($path, string $method = 'get', array $options = [])
    {
        $this->client = new Client([
            'base_uri' => $this->api,
            'headers'   =>  [
                'Accept' =>  'application/json',
                'Authorization' => 'Bearer '. $this->parameters['access_token'],
                'Host' => 'yoomoney.ru'
            ]
        ]);

        $response = $this->client->request($method, $path, $options);
        return json_decode($response->getBody()->getContents(), true);
    }
}
