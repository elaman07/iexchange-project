<?php

namespace App\Common\Packages\Payment\Gateways\Obmenka\Message;

use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseRequest extends AbstractRequest
{

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate(
            'kassa_id', 'secret_key'
        );

        return $this->httpRequest->request->all();
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        return $this->response = new CompletePurchaseResponse($this, $data);
    }
}
