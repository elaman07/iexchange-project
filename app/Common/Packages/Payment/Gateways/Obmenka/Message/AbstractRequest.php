<?php

namespace App\Common\Packages\Payment\Gateways\Obmenka\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Str;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить ID магазина
     *
     * @return string
     */
    public function getKassaId(): string
    {
        return $this->getParameter('kassa_id');
    }

    /**
     * Установить ID магазина
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setKassaId(string $value): AbstractRequest
    {
        return $this->setParameter('kassa_id', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }


    /**
     * Получаем ID провайдера
    */
    public function getProviderId(): string
    {
        $code_currency = str_replace('RUB', 'RUR', Str::upper($this->getCurrency()));

        // Тип транзакции
        switch ($this->getOrderData()->merchant->method_pay)
        {
            // ADVCash USD
            case 1:
                $type = 'advcash.usd';
                break;
            // Visa/Mastercard (RUB, UAH, KZT)
            case 2:
                $type = 'visamaster.'.$code_currency;
                break;
            // BITCOIN
            case 3:
                $type = 'bitcoin';
                break;
            case 4:
                $type = 'bitcoin_cash';
                break;
            case 5:
                $type = 'litecoin';
                break;
            case 6:
                $type = 'ethereum';
                break;
            case 7:
                $type = 'tether';
                break;
            case 8:
                $type = 'usdt_erc20';
                break;
            // Стандартная выплата
            default:
                $type = 'qiwi';
        }
        return $type;
    }

}
