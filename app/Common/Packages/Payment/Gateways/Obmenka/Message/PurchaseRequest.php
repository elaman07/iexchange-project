<?php

namespace App\Common\Packages\Payment\Gateways\Obmenka\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate('kassa_id', 'secret_key', 'amount', 'currency', 'transactionId');


        $fee = (float)$this->getOrderData()->direction_exchange->currency1->commission_merchant_percent;
        // Если комиссию платит обменник
        if($this->getOrderData()->merchant->is_pay_commission == 1) {
            $amount = (float)iex_number_format( (float)$$this->getAmount() + (float)$$this->getAmount() * (float)$fee / 100, 2);
        } else {
            $amount =  $this->getAmount();
        }

        $response = [
            'CLIENT_ID'         => $this->getKassaId(),
            'INVOICE_ID'        => $this->getTransactionId(),
            'AMOUNT'            => $amount,
            'CURRENCY'          => $this->getCurrency(),
            'PAYMENT_CURRENCY'  => $this->getProviderId(),
            'DESCRIPTION'       => $this->getDescription(),
            'SUCCESS_URL'       => $this->getReturnUrl(),
            'FAIL_URL'          => $this->getCancelUrl(),
            'STATUS_URL'        => $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
        ];

        $sign = base64_encode(md5($this->getSecretKey() . base64_encode(sha1(implode('', $response), true)) . $this->getSecretKey(), true));
        $response['SIGN_ORDER'] = implode(';', array_keys($response));
        $response['SIGN'] = $sign;
        return array_filter($response);
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
