<?php


namespace App\Common\Packages\Payment\Gateways\Obmenka\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Facades\Http;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @var
     */
    protected $transaction;


    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        $this->transaction = $this->findTransaction();


        \Log::debug('json: .'. json_encode($this->transaction));
    }

    public function isPending()
    {
        return in_array($this->transaction['status'], ['IN_PROGRESS', 'CREATED']);
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->transaction['status'] == 'FINISHED';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return in_array($this->transaction['status'], ['FAILED', 'BLOCKED', 'REVERSED', 'CANCELED', 'REFILL']);
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->transaction['tracking'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->transaction['payment_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->transaction['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return str_replace('RUR', 'RUB', $this->transaction['accrual_currency']);
    }

    /**
     * Получаем детали транзакции
     *
     * @throws InvalidResponseException
     */
    protected function findTransaction()
    {
        $postParams = [
            'payment_id' => $this->data['order_id']
        ];

        $httpresponse = Http::asJson()->withHeaders([
            'DPAY_CLIENT' => $this->request->getKassaId(),
            'DPAY_SECURE' => base64_encode(md5($this->request->getSecretKey() . base64_encode(sha1(json_encode($postParams), true)) . $this->request->getSecretKey(), true))
        ])->post('https://acquiring_api.obmenka.ua/api/einvoice/status', $postParams);

        if($httpresponse->successful())
        {
            $data = $httpresponse->json();
            if(isset($data['error'])) {
                throw new InvalidResponseException('Obmenka UA: Error Find Transaction, Code: '.json_encode($data));
            }

            if(!isset($data['tracking'])) {
                throw new InvalidResponseException('Obmenka UA: Error Find Transaction, Code: '.json_encode($data));
            }

            return $data;
        } else {
            throw new InvalidResponseException('Obmenka UA: Error Find Transaction, Code: '.json_encode($httpresponse->json()));
        }
    }
}
