<?php

namespace App\Common\Packages\Payment\Gateways\Obmenka;

use Exception;
use Illuminate\Support\Facades\Http;

class APIRequest
{
    /**
     * URL NixMoney API
     *
     * @var string
     */
    const API_URL = 'https://acquiring_api.obmenka.ua';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @return float | string
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->call('/api/payment/balance');
        $key = collect($response['balances'])->filter(function($item) use($currency) {
            return $item['currency'] == $currency;
        })->first();

        if(isset($key['balance'])) {
            return $key['balance'];
        }

        throw new Exception('Obmenka: Неверные настройки');
    }

    /**
     * Отправка средств
     *
     * @param $to
     * @param $amount
     * @param string $currency
     * @param int $order_id
     * @param null $comment
     * @return mixed
     * @throws Exception
     */
    public function transfer($to, $amount, string $currency, int $order_id, $comment = null)
    {
        $create = $this->call('/api/payment/create', [
            'recipient'     => $to,
            'amount'        => $amount,
            'currency'      => $currency,
            'description'   => $comment,
            'payment_id'    => (string)$order_id
        ]);

        if(isset($create['error']) and isset($create['error']['message'])) {
            throw new \Exception(json_encode($create['error'], JSON_UNESCAPED_UNICODE));
        }

        $validate = $this->call('/api/payment/validate', $create);
        if(isset($validate['is_valid']) and $validate['is_valid'] == 1) {
            $response = $this->call('/api/payment/process', $create);
            return array_merge($response, $create);
        }

        throw new Exception('Средства не отправлены');
    }


    /**
     * Форма запроса к платежной системе
     * @param string $path
     * @param array $params
     * @return array|mixed
     */
    public function call(string $path, array $params = [])
    {
        $sign = base64_encode(md5($this->parameters['secret_key'] . base64_encode(sha1(json_encode($params), true)) . $this->parameters['secret_key'], true));

        return Http::baseUrl(self::API_URL)->withHeaders([
            'DPAY_CLIENT' => $this->parameters['kassa_id'],
            'DPAY_SECURE' => $sign
        ])->asJson()->post($path, $params)->json();
    }
}
