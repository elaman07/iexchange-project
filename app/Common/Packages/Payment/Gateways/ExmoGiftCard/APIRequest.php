<?php

namespace App\Common\Packages\Payment\Gateways\ExmoGiftCard;

use Exception;
use Illuminate\Support\Facades\Http;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return string
     * @throws Exception
     */
    public function getBalance(string $currency): string
    {
        $balance = $this->call('/user_info');
        return $balance['balances'][$currency];
    }

    /**
     * Создание купона EX-CODE
     *
     * @param $amount - сумма на которую создается купон
     * @param $currency - наименование валюты купона
     * @return mixed
     * @throws Exception
     */
    public function createCode($amount, $currency = null)
    {
        return $this->call('excode_create', [
            'currency'  =>  $currency,
            'amount'    =>  $amount
        ]);
    }

    /**
     * Загрузка купона EX-CODE
     *
     * @param string $code
     * @return mixed
     * @throws Exception
     */
    public function activateCode(string $code)
    {
        return $this->call('excode_load', [
            'code'  =>  $code
        ]);
    }

    /**
     * Вызов определенных действий для "Exmo"
     *
     * @param string $path
     * @param array $params
     * @return mixed
     *
     * @throws Exception
     */
    public function call(string $path, array $params = [])
    {
        $mt = explode(' ', microtime());
        $params['nonce'] = $mt[1] . substr($mt[0], 2, 6);
        $sign = hash_hmac('sha512', http_build_query($params), $this->parameters['secret_key']);

        $response =  Http::baseUrl('https://api.exmo.com/v1.1')->asForm()->withHeaders([
            'Key' => $this->parameters['public_key'],
            'Sign' => $sign
        ])->post($path, $params)->json();


        if(isset($response['result']) and $response['result'] == false) {
            throw new \Exception($response['error']);
        }

        return $response;
    }
}
