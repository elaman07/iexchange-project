<?php

namespace App\Common\Packages\Payment\Gateways\Stellar\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить основной адрес
     *
     * @return string
     */
    public function getMainAddress() : string
    {
        return $this->getParameter('main_address');
    }

    /**
     * Установка основного адреса
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setMainAddress(string $value): AbstractRequest
    {
        return $this->setParameter('main_address', $value);
    }

    /**
     * Получить приватный ключ
     *
     * @return string
     */
    public function getPrivateKey() : string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установка приватного ключа
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPrivateKey(string $value): AbstractRequest
    {
        return $this->setParameter('private_key', $value);
    }
}
