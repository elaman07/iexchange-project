<?php

namespace App\Common\Packages\Payment\Gateways\Stellar;


use Exception;
use IEXBase\RippleAPI\Ripple;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use ZuluCrypto\StellarSdk\Horizon\Api\PostTransactionResponse;
use ZuluCrypto\StellarSdk\Server;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];


    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return float
     * @throws Exception
     */
    public function getBalance(string $currency): float
    {
        if(Str::upper($currency) != 'XLM') {
            throw new \Exception('Неверный код валюты');
        }
        $server = Server::publicNet();
        $balance = Arr::first($server->getAccount($this->parameters['main_address'])->getBalances());
        return $balance->getBalance();
    }

    /**
     * Детали по заяве
    */
    public function findTransaction($account): array
    {
        $http = Http::withHeaders([
            'Connection' => 'keep-alive',
            'Upgrade-Insecure-Requests' => '1',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language' => 'en-US,en;q=0.8'
        ])->get('https://api.stellar.expert/explorer/public/payments', [
            'asset' => 'XLM',
            'memo' => (string)$account,
            'account' => $this->parameters['main_address']
        ])->json();

        $array = Arr::first($http['_embedded']['records']);


        if(!empty($array) and $array['source_asset'] == 'XLM')
        {
            if($array['amount'] != $array['source_amount']) {
                throw new \Exception('Балансы не совпадают');
            }

            if($array['asset'] != $array['source_asset']) {
                throw new \Exception('Коды валют не совпадают');
            }

            return [
                'address'        =>  $array['from'],
                'amount'         =>  $array['amount'],
                'confirmations'  =>  2,
                'txid'           =>  $array['tx_id']
            ];
        }

        return [];
    }

    /**
     * Отправляем средства клиенту
     *
     * @param $to
     * @param $amount
     * @param array $options
     * @return PostTransactionResponse
     * @throws Exception
     */
    public function transfer($to, $amount, array $options = []): PostTransactionResponse
    {
        $server = Server::publicNet();
        $transaction = $server->buildTransaction($this->parameters['main_address']);

        if(!$server->accountExists($to)) {
            $transaction->addCreateAccountOp($to, $amount);
        } else {
            $transaction->addLumenPayment($to, $amount);
        }

        if(isset($options['memo_id']))
        {
            if(is_int($options['memo_id'])) {
                $transaction->setIdMemo($options['memo_id']);
            } else {
                $transaction->setTextMemo((string)$options['memo_id']);
            }
        }

        return $transaction->submit($this->parameters['private_key']);
    }

    /**
     * Запросы
     *
     * @return Ripple
     */
    public function request(): Ripple
    {
        return new Ripple($this->parameters['main_address'], $this->parameters['private_key'], [
            'wss_node' => 'https://validator.iexbase.com'
        ]);
    }
}
