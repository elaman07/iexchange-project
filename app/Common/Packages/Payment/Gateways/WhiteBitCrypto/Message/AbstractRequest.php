<?php

namespace App\Common\Packages\Payment\Gateways\WhiteBitCrypto\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPublicKey(string $value): AbstractRequest
    {
        return $this->setParameter('public_key', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }
}
