<?php


namespace App\Common\Packages\Payment\Gateways\WhiteBitCrypto\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $request_url = '/api/v4/main-account/create-new-address';
        $baseUrl = 'https://whitebit.com';
        $nonce = (string) (int) (microtime(true) * 10000);

        $options = [
            'ticker' => $this->getCoinCurrency(),
            'request' => $request_url,
            'nonce' => $nonce,
        ];

        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        if(!empty($network_code)) {
            $options['network'] = $network_code;
        }

        $dataJsonStr = json_encode($options, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $this->getSecretKey());


        $headers = [
            'Content-type' => 'application/json',
            'X-TXC-APIKEY' => $this->getPublicKey(),
            'X-TXC-PAYLOAD' => $payload,
            'X-TXC-SIGNATURE' => $signature
        ];

        $http_response = \Http::baseUrl($baseUrl)->asJson()->withHeaders($headers)->post($request_url, $options)->body();

        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'WhiteBitCrypto',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => 'https://whitebit.com/api/v4/main-account/create-new-address',
            'headers' => json_encode($headers),
            'content' => json_encode($options),
            'response' => $http_response
        ]);

        return $this->response = new AddressResponse($this, json_decode($http_response, true));
    }
}
