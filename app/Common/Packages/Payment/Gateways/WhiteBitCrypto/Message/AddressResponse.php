<?php


namespace App\Common\Packages\Payment\Gateways\WhiteBitCrypto\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['account']) && isset($this->data['account']['address']);
    }

    public function isRedirect(): bool
    {
        return false;
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->data['account']['address'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->data['account']['memo'] ?? '';
    }
}
