<?php

namespace App\Common\Packages\Payment\Gateways\WhiteBitCrypto;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * URL WhiteBit API
     *
     * @var string
     */
    const API_URL = 'https://whitebit.com';

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->call('/api/v4/main-account/balance', [
            'ticker' => $currency
        ]);
        return ($response['main_balance'] ?? 0);
    }


    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws Exception
     */
    public function getAllBalances()
    {
        $response = $this->call('/api/v4/main-account/balance');
        return collect($response)->map(function($item, $key) {
            return [
                'id' => $key,
                'value' => $item['main_balance']
            ];
        })->pluck('value','id')->toArray();
    }

    /**
     * Детали транзакции
     *
     * @param string $currency
     * @param string $address
     * @return array|bool
     * @throws Exception
     */
    public function findTransaction(string $currency, string $address)
    {
        $response =  $this->call('/api/v4/main-account/history', [
            'transactionMethod' => '1',
            'ticker' => $currency,
            'address' => $address,
            'limit' => 1,
            'offset' => 0
        ]);

        if(isset($response['records'])) {
            return Arr::first($response['records']);
        }
        return [];
    }

    /**
     * Детали транзакции
     *
     * @param string $currency
     * @param string $address
     * @return array|bool
     * @throws Exception
     */
    public function findTransactionWithdraw($ticker, $order_id)
    {
        $response =  $this->call('/api/v4/main-account/history', [
            'transactionMethod' => '2',
            'ticker' => $ticker,
            'uniqueId'  => (string)$order_id,
            'limit' => 1,
            'offset' => 0
        ]);

        if(isset($response['records'])) {
            return Arr::first($response['records']);
        }
        return [];
    }

    /**
     * Отправка средств
     *
     * @param array $options
     * @return array|bool
     * @throws Exception
     */
    public function transfer(array $options = [])
    {
        $params = [
            'ticker'    => Str::upper($options['currency']),
            'amount'    => (string)$options['amount'],
            'address'   => (string)$options['address'],
            'uniqueId'  => (string)$options['order_id'],
        ];

        $params['memo'] = (string)($options['memo'] ?? $options['order_id']);

        if(isset($options['network']))
            $params['network'] = (string)$options['network'];

        $response = $this->call('/api/v4/main-account/withdraw-pay', $params);

        if(isset($response['errors'])) {
            throw new Exception(json_encode($response['errors']).'----'. json_encode($params));
        }

        return $response;
    }

    /**
     * Запросы К API
     *
     * @param string $url
     * @param array $params
     * @return array|bool
     */
    public function call(string $url, array $params = [])
    {
        $data = array_merge($params, [
            'request' => $url,
            'nonce' => (string) (int) (microtime(true) * 1000),
            'nonceWindow' => true
        ]);

        $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
        $payload = base64_encode($dataJsonStr);
        $signature = hash_hmac('sha512', $payload, $this->parameters['secret_key']);

        $headers = [
            'X-TXC-APIKEY' => $this->parameters['public_key'],
            'X-TXC-PAYLOAD' => $payload,
            'X-TXC-SIGNATURE' => $signature
        ];

        $response =  Http::withHeaders($headers)->asJson()->post(self::API_URL.$url, $data)->json();

        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'WhiteBitCrypto',
            'id_order' => $params['uniqueId'] ?? 0,
            'url' => self::API_URL.$url,
            'headers' => json_encode($headers),
            'content' => json_encode($data),
            'response' => json_encode($response)
        ]);

        return $response;
    }
}
