<?php

namespace App\Common\Packages\Payment\Gateways\KunaPay;

use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * URL KunaPay API
     *
     * @var string
     */
    const API_URL = 'https://paygate.kuna.io';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return int
     * @throws \Exception
     */
    public function getBalance(string $currency): int
    {
        $response = $this->call('GET', '/account');
        return $response['data']['attributes']['currency_accounts'][$currency]['active_balance'] ?? 0;
    }


    /**
     * Отправим средства клиенту на счет
     *
     * @param string $to
     * @param string $currency
     * @param $amount
     * @param int $order_id
     * @param null $description
     * @return mixed
     * @throws \Exception
     */
    public function transfer(string $to, string $currency, $amount, int $order_id, $description = null)
    {
        $response = $this->call('POST', '/payout-invoices', [
            'data' => [
                'type' => 'payout-invoice',
                'attributes' => array_filter([
                    'reference_id' => (string)$order_id,
                    'service' => sprintf('payment_card_%s', \Str::lower($currency)),
                    'currency' => (string)$currency,
                    'amount' => (float)$amount,
                    'fields' => [
                        'card_number' => preg_replace('/\s+/', '', $to)
                    ],
                    'options' => [
                        'auto_process' => true,
                    ],

                    'description' => (empty($description) ? '' : $description)
                ])
            ]
        ]);

        return $response;
    }


    /**
     * Запрос на получение данных
     *
     * @param string $method
     * @param string $route
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    protected function call(string $method, string $route, array $params = [])
    {
        $httpRequest = Http::withBasicAuth($this->parameters['public_key'], $this->parameters['private_key'])
            ->asJson()
            ->send($method, self::API_URL.$route, ['json' => $params]);

        $response = $httpRequest->json();

        if(isset($response['errors'])) {
            throw new \Exception(json_encode($response['errors']));
        }

        return $response;
    }
}
