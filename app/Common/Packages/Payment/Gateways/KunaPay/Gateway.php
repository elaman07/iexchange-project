<?php

namespace App\Common\Packages\Payment\Gateways\KunaPay;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'KunaPay';

    /**
     * Получить Публичный ключ
     *
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Установить Публичный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setPublicKey(string $value): Gateway
    {
        return $this->setParameter('public_key', $value);
    }

    /**
     * Получить Sci Password
     *
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установить SCI Password
     *
     * @param string $value
     * @return Gateway
     */
    public function setPrivateKey(string $value): Gateway
    {
        return $this->setParameter('private_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'public_key' => '',
            'private_key' => '',
            'service_id' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
