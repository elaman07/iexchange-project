<?php

namespace App\Common\Packages\Payment\Gateways\KunaPay\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     * @throws \Exception
     */
    public function getData()
    {
        $this->validate(
            'public_key', 'private_key', 'transactionId', 'currency'
        );

        return array_filter([
            'reference_id' => (string)$this->getTransactionId(),
            'flow' => 'charge',
            'service' => sprintf('payment_card_%s_hpp', Str::lower($this->getCurrency())),
            'currency' => $this->getCurrency(),
            'amount' => $this->getAmount(),
            'callback_url' => $this->getNotifyUrl(),
            'return_urls' => [
                'success' => $this->getReturnUrl(),
                'pending' => $this->getReturnUrl(),
                'fail' => $this->getCancelUrl()
            ],
            'description' => $this->getDescription(),
            'metadata' => [
                'unique_id' => (string)$this->getOrderData()->unique_security_code,
            ]
        ]);
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $httpresponse = Http::withBasicAuth($this->getPublicKey(), $this->getPrivateKey())->asJson()
            ->post('https://paygate.kuna.io/payment-invoices', [
                'data' => [
                    'type' => 'payment-invoices',
                    'attributes' => $data
                ]
            ]);

        $response = $httpresponse->json();

        if(isset($response['errors'])) {
            throw new \Exception(json_encode($response));
        }

        if(isset($response['error'])) {
            throw new \Exception(json_encode($response['error']));
        }

        if(empty($response['data']['attributes']['flow_data'])) {
            throw new \Exception('no http route');
        }

        return $this->response = new PurchaseResponse($this, $response);
    }
}
