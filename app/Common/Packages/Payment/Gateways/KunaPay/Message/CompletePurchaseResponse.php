<?php


namespace App\Common\Packages\Payment\Gateways\KunaPay\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Facades\Request;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);


        \Log::debug('---------'.Request::header('x-signature'));

        // Проверяем, если подписи не совместимы.
        if(Request::header('x-signature') !== $this->signatureResponse()) {
            throw new InvalidResponseException('KunaPay: Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['data']['attributes']['status'] == 'processed';
    }

    /**
     * Если транзакция в ожидации
     *
     * @return boolean
     */
    public function isPending()
    {
        return $this->data['data']['attributes']['status'] == 'process_pending';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->data['data']['attributes']['status'] != 'processed';
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['data']['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return (int)$this->data['data']['attributes']['reference_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['data']['attributes']['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string|integer
     */
    public function getCurrency()
    {
        return str_replace('RUR', 'RUB', $this->data['data']['attributes']['service_currency']);
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse()
    {
        \Log::debug('--------- priv: '.$this->request->getPrivateKey());
        \Log::debug('--------- priv: '.json_encode($this->data));
        \Log::debug( base64_encode(sha1($this->request->getPrivateKey() . json_encode($this->data) . $this->request->getPrivateKey(), true)));

        return base64_encode(sha1($this->request->getPrivateKey() . json_encode($this->data) . $this->request->getPrivateKey(), true));
    }
}
