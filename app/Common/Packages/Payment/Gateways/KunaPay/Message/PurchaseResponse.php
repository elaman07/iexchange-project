<?php

namespace App\Common\Packages\Payment\Gateways\KunaPay\Message;


use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RequestInterface
};

class PurchaseResponse extends AbstractResponse
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['data']['attributes']) and !empty($this->data['data']['attributes']['flow_data'])) {
            return $this->data['data']['attributes']['flow_data']['action'];
        }
    }

    public function getRedirectMethod()
    {
        return 'GET';
    }

    public function getRedirectData(): array
    {
        return [];
    }

    public function getLabel()
    {
        return $this->data['data']['id'];
    }
}
