<?php

namespace App\Common\Packages\Payment\Gateways\Binance;


use App\Common\Packages\Payment\Gateways\BlockIo\Includes\BlockIo;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    protected string $base = 'https://api.binance.com/api/'; // /< REST endpoint for the currency exchange
    protected string $wapi = 'https://api.binance.com/wapi/'; // /< REST endpoint for the withdrawals
    protected string $sapi = 'https://api.binance.com/sapi/'; // /< REST endpoint for the supporting network API

    /**
     * Количество переданных байтов
     *
     * @var integer
     */
    protected int $transfered = 0;

    /**
     * Количество запросов API
     *
     * @var integer
     */
    protected int $requestCount = 0;

    /**
     * Если вы включите это, curl будет выводить информацию об отладке
     *
     * @var boolean
     */
    public bool $httpDebug = false;

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    protected $exchangeInfo = null;

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return string | float
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $array = $this->httpRequest( 'v3/account', 'GET',[],true);
        $response = collect($array['balances'])->filter(function ($item) use ($currency){
            return $item['asset'] == $currency;
        })->first();

        if(!empty($response)) {
            return $response['free'];
        }

        throw new \Exception('Не найден код валюты');
    }


    /**
     * depositAddress - Get the deposit address for an asset
     *
     * @link https://binance-docs.github.io/apidocs/spot/en/#deposit-address-supporting-network-user_data
     *
     * @property int $weight 1
     *
     * @param string $asset    (mandatory)  An asset, e.g. BTC
     * @param string|null $network  (optional)   You can get network in networkList from /sapi/v1/capital/config/getall
     *
     * @return array containing the response
     * @throws \Exception
     */
    public function depositAddress(string $asset, string $network = null)
    {
        $params = [
            "sapi" => true,
            "coin" => $asset,
        ];
        if (is_null($network) === false && empty($network) === false) {
            $params['network'] = $network;
        }

        $return = $this->httpRequest("v1/capital/deposit/address", "GET", $params, true);

        // Adding for backwards compatibility with wapi
        $return['asset'] = $return['coin'];
        $return['addressTag'] = $return['tag'];

        if (!empty($return['address'])) {
            $return['success'] = 1;
        } else {
            $return['success'] = 0;
        }

        return $return;
    }

    /**
     * coins get list coins
     *
     * $coins = $api->coins();
     * @return array with error message or array containing coins
     * @throws \Exception
     */
    public function coins()
    {
        return $this->httpRequest('v1/capital/config/getall', 'GET', [ 'sapi' => true ], true);
    }

    /**
     * Отправляем средства на другой счет
     *
     *
    */
    public function transfer(string $asset, string $address, $amount, $addressTag = null, $addressName = "", bool $transactionFeeFlag = false, $network = null, $orderId = null)
    {
        $options = [
            'coin'      => $asset,
            'address'   => $address,
            'amount'    => $amount,
            'transactionFeeFlag' => $transactionFeeFlag,
            'sapi'      => true,
        ];

        if (is_null($addressName) === false && empty($addressName) === false) {
            $options['name'] = str_replace(' ', '%20', $addressName);
        }

        if (is_null($addressTag) === false && empty($addressTag) === false) {
            $options['addressTag'] = $addressTag;
        }

        if (is_null($network) === false && empty($network) === false) {
            $options['network'] = $network;
        }
        if (is_null($orderId) === false && empty($orderId) === false) {
            $options['withdrawOrderId'] = $orderId;
        }


        return $this->httpRequest("v1/capital/withdraw/apply", "POST", $options, true);
    }

    /**
     * Создать валютный ордер по рыночной цене
     *
     * $quantity = 1;
     * $order = $api->marketBuy("BNBBTC", $quantity);
     *
     * @param $symbol string the currency symbol
     * @param $quantity string the quantity required
     * @param $flags array addtional options for order type
     * @return array with error message or the order details
     * @throws Exception
     */
    public function marketBuy(string $symbol, string $quantity, array $flags = [])
    {
        return $this->order('BUY', $symbol, $quantity, 0, 'MARKET', $flags);
    }



    /**
     * buy attempts to create a currency order
     * each currency supports a number of order types, such as
     * -LIMIT
     * -MARKET
     * -STOP_LOSS
     * -STOP_LOSS_LIMIT
     * -TAKE_PROFIT
     * -TAKE_PROFIT_LIMIT
     * -LIMIT_MAKER
     *
     * You should check the @param $symbol string the currency symbol
     * @param $quantity string the quantity required
     * @param $price string price per unit you want to spend
     * @param $type string type of order
     * @param $flags array addtional options for order type
     * @return array with error message or the order details
     * @throws Exception
     * @throws Exception
     * @see exchangeInfo for each currency to determine
     * what types of orders can be placed against specific pairs
     *
     * $quantity = 1;
     * $price = 0.0005;
     * $order = $api->buy("BNBBTC", $quantity, $price);
     *
     */
    public function buy(string $symbol, string $quantity, string $price, string $type = "LIMIT", array $flags = [])
    {
        return $this->order("BUY", $symbol, $quantity, $price, $type, $flags);
    }


    /**
     * order formats the orders before sending them to the curl wrapper function
     * You can call this function directly or use the helper functions
     *
     * @param $side string typically "BUY" or "SELL"
     * @param $symbol string to buy or sell
     * @param $quantity string in the order
     * @param $price string for the order
     * @param $type string is determined by the symbol bu typicall LIMIT, STOP_LOSS_LIMIT etc.
     * @param $flags array additional transaction options
     * @return array containing the response
     * @throws Exception
     * @see buy()
     * @see sell()
     * @see marketBuy()
     * @see marketSell() $this->httpRequest( "https://api.binance.com/api/v1/ticker/24hr");
     */
    public function order(string $side, string $symbol, string $quantity, string $price, string $type = "LIMIT", array $flags = [])
    {
        $c = $this->numberOfDecimals($this->exchangeInfo()['symbols'][$symbol]['filters'][2]['minQty']);
        $quantity = $this->floorDecimal($quantity, $c);

        $opt = [
            "symbol" => $symbol,
            "side" => $side,
            "type" => $type,
            "quantity" => $quantity,
            "recvWindow" => 60000,
        ];

        // someone has preformated there 8 decimal point double already
        // dont do anything, leave them do whatever they want
        if (gettype($price) !== "string") {
            $price = number_format($price, 8, '.', '');
        }

        if (is_numeric($quantity) === false) {
            echo "warning: quantity expected numeric got " . gettype($quantity) . PHP_EOL;
        }

        if (is_string($price) === false) {
            echo "warning: price expected string got " . gettype($price) . PHP_EOL;
        }

        if ($type === "LIMIT" || $type === "STOP_LOSS_LIMIT" || $type === "TAKE_PROFIT_LIMIT") {
            $opt["price"] = $price;
            $opt["timeInForce"] = isset($flags['timeInForce']) ? Str::upper($flags['timeInForce']) : 'GTC';
        }

        if (isset($flags['stopPrice'])) {
            $opt['stopPrice'] = $flags['stopPrice'];
        }

        if (isset($flags['icebergQty'])) {
            $opt['icebergQty'] = $flags['icebergQty'];
        }

        if (isset($flags['newOrderRespType'])) {
            $opt['newOrderRespType'] = $flags['newOrderRespType'];
        }

        return $this->httpRequest('v3/order', "POST", $opt, true);
    }


    /**
     * exchangeInfo Gets the complete exchange info, including limits, currency options etc.
     *
     * $info = $api->exchangeInfo();
     *
     * @return array with error message or exchange info array
     * @throws \Exception
     */
    public function exchangeInfo(): array
    {
        return \cache()->rememberForever('binance-exchange-info', function ()
        {
            if (!$this->exchangeInfo) {
                $arr = $this->httpRequest("v3/exchangeInfo");

                $this->exchangeInfo = $arr;
                $this->exchangeInfo['symbols'] = null;

                foreach ($arr['symbols'] as $key => $value) {
                    $this->exchangeInfo['symbols'][$value['symbol']] = $value;
                }
            };

            return $this->exchangeInfo;
        });
    }

    /**
     * Запросы
     *
     * @param string $url
     * @param string $method
     * @param array $params
     * @param bool $signed
     * @return array|bool
     * @throws Exception
     */
    private function httpRequest( string $url, string $method = "GET", array $params = [], bool $signed = false )
    {
        $base_url = $this->base;
        if(isset($params['wapi']))
            $base_url = $this->wapi;
        elseif(isset($params['sapi']))
            $base_url = $this->sapi;

        $http = Http::baseUrl($base_url);
        $http->withHeaders([
            'X-MBX-APIKEY' => $this->parameters['api_key']
        ]);

        // Исключаем параметры из массива
        if(isset($params['wapi']))
            unset($params['wapi']);

        if (isset($params['sapi'])) {
            unset($params['sapi']);
        }

        // Если требуется подпись
        if($signed == true) {
            $params['timestamp'] = Carbon::now()->timestamp * 1000;
            $signature = hash_hmac( 'sha256', http_build_query($params), $this->parameters['secret_key'] );
            $params['signature'] = $signature;

        }

        if(Str::lower($method) == 'get') {
            $http_response = $http->get($url, $params)->json();
        } else {
            $http_response = $http->asForm()->post($url, $params)->json();
        }

        if(isset($http_response['msg']) and $http_response['code'] != 0) {
            throw new \Exception($http_response['msg']);
        }

        return $http_response;
    }

    /**
     * assetDetail - Fetch details of assets supported on Binance
     *
     * @link https://binance-docs.github.io/apidocs/spot/en/#asset-detail-user_data
     *
     * @property int $weight 1
     *
     * @return array containing the response
     * @throws Exception
     */
    public function assetDetail()
    {
        $params["sapi"] = true;
        $arr = $this->httpRequest("v1/asset/assetDetail", 'GET', $params, true);
        // wrap into another array for backwards compatibility with the old wapi one
        if (!empty($arr['BTC']['withdrawFee'])) {
            return array(
                'success'     => 1,
                'assetDetail' => $arr,
            );
        } else {
            return array(
                'success'     => 0,
                'assetDetail' => array(),
            );

        }
    }

    /**
     * withdrawFee - Get the withdrawal fee for an asset
     *
     * @property int $weight 1
     *
     * @param string $asset    (mandatory)  An asset, e.g. BTC
     *
     * @return array containing the response
     * @throws \Exception
     */
    public function withdrawFee(string $asset)
    {
        $return = $this->assetDetail();

        if (isset($return['success'], $return['assetDetail'], $return['assetDetail'][$asset]) && $return['success']) {
            return $return['assetDetail'][$asset];
        } else {
            return array();
        }
    }

    /**
     * commissionFee - Fetch commission trade fee
     *
     * @link https://binance-docs.github.io/apidocs/spot/en/#trade-fee-user_data
     *
     * @property int $weight 1
     *
     * @param string $symbol  (optional)  Should be a symbol, e.g. BNBUSDT or empty to get the full list
     *
     * @return array containing the response
     * @throws \Exception
     */
    public function commissionFee(string $symbol = '')
    {
        $params = ['sapi' => true];
        if ($symbol != '' && gettype($symbol) == 'string')
            $params['symbol'] = $symbol;

        return $this->httpRequest("v1/asset/tradeFee", 'GET', $params, true);
    }

    /**
     * numberOfDecimals() returns the signifcant digits level based on the minimum order amount.
     *
     * $dec = numberOfDecimals(0.00001); // Returns 5
     *
     * @param $val float the minimum order amount for the pair
     * @return integer (signifcant digits) based on the minimum order amount
     */
    public function numberOfDecimals(float $val = 0.00000001)
    {
        $val = sprintf("%.14f", $val);
        $parts = explode('.', $val);
        $parts[1] = rtrim($parts[1], "0");
        return strlen($parts[1]);
    }


    protected function floorDecimal($n, $decimals=2)
    {
        return floor($n * pow(10, $decimals)) / pow(10, $decimals);
    }
}
