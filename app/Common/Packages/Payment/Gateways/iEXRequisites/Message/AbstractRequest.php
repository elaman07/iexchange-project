<?php

namespace App\Common\Packages\Payment\Gateways\iEXRequisites\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{

    /**
     * Получить Ключ API
     *
     * @return string
     */
    public function getSiteName() : string
    {
        return $this->getParameter('site_name');
    }

    /**
     * Установка Ключ API
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSiteName(string $value): AbstractRequest
    {
        return $this->setParameter('site_name', $value);
    }
}
