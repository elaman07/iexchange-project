<?php


namespace App\Common\Packages\Payment\Gateways\iEXRequisites\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Gateways\Binance\APIRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $code_xml = \Str::upper($this->getOrderData()->direction_exchange->currency1->designation_xml);

        $http_response = Http::withHeaders([])->baseUrl('https://'.$this->getSiteName())->asJson()->post('/api/v1/post_requisites')->json();


        if(!empty($http_response) and is_array($http_response)) {

            $item = collect($http_response)->first(function($item) use($code_xml) {
               return  $item['code'] == $code_xml;
            });
            return $this->response = new AddressResponse($this, $item);
        }
    }
}
