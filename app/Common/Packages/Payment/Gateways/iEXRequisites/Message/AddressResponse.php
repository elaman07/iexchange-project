<?php


namespace App\Common\Packages\Payment\Gateways\iEXRequisites\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return !empty($this->data);
    }

    public function isRedirect(): bool
    {
        return false;
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->data['value'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return '';
    }
}
