<?php

namespace App\Common\Packages\Payment\Gateways\iEXRequisites;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\iEXRequisites\Message\AddressRequest;


class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'iEXRequisites';

    /**
     * Получить Ключ API
     *
     * @return string
     */
    public function getSiteName() : string
    {
        return $this->getParameter('site_name');
    }

    /**
     * Установка Ключ API
     *
     * @param string $value
     * @return Gateway
     */
    public function setSiteName(string $value): Gateway
    {
        return $this->setParameter('site_name', $value);
    }


    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'site_name' => '',
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return [];
    }
}
