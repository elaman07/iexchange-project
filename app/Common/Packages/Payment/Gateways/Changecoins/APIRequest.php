<?php

namespace App\Common\Packages\Payment\Gateways\Changecoins;


use ChangeCoins\ClientFacade;
use ChangeCoins\Dto\BalanceDto;
use ChangeCoins\Dto\UserDataDto;
use ChangeCoins\Dto\WithdrawalDto;
use ChangeCoins\Exception\ResponseValidationException;
use ChangeCoins\Factory\RequestConfig;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string|null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $balanceDto = new BalanceDto();
        $balanceDto->setNonce(time());

        $result = $this->request()->getBalance($balanceDto)->toArray();
        return isset($result[$currency]) ? $result[$currency]['balance'] : null;
    }

    /**
     * Отправка средств
     *
     * @param array $options
     * @return array|bool
     * @throws \Exception
     */
    public function transfer(array $options = [])
    {
        try {
            $userDataDto = new UserDataDto();
            $userDataDto->setPayee((string)$options['address']);
            if(isset($options['tag']))
                $userDataDto->setMemo($options['tag']);

            $withdrawalDto = new WithdrawalDto();
            $withdrawalDto
                ->setExternalId('order:'.$options['order_id'])
                ->setAmount($options['amount'])

                ->setCurrency(mb_strtoupper($options['currency']))
                ->setUserData($userDataDto)
                ->setNonce(time());

            return $this->request()->createWithdrawal($withdrawalDto)->toArray();
        }catch (ResponseValidationException $exception) {
            throw  new \Exception(sprintf('Error msg: %s. Error code: %s.', $exception->getMessage(), $exception->getCode()));
        }
    }

    /**
     * Запросы WestWallet
     *
     * @return \ChangeCoins\ApiClientInterface
     */
    public function request(): \ChangeCoins\ApiClientInterface
    {
        $clientFacade = new ClientFacade(new RequestConfig($this->parameters['api_key'], $this->parameters['api_secret']));
        $client       = $clientFacade->createClient();


        return $client;
    }
}
