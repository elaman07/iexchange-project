<?php


namespace App\Common\Packages\Payment\Gateways\Changecoins\Message;

use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RequestInterface
};
use Illuminate\Support\Arr;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        $arr = Arr::first($this->data['transactions']);
        return !empty($arr) and $arr['status'] == 'confirmed';
    }

    /**
     * Если транзакция в ожидании
     *
     * @return boolean
     */
    public function isPending(): bool
    {
        $arr = Arr::first($this->data['transactions']);
        return empty($arr);
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return in_array($this->data['status'], ['fail', 'reject']);
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['externalid'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        $arr = Arr::first($this->data['transactions']);
        return !empty($arr) and $arr['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        $arr = Arr::first($this->data['transactions']);
        return !empty($arr) and $arr['currency'];
    }

    /**
     * Получение ID в blockchain
     *
     * @return string
     */
    public function getBlockchainId(): string
    {
        $arr = Arr::first($this->data['transactions']);
        return !empty($arr) and $arr['txid'];
    }

    /**
     * Получение счетчика подтверждений
     *
     * @return mixed
     */
    public function getBlockchainConfirmation()
    {
        return 1;
    }
}
