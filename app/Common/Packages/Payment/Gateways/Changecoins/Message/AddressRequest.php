<?php


namespace App\Common\Packages\Payment\Gateways\Changecoins\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use ChangeCoins\ClientFacade;
use ChangeCoins\Dto\DepositDto;
use ChangeCoins\Exception\ResponseValidationException;
use ChangeCoins\Factory\RequestConfig;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [
            'currency'  =>  $this->getCoinCurrency(),
            'foreign_id'  => (string)$this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $clientFacade = new ClientFacade(new RequestConfig($this->getApiKey(), $this->getApiSecret()));
        $client       = $clientFacade->createClient();

        try {
            $depositDto = new DepositDto();
            $depositDto
                ->setExternalId($data['foreign_id'])
                ->setCurrency($data['currency'])
                ->setNonce(time());

            $response = $client->depositCreate($depositDto)->toArray();

        } catch (ResponseValidationException $exception) {
            throw new \Exception(sprintf('Error msg: %s. Error code: %s.', $exception->getMessage(), $exception->getCode()));
        }

        return $this->response = new AddressResponse($this, $response);
    }
}
