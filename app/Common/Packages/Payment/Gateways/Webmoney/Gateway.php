<?php

namespace App\Common\Packages\Payment\Gateways\Webmoney;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Webmoney';

    /**
     * Получить WMID
     *
     * @return string
     */
    public function getWmid(): string
    {
        return $this->getParameter('wmid');
    }

    /**
     * Установить WMID
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmid(string $value): Gateway
    {
        return $this->setParameter('wmid', $value);
    }

    /**
     * Получить Пароль от файла ключей .kwm
     *
     * @return string
     */
    public function getKeyPass(): string
    {
        return $this->getParameter('key_pass');
    }

    /**
     * Установить Пароль от файла ключей .kwm
     *
     * @param string $value
     * @return Gateway
     */
    public function setKeyPass(string $value): Gateway
    {
        return $this->setParameter('key_pass', $value);
    }

    /**
     * Получить WMZ кошелек
     *
     * @return string
     */
    public function getWmzPurse(): string
    {
        return $this->getParameter('wmz_purse');
    }

    /**
     * Установить WMZ кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmzPurse(string $value): Gateway
    {
        return $this->setParameter('wmz_purse', $value);
    }

    /**
     * Получить Secret Key WMZ кошелька
     *
     * @return string
     */
    public function getWmzSecretKey(): string
    {
        return $this->getParameter('wmz_secret_key');
    }

    /**
     * Установить Secret Key WMZ кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmzSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmz_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMZ кошелька
     *
     * @return string
     */
    public function getWmzX20SecretKey(): string
    {
        return $this->getParameter('wmz_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMZ кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmzX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmz_x20_secret_key', $value);
    }

    /**
     * Получить WMR кошелек
     *
     * @return string
     */
    public function getWmrPurse(): string
    {
        return $this->getParameter('wmr_purse');
    }

    /**
     * Установить WMR кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmrPurse(string $value): Gateway
    {
        return $this->setParameter('wmr_purse', $value);
    }

    /**
     * Получить Secret Key WMR кошелька
     *
     * @return string
     */
    public function getWmrSecretKey(): string
    {
        return $this->getParameter('wmr_secret_key');
    }

    /**
     * Установить Secret Key WMR кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmrSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmr_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMR кошелька
     *
     * @return string
     */
    public function getWmrX20SecretKey(): string
    {
        return $this->getParameter('wmr_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMR кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmrX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmr_x20_secret_key', $value);
    }

    /**
     * Получить WME кошелек
     *
     * @return string
     */
    public function getWmePurse(): string
    {
        return $this->getParameter('wme_purse');
    }

    /**
     * Установить WME кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmePurse(string $value): Gateway
    {
        return $this->setParameter('wme_purse', $value);
    }

    /**
     * Получить Secret Key WME кошелька
     *
     * @return string
     */
    public function getWmeSecretKey(): string
    {
        return $this->getParameter('wme_secret_key');
    }

    /**
     * Установить Secret Key WME кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmeSecretKey(string $value): Gateway
    {
        return $this->setParameter('wme_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WME кошелька
     *
     * @return string
     */
    public function getWmeX20SecretKey(): string
    {
        return $this->getParameter('wme_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WME кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmeX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wme_x20_secret_key', $value);
    }

    /**
     * Получить WMU кошелек
     *
     * @return string
     */
    public function getWmuPurse(): string
    {
        return $this->getParameter('wmu_purse');
    }

    /**
     * Установить WMU кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmuPurse(string $value): Gateway
    {
        return $this->setParameter('wmu_purse', $value);
    }

    /**
     * Получить Secret Key WMU кошелька
     *
     * @return string
     */
    public function getWmuSecretKey(): string
    {
        return $this->getParameter('wmu_secret_key');
    }

    /**
     * Установить Secret Key WMU кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmuSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmu_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMU кошелька
     *
     * @return string
     */
    public function getWmuX20SecretKey(): string
    {
        return $this->getParameter('wmu_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMU кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmuX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmu_x20_secret_key', $value);
    }

    /**
     * Получить WMB кошелек
     *
     * @return string
     */
    public function getWmbPurse(): string
    {
        return $this->getParameter('wmb_purse');
    }

    /**
     * Установить WMB кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmbPurse(string $value): Gateway
    {
        return $this->setParameter('wmb_purse', $value);
    }

    /**
     * Получить Secret Key WMB кошелька
     *
     * @return string
     */
    public function getWmbSecretKey(): string
    {
        return $this->getParameter('wmb_secret_key');
    }

    /**
     * Установить Secret Key WMB кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmbSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmb_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMB кошелька
     *
     * @return string
     */
    public function getWmbX20SecretKey(): string
    {
        return $this->getParameter('wmb_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMB кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmbX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmb_x20_secret_key', $value);
    }

    /**
     * Получить WMY кошелек
     *
     * @return string
     */
    public function getWmyPurse(): string
    {
        return $this->getParameter('wmy_purse');
    }

    /**
     * Установить WMY кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmyPurse(string $value): Gateway
    {
        return $this->setParameter('wmy_purse', $value);
    }

    /**
     * Получить Secret Key WMY кошелька
     *
     * @return string
     */
    public function getWmySecretKey(): string
    {
        return $this->getParameter('wmy_secret_key');
    }

    /**
     * Установить Secret Key WMY кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmySecretKey(string $value): Gateway
    {
        return $this->setParameter('wmy_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMY кошелька
     *
     * @return string
     */
    public function getWmyX20SecretKey(): string
    {
        return $this->getParameter('wmy_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMY кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmyX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmy_x20_secret_key', $value);
    }

    /**
     * Получить WMX кошелек
     *
     * @return string
     */
    public function getWmxPurse(): string
    {
        return $this->getParameter('wmx_purse');
    }

    /**
     * Установить WMX кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmxPurse(string $value): Gateway
    {
        return $this->setParameter('wmx_purse', $value);
    }

    /**
     * Получить Secret Key WMX кошелька
     *
     * @return string
     */
    public function getWmxSecretKey(): string
    {
        return $this->getParameter('wmx_secret_key');
    }

    /**
     * Установить Secret Key WMX кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmxSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmx_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMX кошелька
     *
     * @return string
     */
    public function getWmxX20SecretKey(): string
    {
        return $this->getParameter('wmx_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMX кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmxX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmx_x20_secret_key', $value);
    }

    /**
     * Получить WMK кошелек
     *
     * @return string
     */
    public function getWmkPurse(): string
    {
        return $this->getParameter('wmk_purse');
    }

    /**
     * Установить WMK кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmkPurse(string $value): Gateway
    {
        return $this->setParameter('wmk_purse', $value);
    }

    /**
     * Получить Secret Key WMK кошелька
     *
     * @return string
     */
    public function getWmkSecretKey(): string
    {
        return $this->getParameter('wmk_secret_key');
    }

    /**
     * Установить Secret Key WMK кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmkSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmk_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMK кошелька
     *
     * @return string
     */
    public function getWmkX20SecretKey(): string
    {
        return $this->getParameter('wmk_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMK кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmkX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmk_x20_secret_key', $value);
    }

    /**
     * Получить WML кошелек
     *
     * @return string
     */
    public function getWmlPurse(): string
    {
        return $this->getParameter('wml_purse');
    }

    /**
     * Установить WML кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmlPurse(string $value): Gateway
    {
        return $this->setParameter('wml_purse', $value);
    }

    /**
     * Получить Secret Key WML кошелька
     *
     * @return string
     */
    public function getWmlSecretKey(): string
    {
        return $this->getParameter('wml_secret_key');
    }

    /**
     * Установить Secret Key WML кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmlSecretKey(string $value): Gateway
    {
        return $this->setParameter('wml_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WML кошелька
     *
     * @return string
     */
    public function getWmlX20SecretKey(): string
    {
        return $this->getParameter('wml_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WML кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmlX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wml_x20_secret_key', $value);
    }

    /**
     * Получить WMH кошелек
     *
     * @return string
     */
    public function getWmhPurse(): string
    {
        return $this->getParameter('wmh_purse');
    }

    /**
     * Установить WMH кошелек
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmhPurse(string $value): Gateway
    {
        return $this->setParameter('wmh_purse', $value);
    }

    /**
     * Получить Secret Key WMH кошелька
     *
     * @return string
     */
    public function getWmhSecretKey(): string
    {
        return $this->getParameter('wmh_secret_key');
    }

    /**
     * Установить Secret Key WMH кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmhSecretKey(string $value): Gateway
    {
        return $this->setParameter('wmh_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMH кошелька
     *
     * @return string
     */
    public function getWmhX20SecretKey(): string
    {
        return $this->getParameter('wmh_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMH кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setWmhX20SecretKey(string $value): Gateway
    {
        return $this->setParameter('wmh_x20_secret_key', $value);
    }


    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'wmid' => '',
            'key_pass' => '',
            'wmz_purse' => '',
            'wmr_purse' => '',
            'wme_purse' => '',
            'wmu_purse' => '',
            'wmb_purse' => '',
            'wmy_purse' => '',
            'wmx_purse' => '',
            'wmk_purse' => '',
            'wml_purse' => '',
            'wmh_purse' => '',
            'wmz_secret_key' => '',
            'wmr_secret_key' => '',
            'wme_secret_key' => '',
            'wmu_secret_key' => '',
            'wmb_secret_key' => '',
            'wmy_secret_key' => '',
            'wmx_secret_key' => '',
            'wmk_secret_key' => '',
            'wml_secret_key' => '',
            'wmh_secret_key' => '',
            'wmz_x20_secret_key' => '',
            'wmr_x20_secret_key' => '',
            'wme_x20_secret_key' => '',
            'wmu_x20_secret_key' => '',
            'wmb_x20_secret_key' => '',
            'wmy_x20_secret_key' => '',
            'wmx_x20_secret_key' => '',
            'wmk_x20_secret_key' => '',
            'wml_x20_secret_key' => '',
            'wmh_x20_secret_key' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
