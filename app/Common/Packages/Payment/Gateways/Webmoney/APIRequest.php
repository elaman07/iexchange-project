<?php

namespace App\Common\Packages\Payment\Gateways\Webmoney;


use baibaratsky\WebMoney\Api\X\X2\Request as X2Request;
use baibaratsky\WebMoney\Api\X\X9\Request as X9Request;
use baibaratsky\WebMoney\Api\X\X8\Request as X8Request;
use baibaratsky\WebMoney\Exception\CoreException;
use baibaratsky\WebMoney\Request\Requester\CurlRequester;
use baibaratsky\WebMoney\Signer;
use baibaratsky\WebMoney\WebMoney;
use Exception;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Экземпляр GuzzleHttp
     *
     * @var WebMoney
     */
    protected WebMoney $webmoney;

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
        $this->webmoney = new WebMoney(new CurlRequester);

    }

    /**
     * X8
     *
     * @param string $purse
     * @return string
     * @throws CoreException
     * @throws Exception
     */
    public function x8(string $purse)
    {


        $request = new X8Request;
        $request->setSignerWmid($this->parameters['wmid']);
        $request->setPurse($purse);
        $request->sign($this->sign());

        if ($request->validate()) {
            $response = $this->webmoney->request($request);

            if ($response->getReturnCode() === 1) {
                return (string)$response->getWmid();
            } else {
                throw new Exception( $response->getReturnDescription());
            }
        }
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $request = new X9Request;
        $request->setSignerWmid($this->parameters['wmid']);
        $request->setRequestedWmid($this->parameters['wmid']);
        $request->sign($this->sign());

        $v_type = Str::upper($currency);
        $v_type = str_replace(['WMZ', 'USD'],'Z', $v_type);
        $v_type = str_replace(['RUR', 'WMR', 'RUB'],'R', $v_type);
        $v_type = str_replace(['WME', 'EUR'],'E', $v_type);
        $v_type = str_replace(['WMU', 'UAH'],'U', $v_type);
        $v_type = str_replace(['WMB', 'BYR'],'B', $v_type);
        $v_type = str_replace(['WMY', 'UZS'],'Y', $v_type);
        $v_type = str_replace(['WMG', 'GLD'],'G', $v_type);
        $v_type = str_replace(['WMX', 'BTC'],'X', $v_type);
        $v_type = str_replace(['WMK', 'KZT'],'K', $v_type);
        $v_type = str_replace(['WML', 'LTC'],'L', $v_type);
        $v_type = str_replace(['WMH', 'BCH'],'H', $v_type);

        $enable = ['Z', 'R', 'P', 'E', 'U', 'B', 'Y', 'G', 'X', 'K', 'L', 'H'];
        if(!in_array($v_type, $enable)){
            throw new \Exception('Wrong currency code');
        }

        if ($request->validate()) {
            $response = $this->webmoney->request($request);
            if ($response->getReturnCode() === 0) {
                return $response->getPurseByName($this->parameters[sprintf('wm%s_purse', Str::lower($v_type))])->getAmount();
            }
        }

        throw new \Exception('Ошибка при получении баланса');
    }

    /**
     * Отправка средств на счет клиента
     * @param string $to
     * @param string $currency
     * @param float $amount
     * @param int $order_id
     * @param string|null $comment
     * @return mixed
     * @throws CoreException
     * @throws Exception
     */
    public function transfer(string $to, string $currency, float $amount, int $order_id = 0, string $comment = null)
    {
        $v_type = Str::upper($currency);
        $v_type = str_replace(['WMZ', 'USD'],'Z', $v_type);
        $v_type = str_replace(['RUR', 'WMR', 'RUB'],'R', $v_type);
        $v_type = str_replace(['WME', 'EUR'],'E', $v_type);
        $v_type = str_replace(['WMU', 'UAH'],'U', $v_type);
        $v_type = str_replace(['WMB', 'BYR'],'B', $v_type);
        $v_type = str_replace(['WMY', 'UZS'],'Y', $v_type);
        $v_type = str_replace(['WMG', 'GLD'],'G', $v_type);
        $v_type = str_replace(['WMX', 'BTC'],'X', $v_type);
        $v_type = str_replace(['WMK', 'KZT'],'K', $v_type);
        $v_type = str_replace(['WML', 'LTC'],'L', $v_type);
        $v_type = str_replace(['WMH', 'BCH'],'H', $v_type);

        $enable = ['Z', 'R', 'P', 'E', 'U', 'B', 'Y', 'G', 'X', 'K', 'L', 'H'];
        if(!in_array($v_type, $enable)){
            throw new \Exception('Wrong currency code');
        }

        $request = new X2Request;
        $request->setSignerWmid($this->parameters['wmid']);
        $request->setTransactionExternalId($order_id);
        $request->setPayerPurse($this->parameters[sprintf('wm%s_purse', Str::lower($v_type))]);
        $request->setPayeePurse($to);
        $request->setAmount($amount);
        if(!is_null($comment)) {
            $request->setDescription($comment);
        }

        $request->sign($this->sign());

        if ($request->validate()) {
            $response = $this->webmoney->request($request);

            if ($response->getReturnCode() === 0) {
                return $response->getTransactionId();
            } else {
                throw new Exception('Payment error: ' . $response->getReturnDescription());
            }
        }

        throw new Exception('Request errors: ' . json_encode($request->getErrors()));
    }

    /**
     * Подпись транзакции
     *
     * @throws Exception
     */
    private function sign(): Signer
    {
        return new Signer($this->parameters['wmid'], storage_path($this->parameters['wmid'].'.kwm'), $this->parameters['key_pass']);
    }
}
