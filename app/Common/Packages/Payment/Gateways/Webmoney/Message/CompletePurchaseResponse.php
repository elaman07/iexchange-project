<?php


namespace App\Common\Packages\Payment\Gateways\Webmoney\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;
use Illuminate\Support\Str;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        if(isset($this->data['LMI_PREREQUEST'])) {
            die('YES');
        }

        if ($this->getHash() !== $this->calculateHash()) {
            throw new InvalidResponseException('Invalid hash');
        }

        if($this->getTestMode() == '1') {
            throw new InvalidResponseException('Enable Test Mode');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     * @throws InvalidResponseException
     */
    public function isSuccessful()
    {
        return $this->getHash() == $this->calculateHash();
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     * @throws InvalidResponseException
     */
    public function isCancelled(): bool
    {
        return $this->getHash() != $this->calculateHash();
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['LMI_SYS_TRANS_NO'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['LMI_PAYMENT_NO'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['LMI_PAYMENT_AMOUNT'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->request->getCurrencyByPurse($this->data['LMI_PAYEE_PURSE']);
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['m_sign'];
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return $this->data['LMI_PAYEE_PURSE'];
    }

    public function getHash()
    {
        return $this->data['LMI_HASH'];
    }

    public function getHashType()
    {
        switch (strlen($this->getHash())) {
            case 32:
                return 'md5';
            case 64:
                return 'sha256';
            case 132:
                return 'sign';
            default:
                return null;
        }
    }


    public function getTestMode()
    {
        return (bool) $this->data['LMI_MODE'];
    }

    /**
     * Calculate hash to verify transaction details.
     *
     * The control signature lets the merchant verify the source of data and the integrity of data transferred to the
     * Result URL in the 'Payment notification form '. The control signature is generating by 'sticking' together
     * values of parameters transmitted in the 'Payment notification form' in the following order: Merchant's purse
     * (LMI_PAYEE_PURSE); Amount (LMI_PAYMENT_AMOUNT); Purchase number (LMI_PAYMENT_NO); Test mode flag (LMI_MODE);
     * Account number in WebMoney Transfer (LMI_SYS_INVS_NO); Payment number in WebMoney Transfer (LMI_SYS_TRANS_NO);
     * Date and time of payment (LMI_SYS_TRANS_DATE); Secret Key (LMI_SECRET_KEY); Customer's purse (LMI_PAYER_PURSE);
     * Customer's WM id (LMI_PAYER_WM).
     *
     * @return string
     *
     * @throws InvalidResponseException
     */
    private function calculateHash()
    {
        $hashType = $this->getHashType();

        if ($hashType == 'sign') {
            throw new InvalidResponseException('Control sign forming method "SIGN" is not supported');
        } elseif ($hashType == null) {
            throw new InvalidResponseException('Invalid signature type');
        }

        return strtoupper(hash(
            $hashType,
            $this->data['LMI_PAYEE_PURSE'].
            $this->data['LMI_PAYMENT_AMOUNT'].
            $this->data['LMI_PAYMENT_NO'].
            $this->data['LMI_MODE'].
            $this->data['LMI_SYS_INVS_NO'].
            $this->data['LMI_SYS_TRANS_NO'].
            $this->data['LMI_SYS_TRANS_DATE'].
            $this->request->getSecretKey($this->getCurrency()).
            $this->data['LMI_PAYER_PURSE'].
            $this->data['LMI_PAYER_WM']
        ));
    }
}
