<?php

namespace App\Common\Packages\Payment\Gateways\Webmoney\Message;

use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate( 'transactionId', 'description', 'currency', 'amount');

        $response = [
            'LMI_PAYEE_PURSE'         => $this->getPurseName($this->getCurrency()),
            'LMI_PAYMENT_AMOUNT'      => $this->getAmount(),
            'LMI_PAYMENT_NO'          => $this->getTransactionId(),
            'LMI_PAYMENT_DESC_BASE64' => base64_encode($this->getDescription()),
            'LMI_SIM_MODE'            => $this->getTestMode() ? '2' : '0',
            'LMI_RESULT_URL'          => $this->getNotifyUrl(),
            'LMI_SUCCESS_URL'         => $this->getReturnUrl(),
            'LMI_SUCCESS_METHOD'      => $this->getReturnMethod(),
            'LMI_FAIL_URL'            => $this->getCancelUrl(),
            'LMI_FAIL_METHOD'         => $this->getCancelMethod(),
        ];

        $hashStr = $response['LMI_PAYEE_PURSE'].';'.$response['LMI_PAYMENT_AMOUNT'].';'.$response['LMI_PAYMENT_NO'].';'.$this->getSecretKeyX20($this->getCurrency()).';';
        $response['LMI_PAYMENTFORM_SIGN'] = hash('sha256', $hashStr);

        return $response;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
