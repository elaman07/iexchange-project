<?php

namespace App\Common\Packages\Payment\Gateways\Webmoney\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use Illuminate\Support\Str;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Redirect method conversion table.
     */
    private static array $methodsTable = [
        '1'    => '1',
        '2'    => '2',
        'GET'  => '0',
        'POST' => '1',
        'LINK' => '2',
    ];


    /**
     * Получить WMZ кошелек
     *
     * @return string
     */
    public function getWmzPurse(): string
    {
        return $this->getParameter('wmz_purse');
    }

    /**
     * Установить WMZ кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmzPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmz_purse', $value);
    }

    /**
     * Получить Secret Key WMZ кошелька
     *
     * @return string
     */
    public function getWmzSecretKey(): string
    {
        return $this->getParameter('wmz_secret_key');
    }

    /**
     * Установить Secret Key WMZ кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmzSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmz_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMZ кошелька
     *
     * @return string
     */
    public function getWmzX20SecretKey(): string
    {
        return $this->getParameter('wmz_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMZ кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmzX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmz_x20_secret_key', $value);
    }

    /**
     * Получить WMR кошелек
     *
     * @return string
     */
    public function getWmrPurse(): string
    {
        return $this->getParameter('wmr_purse');
    }

    /**
     * Установить WMR кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmrPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmr_purse', $value);
    }

    /**
     * Получить Secret Key WMR кошелька
     *
     * @return string
     */
    public function getWmrSecretKey(): string
    {
        return $this->getParameter('wmr_secret_key');
    }

    /**
     * Установить Secret Key WMR кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmrSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmr_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMR кошелька
     *
     * @return string
     */
    public function getWmrX20SecretKey(): string
    {
        return $this->getParameter('wmr_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMR кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmrX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmr_x20_secret_key', $value);
    }

    /**
     * Получить WME кошелек
     *
     * @return string
     */
    public function getWmePurse(): string
    {
        return $this->getParameter('wme_purse');
    }

    /**
     * Установить WME кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmePurse(string $value): AbstractRequest
    {
        return $this->setParameter('wme_purse', $value);
    }

    /**
     * Получить Secret Key WME кошелька
     *
     * @return string
     */
    public function getWmeSecretKey(): string
    {
        return $this->getParameter('wme_secret_key');
    }

    /**
     * Установить Secret Key WME кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmeSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wme_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WME кошелька
     *
     * @return string
     */
    public function getWmeX20SecretKey(): string
    {
        return $this->getParameter('wme_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WME кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmeX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wme_x20_secret_key', $value);
    }

    /**
     * Получить WMU кошелек
     *
     * @return string
     */
    public function getWmuPurse(): string
    {
        return $this->getParameter('wmu_purse');
    }

    /**
     * Установить WMU кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmuPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmu_purse', $value);
    }

    /**
     * Получить Secret Key WMU кошелька
     *
     * @return string
     */
    public function getWmuSecretKey(): string
    {
        return $this->getParameter('wmu_secret_key');
    }

    /**
     * Установить Secret Key WMU кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmuSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmu_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMU кошелька
     *
     * @return string
     */
    public function getWmuX20SecretKey(): string
    {
        return $this->getParameter('wmu_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMU кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmuX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmu_x20_secret_key', $value);
    }

    /**
     * Получить WMB кошелек
     *
     * @return string
     */
    public function getWmbPurse(): string
    {
        return $this->getParameter('wmb_purse');
    }

    /**
     * Установить WMB кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmbPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmb_purse', $value);
    }

    /**
     * Получить Secret Key WMB кошелька
     *
     * @return string
     */
    public function getWmbSecretKey(): string
    {
        return $this->getParameter('wmb_secret_key');
    }

    /**
     * Установить Secret Key WMB кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmbSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmb_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMB кошелька
     *
     * @return string
     */
    public function getWmbX20SecretKey(): string
    {
        return $this->getParameter('wmb_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMB кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmbX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmb_x20_secret_key', $value);
    }

    /**
     * Получить WMY кошелек
     *
     * @return string
     */
    public function getWmyPurse(): string
    {
        return $this->getParameter('wmy_purse');
    }

    /**
     * Установить WMY кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmyPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmy_purse', $value);
    }

    /**
     * Получить Secret Key WMY кошелька
     *
     * @return string
     */
    public function getWmySecretKey(): string
    {
        return $this->getParameter('wmy_secret_key');
    }

    /**
     * Установить Secret Key WMY кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmySecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmy_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMY кошелька
     *
     * @return string
     */
    public function getWmyX20SecretKey(): string
    {
        return $this->getParameter('wmy_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMY кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmyX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmy_x20_secret_key', $value);
    }

    /**
     * Получить WMX кошелек
     *
     * @return string
     */
    public function getWmxPurse(): string
    {
        return $this->getParameter('wmx_purse');
    }

    /**
     * Установить WMX кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmxPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmx_purse', $value);
    }

    /**
     * Получить Secret Key WMX кошелька
     *
     * @return string
     */
    public function getWmxSecretKey(): string
    {
        return $this->getParameter('wmx_secret_key');
    }

    /**
     * Установить Secret Key WMX кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmxSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmx_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMX кошелька
     *
     * @return string
     */
    public function getWmxX20SecretKey(): string
    {
        return $this->getParameter('wmx_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMX кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmxX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmx_x20_secret_key', $value);
    }

    /**
     * Получить WMK кошелек
     *
     * @return string
     */
    public function getWmkPurse(): string
    {
        return $this->getParameter('wmk_purse');
    }

    /**
     * Установить WMK кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmkPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmk_purse', $value);
    }

    /**
     * Получить Secret Key WMK кошелька
     *
     * @return string
     */
    public function getWmkSecretKey(): string
    {
        return $this->getParameter('wmk_secret_key');
    }

    /**
     * Установить Secret Key WMK кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmkSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmk_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMK кошелька
     *
     * @return string
     */
    public function getWmkX20SecretKey(): string
    {
        return $this->getParameter('wmk_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMK кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmkX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmk_x20_secret_key', $value);
    }

    /**
     * Получить WML кошелек
     *
     * @return string
     */
    public function getWmlPurse(): string
    {
        return $this->getParameter('wml_purse');
    }

    /**
     * Установить WML кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmlPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wml_purse', $value);
    }

    /**
     * Получить Secret Key WML кошелька
     *
     * @return string
     */
    public function getWmlSecretKey(): string
    {
        return $this->getParameter('wml_secret_key');
    }

    /**
     * Установить Secret Key WML кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmlSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wml_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WML кошелька
     *
     * @return string
     */
    public function getWmlX20SecretKey(): string
    {
        return $this->getParameter('wml_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WML кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmlX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wml_x20_secret_key', $value);
    }

    /**
     * Получить WMH кошелек
     *
     * @return string
     */
    public function getWmhPurse(): string
    {
        return $this->getParameter('wmh_purse');
    }

    /**
     * Установить WMH кошелек
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmhPurse(string $value): AbstractRequest
    {
        return $this->setParameter('wmh_purse', $value);
    }

    /**
     * Получить Secret Key WMH кошелька
     *
     * @return string
     */
    public function getWmhSecretKey(): string
    {
        return $this->getParameter('wmh_secret_key');
    }

    /**
     * Установить Secret Key WMH кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmhSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmh_secret_key', $value);
    }

    /**
     * Получить Secret Key X20 WMH кошелька
     *
     * @return string
     */
    public function getWmhX20SecretKey(): string
    {
        return $this->getParameter('wmh_x20_secret_key');
    }

    /**
     * Установить Secret Key X20 WMH кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setWmhX20SecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('wmh_x20_secret_key', $value);
    }


    public function getPurseName(string $currency)
    {
        return $this->parameters->get(sprintf('wm%s_purse', $this->getFormatedCurrency($currency)));
    }

    public function getSecretKeyX20(string $currency)
    {
        return $this->parameters->get(sprintf('wm%s_x20_secret_key', $this->getFormatedCurrency($currency)));
    }

    public function getSecretKey(string $currency)
    {
        return $this->parameters->get(sprintf('wm%s_secret_key', $this->getFormatedCurrency($currency)));
    }


    /**
     * Detect currency by purse.
     *
     * @param string $purse
     *
     * @return null|string
     */
    public function getCurrencyByPurse(string $purse): ?string
    {
        switch (substr($purse, 0, 1)) {
            case 'Z':
                return 'USD';
            case 'R':
                return 'RUB';
            case 'E':
                return 'EUR';
            case 'U':
                return 'UAH';
            case 'B':
                return 'BYR';
            case 'Y':
                return 'UZS';
            case 'G':
                return 'GLD';
            case 'X':
                return 'BTC';
            case 'K':
                return 'KZT';
            case 'L':
                return 'LTC';
            case 'H':
                return 'BCH';
            default:
                return null;
        }
    }


    protected function getFormatedCurrency(string $currency)
    {
        $v_type = Str::upper($currency);
        $v_type = str_replace(['WMZ', 'USD'],'Z', $v_type);
        $v_type = str_replace(['RUR', 'WMR', 'RUB'],'R', $v_type);
        $v_type = str_replace(['WME', 'EUR'],'E', $v_type);
        $v_type = str_replace(['WMU', 'UAH'],'U', $v_type);
        $v_type = str_replace(['WMB', 'BYR'],'B', $v_type);
        $v_type = str_replace(['WMY', 'UZS'],'Y', $v_type);
        $v_type = str_replace(['WMG', 'GLD'],'G', $v_type);
        $v_type = str_replace(['WMX', 'BTC'],'X', $v_type);
        $v_type = str_replace(['WMK', 'KZT'],'K', $v_type);
        $v_type = str_replace(['WML', 'LTC'],'L', $v_type);
        $v_type = str_replace(['WMH', 'BCH'],'H', $v_type);

        $enable = ['Z', 'R', 'P', 'E', 'U', 'B', 'Y', 'G', 'X', 'K', 'L', 'H'];
        if(!in_array($v_type, $enable)){
            throw new \Exception('Wrong currency code');
        }

        return Str::lower($v_type);
    }

    /**
     * Converts redirect method to WebMoney code: 0, 1 or 2.
     *
     * @param string $method
     *
     * @return string
     */
    public function formatMethod(string $method): string
    {
        $method = strtoupper((string) $method);
        return self::$methodsTable[$method] ?? '0';
    }

    /**
     * Get the return method.
     *
     * @return string return method
     */
    public function getReturnMethod(): string
    {
        return $this->formatMethod('GET');
    }

    /**
     * Get the cancel method.
     *
     * @return string cancel method
     */
    public function getCancelMethod(): string
    {
        return $this->formatMethod('GET');
    }
}
