<?php

namespace App\Common\Packages\Payment\Gateways\LiqPay\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Public key
     *
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Установить Public key
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPublicKey(string $value): AbstractRequest
    {
        return $this->setParameter('public_key', $value);
    }

    /**
     * Получить Private key
     *
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установить Private key
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPrivateKey(string $value): AbstractRequest
    {
        return $this->setParameter('private_key', $value);
    }
}
