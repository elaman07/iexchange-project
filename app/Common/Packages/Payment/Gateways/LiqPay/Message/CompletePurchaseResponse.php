<?php


namespace App\Common\Packages\Payment\Gateways\LiqPay\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * Декодированные данные
     */
    protected $decodeData = [];

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }

        $this->decodeData = json_decode(base64_decode($this->data['data']), true);
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->getSign() == $this->signatureResponse()
            and $this->decodeData['status'] == 'success'
            and $this->decodeData['type'] == 'buy'
            and $this->decodeData['action'] == 'pay';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        //
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->decodeData['transaction_id'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->decodeData['order_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->decodeData['amount'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->decodeData['currency'];
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['signature'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        return base64_encode( hash('sha1',
            $this->request->getPrivateKey() . $this->data['data'] . $this->request->getPrivateKey(),true ));
    }
}
