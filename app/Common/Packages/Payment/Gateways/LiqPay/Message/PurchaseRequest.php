<?php

namespace App\Common\Packages\Payment\Gateways\LiqPay\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Str;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate('transactionId', 'currency', 'amount');

        return [
            'version'     => '3',
            'public_key'  => $this->getPublicKey(),
            'action'      => 'pay',
            'amount'      => $this->getAmount(),
            'currency'    => Str::upper($this->getCurrency()),
            'description' => $this->getDescription(),
            'order_id'    => $this->getTransactionId(),
            'language'    => 'ru',
            'result_url'  => $this->getReturnUrl(),
            'server_url'  => $this->getNotifyUrl()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $data = base64_encode(json_encode($data));
        $sign_string = ($this->getPrivateKey() . $data. $this->getPrivateKey());
        $signature = base64_encode(hash('sha1', $sign_string, true));

        return $this->response = new PurchaseResponse($this, [
            'data' => $data,
            'signature' => $signature
        ]);
    }
}
