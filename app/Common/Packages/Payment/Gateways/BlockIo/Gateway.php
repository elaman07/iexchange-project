<?php

namespace App\Common\Packages\Payment\Gateways\BlockIo;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\BlockIo\Message\AddressRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'BlockIo';


    /**
     * Получить Secret PIN
     *
     * @return string
     */
    public function getSecretPin() : string
    {
        return $this->getParameter('secret_pin');
    }

    /**
     * Установка Secret PIN
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretPin(string $value): Gateway
    {
        return $this->setParameter('secret_pin', $value);
    }

    /**
     * Получить API Key для Bitcoin
     *
     * @return string
     */
    public function getApiKeyBtc() : string
    {
        return $this->getParameter('api_key_btc');
    }

    /**
     * Установка API Key для Bitcoin
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiKeyBtc(string $value): Gateway
    {
        return $this->setParameter('api_key_btc', $value);
    }

    /**
     * Получить API Key для Litecoin
     *
     * @return string
     */
    public function getApiKeyLtc() : string
    {
        return $this->getParameter('api_key_ltc');
    }

    /**
     * Установка API Key для Litecoin
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiKeyLtc(string $value): Gateway
    {
        return $this->setParameter('api_key_ltc', $value);
    }

    /**
     * Получить API Key для Doge
     *
     * @return string
     */
    public function getApiKeyDoge() : string
    {
        return $this->getParameter('api_key_doge');
    }

    /**
     * Установка API Key для Doge
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiKeyDoge(string $value): Gateway
    {
        return $this->setParameter('api_key_doge', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'secret_pin' => '',
            'api_key_btc' => '',
            'api_key_ltc' => '',
            'api_key_doge' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
