<?php

namespace App\Common\Packages\Payment\Gateways\BlockIo;


use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * URL BlockIO API
     *
     * @var string
     */
    const API_URL = 'https://block.io';

    /**
     * Конструктор
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получить баланс
     *
     * @param string $currency
     * @return array|bool
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = $this->request('/get_balance', $currency);

        if($response['status'] == 'success') {
            return $response['data']['available_balance'];
        }

        return 0;
    }

    /**
     * Отправляем средства клиенту на счет
     *
     * @param $currency
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    public function transfer($currency, array $options = [], $fee = '', $is_mass = 0)
    {
        $data = [];

        if(mb_strtoupper($currency) == 'BTC') {
            $data['api_key'] = $this->parameters['api_key_btc'];
        } elseif(mb_strtoupper($currency) == 'LTC') {
            $data['api_key'] = $this->parameters['api_key_ltc'];
        } elseif(mb_strtoupper($currency) == 'DOGE') {
            $data['api_key'] = $this->parameters['api_key_doge'];
        }


        $blockIo = new \BlockIo\Client($data['api_key'], $this->parameters['secret_pin'], 2);

        $params = [
            'amount' => $options['amount'],
            'to_address' => $options['address'],
        ];

        if(!empty($fee)) {
            $params['priority'] = $fee;
        }

        $prepare_transaction_response  = $blockIo->prepare_transaction($params);
        $create_and_sign_transaction_response = $blockIo->create_and_sign_transaction($prepare_transaction_response);
        $submit_transaction_response = $blockIo->submit_transaction(['transaction_data' => $create_and_sign_transaction_response]);


        if($submit_transaction_response->status == 'success')
            return (array)$submit_transaction_response->data;
        throw new \Exception(json_encode($submit_transaction_response));
    }

    /**
     * Массовые выплаты
     *
     * @return mixed
     * @throws Exception
     */
    public function mass_transfer_btc(array $options = [], $fee = ''): array
    {
        $data = [];
        $data['api_key'] = $this->parameters['api_key_btc'];


        $blockIo = new BlockIo($data['api_key'], $this->parameters['secret_pin']);

        $params = [
            'amounts' => $options['amount'],
            'to_address' => $options['address'],
        ];

        if(!empty($fee)) {
            $params['priority'] = $fee;
        }

        $prepare_transaction_response  = $blockIo->prepare_transaction($params);
        $create_and_sign_transaction_response = $blockIo->create_and_sign_transaction($prepare_transaction_response);
        $submit_transaction_response = $blockIo->submit_transaction(['transaction_data' => $create_and_sign_transaction_response]);


        if($submit_transaction_response->status == 'success')
            return (array)$submit_transaction_response->data;
        throw new \Exception(json_encode($submit_transaction_response));
    }


    /**
     * Получение мин. кол-во подтверждений
     *
     * @param string $currency
     * @return int|mixed
     */
    public function getMinConfirm(string $currency)
    {
        $confirm = 5;
        if(mb_strtoupper($currency) == 'BTC') {
            $confirm = $this->parameters['confirm_btc'];
        } elseif(mb_strtoupper($currency) == 'LTC') {
            $confirm = $this->parameters['confirm_ltc'];
        } elseif(mb_strtoupper($currency) == 'DOGE') {
            $confirm = $this->parameters['confirm_doge'];
        }

        return $confirm;
    }

    /**
     * Детали транзакции
     *
     * @param $currency
     * @param string $address
     * @return array|mixed
     * @throws Exception
     */
    public function findTransaction($currency, string $address)
    {
        $response = $this->request('get_transactions', $currency, [
            'type' => 'received',
            'labels' => $address
        ]);

        if($response['status'] == 'success') {
            return Arr::first($response['data']['txs']);
        }

        return [];
    }

    /**
     * Запросы BlockIo
     *
     * @param string $method
     * @param $currency
     * @param array $params
     * @return array|bool
     * @throws Exception
     */
    public function request(string $method, $currency, array $params = [])
    {
        $data = [];
        $data['api_key'] = $this->parameters['api_key_'.mb_strtolower($currency)];
        $response = Http::baseUrl(self::API_URL)->get(
            sprintf('/api/v2/%s', $method),
            array_merge($data, $params)
        );

        $content = $response->json();

        if($content['status'] == 'fail') {
            throw new Exception('BlockIO:'. json_encode($content['data']));
        }

        return $content;
    }
}
