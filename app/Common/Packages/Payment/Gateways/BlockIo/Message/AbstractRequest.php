<?php

namespace App\Common\Packages\Payment\Gateways\BlockIo\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Secret PIN
     *
     * @return string
     */
    public function getSecretPin() : string
    {
        return $this->getParameter('secret_pin');
    }

    /**
     * Установка Secret PIN
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretPin(string $value): AbstractRequest
    {
        return $this->setParameter('secret_pin', $value);
    }

    /**
     * Получить API Key для Bitcoin
     *
     * @return string
     */
    public function getApiKeyBtc() : string
    {
        return $this->getParameter('api_key_btc');
    }

    /**
     * Установка API Key для Bitcoin
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKeyBtc(string $value): AbstractRequest
    {
        return $this->setParameter('api_key_btc', $value);
    }

    /**
     * Получить API Key для Litecoin
     *
     * @return string
     */
    public function getApiKeyLtc() : string
    {
        return $this->getParameter('api_key_ltc');
    }

    /**
     * Установка API Key для Litecoin
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKeyLtc(string $value): AbstractRequest
    {
        return $this->setParameter('api_key_ltc', $value);
    }

    /**
     * Получить API Key для Doge
     *
     * @return string
     */
    public function getApiKeyDoge() : string
    {
        return $this->getParameter('api_key_doge');
    }

    /**
     * Установка API Key для Doge
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKeyDoge(string $value): AbstractRequest
    {
        return $this->setParameter('api_key_doge', $value);
    }
}
