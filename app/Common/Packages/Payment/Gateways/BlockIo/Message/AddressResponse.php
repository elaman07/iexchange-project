<?php


namespace App\Common\Packages\Payment\Gateways\BlockIo\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['status']) && $this->data['status'] == 'success';
    }

    public function isRedirect(): bool
    {
        return false;
    }

    public function getAddress(): string
    {
        if(isset($this->data['data']))
            return $this->data['data']['address'];
    }


    public function getLabel()
    {
        if(isset($this->data['data'])) {
            return $this->data['data']['label'];
        }

        return null;
    }
}
