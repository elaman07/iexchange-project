<?php

namespace App\Common\Packages\Payment\Gateways\BlockIo\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [
            'label'  =>  $this->getTransactionId(),
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $body = http_build_query(array_merge($data, [
            'api_key' => $this->{'getApiKey'.Str::studly($this->getCoinCurrency())}(),
        ]));

        $httpResponse = \Http::baseUrl('https://block.io')->get('api/v2/get_new_address/?'.$body)->json();
        return $this->response = new AddressResponse($this, $httpResponse->getBody()->getContents());
    }
}
