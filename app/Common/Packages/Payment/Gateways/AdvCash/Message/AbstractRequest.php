<?php

namespace App\Common\Packages\Payment\Gateways\AdvCash\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Email адрес владельца счета
     *
     * @return string
     */
    public function getSciAccountEmail() : string
    {
        return $this->getParameter('sci_account_email');
    }

    /**
     * Установка нового значения для E-mail владельца аккаунта
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSciAccountEmail(string $value): AbstractRequest
    {
        return $this->setParameter('sci_account_email', $value);
    }

    /**
     * Название SCI
     *
     * @return string
     */
    public function getSciName(): string
    {
        return $this->getParameter('sci_name');
    }

    /**
     * Установка нового значения для SCI Названия
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSciName(string $value): AbstractRequest
    {
        return $this->setParameter('sci_name', $value);
    }

    /**
     * Пароль SCI
     *
     * @return string
     */
    public function getSCIPassword(): string
    {
        return $this->getParameter('sci_password');
    }

    /**
     * Установка нового значения для SCI пароля
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSciPassword(string $value): AbstractRequest
    {
        return $this->setParameter('sci_password', $value);
    }

    /**
     * Подписать транзакцию
     *
     * @throws InvalidRequestException
     * @return string
     */
    public function signatureTransaction(): string
    {
        return hash('sha256', implode(':', [
            $this->getSciAccountEmail(),
            $this->getSciName(),
            $this->getAmount(),
            $this->getCurrency(),
            $this->getSCIPassword(),
            $this->getTransactionId()
        ]));
    }
}
