<?php

namespace App\Common\Packages\Payment\Gateways\AdvCash\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;


class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl(): string
    {
        return 'https://wallet.advcash.com/sci/';
    }

    public function getRedirectMethod(): string
    {
        return 'POST';
    }

    public function getRedirectData()
    {
        return $this->data;
    }
}

