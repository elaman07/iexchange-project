<?php

namespace App\Common\Packages\Payment\Gateways\AdvCash\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'sci_account_email', 'sci_name', 'amount', 'currency', 'transactionId'
        );

        return [
            'ac_account_email'  =>  $this->getSciAccountEmail(),
            'ac_sci_name'       =>  $this->getSciName(),
            'ac_amount'         =>  $this->getAmount(),
            'ac_currency'       =>  $this->getCurrency(),
            'ac_order_id'       =>  $this->getTransactionId(),
            'ac_sign'           =>  $this->signatureTransaction(),
            'ac_comments'       =>  $this->getDescription(),
            'ac_success_url'    =>  $this->getReturnUrl(),
            'ac_success_url_method' => 'GET',
            'ac_fail_url'       =>  $this->getCancelUrl(),
            'ac_fail_url_method' => 'GET'
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
