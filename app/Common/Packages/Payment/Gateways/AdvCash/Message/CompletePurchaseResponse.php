<?php


namespace App\Common\Packages\Payment\Gateways\AdvCash\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->data['ac_transaction_status'] == 'COMPLETED';
    }

    /**
     * Если транзакция отменена
     *
     * @return boolean
     */
    public function isCancelled(): bool
    {
        return $this->data['ac_transfer'] == 0;
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['ac_transfer'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['ac_order_id'];
    }

    /**
     * Получение суммы
     *
     * @return string | float
     */
    public function getAmount()
    {
        return $this->data['ac_buyer_amount_without_commission'];
    }

    /**
     * Получаем код валюты
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return str_replace('RUR', 'RUB', $this->data['ac_merchant_currency']);
    }

    /**
     * Получаем результат подписи с сервера ADVCash
     *
     * @return string
     */
    public function getSign(): string
    {
        return $this->data['ac_hash'];
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount(): string
    {
        return $this->data['ac_src_wallet'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse(): string
    {
        return hash('sha256', implode(':', [
            $this->data['ac_transfer'],
            $this->data['ac_start_date'],
            $this->data['ac_sci_name'],
            $this->data['ac_src_wallet'],
            $this->data['ac_dest_wallet'],
            $this->data['ac_order_id'],
            $this->data['ac_amount'],
            $this->data['ac_merchant_currency'],
            $this->request->getSCIPassword()
        ]));
    }
}
