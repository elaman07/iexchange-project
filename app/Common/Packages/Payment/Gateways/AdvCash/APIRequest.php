<?php

namespace App\Common\Packages\Payment\Gateways\AdvCash;


use Exception;
use SoapFault;

class APIRequest
{
    private const WSDL_URL = 'https://wallet.advcash.com/wsm/merchantWebService?wsdl';

    /**
     * Конфигрурационные параметры
     *
     * @var array
     */
    protected array $parameters = [];

    public array $soapOptions = [
        'location' => 'https://wallet.advcash.com/wsm/merchantWebService'
    ];


    /**
     * Экземепляр клиента "SoapClient"
     *
     * @var \SoapClient
     */
    protected \SoapClient $soapClient;


    /**
     * Создаем констркутор AdvCASH
     *
     * @param array $parameters
     * @throws SoapFault
     */
    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;

        try {
            $this->soapClient = new \SoapClient(static::WSDL_URL, $this->soapOptions);
        } catch (SoapFault $e) {
            throw $e;
        }
    }

    /**
     * Валидация внутрисистемного перевода.
     *
     * @param $amount
     * @param $currency
     * @param $email
     * @param $walletId
     * @param null $note
     * @param bool|false $savePaymentTemplate
     *
     *
     * @return mixed
     * @throws SoapFault
     */
    public function validateSendMoney($amount, $currency, $email, $walletId, $note = null, bool $savePaymentTemplate = false)
    {
        // Заменить RUB на RUR
        $currency = str_replace('RUB', 'RUR', $currency);
        return $this->call('validationSendMoney', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'email' => $email,
            'walletId' => $walletId,
            'note' => $note,
            'savePaymentTemplate' => $savePaymentTemplate
        ]);
    }

    /**
     * Валидация перевода средств на карту Advanced Cash
     *
     * @param $amount
     * @param $currency
     * @param $email
     * @param $cardType
     * @param null $note
     * @param bool $savePaymentTemplate
     * @return mixed
     * @throws SoapFault
     */
    public function validateSendMoneyToAdvCashCard($amount, $currency, $email, $cardType, $note = null, bool $savePaymentTemplate = false)
    {
        return $this->call('validationSendMoneyToAdvcashCard', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'email' => $email,
            'cardType' => $cardType,
            'note' => $note,
            'savePaymentTemplate' => $savePaymentTemplate
        ]);
    }

    /**
     * Валидация вывода средств на внешнюю карту, не привязанную к системе.
     *
     * @param $amount
     * @param $currency
     * @param $cardNumber
     * @param $expiryMonth
     * @param $expiryYear
     * @param null $note
     * @param bool $savePaymentTemplate
     *
     * @return mixed
     * @throws SoapFault
     */
    public function validateSendMoneyToBankCard($amount, $currency, $cardNumber, $expiryMonth, $expiryYear, $note = null, bool $savePaymentTemplate = false)
    {
        return $this->call('validationSendMoneyToBankCard', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'cardNumber' => $cardNumber,
            'expiryMonth' => $expiryMonth,
            'expiryYear' => $expiryYear,
            'note' => $note,
            'savePaymentTemplate' => $savePaymentTemplate
        ]);
    }

    /**
     * Валидация перевода средств незарегистрированному пользователю по e-mail.
     *
     * @param $amount
     * @param $currency
     * @param $email
     * @param null $note
     *
     * @return mixed
     * @throws SoapFault
     */
    public function validateSendMoneyToEmail($amount, $currency, $email, $note = null)
    {
        return $this->call('validationSendMoneyToEmail', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'email' => $email,
            'note' => $note
        ]);
    }

    /**
     * Внутрисистемный платеж.
     *
     * @param $amount
     * @param $currency
     * @param $email
     * @param $walletId
     * @param null $note
     * @param bool|false $savePaymentTemplate
     *
     * @return mixed
     * @throws SoapFault
     */
    public function sendMoney($amount, $currency, $email, $walletId, $note = null, bool $savePaymentTemplate = false)
    {
        // Заменить RUB на RUR
        $currency = str_replace('RUB', 'RUR', $currency);

        return $this->call('sendMoney', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'email' => $email,
            'walletId' => $walletId,
            'note' => $note,
            'savePaymentTemplate' => $savePaymentTemplate,
        ]);
    }

    /**
     * Перевод средств на карту Advanced Cash.
     *
     * @param $amount
     * @param $currency
     * @param $email
     * @param string $cardType
     * @param null $note
     *
     * @return mixed
     * @throws SoapFault
     */
    public function sendMoneyToAdvCashCard($amount, $currency, $email, string $cardType = 'PLASTIC', $note = null)
    {
        // Заменить RUB на RUR
        $currency = str_replace('RUB', 'RUR', $currency);

        return $this->call('sendMoneyToAdvcashCard', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'email' => $email,
            'cardType' => $cardType,
            'note' => $note
        ]);
    }


    /**
     * Вывод средств на внешнюю банковскую карту
     *
     * @param $amount
     * @param $currency
     * @param string $cardNumber
     * @param string|null $note
     * @param bool $savePaymentTemplate
     *
     * @return mixed
     * @throws SoapFault
     */
    public function sendMoneyToBankCard($amount, $currency, string $cardNumber, string $note = null, bool $savePaymentTemplate = false)
    {
        // Заменить RUB на RUR
        $currency = str_replace('RUB', 'RUR', $currency);

        return $this->call('sendMoneyToBankCard', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'cardNumber' => $cardNumber,
            'note' => $note,
            'savePaymentTemplate' => $savePaymentTemplate
        ]);
    }

    /**
     * Вывод средств незарегистрированному пользователю по e-mail.
     *
     * @param $amount
     * @param $currency
     * @param $email
     * @param null $note
     *
     * @return mixed
     * @throws SoapFault
     */
    public function sendMoneyToEmail($amount, $currency, $email, $note = null)
    {
        // Заменить RUB на RUR
        $currency = str_replace('RUB', 'RUR', $currency);

        return $this->call('sendMoneyToEmail', [
            'amount' => iex_number_format($amount, 2),
            'currency' => $currency,
            'email' => $email,
            'note' => $note
        ]);
    }

    /**
     * Вывод средств в стороннюю платежную систему
     *
     * @param $amount
     * @param $currency
     * @param $eCurrency
     * @param $receiver
     * @param int $number_format
     * @param null $note
     * @param bool $savePaymentTemplate
     * @return mixed
     * @throws SoapFault
     */
    public function sendMoneyToECurrency($amount, $currency, $eCurrency, $receiver, int $number_format = 2, $note = null, bool $savePaymentTemplate = false)
    {
        return $this->call('sendMoneyToEcurrency', [
            'amount' => iex_number_format($amount,  $number_format),
            'currency' => $currency,
            'ecurrency' => $eCurrency,
            'receiver' => $receiver,
            'note' => $note,
            'savePaymentTemplate' => $savePaymentTemplate
        ]);
    }


    /**
     * История транзакций
     *
     *
     * @param int $count
     * @param int $from
     * @return mixed
     * @throws SoapFault
     */
    public function history(int $count = 10, int $from = 0)
    {
        return $this->call('history', [
            'accountName' => null,
            'startTimeFrom' => null,
            'startTimeTo' => null,
            'transactionName' => null,
            'transactionStatus' => null,
            'updatedFrom' => null,
            'updatedTo' => null,
            'walletId' => null,
            'count'     =>  $count,
            'from'      => $from
        ]);
    }

    /**
     * Поиск транзакции по ID.
     *
     * @param $transactionId
     * @return mixed
     * @throws SoapFault
     */
    public function findTransaction($transactionId)
    {
        return $this->call('findTransaction', $transactionId);
    }

    /**
     * Получение баланса по кошелькам пользователя.
     *
     * @param string|null $currency
     * @return array| float
     * @throws SoapFault
     */
    public function getBalance(string $currency)
    {
        // Список доступных кодов валют, платежной системы
        $codes = [
            'U' => 'USD',  'E' => 'EUR', 'R' => 'RUB', 'G' => 'GBP', 'H' => 'UAH'
        ];

        $return  = [];
        foreach ($this->call('getBalances') as $item)
        {
            $split = mb_substr($item['id'], 0, 1);

            // Если нет кошелька
            if(!isset($codes[$split]))
                continue;
            $return[$codes[$split]] = $item['amount'];
        }

        return $return[$currency];
    }

    /**
     * Вызов определенных действий для "SoapClient"
     *
     * @param $method
     * @param null $params
     *
     * @return mixed
     * @throws SoapFault
     * @throws Exception
     */
    protected function call($method, $params = null)
    {
        try {
            $result = $this->soapClient->{$method}([
                'arg0' => [
                    'apiName' => $this->parameters['api_name'],
                    'authenticationToken' => $this->createAuthToken(),
                    'accountEmail' => !empty($this->parameters['sci_account_email']) ? $this->parameters['sci_account_email'] : $this->parameters['api_account_email']
                ],
                'arg1' => $params
            ]);
        } catch (SoapFault $e) {
            throw $e;
        }

        return $this->processResult($result);
    }

    /**
     * Преобразование stdObject в массив
     *
     * @param $result
     * @return mixed
     */
    protected function processResult($result)
    {
        return json_decode(
            json_encode(
                $this->getValue($result, 'return', [])
            ), true
        );
    }

    /**
     * Создаем токен для авторизации на сервисе AdvCash
     *
     * @return string
     * @throws Exception
     */
    protected function createAuthToken(): string
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));

        return strtoupper(hash('sha256', implode(':', [
            $this->parameters['api_secret'],
            $date->format('Ymd'),
            $date->format('H')
        ])));
    }

    protected function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }
        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }
        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            return $array[$key];
        }
        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }
        if (is_object($array)) {
            return $array->$key;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        }
        return $default;
    }
}
