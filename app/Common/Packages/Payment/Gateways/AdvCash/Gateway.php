<?php

namespace App\Common\Packages\Payment\Gateways\AdvCash;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'AdvCash';

    /**
     * Email адрес владельца счета
     *
     * @return string
    */
    public function getSciAccountEmail() : string
    {
        return $this->getParameter('sci_account_email');
    }

    /**
     * Установка нового значения для E-mail владельца аккаунта
     *
     * @param string $value
     * @return Gateway
     */
    public function setSciAccountEmail(string $value): Gateway
    {
        return $this->setParameter('sci_account_email', $value);
    }

    /**
     * Название SCI
     *
     * @return string
     */
    public function getSCIName(): string
    {
        return $this->getParameter('sci_name');
    }

    /**
     * Установка нового значения для SCI Названия
     *
     * @param string $value
     * @return Gateway
     */
    public function setSciName(string $value): Gateway
    {
        return $this->setParameter('sci_name', $value);
    }

    /**
     * Пароль SCI
     *
     * @return string
    */
    public function getSCIPassword(): string
    {
        return $this->getParameter('sci_password');
    }

    /**
     * Установка нового значения для SCI пароля
     *
     * @param string $value
     * @return Gateway
     */
    public function setSciPassword(string $value): Gateway
    {
        return $this->setParameter('sci_password', $value);
    }

    /**
     *
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiName(string $value): Gateway
    {
        return $this->setParameter('api_name', $value);
    }

    /**
     *
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiAccountEmail(string $value): Gateway
    {
        return $this->setParameter('api_account_email', $value);
    }

    /**
     *
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiSecret(string $value): Gateway
    {
        return $this->setParameter('api_secret', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'sci_account_email' => '',
            'sci_name' => '',
            'sci_password' => '',
            'api_account_email' => '',
            'api_name' => '',
            'api_secret' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
