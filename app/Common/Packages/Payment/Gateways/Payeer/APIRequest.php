<?php

namespace App\Common\Packages\Payment\Gateways\Payeer;


use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * URL Payeer API
     *
     * @var string
     */
    const API_URL = 'https://payeer.com/ajax/api/api.php';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Экземпляр GuzzleHttp
     *
     * @var Client
     */
    protected Client $client;

    /**
     * Создаем констркутор Payeer
     *
     * @param $config
     */
    public function __construct($config)
    {
        $this->parameters = $config;
        $this->client = new Client();
    }

    /**
     * Получение баланса по кошелькам пользователя.
     *
     * @param string $currency
     * @return array | float
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $response = (array)$this->call('balance');
        return $response['balance'][$currency]['BUDGET'];
    }

    /**
     * Информация об операции
     *
     * @param $id
     * @return array
     * @throws Exception
     */
    public function getHistoryInfo($id)
    {
        $response = (array)$this->call([
            'action'    =>  'historyInfo',
            'historyId' =>  $id
        ]);
        return $response['info'];
    }

    /**
     * Перевод средств
     *
     * @param array $options
     * @return array
     * @throws Exception
     */
    public function transfer(array $options = []): array
    {
        $array = [
            'action'    =>  'transfer',
            'sumOut'    =>  iex_number_format($options['amount'], 2),
            'curIn'     =>  mb_strtoupper($options['currency']),
            'curOut'    =>  mb_strtoupper($options['currency']),
            'to'        =>  $options['to']
        ];

        // Если комиссию платит клиент
        if(isset($options['who_commission']) and $options['who_commission'] == 1) {
            unset($array['sumOut']);
            $array['sum'] = iex_number_format($options['amount'], 2);
        }

        if(isset($options['comment']))
            $array['comment'] = $options['comment'];

        $response = (array)$this->call($array);
        return $response;
    }

    /**
     * Информация об операции в магазине
     *
     * @param $order_id
     * @return array
     */
    public function getShopOrderInfo($order_id)
    {
        $response = (array)$this->call([
            'action'    =>  'shopOrderInfo',
            'shopId'    =>  $this->parameters['merchant_id'],
            'orderId'   =>  $order_id
        ]);
        return $response;
    }

    /**
     * Проверка существования аккаунта
     *
     * @param $account
     * @return array
     * @throws Exception
     */
    public function checkUser($account)
    {
        $response = (array)$this->call([
            'action'    =>  'checkUser',
            'user'      =>  $account
        ]);
        return $response;
    }

    /**
     * Вызов определенных действий для "PayeerAPI"
     *
     * @param $action
     * @return string
     * @throws Exception
     */
    protected function call($action)
    {
        // Если полученное значение не является массивом
        if(!is_array($action)) $action = ['action' => $action];

        $data = array_merge([
            'account'  =>  $this->parameters['account_id'],
            'apiId'  =>  $this->parameters['api_id'],
            'apiPass' =>  $this->parameters['api_key']
        ], $action);

        // Отправляем язык
        $data['language'] = app()->getLocale();
        $request = $this->client->post(self::API_URL, [
            'form_params'  =>  $data
        ]);

        $response =  json_decode($request->getBody()->getContents(), true);

        if($response['auth_error'] == 1) {
            throw new Exception(array_first($response['errors']));
        }
        return $response;
    }
}
