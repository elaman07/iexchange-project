<?php

namespace App\Common\Packages\Payment\Gateways\Payeer;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Payeer';

    /**
     * Получить ID мерчанта
     *
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->getParameter('merchant_id');
    }

    /**
     * Установить ID мерчанта
     *
     * @param string $value
     * @return Gateway
     */
    public function setMerchantId(string $value): Gateway
    {
        return $this->setParameter('merchant_id', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Получить Номер кошелька
     *
     * @return string
     */
    public function getAccountId() : string
    {
        return $this->getParameter('account_id');
    }

    /**
     * Установка Номера кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountId(string $value): Gateway
    {
        return $this->setParameter('account_id', $value);
    }

    /**
     * Получить API Id
     *
     * @return string
     */
    public function getApiId() : string
    {
        return $this->getParameter('api_id');
    }

    /**
     * Установка API Id
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiId(string $value): Gateway
    {
        return $this->setParameter('api_id', $value);
    }

    /**
     * Получить API ключ
     *
     * @return string
     */
    public function getApiKey() : string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установка API ключа
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiKey(string $value): Gateway
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'secret_key' => '',
            'merchant_id' => '',
            'account_id' => '',
            'api_id' => '',
            'api_key' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
