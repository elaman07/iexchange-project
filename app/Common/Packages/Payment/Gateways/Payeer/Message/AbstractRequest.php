<?php

namespace App\Common\Packages\Payment\Gateways\Payeer\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить ID мерчанта
     *
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->getParameter('merchant_id');
    }

    /**
     * Установить ID мерчанта
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setMerchantId(string $value): AbstractRequest
    {
        return $this->setParameter('merchant_id', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Подписать транзакцию
     *
     * @throws InvalidRequestException
     * @return string
     */
    public function signatureTx(): string
    {
        return strtoupper(hash('sha256', implode(':', [
            $this->getMerchantId(),
            $this->getTransactionId(),
            $this->getAmount(),
            $this->getCurrency(),
            base64_encode($this->getDescription()),
            $this->getSecretKey(),
        ])));
    }
}
