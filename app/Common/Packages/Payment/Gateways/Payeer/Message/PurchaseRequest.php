<?php

namespace App\Common\Packages\Payment\Gateways\Payeer\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData(): array
    {
        $this->validate('transactionId', 'currency', 'amount', 'description');

        return [
            'm_shop'        =>  $this->getMerchantId(),
            'm_orderid'     =>  $this->getTransactionId(),
            'm_amount'      =>  $this->getAmount(),
            'm_curr'        =>  $this->getCurrency(),
            'm_desc'        =>  base64_encode($this->getDescription()),
            'm_sign'        =>  $this->signatureTx(),
            'success_url'   =>  $this->getReturnUrl(),
            'fail_url'      =>  $this->getCancelUrl()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
