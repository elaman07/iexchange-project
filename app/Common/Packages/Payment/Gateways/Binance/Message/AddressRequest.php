<?php


namespace App\Common\Packages\Payment\Gateways\Binance\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Gateways\Binance\APIRequest;
use Illuminate\Support\Str;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        $binance = new APIRequest($this->getParameters());

        $code_xml = $this->getOrderData()->direction_exchange->currency1->designation_xml;

        $network = null;
        if($code_xml == 'USDTERC20') {
            $network = 'ETH';
        } elseif($code_xml == 'USDTTRC20') {
            $network = 'TRX';
        } elseif($code_xml == 'USDTOMNI') {
            $network = 'OMNI';
        } elseif($code_xml == 'USDTBEP20') {
            $network = 'BNB';
        }

        $httpresponse = $binance->depositAddress($this->getCoinCurrency(), $network);
        return $this->response = new AddressResponse($this, $httpresponse);
    }
}
