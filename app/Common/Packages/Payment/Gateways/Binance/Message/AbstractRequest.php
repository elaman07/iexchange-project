<?php

namespace App\Common\Packages\Payment\Gateways\Binance\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{

    /**
     * Получить Ключ API
     *
     * @return string
     */
    public function getApiKey() : string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установка Ключ API
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKey(string $value): AbstractRequest
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey() : string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установка Секретный ключ
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }
}
