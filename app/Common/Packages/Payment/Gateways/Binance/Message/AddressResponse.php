<?php


namespace App\Common\Packages\Payment\Gateways\Binance\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return $this->data['success'] == 1;
    }

    public function isRedirect(): bool
    {
        return false;
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->data['address'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->data['addressTag'] ?? '';
    }
}
