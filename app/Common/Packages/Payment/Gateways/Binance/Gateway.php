<?php

namespace App\Common\Packages\Payment\Gateways\Binance;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\Binance\Message\AddressRequest;


class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Binance';

    /**
     * Получить Ключ API
     *
     * @return string
     */
    public function getApiKey() : string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установка Ключ API
     *
     * @param string $value
     * @return Gateway
     */
    public function setApiKey(string $value): Gateway
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить Секретный ключ
     *
     * @return string
     */
    public function getSecretKey() : string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установка Секретный ключ
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }
    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'api_key' => '',
            'secret_key' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function address(array $parameters = []): AbstractRequest
    {
        return $this->createRequest(AddressRequest::class, $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
