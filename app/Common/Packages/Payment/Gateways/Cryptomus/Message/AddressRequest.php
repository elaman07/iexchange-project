<?php


namespace App\Common\Packages\Payment\Gateways\Cryptomus\Message;

use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Lcobucci\JWT\{
    Configuration,
    Signer\Rsa\Sha256,
    Signer\Key\InMemory
};
use DateTimeImmutable;
use Illuminate\Support\{
    Facades\Http, Str
};



class AddressRequest extends AbstractRequest
{
    /**
     * Ссылка на API
     *
     * @var string
    */
    protected string $endpoint_url = 'https://api.cryptomus.com';

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');


        return [
            'currency'  =>  (string)$this->getCoinCurrency(),
            'amount'    =>  (string)$this->getCoinAmount(),
            'order_id'  =>  (string)$this->getTransactionId()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        // Текущий код валюты (в сети)
        if(!empty($network_code)) {
            $data['network'] = $network_code;
        }

        $data_encode = json_encode($data);
        $sign = md5(base64_encode($data_encode) . $this->getSecretKey());

        $response = Http::baseUrl($this->endpoint_url)->withHeaders([
            'merchant' => $this->getMerchantId(),
            'sign' => $sign
        ])->asJson();

        $httpResponse = $response->post('/v1/payment', $data)->json();

        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'Cryptomus',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => 'https://api.cryptomus.com/v1/payment',
            'headers' => isset($response->getOptions()['headers']) ? json_encode($response->getOptions()['headers']) : null,
            'content' => json_encode($data),
            'response' => json_encode($httpResponse)
        ]);

        if(isset($httpResponse['state']) and $httpResponse['state'] != 0) {
            throw new \Exception(json_encode($httpResponse));
        }

        return $this->response = new AddressResponse($this, $httpResponse);
    }
}
