<?php

namespace App\Common\Packages\Payment\Gateways\Cryptomus\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить merchant_id
     *
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->getParameter('merchant_id');
    }

    /**
     * Установить merchant_id
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setMerchantId(string $value): AbstractRequest
    {
        return $this->setParameter('merchant_id', $value);
    }

    /**
     * Получить secret_key
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить secret_key
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }

}
