<?php

namespace App\Common\Packages\Payment\Gateways\Cryptomus;


use DateTimeImmutable;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string|null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $request = $this->request('/v1/balance');

        try {
            if(isset($request['result']))
            {
                $first = Arr::first($request['result']);
                return Arr::first($first['balance']['merchant'], function($item) use($currency) {
                    return $item['currency_code'] == Str::upper($currency);
                })['balance'];
            }

        }catch (\Exception $exception) {
            return  0;
        }

        return 0;
    }

    /**
     * Отправка средств
     *
     * @param $options
     * @return mixed
     * @throws Exception
     */
    public function transfer($options)
    {
        return $this->request('/v1/payout', $options);
    }


    public function findTransaction(string $id)
    {
        return $this->request('/v1/payment/info', [
            'uuid' => $id
        ]);
    }

    /**
     * Запросы
     *
     * @param $path
     * @param array $options
     * @return array|mixed
     * @throws Exception
     */
    public function request($path, array $options = [])
    {
        $data = json_encode($options);
        if($path == '/v1/payout') {
            $sign = md5(base64_encode($data) . $this->parameters['payout_key']);
        } else {
            $sign = md5(base64_encode($data) . $this->parameters['secret_key']);
        }

        $response = Http::baseUrl('https://api.cryptomus.com')->withHeaders([
                'merchant' => $this->parameters['merchant_id'],
                'sign' => $sign
            ])->asJson()->post($path, $options)->json();

        // Записываем в лог
        add_auto_payment_log_event([
            'provider' => 'Cryptomus',
            'url' => 'https://api.cryptomus.com'.$path,
            'headers' => json_encode([
                'merchant' => $this->parameters['merchant_id'],
                'sign' => $sign
            ]),
            'content' => json_encode($options),
            'response' => json_encode($response)
        ]);

        if(isset($response['state']) and $response['state'] == 0) {
            return $response;
        }

        throw new \Exception(json_encode($response));
    }
}
