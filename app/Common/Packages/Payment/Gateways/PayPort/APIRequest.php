<?php

namespace App\Common\Packages\Payment\Gateways\PayPort;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return float
     * @throws Exception
     */
    public function getBalance($currency): float
    {
        $response = $this->call('get', '/api/v3/balance',[], 'merchant');
        return remove_all_spaces($response['balance']);
    }


    public function findTransaction(int $id)
    {
        return $this->call('post', '/api/v3/payment/check/status', [
            'invoice_id' => (int)$id
        ]);
    }

    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @param $ad_id
     * @return mixed
     * @throws Exception
     */
    public function transfer($amount, string $to, string $currency, $ad_id)
    {
        $request = $this->call('post', '/api/v3/withdrawal/request', [
            'amount'  => iex_number_format($amount, 2),
            'currency' => (string)$currency,
            'merchant_expense' => 1,
            'exact_currency' => 1
        ]);

        $withdrawal = $this->call('post', '/api/v3/withdrawal/create', [
           'ad_id' => 'b_'.$ad_id,
            'amount'  => iex_number_format($amount, 2),
            'currency' => (string)$currency,
            'message'   =>  $to
        ]);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $method
     * @param string $path
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    private function call(string $method, string $path, array $options = [])
    {
        $response = Http::withToken($this->parameters['api_key'])
            ->baseUrl($this->parameters['user_url'])
            ->asJson()->{\Str::lower($method)}($path, $options)->json();

        if(isset($response) and !empty($response['error']))
            throw new \Exception(json_encode($response));

        return $response['data'] ?? '';
    }
}
