<?php

namespace App\Common\Packages\Payment\Gateways\PayPort;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'PayPort';


    public function getMerchantId(): string
    {
        return $this->getParameter('merchant_id');
    }


    public function setMerchantId(string $value): Gateway
    {
        return $this->setParameter('merchant_id', $value);
    }


    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }


    public function setApiKey(string $value): Gateway
    {
        return $this->setParameter('api_key', $value);
    }


    public function getApiKey3(): string
    {
        return $this->getParameter('api_key3');
    }


    public function setApiKey3(string $value): Gateway
    {
        return $this->setParameter('api_key3', $value);
    }


    public function getUserUrl(): string
    {
        return $this->getParameter('user_url');
    }


    public function setUserUrl(string $value): Gateway
    {
        return $this->setParameter('user_url', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return [
            'merchant_id' => '',
            'api_key' => '',
            'api_key3' => '',
            'user_url' => ''
        ];
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
