<?php

namespace App\Common\Packages\Payment\Gateways\PayPort\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->getParameter('merchant_id');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setMerchantId($value): AbstractRequest
    {
        return $this->setParameter('merchant_id', $value);
    }

    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKey($value): AbstractRequest
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить Client ID
     *
     * @return string
     */
    public function getApiKey3(): string
    {
        return $this->getParameter('api_key3');
    }

    /**
     * Установить Client ID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKey3($value): AbstractRequest
    {
        return $this->setParameter('api_key3', $value);
    }


    /**
     * Получить user_url
     *
     * @return string
     */
    public function getUserUrl(): string
    {
        return $this->getParameter('user_url');
    }

    /**
     * Установить user_url
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setUserUrl($value): AbstractRequest
    {
        return $this->setParameter('user_url', $value);
    }
}
