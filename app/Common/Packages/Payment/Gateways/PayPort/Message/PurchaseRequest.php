<?php

namespace App\Common\Packages\Payment\Gateways\PayPort\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'api_key'
        );

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (float)iex_number_format( $amount + $amount * $commission / 100, 2);
        }

        // Через форму
        if($order_data->merchant->type_pay == 0)
        {
            return [
                //'ad_id' => (string)$get_ad_id,
                'order_id'  => (string)$this->getTransactionId(),
                'amount' => iex_number_format($total_amount, 2),
                'currency' => $this->getCurrency(),
                'customer_id' => (string)Carbon::now()->timestamp,
                'server_url'    => $this->getNotifyUrl()
            ];
        }

        // Выдача реквизитов
        if($order_data->merchant->type_pay == 1)
        {
            // Получаем данные платежных систем
            $httpClient = Http::baseUrl($this->getUserUrl())
                ->withToken($this->getApiKey3());


            $response_payment = $httpClient->post('/api/v3/payment/request', [
                'amount' => iex_number_format($total_amount, 2),
                'currency' => $this->getCurrency(),
                'exact_currency' => true,
                'filter_payment_system_types' => ['card_number'],
                'filter_payment_systems' => [$order_data->merchant->bank_name]
            ])->json();


            \Log::debug(json_encode($response_payment));

            $get_ad_id = 0;
            if(isset($response_payment['data']) and is_array($response_payment['data'])) {
                $get_ad_id = collect($response_payment['data'])->first();
            }


            return [
                'ad_id' => (string)$get_ad_id['ad_id'],
                'amount' => iex_number_format($total_amount, 2),
                'currency' => $this->getCurrency(),
                'customer_id' => (string)Carbon::now()->timestamp,
                'order_id' => (string)$this->getTransactionId(),
                'locale' => app()->getLocale(),
                'payment_attributes' => [
                    'client_name' => $order_data->user->name,
                    'client_email' => $order_data->user->email,
                    'phone' => $order_data->phone,
                    'email' => $order_data->user->email
                ]
            ];
        }

        return [];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $order_data = $this->getOrderData();

        if($order_data->merchant->type_pay == 0) {
            $httpResponse = Http::baseUrl($this->getUserUrl())
                ->withToken($this->getApiKey());

            $options = [
                'order_id' => (string)$this->getTransactionId(),
                'amount' => iex_number_format($this->getAmount(), 2),
                'currency' => $this->getCurrency(),
                'order_desc' => $this->getDescription(),
                'response_url' => $this->getReturnUrl(),
                'cancel_url' => $this->getCancelUrl(),
                'server_url' => $this->getNotifyUrl(),
                'locale' => app()->getLocale(),
                'customer_id' => (string)Carbon::now()->timestamp,
            ];

            $responseInvoiceData = $httpResponse->post('/api/v5/invoice/get', $options)->json();

            if($responseInvoiceData['status'] != 1) {
                throw new \Exception(json_encode($responseInvoiceData));
            }

            return $this->response = new PurchaseResponse($this, $responseInvoiceData);

        } else {

            $httpResponse = Http::baseUrl($this->getUserUrl())
                ->withToken($this->getApiKey3());

            $responseInvoiceData = $httpResponse->post('/api/v3/payment/create_noreceipt', $data)->json();

            $approvedTx = $httpResponse->post('/api/v3/payment/check/approved', [
                'invoice_id' =>  $responseInvoiceData['data']['invoice_id']
            ])->json();

            if($responseInvoiceData['status'] != 1) {
                throw new \Exception(json_encode($responseInvoiceData));
            }

            return $this->response = new PurchaseH2HResponse($this, array_merge($approvedTx, ['order_id' => $this->getTransactionId()]));
        }
    }
}
