<?php

namespace App\Common\Packages\Payment\Gateways\Stripe\Message;


use App\Common\Packages\Payment\Engines\Item;
use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Facades\Request;
use Stripe\Checkout\Session;
use Stripe\Stripe;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        // Just validate the parameters.
        $this->validate('api_key', 'transactionId', 'returnUrl', 'cancelUrl');

        return null;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function sendData($data)
    {
        // We use Stripe's SDK to initialise a (Stripe) session. The session gets passed through the process and is
        // used to identify this transaction.
        Stripe::setApiKey($this->getApiKey());

        // Initiate the session.
        // Unfortunately (and very, very annoyingly), the API does not allow negative- or zero value items in the
        // cart, so we have to filter them out (and re-index them) before we build the line items array.
        // Beware because the amount the customer pays is the sum of the values of the remaining items, so if you
        // supply negative-valued items, they will NOT be deducted from the payment amount.
        $session = Session::create(
            [
                'client_reference_id' => $this->getTransactionId(),
                'payment_method_types' => ['card'],
                'payment_intent_data' => [
                    'description' => $this->getDescription(),
                ],
                'line_items' => array_map(
                    function (Item $item) {
                        // Sometimes PHP can't hold the item price accurately, which is why we have to use round()
                        // after multiplying by 100. Eg, 9.95 is stored as 9.9499999999999993 and without round() it
                        // ends up as 994 when it should be 995.
                        return [
                            'name' => $item->getName(),
                            'description' => $this->nullIfEmpty($item->getDescription()),
                            'amount' => (int)round((100 * $item->getPrice())), // @TODO: The multiplier depends on the currency
                            'currency' => $this->getCurrency(),
                            'quantity' => $item->getQuantity(),
                        ];
                    },
                    array_values(
                        array_filter(
                            $this->getItems()->all(),
                            function (Item $item) {
                                return $item->getPrice() > 0;
                            }
                        )
                    )
                ),
                'success_url' => $this->getReturnUrl(),
                'cancel_url' => $this->getCancelUrl(),
            ]
        );

        return $this->response = new PurchaseResponse($this, ['session' => $session]);
    }

    private function nullIfEmpty(string $value = null): ?string
    {
        return empty($value) ? null : $value;
    }
}
