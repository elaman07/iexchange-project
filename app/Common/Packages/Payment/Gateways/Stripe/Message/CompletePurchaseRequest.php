<?php

namespace App\Common\Packages\Payment\Gateways\Stripe\Message;

use App\Common\Packages\Payment\Gateways\Stripe\Lib\ComplexTransactionRef;
use App\Common\Packages\Payment\Exception\{
    InvalidRequestException,
    InvalidResponseException
};
use Stripe\Checkout\Session;
use Stripe\PaymentIntent;

class CompletePurchaseRequest extends AbstractRequest
{
    /**
     * @var string|null
     */
    private $sessionID;

    /**
     * @return string|null
     */
    public function getSessionID()
    {
        return $this->sessionID;
    }

    /**
     * Because we have to pass the session id around, but also save the transaction ref in the same place, they are
     * combined, in JSON format, so this setter has to split them.
     *
     * @param string $value
     */
    public function setTransactionReference($value)
    {
        $this->sessionID = ComplexTransactionRef::buildFromJson($value)->getSessionID();
    }

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws InvalidRequestException
     */
    public function getData()
    {
        // Just validate the parameters.
        $this->validate('api_key');

        return null;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return CompletePurchaseResponse
     * @throws InvalidResponseException
     */
    public function sendData($data)
    {
        // Retrieve the session that would have been started earlier.
        \Stripe\Stripe::setApiKey($this->getApiKey());

        $session = Session::retrieve($this->sessionID);
        $paymentIntent = PaymentIntent::retrieve($session->payment_intent);

        return $this->response = new CompletePurchaseResponse($this, ['paymentIntent' => $paymentIntent]);
    }
}
