<?php

namespace App\Common\Packages\Payment\Gateways\Stripe\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Get the gateway API Key (the "secret key").
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Set the gateway API Key.
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setApiKey($value): AbstractRequest
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Get the gateway public Key (the "publishable key").
     *
     * @return string
     */
    public function getPublic(): string
    {
        return $this->getParameter('public_key');
    }

    /**
     * Set the gateway public Key.
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setPublic($value): AbstractRequest
    {
        return $this->setParameter('public_key', $value);
    }

}
