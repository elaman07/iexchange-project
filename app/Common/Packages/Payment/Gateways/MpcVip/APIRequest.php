<?php

namespace App\Common\Packages\Payment\Gateways\MpcVip;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance($currency = null)
    {
        $response = $this->call('post', '/api/payment/get_balance', []);
        return $response['balance'];
    }

    /**
     * Баланс кошелька
     *
     * @param null $order_id
     * @return mixed
     * @throws Exception
     */
    public function getStatus($order_id = null)
    {
        $response = $this->call('post', '/payment/get_withdraw_order_info_by_id', []);
        return $response;
    }


    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @param $order_id
     * @return mixed
     * @throws Exception
     */
    public function sendToAccount($amount, string $to, string $currency, Task $task)
    {
        return $this->call('post', '/api/payment/withdraw_to_qiwi_v2', [
            'UID' => $task->id,
            'amount'    => (int)round($amount, 0),
            'recipient'   => $to,
            'clientInfo' => [
                'client' => '',
                'recepient' => $task->to_shot,
                'recepient_system' => $task->direction_exchange->currency1->payment->name,
                'c_from' => $task->direction_exchange->currency1->designation_xml,
                'c_to' => $task->direction_exchange->currency2->designation_xml,
                'date' => explode('+', date(DATE_ATOM))[0],
                'txn' => (string)$task->id,
                'ip' => $task->ip,
                'user_agent' => '',
                'email' => $task->email,
                'cid' => '',
                'owner' => iEXContentLanguage('sitename'),
                'owner_email' => iEXSetting('project_email_support')
            ]
        ]);
    }

    /**
     * Отправить на карту
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @return mixed
     * @throws \Exception
     */
    public function sendToCard($amount, string $to, string $currency, Task $task)
    {

        return $this->call('post', '/api/payment/withdraw_to_card_v2', [
            'UID' => (string)$task->id,
            'amount'    => (int)round($amount, 0),
            'recipient'   => $to,
            'clientInfo' => [
                'client' => '',
                'recepient' => $task->to_shot,
                'recepient_system' => $task->direction_exchange->currency1->payment->name,
                'c_from' => $task->direction_exchange->currency1->designation_xml,
                'c_to' => $task->direction_exchange->currency2->designation_xml,
                'date' => explode('+', date(DATE_ATOM))[0],
                'txn' => (string)$task->id,
                'ip' => $task->ip,
                'user_agent' => '',
                'email' => $task->email,
                'cid' => '',
                'owner' => iEXContentLanguage('sitename'),
                'owner_email' => iEXSetting('project_email_support')
            ]
        ]);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $method
     * @param string $path
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    private function call(string $method, string $path, array $options = [])
    {
        $signature = hash_hmac('sha512', json_encode($options), $this->parameters['api_secret']);

        $httpResponse = Http::withHeaders([
            'API-Key' => $this->parameters['api_key'],
            'Signature' => $signature
        ]);

        if(!empty($this->parameters['user_agent'])) {
            $httpResponse = $httpResponse->withUserAgent($this->parameters['user_agent']);
        }

        $httpResponse->asJson()->{$method}('https://masterprocessingvip.ru'.$path, $options)->json();

        if(isset($httpResponse['status']) and $httpResponse['status'] == 'Cannot') {
            throw new \Exception(json_encode($httpResponse));
        }

        if(array_key_exists('success', $httpResponse) and !$httpResponse['success']) {
            throw new \Exception(json_encode($httpResponse));
        }

        return $httpResponse;
    }
}
