<?php

namespace App\Common\Packages\Payment\Gateways\MpcVip\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'api_key', 'api_secret', 'amount', 'currency', 'transactionId'
        );

        // (float)$this->getOrderData()->direction_exchange->currency1->merchant->fixed_fee

        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (int)round($amount + $amount * $commission / 100, 0);
        }

        switch ($order_data->merchant->method_pay)
        {
            case '1':
                $method = 'card';
                break;
            default:
                $method = 'qw';
        }

        $from_shot = preg_replace('/\s+/', '', $this->getOrderData()->from_shot);

        if($order_data->merchant->type_pay == 0)
        {
            return [
                'amount' => (int)$total_amount,
                'expireAt' => (int)iEXSetting('max_time_task', 900),
                'comment' => $this->getDescription(),
                'clientIP' => $order_data->ip,
                'userID'    =>  $order_data->user->id,
                'paySourcesFilter' => $method,
                'cardNumber' => (string)mb_substr($from_shot, -4),
                'email' => $order_data->user->email,
                'userAgent' => $order_data->user->user_agent,
                'successRedirectURL' => $this->getReturnUrl(),
                'failedRedirectURL' => $this->getCancelUrl(),
                'callbackURL' => $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
            ];
        } else {
            return [
                'amount' => (int)$total_amount,
                'paySource' => $order_data->merchant->bank_name == 0 ? 1 : $order_data->merchant->bank_name,
                'uid' => (string)$this->getTransactionId(),
                'callbackURL' => $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
            ];
        }
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $signature = hash_hmac('sha512', json_encode($data), $this->getApiSecret());
        $order_data = $this->getOrderData();

        $url = '';

        $httpResponse = Http::withHeaders([
            'API-Key' => $this->getApiKey(),
            'Signature' => $signature
        ])->asJson();

        if(!empty($this->getUserAgent())) {
            $httpResponse = $httpResponse->withUserAgent($this->getUserAgent());
        }


        if($order_data->merchant->type_pay == 0)
        {

            $httpResponse->baseUrl('https://masterprocessingvip.ru');
            $result = $httpResponse->post('/api/payment/generate_p2p_v3', $data)->json();
            $url = '/api/payment/generate_p2p_v3';
        } else {
            $httpResponse->baseUrl('https://masterprocessingvip.ru');
            $result = $httpResponse->post('/api/payment/create_p2p', $data)->json();
            $url = '/api/payment/create_p2p';
        }


        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'mpcvip',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => 'https://masterprocessingvip.ru'.$url,
            'headers' => isset($httpResponse->getOptions()['headers']) ? json_encode($httpResponse->getOptions()['headers']) : null,
            'content' => json_encode($data),
            'response' => json_encode($result)
        ]);

        if(!$result['success']) {
            throw new \Exception(json_encode($result));
        }
        if($order_data->merchant->type_pay == 0) {
            return $this->response = new PurchaseResponse($this, $result);
        }

        return $this->response = new PurchaseP2PResponse($this, $result);
    }
}
