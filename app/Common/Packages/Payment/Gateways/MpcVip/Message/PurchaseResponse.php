<?php

namespace App\Common\Packages\Payment\Gateways\MpcVip\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;
use Illuminate\Support\Arr;


class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl()
    {
        if(isset($this->data['paymentLinks'])) {
            return Arr::first($this->data['paymentLinks']);
        }
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    public function getRedirectData()
    {
        return $this->data;
    }

    public function getLabel()
    {
        return $this->data['billID'];
    }
}

