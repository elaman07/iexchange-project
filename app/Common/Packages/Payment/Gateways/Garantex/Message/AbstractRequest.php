<?php

namespace App\Common\Packages\Payment\Gateways\Garantex\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить PrivateKey
     *
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->getParameter('private_key');
    }

    /**
     * Установить PrivateKey
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setPrivateKey(string $value): AbstractRequest
    {
        return $this->setParameter('private_key', $value);
    }

    /**
     * Получить Uuid
     *
     * @return string
     */
    public function getUuid(): string
    {
        return $this->getParameter('uuid');
    }

    /**
     * Установить Uuid
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setUuid(string $value): AbstractRequest
    {
        return $this->setParameter('uuid', $value);
    }

}
