<?php


namespace App\Common\Packages\Payment\Gateways\Garantex\Message;

use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use Lcobucci\JWT\{
    Configuration,
    Signer\Rsa\Sha256,
    Signer\Key\InMemory
};
use DateTimeImmutable;
use Illuminate\Support\{
    Facades\Http, Str
};



class AddressRequest extends AbstractRequest
{
    /**
     * Ссылка на API
     *
     * @var string
    */
    protected string $endpoint_url = 'https://garantex.org';

    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        $this->validate('coinCurrency', 'transactionId');

        return [
            'currency'  =>  $this->getCoinCurrency()
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $key = InMemory::plainText(base64_decode($this->getPrivateKey(), true));
        $config = Configuration::forSymmetricSigner(new Sha256(), $key);
        $now   = new DateTimeImmutable();

        $builder = $config->builder()
            ->identifiedBy(bin2hex(random_bytes(12)))
            ->expiresAt($now->modify('+1 hour'))
            ->getToken($config->signer(), $config->signingKey());


            $token_data = Http::asJson()
                ->post('https://dauth.garantex.org/api/v1/sessions/generate_jwt', [
                    'kid' => $this->getUuid(),
                    'jwt_token' => strval($builder->toString())
                ])->json();

            if(!isset($token_data['token'])) {
                throw new \Exception('Токен Garantex не создан');
            }

            $token_id = $token_data['token'];

        $http_request = Http::baseUrl($this->endpoint_url)
            ->withToken($token_id)
            ->asJson();

        $options_currency = $data['currency'];
        $network_code = $this->getOrderData()->direction_exchange->currency1->network_code;
        if(!empty($network_code)) {
            $options_currency = $network_code;
        }

        $httpResponse = $http_request->post('/api/v2/deposit_address', [
            'currency' => trim(Str::lower($options_currency))
        ])->json();

        // Записываем результаты в лог
        add_merchant_log_event([
            'provider' => 'garantex',
            'id_order' => $this->getTransactionId(),
            'ip_address' => $this->getOrderData()->ip,
            'url' => $this->endpoint_url.'/api/v2/deposit_address',
            'headers' => null,
            'content' => json_encode($data),
            'response' => json_encode($httpResponse)
        ]);

        $data = [];
        if(isset($httpResponse['id']))
        {
            $id = intval($httpResponse['id']);
            if($id) {
                sleep(5);
                $data = $http_request->get('/api/v2/deposit_address/details', [
                    'id' => $id
                ])->json();
            }

            // Записываем результаты в лог
            add_merchant_log_event([
                'provider' => 'garantex',
                'id_order' => $this->getTransactionId(),
                'ip_address' => $this->getOrderData()->ip,
                'url' => $this->endpoint_url.'/api/v2/deposit_address/details',
                'headers' => null,
                'content' => json_encode([
                    'id' => $id
                ]),
                'response' => json_encode($data)
            ]);

        }

        return $this->response = new AddressResponse($this, $data);
    }
}
