<?php

namespace App\Common\Packages\Payment\Gateways\Garantex;


use DateTimeImmutable;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string|null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance(string $currency)
    {
        $request = $this->request('get', sprintf('/api/v2/accounts/%s', Str::lower($currency)));

        if(isset($request['balance'])) {
            return $request['balance'];
        }

        return 0;
    }

    /**
     * Отправка средств
     *
     * @param string $currency
     * @param float $amount
     * @param string $address
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function transfer(string $currency, float $amount, string $address)
    {
        $currency = trim(strtolower($currency));
        $amount = trim($amount);
        $address = trim($address);

        $options = [
            'currency' => $currency,
            'amount' => $amount,
            'rid' => $address,
        ];

        return $this->request('get', '/api/v2/withdraws/create', $options);
    }

    public function findTransaction(string $currency)
    {
        $request = $this->request('get', '/api/v2/deposits', [
            'limit' => 100,
            'currency' => trim(strtolower($currency))
        ]);

        $trans = [];
        if(is_array($request) and !isset($request['error'])){
            $trans = $request;
        }
        return $trans;
    }

    public function findWithdrawsTransaction(string $id)
    {
        $request = $this->request('get', '/api/v2/withdraws/'.$id);

        $trans = [];
        if(is_array($request) and !isset($request['error'])){
            $trans = $request;
        }
        return $trans;
    }

    /**
     * Запросы
     * @param $method
     * @param $path
     * @param $options
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function request($method, $path, $options = [])
    {
        $key = InMemory::plainText(base64_decode($this->parameters['private_key'], true));
        $config = Configuration::forSymmetricSigner(new Sha256(), $key);
        $now   = new DateTimeImmutable();

        $builder = $config->builder()
            ->identifiedBy(bin2hex(random_bytes(12)))
            ->expiresAt($now->modify('+1 year'))
            ->getToken($config->signer(), $config->signingKey());

        $token_data = Http::asJson()
            ->post('https://dauth.garantex.org/api/v1/sessions/generate_jwt', [
                'kid' => $this->parameters['uuid'],
                'jwt_token' => strval($builder->toString())
            ])->json();

        if(!isset($token_data['token'])) {
            throw new \Exception('Токен Garantex не создан');
        }

        $token_id = $token_data['token'];



        return  Http::baseUrl('https://garantex.org')->withToken($token_id)->asJson()->{$method}($path, $options)->json();
    }
}
