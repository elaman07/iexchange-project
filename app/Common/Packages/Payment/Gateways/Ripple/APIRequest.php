<?php

namespace App\Common\Packages\Payment\Gateways\Ripple;


use Exception;
use IEXBase\RippleAPI\Ripple;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use XRPL_PHP\Client\JsonRpcClient;
use XRPL_PHP\Core\Networks;
use XRPL_PHP\Wallet\Wallet;
use function XRPL_PHP\Sugar\xrpToDrops;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];


    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
    }

    /**
     * Получение баланса
     *
     * @param string $currency
     * @return float
     * @throws Exception
     */
    public function getBalance(string $currency): float
    {
        if(Str::upper($currency) != 'XRP') {
            throw new \Exception('Неверный код валюты');
        }

        $response = Arr::first($this->request()->getAccountBalances()['balances'], function($item) {
            return $item['currency'] == 'XRP';
        });

        return $response['value'];
    }

    /**
     * Детали по заяве
    */
    public function findTransaction($account, $count = 10, $from = 0): array
    {
        $array = $this->request()->getAccountPayments($this->parameters['main_address'], [
            'destination_tag'   =>  $account
        ]);

        if(!empty($array) and $array['source_currency'] == 'XRP') {
            return [
                'address'        =>  $array['destination'],
                'amount'         =>  $array['delivered_amount'],
                'confirmations'  =>  2,
                'txid'           =>  $array['tx_hash']
            ];
        }

        return [];
    }

    /**
     * Отправляем средства клиенту
     *
     * @param $to
     * @param $amount
     * @param array $options
     * @return array
     * @throws Exception
     */
    public function transfer($to, $amount, array $options = []): array
    {
        $address = $this->parameters['main_address'];
        $secretKey = $this->parameters['private_key'];
        $rpcLink = Networks::getNetwork('mainnet')['jsonRpcUrl'];
        $client = new JsonRpcClient($rpcLink);
        $wallet = Wallet::fromSeed($secretKey);

        $paymentTx = [
            'TransactionType' => 'Payment',
            'Account' => $address,
            'Amount' => xrpToDrops($amount),
            'Destination' => $to
        ];
        if(isset($options['destination_tag'])) {
            $paymentTx['DestinationTag'] = (int)$options['destination_tag'];
        }

        $preparedTx = $client->autofill($paymentTx);
        $signedTx = $wallet->sign($preparedTx);

        $txResponse = $client->submitAndWait($signedTx['tx_blob']);
        $response = $txResponse->getResult();

        if(isset($response['status']) and $response['status'] == 'success' and isset($response['meta'])) {
            return $response;
        }

        throw new \Exception('Выплата не произведена');
    }

    /**
     * Запросы
     *
     * @return Ripple
     */
    public function request(): Ripple
    {
        return new Ripple($this->parameters['main_address'], $this->parameters['private_key'], [
            'wss_node' => 'https://validator.iexbase.com'
        ]);
    }
}
