<?php

namespace App\Common\Packages\Payment\Gateways\Ripple\Message;

use App\Common\Packages\Payment\Engines\Message\ResponseInterface;

class AddressRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return array
     * @throws
     */
    public function getData(): array
    {
        return [];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        return $this->response = new AddressResponse($this, [
            'address' => $this->getMainAddress(),
            'dest_tag' => $this->getOrderData()->id
        ]);
    }
}
