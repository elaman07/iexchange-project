<?php


namespace App\Common\Packages\Payment\Gateways\Ripple\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class AddressResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function isSuccessful(): bool
    {
        return isset($this->data['address']);
    }

    public function isRedirect(): bool
    {
        return false;
    }

    public function getAddress(): string
    {
        return $this->data['address'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->data['dest_tag'];
    }
}
