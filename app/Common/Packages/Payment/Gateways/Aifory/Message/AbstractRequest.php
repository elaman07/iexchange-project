<?php

namespace App\Common\Packages\Payment\Gateways\Aifory\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\AdvCash\Gateway;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить API Key
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('api_key');
    }

    /**
     * Установить API Key
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiKey(string $value): AbstractRequest
    {
        return $this->setParameter('api_key', $value);
    }

    /**
     * Получить API Secret
     *
     * @return string
     */
    public function getApiSecret(): string
    {
        return $this->getParameter('api_secret');
    }

    /**
     * Установить API Secret
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setApiSecret(string $value): AbstractRequest
    {
        return $this->setParameter('api_secret', $value);
    }

    /**
     * Получить API Secret
     *
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->getParameter('user_agent');
    }

    /**
     * Установить API Secret
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setUserAgent(string $value): AbstractRequest
    {
        return $this->setParameter('user_agent', $value);
    }
}
