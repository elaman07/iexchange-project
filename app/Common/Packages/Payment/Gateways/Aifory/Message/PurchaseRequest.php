<?php

namespace App\Common\Packages\Payment\Gateways\Aifory\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'api_key', 'api_secret', 'amount', 'currency', 'transactionId'
        );

        $ip = $this->getOrderData()->ip;
        $user_id = $this->getOrderData()->id_user;


        // Итоговая сумма
        $amount = (float)$this->getAmount();
        $order_data = $this->getOrderData();

        // Если комиссию платит обменник
        if($order_data->merchant->is_pay_commission == 0) {
            $total_amount = $this->getAmount();
        } else {
            $commission = (float)$order_data->direction_exchange->currency1->commission_merchant_percent;
            $total_amount = (int)round($amount + $amount * $commission / 100, 0);
        }


        $options = [];
        // P2P
        if($order_data->merchant->type_pay == 0) {
            $options = [
                'amount' => (float)$total_amount,
                'currencyID' => $order_data->merchant->bank_name,
                'typeID' => 4,
                'clientOrderID' => (string)$this->getTransactionId(),
                'webhookURL' => $this->getNotifyUrl().'?order_id='.$this->getTransactionId(),
                'TTL' => 999,
                'extra' => [
                    "allowedMethodIDs" => [
                        $order_data->merchant->method_pay
                    ],
                    'failedRedirectURL' => $this->getCancelUrl(),
                    'successRedirectURL' => $this->getReturnUrl(),
                    'payerInfo' => [
                        'userAgent' => Request::userAgent(),
                        'IP' => $ip,
                        'userID' => $user_id,
                        'fingerprint' => 'fbb77b9f4265b18538e66cac5a37c6410dc2cdd7f0cddfde6eda25aa10df669b',
                        'registeredAt' => time()
                    ]
                ]
            ];
        } else {
            $options = [
                'amount' => (float)$total_amount,
                'currencyID' => $order_data->merchant->bank_name,
                'typeID' => 3,
                'clientOrderID' => (string)$this->getTransactionId(),
                'webhookURL' => '',
                'TTL' => 999,
                'extra' => [
                    'methodID' => $order_data->merchant->method_pay,
                    'payerInfo' => [
                        'userAgent' => Request::userAgent(),
                        'IP' => $ip,
                        'userID' => $user_id,
                        'fingerprint' => 'fbb77b9f4265b18538e66cac5a37c6410dc2cdd7f0cddfde6eda25aa10df669b',
                        'registeredAt' => time()
                    ]
                ]
            ];
        }


        return $options;
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendData($data)
    {
        $order_data = $this->getOrderData();
        $payload = json_encode($data);
        $hash_hmac = hash_hmac('sha512', $payload, $this->getApiSecret());

        $httpResponse = Http::baseUrl('https://api.aifory.io')->withHeaders([
            'API-Key' => $this->getApiKey(),
            'Content-Type' => 'application/json',
            'Signature' => $hash_hmac,
            'user-agent' => $this->getUserAgent()
        ])->post('/payin/process', $data)->json();



        if(isset($httpResponse['status']) and $httpResponse['status'] == 'accepted') {

            if($order_data->merchant->type_pay == 0) {
                return $this->response = new PurchaseResponse($this, $httpResponse);
            }

            return $this->response = new PurchaseH2HResponse($this, $httpResponse);
        }


        throw new \Exception(json_encode($httpResponse));
    }
}
