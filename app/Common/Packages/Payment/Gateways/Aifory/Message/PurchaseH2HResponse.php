<?php

namespace App\Common\Packages\Payment\Gateways\Aifory\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;
use App\Common\Packages\Payment\Engines\Message\RequestInterface;
use Illuminate\Support\Arr;


class PurchaseH2HResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return isset($this->data['status']) && $this->data['status'] == 'accepted';
    }

    public function isRedirect(): bool {
        return false;
    }

    public function getRedirectMethod(): string
    {
        return 'GET';
    }

    /**
     * Получаем адрес
     *
     * @return string
     */
    public function getAddress(): string
    {
        return mask_formatting_bank_card($this->data['accountNumber']);
    }

    /**
     * Получаем валюту
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return '';
    }

    /**
     * Получаем описание
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->data['orderID'];
    }

    /**
     * Получаем тэг
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->data['accountName'] ?? '';
    }

}
