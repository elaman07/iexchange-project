<?php

namespace App\Common\Packages\Payment\Gateways\Aifory;


use App\Models\Task;
use DiDom\Document;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Тип транзакции
     *
     * @var ?string
    */
    protected ?string $methodPay = null;

    /**
     * Создаем констркутор QIWI
     *
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->parameters = $params;
    }

    /**
     * Баланс кошелька
     *
     * @param null $currency
     * @return mixed
     * @throws Exception
     */
    public function getBalance($currency = null)
    {
        $response = $this->call('get', '/account/balance');
        $balance = collect($response)->first(function($query) use ($currency) {
           return $query['currencyName'] == $currency;
        });

        return $balance['balance'];
    }

    /**
     * Баланс кошелька
     *
     * @param null $order_id
     * @return mixed
     * @throws Exception
     */
    public function findTransaction($order_id = null)
    {
        return $this->call('post', '/payin/details', [
            'ID' => $order_id
        ]);
    }


    /**
     * Отправить на счет
     *
     * @param $amount
     * @param string $to
     * @param string $currency
     * @param $order_id
     * @return mixed
     * @throws Exception
     */
    public function transfer($options = []) {
        return $this->call('post', '/payout/process', $options);
    }

    /**
     * Вызов для получения данных из ADGroup
     *
     * @param string $method
     * @param string $path
     * @param array $options
     * @return mixed
     * @throws Exception
     */
    private function call(string $method, string $path, array $options = [])
    {
        if(empty($options)) {
            $hash_hmac = hash_hmac('sha512', '', $this->parameters['api_secret']);
        } else {
            $payload = json_encode($options);
            $hash_hmac = hash_hmac('sha512', $payload, $this->parameters['api_secret']);
        }

        $httpResponse = Http::baseUrl('https://api.aifory.io')->withHeaders([
            'API-Key' => $this->parameters['api_key'],
            'Content-Type' => 'application/json',
            'Signature' => $hash_hmac,
            'user-agent' => $this->parameters['user_agent']
        ])->{$method}($path, $options)->json();

        if(isset($httpResponse['traceID'])) {
            throw new \Exception(json_encode($httpResponse));
        }

        if(isset($httpResponse['status']) and $httpResponse['status'] == 'refused') {
            throw new \Exception(json_encode($httpResponse));
        }

        return $httpResponse;
    }
}
