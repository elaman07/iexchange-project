<?php

namespace App\Common\Packages\Payment\Gateways\InternalAccount\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
}
