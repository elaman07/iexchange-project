<?php


namespace App\Common\Packages\Payment\Gateways\InternalAccount\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractResponse;
use App\Common\Packages\Payment\Engines\Message\RedirectResponseInterface;

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    /**
     * Ответ успешен?
     *
     * @return boolean
     */
    public function isSuccessful()
    {
        return false;
    }

    /**
     * Ответ требует перенаправления?
     *
     * @return boolean
     */
    public function isRedirect() {
        return true;
    }

    /**
     * Является ли ответ прозрачным перенаправлением?
     *
     * @return boolean
     */
    public function getRedirectUrl() {
        return url('/callbacks/v1/internalaccount/status_account?hash_id='.$this->data['hash_id']);
    }

    /**
     * Отменена ли транзакция пользователем?
     *
     * @return boolean
     */
    public function getRedirectMethod()
    {
        return 'GET';
    }

    /**
     * Получить данные ответа.
     *
     * @return mixed
     */
    public function getRedirectData()
    {
        return $this->data;
    }
}
