<?php

namespace App\Common\Packages\Payment\Gateways\InternalAccount;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Gateways\InternalAccount\Message\AbstractRequest;
use App\Common\Packages\Payment\Gateways\InternalAccount\Message\PurchaseRequest;
use App\Common\Packages\Payment\Gateways\InternalAccount\Message\PurchaseResponse;


class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'InternalAccount';


    /**
     * Параметры по умолчанию
     *
     * @return array
     */
    public function getDefaultParameters(): array
    {
        return [];
    }


    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }


    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return [];
    }
}
