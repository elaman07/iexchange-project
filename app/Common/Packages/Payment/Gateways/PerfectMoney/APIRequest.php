<?php

namespace App\Common\Packages\Payment\Gateways\PerfectMoney;


use DiDom\Document;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class APIRequest
{
    /**
     * URL Perfect Money API
     *
     * @var string
     */
    const API_URL = 'https://perfectmoney.com';

    /**
     * Конфигурация
     *
     * @var array
     */
    protected array $parameters = [];

    /**
     * Экземпляр GuzzleHttp
     *
     * @var Client
     */
    protected $client;

    /**
     * Конструктор
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->parameters = $config;
        $this->client = new Client(['base_uri' => self::API_URL]);
    }

    /**
     * Получить баланс
     *
     * @param null $currency
     * @return array|bool
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance($currency = null)
    {
        $response = $this->request('balance', []);

        $parser = new Document($response);
        if (!$parser->has('input')) {
            throw new \Exception('Баланс не определен');
        }

        $array = [];
        foreach ($parser->find('input') as $input) {
            $array[$input->getAttribute('name')] = $input->getAttribute('value');
        }

        return $array[$this->parameters['account_'.Str::lower($currency)]];
    }

    /**
     * Отправить средства на адрес
     *
     * @param $options
     * @return array
     * @throws \Exception
     */
    public function transferEVoucher($options): array
    {
        return $this->transferVoucher($options);
    }

    /**
     * Отправляем на счет PerfectMoney
     *
     * @param array $options
     * @return array|bool
     * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function transferAccount(array $options = [])
    {
        $currency = ($options['currency'] ?? 'USD');
        $array = [
            'Payer_Account' =>  $this->getAccount($currency),
            'Payee_Account' =>  $options['to'],
            'Amount'        =>  $options['amount'],
            'PAY_IN'        =>  '1'
        ];

        if (array_key_exists('order_id', $options)) {
            $array['PAYMENT_ID'] = $options['order_id'];
        }

        if (array_key_exists('comment', $options)) {
            $array['Memo'] = $options['comment'];
        }

        $response = $this->request('confirm', $array);

        // Разбор HTML Ответа
        $parser = new Document($response);
        if(!$parser->has('input')) {
            throw new \Exception('Транзакция не прошла');
        }

        $data = [];
        foreach ($parser->find('input') as $input) {
            if($input->getAttribute('name') == 'ERROR') {
                throw new \Exception($input->getAttribute('value'));
            }

            $data[$input->getAttribute('name')] = $input->getAttribute('value');
        }

        return $data;
    }

    /**
     * Создаем и отправляем код e-Voucher
     *
     * @param array $options
     * @return array
     * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function transferVoucher(array $options = []): array
    {
        $currency = ($options['currency'] ?? 'USD');
        $array = [
            'Payer_Account' =>  $this->getAccount($currency),
            'Amount'        =>  $options['amount'],
        ];

        $response = $this->request('/ev_create', $array);
        $parser = new Document($response);
        if(!$parser->has('input')) {
            throw new \Exception('Транзакция не прошла');
        }

        $data = [];
        foreach ($parser->find('input') as $input) {
            if($input->getAttribute('name') == 'ERROR') {
                throw new \Exception($input->getAttribute('value'));
            }
            $data[$input->getAttribute('name')] = $input->getAttribute('value');
        }

        return $data;
    }

    /**
     * Получить историю кошелька PerfectMoney
     *
     *   $params = [
     *       'startday' => '09',
     *       'startmonth' => '09',
     *       'startyear' => '2015',
     *       'endday' => '04',
     *       'endmonth' => '10',
     *       'endyear' => '2015',
     *   ];
     *
     *
     * @param array $params
     * @return array|string
     */
    public function history(array $params = [])
    {
        $defaults = [
            'AccountID' => $this->parameters['account_id'],
            'PassPhrase' => $this->parameters['account_password'],
        ];

        $httpParams = http_build_query(array_merge($defaults, $params));
        $scriptUrl = "https://perfectmoney.is/acct/historycsv.asp?{$httpParams}";

        $f = fopen($scriptUrl, 'rb');

        if ($f === false) {
            return 'error openning url';
        }

        $lines = array();
        while (!feof($f)) array_push($lines, trim(fgets($f)));

        fclose($f);
        $ar = array();
        if ($lines[0] != 'Time,Type,Batch,Currency,Amount,Fee,Payer Account,Payee Account,Payment ID,Memo') {
            // print error message
            return $lines[0];
        } else {

            // do parsing
            $ar = array();
            $n = count($lines);
            for ($i = 1; $i < $n; $i++) {

                $item = explode(",", $lines[$i], 9);
                if (count($item) != 9) continue; // line is invalid - pass to next one
                $item_named['Time'] = $item[0];
                $item_named['Type'] = $item[1];
                $item_named['Batch'] = $item[2];
                $item_named['Currency'] = $item[3];
                $item_named['Amount'] = $item[4];
                $item_named['Fee'] = $item[5];
                $item_named['Payer Account'] = $item[6];
                $item_named['Payee Account'] = $item[7];
                $item_named['Memo'] = $item[8];
                array_push($ar, $item_named);
            }

            return $ar;
        }
    }

    /**
     * Запрос к PerfectMoney api
     *
     * @param string $method
     * @param array $params
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, array $params = [])
    {
        $data = array_merge([
            'AccountID' => $this->parameters['account_id'],
            'PassPhrase' => $this->parameters['account_password'],
        ], $params);

        $request = $this->client->post("/acct/{$method}.asp", [
            'form_params' => $data
        ]);

        return $request->getBody()->getContents();
    }

    /**
     * Получаем номер счета
     * @param $currency
     * @return mixed
     * @throws \Exception
     */
    protected function getAccount($currency): string
    {
        return $this->parameters['account_'.Str::lower($currency)];
    }
}
