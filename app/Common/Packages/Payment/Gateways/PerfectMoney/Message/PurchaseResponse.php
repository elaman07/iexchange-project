<?php

namespace App\Common\Packages\Payment\Gateways\PerfectMoney\Message;


use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RequestInterface
};

class PurchaseResponse extends AbstractResponse
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl(): string
    {
        return 'https://perfectmoney.com/api/step1.asp';
    }

    public function getRedirectMethod(): string
    {
        return 'POST';
    }

    public function getRedirectData(): array
    {
        return [
            'order_id'  =>  $this->request->getTransactionId()
        ];
    }
}
