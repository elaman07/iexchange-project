<?php


namespace App\Common\Packages\Payment\Gateways\PerfectMoney\Message;


use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Common\Packages\Payment\Gateways\PerfectMoney\Gateway;
use Illuminate\Support\Str;


abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить ID аккаунта
     *
     * @return string
     */
    public function getAccountId() : string
    {
        return $this->getParameter('account_id');
    }

    /**
     * Установка ID аккаунта
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountId(string $value): AbstractRequest
    {
        return $this->setParameter('account_id', $value);
    }

    /**
     * Получить Пароль от аккаунта
     *
     * @return string
     */
    public function getAccountPassword() : string
    {
        return $this->getParameter('account_password');
    }

    /**
     * Установка Пароль от аккаунта
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountPassword(string $value): AbstractRequest
    {
        return $this->setParameter('account_password', $value);
    }

    /**
     * Получить USD номер кошелька
     *
     * @return string
     */
    public function getAccountUsd() : string
    {
        return $this->getParameter('account_usd');
    }

    /**
     * Установка USD номер кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountUsd(string $value): AbstractRequest
    {
        return $this->setParameter('account_usd', $value);
    }

    /**
     * Получить EUR номер кошелька
     *
     * @return string
     */
    public function getAccountEur() : string
    {
        return $this->getParameter('account_eur');
    }

    /**
     * Установка EUR номер кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountEur(string $value): AbstractRequest
    {
        return $this->setParameter('account_eur', $value);
    }

    /**
     * Получить Gold номер кошелька
     *
     * @return string
     */
    public function getAccountGold() : string
    {
        return $this->getParameter('account_gold');
    }

    /**
     * Установка Gold номер кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountGold(string $value): AbstractRequest
    {
        return $this->setParameter('account_gold', $value);
    }

    /**
     * Получить BTC номер кошелька
     *
     * @return string
     */
    public function getAccountBtc() : string
    {
        return $this->getParameter('account_btc');
    }

    /**
     * Установка BTC номер кошелька
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountBtc(string $value): AbstractRequest
    {
        return $this->setParameter('account_btc', $value);
    }

    /**
     * Получить Имя продавца
     *
     * @return string
     */
    public function getAccountName() : string
    {
        return $this->getParameter('account_name');
    }

    /**
     * Установка Имя продавца
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAccountName(string $value): AbstractRequest
    {
        return $this->setParameter('account_name', $value);
    }

    /**
     * Получить Альтернативной кодовой фразы
     *
     * @return string
     */
    public function getAlternatePhrase() : string
    {
        return $this->getParameter('alternate_phrase');
    }

    /**
     * Установка Альтернативной кодовой фразы
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setAlternatePhrase(string $value): AbstractRequest
    {
        return $this->setParameter('alternate_phrase', $value);
    }


    /**
     * Получаем номер счета
     *
     * @throws \Exception
     */
    public function getAccount(): string
    {
        switch (Str::upper($this->getCurrency()))
        {
            case 'USD':
                return $this->getAccountUsd();
                break;

            case 'EUR':
                return $this->getAccountEur();
                break;

            case 'BTC':
                return $this->getAccountBtc();
                break;
            case 'GOLD':
                return $this->getAccountGold();
                break;
            default:
                throw new \Exception('Не выбран код валюты');
        }


    }

}
