<?php

namespace App\Common\Packages\Payment\Gateways\PerfectMoney\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     * @throws \Exception
     */
    public function getData()
    {
        $this->validate(
            'amount', 'currency', 'transactionId'
        );

        $payment_extra_fields = $this->getPaymentExtraFields();
        switch ($this->getOrderData()->merchant->method_pay)
        {
            case '1':
                $method = 'voucher';
                break;
            default:
                $method = 'account';
        }

        return [
            'AVAILABLE_PAYMENT_METHODS' => $method,
            'PAYEE_ACCOUNT'         =>  $this->getAccount(),
            'PAYEE_NAME'            =>  $this->getAccountName(),
            'PAYMENT_UNITS'         =>  $this->getCurrency(),
            'PAYMENT_ID'            =>  $this->getTransactionId(),
            'PAYMENT_AMOUNT'        =>  $this->getAmount(),
            'STATUS_URL'            =>  $this->getNotifyUrl(),
            'PAYMENT_URL'           =>  $this->getReturnUrl(),
            'PAYMENT_URL_METHOD'    =>  'GET',
            'NOPAYMENT_URL'         =>  $this->getCancelUrl(),
            'NOPAYMENT_URL_METHOD'  =>  'GET',
            'SUGGESTED_MEMO'        =>  $this->getDescription(),
            'SUGGESTED_MEMO_NOCHANGE' => '1',
            'sEmail'                =>  $payment_extra_fields->getClient()->email,
            'BAGGAGE_FIELDS'        =>  'sEmail'
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
