<?php


namespace App\Common\Packages\Payment\Gateways\PerfectMoney\Message;

use App\Common\Packages\Payment\Engines\Message\{AbstractResponse, RequestInterface};
use App\Common\Packages\Payment\Exception\InvalidResponseException;

class CompletePurchaseResponse extends AbstractResponse
{
    /**
     * @var CompletePurchaseRequest|RequestInterface
     */
    protected $request;

    /**
     * @param RequestInterface $request
     * @param $data
     * @throws InvalidResponseException
     */
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);

        // Проверяем, если подписи не совместимы.
        if($this->getSign() !== $this->signatureResponse()) {
            throw new InvalidResponseException('Хэш обратного вызова не соответствует ожидаемому значению');
        }
    }

    /**
     * Если транзакция успешна
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->getSign() == $this->signatureResponse();
    }

    /**
     * ID заявки от платежной системы
     *
     * @return string | integer
     */
    public function getTransferId()
    {
        return $this->data['V2_HASH'];
    }

    /**
     * ID заявки (с сайта)
     *
     * @return string | integer
     */
    public function getTransactionId()
    {
        return $this->data['PAYMENT_ID'];
    }

    /**
     * Получение суммы
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->data['PAYMENT_AMOUNT'];
    }

    /**
     * Получаем код валюты
     *
     * @return string|integer
     */
    public function getCurrency()
    {
        return $this->data['PAYMENT_UNITS'];
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
     */
    public function getFromAccount()
    {
        return $this->data['PAYER_ACCOUNT'];
    }

    /**
     * Получаем результат подписи с сервера
     *
     * @return string
     */
    public function getSign()
    {
        return $this->data['V2_HASH'];
    }

    /**
     * Получаем хэш по параметрам
     *
     * @return string
     */
    private function signatureResponse()
    {
        $data = implode(':', [
            $this->data['PAYMENT_ID'],
            $this->data['PAYEE_ACCOUNT'],
            $this->data['PAYMENT_AMOUNT'],
            $this->data['PAYMENT_UNITS'],
            $this->data['PAYMENT_BATCH_NUM'],
            $this->data['PAYER_ACCOUNT'],
            strtoupper(md5($this->request->getAlternatePhrase())),
            $this->data['TIMESTAMPGMT']
        ]);

        \Log::debug(strtoupper(md5($data)));

        \Log::debug(strtoupper(md5($this->request->getAlternatePhrase())));


        return strtoupper(md5($data));
    }
}
