<?php

namespace App\Common\Packages\Payment\Gateways\PerfectMoney;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'PerfectMoney';

    /**
     * Получить ID аккаунта
     *
     * @return string
     */
    public function getAccountId() : string
    {
        return $this->getParameter('account_id');
    }

    /**
     * Установка ID аккаунта
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountId(string $value): Gateway
    {
        return $this->setParameter('account_id', $value);
    }

    /**
     * Получить Пароль от аккаунта
     *
     * @return string
     */
    public function getAccountPassword() : string
    {
        return $this->getParameter('account_password');
    }

    /**
     * Установка Пароль от аккаунта
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountPassword(string $value): Gateway
    {
        return $this->setParameter('account_password', $value);
    }

    /**
     * Получить USD номер кошелька
     *
     * @return string
     */
    public function getAccountUsd() : string
    {
        return $this->getParameter('account_usd');
    }

    /**
     * Установка USD номер кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountUsd(string $value): Gateway
    {
        return $this->setParameter('account_usd', $value);
    }

    /**
     * Получить EUR номер кошелька
     *
     * @return string
     */
    public function getAccountEur() : string
    {
        return $this->getParameter('account_eur');
    }

    /**
     * Установка EUR номер кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountEur(string $value): Gateway
    {
        return $this->setParameter('account_eur', $value);
    }

    /**
     * Получить Gold номер кошелька
     *
     * @return string
     */
    public function getAccountGold() : string
    {
        return $this->getParameter('account_gold');
    }

    /**
     * Установка Gold номер кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountGold(string $value): Gateway
    {
        return $this->setParameter('account_gold', $value);
    }

    /**
     * Получить BTC номер кошелька
     *
     * @return string
     */
    public function getAccountBtc() : string
    {
        return $this->getParameter('account_btc');
    }

    /**
     * Установка BTC номер кошелька
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountBtc(string $value): Gateway
    {
        return $this->setParameter('account_btc', $value);
    }

    /**
     * Получить Имя продавца
     *
     * @return string
     */
    public function getAccountName() : string
    {
        return $this->getParameter('account_name');
    }

    /**
     * Установка Имя продавца
     *
     * @param string $value
     * @return Gateway
     */
    public function setAccountName(string $value): Gateway
    {
        return $this->setParameter('account_name', $value);
    }

    /**
     * Получить Альтернативной кодовой фразы
     *
     * @return string
     */
    public function getAlternatePhrase() : string
    {
        return $this->getParameter('alternate_phrase');
    }

    /**
     * Установка Альтернативной кодовой фразы
     *
     * @param string $value
     * @return Gateway
     */
    public function setAlternatePhrase(string $value): Gateway
    {
        return $this->setParameter('alternate_phrase', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'account_id' => '',
            'account_password' => '',
            'account_usd' => '',
            'account_eur' => '',
            'account_gold' => '',
            'account_btc' => '',
            'account_name' => '',
            'alternate_phrase' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
