<?php

namespace App\Common\Packages\Payment\Gateways\Alikassa;

use App\Common\Packages\Payment\Engines\AbstractGateway;
use App\Common\Packages\Payment\Engines\Message\AbstractRequest;

class Gateway extends AbstractGateway
{
    /**
     * Название Платежной системы
     *
     * @var string
     */
    protected string $name = 'Alikassa';

    /**
     * Получить E-mail адрес
     *
     * @return string
    */
    public function getAccountEmail() : string
    {
        return $this->getParameter('account_email');
    }

    /**
     * Установка E-mail адрес
     *
     * @param string $value
     * @return Gateway
     */
    public function setMerchantId(string $value): Gateway
    {
        return $this->setParameter('account_email', $value);
    }

    /**
     * Получить Merchant UUID
     *
     * @return string
     */
    public function getMerchantUuid(): string
    {
        return $this->getParameter('merchant_uuid');
    }

    /**
     * Установка  Merchant UUID
     *
     * @param string $value
     * @return Gateway
     */
    public function setMerchantUuid(string $value): Gateway
    {
        return $this->setParameter('merchant_uuid', $value);
    }

    /**
     * Получить Секретный ключи
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключи
     *
     * @param string $value
     * @return Gateway
     */
    public function setSecretKey(string $value): Gateway
    {
        return $this->setParameter('secret_key', $value);
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
    */
    public function getDefaultParameters(): array
    {
        return array(
            'account_email' => '',
            'merchant_uuid' => '',
            'secret_key' => ''
        );
    }

    /**
     * Отправляем запрос на покупку
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function purchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getPurchaseRequest(), $parameters);
    }

    /**
     * Обработка данных, после ответа от платежного шлюза
     *
     * @param array $parameters
     * @return AbstractRequest
     */
    public function completePurchase(array $parameters = []): AbstractRequest
    {
        return $this->createRequest($this->getCompletePurchase(), $parameters);
    }

    /**
     *  Получение данных от базовой версии Api
    */
    public function api()
    {
        return $this->apiService($this->getParameters());
    }
}
