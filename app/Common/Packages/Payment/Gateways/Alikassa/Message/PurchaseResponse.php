<?php

namespace App\Common\Packages\Payment\Gateways\Alikassa\Message;

use App\Common\Packages\Payment\Engines\Message\{
    AbstractResponse,
    RedirectResponseInterface,
    RequestInterface
};

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    public function __construct(RequestInterface $request, $data)
    {
        parent::__construct($request, $data);
    }

    public function isSuccessful(): bool
    {
        return false;
    }

    public function isRedirect(): bool
    {
        return true;
    }

    public function getRedirectUrl()
    {
        return 'https://sci.alikassa.com/payment';
    }

    public function getRedirectMethod(): string
    {
        return 'POST';
    }

    public function getRedirectData()
    {
        return [];
    }
}

