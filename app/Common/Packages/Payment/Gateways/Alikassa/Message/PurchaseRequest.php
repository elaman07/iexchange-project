<?php

namespace App\Common\Packages\Payment\Gateways\Alikassa\Message;


use App\Common\Packages\Payment\Engines\Message\ResponseInterface;
use App\Common\Packages\Payment\Exception\InvalidRequestException;
use Illuminate\Support\Facades\Request;

class PurchaseRequest extends AbstractRequest
{
    /**
     * Получить массив необработанных данных для этого сообщения.
     * Формат этого варьируется от шлюза к шлюзу,
     * но обычно это либо ассоциативный массив, либо SimpleXMLElement.
     *
     * @return mixed
     * @throws InvalidRequestException
     */
    public function getData()
    {
        $this->validate(
            'merchant_uuid', 'currency', 'amount', 'transactionId'
        );

        $payment_extra_fields = $this->getPaymentExtraFields();
        switch ($payment_extra_fields->getDirectionExchange()->currency1->merchant->method_pay)
        {
            case '1':
                $method = 'Card';
                break;
            case '2':
                $method = 'BTC';
                break;
            case '3':
                $method = 'LTC';
                break;
            case '4':
                $method = 'DASH';
                break;
            case '5':
                $method = 'ZEC';
                break;
            default:
                $method = 'AliKassa';
        }

        return [
            'merchantUuid'      =>  $this->getMerchantUuid(),
            'customerIp'        =>  $this->getClientIp(),
            'orderId'           =>  $this->getTransactionId(),
            'amount'            =>  $this->getAmount(),
            'currency'          =>  $this->getCoinCurrency(),
            'desc'              =>  $this->getDescription(),
            'payWayVia'         =>  $method,
            'sign'              =>  $this->signatureResponse(['payWayVia' => $method])
        ];
    }

    /**
     * Отправить запрос с указанными данными
     *
     * @param  mixed $data
     * @return ResponseInterface
     */
    public function sendData($data)
    {
        return $this->response = new PurchaseResponse($this, $data);
    }
}
