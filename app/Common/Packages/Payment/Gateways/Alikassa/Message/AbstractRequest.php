<?php

namespace App\Common\Packages\Payment\Gateways\Alikassa\Message;

use App\Common\Packages\Payment\Engines\Message\AbstractRequest as BaseAbstractRequest;

abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить Merchant UUID
     *
     * @return string
     */
    public function getMerchantUuid(): string
    {
        return $this->getParameter('merchant_uuid');
    }

    /**
     * Установка  Merchant UUID
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setMerchantUuid(string $value): AbstractRequest
    {
        return $this->setParameter('merchant_uuid', $value);
    }

    /**
     * Получить Секретный ключи
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secret_key');
    }

    /**
     * Установить Секретный ключи
     *
     * @param string $value
     * @return AbstractRequest
     */
    public function setSecretKey(string $value): AbstractRequest
    {
        return $this->setParameter('secret_key', $value);
    }


    /**
     * Подписать транзакцию
     *
     * @throws \App\Common\Packages\Payment\Exception\InvalidRequestException
     */
    public function signatureResponse(array $options = []): string
    {
        return base64_encode(hash('sha256', implode(':', array_filter([
            $this->getAmount(),
            $this->getCoinCurrency(),
            $this->getClientIp(),
            $this->getDescription(),
            $this->getMerchantUuid(),
            $this->getTransactionId(),
            $options['payWayVia'],
            $this->getSecretKey()
        ], 'strlen')), true));
    }
}
