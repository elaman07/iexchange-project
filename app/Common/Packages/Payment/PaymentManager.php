<?php


namespace App\Common\Packages\Payment;

use App\Common\Packages\Payment\Engines\GatewayFactory;
use Defuse\Crypto\Exception\EnvironmentIsBrokenException;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;
use Illuminate\Foundation\Application;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class PaymentManager
{
    /**
     * Экземпляр приложения.
     *
     * @var Application
     */
    protected Application $app;

    /**
     * Доступные адаптеры платежных систем
    */
    protected array $gateways = [];

    /**
     * Зарегистрированные пользовательские создатели драйверов.
     *
     * @var array
     */
    protected array $customCreators = [];


    protected GatewayFactory $factory;

    /**
     * Создайте новый экземпляр менеджера.
     *
     * @param Application $app
     * @param GatewayFactory $factory
     */
    public function __construct(Application $app, GatewayFactory $factory)
    {
        $this->app = $app;
        $this->factory = $factory;
    }

    /**
     * Получите экземпляр драйвера платежных систем
     *
     * @param string $name
     * @param string $config_id
     * @return mixed
     * @throws EnvironmentIsBrokenException
     * @throws WrongKeyOrModifiedCiphertextException
     */
    public function merchant(string $name, string $config_id)
    {
        $driver = Str::studly($name);
        return $this->gateways[$driver] = $this->get($driver, $config_id, 'merchant');
    }


    /**
     * Доступ к API платежным система
     *
     * @param string $name
     * @param string $config_id
     * @return mixed
     * @throws EnvironmentIsBrokenException
     * @throws WrongKeyOrModifiedCiphertextException
     */
    public function pay(string $name, string $config_id)
    {
        $driver = Str::studly($name);
        return $this->gateways[$driver] = $this->get($driver, $config_id, 'pay');
    }

    /**
     * Получение расположения по названию шлюза
     *
     * @param $driver
     * @param $config
     * @return Engines\Interfaces\GatewayInterface|mixed
     */
    protected function adapt($driver, $config)
    {
        if (!isset($this->gateways[$driver])) {
            $gateway = $this->factory->create($driver, null, $this->app['request']);
            $gateway->initialize($config);

            $this->gateways[$driver] = $gateway;
        }

        return $this->gateways[$driver];
    }

    /**
     * Попытайтесь получить платеж из локального кеша.
     *
     * @param string $name
     * @param $config_id
     * @param string $type
     * @return mixed
     * @throws EnvironmentIsBrokenException
     * @throws WrongKeyOrModifiedCiphertextException
     */
    protected function get(string $name, $config_id, string $type)
    {
        return $this->adapters[$name] ?? $this->resolve($name, $config_id, $type);
    }

    /**
     * Конфигурационные настройки мерчанта
     *
     * @param string $name
     * @return array
     */
    public function configProvider(string $name): array
    {
        try {
            return json_decode(
                trim(file_get_contents(__DIR__.'/Config/'.Str::lower(\trim($name)).'.json')), true
            );
        }catch (\TypeError $exception) {
            return [];
        }
    }

    /**
     * Resolve the given store.
     *
     * @param string $name
     * @param int $config_id
     * @param string|null $type
     * @return mixed
     * @throws EnvironmentIsBrokenException
     * @throws WrongKeyOrModifiedCiphertextException
     */
    protected function resolve(string $name, int $config_id = 0, string $type = null)
    {
        $lower_name = Str::lower($name);
        $config = protect_gateway_keys($lower_name, $config_id, $type);

        // Получаем список провайдеров
        $finder = Finder::create();

        $finder
            ->in(__DIR__.'/Gateways')
            ->depth(0)
            ->filter(static function (SplFileInfo $file) {
                return $file->isDir() || \preg_match('/\.(php|json)$/', $file->getFilename());
            });

        $providers = [];
        foreach (iterator_to_array($finder, true) as $item) {
            $providers[\Str::lower($item->getFilename())] = $item->getFilename();
        }

        // Для определенных платежных шлюзов, несоздаем новые методы
        if(isset($providers[$lower_name])) {
            $register_name = $providers[$lower_name];
            return $this->adapt($register_name, $config);
        } else {
            $driverMethod = 'create'.ucfirst($name).'Driver';
            if (method_exists($this, $driverMethod)) {
                return $this->{$driverMethod}($config);
            } else {
                throw new InvalidArgumentException("Driver [{$name}] is not supported.");
            }
        }
    }

    /**
     * Вызовите создателя настраиваемого драйвера.
     *
     * @param array $config
     * @return mixed
     */
    protected function callCustomCreator(array $config)
    {
        return $this->customCreators[$config['driver']]($this->app, $config);
    }
}
