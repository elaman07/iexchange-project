<?php


namespace App\Common\Packages\Payment\Exception;


/**
 * Runtime Exception
 */
class RuntimeException extends \RuntimeException implements PaymentException
{
}
