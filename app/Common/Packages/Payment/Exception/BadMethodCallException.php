<?php


namespace App\Common\Packages\Payment\Exception;


/**
 * Bad Method Call Exception
 */
class BadMethodCallException extends \BadMethodCallException implements PaymentException
{
}
