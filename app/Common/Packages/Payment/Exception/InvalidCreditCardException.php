<?php


namespace App\Common\Packages\Payment\Exception;


class InvalidCreditCardException extends \Exception implements PaymentException
{

}
