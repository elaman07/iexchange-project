<?php
namespace App\Common\Packages\Editor;


use App\Common\Packages\Editor\Adapters\BBCodeAdapter;
use App\Common\Packages\Editor\Adapters\MailCodeAdapter;
use Illuminate\Support\Manager;

class EditorManager extends Manager
{
    /**
     * Драйвер BB Code
     */
    public function createBBCodeDriver()
    {
        return new BBCodeAdapter();
    }

    /**
     * Драйвер BB Code
     */
    public function createMailCodeDriver()
    {
        return new MailCodeAdapter();
    }

    /**
     * Драйвер по умолчанию
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return 'bbcode';
    }
}