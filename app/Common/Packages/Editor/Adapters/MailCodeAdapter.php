<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.09.2020
 * Time: 17:46
 */

namespace App\Common\Packages\Editor\Adapters;


class MailCodeAdapter
{
    /**
     *  Request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    protected $rules = [
        '[site_name]',
        '[default_email_from]',
        '[server_name]'
    ];

    protected $replace = [];

    public function __construct()
    {
        $this->request = \request();
        $this->replace = [
            iEXContentLanguage('sitename'),
            iEXSetting('project_email_support'),
            $this->request->getHttpHost()
        ];
    }

    public function build(string $input, array $rules, $replace = [])
    {
        return str_replace(array_merge($this->rules, $rules), array_merge($this->replace, $replace), $input);
    }
}
