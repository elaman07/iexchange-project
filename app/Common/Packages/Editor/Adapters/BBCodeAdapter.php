<?php
namespace App\Common\Packages\Editor\Adapters;


use App\Models\Task;

class BBCodeAdapter
{
    /**
     *  Request
     *
     * @var \Illuminate\Http\Request
    */
    protected $request;

    protected $rules = [
        '[order_id]',
        '[created_at]',
        '[updated_at]',
        '[ip_address]',
        '[email]',
        '[income_amount]',
        '[outcome_amount]',
        '[income_currency]',
        '[outcome_currency]',
        '[course]',
        '[unique_code]',
        '[check_url]',
        '[app_name]',
        '[public_id]'
    ];

    public function __construct()
    {
        $this->request = \request();
    }

    public function order(string $input, $task)
    {
        $replace = [
            $task->id,
            $task->created_at,
            $task->updated_at,
            $task->ip,
            $task->email,
            $task->give_price,
            $task->receiving_price,
            $task->direction_exchange->currency1->payment->name.' '.$task->direction_exchange->currency1->code_currency->name,
            $task->direction_exchange->currency2->payment->name.' '.$task->direction_exchange->currency2->code_currency->name,
            $task->course_display,
            $task->unique_security_code,
            url('/check/'.$task->transaction->transaction),
            iEXContentLanguage('sitename'),
            $task->public_id
        ];

        return str_replace($this->rules, $replace, $input);
    }
}
