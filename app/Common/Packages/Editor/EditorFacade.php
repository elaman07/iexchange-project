<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 04.01.2020
 * Time: 12:26
 */

namespace App\Common\Packages\Editor;

use Illuminate\Support\Facades\Facade;

class EditorFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     * @throws \Exception
     */
    protected static function getFacadeAccessor()
    {
        return 'editor';
    }
}