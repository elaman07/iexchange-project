<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 04.01.2020
 * Time: 12:13
 */

namespace App\Common\Packages\Editor;

use App\Common\Packages\Crypto\Compilers\Compiler;
use App\Common\Packages\Crypto\Contracts\CompilerFactory;
use App\Common\Packages\Crypto\Contracts\Factory;
use Illuminate\Support\ServiceProvider;

class EditorServiceProvider extends ServiceProvider
{
    public function boot() {

    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('editor', function($app) {
            return new EditorManager($app);
        });
    }
}