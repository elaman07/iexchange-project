<?php

declare(strict_types=1);

namespace App\Common\Packages\XSSFilter;

use Illuminate\Support\Arr;
use voku\helper\AntiXSS;

class Cleaner
{
    /**
     * @var string
     */
    protected $scriptsAndIframesPattern = '/(<script.*script>|<frame.*frame>|<iframe.*iframe>|<object.*object>|<embed.*embed>)/isU';

    /**
     * @var string
     */
    protected $inlineListenersPattern = '/on.*=\".*\"(?=.*>)/isU';

    /**
     * Clean
     *
     * @param string $value
     *
     * @return string
     */
    public function clean(string $value): string
    {
        $value = $this->escapeScriptsAndIframes($value);
        $value = $this->removeInlineEventListeners($value);

        return $value;
    }

    /**
     * Extended clean XSS
     *
     * @param string $value
     * @return string
     */
    public function cleanV2(string $value): string  {
        $anti = new AntiXSS();
        $anti->removeEvilHtmlTags(['iframe']);
        return $anti->xss_clean($value);
    }

    /**
     * Escape Scripts And Iframes
     *
     * @param string $value
     *
     * @return string
     */
    protected function escapeScriptsAndIframes(string $value): string
    {
        preg_match_all($this->scriptsAndIframesPattern, $value, $matches);

        foreach (Arr::get($matches, '0', []) as $script) {
            $value = str_replace($script, e($script), $value);
        }

        return $value;
    }

    /**
     * Remove Inline Event Listeners
     *
     * @param string $value
     *
     * @return string
     */
    protected function removeInlineEventListeners(string $value): string
    {
        $string = preg_replace($this->inlineListenersPattern, '', $value);

        return empty($string) ? '' : $string;
    }
}