<?php

declare(strict_types=1);

namespace App\Common\Packages\XSSFilter;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string clean(string $value);
 * @method static string cleanV2(string $value);
 */
class XSSCleaner extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return Cleaner::class;
    }
}