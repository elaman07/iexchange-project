<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 16:25
 */

namespace App\Common\Packages\Cashback\Facades;

use Illuminate\Support\Facades\Facade;

class CashbackFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cashback';
    }
}
