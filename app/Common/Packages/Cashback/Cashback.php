<?php
namespace App\Common\Packages\Cashback;

use App\Models\Reward;
use App\Models\RewardLog;
use App\Models\UserBalance;

class Cashback extends AbstractModel implements CashbackContracts
{
    /**
     * Вызов результата из cashback
     *
     * @param bool $amount
     * @return float|void
     */
    public function call($amount = false)
    {
        // Не выплачиваем партнерские бонусы
        if($this->builder->getModel()->user->is_pay_cashback == 1)
            return;

        // Проверяем, не отключена ли возможность выплаты cashback через заявку
        if($this->builder->getModel()->task_info->is_switch_not_cashback == 1)
            return;

        $left = $this->builder->details('payment_name_left');
        $right = $this->builder->details('payment_name_right');
        $cashback = $this->builder->details('is_cashback');

        if($amount == false)
        {
            // Обновление тарифного плана
            $this->builder->update();

            RewardLog::create([
                'id_reward_link'    =>  $this->builder->getRewardId(),
                'id_user'           =>  $this->builder->getModel()->id_user,
                'bonus'             =>  $this->builder->getAmount(),
                'bonus_string'      =>  $this->builder->getAmountString(),
                'text'              =>  sprintf('Вознаграждение за обмен (%s -> %s)', $left, $right),
                'id_task'           =>  $this->builder->getModel()->id,
                'percent'           =>  $this->builder->getReward()->program->percent,
                'is_cashback'       =>  (int)$cashback
            ]);

            $this->builder->updateBalance();
        }

        return $this->builder->getAmount();
    }

    /**
     * Регистрация кэшбека
     *
     * @param $id_user
     */
    public function register($id_user)
    {
        $user_balance = UserBalance::where([
            ['id_user', $id_user]
        ])->exists();
        $reward_has = Reward::where('user_id', $id_user)->exists();

        iF(! $user_balance) {
            UserBalance::create([
                'id_user'   =>  $id_user,
                'id_code_currency'  =>  config('crypto.currency_id')
            ]);
        }

        if(! $reward_has) {
            Reward::create([
                'user_id'               =>  $id_user,
                'reward_program_id'     =>  1
            ]);
        }

    }
}