<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 18:02
 */

namespace App\Common\Packages\Cashback;


interface CashbackContracts
{
    public function call();
}