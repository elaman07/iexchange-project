<?php
namespace App\Common\Packages\Cashback\Concerns;

use App\Models\Reward;
use App\Models\RewardProgram;
use App\Models\TaskUser;
use App\Models\UserBalance;
use App\Models\VerificationCard;

trait QueriesHandler
{
    /**
     * Знаков, после запятой
     *
     * @return integer
    */
    private $decimal = 2;

    /**
     * Получение процента на текущий тарифный план
     *
     * @return float
     */
    public function getMaxPercent()
    {
        $max = Reward::where('user_id', $this->model->id_user)->first()->program->percent;

        // Дополнительный Cashback для верифицированных клиентов
        $card_percent = 0;
        if((int)iEXSetting('other_cashback_user_verification') > 0 and $this->isVerifiedCard()) {
            $card_percent = iEXSetting('other_cashback_user_verification');
        }

        return $max + $card_percent;
    }

    /**
     * Получение наименования текущего тарифного плана
     *
     * @return float
     */
    public function getName()
    {
        return RewardProgram::whereBetween('amount', [0, $this->convertTo('USD')])->max('name');
    }

    /**
     * Получение ID текущего тарифного плана
     *
     * @return integer
    */
    public function getRewardId()
    {
        return Reward::where('user_id', $this->model->id_user)->first()->reward_program_id;
    }

    /**
     * Статус верификации счета
     *
     * @return boolean
     */
    public function isVerifiedCard()
    {
        $card = preg_replace('/\s/', '', $this->model->from_shot);
        $has = VerificationCard::where([
            ['id_user', $this->model->id_user],
            ['card_number', $card],
            ['id_currency', $this->currencies],
            ['status', 1]
        ])->exists();

        return ($has ? true : false);
    }

    /**
     * Детали бонусной программы
     *
     * @return \App\Models\Reward
    */
    public function getReward() {
        return Reward::whereUserId($this->model->id_user)->first();
    }

    /**
     * Получение цену вознаграждения
     *
     * @return float
     */
    public function getAmount()
    {
        // Проверяем, доступны ли выплаты бонусов по обменному направлению
        if($this->isNotCashback())
            return floor(0);

        return iex_number_format($this->convertBonusToCurrency() * ($this->getMaxPercent() / 100), $this->decimal);
    }

    /**
     * Получение сумму бонуса в строковом формате
     *
     * @return string
     */
    public function getAmountString()
    {
        // Отображения бонуса
        $view_bonus = sprintf('%s %s', $this->getAmount(), $this->getCurrencyPayout(true));
        if(config('crypto.currency_payout_position') == 'left')
            $view_bonus = sprintf('%s %s',$this->getCurrencyPayout(true), $this->getAmount());
        return $view_bonus;
    }


    /**
     * Получаем код валюты
     *
     * @param bool $is_read
     * @return string
     */
    public function getCurrencyPayout(bool $is_read = false)
    {
        if($is_read)
            return config('crypto.currency_payout_sign');
        return config('crypto.currency_payout');
    }

    /**
     * Конвертировать бонус в другой формат
     *
     * @return string
     */
    public function convertBonusToCurrency()
    {
        return $this->convertTo($this->getCurrencyPayout());
    }

    /**
     * Получение баланса рефералла
     *
     * @return float
     */
    public function getRewardBalance()
    {
        $balance = UserBalance::where('id_user', $this->model->id_user)->first();
        return $balance->balance_reward;
    }

    /**
     * Обновление баланса рефералла
     *
     * @return void
     */
    public function updateBalance()
    {
        UserBalance::where('id_user', $this->model->id_user)
            ->update(['balance_reward' => iex_number_format($this->getRewardBalance() + $this->getAmount(), 2)]);

    }


    /**
     * Обновление тарифного плана кэшбэка
     *
     * @return void
     */
    public function update()
    {
        $task_user = TaskUser::where('id_user', $this->model->id_user)->sum('convert_to_usd');
        $bonus_id = RewardProgram::whereBetween('amount',
            [0, floatval(iex_number_format($task_user, 0))])
            ->max('id');

        Reward::where('user_id', $this->model->id_user)->update([
            'reward_program_id' => (!$bonus_id or $bonus_id == 0) ? 1 : $bonus_id
        ]);
    }

    /**
     * Разбираем и обрабатываем значение с авто определением
     *
     * @param string $value
     * @param $summa
     * @return float
     */
    public function addCommissionDynamicAuto(string $value, $summa, $number_format = 8): float
    {
        $value_number = preg_replace('/\D/', '', $value); // Получаем число
        $char_first = mb_substr($value, 0, 1); // Получение первого символа
        $char_last = mb_substr($value, -1); // Получение последнего символа

        // Если нет процента то не пропускаем дальше
        if($value_number == 0) {
            return $summa;
        }

        $first_summa = $summa;
        if(\mb_substr(iex_number_format($first_summa, $number_format), 0, 2) == '0') {
            $summa = 1 / $summa;
        }

        $value_n = $value_number;
        if($char_last == '%') {
            $value_n = ($value_number > 0) ? ($value_number / 100) * $summa : 0;
        }


        // Умножение
        if($char_first == '*') {
            $summa = $summa * $value_n;
        }

        // Деление
        if($char_first == '/') {
            $summa = $summa / $value_n;
        }

        // Вычитание
        if($char_first == '-') {
            $summa = $summa - $value_n;
        }

        if(!in_array($char_first, ['*', '/', '-'])) {
            $summa = $summa + $value_n;
        }

        if(\mb_substr(iex_number_format($first_summa, $number_format), 0, 2) == '0') {
            $summa = (1 / $summa);
        }

        return $summa;
    }
}
