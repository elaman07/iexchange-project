<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 18:17
 */

namespace App\Common\Packages\Cashback\Concerns;


use App\Models\Reward;
use App\Models\User;
use App\Models\UserBalance;

trait BuildsBoot
{
    /**
     * Код валюты в которой будут начисляться вознагражение
     *
     * @var integer
    */
    private $default_code = 1;

    /**
     * Система вознаграждений которая закрепляется по умолчанию
     *
     * @var integer
    */
    private $default = 1;


    /**
     * Определите, была ли создана колонка вознаграждения.
     *
     * @return boolean
     */
    private function hasRewardBootstrapped()
    {
        return Reward::where('user_id', $this->model->id_user)->exists();
    }

    /**
     * Определите, был ли создан баланс для пользователя
    */
    private function hasRewardBalance()
    {
        return UserBalance::where([['id_user', $this->model->id_user], ['id_code_currency', 1]])->exists();
    }

    /**
     * Выплачить кэшбэк
     *
     * @return bool
     */
    public function isNotCashback() {
        return $this->exchange->is_not_bonus;
    }


    /**
     * Создаем базового загрузчика
     *
     * @return void
    */
    public function bootstrap()
    {
        if(! $this->hasRewardBalance())
        {
            UserBalance::create([
                'id_user'   =>  $this->model->id_user,
                'id_code_currency'  =>  1
            ]);
        }

        if(! $this->hasRewardBootstrapped())
        {
            Reward::create([
                'user_id'               =>  $this->model->id_user,
                'reward_program_id'     =>  $this->default
            ]);
        }
    }

    /**
     * Информация о клиенте
     *
     * @return User
    */
    public function getClient() {
        return $this->getModel()->user;
    }
}