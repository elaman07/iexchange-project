<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 16:25
 */

namespace App\Common\Packages\Cashback;

use Illuminate\Support\ServiceProvider;

class CashbackServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('cashback', function() {
            return new Cashback();
        });
    }
}