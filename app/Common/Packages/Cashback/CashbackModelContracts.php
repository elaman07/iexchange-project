<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 16:56
 */

namespace App\Common\Packages\Cashback;


use App\Models\Task;

interface CashbackModelContracts
{
    /**
     * Установить новые данные "Заявок"
     *
     * @param Task $task
     * @return $this
     */
    public function setModel(Task $task);

    /**
     * Получение данных
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel();
}