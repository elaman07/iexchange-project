<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 17:59
 */

namespace App\Common\Packages\Cashback\Exceptions;


use Exception;

class NotFoundException  extends Exception
{
    //
}