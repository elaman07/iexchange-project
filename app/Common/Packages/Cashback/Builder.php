<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 22.02.2018, 16:51
 */

namespace App\Common\Packages\Cashback;

use App\Common\Packages\Cashback\Exceptions\NotFoundException;
use App\Models\CashbackErrorLog;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\Reward;
use App\Models\Task;
use Illuminate\Support\Str;

class Builder
{
    use Concerns\QueriesHandler,
        Concerns\BuildsBoot;

    /**
     * Пример модели Task.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Пример модели Currency
     *
     * @var \App\Models\Currency
     */
    protected $currencies;

    /**
     * Пример модели Direction Exchange
     *
     * @var \App\Models\DirectionExchange
     */
    protected $exchange;

    /**
     * Создайте новый экземпляр работы с Кэшбэк`ом
     *
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->model        =   $task;
        $this->exchange     =   DirectionExchange::find($this->model->id_direction_exchange);
        $this->currencies   =   $this->exchange->currency1;

        $this->bootstrap();
    }

    /**
     * Разобранные детали
     *
     * @param $switch
     * @return string
     */
    public function details($switch)
    {
        switch ($switch)
        {
            case 'is_cashback':
                return ($this->exchange->is_not_bonus ? 0 : 1);
                break;

            case 'code_name':
                return $this->exchange->currency1->code_currency->name;
                break;

            case 'payment_name_left':
                return $this->exchange->currency1->payment->name.' '.$this->exchange->currency1->code_currency->name;
                break;

            case 'payment_name_right':
                return $this->exchange->currency2->payment->name.' '.$this->exchange->currency2->code_currency->name;
                break;
        }
    }

    /**
     * Конверируем полученную сумму в доллары
     *
     * @param $switch
     * @return float
     */
    protected function convertTo($switch)
    {
        $in = remove_all_spaces(Str::upper($this->details('code_name')));
        $out = remove_all_spaces(Str::upper($switch));

        $parser_summa = $this->exchange->currency1->code_currency->internal_rate;
        // Берем курс из источников + добавляем к курсу
        if(isset($this->exchange->currency1->code_currency->parser_exchange))
        {
            $parser = $this->exchange->currency1->code_currency->parser_exchange;
            $parser_summa = $this->addCommissionDynamicAuto($this->exchange->currency1->code_currency->add_to_course, $parser->summa_default, $this->exchange->currency1->number_format);
        }

        // Если 0, то расчитываем автоматически
        if($parser_summa == 0) {
            $response = convert_to_currency($in, $out, (float)$this->model->give_price);
        } else {
            $response = $parser_summa * $this->model->give_price;
        }



        if($response == 0) {
            CashbackErrorLog::create([
                'id_task' => $this->model->id,
                'id_user' => $this->model->id_user,
                'in_code' => $in,
                'out_code' => $out,
                'text' => 'В модуле "Работа парсеров - Список пар" проверьте, добавлена ли пара '.$in.' > '. $out
            ]);
        }


        return $response;
    }

    /**
     * Записываем данные в паременную
     *
     * @param Task $task
     * @return $this
     */
    public function setModel(Task $task)
    {
        $this->model = $task;
        return $this;
    }

    /**
     * Получение данных
     *
     * @return \App\Models\Task
     */
    public function getModel()
    {
        return $this->model;
    }
}
