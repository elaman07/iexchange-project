<?php
namespace App\Common\Packages\Cashback;

use App\Models\Task;

abstract class AbstractModel
{
    /**
     * Реализация строителя.
     *
     * @var Builder
     */
    protected $builder;

    /**
     * Создайте новый экземпляр кэшбэка
     *
     * @param Task $task
     * @return $this
     */
    public function make(Task $task)
    {
        $this->builder = new Builder($task);
        return $this;
    }
}