<?php

namespace App\Common\Packages\ImageSystem\Concerns;

use Illuminate\Support\Facades\Storage;

trait ManagesStorage
{
    /**
     * Загрузка файлов через удаленный сервер (FTP, S3 ...)
     *
     * @param string $path
     * @param string|null $old_filename
     */
    public function saveToRemoteDisk(string $path, string $old_filename = null): void
    {
        $filename = $this->randomName(); // Получаем имя файла
        // Разбиваем путь на несколько частей
        $path = explode('/', $path);
        $fullPath = sprintf('%s/%s', $path[1], $filename);
        $oldFullPath = !is_null($old_filename) ? sprintf('%s/%s', $path[1], $old_filename) : null;

        $storage = Storage::disk($this->getStorageSource());
        // Удаляем старый файл
        if(!empty($oldFullPath) and $storage->exists($oldFullPath)) {
            $storage->delete($oldFullPath);
        }
        $storage->put($fullPath, $this->data->getContent());

        // Записываем имя файла, для сохранения
        $this->setFileName($filename);
    }
}
