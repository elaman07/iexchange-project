<?php

namespace App\Common\Packages\ImageSystem\Concerns;

use Intervention\Image\Facades\Image;

trait ManagesLocal
{
    /**
     * Загрузка файлов через локальный источник
     *
     * @param string $path
     * @param string|null $old_filename
     */
    public function saveToLocal(string $path, string $old_filename = null): void
    {
        // Полный путь
        $oldFullPath = !empty($old_filename) ? sprintf('%s/%s', $path, $old_filename) : null;
        if(!empty($oldFullPath)) {
            iex_file_delete($oldFullPath);
        }

        if(in_array($this->extension, ['png', 'jpeg', 'jpg', 'bmp', 'gif'])) {
            $this->imageIntervention($path);
        } elseif(in_array($this->extension, ['webp', 'svg'])) {
            $this->defaultUploaded($path);
        }
    }

    /**
     * Загрузка файлов, через плагин Intervention Image (Для локальных данных)
     *
     * @param string $path
     */
    protected function imageIntervention(string  $path): void
    {
        $image = Image::make($this->data);

        // Если выбрано значение, то конвертируем загружаемые изображения
        // в выбранный формат из настроек изображений
        if(!empty(iEXSetting('images_converted_to')))
        {
            $this->setExtension(\Str::lower(iEXSetting('images_converted_to')));
            $image->encode(iEXSetting('images_converted_to'));
        }

        // Максимально допустимые размеры оригинального изображения
        $resize = iEXSetting('images_max_up_side');
        if($resize != 0)
        {
            $resize_explode = explode('x', $resize);
            $image->resize($resize_explode[0], $resize_explode[1] ?? null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }

        $filename = $this->randomName();
        $image->save($path.'/'.$filename);
        $this->setFileName($filename);
    }

    /**
     * Загружаем стандартным способом
     *
     * @param $path
     */
    protected function defaultUploaded($path): void
    {
        $filename = $this->randomName();
        $this->data->move($path, $filename);
        $this->setFileName($filename);
    }
}
