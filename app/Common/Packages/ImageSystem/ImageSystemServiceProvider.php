<?php
namespace App\Common\Packages\ImageSystem;


use Illuminate\Support\ServiceProvider;

class ImageSystemServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('imagesystem.file', function ($app) {
            return new ImageSystem();
        });


    }
}
