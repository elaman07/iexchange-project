<?php

namespace App\Common\Packages\ImageSystem\Facades;

use Illuminate\Support\Facades\Facade;

class ImageSystemFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'imagesystem.file';
    }
}

