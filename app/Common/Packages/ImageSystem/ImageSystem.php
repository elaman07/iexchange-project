<?php

namespace App\Common\Packages\ImageSystem;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\{Str, Facades\Storage};
use JetBrains\PhpStorm\ArrayShape;

class ImageSystem
{
    use Concerns\ManagesStorage,
        Concerns\ManagesLocal;

    /**
     * Данные для обработки изображений
     *
     * @var string|UploadedFile
    */
    protected string|UploadedFile $data;

    /**
     * Получаем название диска
     *
     * @var string|null
    */
    protected string|null $disk = null;

    /**
     * Получаем расширение загружаемого файла
     *
     * @return string
     */
    protected string $extension;

    /**
     * Получаем имя загружаемого файла
     *
     * @return string
     */
    protected string $filename;

    /**
     * Получение данных для обработки
     *
     * @param UploadedFile|string $data
     * @return ImageSystem
     */
    public function make(UploadedFile|string $data): static
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Загружаем файл и получаем ответ
     * @param string $path
     * @param string|null $old_filename
     * @return array
     */
    public function put(string $path, string $old_filename = null): array
    {
        if($this->isUrl()) {
            return $this->initFromUrl($this->data, $path, $old_filename);
        } else {
            return $this->initFromAny($this->data, $path, $old_filename);
        }
    }

    /**
     * Обработка изображений и загрузка на сервер
     *
     * @param UploadedFile $data
     * @param $path
     * @param $old_filename
     * @return array
     */
    #[ArrayShape(['is_local_image' => "int", 'filename' => "string"])]
    public function initFromAny(UploadedFile $data, $path, $old_filename): array
    {
        // Получаем расширение загружаемого файла
        $this->setExtension($data->getClientOriginalExtension());

        $is_local = 0;
        // Если активен и выбран диск то, ставим стандартную обработку
        if($this->isEnabled() and !empty($this->getStorageSource())) {
            $this->saveToRemoteDisk($path, $old_filename);
            $is_local = 1;
        } else {
            $this->saveToLocal($path, $old_filename);
        }

        return [
            'is_local_image' => $is_local,
            'filename' => $this->filename
        ];
    }


    public function initFromUrl($data, $path, $old_filename)
    {
        $file_value = file_get_contents($data);
        $filename = sprintf('iex-%s.%s', Str::random(10), '.png');

        $disk = null;
        if($this->disk == 'payment_systems') {
            $disk = iEXSetting('storage_disk_payment_system');
        }

        $is_local = 0;
        // Если активен и выбран диск то, ставим стандартную обработку
        if((int)iEXSetting('is_module_storage_disk') == 1 and !empty($disk))
        {
            $storage = Storage::disk($disk);
            // Удаляем старое фото
            if(!empty($old_filename) and $storage->exists($this->disk.'/'.$filename)) {
                $storage->delete($this->disk.'/'.$filename);
            }

            $storage->put($this->disk.'/'.$filename, $file_value);
            $is_local = 1;
        } else {
            if(!empty($old_filename)) {
                iex_file_delete($path.'/'.$old_filename);
            }
            upload_image_url($data, 60, $path.$filename);
        }

        return [
            'is_local_image' => $is_local,
            'filename' => $filename
        ];
    }


    /**
     * Устанавливаем расширение файла
     *
     * @param $value
     */
    public function setExtension($value): void
    {
        $this->extension = $value;
    }

    /**
     * Устанавливаем имя диска
     *
     * @param null $value
     * @return ImageSystem
     */
    public function setDisk($value = null): static
    {
        $this->disk = $value;
        return $this;
    }

    /**
     * Determines if current source data is url
     *
     * @return boolean
     */
    public function isUrl(): bool
    {
        return (bool) filter_var($this->data, FILTER_VALIDATE_URL);
    }

    /**
     * Проверяем статус активности модуля
     *
     * @return bool
    */
    public function isEnabled(): bool
    {
        return ((int)iEXSetting('is_module_storage_disk') == 1);
    }

    /**
     * Записываем новое имя файла
     *
     * @param string $value
     */
    public function setFileName(string $value): void
    {
        $this->filename = $value;
    }

    /**
     * Получаем имя файла перед загрузкой на сервер
     *
     * @return string
    */
    public function randomName(): string
    {
        return sprintf('%s-%s.%s', Str::lower(Str::studly(iEXContentLanguage('sitename'))), Str::random(15), $this->extension);
    }

    /**
     * Получаем названия хранилища
     *
     * @return string|null
     */
    private function getStorageSource() : string|null
    {
        $disk = null;
        if($this->disk == 'payment_systems') {
            $disk = iEXSetting('storage_disk_payment_system');
        } elseif($this->disk == 'verification_card') {
            $disk = iEXSetting('storage_disk_verification_card');
        }elseif($this->disk == 'news') {
            $disk = iEXSetting('storage_disk_news');
        }

        return $disk;
    }
}
