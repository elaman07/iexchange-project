<?php
namespace App\Common\Packages\Order;


use App\Common\Packages\Order\Wallet\PaymentWallet;
use Illuminate\Support\ServiceProvider;

class OrderServiceProvider extends ServiceProvider
{
    public function boot() {
        //
    }

    public function register()
    {
        $this->app->singleton('order', function() {
            return new Order();
        });

        $this->app->singleton('order.wallet', function() {
            return new PaymentWallet();
        });
    }
}
