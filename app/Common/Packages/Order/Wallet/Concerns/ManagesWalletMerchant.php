<?php

namespace App\Common\Packages\Order\Wallet\Concerns;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionId;
use App\Models\Requisites;
use App\Models\WalletsAddresses;
use Illuminate\Support\Str;

trait ManagesWalletMerchant
{
    /**
     */
    public function getWalletMerchantToArray($merchant_pay, $firstAddress): array|string
    {
        // Подгружаем конфигурацию
        $configProvider = PaymentFacade::configProvider($merchant_pay->gateway->alias);
        // Если доступна конфигурация
        if(isset($configProvider)) {
            // Получаем информацию о реквизитах (По умолчанию)
            if(isset($configProvider['is_crypto']) and $configProvider['is_crypto']) {
                return $this->getDynamicMerchantAddress($firstAddress, $merchant_pay, $configProvider);
            } elseif (isset($configProvider['receiving_requisites_default']) and $configProvider['receiving_requisites_default']) {
                return $this->getDynamicMerchantGateway($firstAddress, $merchant_pay);
            }

            if(isset($configProvider['receiving_requisites_types']) and $configProvider['receiving_requisites_types'] and $merchant_pay->type_pay == 1) {
                // Получаем информацию о реквизитах (Через настраиваемую систему)
                return $this->getDynamicMerchantGateway($firstAddress, $merchant_pay);
            }
        }

        return '';
    }

    /**
     * Динамический сторонний crypto-адрес
     *
     * @param Requisites $requisites
     * @param $merchant_pay
     * @param $configProvider
     * @return array
     */
    private function getDynamicMerchantAddress(Requisites $requisites, $merchant_pay, $configProvider): array
    {
        $statusUrl = route('merchant.receive_money', [$merchant_pay->gateway->alias, $merchant_pay->security_hash]);
        if(isset($configProvider['ipn_status_url']) and $configProvider['ipn_status_url']) {
            $statusUrl = route('merchant.ipn', [$merchant_pay->gateway->alias, $merchant_pay->security_hash]);
        }

        $in_price = $this->order->give_price;
        if($merchant_pay->pay_amount == 1) {
            $in_price = $this->order->give_price_with_comm_pay;
        } elseif($merchant_pay->pay_amount == 2) {
            $in_price = $this->order->give_price_default;
        }

        $response = PaymentFacade::merchant($merchant_pay->gateway->alias, $merchant_pay->id)
            ->address([
                'notifyUrl'         => $statusUrl,
            ])
            ->setCoinAmount($in_price)
            ->setCoinCurrency($this->getInCodeCurrency()->name)
            ->setDescription('#'.$this->order->id)
            ->setTransactionId($this->order->id)
            ->setOrderData($this->order)
            ->send();

        if($response->isSuccessful())
        {
            $meta_tag = method_exists($response, 'getTag') ? $response->getTag() : null;

            // Далее, динамические адреса записываются в историю
            $wallet = WalletsAddresses::create([
                'id_requisites' =>  $requisites->id,
                'address'       =>  $response->getAddress(),
                'memo_id'       =>  $meta_tag,
                'account'       =>  $this->order->id,
                'private_key'   =>  method_exists($response, 'getPrivateKey') ? $response->getPrivateKey() : null,
                'provider'      =>  mb_strtolower($this->getRequisites()->first()->currency->payment->name),
                'service_name'  =>  $merchant_pay->gateway->alias
            ]);

            // Привязываем динамический адрес к заявке
            $update_order = [
                'id_wallets_addresses' =>  $wallet->id,
                'transfer_to_account' => $response->getAddress()
            ];

            $this->order->update($update_order);

            if(Str::lower($merchant_pay->gateway->alias) == 'fixedfloat')
            {
                cache()->rememberForever('merchant-fixedfloat__id'.$this->order->id, function() use($response) {
                    return [
                        'order_id'      =>  $this->order->id,
                        'hash_id'       =>  method_exists($response, 'getUniqueOrderHashId') ? $response->getUniqueOrderHashId() : null,
                        'to_address'    =>  method_exists($response, 'getUniqueToAddress') ? $response->getUniqueToAddress() : null,
                        'address'       =>  $response->getAddress(),
                        'token'         =>  method_exists($response, 'getTokenAddress') ? $response->getTokenAddress() : null,
                    ];
                });
            }

            if(Str::lower($merchant_pay->gateway->alias) == 'cryptomus')
            {
                cache()->rememberForever('merchant-cryptomus__id'.$this->order->id, function() use($response)
                {
                    return [
                        'order_id'      =>  $this->order->id,
                        'uuid'       =>  method_exists($response, 'getLabel') ? $response->getLabel() : null
                    ];
                });
            }


            if(Str::lower($merchant_pay->gateway->alias) == 'alfabitpay')
            {
                cache()->rememberForever('merchant-alfabitpay__id'.$this->order->id, function() use($response)
                {
                    return [
                        'order_id'   =>  $this->order->id,
                        'uuid'       =>  method_exists($response, 'getUniqueOrderHashId') ? $response->getUniqueOrderHashId() : null,
                        'address'    =>  $response->getAddress(),
                        'id'         =>  method_exists($response, 'getUniqueServiceId') ? $response->getUniqueServiceId() : null,
                    ];
                });
            }

            return [
                'address'   => $response->getAddress(),
                'tag'       => $meta_tag,
                'label'     => method_exists($response, 'getLabel') ? $response->getLabel() : null
            ];
        }

        return [];
    }

    /**
     * Динамический сторонний счет
     *
     * @param Requisites $requisites
     * @param $merchant_pay
     * @return array
     */
    private function getDynamicMerchantGateway(Requisites $requisites, $merchant_pay): array
    {
        $statusUrl = route('merchant.receive_money', [$merchant_pay->gateway->alias, $merchant_pay->security_hash]);

        $in_price = $this->order->give_price;
        if($merchant_pay->pay_amount == 1) {
            $in_price = $this->order->give_price_with_comm_pay;
        } elseif($merchant_pay->pay_amount == 2) {
            $in_price = $this->order->give_price_default;
        }

        $response = PaymentFacade::merchant($merchant_pay->gateway->alias, $merchant_pay->id)
            ->purchase([
                'notifyUrl'         => $statusUrl,
            ])
            ->setAmount($in_price)
            ->setCoinCurrency($this->getInCodeCurrency()->name)
            ->setCurrency($this->getInCodeCurrency()->name)
            ->setDescription('#'.$this->order->id)
            ->setTransactionId($this->order->id)
            ->setOrderData($this->order)
            ->send();

        if($response->isSuccessful())
        {
            if(method_exists($response, 'getLabel'))
            {
                MerchantTransactionId::create([
                    'id_task' => $this->order->id,
                    'transaction_id' => $response->getLabel(),
                    'provider' => $merchant_pay->gateway->alias
                ]);
            }

            // Привязываем динамический адрес к заявке
            $update_order = [
                'transfer_to_account' => $response->getAddress()
            ];

            $this->order->update($update_order);

            return [
                'address'   => $response->getAddress(),
                'tag'       => method_exists($response, 'getTag') ? $response->getTag() : null,
                'label'     => method_exists($response, 'getLabel') ? $response->getLabel() : null
            ];
        }

        return [];
    }

}

