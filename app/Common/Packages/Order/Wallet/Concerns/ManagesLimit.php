<?php

namespace App\Common\Packages\Order\Wallet\Concerns;

use App\Models\Task;
use App\Models\TaskRequisites;
use Carbon\Carbon;

trait ManagesLimit
{
    /**
     * Получение адреса с учетом дневного/месячного лимита
     *
     * @return string | mixed
     */
    protected function getAddressForLimit()
    {
        // Сегодня
        $today = [Carbon::today()->startOfDay(), Carbon::today()->endOfDay()];
        // За эту неделю
        // $week = [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()];
        // За этот месяц
        $month = [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()];

        $currentAddress = null;
        foreach ($this->getRequisites()->get() as $item)
        {
            $sum_day = Task::where('id_payment_requisites', $item->id)
                ->whereBetween('updated_at', $today)->where('status', '=', 4)
                ->sum('give_price');

            $sum_month = Task::where('id_payment_requisites', $item->id)
                ->whereBetween('updated_at', $month)->where('status', '=', 4)
                ->sum('give_price');

            // Дневной лимит (Учитываем)
            if($item->limit_day > 0 and $sum_day >= $item->limit_day)
                continue;

            // Месячный лимит (Учитываем)
            if($item->limit_month > 0 and $sum_month >= $item->limit_month)
                continue;

            // Если переменная пуста
            if(is_null($currentAddress)) {
                // Обновляем счетчик просмотров
                $item->update(['view' => $item->view + 1]);
                $currentAddress = $item->account_number;


                // Записываем в базу
                $is_task_check = TaskRequisites::where('id_task',  $this->order->id)->exists();
                if(!$is_task_check)
                {
                    TaskRequisites::create([
                        'id_task' => $this->order->id,
                        'id_direction_exchange' => $this->order->id_direction_exchange,
                        'id_currency' => $this->order->direction_exchange->id_currency1,
                        'type' => 'currency',
                        'account_number' => $currentAddress
                    ]);
                }

                $this->order->update(['id_payment_requisites' => $item->id]);
            }
        }

        return ($currentAddress ?? null);
    }

    /**
     * Получаем адрес по лимиту просмотров
     *
     * @return string | mixed
     */
    protected function getAddressLimitViews()
    {
        $currentAddress = null;
        foreach ($this->getRequisites()->get() as $item)
        {
            if($item->limit_views > 0 and $item->view >= $item->limit_views)
                continue;

            // Если переменная пуста
            if(is_null($currentAddress)) {
                // Обновляем счетчик просмотров
                $item->update(['view' => $item->view + 1]);
                $currentAddress = $item->account_number;

                // Записываем в базу
                $is_task_check = TaskRequisites::where('id_task',  $this->order->id)->exists();
                if(!$is_task_check)
                {
                    TaskRequisites::create([
                        'id_task' => $this->order->id,
                        'id_direction_exchange' => $this->order->id_direction_exchange,
                        'id_currency' => $this->order->direction_exchange->id_currency1,
                        'type' => 'currency',
                        'account_number' => $currentAddress
                    ]);
                }

                $this->order->update(['id_payment_requisites' => $item->id]);
            }
        }
        return $currentAddress;
    }
}
