<?php

namespace App\Common\Packages\Order\Wallet\Concerns;

use App\Models\{
    Task,
    TaskRequisites
};

use Carbon\Carbon;

trait ManagesDirection
{

    /**
     * Получить реквизиты с направлений обмена
     */
    public function getDirectionRequisites()
    {
        if($this->getDirectionExchange()->type_output_requisites == 1 and $this->getDirectionExchange()->direction_requisites->count() > 0) {
            return $this->getDirectionExchange()->direction_requisites->random()->get();
        }

        return $this->getDirectionExchange()->direction_requisites;
    }

    /**
     * Получение реквизитов из направлений
     */
    private function getWalletFromDirectionExchange()
    {
        // Сегодня
        $today = [Carbon::today()->startOfDay(), Carbon::today()->endOfDay()];
        $month = [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()];

        $currentAddress = null;
        foreach ($this->getDirectionRequisites() as $item)
        {
            $sum_day = Task::where('id_direction_requisites', $item->id)
                ->whereBetween('updated_at', $today)->where('status', '=', 4)
                ->sum('give_price');

            $sum_month = Task::where('id_direction_requisites', $item->id)
                ->whereBetween('updated_at', $month)->where('status', '=', 4)
                ->sum('give_price');

            // Дневной лимит (Учитываем)
            if($item->limit_day > 0 and $sum_day >= $item->limit_day)
                continue;

            // Месячный лимит (Учитываем)
            if($item->limit_month > 0 and $sum_month >= $item->limit_month)
                continue;

            // Если переменная пуста
            if(is_null($currentAddress)) {
                // Обновляем счетчик просмотров
                $item->update(['view' => $item->view + 1]);
                $currentAddress = $item->account_number;

                // Записываем в базу
                $is_task_check = TaskRequisites::where('id_task',  $this->order->id)->exists();
                if(!$is_task_check)
                {
                    TaskRequisites::create([
                        'id_task' => $this->order->id,
                        'id_direction_exchange' => $this->order->id_direction_exchange,
                        'id_currency' => $this->order->direction_exchange->id_currency1,
                        'type' => 'direction_exchange',
                        'account_number' => $currentAddress
                    ]);
                }

                $this->order->update(['id_direction_requisites' => $item->id]);
            }
        }

        return ($currentAddress ?? null);
    }
}
