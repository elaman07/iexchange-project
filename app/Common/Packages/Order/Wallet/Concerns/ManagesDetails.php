<?php

namespace App\Common\Packages\Order\Wallet\Concerns;

use App\Models\GatewayMerchant;
use App\Models\Requisites;

trait ManagesDetails
{
    public function getDirectionExchange()
    {
        return $this->order->direction_exchange;
    }

    public function getInCodeCurrency()
    {
        return $this->order->direction_exchange->currency1->code_currency;
    }

    public function getInCurrency()
    {
        return $this->order->direction_exchange->currency1;
    }

    /**
     * Получить резервы
     *
     * @return \App\Models\Requisites
     */
    public function getRequisites()
    {
        return Requisites::activeWallet()->where('id_currency', $this->getInCurrency()->id);
    }

    /**
     * Получаем информацию о мерчанте
     */
    public function findMerchantData($from_order = false)
    {
        if($from_order) {
            return GatewayMerchant::find($this->order->id_merchant);
        }

        if(isset($this->order->id_merchant) and $this->order->id_merchant > 0)
            return GatewayMerchant::find($this->order->id_merchant);


        if($this->getDirectionExchange()->merchants()->count() > 0) {
            return $this->getDirectionExchange()->merchants()->inRandomOrder()->first();
        }

        return $this->getDirectionExchange()->currency1->merchants()->inRandomOrder()->first();
    }
}
