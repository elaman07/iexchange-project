<?php

namespace App\Common\Packages\Order\Wallet;

use App\Models\Requisites;
use App\Models\Task;
use Illuminate\Contracts\Support\Arrayable;

class PaymentWallet implements Arrayable
{
    use Concerns\ManagesWalletMerchant,
        Concerns\ManagesDetails,
        Concerns\ManagesDirection,
        Concerns\ManagesLimit;


    protected Task $order;

    public function __construct()
    {
        //
    }

    public function call(Task $task)
    {
        $this->order = $task;
        return $this;
    }


    public function toArray()
    {
        // Получаем ручной адрес из направления обменов
        if(count($this->getDirectionRequisites()) > 0) {
            return $this->getWalletFromDirectionExchange();
        }

        // Получение первого адреса
        $firstAddress = $this->getFirstAddress();

        $returnWallet = null;
        // Делаем проверку, существует ли кошелек
        if(isset($firstAddress))
        {
            $merchant_pay = $this->findMerchantData(true);

            if(isset($merchant_pay)) {
                return $this->getWalletMerchantToArray($merchant_pay, $firstAddress);
            }

            // Изменить условие получение реквизита
            if($firstAddress->is_unique_shot == 1) {
                return $this->getManualUniqueWallet($firstAddress);
            }

            if($firstAddress->limit_views > 0) {
                return $this->getAddressLimitViews();
            }

            // Если не установлены лимиты
            if($firstAddress->limit_day == 0 and $firstAddress->limit_month == 0)
            {
                // Обновляем счетчик просмотров, выбранной платежной системы
                $firstAddress->update(['view' => $firstAddress->view + 1]);

                // Если добавлены несколько счетов, разделяем
                $receive_money = collect(explode(',', $firstAddress->account_number))->random();


                $is_request_payment_type = 0;
                if($receive_money == '[request_payment]') {
                    $is_request_payment_type = 1;
                }

                // Прикрепляем реквизиты к заявке
                $this->order->update([
                    'id_payment_requisites' => $firstAddress->id,
                    'requisites_receive' => $receive_money,
                    'transfer_to_account' => $receive_money,
                    'is_request_payment_type' => $is_request_payment_type
                ]);
                return ($receive_money ?? null);
            }

            $returnWallet = $this->getAddressForLimit();
        }

        return $returnWallet;
    }

    /**
     * Получение основного реквизита
     *
     * @return \App\Models\Requisites
     */
    protected function getFirstAddress()
    {
        $type_output_requisites = $this->getInCurrency()->type_output_requisites;
        // Выдача по умолчанию
        if($type_output_requisites == 0) {
            return Requisites::activeWallet()->whereIdCurrency($this->getDirectionExchange()->id_currency1)->first();
        }

        // Выдача рандомно
        return Requisites::activeWallet()->whereIdCurrency($this->getDirectionExchange()->id_currency1)->inRandomOrder()->first();
    }

    /**
     * Получение уникального номера счета
     *
     * @return void|null
     */
    private function getManualUniqueWallet($firstAddress)
    {
        $check_shot = Task::where('status', '=',3)->whereHas('direction_exchange', function($query)  use($firstAddress){
            $query->where('id_currency1', $firstAddress->id_currency);
        })->pluck('transfer_to_account')->reject(function($item) {
            return empty($item);
        })->toArray();

        // Если есть активные заявки то меняем номер счета
        if(count($check_shot) > 0) {
            $firstAddress = Requisites::activeWallet()->whereIdCurrency($this->getDirectionExchange()->id_currency1)->whereNotIn('account_number', $check_shot)->first();
        }

        if(!isset($firstAddress)) {
            return null;
        }
    }
}
