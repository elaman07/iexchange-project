<?php

namespace App\Common\Packages\Order;


use App\Models\DirectionExchange;
use App\Models\Settings;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

class Order
{
    use Bindings\ManagerDetails,
        Bindings\ManagerOrder,
        Bindings\ManagerArray,
        Rules\RulesField;

    /**
     * Id направления
     *
     * @var \App\Models\DirectionExchange
    */
    protected $directionId;

    /**
     * Дополнительные параметры
     *
     * @var Collection
    */
    protected $options;

    /**
     * Request
     *
     * @var Request
    */
    protected Request $request;

    /**
     * Информация о заявке
     *
     * @var Task
    */
    protected $order_data;

    /**
     * Валидатор входных параметров
     *
     * @var \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory
    */
    protected $validator;

    /**
     * Обработчик
     *
     * @param Request|null $request
     * @return Order
     * @throws \Exception
     */
    public function request(Request $request = null)
    {
        $this->directionId = DirectionExchange::isEnabled()->where([
            ['id_currency1', intval($request->get('income_payment_system'))],
            ['id_currency2', intval($request->get('outcome_payment_system'))]
        ])->first();

        if(!isset($this->directionId)) {
//            // В случае возникнования подобных ситуаций, баним клиента
//            if(auth()->check()) {
//                add_user_ban(auth()->id(), null, '[iEXBot] - Нарушено правило создание заявок');
//            }

            throw new \Exception('Направление не найдено');
        }

        $this->options = $request->all();
        $this->validator = \validator($request->all());
        return $this;
    }

    /**
     * Обработчик
     *
     * @param Request|null $request
     * @return Order
     * @throws \Exception
     */
    public function request_api($array)
    {
        $this->directionId = DirectionExchange::isEnabled()->where([
            ['id_currency1', intval($array['income_payment_system'])],
            ['id_currency2', intval($array['outcome_payment_system'])]
        ])->first();

        if(!isset($this->directionId)) {
            // В случае возникнования подобных ситуаций, баним клиента
            if(auth()->check()) {
                add_user_ban(auth()->id(), null, '[iEXBot] - Нарушено правило создание заявок');
            }

            throw new \Exception('Направление не найдено');
        }

        $this->options = $array;
        $this->validator = \validator($array);
        return $this;
    }


    /**
     * Проверяем, существует ли направление
     *
     * @return bool
    */
    public function hasDirection() {
        return ! is_null($this->directionId);
    }
}
