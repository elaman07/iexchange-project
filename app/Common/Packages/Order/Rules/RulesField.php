<?php
namespace App\Common\Packages\Order\Rules;


trait RulesField
{
    /**
     * Правила для полей валюты (Отдаю)
     *
     */
    public function rulesFieldIn()
    {
        $fields = $this->currencyInFields();
        if(isset($fields)) {
            foreach ($fields as $item) {
                $array = [];
                if($item->obligatory_field == 0)
                    array_push($array, 'required');

                if($item->field_type == 'integer')
                    array_push($array, 'numeric|nullable');
                elseif($item->field_type == 'string')
                    array_push($array, 'nullable|regex:/^[a-zA-Zа-яА-Я ]+$/u');

                // Язык текста
                if($item->language_field == 1)
                    array_push($array, 'nullable|regex:/^[а-яА-Я ]+$/u');
                elseif($item->language_field == 2)
                    array_push($array, 'nullable|regex:/^[a-zA-Z ]+$/u');

                if($item->min_char > 0)
                    array_push($array, 'min:'.$item->min_char);

                if($item->max_char > 0)
                    array_push($array, 'max:'.$item->max_char);

                $this->getValidator()->sometimes($item->key_id, implode('|', $array), function () use($item) {
                    return request()->has($item->key_id);
                });

                if($this->getValidator()->fails() and $this->getValidator()->errors()->has($item->key_id))
                {
                    return [
                        'field'     =>  $item->key_id,
                        'modal'     =>  false,
                        'message'   =>  $this->getValidator()->errors()->first($item->key_id)
                    ];
                }
            }
        }
    }

    /**
     * Правила для полей валюты (Получаю)
     *
     */
    public function rulesFieldOut()
    {
        $fields = $this->currencyOutFields();
        if(isset($fields))
        {
            foreach ($fields as $item)
            {
                $array = [];
                if($item->obligatory_field == 0)
                    array_push($array, 'required');

                if($item->field_type == 'integer')
                    array_push($array, 'numeric|nullable');
                elseif($item->field_type == 'string')
                    array_push($array, 'nullable|regex:/^[a-zA-Zа-яА-Я ]+$/u');

                // Язык текста
                if($item->language_field == 1)
                    array_push($array, 'nullable|regex:/^[а-яА-Я ]+$/u');
                elseif($item->language_field == 2)
                    array_push($array, 'nullable|regex:/^[a-zA-Z ]+$/u');

                if($item->min_char > 0)
                    array_push($array, 'min:'.$item->min_char);

                if($item->max_char > 0)
                    array_push($array, 'max:'.$item->max_char);

                $this->getValidator()->sometimes($item->key_id, implode('|', $array), function () use($item) {
                    return request()->has($item->key_id);
                });

                if($this->getValidator()->fails() and $this->getValidator()->errors()->has($item->key_id))
                {
                    return [
                        'field'     =>  $item->key_id,
                        'modal'     =>  false,
                        'message'   =>  $this->getValidator()->errors()->first($item->key_id)
                    ];
                }
            }
        }
    }

    /**
     * Получение деталей валидатора
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidator()
    {
        return $this->validator;
    }
}
