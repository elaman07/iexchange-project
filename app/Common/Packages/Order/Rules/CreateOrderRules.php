<?php
namespace App\Common\Packages\Order\Rules;

use App\Common\Packages\Payment\Facades\PaymentFacade;
use App\Models\CurrencyFields;
use App\Models\DirectionField;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Cog\Contracts\Ban\Bannable as BannableContract;

trait CreateOrderRules
{
    /**
     * Если клиенту запрещено создать заявку
     *
     * @return array
    */
    public function ruleUser()
    {
        $user = User::whereEmail($this->getClientEmail())->first();

        if(auth()->check() and auth()->user()->is_order == 1)
        {
            return [
                'field'     =>  'email',
                'modal'     =>  false,
                'message'   =>  __('Вы не можете создать новую заявку, обратитесь в службу поддержки')
            ];
        }

        if(isset($user) and $user instanceof BannableContract && $user->isBanned())
        {
            return [
                'field'     =>  'email',
                'modal'     =>  false,
                'message'   =>  __('Вы не можете создать новую заявку')
            ];
        }

    }

    /**
     * Правила для реквизитов Отдаю
     *
     * @return array | mixed
    */
    public function ruleInPayment()
    {
        if($this->getDetail('input_payment') and $this->minInChar($this->options['income_account']))
        {
            $min_max_error_message = str_replace(
                ['[currency]'],
                [sprintf('%s %s', $this->getInPayment()->name, $this->getInCodeCurrency()->name)],
                $this->getInCurrency()->min_max_error_message
            );


            return [
                'field'     =>  'income_account',
                'modal'     =>  false,
                'message'   =>  !empty($min_max_error_message) ? $min_max_error_message : __('Указан неправильный номер кошелька :payment', [
                    'payment' => $this->getInPayment()->name
                ])
            ];
        }

        // Проверяем реквизиты на целые числа
        if($this->getInCurrency()->allowed_char == 1 and !is_numeric($this->options['income_account']))
        {
            return [
                'field'     =>  'income_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен состоять из целых чисел'), $this->getInPayment()->name.' '.$this->getInCodeCurrency()->name)
            ];

        }elseif($this->getInCurrency()->allowed_char == 2 and preg_match('/[^a-zA-Zа-яА-ЯёЁ]+/', $this->options['income_account']))
        {
            // Проверяем реквзииты на строчные значения
            return [
                'field'     =>  'income_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен быть строкой'), $this->getInPayment()->name.' '.$this->getInCodeCurrency()->name)
            ];
        }elseif($this->getInCurrency()->allowed_char == 3 and preg_match('/[^A-Za-z0-9]+/', $this->options['income_account']))
        {
            // Проверяем реквзииты на строчные значения
            return [
                'field'     =>  'income_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен содержать латинские буквы и цифры'), $this->getInPayment()->name.' '.$this->getInCodeCurrency()->name)
            ];

        }elseif($this->getInCurrency()->allowed_char == 4 and preg_match('/[^A-Za-z]+/', $this->options['income_account']))
        {
            // Проверяем реквзииты на строчные значения
            return [
                'field'     =>  'income_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен содержать только латинские буквы'), $this->getInPayment()->name.' '.$this->getInCodeCurrency()->name)
            ];
        }

        // Проверка дневного лимита
        if($this->getInCurrency()->day_limit_give > 0 and $this->inCurrencyLimitDay() == false) {
            return [
                'field'     =>  'left_limit_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('У валюты %s превышен дневной лимит'), $this->getInPayment()->name.' '. $this->getInCodeCurrency()->name)
            ];
        }

        // Проверка недельного лимита
        if($this->getInCurrency()->week_limit_in > 0 and $this->inCurrencyLimitWeek() == false) {
            return [
                'field'     =>  'left_limit_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('У валюты %s превышен недельный лимит'), $this->getInPayment()->name.' '. $this->getInCodeCurrency()->name)
            ];
        }

        // Проверка месячного лимита
        if($this->getInCurrency()->month_limit_in > 0 and $this->inCurrencyLimitMonth() == false) {
            return [
                'field'     =>  'left_limit_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('У валюты %s превышен месячный лимит'), $this->getInPayment()->name.' '. $this->getInCodeCurrency()->name)
            ];
        }
    }

    /**
     * Валидатор счетов (Отдаю)
     *
     * @return array| bool
     */
    public function ruleInValidator()
    {
        if(empty($this->getInCurrency()->is_valid_account))
            return false;

        if($this->getInCurrency()->visible_give == 0  and !wallet_validator($this->getInCurrency()->is_valid_account, $this->options['income_account']))
        {
            return [
                'field'     =>  'income_account',
                'modal'     =>  false,
                'message'   =>  !empty($this->getInCurrency()->valid_account_error) ? $this->getInCurrency()->valid_account_error : 'error wallet'
            ];
        }
    }

    /**
     * Правила для получаю
     *
     * @return array
    */
    public function ruleOutPayment()
    {
        if($this->getDetail('output_payment') and $this->minOutChar($this->options['outcome_account']))
        {
            $min_max_error_message = str_replace(
                ['[currency]'],
                [sprintf('%s %s', $this->getOutPayment()->name, $this->getOutCodeCurrency()->name)],
                $this->getOutCurrency()->min_max_error_message
            );

            return [
                'field'     =>  'outcome_account',
                'modal'     =>  false,
                'message'   =>  !empty($min_max_error_message) ? $min_max_error_message : __('Указан неправильный номер кошелька :payment', [
                    'payment' => $this->getOutPayment()->name
                ])
            ];
        }

        // Проверяем реквизиты на целые числа
        if($this->getOutCurrency()->allowed_char == 1 and !is_numeric($this->options['outcome_account']))
        {
            return [
                'field'     =>  'outcome_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен состоять из целых чисел'), $this->getOutPayment()->name.' '.$this->getOutCodeCurrency()->name)
            ];
        }elseif($this->getOutCurrency()->allowed_char == 2 and preg_match('/[^a-zA-Zа-яА-ЯёЁ]+/', $this->options['outcome_account']))
        {
            // Проверяем реквзииты на строчные значения
            return [
                'field'     =>  'outcome_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен быть строкой'), $this->getOutPayment()->name.' '.$this->getOutCodeCurrency()->name)
            ];
        }elseif($this->getOutCurrency()->allowed_char == 3 and preg_match('/[^A-Za-z0-9]+/', $this->options['outcome_account']))
        {
            // Проверяем реквзииты на строчные значения
            return [
                'field'     =>  'outcome_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен содержать латинские буквы и цифры'), $this->getOutPayment()->name.' '.$this->getOutCodeCurrency()->name)
            ];
        }elseif($this->getOutCurrency()->allowed_char == 4 and preg_match('/[^A-Za-z]+/', $this->options['outcome_account']))
        {
            // Проверяем реквзииты на строчные значения
            return [
                'field'     =>  'outcome_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('Счет %s должен содержать только латинские буквы'), $this->getOutPayment()->name.' '.$this->getOutCodeCurrency()->name)
            ];
        }

        // Проверка дневного лимита
        if($this->getOutCurrency()->day_limit_receive > 0 and $this->outCurrencyLimitDay() == false) {
            return [
                'field'     =>  'right_limit_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('У валюты %s превышен дневной лимит'), $this->getOutPayment()->name.' '. $this->getOutCodeCurrency()->name)
            ];
        }

        // Проверка недельного лимита
        if($this->getOutCurrency()->week_limit_out > 0 and $this->outCurrencyLimitWeek() == false) {
            return [
                'field'     =>  'right_limit_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('У валюты %s превышен недельный лимит'), $this->getOutPayment()->name.' '. $this->getOutCodeCurrency()->name)
            ];
        }

        // Проверка месячного лимита
        if($this->getOutCurrency()->month_limit_out > 0 and $this->outCurrencyLimitMonth() == false) {
            return [
                'field'     =>  'right_limit_account',
                'modal'     =>  false,
                'message'   =>  sprintf(__('У валюты %s превышен месячный лимит'), $this->getOutPayment()->name.' '. $this->getOutCodeCurrency()->name)
            ];
        }
    }

    /**
     * Валидатор счетов (Получаю)
     *
     * @return array| bool
     */
    public function ruleOutValidator()
    {
        if(empty($this->getOutCurrency()->is_valid_account))
            return false;

        if(!wallet_validator($this->getOutCurrency()->is_valid_account, $this->options['outcome_account']))
        {
            return [
                'field'     =>  'outcome_account',
                'modal'     =>  false,
                'message'   =>  !empty($this->getInCurrency()->valid_account_error) ? $this->getInCurrency()->valid_account_error : 'error wallet'
            ];
        }
    }


    /**
     * Проверка доп. поля из направлений
    */
    public function ruleDirectionFields()
    {
        foreach($this->getAdditionFieldsDirection() as $key => $additionField)
        {
            // Ключ получаем
            $field_item = DirectionField::whereKeyId($key)->first();

            if(isset($field_item))
            {
                // Проверяем реквизиты на целые числа
                if($field_item->field_type == 1 and !is_numeric($additionField))
                {
                    return [
                        'field'     =>  'direction_field_'.$key,
                        'modal'     =>  false,
                        'message'   =>  sprintf(__('Поле %s должен состоять из целых чисел'), $field_item->name)
                    ];
                }elseif($field_item->field_type == 2 and preg_match('/[^a-zA-Zа-яА-ЯёЁ]+/', $additionField))
                {
                    // Проверяем реквзииты на строчные значения
                    return [
                        'field'     =>  'direction_field_'.$key,
                        'modal'     =>  false,
                        'message'   =>  sprintf(__('Поле %s должен быть строкой'), $field_item->name)
                    ];
                }elseif($field_item->field_type == 3 and preg_match('/[^A-Za-z0-9]+/', $additionField))
                {
                    // Проверяем реквзииты на строчные значения
                    return [
                        'field'     =>  'direction_field_'.$key,
                        'modal'     =>  false,
                        'message'   =>  sprintf(__('Поле %s должен содержать латинские буквы и цифры'), $field_item->name)
                    ];
                }elseif($field_item->field_type == 4 and preg_match('/[^A-Za-z]+/', $additionField))
                {
                    // Проверяем реквзииты на строчные значения
                    return [
                        'field'     =>  'direction_field_'.$key,
                        'modal'     =>  false,
                        'message'   =>  sprintf(__('Поле %s должен содержать только латинские буквы'), $field_item->name)
                    ];
                }


                // Проверка текста
                if($field_item->language_field == 1 and !preg_match('/^[а-яА-Я ]+$/u', $additionField))
                {
                    // Проверяем реквзииты на строчные значения
                    return [
                        'field'     =>  'direction_field_'.$key,
                        'modal'     =>  false,
                        'message'   =>  sprintf(__('Поле %s должен содержать только русские лимволы'), $field_item->name)
                    ];
                }elseif($field_item->language_field == 2 and !preg_match('/^[a-zA-Z ]+$/u', $additionField))
                {
                    // Проверяем реквзииты на строчные значения
                    return [
                        'field'     =>  'direction_field_'.$key,
                        'modal'     =>  false,
                        'message'   =>  sprintf(__('Поле %s должен содержать только латинские буквы'), $field_item->name)
                    ];
                }
            }
        }
    }
}
