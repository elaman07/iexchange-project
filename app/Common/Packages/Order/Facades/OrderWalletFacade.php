<?php
namespace App\Common\Packages\Order\Facades;


use App\Common\Packages\Transaction\Transaction;
use App\Models\Settings;
use Illuminate\Support\Facades\Facade;


class OrderWalletFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'order.wallet';
    }
}
