<?php
namespace App\Common\Packages\Order\Bindings;

use App\Common\Packages\Editor\EditorFacade;
use App\Common\Packages\Order\Facades\OrderWalletFacade;
use App\Common\Packages\Order\Wallet\PaymentWallet;
use App\Common\Packages\Payment\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Sessions\CurrentUserResource;
use App\Http\Resources\V2\Order\OrderProcessResource;
use App\Models\CurrencyNotification;
use App\Models\DirectionNotification;
use App\Models\RequirementVerification;
use App\Models\RequisiteField;
use App\Models\TaskField;
use App\Models\User;
use Illuminate\Support\Carbon;

trait ManagerArray
{
    /**
     * Дополнительные параметры в процессе создания новой заявки
     *
     * @param $user
     * @param $order
     * @return array
     */
    public function customArrayForCreate($user, $order): array
    {
        $in_currency = $this->getInCurrency();
        $detail = TransactionFacade::init($order);
        // Статус верификации карты
        $status_verify = $detail->isVerificationCard($in_currency);

        if($status_verify == 1 and $order->is_wallet_issued == 0) {
            $this->getOrder()->update([
                'is_wallet_issued' => 1
            ]);
            OrderWalletFacade::call($this->getOrder())->toArray();
        }

        $public_id = $order->public_id;
        if(empty($public_id)) {
            $public_id =  isset($order->transaction) ? $order->transaction->transaction : null;
        }

        $user_info = User::select('id', 'email', 'name')->where('id', '=', $order->id_user)->first();
        $out_currency = $this->getOutCurrency();

        $in_icon_url = '/storage/payment_systems/'.$in_currency['payment']['logo'];
        if(!empty(iEXSetting('storage_disk_payment_system')) and $in_currency['payment']['is_local_image'] == 1) {
            $in_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$in_currency['payment']['logo'], false);
        }

        $out_icon_url = '/storage/payment_systems/'.$out_currency['payment']['logo'];
        if(!empty(iEXSetting('storage_disk_payment_system')) and $out_currency['payment']['is_local_image'] == 1) {
            $out_icon_url = iex_dynamic_read_view_image('/payment_systems/'.$out_currency['payment']['logo'], false);
        }


        $array = [
            'id'  => (string)$order->id,
            'type' => 'order',
            'attributes' =>  [
                'public_id'     => (string)$public_id,
                'created_at'    => Carbon::parse($order->created_at)->format('c'),
                'income_account'    =>  $order->from_shot,
                'outcome_account'   =>  $order->to_shot,

                'income_amount' => [
                    'amount'    =>  (float)$order->give_price,
                    'currency'  =>  $this->getInCodeCurrency()->name,
                    'number_format' =>  $in_currency['number_format']
                ],
                'outcome_amount' => [
                    'amount'        =>  (float)$order->receiving_price,
                    'currency'      =>  $this->getOutCodeCurrency()->name,
                    'number_format' =>  $out_currency['number_format']
                ],
                'status'            =>  ($order->status != 4 ? 'waiting' : 'success')
            ],
            'relationships' => [
                'user' => [
                    'type' => 'user',
                    'attributes' => [
                        'is_auth'   => (bool)auth()->check(),
                        'email' =>  $user_info->email,
                        'name' => $user_info->name
                    ],
                ],
                'income_payment_system' => [
                    'data' => [
                        'id' =>  $in_currency['id'],
                        'type' => 'payment_system',
                        'attributes' => [
                            'name'                  => ($in_currency['visible_code_currency'] == 0) ? $in_currency['payment']['name'].' '.$in_currency['code_currency']['name'] : $in_currency['payment']['name'],
                            'payment_system'        => $in_currency['payment']['name'],
                            'letter_cod'            => $in_currency['designation_xml'],
                            'currency_iso_code'     => $in_currency['code_currency']['name'],
                            'icon_url'              => $in_icon_url,
                            'field_name_from'       =>  $in_currency['field_name_from']
                        ]
                    ]
                ],
                'outcome_payment_system' => [
                    'data' => [
                        'id' =>  $out_currency['id'],
                        'type' => 'payment_system',
                        'attributes' => [
                            'name'                  => ($out_currency['visible_code_currency'] == 0) ? $out_currency['payment']['name'].' '.$out_currency['code_currency']['name'] : $out_currency['payment']['name'],
                            'payment_system'        => $out_currency['payment']['name'],
                            'letter_cod'            => $out_currency['designation_xml'],
                            'currency_iso_code'     => $out_currency['code_currency']['name'],
                            'icon_url'              => $out_icon_url,
                            'field_name_to'         =>  $out_currency['field_name_to']
                        ]
                    ]
                ]
            ]
        ];



        $aml_text = null;
        if(!empty($in_currency['aml_text_in'])) {
            $aml_text = $in_currency['aml_text_in'];
        } elseif(!empty($out_currency['aml_text_out'])) {
            $aml_text = $out_currency['aml_text_out'];
        }

        $array['attributes']['is_aml'] = !empty($aml_text);
        $array['attributes']['aml_text'] = $aml_text;

        $array['attributes']['verification_status'] = $status_verify;
        $payMerchant = false;

        // Проверяем, прикреплен ли мерчант
        $merchant_pay = $this->findMerchantData(true);

        if($merchant_pay) {
            // Подгружаем конфигурацию
            $configProvider = PaymentFacade::configProvider($merchant_pay->gateway->alias);
            if(isset($configProvider)) {
                // Выдача реквизитов на отдельной странице
                if(isset($configProvider['is_checkout']) and $configProvider['is_checkout']) {
                    $payMerchant = true;
                    // Адрес для оплаты через мерчант
                    $array['attributes']['pay_invoice_url'] = route('merchant.checkout', [
                        (string)$order->transaction->transaction
                    ]);
                }

                // Если переключаемая система (из настроек мерчанта в поле "Тип оплаты")
                if(isset($configProvider['receiving_requisites_types']) and $configProvider['receiving_requisites_types'] and $merchant_pay->type_pay == 1) {
                    $payMerchant = false;
                    $array['attributes']['pay_invoice_url'] = null;
                }
            }


            // Выводим инструкцию к оплате
            if(empty($merchant_pay->instruction_payment))
            {
                if(iEXSetting('type_instruction_merchant') == 0) { // Ничего не выводим
                    $array['attributes']['instructions'] = null;
                }
            } else {
                $array['attributes']['instructions'] = EditorFacade::driver('bbcode')->order($merchant_pay->instruction_payment, $order);
            }

            $array['attributes']['is_enable_merchant_button'] = $merchant_pay->is_enable_merchant_button;
        }


        // max количество заявок в день
        $count_order_day = $this->getCountDayOrderIn();
        if(isset($merchant_pay) and $merchant_pay->day_limit_merchant > 0 and $count_order_day > $merchant_pay->day_limit_merchant) {
            $payMerchant = false;
        }

        // макс. сумма обмена
        if(isset($merchant_pay) and $merchant_pay->max_limit_amount_order > 0 and $this->getInAmount() > $merchant_pay->max_limit_amount_order) {
            $payMerchant = false;
        }

        // Суточный лимит для мерчанта
        if(isset($merchant_pay) and $merchant_pay->day_limit_amount_merchant > 0 and $this->dayLimitAmountIn() > $merchant_pay->day_limit_amount_merchant) {
            $payMerchant = false;
        }


        if(isset($merchant_pay) and in_array($merchant_pay->gateway->alias, ['exmogiftcard', 'whitebitcodes', 'kunacodes'])) {
            $payMerchant = false;
        }

        // Проверяем доступен ли мерчант для метода
        $array['attributes']['pay_merchant']  =  $payMerchant;

        // Инструкция к оплате
        if(!empty($this->directionId->instructions)) {
            $array['attributes']['instructions'] = $this->directionId->instructions;
        }


        // Текст, который выводится на странице подтверждения обмена
        $array['attributes']['other_info'] = [
            'text_order_confirm' => $order->direction_exchange->text_order_confirm,
            'order_button_i_confirm'    =>  $order->direction_exchange->order_button_i_confirm
        ];

        $array['attributes']['course_display'] = $order->course_display;
        // Получаем данные по наличным
        if(isset($order->task_info) and isset($order->task_info->cities) and \Module::find('Cities')->isEnabled()) {
            $array['attributes']['cities_value'] = sprintf('%s - %s', $order->task_info->country_name, $order->task_info->city_name);
        }


        return $array;
    }
}
