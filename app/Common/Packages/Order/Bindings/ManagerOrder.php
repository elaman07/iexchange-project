<?php
namespace App\Common\Packages\Order\Bindings;

use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Common\Packages\Crypto\Facades\CryptoFacade;
use App\Common\Packages\Order\Rules\CreateOrderRules;
use App\Common\Packages\Payment\PaymentFacade;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Jobs\NewOrderJob;
use App\Jobs\RegisterClientJob;
use App\Jobs\TelegramOrderJob;
use App\Models\CurrencyFields;
use App\Models\CurrencyNetwork;
use App\Models\DirectionExchangeCity;
use App\Models\DirectionField;
use App\Models\GeoCountryList;
use App\Models\PromoCode;
use App\Models\Task;
use App\Models\TaskCardDetail;
use App\Models\TaskField;
use App\Models\TaskInfo;
use App\Models\TaskReport;
use App\Models\TaskShot;
use App\Models\TasksRatesData;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserWalletStories;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\CardInfo\CardInfoFacade;
use Modules\Cities\Entities\CitiesModel;
use Nwidart\Modules\Facades\Module;

trait ManagerOrder
{
    use CreateOrderRules,
        ManagerParameters;

    /**
     * ID Пользователя
     *
     */
    public $authId = 0;

    /**
     * Массив данных
     *
     * @var Collection
     */
    protected Collection $items;

    /**
     * Если да, то пользователь найден в списке мошенников
     *
     * @var bool
    */
    protected $is_scam = 0;

    /**
     * Морозить клиентов
     *
     * @var boolean
     */
    protected $is_freeze_scam = 0;


    /**
     * Локальный список должников
     *
     * @var boolean
     */
    protected $is_freeze_local = false;


    /**
     * Обработка и создание заявки
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function created()
    {
        // Заменяем атрибуты на название
        $this->validator->setAttributeNames(
            CurrencyFields::all()->pluck('name', 'key_id')->toArray()
        );

        /// Проверяем ID пользователя
        if(auth()->check() or isset($this->options['email']))
        {
            $authInfo = User::where('email', '=', $this->getClientEmail())->first();
            if(isset($authInfo))
                $this->authId = $authInfo->id;
        }

        $is_limit = $this->isLimit();
        $isReserve = $this->isReserveModal();
        $options = collect($this->options);
        $this->items = collect();

        // Проверяем, используется ли мерчант
        $merchant_pay = $this->findMerchantData();

        if($this->isInCurrencyAllowOrder() == 1)
        {
            $this->items->push([
                'field'     =>  'buy',
                'modal'     =>  false,
                'message' => 'В данное время мы не принимаем на данную платежную систему'
            ]);
        }elseif($this->isCashHandler())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message' => 'Вы не выбрали город/страну'
            ]);
        }elseif($this->hourLimitOrderPending() == true or $this->hourLimitOrderProcess() == true)
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message' => 'Вы не можете создать новую заявку на обмен, т.к. ранее созданные Вами заявки не оплачены.'
            ]);
        }elseif($this->forbiddenEmail())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message' => 'Администратором было запрещено использовать e-mail '.$this->getClientEmail()
            ]);
        }elseif (!empty($this->x19Verify())) {
            $this->items->push([
                'field' => 'x19',
                'modal' => false,
                'message' => $this->x19Verify()
            ]);
        }elseif($this->maxLimitInReserve())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  'Направление недоступно, попробуйте позже'
            ]);
        }elseif(!$this->isCardNumberFound($merchant_pay)) {
            $this->items->push([
                'field'     =>  'card',
                'modal'     =>  false,
                'message'   =>  'Указанная карта не соответствует требованиям, используйте другую карту'
            ]);
        }elseif(!$this->outReserveMaxLimit())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  'Превышен максимально установленный лимит резерва, попробуйте позже'
            ]);
        }
        elseif(!$this->outReserveLimitDay())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  'Превышен установленный суточный лимит, попробуйте завтра'
            ]);
        }elseif(!$this->outReserveLimitMonth())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  'Превышен установленный месячный лимит, попробуйте позже'
            ]);
        }elseif($this->directionId->is_holding_direction == 1)
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  __('Направление заморожено')
            ]);
        }elseif($this->isGetLanguages())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  __('Направление запрещено в вашей стране')
            ]);
        } elseif($this->isGetDevice())
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  __('Направление запрещено для вашего типа устройства')
            ]);
        } elseif($this->getInMax() > 0 && $options->get('income_amount') > $this->getInMax() and !$is_limit)
        {
            $amount = (float)$options->get('income_amount') - (float)$this->getDetail('max_price1');

            $translate_min_reserve_1 = __('В данное время нам не требуется больше чем :sum на :payment', [
                'sum' => $this->getDetail('max_price1'),
                'payment' => $this->getInPayment()->name
            ]);
            $translate_min_reserve_2 = __('Лимит превышен на :limit', [
                'limit' => $this->getInCurrencyNumber($amount)
            ]);

            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   => nl2br($translate_min_reserve_1.'<br />'.$translate_min_reserve_2, false)
            ]);
        } elseif($this->getInMin() > 0 && $options->get('income_amount') < $this->getInMin() and !$is_limit)
        {
            $this->items->push([
                'field'     =>  'sell',
                'modal'     =>  false,
                'message'   =>  __('Слишком маленькая сумма для перевода. Сумма обмена должна быть не меньше :min', ['min' => $this->getDetail('min_price1')])
            ]);
        } elseif($this->getOutReserve()->summa < $options->get('outcome_amount') and !$isReserve and !$is_limit)
        {
            if(iEXSetting('unlimited_reserve') == 1 and $this->getOutCurrency()->unlimited_reserve == 1)
            {
                $this->items->push([
                    'modal'     =>  true,
                    'message'   =>  __('В данное время резерва валюты не достаточно для обмена. Если вы желаете создать заявку на нашем сайте то выполнения заявки может затянуться примерно от 1 до 2 часов')
                ]);
            } else {
                $this->items->push([
                    'field'     =>  'buy',
                    'modal'     =>  false,
                    'message'   =>  __('В данное время резерва валюты не достаточно для обмена')
                ]);
            }
        } elseif($this->getOutMin() > 0 && $options->get('outcome_amount') < $this->getOutMin() and !$is_limit)
        {
            $this->items->push([
                'field'     =>  'buy',
                'modal'     =>  false,
                'message'   =>  __('Слишком маленькая сумма для перевода. Сумма обмена должна быть не меньше :min', ['min' => $this->getDetail('min_price2')])
            ]);
        } elseif ($this->getOutMax() > 0 && $options->get('outcome_amount') > $this->getOutMax() and !$is_limit)
        {
            $this->items->push([
                'field'     =>  'buy',
                'modal'     =>  false,
                'message'   =>  __('Не разрешается переводить больше чем').' '.$this->directionId->max_price2. $this->getDetail('sign2')
            ]);
        } elseif($this->isMaxCountUserOrder() or $this->isMaxOrderOneUser() or $this->isMaxOrderOneUserDay()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Превышен лимит создания заявок, обратитесь к оператору')
            ]);

        } elseif($this->isMaxOrderOneEmail() or $this->isMaxOrderOneEmailDay()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Превышен лимит создания заявок с одного e-mail адреса')
            ]);

        } elseif($this->isMaxOrderOneIp() or $this->isMaxOrderOneIpDay()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Превышен лимит создания заявок с одного ip адреса')
            ]);

        } elseif($this->isMaxOrderOneAccountIn() or $this->isMaxOrderOneAccountInDay()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Превышен лимит создания заявок для :account', [
                    'account' => security_xss($this->getInComeAccount())
                ])
            ]);

        } elseif($this->isMaxOrderOneAccountOut() or $this->isMaxOrderOneAccountOutDay()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' =>  __('Превышен лимит создания заявок для :account', [
                    'account' => security_xss($this->getInComeAccount())
                ])
            ]);

        } elseif($this->notIpOrder()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Вы не можете создать заявку, обратитесь к оператору')
            ]);

        } elseif($this->isMaxCountUserOrderHour()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Превышен часовой лимит создания заявок, обратитесь к оператору')
            ]);

        } elseif($this->isMaxCountUserOrderDay()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Превышен дневной лимит создания заявок, обратитесь к оператору')
            ]);

        } elseif($this->hasVerificationLimitAccess()) {
            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Вам ограничен доступ к данному направлению')
            ]);
        } elseif (\auth()->check() and $this->isBlockedSpamOrders())
        {
            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Вы не можете создать заявку, обратитесь к оператору')
            ]);

            add_user_ban(\auth()->id(), '+1 day', 'Нарушено правило создание заявок');
        } elseif($this->getVerifiedAccount())
        {
            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Заявку могут создать только для верифицированные пользователи')
            ]);
        }elseif($this->hasMaxLimitNewbie())
        {
            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Вы не можете обменять больше чем на').' '.$this->directionId->max_amount_newbie.' '.$this->getInCodeCurrency()->name
            ]);
        } elseif($this->hasAMLValidatorIn()) {

            $this->items->push([
                'field' =>  'amlvalidator_check',
                'modal' => false,
                'message' =>  $this->getAMLBotInMessage()
            ]);

        } elseif($this->hasAMLValidatorOut()) {

            $this->items->push([
                'field' =>  'amlvalidator_check',
                'modal' => false,
                'message' =>  $this->getAMLBotOutMessage()
            ]);

        }  elseif($this->isGeoForbiddenCounties()) {

            $this->items->push([
                'field' =>  'get_ip_validate',
                'modal' => false,
                'message' =>  __('Сервис ограничил доступ на обмен в вашей стране')
            ]);
        } elseif($this->isGeoAllowedCounties()) {
            $this->items->push([
                'field' =>  'get_ip_validate',
                'modal' => false,
                'message' =>  __('Сервис ограничил доступ на обмен в вашей стране')
            ]);
        } elseif(!$this->minCountOrderClient()) {

            $this->items->push([
                'field' =>  'limit_order',
                'modal' => false,
                'message' => __('Вы не можете создать заявку по этому направлению, у вас недостаточно успешных заявок')
            ]);

        } else {
            $this->items->push($this->ruleUser());
            $this->items->push($this->ruleDirectionFields());

            // Поля для Валюты (Отдаю и получаю)
            $this->items->push($this->rulesFieldIn());
            $this->items->push($this->rulesFieldOut());

            // Поля отдаю
            $this->items->push($this->ruleInPayment());
            $this->items->push($this->ruleInValidator());

            // Поля получаю
           $this->items->push($this->ruleOutPayment());
           $this->items->push($this->ruleOutValidator());
        }

        // Если дальше не проблем, продолжает записывать данные в базу
        $newClient = false;
        if($this->items->filter()->isEmpty())
        {
            if(Auth::guest())
            {
                // Если отключена автоматическая регистрация клиентов
                if((int)iEXSetting('auto_register') == 1) {
                    $userGuest = User::where('is_guest', '=', 1)->first();
                    $user = $userGuest;
                } else {

                    // Если отключена авто-регистрация в направлениях обмена
                    if($this->directionId->is_disable_auto_reg == 1)
                    {
                        $userGuest = User::where('is_guest', '=', 1)->first();
                        $user = $userGuest;
                    } else {

                        $userID = User::where('email', mb_strtolower($options->get('email')));
                        // Если введенный E-mail не найден в базе

                        if(!$userID->exists())
                        {
                            $newClient = true;
                            $user = $this->registerClient();
                        } else {
                            $user = $userID->first();

                            if(!isset($user->program)) {
                                CashbackFacade::register($user->id);
                            }
                            if(!isset($user->referral_links)) {
                                ReferralSystemFacade::register($user, request()->cookie('ref'));
                            }
                        }
                    }
                }
               // $user = (!$userID->exists() ? $this->registerClient() : $userID->first());
            } else {
                $user = Auth::user();
            }

            if(!$this->isEmailVerificationModal() and !$newClient) {
                // Отправляем уведомление на почту
                $user->sendEmailVerificationNotification();
            }

            // Добавляем данные в базу
            $order = $this->addToDatabase($user, $merchant_pay);

            // Добавлять параметры к общему массиву
            $this->items->put('data', $this->customArrayForCreate($user, $order));
        }


        return $this->items->filter()->sortKeys()->all();
    }

    /**
     * Дополнительная информация в базу
     *
     * @param $user
     * @param $merchant_pay
     * @return Task|\Illuminate\Database\Eloquent\Model
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected $selectedExchangeAmount = 0;
    protected function addToDatabase($user, $merchant_pay)
    {
        // Перед добавление удаляем ненужные точки из суммы в конце
        $in_price = preg_replace("/\.$/", "", $this->getFilterVar('income_amount'));

        // Убираем ненужные дробные числа
        $in_decimal = (int)strlen(substr(strrchr($in_price, '.'), 1));
        $result_in_price = iex_number_format(
            $in_price, ($in_decimal > $this->getInCurrency()->number_format) ? $this->getInCurrency()->number_format : $in_decimal
        );

        // Если есть дополнительная комиссия для отдаю, то прибавляем к общей сумме
        $fee_percent_in = $this->directionId->oth_comm_percent;
        $fee_currency_in = $this->directionId->oth_comm_currency;
        $oth_min_comm = $this->directionId->oth_min_comm;
        $fee_oth_comm1 = 0;
        $result_format_in_price = (float)$result_in_price;
        if($fee_percent_in > 0 or $fee_currency_in > 0) {
            $fee_oth_comm1 = (($result_in_price * $fee_percent_in) / 100) + $fee_currency_in;
            // Минимальная комиссия (Для отдаю)
            if($oth_min_comm > 0 and $fee_oth_comm1 < $oth_min_comm) {
                $fee_oth_comm1 = $oth_min_comm;
            }

            $result_in_price = iex_number_format(floatval($result_in_price) + $fee_oth_comm1, $this->getInCurrency()->number_format);
        }


        // Если есть комиссия платежной системы
        $fee_pay_percent_in = $this->directionId->pay_comm_percent;
        $fee_pay_currency_in = $this->directionId->pay_comm_currency;
        $pay_min_comm = $this->directionId->pay_min_comm;
        $fee_pay_comm1 = 0;
        if($fee_pay_percent_in > 0 or $fee_pay_currency_in > 0) {
            $fee_pay_comm1 = (($result_in_price * $fee_pay_percent_in) / 100) + $fee_pay_currency_in;
            // Минимальная комиссия (Для отдаю)
            if($pay_min_comm > 0 and $fee_pay_comm1 < $pay_min_comm) {
                $fee_pay_comm1 = $pay_min_comm;
            }
            $result_in_price = iex_number_format(floatval($result_in_price) + $fee_pay_comm1, $this->getInCurrency()->number_format);
        }

        $is_new_user = $this->isIdentifyNewbie();
        $currency1 = $this->directionId->currency1;

        /// Пересчитываем курс (мало ли)
        $convert = ConvertFacade::call($this->directionId)->in();

        // Комиссия зависящая от суммы обмена (Настройка)
        if(isset($this->directionId->direction_exchange_percentage_amount) and $this->directionId->direction_exchange_percentage_amount->count() > 0)
        {
            $this->directionId->direction_exchange_percentage_amount->reject(function($item) {
                return $item->amount == 0;
            })->map(function($item) use($result_in_price) {
                if((float)$result_in_price >= (float)$item->amount) {
                    $this->selectedExchangeAmount = $item->id;
                }
            });
        }

        if(isset($this->options['city_id']))
        {
            $city_id = (isset($this->options['city_id']['city_id']) ? $this->options['city_id']['city_id'] : 0);
            $convert = ConvertFacade::call($this->directionId)->in_city($city_id);
        }

        if($this->selectedExchangeAmount > 0) {
            $recount = isset($convert->toArray()['exchange_amount']) ? $convert->toArray()['exchange_amount'][$this->selectedExchangeAmount]['amount'] : $convert->getAmount();
        } else {
            $recount = ($convert->getAmount() > 0) ? $convert->getAmount() : 0;
        }

        if($recount == 0) {
            throw new \Exception('Ошибка при создании заявки');
        }
        $recount_out = iex_number_format($result_format_in_price * $recount, $this->getOutCurrency()->number_format);

        // Скидка на обмен
        $personal_discount_fee = 0;
        if(\auth()->check() and $this->directionId->is_enable_user_discount == 0)
        {
            $personal_discount = \auth()->user()->personal_discount;
            if($personal_discount > 0) {
                $personal_discount_fee = ($recount_out * $personal_discount) / 100;
                $recount_out = iex_number_format((float)$recount_out + $personal_discount_fee, $this->getOutCurrency()->number_format);
            }
        }



        // Если есть дополнительная комиссия для получаю, то прибавляем к общей сумме
        $fee_percent_out = $this->directionId->oth_comm2_percent;
        $fee_currency_out = $this->directionId->oth_comm2_currency;
        $oth_min2_comm = $this->directionId->oth_min2_comm;
        $fee_oth_comm2 = 0;

        $result_format_out_price = (float)$recount_out;

        if($fee_percent_out > 0 or $fee_currency_out > 0) {
            $fee_oth_comm2 = (($recount_out * $fee_percent_out) / 100) + $fee_currency_out;
            // Минимальная комиссия (Для получаю)
            if($oth_min2_comm > 0 and $fee_oth_comm2 < $oth_min2_comm) {
                $fee_oth_comm2 = $oth_min2_comm;
            }

            $recount_out = iex_number_format($recount_out - $fee_oth_comm2, $this->getOutCurrency()->number_format);
        }

        // Если есть комиссия платежной системы для получаю
        $fee_pay_percent_out = $this->directionId->pay_comm2_percent;
        $fee_pay_currency_out = $this->directionId->pay_comm2_currency;
        $pay_min2_comm = $this->directionId->pay_min2_comm;
        $fee_pay_comm2 = 0;

        if($fee_pay_percent_out > 0 or $fee_pay_currency_out > 0)
        {
            $fee_pay_comm2 = (($recount_out * $fee_pay_percent_out) / 100) + $fee_pay_currency_out;
            // Минимальная комиссия (Для получаю)
            if($pay_min2_comm > 0 and $fee_pay_comm2 < $pay_min2_comm) {
                $fee_pay_comm2 = $pay_min2_comm;
            }
            $recount_out = iex_number_format($recount_out - $fee_pay_comm2, $this->getOutCurrency()->number_format);
        }



        // Комиссии, отдаю
        $in_price_fee = 0;
        if(!empty($this->getOutCurrency()->income_notice)) {
            //$result_in_price

            $income_fee_amount = $this->getInCurrency()->commission_merchant_currency;

            $let_price = $result_in_price;
            $in_price_fee = ($let_price + $let_price * $this->getInCurrency()->commission_merchant_percent / 100);
            if($income_fee_amount > 0) {
                $in_price_fee = ($let_price * $this->getInCurrency()->commission_merchant_percent) + $income_fee_amount;
            }
        }


        // Комиссии, получаю
        $out_price_fee = 0;
        if(!empty($this->getOutCurrency()->notice_out)) {
            $outcome_fee_amount = $this->getOutCurrency()->commission_payment_currency;
            $out_price_fee = iex_number_format($recount_out - $outcome_fee_amount, $this->getOutCurrency()->number_format);
        }


        // Здесь мы получаем данные промо кода
        $promo_code_id = 0;
        $promo_code_percent = 0;
        if(isset($this->options['promo_code']) and !empty($this->options['promo_code']))
        {
            $promoCode = PromoCode::where('code', security_xss(trim($this->options['promo_code'])))->first();
            $start_time = Carbon::now();

            if(!empty($promoCode) and $promoCode->status == 1 and Carbon::parse($promoCode->expired_at)->gt($start_time))
            {
                $allowed_to_pass = true;
                // Проверяем, промо-код в списке разрешенных
                $allowed_promo = explode(',', (string)$promoCode->permitted_directions);
                if(!empty($promoCode->permitted_directions) and !in_array($this->directionId->id, $allowed_promo)) {
                    $allowed_to_pass = false;
                }

                // Проверяем, промо-код в списке разрешенных
                $disabled_promo = explode(',', (string)$promoCode->forbidden_directions);
                if(!empty($promoCode->forbidden_directions) and in_array($this->directionId->id, $disabled_promo)) {
                    $allowed_to_pass = false;
                }

                if($allowed_to_pass == true)
                {
                    if(($promoCode->used < $promoCode->count_uses))
                    {
                        $promo_code_id = $promoCode->id;
                        $promoPercent = ($recount_out / 100) * $promoCode->discount_percent;
                        $promo_code_percent = $promoCode->discount_percent;

                        $recount_out = iex_number_format((float)$recount_out + (float)$promoPercent, $this->getOutCurrency()->number_format);
                        $promoCode->update(['used' => $promoCode->used + 1]);

                        if($promoCode->used >= $promoCode->count_uses) {
                            $promoCode->update(['status' => 2]);
                        }
                    }
                }
            }
        }

        $newOrder = Task::create([
            'public_id'  => iex_order_id(),
            'id_user'  =>  $user->id,
            'is_hold'   =>  $this->isHold(),
            'course_float' => $recount,
            'course_display' => $convert->getCourse(),
            'id_direction_exchange' => $this->directionId->id,
            'is_new_user'   =>  (isset($this->options['email']) ? 1 : 0),
            'id_payment_requisites' => 0,
            'give_price' => $result_in_price - $fee_pay_comm1,
            'give_price_default' => $result_format_in_price,
            'give_price_with_comm'  =>  $result_in_price - $fee_pay_comm1,
            'give_price_with_comm_pay' => (float)$result_in_price,
            'give_price_fee_comm' => $fee_oth_comm1,
            'give_price_fee_pay' => $fee_pay_comm1,

            'receiving_price' => (float)$recount_out,
            'receiving_price_default' => $result_format_out_price - $personal_discount_fee,
            'receiving_price_with_comm' => (float)$result_format_out_price - $fee_oth_comm2,
            'receiving_price_with_comm_pay' => $recount_out,
            'receiving_price_fee_comm' => $fee_oth_comm2,
            'receiving_price_fee_pay' => $fee_pay_comm2,

            'in_price_fee' => $in_price_fee,
            'out_price_fee' => $out_price_fee,
            'from_shot' => security_xss($this->getInComeAccount()),
            'to_shot' => security_xss($this->getOutComeAccount()),
            'markup1' => ($this->directionId->add_course1 ?? null),
            'markup2' => ($this->directionId->add_course2 ?? null),
            'status' => 2,
            'email' => $this->getClientEmail(),
            'phone' =>   $this->getClientPhone(),
            'ip' => $this->clientIp(),
            'unique_security_code' => generate_security_code(51), // Секретный код
            'telegram_id'   =>  ($this->options['telegram_id'] ?? null),
            'id_promo_code' =>  $promo_code_id,
            'promo_code_discount' => $promo_code_percent,
            'referral_hash'       =>  request()->cookie('ref') ?? null
        ]);

        // Записываем номер счета в уникальную базу для проверки на изменения
        if(!empty($this->getOutComeAccount()))
        {
            TaskShot::create([
                'id_task' => $newOrder->id,
                'account' => security_xss($this->getOutComeAccount()),
            ]);
        }

        if(!empty($this->getAdditionFieldsDirection()))
        {
            $task_direction_fields = [];

            foreach($this->getAdditionFieldsDirection() as $key => $additionField)
            {
                // Ключ получаем
                $field_item = DirectionField::whereKeyId($key)->first();

                if(isset($field_item))
                {
                    // Проверяем, нужно ли удалять лишние пробелы
                    if($field_item->remove_spaces == 1) {
                        $additionField = remove_all_spaces($additionField);
                    }

                    $task_direction_fields[] = [
                        'id_task' => $newOrder->id,
                        'id_field' => $field_item->id,
                        'field_name' => $field_item->name,
                        'field_value' => $additionField,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'alias' => 'direction'
                    ];
                }
            }

            if(!empty($task_direction_fields)) {
                TaskField::insert($task_direction_fields);
            }
        }

        $income_fields = $this->getAdditionFieldsIn();
        $outcome_fields = $this->getAdditionFieldsOut();
        $task_fields = [];


        // Записываем доп. поля (Для отдаю)
        if(!empty($income_fields))
        {
            foreach($income_fields as $key => $additionField)
            {
                // Ключ получаем
                $field_item = CurrencyFields::whereKeyId($key)->first();

                if(isset($field_item)) {

                    // Фильтруем полученные значения
                    $filter_field_value = $additionField;
                    if($field_item->remove_spaces == 1) {
                        $filter_field_value = remove_all_spaces($filter_field_value);
                    }

                    $task_fields[] = [
                        'type_field' => 'in',
                        'id_task' => $newOrder->id,
                        'id_field' => $field_item->id,
                        'field_name' => $field_item->name,
                        'field_value' => $filter_field_value,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'alias' => 'currency'
                    ];
                }
            }
        }

        // Записываем доп. поля (Для получаю)
        if(!empty($outcome_fields))
        {
            foreach($outcome_fields as $key => $additionField)
            {
                // Ключ получаем
                $field_item = CurrencyFields::whereKeyId($key)->first();
                if(isset($field_item))
                {

                    // Фильтруем полученные значения
                    $filter_field_value = $additionField;
                    if($field_item->remove_spaces == 1) {
                        $filter_field_value = remove_all_spaces($filter_field_value);
                    }

                    $task_fields[] = [
                        'type_field' => 'out',
                        'id_task' => $newOrder->id,
                        'id_field' => $field_item->id,
                        'field_name' => $field_item->name,
                        'field_value' => $filter_field_value,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                        'alias' => 'currency'
                    ];
                }
            }
        }

        if(!empty($task_fields)) {
            TaskField::insert($task_fields);
        }

        //
        $authInfo = $user;
        $this->authId = $authInfo->id;
        $this->authInfo = $authInfo;

        if((int)iEXSetting('is_style_agreement_rules') == 1 and (int)iEXSetting('is_exchange_rules_remember_checkbox') == 1)
        {
            // Проверка галочек
            if($authInfo->is_checkbox_rules == 0) {
                $authInfo->is_checkbox_rules = $this->options['exchange_rules'] ?? 0;
            }

            if($this->directionId->currency1->is_kyc_checkbox == 1 and $authInfo->is_checkbox_aml == 0) {
                $authInfo->is_checkbox_aml = $this->options['exchange_aml'] ?? 0;
            }
        }

        $authInfo->save();


        // Записываем ID Заявки
        $this->setOrder($newOrder);

        if(isset($merchant_pay) and $newOrder->id_merchant == 0) {
            // Если, ID мерчанта не зафиксировано, Фиксируем.
            if($this->getOrder()->id_merchant == 0) {
                $newOrder->id_merchant = $merchant_pay->id;
                $newOrder->merchant_provider = $merchant_pay->gateway->alias;
            }
        }


        // Для дополнительной безопасности, пересчитаем сумму по курсу которю отдал клиент
        // $this->recount($result_in_price);

        // ФУНКЦИИ УСТАРЕЛИ
        $newOrder->big_id = $this->orderBigId($newOrder->id);
        $newOrder->random_id = $this->orderRandomId();


        if(isset($merchant_pay))
        {
            $configProvider = PaymentFacade::configProvider($merchant_pay->gateway->alias);
            // Транзакция будет выполнена через cron
            if(isset($configProvider) and isset($configProvider['check_payment']) and $configProvider['check_payment'] and (int)$merchant_pay->max_register_blockchain > 0) {
                $newOrder->is_bot = 1;
                $newOrder->is_auto_check_pay = 1;
            }
        }


        // Дополнительная проверка заявки (Coin -> Coin)
        if(iEXSetting('is_extended_order_check') == 1)
            $this->verifyCurrentCryptoRate($newOrder);

        // Заполнение доп. полей
        foreach ($this->availableDefaultFields as $key_id) {
            if(isset($this->options[$key_id])) {
                $newOrder->{$key_id} = security_xss($this->options[$key_id]);
            }
        }

        $is_check_modal = (int)$this->getInCurrency()->is_auto_check_modal;
        if($is_check_modal == 1) {
            $newOrder->is_autopay_modal = 1;
        }

        $newOrder->save();

        // Записываем данные в файл
        if((int)iEXSetting('is_save_order_data_to_file') == 1)
        {
            if(!\File::isDirectory(storage_path('/orders'))) {
                \File::makeDirectory(storage_path('/orders'), 0777, true, true);
            }
            \File::put(storage_path('/orders/order_'. $newOrder->id.'_create').'.txt', json_encode($newOrder));
        }

        // Записываем информацию по карте
        if($this->getOutCurrency()->is_card_detail == 1 and wallet_validator('luhn', $newOrder->to_shot))
        {
            if(Module::find('CardInfo')->isEnabled() == 1)
            {
                try {
                    $bin_bank = CardInfoFacade::driver((string)iEXSetting('cardinfo_driver'))->setCardNumber($newOrder->to_shot);

                    TaskCardDetail::create([
                        'id_task' => $newOrder->id,
                        'card_number' => $newOrder->to_shot,
                        'payment_system' => $bin_bank->getPaymentSystem(),
                        'type_card' => $bin_bank->getType(),
                        'brand_card' => $bin_bank->getBrand(),
                        'country_name' => $bin_bank->getCountryName(),
                        'country_currency' => $bin_bank->geCurrency(),
                        'bank_name' => $bin_bank->getBankName(),
                        'bank_url' => $bin_bank->getBankUrl(),
                        'bank_phone' => $bin_bank->getBankSupportNumber(),
                    ]);
                }catch (\Exception $exception) {
                    \Log::debug($exception->getMessage());
                }
            }
        }

        $this->additionInfo($newOrder, $is_new_user);
        return $newOrder;
    }

    /**
     * Дополнительные параметры проверки
     *
     * @param Task $order
     * @param bool $is_newbie
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function additionInfo(Task $order, bool $is_newbie = false)
    {
        // Создаем tx
        Transaction::create([
            'id_task'       =>  $order->id,
            'transaction'   =>  Str::orderedUuid()
        ]);

        // Проверяем данные в белом списке
        $is_whitelist = $this->searchByLocalWhitelist($order);

        $is_local_scam = 0;
        // Для начала ищем в своей базе
        if($this->searchByLocalBlacklist($order) and !$is_whitelist) {
            $this->is_scam = 1;
            $is_local_scam = 1;
        }

        // Если включена данная функция, то при определении мошенника из bestchange,
        // заявка будет заморожено (Этот пункт разрешается правилами bestchange)
        if((int)iEXSetting('is_freeze_scam_order') == 1) {
            $this->is_freeze_scam = $this->scamCheckBestchange($order);
        } else {
            $this->is_freeze_scam = $this->searchByLocalDebtors($order);

            if($this->is_freeze_scam)
                $this->is_freeze_local = true;

            //Проверяем, email, ip на мошеничество
            if($is_local_scam == 0 and !$is_whitelist) {
                $this->is_scam = $this->searchForBlacklist($order);
            }
        }


        $city_id = 0;
        $city_name = null;
        $country_id = 0;
        $country_name = null;
        if(Module::find('Cities')->isEnabled() == 1)
        {
            if(isset($this->options['city_id']))
            {
                $city_id = (isset($this->options['city_id']['city_id']) ? $this->options['city_id']['city_id'] : 0);
                if($city_id > 0)
                {
                    $direction_city = DirectionExchangeCity::find($city_id);
                    $city_item = CitiesModel::find($direction_city->city_id);
                    $city_id = $city_item->id;
                    $city_name = $city_item->name;
                }
            }

            if(isset($this->options['country_id']))
            {
                $country_id = (isset($this->options['country_id']['country_id']) ? $this->options['country_id']['country_id'] : 0);
                if($country_id > 0) {
                    $country_item = GeoCountryList::find($country_id);
                    $country_name = $country_item->value;
                }
            }
        }

//        $network_id = 0;
//        $network_name = null;
//            if(isset($this->options['network_id'])) {
//                $network_id = (isset($this->options['network_id']['network_id']) ? $this->options['network_id']['network_id'] : 0);
//                if($network_id > 0) {
//                    $network_item = CurrencyNetwork::find($network_id);
//                    $network_name = $network_item->name;
//                }
//            }

        // Для безопасной проц
        $task_info = TaskInfo::create([
            'id_task'       =>  $order->id,
            'is_scam'       =>  $this->is_scam,
            'is_freeze_scam' => $this->is_freeze_scam,
            'is_freeze_local' => $this->is_freeze_local,
            'is_local_scam' =>  $is_local_scam,
            'ip'            =>  $order->ip,
            'device'        =>  $this->getUserDevice(),
            'newbie'        =>  ($is_newbie ? 0 : 1),
            'language'      =>  app()->getLocale(),
            'is_not_bonus'  => $this->directionId->is_not_bonus,
            'is_not_partner' => $this->directionId->is_not_partner,
            'is_bestchange_parser' => $this->directionId->enable_bestchange,
            'bestchange_position'  => $this->getBestchangePosition(),
            'in_min_amount'         =>  $this->directionId->min_price1,
            'in_max_amount'         =>  $this->directionId->max_price1,
            'aml_address_risk_in'        =>  $this->aml_address_risk_in,
            'aml_address_risk_out'       =>  $this->aml_address_risk_out,
            'city_id'                   =>  $city_id,
            'city_name'                 => $city_name,
            'country_id'                   =>  $country_id,
            'country_name'                 => $country_name,
            'dot_not_remember_data'         =>  $this->options['dot_not_remember_data'] ?? 0
        ]);

        // Записываем прибыль в USD
        $profit_percent = $this->getOrder()->give_price / 100 * $this->directionId->profit;
        $profit_group = 0;
        // Групповой прибыль
        if($this->directionId->enable_group_commission)
            $profit_group = $this->getOrder()->give_price / 100 * $this->directionId->group_commission->give;

        // Получаем прибыль в рублях
        $profit_rub = convert_to_rub(
            $this->getInCodeCurrency()->name,
            $profit_percent + $profit_group + $this->directionId->profit_s
        );

        TaskReport::create([
            'id_task'                   =>  $order->id,
            'profit'                    =>  $this->directionId->profit,
            'profit_s'                  =>  $this->directionId->profit_s,
            'add_course'                =>  $this->directionId->add_course1,
            'your_add_course'           =>  $this->directionId->your_add_course1,
            'commission'                =>  $this->directionId->commission1,
            'commission_s'              =>  $this->directionId->commission_s1,
            'group_commission_value'    =>  ($this->directionId->enable_group_commission ? $this->directionId->group_commission->give : 0),
            'sign'                      =>  $this->directionId->sign,
            'profit_rub'                =>  iex_number_format($profit_rub, 4),
            'is_bestchange'             =>  $this->directionId->enable_bestchange
        ]);

        // Отправляем уведомление в Telegram о создании заявки
        if((int)iEXSetting('is_notify_process_create_order') == 1) {
            $delay = now()->addSeconds(15);
            dispatch(new TelegramOrderJob($this->getOrder()))
                ->delay($delay)
                ->onQueue('high');
        }

        // Отправляем уведомление клиенту о новой заявке
        iex_email_notify_new_order($this->getOrder(), 1);

        try {
            $this->clientGeo($task_info);
        }catch (\Exception $exception) {
            \Log::error('GeoIP Error: '. $exception->getMessage());
        }
    }

    /**
     * Определение новичка
     *
     * @return boolean
    */
    protected function isIdentifyNewbie(): bool
    {
        foreach (explode(',', (string)iEXSetting('order_identify_newbie')) as $item)
        {
            if($item == 0 and !is_null($this->getInComeAccount()) and Task::where('from_shot', '=', $this->getInComeAccount())->exists())
                return true;
            elseif($item == 1 and !is_null($this->getOutComeAccount()) and Task::where('to_shot', '=', $this->getOutComeAccount())->exists())
                return true;
            elseif($item == 2 and !is_null($this->getClientEmail()) and Task::where('email', '=', $this->getClientEmail())->exists())
                return true;
            elseif($item == 3 and !is_null($this->clientIp()) and Task::where('ip', '=', $this->clientIp())->exists())
                return true;
        }
        return false;
    }

    /**
     * Для проверки, записываем курс из CoinMarketCap
     *
     * @param Task $newOrder
     */
    protected function verifyCurrentCryptoRate(Task $newOrder)
    {
        //Публикуем еще
        $in_currency = $newOrder->direction_exchange->currency1;
        $out_currency = $newOrder->direction_exchange->currency2;

        //Если обмен между криптовалютами
        if($in_currency->id_filter_currency == 3 and $out_currency->id_filter_currency == 3)
        {
            $rate = CryptoFacade::driver('CoinMarketCap')
                ->set(
                    mb_strtoupper($in_currency->code_currency->name),
                    mb_strtoupper($out_currency->code_currency->name)
                )
                ->getPrice(true, $newOrder->give_price);

            TasksRatesData::create([
                'id_task'           =>  $newOrder->id,
                'exchange_course'   =>  $this->toFormat($newOrder->give_price * $newOrder->course_float),
                'market_course'     =>  $this->toFormat($rate)
            ]);
        }
    }

    protected function getFilterVar($type) {
        return (isset($this->options[$type]) ? $this->options[$type]: null);
    }

    /**
     * Создаем учетную запись
     *
     * @return \App\Models\User
    */
    protected function registerClient()
    {
        // Создание пароля в случае если клиента нет в базе
        $password = Str::random(10);

        $newUser = User::create([
            'name'      =>  'iEXDefault',
            'ip_address'=>  $this->clientIp(),
            'email'     =>  mb_strtolower($this->options['email']),
            'password'  =>  Hash::make($password),
            'language'  =>  app()->getLocale()
        ]);

        // Регистрируем имя
        if(!empty(iEXContentLanguage('username_new_user')))
        {
            $name = str_replace(
                [':id:', ':random:'], [$newUser->id, Str::random(7)], iEXContentLanguage('username_new_user')
            );
            $newUser->update(['name' => $name]);
        }

        //Регистрация реферала
        ReferralSystemFacade::register($newUser, request()->cookie('ref'));
        //Регистрируем бонусную программу
        CashbackFacade::register($newUser->id);

        // Отправляем уведомление о регистрационных данных
        if(iEXSetting('is_email_auto_user'))
        {
            $delay = now()->addSeconds(5);
            dispatch(new RegisterClientJob($newUser, $password))->delay($delay)->onQueue('low');
        }

        // Отправляем уведомление на почту
        $newUser->sendEmailVerificationNotification();

        //Если пользователь в процессе создания заявки не прошел авторизацию то авторизуем его
        $remember = (iEXSetting('is_remember_login') == 1 ? true : false);
        Auth::login($newUser, $remember);

        return $newUser;
    }

    /**
     * Создание новой заявки
     *
     * @param string $type
     * @param array $params
     * @return array|null|string
     */
    public function getMessages($type, $params = [])
    {
        switch ($type)
        {
            case 'validate_luhn':
                return __('ticker.error.credit_card');
            case 'validate_yandexmoney':
                return __('ticker.error.yandex_money_valid');
            case 'validate_in':
                return __('ticker.error.wrong_address', [
                    'payment'   =>  $this->getInPayment()->name
                ]);
            case 'validate':
                return __('ticker.error.wrong_address', [
                    'payment'   =>  $this->getOutPayment()->name
                ]);
        }
    }

    protected function getDetail($name)
    {
        switch ($name) {
            case 'input_payment':
                return $this->getInCurrency()->visible_give == 0 &&
                    $this->getInCurrency()->min_char != 0 &&
                    $this->getInCurrency()->max_char != 0;
            case 'output_payment':
                return $this->getOutCurrency()->visible_receiving == 0 &&
                    $this->getOutCurrency()->min_char != 0 &&
                    $this->getOutCurrency()->max_char != 0;
            case 'sign1':
                return $this->getInCodeCurrency()->sign;
            case 'sign2':
                return $this->getOutCodeCurrency()->sign;
            case 'min_price1':
                return $this->getInMinPrice(true);
            case 'min_price2':
                return $this->getOutMinPrice(true);
            case 'max_price1':
                return $this->getInMaxPrice(true);
            case 'max_price2':
                return $this->getOutMaxPrice(true);
            case 'payment1_name':
                return $this->getInPayment()->name;
            case 'reserve':
                return iex_reserve_amount($this->getOutCurrency()->reserve);
            case 'reserve_sign':
                return $this->convertToIn().' '.$this->getInCodeCurrency()->sign;
            case 'black_reserve':
                return $this->getOutCurrency()->reserve->black_amount;
            case 'is_black_reserve':
                return $this->getOutCurrency()->reserve->is_non_standard;
        }
    }
}
