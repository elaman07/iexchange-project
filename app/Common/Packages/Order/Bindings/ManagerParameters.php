<?php
namespace App\Common\Packages\Order\Bindings;

use App\Models\BlacklistOrder;
use App\Models\TaskInfo;
use App\Models\WhitelistOrder;
use Modules\Debtors\Entities\Debtor;

trait ManagerParameters
{
    private $amlbot_in_message = null;
    private $amlbot_out_message = null;

    /**
     * Получение сообщений (Отдаю)
     */
    public function getAMLBotInMessage()
    {
        return $this->amlbot_in_message;
    }

    /**
     * Запись сообщений (Отдаю)
     *
     * @param string $text
     */
    public function setAMLBotInMessage(string $text) {
        $this->amlbot_in_message = $text;
        return;
    }

    /**
     * Получение сообщений (Получаю)
     */
    public function getAMLBotOutMessage()
    {
        return $this->amlbot_out_message;
    }

    /**
     * Запись сообщений (Получаю)
     * @param string $text
     */
    public function setAMLBotOutMessage(string $text) {
        $this->amlbot_out_message = $text;
        return;
    }

    /**
     * Определяем устройство с которого была создана заявка
     *
     * @return string
    */
    public function getUserDevice(): string
    {
        if(\Browser::isDesktop())
            return 'desktop';
        elseif(\Browser::isMobile())
            return 'mobile';
        elseif(\Browser::isTablet())
            return 'tablet';
        return 'desktop';
    }

    /**
     * Собираем пользовательскую информацию
     *
     * @param TaskInfo $taskInfo
     * @throws \Exception
     */
    public function clientGeo(TaskInfo $taskInfo)
    {
        if(!iEXSetting('is_geo_ip'))
            return;

        $geo = geoip(request()->ip());

        if($geo->isAttribute('iso_code')) {
            $taskInfo->code_country = $geo->getAttribute('iso_code');
        }

        if($geo->isAttribute('country')) {
            $taskInfo->country = $geo->getAttribute('country');
        }

        if($geo->isAttribute('city')) {
            $taskInfo->city = $geo->getAttribute('city');
        }

        $taskInfo->save();
    }

    /**
     * Ищем мошенников в своей базе
     *
     * @param $order
     * @return bool
     */
    public function searchByLocalBlacklist($order)
    {
        // Статус активации черного списка
        if(!iEXSetting('is_local_blacklist'))
            return false;

        $response = BlacklistOrder::where(function ($query) use($order) {

            $query->where('value', 'like', '%' . $order->ip . '%')
                ->orWhere('value', 'like', '%' . $order->email . '%');

            // Проверка получающих счетов
            if(!is_null($order->to_shot)) {
                $query->orWhere('value', 'like', '%' . preg_replace('/\s+/', '', $order->to_shot) . '%');
            }

            // Проверка отдающих счетов
            if(!is_null($order->from_shot)) {
                $query->orWhere('value', 'like', '%' . preg_replace('/\s+/', '', $order->from_shot) . '%');
            }
        });


        return $response->exists();
    }

    /**
     * Ищем мошенников в своей базе
     *
     * @param $order
     * @return bool
     */
    public function searchByLocalDebtors($order)
    {
        $response = Debtor::where(function ($query) use($order) {

            $query->where('value', 'like', '%' . $order->ip . '%')
                ->orWhere('value', 'like', '%' . $order->email . '%');

            // Проверка получающих счетов
            if(!is_null($order->to_shot)) {
                $query->orWhere('value', 'like', '%' . preg_replace('/\s+/', '', $order->to_shot) . '%');
            }

            // Проверка отдающих счетов
            if(!is_null($order->from_shot)) {
                $query->orWhere('value', 'like', '%' . preg_replace('/\s+/', '', $order->from_shot) . '%');
            }
        });


        return $response->exists();
    }

    /**
     * Проверяем данные в белом списке
     *
     * @param $order
     * @return bool
     */
    public function searchByLocalWhitelist($order)
    {
        // Статус активации черного списка
        if(!iEXSetting('is_local_whitelist'))
            return false;

        return WhitelistOrder::where('value', $order->ip)
            ->orWhere('value', $order->email)
            ->orWhere('value', $order->to_shot)->exists();
    }

    /**
     * Ищем клиента в базе мошенников bestchange
     *
     * @param null $order
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchForBlacklist($order)
    {
        // Проверяем, включена ли функция поиска
        if(!iEXSetting('is_bestchange_blacklist')) return false;
        // Если значение не массивное, останавливаем процесс
        if(!is_array(explode(',', iEXSetting('bestchange_api_categories')))) return false;

        foreach (explode(',', iEXSetting('bestchange_api_categories')) as $item) {
            if($item == 'ip' and !empty($order->ip) and isBlacklist($order->ip) != 0) return true;
            if($item == 'email' and !empty($order->email) and isBlacklist($order->email) != 0) return true;
            if($item == 'wallet_from' and !empty($order->from_shot) and isBlacklist($order->from_shot) != 0) return true;
            if($item == 'wallet_to' and !empty($order->to_shot) and isBlacklist($order->to_shot) != 0) return true;
        }

        return false;
    }


    /**
     * Определяем мошенников
     *
     * @param $order
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function scamCheckBestchange($order)
    {
        // Проверяем, включена ли функция поиска
        if(!iEXSetting('is_bestchange_blacklist')) return false;

        $options = [
            'type' => 's',
            'columns' => 'c',
            'reject'    =>  true
        ];

        if(!empty($order->email) and isBlacklist($order->email, $options) != 0)
            return true;
        if(!empty($order->to_shot) and isBlacklist($order->to_shot, $options) != 0)
            return true;
        if(!empty($order->from_shot) and isBlacklist($order->from_shot, $options) != 0)
            return true;

        return false;
    }
}
