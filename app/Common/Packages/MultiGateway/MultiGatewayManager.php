<?php
namespace App\Common\Packages\MultiGateway;


use App\Common\Packages\MultiGateway\Adapters\QiwiAdapter;
use App\Common\Packages\MultiGateway\Adapters\QiwiCardAdapter;
use App\Common\Packages\MultiGateway\Adapters\YandexMoneyAdapter;
use Illuminate\Support\Manager;

class MultiGatewayManager extends Manager
{
    /**
     * Драйвер BB Code
     */
    public function createYandexMoneyDriver()
    {
        return new YandexMoneyAdapter();
    }

    /**
     * Драйвер BB Code
     */
    public function createQiwiDriver()
    {
        return new QiwiAdapter();
    }

    /**
     * Драйвер BB Code
     */
    public function createQiwiCardDriver()
    {
        return new QiwiCardAdapter();
    }

    /**
     * Драйвер по умолчанию
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return null;
    }
}