<?php
namespace App\Common\Packages\MultiGateway;

use App\Common\Packages\Crypto\Compilers\Compiler;
use App\Common\Packages\Crypto\Contracts\CompilerFactory;
use App\Common\Packages\Crypto\Contracts\Factory;
use Illuminate\Support\ServiceProvider;

class MultiGatewayServiceProvider extends ServiceProvider
{
    public function boot() {

    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('multi-gateway', function($app) {
            return new MultiGatewayManager($app);
        });
    }
}