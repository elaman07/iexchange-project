<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.10.2020
 * Time: 8:25
 */

namespace App\Common\Packages\MultiGateway\Adapters;


abstract class AbstractAdapter
{
    /**
     * Тип драйвера
     *
     * @var string
    */
    protected $type_driver = null;
}