<?php
namespace App\Common\Packages\MultiGateway\Adapters;


class YandexMoneyAdapter
{
    /**
     * Добавить новый мерчант (автовыплату)
     *
     * @param array $options
     * @return bool
     */
    public function create(array $options = [])
    {
        $array = var_export([
            'iex_key'        => isset($options['account']) ? $options['account'] : null,
            'account'        => isset($options['account']) ? $options['account'] : null,
            'password'       => isset($options['password']) ? $options['password'] : null,
            'client_id'      => !empty($options['client_id']) ? $options['client_id'] : null,
            'client_secret'  => !empty($options['client_secret']) ? $options['client_secret'] : null,
            'paymentType'    => 'PC',
            'access_token'   => ''
        ], true);

        $filename = config_path('iex-gateways/yandexmoney/'.$options['account'].'.php');
        if(!\File::exists($filename)) {
        \File::put($filename, "<?php\n return {$array};\n");
            return true;
        }
        return false;
    }

    public function update(array $options = [])
    {
        // Получить файл
        $config = \Config::get('iex-gateways.yandexmoney.'.$options['account']);
        $array = var_export([
            'iex_key'        => isset($options['account']) ? $options['account'] : $config['account'],
            'account'        => isset($options['account']) ? $options['account'] : $config['account'],
            'password'       => isset($options['password']) ? $options['password'] : $config['password'],
            'client_id'      => !empty($options['client_id']) ? $options['client_id'] : $config['client_id'],
            'client_secret'  => !empty($options['client_secret']) ? $options['client_secret'] : $config['client_secret'],
            'paymentType'    => 'PC',
            'access_token'   => !empty($options['access_token']) ? $options['access_token'] : '',
        ], true);

        $filename = config_path('iex-gateways/yandexmoney/'.$options['account'].'.php');
        if(\File::isFile($filename)) {
            \File::put($filename, "<?php\n return {$array};\n");
            return true;
        }
        return false;
    }


    /**
     * Валидатор полей
     *
     * @return array
    */
    public function validator()
    {
        return [
            'account' => ['numeric', 'required'],
            'password' => ['required']
        ];
    }

    public function remove()
    {

    }
}