<?php
namespace App\Common\Packages\MultiGateway\Adapters;


class QiwiAdapter
{
    /**
     * Добавить новый мерчант (автовыплату)
     *
     * @param array $options
     * @return bool
     */
    public function create(array $options = [])
    {
        $array = var_export([
            'iex_key'        => isset($options['account']) ? $options['account'] : null,
            'account'        => isset($options['account']) ? $options['account'] : null,
            'secret_key'       => isset($options['secret_key']) ? $options['secret_key'] : null,
            'hook_id'      => !empty($options['hook_id']) ? $options['hook_id'] : null,
            'webhook_key'  => !empty($options['webhook_key']) ? $options['webhook_key'] : null,
        ], true);

        $filename = config_path('iex-gateways/qiwi/'.$options['account'].'.php');
        if(!\File::exists($filename)) {
        \File::put($filename, "<?php\n return {$array};\n");
            return true;
        }
        return false;
    }

    public function update(array $options = [])
    {
        // Получить файл
        $config = \Config::get('iex-gateways.qiwi.'.$options['account']);

        $array = var_export([
            'iex_key'        => isset($options['account']) ? $options['account'] : $config['account'],
            'account'        => isset($options['account']) ? $options['account'] : $config['account'],
            'secret_key'       => isset($options['secret_key']) ? $options['secret_key'] : $config['secret_key'],
            'hook_id'      => !empty($options['hook_id']) ? $options['hook_id'] : $config['hook_id'],
            'webhook_key'  => !empty($options['webhook_key']) ? $options['webhook_key'] : $config['webhook_key'],
        ], true);

        $filename = config_path('iex-gateways/qiwi/'.$options['account'].'.php');
        if(\File::isFile($filename)) {
            \File::put($filename, "<?php\n return {$array};\n");
            return true;
        }
        return false;
    }


    /**
     * Валидатор полей
     *
     * @return array
    */
    public function validator()
    {
        return [
            'account' => ['numeric', 'required'],
            'secret_key' => ['required']
        ];
    }

    public function remove()
    {

    }
}