<?php

namespace App\Common\Packages\Luhn;

use Illuminate\Contracts\Validation\Rule;

class LuhnRule implements Rule
{
    protected $luhn;
    public function __construct(LuhnAlgorithmContract $luhn = null)
    {
        $this->luhn = $luhn ?? app(LuhnAlgorithmContract::class);
    }
    public function passes($attribute, $value)
    {
        return $this->luhn->isValid($value);
    }
    public function message()
    {
        return trans('validation.luhn') != 'validation.luhn'
            ? trans('validation.luhn')
            : trans('luhn::validation.luhn');
    }
}