<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 10.02.2019
 * Time: 10:14
 */

namespace App\Common\Packages\Luhn\Facades;

use Illuminate\Support\Facades\Facade;

class LuhnFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'luhn';
    }
}
