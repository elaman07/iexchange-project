<?php
namespace App\Common\Packages\Angular;

use Illuminate\Support\Facades\Facade;

/**

 */
class AngularFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'angular';
    }
}
