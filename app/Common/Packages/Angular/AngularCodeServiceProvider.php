<?php
namespace App\Common\Packages\Angular;

use App\Common\Packages\Angular\Angular;
use Illuminate\Support\ServiceProvider;

class AngularCodeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Зарегистрируйте поставщика услуг.
     *
     * @return void
     */

    public function register()
    {
        $this->app->bind('angular', function() {
            return new Angular();
        });
    }
}