<?php
namespace App\Common\Packages\Angular\Manages;


use App\Common\Packages\Payment\PaymentFacade;
use App\Http\Resources\Angular\UserWalletStoryResource;
use App\Http\Resources\Sessions\CurrentUserResource;
use App\Http\Resources\V2\Review\ReviewResponse;
use App\Models\Advantage;
use App\Models\Banner;
use App\Models\CodeCurrency;
use App\Models\Contact;
use App\Models\DirectionExchange;
use App\Models\InfoStatistic;
use App\Models\LinksFooterGroup;
use App\Models\Menu;
use App\Models\News;
use App\Models\NoticeExchange;
use App\Models\Partner;
use App\Models\PromoCode;
use App\Models\Reserve;
use App\Models\Review;
use App\Models\RewardProgram;
use App\Models\SelectedCourse;
use App\Models\SocialAuthSystem;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\User;
use App\Models\UserWalletStories;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Modules\Contests\Entities\ContestModel;
use Nwidart\Modules\Facades\Module;

trait ManagesConfig
{
    protected function cashbackConfig()
    {
        return Cache::remember('cashback-system', Carbon::now()->addHour(), function()
        {
            return RewardProgram::cursor()->map(function($item) {
                return [
                    'id'        =>  $item->id,
                    'amount'    =>  $item->amount,
                    'percent'   =>  $item->percent
                ];
            })->toArray();
        });
    }


    public function configBanners()
    {
        return Banner::with('buttons')
            ->where('status', '=', 0)->orderBy('sorting')
            ->get()->map(function($item) {
            return [
                'title' => $item->title,
                'text' => $item->text,
                'icon' => $item->images,
                'color_title' => $item->color_title,
                'color_text' => $item->color_text,
                'banner_image' => $item->images_banner,
                'buttons' => $item->buttons->map(function($button) {
                    return [
                        'name' => $button->name,
                        'link' => $button->link,
                        'color_bg' => $button->color_bg_button,
                        'color_text' => $button->color_text_button
                    ];
                })
            ];
        });
    }


    /**
     * Информация о контактах
     *
     * @return array|mixed
     */
    protected function contacts(): mixed
    {
        if(config('cache.enable_cache') and (int)iEXSetting('enable_cache_contacts'))
        {
            return Cache::remember('contacts_'.$this->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->unCacheContacts();
            });
        }

        return $this->unCacheContacts();
    }

    /**
     * Информация о контактах
    */
    protected function unCacheContacts(): array
    {
        return Contact::where('is_home', '=', 1)->orderBy('sorting')->get()->map(function($item)
        {
            return [
                'name' => $item->name,
                'type' => $item->type,
                'value' => $item->value,
                'url' => $item->url,
                'block_size' => $item->block_size,
                'sorting' => $item->sorting,
                'is_home' => $item->is_home,
                'is_button' => $item->is_button,
                'icon_url'  => !empty($item->icon) ? '/storage/contact/'.$item->icon : null,
                'text_color' => $item->text_color
            ];
        })->toArray();
    }

    protected function socialAuth()
    {
        return Cache::remember('social-auth-login', Carbon::now()->addHour(), function()
        {
            return SocialAuthSystem::where('status', '=', 1)
                ->orderBy('sorting')->cursor()
                ->pluck('alias')->toArray();
        });
    }

    /**
     * Кэшируем меню
     */
    protected function configMenu()
    {
        if(config('cache.enable_cache'))
        {
            return Cache::remember('menu_'.$this->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return Menu::where('status', 1)->orderBy('sorting')->get()->map(function($item) {
                    return [
                        'name'  => $item->name,
                        'slug'  => $item->slug,
                        'text_color' => $item->text_color
                    ];
                });
            });
        }

        return Menu::where('status', 1)->orderBy('sorting')->get()->map(function($item) {
            return [
                'name'  => $item->name,
                'slug'  => $item->slug,
                'text_color' => $item->text_color
            ];
        });
    }

    /**
     * Footer
     */
    protected function configFooter()
    {
        if(config('cache.enable_cache') and (int)iEXSetting('enable_cache_footer'))
        {
            return Cache::remember('footer_'.$this->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->unCacheConfigFooter();
            });
        }

        return $this->unCacheConfigFooter();
    }

    /**
     * Footer
     *
     * @return array
     */
    protected function unCacheConfigFooter(): array
    {
        return LinksFooterGroup::with(['links_footer'])->orderBy('sorting', 'asc')->get()->map(function($item) {
            return [
                'name'  => $item->name,
                'links' => $item->links_footer->map(function($item2) {
                    return [
                        'name' => $item2->name,
                        'url' => $item2->url,
                        'is_blank' => $item2->is_blank
                    ];
                })
            ];
        })->toArray();
    }


    /**
     * Список уведомлений
     */
    protected function configNoticesAll(): array
    {
        if(config('cache.enable_cache') and (int)iEXSetting('enable_cache_notification')) {
            return $this->unCacheNotifcesAll();
        }

        return $this->unCacheNotifcesAll();
    }

    protected function unCacheNotifcesAll()
    {
        return NoticeExchange::where('status', '=',1)
            ->orderBy('sorting')->get()->filter(function($filter)
            {
                if($filter->is_enabled_schedule == 0) {
                    return $filter;
                }
                // Отображаем по расписанию
                if($filter->is_enabled_schedule == 1 and
                    Carbon::now()->format('H:i') > $filter->from_time and
                    Carbon::now()->format('H:i') < $filter->to_time) {
                    return $filter;
                }

            })->map(function($item) {
                return [
                    'text'          => $item->text,
                    'bg_color'      => $item->bg_color,
                    'text_color'    => $item->text_color,
                    'icon_notice'   => $item->icon_notice ? '/storage/notices/'.$item->icon_notice : '',
                    'text_size'     => $item->text_size,
                    'link'          => $item->link,
                    'is_blank'      => $item->is_blank
                ];
            })->toArray();
    }


    protected function advantages(): array
    {
        if(config('cache.enable_cache') and (int)iEXSetting('enable_cache_advantages'))
        {
            return Cache::remember('home_advantage_'.$this->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->unCacheAdvantage();
            });
        }

        return $this->unCacheAdvantage();
    }

    /**
     * Преимущество
     * @return array
     */
    protected function unCacheAdvantage(): array
    {
        return Advantage::where('status', '=',1)->orderBy('sorting')->get()
            ->map(function($item) {
                return [
                    'title'     =>  $item->title,
                    'content'   =>  $item->content,
                    'icon'      =>  $item->icon,
                    'link'      =>  $item->link,
                    'is_target'   =>  $item->is_target
                ];
            })->toArray();
    }


    protected function partners(): array
    {
        if(config('cache.enable_cache') and (int)iEXSetting('enable_cache_partners'))
        {
            return Cache::remember('home_partners_'.$this->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->unCachePartners();
            });
        }

        return $this->unCachePartners();
    }

    /**
     * Преимущество
     * @return array
     */
    protected function unCachePartners(): array {
        return Partner::orderBy('sorting')->get()->toArray();
    }

    /**
     * Настройки для отгрузки в Angular
    */
    protected function configSettings(): array
    {
        $array = [
            'is_banner_autoplay'                =>  (int)iEXSetting('is_banner_autoplay', 0),
            'number_banner_autoplay_timeout'     =>  (int)iEXSetting('number_banner_autoplay_timeout', 2),
            'is_banner_hidden_nav'              =>  (int)iEXSetting('is_banner_hidden_nav', 0),
            'is_dot_not_remember_data_order'    => (int)iEXSetting('is_dot_not_remember_data_order'),
            'is_cashback_disabled'          =>  (int)iEXSetting('is_cashback_disabled'),
            'is_view_logotype_partner'      =>  (int)iEXSetting('is_view_logotype_partner'),
            'is_working_view_header'        => (int)iEXSetting('is_working_view_header'),
            'working_online_text'           =>  iEXContentLanguage('working_online_text'),
            'working_offline_text'           =>  iEXContentLanguage('working_offline_text'),
            'working_site'                  =>  isJobOffline(),
            'locale'                        =>  app()->getLocale(),
            'is_disabled_group_filter_direction'    =>  (int)iEXSetting('is_disabled_group_filter_direction', 0),
            'is_disabled_writing_links_reviews' =>  (int)iEXSetting('is_disabled_writing_links_reviews', 0),
            'input_footer_title'            =>  iEXContentLanguage('input_footer_title'),
            'description_footer_text'       =>  iEXContentLanguage('description_footer_text'),
            'type_view_field_exchange_amount'   =>  (int)iEXSetting('type_view_field_exchange_amount'),
            'type_view_language_format'         => (int)iEXSetting('type_view_language_format'),
            'block_visible_reserve'         =>  (int)iEXSetting('block_visible_reserve'),
            'template_block_reserve'        =>  (int)iEXSetting('template_block_reserve'),
            'visible_last_exchange'         =>  iEXSetting('visible_last_exchange'),
            'visible_popular_exchange'      =>  (int)iEXSetting('visible_popular_exchange'),
            'visible_description'           =>  iEXSetting('visible_description'),
            'visible_main_header_title'     =>  iEXSetting('visible_main_header_title'),
            'visible_statistics'            =>  iEXSetting('visible_statistics'),
            'visible_my_order'              =>  iEXSetting('visible_my_order'),
            'hide_exchange_guest'           =>  iEXSetting('hide_exchange_guest'),
            'allow_filter_currency'         =>  iEXSetting('allow_filter_currency'),
            'allow_filter_currency_select'  =>  iEXSetting('allow_filter_currency_select', 0),
            'currency_display_type_dynamic' =>  iEXSetting('currency_display_type_dynamic', 0),
            'count_individual_num_currency' => (int)iEXSetting('count_individual_num_currency', 2),
            'is_mobile_hidden_reserve_out'  => (int)iEXSetting('is_mobile_hidden_reserve_out', 0),
            'is_web_hidden_reserve_out'     => (int)iEXSetting('is_web_hidden_reserve_out', 0),
            'visible_advantage'             =>  iEXSetting('visible_advantage'),
            'visible_banner'                =>  iEXSetting('visible_banner'),
            'visible_banner_mobile'         =>  iEXSetting('visible_banner_mobile'),
            'visible_partners'              =>  iEXSetting('visible_partners'),
            'visible_reviews'               =>  (int)iEXSetting('visible_reviews'),
            'visible_news'                  =>  (int)iEXSetting('visible_news'),
            'is_reserves_rounding'          =>  (int)iEXSetting('is_reserves_rounding'),
            'is_enabled_course_reserve'     => (int)iEXSetting('is_enabled_out_course_reserve'),
            'is_reserve_shortage_notice'    =>  (int)iEXSetting('is_reserve_shortage_notice'),
            'is_hide_currency_reserve'      =>  (int)iEXSetting('is_hide_currency_reserve'),
            'is_enabled_convert_exchange_to_usd'    => (int)iEXSetting('is_enabled_convert_exchange_to_usd', 0),
            'is_loading_filter_currency'    =>  (int)iEXSetting('is_loading_save_filter_currency'),
            'is_order_phone_number'         =>  (int)iEXSetting('is_order_phone_number'),
            'pos_order_phone_number'        =>  (int)iEXSetting('pos_order_phone_number'),
            'pos_order_email'               =>  (int)iEXSetting('pos_order_email'),
            'is_enabled_module_socket'      =>  (int)iEXSetting('is_enabled_module_socket'),
            'is_not_exceed_amount_reserve'  =>  (int)iEXSetting('is_not_exceed_amount_reserve'),
            'type_selected_currency'        =>  (int)iEXSetting('type_selected_currency', 0),
            'interface_exchange'            =>  iEXSetting('interface_exchange', 1),
            'is_style_agreement_rules'      => (int)iEXSetting('is_style_agreement_rules', 0),
            'is_exchange_rules_remember_checkbox'   => (int)iEXSetting('is_exchange_rules_remember_checkbox', 0),
            'is_style_agreement_checkbox'   =>  (int)iEXSetting('is_style_agreement_checkbox', 0),
            'interface_regauth_text1'      =>   iEXContentLanguage('interface_regauth_text1'),
            'interface_regauth_text2'      =>   iEXContentLanguage('interface_regauth_text2'),
            'chat_displayed_statuses'      =>    iEXSetting('chat_displayed_statuses') ?? [],
            'input_footer_select'           =>  (int)iEXSetting('input_footer_select'),
            'input_footer_image'            =>  (string)iEXSetting('input_footer_image'),
            'is_enabled_exchange_verify_account'    => (int)iEXSetting('is_enabled_exchange_verify_account', 0),
            'iex_interface_dynamic_colors'  => (int)iEXSetting('iex_interface_dynamic_colors', 0),

            'is_enable_footer'              =>  (int)iEXSetting('is_enable_footer', 0),
            'iex_interface_toolbar_row_menu' => (int)iEXSetting('iex_interface_toolbar_row_menu', 1),
            'max_time_task'                 =>  (int)iEXSetting('max_time_task', 100),
            'exchange_fon_type'             =>  (int)iEXSetting('exchange_fon_type', 0),
            'exchange_fon_type_rules'       => (string)iEXSetting('exchange_fon_type_rules'),
            'exchange_fon_file'             =>  '/images/'.(iEXSetting('view_layout_exchange_background') ? iEXSetting('view_layout_exchange_background') : 'iex-bg.jpg'),
            'exchange_fon_file_dark'             =>  '/images/'.(iEXSetting('view_layout_exchange_background_dark') ? iEXSetting('view_layout_exchange_background_dark') : iEXSetting('view_layout_exchange_background')),
            'type_view_background'          =>  iEXSetting('type_view_background', 0),
            'is_user_status_operator'          =>   iEXSetting('is_user_status_operator', 0),
            'is_gradient_text_color'          =>  (int)iEXSetting('is_gradient_text_color', 0),
            'break_title'                   =>  iEXContentLanguage('tech_breach_title'),
            'break_text'                    => iEXContentLanguage('tech_breach_text'),
            'working_offline_notify'        =>  iEXContentLanguage('working_offline_notify'),
            'disable_lang'                  =>  (int)iEXSetting('is_language_deactivation', 0),
            'iex_interface_text_entry'      =>  (int)iEXSetting('iex_interface_text_entry', 0),
            'iex_interface_header_style'    =>  (int)iEXSetting('iex_interface_header_style', 0),
            'is_enable_autogen_order_email' => (int)iEXSetting('is_enable_autogen_order_email', 0),
            'is_disabled_email_field_optional' => (int)iEXSetting('is_disabled_email_field_optional', 0),
            'exchange_rules_link'           =>  iEXSetting('exchange_rules_link', '/rules'),
            'exchange_kyc_link'           =>  iEXSetting('exchange_kyc_link', '/pages/kyc'),

            'is_view_contests_home'        => (int)iEXSetting('is_view_contests_home'),
            'is_view_contests_account'      =>  (int)iEXSetting('is_view_contests_account'),
            'currency_exchange_selected_default' => (int)iEXSetting('currency_exchange_selected_default'),
            'currency_exchange_selected_price'  => (int)iEXSetting('currency_exchange_selected_price'),

            'is_visible_direction_order_at' => (int)iEXSetting('is_visible_direction_order_at', 0),
            'jivosite_type_message' => (int)iEXSetting('jivosite_type_message'),
            'jivosite_text_message' => (string)iEXContentLanguage('jivosite_text_message'),
            'jivosite_info_user'    => (int)iEXSetting('jivosite_info_user'),


            'interval_rates_confirm_dialog' => (int)iEXSetting('interval_rates_confirm_dialog', 3) * 1000,
        ];

        $array['telegram_bot'] = [];
        if(iEXSetting('is_enabled_telegram_bot_block') == 1) {
            $array['telegram_bot']['url'] =  'tg://resolve?domain='.iEXSetting('link_telegram_bot_block');
            $array['telegram_bot']['telegram_block']['title'] = iEXContentLanguage('telegram_block_title');
            $array['telegram_bot']['telegram_block']['description'] = iEXContentLanguage('telegram_block_description');
            $array['telegram_bot']['telegram_block']['style'] = (int)iEXSetting('telegram_block_style');
            $array['telegram_bot']['telegram_block']['button'] = iEXContentLanguage('telegram_block_button');
        }


        if(\auth()->check())
        {
            if(config('cache.enable_cache') and config('cache.order_count') == 1) {
                $tasks = \Cache::remember('order_count-'.auth()->id(), now()->addMinutes(config('cache.clear_cache')), function() {
                    return Task::where([['id_user', '=', auth()->id()]])->whereIn('status', [3, 7, 8])->count();
                });
                $array['order_count'] = $tasks;
            } else {
                $tasks = Task::where([['id_user', '=', auth()->id()]])->whereIn('status', [3, 7, 8])->count();
                $array['order_count'] = $tasks;
            }
        }

        if(iEXSetting('visible_last_exchange')) {
            $array['last_items'] = $this->cacheLastExchange();
        }

        if(iEXSetting('visible_popular_exchange')) {
            $array['popular_items'] = $this->cachePopularExchange();
        }

        if(iEXSetting('visible_description'))
        {
            $array['welcome_title'] = iEXContentLanguage('welcome_title');
            $array['welcome_description'] = iEXContentLanguage('welcome_description');;
        }

        if(iEXSetting('visible_main_header_title'))
        {
            $array['main_title_header'] = iEXContentLanguage('main_title_header');
            $array['main_value_header'] = iEXContentLanguage('main_value_header');;
        }


        $array['lottery'] = ['num' => 0];
        if(Module::find('Contests')->isEnabled() == 1)
        {
            $item = ContestModel::where('status', '=', 1)->first();
            if(isset($item)) {
                $array['lottery'] = [
                    'title' => str_replace('[amount]', iex_number_format($item->bank, 2, true),$item->title),
                    'title_color' => $item->title_color,
                    'subtitle' => str_replace('[amount]', iex_number_format($item->bank, 2, true),$item->subtitle),
                    'subtitle_color' => $item->subtitle_color,
                    'button_name' => $item->button_name,
                    'num' => $item->bank,
                    'amount' => iex_number_format($item->bank, 0, true).' '.$item->code_sign,
                    'icon_url_home' => !is_null($item->icon_url_home) ? '/storage/contests/'.$item->icon_url_home : '',
                    'icon_url_account' => !is_null($item->icon_url_account) ? '/storage/contests/'.$item->icon_url_account : ''
                ];
            };
        }

        $array['promo_codes'] = 0;
        if(Module::find('PromoCodes')->isEnabled() == 1)
        {
            $item_promo = PromoCode::where('status', '=', 1)->count();
            if($item_promo > 0) {
                $array['promo_codes'] = 1;
            }
        }

        if(iEXSetting('visible_reviews')) {
            $array['reviews'] = $this->cacheReviews();
        }

        if(iEXSetting('visible_statistics')) {
            $array['statistics'] = $this->cacheStatistics();
        }

        if(iEXSetting('visible_news')) {
            $array['news'] = $this->cacheNews();
        }

        // Список преимуществ
        if(iEXSetting('visible_advantage')) {
            $array['advantages'] = $this->advantages();
        }

        if(iEXSetting('visible_banner')) {
            $array['banners'] = $this->configBanners();
        }

        // Список партнеров
        if(iEXSetting('visible_partners')) {
            $array['partners'] = $this->partners();
        }

        return $array;
    }

    /**
     * Получение избранных курсов
     *
     * @return array
     */
    protected function handleSelectedCourse(): array
    {
        // Если отключено то дальше не пускаем
        if(!iEXSetting('is_enabled_top_selected_courses'))
            return [];

        return SelectedCourse::select('id_crypto_parser', 'service_name', 'name', 'number_format')
            ->where('status', '=', 1)->orderBy('sorting')->get()->map(function($item) {
                $item['summa'] = number_format(
                    (isset($item->parser_exchange) ? $item->parser_exchange->summa_default : 0)
                    , $item->number_format, '.', ' ');

                return $item;
            })->toArray();
    }

    /**
     * Получаем статистику
     */
    protected function cacheStatistics()
    {
        if(config('cache.enable_cache') and iEXSetting('enable_cache_statistics')) {
            return Cache::remember('statistics-exchange_'.app()->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->queryStatistics();
            });
        }

        return $this->queryStatistics();
    }

    /**
     * Получение новостей
     */
    protected function cacheNews()
    {
        if(config('cache.enable_cache') and config('cache.enable_cache_news')) {
            return Cache::remember('news-exchange_'.app()->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->queryNews();
            });
        }

        return $this->queryNews();
    }


    /**
     * Type: Caching
     * Name: Получать данные о последних обменах
     *
     * @return array
     */
    protected function cacheLastExchange(): array
    {
        if(config('cache.enable_cache') and config('cache.enable_cache_last_exchange'))
        {
            return Cache::remember('exchange_last_'.$this->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->queryLastExchange();
            });
        }

        return $this->queryLastExchange();
    }

    /**
     * Type: Caching
     * Name: Получать данные о популярных направлений
     *
     * @return array
     */
    protected function cachePopularExchange(): array
    {
        if(config('cache.enable_cache')) {
            return Cache::remember('exchange_popular_'.$this->getLocale(), now()->addHours(), function () {
                return $this->queryPopularExchange();
            });
        }

        return $this->queryPopularExchange();
    }


    /**
     * Получить последние отзывы
     */
    protected function cacheReviews()
    {
        if(config('cache.enable_cache') and iEXSetting('enable_cache_reviews_exchange')) {
            return Cache::remember('reviews-exchange_'.app()->getLocale(), now()->addMinutes(config('cache.clear_cache')), function () {
                return $this->queryReviews();
            });
        }

        return $this->queryReviews();
    }

    /**
     * Последние новости
     */
    protected function queryNews(): array
    {
        return News::orderByDesc('id')
            ->limit((int)iEXSetting('count_news_exchange', 10))
            ->get()->map(function($item)
            {
                $news_image = '/storage/news/'.$item->image;
                if(!empty(iEXSetting('storage_disk_news')) and $item->is_local_image == 1) {
                    $news_image = iex_dynamic_read_view_image('/news/'.$item->image, false);
                }

                return [
                    'slug'          =>  $item->id.'-'.$item->parent_url,
                    'name'          =>  $item->name,
                    'created_at'    =>  Carbon::parse($item->created_at)->translatedFormat('d M Y H:i'),
                    'image'         =>  $news_image,
                    'views'         =>  $item->views
                ];
            })->toArray();
    }

    /**
     * Получение не-закэшированных данных по статистике
     */
    protected function queryStatistics()
    {
        return InfoStatistic::where('status', '=', 1)->orderBy('sorting')->get()->map(function($item) {

            $response['name'] = $item->name;
            $response['value'] = $item->value;
            $response['link'] = $item->link;

            if($item->is_automatic) {
                // Пользователей за сутки
                if($item->type == 'users_today') {
                    $response['value'] = iex_number_format(
                        User::whereDate('created_at', '=', Carbon::today())->count(), 0,true
                    );
                }

                // Всего пользователей
                if($item->type == 'users') {
                    $response['value'] = iex_number_format(User::count(), 0,true);
                }

                // Обменов за сутки
                if($item->type == 'orders_today') {
                    $response['value'] = iex_number_format(
                        Task::where('status', '=', 4)
                            ->whereDate('created_at', '=', Carbon::today())->count(),0, true
                    );
                }

                // Всего обменов
                if($item->type == 'orders') {
                    $response['value'] = iex_number_format(
                        Task::where('status', '=', 4)->count(), 0, true
                    );
                }

                // Среднее время выполнения заявки
                if($item->type == 'average_order') {
                    $response['value'] = common_order_average();
                }

                // TS PerfectMoney
                if($item->type == 'perfect_money') {

                    $ts_count = 0;
                    try {
                        $ts_count = Http::get('https://perfectmoney.is/acct/ts.asp', ['account' => (string)$item->account_number])->json();
                    }catch (\Exception $exception) {
                        //
                    }

                    $response['value'] = iex_number_format($ts_count, 2,true);
                }

                // Счетчик отзывов из BestChange
                if($item->type == 'bestchange_comment') {
                    $response['value'] = iex_number_format(
                        bestchange_exchanger_count($item->link, $item->id,1), 0, true
                    );
                }

                // Оборот в сутки в рублях
                if($item->type == 'daily_turnover_rub') {
                    $daily = TaskConvertLog::selectRaw('created_at')
                        ->orderByDesc('id')
                        ->whereDate('created_at', '=', Carbon::today())->sum('to_rub');

                    $response['value'] = formatToHuman($daily);
                }

                // Оборот в сутки в $
                if($item->type == 'daily_turnover_usd') {
                    $daily = TaskConvertLog::selectRaw('created_at')
                        ->orderByDesc('id')
                        ->whereDate('created_at', '=', Carbon::today())->sum('to_usd');

                    $response['value'] = '$'.formatToHuman($daily);
                }

                // Резервы в usd
                if($item->type == 'reserve_usd') {
                    $reserve = Reserve::with(['currency' => function($q) {
                        $q->select('id', 'id_code_currency');
                    }, 'currency.code_currency' => function($q) {
                        $q->select('id', 'name');
                    }])->select('id', 'status', 'id_currency', 'summa')->where('status', '=', 0)->get()->map(function($item) {
                        if(!isset($item->currency->code_currency))
                            return null;
                        return (float)convert_to_usd($item->currency->code_currency->name, $item['summa']);
                    })->sum();

                    $response['value'] = '$'.formatToHuman($reserve);
                }

                // Резервы в рублях
                if($item->type == 'reserve_rub')
                {
                    $reserve = Reserve::with(['currency' => function($q) {
                        $q->select('id', 'id_code_currency');
                    }, 'currency.code_currency' => function($q) {
                        $q->select('id', 'name');
                    }])->select('id', 'status', 'id_currency', 'summa')->where('status', '=', 0)
                        ->get()->map(function($item) {
                        if(!isset($item->currency->code_currency))
                            return null;
                        return (float)convert_to_rub($item->currency->code_currency->name, $item['summa']);
                    })->sum();

                    $response['value'] = formatToHuman($reserve);
                }
            }

            return $response;
        });
    }


    /**
     * Запрос на получение последних отзывов
     */
    protected function queryReviews(): ReviewResponse
    {
        $items = Review::with(['tasks' => function($q) {
            $q->select('id', 'public_id', 'id_direction_exchange', 'give_price', 'receiving_price');
        }, 'tasks.direction_exchange' => function($q) {
            $q->select('id', 'id_currency1', 'id_currency2');
        }, 'tasks.direction_exchange.currency1' => function($q) {
            $q->select('id', 'id_payment', 'id_code_currency');
        }, 'tasks.direction_exchange.currency2' => function($q) {
            $q->select('id', 'id_payment', 'id_code_currency');
        }, 'tasks.direction_exchange.currency1.payment' => function($q) {
            $q->select('id', 'name', 'logo');
        }, 'tasks.direction_exchange.currency2.payment' => function($q) {
            $q->select('id', 'name', 'logo');
        }, 'tasks.direction_exchange.currency1.code_currency' => function($q) {
            $q->select('id', 'name');
        }, 'tasks.direction_exchange.currency2.code_currency' => function($q) {
            $q->select('id', 'name');
        }])->where('version', '=', 1)->where('status', '=', 1)->orderByDesc('id')->limit(
            (int)iEXSetting('count_reviews_exchange', 20)
        );

        return new ReviewResponse($items->get());
    }

    /**
     * Запрос на получение последних обменов
     *
     * @return array
     */
    protected function queryLastExchange(): array
    {
        //last items
        $lastItems = Task::with(['direction_exchange' => function($q) {
            $q->select('id', 'id_currency1', 'id_currency2');
        },'direction_exchange.currency1' => function($q) {
            $q->select('id', 'number_format', 'id_payment', 'id_code_currency');
        },'direction_exchange.currency1.payment' => function($q) {
            $q->select('id', 'name', 'enable_svg', 'logo');
        },'direction_exchange.currency1.code_currency' => function($q) {
            $q->select('id', 'name', 'sign');
        },'direction_exchange.currency2' => function($q) {
            $q->select('id', 'number_format', 'id_payment', 'id_code_currency');
        },'direction_exchange.currency2.payment' => function($q) {
            $q->select('id', 'name', 'enable_svg', 'logo');
        },'direction_exchange.currency2.code_currency' => function($q) {
            $q->select('id', 'name', 'sign');
        }])->select('id', 'id_direction_exchange', 'status', 'started_at', 'give_price', 'receiving_price', 'updated_at', 'created_at')->where('status', '=', 4)->orderByDesc('id')
            ->limit(iEXSetting('count_last_exchange'))->get();

        $array = [];
        foreach ($lastItems as $lastItem)
        {
            $in_price = (float)iex_number_format($lastItem->give_price, $lastItem->direction_exchange->currency1->number_format);
            $out_price = (float)iex_number_format($lastItem->receiving_price, $lastItem->direction_exchange->currency2->number_format);

            $start = Carbon::parse($lastItem->started_at);
            $lead_time = Carbon::parse($lastItem->updated_at);

            $getSecond = $start->diffForHumans($lead_time, true, true);

            $array[] = [
                'image_in' => '/storage/payment_systems/'.$lastItem->direction_exchange->currency1->payment->logo,
                'image_out' => '/storage/payment_systems/'.$lastItem->direction_exchange->currency2->payment->logo,
                'in_code' => $lastItem->direction_exchange->currency1->code_currency->sign,
                'out_code' => $lastItem->direction_exchange->currency2->code_currency->sign,
                'in_price' => $in_price,
                'out_price' => $out_price,
                'updated_at' => Carbon::parse($lastItem->created_at)->diffForHumans(),
                'finished_at' => $getSecond
            ];
        }

        return $array;
    }

    /**
     * Запрос на получение последних обменов
     *
     * @return array
     */
    protected function queryPopularExchange(): array
    {
        //last items
        $lastItems = DirectionExchange::leftJoin('tasks', 'id_direction_exchange', '=', 'direction_exchange.id')
            ->with([
                'currency1' => function($q) {
                    $q->select('id', 'designation_xml', 'id_payment', 'id_code_currency');
                }, 'currency2' => function($q) {
                    $q->select('id', 'designation_xml', 'id_payment', 'id_code_currency');
                }, 'currency1.payment' => function($q) {
                    $q->select('id', 'name', 'logo');
                }, 'currency2.payment' => function($q) {
                    $q->select('id', 'name', 'logo');
                }, 'currency1.code_currency' => function($q) {
                    $q->select('id', 'name');
                }, 'currency2.code_currency' => function($q) {
                    $q->select('id', 'name');
                }])
            ->select('direction_exchange.id','direction_exchange.id_currency1','direction_exchange.id_currency2','tasks.status', DB::raw("count('tasks.id') as orders_count"))
            ->where('tasks.status', '=', 4)
            ->where('direction_exchange.status', '=', 1)
            ->groupBy('direction_exchange.id')
            ->orderBy('orders_count', 'desc')
            ->limit((int)iEXSetting('count_popular_exchange', 10))
            ->get()->toArray();

        $array = [];
        foreach ($lastItems as $lastItem)
        {
            $array[] = [
                'image_in' => '/storage/payment_systems/'.$lastItem['currency1']['payment']['logo'],
                'image_out' => '/storage/payment_systems/'.$lastItem['currency2']['payment']['logo'],
                'in_code' => $lastItem['currency1']['code_currency']['name'],
                'out_code' => $lastItem['currency2']['code_currency']['name'],
                'name_in' => $lastItem['currency1']['payment']['name'].' '.$lastItem['currency1']['code_currency']['name'],
                'name_out' => $lastItem['currency2']['payment']['name'].' '.$lastItem['currency2']['code_currency']['name'],
                'code_in' => $lastItem['currency1']['designation_xml'],
                'code_out' => $lastItem['currency2']['designation_xml'],
            ];
        }

        return $array;
    }
}
