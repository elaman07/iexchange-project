<?php
namespace App\Common\Packages\Angular;

use App\Http\Resources\Angular\UserWalletStoryResource;
use App\Http\Resources\Sessions\CurrentUserResource;
use App\Models\CodeCurrency;
use App\Models\Contact;
use App\Models\RewardProgram;
use App\Models\SocialAuthSystem;
use App\Models\UserWalletStories;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Angular
{
    use Manages\ManagesConfig;


    public function getLocale(): string
    {
        return app()->getLocale();
    }

    /**
     * Собираем конфигурационные настройки
     *
     * @return array
     */
    public function buildConfig(): array
    {
        $logotype_web = (string)iEXSetting('logotype_web');
        if(empty($logotype_web)) {
            $logotype_web = url('/images/logo.png?'.(string)iEXSetting('last_update_logo'), [], true);
        } else {
            $logotype_web = url('/images/'.$logotype_web, [], true);
        }

        // Для темных стилей
        $logotype_dark_name = (string)iEXSetting('logotype_dark');
        if(!empty($logotype_dark_name)) {
            $logotype_dark = url('/images/'.(string)iEXSetting('logotype_dark'), [], true);
        } else {
            $logotype_dark = $logotype_web;
        }

        $logotype_mobile = (string)iEXSetting('logotype_mobile');
        if(empty($logotype_mobile)) {
            $logotype_mobile = url('/images/logo_mobile.png?'.(string)iEXSetting('last_update_logo'), [], true);
        } else {
            $logotype_mobile = url('/images/'.$logotype_mobile, [], true);
        }


        // Настройка уведомлений
        $all_notices = $this->configNoticesAll();



        $angular = [
            'base_uri'          =>  config('app.url'),
            'phone_mask'        =>  (string)iEXSetting('angular_phone_mask', ''),
            'title'             =>  sprintf('%s - %s', iEXContentLanguage('sitename'), iEXContentLanguage('sitename_desc')),
            'sitename'          =>  iEXContentLanguage('sitename'),
            'interval_rates'    =>  (float)iEXSetting('interval_rates', 50) * 1000,
            'wss_key'           =>  config('broadcasting.connections.pusher.key'),
            'wss_cluster'       =>  config('broadcasting.connections.pusher.cluster'),
            'recaptcha_key'     =>  config('captcha.sitekey'),
            'rates_cache'       =>  iEXSetting('type_cached_rates', 0),

                          'languages' => config('app.all_locale'),
                           'settings'          =>  $this->configSettings(),
                           'settings_themes' => [
                               'description'       =>  sprintf('%s - %s', iEXContentLanguage('sitename'), iEXContentLanguage('sitename_desc')),
                               'logotype'          =>  $logotype_web,
                               'logotype_dark'     =>  $logotype_dark,
                               'logo_alt'          => iEXContentLanguage('sitename'),
                               'logotype_text'     => iEXSetting('logotype_text'),
                               'logotype_type'     => iEXSetting('logotype_type'),

                               'logotype_mobile'          =>  $logotype_mobile,
                               'logotype_text_mobile'     => iEXSetting('logotype_text_mobile'),
                               'logotype_type_mobile'     => iEXSetting('logotype_type_mobile')
                           ],
                           'working_site' => isJobOffline(),
                           'contact'   =>  [
                               'lists' => $this->contacts()
                           ],

                           'cashbacks' => $this->cashbackConfig(),
                           'notices' =>  $all_notices,
                           'menu' => $this->configMenu(),
                           'footers' => $this->configFooter(),
                           'social_auth' => $this->socialAuth(),

            'is_enable_archive' => (int)iEXSetting('orderarchive_enable_upload_history', 0)
        ];


        if(Auth::check()) {

            $user_wallets = Cache::remember('user_wallet_stories_'.auth()->id(), Carbon::now()->addHour(), function() {
                return UserWalletStories::where([
                    'id_user' => auth()->id()
                ])->get();
            });

            $angular['user_wallets'] = new UserWalletStoryResource($user_wallets);

            $user = Auth::user();
            $minimum_bonus_payout = iEXSetting('minimum_bonus_payout');

            $allowOutput = $user->user_balance->balance > $minimum_bonus_payout || $user->user_balance->balance_reward > $minimum_bonus_payout;
            $user = Auth::user();
            $angular['user'] = new CurrentUserResource($user);

            $angular['payout'] = [
                'payout_output'                 =>  $allowOutput,
                'minimum_bonus_payout'          =>  $minimum_bonus_payout
            ];
        } else {

            $angular['user_wallets'] = [
                'data' => []
            ];
            $angular['user'] = [
                'id' => 0,
                'type' => 'user',
                'attributes' => [
                    'is_auth'   => false,
                ]
            ];
        }


        if((bool)config('payment.enable_internal_account'))
        {
            $codes = CodeCurrency::all();
            $array_codes = [];
            foreach ($codes as $code)
            {
                $array_codes[] = [
                    'balance' => isset($code->internal_account) ? $code->internal_account->balance : 0,
                    'name' => $code->name
                ];
            }
            $angular['internal_account'] = $array_codes;
        }

        $angular['payout']['currency_payout_sign'] = config('crypto.currency_payout_sign');
        $angular['payout']['currency_payout_sign_alt'] = config('crypto.currency_payout_sign_alt');

        if(iEXSetting('is_enabled_top_selected_courses')) {
            $angular['selected_course'] = $this->handleSelectedCourse();
        }

        return $angular;
    }
}
