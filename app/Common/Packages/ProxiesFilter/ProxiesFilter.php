<?php
namespace App\Common\Packages\ProxiesFilter;

use GuzzleHttp\Client;
use UnexpectedValueException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Str;

class ProxiesFilter
{
    public const IP_VERSION_4 = 1 << 0;

    public const IP_VERSION_6 = 1 << 1;

    public const IP_VERSION_ANY = self::IP_VERSION_4 | self::IP_VERSION_6;


    /**
     * Retrieve Cloudflare proxies list.
     *
     * @param  int  $type
     *
     * @return array
     */
    public function load($type = self::IP_VERSION_ANY) : array
    {
        $proxies = [];

        $sources = iEXSetting('proxiesfilter_sources_ddos', null);

        if(empty($sources))
            return [];

        $method = 'import'.Str::studly($sources);
        if(method_exists($this,$method)) {
            return $this->{$method}($type);
        } else {
            $proxies = config('ddos.proxies.'.\Str::lower($sources)) ?? [];
        }

        return $proxies;
    }

    /**
     * Данные из Cloudflare
     *
     * @param $type
     * @return array
     * @throws GuzzleException
     */
    private function importCloudflare($type)
    {
        $proxies = [];
        if ((bool) ($type & self::IP_VERSION_4)) {
            $proxies = $this->retrieve('ips-v4');
        }

        if ((bool) ($type & self::IP_VERSION_6)) {
            $proxies = array_merge($proxies, $this->retrieve('ips-v6'));
        }

        return $proxies;
    }


    /**
     * Retrieve requested proxy list by name.
     *
     * @param  string $name requet name
     *
     * @return array
     * @throws GuzzleException
     */
    protected function retrieve($name) : array
    {
        try {
            $client = new Client(['base_uri' => 'https://www.cloudflare.com/']);
            $response = $client->request('GET', $name);
        } catch (\Exception $e) {
            throw new UnexpectedValueException('Failed to load trust proxies from Cloudflare server.', 1, $e);
        }

        if ($response->getStatusCode() != 200) {
            throw new UnexpectedValueException('Failed to load trust proxies from Cloudflare server.');
        }

        return array_filter(explode("\n", (string) $response->getBody()));
    }
}
