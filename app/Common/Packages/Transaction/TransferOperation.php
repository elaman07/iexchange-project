<?php
namespace App\Common\Packages\Transaction;


class TransferOperation
{
    /**
     * Экземпляр "Transaction"
     *
     * @var Transaction
    */
    protected $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }
}