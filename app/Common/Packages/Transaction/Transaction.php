<?php
namespace App\Common\Packages\Transaction;


use App\Models\Task;

class Transaction
{
    use Concerns\ManagersReserves,
        Concerns\HistoryIncome,
        Concerns\ManagersBalance,
        Concerns\SupportsAutoPayments,
        Concerns\SupportsCheckPayment,
        Concerns\ProcessCheckPayment,
        Concerns\UpdateCustomFields,
        Concerns\IPNCheckPayment,
        Bindings\ManagersEmployees,
        Bindings\ManagersNotification,
        Bindings\ManagersSuccess,
        Bindings\ManagersFailed,
        Bindings\ManagersDefer,
        Bindings\ManagersDetails,
        Bindings\ManagersBootstrap,
        Bindings\ManagersPayment,
        Bindings\ManagersBonus,
        Bindings\ManagersClient,
        Bindings\ManagersEvent,
        Bindings\ManagersRecount;

    /**
     * Детали транзакции
     *
     * @var ?Task
    */
    protected ?Task $transaction = null;

    /**
     * ID Транзакции
     *
     * @return integer
    */
    protected int $id;

    /**
     * Статус вычитывании комиссии из автоплатежа
     *
     * @var boolean
    */
    protected bool $isConsoleCommission = false;

    /**
     * Запуск, использя крон
     *
     * @var boolean
    */
    protected bool $isCron = false;

    /**
     * Дополнительные параметры транзакции
    */
    protected array $parameters = [
        'preview'           =>  null,
        'commission'        =>  0,
        'type'              =>  'default',
        'notPartner'        =>  false,
        'notCashback'       =>  false,
        'idTransaction'     => null,
        'textMessageOrder'  =>  null,
        'category_reject'   => 0,
        'pin_code'          =>   null
    ];

    /**
     * Комиссия из RPC
     *
     * @var float
    */
    protected $rpc_commission = 0.00;

    /**
     * Создаем новый экземпляр транзакции.
    */
    public function __construct()
    {
        //
    }

    /**
     * Получение ID транзакции
     *
     * @param $id
     * @param array $config
     * @return Transaction
     * @throws \Exception
     */
    public function find($id, array $config = []) : Transaction
    {
        // Получение удаленной заявки
        if($this->isTrashed()) {
            $this->transaction = Task::onlyTrashed()->find($id);
            $this->parameters = $config;

            return $this;
        }elseif($this->hasFind($id))
        {
            $this->transaction = Task::find($id);
            $this->parameters = $config;

            if(!$this->hasPreview()) {
                $this->bootstrap();
            }
            return $this;
        }else {
            throw new \Exception('Транзакции не существует');
        }
    }

    /**
     * Указать ID транзакции
     *
     * @param int $id
     * @return Transaction
     */
    public function setId(int $id): Transaction
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Вызвать событие для получения всех необходимых параметров
     *
     * @param $id
     * @return Transaction
     */
    public function call($id): Transaction
    {
        $this->transaction = Task::find($id);
        return $this;
    }

    /**
     * Детали транзакции
     *
     * @param Task $task
     * @return Transaction
     * @throws \Exception
     */
    public function init(Task $task): Transaction
    {
        if(!isset($task)) {
            throw new \Exception('Транзакция не найдена');
        }

        $this->transaction = $task;
        return $this;
    }

    /**
     * Очистка данных
     *
     * @return Transaction
     */
    public function clear_init(): Transaction
    {
        $this->transaction = null;
        return $this;
    }

    /**
     * Тип транзакции
     *
     * @return string
    */
    public function getType()
    {
        return $this->parameters['type'];
    }

    /**
     * Получаем сумму комиссии
     *
     * @return float
    */
    public function getCommission()
    {
        return $this->getTransferCommission();
    }

    /**
     * Причина отклонения заявки
    */
    public function getCategoryReject()
    {
        return ($this->parameters['rejection_status'] ?? 1);
    }

    /**
     * Причины отклоения заявков
     *
     * @param $value
     * @return Transaction
     */
    public function setCategoryReject($value)
    {
        if(!isset($this->parameters['rejection_status'])) {
            $this->parameters['rejection_status'] = $value;
        }

        return $this;
    }

    /**
     * Комиссия из RPC
     *
     * @param $value
     */
    public function setRPCCommission($value = 0) {
        $this->rpc_commission = $value;
    }

    /**
     * Комиссия из RPC
     */
    public function getRPCCommission() {
        return abs($this->rpc_commission);
    }

    /**
     * Проверить актуальность транзакции
     *
     * @return bool
    */
    public function hasNotActive()
    {
        return $this->getStatus() != 3 and $this->getStatus() != 7;
    }

    /**
     * Получить действией которая вызвана
    */
    public function hasAction() {
        return ($this->parameters['actions'] ?? null);
    }

    /**
     * Удаленная заявка
     *
     * @return boolean
    */
    public function isTrashed() {
        return request()->has('trashed') == true;
    }

    /**
     * Дает полное права обработать транзакцию по выбранным действиям
     * @throws \Exception|\Throwable
     */
    public function handlerAction()
    {
        if($this->hasAction() == 'skip_reserve') {
            $this->skipReserve();

        }elseif($this->hasAction() == 'run_process') {
            $this->start();

        } elseif($this->hasAction() == 'payment') {
            $this->checkAndEnroll();

        } elseif($this->hasAction() == 'logout') {
            $this->logout();

        } elseif($this->hasAction() == 'check') {
            $this->sendCheck($this->getMessage());

        } elseif($this->hasAction() == 'notification') {
            $this->sendNotification($this->getMessage(), $this->getTypeMessage());

        } elseif($this->hasAction() == 'success') {
            $this->success();

        } elseif($this->hasAction() == 'defer') {
          $this->defer();

        } elseif ($this->hasAction() == 'reject') {
            $this->failed();

        } elseif($this->hasAction() == 'recount') {
            $this->recount();
        } elseif($this->hasAction() == 'update_fields_in') {
            $this->updateInCustomField();
        }elseif($this->hasAction() == 'update_fields_out') {
            $this->updateOutCustomField();
        } elseif($this->hasAction() == 'send_to_shot_telegram') {
            $this->sentToShotTelegram();
        } elseif($this->hasAction() == 'switch_to_manual_mode') {
            $this->disableIsBot();
        } elseif($this->hasAction() == 'edit_data') {
            $this->updateEditData();
        } elseif($this->hasAction() == 'add_requisites_payment') {

            exit;
            $this->updateAddRequisitesPayment();
        }
    }

    /**
     * Правила обновления страницы
     *
     * @return bool
    */
    public function rulesRedirect()
    {
        return array_key_exists($this->parameters['actions'], [
            'run_process',
            'payment',
            'check',
        ]);
    }

    /**
     *
    */
    public function hasPreview()
    {
        return $this->parameters['preview'];
    }

    /**
     * Получение текст сообщения которая будет привязываться к опеределенным действиям
     *
     * @return string
    */
    public function getMessage()
    {
        return $this->parameters['message'];
    }

    /**
     * Тип отправки письма
     *
     * @return integer
    */
    public function getTypeMessage()
    {
        return $this->parameters['type_message'];
    }

    /**
     * Не выплачитьва cashback
     *
     * @return boolean
     */
    public function getSwitchNotCashback()
    {
        return $this->parameters['notCashback'];
    }

    /**
     * Не выплачить партнерские бонусы
     *
     * @return boolean
     */
    public function getSwitchNotPartner()
    {
        return $this->parameters['notPartner'];
    }

    /**
     * Получение ID Транзакции для прикрпления к заявке
     *
     * @return boolean
     */
    public function getBlockchainTransaction()
    {
        return $this->parameters['idTransaction'];
    }


    /**
     * Получение доп. комментария
     *
     * @return boolean
     */
    public function getTextMessageOrder()
    {
        return $this->parameters['textMessageOrder'];
    }

    /**
     * Получение код безопасности
     *
     * @return boolean
     */
    public function getSecurityOrderCodeConfirm()
    {
        return $this->parameters['pin_code'];
    }

    /**
     * Получить детали транзакции
     *
     */
    public function getItem(): ?Task
    {
        return $this->transaction;
    }

    /**
     * Получение по IP Геолокационных данных
     *
     * @return \App\Common\Packages\GeoIP\GeoIP|\App\Common\Packages\GeoIP\Location
    */
    public function getGeoIp() {
        return geoip($this->getItem()->ip);
    }

    /**
     * Проверяем, существует ли транзакция
     *
     * @param $id
     * @return bool
     */
    protected function hasFind($id) : bool {
        return Task::where('id', $id)->exists();
    }

    /**
     * Установка статус крон
     *
     * @param $value
     * @return Transaction
     */
    public function setIsCron($value = false)
    {
        $this->isCron = $value;
        return $this;
    }

    /**
     * Получение статуса крон
     *
     * @return bool
     */
    public function getIsCron() {
        return $this->isCron;
    }


    /**
     * Детали заявки
     *
     * @return Task
    */
    public function getTransaction(): Task {
        return $this->transaction;
    }

    public function isToArchive() {
        return in_array($this->getStatus(), [4, 10, 5, 9, 11]);
    }

    public function isToArchiveNotPreview() {
        return !in_array($this->getStatus(), [3, 7]);
    }
}
