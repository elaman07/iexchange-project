<?php

namespace App\Common\Packages\Transaction\Concerns;


use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * Автоматическая проверка оплаты
 */
trait SupportsCheckPayment
{
    /**
     * Дополнительно проверяем полученную сумму
     *
     * @var boolean
     */
    protected bool $accurate_balance = false;

    /**
     * Проверить поступление средств
     *
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function checkInPayment(array $params = []): array
    {
        $merchantPay = $this->getMerchant();
        // Проверяем, включен ли метод проверки оплаты
        if (!isset($merchantPay)) {
            throw new Exception('Не включена функция проверки оплаты');
        }

        // Название провайдера
        $provider = Str::studly($merchantPay->gateway->alias);
        $method = $provider;
        $redirect = (isset($params['api_panel']) and $params['api_panel'] == 'v1') ? '/applications/details/' : '/tx/';
        $class = "\\App\\Common\\Packages\\Transaction\\Payments\\CheckPayments\\{$method}Check";

        // Проверяем, существует ли класс выплаты
        if (class_exists($class))
        {
            $response = new $class($this);
            return array_merge($response->handle(),
                ['redirect' => admin_base_path($redirect . $this->transaction->id)]
            );
        }
    }

    /**
     * Включение или отключение дополнительной проверки полученной суммы
     *
     * @param bool $bool
     */
    public function setCheckAccurateBalance(bool $bool = false)
    {
        $this->accurate_balance = $bool;
    }

    /**
     * Включение или отключение дополнительной проверки полученной суммы
     *
     * @return bool
     */
    public function getCheckAccurateBalance(): bool
    {
        return $this->accurate_balance;
    }
}
