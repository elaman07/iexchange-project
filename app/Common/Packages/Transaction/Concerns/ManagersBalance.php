<?php
namespace App\Common\Packages\Transaction\Concerns;


use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait ManagersBalance
{
    private float $api_balance = 0;


    /**
     * Проверяем баланс перед выплатой
     *
     * @return int
     */
    public function hasBalance()
    {

        $pay = $this->getPay();
        if(isset($pay)) {

            if($pay->hide_check_balance == 1) {
                return 0;
            } else {
                try {
                    $this->api_balance = $this->getBalanceByCurrency();
                    return ($this->getBalanceByCurrency() < $this->getAmountOut());
                }catch (\Exception $exception) {
                    return 1;
                }
            }
        }

        return 1;
    }

    /**
     * Получение баланса
     */
    public function getAPIBalance() {
        return $this->api_balance;
    }

    /**
     * Получить баланс по валюте
     * @param null $type
     * @return mixed
     */
    public function getBalanceByCurrency($type = null)
    {
        $pay = $this->getPay();
        if(!is_null($pay) or !is_null($type))
        {
            $studly = (!is_null($type) ? $type : $pay->gateway->alias);
            $method = 'balance'.Str::studly($studly);

            $configProvider = PaymentFacade::configProvider($studly);
            // Проверка баланса
            if(isset($configProvider) and isset($configProvider['balance']) and $configProvider['balance'])
            {
                $api = PaymentFacade::pay($studly, $pay->id)
                    ->api();

                if(method_exists($api,'setMethodPay')) {
                    $api = $api->setMethodPay($this->getPay()->method_pay);
                }


                $getBalanceAPI = $this->getCodeOut()->name;
                if($studly != 'whitebitcrypto')
                {
                    // Получаем название сети
                    $network_code = $this->getCurrencyOut()->network_code;
                    // Для Network Codes
                    if(!empty($network_code)) {
                        $getBalanceAPI = $network_code;
                    }
                }


                return (float)$api->getBalance($getBalanceAPI);
            }

            if(method_exists($this, $method)) {
                return $this->$method($studly, $pay->id);
            }
        }
    }

    /**
     * Получение баланса для Obmenka UA (QIWI)
     *
     * @return string
     */
    public function balanceObmenka(string $provider, string $id)
    {
        $code_currency = str_replace('RUB', 'RUR', Str::upper($this->getCodeOut()->name));

        // Тип транзакции
        switch ($this->getPay()->method_pay)
        {
            // ADVCash USD
            case 1:
                $type = 'ADVCASH.USD';
                break;
            // Visa/Mastercard (RUB, UAH, KZT)
            case 2:
                $type = 'VISAMASTER.'.$code_currency;
                break;
            // BITCOIN
            case 3:
                $type = 'BITCOIN';
                break;
            case 4:
                $type = 'BITCOIN_CASH';
                break;
            case 5:
                $type = 'LITECOIN';
                break;
            case 6:
                $type = 'ETHEREUM';
                break;
            case 7:
                $type = 'TETHER';
                break;
            case 8:
                $type = 'USDT_ERC20';
                break;

            // Стандартная выплата
            default:
                $type = 'QIWI';
        }

        return PaymentFacade::pay($provider, $id)->api()
            ->getBalance($type);
    }

    /**
     * Получение баланса для внутреннего счета
     *
     * @return string
     */
    public function balanceInternalaccount()
    {

        return $this->getAmountOut() + 100;
    }
}
