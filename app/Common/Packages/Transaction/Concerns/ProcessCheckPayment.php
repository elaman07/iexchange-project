<?php
namespace App\Common\Packages\Transaction\Concerns;

use Illuminate\Support\Carbon;

trait ProcessCheckPayment
{
    /**
     * Получаем время автовыплаты
     *
     * @return string
     */
    public function hasTimeOutCheckPayment(): string
    {
        $max_register_limit = (int)$this->getMerchant()->max_register_blockchain + 10;
        return Carbon::parse($this->transaction->created_at)->addMinutes($max_register_limit) < Carbon::now();
    }
}
