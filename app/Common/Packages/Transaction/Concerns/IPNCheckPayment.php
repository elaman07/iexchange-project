<?php
namespace App\Common\Packages\Transaction\Concerns;


use App\Common\Packages\Payment\Exception\InvalidRequestException;
use App\Models\WalletTransaction;
use Illuminate\Support\Collection;

trait IPNCheckPayment
{
    protected $ipnParameter = [];

    /**
     * Проверка IPN для поступления средства
     *
     * @param array $options
     * @return array
     * @throws InvalidRequestException
     */
    public function IPNWestWallet($options = [])
    {
        /// Валидация входных параметров
        $this->validateIPN(
            collect($options),
            'id', 'amount', 'address', 'status', 'currency'
        );

        $array = [];
        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($options['status'] == 'pending')
        {
            if($this->transaction->register_tx == 0) {
                $this->addEventRegisterTransaction(
                    $options['blockchain_hash'],
                    $options['blockchain_confirmations'],
                    $options['amount'],
                    'westwallet'
                );
                $this->transaction->update(['register_tx' => 1]);
            }

            $array['status'] = 1;
            $array['message'] = 'Транзакция найдена в Блокчейне, Подтвердждений: 0';
        }

        // В случае если все удавно
        if($options['status'] == 'completed')
        {
            // Получение баланса
            $balance = (float)$options['amount'];

            // Ставим дополнительное условие, которая приостановит автоматическую обработку,
            // в случае если он отправим меньше установленной минимальной цены
            if($this->accurate_balance and $balance < $this->getOrderMinAmount())
            {
                // Добавляем в событие
                $this->addEventForMinAmount($balance, 'westwallet');
                $array['status'] = 1;
                $this->inFlowFunds();
                // Автоматически баним мошеннические заявки
                $this->autoBanClientForAutoPayment();
                return $array;
            }

            // Дополнительное условие для баланса
            if($this->accurate_balance == true and $balance < (float)$this->getAmountIn()) {
                // Автоматический пересчет по новой цене
                $this->recount(0, $balance);
                // Добавляем в событие
                $this->addEventForMinOrder($balance, 'westwallet');
            }

            // Проверить в истории
            $wallet_tx = WalletTransaction::where('id_task', $this->transaction->id)->exists();
            if($wallet_tx == false)
            {
                // Записываем в лог
                $this->writePaymentTxHistory([
                    'address'           =>  (isset( $options['address']) ? $options['address'] : '~'),
                    'amount'            =>  $balance,
                    'confirmations'     =>  $options['blockchain_confirmations'],
                    'txid'              =>  $options['blockchain_hash']
                ], 'westwallet');

                // Меняем статус транзакции
                if($this->getStatus() != 4)
                    $this->setStatus(7);

            } else {
                WalletTransaction::where('id_task', $this->transaction->id)->update([
                    'confirmations' =>  $options['blockchain_confirmations'],
                ]);

                // Записываем в лог подтверждений
                $this->logConfirmation($options['blockchain_confirmations']);
            }

            $array['status'] = 0;
        }

        return $array;
    }

    /**
     * Валидация входных параметров
     *
     * @param Collection $options
     * @param array $args
     * @throws InvalidRequestException
     */
    public function validateIPN(Collection $options, ...$args)
    {
        foreach ($args as $key) {
            if (! $options->has($key)) {
                throw new InvalidRequestException("The {$key} parameter is required");
            }
        }
    }
}