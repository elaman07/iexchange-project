<?php
namespace App\Common\Packages\Transaction\Concerns;

use App\Mail\DeferredTaskMail;
use App\Models\ReserveLog;
use Illuminate\Support\Facades\Mail;

trait ManagersReserves
{
    /**
     * Корректировка резерва по указанным ценам
     * Node: Если недостаточно резерва то временно замораживаем ее
     *
     * @param bool $reset
     * @throws \Exception
     */
    public function changeReserves($reset = false)
    {
        //Сбрасываем резерв с случае неудачи
        if($reset == true) {
            $this->resetReserve();
        } else {
            if($this->hasAction() == 'success') {
                $this->successReserve();
            } else {
                $this->highlightReserve();
            }
        }
    }

    /**
     * Глобольное изменение резервов
     *
     * @return void
     */
    public function changeGlobalReserves()
    {
        $in = $this->getReserveIn();
        $out = $this->getReserveOut();

        $inNumberFormat = $this->getCurrencyIn()->number_format;
        if((int)$inNumberFormat > (int)iEXSetting('max_number_format_reserve')) {
            $inNumberFormat = (int)iEXSetting('max_number_format_reserve');
        }

        $outNumberFormat = $this->getCurrencyOut()->number_format;
        if((int)$outNumberFormat > (int)iEXSetting('max_number_format_reserve')) {
            $outNumberFormat = (int)iEXSetting('max_number_format_reserve');
        }


        $result_in = iex_number_format($in->summa + $this->getAmountIn(), $inNumberFormat);
        $result_out = iex_number_format($out->summa - $this->getAmountOut(), $outNumberFormat);

        if($this->hasInCurrencyReserve() == false)
        {
            ReserveLog::create([
                'id_task'      => $this->transaction->id,
                'id_currency'  => $this->getCurrencyIn()->id,
                'id_reserve'   => $this->getReserveIn()->id,
                'summa'        => $this->getAmountIn(),
                'type'         => 'plus',
                'history'      =>  $in->summa.' > '. $result_in
            ]);

            // Не обновляем резерв
            if($in->is_fixed_reserve == 1) {
                return;
            }

            $this->getReserveIn()->update(['summa' =>  $result_in]);
        }


        if($this->hasOutCurrencyReserve() == false)
        {
            ReserveLog::create([
                'id_task'           =>  $this->transaction->id,
                'id_currency'       =>  $this->getCurrencyOut()->id,
                'id_reserve'        =>  $this->getReserveOut()->id,
                'summa'             =>  $this->getAmountOut(),
                'commission'        =>  0,
                'type'              => 'minus',
                'history'           =>  $out->summa.' > '. $result_out
            ]);

            // Не обновляем резерв
            if($out->is_fixed_reserve == 1) {
                return;
            }

            $this->getReserveOut()->update(['summa' =>  $result_out]);
        }
    }

    /**
     * Выделяем резерв после нажития кнопки "Я оплатил"
     *
     */
    public function highlightReserve()
    {
        $reserve_out = $this->getReserveOut();

        $outNumberFormat = $this->getCurrencyOut()->number_format;
        if((int)$outNumberFormat > (int)iEXSetting('max_number_format_reserve')) {
            $outNumberFormat = (int)iEXSetting('max_number_format_reserve');
        }

        $result_out = iex_number_format($reserve_out->summa - $this->getAmountOut(), $outNumberFormat);

        if($this->hasOutCurrencyReserve() == false)
        {
            ReserveLog::create([
                'id_task'           =>  $this->transaction->id,
                'id_currency'       =>  $this->getCurrencyOut()->id,
                'id_reserve'        =>  $reserve_out->id,
                'summa'             =>  $this->getAmountOut(),
                'commission'        =>  0,
                'type'              => 'minus',
                'history'           =>  $reserve_out->summa.' > '. $result_out
            ]);

            // Не обновляем резерв
            if($reserve_out->is_fixed_reserve == 1) {
                return;
            }

            $reserve_out->update(['summa' =>  $result_out]);
        }

    }

    /**
     * Проверяем, хватает ли резервов для выделения, в противном случае замараживаем средства.
     *
     * @deprecated
     */
    private function isReserve() {
        return true;
    }

    /**
     * Зачислить в резерв
     */
    public function skipReserve()
    {
        if($this->transaction->skip_reserve == 0)
        {
            $this->successReserve();
            $this->transaction->update([
                'skip_reserve'  =>  1
            ]);
        }
    }

    /**
     * Снимаем резервы с валюты которая находится в блоке "Отдаю"
     */
    public function successReserve()
    {
        $reserve_in = $this->getReserveIn();

        $inNumberFormat = $this->getCurrencyIn()->number_format;
        if((int)$inNumberFormat > (int)iEXSetting('max_number_format_reserve')) {
            $inNumberFormat = (int)iEXSetting('max_number_format_reserve');
        }

        $result_in = iex_number_format($reserve_in->summa + $this->getAmountIn(), $inNumberFormat);

        if($this->hasInCurrencyReserve() == false)
        {
            ReserveLog::create([
                'id_task'      => $this->transaction->id,
                'id_currency'  => $this->getCurrencyIn()->id,
                'id_reserve'   => $reserve_in->id,
                'summa'        => $this->getAmountIn(),
                'type'         => 'plus',
                'history'      =>  $reserve_in->summa.' > '. $result_in
            ]);

            // Не обновляем резерв
            if($reserve_in->is_fixed_reserve == 1) {
                return;
            }

            $reserve_in->update(['summa' =>  $result_in]);
        }
    }

    /**
     * Общая корректировка резерва
     * @throws \Exception
     */
    public function totalChangeReserve()
    {
        $this->skipReserve();
        $this->changeReserves();
    }

    /**
     * Сбросить выделеннные для заявки резервы
     *
     * @throws \Exception
     */
    private function resetReserve()
    {
        $reserve_in = $this->getReserveIn();
        $reserve_out = $this->getReserveOut();

        // Не обновляем резерв
        if($reserve_in->is_fixed_reserve == 1 or $reserve_out->is_fixed_reserve == 1) {
            return;
        }

        ReserveLog::where('id_task', $this->transaction->id)->delete();
        $result_out = number_format($reserve_out->summa + $this->getAmountOut(), $this->getCurrencyOut()->number_format,'.','');


        $reserve_out->update(['summa' =>  $result_out]);

        //В случае если сумма заранее добавлена в резерв то обновляем
        if($this->transaction->skip_reserve == 1)
        {
            $result_in = number_format($reserve_in->summa - $this->getAmountIn(), $this->getCurrencyIn()->number_format,'.','');
            $reserve_in->update(['summa' => $result_in]);
        }
    }

    /**
     *
     * @param $old_amount
     * @throws \Exception
     */
    public function updateReserve($old_amount)
    {
        $reserve_out = $this->getReserveOut();

        // Не обновляем резерв
        if($reserve_out->is_fixed_reserve == 1) {
            return;
        }

        ReserveLog::where('id_task', $this->transaction->id)->delete();
        $result_out = number_format($reserve_out->summa + $old_amount, $this->getCurrencyOut()->number_format,'.','');
        $reserve_out->update(['summa' =>  $result_out]);


        $this->highlightReserve();
    }


    /**
     * Проверяем, увеличивался ли ранее резерв для валюты (Отдаю)
     *
     * @return boolean
     */
    protected function hasInCurrencyReserve() {

        // Проверяем, уменьшался ли резерв ранее
        $exists = ReserveLog::where([
            ['id_task', '=', $this->transaction->id],
            ['id_currency', '=', $this->getCurrencyIn()->id],
            ['id_reserve', '=', $this->getReserveIn()->id],
            ['type', '=', 'plus']
        ])->exists();

        return $exists;
    }

    /**
     * Проверяем, увеличивался ли ранее резерв для валюты (Получаю)
     *
     * @return boolean
     */
    protected function hasOutCurrencyReserve() {

        // Проверяем, уменьшался ли резерв ранее
        $exists = ReserveLog::where([
            ['id_task', '=', $this->transaction->id],
            ['id_currency', '=', $this->getCurrencyOut()->id],
            ['id_reserve', '=', $this->getReserveOut()->id],
            ['type', '=', 'minus']
        ])->exists();

        return $exists;
    }
}
