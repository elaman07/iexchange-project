<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.07.2019
 * Time: 8:22
 */

namespace App\Common\Packages\Transaction\Concerns;

use App\Models\CurrencyFields;
use Illuminate\Support\Facades\Request;

trait UpdateCustomFields
{
    /**
     * Обновление доп. полей для отдаю
     */
    public function updateInCustomField()
    {
        $input = Request::all();
        foreach (CurrencyFields::all() as $item) {
            if(isset($input[$item->key_id])) {
                $this->transaction->{$item->key_id} = security_xss($input[$item->key_id]);
            }
        }
        $this->transaction->save();
        flash('Данные обновлены', ['alert alert-success']);
    }

    /**
     * Обновление доп. полей для отдаю
     */
    public function updateOutCustomField()
    {
        $input = Request::all();
        foreach (CurrencyFields::all() as $item) {
            if(isset($input[$item->key_id])) {
                $this->transaction->{$item->key_id} = security_xss($input[$item->key_id]);
            }
        }
        $this->transaction->save();
        flash('Данные обновлены', ['alert alert-success']);
    }

    public function updateEditData()
    {
        if($this->transaction->status == 4) {
            return;
        }

        $this->transaction->update([
            'from_shot' => Request::has('from_shot') ? Request::get('from_shot') : null,
            'to_shot' => Request::has('to_shot') ? Request::get('to_shot') : null,
            'id_edit_data_manager' => auth()->id()
        ]);

        flash('Данные обновлены', ['alert alert-success']);
    }

    public function updateAddRequisitesPayment(string $account, $requisites_description = null)
    {
        $this->transaction->update([
            'requisites_receive' => $account,
            'requisites_description' => $requisites_description
        ]);
    }
}
