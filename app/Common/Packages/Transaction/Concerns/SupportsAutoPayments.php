<?php
namespace App\Common\Packages\Transaction\Concerns;

use App\Events\AutoPaymentSuccessful;
use App\Models\AutoSenderPayment;
use App\Models\LogAutoPayment;
use App\Models\Task;
use App\Models\WalletsHistory;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Modules\AMLServices\AMLServiceFacade;
use Nwidart\Modules\Facades\Module;

/**
 * Поддержка автовыплат
*/
trait SupportsAutoPayments
{

    /**
     * Получаем время автовыплаты
     *
     * @return string
    */
    public function hasTimeOutAutoPayment()
    {
        $max_register_limit = (int)$this->getMerchant()->max_register_blockchain;
        $max_first_confirm = (int)$this->getMerchant()->max_first_confirm_blockchain;
        $now = Carbon::now();

        // В случае если транзакция не зарегистировано в сети
        if($this->transaction->register_tx == 0)
        {
            if(Carbon::parse($this->transaction->created_at)->addMinutes($max_register_limit) < $now)
            {
                $this->transaction->update(['is_bot' => 0]);
                $is_check_pay = $this->getMerchant();
                $provider_alias = (isset($is_check_pay) ? $is_check_pay->gateway->alias : '~');

                // Лог в случае если установленно время истекло
                iex_order_check_pay_log($this->transaction->id, 1,
                    'Установленное время истекло, заявка автоматически отклонена', $provider_alias);
                $this->failed();
            }

            return Carbon::parse($this->transaction->created_at)->addMinutes($max_register_limit) < $now;
        }

        // В ожидании 1-го подтверждения от сети
        return Carbon::parse($this->transaction->created_at)->addHours($max_first_confirm) < $now;
    }

    /**
     * Автовыплата и закрытие заявки
     *
     * @throws \Exception
     */
    public function paymentRelatedInstanceFor()
    {
        // Если автовыплаты отключены, ничего не делаем
        if(iEXSetting('is_enabled_autopayment') != 1) {
            return;
        }
        $pay = $this->getPay();

        \Log::info('-- Запуск процесс автовыплаты по заявке №'.$this->transaction->id);

        // Приостанавливаем автовыплату, в случае холда
        if($this->transaction->is_hold == 1) {
            return;
        }

        // AML Защита
        if(Module::find('AMLServices')->isEnabled() == 1)
        {
            if(isset($this->getCurrencyIn()->aml_service_model) and $this->transaction->task_info->is_aml_analysis == 1 and $this->getCurrencyIn()->is_in_aml_check_tx == 2)
            {
                if($this->transaction->task_info->is_aml_high_risk == 1) {
                    throw new \Exception('Выплата не произведена, высокий риск AML опасности. Процент риска:'.$this->transaction->task_info->aml_result_value);
                }

                // Для GETBlock
                if($this->getCurrencyIn()->aml_service_model->aml_type == 'getblock')
                {
                    if(isset($this->transaction->aml_analytics_log_getblock_success))
                    {
                        $aml_other_params = collect($this->transaction->aml_analytics_log_getblock_success->json_value['result']['check']['report']['signals'])
                            ->only(['dark_market', 'dark_service', 'exchange_fraudulent', 'gambling', 'mixer', 'ransom', 'sanctions', 'scam', 'stolen_coins', 'terrorism_financing'])
                            ->reject(function($value, $key) {
                                return $value == 0;
                            });

                        $aml_services = AMLServiceFacade::make(
                            $this->getCurrencyIn()->aml_service,
                            $this->getCurrencyIn()->aml_service_model->aml_type
                        );

                        foreach ($aml_other_params as $key => $value)
                        {
                            $value_percent = (float)$value*100;
                            $validate_many_risk = (float)$aml_services->getValidateManyRisk($key);

                            if(!empty($validate_many_risk) and $validate_many_risk > 0 and $value_percent >= $validate_many_risk)
                            {
                                throw new \Exception('Выплата не произведена, высокий риск AML опасности. Процент риска из категории ('.$key.'):'.$value_percent.'%');
                            }

                        }
                    }
                }
            }
        }

        // Проверяем для начало выбрана ли система выплаты и настроена ли конфигурация
        if(isset($pay))
        {
            \Log::info('-- Начинаем перевод средства клиенту по заявке №'.$this->transaction->id);
            $alias = $pay->gateway->alias;

            $error = [];
            // Проверка мин. макс. суммы автовыплат
            if($this->getCurrencyOut()->out_pay_min_amount > 0 && $this->transaction->receiving_price < $this->getCurrencyOut()->out_pay_min_amount) {
                iex_order_auto_payment_log($this->transaction->id,1,'Превышена мин. сумма автовыплаты', $alias);
                $error['status'] = 1;
            } elseif($this->getCurrencyOut()->out_pay_max_amount > 0 && $this->transaction->receiving_price > $this->getCurrencyOut()->out_pay_max_amount) {
                iex_order_auto_payment_log($this->transaction->id,1,'Превышена макс. сумма автовыплаты', $alias);
                $error['status'] = 1;
            }

            // Проверка установленного суточного лимита выплат
            if($this->getCurrencyOut()->out_pay_day_limit > 0 && $this->getAutoPaymentLimitDay() > $this->getCurrencyOut()->out_pay_day_limit) {
                iex_order_auto_payment_log($this->transaction->id,1,'Превышен суточный лимит', $alias);
                $error['status'] = 1;
            }

            // Проверка установленного месячного лимита выплат
            if($this->getCurrencyOut()->out_pay_month_limit > 0 && $this->getAutoPaymentLimitMonth() > $this->getCurrencyOut()->out_pay_month_limit) {
                iex_order_auto_payment_log($this->transaction->id,1,'Превышен месячный лимит', $alias);
                $error['status'] = 1;
            }

            // Задержка автовыплаты
            if($this->getCurrencyOut()->is_hold == 1 and !is_null($this->transaction->started_at))
            {
                $timeout = Carbon::parse($this->transaction->started_at)->addHours($this->getCurrencyOut()->hold_in_hours);
                $now = Carbon::now();

                if($timeout->gt($now) > 0 and $this->getTypeHold() == true) {
                    iex_order_auto_payment_log($this->transaction->id,1,'Активировано задержка автовыплаты для клиента', $alias);
                    $error['status'] = 1;
                }
            }

            // Проверка баланса
            if($this->hasBalance() == 1) {
                iex_order_auto_payment_log($this->transaction->id,1,'Недостаточно средств для автовыплаты', $alias);
                $error['status'] = 1;
            }

            // Если не найдены ошибки то выполняем автовыплату
            if(empty($error)) {
                $this->disableIsBot();
                $this->autoPaymentLoaded();
                $this->successReserve();
                $this->success(['disable_reserve' => true]);
            } else {
                $this->disableIsBot();
            }
        }
    }

    /**
     * Обработчик автовыплат
     *
     * @throws \Exception
     */
    public function autoPaymentLoaded()
    {
        //\Log::debug('-- Начинаем обработку и отправлять средства клиенту по заявке #'.$this->transaction->id);
        // Функция, работает только через CRON
        if($this->getIsCron() == true)
        {
            // Исключаем двойные снятия
            $find = AutoSenderPayment::where('id_order', $this->transaction->id);
            if($find->exists()) {
                $this->turnOffAutoOutput();
                throw new \Exception("Неудачная попытка двойных снятий средств по заявке №{$this->transaction->id}");
            }
        }

        AutoSenderPayment::updateOrCreate([
            'id_order' => $this->transaction->id
        ], [
            'comment' => "Неудачная попытка двойных снятий средств по заявке №{$this->transaction->id}"
        ]);

        // Не делаем никаких выплат в случае активности Hold
        if($this->transaction->is_hold == 1 and $this->isHold() == true) {
            throw new \Exception('Транзакция не разморожено, осталось еще'. $this->holdTimeOut());
        }

        $this->isConsoleCommission = true;
        $payment = Str::studly($this->getPay()->gateway->alias);

        //\Log::debug('--Запрос к сервису для выплаты #'.$this->transaction->id);
        $class = "\\App\\Common\\Packages\\Transaction\\Payments\\{$payment}Payment";
        // Проверяем, существует ли класс выплаты
        if(class_exists($class)) {
            return new $class($this, $this->withAsPaymentOptions());
        }

        throw new \Exception('Не найден провайдер автовыплат '.$payment);
    }

    /**
     * Получаем суточный лимит выплат
     *
     * @return float
     */
    private function getAutoPaymentLimitDay()
    {
        $sum = Task::with(['de_no_group' => function($q) {
            $q->where('id_currency2', '=', $this->getCurrencyOut()->id);
        }])->where('status', '=', 4)
            ->whereDate('updated_at', Carbon::today())->sum('receiving_price');

        return (float)$sum + $this->transaction->receiving_price;
    }

    /**
     * Получаем месячный лимит выплат
     *
     * @return float
     */
    private function getAutoPaymentLimitMonth()
    {
        $sum = Task::with(['de_no_group' => function($q) {
            $q->where('id_currency2', '=', $this->getCurrencyOut()->id);
        }])->where('status', '=', 4)
            ->whereBetween('updated_at', [
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
            ])->sum('receiving_price');

        return (float)$sum + $this->transaction->receiving_price;
    }

    /**
     * Для кого задержка
     *
     * @return bool
     */
    public function getTypeHold()
    {
        // Тип задержки
        $type_delay = false;
        if($this->getCurrencyOut()->hold_delay == 1) // Для всех
            $type_delay = true;
        elseif($this->getCurrencyOut()->hold_delay == 0) // Для новых клиентов
            $type_delay = ($this->getClient()->order_num == 0);
        return $type_delay;
    }

    /**
     * Дополнительные опции которые будут переданы в класс выплаты
     *
     * @return array
    */
    private function withAsPaymentOptions()
    {
        return [
            'code' => $this->getCodeOut()->name
        ];
    }
}
