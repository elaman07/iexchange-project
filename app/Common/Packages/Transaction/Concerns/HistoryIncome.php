<?php
namespace App\Common\Packages\Transaction\Concerns;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

trait HistoryIncome
{
    /**
     * Получить историю транзакции с сервера
    */
    public function getTransactionById()
    {
        // Если не найдено
        if(!$this->historyPaymentTx()) {
            return false;
        };

        $merchant = $this->getMerchant();
        if(!is_null($merchant)) {
            $method = 'getTx'.Str::studly($merchant->gateway->alias);
            if(method_exists($this, $method) and $this->getCurrencyIn()->is_server_tx_merchant) {
                return $this->$method();
            }
        }
    }


    /**
     * Проверка транзакций
    */
    public function checkedMerchantOrder()
    {
        if(isset($this->transaction->history_payment_transaction))
        {
            $merchant = $this->getMerchant();
            if(!is_null($merchant)) {
                $method = 'getTx'.Str::studly($merchant->gateway->alias);
                if(method_exists($this, $method) and $this->getCurrencyIn()->is_server_tx_merchant) {
                    try {
                        $response = $this->$method($merchant->id);
                        return (!empty($response) and $response['currency'] == $this->getCodeIn()->name ? 1 : 0);
                    }catch (\Exception $e) {
                        return 0;
                    }
                }
            }
        }

        return 1;
    }

    /**
     * Получаем информацию по AdvCash
     */
    private function getTxAdvCash()
    {
        $response = \Cache::rememberForever('in-history-advcash-'.$this->transaction->id, function()
        {
            return PaymentFacade::merchant('advcash', $this->getMerchant()->id)
                ->api()
                ->findTransaction($this->historyPaymentTx()->transfer);
        });

        if(!empty($response)) {
            return $this->formatMessages([
                'id' => '',
                'datetime' => Carbon::parse(strtotime($response->startTime))->translatedFormat('d M Y, H:i'),
                'to' => $response->walletDestId,
                'amount' => $response->amount,
                'currency' => str_replace('RUR', 'RUB', $response->currency)
            ]);
        }
    }

    /**
     * Получаем информацию по ePayCore
     */
    private function getTxePayCore()
    {
        $response = \Cache::rememberForever('in-history-epaycore-'.$this->transaction->id, function() {
            return \Payment::merchant('epaycore', $this->getMerchant()->id)
                ->api()
                ->findTransaction($this->historyPaymentTx()->transfer);
        });

        if(!empty($response)) {
            return $this->formatMessages([
                'id' => $response->batch,
                'datetime' => null,
                'to' => $response->dst->account,
                'amount' => $response->dst->amount,
                'currency' => $response->dst->currency->code
            ]);
        }
    }

    /**
     * Получаем информацию по Payeer
    */
    private function getTxPayeer()
    {
        // Запрашиваем инфомарцию один раз и кэшируем
        $response = \Cache::rememberForever('in-history-payeer-'.$this->transaction->id, function() {
            return \Payment::merchant('payeer', $this->getMerchant()->id)
                ->api()
                ->getHistoryInfo($this->historyPaymentTx()->transfer);
        });

        if(!empty($response)) {
            return $this->formatMessages([
                'id' => $response['id'],
                'datetime' => $response['dateCreate'],
                'to' => $response['to'],
                'amount' => $response['sumOut'],
                'currency' => $response['curOut']
            ]);
        }
    }


    /**
     * Шаблон результатов
     *
     * @param $data
     * @return array
     */
    private function formatMessages($data): array
    {
        return [
            'id'        =>  ($data['id'] ?? null),
            'date'      =>  ($data['datetime'] ?? null),
            'to'        =>  ($data['to'] ?? null),
            'amount'    =>  ($data['amount'] ?? 0),
            'currency'  =>  ($data['currency'] ?? null),
            'codepro'   =>  ($data['codepro'] ?? null),
            'unaccepted'=>  ($data['unaccepted'] ?? null)
        ];
    }
}
