<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class FirekassaPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Firekassa';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('firekassa', $this->pay_id)
            ->api();

        $site_account = $this->getGatewayPay()->site_account;

        // Тип транзакции
        $response_method = match ($this->getGatewayPay()->method_pay) {
            1 => $facade->transfer('card', $this->getAmount(), $this->getScore(), $this->getComment(), $this->getComment(true), $this->order, $site_account),
            2 => $facade->transfer('yandex', $this->getAmount(), $this->getScore(), $this->getComment(), $this->getComment(true), $this->order, $site_account),
            default => $facade->transfer('wallet', $this->getAmount(), str_replace(['(', ')', ' '], '', preg_replace('/^\+?([87])/', '7', $this->getScore())), $this->getComment(), $this->getComment(true), $this->order, $site_account)
        };

        // Запись ID, для получения статус платежа
        if(isset($response_method['id']))
        {
            $this->order->update([
                'is_status_pay_callback' => $response_method['id'],
            ]);
        }

    }
}
