<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class WebmoneyPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Webmoney';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('webmoney', $this->pay_id)
            ->api();

        $order_id = $this->order->id;
        if(!empty($this->getGatewayPay()->num_request)) {
            $order_id = (int)str_replace(['[order_id]', '[code]'], [$this->order->id, $this->order->unique_security_code], $this->getGatewayPay()->num_request);
        }

        $response = $facade->transfer(
            Str::upper($this->getScore()),
            Str::upper($this->getCode()),
            $this->amount,
            $order_id,
            $this->getComment()
        );


        if(!empty($response)) {
            $this->addToHistory($response);
        }
    }
}
