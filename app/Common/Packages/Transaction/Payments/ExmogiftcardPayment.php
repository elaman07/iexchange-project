<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Jobs\HistoryCodeSendJob;
use App\Models\HistoryCode;

class ExmogiftcardPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'ExmoGiftCard';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $facade = PaymentFacade::pay('exmogiftcard', $this->pay_id)
            ->api();

        $response = $facade->createCode($this->getCode(), $this->amount);

        if($response['task_id'] == 0) {
            throw new \Exception('Возникли проблемы при отправки ExCode');
        }

        if(!array_key_exists('code', $response)) {
            throw new \Exception('ExCode не создан');
        }

        HistoryCode::create([
            'id_task' => $this->order->id,
            'code' => $response['code'],
            'amount'    =>  $response['amount'],
            'currency' => $response['currency'],
            'provider_id' => 'exmogiftcard',
            'id_pay' => $this->pay_id
        ]);

        // Отправляем ex-code на e-mail адрес клиента
        // Поставить в очередь за выполненение
        $delay = now()->addSeconds(30);
        dispatch(new HistoryCodeSendJob($this->order, $response['code']))
            ->delay($delay)->onQueue('low');
    }
}
