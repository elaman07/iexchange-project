<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\PayTransactionHash;
use Illuminate\Support\Str;

class GarantexPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Garantex';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Garantex', $this->pay_id)
            ->api();

        $code_currency = $this->getDynamicCodeCurrency($this->order->direction_exchange->currency2);
        $response = $facade->transfer($code_currency, $this->amount, $this->getScore());

        if(isset($response['id'])) {
            $this->addToHistory($response['id']);

            PayTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'garantex',
                'id_currency' => $this->order->direction_exchange->id_currency2,
                'id_task' => $this->order->id,
                'api_transfer_id' => $response['id']
            ]);

            $this->order->task_info->update([
                'is_wait_hash_pay' => 1
            ]);
        }
    }
}
