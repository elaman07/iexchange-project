<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class PayeerPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Payeer';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('payeer', $this->pay_id)
            ->api();

        $response = $facade->transfer([
            'amount'            =>  $this->getAmount(),
            'currency'          =>  $this->code,
            'to'                =>  $this->getScore(),
            'comment'           =>  $this->getComment(),
            'who_commission'    =>  $this->whoPayCommission()
        ]);

        if($response['auth_error'] == 1) {
            throw new \Exception(json_encode($response['errors']));
        }

        if($response['success'] == true) {
            $this->addToHistory($response['historyId']);
        }
    }
}
