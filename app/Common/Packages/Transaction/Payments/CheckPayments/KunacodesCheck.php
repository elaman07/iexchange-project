<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\HistoryCode;
use Illuminate\Support\Str;

class KunacodesCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'KunaCodes';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('KunaCodes', $this->pay_id)->api();

        $incomeCode = $this->order->income_code;

        if(empty($incomeCode)) {
            iex_order_check_pay_log($this->order->id, 1, 'Kuna код не найден', 'kunacodes');
            $this->order->update(['is_bot' => 0]);

            return [
                'status' => 1,
                'message' => 'Kuna код не найден'
            ];
        }

        try {
            $activate_code = $provider->activateCode($incomeCode);
        }catch (\Exception $exception) {

            $this->order->update(['is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Kuna код код не найден'
            ];
        }

        if(!is_array($activate_code))
        {
            $this->order->update(['is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Возникли ошибки при проверки Kuna кода'
            ];
        }

        if($activate_code['amount'] <= 0) {
            iex_order_check_pay_log($this->order->id, 1, 'Указанный Kuna код некорректный', 'kunacodes');
            $this->order->update(['check_income_code' => 1, 'is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Указанный Kuna код некорректный'
            ];
        }

        // Проверяем код валюты
        if(Str::upper($activate_code['currency']) != Str::upper($this->transaction->getCodeIn()->name))
        {
            $this->order->update(['check_income_code' => 1, 'is_bot' => 0]);
            $this->transaction->addEventIncorrectCode($activate_code['amount'], $activate_code['currency']);

            return [
                'status' => 1,
                'message' => 'Некорректная валюта Kuna код'
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$activate_code['amount'] < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($activate_code['amount'], 'kunacodes');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }


        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$activate_code['amount'] < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $activate_code['amount']);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($activate_code['amount'], 'kunacodes');
        }

        $this->order->update(['register_tx' => 1, 'check_income_code' => 2]);


        // Также записываем код в историю
        HistoryCode::create([
            'id_task' =>  $this->order->id,
            'code' =>  $incomeCode,
            'provider_id' => 'kunacodes',
            'amount' => $activate_code['amount'],
            'currency' => $activate_code['currency'],
            'type' => 'load'
        ]);

        // Меняем статус транзакции
        if($this->transaction->getStatus() != 4)
            $this->transaction->setStatus(7);

        // Добавляем в событие
        $this->transaction->addEventOtherCode($activate_code['amount'], $activate_code['currency'], [
            'name' => 'Kuna код',
            'alias' => 'kunacodes'
        ]);

        return ['status' => 0];
    }
}
