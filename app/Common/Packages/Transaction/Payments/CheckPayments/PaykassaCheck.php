<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\HistoryPaymentTransaction;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;

class PaykassaCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'PayKassa';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('PayKassa', $this->pay_id)->api();

        $wallet_id = HistoryPaymentTransaction::where([
            'id_task' => $this->order->id,
            'payment' => 'paykassa'
        ])->first();

        // Если ID платежа не найден
        if(empty($wallet_id)) {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Средства не поступили'
            ];
        }

        // Получаем детали транзакции по адресу с сервера
        $tx_refs = $provider->findTransaction($wallet_id->transfer);
        // Перед обработкой проверяем, существует ли вообще транзакция по адресу
        $validate_tx = !empty($tx_refs) and $tx_refs['order_id'] == $this->order->id;

        // Если не подтвержден входщий платеж, не пускаем дальше
        if(!$validate_tx) {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Транзакция не найдена в Блокчейне'
            ];
        }

        // Получение баланса
        $balance = (float)$tx_refs['amount'];

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0) {
            $this->transaction->addEventRegisterTransaction($tx_refs['txid'], $tx_refs['confirmations'], $tx_refs['amount'], 'paykassa');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['txid']) and !empty($tx_refs['txid']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'paykassa',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['txid']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['amount']
                ]);
            }
        }

        if($tx_refs['status'] == 'no')
        {
            return [
                'status' => 1,
                'event' => 'not_completed',
                'message' => 'Транзакция в найдено в Blockchain и находится в обработке'
            ];
        }

        // Если кол-во подтверждений в сети меньше чем установлено, не пускаем дальше
        // до тех пор, пока не выполнится это условие
        if($tx_refs['confirmations'] < $tx_refs['required_confirmations']) {
            return [
                'status' => 1,
                'event' => 'not_min_confirm',
                'message' => 'Недостаточно подтверждений'
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and $balance < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($balance, 'paykassa');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$balance < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $balance);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($balance, 'paykassa');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  ($tx_refs['address'] ?? null),
                'amount'        =>  ($tx_refs['amount'] ?? null),
                'confirmations' =>  ($tx_refs['confirmations'] ?? null),
                'txid'          =>  ($tx_refs['txid'] ?? null),
            ], 'westwallet');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);

        } else {
            WalletTransaction::whereIdTask($this->order->id)->update([
                'confirmations' =>  $tx_refs['confirmations'],
            ]);

            // Записываем в лог подтверждений
            $this->logConfirmation($tx_refs['confirmations']);
        }

        return [
            'status' => 0,
            'event' => 'competed_tx'
        ];
    }
}
