<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\HistoryPaymentTransaction;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AlfabitpayCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'AlfaBitPay';


    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('AlfaBitPay', $this->pay_id)->api();
        $getCache = cache()->get('merchant-alfabitpay__id'.$this->order->id);

        // Если ID платежа не найден
        if(empty($getCache))
        {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Средства не поступили'
            ];
        }

        $tx_refs = $provider->findTransaction($getCache['uuid']);

        // Проверяем, есть ли транзакция
        if(!is_array($tx_refs) or empty($tx_refs)) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        if(!isset($tx_refs['result'])) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили (Данные не найдены)'
            ];
        }

        if(!isset($tx_refs['result']['tx'])) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили (Транзакция не найдена)'
            ];
        }


        $tx_data = $tx_refs['result']['tx'];
        // Получение баланса
        $balance = (float)$tx_data['amount'];

        // Текущий код валюты (в сети)
        $code_currency = $this->transaction->getCodeIn()->name;
        $network_code = $this->transaction->getCurrencyIn()->network_code;
        if(!empty($network_code)) {
            $code_currency = $network_code;
        }

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0 and !empty($tx_data['txHashIn'])) {
            $this->transaction->addEventRegisterTransaction($tx_data['txHashIn'] ?? null , 1, $balance, 'alfabitpay');
            $this->order->update(['register_tx' => 1]);
        }


        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_data['txHashIn']) and !empty($tx_data['txHashIn']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'alfabitpay',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_data['txHashIn']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($balance) and empty($this->order->in_amount_merchant))
            {
                $this->order->update([
                    'in_amount_merchant' => $balance
                ]);
            }
        }

        // Если мы получаем не "completed" то не пускаем дальше
        if(in_array($tx_data['status'], ['waitDeposit', 'waitConfirmation', 'pending']))
        {
            return [
                'status' => 1,
                'event' => 'not_completed',
                'message' => empty($tx_data['txHashIn']) ? 'Транзакция находится в обработке.' : 'Транзакция в найдено в Blockchain и находится в обработке'
            ];
        }

        // Проверяем код валюты
        if(Str::upper($tx_data['tokenCode']) != Str::upper($code_currency))
        {
            iex_order_check_pay_log($this->order->id, 1, 'Отправлена ошибочная транзакция, код валюты не соответствует', 'alfabitpay');
            $this->order->update(['is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Отправлена ошибочная транзакция, код валюты не соотвествует'
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$balance < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($balance, 'alfabitpay');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() and (float)$balance < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $balance);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($balance, 'alfabitpay');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  ($tx_data['actorAddress'] ?? null),
                'amount'        =>  $balance,
                'confirmations' =>  1,
                'txid'          =>  $tx_data['txHashIn'],
            ], 'alfabitpay');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        }

        return [
            'status' => 0
        ];
    }
}
