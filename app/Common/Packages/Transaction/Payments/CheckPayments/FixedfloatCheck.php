<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\HistoryPaymentTransaction;
use App\Models\WalletTransaction;

class FixedfloatCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'FixedFloat';


    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('FixedFloat', $this->pay_id)->api();
        $getCache = cache()->get('merchant-fixedfloat__id'.$this->order->id);

        // Если ID платежа не найден
        if(empty($getCache)) {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Средства не поступили'
            ];
        }

        $tx_refs = $provider->findTransaction($getCache['hash_id'], $getCache['token']);

        // Проверяем, есть ли транзакция
        if(!is_array($tx_refs) or empty($tx_refs)) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0 and in_array($tx_refs['data']['status'], ['NEW', 'PENDING', 'EXCHANGE', 'DONE'])) {
            $this->transaction->addEventRegisterTransaction($tx_refs['data']['from']['tx']['id'] ?? null , 1, $tx_refs['data']['from']['tx']['amount'], 'fixedfloat');
            $this->order->update(['register_tx' => 1]);
        }

        if(intval($tx_refs['data']['from']['tx']['confirmations']) < $tx_refs['data']['from']['reqConfirmations'])
        {
            return [
                'status' => 1,
                'message' => 'Нехватает кол-во подтверждений для успешного выполнения'
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$tx_refs['data']['from']['tx']['amount'] < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($tx_refs['data']['from']['tx']['amount'], 'fixedfloat');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$tx_refs['data']['from']['tx']['amount'] < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $tx_refs['data']['from']['tx']['amount']);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($tx_refs['data']['to']['from']['amount'], 'fixedfloat');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  ($tx_refs['address'] ?? null),
                'amount'        =>  (isset($tx_refs['data']['from']['tx']) ? $tx_refs['data']['from']['tx']['amount'] : null),
                'confirmations' =>  ($tx_refs['data']['from']['tx']['confirmations'] ?? null),
                'txid'          =>  null,
            ], 'fixedfloat');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        } else {

            WalletTransaction::whereIdTask($this->order->id)->update([
                'confirmations' =>  $tx_refs['data']['from']['tx']['confirmations'],
            ]);
            // Записываем в лог подтверждений
            $this->logConfirmation($tx_refs['data']['from']['tx']['confirmations']);
        }

        return [
            'status' => 0
        ];
    }
}
