<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;
use Illuminate\Support\Str;

class RapiraCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Rapira';


    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('Rapira', $this->pay_id)->api();

        $address = $this->transaction->getWalletAddresses();
        $address_id = $address->address;
        $asset_id = \Str::upper($this->transaction->getCodeIn()->name);
        $currency_in = $this->order->direction_exchange->currency1;

        // Текущий код валюты (в сети)
        $code_currency = $this->transaction->getCodeIn()->name;
//        $network_code = $this->transaction->getCurrencyIn()->network_code;
//        if(!empty($network_code)) {
//            $code_currency = $network_code;
//        }

        $tx_refs = collect($provider->findTransaction($code_currency))->filter(function($item) use($address_id) {
            return $item['address'] == $address_id;
        })->first();


        \Log::debug(json_encode($tx_refs));

        // Проверяем, есть ли транзакция
        if(!is_array($tx_refs) or empty($tx_refs) or !isset($tx_refs['id'])) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        if(!in_array($tx_refs['status'], ['MEMPOOL', 'PENDING_CONFIRMATIONS', 'PENDING_AML', 'MANUAL_CHECK', 'SUCCESS'])) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили, подождите...'
            ];
        }

        $realpay_st = ['SUCCESS'];
        $coldpay_st = ['MEMPOOL', 'PENDING_CONFIRMATIONS','PENDING_AML','MANUAL_CHECK'];
        $coldpay_st_need = ['MEMPOOL', 'PENDING_CONFIRMATIONS','PENDING_AML','MANUAL_CHECK'];


        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0)
        {
            $this->transaction->addEventRegisterTransaction($tx_refs['txid'], $tx_refs['confirmations'], $tx_refs['amount'], 'rapira');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['txid']) and !empty($tx_refs['txid']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'rapira',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['txid']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['amount']
                ]);
            }
        }

        if(in_array($tx_refs['status'], $coldpay_st))
        {
            return [
                'status' => 1,
                'message' => 'Заявка обрабатывается....'
            ];
        }

        $aml_provider = $this->getAMLProviderIn($currency_in);
        if (class_exists($aml_provider))
        {
            $response = new $aml_provider($this->order, $this->transaction);
            // Проверяем, существует ли транзакций
            if(isset($tx_refs['txid']) and !empty($tx_refs['txid']))
            {
                $aml_validator = $response->handleTx($address_id, $asset_id, $tx_refs['txid']);
                if($aml_validator['status'] == 1)
                {
                    return [
                        'status' => 1,
                        'message' => $aml_validator['message']
                    ];
                }
            }
        }


        if($tx_refs['confirmations'] < $this->transaction->getMerchant()->min_confirm)
        {
            return [
                'status' => 1,
                'message' => 'Нехватает кол-во подтверждений для успешного выполнения'
            ];
        }

        if(!in_array($tx_refs['status'], $realpay_st))
        {
            return [
                'status' => 1,
                'message' => 'Заказ еще находится в обработке...'
            ];
        }

        // Проверяем код валюты
        if(Str::upper($tx_refs['unit']) != Str::upper($code_currency))
        {
            $this->order->update(['is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Некорректная валюта Rapira'
            ];
        }


        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$tx_refs['amount'] < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($tx_refs['amount'], 'rapira');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$tx_refs['amount'] < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $tx_refs['amount']);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($tx_refs['amount'], 'rapira');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  ($tx_refs['address'] ?? null),
                'amount'        =>  ($tx_refs['amount'] ?? null),
                'confirmations' =>  ($tx_refs['confirmations'] ?? null),
                'txid'          =>  ($tx_refs['txid'] ?? null),
            ], 'rapira');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        } else {

            WalletTransaction::whereIdTask($this->order->id)->update([
                'confirmations' =>  $tx_refs['confirmations'],
            ]);
            // Записываем в лог подтверждений
            $this->logConfirmation($tx_refs['confirmations']);
        }

        return [
            'status' => 0
        ];
    }
}
