<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;
use Illuminate\Support\Arr;

class TronCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Tron';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $wallet_id = $this->getWalletAddress();

        $provider = PaymentFacade::merchant('Tron', $this->pay_id)->api();

        // Получаем детали транзакции по адресу с сервера
        $tx_refs = $provider->getTransactionByAddress($wallet_id);

        // Проверяем, есть ли транзакция
        if(empty($tx_refs['data'])) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        $tx = Arr::first($tx_refs['data']);

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0)
        {
            // Добавляем в событие регистрацию в транзакции
            $amount = iex_number_format($tx['contractData']['amount'] / 1e6);
            $this->transaction->addEventRegisterTransaction($tx['hash'], 1, $amount, 'tron');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx['hash']) and !empty($tx['hash']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'tron',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx['hash']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($amount) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $amount
                ]);
            }
        }

        // Если у транзакции есть подтверждение, начинаем обработку
        if($tx['confirmed'] != true)
        {
            return [
                'status' => 1,
                'message' => 'Транзакция найдена в Блокчейне, Подтвердждений нет'
            ];
        }

        if($tx['revert'] == true) {
            return [
                'status' => 1,
                'message' => 'Неизвестный статус'
            ];
        }

        if($tx['contractType'] != 1) {
            return [
                'status' => 1,
                'message' => 'Тип транзакции неверный'
            ];
        }

        // Получение баланса
        $balance = (float)$provider->getBalanceFromAddress($wallet_id);


        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$balance < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($balance, 'tron');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$balance < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $balance);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($balance, 'tron');
        }

        // Записываем в лог
        // Проверить в истории
        $wallet_tx = WalletTransaction::where('id_task', $this->order->id)->exists();
        if($wallet_tx == false)
        {
            $this->writePaymentTxHistory([
                'address'           =>  $tx['contractData']['to_address'],
                'amount'            =>  $balance,
                'confirmations'     =>  1,
                'txid'              =>  $tx['hash']
            ], 'tron');

            // Делаем транзакцию, которая отправит средства на основной адрес
            // В случае если отправленная сумма меньше чем мин. сумма то не делаем перевод
            if($balance >= $this->transaction->getOrderMinAmount()) {
                $provider->sendTransactionToBase($wallet_id, $this->transaction->getWalletAddresses()->private_key, $balance-2);
            } else {
                $this->transaction->getWalletAddresses()->update(['is_failed_send' => 1]);
            }

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        }

        return [
            'status' => 0
        ];
    }
}
