<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Transaction\Transaction;
use App\Jobs\TelegramConfirmBlockchainJob;
use App\Models\AmlAnalysisLog;
use App\Models\Task;
use App\Models\TaskLogConfirmation;
use App\Models\WalletTransaction;
use Modules\GetBlockBot\GetBlockBotFacade;
use Nwidart\Modules\Facades\Module;

abstract class AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName;

    /**
     * Детали транзакции
     *
     * @var Transaction
     */
    protected Transaction $transaction;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected Task $order;

    /**
     * ID системы проверки
     *
     * @var string
    */
    protected string $pay_id;


    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
        $this->order = $transaction->getTransaction();
        $this->pay_id = $this->order->id_merchant;
    }


    /**
     * Обработчик автовыплаты
     *
     * @return array
     */
    abstract public function handle(): array;


    /**
     * Записываем итоговый результат в базу
     *
     * @param array $detail
     * @param string|null $provider
     * @return void
     */
    protected function writePaymentTxHistory(array $detail = [], string $provider = null)
    {
        $response = WalletTransaction::create([
            'id_task'       =>  $this->order->id,
            'address'       =>  ($detail['address'] ?? null),
            'amount'        =>  ($detail['amount'] ?? null),
            'confirmations' =>  ($detail['confirmations'] ?? null),
            'txid'          =>  ($detail['txid'] ?? null),
        ]);

        // Записываем в лог мерчанта
        $event_value = 'Транзакция зафиксировано в блокчейне<br />';
        $event_value .= 'Сумма: <span style="color: green;">'.$response->amount.'</span><br />';
        $event_value .= 'Код валюты: <span style="color: green;">'.$this->transaction->getCodeIn()->name.'</span><br />';
        $event_value .= 'ID Транзакции: '.$response->txid;
        iex_order_merchant_log($this->order->id, 1, 2, $event_value, $provider);

        // Отправляем уведомление в Telegram канал
        if(config('telegram.enable') and (int)iEXSetting('is_notify_first_confirm_blockchain') == 1) {
            $delay = now()->addSeconds(30);
            dispatch(new TelegramConfirmBlockchainJob($this->order, $provider))->delay($delay)
                ->onQueue('low');
        }


        // Записываем в лог подтверждений
        if(isset($detail['confirmations'])) {
            $this->logConfirmation($detail['confirmations']);
        }
    }


    /**
     * Получаем AML провайдера
     *
     * @param $currency_in
     * @return string|null
     */
    protected function getAMLProviderIn($currency_in): ?string
    {
        $aml_service_class = null;

        if(Module::find('AMLServices')->isEnabled() == 1 and isset($currency_in->aml_service_model))
        {
            if($currency_in->aml_service_model->aml_type == 'getblock') {
                $aml_service_class = "\\App\\Common\\Packages\\Transaction\\Payments\\AMLCheckDeposit\\GETBlockAdapter";
            } elseif($currency_in->aml_service_model->aml_type == 'amlbot') {
                $aml_service_class = "\\App\\Common\\Packages\\Transaction\\Payments\\AMLCheckDeposit\\AMLBotAdapter";
            } elseif($currency_in->aml_service_model->aml_type == 'bitok') {
                $aml_service_class = "\\App\\Common\\Packages\\Transaction\\Payments\\AMLCheckDeposit\\BitokAdapter";
            }
        }

        return $aml_service_class;
    }


    public function getWalletAddress(): ?string
    {
        return $this->transaction->getWalletAddresses()->address;
    }

    /**
     * Лог подтверждений
     *
     * @param int $value
     */
    protected function logConfirmation(int $value = 0)
    {
        TaskLogConfirmation::create([
            'id_task'   =>  $this->order->id,
            'confirmation'  =>  $value
        ]);
    }

    public function getCreditAmount()
    {
        // Сумма, которая должна поступить на счет
        $credit_amount = $this->order->give_price;
        if($this->transaction->getMerchant()->credit_amount == 1) {
            $credit_amount = $this->order->give_price_with_comm_pay;
        } elseif($this->transaction->getMerchant()->credit_amount == 2) {
            $credit_amount = $this->order->give_price_default;
        }

        return $credit_amount;
    }
}
