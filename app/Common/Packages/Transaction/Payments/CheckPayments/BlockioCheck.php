<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;

class BlockioCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'BlockIo';


    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('BlockIo', $this->pay_id)->api();

        $tx_refs = $provider->findTransaction($this->transaction->getCodeIn()->name, $this->order->id);


        // Проверяем, есть ли транзакция
        if(!is_array($tx_refs) or empty($tx_refs)) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0) {
            $this->transaction->addEventRegisterTransaction($tx_refs['txid'], $tx_refs['confirmations'], $tx_refs['amounts_received'][0]['amount'], 'blockio');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['txid']) and !empty($tx_refs['txid']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'blockio',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['txid']
            ]);


            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['amounts_received'][0]['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['amounts_received'][0]['amount']
                ]);
            }
        }

        if($tx_refs['confirmations'] < $provider->getMinConfirm($this->transaction->getCodeIn()->name))
        {
            return [
                'status' => 1,
                'message' => 'Нехватает кол-во подтверждений для успешного выполнения'
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$tx_refs['amounts_received'][0]['amount'] < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($tx_refs['amounts_received'][0]['amount'], 'blockio');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$tx_refs['amounts_received'][0]['amount'] < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $tx_refs['amounts_received'][0]['amount']);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($tx_refs['amounts_received'][0]['amount'], 'blockio');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  ($tx_refs['address'] ?? null),
                'amount'        =>  (isset($tx_refs['amounts_received']) ? $tx_refs['amounts_received'][0]['amount'] : null),
                'confirmations' =>  ($tx_refs['confirmations'] ?? null),
                'txid'          =>  ($tx_refs['txid'] ?? null),
            ], 'blockio');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        } else {

            WalletTransaction::whereIdTask($this->order->id)->update([
                'confirmations' =>  $tx_refs['confirmations'],
            ]);
            // Записываем в лог подтверждений
            $this->logConfirmation($tx_refs['confirmations']);
        }

        return [
            'status' => 0
        ];
    }
}
