<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;

class CryptomusCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Cryptomus';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception|\Throwable
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('Cryptomus', $this->pay_id)->api();
        $getCache = cache()->get('merchant-cryptomus__id'.$this->order->id);

        // Если ID платежа не найден
        if(empty($getCache))
        {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Средства не поступили'
            ];
        }

        // Получаем детали транзакции по адресу с сервера
        $tx_refs = $provider->findTransaction($getCache['uuid']);


        if(!isset($tx_refs['result']))
        {
            return [
                'status' => 1,
                'message' => 'Транзакция не найдена'
            ];
        }

        // Перед обработкой проверяем, существует ли вообще транзакция по адресу
        if(!is_array($tx_refs['result']) or empty($tx_refs['result']))
        {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        // Получение баланса
        $balance = (float)$tx_refs['result']['amount'];


        // Если транзакция отменена
        if(in_array($tx_refs['result']['status'], ['cancel', 'fail', 'system_error']))
        {
            $this->transaction->failed();
            return [
                'status' => 1,
                'message' => 'Заявка отменена'
            ];
        }

        // Если транзакция отменена
        if($tx_refs['result']['status'] == 'check')
        {
            return [
                'status' => 1,
                'message' => 'Транзакция в ожидании'
            ];
        }

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0) {
            $this->transaction->addEventRegisterTransaction($tx_refs['result']['txid'], 1, $tx_refs['result']['amount'], 'cryptomus');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['result']['txid']) and !empty($tx_refs['result']['txid']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'cryptomus',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['result']['txid']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['result']['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['result']['amount']
                ]);
            }
        }



        if(empty($order->task_info->id_transaction_merchant) and array_key_exists('txid', $tx_refs['result']))
        {
            $this->order->task_info->update([
                'id_transaction_merchant' => $tx_refs['result']['txid']
            ]);
        }

        // Если транзакция отменена
        if($tx_refs['result']['status'] == 'process')
        {
            return [
                'status' => 1,
                'message' => 'Транзакция ожидает подтверждения, подождите...'
            ];
        }

        // Если транзакция прошла успешна
        if($tx_refs['result']['status'] == 'paid')
        {
            // Ставим дополнительное условие, которая приостановит автоматическую обработку,
            // в случае если он отправим меньше установленной минимальной цены
            if($this->transaction->getCheckAccurateBalance() and $balance < $this->transaction->getOrderMinAmount())
            {
                // Добавляем в событие
                $this->transaction->addEventForMinAmount($balance, 'cryptomus');
                $this->transaction->inFlowFunds();
                // Автоматически баним мошеннические заявки
                $this->transaction->autoBanClientForAutoPayment();

                return [
                    'status' => 1
                ];
            }

            // Дополнительное условие для баланса
            if($this->transaction->getCheckAccurateBalance() == true and (float)$balance < (float)$this->getCreditAmount())
            {
                // Автоматический пересчет по новой цене
                $this->transaction->recount(0, $balance);
                // Записываем в событие
                $this->transaction->addEventForMinOrder($balance, 'cryptomus');

            }

            $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
            if($wallet_tx == false)
            {
                // Записываем в лог
                $this->writePaymentTxHistory([
                    'amount'            =>  $tx_refs['result']['amount'],
                    'confirmations'     =>  1,
                    'txid'              =>  $tx_refs['result']['txid']
                ], 'cryptomus');

                if($this->transaction->getStatus() != 4)
                    $this->transaction->setStatus(7);
            }

            return [
                'status' => 0
            ];
        }

        return [
            'status' => 1,
            'event' => 'un_confirm'
        ];
    }
}
