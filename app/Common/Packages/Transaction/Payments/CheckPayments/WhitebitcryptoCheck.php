<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\AmlAnalysisLog;
use App\Models\EventReserve;
use App\Models\MerchantTransactionHash;
use App\Models\TaskSingleLogConfirm;
use App\Models\WalletTransaction;
use Illuminate\Support\Str;
use Nwidart\Modules\Facades\Module;

class WhitebitcryptoCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'WhiteBitCrypto';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception|\Throwable
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('WhiteBitCrypto', $this->pay_id)->api();
        $address = $this->transaction->getWalletAddresses();
        $address_id = (!empty($address->memo_id)) ?  $address->memo_id : $address->address;
        $asset_id = \Str::upper($this->transaction->getCodeIn()->name);

        // Получаем детали транзакции по адресу с сервера
        $tx_refs = $provider->findTransaction($asset_id, $address_id);

        // Перед обработкой проверяем, существует ли вообще транзакция по адресу
        if(!is_array($tx_refs) or empty($tx_refs))
        {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        if(\Str::lower($tx_refs['method']) != 1) {
            return [
                'status' => 1,
                'message' => 'Ошибка платежа'
            ];
        }


        $currency_in = $this->order->direction_exchange->currency1;
        $aml_provider = $this->getAMLProviderIn($currency_in);
        if (class_exists($aml_provider))
        {
            $response = new $aml_provider($this->order, $this->transaction);
            // Проверяем, существует ли транзакций
            if(isset($tx_refs['transactionHash']) and !empty($tx_refs['transactionHash']))
            {
                $aml_validator = $response->handleTx($address_id, $asset_id, $tx_refs['transactionHash']);
                if($aml_validator['status'] == 1) {
                    return [
                        'status' => 1,
                        'message' => $aml_validator['message']
                    ];
                }
            }
        }

        // Получаем данные по подтверждениям
        if(isset($tx_refs['confirmations']) and is_array($tx_refs['confirmations']) and isset($tx_refs['transactionHash']))
        {
            TaskSingleLogConfirm::updateOrCreate([
                'id_task'   => $this->order->id,
            ],[
                'id_task'   => $this->order->id,
                'needed_confirm' => $tx_refs['confirmations']['required'],
                'received_confirm' => $tx_refs['confirmations']['actual'] ?? 0
            ]);
        }

        // Получение баланса
        $balance = (float)$tx_refs['amount'];

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0)
        {
            $this->transaction->addEventRegisterTransaction($tx_refs['transactionHash'], 1, $tx_refs['amount'], 'whitebitcrypto');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['transactionHash']) and !empty($tx_refs['transactionHash']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'whitebitcrypto',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['transactionHash']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['amount']
                ]);
            }
        }

        if(empty($order->task_info->id_transaction_merchant) and array_key_exists('transactionHash', $tx_refs))
        {
            $this->order->task_info->update([
                'id_transaction_merchant' => $tx_refs['transactionHash']
            ]);
        }

        // Если транзакция отменена
        if(in_array($tx_refs['status'], [4, 9]))
        {
            $this->transaction->failed();
            return [
                'status' => 1,
                'message' => 'Заявка отменена'
            ];
        }

        // Текущий код валюты (в сети)
        $code_currency = $this->transaction->getCodeIn()->name;

        // Проверяем код валюты
        if(Str::upper($tx_refs['ticker']) != Str::upper($code_currency))
        {
            iex_order_check_pay_log($this->order->id, 1, 'Отправлена ошибочная транзакция, код валюты не соответствует', 'whitebitcrypto');
            $this->transaction->failed();
            $this->order->update(['is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Отправлена ошибочная транзакция, код валюты не соответствует'
            ];
        }

        $address = $this->transaction->getWalletAddresses();
        $address_id = $address->address;
        $asset_id = \Str::upper($this->transaction->getCodeIn()->name);

        $aml_provider = $this->getAMLProviderIn($currency_in);
        if (class_exists($aml_provider))
        {
            $response = new $aml_provider($this->order, $this->transaction);
            // Проверяем, существует ли транзакций
            if(isset($tx_refs['transactionHash']) and !empty($tx_refs['transactionHash']))
            {
                $aml_validator = $response->handleTx($address_id, $asset_id, $tx_refs['transactionHash']);
                if($aml_validator['status'] == 1) {
                    return [
                        'status' => 1,
                        'message' => $aml_validator['message']
                    ];
                }
            }
        }

        if(isset($tx_refs['confirmations']) and $tx_refs['confirmations']['actual'] < $tx_refs['confirmations']['required'])
        {
            return [
                'status' => 1,
                'message' => sprintf('Необходимо %s/%s подтверждений, для выполнения заявки.', $tx_refs['confirmations']['actual'], $tx_refs['confirmations']['required'])
            ];
        }

        // Если транзакция прошла успешна
        if(in_array($tx_refs['status'], [3, 7]))
        {
            // Ставим дополнительное условие, которая приостановит автоматическую обработку,
            // в случае если он отправит меньше установленной минимальной цены
            if($this->transaction->getCheckAccurateBalance() and $balance < $this->transaction->getOrderMinAmount())
            {
                // Добавляем в событие
                $this->transaction->addEventForMinAmount($balance, 'whitebitcrypto');
                $this->transaction->inFlowFunds();
                // Автоматически баним мошеннические заявки
                $this->transaction->autoBanClientForAutoPayment();

                return [
                    'status' => 1
                ];
            }

            // Дополнительное условие для баланса
            if($this->transaction->getCheckAccurateBalance() and (float)$balance < (float)$this->getCreditAmount())
            {
                // Автоматический пересчет по новой цене
                $this->transaction->recount(0, $balance);
                // Записываем в событие
                $this->transaction->addEventForMinOrder($balance, 'whitebitcrypto');
            }

            $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
            if(!$wallet_tx)
            {
                // Записываем в лог
                $this->writePaymentTxHistory([
                    'amount'            =>  $tx_refs['amount'],
                    'confirmations'     =>  isset($this->order->task_single_log_confirm) ? $this->order->task_single_log_confirm->received_confirm : 0,
                    'txid'              =>  $tx_refs['transactionHash']
                ], 'whitebitcrypto');

                if($this->transaction->getStatus() != 4)
                    $this->transaction->setStatus(7);
            }

            return [
                'status' => 0
            ];
        }

        return [
            'status' => 1,
            'event' => 'un_confirm'
        ];
    }
}
