<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionHash;
use App\Models\MerchantTransactionId;
use App\Models\WalletTransaction;
use Illuminate\Support\Str;

class CryptoswapCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Cryptoswap';


    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('CryptoSwap', $this->pay_id)->api();
        $getCache = cache()->get('merchant-callback__cryptoswap'.$this->order->id);
        $txId = $getCache['transaction_id'];


        $currency_in = $this->order->direction_exchange->currency1;

        // Текущий код валюты (в сети)
        $code_currency = $this->transaction->getCodeIn()->name;
        $network_code = $this->transaction->getCurrencyIn()->designation_xml;

        $tx_refs = $provider->findTransaction($txId)['bid'];


        // Проверяем, есть ли транзакция
        if(!is_array($tx_refs) or empty($tx_refs) or !isset($tx_refs['id'])) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        if(!in_array($tx_refs['status_id'], [19, 14, 16, 17, 15, 18]))
        {
            return [
                'status' => 1,
                'message' => 'Средства не поступили, подождите...'
            ];
        }

        $realpay_st = [19];
        $coldpay_st = [14, 16, 17, 15, 18];


        if(in_array($tx_refs['status_id'], $coldpay_st))
        {
            return [
                'status' => 1,
                'message' => 'Заявка обрабатывается....'
            ];
        }

        if(!in_array($tx_refs['status_id'], $realpay_st))
        {
            return [
                'status' => 1,
                'message' => 'Заказ еще находится в обработке...'
            ];
        }

        // Проверяем код валюты
        if(Str::upper($tx_refs['get_code']) != Str::upper($network_code))
        {
            $this->order->update(['is_bot' => 0]);
            return [
                'status' => 1,
                'message' => 'Некорректная валюта '
            ];
        }


        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$tx_refs['get_sum'] < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($tx_refs['get_sum'], 'cryptoswap');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$tx_refs['get_sum'] < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $tx_refs['get_sum']);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($tx_refs['get_sum'], 'cryptoswap');
        }

        if($this->transaction->getStatus() != 4)
            $this->transaction->setStatus(7);

        return [
            'status' => 0
        ];
    }
}
