<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\AmlAnalysisLog;
use App\Models\EventReserve;
use App\Models\MerchantTransactionHash;
use App\Models\TaskSingleLogConfirm;
use App\Models\WalletTransaction;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Modules\GetBlockBot\GetBlockBotFacade;
use Nwidart\Modules\Facades\Module;

class RpcCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Rpc';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $transaction_id = '№'.$this->order->id;

        $provider = PaymentFacade::merchant('Rpc', $this->pay_id)->api();
        $tx_refs = Arr::first($provider->listTransactions($transaction_id));

        // Проверяем, есть ли транзакция
        if(!is_array($tx_refs) or empty($tx_refs)) {
            return [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        if(\Str::lower($tx_refs['category']) != 'receive')
        {
            return [
                'status' => 1,
                'message' => 'Ошибка платежа'
            ];
        }

        $min_confirm = $this->transaction->getMerchant()->min_confirm;

        // Обновление счетчика подтверждений
        TaskSingleLogConfirm::updateOrCreate([
            'id_task'   => $this->order->id,
        ],[
            'id_task'   => $this->order->id,
            'needed_confirm' => $min_confirm,
            'received_confirm' => $tx_refs['confirmations'] ?? 0
        ]);

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0)
        {
            $tx_id = $tx_refs['txid'] ?? null;
            $confirm = $tx_refs['confirmations'] ?? 0;
            $amount = $tx_refs['amount'] ?? 0;

            // Добавляем в событие регистрацию в транзакции
            $this->transaction->addEventRegisterTransaction($tx_id, $confirm, $amount, 'rpc');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['txid']) and !empty($tx_refs['txid']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'rpc',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['txid']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['amount']
                ]);
            }
        }

        $address = $this->transaction->getWalletAddresses();
        $address_id = $address->address;
        $asset_id = \Str::upper($this->transaction->getCodeIn()->name);
        $currency_in = $this->order->direction_exchange->currency1;

        $aml_provider = $this->getAMLProviderIn($currency_in);
        if (class_exists($aml_provider))
        {
            $response = new $aml_provider($this->order, $this->transaction);
            // Проверяем, существует ли транзакций
            if(isset($tx_refs['txid']) and !empty($tx_refs['txid']))
            {
                $aml_validator = $response->handleTx($address_id, $asset_id, $tx_refs['txid']);
                if($aml_validator['status'] == 1) {
                    return [
                        'status' => 1,
                        'message' => $aml_validator['message']
                    ];
                }
            }
        }

        if($tx_refs['confirmations'] < $this->transaction->getMerchant()->min_confirm)
        {
            return [
                'status' => 1,
                'message' => sprintf('Необходимо %s/%s подтверждений, для выполнения заявки.', $tx_refs['confirmations'], $min_confirm)
            ];
        }


        // Получение баланса
        $balance = (float)$tx_refs['amount'] ?? 0;
        if($balance <= 0) {
            return [
                'status' => 1,
                'event' => 'empty_balance',
            ];
        }


        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$balance < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($balance, 'rpc');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() and (float)$balance < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $balance);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($balance, 'rpc');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory($tx_refs, 'rpc');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        } else {

            WalletTransaction::whereIdTask('id_task', $this->order->id)->update([
                'confirmations' =>  $tx_refs['confirmations'],
            ]);
            // Записываем в лог подтверждений
            $this->logConfirmation($tx_refs['confirmations']);
        }

        return [
            'status' => 0
        ];
    }
}
