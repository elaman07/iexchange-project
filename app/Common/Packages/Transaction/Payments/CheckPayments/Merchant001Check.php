<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionId;
use App\Models\WalletTransaction;

class Merchant001Check extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Merchant001';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('merchant001', $this->pay_id)->api();

        $wallet_id = MerchantTransactionId::where([
            'id_task' => $this->order->id,
            'provider' => 'merchant001'
        ])->first();

        // Если ID платежа не найден
        if(empty($wallet_id))
        {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Средства не поступили'
            ];
        }

        // Получаем детали транзакции по адресу с сервера
        $tx_refs = $provider->findTransaction($wallet_id->transaction_id);
        // Перед обработкой проверяем, существует ли вообще транзакция по адресу
        $validate_tx = (!empty($tx_refs) and $tx_refs['transaction']['invoiceId'] == $this->order->id);

        // Если не подтвержден входящий платеж, не пускаем дальше
        if(!$validate_tx)
        {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Транзакция не найдена'
            ];
        }

        // Получение баланса
        $balance = (float)$tx_refs['transaction']['pricing']['local']['amount'];

        // Фиксируем сумму, которую отправил клиент
        if(!empty($balance) and empty($this->order->in_amount_merchant))
        {
            $this->order->update([
                'in_amount_merchant' => $balance
            ]);
        }

        if(in_array($tx_refs['status'], ['CREATED', 'PENDING', 'IN_PROGRESS'])) {
            return [
                'status' => 1,
                'event' => 'not_completed',
                'message' => 'Ожидается поступление на счет'
            ];
        }

        if(in_array($tx_refs['status'], ['FAILED', 'EXPIRED', 'CANCELED']))
        {
            $this->transaction->failed();
            return [
                'status' => 1
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and $balance < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($balance, 'merchant001');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() and (float)$balance < (float)$this->getCreditAmount()) {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $balance);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($balance, 'merchant001');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if(!$wallet_tx) {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  null,
                'amount'        =>  ($balance ?? null),
                'confirmations' =>  0,
                'txid'          =>  null,
            ], 'aifory');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);
        }

        return [
            'status' => 0,
            'event' => 'competed_tx'
        ];
    }
}
