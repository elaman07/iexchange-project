<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\AmlAnalysisLog;
use App\Models\EventReserve;
use App\Models\HistoryPaymentTransaction;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;
use Illuminate\Support\Carbon;
use Nwidart\Modules\Facades\Module;

class WestwalletCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'WestWallet';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $provider = PaymentFacade::merchant('WestWallet', $this->pay_id)->api();

        // Текущий код валюты (в сети)
        $code_currency = $this->transaction->getCodeIn()->name;
        $network_code = $this->transaction->getCurrencyIn()->network_code;
        if(!empty($network_code)) {
            $code_currency = $network_code;
        }

        $wallet_id = $provider->findTransactions(\Str::upper($code_currency), $this->order->id);

//        $wallet_id = HistoryPaymentTransaction::where([
//            'id_task' => $this->order->id,
//            'payment' => 'westwallet'
//        ])->first();
//
        // Если ID платежа не найден
        if(empty($wallet_id) and !is_array($wallet_id))
        {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Средства не поступили'
            ];
        }

        // Получаем детали транзакции по адресу с сервера
        $tx_refs = $wallet_id;

        // Перед обработкой проверяем, существует ли вообще транзакция по адресу
        $validate_tx = !empty($tx_refs) and $tx_refs['label'] == $this->order->id and $tx_refs['type'] == 'receive';

        // Если не подтвержден входящий платеж, не пускаем дальше
        if(!$validate_tx) {
            return [
                'status' => 1,
                'event' => 'not_register',
                'message' => 'Транзакция не найдена в Блокчейне'
            ];
        }

        // Получение баланса
        $balance = (float)$tx_refs['amount'];

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0) {
            $this->transaction->addEventRegisterTransaction($tx_refs['blockchain_hash'], $tx_refs['blockchain_confirmations'], $tx_refs['amount'], 'westwallet');
            $this->order->update(['register_tx' => 1]);
        }

        $address = $this->transaction->getWalletAddresses();
        $address_id = $address->address;
        $asset_id = \Str::upper($this->transaction->getCodeIn()->name);
        $currency_in = $this->order->direction_exchange->currency1;


        $aml_provider = $this->getAMLProviderIn($currency_in);
        if (class_exists($aml_provider))
        {
            $response = new $aml_provider($this->order, $this->transaction);
            // Проверяем, существует ли транзакций
            if(isset($tx_refs['blockchain_hash']) and !empty($tx_refs['blockchain_hash']))
            {
                $aml_validator = $response->handleTx($address_id, $asset_id, $tx_refs['blockchain_hash']);
                if($aml_validator['status'] == 1) {
                    return [
                        'status' => 1,
                        'message' => $aml_validator['message']
                    ];
                }
            }
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['blockchain_hash']) and !empty($tx_refs['blockchain_hash']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'westwallet',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['blockchain_hash']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($balance) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $balance
                ]);
            }
        }

        // Если мы получаем не "completed" то не пускаем дальше
        if($tx_refs['status'] == 'pending')
        {
            return [
                'status' => 1,
                'event' => 'not_completed',
                'message' => 'Транзакция в найдено в Blockchain и находится в обработке'
            ];
        }

        // Если кол-во подтверждений в сети меньше чем установлено, не пускаем дальше
        // до тех пор, пока не выполнится это условие
        if($tx_refs['blockchain_confirmations'] < $this->transaction->getMerchant()->min_confirm)
        {
            return [
                'status' => 1,
                'event' => 'not_min_confirm',
                'message' => 'Недостаточно подтверждений'
            ];
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and $balance < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($balance, 'westwallet');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$balance < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $balance);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($balance, 'westwallet');
        }

        $wallet_tx = WalletTransaction::whereIdTask($this->order->id)->exists();
        if($wallet_tx == false)
        {
            // Записываем в лог
            $this->writePaymentTxHistory([
                'address'       =>  ($tx_refs['address'] ?? null),
                'amount'        =>  ($tx_refs['amount'] ?? null),
                'confirmations' =>  ($tx_refs['blockchain_confirmations'] ?? null),
                'txid'          =>  ($tx_refs['blockchain_hash'] ?? null),
            ], 'westwallet');

            if($this->transaction->getStatus() != 4)
                $this->transaction->setStatus(7);

        } else {
            WalletTransaction::whereIdTask($this->order->id)->update([
                'confirmations' =>  $tx_refs['blockchain_confirmations'],
            ]);

            // Записываем в лог подтверждений
            $this->logConfirmation($tx_refs['blockchain_confirmations']);
        }

        return [
            'status' => 0,
            'event' => 'competed_tx'
        ];
    }
}
