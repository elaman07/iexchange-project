<?php


namespace App\Common\Packages\Transaction\Payments\CheckPayments;


use App\Common\Packages\Payment\PaymentFacade;
use App\Models\MerchantTransactionHash;
use App\Models\WalletTransaction;
use Illuminate\Support\Arr;

class RippleCheck extends AbstractCheckPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Ripple';

    /**
     * Обработчик проверки оплаты
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        $transaction_id = $this->order->id;

        $provider = PaymentFacade::merchant('Ripple', $this->pay_id)->api();
        $balance = $provider->getBalance('XRP');

        $tx_refs = $provider->findTransaction($transaction_id);
        if(!is_array($tx_refs) or empty($tx_refs))
        {
            iex_order_check_pay_log($transaction_id, 1, 'Средства не поступили', 'ripple');
            return  [
                'status' => 1,
                'message' => 'Средства не поступили'
            ];
        }

        // Если на счету нехватает средств.
        if((float)$balance < 11)
        {
            return [
                'status' => 1,
                'message' => 'На балансе должно быть больше 10XRP'
            ];
        }

        // Если транзакция найдена, это значит что у нее уже есть подтверждение и далее
        // записываем в событие и регистрируем транзакцию
        if($this->order->register_tx == 0)
        {
            // Добавляем в событие регистрацию в транзакции
            $this->transaction->addEventRegisterTransaction($tx_refs['txid'], 1, $tx_refs['amount'], 'ripple');
            $this->order->update(['register_tx' => 1]);
        }

        // Добавляем blockchain hash
        if($this->order->register_tx == 1 and isset($tx_refs['txid']) and !empty($tx_refs['txid']))
        {
            MerchantTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'ripple',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $tx_refs['txid']
            ]);

            // Фиксируем сумму, которую отправил клиент
            if(!empty($tx_refs['amount']) and empty($this->order->in_amount_merchant)) {
                $this->order->update([
                    'in_amount_merchant' => $tx_refs['amount']
                ]);
            }
        }

        // Ставим дополнительное условие, которая приостановит автоматическую обработку,
        // в случае если он отправим меньше установленной минимальной цены
        if($this->transaction->getCheckAccurateBalance() and (float)$tx_refs['amount'] < $this->transaction->getOrderMinAmount())
        {
            // Добавляем в событие
            $this->transaction->addEventForMinAmount($tx_refs['amount'], 'ripple');
            $this->transaction->inFlowFunds();
            // Автоматически баним мошеннические заявки
            $this->transaction->autoBanClientForAutoPayment();

            return [
                'status' => 1
            ];
        }

        // Дополнительное условие для баланса
        if($this->transaction->getCheckAccurateBalance() == true and (float)$tx_refs['amount'] < (float)$this->getCreditAmount())
        {
            // Автоматический пересчет по новой цене
            $this->transaction->recount(0, $tx_refs['amount']);
            // Записываем в событие
            $this->transaction->addEventForMinOrder($tx_refs['amount'], 'ripple');
        }

        // Записываем в лог
        $this->writePaymentTxHistory($tx_refs, 'ripple');

        if($this->transaction->getStatus() != 4)
            $this->transaction->setStatus(7);


        return [
            'status' => 0
        ];
    }
}
