<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class AdgroupPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'AdGroup';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('adgroup', $this->pay_id)
            ->api();


        // Тип транзакции
        switch ($this->getGatewayPay()->method_pay)
        {
            // Выплата на карты
            case 1:
                $response = $facade->sendToCard($this->getAmount(), $this->getScore(), Str::upper($this->code));
                break;
            // Выплата на Yandex
            case 2:
                $response = $facade->sendToYandex($this->getAmount(), $this->getScore(), Str::upper($this->code));
                break;
            // Выплата на Webmoney
            case 3:
                $response = $facade->sendToWebMoney($this->getAmount(), $this->getScore(), Str::upper($this->code));
                break;
            // Выплаты на Номера телефонов
            case 4:
                $new_number = preg_replace('/^\+?([87])/', '7', $this->getScore());
                $to = str_replace(['(', ')', ' '], '', $new_number);
                $response = $facade->sendToMobile($this->getAmount(), $to, \mb_strtoupper($this->code), $this->getGatewayPay()->country_code);
                break;
            // Выплата на Qiwi счет
            default:
                $new_number = preg_replace('/^\+?([87])/', '7', $this->getScore());
                $to = str_replace(['(', ')', ' '], '', $new_number);
                $response = $facade->sendToAccount($this->getAmount(), $to, \mb_strtoupper($this->code));
        }

        if(isset($response['_id'])) {
            $this->addToHistory($response['_id']);
        }
    }
}
