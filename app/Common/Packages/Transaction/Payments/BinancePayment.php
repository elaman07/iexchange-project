<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class BinancePayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Binance';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Binance', $this->pay_id)
            ->api();

        $options = $this->getGatewayPay();
        // Валюта покупки
        $symbol = Str::upper(sprintf('%s%s', $this->getCode(), $options['exchange_buy_curr']));

        // Получить комиссию за вывод
        $withdraw_fee = 0;
        if($options['is_auto_take_fee'] == 1) {
            $withdraw_fee = $facade->withdrawFee($this->getCode())['withdrawFee'];
        }

        // Тип ордера
        $typeBuy = $options['exchange_buy_type'];

        // Докупить и отправить клиенту
        if($options['exchange_buy'] == 0) // Докупить
        {
            $balance = $facade->getBalance($this->getCode());
            $final_withdraw_balance = (float)$this->getAmount() + $withdraw_fee;


            // Проверяем, нужно ли докупать
            if($balance > $final_withdraw_balance)
            {
                $transfer = $facade->transfer(
                    $this->getCode(),
                    $this->getScore(),
                    $final_withdraw_balance,
                    !empty($this->order->outcome_unk) ? $this->order->outcome_unk : null);

                if(isset($transfer['id'])) {
                    $this->addToHistory($transfer['id']);
                }
            }
            // Проверяем, нужно ли докупать
            $amount_buy = (float)($this->getAmount() - $balance);

            // Снимаем комиссию
            if($options['exchange_fee'] > 0) {
                $amount_buy = $amount_buy + ($amount_buy * ($options['exchange_fee'] / 100));
            }

            // Если больше 0, то значит часть нужно докупить
            if($amount_buy > 0) {

                $finalAmount = ($amount_buy + $withdraw_fee);
                // Начинаем обрабатывать ордера
                if($typeBuy == 0) {
                    $create = $facade->marketBuy($symbol, $finalAmount);
                } elseif($typeBuy == 1) {
                    $create = $facade->buy($symbol, $finalAmount, 'LIMIT', 0, [
                        'timeInForce' => $options['time_in_force']
                    ]);
                }

                if(isset($create['status']) AND $create['status'] == 'FILLED')
                {
                    sleep(3);
                    $transfer = $facade->transfer(
                        $this->getCode(),
                        $this->getScore(),
                        $this->getAmount() + $withdraw_fee,
                        !empty($this->order->outcome_unk) ? $this->order->outcome_unk : null);

                    if(isset($transfer['id'])) {
                        $this->addToHistory($transfer['id']);
                    }
                }
            }

        } elseif($options['exchange_buy'] == 2) // Докупить и не выводить
        {
            // Проверяем, нужно ли докупать
            $amount_buy = $this->getAmount() - $facade->getBalance($this->getCode());
            // Снимаем комиссию
            if($options['exchange_fee'] > 0) {
                $amount_buy = $amount_buy + ($amount_buy * ($options['exchange_fee'] / 100));
            }

            // Если больше 0, то значит часть нужно докупить
            if($amount_buy > 0)
            {
                $finalAmount = ($amount_buy + $withdraw_fee);
                // Начинаем обрабатывать ордера
                if($typeBuy == 0) {
                    $create = $facade->marketBuy($symbol, $finalAmount);
                } elseif($typeBuy == 1) {
                    $create = $facade->buy($symbol, $finalAmount, 'LIMIT', 0, [
                        'timeInForce' => $options['time_in_force']
                    ]);
                }
            }

            throw new \Exception('Ордер на покупку выставлен, обновите страницу');

        } elseif($options['exchange_buy'] == 3) // Купить на всю сумму
        {
            $amount_buy = $this->getAmount();
            // Снимаем комиссию
            if($options['exchange_fee'] > 0) {
                $amount_buy = $amount_buy + ($amount_buy * ($options['exchange_fee'] / 100));
            }

            $finalAmount = ($amount_buy + $withdraw_fee);
            // Начинаем обрабатывать ордера
            if($typeBuy == 0) {
                $create = $facade->marketBuy($symbol, $finalAmount);
            } elseif($typeBuy == 1) {
                $create = $facade->buy($symbol, $finalAmount, 'LIMIT', 0, [
                    'timeInForce' => $options['time_in_force']
                ]);
            }

            if(isset($create['status']) AND $create['status'] == 'FILLED')
            {
                sleep(2);
                $transfer = $facade->transfer($this->getCode(), $this->getScore(), $this->getAmount() + $withdraw_fee, !empty($this->order->outcome_unk) ? $this->order->outcome_unk : null);
                if(isset($transfer['id'])) {
                    $this->addToHistory($transfer['id']);
                }
            }

            throw new \Exception('Ордер не создан');
        } else {  /// Не докупать
            $transfer = $facade->transfer($this->getCode(), $this->getScore(), $this->getAmount() + $withdraw_fee, !empty($this->order->outcome_unk) ? $this->order->outcome_unk : null);
            if(isset($transfer['id'])) {
                $this->addToHistory($transfer['id']);
            }
        }
    }
}
