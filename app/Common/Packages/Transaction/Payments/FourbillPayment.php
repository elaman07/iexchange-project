<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class FourbillPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'FourBill';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('fourbill', $this->pay_id)
            ->api();

        $response = $facade->transfer($this->getScore(), Str::upper($this->code), $this->getAmount(), $this->order->id, $this->getComment());
        if(isset($response['response']['id'])) {
            $this->addToHistory($response['response']['id']);
        }

    }
}
