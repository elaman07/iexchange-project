<?php
namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class BlockioPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'BlockIo';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 8;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if((int)iEXSetting('blockio_mass_payouts', 0) == 0)
        {
            $this->default_transfer();
        } else {
            if($this->order->direction_exchange->currency2->pay->is_mass_payouts == 0)
            {
                $this->default_transfer();
            } else {

                if(Str::lower($this->getCode()) == 'BTC')
                {
                    // Заявка отправлена в очередь
                    $this->order->update([
                        'queue_status' => 1
                    ]);
                } else {
                    $this->default_transfer();
                }
            }
        }

    }


    protected function default_transfer()
    {
        $facade = PaymentFacade::pay('BlockIo', $this->pay_id)
            ->api();


        $options = [
            'address' => $this->getScore(),
            'amount' => $this->getAmount(),
        ];

        $response = $facade->transfer($this->getCode(), $options, $this->order->direction_exchange->currency2->pay->priority_fee);

        if(isset($response['txid'])) {
            $this->addToHistory($response['txid']);
        }
    }
}
