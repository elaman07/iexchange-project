<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class AiforyPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Aifory';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Aifory', $this->pay_id)
            ->api();

        $option = [
            'currencyID' => (int)$this->getGatewayPay()->code_currency,
            'amount' => (float)round($this->amount, 0),
            'recipientTypeID' => (int)$this->getGatewayPay()->method_pay,
            'recipient' => $this->getScore(),
            'clientOrderID' => (string)$this->getComment(true),
            'webhookURL' => ''
        ];

        $response = $facade->transfer($option);

        if(isset($response['orderID']) and (float)$response['orderID'] > 0) {
            $this->addToHistory($response['orderID']);
        }
    }
}
