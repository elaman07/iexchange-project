<?php
namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\PayTransactionHash;

class WhitebitcryptoPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'WhiteBitCrypto';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 8;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $facade = PaymentFacade::pay('whitebitcrypto', $this->pay_id)
            ->api();

        $options = [
            'address'       => $this->getScore(),
            'amount'        => $this->amount,
            'currency'      => $this->getCode(),
            'order_id'      => $this->order->id,
        ];
        if(!empty($this->order->outcome_unk)) {
            $options['memo'] = $this->order->outcome_unk;
        }

        // Получаем код валюты из модуля "Автовыплаты"
        $network_code = $this->getDynamicCodeCurrencyWhitebit($this->order->direction_exchange->currency2);
        if(!empty($network_code)) {
            $options['network'] = $network_code;
        }

        $response = $facade->transfer($options);

        if(empty($response))
        {
            PayTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'whitebitcrypto',
                'id_currency' => $this->order->direction_exchange->id_currency2,
                'id_task' => $this->order->id,
                'api_transfer_id' => 'empty'
            ]);

            $this->order->task_info->update([
                'is_wait_hash_pay' => 1
            ]);
        }

    }
}
