<?php
namespace App\Common\Packages\Transaction\Payments;


use App\Common\Packages\Editor\EditorFacade;
use App\Common\Packages\Transaction\Contracts\PaymentContract;
use App\Common\Packages\Transaction\Transaction;
use App\Events\AutoPaymentSuccessful;
use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\GatewayPayment;
use App\Models\Task;
use App\Models\WalletsHistory;
use Illuminate\Support\Str;

/**
 * Абстрактный класс системы автовыплат
*/
abstract class AbstractPayment implements PaymentContract
{
    /**
     * Название Платежной система
     *
     * @var string
    */
    protected string $paymentName;

    /**
     * Детали транзакции
     *
     * @var \App\Models\Task
     */
    protected Task $order;

    /**
     * Детали
     *
     * @var Transaction
     */
    protected Transaction $transaction;

    /**
     * Сумма перевода
     *
     * @var string | float
     */
    protected $amount;

    /**
     * Код валюты
     *
     * @var string
     */
    protected string $code;

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Статус возврата
     *
     * @var boolean
    */
    protected bool $refund = false;


    protected string $pay_id;

    protected $gateway;

    /**
     * Конструктор
     *
     * @param Task $task
     * @param array $options
     * @throws \Exception
     */
    public function __construct(Transaction $transaction, array $options = [])
    {
        $this->transaction = $transaction;
        $this->order = $transaction->getTransaction();

        if(isset($options['refund'])) {
            $this->refund = true;
        }

        $gateway_payment = $this->order->direction_exchange->currency2->pay;
        $this->pay_id = $gateway_payment->id;
        $this->gateway = $gateway_payment;
        $this->decimal = $this->getCurrency()->number_format;

        if($this->refund == true) {
            $this->setAmount($this->order->give_price);
        } else {

            if((double)$this->order->out_price_fee > 0) {
                $this->setAmount($this->order->out_price_fee);
            } else {

                // Меняем сумму в зависимости от установленных настроек авто-выплат
                $price = match ($gateway_payment->pay_amount_type)
                {
                    0 => $this->order->receiving_price_with_comm_pay,
                    1 => $this->order->receiving_price_with_comm,
                    2 => $this->order->receiving_price_default,
                    default => $this->order->receiving_price_with_comm_pay
                };
                $this->setAmount($price);

            }

        }

        if($this->refund == true and empty($this->order->from_shot)) {
            throw new \Exception('Адрес для возврата не указан');
        }

        // Получение кода валюты
        if(array_key_exists('code', $options)) {
            $this->setCode($options['code']);
        }

        // Если в обработчике найден метод автовыплаты, выполняем ее сразу.
        if(method_exists($this,'handle'))
        {
            $this->handle();

            // Записываем данные в базу
            $volume_to_usd = convert_to_usd($this->getCode(), $this->amount);
            $gateway_payment->update([
                'order_count'       =>  $gateway_payment->order_count + 1,
                'volume_to_usd'     =>  $gateway_payment->volume_to_usd + $volume_to_usd,
                'last_order_id'     =>  $this->order->id
            ]);

            // Событие, после успешной автовыплаты
            if($this->refund == false) {
                event(new AutoPaymentSuccessful($this->order, $this->paymentName));
            }
        }
    }

    /**
     * Обработчик автовыплаты
     *
     * @return void
     */
    abstract public function handle();

    /**
     * Записываем сумму получения
     *
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Получаем сумму получения
     *
     * @return string|float
     */
    public function getAmount()
    {
        return iex_number_format($this->amount, $this->decimal);
    }

    /**
     * Получаем сумму получения для токенов
     *
     * @return string|float
     */
    public function getTokenAmount()
    {
        return $this->amount * $this->getTokenDecimal();
    }

    /**
     * Код валюты получателя
     *
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * Получаем код валюты
     *
     * @return string
    */
    public function getCode()
    {
        return Str::upper($this->code);
    }

    /**
     * Номер счета получателя
     *
     * @return string
     */
    public function getScore()
    {
        if($this->refund == true) {
            return remove_all_spaces($this->order->from_shot);
        }

        return remove_all_spaces($this->order->to_shot);
    }

    /**
     * Прикрепляем комментарий к транзакции
     *
     * @param bool $is_number
     * @return int|string
     */
    public function getComment(bool $is_number = false)
    {
        if($is_number == true)
            return $this->order->id;

        // В случае возврата
        if($this->refund == true) {
            return iEXContentLanguage('sitename').': Возврат по заявке №'.$this->order->id;
        }

        // Проверяем Примечание к платежу
        $comment = iEXContentLanguage('sitename').': Оплата по заявке №'.$this->order->id;
        if(isset($this->order->direction_exchange->currency2->pay) and !empty($this->order->direction_exchange->currency2->pay->comment)) {
             $comment = EditorFacade::driver('bbcode')->order($this->order->direction_exchange->currency2->pay->comment, $this->order);
        }

        return $comment;
    }

    /**
     * Детали направления обмена
     *
     * @return DirectionExchange
    */
    public function getDirectionExchange(): DirectionExchange
    {
        return $this->order->direction_exchange;
    }

    public function getOutPayment() {
        return $this->getDirectionExchange()->currency2->payment;
    }

    /**
     * Кто платит комиссию
     *
     * @return bool
    */
    public function whoPayCommission()
    {
        return $this->getDirectionExchange()->out_who_pay_commission;
    }

    /**
     * Получаем детали валюты
     *
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->getDirectionExchange()->currency2;
    }

    /**
     * Получаем название токена
     *
     * @return string | integer
    */
    public function getTokenValue()
    {
        return $this->getDirectionExchange()->currency2->code_currency->token_value;
    }

    /**
     * Получаем название токена
     *
     * @return string | integer
     */
    public function getTokenDecimal() {
        return $this->getDirectionExchange()->currency2->code_currency->token_decimal;
    }



    public function getGatewayPay(): GatewayPayment {
        return $this->order->direction_exchange->currency2->pay;
    }

    /**
     * Добавить в историю транзакции
     *
     * @param string $tx
     */
    protected function addToHistory(string $tx)
    {
        if($this->refund == false) {
            WalletsHistory::create([
                'id_task'   =>  $this->order->id,
                'txid'      =>  $tx
            ]);
        }
    }

    protected function hasHistory() {
        return WalletsHistory::whereIdTask($this->order->id)->exists();
    }

    /**
     * Получение номера банковской карты
     *
     * @param null $card_number
     * @return int|string
     */
    public function isValidateCard($card_number = null)
    {
        $types = [
            'electron' => '/^(4026|417500|4405|4508|4844|4913|4917)/',
            'interpayment' => '/^636/',
            'unionpay' => '/^(62|88)/',
            'discover' => '/^6(?:011|4|5)/',
            'maestro' => '/^(50|5[6-9]|6)/',
            'visa' => '/^4/',
            'mastercard' => '/^(5[1-5]|(?:222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720))/', // [2221-2720]
            'amex' => '/^3[47]/',
            'diners' => '/^3(?:0([0-5]|95)|[689])/',
            'jcb' => '/^(?:2131|1800|(?:352[89]|35[3-8][0-9]))/', // 3528-3589
            'mir' => '/^220[0-4]/',
        ];
        foreach($types as $type => $regexp) {
            if(preg_match($regexp, $card_number)) {
                return $type;
            }
        }

        return 'undefined';
    }

    public function getDynamicCodeCurrency(Currency $currency)
    {
        // Получаем код валюты из модуля "Автовыплаты"
        $pay_code = $currency->pay->code_currency;
        // Получаем код валюты из самих валют
        $currency_code = $currency->network_code_out;

        // Если пусты, оба значения, то берем стандартный код
        if(empty($pay_code) and empty($currency_code)) {
            return $this->getCode();
        }

        // Если код валюты из автовыплаты пуста, то берем данные по умолчанию
        if(empty($pay_code))
            return $currency_code;
        return $pay_code;
    }

    public function getDynamicCodeCurrencyWhitebit(Currency $currency)
    {
        // Получаем код валюты из модуля "Автовыплаты"
        $pay_code = $currency->pay->code_currency;
        // Получаем код валюты из самих валют
        $currency_code = $currency->network_code_out;

        // Если пусты, оба значения, то берем стандартный код
        if(empty($pay_code) and empty($currency_code)) {
            return '';
        }

        // Если код валюты из автовыплаты пуста, то берем данные по умолчанию
        if(empty($pay_code))
            return $currency_code;
        return $pay_code;
    }
}
