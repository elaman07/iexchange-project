<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;

class BinancechainPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'BinanceChain';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 6;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('BinanceChain', $this->pay_id)
            ->api();

        $response = $facade->transfer(
            $this->getScore(), (float)$this->getAmount(), $this->order->outcome_unk
        );

        if(isset($response['hash'])) {
            $this->addToHistory($response['hash']);
        }
    }
}
