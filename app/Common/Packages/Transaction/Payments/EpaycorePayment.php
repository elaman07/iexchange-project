<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class EpaycorePayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'EpayCore';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('epaycore', $this->pay_id)
            ->api();

        $response = $facade->sendMoney($this->getAmount(), Str::upper($this->code), Str::upper($this->getScore()), $this->getComment());


        if(!isset($response['batch']))
            throw new \Exception('Выплата не прошла');
        $this->addToHistory($response['batch']);

    }
}
