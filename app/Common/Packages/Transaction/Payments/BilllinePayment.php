<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;

class BilllinePayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Billline';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('billline', $this->pay_id)
            ->api();

        $response = $facade->transfer([
            'currency' => $this->getCode(),
            'amount' => $this->getAmount(),
            'account' => $this->getScore(),
            'payout_id' => $this->getComment(true)
        ]);

        if(isset($response['_id'])) {
            $this->addToHistory($response['_id']);
        }
    }
}
