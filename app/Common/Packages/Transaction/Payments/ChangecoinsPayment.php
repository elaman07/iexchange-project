<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class ChangecoinsPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Changecoins';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Changecoins', $this->pay_id)
            ->api();

        $options = [
            'address'       => $this->getScore(),
            'amount'        => $this->amount,
            'currency'      => $this->getCode(),
            'order_id'      => $this->getComment(true),
        ];
        if(!empty($this->order->outcome_unk)) {
            $options['tag'] = $this->order->outcome_unk;
        }

        $response = $facade->transfer($options);
        \Log::debug('asdasdsa'.json_encode($response));

    }
}
