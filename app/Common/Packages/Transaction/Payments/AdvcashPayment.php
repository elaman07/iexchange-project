<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class AdvcashPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'AdvCash';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('advcash', $this->pay_id)
            ->api();


        // Тип транзакции
        switch ($this->getGatewayPay()->method_pay)
        {
            // Выплата на карты
            case 1:
                $response = $facade->sendMoneyToBankCard($this->getAmount(), Str::upper($this->code), Str::upper($this->getScore()), $this->getComment());
                break;
            // Выплата на Email
            case 2:
                $response = $facade->sendMoneyToEmail($this->getAmount(), Str::upper($this->code), Str::upper($this->getScore()), $this->getComment());
                break;

            // Стандартная выплата
            default:
                $response = $facade->sendMoney($this->getAmount(), Str::upper($this->code), '', Str::upper($this->getScore()), $this->getComment(), true);
        }

        $this->addToHistory($response);

    }
}
