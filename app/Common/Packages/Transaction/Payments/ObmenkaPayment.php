<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class ObmenkaPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'AdvCash';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('obmenka', $this->pay_id)
            ->api();

        $code_currency = str_replace('RUB', 'RUR', Str::upper($this->getCode()));

        // Тип транзакции
        $score = Str::upper($this->getScore());
        switch ($this->getGatewayPay()->method_pay)
        {
            // ADVCash USD
            case 1:
                $type = 'advcash.usd';
                break;
            // Visa/Mastercard (RUB, UAH, KZT)
            case 2:
                $type = 'visamaster.'.$code_currency;
                break;
            // BITCOIN
            case 3:
                $type = 'bitcoin';
                break;
            case 4:
                $type = 'bitcoin_cash';
                break;
            case 5:
                $type = 'litecoin';
                break;
            case 6:
                $type = 'ethereum';
                break;
            case 7:
                $type = 'tether';
                break;
            case 8:
                $type = 'usdt_erc20';
                break;
            // Стандартная выплата
            default:
                $score = preg_replace('/^\+?([87])/', '7', $this->getScore());
                $type = 'qiwi';
        }


        $response = $facade->transfer(
            $score,
            $this->amount,
            $type,
            $this->order->id,
            $this->getComment()
        );

        if(isset($response['tracking'])) {
            $this->addToHistory($response['tracking']);
        }

    }
}
