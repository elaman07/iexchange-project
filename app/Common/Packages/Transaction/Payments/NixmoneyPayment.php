<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Exception;

class NixmoneyPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'NixMoney';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('nixmoney', $this->pay_id)
            ->api();


        $response = $facade->transfer([
            'amount'    =>  $this->amount,
            'currency'  =>  $this->getCode(),
            'to'        =>  $this->getScore(),
            'comment'   =>  $this->getComment()
        ]);

        if(isset($response['PAYMENT_BATCH_NUM'])) {
            $this->addToHistory($response['PAYMENT_BATCH_NUM']);
        }
    }
}
