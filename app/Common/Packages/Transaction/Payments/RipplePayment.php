<?php


namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\PayTransactionHash;

class RipplePayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Ripple';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 6;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Ripple', $this->pay_id)
            ->api();

        $options = [];
        if(!empty($this->order->outcome_unk)) {
            $options['destination_tag'] = (int)$this->order->outcome_unk;
        }

        $wallet = $facade->transfer($this->getScore(), $this->getAmount(), $options);
        // Если транзакция прошла успешна, добавляем в историю
        if(isset($wallet['hash']))
        {
            $this->addToHistory($wallet['hash']);

            PayTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'rpc',
                'id_currency' => $this->order->direction_exchange->id_currency1,
                'id_task' => $this->order->id,
                'transaction_hash' => $wallet['hash']
            ]);
        }
    }
}
