<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class FtxPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Ftx';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Ftx', $this->pay_id)
            ->api();


        $transfer = $facade->transfer($this->getCode(), $this->getAmount(), $this->getScore(), !empty($this->order->outcome_unk) ? $this->order->outcome_unk : null);

        \Log::debug('asdasdas'. \Graze\GuzzleHttp\JsonRpc\json_encode($transfer));

//        if(isset($transfer['id'])) {
//            $this->addToHistory($transfer['id']);
//        }
    }
}
