<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Jobs\HistoryCodeSendJob;
use App\Models\HistoryCode;

class KunacodesPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'KunaCodes';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $facade = PaymentFacade::pay('kunacodes', $this->pay_id)
            ->api();

        $response = $facade->createCode($this->getCode(), $this->amount, $this->getComment());

        if($response['id'] == 0) {
            throw new \Exception('Возникли проблемы при создании KunaCode');
        }

        if(!array_key_exists('code', $response)) {
            throw new \Exception('KunaCode не создан');
        }

        HistoryCode::create([
            'id_task'       =>  $this->order->id,
            'code'          =>  $response['code'],
            'amount'        =>  $response['amount'],
            'currency'      =>  $response['currency'],
            'provider_id'   =>  'kunacodes',
            'id_pay'        =>  $this->pay_id
        ]);

        // Отправляем ex-code на e-mail адрес клиента
        // Поставить в очередь за выполненение
        $delay = now()->addSeconds(30);
        dispatch(new HistoryCodeSendJob($this->order, $response['code']))
            ->delay($delay)->onQueue('low');
    }
}
