<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\PayTransactionHash;
use Illuminate\Support\Str;

class WestwalletPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'WestWallet';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('westwallet', $this->pay_id)
            ->api();

        $code_currency = $this->getDynamicCodeCurrency($this->order->direction_exchange->currency2);

        $options = [
            'address'       => $this->getScore(),
            'amount'        => $this->amount,
            'currency'      => $code_currency,
            'description'   => $this->getComment(),
        ];

        if(!empty($this->order->outcome_unk)) {
            $options['dest_tag'] = $this->order->outcome_unk;
        }

        $response = $facade->transfer($options);

        // Добавляем ID заявки выплаты, чтобы потом в консоле
        // проверять и вызвать запрос для получения транзакции
        if(isset($response['id']))
        {
            PayTransactionHash::updateOrCreate([
                'id_task' => $this->order->id
            ], [
                'provider' => 'westwallet',
                'id_currency' => $this->order->direction_exchange->id_currency2,
                'id_task' => $this->order->id,
                'api_transfer_id' => $response['id']
            ]);

            $this->order->task_info->update([
                'is_wait_hash_pay' => 1
            ]);
        }

    }
}
