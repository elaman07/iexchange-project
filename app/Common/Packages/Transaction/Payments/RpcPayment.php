<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\PayTransactionHash;
use Illuminate\Support\Str;

class RpcPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Rpc';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        \Log::debug('Процесс выплаты по заявке '.$this->order->id.'-----'.$this->pay_id);

        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('rpc', $this->pay_id)
            ->api();

        if(!empty($this->getComment())) {

        } else {

        }

        if(!empty($this->getCurrency()->is_valid_account and !wallet_validator($this->getCurrency()->is_valid_account, $this->getScore()))) {
            throw new \Exception('Ошибка автовыплаты: Адрес не прошел валидацию по валюте');
        }


        $response = $facade->transfer($this->getScore(), iex_number_format($this->amount, $this->getCurrency()->number_format), [
            'comment'   =>  $this->getComment(),
        ]);

        //Записываемм в историю
        if(!empty($response))
        {
            $commission = $facade->getTransaction($response);
            // Проверяем, найдено ли колонка для вычитывания комиссии
            if(isset($commission['fee'])) {
                $this->transaction->setRPCCommission($commission['fee']);
            }

            if(is_string($response)) {

                PayTransactionHash::updateOrCreate([
                    'id_task' => $this->order->id
                ], [
                    'provider' => 'rpc',
                    'id_currency' => $this->order->direction_exchange->id_currency1,
                    'id_task' => $this->order->id,
                    'transaction_hash' => $response
                ]);

                $this->addToHistory($response);
            }
        }
    }
}
