<?php
namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;

class CryptomusPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Cryptomus';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 8;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $facade = PaymentFacade::pay('cryptomus', $this->pay_id)
            ->api();

        $options = [
            'address'       => $this->getScore(),
            'amount'        => (string)$this->getAmount(),
            'currency'      => $this->getCode(),
            'order_id'      => (string)$this->order->id,
            'is_subtract'   => $this->gateway->is_subtract == 0 ? 1 : 0
        ];

        if(!empty($this->gateway->currency_code)) {
            $options['from_currency'] = \Str::upper($this->gateway->currency_code);
        }

        $network_code = $this->getDynamicCodeCurrencyWhitebit($this->order->direction_exchange->currency2);
        // Текущий код валюты (в сети)
        if(!empty($network_code)) {
            $options['network'] = $network_code;
        }

        $response = $facade->transfer($options);
    }
}
