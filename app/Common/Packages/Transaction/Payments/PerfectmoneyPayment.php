<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Jobs\eVoucherJob;
use App\Models\EVoucherCode;
use Exception;
use Illuminate\Support\Str;

class PerfectmoneyPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'PerfectMoney';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('PerfectMoney', $this->pay_id)
            ->api();

        // Тип транзакции
        switch ($this->getGatewayPay()->method_pay)
        {
            // Выплата на e-Voucher
            case 1:
                $response = $facade->transferVoucher([
                    'amount'    =>  $this->getAmount(),
                    'currency'  =>  Str::upper($this->code),
                ]);

                // Для e-Voucher
                if(isset($response['VOUCHER_NUM']))
                {
                    EVoucherCode::create([
                        'id_task'           =>  $this->order->id,
                        'account'           =>  $response['Payer_Account'],
                        'amount'            =>  $response['PAYMENT_AMOUNT'],
                        'batch_num'         =>  $response['PAYMENT_BATCH_NUM'],
                        'voucher_num'       =>  $response['VOUCHER_NUM'],
                        'voucher_code'      =>  $response['VOUCHER_CODE'],
                        'voucher_amount'    =>  $response['VOUCHER_AMOUNT']
                    ]);

                    // Отправляем e-voucher на e-mail адрес клиента
                    // Поставить в очередь за выполненение
                    $delay = now()->addSeconds(30);
                    dispatch(new eVoucherJob($this->order))
                        ->delay($delay)->onQueue('low');
                }
                break;

            // Стандартная выплата
            default:
                $response = $facade->transferAccount([
                    'amount'    =>  $this->getAmount(),
                    'currency'  =>  Str::upper($this->code),
                    'to'        =>  Str::upper($this->getScore()),
                    'comment'   =>  $this->getComment(),
                    'order_id'  =>  $this->order->id
                ]);
        }

        if(isset($response['PAYMENT_BATCH_NUM'])) {
            $this->addToHistory($response['PAYMENT_BATCH_NUM']);
        }
    }
}
