<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class PrivatbankPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Privatbank';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('PrivatBank', $this->pay_id)
            ->api();

        $account = mb_strtoupper($this->getScore());
        $response = $facade->transfer($this->order->id, $account, $this->getAmount(), $this->getComment(), mb_strtoupper($this->code));

    }
}
