<?php

namespace App\Common\Packages\Transaction\Payments;


use App\Models\InternalAccount;

class InternalaccountPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'InternalAccount';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int  $decimal = 8;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     */
    public function handle()
    {
        $first = InternalAccount::where([
            ['id_user', '=', $this->order->id_user],
            ['id_code_currency', '=', $this->getCurrency()->code_currency->id]
        ])->first();

        if(isset($first)) {
            $first->update([
                'balance' => (floatval($first->balance) + $this->order->receiving_price)
            ]);
        } else {
            $first = InternalAccount::create([
                'id_user' =>  $this->order->id_user,
                'id_code_currency' =>  $this->getCurrency()->code_currency->id,
                'balance' =>  $this->order->receiving_price
            ]);
        }

//        HistoryInternalAccount::create([
//            'id_task' => $this->transaction->id,
//            'id_user' => $this->transaction->id_user,
//            'type_history' => 1,
//            'description' => 'Счет клиента пополнен на '.$this->transaction->receiving_price.' '. $this->getCode(),
//            'amount' => $this->transaction->receiving_price
//        ]);

    }
}
