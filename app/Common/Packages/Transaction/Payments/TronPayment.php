<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;

class TronPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Tron';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 6;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Tron', $this->pay_id)
            ->api();

        $response = $facade->transfer(
            $this->getScore(), (float)$this->getAmount()
        );

        // Если транзакция прошла успешна, добавляем в историю
        if(isset($response['hash'])) {
            $this->addToHistory($response['hash']);
        }
    }
}
