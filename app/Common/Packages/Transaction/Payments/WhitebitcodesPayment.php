<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use App\Jobs\HistoryCodeSendJob;
use App\Models\HistoryCode;

class WhitebitcodesPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'WhiteBitCodes';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $facade = PaymentFacade::pay('whitebitcodes', $this->pay_id)
            ->api();

        $response = $facade->createCode($this->getCode(), $this->amount, 'Order:'.$this->order->id);

        HistoryCode::create([
            'id_task' => $this->order->id,
            'code' => $response['code'],
            'provider_id' => 'whitebitcodes',
            'id_pay' => $this->pay_id
        ]);

        // Отправляем ex-code на e-mail адрес клиента
        // Поставить в очередь за выполненение
        $delay = now()->addSeconds(30);
        dispatch(new HistoryCodeSendJob($this->order, $response['code']))
            ->delay($delay)->onQueue('low');
    }
}
