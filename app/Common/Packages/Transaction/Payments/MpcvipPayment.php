<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class MpcvipPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'MpcVip';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('MpcVip', $this->pay_id)
            ->api();


        // Тип транзакции
        switch ($this->getGatewayPay()->method_pay)
        {
            // Выплата на карты
            case 1:
                $response = $facade->sendToCard($this->getAmount(), $this->getScore(), Str::upper($this->code), $this->order);
                break;
            // Выплаты на Номера телефонов
            // Выплата на Qiwi счет
            default:
                $new_number = preg_replace('/^\+?([87])/', '7', $this->getScore());
                $to = str_replace(['(', ')', ' '], '', $new_number);
                $response = $facade->sendToAccount($this->getAmount(), $to, \mb_strtoupper($this->code), $this->order);
        }

        if(isset($response['externalID'])) {
            $this->addToHistory($response['externalID']);
        }
    }
}
