<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class PayonePayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Payone';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected int $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Payone', $this->pay_id)
            ->api();


        $dataPost = [
            'method' => "client/order/withdraw/create",
            'params' => [
                'client_tx_id' => (string)$this->getComment(true),
                'currency_uuid' => (string)$this->getGatewayPay()->method_pay,
                'pan' => $this->getScore(),
                'sum' => (float)$this->amount,
            ]
        ];

        $response = $facade->transfer($dataPost);
    }
}
