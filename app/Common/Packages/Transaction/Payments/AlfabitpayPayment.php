<?php

namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

class AlfabitpayPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'AlfaBitPay';

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('alfabitpay', $this->pay_id)
            ->api();

        $network_code = $this->order->direction_exchange->currency2->network_code;
        $code_currency = $this->getCode();

        // Текущий код валюты (в сети)
        if(!empty($network_code)) {
            $code_currency = $network_code;
        }

        $options = [
            'address'       => $this->getScore(),
            'amount'        => $this->amount,
            'currency'      => $code_currency,
            'description'   => (string)$this->order->id,
        ];

        if(!empty($this->order->outcome_unk)) {
            $options['dest_tag'] = $this->order->outcome_unk;
        }

        $response = $facade->transfer($options);
        if(isset($response['id'])) {
            $this->addToHistory($response['id']);
        }

    }
}
