<?php

namespace App\Common\Packages\Transaction\Payments\AMLCheckDeposit;

use App\Models\AmlAnalysisLog;
use App\Models\EventReserve;
use Illuminate\Support\Carbon;
use Modules\AMLServices\AMLServiceFacade;
use Modules\AMLServices\Entities\AMLService;
use Nwidart\Modules\Facades\Module;

class BitokAdapter extends AbstractAdapter
{

    public function handleTx($address, $asset, $tx = null): array
    {
        // Проверяем, включена ли проверка транзакций
        if($this->order->direction_exchange->currency1->is_in_aml_check_tx == 0) {
            return ['status' => 0];
        }

        $is_aml_check = ($this->order->task_info->is_aml_analysis == 0 and isset($tx) and !empty($tx));

        // Если не проходит по системе, пропускаем валидацию
        if(!$is_aml_check) {
            return [
                'status' => 0
            ];
        }


        $aml_services = AMLServiceFacade::make(
            $this->order->direction_exchange->currency1->aml_service,
            $this->order->direction_exchange->currency1->aml_service_model->aml_type
        );

        $aml_check = $aml_services->query('tx', [
            'client_id' => $this->order->id_user,
            'direction' => 'incoming',
            'asset' => $asset,
            'address' => preg_replace('/\s/', '', $address),
            'tx' => $tx,
        ]);

        $array = [];

        if($aml_check['tx_status'] == 'binding')
        {
            // Записываем в лог сумму
            $check_aml = AmlAnalysisLog::where('id_task', $this->order->id)->exists();
            if(!$check_aml and $this->order->direction_exchange->currency1->aml_check_cost > 0)
            {
                $this->order->direction_exchange->currency1->update([
                    'aml_analyses_count' =>  $this->order->direction_exchange->currency1->aml_analyses_count+1,
                    'aml_analyses_price' =>  floatval($this->order->direction_exchange->currency1->aml_analyses_count+$this->order->direction_exchange->currency1->aml_check_cost)
                ]);

                // Учитываем в резерве сумму проверки AML
                if($this->order->direction_exchange->currency1->is_aml_check_cost_reserve == 1)
                {
                    $money_aml = 1 / convert_to_usd($this->order->direction_exchange->currency1->code_currency->name, 1);
                    $new_money = (float)$this->order->direction_exchange->currency1->reserve->summa - ($this->order->direction_exchange->currency1->aml_check_cost * $money_aml);

                    // Записываем в лог резервов
                    $event = EventReserve::create([
                        'id_user'           =>  1,
                        'id_currency'       =>  $this->order->direction_exchange->currency1->id,
                        'id_reserve'        =>  $this->order->direction_exchange->currency1->reserve->id,
                        'type'              =>  0,
                        'text'              =>  'Резерв уменьшен с '.$this->order->direction_exchange->currency1->reserve->summa.' на '.$new_money,
                        'value_before'      =>  (float)$this->order->direction_exchange->currency1->reserve->summa,
                        'value_after'       =>  (float)$new_money,
                        'comment'           =>  'AML Check'
                    ]);
                }
            }

            $this->aml_log('bitok', [
                'tx_id'         => $tx,
                'address'       => $address,
                'code_name'     => $asset,
                'riskscore'     => $aml_check['risk_level'],
                'status'        =>  $aml_check['tx_status'],
                'event_type'    =>  'check_tx.pending',
                'event_message' =>  'Транзакция отправлена на AML-анализ. Пожалуйста, подождите...',
                'json_value'    =>  json_encode($aml_check),
                'price'         =>  (!$check_aml) ? $this->order->direction_exchange->currency1->aml_check_cost : 0
            ]);

            return [
                'status' => 1,
                'message' => 'Транзакция отправлена на AML-анализ. Пожалуйста, подождите...'
            ];
        }

        if($aml_check['tx_status'] == 'bound' and  isset($aml_check['check_state']) and $aml_check['check_state']['exposure'] == 'checked')
        {
            $this->aml_log('bitok', [
                'tx_id'         => $tx,
                'address'       => $address,
                'code_name'     => $asset,
                'riskscore'     => $aml_check['risk_level'],
                'status'        =>  $aml_check['tx_status'],
                'event_type'    => 'check_tx.success',
                'event_message' => 'AML-анализ успешно пройден',
                'json_value'    => json_encode($aml_check)
            ]);

            $this->order->task_info->update([
                'aml_result_value' => $aml_check['risk_level'],
                'is_aml_high_risk' =>  $aml_check['is_high_level'],
                'is_aml_analysis'   => 1
            ]);
        }


        return [
            'status' => 0
        ];
    }
}
