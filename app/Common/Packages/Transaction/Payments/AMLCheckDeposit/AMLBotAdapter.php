<?php

namespace App\Common\Packages\Transaction\Payments\AMLCheckDeposit;

use App\Models\AmlAnalysisLog;
use App\Models\EventReserve;
use Illuminate\Support\Carbon;
use Modules\AMLBot\AMLBotFacade;
use Nwidart\Modules\Facades\Module;

class AMLBotAdapter extends AbstractAdapter
{
    public function handleTx($address, $asset, $tx = null): array
    {
        // Проверяем, включена ли проверка транзакций
        if($this->order->direction_exchange->currency1->is_amlbot_tx_in == 0) {
            return ['status' => 0];
        }

        $is_aml_check = ($this->order->task_info->is_aml_analysis == 0 and isset($tx) and !empty($tx));
        // Если не проходит по системе, пропускаем валидацию
        if(!$is_aml_check) {
            return [
                'status' => 0
            ];
        }

        // Счетчик проверок (дневных)
        $aml_analytics_count = AmlAnalysisLog::where('status', 'SUCCESS')
            ->where('id_currency', $this->order->direction_exchange->id_currency1)
            ->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();

        $day_limit_aml = $this->order->direction_exchange->currency1->aml_day_limit_count;
        if($day_limit_aml > 0 and $aml_analytics_count > $day_limit_aml)
        {
            return [
                'status' => 0
            ];
        }

        $aml_check = $this->adapter($address, $asset, $tx);
        $array = [];

        // Если найдена ошибка
        if($aml_check['result'] == false)
        {
            return [
                'status' => 1,
                'message' => 'Invalid Token'
            ];
        }

        if(isset($aml_check['data']) and $aml_check['data']['status'] == 'pending')
        {
            // Записываем в лог сумму
            $check_aml = AmlAnalysisLog::where('id_task', $this->order->id)->exists();
            if(!$check_aml and $this->order->direction_exchange->currency1->aml_check_cost > 0)
            {
                $this->order->direction_exchange->currency1->update([
                    'aml_analyses_count' =>  $this->order->direction_exchange->currency1->aml_analyses_count+1,
                    'aml_analyses_price' =>  floatval($this->order->direction_exchange->currency1->aml_analyses_count+$this->order->direction_exchange->currency1->aml_check_cost)
                ]);

                // Учитываем в резерве сумму проверки AML
                if($this->order->direction_exchange->currency1->is_aml_check_cost_reserve == 1)
                {
                    $money_aml = 1 / convert_to_usd($this->order->direction_exchange->currency1->code_currency->name, 1);
                    $new_money = (float)$this->order->direction_exchange->currency1->reserve->summa - ($this->order->direction_exchange->currency1->aml_check_cost * $money_aml);

                    // Записываем в лог резервов
                    $event = EventReserve::create([
                        'id_user'           =>  1,
                        'id_currency'       =>  $this->order->direction_exchange->currency1->id,
                        'id_reserve'        =>  $this->order->direction_exchange->currency1->reserve->id,
                        'type'              =>  0,
                        'text'              =>  'Резерв уменьшен с '.$this->order->direction_exchange->currency1->reserve->summa.' на '.$new_money,
                        'value_before'      =>  (float)$this->order->direction_exchange->currency1->reserve->summa,
                        'value_after'       =>  (float)$new_money,
                        'comment'           =>  'AML Check'
                    ]);
                }
            }

            $this->aml_log('amlbot', [
                'tx_id'         => $tx,
                'address'       => $address,
                'code_name'     => $asset,
                'riskscore'     => '0',
                'status'        =>  $aml_check['data']['status'],
                'event_type'    =>  'check_tx.pending',
                'event_message' =>  'Транзакция отправлена на AML-анализ. Пожалуйста, подождите...',
                'json_value'    =>  json_encode($aml_check),
                'price'         =>  (!$check_aml) ? $this->order->direction_exchange->currency1->aml_check_cost : 0
            ]);

            return [
                'status' => 1,
                'message' => 'Транзакция отправлена на AML-анализ. Пожалуйста, подождите...'
            ];
        }

        if(isset($aml_check['data']) and $aml_check['data']['status'] == 'error')
        {
            $this->aml_log('amlbot', [
                'tx_id'         => $tx,
                'address'       => $address,
                'code_name'     => $asset,
                'riskscore'     => '0',
                'status'        =>  $aml_check['data']['status'],
                'event_type'    => 'check_tx.error',
                'event_message' => 'AML-анализ не смог проверить транзакцию. Попробуйте вручную...',
                'json_value'    => json_encode($aml_check)
            ]);

            return [
                'status' => 1,
                'message' => 'AML-анализ не смог проверить транзакцию. Попробуйте вручную...'
            ];
        }

        if(isset($aml_check['data']) and $aml_check['data']['status'] == 'success')
        {
            $this->aml_log('getblock', [
                'tx_id'         => $tx,
                'address'       => $address,
                'code_name'     => $asset,
                'riskscore'     => (float)$aml_check['data']['riskscore']*100,
                'status'        =>  $aml_check['result']['check']['status'],
                'event_type'    => 'check_tx.success',
                'event_message' => 'AML-анализ успешно пройден',
                'json_value'    => json_encode($aml_check)
            ]);

            $this->order->task_info->update([
                'aml_riskscore' => (float)$aml_check['data']['riskscore']*100,
                'is_aml_analysis'   => 1
            ]);
        }

        return [
            'status' => 0
        ];

    }

    protected function adapter(string $address, string $asset, string $tx)
    {
        // Проверяем, включен ли сам модуль
        if(Module::find('AMLBot')->isEnabled() == 0)
            return false;

        $account = preg_replace('/\s/', '', $address);
        $response = AMLBotFacade::txVerification($account, $asset, $tx);
        return $response;
    }
}
