<?php

namespace App\Common\Packages\Transaction\Payments\AMLCheckDeposit;

use App\Common\Packages\Transaction\Transaction;
use App\Models\AmlAnalysisLog;
use App\Models\Task;
use Illuminate\Support\Carbon;

abstract class AbstractAdapter
{
    /**
     * @var Task
    */
    protected Task $order;


    public function __construct(Task $task, Transaction $transaction)
    {
        $this->order = $task;
    }


    public function getAnalyticsCountByCurrency()
    {
        AmlAnalysisLog::where('status', 'SUCCESS')->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])->count();
    }

    public function aml_log($adapter, $array = []): void
    {
        AmlAnalysisLog::create([
            'id_currency' => $this->order->direction_exchange->id_currency1,
            'id_task' => $this->order->id,
            'provider_name' => $adapter,
            'tx_id' => $array['tx_id'],
            'address' => $array['address'],
            'code_name' => $array['code_name'],
            'riskscore' => $array['riskscore'],
            'status' => $array['status'],
            'event_type' => $array['event_type'],
            'json_value' => $array['json_value'],
            'event_message' => $array['event_message'],
            'price' => $array['price'] ?? 0
        ]);
    }
}
