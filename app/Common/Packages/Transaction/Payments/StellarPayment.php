<?php


namespace App\Common\Packages\Transaction\Payments;

use App\Common\Packages\Payment\PaymentFacade;

class StellarPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected string $paymentName = 'Stellar';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected int $decimal = 8;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if($this->hasHistory()) {
            throw new \Exception('Ошибка автовыплаты: Запрещено отправлять несколько раз');
        }

        $facade = PaymentFacade::pay('Stellar', $this->pay_id)
            ->api();

        $options = [];
        if(!empty($this->order->outcome_unk)) {
            $options['memo_id'] = (int)$this->order->outcome_unk;
        }

        $wallet = $facade->transfer($this->getScore(), $this->amount, $options);

        $rawData = $wallet->getRawData();
        // Если транзакция прошла успешна, добавляем в историю
        if(isset($rawData['hash'])) {
            $this->addToHistory($rawData['hash']);
        }
    }
}
