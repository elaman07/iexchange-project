<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Events\OrderStatusesEvent;
use App\Models\PendingOrderStatus;

trait ManagersDefer
{
    /**
     * Тип отложения
     *
     * @var PendingOrderStatus
    */
    private $pending_status;

    /**
     * Указываем причину отложения заявки
     *
     * @param int $id
     * @return ManagersDefer
     */
    public function setDeferType($id = 1)
    {
        $this->pending_status = PendingOrderStatus::find($id);
        return $this;
    }

    /**
     * Получение данных по отложенной заявке
     *
     * @return PendingOrderStatus
    */
    public function getPendingStatus()
    {
        return $this->pending_status;
    }

    /**
     * Если заявку необходимо временно заморозить
     */
    public function defer()
    {
        if($this->transaction->is_reserve == 0) {
            $this->changeReserves(true);
        }

        $this->setOperator(0);
        $this->setStatus(8);

        // Уведомление в реальном времени
        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new OrderStatusesEvent($this->transaction));
        }

        if(isset($this->pending_status))
        {
            $this->transaction->update([
                'id_pending_status' => $this->pending_status->id
            ]);
            $this->notification();
        }
    }
}