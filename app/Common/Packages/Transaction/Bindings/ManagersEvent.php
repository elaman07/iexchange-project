<?php
namespace App\Common\Packages\Transaction\Bindings;


trait ManagersEvent
{
    /**
     * Лог событий мерчанта
     *
     * @param string $name
     * @param string $alias
     * @param string|null $ip_address
     * @return ManagersEvent
     */
    public function addEventMerchantLog(string $name, string $alias, string $ip_address = null, $options = [])
    {
        switch ($name)
        {
            case 'security_hash':
                iex_order_merchant_log($this->transaction->id, 1, 2, 'Секретный хэш мерчанта не соответствует указанной', $alias);
                break;

            case 'allow_ip_address':
                iex_order_merchant_log($this->transaction->id, 1, 2, 'В списке разрешенных IP, не найден '.$ip_address, $alias);
                break;

            case 'merchant_log_checked':
                iex_order_merchant_log($this->transaction->id, 1, 4, 'Внезапно изменился провайдер мерчанта', $alias);
                break;

            case 'provider_alias':
                iex_order_merchant_log($this->transaction->id, 1, 4, 'Оплачено с другого провайдера, актуальный '.$this->getMerchant()->gateway->alias, $alias);
                break;

            case 'check_currency':
                iex_order_merchant_log($this->transaction->id, 1, 4, 'Оплачено другим кодом валюты', $alias);
                break;

            case 'verify_wallet':
                iex_order_merchant_log($this->transaction->id, 1, 2, 'Указанный номер счета отличается от номера отправителя', $alias);
                break;

            case 'check_local_black_list':
                iex_order_merchant_log($this->transaction->id, 1, 2, 'Номер отправителя находится в черном списке', $alias);
                break;

            case 'is_disable_payment_server':
                iex_order_merchant_log($this->transaction->id, 1, 2, 'Транзакция не найдена в базе', $alias);
                break;

            case 'verify_income_balance':
                // Записываем в лог мерчанта
                $event_value = 'Оплата не в полном объеме<br />';
                $event_value .= 'Необходимая сумма: <span style="color: green;">' . $this->transaction->give_price . '</span><br />';
                $event_value .= 'Получено: <span style="color: red;">' . $options['amount'] . '</span><br />';
                $event_value .= 'Код валюты: <span style="color: green;">' . $options['currency'] . '</span><br />';
                $event_value .= 'ID Транзакции: ' . $options['id'];
                iex_order_merchant_log($this->transaction->id, 1, 2, $event_value, $alias);
                break;

            case 'income_balance_done':
                // Записываем в лог мерчанта
                $event_value = 'Оплата успешно произведена<br />';
                $event_value .= 'Сумма: <span style="color: green;">'. $options['amount'].'</span><br />';
                $event_value .= 'Код валюты: <span style="color: green;">'.$options['currency'].'</span><br />';
                $event_value .= 'ID Транзакции: '.$options['id'];
                iex_order_merchant_log($this->transaction->id, 1, 2, $event_value, $alias);
                break;

            default;
                break;
        }

        return $this;
    }


    /**
     * Событие в случае регистрации транзакции в сети
     *
     * @param $tx_hash
     * @param $confirm
     * @param $amount
     * @param $alias
     */
    public function addEventRegisterTransaction($tx_hash, $confirm, $amount, $alias)
    {
        $event_value = 'Транзакция зафиксировано в Blockchain<br />';
        $event_value .= 'ID Транзакции: '.$tx_hash.'<br />';
        $event_value .= 'Кол. подтверждений: '.$confirm.'<br />';
        $event_value .= 'Получено: <span style="color: green">'.$amount.'</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 0, $event_value, mb_strtolower($alias));
    }

    /**
     * Событие в случае если сумма не соотвествует указанному
     *
     * @param $balance
     * @param $alias
     */
    public function addEventForMinOrder($balance, $alias)
    {
        $event_value = 'Отправленная сумма не соотвествует указанному<br />';
        $event_value .= 'Полученная сумма: <span style="color: red">'.$balance.'</span><br />';
        $event_value .= 'Указано в заявке: <span style="color: springgreen">'.$this->getCreditAmount().'</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 1, $event_value, mb_strtolower($alias));
    }

    /**
     * События в случае если клиент отправил меньше установленной цены
     *
     * @param $balance
     * @param $alias
     */
    public function addEventForMinAmount($balance, $alias)
    {
        $event_value = 'Клиент отправил меньше установленной минимальной цены<br />';
        $event_value .= 'Полученная сумма: <span style="color: red">' . $balance . '</span><br />';
        $event_value .= 'Указано в заявке: <span style="color: springgreen">' . $this->getCreditAmount() . '</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 1, $event_value, mb_strtolower($alias));
    }

    /**
     * В случае если клиента отправил средств на другой токен
     *
     * @param $balance
     * @param $alias
     * @param $token
     */
    public function addEventForInvalidToken($balance, $alias, $token)
    {
        $event_value = 'Клиент отправил средства на другой токена<br />';
        $event_value .= 'Полученная сумма: <span style="color: red">' . $balance . ' '.$token.'</span><br />';
        $event_value .= 'Указано в заявке: <span style="color: springgreen">' . $this->getCreditAmount() . ' ' . $this->getCodeIn()->token_value . '</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 1, $event_value, mb_strtolower($alias));
    }

    /**
     * Событие в случае если клиент отправил неправильную валюту
     *
     * @param $amount
     * @param $currency
     */
    public function addEventIncorrectCode($amount, $currency)
    {
        $event_value = 'Клиент создал EXCode в другой валюте<br />';
        $event_value .= 'Полученная сумма: <span style="color: red">'.$amount.' '.$currency.'</span><br />';
        $event_value .= 'Указано в заявке: <span style="color: springgreen">'.$this->getCreditAmount().' '.$this->getCodeIn()->name.'</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 1, $event_value, 'exmo');
    }

    /**
     * Событие в случае успешного получения кода
     *
     * @param $amount
     * @param $currency
     */
    public function addEventExmoCode($amount, $currency)
    {
        $event_value = 'EXMO Код получен<br />';
        $event_value .= 'Сумма: '.$amount.'<br />';
        $event_value .= 'Валюта: <span style="color: green">'.$currency.'</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 0, $event_value, 'exmo');
    }


    /**
     * Событие в случае успешного получения кода
     *
     * @param $amount
     * @param $currency
     * @param array $provider
     */
    public function addEventOtherCode($amount, $currency, $provider = [])
    {
        $event_value = $provider['name'].' получен<br />';
        $event_value .= 'Сумма: '.$amount.'<br />';
        $event_value .= 'Валюта: <span style="color: green">'.$currency.'</span>';

        // Записываем событие в лог
        iex_order_check_pay_log($this->transaction->id, 0, $event_value,  $provider['alias']);
    }
}
