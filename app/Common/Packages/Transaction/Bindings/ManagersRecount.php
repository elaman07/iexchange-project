<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 28.11.2020
 * Time: 11:01
 */

namespace App\Common\Packages\Transaction\Bindings;


use App\Common\Packages\Converter\Facades\ConvertFacade;
use Illuminate\Support\Carbon;

trait ManagersRecount
{
    /**
     * Общий пересчет заявок
     * @throws \Exception
     */
    public function generalRecountOrder()
    {
        // Если в валютах включен стандартный пересчет, то пересчитываем
        if((int)iEXSetting('currency_is_recount_default') == 1) {
            $this->recount(1, $this->getAmountIn());
        }

        // Пересчет курс, если условие больше >
        if((int)iEXSetting('currency_recount_percent', 0) > 0)
        {
            $new_rates = ConvertFacade::call($this->getDirectionExchange())->all();
            $percent_diff = 0;
            try {
                $percent_diff = iex_number_format((
                        $this->getDisplayOutPrice(true) / ($new_rates['amount'] * $this->getDisplayInPrice(true)) - 1
                    ) * 100, 2);
            }catch (\Exception $exception) {
                //
            }

            // Пересчитываем, если курс больше чем указано в заявках
            if($percent_diff > (int)iEXSetting('currency_recount_percent', 0)) {
                $this->recount(1, $this->getAmountIn());
            }
        }

        // Пересчет после того как пройдет установленная время
        if(!empty(iEXSetting('currency_recount_time_minutes')) or !empty(iEXSetting('currency_recount_time_hours')))
        {
            $created = $this->transaction->created_at->addMinutes(iEXSetting('currency_recount_time_minutes'))->addHours(iEXSetting('currency_recount_time_hours'));
            if(Carbon::now()->gt($created)) {
                $this->recount(1, $this->getAmountIn());
            }
        }
    }

    /**
     * Индивидуальный пересчет заявок
    */
    public function individualRecountOrder()
    {
        $currency = $this->getCurrencyOut();

        // Если в валютах включен стандартный пересчет, то пересчитываем
        if((int)$currency->is_enable_auto_recount_order == 1) {
            $this->recount(1, $this->getAmountIn());
        }

        // Пересчет курс, если условие больше >
        if((int)$currency->unique_recount_percent > 0)
        {
            $new_rates = ConvertFacade::call($this->getDirectionExchange())->all();
            $percent_diff = 0;
            try {
                $percent_diff = iex_number_format((
                        $this->getDisplayOutPrice(true) / ($new_rates['amount'] * $this->getDisplayInPrice(true)) - 1
                    ) * 100, 2);
            }catch (\Exception $exception) {
                //
            }

            // Пересчитываем, если курс больше чем указано в заявках
            if($percent_diff > (int)$currency->unique_recount_percent) {
                $this->recount(1, $this->getAmountIn());
            }
        }

        // Пересчет после того как пройдет установленная время
        if($currency->recount_time_hours > 0 or $currency->recount_time_minutes > 0) {
            $created = $this->transaction->created_at->addMinutes($currency->recount_time_minutes)->addHours($currency->recount_time_hours);
            if(Carbon::now()->gt($created)) {
                $this->recount(1, $this->getAmountIn());
            }
        }
    }

}
