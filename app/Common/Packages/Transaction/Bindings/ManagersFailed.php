<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Events\OrderStatusesEvent;
use App\Models\BannedUser;
use App\Models\TaskStatusLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait ManagersFailed
{
    public $failedOptions = [
        'fixed_manager' => true
    ];

    /**
     * Если заявка была неоплаченно то удаляем ее
     *
     * @param array $options
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function failed(array $options = [])
    {
        if(!empty($options) and is_array($options)) {
            $this->failedOptions = $options;
        }

        if($this->getStatus() == 5) {
            throw new \Exception('Транзакция уже была отклонена');
        }

        if(array_key_exists('allow_user_ban', $options) and $options['allow_user_ban'] == true) {
            // Если данные не подходят, отправляем в бан
            add_user_ban($this->getClient()->id, null, 'Violation of the rules of payment through a merchant');
        }

        // Авто-возврат (Только в случае оплаченной заявки)
        if(iEXSetting('is_order_auto_refund'))
        {
            $merchant_check = $this->getMerchant();
            if($this->getCategoryReject() == 2 and $this->getStatus() == 7 and isset($merchant_check))
            {
                $payment = Str::studly($this->getMerchant()->alias);
                $class = "\\App\\Common\\Packages\\Transaction\\Payments\\{$payment}Payment";
                // Проверяем, существует ли класс выплаты
                if(class_exists($class)) {
                    \Log::info(sprintf('-- Возврат по заявке %s прошла успешно', $this->transaction->id));
                    return new $class($this->transaction, [
                        'code' => $this->getCodeIn()->name,
                        'refund' => true
                    ]);
                }
            }
        }

        $this->changeReserves(true);
        $this->setStatus(5);
        $this->addErrorLogToSignOrder(0,null);

        // Уведомление в реальном времени
        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new OrderStatusesEvent($this->transaction));
        }

        //В случае если заявка мошенническая
        if($this->getCategoryReject() == 4) {
            // Если включено, то данные клиента заносятся в черный список
            if(iEXSetting('allow_order_blacklist')) {
                $this->clientToBlackList();
            }


            // Забанить клиента
            if(config('admin.ban_cheater_user') == true) {
                $this->clientBan([
                    'comment' => sprintf('Заявка №%s признана мошеннической', $this->transaction->id)
                ]);
            }
        }

        // Если заявка создана из Telegram, клиенту отправляем сообщение
        if(!empty($this->transaction->telegram_id))
        {
            $telegram_message = view('_parent.bot.website.failed_order', ['order' => $this->transaction])->render();

            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => '🔔 Поддержка', 'url' => url('/contacts')])
                );

            Telegram::sendMessage([
                'chat_id'           => $this->transaction->telegram_id,
                'text'              => $telegram_message,
                'parse_mode'        => 'HTML',
                'reply_markup' => $keyboard,
            ]);
        }

        $this->notification();
    }

    public function failedInvalidBid()
    {
        $this->changeReserves(true);
        $this->setStatus(10);

        $this->notification();
    }

    /**
     * Отклоняем заявку не затрагивая резервы
    */
    public function failedNotReserves()
    {
        $this->setStatus(5);
        $this->notification();
    }

    public function hasFailedOptionsName(string $name, bool $boolean)
    {
        // isset($this->failedOptions['fixed_manager']) and $this->failedOptions['fixed_manager'] == true
        return isset($this->failedOptions[$name]) and $this->failedOptions[$name] == $boolean;
    }
}
