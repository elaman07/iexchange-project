<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Models\Employees;
use App\Models\EventEmployees;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

trait ManagersEmployees
{
    /**
     * Начиляем бонусы операторам
    */
    protected function employeesBonus()
    {
        $detail = Employees::where('id_user', Auth::id());

        if($detail->exists()) {
            $item = $detail->first();

            if($item->percent > 0 or $item->bonus_rub > 0)
            {
                $bonus = 0;
                if($item->percent > 0) {
                    $bonus = floor($this->getAmountConvertIn()) * ($item->percent / 100);
                }

                EventEmployees::create([
                    'id_task'       =>  $this->transaction->id,
                    'id_employees'  =>  $item->id,
                    'type'          =>  1,
                    'amount'        =>  $bonus + $item->bonus_rub
                ]);
            }
        }
    }
}