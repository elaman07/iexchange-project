<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Events\OrderChatMessagesEvent;
use App\Events\OrderChatNotificationEvent;
use App\Jobs\OrderChatJob;
use App\Jobs\SendClientCheckJob;
use App\Models\ApplicationStepLog;
use App\Models\HistoryRecalculation;
use App\Models\OperationLevel;
use App\Models\RequisiteField;
use App\Models\ReserveLog;
use App\Models\Task;
use App\Models\TaskChat;
use App\Models\TaskHistoryOperator;
use App\Models\TaskMessage;
use App\Models\TaskShot;
use App\Models\VerificationCard;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpFoundation\IpUtils;

trait ManagersDetails
{
    /**
     * Изменить этап заявки
    */
    public function changeOrderStep(int $type)
    {
        // Записываем в лог этапы для заявок
        ApplicationStepLog::create([
           'id_step' => $type,
           'id_manager' => \auth()->id(),
           'id_task' => $this->transaction->id,
           'id_order_status' => $this->transaction->status
        ]);

        $this->transaction->update([
            'id_order_step' => $type
        ]);
    }


    /**
     * Дополнительная защита для мерчанта (Ограниченные IP Адреса)
     *
     * @param string $ip
     * @return bool
     */
    public function isMerchantValidIpRange(string $ip): bool
    {
        $text = preg_replace('~[\r\n]+~', '', $this->getMerchant()->allow_ip_address);
        return IpUtils::checkIp($ip, explode(',', $text));
    }

    /**
     * Сумму которую отдал клиент (Для отображения)
     *
     * @return float | integer
    */
    public function getDisplayInPrice($split = false) {

        $give_price = $this->transaction->give_price_with_comm;

        if($this->getCurrencyIn()->is_allow_amount_space == 1 and $split == false) {
            $in_decimal = (int)strlen(substr(strrchr($give_price, '.'), 1));
            return number_format($give_price, $in_decimal,'.',' ');
        }

        return $give_price;
    }

    /**
     * Сумма которую переводит сервис (Для отображения)
     *
     * @return float | integer
     */
    public function getDisplayOutPrice($split = false)
    {
        $receiving_price = $this->transaction->receiving_price_with_comm_pay;

        if($this->getCurrencyOut()->is_allow_amount_space == 1 and $split == false) {
            $out_decimal = (int)strlen(substr(strrchr($receiving_price, '.'), 1));
            return number_format($receiving_price, $out_decimal,'.',' ');
        }

        return $receiving_price;
    }

    /**
     * Сумма которую переводит сервис (Для отображения) - с вычетом комиссии
     *
     * @param bool $split
     * @return float | integer
     */
    public function getDisplayOutPriceFee(bool $split = false) {

        if($this->getCurrencyOut()->is_allow_amount_space == 1 and $split == false) {
            $out_decimal = (int)strlen(substr(strrchr($this->transaction->out_price_fee, '.'), 1));
            return number_format($this->transaction->out_price_fee, $out_decimal,'.',' ');
        }

        return $this->transaction->out_price_fee;
    }

    /**
     * Получение статуса транзации
     *
     * @return int
    */
    public function getStatus() {
        return $this->transaction->status;
    }

    public function getStatusName() {
        return $this->transaction->task_status->name;
    }

    public function getCurrentCourse()
    {
        return $this->transaction->course_display;
    }

    /**
     * Получить время задержки в часах
     *
     * @return int
    */
    public function getHoldInHours(): int {
        return $this->getCurrencyIn()->hold_in_hours;
    }

    /**
     * Статус верификации счета
     *
     * @return boolean
    */
    public function isVerifiedCard()
    {
        $card = preg_replace('/\s/', '', $this->transaction->from_shot);
        $has = VerificationCard::where([
            ['id_user', $this->transaction->id_user],
            ['card_number', $card],
            ['id_currency', $this->transaction->direction_exchange->id_currency1],
            ['status', 1]
        ])->exists();

        return (bool)$has;
    }

    /**
     * Статус верификации карт
     *
     * @param null $currency
     * @return int
     */
    public function isVerificationCard($currency = null): int
    {
        if(is_null($currency)) {
            $currency = $this->getCurrencyIn()->toArray();
        }

        // Проверяем если отлючена пользовательская проверка карты
        if($currency['is_user_verification'] == 1 and !isset($this->authInfo->is_verification))
            return 1;

        // Если включена индивидуальная проверка карты
        if($currency['is_user_verification'] == 1 and $this->getClient()->is_verification == 1)
        {
            if($currency['min_amount_verification'] == 0)
                return 1;

            // Если сумма не превышает установленную ПС то не требуется верификация клиента
            if($currency['min_amount_verification'] > 0 and $this->getAmountIn() < $currency['min_amount_verification']) {
                return 1;
            }

            // Если сумма не превышает установленную ПС то не требуется верификация клиента
            if($currency['min_amount_verification'] > 0 and $this->getAmountIn() < $currency['min_amount_verification']) {
                return 1;
            }

            return $this->statusVerificationCard();
        }

        // Проверяем, включена ли верификация
        if($currency['is_enabled_verification'] == 0) {
            return 1;
        }

        // Если сумма не превышает установленную ПС то не требуется верификация клиента
        if($currency['is_enabled_verification'] == 2 and $this->getAmountIn() < $currency['min_amount_verification']) {
            return 1;
        }

        return $this->statusVerificationCard();
    }

    private function statusVerificationCard()
    {
        $card = preg_replace('/\s/', '', $this->transaction->from_shot);
        $hasVerify = VerificationCard::where([
            ['email', $this->transaction->email],
            ['card_number', $card],
            ['id_currency', $this->transaction->direction_exchange->id_currency1],
            ['status', 1]]
        )->exists();


        if(!$hasVerify) {
            $hasVerifyCheck = VerificationCard::where([
                ['id_order', $this->transaction->id],
                ['email', $this->transaction->email],
                ['card_number', $card],
                ['id_currency', $this->transaction->direction_exchange->id_currency1],
            ])->whereIn('status', [0, 1])->first();

            return $hasVerifyCheck->status ?? -1;
        }

        return $hasVerify == 1 ?? -1;
    }


    public function isEmailVerificationModal() {
        if($this->getCurrencyIn()->is_email_verification_modal == 0)
            return true;
        return auth()->check() and !is_null(auth()->user()->email_verified_at);
    }

    /**
     * Получение счетчика заявок  за день
     *
     * @return integer
     */
    public function getCountDayOrderIn(): int
    {
        return Task::whereHas('direction_exchange', function ($query) {
            $query->where('id_currency1', '=', $this->transaction->direction_exchange->id_currency1);
        })->whereIn('status', [3, 4])
            ->whereBetween('created_at', [
                Carbon::today()->startOfDay(),
                Carbon::today()->endOfDay()
            ])
            ->count();
    }


    /**
     * Статус заморозки транзакции
     *
     * @return bool
     */
    public function isHold() : bool
    {
        // Если не активен холд, пропускаем
        if($this->transaction->is_hold == 0) {
            return false;
        }

        $timeout = Carbon::parse($this->transaction->started_at)->addHours($this->getHoldInHours());
        $now = Carbon::now();

        return $timeout->gt($now) ? true : false;
    }


    /**
     * Статус заморозки транзакции
     *
     * @return bool
     */
    public function isHoldAutoPayment() : bool
    {
        // Если не активен холд, пропускаем
        if($this->transaction->is_hold == 0) {
            return false;
        }

        $timeout = Carbon::parse($this->transaction->started_at)->addHours($this->getHoldInHours());
        $now = Carbon::now();

        return $timeout->gt($now) ? true : false;
    }

    /**
     * Сколько минут осталось до разморозки
     *
     * @return string
    */
    public function holdTimeOut()
    {
        $now =  Carbon::now();
        $response =  Carbon::parse($this->transaction->started_at)->addHours($this->getHoldInHours());

        return ($response->diffInMinutes($now) <= 60 ?
            $response->diffInMinutes($now). ' м.' :
            $response->diffInHours($now). ' ч.');
    }

    /**
     * Разрешить пополнять резерв оператору
     *
     * @return bool
    */
    public function isAllowTransferReserve(): bool
    {
        return iEXSetting('allow_transfer_reserve') and $this->transaction->skip_reserve == 0 and $this->transaction->status == 3;
    }

    /**
     * Комиссия от перевода
     *
     * @return float | integer
    */
    public function getTransferCommission()
    {
        if($this->getCurrencyOut()->transfer_amount_reserve > 0) {
            return $this->getCurrencyOut()->transfer_amount_reserve;
        } elseif($this->getCurrencyOut()->transfer_percent_reserve > 0) {
            $amount = ($this->getAmountOut() * $this->getCurrencyOut()->transfer_percent_reserve / 100);
            return iex_number_format($amount, $this->getCurrencyOut()->number_format);
        }

        return 0;
    }

    /**
     * Переводим в ручной режим
    */
    public function disableIsBot()
    {
        $this->transaction->update([
            'is_bot' =>  0,
            'is_autopay_off' => 1
        ]);
    }

    public function addErrorLogToSignOrder(int $type = 0, string $message = null): void
    {
        // Устанавливаем метку на заявку (Если есть проблема в API)
        $this->transaction->update([
            'int_error_type' => $type,
            'error_type_text' => $message
        ]);
    }

    /**
     * Отключаем все автовыплаты если зафиксировано двойное снятие средств
    */
    public function turnOffAutoOutput()
    {
        $this->transaction->update([
            'double_withdrawal' => 1,
            'is_bot' => 0
        ]);
    }

    /**
     * Корректность заявки
    */
    public function isValidityOrder()
    {
        if($this->isCrypto() and $this->getRatesData() != null)
        {
            $rate = $this->getRatesData();

            $level = 0;
//            //Plain
//            if($this->interestRate() > 15) {
//                $level = 1;
//            }
//
//            //Average
//            if($this->interestRate() > 30) {
//                $level = 2;
//            }
//
//            //Hard
//            if($this->interestRate() > 50) {
//                $level = 3;
//            }

            if($this->interestRate() < 0) {
                $amount = $rate->exchange_course - $rate->market_course;
                $level = 3;
            } else {
                $amount = $rate->market_course - $rate->exchange_course;
            }

            return [
                'level'     =>  $level,
                'percent'   =>  $this->interestRate(),
                'amount'    =>  $amount
            ];
        }
    }

    /**
     * Проверяем номер счета получателя на уникальность
    */
    public function uniqueToShot()
    {
        $shot = TaskShot::where('id_task', '=', $this->transaction->id)->first();

        if(isset($shot)) {
            return $shot->account == $this->transaction->to_shot;
        }

        return true;
    }

    /**
     * Процентная разница между двумя курсами (Биржевой курс и Курс обмена)
    */
    public function interestRate() : float
    {
        $rate = $this->getRatesData();
        $result = (($rate->market_course - $rate->exchange_course) / $rate->market_course) * 100;
        return number_format($result,2,'.','');
    }

    /**
     * Проверяем, действительно ли заявка между криптовалютами
     *
     * @return boolean
    */
    public function isCrypto() : bool
    {
        return ($this->getCurrencyIn()->id_filter_currency and $this->getCurrencyOut()->id_filter_currency === 3);
    }

    /**
     * Получение адреса кошелька
     *
     * @return string|array
     */
    public function getAddresses()
    {
        if($this->transaction->direction_requisites != null) {
            return $this->transaction->direction_requisites->account_number;
        }elseif($this->transaction->wallets_addresses != null)
        {
            if($this->transaction->wallets_addresses->memo_id != null) {
                return [
                    'memo_id' => $this->transaction->wallets_addresses->memo_id,
                    'address' => $this->transaction->wallets_addresses->address
                ];
            }
            return $this->transaction->wallets_addresses->address;

        } elseif($this->transaction->payment_requisites != null)
        {
            if($this->transaction->payment_requisites->account_number == '[request_payment]') {
                return $this->transaction->requisites_receive;
            }
            return $this->transaction->payment_requisites->account_number;
        } elseif($this->transaction->payment_address != null) {
            return $this->transaction->payment_address;
        } elseif(!empty($this->transaction->transfer_to_account)) {
            return $this->transaction->transfer_to_account;
        }else {
            return 'Не определен';
        }
    }

    /**
     * Получение приватного ключа адреса
     *
     * @return string
    */
    public function getPrivateKeyForAddress()
    {
        if(isset($this->transaction->wallets_addresses)) {
            return $this->transaction->wallets_addresses->private_key;
        }
    }

    /**
     * Информация по картам
    */
    public function getCardDetails()
    {
        if(isset($this->transaction->tasks_card_details)) {
            return $this->transaction->tasks_card_details;
        }

        return [];
    }


    /**
     * Отключаем бота в случае неполного поступления средств от клиента
     *
     * @return void
    */
    public function inFlowFunds()
    {
        $this->transaction->update([
            'in_flow_funds' =>  1,
            'is_bot'    => 0
        ]);
    }

    /**
     * Доп. поля для направлений
     */
    public function getTasksFields(): array
    {
        return [
            'many' => $this->transaction->tasks_fields_base ?? [],
            'currency_in' => $this->transaction->tasks_fields_currency_in ?? [],
            'currency_out' => $this->transaction->tasks_fields_currency_out ?? [],
            'requisites_info_field' => $this->transaction->payment_requisites->requisites_info_fields ?? []
        ];
    }

    /**
     * Доп. поля для отдаю
    */
    public function inOrderAdditionFields() {
        if(isset($this->transaction->tasks_in_addition_fields))
            return $this->transaction->tasks_in_addition_fields;
        return [];
    }

    /**
     * Доп. поля для получаю
    */
    public function outOrderAdditionFields() {
        if(isset($this->transaction->tasks_out_addition_fields))
            return $this->transaction->tasks_out_addition_fields;
        return [];
    }


    /**
     * Получаем название custom название кошелька
     *
     * @return string | mixed
    */
    public function getFieldAccountNumber() {
        return $this->getCurrencyIn()->account_number_field;
    }

    /**
     * Установка нового статуса
     *
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        // Записать в лог статус
        iex_order_status_log($this->transaction, $status);

        $update['status'] = $status;

        // Записываем менеджера
        if(in_array($status, [4, 5, 10]) and isset($this->failedOptions['fixed_manager']) and $this->failedOptions['fixed_manager'] == true)  {
            $update['id_manager'] = \Auth::id();
        }

        // Информация об отклоненной заявки
        if(in_array($status, [5, 10])) {
            $update['id_rejection_status'] = $this->getCategoryReject();
        }

        if($status == 7) {
            $update['lead_time'] = Carbon::now()->addMinute(iEXSetting('lead_time_task'));
            $update['started_at'] = Carbon::now()->toDateTimeString();
        }

        if(in_array($status, [4, 5, 8])) {
            $update['is_bot'] = 0;
        }

        $this->transaction->update($update);
        return $this;
    }

    public function getLeadTime()
    {
        $start = Carbon::parse($this->transaction->started_at);
        $lead_time = Carbon::parse($this->transaction->updated_at);

        $getSecond = $start->diffForHumans($lead_time, true);
       return $getSecond;
    }

    /**
     * Минимальная цена обмена
     *
     * @return float
     */
    public function getOrderMinAmount() {
        return (float)$this->transaction->task_info->in_min_amount;
    }

    /**
     * Максимальная цена обмена
     *
     * @return float
     */
    public function getOrderMaxAmount() {
        return (float)$this->transaction->task_info->in_max_amount;
    }

    /**
     * Получаем EXMO Code который отправил клиент
     *
     * @return string
     */
    public function getExCode()
    {
        return $this->transaction->excode;
    }

    /**
     * Получаем Kuna Code который отправил клиент
     *
     * @return string
     */
    public function getKunaCode()
    {
        return $this->transaction->excode;
    }

    /**
     * Получаем WhiteBit который отправил клиент
     *
     * @return string
     */
    public function getWhiteBit()
    {
        return $this->transaction->excode;
    }

    /**
     * Запускаем процесс выполнения транзакции
     *
     * @return $this
     */
    public function start() {
        $this->transaction->update(['start' => 1]);
        return $this;
    }

    /**
     * Сбрасываем процесс выполнения транзакции
     *
     * @return $this
     */
    public function unstart() {
        $this->transaction->update(['start' => 0]);
        return $this;
    }

    /**
     * Проверяем запущена ли заявка
     *
     * @return boolean
    */
    public function isStart() {
        return $this->transaction->start;
    }

    /**
     * Пересчитать сумму которую фактически отдал клиент
     *
     * @param int $at_rate
     * @param int $give
     * @return void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function recount($at_rate = null, $give = null)
    {
        $tx_rate = (is_null($at_rate) ? Request::input('at_rate') : $at_rate);
        $tx_give = (is_null($give) ? Request::input('give') : $give);


        $at_rate = intval($tx_rate);
        $in = str_replace(',','.', $tx_give);

        if($at_rate == 0) {
            $newCourse = [
                'amount' =>  ($this->transaction->course_float > 0) ? $this->transaction->course_float : 0,
                'curs'  => $this->transaction->course_display
            ];

        } else {
            $direction_exchange = $this->getDirectionExchange();
            $convertFacade = ConvertFacade::call($direction_exchange)->in()->toArray();

            if($this->getTaskInfo()->city_id > 0 and isset($convertFacade['cities']))
            {
                $city_id = $direction_exchange->direction_exchange_cities->filter(function($item) {
                    return $item['city_id'] == $this->getTaskInfo()->city_id;
                })->first()->toArray();

                // Если используется работа с наличными
                $newCourse = [
                    'amount' => $convertFacade['cities'][$city_id['id']]['amount'],
                    'curs'  =>  $convertFacade['cities'][$city_id['id']]['curs'],
                ];
            } else {
                $newCourse = [
                    'amount' => $convertFacade['amount'] ?? 0,
                    'curs'  => $convertFacade['curs'] ?? null
                ];
            }
        }

        if($newCourse['amount'] > 0) {
//            $out = number_format($in * $recount,
//                $this->getCurrencyOut()->number_format,'.','');

            $old_amount = preg_replace('/[^\d.]/', "", $this->getAmountOut());
            $old_in_amount = preg_replace('/[^\d.]/', "", $this->getAmountIn());


            //Фиксируем последнее изменение
            HistoryRecalculation::create([
                'id_task'   =>  $this->transaction->id,
                'amount'    =>  $in,
                'old_amount'=>  $old_in_amount,
                'type'      =>  $at_rate,
                'course'    =>  $newCourse['curs']
            ]);

            // Пересчет
            $in_price = $in;
            // Убираем ненужные дробные числа
            $in_decimal = (int)strlen(substr(strrchr($in_price, '.'), 1));
            $result_in_price = iex_number_format(
                $in_price, ($in_decimal > $this->getCurrencyIn()->number_format) ? $this->getCurrencyIn()->number_format : $in_decimal
            );

            // Если есть дополнительная комиссия для отдаю, то прибавляем к общей сумме
            $fee_percent_in = $this->transaction->direction_exchange->oth_comm_percent;
            $fee_currency_in = $this->transaction->direction_exchange->oth_comm_currency;
            $oth_min_comm = $this->transaction->direction_exchange->oth_min_comm;
            $fee_oth_comm1 = 0;
            $result_format_in_price = (float)$result_in_price;
            $result_calc_out = (float)$result_in_price;

            if($fee_percent_in > 0 or $fee_currency_in > 0) {
                $fee_oth_comm1 = (($result_in_price * $fee_percent_in) / 100) + $fee_currency_in;
                // Минимальная комиссия (Для отдаю)
                if($oth_min_comm > 0 and $fee_oth_comm1 < $oth_min_comm) {
                    $fee_oth_comm1 = $oth_min_comm;
                }

                $result_calc_out = iex_number_format(floatval($result_calc_out) + $fee_oth_comm1, $this->getCurrencyIn()->number_format);
            }


            // Если есть комиссия платежной системы
            $fee_pay_percent_in = $this->transaction->direction_exchange->pay_comm_percent;
            $fee_pay_currency_in = $this->transaction->direction_exchange->pay_comm_currency;
            $pay_min_comm = $this->transaction->direction_exchange->pay_min_comm;
            $fee_pay_comm1 = 0;

            if($fee_pay_percent_in > 0 or $fee_pay_currency_in > 0) {
                $fee_pay_comm1 = (($result_in_price * $fee_pay_percent_in) / 100) + $fee_pay_currency_in;
                // Минимальная комиссия (Для отдаю)
                if($pay_min_comm > 0 and $fee_pay_comm1 < $pay_min_comm) {
                    $fee_pay_comm1 = $pay_min_comm;
                }
                $result_in_price = iex_number_format(floatval($result_in_price) - $fee_pay_comm1, $this->getCurrencyIn()->number_format);
                $result_calc_out = iex_number_format(floatval($result_calc_out) - $fee_pay_comm1, $this->getCurrencyIn()->number_format);
            }


            $recount_out = iex_number_format($result_calc_out * $newCourse['amount'], $this->getCurrencyOut()->number_format);

            // Скидка на обмен
            $personal_discount_fee = 0;
            if($this->getClient()->personal_discount > 0 and $this->transaction->direction_exchange->is_enable_user_discount == 0)
            {
                $personal_discount_fee = ($recount_out * $this->getClient()->personal_discount) / 100;
                $recount_out = iex_number_format((float)$recount_out + $personal_discount_fee, $this->getCurrencyOut()->number_format);
            }


            // Если есть дополнительная комиссия для получаю, то прибавляем к общей сумме
            $fee_percent_out = $this->transaction->direction_exchange->oth_comm2_percent;
            $fee_currency_out = $this->transaction->direction_exchange->oth_comm2_currency;
            $fee_oth_comm2 = 0;
            $result_format_out_price = (float)$recount_out;
            if($fee_percent_out > 0 or $fee_currency_out > 0)
            {
                $fee_oth_comm2 = (($result_format_out_price * $fee_percent_out) / 100) + $fee_currency_in;
                $recount_out = iex_number_format($result_format_out_price - $fee_oth_comm2, $this->getCurrencyOut()->number_format);
            }

            // Если есть комиссия платежной системы для получаю
            $fee_pay_percent_out = $this->transaction->direction_exchange->pay_comm2_percent;
            $fee_pay_currency_out = $this->transaction->direction_exchange->pay_comm2_currency;
            $fee_pay_comm2 = 0;
            if($fee_pay_percent_out > 0 or $fee_pay_currency_out > 0)
            {
                $fee_pay_comm2 = (($result_format_out_price * $fee_pay_percent_out) / 100) + $fee_pay_currency_out;
                $recount_out = iex_number_format($result_format_out_price - $fee_pay_comm2, $this->getCurrencyOut()->number_format);
            }



            $this->transaction->update([
                'give_price' => $result_format_in_price,
                'give_price_default' => iex_number_format(floatval($result_in_price) + $fee_oth_comm1, $this->getCurrencyIn()->number_format),
                'give_price_with_comm'  =>  (float)$result_in_price + $fee_pay_comm1,
                'give_price_with_comm_pay' => iex_number_format((float)$result_format_in_price + $fee_pay_comm1, $this->getCurrencyIn()->number_format),
                'give_price_fee_comm' => $fee_oth_comm1,
                'give_price_fee_pay' => $fee_pay_comm1,

                'receiving_price' => (float)$recount_out,
                'receiving_price_default' => $result_format_out_price - $personal_discount_fee,
                'receiving_price_with_comm' => (float)$result_format_out_price - $fee_oth_comm2,
                'receiving_price_with_comm_pay' => $recount_out,
                'receiving_price_fee_comm' => $fee_oth_comm2,
                'receiving_price_fee_pay' => $fee_pay_comm2,
                'course_display'    =>  $newCourse['curs']
            ]);
//            $this->transaction->update([
//                'give_price' => (float)iex_number_format($result_format_in_price, $this->getCurrencyIn()->number_format),
//                'give_price_default' => (float)iex_number_format($result_in_price + $fee_oth_comm1, $this->getCurrencyIn()->number_format),
//                'give_price_with_comm'  =>  iex_number_format((float)$result_in_price + $fee_oth_comm1, $this->getCurrencyIn()->number_format),
//                'give_price_with_comm_pay' => (float)$result_format_in_price + $fee_pay_comm1,
//                'give_price_fee_comm' => $fee_oth_comm1,
//                'give_price_fee_pay' => $fee_pay_comm1,
//
//                'receiving_price' => (float)iex_number_format($recount_out - $fee_pay_comm2, $this->getCurrencyOut()->number_format),
//                'receiving_price_default' => (float)iex_number_format($result_format_out_price, $this->getCurrencyOut()->number_format),
//                'receiving_price_with_comm' => (float)iex_number_format($recount_out, $this->getCurrencyOut()->number_format),
//                'receiving_price_with_comm_pay' => (float)iex_number_format($recount_out - $fee_pay_comm2, $this->getCurrencyOut()->number_format),
//                'receiving_price_fee_comm' => $fee_oth_comm2,
//                'receiving_price_fee_pay' => $fee_pay_comm2,
//                'course_display'    =>  $course_display
//            ]);

            $this->updateReserve($old_amount);
        }
    }

    /**
     * Получаем сумму которую клиент отдает
     *
     * @return float
     */
    public function getAmountIn()
    {
        $give_price = $this->transaction->give_price;
        if(iEXSetting('give_price_type') == 1) {
            $give_price = $this->transaction->give_price_with_comm_pay;
        }elseif(iEXSetting('give_price_type') == 2) {
            $give_price = $this->transaction->give_price_default;
        }
        return $give_price;
    }

    public function getCreditAmount()
    {
        // Сумма, которая должна поступить на счет
        $credit_amount = $this->transaction->give_price;
        $merchant = $this->transaction->getMerchant();

        if(isset($merchant))
        {
            if($this->transaction->getMerchant()->credit_amount == 1) {
                $credit_amount = $this->transaction->give_price_with_comm_pay;
            } elseif($this->transaction->getMerchant()->credit_amount == 2) {
                $credit_amount = $this->transaction->give_price_default;
            }
        }

        return $credit_amount;
    }

    /**
     * Преобразовываем сумму которую отдает клиент в RUB
     *
     * @return float
    */
    public function getAmountConvertIn() {
        return convert_to_rub(mb_strtoupper($this->getCodeIn()->name), $this->transaction->give_price);
    }

    /**
     * Получаем сумму которую клиент желает получить
     *
     * @return float
    */
    public function getAmountOut()
    {
        $receiving_price = $this->transaction->receiving_price_with_comm_pay;
        if(iEXSetting('receiving_price_type') == 1) {
            $receiving_price = $this->transaction->receiving_price_with_comm;
        }elseif(iEXSetting('receiving_price_type') == 2) {
            $receiving_price = $this->transaction->receiving_price_default;
        }
        return $receiving_price;
    }

    /**
     * Получение дневного лимита заявок
     *
     * @return bool
     */
    public function dayLimitAmountIn()
    {
        return Task::whereBetween('updated_at', [
            Carbon::today()->startOfDay(),
            Carbon::today()->endOfDay()
        ])->where('status', '=', 4)
            ->whereHas('direction_exchange', function ($query) {
                $query->where('id_currency1', '=', $this->getCurrencyIn()->id);
            })->sum('give_price');
    }


    /**
     * Получаем детали с hash из сторонних сервисов
     *
     * @return \App\Models\TaskPrivateHash
    */
    public function getPrivateHash()
    {
        return $this->transaction->task_private_hash;
    }

    /**
     * Получаем номер счета отправителя
     *
     * @return string
    */
    public function getFromShot(): string
    {
        return preg_replace('/\s/', '', $this->transaction->from_shot);
    }

    /**
     * Не допускать лишних операторов к транзации
     *
     * @return bool
    */
    public function hasOperator() {
        return ($this->transaction->scans > 0 and $this->transaction->scans != Auth::id());
    }

    /**
     * Указываем нового оператора
     *
     * @param int $scans
     */
    public function setOperator($scans = 0)
    {
        $array = [
            'scans' => $scans,
            'scan_name' => \auth()->user()->name,
            'operator_started_at' => Carbon::now()
        ];

        if(empty($this->transaction->id_main_operator)) {
            $array['id_main_operator'] = $scans;
        }

        $this->transaction->update($array);
    }

    /**
     * Добавляем оператора в историю
     *
     * @param int $scan
     */
    public function addOperatorToHistory($scan = 0)
    {
        if((int)iEXSetting('count_change_operator') > 0) {
            $this->transaction->task_info->update([
                'count_change_operator' => $this->transaction->task_info->count_change_operator + 1
            ]);
        }

        TaskHistoryOperator::create([
            'type' => 1,
            'id_task' => $this->transaction->id,
            'id_from_manager' => $this->transaction->scans,
            'id_manager' => $scan,
            'id_status' => $this->getStatus(),
            'in_amount' => $this->getAmountIn(),
            'out_amount' => $this->getAmountOut(),
            'course' => $this->getCurrentCourse()
        ]);
    }

    /**
     * Маск кол-во раз, когда оператора могут сменить
     *
     * @return bool
    */
    public function maxCountChangeOperator() {
        return $this->transaction->task_info->count_change_operator >= iEXSetting('count_change_operator', 0);
    }

    /**
     * Отвязываем оператора
     */
    public function deleteOperator()
    {
        $this->transaction->update([
            'scans' => 0,
            'operator_started_at' => null
        ]);
    }

    /**
     * Отправляем чек клиенту на электронный адрес
     *
     * @param null $message
     */
    public function sendCheck($message = null)
    {
        $this->transaction->update([
            'message_success'   =>  $message,
            'check_status'      =>  1
        ]);
        $input['message_success'] = $message;

        //Отложенное уведомление (Отправка чека)
        dispatch(new SendClientCheckJob(
            $this->getClient()->email,
            $this->transaction,
            $input
        ))->delay(now()->addMinutes(3))
            ->onQueue('low');
    }

    /**
     * Отправляем сообщение клиенту на электронный адрес
     *
     * @param null $message
     * @param int $type
     */
    public function sendNotification($message = null, $type = 0)
    {

        // Добавление сообщение в историю
        $item = TaskMessage::create([
            'user_id' => \auth()->id(),
           // 'user_id' => $this->transaction->id_user,
            'id_task' => $this->transaction->id,
            'message' => security_xss($message),
            'type_user' => 1,
            'is_view' => 1
        ]);

        $input['message_success'] = $message;

        //Отложенное уведомление (Отправка чека)
        if($type == 0)
        {
            dispatch(new OrderChatJob($this->getClient()->email, $this->transaction, $input
            ))->delay(now()->addMinutes(3))
                ->onQueue('low');
        } elseif($type == 1)
        {
            if(iEXSetting('is_enabled_module_socket') == 1)
            {
                broadcast(new OrderChatMessagesEvent($this->transaction->public_id, $this->transaction->id_user, [
                    'message' => $item->message,
                    'date' => \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans(),
                    'type_user' => $item->type_user
                ]));


                // Отправляем уведомление клиенту
                broadcast(new OrderChatNotificationEvent([
                    'user_id' => $item->user_id,
                    'message' => 'Менеджер ответил вам в чате по заявке '.$item->id_task,
                    'order_id' => $this->transaction->public_id
                ]));
            }
        }
    }

    /**
     * Отмечаем заявку как непрочитанную
     *
     * @return void
    */
    public function logout() {
        $this->transaction->update(['scans' => 0]);
    }

    /**
     * Получаем описание транзакции для оплаты через мерчант
     *
     * @param string $host
     * @param string $alias
     * @return string
     */
    public function merchantDescription(string $host, string $alias)
    {
        // Индивидуальные описания для каждого мерчанта
        $description = sprintf('Pay_to_%s_%s', $host, $this->transaction->id);
        if(in_array($alias, ['advcash', 'payeer'])) {
            $description = sprintf('Обмен на %s. Заявка #%s', $host, $this->transaction->id);
        } elseif($alias == 'yandexmoney') {
            $description = sprintf('Перевод личных средств №%s', $this->transaction->id);
        }

        return $description;
    }


    /**
     * Отправляем уведомление клиенту
     *
     * 1). (success) - Все успешно выполнено
     * 2). (failed)  - Заявка отклонена
     * 3). (check)   - Клиенту отправлен чек по транзакции
     * 4). (defer)   - Если заявка отложено
     *
     * @param null $message
     */
    public function notification($message = null)
    {
        if(iEXSetting('notify_change_status') == 1)
        {
            // Успешная закрытие заявки
            if($this->transaction->status == 4)
                $this->notifySuccessful();

            // Отложенная заявка
            if($this->transaction->status == 8 and (int)iEXSetting('is_notify_order_defer') == 1)
                $this->notifyDefer();

            // Удаленная заявка
            if($this->transaction->status == 5)
                $this->notifyFailed();
        }
    }

    public function frozenGlobalBalance($reset = false)
    {
        $in = $this->getReserveIn()->summa;
        $out = $this->getReserveOut()->summa;

        if($reset == false)
        {
            $result_in = number_format($in + $this->getAmountIn(), $this->getCurrencyIn()->number_format,'.','');
            $result_out = number_format($out - $this->getAmountOut(), $this->getCurrencyOut()->number_format,'.','');

            ReserveLog::create([
                'id_task'      => $this->transaction->id,
                'id_currency'  => $this->getCurrencyIn()->id,
                'id_reserve'   => $this->getReserveIn()->id,
                'summa'        => $this->getAmountIn(),
                'type'         => 'plus',
                'history'      =>  $in.' > '. $result_in
            ]);

            ReserveLog::create([
                'id_task'           =>  $this->transaction->id,
                'id_currency'       =>  $this->getCurrencyOut()->id,
                'id_reserve'        =>  $this->getReserveOut()->id,
                'summa'             =>  $this->getAmountOut(),
                'commission'        =>  0,
                'type'              => 'minus',
                'history'           =>  $out.' > '. $result_out
            ]);

            $this->getReserveIn()->update(['summa' =>  $result_in]);
            $this->getReserveOut()->update(['summa' =>  $result_out]);

        } else {
            ReserveLog::where('id_task', $this->transaction->id)->delete();
            $result_in = number_format($in - $this->getAmountIn(), $this->getCurrencyIn()->number_format,'.','');
            $result_out = number_format($out + $this->getAmountOut(), $this->getCurrencyOut()->number_format,'.','');

            $this->getReserveOut()->update(['summa' =>  $result_out]);
            $this->getReserveIn()->update(['summa' =>  $result_in]);
        }
    }


    /**
     * Лимит обменов для операторов
    */
    public function limit_operator_view()
    {
        $item = OperationLevel::where([
            ['status', '=', 1],
            ['id_operator', '=', \auth()->id()]
        ])->first();

        if(empty($item))
            return true;

        $in_amount = convert_to_usd($this->getCodeIn()->name, $this->getAmountIn());
        $min = (float)$item->level_group->from_limit;
        $max = (float)$item->level_group->to_limit;

        return ($min <= $in_amount and $max >= $in_amount);
    }
}
