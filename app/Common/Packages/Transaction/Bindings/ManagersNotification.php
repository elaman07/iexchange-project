<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Jobs\OrderRecountJob;
use App\Jobs\OrderRestoreJob;
use App\Jobs\OrderShotBlackListJob;
use App\Jobs\OrderStatusJob;
use App\Jobs\TelegramClientOrderJob;
use App\Jobs\TelegramClientToShotJob;
use App\Models\WalletsHistory;
use Illuminate\Support\Facades\Log;

trait ManagersNotification
{
    /**
     * Если заявка восстновлена
     *
     * @return void
    */
    public function notifyRestore()
    {
        $delay = now()->addSeconds(20);

        dispatch(new OrderRestoreJob(
            $this->getClient()->email,
            $this->transaction
        ))->delay($delay)->onQueue('low');
    }

    /**
     * Если заявка успешно выполнена
     *
     * @return mixed
     */
    public function notifySuccessful()
    {
        try {
            $delay = now()->addSeconds(5);

            // Поставить в очередь за выполненение
            dispatch(new OrderStatusJob(
                $this->getClient()->email,
                $this->transaction
            ))->delay($delay)->onQueue('low');

            $this->notifyTelegramOrder('success');
        }catch (\Exception $exception) {
            Log::error('Notification: '. $exception->getMessage());
        }
    }

    /**
     * Если заявка отложено
     *
     * @param null $message
     * @return mixed
     */
    public function notifyDefer($message = null)
    {
        $delay = now()->addMinutes(5);

        // Поставить в очередь за выполненение
        dispatch(new OrderStatusJob(
            $this->getClient()->email,
            $this->transaction,
            ['message' => $message])
        )->delay($delay)->onQueue('low');
    }

    /**
     * Если заявка удалена
     *
     * @return mixed
     */
    public function notifyFailed()
    {
        $delay = now()->addMinutes(5);

        // Поставить в очередь за выполненение
        dispatch(new OrderStatusJob(
            $this->getClient()->email,
            $this->transaction
        ))->delay($delay)->onQueue('low');

        $this->notifyTelegramOrder('failed');
    }

    /**
     * Уведомлять клиента в случае пересчета
     *
     * @return mixed
    */
    public function notifyRecount()
    {
        $delay = now()->addMinute();

        // Поставить в очередь за выполненение
        dispatch(new OrderRecountJob(
            $this->getClient()->email,
            $this->transaction
        ))->delay($delay)->onQueue('low');
    }

    /**
     * Уведомление в Telegram для клиентов
     *
     * @param null $type
     */
    protected function notifyTelegramOrder($type = null)
    {
        // Если не указано telegram id то не отправляем уведомление
        if(empty($this->transaction->user->telegram)) {
            return;
        }

        // Если не включен модуль уведомление, дальше не пускаем
        if(!iEXSetting('is_notify_order_telegram_client')) {
            return;
        }

        $delay = now()->addSeconds(30);
        dispatch((new TelegramClientOrderJob($this->transaction, $type)))
            ->onQueue('low')->delay($delay);
    }

    /**
     * Отправить в Telegram номера счета получателя
    */
    protected function sentToShotTelegram()
    {
        if(iEXSetting('telegram_to_shot_notification')) {
            $delay = now()->addSeconds(5);
            dispatch((new TelegramClientToShotJob($this->transaction)))
                ->onQueue('low')->delay($delay);
        }
    }

    /**
     * Уведомлениям клиента о том что его данные находятся в черном списке
     *
     * @param string $text
     */
    protected function sendToClientBlackListShot(string $text)
    {
        if(iEXSetting('is_order_shot_black_list') == 1)
        {
            $delay = now()->addMinutes(5);
            dispatch(
                (new OrderShotBlackListJob($this->getClient()->email, $this->transaction, $text))
            )->onQueue('low')->delay($delay);
        }
    }
}
