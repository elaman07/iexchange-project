<?php
namespace App\Common\Packages\Transaction\Bindings;


use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Models\RewardLog;

trait ManagersBonus
{
    public function rewardBonus()
    {
        $reward = RewardLog::where([
            ['id_user', $this->getClient()->id],
            ['id_task', $this->transaction->id]]);

        return ($reward->exists() ? $reward->first()->bonus: 0);
    }

    public function rewardBonusString()
    {
        if($this->rewardBonus() == 0)
            return 0;


        $info = $this->getTaskInfo();

        // Отображения бонуса
        $view_bonus = sprintf('%s %s', $this->rewardBonus(), $info->currency_sign_payout);
        if($info->currency_position_payout == 'left')
            $view_bonus = sprintf('%s %s', $info->currency_sign_payout, $this->rewardBonus());
        return $view_bonus;
    }

    /**
     * Получаем код валюты
     *
     * @param bool $is_read
     * @return string
     */
    public function getCurrencyPayout(bool $is_read = false)
    {
        if($is_read)
            return config('crypto.currency_payout_sign');
        return config('crypto.currency_payout');
    }

    public function referralBonus()
    {
        $referral = ReferralLog::where([
            ['id_referral', $this->getClient()->id],
            ['id_task', $this->transaction->id]]);

        return ($referral->exists() ? $referral->first() : null);
    }

    public function referralBonusString()
    {
        $bonus = $this->referralBonus();
        if(!isset($bonus))
            return 0;
        return $this->referralBonus()->bonus;
    }

    /**
     * Выплачить партнерские бонусы
     *
     * @return bool
    */
    public function isNotPartner(): bool {
        return $this->getDirectionExchange()->is_not_partner;
    }
}
