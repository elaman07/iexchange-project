<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Events\OrderStatusesEvent;
use App\Jobs\SMSSuccessOrderJob;
use App\Models\CurrencyAnalytics;
use App\Models\Fund;
use App\Models\LinksReview;
use App\Models\PayTransactionHash;
use App\Models\ReserveLog;
use App\Models\ReserveLogProfit;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\TaskInfo;
use App\Models\TaskProfit;
use App\Models\TaskStatusLog;
use App\Models\TaskUser;
use App\Models\UserWalletStories;
use App\Models\WalletsHistory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Contests\Entities\ContestModel;
use Nwidart\Modules\Facades\Module;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait ManagersSuccess
{
    private $disableRelation = false;

    /***
     * Успешная закрытия транзакции
     *
     * @param array $optionsData
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    public function success(array $optionsData = [])
    {
        // Обработчик в случае несколько выполнений
        if($this->getStatus() == 4) {
            throw new \Exception('Заявка уже была исполнена');
        }

        if($this->disableRelation == true) {
            throw new \Exception('Заявка выполняется, пожалуйста подождите ...');
        }

        // Проверяем код безопасности
        ///////////////////////////////////////////////////
        if($this->getType() == 'merchant')
        {
            $code_order_confirm = config('security-codes.order-confirm');
            if(!empty($code_order_confirm) and $this->getSecurityOrderCodeConfirm() != $code_order_confirm) {
                throw new \Exception('Неверный пин-код');
            }
        }
        ///////////////////////////////////////////////////


        // Индивидуальный пересчет заявок
        if($this->getCurrencyOut()->is_unique_recount_order == 1) {
            $this->individualRecountOrder();
        } else {
            $this->generalRecountOrder();
        }


        $this->disableRelation = true;
        $pay = $this->getPay();

        //Первым делом производим автовыплату для определенных клиентов
        if($this->getType() == 'merchant' and isset($pay))
        {
            // Макс. кол-во раз, которое можно нажать "Оплатить и завершить"
            if((int)iEXSetting('max_num_autopay_button') > 0 and $this->transaction->pay_num > (int)iEXSetting('max_num_autopay_button')) {
                throw new \Exception('Превышен лимит макс. нажатий "Оплатить и завершить"');
            }

            // Счетчик автовыплат
            $this->transaction->update([
                'pay_num' => ($this->transaction->pay_num + 1),
                'is_bot' => 0
            ]);

            $this->autoPaymentLoaded();
        }

        $this->investInFond();
        $this->employeesBonus();

        // Отключаем выплаты партнерских бонусов
        if($this->getSwitchNotPartner() == true)
            $this->transaction->task_info->update(['is_switch_not_partner' => 1]);

        // Отключаем выплаты cashback
        if($this->getSwitchNotCashback() == true)
            $this->transaction->task_info->update(['is_switch_not_cashback' => 1]);


        // Выплачиваем партнеру, боунсы за обмен
        if(!empty($this->transaction->referral_hash)) {
            ReferralSystemFacade::builder($this->transaction)->handle();
        }

        if((int)iEXSetting('is_cashback_disabled') == 0) {
            CashbackFacade::make($this->transaction)->call();
        }

        if($this->transaction->skip_reserve == 0 and !isset($optionsData['disable_reserve']))
        {
            if($this->transaction->is_reserve == 1) {
                $this->changeGlobalReserves();
            } else {
                $this->changeReserves();
            }
        }

        // Записываем в базу данные по выплтам
        TaskInfo::where('id_task', $this->transaction->id)->update([
            'currency_name_payout'      =>  config('crypto.currency_payout'),
            'currency_sign_payout'      =>  config('crypto.currency_payout_sign'),
            'currency_position_payout'  =>  config('crypto.currency_payout_position'),
            'text_message_order'        =>  $this->getTextMessageOrder()
        ]);


        // Номер счета Отдаю
        $from_shot_stories = security_xss(preg_replace('/\s/', '', $this->transaction->from_shot));
        // Номер счета Получаю
        $to_shot_stories = security_xss(preg_replace('/\s/', '', $this->transaction->to_shot));

        // Если пользователь авторизован
        if(!empty($from_shot_stories))
        {
            // Проверяем если в базе нет счета, добавляем
            $from_check_user_wallet = UserWalletStories::where([
                ['id_user', $this->transaction->id_user],
                ['wallet', $from_shot_stories],
                ['id_currency', $this->transaction->direction_exchange->id_currency1]
            ])->exists();

            if(!$from_check_user_wallet and (int)iEXSetting('is_saved_user_stories') == 0)
            {
                UserWalletStories::create([
                    'id_user' => $this->transaction->id_user,
                    'wallet' => $from_shot_stories,
                    'id_currency' => $this->transaction->direction_exchange->id_currency1
                ]);
            }
        }

        // Если пользователь авторизован
        if(!empty($to_shot_stories) and (int)iEXSetting('is_saved_user_stories') == 0)
        {
            if($this->transaction->task_info->dot_not_remember_data == 0)
            {
                // Проверяем если в базе нет счета, добавляем
                $to_check_user_wallet = UserWalletStories::where([
                    ['id_user', $this->transaction->id_user],
                    ['wallet', $to_shot_stories],
                    ['id_currency', $this->transaction->direction_exchange->id_currency2]
                ])->exists();

                if(!$to_check_user_wallet)
                {
                    UserWalletStories::create([
                        'id_user' => $this->transaction->id_user,
                        'wallet' => $to_shot_stories,
                        'id_currency' => $this->transaction->direction_exchange->id_currency2
                    ]);
                }
            }
        }


        // Записываем прибыль от сделки в базу
        $direction_profit_percent = $this->getDirectionExchange()->profit;
        $direction_profit_currency = $this->getDirectionExchange()->profit_s;


        if($direction_profit_currency > 0 or $direction_profit_percent > 0)
        {
            $inNumberFormat = (int)$this->getCurrencyIn()->number_format;
            if((int)$inNumberFormat > (int)iEXSetting('max_number_format_reserve')) {
                $inNumberFormat = (int)iEXSetting('max_number_format_reserve');
            }

            $direction_amount = iex_number_format(($this->getAmountIn() * $direction_profit_percent / 100) + $direction_profit_currency, $inNumberFormat);

            TaskProfit::updateOrCreate([
                'id_task' => $this->transaction->id
            ], [
                'id_task' => $this->transaction->id,
                'profit_percent' => $direction_profit_percent,
                'profit_currency' => $direction_profit_currency,
                'profit_usd' => convert_to_usd($this->getCodeIn()->name, $direction_amount),
                'profit_rub' => convert_to_rub($this->getCodeIn()->name, $direction_amount)
            ]);

        }


        $this->calculateCommission();

        if(!is_null($this->getBlockchainTransaction()))
        {
            PayTransactionHash::create([
                'id_task' => $this->transaction->id,
                'transaction_hash' => $this->getBlockchainTransaction(),
                'id_currency' => $this->getCurrencyOut()->id
            ]);
        }

        // После успешного выполнения заявки, снимаем % прибыли с валюты "Отдаю" (profit_percent_reserve)
        $profit_percent = (float)$this->getCurrencyIn()->profit_percent_reserve;

        if($profit_percent > 0)
        {
            $inNumberFormat = (int)$this->getCurrencyIn()->number_format;
            if((int)$inNumberFormat > (int)iEXSetting('max_number_format_reserve')) {
                $inNumberFormat = (int)iEXSetting('max_number_format_reserve');
            }

            $amount = iex_number_format($this->getAmountIn() * $profit_percent / 100, $inNumberFormat);

            ReserveLogProfit::create([
                'id_task'       =>  $this->transaction->id,
                'id_currency'   =>  $this->getCurrencyIn()->id,
                'amount'        =>  $amount,
                'amount_usd'    =>  convert_to_usd($this->getCodeIn()->name, $amount),
                'percent'       =>  $profit_percent
            ]);

            $this->getReserveIn()->update([
                'summa' =>  iex_number_format($this->getReserveIn()->summa - $amount, $inNumberFormat)
            ]);
        }

        $this->setStatus(4);

        $this->addErrorLogToSignOrder(0,null);
        $this->disableRelation = false;

        // Увеличиваем счетчик кол-во заявок в мерчантах
        $merchant = $this->getMerchant();
        if(isset($merchant) and !empty($merchant))
        {
            $merchant_total_usd = (float)$merchant->total_usd;
            $total_usd = (float)($merchant_total_usd + convert_to_usd($this->getCodeIn()->name, $this->transaction->give_price));

            $merchant->update([
                'order_num' => $merchant->order_num+1,
                'total_usd' => $total_usd
            ]);
        }


        // Уведомление в реальном времени
        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new OrderStatusesEvent($this->transaction));
        }

        // Увеличиваем счетчик успешных заявок
        $this->updateOrderNum();

        // Обновление направлений
        $directionUpdateArray = [
            'last_order_id' => $this->transaction->id,
            'last_order_at' => Carbon::now()->format('c'),
        ];

        if($this->transaction->direction_exchange->first_order_id == 0) {
            $directionUpdateArray['first_order_id'] = $this->transaction->id;
        }

        $this->transaction->direction_exchange->update($directionUpdateArray);

        if(isset($this->transaction->direction_exchange->currency2->pay))
        {
            $this->transaction->update([
                'id_payment_gateway' => $this->transaction->direction_exchange->currency2->id_pay
            ]);
        }

        //
        $in_currencies = $this->getCurrencyIn();
        $out_currencies = $this->getCurrencyOut();


        $order_count = Task::leftJoin('direction_exchange', 'direction_exchange.id', '=','tasks.id_direction_exchange')
            ->selectRaw("count(case when direction_exchange.id_currency1 = {$in_currencies->id} then 1 end) as in_order_count")
            ->selectRaw("count(case when direction_exchange.id_currency2 = {$out_currencies->id} then 1 end) as out_order_count")
            ->where('tasks.status', '=', 4)
            ->first();


        $in_amount_convert = ((isset($in_currencies->currency_analytics)) ? $in_currencies->currency_analytics->in_amount + $this->getAmountIn() : $this->getAmountIn());
        $out_amount_convert = (isset($out_currencies->currency_analytics) ? $out_currencies->currency_analytics->out_amount + $this->getAmountOut() : $this->getAmountOut());


        // Увеливаем статистику валют
        CurrencyAnalytics::where('id_currency', $in_currencies->id)->updateOrCreate([
            'id_currency'       => $in_currencies->id
        ], [
            'id_currency'       => $in_currencies->id,
            'in_amount'         => $in_amount_convert,
            'in_amount_usd'     =>  convert_to_usd($this->getCodeIn()->name, $in_amount_convert),
            'in_count_exchange' => ((isset($in_currencies->currency_analytics)) ? $in_currencies->currency_analytics->in_count_exchange + 1 : 1),
            'in_order_count'    =>  $order_count['in_order_count']
        ]);

        // Увеливаем статистику валют
        CurrencyAnalytics::where('id_currency', $out_currencies->id)->updateOrCreate([
            'id_currency' => $out_currencies->id
        ], [
            'id_currency'           =>  $out_currencies->id,
            'out_amount'            =>  $out_amount_convert,
            'out_amount_usd'        =>  convert_to_usd($this->getCodeOut()->name, $out_amount_convert),
            'out_count_exchange'    =>  (isset($out_currencies->currency_analytics) ? $out_currencies->currency_analytics->out_count_exchange + 1 : 1),
            'out_order_count'       =>  $order_count['out_order_count']
        ]);


        $this->notification();

        // Если заявка создана из Telegram, клиенту отправляем сообщение
        if(!empty($this->transaction->telegram_id))
        {
            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => '📃 Ссылка на чек', 'url' => url('/orders/'.$this->transaction->public_id)])
                );


            $links = LinksReview::orderBy('sorting')->get();
            $telegram_message = view('_parent.bot.website.success_order', ['order' => $this->transaction, 'links' => $links])->render();

            Telegram::sendMessage([
                'chat_id'           => $this->transaction->telegram_id,
                'text'              => $telegram_message,
                'parse_mode'        => 'HTML',
                'reply_markup' => $keyboard,
            ]);
        }

        // Если включено SMS уведомление
        if(iEXSetting('sms_notify_success_order'))
        {
            $delay = now()->addSeconds(20);
            dispatch(new SMSSuccessOrderJob($this->getClient(), $this->transaction))
                ->delay($delay)
                ->onQueue('low');
        }

        $this->converterAmountGive();
    }

    /**
     * Вычитывает дополнительную комиссию
     * Node: Она пока необходимо для транзакции с Сбербанк
     *
     * @return mixed
     */
    public function calculateCommission()
    {
        $fee = ((float)$this->getRPCCommission() > 0 ? sprintf('%.8f', $this->getRPCCommission()) :
            $this->getCommission());

        //Информация о логе
        $reserve_log = ReserveLog::where([
            ['id_task', $this->transaction->id],
            ['id_reserve', $this->getReserveOut()->id],
        ]);

        // В случае если по какой та причине в логе не найдено данные, сбрасываем ее
        if(!$reserve_log->exists()) {
            $this->changeReserves(true);
            $this->highlightReserve();
        }

        $item = ReserveLog::where([
            ['id_task', $this->transaction->id],
            ['id_reserve', $this->getReserveOut()->id],
        ])->first();

        $item->update([
            'commission' => $fee,
            'summa_not_fee' => $item->summa,
            'summa_with_fee' => iex_number_format($item->summa + $fee, $this->getCurrencyOut()->number_format),
        ]);

        $this->getReserveOut()->update([
            'summa' =>  $this->getReserveOut()->summa - $fee,
        ]);

        return $this;
    }

    /**
     * Получаем общую прибыль от заявки с вычетом всех бонусов
     *
     * @return float | double
    */
    public function getTotalProfit()
    {
//        $referral = 0;
//        if(!is_null($this->referralBonus()))
//            $referral = $this->referralBonus()->bonus;
//
//        if(!is_null($this->getTaskReport()) and $this->getTaskReport()->profit_rub > 0) {
//            return (float)$this->getTaskReport()->profit_rub - $this->rewardBonus() - $referral;
//        }

        return 0;
    }

    /**
     * 0.1% от транзакции инвестируем в общий фонд
     *
     * @return mixed
    */
    private function investInFond()
    {

        // Зачисление в фонд
        if(Module::find('Contests')->isEnabled() == 1)
        {
            $contest = ContestModel::where('status', '=', 1)->first();

            if(isset($contest) and $contest->is_manual_bank == 0)
            {
                $fund = floor($this->getAmountIn($contest->code_currency->name) * ($contest->percent / 100));

                $contest->update([
                    'bank' => iex_number_format((float)$contest->bank + $fund, 2),
                    'bank_base' => iex_number_format((float)$contest->bank + $fund, 2)
                ]);
            }
        }

    }

    /**
     * В конце конверируем сумму которую отдает клиент
    */
    private function converterAmountGive()
    {
        $usd = convert_to_usd($this->getCodeIn()->name, $this->getAmountIn());
        $rub = convert_to_rub($this->getCodeIn()->name, $this->getAmountIn());

        TaskConvertLog::create([
            'id_task'   =>  $this->transaction->id,
            'to_usd'    =>  iex_number_format($usd, 2),
            'to_rub'    =>  iex_number_format($rub, 2)
        ]);

        $get = TaskUser::where('id_user', $this->getClient()->id)->first();

        $to_rub = 0;
        $to_usd = 0;
        if(!empty($get)) {
            $to_rub = $get['convert_to_rub'];
            $to_usd = $get['convert_to_usd'];
        }


        TaskUser::updateOrCreate([
            'id_user'           =>  $this->getClient()->id,
        ], [
            'id_user'           =>  $this->getClient()->id,
            'convert_to_usd'    =>  iex_number_format($to_usd+$usd, 2),
            'convert_to_rub'    =>  iex_number_format($to_rub+$rub, 2)
        ]);
    }
}
