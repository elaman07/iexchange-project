<?php
namespace App\Common\Packages\Transaction\Bindings;


use App\Jobs\OrderShotBlackListJob;
use App\Models\BannedUser;
use App\Models\BlacklistOrder;
use Illuminate\Support\Carbon;

trait ManagersClient
{
    /**
     * Текст в случае отклоения заявки
     *
     * @var string
     */
    protected $failedMessage = null;

    /**
     * Забанить клиента в случае подозрительных действий
     *
     * @param array $attributes
     */
    public function clientBan(array $attributes = [])
    {
        //Определяем тип бана
        $expired_at = Carbon::now()->addDays(7)->toDateTimeString();
        if($this->clientBanLog() == 2) {
            $expired_at = Carbon::now()->addWeek(2)->toDateTimeString();
        } elseif($this->clientBanLog() >= 3) {
            $expired_at = Carbon::now()->addYear(10)->toDateTimeString();
        }

        $this->getClient()->ban([
            'expired_at'    =>  $expired_at,
            'comment'       =>  (isset($attributes['comment']) ? $attributes['comment'] : null)
        ]);

        $this->getClient()->update([
            'banned_at' =>  $expired_at
        ]);
    }

    /**
     * Записываем каждого забаненного клиента
     *
     * @return integer
    */
    private function clientBanLog(): int
    {
        BannedUser::create([
            'id_user'   =>  $this->getClient()->id,
            'id_task'   =>  $this->transaction->id,
        ]);

        return BannedUser::where('id_user', $this->getClient()->id)->count();
    }

    /**
     * Добавить реквизиты клиента в черный список
     *
     * @return void
     */
    public function clientToBlackList()
    {
        $text = sprintf('Заявка №%s от клиента была признана мошеннической.', $this->transaction->id);

        $contacts = 'Email: '.$this->getClient()->email.PHP_EOL;
        $contacts .= 'IP: '.$this->transaction->ip.PHP_EOL;

        if(!is_null($this->transaction->from_shot)) {
            if(!is_null($this->transaction->sender_fullname)) {
                $contacts .= 'ФИО Отправителя: '.$this->transaction->sender_fullname.PHP_EOL;
            }
            $contacts .= 'Номер счета: '.preg_replace('/\s+/', '', $this->transaction->from_shot).PHP_EOL;
        }
        if(!is_null($this->transaction->to_shot)) {
            if(!is_null($this->transaction->recipient_fullname)) {
                $contacts .= 'ФИО Получателя '.$this->transaction->recipient_fullname.PHP_EOL;
            }
            $contacts .= 'Номер счета: '.preg_replace('/\s+/', '', $this->transaction->to_shot).PHP_EOL;
        }

        // Добавляем IP, Email в черный список
        BlacklistOrder::create([
            'value' =>  $contacts,
            'text'  =>  $text,
            'type'  =>  3
        ]);


        // Заносим данные клиента в черный список bestchange
        if(!is_null($this->failedMessage) and $this->failedMessage == 'merchant:scam') {
            $message = '[iEXBot] - Бот зафиксировал мошенническую операцию. Создает заявку и оплачивает неполную сумму через систему мерчант.';

            $contacts = 'Email: '.$this->getClient()->email.PHP_EOL;
            $contacts .= 'IP: '.$this->transaction->ip.PHP_EOL;

            if(!is_null($this->transaction->from_shot)) {
                if(!is_null($this->transaction->sender_fullname)) {
                    $contacts .= 'ФИО Отправителя: '.$this->transaction->sender_fullname.PHP_EOL;
                }
                $contacts .= 'Номер счета: '.preg_replace('/\s+/', '', $this->transaction->from_shot).PHP_EOL;
            }
            if(!is_null($this->transaction->to_shot)) {
                if(!is_null($this->transaction->recipient_fullname)) {
                    $contacts .= 'ФИО Получателя '.$this->transaction->recipient_fullname.PHP_EOL;
                }
                $contacts .= 'Номер счета: '.preg_replace('/\s+/', '', $this->transaction->to_shot).PHP_EOL;
            }

            addToBestchangeList($contacts, $message);
        }
    }

    /**
     * Добавить реквизиты клиента в черный список
     *
     * @return void
     */
    public function clientOrderDataBan()
    {
        if($this->transaction->is_ban_order_data == 1) {
            flash('Информация по заявке уже находится в черном списке', ['alert alert-danger']);
            return redirect()->back();
        }

        $text = sprintf('Данные клиента по заявке  №%s заблокированы.', $this->transaction->id);

        $contacts = 'Email: '.$this->getClient()->email.PHP_EOL;
        $contacts .= 'IP: '.$this->transaction->ip.PHP_EOL;

        if(!is_null($this->transaction->from_shot)) {
            if(!is_null($this->transaction->sender_fullname)) {
                $contacts .= 'ФИО Отправителя: '.$this->transaction->sender_fullname.PHP_EOL;
            }
            $contacts .= 'Номер счета: '.preg_replace('/\s+/', '', $this->transaction->from_shot).PHP_EOL;
        }
        if(!is_null($this->transaction->to_shot)) {
            if(!is_null($this->transaction->recipient_fullname)) {
                $contacts .= 'ФИО Получателя '.$this->transaction->recipient_fullname.PHP_EOL;
            }
            $contacts .= 'Номер счета: '.preg_replace('/\s+/', '', $this->transaction->to_shot).PHP_EOL;
        }

        // Добавляем IP, Email в черный список
        BlacklistOrder::create([
            'value' =>  $contacts,
            'text'  =>  $text,
            'type'  =>  3
        ]);

        // Уведомлениям клиента о том, что его данные находятся в черном списке
        $this->sendToClientBlackListShot($contacts);

        $this->transaction->update(['is_ban_order_data' => 1]);
    }

    /**
     * Увеличиваем счетчик заявок клиента
     *
     * @return void
    */
    public function updateOrderNum()
    {
        $this->getClient()->update([
            'order_num' =>  $this->getClient()->order_num + 1
        ]);
    }

    /**
     * Баним клиента в случае если система определения заявку как мошенническую
     *
     * @return void
    */
    public function autoBanClientForAutoPayment()
    {
        if((int)iEXSetting('ban_cheater_user') == 1) {
            // Категория, мошенническая заявка
            $this->setCategoryReject(4);
            $this->failed();
        }
    }

    /**
     * Сообщение перед блокировкой заявки
     *
     * @param string $text
     * @return ManagersClient
     */
    public function failedBanMessage(string $text) {
        $this->failedMessage = $text;
        return $this;
    }
}
