<?php
namespace App\Common\Packages\Transaction\Bindings;

use App\Common\Packages\Payment\PaymentFacade;

trait ManagersPayment
{
    /**
     * Проверяем и зачисляем оплату
     *
     * @return void
     */
    public function checkAndEnroll()
    {
        //
    }

    /**
     * Проверите поступление средств (Для Coin)
     *
     * @return boolean
    */
    public function checkPayment(): bool
    {
        if(!$this->getMerchant())
            return false;

        $configProvider = PaymentFacade::configProvider($this->getMerchant()->gateway->alias);
        return isset($configProvider) and isset($configProvider['check_payment']) and $configProvider['check_payment'];
    }
}
