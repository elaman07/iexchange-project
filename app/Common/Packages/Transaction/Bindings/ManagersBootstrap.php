<?php
namespace App\Common\Packages\Transaction\Bindings;


use App\Events\OrderStatusesEvent;
use App\Jobs\AdminNewOrderJob;
use App\Jobs\NewOrderJob;
use App\Jobs\NexmoNewOrder;
use App\Jobs\TelegramOrderJob;
use App\Models\DirectionExchange;
use App\Models\Reserve;
use App\Models\Task;
use App\Models\TaskInfo;
use App\Models\TaskStatusLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait ManagersBootstrap
{
    /**
     * Создаем базового загрузчика
     *
     * @return void
     */
    public function bootstrap()
    {
        if(!$this->hasOperator() and !$this->hasPreview() and $this->transaction->scans == 0) {
            $this->setOperator(Auth::id());
        }

        //Прикрепляем оператора к транзакции
        $this->isActiveOperator();
    }

    /**
     * Активность оператора в заявке
    */
    public function isActiveOperator()
    {
        if(iEXSetting('order_active_operator') == 0)
            return;

        if(is_null($this->transaction->operator_started_at))
            return;

        if(Carbon::now() > $this->transaction->operator_started_at->addMinutes((float)iEXSetting('order_active_operator')))
            $this->deleteOperator();
    }

    /**
     * Восстановление отклоенной заявки
     *
     * @return void
     */
    public function restoreOrder()
    {
        // Изменяем резерв
        $this->changeReserves();
        $this->setStatus(3);

        // Уведомлять клиента о восстановлении заявки
        if((int)iEXSetting('is_notify_order_restore') == 1) {
            $this->notifyRestore();
        }
    }


    /**
     * Восстановление отклоенной заявки
     *
     * @return void
     */
    public function handlerOrderFromCron()
    {
        // Записываем лог статус
        TaskStatusLog::create([
            'id_task'       =>  $this->transaction->id,
            'user_id'       =>  $this->transaction->id_user,
            'old_status'    =>  $this->transaction->status,
            'new_status'    =>  3,
            'in_price'      =>  $this->transaction->give_price,
            'out_price'     =>  $this->transaction->receiving_price,
            'place_change'  =>  0,
            'course_display'  => $this->transaction->course_display
        ]);

        $this->transaction->update([
            'status'                =>  3,
            'is_autopay_limit'      =>  1,
            'lead_time'             =>  \Illuminate\Support\Carbon::now()->addMinutes((int)iEXSetting('lead_time_task')),
            'started_at'            =>  Carbon::now()->toDateTimeString(),
            'next_checkout_at'      =>  Carbon::now()->addSeconds(30)->toDateTimeString()
        ]);

        // Изменить резерв
        $this->changeReserves();

        // Если заявка мошенническая то морозим
        if($this->transaction->task_info->is_freeze_scam == 1) {
            $this->setDeferType(5);
            $this->defer();
        } else {

            if(iEXSetting('is_enabled_module_socket') == 1) {
                broadcast(new OrderStatusesEvent($this->transaction));
            }

            // Уведомляем оператора о новой заявки
            if(config('crypto.order_mail')) {
                // Уведомляем оператора о новой заявки
                dispatch(new AdminNewOrderJob($this->transaction))->delay(
                    now()->addSeconds(30)
                )->onQueue('low');
            }

            // SMS Уведомление для оператора о новой заявке
            if (iEXSetting('enable_sms_notification') and iEXSetting('sms_new_order')) {
                $delay = now()->addMinute();
                dispatch(new NexmoNewOrder($this->transaction))->delay($delay)->onQueue('low');
            }

            // Уведомлять о новых заявках в Telegram (Для операторов)
            if((int)iEXSetting('enable_tg_notify_operator', 0) == 1) {
                $delay = now()->addSeconds(30);
                dispatch(new TelegramOrderJob($this->transaction))->delay($delay)->onQueue('low');
            }

            // Отправляем уведомление о новой заявке (На почту для клиентов)
            if((int)iEXSetting('notify_change_status', 0) == 1) {
                $delay = now()->addSeconds(15);
                dispatch(new NewOrderJob($this->transaction))->delay($delay)->onQueue('high');
            }
        }
    }

    /**
     * Получить детали "Направления"
    */
    public function getDirectionExchange(): DirectionExchange
    {
        return $this->transaction->direction_exchange;
    }

    /**
     * Получить детали валюты "Отдаю"
     *
     * @return \App\Models\Currency
    */
    public function getCurrencyIn()
    {
        return $this->transaction->direction_exchange->currency1;
    }

    /**
     * Получить детали валюты "Получаю"
     *
     * @return \App\Models\Currency
     */
    public function getCurrencyOut()
    {
        return $this->transaction->direction_exchange->currency2;
    }

    /**
     * Получаем детали выплаты
     *
     * @return \App\Models\GatewayPayment
    */
    public function getPay(){
        return $this->transaction->direction_exchange->currency2->pay;
    }

    /**
     * Получение дневной суммы обменов
     *
     * @return float
    */
    public function getAmountOutDay()
    {
        $order = Task::whereBetween('created_at', [Carbon::today()->startOfDay(), Carbon::today()->endOfDay()])
            ->where('status', '=',4)->sum('receiving_price');

        return (float)$order + $this->transaction->receiving_price;
    }

    /**
     * Получаем детали мерчанта
     *
     * @return \App\Models\GatewayMerchant
    */
    public function getMerchant() {
        return $this->transaction->merchant;
    }

    public function historyPaymentTx() {
        return $this->transaction->history_payment_transaction;
    }

    /**
     * Получить детали платежной системы "Отдаю"
     *
     * @return \App\Models\Payment
     */
    public function getPaymentIn()
    {
        return $this->getCurrencyIn()->payment;
    }

    /**
     * Получить детали платежной системы "Получаю"
     *
     * @return \App\Models\Payment
     */
    public function getPaymentOut()
    {
        return $this->getCurrencyOut()->payment;
    }

    /**
     * Получить код валюты "Отдаю"
     *
     * @return \App\Models\CodeCurrency
     */
    public function getCodeIn()
    {
        return $this->getCurrencyIn()->code_currency;
    }

    /**
     * Получить код валюты "Получаю"
     *
     * @return \App\Models\CodeCurrency
     */
    public function getCodeOut()
    {
        return $this->getCurrencyOut()->code_currency;
    }

    /**
     * Получить отчет о заявке
     *
     * @return \App\Models\TaskReport
     */
    public function getTaskReport()
    {
        return $this->transaction->task_report;
    }


    /**
     * Получить резервы "Отдаю"
     *
     * @return \App\Models\Reserve
     */
    public function getReserveIn()
    {
        // Используем объединенный резерв
        if(isset($this->getCurrencyIn()->reserve->main_reserve))
            return $this->getCurrencyIn()->reserve->main_reserve;
        return $this->getCurrencyIn()->reserve;
    }

    /**
     * Получить резервы "Получаю"
     *
     * @return \App\Models\Reserve
     */
    public function getReserveOut()
    {
        // Используем объединенный резерв
        if(isset($this->getCurrencyOut()->reserve->main_reserve))
            return $this->getCurrencyOut()->reserve->main_reserve;
        return $this->getCurrencyOut()->reserve;
    }

    /**
     * Получить детали реквизитов
     *
    */
    public function getRequisites() {
        return $this->transaction->payment_requisites;
    }

    public function getRatesData() {
        return $this->transaction->tasks_rates_data;
    }

    /**
     * Получаем детали динамаческий адресов
     *
     * @return \App\Models\WalletsAddresses
    */
    public function getWalletAddresses()
    {
        return $this->transaction->wallets_addresses;
    }

    /**
     * Получить резервы "Отдаю"
     *
     * @param bool $spaces
     * @return \App\Models\Reserve
     */
    public function getPropsIn($spaces = false)
    {
        if($spaces == true) {
            return remove_all_spaces($this->transaction->from_shot);
        }
        return $this->transaction->from_shot;
    }

    /**
     * Получить резервы "Получаю"
     *
     * @param bool $spaces
     * @return \App\Models\Reserve
     */
    public function getPropsOut($spaces = false)
    {
        if($spaces == true) {
            return remove_all_spaces($this->transaction->to_shot);
        }
        return $this->transaction->to_shot;
    }

    public function getWalletTransaction() {
        return $this->transaction->wallet_transaction;
    }

    public function getHistoryCode() {
        return $this->transaction->history_code;
    }

    /**
     * Дополнительная информация о заявке
     *
     * @return TaskInfo
    */
    public function getTaskInfo()
    {
        return $this->transaction->task_info;
    }

    /**
     * История пересчета
     *
     * @return \App\Models\HistoryRecalculation
    */
    public function getHistoryRecalculation()
    {
        return $this->transaction->history_recalculation;
    }

    /**
     * История операторов
     *
     * @return \App\Models\TaskHistoryOperator
     */
    public function getHistoryOperators()
    {
        return $this->transaction->history_operators;
    }

    /**
     * Включить ручную выплату
    */
    public function isDisabledManualButton()
    {

        $pay = $this->getPay();
        if(isset($pay) and $pay->manual_pay_order == 1)
            return 1;

        return 0;
    }

    /**
     * Получении истории чата
     *
     * @return \App\Models\TaskChat
    */
    public function getHistoryChat()
    {
        return $this->transaction->tasks_chat;
    }

    /**
     * Получить детали клиента
     *
     * @return \App\Models\User
    */
    public function getClient()
    {
        return $this->transaction->user;
    }
}
