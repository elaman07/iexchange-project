<?php

namespace App\Common\Packages\GeoIP;

use ArrayAccess;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class Location implements ArrayAccess
{
    /**
     * Атрибуты местоположения
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Создайте новый экземпляр местоположения.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * Определите, находится ли местоположение для того же IP-адреса.
     *
     * @param  string $ip
     *
     * @return bool
     */
    public function same($ip)
    {
        return $this->getAttribute('ip') == $ip;
    }

    /**
     * Установите данный атрибут в этом месте.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;

        return $this;
    }

    /**
     * Получить атрибут из массива $attributes.
     *
     * @param  string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        $value = Arr::get($this->attributes, $key);

        // Сначала мы проверим наличие мутатора для заданной операции
        // который просто позволяет разработчикам настраивать атрибут по мере его установки.
        if (method_exists($this, 'get' . Str::studly($key) . 'Attribute')) {
            $method = 'get' . Str::studly($key) . 'Attribute';

            return $this->{$method}($value);
        }
        return $value;
    }

    public function isAttribute($key)
    {
        $value = Arr::get($this->attributes, $key);
        return (isset($value) and $value != null) ? true : false;
    }

    /**
     * Верните отображаемое имя местоположения.
     *
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        return preg_replace('/^,\s/', '', "{$this->city}, {$this->state}");
    }

    /**
     * Является ли местоположение по умолчанию.
     *
     * @return bool
     */
    public function getDefaultAttribute($value)
    {
        return is_null($value) ? false : $value;
    }

    /**
     * Получите экземпляр как массив.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * Получить атрибут местоположения
     *
     * @param  string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Укажите атрибут местоположения
     *
     * @param  string $key
     * @param  mixed  $value
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Определите, существует ли данный атрибут.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    /**
     * Получите значение для данного смещения.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * Установите значение для заданного смещения.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * Отмените значение для данного смещения.
     *
     * @param  mixed  $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    /**
     * Проверьте, установлен ли атрибут местоположения
     *
     * @param $key
     *
     * @return bool
     */
    public function __isset($key)
    {
        return array_key_exists($key, $this->attributes);
    }

    /**
     * Отсоедините атрибут местоположения.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->attributes[$key]);
    }
}