<?php

if (!function_exists('geoip'))
{
    /**
     * Получите местоположение предоставленного IP-адреса.
     *
     * @param string $ip
     *
     * @return \App\Common\Packages\GeoIP\GeoIP|\App\Common\Packages\GeoIP\Location
     * @throws Exception
     */
    function geoip($ip = null)
    {
        if (is_null($ip)) {
            return app('geoip');
        }

        return app('geoip')->getLocation($ip);
    }
}