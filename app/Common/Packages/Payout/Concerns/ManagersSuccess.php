<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 14.10.2019
 * Time: 7:46
 */

namespace App\Common\Packages\Payout\Concerns;


use App\Jobs\SMSPayoutJob;
use App\Jobs\WithdrawalRequestJob;
use App\Models\Reserve;
use App\Models\ReserveLog;
use App\Models\UserBalance;
use App\Models\WithdrawalRequestLog;

trait ManagersSuccess
{
    /**
     * Если бонусы успешно выплачены
     *
     * @return void
    */
    public function success()
    {
        //Первым делом производим автовыплату для определенных клиентов
        $pay = $this->getCurrencyPay();
        if($this->getType() == 'merchant' and isset($pay)) {
            $this->autoPaymentLoaded();
        }

        $payment = sprintf('%s %s', $this->getPayment()->name, $this->getCurrencyCode()->name);
        $balance = sprintf('%s %s', $this->getTotalAmount(), $this->getCurrencyCode()->sign);

        // Если включено SMS уведомление
        if(iEXSetting('sms_notify_success_payouts')) {
            $delay = now()->addMinutes(5);
            dispatch(new SMSPayoutJob($this->getUser(), $this->payout))
                ->delay($delay)
                ->onQueue('low');
        }


        $int_price = $this->getTotalAmount();

        //Лог выплаты
        WithdrawalRequestLog::create([
            'id_withdrawal_request' =>  $this->payout->id,
            'text'                  =>  sprintf('Средства переведены на %s, сумма выплаты %s', $payment, $balance),
            'amount'                =>  iex_number_format($this->getTotalAmount(),2),
            'remainder'             =>  0,
            'id_manager'            =>  \auth()->id()
        ]);

        $finalAmount = ($this->payout->base_referral + $this->payout->base_reward);
        $user_balance = UserBalance::where('id_user', $this->payout->id_user)->first();

        $balance_array['balance'] = 0;
        $balance_array['balance_reward'] = 0;
        $balance_array['referral_total_withdrawal'] = $user_balance->referral_total_withdrawal + $finalAmount;
        //Обновление цен
        $user_balance->update($balance_array);


        $this->payout->update([
            'id_manager'        =>  \auth()->id(),
            'status'            => 1,
            'balance_reward'    => 0,
            'balance_referral'  => 0
        ]);

        ///////////////////////////////
        ///     Снять с резерва     ///
        /// ///////////////////////////
        //Снимаем деньги с резерва
        $reserve = Reserve::where('id_currency', $this->getCurrency()->id);

        //Получаем текущую сумму
        $summa = $reserve->first()->summa;

        $result_price = ($summa - $int_price);

        $reserve->update([
            'summa' => iex_number_format($result_price, $reserve->first()->currency->number_format)
        ]);

        //В процессе успешно обработки заявки, мы меняем резервную цену
        ReserveLog::create([
            'id_currency'       => $this->getCurrency()->id,
            'id_reserve'        => $reserve->first()->id,
            'summa'             => $int_price,
            'commission'        =>  0,
            'type'              => 'minus'
        ]);

        ////////////////////////////////////////////////////


        // Отложенная отправки писем
        if(iEXSetting('is_on_notify_referral_system') == 0) {
            dispatch(new WithdrawalRequestJob($this->payout))
                ->delay(now()->addMinutes(10))->onQueue('low');
        }
    }
}
