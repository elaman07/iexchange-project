<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 13.10.2019
 * Time: 22:40
 */

namespace App\Common\Packages\Payout\Concerns;

use Illuminate\Support\Str;

trait ManagersBootstrap
{
    protected float $api_balance = 0;
    /**
     * Проверяем баланс перед выплатой
     *
     * @return int
     */
    public function hasBalance()
    {
        $pay = $this->payout->currency->pay;
        if(isset($pay) and in_array($pay->gateway->alias, config('payment.providers.payouts'))) {
            $this->api_balance = (float)$this->getBalanceByCurrency();
            return ($this->getBalanceByCurrency() < $this->getTotalAmount());
        }

        return 2;
    }

    /**
     * Получение баланса
     *
     * @return float
     */
    public function getAPIBalance(): float
    {
        return $this->api_balance;
    }

    /**
     * Автовыплата
     *
     * @throws \Exception
     */
    public function autoPaymentLoaded()
    {
        $payment = Str::studly($this->getCurrencyPay()->gateway->alias);

        // Для RPC систем
        $class = "\\App\\Common\\Packages\\Payout\\Payments\\{$payment}Payment";
        // Проверяем, существует ли класс выплаты
        if(class_exists($class)) {
            \Log::info(sprintf('-- Автовыплаты по заявке %s прошла успешно', $this->payout->big_id));
            return new $class($this->payout, $this->withAsPaymentOptions());
        }

        throw new \Exception('Не найден провайдер автовыплат '.$payment);
    }

    /**
     * Дополнительные опции которые будут переданы в класс выплаты
     *
     * @return array
     */
    private function withAsPaymentOptions()
    {
        return [
            'code' => $this->getCurrencyCode()->name,
            'total_amount' => $this->getTotalAmount()
        ];
    }
}
