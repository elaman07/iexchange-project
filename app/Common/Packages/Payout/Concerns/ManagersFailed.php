<?php
namespace App\Common\Packages\Payout\Concerns;


use App\Models\UserBalance;

trait ManagersFailed
{
    /**
     * Удаление заявки на выплату бонусов
     *
     * @return void
    */
    public function failed()
    {
        $user_balance = UserBalance::find($this->payout->id_user);
        $user_balance->update([
            'balance'   =>  iex_number_format($user_balance->balance + $this->payout->base_referral),
            'balance_reward'   =>  iex_number_format($user_balance->balance_reward + $this->payout->base_reward),
        ]);
        $this->payout->delete();
    }
}