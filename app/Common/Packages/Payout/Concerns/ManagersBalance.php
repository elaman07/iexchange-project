<?php
namespace App\Common\Packages\Payout\Concerns;


use App\Common\Packages\Payment\PaymentFacade;
use Illuminate\Support\Str;

trait ManagersBalance
{
    private $available_code = [
        'USD' => null,
        'EUR' => null,
        'RUB' => null
    ];

    /**
     * Получить баланс по валюте
     *
     * @param null $type
     * @return mixed
     */

    public function getBalanceByCurrency($type = null)
    {
        $pay = $this->getCurrencyPay();

        if(!is_null($pay) or !is_null($type))
        {
            $studly = (!is_null($type) ? $type : $pay->gateway->alias);
            $method = 'balance'.Str::studly($studly);


            if(in_array($studly, config('payment.providers.payouts'))) {
                $api = PaymentFacade::pay($studly, $pay->id)
                    ->api();

                if(method_exists($api,'setMethodPay')) {
                    $api = $api->setMethodPay($pay->method_pay);
                }
                return (float)$api->getBalance($this->getCurrencyCode()->name);
            }

            if(method_exists($this, $method)) {
                return $this->$method($studly, $pay->id);
            }
        }
    }
}
