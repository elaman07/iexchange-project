<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 14.10.2019
 * Time: 7:43
 */

namespace App\Common\Packages\Payout\Concerns;


trait ManagersDetails
{
    /**
     * Детали автовыплаты по заявкам
     *
     * @return  \App\Models\Merchant
    */
    public function getCurrencyPay()
    {
        return $this->payout->currency->pay;
    }

    public function getPayment() {
        return $this->payout->currency->payment;
    }

    public function getCurrency() {
        return  $this->payout->currency;
    }

    public function getUser() {
        return $this->payout->user;
    }

    /**
     * Получаем код валюты
     *
     * @return \App\Models\CodeCurrency
    */
    public function getCurrencyCode()  {
        return $this->payout->currency->code_currency;
    }

    /**
     * Итоговый сумма выплаты
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return floatval($this->payout->balance_referral + $this->payout->balance_reward);
    }
}
