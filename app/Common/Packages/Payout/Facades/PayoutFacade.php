<?php
namespace App\Common\Packages\Payout\Facades;


use App\Common\Packages\Transaction\Transaction;
use Illuminate\Support\Facades\Facade;

/**
 */

class PayoutFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payouts';
    }
}