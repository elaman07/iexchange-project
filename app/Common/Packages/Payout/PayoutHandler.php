<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 13.10.2019
 * Time: 22:37
 */

namespace App\Common\Packages\Payout;


use App\Models\WithdrawalRequest;
use Illuminate\Http\Request;

class PayoutHandler
{
    use Concerns\ManagersBootstrap,
        Concerns\ManagersBalance,
        Concerns\ManagersDetails,
        Concerns\ManagersSuccess,
        Concerns\ManagersFailed;


    /**
     * Детали Request
     *
     * @var \Illuminate\Http\Request
    */
    protected $request;

    /**
     * Дополнительные параметры транзакции
     */
    protected $parameters = [
        'type'  =>  'default',
    ];

    /**
     * Детали выплат
     *
     * @var \App\Models\WithdrawalRequest
    */
    protected $payout;

    /**
     * Получение Деталей выплаты
     * @param int $id
     * @param array $config
     * @param Request $request
     * @return PayoutHandler
     */
    public function call(int $id,  $config, Request $request)
    {
        $this->payout = WithdrawalRequest::findOrFail($id);
        $this->parameters = is_null($config) ? []: $config;
        $this->request = $request;

        return $this;
    }

    /**
     * Тип транзакции
     *
     * @return string
     */
    public function getType()
    {
        return $this->parameters['type'];
    }

    /**
     * Получить действией которая вызвана
     *
     * @return string
     */
    public function hasAction()
    {
        return ($this->parameters['actions'] ?? null);
    }

    /**
     * Дает полное права обработать транзакцию по выбранным действиям
     *
     * @param Request $request
     * @throws \Exception
     */
    public function handlerAction()
    {
        // Роутер путей
        switch ($this->hasAction())
        {
            case 'success':
                $this->success();
                break;

            case 'failed':
                $this->failed();
                break;
        }
    }

    /**
     * Детали бонусов на выплат
     *
     * @return \App\Models\WithdrawalRequest
    */
    public function getPayouts()
    {
        return $this->payout;
    }
}
