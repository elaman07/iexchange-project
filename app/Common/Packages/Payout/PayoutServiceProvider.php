<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 13.10.2019
 * Time: 22:34
 */

namespace App\Common\Packages\Payout;


use Illuminate\Support\ServiceProvider;

class PayoutServiceProvider extends ServiceProvider
{
    public function boot() {
        //
    }

    public function register()
    {
        $this->app->singleton('payouts', function() {
            return new PayoutHandler();
        });
    }
}