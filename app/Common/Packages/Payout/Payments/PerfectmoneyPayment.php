<?php
namespace App\Common\Packages\Payout\Payments;


use Illuminate\Support\Str;

class PerfectmoneyPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected $paymentName = 'PerfectMoney';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     */
    public function handle()
    {
        \Payment::pay('PerfectMoney', $this->pay_id)
            ->api()
            ->transferAccount([
                'amount'    =>  $this->getAmount(),
                'currency'  =>  Str::upper($this->code),
                'to'        =>  Str::upper($this->getScore()),
                'comment'   =>  $this->getComment(),
                'order_id'  =>  $this->payout->big_id
            ]);
    }
}
