<?php
namespace App\Common\Packages\Payout\Payments;


class PayeerPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected $paymentName = 'Payeer';

    /**
     * Знаков, после запятой
     *
     * @var integer
     */
    protected $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
     */
    public function handle()
    {
        \Payment::pay('payeer', $this->pay_id)
            ->api()
            ->transfer([
                'amount'    =>  $this->getAmount(),
                'currency'  =>  $this->code,
                'to'        =>  $this->getScore(),
                'comment'   =>  $this->getComment(),
                'who_commission' => 1
            ]);
    }
}
