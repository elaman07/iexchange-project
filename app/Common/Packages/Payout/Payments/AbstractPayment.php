<?php
namespace App\Common\Packages\Payout\Payments;
use App\Common\Packages\Transaction\Contracts\PaymentContract;
use App\Events\AutoPaymentSuccessful;
use App\Models\Task;
use App\Models\WalletsHistory;
use App\Models\WithdrawalRequest;

/**
 * Абстрактный класс системы автовыплат
*/
abstract class AbstractPayment implements PaymentContract
{
    /**
     * Название Платежной система
     *
     * @var string
    */
    protected $paymentName = 'default';

    /**
     * Детали транзакции
     *
     * @var \App\Models\WithdrawalRequest
    */
    protected $payout;

    /**
     * Сумма перевода
     *
     * @var string | float
     */
    protected $amount;

    /**
     * Код валюты
     *
     * @var string
     */
    protected $code;

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected $decimal = 2;


    protected $pay_id;
    /**
     * Конструктор
     *
     * @param WithdrawalRequest $withdrawalRequest
     * @param array $options
     */
    public function __construct(WithdrawalRequest $withdrawalRequest, array $options = [])
    {
        $this->payout = $withdrawalRequest;
        $gateway_payment = $this->payout->currency->pay;
        $this->pay_id = $gateway_payment->id;

        if(isset($options['total_amount'])) {
            $this->setAmount($options['total_amount']);
        }

        // Получение кода валюты
        if(array_key_exists('code', $options)) {
            $this->setCode($options['code']);
        }

        // Если в обработчике найден метод автовыплаты, выполняем ее сразу.
        if(method_exists($this,'handle')) {
            $this->handle();
        }
    }

    /**
     * Обработчик автовыплаты
     *
     * @return void
     */
    abstract public function handle();

    /**
     * Записываем сумму получения
     *
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Получаем сумму получения
     *
     * @return string|float
    */
    public function getAmount()
    {
        return iex_number_format($this->amount, $this->decimal);
    }

    /**
     * Код валюты получателя
     *
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * Номер счета получателя
     *
     * @return string
     */
    public function getScore()
    {
        return remove_all_spaces($this->payout->score);
    }

    /**
     * Прикрепляем комментарий к транзакции
     *
     * @param bool $is_number
     * @return int|string
     */
    public function getComment(bool $is_number = false)
    {
        if($is_number == true)
            return $this->payout->big_id;
        return iEXContentLanguage('sitename').': Выплата бонусов по заявке №'.$this->payout->big_id;
    }
}
