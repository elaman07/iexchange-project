<?php
namespace App\Common\Packages\Payout\Payments;


class AdvcashPayment extends AbstractPayment
{
    /**
     * Название Платежной система
     *
     * @var string
     */
    protected $paymentName = 'AdvCash';

    /**
     * Знаков, после запятой
     *
     * @var integer
    */
    protected $decimal = 2;

    /**
     * Обработчик автовыплаты
     *
     * @return void
    */
    public function handle()
    {
        \Payment::pay('advcash', $this->pay_id)
            ->api()
            ->sendMoney($this->getAmount(), \mb_strtoupper($this->code), '', $this->getScore(), $this->getComment(), true);
    }
}
