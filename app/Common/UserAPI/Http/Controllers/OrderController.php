<?php

namespace App\Common\UserAPI\Http\Controllers;

use App\Common\Packages\Converter\Facades\ConvertFacade;
use App\Common\Packages\Editor\EditorFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Events\OrderStatusesEvent;
use App\Http\Resources\Rates\PaymentSystemsResource;
use App\Http\Resources\Sessions\CurrentUserResource;
use App\Jobs\AdminNewOrderJob;
use App\Jobs\NexmoNewOrder;
use App\Jobs\TelegramOrderJob;
use App\Models\Currency;
use App\Models\CurrencyNotification;
use App\Models\DirectionExchange;
use App\Models\DirectionNotification;
use App\Models\Task;
use App\Models\TaskInfo;
use App\Models\TaskStatusLog;
use App\Models\Transaction;
use App\Models\VerificationCardCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class OrderController extends AbstractAPIController
{
    protected array $items = [];

    /**
     * Статус заявки
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function status(Request $request): JsonResponse
    {
        // Если нет доступа
        if(!$request->user()->tokenCan('status-order'))
        {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /order/status'
            ]);
        }

        $id = $request->has('id') ? $request->get('id') : 0;
        if($id == 0) {
            return response()->json([
                'error' => 'Order not found'
            ]);
        }

        $order = Task::where('public_id', security_xss($id))->first();
        if($order)
        {
            if(!isset($order->status))
            {
                return response()->json([
                    'status' => 1,
                    'message' => 'Error'
                ]);
            }

            $status = 'cancel';
            if($order->status == 2) {
                $status = 'pending';
            } elseif($order->status == 9) {
                $status = 'merchant';
            } elseif($order->status == 3) {
                $status = 'process';
            } elseif($order->status == 7) {
                $status = 'pay';
            } elseif($order->status == 4) {
                $status = 'success';
            }

            $is_paid = in_array($order->status, [4, 7]);

            $data = [
                'id'       =>  iEXSetting('client_id_type_for_order') == 1 ?  $order->public_id : $order->id,
                'public_id' =>  (string)$order->public_id,
                'type' => 'order_status',
                'attributes'    =>  [
                    'created_at'    =>  $order->created_at,
                    'is_paid'       =>  $is_paid,
                    'is_handler'    =>  $order->status == 3,
                    'status'        =>  $status
                ]
            ];

            if($order->status ==  2 and $order->payment_requisites != null and $order->payment_requisites->account_number == '[request_payment]') {
                $data['attributes']['custom_payment_field_value'] = $order->requisites_receive;
            }

            // Если заявка выполнена
            if($order->status == 4) {
                $data['attributes']['events']['message'] = iEXSetting('s_order_notify_text');
            }

            $this->items['data'] = $data;
        }

        return response()->json($this->items);
    }

    /**
     * Создание заявки
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        // Если нет доступа
        if(!$request->user()->tokenCan('create-order'))
        {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /order/create'
            ]);
        }

        // Если обменник отключен, то не даем создавать заявки
        if(isJobOffline() == 1)
        {
            $this->items = [
                'field'   => 'offline',
                'message' => 'Нет связи с сервером.'
            ];
            return response()->json($this->items);
        }

        $validator = $this->validatorFields($request);

        // Валидация полученной формы
        if($validator->fails())
        {
            $fields = ['email', 'amount', 'direction', 'phone'];
            foreach ($fields as $field)
            {
                if($validator->errors()->has($field))
                {
                    $this->items = [
                        'field'     =>  $field,
                        'message'   =>  $validator->errors()->first($field)
                    ];
                }
            }
            return response()->json($this->items);
        } else {

            $direction = $request->get('direction');
            $direction_find =  DirectionExchange::where([
                ['id_currency1', (int)$request->get('income_payment_system')],
                ['id_currency2', (int)$request->get('outcome_payment_system')],
            ])->first();

            $calculation = $this->calculation($direction_find);
            $rates_amount = (float)$calculation['r']['amount'];

            if($direction == 'from') {
                $resultAmountIn = (float)$request->get('amount');
                $resultAmountOut = iex_number_format($rates_amount * (float)$resultAmountIn, $calculation['in_amount_decimal']);
            }elseif($direction == 'to') {
                $resultAmountOut = (float)$request->get('amount');
                $resultAmountIn = iex_number_format((float)$resultAmountOut / $rates_amount, $calculation['out_amount_decimal']);
            }

            $array = [
                'income_account'    => $request->has('income_account') ? security_xss($request->get('income_account')) : null,
                'outcome_account'   => $request->has('outcome_account') ? security_xss($request->get('outcome_account')) : null,
                'sender_fullname'   => $request->has('sender_fullname') ? security_xss($request->get('sender_fullname')) : null,
                'recipient_fullname'=> $request->has('recipient_fullname') ? security_xss($request->get('recipient_fullname')) : null,
                'fields_in'         => $request->has('fields_in') ? security_xss($request->get('fields_in')) : null,
                'fields_out'        => $request->has('fields_out') ? security_xss($request->get('fields_out')) : null,
                'city_id'           => $request->has('city_id') ? security_xss($request->get('city_id')) : null,
                'country_id'        => $request->has('country_id') ? security_xss($request->get('country_id')) : null,
                'direction_fields'  => $request->has('direction_fields') ? security_xss($request->get('direction_fields')) : null,
                'email'             => $request->has('email') ? security_xss($request->get('email')) : null,
                'phone'             => $request->has('phone') ? security_xss($request->get('phone')) : null,
                'income_amount'     => $resultAmountIn  ,
                'outcome_amount'     => $resultAmountOut,
                'income_payment_system'     => $request->has('income_payment_system') ? $request->get('income_payment_system') : null,
                'outcome_payment_system'     => $request->has('outcome_payment_system') ? $request->get('outcome_payment_system') : null,
            ];

            $order = \Order::request_api($array);
            $response = $order->created();
            if(is_array($response)) {
                if(isset($response['data']) and isset($response['data']['relationships'])) {
                    $response['data']['id'] = $response['data']['attributes']['public_id'];
                    unset($response['data']['relationships']);
                }
            }
            return response()->json($response);
        }



//        'income_account'          :   this.fields.leftInputAccount,
//            'outcome_account'         :   this.fields.rightInputAccount,
//            'sender_fullname'       :   this.fields.senderFullname,
//            'recipient_fullname'    :   this.fields.recipientFullname,
//            'outcome_unk'           :   this.fields.outcomeUnk,
//            'income_unk'            :   this.fields.incomeUnk,
//            'fields_in'             :   this.model_exchange_in,
//            'fields_out'            :   this.model_exchange_out,
//            'city_id'               :   this.direction_city_id,
//            'country_id'            :   this.direction_country_id,
//            'direction_fields'      :   this.model_exchange_direction,
//            'email'                 :   this.fields.clientEmail,
//            'phone'                 :   this.fields.phoneNumber,
//            'promo_code'                 :   this.fields.promoCode,
//            'income_amount'         :   this.inExchange.price,
//            'outcome_amount'        :   this.outExchange.price,
//            'income_payment_system' :   this.isIndexIn,
//            'outcome_payment_system':   this.isIndexOut
    }

    /**
     * Заявка отклонена
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function cancel(Request $request): JsonResponse
    {
        // Если нет доступа
        if(!$request->user()->tokenCan('cancel-order'))
        {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /order/cancel'
            ]);
        }

        $id = $request->has('id') ? $request->get('id') : 0;
        if($id == 0) {
            return response()->json([
                'error' => 'Order not found'
            ]);
        }

        $order = Task::where('public_id', $id)->first();

        if($order->status == 6) {
            return response()->json([
                'status' => 'cancel'
            ]);
        }

        // Страховка, в случае если клиент попытается обмануть.
        if(!in_array($order->status, [2, 9])) {
            throw new \Exception('Ошибка при создании заявки №'.$order->id);
        }

        // Записываем лог статус
        TaskStatusLog::create([
            'id_task'       => $order->id,
            'user_id'       => $order->id_user,
            'old_status'    => $order->status,
            'new_status'    => 6,
            'in_price'      =>  $order->give_price,
            'out_price'     =>  $order->receiving_price,
            'course_display'  => $order->course_display
        ]);

        $order->update([
            'status' => 6,
            'is_bot' => 0
        ]);

        return response()->json([
            'status' => 'cancel'
        ]);
    }

    /**
     * Подтверждение оплаты
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function confirm(Request $request): JsonResponse
    {
        // Если нет доступа
        if(!$request->user()->tokenCan('confirm-order'))
        {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /order/cancel'
            ]);
        }

        $validate = validator($request->all(), [
            'id' =>  'required|numeric|exists:tasks,public_id'
        ], [
            'income_code.min'    =>  'Укажите правильный "Номер чека" '
        ]);


        //Проверяем EXCode на доступность
        $validate->sometimes('income_code', 'required|min:15', function () use($request) {
            return $request->has('income_code');
        });

        if($validate->fails()) {
            $this->items = [
                'status' => 1,
                'message' => $validate->errors()->first()
            ];
        } else
        {
            $task =  Task::where('public_id', $request->id)->first();

            // Страховка, в случае если клиент попытается обмануть.
            if($task->status != 2)
            {
                return response()->json([
                    'status' => 1,
                    'message' => 'Order error'
                ]);
            }

            $type_pay = 3;


            // Записываем лог статус
            TaskStatusLog::create([
                'id_task'       => $task->id,
                'user_id'       => $task->id_user,
                'old_status'    => $task->status,
                'new_status'    => $type_pay,
                'in_price'      =>  $task->give_price,
                'out_price'     =>  $task->receiving_price,
                'place_change'  =>  0,
                'course_display'  => $task->course_display
            ]);

            $task->status           =   $type_pay;
            $task->lead_time        =   Carbon::now()->addMinutes((int)iEXSetting('lead_time_task'));
            $task->started_at       =   Carbon::now()->toDateTimeString();

            if($request->has('income_code')) {
                $task->income_code  = $request->income_code;
            }

            $task->save();

            if($type_pay == 3)
            {
                $tx = TransactionFacade::call($task->id);
                // Изменить резерв
                $tx->changeReserves();

                // Если заявка мошенническая то морозим
                if($task->task_info->is_freeze_scam == 1) {
                    $tx->setDeferType(5);
                    $tx->defer();
                } else {

                    if(iEXSetting('is_enabled_module_socket') == 1) {
                        broadcast(new OrderStatusesEvent($task));
                    }

                    // Уведомляем оператора о новой заявки
                    if(config('crypto.order_mail')) {
                        // Уведомляем оператора о новой заявки
                        dispatch(new AdminNewOrderJob($task))->delay(
                            now()->addSeconds(30)
                        )->onQueue('low');
                    }

                    // SMS Уведомление для оператора о новой заявке
                    if (iEXSetting('enable_sms_notification') and iEXSetting('sms_new_order')) {
                        $delay = now()->addMinute();
                        dispatch(new NexmoNewOrder($task))->delay($delay)->onQueue('low');
                    }

                    // Уведомлять о новых заявках в Telegram (Для операторов)
                    if((int)iEXSetting('enable_tg_notify_operator', 0) == 1) {
                        $delay = now()->addSeconds(15);
                        dispatch(new TelegramOrderJob($task))->delay($delay)->onQueue('high');
                    }
                }
            }
            $this->items = [
                'status' => 0,
                'result' => 'ok'
            ];
        }

        return response()->json($this->items);
    }

    /**
     * Валидация формы формы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFields(Request $request)
    {
        $rules = [
            'amount'                    =>   'required|numeric|min:0',
            'direction'                 =>   'required|min:1',
            'income_payment_system'     =>   'required|numeric|exists:currencies,id|regex:/^[0-9]+$/',
            'outcome_payment_system'    =>   'required|numeric|exists:currencies,id|regex:/^[0-9]+$/',
        ];

        if(iEXSetting('is_order_phone_number')) {
            $rules['phone']  = ['required', 'min:5'];
        }

        $validator = \validator($request->all(), $rules);

        //unique:users
        $validator->sometimes('email', 'email|required|max:150', function () {
            return \Auth::guest();
        });

        $validator->setAttributeNames([
            'email' => 'Email адрес',
            'income_amount' => 'Отдаю',
            'outcome_amount' => 'Получаю',
            'phone' => 'Ваш номер'
        ]);

        return $validator;
    }

    protected function calculation($item)
    {
        // Если есть курсы с городами, сделаем серверным
        $r = [
            'amount' => $item->course_value,
            'curs'   => $item->exchange_rate,
            'cur_str' => $item->exchange_rate_str
        ];
        if(\Module::find('Cities')->isEnabled() == 1 and $item->direction_exchange_cities->count() > 0) {
            $r = ConvertFacade::call($item)->all();
        }

        $arr_direction = [
            'r' =>  $r,
            'min_in' => $item->min_price1,
            'max_in' => $item->max_price1,
            'min_out' => $item->min_price2,
            'max_out' => $item->max_price2,
            'in_amount_decimal' => $item->currency1->number_format,
            'out_amount_decimal' => $item->currency2->number_format,
        ];

        if(\Module::find('Cities')->isEnabled() == 1 and $item->direction_exchange_cities->count() > 0)
        {
            $arr_direction['prices'] = [];
            foreach ($item->direction_exchange_cities as $city_price) {
                $arr_direction['prices'][$city_price['id']] = [
                    'min_price' => $city_price['min_price'],
                    'max_price' => $city_price['max_price']
                ];
            }
        }

        return $arr_direction;
    }
}
