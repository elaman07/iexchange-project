<?php

namespace App\Common\UserAPI\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PingController extends AbstractAPIController
{
    /**
     * Проверка соединения с API
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ping(Request $request): JsonResponse
    {
        return response()->json([
            'message' => 'PONG',
            'timestamp' => Carbon::now()->timestamp
        ]);
    }
}
