<?php

namespace App\Common\UserAPI\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

abstract class AbstractAPIController extends Controller
{
    protected string |null $api_key = null;
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function filter_column($array, $columns)
    {
        if(is_string($columns) == 'paginate')
        {
            if(isset($array['links'])) {
                unset($array['links']);
            }

            if(isset($array['meta']['links'])) {
                unset($array['meta']['links']);
            }

            if(isset($array['meta']['path'])) {
                unset($array['meta']['path']);
            }
        }

        return $array;
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public function viewJson($data): JsonResponse
    {
        return response()->json($data);
    }
}
