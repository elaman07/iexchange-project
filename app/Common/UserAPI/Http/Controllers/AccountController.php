<?php

namespace App\Common\UserAPI\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends AbstractAPIController
{
    /**
     * User Info
    */
    public function info(Request $request): \Illuminate\Http\JsonResponse
    {
        // Если нет доступа
        if(!$request->user()->tokenCan('account'))
        {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /account/info'
            ]);
        }

        $user_data = $request->user();

        return response()->json([
            'login' =>  $user_data->name,
            'email' =>  $user_data->email,
            'created_at' => $user_data->created_at->timestamp,
            'last_visit' => $user_data->last_activity_at->timestamp
        ]);
    }
}
