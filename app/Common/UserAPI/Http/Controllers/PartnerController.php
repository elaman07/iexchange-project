<?php

namespace App\Common\UserAPI\Http\Controllers;

use App\Common\Packages\Referral\Models\ReferralLog;
use App\Common\UserAPI\Http\Resources\{
    PartnerExchangesResource,
    PartnerPayoutResource
};
use App\Models\{
    ReferralStatistics,
    WithdrawalRequest
};
use Illuminate\Http\{JsonResponse, Request};

class PartnerController extends AbstractAPIController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);

        // Если нет доступа
        if(!$request->user()->tokenCan('partner'))
        {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /partner/*'
            ]);
        }
    }

    /**
     * Партнерская информация
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function info(Request $request): JsonResponse
    {
        return response()->json([
            'status' => 0,
            'data' => [
                'balance' =>  (float)$request->user()->user_balance->balance,
                'currency_payout' => config('crypto.currency_payout_sign'),
                'min_payout' =>  iEXSetting('minimum_bonus_payout')
            ]
        ]);
    }

    /**
     * Партнерские переходы
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function links(Request $request): JsonResponse
    {
        $exchanges = ReferralStatistics::with(['user'])
            ->leftJoin('referral_links', 'referral_statistics.ref_hash', '=', \DB::raw('referral_links.code'))
            ->leftJoin('users as referral_user', 'referral_links.user_id', '=', 'referral_user.id')
            ->select(
                'referral_statistics.ref_hash',
                'referral_statistics.user_agent',
                \DB::raw('count(*) as total'),
                'referral_statistics.created_at',
                'referral_statistics.from_and_to',
                'referral_statistics.cur_from',
                'referral_statistics.cur_to',
                'referral_statistics.id_user',
                'referral_statistics.ip_address',
                'referral_user.id as referral_user_id',
                'referral_user.name as referral_user_name',
                'referral_user.email as referral_user_email')
            ->where('ref_hash', $request->user()->referral_links->code);

        // С использованием дат (от / до)
        if($this->request->has('from_date') and $this->request->has('to_date')) {
            $exchanges = $exchanges->whereBetween('referral_statistics.created_at', [$this->request->get('from_date'), $this->request->get('to_date')]);
        }

        $exchanges = $exchanges->groupBy('from_and_to')
            ->whereNotNull('from_and_to')
            ->orderByDesc('total')->cursor()->map(function($item) {

                return [
                    'created_at' => $item->created_at->toDateTimeString(),
                    'ref_hash'   => $item->ref_hash,
                    'cur_from'   => $item->cur_from,
                    'cur_to'     => $item->cur_to,
                    'views'      => $item->total
                ];
            });

        return $this->viewJson([
            'status' => 0,
            'data' => $exchanges
        ]);
    }

    /**
     * Партнерские обмены
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function exchanges(Request $request): JsonResponse
    {
        $exchanges = ReferralLog::whereIdUser($request->user()->id);

        // С использованием дат (от / до)
        if($this->request->has('from_date') and $this->request->has('to_date'))
        {
            $exchanges = $exchanges->whereBetween('created_at', [
                $this->request->get('from_date'), $this->request->get('to_date')
            ]);
        }

        $exchanges = $exchanges->has('tasks')->paginate(20);
        $response = PartnerExchangesResource::collection($exchanges)->response()->getData(true);

        return $this->viewJson([
            'status' => 0,
            ...$this->filter_column($response, 'paginate')
        ]);
    }

    /**
     * Партнерские выплаты
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function payouts(Request $request): JsonResponse
    {
        $withdrawals = WithdrawalRequest::where('id_user', '=', $request->user()->id);

        // С использованием дат (от / до)
        if($this->request->has('from_date') and $this->request->has('to_date'))
        {
            $withdrawals = $withdrawals->whereBetween('created_at', [
                $this->request->get('from_date'),
                $this->request->get('to_date')
            ]);
        }

        $response = PartnerPayoutResource::collection($withdrawals->paginate(1))->response()->getData(true);

        return $this->viewJson([
            'status' => 0,
            ...$this->filter_column($response, 'paginate')
        ]);
    }
}
