<?php

namespace App\Common\UserAPI\Http\Controllers;

use App\Common\Packages\Crypto\Contracts\CompilerFactory;
use App\Common\Packages\Crypto\Facades\CompilerFacade;
use App\Common\Packages\Crypto\Facades\CryptoFacade;
use Illuminate\Http\Request;

class ExchangeController extends AbstractAPIController
{
    /**
     * Список платежных систем
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function details(Request $request): \Illuminate\Http\JsonResponse
    {
        // Если нет доступа
        if(!$request->user()->tokenCan('exchange')) {
            return response()->json([
                'status' => 1,
                'error' => 'You don\'t have access to /exchange/details'
            ]);
        }


        $response = CompilerFacade::compile()->toArray();
        $response['layout'] = 'api';
        unset($response['cs']['app_version']);
        unset($response['cs']['groups']);
        unset($response['cs']['filters']);
        unset($response['initialDirection']);

        return response()->json($response);
    }
}
