<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 28.10.2017
 * Time: 11:19
 */

namespace App\Common\Support\Traits;



use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

trait SendsPasswordResetEmails
{
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        //
        $array = [];

        $validator = $this->getValidationFactory()->make($request->all(), [
            'email' => 'required|email'
        ]);

        if(!$validator->fails())
        {
            $check = User::where('email', $request->email);

            if($check->count() == 0) {
                $array['status']    =   1;
                $array['message']   =   'Увы, мы не можем найти ваш E-mail адрес';
            }else {
                $array['status'] = 0;

                // We will send the password reset link to this user. Once we have attempted
                // to send the link, we will examine the response then see the message we
                // need to show to the user. Finally, we'll send out a proper response.
                $response = $this->broker()->sendResetLink(
                    $request->only('email')
                );

              //  $this->sendResetLinkResponse($response);
            }


        }else {
            $array['status'] = 1;
        }


        return response()->json($array);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse($response)
    {
        return back()->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()->withErrors(
            ['email' => trans($response)]
        );
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }
}
