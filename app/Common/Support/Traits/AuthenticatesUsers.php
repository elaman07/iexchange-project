<?php
namespace App\Common\Support\Traits;

use App\Http\Resources\Sessions\CurrentUserResource;
use App\Jobs\UserLogJob;
use App\Models\User;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins, ErrorTrait;

    /**
     * Обработка авторизации
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|exists:users',
            'password' => 'required',
        ]);

        if($validator->fails())
        {
            $this->items['status'] = 1;
            $this->items['error'] = $validator->messages()->first();

        }else
        {
            //Дополнительная валидация полученных параметров
            $this->validateLogin($request);

            // Если класс использует признак ThrottlesLogins, мы можем автоматически дроссельной заслонки
            // Логина попытки для этого приложения. Мы это ключ от имени пользователя и
            // IP-адрес клиента, что делает эти запросы в это приложение.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return $this->sendLockoutResponse($request);
            }

            // Статус активации пользователя
            $user_info = User::whereEmail($request->get('email'))->first();
            if(isset($user_info) and $user_info->deactivation == 1)
            {
                $this->items['status'] = 1;
                $this->items['error'] = 'Ваша Учетная запись деактивирована';

            }elseif($user_info->isBanned()) {

                $this->items['status'] = 1;
                $this->items['error'] = 'Ваша Учетная запись заблокировно до '.$user_info->banned_at;

            }elseif (Auth::validate($this->credentials($request)) == true)
            {
                if($this->attemptLogin($request)) {
                    $this->sendLoginResponse($request);
                    $this->items = new CurrentUserResource($user_info);
                }else {
                    $this->items['status'] = 1;
                }
            }else {
                $this->items['status'] = 1;
            }

            // Записываем неудачную попытку авторизации в лог
            if(iEXSetting('event_log_is_login_fail') and $this->items['status'] == 1)
            {
                $log = sprintf('Пользователь %s неудачно авторизовался', $user_info->name);
                main_event_log('event_log_login_fail', $log, $user_info->id);
            }

            $this->incrementLoginAttempts($request);
        }
        return response()->json($this->items);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);

        $this->authenticated($request, $this->guard()->user());
    }

    /**
     * В случае множественных неудачных авторизаций
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return $this->fieldError(Lang::get('auth.throttle', ['seconds' => $seconds]));
    }

    /**
     * Get the throttle key for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function throttleKey(Request $request)
    {
        if(iEXSetting('is_ip_throttle_key'))
            return $request->ip();
        return Str::lower($request->input($this->username())).'|'.$request->ip();
    }

    /**
     * Проверка имени пользователя
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required',
        ]);
    }

    protected function attemptLogin(Request $request)
    {
        $remember = iEXSetting('is_remember_login') == 1;
        return $this->guard()->attempt(
            $this->credentials($request), $remember
        );
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    public function username()
    {
        return 'email';
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();

        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Get the maximum number of attempts to allow.
     *
     * @return int
     */
    public function maxAttempts()
    {
        return config('auth.max_attempts');
    }

    /**
     * Get the number of minutes to throttle for.
     *
     * @return int
     */
    public function decayMinutes()
    {
        return config('auth.decay_minutes');
    }
}
