<?php
namespace App\Common\Support\Traits;


trait ErrorTrait
{
    /**
     * Собираем результаты
     *
     * @var array
    */
    protected $items;

    /**
     * Выводим ошибки
     *
     * @param $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function fieldError($value)
    {
        return response()->json([
            'status' => 1,
            'error' => $value
        ]);
    }
}