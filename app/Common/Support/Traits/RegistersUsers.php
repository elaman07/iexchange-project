<?php
namespace App\Common\Support\Traits;

use App\Http\Resources\Sessions\CurrentUserResource;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait RegistersUsers
{
    use RedirectsUsers, ErrorTrait;

    /**
     * Request объект
     *
     * @var Request
    */
    protected $request;

    /**
     * Регистрационная форма
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {
        return view('auth.register');
    }

    /**
     * Обработчик регистрации нового клиента
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        $this->request = $request;

        //Ограничить регистрацию клиентов
        if(config('auth.max_users') > 0 and User::count() >= config('auth.max_users')) {
            return $this->fieldError('Сервис не принимает новых клиентов');
        }

        // Фильтр при регистрации нового пользователя
        $filter_register = isFilterBanned($this->request->get('email'));
        if(is_array($filter_register)) {
            return $this->fieldError(
                'Администратором было запрещено использовать данный e-mail, по причине: '. $filter_register['description']
            );
        }

        // Если включена регистрация с одного IP
        if(iEXSetting('reg_multi_ip') == 0) {
            if (User::where('ip_address', $this->request->ip())->exists() == true) {
                return $this->fieldError('Клиент в этим IP уже зарегистрирован');
            }
        }

        // Если найдены ошибки в валидаторе формы
        if($validator->fails()) {
            $this->items['status'] = 1;
            $this->items['error'] = $validator->messages()->first();
        }else {

            // Если все удачно, регистрируем пользователя
            event(new Registered($user = $this->create($request->all())));
            $this->guard()->login($user);
            $this->registered($request, $user) ?: redirect($this->redirectPath());
            $this->items = new CurrentUserResource($user);
        }

        return response()->json($this->items);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
