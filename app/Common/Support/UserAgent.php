<?php
namespace App\Common\Support;

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\AbstractDeviceParser;

class UserAgent
{
    protected $userAgent   =   [];

    public function __construct()
    {
        if(isset($_SERVER['HTTP_USER_AGENT'])) {
            AbstractDeviceParser::setVersionTruncation(AbstractDeviceParser::VERSION_TRUNCATION_NONE);
            $this->userAgent = new DeviceDetector($_SERVER['HTTP_USER_AGENT']);
            $this->userAgent->discardBotInformation();
            $this->userAgent->skipBotDetection();
            $this->userAgent->parse();
        }
    }

    /***
     * Получение операционной системы
     */
    public function getOS()
    {
        if(empty($this->userAgent))
            return null;
        return $this->userAgent->getOs('name').' '.$this->userAgent->getOs('version').' ('.$this->userAgent->getOs('platform').')';
    }

    /***
     * Информация о UserAgent
     */
    public function getUserAgent()
    {
        if(empty($this->userAgent))
            return null;
        return $this->userAgent->getUserAgent();
    }

    /***
     * Получение информации о браузере
     */
    public function getClient()
    {
        if(empty($this->userAgent))
            return null;
        return $this->userAgent->getClient('name').' '.$this->userAgent->getClient('version');
    }

    /***
     * Получение IP адреса
     */
    public function getIP()
    {
        if(empty($this->userAgent))
            return null;
        return request()->ip();
    }
}
