<?php

namespace App\Common\Support;

use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

trait ConfirmAuthOrderPageHandler
{
    /**
     * Ключ сессии
     *
     * @var string
     */
    protected string $key = 'iex-security-code-order-page';

    /**
     * Код для проверки неудачных попыток
     *
     * @var string
     */
    protected $throttleSuffix = '::codeConfirmOrderPage';

    /**
     * Получаем детали кэшированных резервных кодов
     *
     * @return mixed
     */
    public function getCurrentCode()
    {
        return config('security-codes.order-page');
    }

    /**
     * Записываем данные в базу и в сессиюю
     *
     * @param Request $request
     */
    public function insertCode(Request $request)
    {
        // Записываем код в базу
        $secret = hash('sha256', $request->get('code').'-'.$request->user()->id);
        // Обновляем ключ в базе
        $request->user()->update(['security_order_page_code' => $secret]);
        // Записываем в сессию
        \Session::put($this->key.$request->user()->id, $secret);
    }

    /**
     * Проверка полученного кода
     *
     * @param Request $request
     * @return bool
     */
    public function checkedCode(Request $request)
    {
        $code = $this->getCurrentCode();
        if(!$code) return false;
        return $code == $request->get('code');
    }

    /**
     * Определяем, есть ли у пользователя слишком много неудачных попыток ввода резервного кода.
     *
     * @param  \Illuminate\Http\Request $request
     * @param RateLimiter $limiter
     * @return bool
     */
    private function hasTooManyRecoveryAttempts(Request $request, RateLimiter $limiter)
    {
        return $limiter->tooManyAttempts(
            $this->throttleKey($request), $this->maxAttempts()
        );
    }

    /**
     * В случае множественных неудачных вводов, блокируем все действия
     *
     * @param Request $request
     * @param RateLimiter $limiter
     * @return string
     */
    protected function sendLockoutResponse(Request $request, RateLimiter $limiter)
    {
        $availableAt = now()->addSeconds(
            $limiter->availableIn($this->throttleKey($request))
        )->ago();
        return sprintf("Слишком много попыток ввода. Пожалуйста, попробуйте еще раз %s ", $availableAt);
    }

    /**
     * Увеличиваем количество неудачных попыток ввода резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     */
    protected function incrementAttempts(Request $request, RateLimiter $limiter) {
        $limiter->hit(
            $this->throttleKey($request), 320 * 60
        );
    }

    /**
     * Очищаем попытки ввода резервного кода
     *
     * @param Request $request
     * @param RateLimiter $limiter
     */
    protected function clearAttempts(Request $request, RateLimiter $limiter)
    {
        $limiter->clear(
            $this->throttleKey($request)
        );
    }

    /**
     * Ключ, для проверки кол-во неудачно введенных кодов
     *
     * @param Request $request
     * @return string
     */
    private function throttleKey(Request $request)
    {
        return $request->ip() . '|' . $request->user()->id . $this->throttleSuffix;
    }

    /**
     * Макс. количество неудачных попыток ввода резервного кода
     *
     * @return integer
     */
    protected function maxAttempts()
    {
        return 5;
    }
}
