<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 29.02.2020
 * Time: 22:07
 */

namespace App\Common\Support;


class SetupHelpers
{
    public static function getMemoryLimit()
    {
        $limit_string = ini_get('memory_limit');
        $unit = strtolower(mb_substr($limit_string, -1 ));
        $bytes = intval(mb_substr($limit_string, 0, -1), 10);

        switch ($unit)
        {
            case 'k':
                $bytes *= 1024;
                break 1;

            case 'm':
                $bytes *= 1048576;
                break 1;

            case 'g':
                $bytes *= 1073741824;
                break 1;

            default:
                break 1;
        }

        return $bytes;
    }
}