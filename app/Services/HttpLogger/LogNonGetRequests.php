<?php
namespace App\Services\HttpLogger;

use Illuminate\Http\Request;

class LogNonGetRequests extends \Spatie\HttpLogger\LogNonGetRequests
{
    public function shouldLogRequest(Request $request): bool
    {
        return in_array(strtolower($request->method()), ['post', 'put', 'patch', 'delete']);
    }
}