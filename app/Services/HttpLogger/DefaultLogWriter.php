<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 04.03.2019
 * Time: 8:09
 */

namespace App\Services\HttpLogger;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DefaultLogWriter extends \Spatie\HttpLogger\DefaultLogWriter
{
    public function logRequest(Request $request): void
    {
        $method = strtoupper($request->getMethod());

        $uri = $request->getPathInfo();

        $bodyAsJson = json_encode($request->except(config('http-logger.except')));

        $files = array_map(function (UploadedFile $file) {
            return $file->getClientOriginalName();
        }, iterator_to_array($request->files));

        $ip = $request->ip();
        $message = "{$method} {$uri} - Body: {$bodyAsJson} - IP {$ip} - Files: ".implode(', ', $files);

        Log::info($message);
    }
}