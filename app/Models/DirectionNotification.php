<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 23.06.2019
 * Time: 12:26
 */

namespace App\Models;


use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DirectionNotification
 *
 * @property int $id
 * @property int $id_direction_exchange
 * @property string|null $description
 * @property string $css_class
 * @property int $sorting
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_enabled_schedule
 * @property string|null $from_time
 * @property string|null $to_time
 * @property-read \App\Models\DirectionExchange $direction_exchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereCssClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereFromTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereIdDirectionExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereIsEnabledSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereToTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionNotification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DirectionNotification extends Model
{
    use HasTranslations;

    protected $table = 'direction_notification';

    protected $fillable = [
        'id_direction_exchange',
        'description',
        'sorting',
        'status',
        'is_enabled_schedule',
        'from_time',
        'to_time',
        'title',
        'is_order_detail',
        'text_color',
        'bg_color'
    ];

    public $translatable  = [
        'description',
        'title'
    ];

    public function direction_exchange() {
        return $this->hasOne(DirectionExchange::class,'id','id_direction_exchange');
    }
}
