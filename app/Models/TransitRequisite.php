<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;


class TransitRequisite extends Model
{
    use Filterable;

    protected $table = 'transit_requisites';

    protected $fillable = [
        'id_currency',
        'account',
        'status',
        'view',
        'received'
    ];

    public function currency()
    {
        return $this->hasOne(Currency::class,'id','id_currency');
    }
}
