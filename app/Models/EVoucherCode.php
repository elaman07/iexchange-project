<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EVoucherCode
 *
 * @property int $id
 * @property int $id_task
 * @property string|null $account
 * @property string|null $amount
 * @property string|null $batch_num
 * @property string|null $voucher_num
 * @property string|null $voucher_code
 * @property string|null $voucher_amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereBatchNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereVoucherAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereVoucherCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EVoucherCode whereVoucherNum($value)
 * @mixin \Eloquent
 */
class EVoucherCode extends Model
{
    protected $table = 'e_voucher_codes';

    protected $fillable = [
        'id_task',
        'account',
        'amount',
        'batch_num',
        'voucher_num',
        'voucher_code',
        'voucher_amount'
    ];
}
