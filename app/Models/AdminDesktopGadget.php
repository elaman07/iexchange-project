<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminDesktopGadget
 *
 * @property int $id
 * @property int $id_desktop
 * @property string|null $name
 * @property string|null $alias
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $column_id
 * @property int $id_user
 * @property string|null $hash_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereColumnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereHashId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereIdDesktop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktopGadget whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $id_widget
 * @method static \Illuminate\Database\Eloquent\Builder|AdminDesktopGadget whereIdWidget($value)
 */
class AdminDesktopGadget extends Model
{
    protected $table = 'admin_desktop_gadgets';

    protected $fillable = [
        'id_desktop',
        'hash_id',
        'id_widget',
        'id_user',
        'column_id',
        'name',
        'alias',
        'sorting'
    ];
}