<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FavoriteLink
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $link
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $id_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FavoriteLink whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FavoriteLink extends Model
{
    protected $table = 'favorites_links';

    protected $fillable = [
        'name',
        'link',
        'id_user',
        'sorting'
    ];
}
