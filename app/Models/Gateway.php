<?php

namespace App\Models;

use App\Models\Casts\JsonCasts;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'alias',
        'class_name',
        'status',
        'is_merchant',
        'is_pay',
        'is_check_pay',
        'is_rpc',
        'version',
        'security_options',
    ];


    protected $casts = [
        'security_options' => JsonCasts::class,
    ];
}
