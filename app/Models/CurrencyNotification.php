<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyNotification
 *
 * @property int $id
 * @property int $id_currency
 * @property string|null $description
 * @property string|null $css_class
 * @property int $sorting
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Currency $currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereCssClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyNotification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CurrencyNotification extends Model
{
    protected $table = 'currencies_notification';

    protected $fillable = [
        'id_currency',
        'description',
        'css_class',
        'sorting',
        'status'
    ];

    public function currency() {
        return $this->hasOne(Currency::class,'id','id_currency');
    }
}
