<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BestchangeBlHistory extends Model
{
    protected $table = 'bestchange_bl_histories';

    protected $fillable = [
        'bs_id',
        'type',
        'date',
        'author',
        'contacts',
        'desc'
    ];
}
