<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BestchangeCurrency
 *
 * @property int $id
 * @property int $bestchange_id
 * @property string|null $name
 * @property int $sorting
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereBestchangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeCurrency whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BestchangeCurrency extends Model
{
    protected $table = 'bestchange_currencies';

    protected $fillable = [
        'bestchange_id',
        'name',
        'sorting',
        'status'
    ];
}
