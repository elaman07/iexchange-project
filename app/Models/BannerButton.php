<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class BannerButton extends Model
{
    use HasTranslations;

    protected $table = 'banners_buttons';

    public $translatable = ['name', 'link'];

    protected $fillable = [
        'name',
        'link',
        'color_text_button',
        'color_bg_button'
    ];
}
