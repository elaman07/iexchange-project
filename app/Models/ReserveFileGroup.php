<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ReserveFileGroup extends Model
{
    protected $table = 'reserves_files_groups';

    protected $fillable = [
        'name',
        'status',
        'link',
    ];

    public function reserves_files()
    {
        return $this->hasMany(ReserveFile::class,'id_group','id');
    }
}
