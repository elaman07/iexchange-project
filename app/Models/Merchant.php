<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Merchant
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $status
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_merchant Мерчант
 * @property int $is_pay Выплата
 * @property float $version
 * @property int $is_rpc
 * @property int $is_api
 * @property int $is_action
 * @property int $is_option
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsApi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsMerchant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsRpc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereVersion($value)
 * @property int $is_check_pay
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsCheckPay($value)
 * @property string|null $class_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereClassName($value)
 * @property int $is_reserve
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsReserve($value)
 * @property int $is_other_service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Merchant whereIsOtherService($value)
 */
class Merchant extends Model
{
    protected $fillable = [
        'name',
        'alias',
        'status',
        'is_merchant',
        'is_pay',
        'is_reserve',
        'is_check_pay',
        'is_api',
        'is_rpc',
        'version',
        'is_action',
        'is_other_service',
        'description',
        'is_option'
    ];

    public function supported()
    {
        return in_array($this->alias, [
            'westwallet'
        ]);
    }
}
