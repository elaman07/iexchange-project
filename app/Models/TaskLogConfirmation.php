<?php

namespace App\Models;

use App\Models\Filters\TaskLogConfirmationFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskLogConfirmation
 *
 * @property int $id
 * @property int $id_task
 * @property int $confirmation
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereConfirmation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLogConfirmation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskLogConfirmation extends Model
{
    use Filterable;

    protected $table = 'task_log_confirmation';

    protected $fillable = [
        'id_task',
        'confirmation'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(TaskLogConfirmationFilter::class);
    }
}
