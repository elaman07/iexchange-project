<?php
namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TaskRequisites extends Model
{
    use Filterable;

    protected $table = "tasks_requisites";

    protected $fillable = [
        'type',
        'account_number',
        'id_task',
        'id_direction_exchange',
        'id_currency'
    ];
}
