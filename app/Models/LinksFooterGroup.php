<?php
namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class LinksFooterGroup extends Model
{
    use HasTranslations;

    protected $table = 'links_footer_groups';

    protected $fillable = [
        'name',
        'sorting'
    ];

    public $translatable = ['name'];

    public function links_footer() {
        return $this->hasMany(LinksFooter::class,'id_group','id');
    }

    public function links_footer_large(): HasMany
    {
        return $this->hasMany(LinksFooter::class,'id_group','id')->where('is_review', '=', 1)->orderBy('sorting');
    }

    public function links_footer_url(): HasMany
    {
        return $this->hasMany(LinksFooter::class,'id_group','id')->where('is_review', '=', 0)->orderBy('sorting');
    }
}
