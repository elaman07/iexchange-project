<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Translatable\HasTranslations;


class RequisiteField extends Model
{
    use HasTranslations;

    protected $table = 'requisites_fields';

    public $translatable = ['comment', 'name'];

    protected $fillable = [
        'name',
        'value',
        'status',
        'sorting',
        'id_currency',
        'comment',
        'prefix'
    ];


    public function currencies(): MorphToMany
    {
        return $this->morphedByMany(Currency::class, 'model','currency_requisites_has_fields','field_id');
    }
}
