<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParserApiKey
 *
 * @property int $id
 * @property int $id_type
 * @property string|null $api_key
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $view_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereApiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereIdType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserApiKey whereViewCount($value)
 * @mixin \Eloquent
 */
class ParserApiKey extends Model
{
    protected $table = 'parser_api_keys';

    protected $fillable = [
        'id_type',
        'api_key',
        'status',
        'view_count',
        'provider_id'
    ];
}
