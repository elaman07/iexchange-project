<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TaskField extends Model
{
    protected $table = 'tasks_fields';

    protected $fillable = [
        'id_task',
        'id_field',
        'field_name',
        'field_value',
        'type_field',
        'alias'
    ];

    public function requisites_fields()
    {
        return $this->hasOne(RequisiteField::class,'id','id_field');
    }
}
