<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 02.10.2017
 * Time: 16:47
 */

namespace App\Models;


use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $name
 * @property string|null $cr_text
 * @property string|null $text
 * @property string|null $image
 * @property string $parent_url
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCrText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereParentUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $views
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereViews($value)
 * @property string|null $slug_name
 * @method static \Illuminate\Database\Eloquent\Builder|News whereSlugName($value)
 */
class News extends Model
{
    use HasTranslations;
    protected $table = 'news';

    public $translatable = ['name', 'text'];


    protected $fillable = [
        'name',
        'cr_text',
        'text',
        'parent_url',
        'image',
        'views',
        'slug_name',
        'is_local_image'
    ];
}
