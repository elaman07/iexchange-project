<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 24.03.2019
 * Time: 11:22
 */

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class FileStorageModel extends Model
{
    protected $table = 'file_storages';

    protected $fillable = [
        'id_manager',
        'name',
        'disk',
        'disk_type',
        'status'
    ];
}
