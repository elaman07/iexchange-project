<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FileParserGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int $status
 * @property string $link
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FileParserRates[] $file_parser_rates
 * @property-read int|null $file_parser_rates_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FileParserRates[] $rates
 * @property-read int|null $rates_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FileParserGroup extends Model
{
    protected $table = 'file_parser_groups';

    protected $fillable = [
        'name',
        'status',
        'link',
        'sorting'
    ];

    public function rates() {
        return $this->hasMany(FileParserRates::class,'id_group', 'id');
    }

    public function file_parser_rates() {
        return $this->hasMany(FileParserRates::class,'id_group', 'id')->where('status', '=', 1);
    }
}
