<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BannedUser
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_task
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BannedUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BannedUser extends Model
{
    protected $table = 'banned_user';

    protected $fillable = [
        'id_user',
        'id_task',
        'expired_at',
        'comment'
    ];
}
