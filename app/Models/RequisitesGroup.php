<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RequisitesGroup
 *
 * @property int $id
 * @property int $id_currency
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @property int $sorting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Requisites[] $requisites
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesGroup whereStatus($value)
 * @property-read int|null $requisites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Requisites[] $requisites_all
 * @property-read int|null $requisites_all_count
 */
class RequisitesGroup extends Model
{
    protected $table = 'requisites_group';

    protected $fillable = [
        'name',
        'status',
        'sorting'
    ];

    public function requisites() {
        return $this->hasMany(Requisites::class,'id_group', 'id')->where('is_history', '=', 0)->filter([]);
    }

    public function requisites_all() {
        return $this->hasMany(Requisites::class,'id_group', 'id');
    }


    public function getRequisitesPaginatedAttribute()
    {
        return $this->requisites()->paginate(10);
    }
}
