<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

/**
 * App\Models\Employees
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $started_work
 * @property int $job_from
 * @property int $job_to
 * @property float $salary
 * @property int $fines
 * @property float|null $total
 * @property int $percent
 * @property float $bonus_rub
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EventEmployees[] $event_employees
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FineEmployees[] $fine_employees
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereBonusRub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereFines($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereJobFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereJobTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereStartedWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Employees whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $event_employees_count
 * @property-read int|null $fine_employees_count
 */
class Employees extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'id_user',
        'started_work',
        'job_from',
        'job_to',
        'salary',
        'fines',
        'total',
        'percent',
        'bonus_rub',
        'status'
    ];

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function fine_employees()
    {
        $month = Carbon::today()->startOfMonth();
        $month_end = Carbon::today()->endOfMonth();

        if(request()->has('last_month')) {
            $month = Carbon::today()->subMonth(1)->startOfMonth();
            $month_end = Carbon::today()->subMonth(1)->endOfMonth();
        }

        return $this->hasMany(FineEmployees::class,'id_employees', 'id')
            ->whereBetween('created_at', [$month, $month_end]);
    }

    public function event_employees()
    {
        $month = Carbon::today()->startOfMonth();
        $month_end = Carbon::today()->endOfMonth();

        if(request()->has('last_month')) {
            $month = Carbon::today()->subMonth(1)->startOfMonth();
            $month_end = Carbon::today()->subMonth(1)->endOfMonth();
        }

        return $this->hasMany(EventEmployees::class,'id_employees','id')
            ->whereBetween('created_at', [$month, $month_end]);
    }

    /**
     * Подсчитываем итоговую зарплату менеджера
     *
     * @return float|integer
     */
    public function total()
    {
        $fine = $this->fine_employees()->sum('amount');
        $bonus = $this->event_employees()->sum('amount');

        $total = ($this->getAttribute('salary') + $bonus - $fine);
        return $total;
    }
}
