<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DocsCategory
 *
 * @property int $id
 * @property int $id_type
 * @property int $parent_id
 * @property string $parent_url
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\DocsCategoryType $docs_category_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DocsItem[] $docs_items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereIdType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereParentUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $docs_items_count
 */
class DocsCategory extends Model
{
    protected $table = 'docs_category';

    protected $fillable = [
        'id_type',
        'parent_id',
        'parent_url',
        'name'
    ];

    /***
     * Данные из типов категории
     */
    public function docs_category_type()
    {
        return $this->hasOne(DocsCategoryType::class,'id','id_type');
    }

    public function docs_items() {

        return $this->hasMany(DocsItem::class,'category_id','id');
    }
}
