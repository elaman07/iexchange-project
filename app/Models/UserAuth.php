<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserAuth
 *
 * @property int $id
 * @property int $id_user
 * @property int $source
 * @property string|null $browser
 * @property string|null $ip
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $os
 * @property int $status
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereOs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $country
 * @property string|null $city
 * @property string|null $iso_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereIsoCode($value)
 * @property string|null $old_ip_address
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereOldIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereLike($column, $value, $boolean = 'and')
 * @property string|null $user_agent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuth whereUserAgent($value)
 */
class UserAuth extends Model
{
    use Filterable;

    protected $table = 'user_auth';

    protected $fillable = [
        'id_user',
        'browser',
        'source',
        'os',
        'ip',
        'user_agent',
        'old_ip_address',
        'status',
        'country',
        'city',
        'iso_code'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\UserAuthLogFilter::class);
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }
}
