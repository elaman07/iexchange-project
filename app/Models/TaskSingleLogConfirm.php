<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskSingleLogConfirm extends Model
{

    protected $table = 'task_single_log_confirm';

    protected $fillable = [
        'id_task',
        'needed_confirm',
        'received_confirm',
        'transaction_id'
    ];
}
