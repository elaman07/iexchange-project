<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BestchangeParserError
 *
 * @property int $id
 * @property int $id_bestchange
 * @property int $status
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $id_direction_exchange
 * @property-read \App\Models\BestChangeRatesModel $bestchange_rates
 * @property-read \App\Models\DirectionExchange $direction_exchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereIdBestchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereIdDirectionExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeParserError whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BestchangeParserError extends Model
{
    protected $table = 'bestchange_parser_error';

    protected $fillable = [
        'id_bestchange',
        'id_direction_exchange',
        'status',
        'description'
    ];

    public function bestchange_rates() {
        return $this->hasOne(BestChangeRatesModel::class,'id','id_bestchange');
    }

    public function direction_exchange() {
        return $this->hasOne(DirectionExchange::class,'id','id_direction_exchange');
    }
}
