<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ParserFormulaCoefficient extends Model
{
    protected $table = 'parser_formula_coefficient';

    protected $fillable = [
        'name',
        'summa',
        'alias',
    ];
}
