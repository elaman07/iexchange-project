<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskPrivateHash
 *
 * @property int $id
 * @property int $id_task
 * @property string|null $hash
 * @property string|null $provider
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskPrivateHash whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskPrivateHash extends Model
{
    protected $table = 'task_private_hash';

    protected $fillable = [
        'id_task',
        'hash',
        'provider'
    ];
}