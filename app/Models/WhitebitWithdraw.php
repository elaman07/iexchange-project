<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WhitebitWithdraw extends Model
{
    protected $table = 'whitebit_withdraw';

    protected $fillable = [
        'method_type',
        'id_task',
        'address',
        'amount',
        'currency',
        'ticker',
        'fee',
        'status',
        'transaction_hash',
        'unique_id'
    ];
}
