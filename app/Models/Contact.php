<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Contact
 *
 * @property int $id
 * @property string|null $name
 * @property string $type
 * @property string|null $value
 * @property string|null $url
 * @property int $block_size
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sorting
 * @property int $is_home
 * @property int $is_button
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereBlockSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereIsButton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereIsHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Contact whereValue($value)
 * @mixin \Eloquent
 */
class Contact extends Model
{
    use HasTranslations;

    protected $table = 'contacts';

    protected $fillable = [
        'name',
        'type',
        'value',
        'url',
        'block_size',
        'sorting',
        'is_home',
        'is_button',
        'icon',
        'text_color'
    ];

    protected $translatable = ['name', 'value', 'url'];
}
