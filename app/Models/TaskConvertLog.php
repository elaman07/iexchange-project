<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TaskConvertLog
 *
 * @property int $id
 * @property int $id_task
 * @property float|null $to_usd
 * @property float|null $to_rub
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereToRub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereToUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TaskConvertLog onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskConvertLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TaskConvertLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TaskConvertLog withoutTrashed()
 */
class TaskConvertLog extends Model
{
    use SoftDeletes;

    protected $table = 'tasks_convert_log';

    protected $fillable = [
        'id_task',
        'to_usd',
        'to_rub'
    ];

    public function task() {
        return $this->hasOne(Task::class,'id','id_task');
    }
}
