<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Modules\ProxyManager\Entities\ProxyModel;

/**
 * App\Models\GatewayPayment
 *
 * @property int $id
 * @property string|null $name
 * @property int $id_gateways
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $max_register_blockchain
 * @property int $max_first_confirm_blockchain
 * @property int $min_confirm
 * @property int $id_proxy
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Currency[] $currency
 * @property-read int|null $currency_count
 * @property-read \App\Models\Merchant $gateway
 * @property-read \App\Models\ProxyPayment $proxy
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereIdGateways($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereIdProxy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereMaxFirstConfirmBlockchain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereMaxRegisterBlockchain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereMinConfirm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $comment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayPayment whereComment($value)
 * @property int $manual_pay_order
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayPayment whereManualPayOrder($value)
 */
class GatewayPayment extends Model
{
    use HasTranslations;

    protected $table = 'gateways_payments';

    protected $fillable = [
        'name',
        'max_register_blockchain',
        'max_first_confirm_blockchain',
        'min_confirm',
        'id_gateways',
        'status',
        'id_proxy',
        'comment',
        'manual_pay_order',
        'method_pay',
        'country_code',
        'num_request',
        'priority_fee',
        'exchange_buy',
        'exchange_fee',
        'exchange_buy_type',
        'time_in_force',
        'is_auto_take_fee',
        'exchange_buy_curr',
        'volume_to_usd',
        'last_order_id',
        'order_count',
        'mass_coins',
        'is_mass_payouts',
        'hide_check_balance',
        'is_subtract',
        'currency_code',
        'pay_amount_type',
        'direction',
        'site_account',
        'bank_name',
        'code_currency'
    ];

    protected array $translatable = [
        'comment'
    ];

    public function gateway() {
        return $this->hasOne(Gateway::class,'id', 'id_gateways');
    }

    public function currency() {
        return $this->hasMany(Currency::class,'id_pay', 'id');
    }

    public function proxy() {
        return $this->hasOne(ProxyModel::class,'id', 'id_proxy');
    }
}
