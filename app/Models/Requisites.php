<?php
namespace App\Models;


use Carbon\Carbon;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ProxyManager\Entities\ProxyModel;
use Spatie\Translatable\HasTranslations;


class Requisites extends Model
{
    use SoftDeletes, Filterable, HasTranslations;

    protected $table = "requisites";

    /**
     * Атрибуты, которые должны быть видоизменены по датам.
     *
     * @var array
     */
    protected $casts = [
        'deleted_at' => 'datetime',
        'history_at' => 'datetime'
    ];

    protected $fillable = [
        'account_number',
        'id_currency',
        'id_user',
        'id_group',
        'status',
        'limit_day',
        'limit_week',
        'limit_month',
        'view',
        'limit_views',
        'recipient',
        'is_history',
        'history_at',
        'is_enabled_merchant',
        'id_proxy',
        'is_drain',
        'max_wallet_limit',
        'drain_comment',
        'drain_room',
        'is_unique_shot',
        'photo_name',
        'photo_status',
        'text_request_payment'
    ];

    public $translatable = ['text_request_payment'];


    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\RequisitesFilter::class);
    }

    public function drain_requisites() {
        return $this->hasOne(Requisites::class,'id','drain_room');
    }

    /**
     * Запрос на включенение только активных счетов
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActiveWallet($query)
    {
        return $query->where('status', '=',1)
            ->where('is_history', '=', 0);
    }

    /**
     * Информационные поля
     */
    public function requisites_info_fields(): MorphToMany
    {
        return $this->morphToMany(
            RequisiteInfoField::class,
            'model',
            'requisites_has_info_fields',
            'model_id',
            'field_id'
        );
    }
    /**
     * Запрос на включение только архивированных реквизитов
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsHistory($query)
    {
        return $query->where('is_history', '=', 1);
    }

    /**
     * Запрос на отключение архивированных реквизитов
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsNotHistory($query)
    {
        return $query->where('is_history', '=', 0);
    }

    public function currency() {
        return $this->hasOne(Currency::class,'id','id_currency');
    }

    public function requisites_list() {
        return $this->hasOne(RequisitesList::class,'id_requisites','id')->inRandomOrder();
    }

    public function requisites_single() {
        return $this->hasOne(RequisitesList::class,'id_requisites','id')->where('status','=',0);
    }

    public function requisites_list2() {
        return $this->hasMany(RequisitesList::class,'id_requisites','id')->where('status','=',0);
    }

    public function tasks(){
        return $this->hasOne(Task::class,'id_payment_requisites','id');
    }

    public function tasks_many() {
        return $this->hasMany(Task::class,'id_payment_requisites','id');
    }


    public function summa_day()
    {
        $summa = (float)$this->hasOne(Task::class,'id_payment_requisites', 'id')
            ->whereBetween('created_at', [
                Carbon::now()->startOfDay(),
                Carbon::now()->endOfDay()
            ])->where('status', '=', 4)->sum('give_price');


        return  number_format($summa, $this->currency->number_format,'.', ' ');
    }

    public function summa_month()
    {
        $summa = $this->hasOne(Task::class,'id_payment_requisites', 'id')
            ->whereBetween('created_at', [
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth()
            ])->where('status', '=', 4)->sum('give_price');

        return number_format($summa, $this->currency->number_format,'.', ' ');
    }

    public function summa_week()
    {
        $summa = $this->hasOne(Task::class,'id_payment_requisites', 'id')
            ->whereBetween('created_at', [
                Carbon::now()->startOfWeek(),
                Carbon::now()->endOfWeek()
            ])->where('status', '=', 4)->sum('give_price');

        return number_format($summa, $this->currency->number_format,'.', ' ');
    }

    public function proxy() {
        return $this->hasOne(ProxyModel::class,'id','id_proxy');
    }
}
