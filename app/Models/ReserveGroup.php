<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReserveGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int $id_user
 * @property int $status
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reserve[] $reserve
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $reserve_count
 */
class ReserveGroup extends Model
{
    protected $table = 'reserve_group';

    protected $fillable = [
        'name',
        'id_user',
        'status',
        'sorting'
    ];

    public function reserve() {
        return $this->hasMany(Reserve::class,'id_group', 'id');
    }
}
