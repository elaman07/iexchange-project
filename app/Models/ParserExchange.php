<?php
namespace App\Models;

use App\Models\Filters\ParserExchangeFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParserExchange
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $id_group
 * @property int $value
 * @property string|null $summa
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $status
 * @property int $id_type_price
 * @property int $number_format
 * @property int $type
 * @property int $value_default
 * @property string $summa_default
 * @property-read \App\Models\GroupParserExchange $group_parse_exchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereIdTypePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereNumberFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereSumma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereSummaDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchange whereValueDefault($value)
 * @mixin \Eloquent
 * @property-read \App\Models\SelectedCourse $selected_courses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange
 * @property-read int|null $direction_exchange_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParserExchangeLog[] $parser_exchange_log
 * @property-read int|null $parser_exchange_log_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SelectedCourse[] $selected_courses_many
 * @property-read int|null $selected_courses_many_count
 */
class ParserExchange extends Model
{
    use Filterable;

    protected $table = "parser_exchange";

    protected $fillable = [
        'name',
        'id_group',
        'value',
        'summa',
        'type',
        'number_format',
        'id_type_price',
        'status',
        'code_in',
        'code_out',
        'value_default',
        'summa_default',
        'is_not_update',
        'code',
        'type_price'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(ParserExchangeFilter::class);
    }

    /**
     * Get the user that owns the phone.
     */
    public function selected_courses()
    {
        return $this->hasOne('App\Models\SelectedCourse' , 'id_crypto_parser','id');
    }


    /**
     * Get the user that owns the phone.
     */
    public function selected_courses_many()
    {
        return $this->hasMany('App\Models\SelectedCourse' , 'id_crypto_parser','id');
    }

    public function parser_exchange_log() {
        return $this->hasMany(ParserExchangeLog::class,'id_parser_exchange', 'id');
    }

    public function group_parse_exchange() {
        return $this->hasOne(GroupParserExchange::class, 'id', 'id_group');
    }

    public function duplicate_count() {
        return $this->hasMany(ParserExchange::class,'name','name')->where('status', '=', 1)->count();
    }

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_crypto_parser','id');
    }
}
