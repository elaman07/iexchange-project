<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RequisitesList
 *
 * @property int $id
 * @property int $id_requisites
 * @property string|null $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status
 * @property-read \App\Models\Requisites $requisites
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList whereIdRequisites($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesList whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RequisitesList extends Model
{
    protected $table = 'requisites_list';

    protected $fillable = [
        'id_requisites', 'address'
    ];

    public function requisites() {
        return $this->hasOne(Requisites::class,'id','id_requisites');
    }
}
