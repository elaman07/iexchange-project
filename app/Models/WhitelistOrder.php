<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WhitelistOrder
 *
 * @property int $id
 * @property string|null $value
 * @property int $type
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WhitelistOrder whereValue($value)
 * @mixin \Eloquent
 */
class WhitelistOrder extends Model
{
    use Filterable;

    protected $table = 'whitelist_order';

    protected $fillable = [
        'value',
        'type',
        'text'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\WhitelistOrderFilter::class);
    }
}
