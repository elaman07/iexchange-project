<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;


class DirectionExchangeMode extends Model
{
    protected $table = 'direction_exchange_modes';

    protected $fillable = [
        'name',
        'status'
    ];

    public function direction_exchange(): MorphToMany
    {
        return $this->morphedByMany(DirectionExchange::class, 'model','directions_has_modes');
    }

}
