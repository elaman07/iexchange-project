<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Backgrounds
 *
 * @property int $id
 * @property string|null $background
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Backgrounds whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Backgrounds extends Model
{
    protected $table = 'backgrounds';

    protected $fillable = [
        'background'
    ];
}
