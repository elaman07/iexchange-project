<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Cities\Entities\CitiesModel;

class TaskInfo extends Model
{
    use SoftDeletes;

    protected $table = 'tasks_info';

    protected $fillable = [
        'id_task',
        'code_country',
        'country',
        'city',
        'ip',
        'device',
        'newbie',
        'is_local_scam',
        'is_scam',
        'language',
        'is_not_bonus',
        'is_not_partner',
        'is_switch_not_cashback',
        'is_switch_not_partner',
        'is_bestchange_parser',
        'bestchange_position',
        'in_min_amount',
        'in_max_amount',
        'is_freeze_scam',
        'is_freeze_local',
        'num_transaction',
        'currency_sign_payout',
        'currency_name_payout',
        'currency_position_payout',
        'notify_statuses',
        'count_change_operator',
        'note_tx',
        'amlbot_risk_in', //deleted
        'amlbot_risk_out', // deleted
        'is_pending',
        'blockchain_confirm',
        'blockchain_hash',
        'id_transaction_merchant',
        'id_transaction_pay',
        'getblockbot_risk_in', //deleted
        'getblockbot_risk_out', // deleted
        'network_id',
        'network_name',
        'city_id',
        'city_name',
        'aml_riskscore',
        'is_aml_analysis',
        'is_wait_hash_pay',
        'recalculated_at',
        'country_id',
        'country_name',
        'is_blocked_chat',
        'dot_not_remember_data',
        'aml_address_risk_in',
        'aml_address_risk_out',
        'text_message_order',

        'aml_result_value',
        'is_aml_high_risk'
    ];


    public function cities(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(CitiesModel::class,'id', 'city_id');
    }

    public function pay_transaction_hash()
    {
        return $this->hasOne(PayTransactionHash::class,'id_task', 'id_task');
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id','id_task');
    }
}
