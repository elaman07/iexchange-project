<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UsersHistoryProfile
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $event_name
 * @property int $id_changed
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property string|null $url
 * @property-read \App\Models\User $user
 * @property-read \App\Models\User $user_changed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereEventName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereIdChanged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsersHistoryProfile whereUserAgent($value)
 * @mixin \Eloquent
 */
class UsersHistoryProfile extends Model
{
    protected $table = 'users_history_profiles';

    protected $fillable = [
        'id_user',
        'event_name',
        'id_changed',
        'text',
        'ip_address',
        'user_agent',
        'url'
    ];

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function user_changed() {
        return $this->hasOne(User::class,'id','id_changed');
    }
}