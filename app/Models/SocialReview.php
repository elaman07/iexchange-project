<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialReview
 *
 * @property int $id
 * @property string|null $name
 * @property string $type
 * @property string|null $link
 * @property int $sorting
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialReview whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SocialReview extends Model
{
    protected $table = 'social_reviews';

    protected $fillable = [
        'name',
        'type',
        'sorting',
        'status',
        'link'
    ];
}
