<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\QiwiCurrency
 *
 * @property int $id
 * @property int $id_currency
 * @property int $selected
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Requisites $requisites
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency whereSelected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QiwiCurrency whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class QiwiCurrency extends Model
{
    protected $table = 'qiwi_currencies';

    protected $fillable = [
        'id_currency',
        'status',
        'selected'
    ];

    public function requisites() {
        return $this->hasOne(Requisites::class,'id_currency','id_currency');
    }
}
