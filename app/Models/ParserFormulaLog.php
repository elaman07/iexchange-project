<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParserFormulaLog extends Model
{
    protected $table = 'parser_formula_logs';

    protected $fillable = [
        'id_parser_formula',
        'name',
        'amount',
        'formula'
    ];

    public function parser_exchange() {
        return $this->hasOne(ParserFormulaRates::class,'id','id_parser_formula');
    }
}
