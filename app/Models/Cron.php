<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cron
 *
 * @property int $id
 * @property int $id_group
 * @property string|null $name
 * @property string|null $interval
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron whereInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cron whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Cron extends Model
{
    protected $table = 'cron';

    protected $fillable = [
        'id_group', 'name', 'interval'
    ];
}
