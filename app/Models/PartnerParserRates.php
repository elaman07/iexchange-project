<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerParserRates extends Model
{
    protected $table = 'partner_parser_rates';

    protected $fillable = [
        'name',
        'exchange_in',
        'exchange_out',
        'id_group',
        'rate',
        'status',
        'partner_type',
        'partner_id',
        'group_name',
        'number_format',
        'last_updated_at'
    ];


    protected $casts = [
        'last_updated_at' => 'datetime'
    ];

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_partner_parser_rate', 'id');
    }

    public function partner_parser_group() {
        return $this->hasOne(PartnerParserGroup::class,'id', 'partner_id');
    }
}
