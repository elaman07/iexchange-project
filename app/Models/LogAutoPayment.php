<?php

namespace App\Models;

use App\Models\Filters\LogAutoPaymentFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LogAutoPayment
 *
 * @property int $id
 * @property int $id_task
 * @property int $status
 * @property string|null $event_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereEventValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $provider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogAutoPayment whereProvider($value)
 */
class LogAutoPayment extends Model
{
    use Filterable;

    protected $table = 'log_autopayments';

    protected $fillable = [
        'id_task',
        'status',
        'event_value',
        'provider'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(LogAutoPaymentFilter::class);
    }
}
