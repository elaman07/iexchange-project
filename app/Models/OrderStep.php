<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class OrderStep extends Model
{
    use HasTranslations;

    protected $table = 'order_steps';

    public $translatable = ['name'];

    protected $fillable = [
        'name',
        'id_manager',
        'status',
        'sorting',
        'icon'
    ];
}
