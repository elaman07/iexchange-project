<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskShot
 *
 * @property int $id
 * @property int|null $id_task
 * @property string|null $account
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskShot whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskShot extends Model
{
    protected $table = 'tasks_shots';

    protected $fillable = [
        'id_task',
        'account'
    ];
}
