<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Favorites
 *
 * @property int $id
 * @property int $id_direction_exchange
 * @property int $id_currency1
 * @property int $id_currency2
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\DirectionExchange $direction_exchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereIdCurrency1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereIdCurrency2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereIdDirectionExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $sorting
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorites whereSorting($value)
 */
class Favorites extends Model
{
    protected $table = 'favorites';

    protected $fillable = [
        'id_direction_exchange',
        'type',
        'id_currency1',
        'id_currency2',
        'sorting'
    ];

    public function direction_exchange() {
        return $this->hasOne(DirectionExchange::class,'id','id_direction_exchange');
    }
}
