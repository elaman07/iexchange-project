<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Translatable\HasTranslations;

class DirectionField extends Model
{
    use HasTranslations;

    protected $table = 'directions_fields';

    protected $fillable = [
        'name',
        'min_char',
        'max_char',
        'obligatory_field',
        'status',
        'key_id',
        'field_type',
        'remove_spaces',
        'description',
        'sorting',
        'language_field'
    ];

    public $translatable = ['name', 'description'];


    public function direction_exchange(): MorphToMany
    {
        return $this->morphedByMany(DirectionExchange::class, 'model','directions_has_fields','direction_field_id');
    }

}
