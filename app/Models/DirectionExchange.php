<?php
namespace App\Models;

use App\Models\Filters\DirectionExchangeFilter;
use App\Models\Scopes\DirectionExchangeScope;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Cities\Entities\CitiesModel;
use Spatie\Translatable\HasTranslations;

class DirectionExchange extends Model
{
    use SoftDeletes,
        Filterable,
        HasTranslations;

    protected $table = 'direction_exchange';

    protected $casts = [
        'deleted_at' => 'datetime'
    ];

    protected $fillable = [
        'id_currency1',
        'id_currency2',
        'sorting_1',
        'sorting_2',
        'sorting_tariffs',
        'id_crypto_parser',
        'id_your_exchange',
        'add_course1',
        'add_course1_s',
        'status',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'id_group_commission',
        'deadline',
        'instructions',
        'min_price1',
        'min_price2',
        'max_price1',
        'max_price2',
        'commission1',
        'commission2',
        'commission_s1',
        'commission_s2',
        'your_add_course1',
        'your_add_course1_s',
        'profit',
        'profit_s',
        'profit_partner',
        'sign',
        'enable_bestchange',
        'id_bestchange_rates',
        'bestchange_position',
        'parent_url',
        'is_not_bonus',
        'is_not_partner',
        'individual_percentage',
        'fixed_payout',
        'export_label_param',
        'export_label_minamount',
        'export_label_maxamount',
        'minimum_payout',
        'maximum_payout',
        'allow_export',
        'allow_export_from',
        'allow_export_to',
        'is_main',
        'id_group_direction',
        'is_restrict_editing',
        'is_enabled_exchange',
        'bestchange_step',
        'bestchange_min_reserve',
        'bc_enable_your_course',
        'bc_id_your_exchange',
        'bc_your_add_course',
        'bestchange_bl',
        'bestchange_wl',
        'from_on_time',
        'to_on_time',
        'hidden_export_label_param',
        'is_manual_min_price1',
        'is_manual_max_price1',
        'is_manual_min_price2',
        'is_manual_max_price2',
        'in_who_pay_commission',
        'out_who_pay_commission',
        'id_competitor',
        'course_in',
        'course_our',
        'max_amount_newbie',
        'bc_min_sum',
        'bc_max_sum',
        'bc_id_new_rate',
        'bc_add_course',
        'max_order_one_ip',
        'max_order_one_account1',
        'max_order_one_account2',
        'max_order_one_user',
        'max_order_one_email',
        'not_ip',
        'cr_min_sum',
        'cr_max_sum',
        'cr_add_course',
        'cr_id_new_rate',
        'max_percent_partner',
        'xml_juridical',
        'languages',
        'is_hidden_not_locale',
        'device',
        'bc_new_commission',
        'rl_min2_course',
        'rl_max2_course',
        'rl_id_parser_exchange',
        'rl_add_course',
        'is_change_bestchange_range',
        'bestchange_range',
        'oth_comm_percent',
        'oth_comm_currency',
        'oth_comm2_percent',
        'oth_comm2_currency',
        'oth_min_comm',
        'oth_min2_comm',
        'auto_del_order_status',
        'auto_del_order_day',
        'auto_del_order_hour',
        'auto_del_order_minute',
        'is_hidden_tariffs',
        'is_holding_direction',
        'bestchange_range_from',
        'bestchange_range_to',
        'rl_id_your_exchange',
        'rl_your_add_course',
        'reserve_max_limit',
        'reserve_limit_day',
        'reserve_limit_month',
        'is_num_transaction',
        'num_transaction_label',
        'is_note_tx',
        'note_tx_label',
        'max_order_one_ip_day',
        'max_order_one_user_day',
        'max_order_one_email_day',
        'max_order_one_account1_day',
        'max_order_one_account2_day',
        'enable_file_parser_rate',
        'id_file_parser_rate',
        'bs_alt_parser_course',
        'id_bs_alt_parser',
        'is_enable_alt_bs_parser',
        'is_disable_bs_error',
        'x19_mode',
        'course_value',
        'exchange_rate',
        'is_error_rate',
        'exchange_rate_str',
        'error_rate_text',
        'tech_name',
        'last_order_id',
        'last_order_at',
        'first_order_id',
        'desc_exchange',
        'desc_exchange_dop',
        'id_partner_parser_rate',
        'id_parser_formula_rate',
        'type_output_requisites',
        'formalization_text',
        'min_count_exchanges_client',
        'order_button_i_pay_text',
        'order_button_i_pay',
        'is_allow_telegram_bot',
        'manual_rate_value',
        'parser_source_name',
        'other_docs',
        'is_notify_exchange_amount',
        'is_disable_auto_reg',
        'label_floating',
        'label_delay',
        'pay_comm_percent',
        'pay_comm_currency',
        'pay_comm2_percent',
        'pay_comm2_currency',
        'pay_min_comm',
        'pay_min2_comm',
        'sorting_admin',
        'is_enable_user_discount',
        'type_profit_field',
        'text_order_success',
        'text_order_failed',
        'text_order_handler',
        'is_email_instructions',
        'is_email_desc_exchange_dop',
        'is_email_other_docs',
        'is_verified_account',
        'text_order_confirm',
        'order_button_i_confirm',
        'notice_process_desc',
        'bestchange_city',
        'multiplicity_amount',
        'multiplicity_type',
        'multiplicity_comment'
    ];

    public $translatable  = [
        'desc_exchange',
        'desc_exchange_dop',
        'instructions',
        'formalization_text',
        'order_button_i_pay_text',
        'order_button_i_pay',
        'other_docs',
        'text_order_success',
        'text_order_failed',
        'text_order_handler',
        'text_order_confirm',
        'order_button_i_confirm',
        'notice_process_desc',
        'multiplicity_comment'
    ];


    /**
     * Фильтры
    */
    public function modelFilter() {
        return $this->provideFilter(DirectionExchangeFilter::class);
    }

    /**
     * Scope a query to only include active.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', '!=', 2);
    }

    /**
     * Только включенные направления
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsEnabled($query)
    {
        return $query->where('status', '=', 1);
    }


    public function direction_exchange_cities()
    {
        return $this->hasMany(DirectionExchangeCity::class,'id_direction_exchange','id');
    }

    public function direction_exchange_percentage_amount()
    {
        return $this->hasMany(DirectionExchangePercentAmount::class,'id_direction_exchange','id');
    }

    /**
     * Мерчант
     */
    public function merchants(): MorphToMany
    {
        return $this->morphToMany(
            GatewayMerchant::class,
            'model',
            'direction_exchange_merchants',
            'model_id',
            'gateway_merchant_id'
        );
    }

    /**
     * Сети для валют
     */
    public function direction_network(): MorphToMany
    {
        return $this->morphToMany(
            CurrencyNetwork::class,
            'model',
            'directions_has_networks',
            'model_id',
            'network_id'
        );
    }

    /**
     * Мерчант
     */
    public function direction_field(): MorphToMany
    {
        return $this->morphToMany(
            DirectionField::class,
            'model',
            'directions_has_fields',
            'model_id',
            'direction_field_id'
        );
    }

    /**
     * Мерчант
     */
    public function direction_field_sorting(): MorphToMany
    {
        return $this->morphToMany(
            DirectionField::class,
            'model',
            'directions_has_fields',
            'model_id',
            'direction_field_id'
        )->where('status', '=', 0)->orderBy('sorting');
    }

    /**
     * Платежные реквизиты для направлений
     */
    public function direction_requisites(): MorphToMany
    {
        return $this->morphToMany(
            DirectionRequisite::class,
            'model',
            'directions_has_requisites',
            'model_id',
            'direction_requisite_id'
        );
    }

    /**
     * Запрещенные страны
     */
    public function direction_forbidden_countries(): MorphToMany
    {
        return $this->morphToMany(
            GeoCountryList::class,
            'model',
            'directions_has_forbidden_countries',
            'model_id',
            'geo_country_list_id'
        );
    }

    /**
     * Разрешенные страны
     */
    public function direction_allowed_countries(): MorphToMany
    {
        return $this->morphToMany(
            GeoCountryList::class,
            'model',
            'directions_has_allowed_countries',
            'model_id',
            'geo_country_list_id'
        );
    }

    /**
     * Использование направлений под разные режимы работ
     */
    public function direction_modes(): MorphToMany
    {
        return $this->morphToMany(
            DirectionExchangeMode::class,
            'model',
            'directions_has_modes',
            'model_id',
            'direction_exchange_mode_id'
        );
    }

    /**
     * Получаем дубликат
     *
     * @return int
    */
    public function duplicate() {
        return $this->hasMany(DirectionExchange::class,'parent_url', 'parent_url')->count();
    }


    public function currency1() {
        return $this->hasOne(Currency::class,'id','id_currency1');
    }

    public function currency1_many() {
        return $this->hasMany(Currency::class,'id','id_currency1');
    }

    public function currency2() {
        return $this->hasOne(Currency::class,'id','id_currency2');
    }

    public function currency2_many() {
        return $this->hasMany(Currency::class,'id','id_currency2');
    }

    public function parser_exchange() {
        return $this->hasOne(ParserExchange::class,'id','id_crypto_parser');
    }

    public function bc_new_rate() {
        return $this->hasOne(ParserExchange::class,'id','bc_id_new_rate');
    }

    public function bc_your_rate() {
        return $this->hasOne(YourExchange::class,'id','bc_id_your_exchange');
    }

    public function cr_new_rate() {
        return $this->hasOne(ParserExchange::class,'id','cr_id_new_rate');
    }

    public function rl_parser_exchange() {
        return $this->hasOne(ParserExchange::class,'id','rl_id_parser_exchange');
    }

    public function rl_your_exchange() {
        return $this->hasOne(YourExchange::class,'id','rl_id_your_exchange');
    }

    public function your_exchange() {
        return $this->hasOne(YourExchange::class,'id','id_your_exchange');
    }

    public function bestchange_rates() {
        return $this->hasOne(BestChangeRatesModel::class,'id','id_bestchange_rates');
    }

    public function competitor_rates() {
        return $this->hasOne(CompetitorRates::class,'id','id_competitor');
    }

    public function bs_alt_parser() {
        return $this->hasOne(ParserExchange::class,'id', 'id_bs_alt_parser');
    }

    public function file_parser_rates() {
        return $this->hasOne(FileParserRates::class,'id','id_file_parser_rate');
    }

    public function partner_parser_rates() {
        return $this->hasOne(PartnerParserRates::class,'id','id_partner_parser_rate');
    }

    public function parser_formula_rates()
    {
        return $this->hasOne(ParserFormulaRates::class,'id', 'id_parser_formula_rate');
    }

    public function group_commission() {
        return $this->hasOne(GroupCommision::class,'id','id_group_commission');
    }

    public function history_field1() {
        return $this->hasOne(HistoryField::class,'id_currency1','id_currency1');
    }

    public function history_field2() {
        return $this->hasOne(HistoryField::class,'id_currency2','id_currency2');
    }

    public function direction_day() {
        return $this->hasOne(DirectionDay::class,'id_direction_exchange','id');
    }

    public function tasks() {
        return $this->hasMany(Task::class,'id_direction_exchange', 'id');
    }
}
