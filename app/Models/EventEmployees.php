<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EventEmployees
 *
 * @property int $id
 * @property int $id_employees
 * @property int $id_task
 * @property int $type
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereIdEmployees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventEmployees whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EventEmployees extends Model
{
    protected $table = 'event_employees';

    protected $fillable = [
        'id_employees',
        'id_task',
        'type',
        'amount'
    ];
}
