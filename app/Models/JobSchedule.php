<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class JobSchedule extends Model
{
    protected $table = 'job_schedules';

    protected $fillable = [
        'id_user',
        'name',
        'status',
        'from_time',
        'to_time',
        'work_days'
    ];

}
