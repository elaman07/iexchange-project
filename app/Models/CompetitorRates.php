<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CompetitorRates
 *
 * @property int $id
 * @property string|null $name
 * @property int $id_competitor
 * @property int $value
 * @property string|null $summa
 * @property int $status
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $number_format
 * @property string|null $exchange_in
 * @property string|null $exchange_out
 * @property-read \App\Models\CompetitorLink $competitor_link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange
 * @property-read int|null $direction_exchange_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereExchangeIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereExchangeOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereIdCompetitor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereNumberFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereSumma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRates whereValue($value)
 * @mixin \Eloquent
 */
class CompetitorRates extends Model
{
    protected $table = 'competitor_rates';

    protected $fillable = [
        'name',
        'exchange_in',
        'exchange_out',
        'id_competitor',
        'value',
        'summa',
        'status',
        'number_format',
        'type',
        'code'
    ];

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_competitor','id');
    }

    public function duplicate_count() {
        return $this->hasMany(CompetitorRates::class,'name','name')->count();
    }

    public function competitor_link() {
        return $this->hasOne(CompetitorLink::class,'id', 'id_competitor');
    }
}
