<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialAuthSystem
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $alias
 * @property string|null $client_id
 * @property string|null $client_secret
 * @property int $sorting
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereClientSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialAuthSystem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SocialAuthSystem extends Model
{
    protected $table = 'social_auth_system';

    protected $fillable = [
        'name',
        'alias',
        'client_id',
        'client_secret',
        'sorting',
        'status'
    ];
}

