<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LogDrain
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $text
 * @property int $id_requisites
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $from_value
 * @property int $id_task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereFromValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereIdRequisites($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogDrain whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LogDrain extends Model
{
    protected $table = 'log_drain';

    protected $fillable = [
        'type',
        'text',
        'id_requisites',
        'from_value',
        'id_task'
    ];
}
