<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Cities\Entities\CitiesModel;


class DirectionExchangePercentAmount extends Model
{
    protected $table = 'direction_exchange_percent_amount';

    protected $fillable = [
        'id_direction_exchange',
        'percentage',
        'amount',
    ];
}
