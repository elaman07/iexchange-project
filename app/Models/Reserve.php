<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.09.2017
 * Time: 16:27
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Reserve
 *
 * @property int $id
 * @property int $id_currency
 * @property float $summa
 * @property int $status
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $id_code_currency
 * @property int|null $is_non_standard
 * @property float|null $black_amount
 * @property-read \App\Models\CodeCurrency $code_currency
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\ReserveLog $reserve_log
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereBlackAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereIdCodeCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereIsNonStandard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereSumma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id_user
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereIdUser($value)
 * @property int $id_main ID базового резерва
 * @property-read \App\Models\Reserve $main_reserve
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereIdMain($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EventReserve[] $event_reserve
 * @property int $id_group
 * @property int $sorting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ReserveGroup[] $reserve_groups
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reserve whereSorting($value)
 * @property-read \App\Models\ReserveAlert $reserve_alerts
 * @property-read int|null $event_reserve_count
 * @property-read int|null $reserve_groups_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reserve[] $main_reserves
 * @property-read int|null $main_reserves_count
 * @property int $is_fixed_reserve
 * @property int $is_star
 * @method static \Illuminate\Database\Eloquent\Builder|Reserve whereIsFixedReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reserve whereIsStar($value)
 */
class Reserve extends Model
{
    protected $fillable = [
        'id_currency',
        'id_code_currency',
        'id_user',
        'summa',
        'black_amount',
        'created_at',
        'updated_at',
        'status',
        'is_non_standard',
        'id_user',
        'id_group',
        'id_main',
        'sorting',
        'is_star',
        'is_fixed_reserve',
        'id_file_reserve',
        'id_server_reserve'
    ];

    protected $table = 'reserves';

    public function reserve_log() {
        return $this->hasOne(ReserveLog::class,'id_reserve', 'id');
    }

    public function code_currency() {
        return $this->hasOne(CodeCurrency::class, 'id', 'id_code_currency');
    }

    public function currency() {
        return $this->hasOne(Currency::class, 'id', 'id_currency');
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function main_reserve() {
        return $this->hasOne(Reserve::class,'id','id_main');
    }

    public function main_reserves() {
        return $this->hasMany(Reserve::class,'id_main','id');
    }


    public function event_reserve() {
        return $this->hasMany(EventReserve::class,'id_currency', 'id_currency');
    }

    public function reserve_groups() {
        return $this->hasMany(ReserveGroup::class,'id','id_group');
    }

    public function reserve_alerts() {
        return $this->hasOne(ReserveAlert::class,'id','id_reserve');
    }
}
