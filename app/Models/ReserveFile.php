<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReserveFile extends Model
{
    protected $table = 'reserves_files';

    protected $fillable = [
        'name',
        'id_group',
        'amount',
        'status',
        'number_format',
    ];

    public function reserves() {
        return $this->hasMany(Reserve::class,'id_file_reserve', 'id');
    }

    public function file_parser_group() {
        return $this->hasOne(FileParserGroup::class,'id', 'id_group');
    }
}
