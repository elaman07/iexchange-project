<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

/**
 * App\Models\PermissionsGroup
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionsGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $permissions_count
 */
class PermissionsGroup extends Model
{
    protected $table = 'permissions_group';

    protected $fillable = [
        'id', 'name'
    ];


    public function permissions() {
        return $this->hasMany(Permission::class,'id_permissions_group','id');
    }
}
