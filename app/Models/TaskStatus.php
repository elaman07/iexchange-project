<?php
namespace App\Models;


use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskStatus
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_export
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereIsExport($value)
 * @property string|null $color
 * @property string|null $class
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereColor($value)
 * @property int $allow_delete
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatus whereAllowDelete($value)
 */
class TaskStatus extends Model
{
    use HasTranslations;

    protected $table = "tasks_status";

    protected $fillable = [
        'name',
        'is_export',
        'class',
        'color'
    ];

    protected $translatable = [
        'name'
    ];
}
