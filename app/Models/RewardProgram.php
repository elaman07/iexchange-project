<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class RewardProgram extends Model
{
    use SoftDeletes, HasTranslations;

    protected $table = 'reward_programs';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'is_reg',
        'amount',
        'percent',
        'sign',
        'title',
        'description',
        'style_width'
    ];

    protected $translatable = [
        'title',
        'description'
    ];

    protected $casts = [
        'deleted_at' => 'datetime'
    ];

    public function links()
    {
        return $this->hasMany(Reward::class, 'reward_program_id', 'id');
    }
}
