<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReferralInfoLog
 *
 * @property int $id
 * @property int $id_referral
 * @property int $id_user
 * @property int $type
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $referral
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereIdReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralInfoLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReferralInfoLog extends Model
{
    protected $table = 'referrals_info_logs';

    protected $fillable = [
        'id_referral',
        'id_user',
        'type',
        'text'
    ];

    public function referral() {
        return $this->hasOne(User::class,'id', 'id_referral');
    }

    public function user() {
        return $this->hasOne(User::class,'id', 'id_user');
    }
}