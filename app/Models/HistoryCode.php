<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HistoryCode extends Model
{
    protected $table = 'histories_codes';

    protected $fillable = [
        'id_task',
        'code',
        'provider_id',
        'id_pay',
        'amount',
        'currency',
        'type'
    ];
}
