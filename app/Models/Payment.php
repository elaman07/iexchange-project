<?php
namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasTranslations;

    protected $table = 'payments';

    protected $fillable = [
        'name',
        'logo',
        'logo_svg',
        'enable_svg',
        'svg_name',
        'svg_class',
        'is_delete',
        'blockchain_url',
        'is_import',
        'is_local_image'
    ];


    public $translatable = ['name'];


    public function scopeActive($query) {
        return $query->where('is_delete', '!=', 1);
    }

    public function explorer() {
        return $this->hasOne(PaymentExplorer::class,'id_payment','id');
    }

    public function currencies() {
        return $this->hasMany(Currency::class,'id_payment', 'id');
    }
}
