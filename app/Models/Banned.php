<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Banned
 *
 * @property int $id
 * @property string|null $description
 * @property string|null $expired_at
 * @property string|null $filter_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned whereFilterName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banned whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Banned extends Model
{
    protected $table = 'banned';

    protected $fillable = [
        'description',
        'expired_at',
        'filter_name'
    ];
}
