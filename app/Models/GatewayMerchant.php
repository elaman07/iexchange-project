<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\GatewayMerchant
 *
 * @property int $id
 * @property string|null $name
 * @property int $id_gateways
 * @property int $status
 * @property string|null $instruction_payment
 * @property string|null $allow_ip_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $security_hash
 * @property int $is_check_from_shot
 * @property int $is_disable_code_currency
 * @property int $is_disable_log
 * @property int $is_disable_payment_server
 * @property string|null $comment
 * @property int $is_deny_ip_address
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Currency[] $currency
 * @property-read int|null $currency_count
 * @property-read \App\Models\Merchant $gateway
 * @property-read \App\Models\Merchant $merchant
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereAllowIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIdGateways($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereInstructionPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIsCheckFromShot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIsDenyIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIsDisableCodeCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIsDisableLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIsDisablePaymentServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereSecurityHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_pay_commission
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GatewayMerchant whereIsPayCommission($value)
 * @property string|null $instruction_pay
 * @property int $day_limit_merchant
 * @property float $max_limit_amount_order
 * @property float $amount_fault
 * @property float $day_limit_amount_merchant
 * @property int $is_enable_merchant_button
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant filter(array $input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant simplePaginateFilter(?int $perPage = null, ?int $columns = [], ?int $pageName = 'page', ?int $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereAmountFault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereBeginsWith(string $column, string $value, string $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereDayLimitAmountMerchant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereDayLimitMerchant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereEndsWith(string $column, string $value, string $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereInstructionPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereIsEnableMerchantButton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereLike(string $column, string $value, string $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|GatewayMerchant whereMaxLimitAmountOrder($value)
 */
class GatewayMerchant extends Model
{
    use Filterable, HasTranslations;

    protected $table = 'gateways_merchants';

    protected $fillable = [
        'name',
        'id_gateways',
        'status',
        'description',
        'allow_ip_address',
        'security_hash',
        'is_check_account_number',
        'is_check_from_shot',
        'is_disable_code_currency',
        'is_merchant_log',
        'is_check_api',
        'is_pay_commission',
        'comment',
        'instruction_payment',
        'max_limit_amount_order',
        'day_limit_merchant',
        'amount_fault',
        'is_enable_merchant_button',
        'method_pay',
        'day_limit_amount_merchant',
        'fixed_fee',
        'min_confirm',
        'max_register_blockchain',
        'max_first_confirm_blockchain',
        'total_usd',
        'last_order_id',
        'is_config_done',
        'is_card_found',
        'exchange_fee',
        'pay_amount',
        'credit_amount',
        'order_num',
        'bank_name',
        'type_pay',
        'site_account',
        'code_currency'
    ];

    protected $translatable = [
        'instruction_payment',
        'comment'
    ];

    public function currencies(): MorphToMany
    {
        return $this->morphedByMany(Currency::class, 'model','currency_merchants');
    }

    public function direction_exchange(): MorphToMany
    {
        return $this->morphedByMany(DirectionExchange::class, 'model','currency_merchants');
    }

    public function gateway(): HasOne
    {
        return $this->hasOne(Gateway::class, 'id', 'id_gateways');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class,'id_merchant','id');
    }

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\GatewayMerchantFilter::class);
    }
}
