<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\HistoryPaymentTransaction
 *
 * @property int $id
 * @property int|null $id_task
 * @property string $payment
 * @property string $transfer
 * @property string|null $details
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryPaymentTransaction onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryPaymentTransaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryPaymentTransaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryPaymentTransaction withoutTrashed()
 */
class HistoryPaymentTransaction extends Model
{
    use SoftDeletes;

    protected $table = 'history_payment_transactions';

    protected $fillable = [
        'id', 'id_task', 'payment', 'transfer', 'details'
    ];
}
