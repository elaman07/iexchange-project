<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class TaskFile extends Model
{
    use HasFactory;

    protected $table = 'tasks_files';

    protected $fillable = [
        'id_manager',
        'id_task',
        'text',
        'file'
    ];


    public function user() {
        return $this->hasOne(User::class,'id','id_manager');
    }
}
