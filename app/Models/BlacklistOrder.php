<?php
namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BlacklistOrder
 *
 * @property int $id
 * @property string|null $value
 * @property int $type
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereLike($column, $value, $boolean = 'and')
 * @property int $is_bestchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BlacklistOrder whereIsBestchange($value)
 */
class BlacklistOrder extends Model
{
    use Filterable;

    protected $table = 'blacklist_order';

    protected $fillable = [
        'value',
        'type',
        'text',
        'is_bestchange',
        'is_iex',
        'hash_id'
    ];


    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\BlacklistOrderFilter::class);
    }

}
