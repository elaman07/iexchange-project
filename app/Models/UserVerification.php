<?php

namespace App\Models;

use App\Models\Filters\VerificationCardFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserVerification extends Model
{
    use Notifiable, Filterable;

    protected $table = 'user_verification';

    protected $fillable = [
        'user_id',
        'file_one',
        'file_two',
        'fio_user',
        'status',
        'ip_address',
        'user_agent',
        'hash_id'
    ];

    public function user() {
        return $this->hasOne(User::class,'id','user_id');
    }
}
