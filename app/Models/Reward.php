<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Reward
 *
 * @property int $id
 * @property int $user_id
 * @property int $reward_program_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\RewardProgram $program
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereRewardProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereUserId($value)
 * @mixin \Eloquent
 */
class Reward extends Model
{
    protected $table = 'reward';

    protected $fillable = ['user_id', 'reward_program_id'];

    public function user()
    {
        $usersModel = config('auth.providers.users.model');
        return $this->belongsTo($usersModel);
    }


    public function program()
    {
        return $this->belongsTo(RewardProgram::class, 'reward_program_id');
    }
}
