<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\HistoryRecalculation
 *
 * @property int $id
 * @property int $id_task
 * @property float $amount
 * @property float $old_amount
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $course
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereOldAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryRecalculation onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryRecalculation whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryRecalculation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryRecalculation withoutTrashed()
 * @property-read \App\Models\Task $tasks
 */
class HistoryRecalculation extends Model
{
    use SoftDeletes;

    protected $table = 'history_recalculation';

    protected $fillable = [
        'id_task',
        'amount',
        'old_amount',
        'type',
        'course'
    ];

    public function tasks() {
        return $this->hasOne(Task::class,'id','id_task');
    }

    public function tasks_info() {
        return $this->hasOne(TaskInfo::class,'id_task','id_task');
    }
}
