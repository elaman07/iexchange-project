<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CashbackErrorLog
 *
 * @property int $id
 * @property int $id_task
 * @property int $id_user
 * @property string|null $in_code
 * @property string|null $out_code
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $text
 * @property-read \App\Models\Task|null $tasks
 * @property-read \App\Models\User|null $users
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereInCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereOutCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CashbackErrorLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CashbackErrorLog extends Model
{
    protected $table = 'cashback_error_log';

    protected $fillable = [
        'id_task',
        'id_user',
        'text',
        'in_code',
        'out_code'
    ];

    public function tasks()
    {
        return $this->hasOne(Task::class,'id','id_task');
    }

    public function users()
    {
        return $this->hasOne(User::class,'id','id_user');
    }
}
