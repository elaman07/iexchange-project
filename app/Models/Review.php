<?php

namespace App\Models;

use App\Models\Filters\ReviewFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Review
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $email
 * @property string|null $ip_address
 * @property int $status
 * @property string|null $text
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $user_agent
 * @property int $id_admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereIdAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUserAgent($value)
 * @mixin \Eloquent
 * @property int $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereType($value)
 * @property string|null $telegram_id
 * @property int $rate_full
 * @property int $rate_speed
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRateFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRateSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereTelegramId($value)
 */
class Review extends Model
{
    use Filterable;

    protected $table = 'reviews';

    protected $fillable = [
        'id_admin',
        'ip_address',
        'user_agent',
        'status',
        'text',
        'name',
        'rate_speed',
        'id_task',
        'id_direction_exchange',
        'version',
        'id_user'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(ReviewFilter::class);
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id','id_task');
    }
}
