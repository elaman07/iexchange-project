<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TasksRatesData
 *
 * @property int $id
 * @property int $id_task
 * @property string $exchange_course
 * @property string $market_course
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereExchangeCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereMarketCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TasksRatesData onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TasksRatesData whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TasksRatesData withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TasksRatesData withoutTrashed()
 */
class TasksRatesData extends Model
{
    use SoftDeletes;

    protected $table = 'tasks_rates_data';

    protected $fillable = [
        'id_task',
        'exchange_course',
        'market_course'
    ];
}
