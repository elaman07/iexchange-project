<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EventReserve
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_currency
 * @property int $id_reserve
 * @property int $type
 * @property string|null $text
 * @property string $value_after Значение после
 * @property string $value_before Значение до
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereIdReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereValueAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereValueBefore($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Currency[] $currencyMany
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $userMany
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereLike($column, $value, $boolean = 'and')
 * @property string|null $comment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventReserve whereComment($value)
 * @property-read int|null $currency_many_count
 * @property-read int|null $user_many_count
 */
class EventReserve extends Model
{
    use Filterable;

    protected $table = 'event_reserve';

    protected $fillable = [
        'id_user',
        'id_currency',
        'id_reserve',
        'type',
        'text',
        'value_before',
        'value_after',
        'comment'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\EventReserveFilter::class);
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function currency() {
        return $this->hasOne(Currency::class,'id','id_currency');
    }

    public function currencyMany() {
        return $this->hasMany(Currency::class,'id', 'id_currency');
    }

    public function userMany() {
        return $this->hasMany(User::class,'id', 'id_user');
    }
}
