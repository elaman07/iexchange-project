<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.02.2020
 * Time: 12:07
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminDesktop
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $name
 * @property int $columns
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $flex_num1
 * @property int $flex_num2
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdminDesktopGadget[] $gadgets
 * @property-read int|null $gadgets_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereColumns($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereFlexNum1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereFlexNum2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $flex_num3
 * @property array|null $flex_nums
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereFlexNum3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminDesktop whereFlexNums($value)
 */
class AdminDesktop extends Model
{
    protected $table = 'admin_desktops';

    protected $fillable = [
        'id_user',
        'name',
        'columns',
        'sorting',
        'flex_num1',
        'flex_num2',
        'flex_num3',
        'flex_nums'
    ];

    /**
     * Атрибуты, которые должны быть приведены к нативным типам.
     *
     * @var array
     */
    protected $casts = [
        'id_user' => 'integer',
        'name' => 'string',
        'columns' => 'integer',
        'sorting' => 'integer',
        'flex_num1' => 'integer',
        'flex_num2' => 'integer',
        'flex_nums' => 'array'
    ];

    public function gadgets() {
        return $this->hasMany(AdminDesktopGadget::class,'id_desktop', 'id');
    }
}