<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class CurrencyLabel extends Model
{
    use HasTranslations;

    protected $table = 'currencies_labels';

    protected $fillable = [
        'title',
        'text_color',
        'bg_color',
        'image'
    ];

    protected $translatable = [
        'title'
    ];

    public function currencies() {
        return $this->hasMany(Currency::class,'id_label', 'id');
    }
}
