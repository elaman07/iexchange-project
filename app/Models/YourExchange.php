<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class YourExchange extends Model
{
    protected $fillable = [
        'name',
        'exchange_rate',
        'number_format',
        'id_group'
    ];

    protected $table = 'your_exchange';

    public function direction_exchange(): HasMany
    {
        return $this->hasMany(DirectionExchange::class,'id_your_exchange','id');
    }
}
