<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 20.09.2020
 * Time: 23:53
 */

namespace App\Models;


use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IEXScriptConfig
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig query()
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IEXConfig whereValue($value)
 * @mixin \Eloquent
 */
class IEXScriptConfig extends Model
{
    use HasTranslations;

    protected $table = 'iex_script_config';

    protected $fillable = [
        'key',
        'value',
    ];

    public $translatable = ['value'];
}
