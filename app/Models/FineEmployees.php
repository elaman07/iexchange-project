<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FineEmployees
 *
 * @property int $id
 * @property int $id_employees
 * @property int $amount
 * @property string|null $text
 * @property int $number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereIdEmployees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FineEmployees whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FineEmployees extends Model
{
    protected $table = 'fine_employees';

    protected $fillable = [
        'id_employees',
        'amount',
        'text',
        'number'
    ];
}
