<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 10.11.2019
 * Time: 22:41
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LogMail
 *
 * @property int $id
 * @property string|null $from_mail
 * @property string|null $to_mail
 * @property string|null $subject
 * @property string|null $body
 * @property string|null $headers
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereFromMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereHeaders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereToMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LogMail extends Model
{
    protected $table = 'logs_email';

    protected $fillable = [
        'from_mail',
        'to_mail',
        'subject',
        'body',
        'headers'
    ];
}
