<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 21.09.2017
 * Time: 0:19
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GroupParserExchange
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property string|null $type_amount
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParserExchange[] $parserExchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereTypeAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParserExchange[] $parser_exchange_enabled
 * @property string|null $alias
 * @property int $sorting
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupParserExchange whereSorting($value)
 * @property-read int|null $parser_exchange_count
 * @property-read int|null $parser_exchange_enabled_count
 * @property int $priority
 * @method static \Illuminate\Database\Eloquent\Builder|GroupParserExchange wherePriority($value)
 */
class GroupParserExchange extends Model
{
    protected $table = "group_parser_exchange";

    protected $fillable = [
        'name',
        'status',
        'type_amount',
        'sorting',
        'alias',
        'provider_id',
        'provider_url',
        'proxy_id',
        'last_updated_at',
        'last_imported_at',
        'is_import_rates'
    ];

    protected $casts = [
        'last_updated_at' => 'datetime',
        'last_imported_at' => 'datetime'
    ];


    public function parserExchange() {
        return $this->hasMany(ParserExchange::class,'id_group','id');
    }

    public function parser_exchange_enabled() {
        return $this->hasMany(ParserExchange::class,'id_group','id')->where('status', '=', 1);
    }

    public function parser_exchange($id_group = 0, $pair = null, $orderBy = false)
    {
        $parser = $this->hasMany(ParserExchange::class,'id_group','id')->where('status','=',1);
        if($id_group > 0) {
            $parser->where('id_group','=', $id_group);
        }

        if($pair != null) {
            $parser->where('name', $pair);
        }

        if($orderBy == true) {
            $parser->orderBy('type', 'asc');
        }

        return $parser;
    }
}
