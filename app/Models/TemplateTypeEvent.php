<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TemplateTypeEvent
 *
 * @property int $id
 * @property string|null $event_name
 * @property int $event_type
 * @property string|null $name
 * @property string|null $description
 * @property int $id_user
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmailTemplate[] $email_template
 * @property-read int|null $email_template_count
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereEventName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereEventType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TemplateTypeEvent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TemplateTypeEvent extends Model
{
    protected $table = 'template_type_events';

    protected $fillable = [
        'event_name',
        'event_type',
        'name',
        'description',
        'id_user'
    ];

    public function email_template() {
        return $this->hasMany(EmailTemplate::class,'id_event_type', 'id');
    }
}
