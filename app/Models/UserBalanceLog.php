<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserBalanceLog
 *
 * @property int $id
 * @property int $id_user
 * @property int $type
 * @property int $id_manager
 * @property string|null $text
 * @property string|null $from_balance
 * @property string|null $to_balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $route_type
 * @property-read \App\Models\User $manager
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereFromBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereIdManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereRouteType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereToBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalanceLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserBalanceLog extends Model
{
    protected $table = 'user_balance_log';

    protected $fillable = [
        'id_user',
        'id_manager',
        'type',
        'route_type',
        'text',
        'from_balance',
        'to_balance'
    ];

    public function manager() {
        return $this->hasOne(User::class,'id', 'id_manager');
    }
}
