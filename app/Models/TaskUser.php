<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskUser
 *
 * @property int $id
 * @property int $id_task
 * @property int $id_user
 * @property float $convert_to_usd
 * @property float $convert_to_rub
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereConvertToRub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereConvertToUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskUser extends Model
{
    protected $table = 'tasks_user';

    protected $fillable = [
        'id_task',
        'id_user',
        'convert_to_usd',
        'convert_to_rub'
    ];
}
