<?php

namespace App\Models;

use App\Models\Filters\LogMerchantFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;


class LogAutoPaymentEvent extends Model
{
    use Filterable;

    protected $table = 'logs_autopayment_events';

    protected $fillable = [
        'provider',
        'id_order',
        'ip_address',
        'url',
        'headers',
        'content',
        'response'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\LogAutoPaymentEventFilter::class);
    }
}
