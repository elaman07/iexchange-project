<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyLog
 *
 * @property int $id
 * @property int $status
 * @property int $id_currency
 * @property int $id_user
 * @property string $from_where
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Currency $currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereFromWhere($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CurrencyLog extends Model
{
    protected $table = 'currencies_log';

    protected $fillable = [
        'status',
        'id_currency',
        'id_user',
        'from_where',
        'text',
        'name'
    ];

    public function currency() {
        return $this->hasOne(Currency::class,'id','id_currency');
    }
}
