<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DocsItem
 *
 * @property int $id
 * @property int $category_id
 * @property int $type_id
 * @property string $parent_url
 * @property string $name
 * @property string $text
 * @property int $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\DocsCategory $docs_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereParentUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DocsItem extends Model
{
    protected $table = 'docs_items';

    protected $fillable = [
        'category_id',
        'type_id',
        'parent_url',
        'name',
        'text',
        'code'
    ];

    public function docs_category() {
        return $this->hasOne(DocsCategory::class,'id','category_id');
    }
}
