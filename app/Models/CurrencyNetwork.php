<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.10.2019
 * Time: 7:57
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class CurrencyNetwork extends Model
{
    protected $table = 'currencies_networks';

    protected $fillable = [
        'name',
        'logo',
        'status'
    ];
}
