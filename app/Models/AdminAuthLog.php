<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminAuthLog
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $ip_address
 * @property string|null $prev_ip_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_successful
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereIsSuccessful($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog wherePrevIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminAuthLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdminAuthLog extends Model
{
    use Filterable;

    protected $table = 'admin_auth_log';

    protected $fillable = [
      'id_user',
      'ip_address',
      'prev_ip_address',
      'is_successful'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\AdminAuthLogFilter::class);
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }
}
