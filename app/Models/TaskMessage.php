<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TaskMessage extends Model
{
    protected $table = 'tasks_messages';

    protected $fillable = [
        'id_task',
        'user_id',
        'message',
        'type_user',
        'is_view'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
