<?php
namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\LinksReviewGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LinksReview[] $links_review
 * @property-read int|null $links_review_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReviewGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LinksReviewGroup extends Model
{
    use HasTranslations;

    protected $table = 'links_review_groups';

    protected $fillable = [
        'name',
        'sorting'
    ];

    public $translatable = ['name'];

    public function links_review() {
        return $this->hasMany(LinksReview::class,'id_group','id');
    }

    public function links_review_large(): HasMany
    {
        return $this->hasMany(LinksReview::class,'id_group','id')->where('is_review', '=', 1)->orderBy('sorting');
    }

    public function links_review_url(): HasMany
    {
        return $this->hasMany(LinksReview::class,'id_group','id')->where('is_review', '=', 0)->orderBy('sorting');
    }
}
