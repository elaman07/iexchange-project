<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GetBlockRequest extends Model
{
    /**
     * The activity model uses the 'sessions' database.
     *
     * @var string
     */
    protected $table = 'getblock_requests';


    protected $fillable = [
        'id_order',
        'url',
        'headers',
        'response',
        'options'
    ];
}
