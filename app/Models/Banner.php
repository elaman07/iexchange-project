<?php

namespace App\Models;


use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;


class Banner extends Model
{
    use HasTranslations;

    protected $table = 'banners';

    public $translatable = ['title', 'text'];

    protected $fillable = [
        'title',
        'text',
        'images',
        'color_text',
        'color_title',
        'images_banner',
        'sorting',
        'status'
    ];


    /**
     * Дополнительные поля для реквизитов
     */
    public function buttons(): MorphToMany
    {
        return $this->morphToMany(
            BannerButton::class,
            'model',
            'banners_has_banners_buttons',
            'model_id',
            'banners_button_id'
        );
    }
}
