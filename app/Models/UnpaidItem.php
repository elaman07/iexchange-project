<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UnpaidItem
 *
 * @property int $id
 * @property int $auto_delete
 * @property int $task_time_over
 * @property int $task_time
 * @property int $task_denied
 * @property int $removal_time
 * @property int $removal_minute
 * @property int $rules_cron
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem query()`
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereAutoDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereRemovalMinute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereRemovalTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereRulesCron($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereTaskDenied($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereTaskTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereTaskTimeOver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $order_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UnpaidItem whereOrderStatus($value)
 */
class UnpaidItem extends Model
{
    protected $table = 'unpaid_items';

    protected $fillable =[
        'auto_delete',
        'order_status',
        'removal_time',
        'removal_minute',
        'rules_cron'
    ];
}
