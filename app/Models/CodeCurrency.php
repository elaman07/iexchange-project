<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;


class CodeCurrency extends Model
{
    public $table = 'code_currency';

    protected $fillable = [
        'name',
        'sign',
        'balance',
        'id_parser_exchange',
        'add_to_course',
        'is_trashed',
        'internal_rate',
        'is_import',
        'id_parser_formula',
        'add_to_course_formula'
    ];


    public function scopeActive(Builder $query) {
        return $query->where('is_trashed', '=', 0);
    }


    public function parser_exchange(): HasOne
    {
        return $this->hasOne(ParserExchange::class,'id','id_parser_exchange');
    }

    public function parser_formula(): HasOne
    {
        return $this->hasOne(ParserFormulaRates::class,'id','id_parser_formula');
    }

    public function reserves()
    {
        $currencies = Currency::where('id_code_currency', $this->id)->pluck('id');
        return Reserve::whereIn('id_currency', $currencies)->sum('summa');
    }

    public function internal_account(): HasOne
    {
        return $this->hasOne(InternalAccount::class,'id_code_currency', 'id')->where('id_user', '=', auth()->id());
    }

    public function currency(): HasMany
    {
        return $this->hasMany(Currency::class,'id_code_currency','id');
    }

    public function reserve_amount() {
        return Reserve::whereIn('id_currency', $this->currency()->pluck('id'))->sum('summa');
    }
}
