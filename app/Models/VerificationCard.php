<?php

namespace App\Models;

use App\Models\Filters\VerificationCardFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\VerificationCard
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_order
 * @property string|null $card_number
 * @property string|null $image
 * @property int $is_verified
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $hash_id
 * @property int $id_currency
 * @property string|null $name
 * @property int $status
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property string|null $card_number_string
 * @property-read \App\Models\Currency $currency
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereCardNumberString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereHashId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereIdOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VerificationCard whereUserAgent($value)
 * @mixin \Eloquent
 * @property-read int|null $notifications_count
 */
class VerificationCard extends Model
{
    use Notifiable, Filterable;

    protected $table = 'verification_card';

    protected $fillable = [
        'id_user',
        'id_order',
        'card_number',
        'card_number_string',
        'name',
        'image',
        'email',
        'is_verified',
        'hash_id',
        'id_currency',
        'status',
        'ip_address',
        'user_agent',
        'is_local_image',
        'text_message'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(VerificationCardFilter::class);
    }

    public function currency() {
        return $this->hasOne(Currency::class,'id','id_currency');
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id', 'id_order');
    }
}
