<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use App\Models\Filters\VerificationCardFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class VerificationCardCategory extends Model
{
    use HasTranslations;

    protected $table = 'verification_card_category';

    public $translatable = ['name'];

    protected $fillable = [
        'name', 'sorting', 'status'
    ];

    public function instructions()
    {
        return $this->hasMany(VerificationCardInstruction::class,'id_category', 'id');
    }
}
