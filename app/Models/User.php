<?php

namespace App\Models;

use App\Common\Packages\ReferralSystem\Models\ReferralRelationship;
use App\Common\Packages\ReferralSystem\Traits\ReferralsMember;
use App\Jobs\VerifyEmailJob;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmail;
use Carbon\Carbon;
use Cog\Laravel\Ban\Traits\Bannable;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Cog\Contracts\Ban\Bannable as BannableContract;
use EloquentFilter\Filterable;
use Illuminate\Contracts\Auth\MustVerifyEmail;


class User extends Authenticatable implements BannableContract, MustVerifyEmail, HasLocalePreference
{
    use Notifiable,
        HasRoles,
        HasFactory,
        ReferralsMember,
        HasApiTokens,
        Bannable,
        Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_report_referral_data',
        'email',
        'password',
        'phone',
        'telegram',
        'last_login_at', // Последний вход
        'last_logout_at', // Последний выход
        'last_activity_at', // Последняя активность
        'google2fa_secret',
        'banned_at',
        'language',
        'safe_input',
        'restriction',
        'username',
        'ip_address',
        'deactivation',
        'is_order',
        'auto_withdrawal',
        'min_withdrawal',
        'current_page',
        'is_unique_user',
        'order_num',
        'is_horizon',
        'ip_changed',
        'role_expired_at',
        'is_follow_referral',
        'is_notify_email',
        'is_password_reset',
        'user_agent',
        'is_pay_referral',
        'is_pay_cashback',
        'is_backup',
        'num_auth',
        'is_verification',
        'provider',
        'provider_id',
        'is_download_codes',
        'backup_code_secret',
        'phone_code',
        'admin_phone',
        'phone_hash',
        'is_vip_client',
        'user_browser',
        'user_device',
        'user_style',
        'restapi_key',
        'is_enabled_restapi',
        'is_guest',
        'personal_discount',
        'personal_ref_discount',
        'max_ref_discount',
        'security_order_page_code',
        'is_enable_order_paginate',
        'is_verify_account',
        'is_hidden_ip_address',
        'is_checkbox_rules',
        'is_checkbox_aml'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'google2fa_secret'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_follow_referral'  => 'boolean',
        'role_expired_at' => 'datetime',
        'last_login_at' => 'datetime',
        'last_logout_at' => 'datetime',
        'last_activity_at' => 'datetime',
        'password' => 'hashed',
    ];


    //Add the below function
    //Add the below function
    public function messages()
    {
        return $this->hasMany(TaskMessage::class);
    }

    /**
     * Отправляем уведомление для подтверждения почты
     *
     * @return mixed
    */
    public function sendEmailVerificationNotification() {

        if(!iEXSetting('is_user_verified_email')) {
            return;
        }

        VerifyEmailJob::dispatch($this)->delay(now()->addSeconds(2))
        ->onQueue('low');
    }

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\UserFilter::class);
    }

    public function getIsAdminAttribute() {
        return true;
    }

    public function user_balance() {
        return $this->hasOne(UserBalance::class, 'id_user', 'id');
    }

    public function withdrawal_request()
    {
        return $this->hasOne(WithdrawalRequest::class, 'id_user', 'id')->orderByDesc('id');
    }

    public function unpaid_withdrawal_request() {
        return $this->hasOne(WithdrawalRequest::class,'id_user','id')->where('status', '=', 0);
    }

    public function isOnline() {
        return Cache::has('user-is-online-' . $this->id);
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return config('services.slack.webhook_url');
    }

    /**
     * Ecrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string
     */
    public function setGoogle2faSecretAttribute($value)
    {
        $this->attributes['google2fa_secret'] = encrypt($value);
    }

    /**
     * Decrypt the user's google_2fa secret.
     *
     * @param  string  $value
     * @return string
     */
    public function getGoogle2faSecretAttribute($value)
    {
        if(empty($value)) {
            return 'У вас отсутствует ключ, Обратитесь к администратору';
            exit;
        }

        return decrypt($value);
    }

    /**
     * Статистика по заявкам
     *
     * @param string $time
     * @param int $status
     * @return integer
     */
    public function countStatTask($status = 4, $time = 'today')
    {
        $carbon = null;
       if($time == 'today') {
           $carbon = Carbon::today();
       } elseif($time == 'week') {
           $carbon = Carbon::today()->startOfWeek();
       } elseif($time == 'month') {
           $carbon = Carbon::today()->startOfMonth();
       }

       return Task::where([
           ['id_manager', $this->getAttribute('id')],
           ['status', $status]])->whereDate('created_at', ($time == 'today' ? '=' : '>='), $carbon)->count();
    }

    public function totalStatTask($time)
    {
        $carbon = null;
        $type = null;
        if($time == 'today') {
            $carbon = Carbon::today();
        } elseif($time == 'week') {
            $carbon = Carbon::today()->startOfWeek();
        } elseif($time == 'month') {
            $carbon = Carbon::today()->startOfMonth();
        }

        $task = TaskConvertLog::orderBy('id', 'desc');

        $task->whereHas('task', function ($query)
        {
            $query->where([
                ['id_manager', $this->getAttribute('id')],
                ['status', 4]
            ]);
        })->whereDate('created_at', ($time == 'today' ? '=' : '>='), $carbon);

        $amount = 0;
        if($task->count() > 0) {
            foreach($task->get() as $item) {
                $amount += $item->to_rub;
            }
        }

        return number_format($amount,2,'.',' ');
    }

    public function managerHistory($type = 'count', $time = null)
    {
        return (float)\App\Models\Task::where([
            ['id_manager', $this->getAttribute('id')],
            ['status', 4]])
            ->whereDate('updated_at', '=',  \Illuminate\Support\Carbon::parse($time)->toDateString())->count();
    }

    /**
     * Количество успешных обменов
     *
     * @return float
    */
    public function successOrdersCount()
    {
        return $this->hasMany(Task::class,'id_user', 'id')->count();
    }

    public function successOrderSum()
    {
        return $this->hasMany(TaskUser::class, 'id_user', 'id')->sum('convert_to_usd');
    }

    public function task_user() {
        return $this->hasMany(TaskUser::class, 'id_user', 'id');
    }


    public function tasks() {
        return $this->hasMany(Task::class,'id_user', 'id');
    }

    /**
     * Лог авторизаций
     */
    public function user_auth() {
        return $this->hasMany(UserAuth::class, 'id_user', 'id');
    }

    /**
     * Количество IP Адресов
     */
    public function countIPAddress() {
        return $this->hasMany(UserAuth::class, 'ip', 'ip_address')->count();
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * От реферала
    */
    public function fromReferral()
    {
        return $this->hasOne(ReferralRelationship::class,'user_id', 'id');
    }

    /**
     * Количество удачных сделок
     *
     * @return integer
     */
    public function countSuccessfulTransaction()
    {
        return $this->hasMany(Task::class,'id_user', 'id')->where('status','=',4)->count();
    }

    /**
     * Количество отклоненных сделок
     *
     * @return integer
     */
    public function countFailedTransaction()
    {
        return $this->hasMany(Task::class,'id_user', 'id')->where('status','=',5)->count();
    }

    /**
     * Информация о последней заявке
    */
    public function lastedOrderAt()
    {
        return $this->hasOne(Task::class,'id_user','id')->where('status', '=', 4)->orderByDesc('id');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Маршрутные уведомления для канала Nexmo.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        return $this->phone;
    }

    /**
     * Маршрутные уведомления для канала Smscru.
     *
     * @return string
     */
    public function routeNotificationForSmscru()
    {
        return $this->phone;
    }

    public function routeNotificationForSmsru()
    {
        return $this->phone;
    }

    public function notifyAuthenticationLogVia()
    {
        return ['mail'];
    }

    public function preferredLocale()
    {
        return $this->language;
    }
}
