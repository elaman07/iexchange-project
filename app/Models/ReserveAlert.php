<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReserveAlert
 *
 * @property int $id
 * @property int $id_reserve
 * @property int $is_telegram
 * @property int $is_email
 * @property float $threshold
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $count_alert
 * @property-read \App\Models\Reserve $reserves
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereCountAlert($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereIdReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereIsEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereIsTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveAlert whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReserveAlert extends Model
{
    protected $table = 'reserves_alerts';

    protected $fillable = [
        'id_reserve',
        'is_telegram',
        'is_email',
        'threshold',
        'count_alert'
    ];

    public function reserves() {
        return $this->hasOne(Reserve::class,'id','id_reserve');
    }
}
