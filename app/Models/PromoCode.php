<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PromoCode extends Model
{
    protected $table = 'promo_codes';

    protected $fillable = [
        'name',
        'id_manager',
        'count_uses',
        'used',
        'discount_percent',
        'code',
        'expired_at',
        'status',
        'permitted_directions',
        'forbidden_directions'
    ];

    public function manager(): HasOne
    {
        return $this->hasOne(User::class,'id','id_manager');
    }
}
