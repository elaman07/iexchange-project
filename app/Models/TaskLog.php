<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskLog
 *
 * @property int $id
 * @property int $id_task
 * @property int $id_user
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskLog extends Model
{
    protected $table = 'task_log';

    protected $fillable = [
        'id_task','id_user','text'
    ];
}
