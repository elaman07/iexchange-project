<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DirectionHasField extends Model
{
    use HasFactory;


    protected $table = 'directions_has_fields';


    protected $fillable = [
        'model_id',
        'model_type',
        'direction_field_id',
    ];
}
