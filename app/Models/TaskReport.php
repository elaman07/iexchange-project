<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskReport
 *
 * @property int $id
 * @property int $id_task
 * @property float $profit
 * @property float $add_course
 * @property float $your_add_course
 * @property float $commission
 * @property float $commission_s
 * @property int $is_group_commission
 * @property float $group_commission_value
 * @property int $sign
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $profit_usd
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereAddCourse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereCommissionS($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereGroupCommissionValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereIsGroupCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereProfit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereProfitUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereSign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereYourAddCourse($value)
 * @mixin \Eloquent
 * @property float $profit_s
 * @property float $profit_rub
 * @property int $is_bestchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereIsBestchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereProfitRub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskReport whereProfitS($value)
 */
class TaskReport extends Model
{
    protected $table = 'task_reports';

    protected $fillable = [
        'id_task',
        'profit',
        'add_course',
        'your_add_course',
        'commission',
        'commission_s',
        'is_group_commission',
        'group_commission_value',
        'sign',
        'profit_usd',
        'profit_rub',
        'is_bestchange'
    ];
}
