<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 24.03.2019
 * Time: 11:22
 */

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Advantage
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $title
 * @property string|null $content
 * @property string|null $icon
 * @property string|null $link
 * @property bool $is_target
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereIsTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User $user
 * @property int $sorting
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Advantage whereSorting($value)
 */
class Advantage extends Model
{
    use HasTranslations;

    protected $table = 'advantage';

    public $translatable = ['title', 'content'];

    protected $fillable = [
        'id_user',
        'title',
        'content',
        'icon',
        'link',
        'is_target',
        'status',
        'sorting'
    ];

    /**
     * Атрибуты, которые должны быть приведены к нативным типам.
     *
     * @var array
     */
    protected $casts = [
        'is_target' =>  'boolean',
        'status'    =>  'integer'
    ];


    public function user() {
        return $this->hasOne(User::class,'id', 'id_user');
    }
}
