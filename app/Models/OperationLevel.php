<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OperationLevel
 *
 * @property int $id
 * @property int $id_level_group
 * @property int $id_operator
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\OperatorLevelGroup $level_group
 * @property-read \App\Models\User $manager
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel whereIdLevelGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel whereIdOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationLevel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OperationLevel extends Model
{
    protected $table = 'operation_levels';

    protected $fillable = [
        'id_level_group',
        'id_operator',
        'status'
    ];

    public function manager() {
        return $this->hasOne(User::class,'id','id_operator');
    }

    public function level_group() {
        return $this->hasOne(OperatorLevelGroup::class,'id','id_level_group');
    }
}
