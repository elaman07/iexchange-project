<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawalWallets extends Model
{
    protected $table = 'withdrawal_wallets';

    protected $fillable = [
        'id_user',
        'wallet',
        'id_currency'
    ];
}
