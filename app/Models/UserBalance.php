<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserBalance
 *
 * @property int $id
 * @property int $id_user
 * @property float|null $balance
 * @property float|null $balance_reward
 * @property int $id_code_currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CodeCurrency $code_currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereBalanceReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereIdCodeCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBalance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserBalance extends Model
{
    protected $table = 'user_balance';

    protected $fillable = [
        'id_user',
        'id_code_currency',
        'balance',
        'balance_reward',
        'referral_total_profit',
        'referral_total_withdrawal'
    ];


    public function code_currency() {
        return $this->hasOne(CodeCurrency::class,'id','id_code_currency');
    }
}
