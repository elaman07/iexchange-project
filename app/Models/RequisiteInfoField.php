<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Translatable\HasTranslations;


class RequisiteInfoField extends Model
{
    use HasTranslations;

    protected $table = 'requisites_info_fields';

    public $translatable = ['key_name', 'value_name'];

    protected $fillable = [
        'key_name',
        'value_name',
        'status',
        'user_id'
    ];


    public function requisites(): MorphToMany
    {
        return $this->morphedByMany(Requisites::class, 'model','requisites_has_info_fields','field_id');
    }
}
