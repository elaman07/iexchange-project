<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FaqCategory
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Faq[] $faq
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $faq_count
 * @property int $sorting
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereSorting($value)
 */
class FaqCategory extends Model
{
    use HasTranslations;

    protected $table = 'faq_category';

    protected $fillable = [
        'name',
        'sorting'
    ];


    public $translatable = ['name'];


    public function faq() {
        return $this->hasMany(Faq::class,'id_group','id');
    }
}
