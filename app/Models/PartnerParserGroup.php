<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerParserGroup extends Model
{
    protected $table = 'partner_parser_groups';

    protected $fillable = [
        'name',
        'status',
        'link',
        'sorting'
    ];

    public function rates() {
        return $this->hasMany(PartnerParserRates::class,'partner_id', 'id');
    }
}
