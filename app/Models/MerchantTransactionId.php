<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MerchantTransactionId
 *
 * @property int $id
 * @property int $id_task
 * @property string|null $transaction_id
 * @property string|null $provider
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantTransactionId whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Task|null $tasks
 */
class MerchantTransactionId extends Model
{
    protected $table = 'merchant_transaction_ids';

    protected $fillable = [
        'id_task',
        'transaction_id',
        'provider'
    ];

    public function tasks()
    {
        return $this->hasOne(Task::class,'id','id_task');
    }
}
