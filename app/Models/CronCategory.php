<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CronCategory
 *
 * @property int $id
 * @property int $pos
 * @property string|null $name
 * @property int $plus
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cron[] $cron
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory wherePlus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory wherePos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CronCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $cron_count
 */
class CronCategory extends Model
{
    protected $table = 'cron_category';

    protected $fillable = [
        'name'
    ];

    public function cron()
    {
        return $this->hasMany(Cron::class, 'id_group','id');
    }
}
