<?php

namespace App\Models;

use App\Models\Filters\LogMerchantFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;


class LogMerchantEvent extends Model
{
    use Filterable;

    protected $table = 'log_merchants_events';

    protected $fillable = [
        'provider',
        'id_order',
        'ip_address',
        'url',
        'headers',
        'content',
        'response'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\LogMerchantEventFilter::class);
    }
}
