<?php

namespace App\Models;

use App\Models\Filters\LogCheckPayFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LogCheckPay
 *
 * @property int $id
 * @property int $id_task
 * @property int $id_user
 * @property int $event_type
 * @property string|null $event_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $provider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereEventType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereEventValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogCheckPay whereLike($column, $value, $boolean = 'and')
 */
class LogCheckPay extends Model
{
    use Filterable;

    protected $table = 'log_check_pay';

    protected $fillable = [
        'id_task',
        'id_user',
        'provider',
        'event_type',
        'event_value'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(LogCheckPayFilter::class);
    }
}
