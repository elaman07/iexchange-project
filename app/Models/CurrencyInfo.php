<?php
namespace App\Models;

use App\Models\Filters\CurrencyFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class CurrencyInfo extends Model
{


    protected $table = "currencies";

    protected $fillable = [
        'id_payment',
        'id_code_currency',
        'designation_xml',
        'convert_by',
        'number_format',
        'day_limit_give',
        'day_limit_receive',
        'month_limit_in',
        'month_limit_out',
        'min_char',
        'visible',
        'max_char',
        'field_name_from',
        'field_name_to',
        'field_comment_from',
        'field_comment_to',
        'allowed_char',
        'status',
        'created_at',
        'updated_at',
        'visible_give',
        'visible_receiving',
        'id_filter_currency',
        'visible_code_currency',
        'background_color',
        'is_email_verification_modal',
        'text_color',
        'char_default',
        'mask_input',
        'is_auto_check_modal',
        'unlimited_reserve',
        'sorting_1',
        'sorting_tariffs',
        'id_pay',
        'is_valid_account',
        'is_qrcode',
        'prefix_qrcode',
        'is_qrcode_amount',
        'is_payment_default',
        'is_payment_unique',
        'account_number_field',
        'notice_in',
        'transfer_percent_reserve',
        'transfer_amount_reserve',
        'remove_spaces_requisite',
        'payout_commission',
        'sorting_reserve',
        'is_archive',
        'id_group',
        'is_hold',
        'is_enabled_verification',
        'is_out_enabled_verification',
        'min_amount_verification',
        'min_out_amount_verification',
        'is_user_verification',
        'hold_in_hours',
        'commission_merchant_percent',
        'commission_merchant_currency',
        'payout_commission_amount',
        'fee_no_verified_merchant',
        'hold_delay',
        'id_auto_reserve',
        'max_limit_in_reserve',
        'hour_limit_order_pending',
        'hour_limit_order_process',
        'profit_percent_reserve',
        'is_card_detail',
        'max_display_reserve',
        'is_income_banner',
        'recount_percent',
        'is_recount_default',
        'commission_payment_currency',
        'notice_out',
        'is_unique_recount_order',
        'is_enable_auto_recount_order',
        'unique_recount_percent',
        'recount_time_hours',
        'recount_time_minutes',
        'desc_exchange',
        'is_amlbot_in',
        'is_amlbot_out',

        'is_getblockbot_in',
        'is_getblockbot_out',
        'is_getblockbot_tx_in',

        'out_pay_min_amount',
        'out_pay_max_amount',
        'out_pay_day_limit',
        'out_pay_month_limit',
        'is_allow_amount_space',

        'created_user_id',
        'updated_user_id',
        'is_fire',
        'tech_currency_name',
        'button_create_order',
        'button_create_order_text',
        'kyc_enabled',
        'formalization_text',
        'is_aml_check_cost_reserve',
        'aml_check_cost'
    ];

    public $translatable  = [
        'desc_exchange',
        'notice_in',
        'notice_out',
        'field_name_from',
        'field_name_to',
        'field_comment_from',
        'field_comment_to',
        'account_number_field',
        'button_create_order',
        'button_create_order_text',
        'formalization_text'
    ];

    protected $casts = [
        'remove_spaces_requisite' => 'boolean'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(CurrencyFilter::class);
    }


    /**
     * Включите в запрос, чтобы отображались только активные валюты.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query) {
        return $query->where('status', '!=', 2);
    }

    public function scopeEnabledCurrency($query) {
        return $query->where('status', '!=', 2)->where('is_archive', '=', 0);
    }

    public function scopeHideDisabled($query) {
        return $query->where('status', '!=', 1);
    }

    /**
     * Дополнительные поля, отдаю
     */
    public function currency_in_fields(): MorphToMany
    {
        return $this->morphToMany(
            CurrencyFields::class,
            'model',
            'currency_in_has_fields',
            'model_id',
            'field_id'
        );
    }

    /**
     * Дополнительные поля, получаю
     */
    public function currency_out_fields(): MorphToMany
    {
        return $this->morphToMany(
            CurrencyFields::class,
            'model',
            'currency_out_has_fields',
            'model_id',
            'field_id'
        );
    }


    /**
     * Дополнительные поля для реквизитов
     */
    public function requisites_fields(): MorphToMany
    {
        return $this->morphToMany(
            RequisiteField::class,
            'model',
            'currency_requisites_has_fields',
            'model_id',
            'field_id'
        );
    }


    /**
     * Выплаты
     */
    public function pay() {
        return $this->hasOne(GatewayPayment::class,'id','id_pay');
    }


//    /**
//     * Мерчант
//     */
//    public function merchant() {
//        return $this->hasOne(GatewayMerchant::class, 'id','id_merchant');
//    }

    /**
     * Мерчант
     */
    public function merchants(): MorphToMany
    {
        return $this->morphToMany(
            GatewayMerchant::class,
            'model',
            'currency_merchants',
            'model_id',
            'gateway_merchant_id'
        );
    }


    /**
     * Проверка оплаты
    */
    public function check_pay() {
        return $this->hasOne(GatewayPayment::class,'id', 'id_check_pay');
    }

    public function auto_reserve() {
        return $this->hasOne(GatewayPayment::class,'id','id_auto_reserve');
    }

    public function payment() {
        return $this->hasOne(Payment::class,'id', 'id_payment');
    }



    public function payments() {
        return $this->hasMany(Payment::class,'id', 'id_payment');
    }

    public function code_currency() {
        return $this->hasOne(CodeCurrency::class,'id', 'id_code_currency');
    }

    public function reserve() {
        return $this->hasOne(Reserve::class, 'id_currency', 'id');
    }

    public function requisites() {
        return $this->hasOne(Requisites::class,'id_currency', 'id')->where('status','=',1);
    }

    public function requisites_many() {
        return $this->hasMany(Requisites::class,'id_currency', 'id')->where('status', '=', 1);
    }

    public function filter_currency() {
        return $this->hasOne(FilterCurrency::class,'id','id_filter_currency');
    }

    public function filter_currency_many() {
        return $this->hasMany(FilterCurrency::class,'id','id_filter_currency')->groupBy('name');
    }

    public function direction_exchange1() {
        return $this->hasOne(DirectionExchange::class,'id_currency1', 'id');
    }

    public function direction_exchange2() {
        return $this->hasOne(DirectionExchange::class,'id_currency2', 'id');
    }

    /**
     * Счетчик заявок (Отдали)
     */
    public function tasks_in_count() {
        return  $this->hasMany(DirectionExchange::class,'id_currency1', 'id')
            ->leftJoin('tasks', 'direction_exchange.id', '=', 'tasks.id_direction_exchange')
            ->whereNotIn('tasks.status', [11])
            ->count('tasks.id');
    }

    /**
     * Счетчик заявок (Получили)
    */
    public function tasks_out_count() {
        return  $this->hasMany(DirectionExchange::class,'id_currency2', 'id')
            ->leftJoin('tasks', 'direction_exchange.id', '=', 'tasks.id_direction_exchange')
            ->whereNotIn('tasks.status', [11])
            ->count('tasks.id');
    }

    /**
     * Информация по направления для (Отдаю)
     */
    public function direction_exchange_in() {
        return $this->hasMany(DirectionExchange::class,'id_currency1', 'id');
    }
    /**
     * Информация по направления для (Получаю)
    */
    public function direction_exchange_out() {
        return $this->hasMany(DirectionExchange::class,'id_currency2', 'id');
    }

    public function in_base_currency_fields() {
        return $this->hasMany(CurrencyFields::class,'id_currency', 'id')
            ->where('when_print', '=',0)->whereIn('key_id', ['sender_fullname', 'income_unk']);
    }

    public function in_addition_currency_fields() {
        return $this->hasMany(CurrencyFields::class,'id_currency', 'id')
            ->where('when_print', '=',0)->whereNotIn('key_id', ['sender_fullname', 'income_unk']);
    }

    /**
     * Получение общего резерва в USD
    */
    public static function totalAmountUSD()
    {
        $total = 0;
        foreach (Currency::active()->where('status', 0)->get() as $item)
        {
            if(isset($item->reserve->summa) and $item->reserve->summa > 0) {
                $total += convert_to_usd($item->code_currency->name, $item->reserve->summa);
            }
        }

        return number_format($total, 2,'.',' ');
    }

    public function totalIn()
    {
        return $this->hasMany(DirectionExchange::class,'id_currency1', 'id')
            ->select('direction_exchange.id', 'tasks.id_direction_exchange', 'tasks.status', 'tasks.give_price')
            ->leftJoin('tasks', 'direction_exchange.id', '=', 'tasks.id_direction_exchange')
            ->where('tasks.status', '=', 4)
            ->sum('tasks.give_price');
    }

    public function totalOut()
    {
        return $this->hasMany(DirectionExchange::class,'id_currency2', 'id')
            ->select('direction_exchange.id', 'tasks.id_direction_exchange', 'tasks.status', 'tasks.receiving_price')
            ->leftJoin('tasks', 'direction_exchange.id', '=', 'tasks.id_direction_exchange')
            ->where('tasks.status', '=', 4)
            ->sum('tasks.receiving_price');

    }

    public function mainstream() {
        return $this->hasOne(DirectionExchange::class,'id_currency1', 'id')
            ->where('is_main', '=', 1)->first();
    }

    public function commands() {
        return $this->hasMany(CurrencyCommand::class,'id_currency','id')->orderBy('sorting');
    }
    public function currency_analytics() {
        return $this->hasOne(CurrencyAnalytics::class,'id_currency', 'id');
    }

    public function updated_user()
    {
        return $this->hasOne(User::class,'id', 'updated_user_id');
    }
}
