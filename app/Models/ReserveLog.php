<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReserveLog
 *
 * @property int $id
 * @property int $id_reserve
 * @property int $id_task
 * @property int $id_currency
 * @property float|null $summa
 * @property float|null $commission
 * @property string|null $history
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task $tasks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereHistory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereIdReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereSumma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $summa_not_fee
 * @property string|null $summa_with_fee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereSummaNotFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLog whereSummaWithFee($value)
 */
class ReserveLog extends Model
{
    protected $table = 'reserve_log';

    protected $fillable = [
        'id_reserve',
        'id_currency',
        'summa',
        'commission',
        'type',
        'id_task',
        'summa_not_fee',
        'summa_with_fee',
        'history'
    ];

    public function tasks() {
        return $this->hasOne(Task::class,'id', 'id_task');
    }
}
