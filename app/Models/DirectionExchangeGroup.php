<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DirectionExchangeGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int $id_user
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DirectionExchangeGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange
 * @property-read int|null $direction_exchange_count
 */
class DirectionExchangeGroup extends Model
{
    protected $table = 'direction_exchange_group';

    protected $fillable = [
        'name',
        'id_user'
    ];

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_group_direction', 'id');
    }
}
