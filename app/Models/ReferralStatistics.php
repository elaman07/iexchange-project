<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.05.2019
 * Time: 20:50
 */

namespace App\Models;

use App\Common\Packages\Referral\Models\ReferralLink;
use App\Models\Filters\ReferralStatisticsFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReferralStatistics
 *
 * @property int $id
 * @property string|null $ref_hash
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $ip_address
 * @property int $id_user
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereRefHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_archive
 * @property string|null $user_agent
 * @property string|null $cur_from
 * @property string|null $cur_to
 * @property string|null $from_and_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereCurFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereCurTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereFromAndTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereIsArchive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReferralStatistics whereUserAgent($value)
 */
class ReferralStatistics extends Model
{
    use Filterable;

    protected $table = 'referral_statistics';

    protected $fillable = [
        'ref_hash',
        'ip_address',
        'id_user',
        'user_agent',
        'is_archive',
        'cur_from',
        'cur_to',
        'from_and_to'
    ];

    protected $casts = [
        'ref_hash' => 'string'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(ReferralStatisticsFilter::class);
    }


    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }
}