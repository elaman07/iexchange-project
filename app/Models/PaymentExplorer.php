<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentExplorer
 *
 * @property int $id
 * @property int $id_payment
 * @property string|null $link
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Payment $payment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereIdPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentExplorer whereText($value)
 */
class PaymentExplorer extends Model
{
    protected $table = 'payment_explorer';

    protected $fillable = [
        'id_payment',
        'link',
        'text',
        'status'
    ];

    public function payment() {
        return $this->hasOne(Payment::class,'id','id_payment');
    }
}
