<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArchiveReport
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $hash_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArchiveReport whereHashId($value)
 */
class ArchiveReport extends Model
{
    protected $table = 'archive_reports';

    protected $fillable = [
        'name',
        'hash_id'
    ];
}
