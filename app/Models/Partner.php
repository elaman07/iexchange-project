<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Partner
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $link
 * @property string|null $logo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $sorting
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereSorting($value)
 */
class Partner extends Model
{
    use HasTranslations;

    protected $table = 'partners';

    public $translatable = ['name', 'link'];

    protected $fillable = [
        'name',
        'logo',
        'link',
        'sorting'
    ];
}
