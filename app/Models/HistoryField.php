<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\HistoryField
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_currency1
 * @property int $id_currency2
 * @property string|null $filed_give
 * @property string|null $filed_receiving
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereFiledGive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereFiledReceiving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereIdCurrency1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereIdCurrency2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryField whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HistoryField extends Model
{
    protected $table = 'history_fields';

    protected $fillable = [
        'id_user',
        'id_currency1',
        'id_currency2',
        'filed_give',
        'filed_receiving'
    ];
}
