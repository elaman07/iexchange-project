<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\WalletsHistory
 *
 * @property int $id
 * @property int $id_task ID заявки
 * @property string|null $txid ID транзакции
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory whereTxid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WalletsHistory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsHistory whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WalletsHistory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WalletsHistory withoutTrashed()
 */
class WalletsHistory extends Model
{
    use SoftDeletes;

    protected $table = 'wallets_history';

    protected $fillable = [
        'id_task',
        'txid'
    ];
}
