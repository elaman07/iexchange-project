<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BestchangeDataLog
 *
 * @property int $id
 * @property int $type_log
 * @property int $where_from
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog whereTypeLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeDataLog whereWhereFrom($value)
 * @mixin \Eloquent
 */
class BestchangeDataLog extends Model
{
    protected $table = 'bestchange_data_log';

    protected $fillable = [
        'type_log',
        'message',
        'where_from'
    ];
}
