<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\YandexCurrency
 *
 * @property int $id
 * @property int $id_currency
 * @property int $selected
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Requisites $requisites
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency whereSelected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\YandexCurrency whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class YandexCurrency extends Model
{
    protected $table = 'yandex_currencies';

    protected $fillable = [
        'id_currency',
        'status',
        'selected'
    ];

    public function requisites() {
        return $this->hasOne(Requisites::class,'id_currency','id_currency');
    }
}