<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParserExchangeHttpLog extends Model
{
    protected $table = 'parser_exchange_http_logs';

    protected $fillable = [
        'source',
        'status',
        'data',
        'url'
    ];
}
