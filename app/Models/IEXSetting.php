<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 20.09.2020
 * Time: 23:53
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\IEXSetting
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @method static \Illuminate\Database\Eloquent\Builder|IEXSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IEXSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IEXSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|IEXSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IEXSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IEXSetting whereValue($value)
 * @mixin \Eloquent
 */
class IEXSetting extends Model
{
    protected $table = 'iex_settings';

    protected $fillable = [
        'key',
        'value',
    ];
}