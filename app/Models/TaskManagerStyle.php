<?php
namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskManagerStyle extends Model
{
    protected $table = "tasks_managers_styles";

    protected $fillable = [
        'user_id',
        'count_block_home',
        'is_hide_block_client',
        'is_hide_block_receive_service',
        'is_hide_block_give_service'
    ];

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
