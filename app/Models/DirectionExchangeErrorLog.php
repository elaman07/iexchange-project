<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class DirectionExchangeErrorLog extends Model
{
    protected $table = 'direction_exchange_error_log';

    protected $fillable = [
        'id_direction_exchange',
        'direction_name',
        'where_error',
        'status',
        'level_risk',
        'text'
    ];

    public function direction_exchange() {
        return $this->hasOne(DirectionExchange::class,'id','id_direction_exchange');
    }
}
