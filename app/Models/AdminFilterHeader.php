<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminFilterHeader
 *
 * @property int $id
 * @property int $id_user
 * @property int $sorting
 * @property string|null $name
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_common
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AdminFilterUser[] $admin_filters_user
 * @property-read int|null $admin_filters_user_count
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereIsCommon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterHeader whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdminFilterHeader extends Model
{
    protected $table = 'admin_filter_header';

    protected $fillable = [
        'id_user',
        'name',
        'sorting',
        'status',
        'is_common',
        'type_filter'
    ];

    public function admin_filters_user()
    {
        return $this->hasMany(AdminFilterUser::class,'id_filter_header','id');
    }
}
