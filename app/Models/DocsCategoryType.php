<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DocsCategoryType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DocsCategory[] $docs_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DocsCategoryType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read int|null $docs_category_count
 */
class DocsCategoryType extends Model
{
    protected $table = 'docs_category_type';

    protected $fillable = [
        'name'
    ];

    public function docs_category() {
        return $this->hasMany(DocsCategory::class,'id_type','id');
    }
}
