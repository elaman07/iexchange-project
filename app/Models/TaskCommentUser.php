<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskCommentUser extends Model
{
    use HasFactory;

    protected $table = 'tasks_comments_users';

    protected $fillable = [
        'id_manager',
        'id_task',
        'message',
        'class_style'
    ];


    public function user() {
        return $this->hasOne(User::class,'id','id_manager');
    }
}
