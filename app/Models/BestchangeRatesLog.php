<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BestchangeRatesLog
 *
 * @property int $id
 * @property int $id_bestchange_rates
 * @property float $in_price
 * @property float $out_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\BestChangeRatesModel $bestchange_rates
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog whereIdBestchangeRates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog whereInPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog whereOutPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestchangeRatesLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BestchangeRatesLog extends Model
{
    protected $table = 'bestchange_rates_log';

    protected $fillable = [
        'id_bestchange_rates',
        'in_price',
        'out_price'
    ];

    public function bestchange_rates() {
        return $this->hasOne(BestChangeRatesModel::class,'id','id_bestchange_rates');
    }
}
