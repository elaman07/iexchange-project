<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\WalletTransaction
 *
 * @property int $id
 * @property int $id_task
 * @property string $amount
 * @property string $txid
 * @property string $address
 * @property int $confirmations
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereConfirmations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereTxid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WalletTransaction onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletTransaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WalletTransaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WalletTransaction withoutTrashed()
 */
class WalletTransaction extends Model
{
    use SoftDeletes;

    protected $table = 'wallet_transactions';

    protected $fillable = [
        'id_task',
        'address',
        'amount',
        'confirmations',
        'txid'
    ];
}
