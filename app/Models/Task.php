<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.09.2017
 * Time: 22:20
 */

namespace App\Models;

use App\Common\Packages\ReferralSystem\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Task
 *
 * @property int $id
 * @property string $big_id
 * @property string $random_id
 * @property string $type
 * @property int $id_user
 * @property int $id_direction_exchange
 * @property int $id_payment_requisites
 * @property int|null $id_wallets_addresses
 * @property string|null $give_price
 * @property string|null $receiving_price
 * @property string|null $description
 * @property string $ip
 * @property int $status
 * @property string|null $from_shot
 * @property string|null $to_shot
 * @property int $passed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $started_at
 * @property string|null $description_receiving
 * @property string|null $description_give
 * @property float $discount1
 * @property float $discount2
 * @property float $markup1
 * @property float $markup2
 * @property string|null $email
 * @property int $scans Кто просматривает заявку
 * @property int $id_payment_list
 * @property string|null $payment_address Генерируемые номера счетов (Bitcoin, Eth ...)
 * @property string|null $rejection_reason Причина отклонения заявки
 * @property int $start Запускаем выполнение заявки (0 -> Не запущен процесс) (1 -> Запущен)
 * @property string|null $lead_time Время выполнения заявки со статусов "Ожидается выполнение"
 * @property string|null $message_success Текст при успешном завершении заявки
 * @property int $check_status Статус отправки чека клиенту
 * @property int $id_manager ID менеджера, который выполнил заявку.
 * @property string|null $course_display
 * @property int $merchant_status Был ли переход на мерчант
 * @property string|null $excode
 * @property int $check_excode
 * @property string|null $sender_fullname
 * @property string|null $recipient_fullname
 * @property int $is_frozen Замороженные средства
 * @property int $is_reserve Каким образом будут сниматься резервы.
 * @property int $skip_reserve Пропустить пополнение резерва
 * @property string|null $course_float
 * @property string|null $payment_field
 * @property int $category_reject
 * @property string|null $outcome_unk
 * @property int $is_archive
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \App\Models\DirectionExchange $direction_exchange
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange_many
 * @property-read \App\Models\HistoryExCode $history_excode
 * @property-read \App\Models\HistoryPaymentTransaction $history_payment_transaction
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\HistoryRecalculation[] $history_recalculation
 * @property-read \App\Models\User $manager
 * @property-read \App\Models\User $operator
 * @property-read \App\Models\Requisites $payment_requisites
 * @property-read \App\Models\RewardLog $reward_log
 * @property-read \App\Models\TaskConvertLog $task_convert_log
 * @property-read \App\Models\TaskInfo $task_info
 * @property-read \App\Models\TaskStatus $task_status
 * @property-read \App\Models\TasksRatesData $tasks_rates_data
 * @property-read \App\Models\Transaction $transaction
 * @property-read \App\Models\User $user
 * @property-read \App\Models\WalletTransaction $wallet_transaction
 * @property-read \App\Models\WalletsAddresses $wallets_addresses
 * @property-read \App\Models\WalletsHistory $wallets_history
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task filter($input = array(), $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task limitOutput()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Task onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereBigId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCategoryReject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCheckExcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCheckStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCourseDisplay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCourseFloat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDescriptionGive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDescriptionReceiving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDiscount1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDiscount2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereExcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereFromShot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereGivePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdDirectionExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdPaymentList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdPaymentRequisites($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdWalletsAddresses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsArchive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsFrozen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereLeadTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMarkup1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMarkup2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMerchantStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMessageSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereOutcomeUnk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task wherePassed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task wherePaymentAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task wherePaymentField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereRandomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereReceivingPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereRecipientFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereRejectionReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereScans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereSenderFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereSkipReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereToShot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Task withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Task withoutTrashed()
 * @mixin \Eloquent
 * @property-read \App\Common\Packages\Referral\Models\ReferralLog $referral_log
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks_convert_log_many
 * @property \Illuminate\Support\Carbon|null $operator_started_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereOperatorStartedAt($value)
 * @property int $id_rejection_status
 * @property int $id_pending_status
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\PendingOrderStatus $pending_order_status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskChat[] $tasks_chat
 * @property-read \App\Models\TaskRejectionStatus $tasks_rejection_status
 * @property-read \App\Models\WalletsHistory $wallet_history
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdPendingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdRejectionStatus($value)
 * @property int $is_favorites
 * @property int $is_spam
 * @property-read \App\Models\TaskReport $task_report
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsFavorites($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsSpam($value)
 * @property int $is_bot
 * @property int $register_tx
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereRegisterTx($value)
 * @property int $merchant_incomplete_payment
 * @property int $merchant_overpayment
 * @property int $is_hold
 * @property int $in_flow_funds
 * @property \Illuminate\Support\Carbon|null $next_checkout_at
 * @property-read \App\Models\VerificationCard $verification_card
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereInFlowFunds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMerchantIncompletePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMerchantOverpayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereNextCheckoutAt($value)
 * @property string|null $income_unk1
 * @property string|null $income_unk2
 * @property string|null $outcome_unk1
 * @property string|null $outcome_unk2
 * @property-read int|null $audits_count
 * @property-read int|null $direction_exchange_many_count
 * @property-read int|null $history_recalculation_count
 * @property-read int|null $notifications_count
 * @property-read int|null $tasks_chat_count
 * @property-read int|null $tasks_convert_log_many_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIncomeUnk1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIncomeUnk2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereOutcomeUnk1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereOutcomeUnk2($value)
 * @property int $double_withdrawal
 * @property string|null $phone
 * @property int $is_drain_merchant
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDoubleWithdrawal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsDrainMerchant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task wherePhone($value)
 * @property int $pay_num
 * @property int $is_autopay_off
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsAutopayOff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task wherePayNum($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $de_no_group
 * @property-read int|null $de_no_group_count
 * @property string|null $merchant_provider
 * @property int $id_referral_link
 * @property int $is_new_user
 * @property-read \App\Models\TaskPrivateHash $task_private_hash
 * @property-read \App\Models\TaskCardDetail $tasks_card_details
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskField[] $tasks_in_addition_fields
 * @property-read int|null $tasks_in_addition_fields_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskField[] $tasks_out_addition_fields
 * @property-read int|null $tasks_out_addition_fields_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdReferralLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsNewUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMerchantProvider($value)
 * @property int $type_finished_order
 * @property string|null $unique_security_code
 * @property string|null $kunacode
 * @property int $is_autopay_modal
 * @property int $is_autopay_limit
 * @property int $id_edit_data_manager
 * @property-read \App\Models\User|null $edit_data_manager
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskDirectionField[] $tasks_direction_fields
 * @property-read int|null $tasks_direction_fields_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIdEditDataManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsAutopayLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsAutopayModal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereKunacode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereTypeFinishedOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUniqueSecurityCode($value)
 * @property string|null $telegram_id
 * @property string $in_price_fee
 * @property string $out_price_fee
 * @property int $is_ban_order_data
 * @property int $id_main_operator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskHistoryOperator[] $history_operators
 * @property-read int|null $history_operators_count
 * @property-read \App\Models\User|null $main_operator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TaskComment[] $task_comment
 * @property-read int|null $task_comment_count
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereIdMainOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereInPriceFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereIsBanOrderData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereOutPriceFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTelegramId($value)
 */
class Task extends Model
{
    use SoftDeletes, Filterable, Notifiable;

    protected $table = "tasks";

    protected $casts = [
        'operator_started_at' => 'datetime',
        'next_checkout_at' => 'datetime',
        'started_at' => 'datetime'
    ];

    protected $fillable = [
        'email',
        'id_user',
        'is_autopay_limit',
        'id_direction_exchange',
        'give_price',
        'scans',
        'scan_name',
        'operator_started_at',
        'receiving_price',
        'description',
        'ip',
        'status',
        'big_id',
        'random_id',
        'telegram_id',
        'from_shot',
        'to_shot',
        'discount1',
        'discount2',
        'id_order_step',
        'is_autopay_modal',
        'markup1',
        'markup2',
        'description_receiving',
        'description_give',
        'passed',
        'id_payment_requisites',
        'id_wallets_addresses',
        'id_payment_list',
        'payment_address',
        'type_finished_order',
        'rejection_reason',
        'start',
        'lead_time',
        'message_success',
        'check_status',
        'id_manager',
        'course_display',
        'id_main_operator',
        'course_float',
        'merchant_status',
        'income_code',
        'check_income_code',
        'kunacode',
        'sender_fullname',
        'recipient_fullname',
        'is_frozen',
        'is_reserve',
        'skip_reserve',
        'payment_field',
        'category_reject',
        'type',
        'is_archive',
        'id_rejection_status',
        'id_pending_status',
        'is_favorites',
        'is_ban_order_data',
        'is_spam',
        'is_bot',
        'register_tx',
        'merchant_incomplete_payment',
        'in_flow_funds',
        'merchant_overpayment',
        'is_hold',
        'is_drain_merchant',
        'next_checkout_at',
        'phone',
        'id_edit_data_manager',
        'double_withdrawal',
        'is_new_user',
        'pay_num',
        'is_autopay_off',
        'merchant_provider',
        'income_unk',
        'outcome_unk',
        'unique_security_code',
        'id_referral_link',
        'in_price_fee',
        'out_price_fee',
        'requisites_receive',
        'requisites_description',
        'queue_status',
        'id_merchant',
        'id_pay',
        'is_auto_check_pay',
        'id_payment_gateway',
        'id_promo_code',
        'promo_discount',
        'int_status_verification_card',
        'id_direction_requisites',
        'in_amount_merchant',
        'out_amount_pay',
        'public_id',
        'transfer_to_account',
        'is_status_pay_callback',
        'status_pay_api',
        'is_status_pay_email',
        'is_attempts_pay',
        'referral_hash',
        'give_price_default',
        'give_price_with_comm',
        'give_price_with_comm_pay',
        'give_price_fee_comm',
        'give_price_fee_pay',
        'give_price_reserve',
        'receiving_price_default',
        'receiving_price_with_comm',
        'receiving_price_with_comm_pay',
        'receiving_price_fee_comm',
        'receiving_price_fee_pay',
        'receiving_price_reserve',
        'receiving_price_discount',
        'receiving_price_discount_fee',
        'int_error_type',
        'error_type_text',
        'is_send_mail_create',
        'is_request_payment_type',
        'is_wallet_issued',
        'is_file_check',
        'is_wait_callback'
    ];

    public function scopeLimitOutput($query) {
        return $query->where('status', '!=', 2);
    }

    public function tasks_check_images(): HasOne
    {
        return $this->hasOne(TasksCheckImage::class, 'id_order','id');
    }

    public function task_messages()
    {
        return $this->hasMany(TaskMessage::class,'id_task','id')->orderBy('id', 'desc');
    }

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\OrderFilter::class);
    }

    public function promo_code(): HasOne {
        return $this->hasOne(PromoCode::class,'id', 'id_promo_code');
    }

    public function task_status() {
        return $this->hasOne(TaskStatus::class,'id','status');
    }

    public function task_info() {
        return $this->hasOne(TaskInfo::class,'id_task','id');
    }

    public function task_report() {
        return $this->hasOne(TaskReport::class,'id_task', 'id');
    }

    public function history_recalculation() {
        return $this->hasMany(HistoryRecalculation::class,'id_task','id');
    }


    public function tasks_chat() {
        return $this->hasMany(TaskChat::class,'id_task', 'id');
    }

    public function direction_exchange() {
        return $this->hasOne(DirectionExchange::class,'id','id_direction_exchange');
    }

    public function direction_exchange_many() {
        return $this->hasMany(DirectionExchange::class,'id', 'id_direction_exchange')
            ->groupBy('id_direction_exchange');
    }

    public function de_no_group() {
        return $this->hasMany(DirectionExchange::class,'id', 'id_direction_exchange');
    }

    public function transaction() {
        return $this->hasOne(Transaction::class,'id_task','id');
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function operator() {
        return $this->hasOne(User::class,'id','scans');
    }

    public function payment_requisites() {
        return $this->hasOne(Requisites::class,'id','id_payment_requisites');
    }

    public function direction_requisites(): HasOne
    {
        return $this->hasOne(DirectionRequisite::class,'id', 'id_direction_requisites');
    }

    public function wallets_addresses() {
        return $this->hasOne(WalletsAddresses::class,'id','id_wallets_addresses');
    }

    public function wallets_history() {
        return $this->hasOne(WalletsHistory::class,'id_task','id');
    }

    public function manager() {
        return $this->hasOne(User::class,'id','id_manager');
    }

    public function task_convert_log() {
        return $this->hasOne(TaskConvertLog::class,'id_task','id');
    }

    public function tasks_convert_log_many() {
        return $this->hasMany(Task::class,'id_task', 'id');
    }

    public function history_payment_transaction()
    {
        return $this->hasOne(HistoryPaymentTransaction::class, 'id_task','id');
    }

    public function history_code(){
        return $this->hasOne(HistoryCode::class,'id_task','id');
    }

    public function reward_log() {
        return $this->hasOne(RewardLog::class,'id_task','id');
    }

    public function tasks_rejection_status() {
        return $this->hasOne(TaskRejectionStatus::class,'id', 'id_rejection_status');
    }

    public function pending_order_status() {
        return $this->hasOne(PendingOrderStatus::class, 'id', 'id_pending_status');
    }

    public function wallet_transaction() {
        return $this->hasOne(WalletTransaction::class,'id_task','id');
    }

    public function wallet_history() {
        return $this->hasOne(WalletsHistory::class, 'id_task', 'id');
    }

    public function tasks_rates_data() {
        return $this->hasOne(TasksRatesData::class,'id_task','id');
    }

    /**
     * Информация о полях валюты отдаю
     *
     * @return HasMany
     */
    public function tasks_fields_currency_in(): HasMany
    {
        return $this->hasMany(TaskField::class,'id_task', 'id')
            ->where('alias', '=','currency')->where('type_field', '=', 'in');
    }

    /**
     * Информация о полях валюты отдаю
     *
     * @return HasMany
     */
    public function tasks_fields_currency_out(): HasMany
    {
        return $this->hasMany(TaskField::class,'id_task', 'id')
            ->where('alias', '=','currency')->where('type_field', '=', 'out');
    }

    /**
     * Получаем информацию о полях (Платежных реквизитов и направлений)
     *
     * @return HasMany
     */
    public function tasks_fields_base(): HasMany
    {
        return $this->hasMany(TaskField::class,'id_task', 'id')->whereIn('alias', ['direction', 'requisites']);
    }


    public function tasks_fields_in_out(): HasMany
    {
        return $this->hasMany(TaskField::class,'id_task', 'id')
            ->where('alias', '=','currency')->whereIn('type_field', ['in', 'out']);
    }

    /**
     * Реферальный лог по заявке
     *
     * @return HasOne
     */
    public function referral_log()
    {
        return $this->hasOne(ReferralLog::class,'id_task', 'id');
    }

    public function referral_link(): HasOne
    {
        return $this->hasOne(ReferralLink::class,'id','id_referral_link');
    }

    public function verification_card() {
        return $this->hasOne(VerificationCard::class,'id_order','id')->where('status', 0);
    }

    public function verification_card_active_user() {
        return $this->hasMany(VerificationCard::class,'id_user','id_user')->where('status','=',1);
    }

    public function verification_card_active() {
        return $this->hasOne(VerificationCard::class,'id_order','id')->whereNotIn('status', [2, 3]);
    }

    public function task_private_hash() {
        return $this->hasOne(TaskPrivateHash::class,'id_task', 'id');
    }

    public function tasks_card_details() {
        return $this->hasOne(TaskCardDetail::class,'id_task','id');
    }

    /**
     * Получаем сумму которую должен отдать клиент
     *
     * @return float
     */
    public function displayInPrice()
    {
        if($this->direction_exchange->currency1->is_allow_amount_space == 1) {
            $in_decimal = (int)strlen(substr(strrchr($this->give_price, '.'), 1));
            return number_format($this->give_price, $in_decimal,'.',' ');
        }

        return $this->give_price;
    }

    /**
     * Получаем сумму которую должен переводить сервис
     *
     * @return float
     */
    public function displayOutPrice()
    {
        if($this->direction_exchange->currency2->is_allow_amount_space == 1) {
            $in_decimal = (int)strlen(substr(strrchr($this->receiving_price, '.'), 1));
            return number_format($this->receiving_price, $in_decimal,'.',' ');
        }

        return $this->receiving_price;
    }

    public function edit_data_manager() {
        return $this->hasOne(User::class,'id','id_edit_data_manager');
    }

    public function history_operators() {
        return $this->hasMany(TaskHistoryOperator::class,'id_task','id');
    }

    public function main_operator() {
        return $this->hasOne(User::class,'id','id_main_operator');
    }

    public function task_user() {
        return $this->hasMany(TaskUser::class,'id_user','id_user');
    }

    public function task_comment()
    {
        return $this->hasMany(TaskComment::class,'id_task', 'id')->orderByDesc('id');
    }

    public function task_comment_user()
    {
        return $this->hasMany(TaskCommentUser::class,'id_task', 'id')->orderByDesc('id');
    }

    public function task_file()
    {
        return $this->hasMany(TaskFile::class,'id_task', 'id')->orderByDesc('id');
    }

    /**
     * Получаем информацию о мерчанте
    */
    public function merchant(): HasOne
    {
        return $this->hasOne(GatewayMerchant::class,'id','id_merchant');
    }

    public function task_single_log_confirm() {
        return $this->hasOne(TaskSingleLogConfirm::class,'id_task','id');
    }

    public function merchant_transaction_hash()
    {
        return $this->hasOne(MerchantTransactionHash::class,'id_task', 'id');
    }

    public function pay_transaction_hash()
    {
        return $this->hasOne(PayTransactionHash::class,'id_task', 'id');
    }

    public function aml_analytics_log_getblock_success()
    {
        return $this->hasOne(AmlAnalysisLog::class,'id_task', 'id')->where('provider_name', 'getblock')->where('event_type', 'check_tx.success');
    }

    public function steps_logs()
    {
        return $this->hasMany(ApplicationStepLog::class,'id_task', 'id');
    }
}
