<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WithdrawalRequestLog
 *
 * @property int $id
 * @property int $id_withdrawal_request
 * @property int $id_manager
 * @property string $text
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $remainder
 * @property-read \App\Models\User $manager
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereIdManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereIdWithdrawalRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereRemainder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequestLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class WithdrawalRequestLog extends Model
{
    protected $table = 'withdrawal_request_log';

    protected $fillable = [
        'id_withdrawal_request','text', 'amount','remainder','id_manager'
    ];

    public function manager() {
        return $this->hasOne(User::class,'id','id_manager');
    }
}
