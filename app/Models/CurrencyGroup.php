<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int $id_user
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CurrencyGroup extends Model
{
    protected $table = 'currencies_groups';

    protected $fillable = [
        'name',
        'id_user',
        'sorting'
    ];

    public function currencies() {
        return $this->hasMany(Currency::class,'id_group', 'id');
    }
}
