<?php

namespace App\Models;

use App\Models\Filters\RewardLogFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\RewardLog
 *
 * @property int $id
 * @property int $id_reward_link
 * @property int $id_task
 * @property int $id_user
 * @property string $text
 * @property string $bonus
 * @property string|null $bonus_string
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereBonusString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereIdRewardLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RewardLog onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RewardLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RewardLog withoutTrashed()
 * @property float|null $percent
 * @property int $is_cashback
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereIsCashback($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RewardLog wherePercent($value)
 */
class RewardLog extends Model
{
    use SoftDeletes, Filterable;

    protected $table = 'reward_log';

    protected $fillable = [
        'id_user',
        'id_reward_link',
        'text',
        'bonus',
        'id_task',
        'bonus_string',
        'percent',
        'is_cashback',
        'summa_not_fee'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(RewardLogFilter::class);
    }



    public function task() {
        return $this->hasOne(Task::class,'id','id_task');
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }
}
