<?php

namespace App\Models;

use App\Models\Casts\JsonCasts;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AdminFilterUser
 *
 * @property int $id
 * @property int $id_filter_header
 * @property string|null $type_filter
 * @property int $id_user
 * @property array|null $options
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereIdFilterHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereTypeFilter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminFilterUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdminFilterUser extends Model
{
    protected $table = 'admin_filters_user';


    protected $fillable = [
        'id_filter_header',
        'type_filter',
        'id_user',
        'options',
        'status',
        'section'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
    ];
}
