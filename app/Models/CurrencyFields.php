<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{HasOne, MorphToMany};
use Spatie\Translatable\HasTranslations;

class CurrencyFields extends Model
{
    use HasTranslations;

    protected $table = 'currency_fields';

    protected $fillable = [
        'name',
        'min_char',
        'max_char',
        'first_char',
        'obligatory_field',
        'status',
        'key_id',
        'checkbox_title',
        'field_type',
        'language_field',
        'description',
        'when_print',
        'remove_spaces',
        'sorting',
        'sorting_out',
        'type_field',
        'list_text'
    ];

    public $translatable = ['name', 'first_char', 'checkbox_title', 'description', 'list_text'];

    public function currency(): HasOne
    {
        return $this->hasOne(Currency::class,'id','id_currency');
    }

    public function currencies_in(): MorphToMany
    {
        return $this->morphedByMany(Currency::class, 'model','currency_in_has_fields','field_id');
    }

    public function currencies_out(): MorphToMany
    {
        return $this->morphedByMany(Currency::class, 'model','currency_out_has_fields','field_id');
    }
}
