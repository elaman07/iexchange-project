<?php

namespace App\Models;

use App\Models\Filters\WalletAddressesFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WalletsAddresses
 *
 * @property int $id
 * @property int $id_requisites ID платежной системы
 * @property string|null $address Адрес кошелька
 * @property string|null $account Метки
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereIdRequisites($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $private_key
 * @property int $is_failed_send
 * @property int $is_fee_sent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereIsFailedSend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereIsFeeSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses wherePrivateKey($value)
 * @property string|null $provider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereProvider($value)
 * @property string|null $service_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereServiceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WalletsAddresses whereLike($column, $value, $boolean = 'and')
 * @property-read \App\Models\Task|null $tasks
 * @property string|null $memo_id
 * @method static \Illuminate\Database\Eloquent\Builder|WalletsAddresses whereMemoId($value)
 */
class WalletsAddresses extends Model
{
    use Filterable;

    protected $table = 'wallets_addresses';

    protected $fillable = [
        'id_requisites',
        'address',
        'private_key',
        'account',
        'memo_id',
        'is_fee_sent',
        'is_failed_send',
        'provider',
        'service_name'
    ];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(WalletAddressesFilter::class);
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id_wallets_addresses', 'id');
    }
}
