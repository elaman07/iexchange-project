<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;


class ApplicationStepLog extends Model
{
    protected $table = 'applications_steps_logs';

    protected $fillable = [
      'id_step',
      'id_manager',
      'id_task',
      'id_order_status'
    ];

    public function manager()
    {
        return $this->hasOne(User::class,'id', 'id_manager');
    }

    public function order_step()
    {
        return $this->hasOne(OrderStep::class,'id', 'id_step');
    }

    public function order_status()
    {
        return $this->hasOne(TaskStatus::class,'id', 'id_order_status');
    }
}
