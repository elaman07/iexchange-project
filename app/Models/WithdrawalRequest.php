<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\WithdrawalRequest
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_manager
 * @property int $id_currency
 * @property int|null $referral
 * @property int $reward
 * @property int $status
 * @property string|null $score
 * @property float|null $balance_referral
 * @property float $balance_reward
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $ip
 * @property float|null $base_referral
 * @property float $base_reward
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\User $manager
 * @property-read \App\Models\User $user
 * @property-read \App\Models\WithdrawalRequestLog $withdrawal_request_log
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WithdrawalRequest onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBalanceReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBalanceReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBaseReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBaseReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereIdManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WithdrawalRequest withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WithdrawalRequest withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property \Illuminate\Support\Carbon|null $verified_at
 * @property string|null $tx_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereTxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereLike($column, $value, $boolean = 'and')
 * @property-read int|null $notifications_count
 * @property int $big_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBigId($value)
 * @property int $is_black_list
 * @property string|null $black_list_text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereBlackListText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WithdrawalRequest whereIsBlackList($value)
 * @property string|null $view_balance_referral
 * @property string|null $view_balance_reward
 * @method static \Illuminate\Database\Eloquent\Builder|WithdrawalRequest whereViewBalanceReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WithdrawalRequest whereViewBalanceReward($value)
 */
class WithdrawalRequest extends Model
{
    use SoftDeletes, Notifiable, Filterable;

    protected $table = 'withdrawal_request';

    protected $fillable = [
        'id_user',
        'id_manager',
        'id_currency',
        'referral',
        'reward',
        'score',
        'status',
        'balance_referral',
        'balance_reward',
        'base_referral',
        'base_reward',
        'ip',
        'verified_at',
        'tx_id',
        'big_id',
        'is_black_list',
        'black_list_text',
        'view_balance_referral',
        'view_balance_reward'
    ];

    protected $casts = [
        'verified_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];


    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\WithdrawalRequestFilter::class);
    }

    public function currency() {
        return $this->hasOne(Currency::class, 'id','id_currency');
    }

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function manager() {
        return $this->hasOne(User::class,'id','id_manager');
    }

    public function withdrawal_request_log() {
        return $this->hasOne(WithdrawalRequestLog::class,'id_withdrawal_request','id');
    }
}
