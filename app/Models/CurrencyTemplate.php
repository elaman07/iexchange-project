<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class CurrencyTemplate extends Model
{
    use HasTranslations;

    protected $table = 'currencies_templates';

    protected $fillable = [
        'id_type',
        'text',
        'type_view_info'
    ];

    protected $translatable = [
        'text'
    ];
}
