<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskChat
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_task
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id_client
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskChat whereIdClient($value)
 */
class TaskChat extends Model
{
    protected $table = 'tasks_chat';

    protected $fillable = [
        'id_task',
        'id_client',
        'id_user',
        'message'
    ];


    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }
}
