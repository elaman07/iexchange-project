<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyMerchant extends Model
{
    use HasFactory;

    protected $table = 'currency_merchants';

    protected $fillable = [
        'currency_id',
        'gateway_merchant_id',
        'currency_type'
    ];


}
