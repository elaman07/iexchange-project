<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SettingsReward
 *
 * @property int $id
 * @property string|null $account_text
 * @property string|null $partners_text
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $block_text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward whereAccountText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward whereBlockText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward wherePartnersText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SettingsReward whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SettingsReward extends Model
{
    protected $table = 'settings_reward';

    protected $fillable = [
        'account_text', 'partners_text', 'block_text'
    ];
}
