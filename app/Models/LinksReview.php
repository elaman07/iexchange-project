<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LinksReview
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $url
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereUrl($value)
 * @mixin \Eloquent
 * @property string|null $icon
 * @property int $is_review
 * @property string|null $type
 * @property string|null $description
 * @property int $id_group
 * @property-read \App\Models\LinksReviewGroup $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereIsReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinksReview whereType($value)
 */
class LinksReview extends Model
{
    use HasTranslations;

    protected $table = 'links_reviews';

    protected $fillable = [
        'name',
        'url',
        'type',
        'icon',
        'is_review',
        'id_group',
        'sorting',
        'description',
        'is_bot',
        'count_review'
    ];

    public $translatable = ['name', 'description', 'url'];

    public function group() {
        return $this->hasOne(LinksReviewGroup::class,'id','id_group');
    }
}
