<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CollaborationPrModel
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $type
 * @property string|null $value
 * @property string|null $url
 * @property int|null $sorting
 * @property int|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_button
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereIsButton($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CollaborationPrModel whereValue($value)
 * @mixin \Eloquent
 */
class CollaborationPrModel extends Model
{
    protected $table = 'collaboration_pr';

    protected $fillable = [
        'name',
        'type',
        'value',
        'url',
        'sorting',
        'status',
        'is_button'
    ];
}
