<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PendingOrderStatus
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_not_delete
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus whereIsNotDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PendingOrderStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PendingOrderStatus extends Model
{
    use HasTranslations;

    protected $table = 'pending_order_status';

    protected $fillable = [
        'name',
        'is_not_delete'
    ];

    protected $translatable = [
        'name'
    ];
}
