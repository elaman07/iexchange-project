<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SelectedCourse
 *
 * @property int $id
 * @property int $id_crypto_parser
 * @property string|null $name
 * @property string|null $service_name
 * @property int $status
 * @property int $sorting
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $number_format
 * @property-read \App\Models\ParserExchange $parser_exchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereIdCryptoParser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereNumberFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereServiceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SelectedCourse whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SelectedCourse extends Model
{
    protected $table = 'selected_courses';

    protected $fillable = [
        'id_crypto_parser',
        'name',
        'service_name',
        'number_format',
        'status',
        'sorting'
    ];

    public function parser_exchange() {
        return $this->belongsTo(ParserExchange::class,'id_crypto_parser');
    }
}
