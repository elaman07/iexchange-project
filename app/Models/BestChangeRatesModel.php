<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BestChangeRatesModel
 *
 * @property int $id
 * @property string|null $name
 * @property int $exchanger_id1
 * @property int $exchanger_id2
 * @property string|null $value
 * @property string|null $summa
 * @property string|null $position
 * @property int $status
 * @property int|null $number_format
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereExchangerId1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereExchangerId2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereNumberFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereSumma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereValue($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BestChangeRatesModel[] $duplicate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BestchangeRatesLog[] $bestchange_rates_log
 * @property int $sort_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereSortBy($value)
 * @property-read int|null $bestchange_rates_log_count
 * @property-read int|null $duplicate_count
 * @property int $is_automatic_sort_by
 * @property int $is_automatic_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange
 * @property-read int|null $direction_exchange_count
 * @property-read \App\Models\DirectionExchange $direction_first
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereIsAutomaticSortBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereIsAutomaticType($value)
 * @property int $is_not_pair
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereIsNotPair($value)
 * @property string|null $source_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BestChangeRatesModel whereSourceName($value)
 */
class BestChangeRatesModel extends Model
{
    protected $table = 'bestchange_rates';

    protected $fillable = [
        'name',
        'exchanger_id1',
        'exchanger_id2',
        'source_name',
        'value',
        'summa',
        'status',
        'number_format',
        'is_not_pair'
    ];

    public function duplicate() {
        return $this->hasMany(BestChangeRatesModel::class,'name','name');
    }



    public function bestchange_rates_log() {
        return $this->hasMany(BestchangeRatesLog::class,'id_bestchange_rates','id');
    }

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_bestchange_rates','id');
    }

    public function direction_first() {
        return $this->hasOne(DirectionExchange::class,'id_bestchange_rates', 'id');
    }
}
