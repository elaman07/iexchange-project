<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ParserFormulaRates extends Model
{
    use Filterable;

    protected $table = 'parser_formula_rates';

    protected $fillable = [
        'title',
        'name',
        'exchange_in',
        'exchange_out',
        'value',
        'summa',
        'status',
        'number_format',
        'formula',
        'is_coefficient',
        'coefficient_name',
        'coefficient_formula'
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\ParserFormulaRatesFilter::class);
    }

    public function direction_exchange(): HasMany
    {
        return $this->hasMany(DirectionExchange::class,'id_parser_formula_rate', 'id');
    }
}
