<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MainEventLog
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $type_event
 * @property string|null $event
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $ip_address
 * @property string|null $url
 * @property int $id_admin
 * @property-read \App\Models\User $user
 * @property-read \App\Models\User $user_admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereIdAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereTypeEvent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainEventLog whereUrl($value)
 * @mixin \Eloquent
 */
class MainEventLog extends Model
{
    protected $table = 'main_event_logs';

    protected $fillable = [
        'id_user',
        'type_event',
        'event',
        'ip_address',
        'url',
        'id_admin'
    ];

    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function user_admin() {
        return $this->hasOne(User::class,'id','id_admin');
    }
}
