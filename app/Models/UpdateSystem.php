<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UpdateSystem
 *
 * @property int $id
 * @property string|null $update_site_proxy_addr
 * @property string|null $update_site_proxy_port
 * @property string|null $update_site_proxy_user
 * @property string|null $update_site_proxy_pass
 * @property int $stable_versions_only
 * @property int $update_autocheck
 * @property int $update_stop_autocheck
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereStableVersionsOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdateAutocheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdateSiteProxyAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdateSiteProxyPass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdateSiteProxyPort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdateSiteProxyUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdateStopAutocheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UpdateSystem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UpdateSystem extends Model
{
    protected $table = 'update_systems';

    protected $fillable = [
        'update_site_proxy_addr',
        'update_site_proxy_port',
        'update_site_proxy_user',
        'update_site_proxy_pass',
        'stable_versions_only',
        'update_autocheck',
        'update_stop_autocheck'
    ];
}
