<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 26.02.2020
 * Time: 12:07
 */

namespace App\Models;

use App\Common\Packages\Pages\Models\Page;
use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class RulesPage extends Model
{
    use HasTranslations;

    protected $table = 'rules_pages';

    protected $fillable = [
        'title',
        'id_page',
        'status',
        'sorting'
    ];

    public $translatable = [
        'title'
    ];


    public function page() {
        return $this->hasOne(Page::class,'page_id','id_page');
    }
}
