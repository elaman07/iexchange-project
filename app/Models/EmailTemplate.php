<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailTemplate
 *
 * @property int $id
 * @property string|null $mailable
 * @property string|null $subject
 * @property string|null $html_template
 * @property string|null $text_template
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $id_event_type
 * @property string|null $email_to
 * @property int $status
 * @property int $template
 * @property-read \App\Models\TemplateTypeEvent|null $template_type_event
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereEmailTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereHtmlTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereIdEventType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereMailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereTextTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmailTemplate extends Model
{
    protected $table = 'email_templates';

    protected $fillable = [
        'id_event_type',
        'mailable',
        'template',
        'subject',
        'email_to',
        'status',
        'text_template',
        'html_template'
    ];

    public function template_type_event()
    {
        return $this->hasOne(TemplateTypeEvent::class,'id','id_event_type');
    }
}
