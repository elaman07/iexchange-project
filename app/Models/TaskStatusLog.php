<?php
namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TaskStatusLog
 *
 * @property int $id
 * @property int $user_id
 * @property int $id_task
 * @property int $old_status
 * @property int $new_status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\TaskStatus $newStatus
 * @property-read \App\Models\TaskStatus $oldStatus
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereNewStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereOldStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereUserId($value)
 * @mixin \Eloquent
 * @property float $in_price
 * @property float $out_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereInPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereOutPrice($value)
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TaskStatusLog onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TaskStatusLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TaskStatusLog withoutTrashed()
 * @property int|null $place_change
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskStatusLog wherePlaceChange($value)
 */
class TaskStatusLog extends Model
{
    use Filterable, SoftDeletes;

    protected $table = "tasks_status_log";

    protected $fillable = [
        'id_task',
        'user_id',
        'course_display',
        'old_status',
        'new_status',
        'in_price',
        'out_price',
        'place_change' // 0 - На сайте, 1 - В админ панели, 2 - В мерчанте
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\OrderStatusLogFilter::class);
    }


    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function oldStatus() {
        return $this->hasOne(TaskStatus::class, 'id', 'old_status');
    }

    public function newStatus() {
        return $this->hasOne(TaskStatus::class, 'id', 'new_status');
    }
}
