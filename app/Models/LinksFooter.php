<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class LinksFooter extends Model
{
    use HasTranslations;

    protected $table = 'links_footers';

    protected $fillable = [
        'name',
        'url',
        'id_group',
        'is_blank',
        'sorting',
    ];

    public $translatable = ['name', 'url'];

    public function group() {
        return $this->hasOne(LinksFooterGroup::class,'id','id_group');
    }
}
