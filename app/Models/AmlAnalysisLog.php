<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use App\Models\Casts\AMLJsonCasts;
use App\Models\Casts\JsonCasts;
use Illuminate\Database\Eloquent\Model;

class AmlAnalysisLog extends Model
{
    protected $table = 'aml_analysis_logs';

    protected $fillable = [
        'provider_name',
        'id_task',
        'tx_id',
        'address',
        'code_name',
        'riskscore',
        'status',
        'event_type',
        'json_value',
        'event_message',
        'price',
        'id_currency'
    ];

    protected $casts = [
        'json_value' => AMLJsonCasts::class,
    ];


    public function tasks() {
        return $this->hasOne(Task::class,'id', 'is_task');
    }
}
