<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use App\Models\Filters\VerificationCardFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VerificationCardInstruction extends Model
{
    use HasTranslations;

    protected $table = 'verification_card_instructions';

    public $translatable = ['name', 'text', 'notice_text'];

    protected $fillable = [
        'id_category',
        'name',
        'text',
        'notice_text',
        'status',
        'sorting',
        'image'
    ];

    public function category() {
        return $this->hasOne(VerificationCardCategory::class,'id','id_category');
    }
}
