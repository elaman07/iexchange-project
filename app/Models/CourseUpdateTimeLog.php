<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CourseUpdateTimeLog extends Model
{
    protected $table = 'course_update_time_logs';

    protected $fillable = [
      'time',
      'type_rate',
      'source'
    ];
}
