<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Cities\Entities\CitiesModel;


class DirectionExchangeCity extends Model
{
    protected $table = 'direction_exchange_cities';

    protected $fillable = [
        'id_direction_exchange',
        'city_id',
        'add_comm',
        'param',
        'min_price',
        'max_price',
        'id_country'
    ];

    public function city() {
        return $this->hasOne(CitiesModel::class,'id','city_id');
    }

    public function geo_country() {
        return $this->hasOne(GeoCountryList::class,'id','id_country');
    }
}
