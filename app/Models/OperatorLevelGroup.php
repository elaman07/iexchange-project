<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OperatorLevelGroup
 *
 * @property int $id
 * @property string|null $name
 * @property int $status
 * @property string|null $from_limit
 * @property string|null $to_limit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OperationLevel[] $operation_level
 * @property-read int|null $operation_level_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereFromLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereToLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatorLevelGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OperatorLevelGroup extends Model
{
    protected $table = 'operation_level_groups';

    protected $fillable = [
        'name',
        'status',
        'from_limit',
        'to_limit'
    ];

    public function operation_level()
    {
        return $this->hasMany(OperationLevel::class,'id_level_group', 'id');
    }
}
