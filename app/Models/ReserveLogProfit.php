<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReserveLogProfit
 *
 * @property int $id
 * @property int $id_task
 * @property int $id_currency
 * @property float $percent
 * @property float $amount
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $amount_usd
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\Task $tasks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereAmountUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReserveLogProfit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReserveLogProfit extends Model
{
    protected $table = 'reserve_log_profit';

    protected $fillable = [
        'id_task',
        'id_currency',
        'percent',
        'amount',
        'amount_usd',
        'comment'
    ];

    public function currency() {
        return $this->hasOne(Currency::class,'id','id_currency');
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id','id_task');
    }
}
