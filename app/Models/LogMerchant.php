<?php

namespace App\Models;

use App\Models\Filters\LogMerchantFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LogMerchant
 *
 * @property int $id
 * @property int $id_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status
 * @property int $event_type
 * @property string|null $event_value
 * @property string|null $provider
 * @property-read \App\Models\Task $tasks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereEventType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereEventValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereIdOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LogMerchant whereLike($column, $value, $boolean = 'and')
 */
class LogMerchant extends Model
{
    use Filterable;

    protected $table = 'log_merchants';

    protected $fillable = [
        'id_order',
        'status',
        'event_type',
        'event_value',
        'provider'
    ];


    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(LogMerchantFilter::class);
    }

    public function tasks() {
        return $this->hasOne(Task::class,'id','id_order');
    }
}
