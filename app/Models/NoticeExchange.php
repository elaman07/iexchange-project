<?php
namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class NoticeExchange extends Model
{
    use HasTranslations;

    protected $table = 'notices_exchange';

    protected $fillable = [
        'text',
        'status',
        'color',
        'link',
        'sorting',
        'is_enabled_schedule',
        'from_time',
        'to_time',
        'is_blank',
        'text_color',
        'bg_color',
        'icon_notice',
        'text_size'
    ];

    public $translatable = ['text'];
}
