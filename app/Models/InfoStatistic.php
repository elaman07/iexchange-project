<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InfoStatistic
 *
 * @property int $id
 * @property string $type
 * @property string|null $name
 * @property string|null $value
 * @property int $is_automatic
 * @property int $sorting
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $account_number
 * @property string|null $link
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereIsAutomatic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoStatistic whereValue($value)
 * @mixin \Eloquent
 */
class InfoStatistic extends Model
{
    protected $table = 'info_statistics';

    protected $fillable = [
        'type',
        'name',
        'value',
        'account_number',
        'link',
        'is_automatic',
        'status',
        'sorting'
    ];
}
