<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Fund
 *
 * @property int $id
 * @property string|null $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Fund whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Fund extends Model
{
    protected $table = 'fund';

    protected $fillable = [
        'amount'
    ];
}
