<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParserExchangeLog
 *
 * @property int $id
 * @property int $id_parser_exchange
 * @property float $in_price
 * @property float $out_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ParserExchange $parser_exchange
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog whereIdParserExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog whereInPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog whereOutPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserExchangeLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ParserExchangeLog extends Model
{
    protected $table = 'parser_exchange_log';

    protected $fillable = [
        'id_parser_exchange',
        'in_price',
        'out_price'
    ];

    public function parser_exchange() {
        return $this->hasOne(ParserExchange::class,'id','id_parser_exchange');
    }
}
