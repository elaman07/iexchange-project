<?php
namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;


/***
 * @deprecated Модуль будет удален скоро
 */
class ReserveRequest extends Model
{
    use Filterable;

    protected $fillable = [
        'id_direction_exchange', // Устарело
        'phone',
        'email',
        'price',
        'status',
        'is_delete',
        'id_currency'
    ];

    protected $table = 'reserve_request';


    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\ReserveRequestFilter::class);
    }

    public function direction_exchange() {
        return $this->hasOne(DirectionExchange::class, 'id', 'id_direction_exchange');
    }

    public function currency() {
        return $this->hasOne(Currency::class,'id', 'id_currency');
    }
}
