<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use App\Models\Filters\FaqFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Faq
 *
 * @property int $id
 * @property int $id_group
 * @property string|null $title
 * @property string|null $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\FaqCategory $faq_category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @property int $sorting
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereSorting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Faq whereStatus($value)
 */
class Faq extends Model
{
    use Filterable, HasTranslations;

    protected $table = 'faq';

    protected $fillable = [
        'title',
        'text',
        'status',
        'sorting',
        'id_group'
    ];

    protected $translatable = ['title', 'text'];

    /**
     * Фильтры
     */
    public function modelFilter() {
        return $this->provideFilter(FaqFilter::class);
    }


    public function faq_category() {
        return $this->hasOne(FaqCategory::class,'id','id_group');
    }
}
