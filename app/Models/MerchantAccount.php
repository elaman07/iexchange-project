<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MerchantAccount
 *
 * @property int $id
 * @property int $id_task
 * @property string|null $account
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $provider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount whereIdTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MerchantAccount whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Task $tasks
 */
class MerchantAccount extends Model
{
    protected $table = 'merchant_account';

    protected $fillable = [
        'id_task',
        'account',
        'provider'
    ];

    public function tasks() {
        return $this->hasOne(Task::class,'id','id_task');
    }
}
