<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FileParserRates
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $exchange_in
 * @property string|null $exchange_out
 * @property int $id_group
 * @property int $value
 * @property string|null $summa
 * @property int $status
 * @property int $number_format
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange
 * @property-read int|null $direction_exchange_count
 * @property-read \App\Models\FileParserGroup $file_parser_group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereExchangeIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereExchangeOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereNumberFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereSumma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FileParserRates whereValue($value)
 * @mixin \Eloquent
 */
class FileParserRates extends Model
{
    protected $table = 'file_parser_rates';

    protected $fillable = [
        'name',
        'exchange_in',
        'exchange_out',
        'id_group',
        'value',
        'summa',
        'status',
        'number_format',
        'type',
        'code'
    ];

    public function duplicate_count() {
        return $this->hasMany(FileParserRates::class,'name','name')->count();
    }

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_file_parser_rate', 'id');
    }

    public function file_parser_group() {
        return $this->hasOne(FileParserGroup::class,'id', 'id_group');
    }
}
