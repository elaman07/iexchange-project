<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GroupCommision
 *
 * @property int $id
 * @property string $name
 * @property string $give
 * @property string $receiving
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision whereGive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision whereReceiving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GroupCommision whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DirectionExchange[] $direction_exchange
 * @property-read int|null $direction_exchange_count
 */
class GroupCommision extends Model
{
    protected $table = 'group_commission';

    protected $fillable = [
        'name',
        'give',
        'receiving'
    ];

    public function direction_exchange() {
        return $this->hasMany(DirectionExchange::class,'id_group_commission', 'id');
    }
}
