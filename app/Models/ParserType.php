<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParserType
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ParserType whereValue($value)
 * @mixin \Eloquent
 */
class ParserType extends Model
{
    protected $table = 'parser_type';
}
