<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PayoutAddress
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $payment
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PayoutAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PayoutAddress extends Model
{
    protected $table = 'payout_address';

    protected $fillable = [
        'email','address','payment'
    ];
}
