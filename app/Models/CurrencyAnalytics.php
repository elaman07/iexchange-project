<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyAnalytics
 *
 * @property int $id
 * @property string|null $in_amount
 * @property string|null $out_amount
 * @property int $in_count_exchange
 * @property int $out_count_exchange
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $id_currency
 * @property int $in_order_count
 * @property int $out_order_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereIdCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereInAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereInCountExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereInOrderCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereOutAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereOutCountExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereOutOrderCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyAnalytics whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $in_amount_usd
 * @property float $out_amount_usd
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyAnalytics whereInAmountUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyAnalytics whereOutAmountUsd($value)
 */
class CurrencyAnalytics extends Model
{
    protected $table = 'currencies_analytics';

    protected $fillable = [
        'id_currency',
        'in_amount',
        'out_amount',
        'in_amount_usd',
        'out_amount_usd',
        'in_count_exchange',
        'out_count_exchange',
        'in_order_count',
        'out_order_count'
    ];
}
