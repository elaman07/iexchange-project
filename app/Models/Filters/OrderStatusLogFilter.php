<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class OrderStatusLogFilter extends ModelFilter
{
    /**
     * Установночный фильтр только по выбраннным статусам
     */
    public function setup()
    {
        //
    }

    /**
     * Поиск по ID пользователя
     *
     * @param $id
     * @return OrderStatusLogFilter
     */
    public function idUser($id)
    {
        return $this->where('user_id', '=', $id);
    }


    /**
     * Поиск по номеру заявки
     *
     * @param $id
     * @return OrderStatusLogFilter
     */
    public function idOrder($id)
    {
        return $this->where('id_task', '=', $id);
    }

    /**
     * Фильтр по Email адресу клиента
     *
     * @param $value
     * @return OrderStatusLogFilter
     */
    public function emailAddress($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            array_key_exists('checkbox_full_email', $this->input) ?
                $query->where('email', $value) :
                $query->where('email', 'like', '%' . $value . '%');
        });
    }

    /**
     * Фильтр по IP Адресу
     *
     * @param $value
     * @return OrderStatusLogFilter
     */
    public function ipAddress($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            $query->where('ip_address', $value);
        });
    }
}