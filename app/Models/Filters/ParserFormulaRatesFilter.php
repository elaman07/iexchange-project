<?php

namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class ParserFormulaRatesFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтровать по категориям
     *
     * @param $value
     * @return ParserFormulaRatesFilter
     */
    public function title($value)
    {
        return (array_key_exists('checkbox_full_name', $this->input) ?
            $this->where('title', $value) :
            $this->where('title', 'like', '%' . $value . '%')
        );
    }
}
