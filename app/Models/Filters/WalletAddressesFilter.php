<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 09.12.2019
 * Time: 8:55
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class WalletAddressesFilter extends ModelFilter
{
    /**
     * Искать провайдерам
     *
     * @param $value
     * @return WalletAddressesFilter
     */
    public function provider($value)
    {
        return $this->whereIn('service_name', $value);
    }

    /**
     * Поиск по адресу
     * 
     * @param $value
     * @return WalletAddressesFilter
     */
    public function address($value) {
        return $this->where('address', $value);
    }
}