<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.06.2019
 * Time: 8:37
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class LogMerchantFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтр по ID заявки
     *
     * @param $id
     * @return LogMerchantFilter
     */
    public function idOrder($id)
    {
        return $this->where('id_order','=', $id);
    }

    /**
     * Фильтр по типу событий
     *
     * @param $ids
     * @return LogMerchantFilter
     */
    public function eventType($ids) {
        return $this->whereIn('event_type', $ids);
    }

    /**
     * Фильтр по провайдеру
     *
     * @param $provider
     * @return LogMerchantFilter
     */
    public function provider($provider) {
        return $this->where('provider', '=', $provider);
    }
}