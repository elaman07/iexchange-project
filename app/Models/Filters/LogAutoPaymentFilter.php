<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.06.2019
 * Time: 8:24
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class LogAutoPaymentFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтр по ID заявки
     * @param $id
     * @return LogAutoPaymentFilter
     */
    public function idOrder($id) {
        return $this->where('id_task' ,'=',$id);
    }
}