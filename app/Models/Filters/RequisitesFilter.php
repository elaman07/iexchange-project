<?php
namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class RequisitesFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтр по ID валюты
     *
     * @param $ids
     * @return RequisitesFilter
     */
    public function idCurrencies($ids)
    {
        return $this->whereIn('id_currency', $ids);
    }


    /**
     * Фильтр по номеру счета
     *
     * @param $value
     * @return RequisitesFilter
     */
    public function accountNumber($value)
    {
        return $this->where('account_number', '=', $value);
    }

    /**
     * Фильтр по статусу
     *
     * @param $value
     * @return RequisitesFilter
     */
    public function status($value)
    {
        return $this->whereIn('status', $value);
    }
}