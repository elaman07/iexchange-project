<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class ReferralStatisticsFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Поиск по ID пользователя
     *
     * @param $id
     * @return ReferralStatisticsFilter
     */
    public function idUser($id)
    {
        return $this->where('id_user', '=', $id);
    }

    /**
     * Поиск по IP Адресу
     *
     * @param $value
     * @return ReferralStatisticsFilter
     */
    public function ipAddress($value)
    {
        return $this->where('ip_address', '=', $value);
    }
}