<?php

namespace App\Models\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class CitiesModelFilter extends ModelFilter
{

    /**
     * Поиск по названию
     *
     * @param $value
     * @return CitiesModelFilter
     */
    public function name($value)
    {
        return array_key_exists('checkbox_name', $this->input) ?
            $this->where('name->'.app()->getLocale(), $value) :
            $this->where('name->'.app()->getLocale(), 'like', '%' . $value . '%');
    }

    /**
     * Искать Обозначение XML
     *
     * @param $value
     * @return CitiesModelFilter
     */
    public function designationXml($value)
    {
        return array_key_exists('checkbox_designation_xml', $this->input) ?
            $this->where('designation_xml', $value) :
            $this->where('designation_xml', 'like', '%' . $value . '%');
    }
}
