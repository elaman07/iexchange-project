<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class OrderFilter extends ModelFilter
{
    protected $allowedSorting = [
        'sorting_ids',
        'sorting_status',
        'sorting_created_at',
        'sorting_updated_at',
        'sorting_order_amount'
    ];

    /**
     * Установночный фильтр только по выбраннным статусам
    */
    public function setup()
    {
        $this->where('is_archive', '=',0);

        // Не отображать спам заявки
        if(iEXSetting('order_is_allow_spam') == 0) {
            $this->where('is_spam', '=', 0);
        }

//        // Допускаем только выбранные статусы
//        $order = explode(',', iEXSetting('order_priority'));
//        if(count($order) > 0)
//            $this->whereIn('status', $order);

//        // Сортировка по позициям
//        if(! is_null(iex_sorting_order('sorting_order_id')))
//            $this->orderBy('id', iex_sorting_order('sorting_order_id'));
//
//        // Сортировка по статусу
//        if(! is_null(iex_sorting_order('sorting_order_status')))
//            $this->orderBy('status', iex_sorting_order('sorting_order_status'));
//
//        // Сортировка по дате создания
//        if(! is_null(iex_sorting_order('sorting_created_at')))
//            $this->orderBy('created_at', iex_sorting_order('sorting_created_at'));
//
//        // Сортировка по дате посл. обновления
//        if(! is_null(iex_sorting_order('sorting_updated_at')))
//            $this->orderBy('updated_at', iex_sorting_order('sorting_updated_at'));
//
//        // Сортировка по сумме обмена в ($)
//        if(! is_null(iex_sorting_order('sorting_order_amount'))) {
//            $this->join('tasks_convert_log as sort_convert_log', 'tasks.id', '=', 'sort_convert_log.id_task');
//            $this->orderBy('sort_convert_log.to_usd', iex_sorting_order('sorting_order_amount'));
//        }
    }

    /**
     * Поиск по ID
     *
     * @param $id
     * @return OrderFilter
     */
    public function id($id)
    {
        if(is_numeric($id)) {
            return $this->where('id', '=', $id);
        }
        $ids = explode('-', $id);
        return $this->whereBetween('id', $ids);
    }

    /**
     * Поиск по Public ID
     *
     * @param $id
     * @return OrderFilter
     */
    public function IdPublic($id)
    {
        if(is_numeric($id)) {
            return $this->where('public_id', '=', $id);
        }
        $ids = explode('-', $id);
        return $this->whereBetween('public_id', $ids);
    }

    /**
     * Фильтр по Статусам
     *
     * @param $ids
     * @return OrderFilter
     */
    public function status($ids)
    {
        return $this->whereIn('status', $ids);
    }

    /**
     * Фильтр по статусам платежей через мерчант
     *
     * @param $ids
     * @return OrderFilter
     */
    public function merchantOverpayment($ids)
    {
        return $this->whereIn('merchant_overpayment', $ids);
    }

    /**
     * Поиск по ID (От)
     *
     * @param $id
     * @return OrderFilter
     */
    public function fromOrderId($id)
    {
        return $this->where('id', '>=', $id);
    }

    /**
     * Поиск по ID (Да)
     *
     * @param $id
     * @return OrderFilter
     */
    public function toOrderId($id)
    {
        return $this->where('id', '<=', $id);
    }

    /**
     * Минимальная сумма (Отдаю)
     *
     * @param $value
     * @return OrderFilter
     */
    public function minAmountIn($value)
    {
        return $this->where('give_price', '>=', $value);
    }

    /**
     * Максимальная сумма (Отдаю)
     *
     * @param $value
     * @return OrderFilter
     */
    public function maxAmountIn($value)
    {
        return $this->where('give_price', '<=', $value);
    }

    /**
     * Минимальная сумма (Получаю)
     *
     * @param $value
     * @return OrderFilter
     */
    public function minAmountOut($value)
    {
        return $this->where('receiving_price', '>=', $value);
    }

    /**
     * Максимальная сумма (Получаю)
     *
     * @param $value
     * @return OrderFilter
     */
    public function maxAmountOut($value)
    {
        return $this->where('receiving_price', '<=', $value);
    }

    /**
     * Фильтр по дате создания
     *
     * @param $value
     * @return OrderFilter
     */
    public function createdAt($value)
    {
        return $this->whereDate('created_at', '=', $value);
    }

    /**
     * Фильтр по дате обновления
     *
     * @param $value
     * @return OrderFilter
     */
    public function updatedAt($value)
    {
        return $this->whereDate('updated_at', '=', $value);
    }

    /**
     * Фильтр по дате создания (От)
     *
     * @param $value
     * @return OrderFilter
     */
    public function fromCreatedAt($value)
    {
        return $this->where('created_at', '>=', $value);
    }

    /**
     * Фильтр по дате создания (До)
     *
     * @param $value
     * @return OrderFilter
     */
    public function toCreatedAt($value)
    {
        return $this->where('created_at', '<=', $value);
    }

    /**
     * Фильтр по дате обновления (От)
     *
     * @param $value
     * @return OrderFilter
     */
    public function fromUpdatedAt($value)
    {
        return $this->where('updated_at', '>=', $value);
    }

    /**
     * Фильтр по дате обновления (До)
     *
     * @param $value
     * @return OrderFilter
     */
    public function toUpdatedAt($value)
    {
        return $this->where('updated_at', '<=', $value);
    }

    /**
     * Фильтр по направлениям
     *
     * @param $values
     * @return OrderFilter
     */
    public function directionExchange($values)
    {
        if(!is_array($values))
            return $this->whereIn('id_direction_exchange', $values);

        return $this->where('id_direction_exchange', $values);
    }

    /**
     * Фильтр по валюте (Отдаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function currenciesIn($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange', function(Builder $query) use($value) {
                $query->whereIn('id_currency1', $value);
            });
        }

        return $this->whereHas('direction_exchange', function(Builder $query) use($value) {
            $query->where('id_currency1', '=', $value);
        });
    }

    /**
     * Фильтр по валюте (Получаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function currenciesOut($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange', function(Builder $query) use($value) {
                $query->whereIn('id_currency2', $value);
            });
        }

        return $this->whereHas('direction_exchange', function(Builder $query) use($value) {
            $query->where('id_currency2', '=', $value);
        });
    }

    /**
     * Фильтры по Имени партнера
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function partnerName($value)
    {
        return $this->whereHas('user.fromReferral.referral_link.user', function(Builder $query) use($value) {
            array_key_exists('checkbox_partner_name', $this->input) ?
                $query->where('name', $value) :
                $query->where('name', 'like', '%' . $value . '%');
        });
    }

    /**
     * IP Адрес партнера
     *
     * @param $value
     * @return OrderFilter|Builder
    */
    public function partnerIpAddress($value) {
        return $this->whereHas('user.fromReferral.referral_link.user', function(Builder $query) use($value) {
            $query->where('ip_address', '=',$value);
        });
    }

    /**
     * Фильтры по E-mail партнера
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function partnerEmail($value)
    {
        return $this->whereHas('user.fromReferral.referral_link.user', function(Builder $query) use($value) {
            array_key_exists('checkbox_partner_email', $this->input) ?
                $query->where('email', $value) :
                $query->where('email', 'like', '%' . $value . '%');
        });
    }

    /**
     * Фильтры по Реферальному хэшу
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function referralHash($value)
    {
        return $this->whereHas('user.fromReferral.referral_link', function(Builder $query) use($value) {
            $query->where('code', $value);
        });
    }

    /**
     * Фильтры по ID партнера
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function partnerId($value)
    {
        return $this->whereHas('user.fromReferral.referral_link.user', function(Builder $query) use($value) {
            $query->where('id', $value);
        });
    }

    /**
     * Фильтр по платежной системе (Отдаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function paymentIn($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
                $query->whereIn('id_payment', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
            $query->where('id_payment', '=', $value);
        });
    }

    /**
     * Фильтр по платежной системе (Получаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function paymentOut($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
                $query->whereIn('id_payment', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
            $query->where('id_payment', '=', $value);
        });
    }

    /**
     * Фильтр по коду валют (Отдаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function codeCurrencyIn($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
                $query->whereIn('id_code_currency', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
            $query->where('id_code_currency', '=', $value);
        });
    }

    /**
     * Фильтр по коду валют (Получаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function codeCurrencyOut($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
                $query->whereIn('id_code_currency', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
            $query->where('id_code_currency', '=', $value);
        });
    }

    /**
     * Фильтр по альтернативных кодов валют  (Отдаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function designationIn($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
                $query->whereIn('designation_xml', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
            $query->where('designation_xml', '=', $value);
        });
    }

    /**
     * Фильтр по альтернативных кодов валют  (Получаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function designationOut($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
                $query->whereIn('designation_xml', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
            $query->where('designation_xml', '=', $value);
        });
    }

    /**
     * Фильтрованные валюты (Отдаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function filterCurrencyIn($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
                $query->whereIn('id_filter_currency', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency1', function(Builder $query) use($value) {
            $query->where('id_filter_currency', '=', $value);
        });
    }

    /**
     * Фильтрованные валюты (Получаю)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function filterCurrencyOut($value)
    {
        if(is_array($value)) {
            return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
                $query->whereIn('id_filter_currency', $value);
            });
        }

        return $this->whereHas('direction_exchange.currency2', function(Builder $query) use($value) {
            $query->where('id_filter_currency', '=', $value);
        });
    }

    /**
     * Фильтр по отключенному кэшбэку
     *
     * @return OrderFilter|Builder
     */
    public function isNotBonus()
    {
        return $this->whereHas('task_info', function (Builder $query) {
            $query->where('is_not_bonus', '=', 1);
        });
    }

    /**
     * Фильтр по номеру денежного перевода
     *
     * @param null $value
     * @return OrderFilter|Builder
     */
    public function numTransaction($value = null)
    {
        return $this->whereHas('task_info', function (Builder $query) use($value) {
            $query->where('num_transaction', $value);
        });
    }

    public function uniqueSecurityCode($value = null)
    {
        return $this->where('unique_security_code', '=', $value);
    }

    /**
     * Фильтр по отключенным партнерским выплатам
     *
     * @return OrderFilter|Builder
     */
    public function isNotPartner()
    {
        return $this->whereHas('task_info', function (Builder $query) {
            $query->where('is_not_partner', '=', 1);
        });
    }


    /**
     * Фильтр по заявкам которые были созданы по курсу bestchange
     *
     * @return OrderFilter|Builder
    */
    public function isBestchangeParser()
    {
        return $this->whereHas('task_info', function (Builder $query) {
            $query->where('is_bestchange_parser', '=', 1);
        });
    }

    /**
     * Поиск по ID (От)
     *
     * @param $value
     * @return OrderFilter
     */
    public function fromBestchangePosition($value)
    {
        return $this->whereHas('task_info', function (Builder $query) use ($value) {
            $query->where('bestchange_position', '>=', $value);
        });
    }

    /**
     * Поиск по ID (Да)
     *
     * @param $value
     * @return OrderFilter
     */
    public function toBestchangePosition($value)
    {
        return $this->whereHas('task_info', function (Builder $query) use($value) {
            $query->where('bestchange_position', '<=', $value);
        });
    }

    /**
     * Фильтр по IP Адресу
     *
     * @param $value
     * @return OrderFilter
     */
    public function ipAddress($value)
    {
        return $this->where('ip', '=', $value);
    }

    /**
     * Фильтр по менержерам
     *
     * @param $value
     * @return OrderFilter
     */
    public function manager($value)
    {
        if(is_array($value))
            return $this->whereIn('id_manager', $value);

        return $this->where('id_manager', $value);
    }

    /**
     * Фильтр по ID клиента
     *
     * @param $value
     * @return OrderFilter
     */
    public function idUser($value)
    {
        return $this->where('id_user', '=', $value);
    }

    /**
     * Количество заявок (От)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function fromOrderNum($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            $query->where('order_num', '>=', $value);
        });
    }

    /**
     * Количество заявок (До)
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function toOrderNum($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            $query->where('order_num', '<=', $value);
        });
    }

    /**
     * Фильтр заявок по забаненному клиенту
     *
     * @return OrderFilter|Builder
     */
    public function isBan()
    {
        return $this->whereHas('user', function (Builder $query) {
            $query->onlyBanned();
        });
    }

    /**
     * Фильтр заявок по верифицированному клиенту
     *
     * @return OrderFilter|Builder
     */
    public function isVerified()
    {
        return $this->whereHas('user', function (Builder $query) {
            $query->whereNotNull('email_verified_at');
        });
    }

    /**
     * Фильтр заявок по telegram
     *
     * @param string $value
     * @return OrderFilter|Builder
     */
    public function telegram(string $value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            $query->where('telegram', '=', $value);
        });
    }

    /**
     * Фильтр по Email адресу клиента
     *
     * @param $value
     * @return OrderFilter
     */
    public function emailAddress($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            array_key_exists('checkbox_full_email', $this->input) ?
                $query->where('email', $value) :
                $query->where('email', 'like', '%' . $value . '%');
        });
    }

    /**
     * Фильтр по Номеру телефона
     *
     * @param $value
     * @return OrderFilter
     */
    public function phone($value)
    {
        return array_key_exists('checkbox_full_phone', $this->input) ?
            $this->where('phone', $value) :
            $this->where('phone', 'like', '%' . $value . '%');
    }

    /**
     * Фильтр по имени клиента
     *
     * @param $value
     * @return OrderFilter
     */
    public function userName($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            array_key_exists('checkbox_full_name', $this->input) ?
                $query->where('name', $value) :
                $query->where('name', 'like', '%' . $value . '%');
        });
    }

    /**
     * Фильтр по провайдерам
     *
     * @param string $value
     * @return OrderFilter|Builder
     */
    public function userProvider($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            $query->whereIn('provider', $value);
        });
    }

    /**
     * Фильтр (Со счета)
     *
     * @param $value
     * @return OrderFilter
     */
    public function fromShot($value)
    {
        return $this->where('from_shot', 'like', '%' . $value . '%');
    }

    /**
     * Фильтр (На счет)
     *
     * @param $value
     * @return OrderFilter
     */
    public function toShot($value)
    {
        return $this->where('to_shot', '=', $value);
    }

    /**
     * Фильтр по то кто создал заявку "Новичок" или "Постоянный клиент"
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function newbie($value)
    {
        return $this->whereHas('task_info', function (Builder $query) use($value) {
            $query->where('newbie', '=', 1);
        });
    }

    /**
     * Фильтр по мошеническим заявкам
     *
     * @return OrderFilter|Builder
     */
    public function isScam()
    {
        return $this->whereHas('task_info', function (Builder $query) {
            $query->where('is_scam', '=', 1);
            $query->orWhere('is_local_scam', '=', 1);
        });
    }

    /**
     * Фильтр по мошенникам которые находятся в локальной базе данных
     *
     * @return OrderFilter|Builder
    */
    public function isLocalScam()
    {
        return $this->whereHas('task_info', function (Builder $query) {
            $query->where('is_local_scam', '=', 1);
        });
    }

    /**
     * Замороженные средства.
     *
     * @return OrderFilter
     */
    public function isFrozen()
    {
        return $this->where('is_frozen', '=', 1);
    }

    /**
     * Арестованные средства.
     *
     * @return OrderFilter
     */
    public function isFreezeScam()
    {
        return $this->where('is_freeze_scam', '=', 1);
    }

    /**
     * Статус отправки чека
     *
     * @return OrderFilter
     */
    public function checkStatus()
    {
        return $this->where('check_status', '=', 1);
    }

    /**
     * Фильтр записей по странам
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function userCountry($value)
    {
        if(is_array($value)) {
            return $this->whereHas('task_info', function (Builder $query) use($value) {
                $query->whereIn('country', $value);
            });
        }

        return $this->whereHas('task_info', function (Builder $query) use($value) {
            $query->where('country', '=', $value);
        });
    }

    /**
     * Фильтр по девайсам
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function devices($value)
    {
        if(is_array($value)) {
            return $this->whereHas('task_info', function (Builder $query) use($value) {
                $query->whereIn('device', $value);
            });
        }

        return $this->whereHas('task_info', function (Builder $query) use($value) {
            $query->where('device', '=', $value);
        });
    }

    /**
     * Фильтр по ID транзакции
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function idTransaction($value)
    {
        return $this->whereHas('transaction', function(Builder $query) use($value) {
            $query->where('transaction', '=', $value);
        });
    }

    /**
     * Фильтр по суммам
     *
     * @param $value
     * @return \Illuminate\Database\Query\Builder
     */
    public function price($value)
    {
        // Пропускаем фильтр, если получили нулевые значения
        if($value == '0;0') return $this->newQuery();

        return $this->whereHas('task_convert_log', function (Builder $query) use($value) {
            $query->whereBetween('to_usd', explode(';', $value));
        });
    }

    /**
     * Фильтр по ID Транзакции мерчанта
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function idTransactionMerchant($value)
    {
        return $this->whereHas('history_payment_transaction', function (Builder $query) use ($value) {
            $query->where('transfer', '=', $value);
        });
    }

    /**
     * Фильтр по ID автовыплаты транзакции
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function idTransactionAutoPayment($value)
    {
        return $this->whereHas('wallet_history', function (Builder $query) use ($value) {
            $query->where('txid', '=', $value);
        });
    }

    /**
     * Фильтр по ID проверки оплаты транзакции
     *
     * @param $value
     * @return OrderFilter|Builder
     */
    public function idTransactionCheckPayment($value)
    {
        return $this->whereHas('wallet_transaction', function (Builder $query) use ($value) {
            $query->where('txid', '=', $value);
        });
    }

    /**
     * Только заявки из Telegram
     *
     * @return OrderFilter|Builder
    */
    public function isTelegram()
    {
        return $this->whereNotNull('telegram_id');
    }
}
