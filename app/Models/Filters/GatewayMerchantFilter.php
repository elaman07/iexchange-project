<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 29.04.2021
 * Time: 23:47
 */

namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class GatewayMerchantFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтр по активности
     *
     * @param $value
     * @return GatewayMerchantFilter
     */
    public function status($value)
    {
        if(is_array($value))
            return $this->whereIn('status', $value);
        return $this->where('status', $value);
    }

    /**
     * Фильтровать по категориям
     *
     * @param $value
     * @return GatewayMerchantFilter
     */
    public function name($value)
    {
        return (array_key_exists('checkbox_full_name', $this->input) ?
            $this->where('name', $value) :
            $this->where('name', 'like', '%' . $value . '%')
        );
    }
}