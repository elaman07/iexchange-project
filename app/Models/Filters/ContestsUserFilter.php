<?php

namespace App\Models\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class ContestsUserFilter extends ModelFilter
{

    /**
     * Искать значение
     *
     * @param $value
     * @return ContestsUserFilter
     */
    public function email($value): ContestsUserFilter
    {
        return $this->where('email', $value);
    }
}
