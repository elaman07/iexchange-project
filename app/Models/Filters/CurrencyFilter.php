<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class CurrencyFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
    }

    /**
     * Фильтр по активности
     *
     * @param $value
     * @return CurrencyFilter
     */
    public function status($value)
    {
        if(is_array($value))
            return $this->whereIn('status', $value);
        return $this->where('status', $value);
    }

    /**
     * Фильтровать по платежным системам
     *
     * @param $value
     * @return CurrencyFilter
     */
    public function payments($value)
    {
        if(is_array($value))
            return $this->whereIn('id_payment', $value);
        return $this->where('id_payment', $value);
    }

    /**
     * Фильтровать по коду валют
     *
     * @param $value
     * @return CurrencyFilter
     */
    public function codeCurrency($value)
    {
        if(is_array($value))
            return $this->whereIn('id_code_currency', $value);
        return $this->where('id_code_currency', $value);
    }

    /**
     * Фильтровать по фильтрам валют
     *
     * @param $value
     * @return CurrencyFilter
     */
    public function filterCurrency($value)
    {
        if(is_array($value))
            return $this->whereIn('id_filter_currency', $value);
        return $this->where('id_filter_currency', $value);
    }

    /**
     * Фильтровать по группам
     *
     * @param $value
     * @return CurrencyFilter
     */
    public function idGroup($value)
    {
        if(is_array($value))
            return $this->whereIn('id_group', $value);
        return $this->where('id_group', $value);
    }

    /**
     * Сортировка по колонкам
     *
     * @param $value
     * @return CurrencyFilter
     */
    public function sortingBy($value)
    {
        if($value == 'default')
            return;

        list($column, $direction) = explode('-', $value);
        return $this->orderBy($column, $direction);
    }

    public function designationXml($value)
    {
        return $this->where('designation_xml', 'LIKE', "{$value}%");
    }
}
