<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

class AdminAuthLogFilter extends ModelFilter
{
    /**
     * Поиск по ID пользователя
     *
     * @param $id
     * @return UserAuthLogFilter
     */
    public function idUser($id)
    {
        return $this->where('id_user', '=', $id);
    }

    /**
     * Фильтр по IP Адресу
     *
     * @param $value
     * @return UserAuthLogFilter
     */
    public function ipAddress($value)
    {
        return $this->where('ip_address', '=', $value);
    }

    /**
     * Фильтр по Email адресу клиента
     *
     * @param $value
     * @return UserAuthLogFilter
     */
    public function emailAddress($value)
    {
        return $this->whereHas('user', function (Builder $query) use($value) {
            array_key_exists('checkbox_full_email', $this->input) ?
                $query->where('email', $value) :
                $query->where('email', 'like', '%' . $value . '%');
        });
    }

    /**
     * Фильтр по дате создания (От)
     *
     * @param $value
     * @return UserAuthLogFilter
     */
    public function fromCreatedAt($value)
    {
        return $this->where('created_at', '>=', $value);
    }

    /**
     * Фильтр по дате создания (До)
     *
     * @param $value
     * @return UserAuthLogFilter
     */
    public function toCreatedAt($value)
    {
        return $this->where('created_at', '<=', $value);
    }
}
