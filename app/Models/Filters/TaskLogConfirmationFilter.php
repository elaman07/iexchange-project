<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 18.05.2019
 * Time: 18:06
 */

namespace App\Models\Filters;


use App\Models\TaskLogConfirmation;
use EloquentFilter\ModelFilter;

class TaskLogConfirmationFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтровать по ID заявки
     *
     * @param int $value
     * @return TaskLogConfirmation
     */
    public function idTask(int $value)
    {
        return $this->where('id_task', $value);
    }
}