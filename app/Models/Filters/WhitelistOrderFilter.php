<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class WhitelistOrderFilter extends ModelFilter
{

    /**
     * Искать значение
     *
     * @param $value
     * @return WhitelistOrderFilter
     */
    public function fromValue($value)
    {
        return $this->where('value', '=', $value);
    }

    /**
     * Искать значение
     *
     * @param $value
     * @return WhitelistOrderFilter
     */
    public function types($value)
    {
        return $this->whereIn('type', $value);
    }

    /**
     * Фильтр по дате создания (От)
     *
     * @param $value
     * @return WhitelistOrderFilter
     */
    public function fromCreatedAt($value)
    {
        return $this->where('created_at', '>=', $value);
    }

    /**
     * Фильтр по дате создания (До)
     *
     * @param $value
     * @return WhitelistOrderFilter
     */
    public function toCreatedAt($value)
    {
        return $this->where('created_at', '<=', $value);
    }
}