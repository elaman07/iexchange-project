<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class ReserveRequestFilter  extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Поиск по ID пользователя
     *
     * @param $ids
     * @return ReserveRequestFilter
     */
    public function idDirectionExchange($ids)
    {
        return $this->whereIn('id_currency', $ids);
    }

    /**
     * Фильтр по дате создания (От)
     *
     * @param $value
     * @return ReserveRequestFilter
     */
    public function fromCreatedAt($value)
    {
        return $this->where('created_at', '>=', $value);
    }

    /**
     * Фильтр по дате создания (До)
     *
     * @param $value
     * @return ReserveRequestFilter
     */
    public function toCreatedAt($value)
    {
        return $this->where('created_at', '<=', $value);
    }


    /**
     * Фильтр по статусу
     *
     * @param $ids
     * @return ReserveRequestFilter
     */
    public function status($ids)
    {
        return $this->whereIn('status', $ids);
    }

    /**
     * Фильтр по E-mail адресу
     *
     * @param $value
     * @return ReserveRequestFilter
     */
    public function email($value)
    {
        return array_key_exists('checkbox_email', $this->input) ?
            $this->where('email', $value) :
            $this->where('email', 'like', '%' . $value . '%');
    }

}