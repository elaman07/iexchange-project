<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 20.10.2019
 * Time: 7:56
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class ReviewFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтр по статусам
     *
     * @param $value
     * @return ReviewFilter
     */
    public function status($value)
    {
        if(is_array($value))
            return $this->whereIn('status', $value);
        return $this->where('status', $value);
    }


    /**
     * Поиск по имени
     *
     * @param $name
     * @return ReviewFilter
     */
    public function name($name)
    {
        return (array_key_exists('checkbox_full_name', $this->input) ?
            $this->where('name', $name) :
            $this->where('name', 'like', '%' . $name . '%')
        );
    }

    /**
     * Поиск по IP
     *
     * @param $ip
     * @return ReviewFilter
     */
    public function ipAddress($ip) {
        return $this->where('ip_address', '=', $ip);
    }
}
