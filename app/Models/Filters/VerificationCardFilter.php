<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 09.07.2019
 * Time: 14:48
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class VerificationCardFilter extends ModelFilter
{
    /**
     * Фильтр по активности
     *
     * @param $value
     * @return VerificationCardFilter
     */
    public function status($value)
    {
        if(is_array($value))
            return $this->whereIn('status', $value);
        return $this->where('status', $value);
    }

    /**
     * Номер карты
     *
     * @param $value
     * @return VerificationCardFilter
     */
    public function cardNumber($value) {
        return (array_key_exists('checkbox_card_number', $this->input) ?
            $this->where('card_number', $value) :
            $this->where('card_number', 'like', '%' . $value . '%')
        );
    }

    /**
     * Поиск по IP Адресу
     *
     * @param $ip
     * @return VerificationCardFilter
     */
    public function ipAddress($ip) {
        return $this->where('ip_address', $ip);
    }

    /**
     * Поиск по ID Пользователя
     *
     * @param $ip
     * @return VerificationCardFilter
     */
    public function idUser($ip) {
        return $this->where('id_user', $ip);
    }

    /**
     * Поиск по Валютам
     *
     * @param $value
     * @return VerificationCardFilter
     */
    public function currencies($value) {
        return $this->whereIn('id_currency', $value);
    }
}