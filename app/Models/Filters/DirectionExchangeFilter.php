<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class DirectionExchangeFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup() {

        if(!isset($this->input['filter_by']))
            $this->orderBy('id', 'desc');
    }

    /**
     * Фильтр по группам
     *
     * @param $value
     * @return DirectionExchangeFilter
     */
    public function idGroupDirection($value)
    {
        if(is_array($value))
            return $this->whereIn('id_group_direction', $value);

        return $this->where('id_group_direction', '=', $value);
    }


    public function isErrorRate($value) {
        return $this->where('is_error_rate', '=', 1);
    }
    /**
     * Фильтр валюты (Отдаю)
     *
     * @param $value
     * @return DirectionExchangeFilter
     */
    public function currenciesIn($value)
    {
        if(is_array($value))
            return $this->whereIn('id_currency1', $value);
        return $this->where('id_currency1', '=', $value);
    }

    /**
     * Фильтр валюты (Получаю)
     *
     * @param $value
     * @return DirectionExchangeFilter
     */
    public function currenciesOut($value)
    {
        if(is_array($value))
            return $this->whereIn('id_currency2', $value);
        return $this->where('id_currency2', '=', $value);
    }

    /**
     * Фильтр по активности
     *
     * @param $value
     * @return DirectionExchangeFilter
     */
    public function status($value)
    {
        if(is_array($value))
            return $this->whereIn('status', $value);
        return $this->where('status', $value);
    }

    /**
     * Отображать только направления которые парсятся из Bestchange
     *
     * @return DirectionExchangeFilter
    */
    public function isEnableBestchange()
    {
        return $this->where('enable_bestchange', '=', 1);
    }

    /**
     * Замороженные направления
     *
     * @return DirectionExchangeFilter
     */
    public function isHoldingDirection()
    {
        return $this->where('is_holding_direction', '=', 1);
    }

    /**
     * Отображать только направления у которых групповая комиссия
     *
     * @return DirectionExchangeFilter
    */
    public function isGroupCommission()
    {
        return $this->where('enable_group_commission', '=',1);
    }

    /**
     * Только с отключенными кэшбэк
     *
     * @return DirectionExchangeFilter
     */
    public function isNotBonus() {
        return $this->where('is_not_bonus', '=',1);
    }

    /**
     * Только с отключенными партнескими выплатами
     *
     * @return DirectionExchangeFilter
     */
    public function isNotPartner() {
        return $this->where('is_not_partner', '=',1);
    }

    /**
     * Фильтровать по направлениям которые запрещены к экпорту
     *
     * @return DirectionExchangeFilter
    */
    public function isAllowExport() {
        return $this->where('allow_export', '=',1);
    }

    /**
     * Сортировка по колонкам
     *
     * @param $value
     * @return DirectionExchangeFilter
     */
    public function sortingBy($value)
    {
        list($column, $direction) = explode('-', $value);
        return $this->orderBy($column, $direction);
    }
}
