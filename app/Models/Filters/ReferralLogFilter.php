<?php
namespace App\Models\Filters;

use EloquentFilter\ModelFilter;

class ReferralLogFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Поиск по ID пользователя
     *
     * @param $id
     * @return ReferralStatisticsFilter
     */
    public function idUser($id)
    {
        return $this->where('id_user', '=', $id);
    }

    /**
     * Поиск по ID пользователя
     *
     * @param $id
     * @return ReferralStatisticsFilter
     */
    public function idReferral($id)
    {
        return $this->where('id_referral', '=', $id);
    }

    /**
     * Поиск по ID заявки
     *
     * @param $value
     * @return ReferralStatisticsFilter
     */
    public function idTask($value)
    {
        return $this->where('id_task', '=', $value);
    }

    /**
     * Поиск по ID (От)
     *
     * @param $id
     * @return OrderFilter
     */
    public function fromIdTask($id)
    {
        return $this->where('id_task', '>=', $id);
    }

    /**
     * Поиск по ID (Да)
     *
     * @param $id
     * @return OrderFilter
     */
    public function toIdTask($id)
    {
        return $this->where('id', '<=', $id);
    }

    /**
     * Фильтр по дате создания (От)
     *
     * @param $value
     * @return OrderFilter
     */
    public function fromCreatedAt($value)
    {
        return $this->where('created_at', '>=', $value);
    }

    /**
     * Фильтр по дате создания (До)
     *
     * @param $value
     * @return OrderFilter
     */
    public function toCreatedAt($value)
    {
        return $this->where('created_at', '<=', $value);
    }
}