<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 19.06.2019
 * Time: 8:24
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class LogMerchantEventFilter extends ModelFilter
{
    /**
     * Установночный фильтр
     */
    public function setup()
    {
        //
    }

    /**
     * Фильтр по ID заявки
     *
     * @param $id
     * @return LogMerchantEventFilter
     */
    public function idOrder($id) {
        return $this->where('id_order' ,'=',$id);
    }

    /**
     * Фильтр по провайдеру
     * @param $value
     * @return LogMerchantEventFilter
     */
    public function provider($value) {
        return $this->where('provider' ,'=', $value);
    }
}
