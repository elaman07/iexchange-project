<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 09.07.2019
 * Time: 14:48
 */

namespace App\Models\Filters;


use EloquentFilter\ModelFilter;

class ParserExchangeFilter extends ModelFilter
{


    /**
     * Номер карты
     *
     * @param $value
     * @return ParserExchangeFilter
     */
    public function name($value) {
        return (array_key_exists('checkbox_name', $this->input) ?
            $this->where('name', $value) :
            $this->where('name', 'like', '%' . $value . '%')
        );
    }

    public function idGroup($value) {
        return $this->where('id_group', '=', $value);
    }
}
