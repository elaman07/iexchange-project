<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class FilterCurrency extends Model
{
    use HasTranslations;

    protected $table = 'filter_currency';

    protected $fillable = [
        'name',
        'sorting'
    ];

    public $translatable = ['name'];

    public function currencies() {
        return $this->hasMany(Currency::class,'id_filter_currency', 'id');
    }

    public function currency1_many() {
        return $this->hasMany(DirectionExchange::class,'id_currency1','id');
    }
}
