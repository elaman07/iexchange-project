<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CompetitorRatesLog
 *
 * @property int $id
 * @property int $id_competitors
 * @property string|null $in_price
 * @property string|null $out_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CompetitorRates $rates
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog whereIdCompetitors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog whereInPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog whereOutPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompetitorRatesLog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CompetitorRatesLog extends Model
{
    protected $table = 'competitor_rates_log';

    protected $fillable = [
        'id_competitors',
        'in_price',
        'out_price'
    ];

    public function rates() {
        return $this->hasOne(CompetitorRates::class,'id','id_competitors');
    }
}
