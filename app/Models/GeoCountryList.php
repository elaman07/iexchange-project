<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 10.11.2020
 * Time: 9:01
 */

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;


class GeoCountryList extends Model
{
    use HasTranslations;

    /**
     * The activity model uses the 'sessions' database.
     *
     * @var string
     */
    protected $table = 'geo_country_list';


    protected $fillable = [
        'code',
        'value',
    ];

    protected $translatable = [
        'value'
    ];
}
