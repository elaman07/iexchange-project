<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;
class LanguageContent extends Model
{
    use HasTranslations;

    protected $table = "language_contents";

    protected $fillable = [
        'description_contact',
        'interface_regauth_text1',
        'interface_regauth_text2',
        'telegram_bot_name_button',
        'tech_breach_title',
        'tech_breach_text',
        'input_footer_title',
        'description_footer_text',
        'welcome_title',
        'welcome_description',
        'telegram_block_title',
        'telegram_block_description',
        'telegram_block_button',
        'description_verification_card',
        'error_verification_card',
        'sitename',
        'sitename_desc',
        'description',
        'keywords',
        'username_new_user',
        'main_title_header',
        'main_value_header',
        'chat_app_id',
        'working_online_text',
        'working_offline_text',
        'referral_system_text',
        'referral_system_text_footer',
        'cashback_text',
        'cashback_footer',
        'monitoring_text',
        'description_pr',
        'description_review',
        'working_offline_notify',
        's_order_notify_text',
        'jivosite_text_message',
        'title_rules_page',
        'description_rules_page',
        'description_request_payment'
    ];

    public $translatable  = [
        'description_contact',
        'interface_regauth_text1',
        'interface_regauth_text2',
        'telegram_bot_name_button',
        'tech_breach_title',
        'tech_breach_text',
        'input_footer_title',
        'description_footer_text',
        'welcome_title',
        'welcome_description',
        'telegram_block_title',
        'telegram_block_description',
        'telegram_block_button',
        'description_verification_card',
        'error_verification_card',
        'sitename',
        'sitename_desc',
        'description',
        'keywords',
        'username_new_user',
        'main_title_header',
        'main_value_header',
        'chat_app_id',
        'working_online_text',
        'working_offline_text',
        'referral_system_text',
        'referral_system_text_footer',
        'cashback_text',
        'cashback_footer',
        'monitoring_text',
        'description_pr',
        'description_review',
        'working_offline_notify',
        's_order_notify_text',
        'jivosite_text_message',
        'title_rules_page',
        'description_rules_page',
        'description_request_payment'
    ];
}
