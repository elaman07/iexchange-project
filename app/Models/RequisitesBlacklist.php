<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RequisitesBlacklist
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $score
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequisitesBlacklist whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RequisitesBlacklist extends Model
{
    protected $table = 'requisites_blacklist';

    protected $fillable = [
        'id_user',
        'score',
        'text'
    ];
}
