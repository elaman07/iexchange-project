<?php

namespace App\Models;

use App\Common\Support\HasTranslations;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskRejectionStatus
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_not_delete
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus whereIsNotDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TaskRejectionStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TaskRejectionStatus extends Model
{
    use HasTranslations;

    protected $table = 'tasks_rejection_status';

    protected $fillable = [
        'name',
        'is_not_delete'
    ];

    protected $translatable = [
        'name'
    ];
}
