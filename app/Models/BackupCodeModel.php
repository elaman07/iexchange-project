<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BackupCodeModel
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $num
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel whereNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BackupCodeModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BackupCodeModel extends Model
{
    protected $table = 'backup_codes';

    protected $fillable = [
        'id_user',
        'num',
        'code'
    ];
}
