<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class DirectionRequisite extends Model
{
    use Filterable;

    protected $table = 'direction_requisites';

    protected $fillable = [
        'name',
        'account_number',
        'view',
        'limit_day',
        'limit_month',
        'limit_views',
        'status'
    ];

    /**
     * Запрос на включенение только активных счетов
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActiveWallet($query)
    {
        return $query->where('status', '=',0);
    }

    public function direction_exchange(): MorphToMany
    {
        return $this->morphedByMany(DirectionExchange::class, 'model','directions_has_requisites','direction_requisite_id');
    }

}
