<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Menu extends Model
{
    use HasTranslations;

    protected $table = 'menu';

    protected $fillable = [
        'name',
        'name_alt',
        'sorting',
        'slug',
        'status',
        'text_color'
    ];

    public $translatable = ['name'];
}
