<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 30.09.2017
 * Time: 18:19
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Settings
 *
 * @property int $id
 * @property string|null $sitename
 * @property string|null $sitename_desc
 * @property string|null $url
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $job
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $description_home
 * @property int $allow_reserve
 * @property int $allow_partner
 * @property int $block_visible_reserve
 * @property int $notify_change_status
 * @property int $auto_register
 * @property int $max_time_task
 * @property int $service_break Технический перерыв
 * @property int $service_break_time
 * @property string|null $service_break_interval
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $update_parser
 * @property int $language
 * @property int $multi_language
 * @property int $task_denied
 * @property int $task_time_over
 * @property int $task_success
 * @property int $task_time
 * @property int $visible_description
 * @property int $visible_last_exchange
 * @property int $count_last_exchange
 * @property string $fund
 * @property string|null $email_support
 * @property int $lead_time_task
 * @property int $network_bitcoin Загруженность сети bitcoin
 * @property int $telegram_notify
 * @property string $telegram_chat_id
 * @property string $tg_stat_chat_id
 * @property int $show_task_page Количество, отображаемых заявок на странице
 * @property string|null $active_direction
 * @property int|null $generate_min_price
 * @property int $position_bestchange
 * @property int $unlimited_reserve
 * @property string|null $analytics_email
 * @property int $analytics_status
 * @property int $visible_my_order
 * @property int $disable_frame
 * @property int $reg_multi_ip
 * @property int $hide_exchange_guest
 * @property int $enable_cache
 * @property string|null $admin_allowed_ip
 * @property string|null $offline_reason
 * @property int $allow_filter_currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereActiveDirection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAdminAllowedIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAllowFilterCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAllowPartner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAllowReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAnalyticsEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAnalyticsStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereAutoRegister($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereBlockVisibleReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereCountLastExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereDescriptionHome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereDisableFrame($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereEmailSupport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereEnableCache($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereFund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereGenerateMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereHideExchangeGuest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereLeadTimeTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereMaxTimeTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereMultiLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereNetworkBitcoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereNotifyChangeStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereOfflineReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings wherePositionBestchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereRegMultiIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereServiceBreak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereServiceBreakInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereServiceBreakTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereShowTaskPage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereSitename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereSitenameDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTaskDenied($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTaskSuccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTaskTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTaskTimeOver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTelegramChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTelegramNotify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereTgStatChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereUnlimitedReserve($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereUpdateParser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereVisibleDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereVisibleLastExchange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereVisibleMyOrder($value)
 * @mixin \Eloquent
 * @property string|null $contact_job
 * @property string|null $contact_email
 * @property string|null $contact_telegram
 * @property string|null $contact_phone
 * @property int $is_contact_job
 * @property int $is_contact_email
 * @property int $is_contact_telegram
 * @property int $is_contact_phone
 * @property string|null $monitoring_description
 * @property int $captcha_restoring_password
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereCaptchaRestoringPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereContactJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereContactTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereIsContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereIsContactJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereIsContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereIsContactTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereMonitoringDescription($value)
 * @property string|null $order_priority
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Settings whereOrderPriority($value)
 */
class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'sitename','sitename_desc','email','phone','job','keywords','description','service_break',
        'count_last_exchange','visible_last_exchange','url','email_support','lead_time_task',
        'telegram_notify','telegram_chat_id','show_task_page','tg_stat_chat_id','active_direction','generate_min_price',
        'position_bestchange', 'unlimited_reserve','notify_change_status','analytics_email','analytics_status',
        'visible_description', 'description_home','visible_my_order', 'disable_frame','reg_multi_ip','hide_exchange_guest',
        'admin_allowed_ip', 'service_break_time', 'service_break_interval','offline_reason', 'allow_filter_currency',
        'contact_job','contact_email','contact_telegram','contact_phone', 'is_contact_job', 'is_contact_email',
        'is_contact_telegram', 'is_contact_phone','monitoring_description'
    ];
}