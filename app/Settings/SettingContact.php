<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class SettingContact extends Settings
{

    public static function group(): string
    {
        return 'contacts';
    }
}