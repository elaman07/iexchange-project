<?php

namespace App\Jobs;

use App\Models\Task;
use App\Notifications\TelegramClientToShotNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TelegramClientToShotJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        // Отправляем уведомления в бот
        if(iEXSetting('telegram_type_notification') == 0) {
            telegram_notification('user.to_shot', $this->order);
        } else {
            $this->order->notify(new TelegramClientToShotNotification());
        }

    }
}
