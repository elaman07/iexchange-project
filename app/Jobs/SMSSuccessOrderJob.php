<?php

namespace App\Jobs;

use App\Models\Task;
use App\Models\User;
use App\Notifications\SMSOrderNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SMSSuccessOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Информация о пользователе
     *
     * @var \App\Models\User
     */
    protected $user;

    /**
     * Информация о заявке
     *
     * @var \App\Models\Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Task $task
     */
    public function __construct(User $user, Task $task)
    {
        $this->user = $user;
        $this->order = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Если включено SMS уведомление
        if(iEXSetting('enable_sms_notification')) {
            $this->user->notify(new SMSOrderNotification($this->order));
        }
    }
}
