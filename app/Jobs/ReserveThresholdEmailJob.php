<?php
namespace App\Jobs;

use App\Mail\ReserveThresholdAlert;
use App\Models\ReserveAlert;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReserveThresholdEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $alert;

    /**
     * Create a new job instance.
     *
     * @param ReserveAlert $alert
     */
    public function __construct(ReserveAlert $alert)
    {
        $this->alert = $alert;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (get_admin_recipient_email() as $value)
        {
            \Mail::to($value)->send(new ReserveThresholdAlert($this->alert));
        }
    }
}
