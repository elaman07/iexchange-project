<?php
namespace App\Jobs;

use App\Mail\Order\OrderStatusMail;
use App\Models\Task;
use App\Notifications\TelegramNewOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        // Отправляем уведомления в бот
        if((int)iEXSetting('telegram_type_notification', 0) == 0) {
            telegram_notification('new_order', ['id' => iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id], [
                ['text' => '🕕 Детали заявки', 'callback_data' => 'order-id_admindata_'.$this->order->id]
            ]);
        } else {
            $this->order->notify(new TelegramNewOrder());
        }
    }
}
