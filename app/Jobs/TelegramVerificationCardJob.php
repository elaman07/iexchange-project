<?php

namespace App\Jobs;

use App\Models\VerificationCard;
use App\Notifications\TelegramVerificationCard;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramVerificationCardJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали карты
     *
     * @var \App\Models\VerificationCard
    */
    protected $verification_card;

    /**
     * Create a new job instance.
     *
     * @param VerificationCard $verificationCard
     */
    public function __construct(VerificationCard $verificationCard)
    {
        $this->verification_card = $verificationCard;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->verification_card->notify(new TelegramVerificationCard());
    }
}
