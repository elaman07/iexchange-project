<?php
namespace App\Jobs;

use App\Models\Task;
use App\Notifications\TelegramPaymentFailed;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class TelegramAutoPaymentFailed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;


    /**
     * Текст ошибки
     *
     * @var string
     */
    protected $error_text;

    /**
     * Create a new job instance.
     *
     * @param Task $order
     * @param null $error
     */
    public function __construct(Task $order, $error = null)
    {
        $this->order = $order;
        $this->error_text = $error;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle() {
        $this->order->notify(new TelegramPaymentFailed($this->error_text));
    }
}
