<?php

namespace App\Jobs;

use App\Common\Packages\Payment\Facades\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RefundFeeEthereumTokenJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $options;

    /**
     * Create a new job instance.
     *
     * @param $array
     */
    public function __construct($array)
    {
        $this->options = $array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $provider = PaymentFacade::wallet($this->options['provider']);
        $balance = (float)$provider->getBalance($this->options['address']);

        if($balance > 0) {
            $provider->sendRefundTransaction($this->options['address'], $this->options['private_key'], $balance);
        }
    }
}
