<?php
namespace App\Jobs;

use App\Mail\Order\OrderStatusMail;
use App\Models\LinksReview;
use App\Models\Task;
use App\Notifications\TelegramNewOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramClientOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Тип заявки
     *
     * @var string
     */
    protected $type;

    /**
     * Create a new job instance.
     *
     * @param Task $order
     * @param string $type
     */
    public function __construct(Task $order, $type = 'new')
    {
        $this->order = $order;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        if ($this->type == 'success') {
            $links = LinksReview::orderBy('sorting')->get();
            telegram_notification('user.success_order', [
                'order' => $this->order,
                'links' => $links
            ], null, $this->order->user->telegram);
        } elseif ($this->type == 'failed') {
            telegram_notification('user.failed_order', $this->order, null, $this->order->user->telegram);
        } else {
            telegram_notification('user.success_order', $this->order, null, $this->order->user->telegram);
        }
    }
}