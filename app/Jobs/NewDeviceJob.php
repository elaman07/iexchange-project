<?php
namespace App\Jobs;

use App\Models\User;
use App\Models\UserAuth;
use App\Notifications\NewDeviceNotification;
use App\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NewDeviceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Данные пользователя
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    protected $user;

    /**
     * Данные авторизаций
     *
     * @var UserAuth
     */
    protected $userAuth;

    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param UserAuth $userAuth
     */
    public function __construct(Authenticatable $user, UserAuth $userAuth)
    {
        $this->user = $user;
        $this->userAuth = $userAuth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new NewDeviceNotification($this->userAuth));
    }
}