<?php
namespace App\Jobs;

use App\Mail\NotificationNewOrder;
use App\Models\Task;
use App\Models\UserBalance;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class AdminNewOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Task $task)
    {
        $this->order = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (get_admin_recipient_email() as $value)
        {
            Mail::to($value)->send(new NotificationNewOrder([
                'id' => iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id,
                'name' => $this->order->user->name
            ]));
        }
    }
}
