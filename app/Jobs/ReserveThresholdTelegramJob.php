<?php
namespace App\Jobs;

use App\Models\ReserveAlert;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReserveThresholdTelegramJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $alert;

    /**
     * Create a new job instance.
     *
     * @param ReserveAlert $alert
     */
    public function __construct(ReserveAlert $alert)
    {
        $this->alert = $alert;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        telegram_notification('reserve_alert', $this->alert);
    }
}
