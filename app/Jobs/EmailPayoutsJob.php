<?php

namespace App\Jobs;

use App\Mail\Payouts\NewPayoutsMail;
use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EmailPayoutsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var WithdrawalRequest
     */
    protected $withdrawalRequest;

    /**
     * Create a new job instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (get_admin_recipient_email() as $value) {
            \Mail::to($value)->send(new NewPayoutsMail($this->withdrawalRequest));
        }
    }
}
