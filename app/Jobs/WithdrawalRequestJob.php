<?php

namespace App\Jobs;

use App\Mail\WithdrawalRequestMail;
use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class WithdrawalRequestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Объявляем модель WithdrawalRequest
     *
     * @var WithdrawalRequest
    */
    protected $withdrawalRequest;

    /**
     * Сумма выплаты
    */
    protected $amount;

    /**
     * Create a new job instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     * @param null $amount
     */
    public function __construct(WithdrawalRequest $withdrawalRequest, $amount = null)
    {
        $this->withdrawalRequest = $withdrawalRequest;
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Отправляем уведомление
        Mail::to($this->withdrawalRequest->user->email)
            ->send(new WithdrawalRequestMail($this->withdrawalRequest->id, $this->amount));
    }
}
