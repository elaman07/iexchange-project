<?php

namespace App\Jobs\Verification;

use App\Mail\VerificationFailMail;
use App\Models\VerificationCard;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Fail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали верификации
     *
     * @var VerificationCard
     */
    protected $verification;

    /**
     * Create a new job instance.
     *
     * @param VerificationCard $verificationCard
     */
    public function __construct(VerificationCard $verificationCard)
    {
        $this->verification = $verificationCard;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->verification->user->email)
            ->send(new VerificationFailMail($this->verification));
    }
}
