<?php

namespace App\Jobs;

use App\Models\Task;
use App\Notifications\TelegramAutoPaymentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramAutoPaymentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Провайдер
     *
     * @var string
    */
    protected $provider;

    /**
     * Create a new job instance.
     *
     * @param Task $order
     * @param string $provider
     */
    public function __construct(Task $order, string $provider)
    {
        $this->order = $order;
        $this->provider = $provider;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        if(config('telegram.enable'))
        {
            $price = $this->order->receiving_price.' '.$this->order->direction_exchange->currency2->code_currency->name;
            // Отправляем уведомления в бот
            if(iEXSetting('telegram_type_notification') == 0) {
                telegram_notification([
                    'message' => '['.$this->provider.'] - автовыплата по заявке №'.$this->order->id.' на сумму '.$price.' успешно завершена.'
                ]);
            } else {
                $this->order->notify(new TelegramAutoPaymentNotification($this->provider));
            }
        }
    }
}
