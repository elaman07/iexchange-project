<?php

namespace App\Jobs;

use App\Models\Task;
use App\Notifications\TelegramOrderPostponedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TelegramOrderPostponedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param Task $order
     */
    public function __construct(Task $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        if(config('telegram.enable'))
        {
            // Отправляем уведомления в бот
            if(iEXSetting('telegram_type_notification') == 0) {
                telegram_notification([
                    'message' => 'Заявка №'.iEXSetting('client_id_type_for_order') == 1 ? $this->order->public_id : $this->order->id.' по неизвестной причине была отложенотложена'
                ]);
            } else {
                $this->order->notify(new TelegramOrderPostponedNotification());
            }
        }
    }
}
