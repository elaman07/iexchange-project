<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramAdminAuthJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Информация о пользователе
     *
     * @var \App\Models\User
    */
    protected $user;


    /**
     * Статус авторизации
     *
     * @var boolean
    */
    protected $is_successful;

    /**
     * Тип авторизации
     *
     * @var string
     */
    protected $type;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param bool $is_successful
     * @param string $type
     */
    public function __construct(User $user, $is_successful = false, $type = '2fa')
    {
        $this->user = $user;
        $this->is_successful = $is_successful;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        if($this->is_successful == true) {
            $notify = $this->user->name. ' успешно авторизовался в админпанели';
        }else {
            $notify = 'Неудачная попытка '.$this->user->name. ' авторизоваться в админпанели через '.$this->type;
        }


        telegram_notification([
            'message' => $notify
        ]);
    }
}
