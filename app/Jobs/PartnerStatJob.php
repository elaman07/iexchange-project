<?php

namespace App\Jobs;

use App\Common\Packages\Referral\Models\ReferralLog;
use App\Common\Packages\Referral\Models\ReferralRelationship;
use App\Mail\PartnerStatMail;
use App\Models\ReferralStatistics;
use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class PartnerStatJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $user;

    /**
     * Create a new job instance.
     *
     * @param int $user_id
     */
    public function __construct(int $user_id)
    {
        $this->user = User::find($user_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $filename = \Carbon\Carbon::now()->format('dmYHis');

        $transitions_month = ReferralStatistics::whereRaw("BINARY `ref_hash` = ?",[ $this->user->referral_links->code])
            ->where('is_archive', '=', 0)->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();

        $register_month = ReferralRelationship::where('referral_link_id', '=', $this->user->referral_links->id)
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();

        $exchange_month = ReferralLog::where('id_referral_link', $this->user->referral_links->id)
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->count();

        $orders = Task::where([
            ['id_referral_link', $this->user->referral_links->id],
            ['status', '=', 4]
        ])->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->orderByDesc('id')->cursor()->reject(function ($item) {
            return !isset($item->referral_log);
        });

        //Общая сумма заработка за рефералы
        $sum_amount = ReferralLog::where('id_referral_link', $this->user->referral_links->id)
            ->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum('bonus_number');

        \PDF::loadView('_parent.pdf.partner_stat', [
            'orders'    =>  $orders,
            'register_month' => $register_month,
            'exchange_month' => $exchange_month,
            'transitions_month' => $transitions_month,
            'referral_ref' => $this->user->referral_links->program->percent,
            'total' => iex_number_format($sum_amount, 2, true)
        ])->save(storage_path('app/partners/'.$filename.'.pdf'));
        \Mail::to('netclion@gmail.com')->send(new PartnerStatMail($filename));
    }
}
