<?php
namespace App\Jobs;

use App\Mail\Auth\LockoutAuthMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LockoutAuthJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали пользователя
     *
     * @var User
     */
    protected $user;

    /**
     * Дополнительная информация
     *
     * @var string
     */
    protected $ip;

    /**
     * Create a new job instance.
     *
     * @param null $user
     * @param string $ip
     */
    public function __construct($user = null, $ip = null)
    {
        $this->user = $user;
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Данные по IP
        $geo = geoip($this->ip);

        $array = [
            'code_country' => '',
            'country' => '',
            'city' => ''
        ];

        $array['ip'] = $this->ip;
        if($geo->isAttribute('iso_code'))
            $array['code_country'] = $geo->getAttribute('iso_code');

        if($geo->isAttribute('country'))
            $array['country'] = $geo->getAttribute('country');

        if($geo->isAttribute('city'))
            $array['city'] = $geo->getAttribute('city');

        \Mail::to($this->user->email)
            ->send(new LockoutAuthMail($this->user, $array));
    }
}