<?php

namespace App\Jobs;

use App\Models\WithdrawalRequest;
use App\Notifications\TelegramPayouts;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramPayoutsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Объявляем модель WithdrawalRequest
     *
     * @var WithdrawalRequest
     */
    protected $withdrawalRequest;

    /**
     * Create a new job instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        // Отправляем уведомления в бот
        if(iEXSetting('telegram_type_notification') == 0) {
            telegram_notification('payouts.new_payouts', [
                'id' => $this->withdrawalRequest->id
            ]);
        } else {
            $this->withdrawalRequest->notify(new TelegramPayouts());
        }
    }
}
