<?php
namespace App\Jobs;

use App\Notifications\LoginSuccessfulNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EmailLoginSuccessfulJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Данные пользователя
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    protected $user;

    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     */
    public function __construct(Authenticatable $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Местоположение
        $geo_string = null;
        if(iEXSetting('is_geo_ip')) {
            $geo_ip = geoip($this->user->ip_address);
            $country = ($geo_ip->isAttribute('country') ? $geo_ip->getAttribute('country') : null);
            $city = ($geo_ip->isAttribute('city') ? $geo_ip->getAttribute('city') : null);
            $geo_string =  sprintf('%s %s', $country, $city);
        }

        // Here the email verification will be sent to the user
        $this->user->notify(new LoginSuccessfulNotification($this->user, $geo_string));
    }
}
