<?php

namespace App\Jobs;

use App\Mail\NotifyChangeReserve;
use App\Models\EventReserve;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class NotifyChangeReserveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * События резервов
     *
     * @var \App\Models\EventReserve
     */
    protected $eventReserve = [];

    /**
     * Create a new job instance.
     *
     * @param EventReserve $eventReserve
     */
    public function __construct(EventReserve $eventReserve)
    {
        $this->eventReserve = $eventReserve;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (get_admin_recipient_email() as $value)
        {
            Mail::to($value)->send(new NotifyChangeReserve($this->eventReserve));
        }
    }
}
