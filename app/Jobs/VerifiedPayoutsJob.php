<?php

namespace App\Jobs;

use App\Mail\Payouts\NewPayoutsMail;
use App\Mail\Payouts\VerifiedPayoutsMail;
use App\Models\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class VerifiedPayoutsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var WithdrawalRequest
     */
    protected $withdrawalRequest;

    /**
     * Create a new job instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->withdrawalRequest->user->email)
            ->send(new VerifiedPayoutsMail($this->withdrawalRequest));
    }
}
