<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 07.03.2020
 * Time: 11:55
 */

namespace App\Jobs;


use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Carbon;

class StatusSiteOfflineJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    public function handle()
    {
        $commonAmount = TaskConvertLog::whereDate('created_at', '=', Carbon::today());
        //Сумма обменов за сегодня
        $amount_exchanges = 0;
        if($commonAmount->count() > 0) {
            foreach($commonAmount->get() as $item) {
                $amount_exchanges += $item->to_rub;
            }
        }

        $today_order = Task::whereDate('created_at', '=', Carbon::today())->count();
        $order_success = Task::where('status', '=', 4)->whereDate('created_at', '=', Carbon::today())->count();
        $order_waiting = Task::where('status', '=', 3)->whereDate('created_at', '=', Carbon::today())->count();
        $order_cancel = Task::where('status', '=',5)->whereDate('created_at', '=', Carbon::today())->count();
        $new_users = User::whereDate('created_at', Carbon::today())->count();

        telegram_notification('stop_service', [
            'today_order' => $today_order,
            'order_success' => $order_success,
            'order_waiting' =>  $order_waiting,
            'cancel_order' => $order_cancel,
            'new_users' => $new_users,
            'amount_exchanges' => $amount_exchanges
        ]);
    }
}
