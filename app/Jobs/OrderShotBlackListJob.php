<?php

namespace App\Jobs;

use App\Mail\Order\OrderStatusMail;
use App\Mail\OrderShotBlackList;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrderShotBlackListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * E-mail получателя
     *
     * @var string
     */
    protected $to_email;

    /**
     * Текст уведомления
     *
     * @var string
     */
    protected $text = null;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param null $to
     * @param Task $order
     * @param string $text
     */
    public function __construct($to, Task $order, string $text)
    {
        $this->to_email = $to;
        $this->text = $text;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->to_email)->send(new OrderShotBlackList(
            $this->order, $this->text
        ));
    }
}
