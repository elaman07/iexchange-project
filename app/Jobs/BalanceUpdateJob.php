<?php

namespace App\Jobs;

use App\Models\UserBalance;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BalanceUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = UserBalance::where('balance', '>', 0)
            ->orWhere('balance_reward', '>', 0)->cursor();

        foreach ($user as $item)
        {
            $update = [];
            $update['id_code_currency'] = config('crypto.currency_id');
            // Обновление cashback
            if($item->balance > 0)
            {
                $update['balance'] = convert_to_currency(
                    $item->code_currency->name,
                    config('crypto.currency_payout'),
                    $item->balance
                );
            }

            // Обновление cashback
            if($item->balance_reward > 0) {
                $update['balance_reward'] = convert_to_currency(
                    $item->code_currency->name,
                    config('crypto.currency_payout'),
                    $item->balance_reward
                );
            }

            if(!empty($update)) {
                $item->update($update);
            }
        }
    }
}
