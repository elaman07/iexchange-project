<?php
namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UserLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * E-mail получателя
     *
     * @var \App\Models\User
     */
    protected $user;

    /**
     * Статус авторизации
     *
     * @var integer
     */
    protected $status = 0;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param int $status
     */
    public function __construct(User $user, $status = 0)
    {
        $this->user = $user;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        user_auth_log($this->user->id, $this->status);
    }
}
