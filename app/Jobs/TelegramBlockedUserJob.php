<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TelegramBlockedUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Текст письма
     *
     * @var string
    */
    protected $message;

    /**
     * Информация о пользователе
     *
     * @var User
    */
    protected $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param string $message
     */
    public function __construct(User $user, string  $message)
    {
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        telegram_notification(['message' => $this->message]);
    }
}
