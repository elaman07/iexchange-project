<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\WithdrawalRequest;
use App\Notifications\SMSPayoutNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SMSPayoutJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Информация о пользователе
     *
     * @var \App\Models\User
     */
    protected $user;

    /**
     * Информация о бонусных вознаграждений
     *
     * @var \App\Models\WithdrawalRequest
     */
    protected $withdrawalRequest;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(User $user, WithdrawalRequest $withdrawalRequest)
    {
        $this->user = $user;
        $this->withdrawalRequest = $withdrawalRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Если включено SMS уведомление
        if(iEXSetting('enable_sms_notification') and iEXSetting('sms_notify_success_payouts')) {
            $this->user->notify(new SMSPayoutNotification($this->withdrawalRequest));
        }
    }
}
