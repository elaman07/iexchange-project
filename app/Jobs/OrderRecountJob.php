<?php
namespace App\Jobs;

use App\Mail\Order\OrderRecountMail;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrderRecountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * E-mail получателя
     *
     * @var string
     */
    protected $to_email;

    /**
     * Дополнительные опции к отправке
     *
     * @var array
     */
    protected $options = [];

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param null $to
     * @param Task $order
     */
    public function __construct($to, Task $order)
    {
        $this->to_email = $to;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::to($this->to_email)->send(new OrderRecountMail($this->order));
    }
}
