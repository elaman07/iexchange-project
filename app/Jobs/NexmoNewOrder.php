<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 05.10.2019
 * Time: 14:36
 */

namespace App\Jobs;

use App\Models\Task;
use App\Notifications\SMSNewOrderNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use NotificationChannels\SmscRu\SmscRuChannel;

class NexmoNewOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали заявки
     *
     * @var Task
     */
    protected $order;

    /**
     * Create a new job instance.
     *
     * @param null $order
     */
    public function __construct($order = null)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $channel = 'nexmo';
        if(config('crypto.sms_driver') == 'smscru')
            $channel = 'smscru';
        elseif(config('crypto.sms_driver') == 'smsru')
            $channel = 'smsru';

        Notification::route($channel, iEXSetting('sms_phone_number_system'))
            ->notify(new SMSNewOrderNotification($this->order));
    }
}
