<?php

namespace App\Jobs;

use App\Models\LogDrain;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BulkSendingQiwiJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Список аккаунтов
     *
     * @
    */
    protected $accounts;

    /**
     * Create a new job instance.
     *
     * @param $accounts
     */
    public function __construct($accounts)
    {
        $this->accounts = $accounts;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $limit = (int)iEXSetting('qiwi_send_limit');

        foreach ($this->accounts as $account)
        {
            $selectedBalance = \Payment::driver('qiwi')->getFactory(config('payment.payment_qiwi_selected'))->getBalances();

            // Если превыщен лимит
            if($limit > 0 and $selectedBalance > $limit) {
                throw new \Exception('Превышен установленный лимит для '.config('payment.payment_qiwi_selected'));
            }

            $facade = \Payment::driver('qiwi')->getFactory($account->account_number);
            $balance = (float)$facade->getBalances();
            $id = rand(1111, 9999) * time();

            if($balance > 0)
            {
                $commision = $balance - $balance / 100 * 2;
                try {
                    \Payment::driver('qiwi')->getFactory($account->account_number)->sendMoneyToAccount([
                        'id' => (string)$id,
                        'sum' => [
                            'amount'   => (float)iex_number_format($commision, 2),
                            'currency' => '643'
                        ],
                        'paymentMethod' => [
                            'type' => 'Account',
                            'accountId' => '643'
                        ],
                        'comment' => (iEXSetting('qiwi_send_comment') ? iEXSetting('qiwi_send_comment') : ''),
                        'fields' => [
                            'account' => config('payment.payment_qiwi_selected')
                        ]
                    ]);
                }catch (\Exception $exception) {
                    LogDrain::create([
                        'type'          => 'qiwi',
                        'from_value'    => 'очередь',
                        'text'          =>  "[Массовый слив]: ({$account->account_number}): ".$exception->getMessage(),
                        'id_requisites' =>  (isset($account->id) ? $account->id : 0)
                    ]);
                }

                // Ожидание каждые 30 сек.
                sleep(30);
            }
        }
    }
}
