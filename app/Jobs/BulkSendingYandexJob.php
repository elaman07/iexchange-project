<?php

namespace App\Jobs;

use App\Models\LogDrain;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BulkSendingYandexJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $comment = null;

    protected $limit = 0;

    protected $accounts;

    /**
     * Create a new job instance.
     *
     * @param $accounts
     * @param null $comment
     * @param int $limit
     */
    public function __construct($accounts, $comment = null, $limit = 0)
    {
        $this->accounts = $accounts;
        $this->comment = $comment;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        foreach ($this->accounts as $account)
        {
            $selectedBalance = \Payment::driver('YandexMoney')->getFactory(config('payment.payment_yandexmoney_selected'))->getBalances();

            // Если превыщен лимит
            if($this->limit > 0 and $selectedBalance > $this->limit) {
                throw new \Exception('Превышен установленный лимит для '.config('payment.payment_yandexmoney_selected'));
            }

            $facade = \Payment::driver('YandexMoney')->getFactory($account->account_number);
            $balance = (float)$facade->getBalance();

            if($balance > 0)
            {
                $commision = $balance - $balance / 100 * 0.5;
                try {

                    $refund = \Payment::driver('YandexMoney')->getFactory($account->account_number);

                    $request_payment = $refund->requestPayment([
                        'pattern_id' => 'p2p',
                        'to' => trim($account->account_number),
                        'amount_due' => (float)iex_number_format($commision, 2),
                        'comment' => $this->comment,
                        'message' => $this->comment,
                        'codepro' => false,
                    ]);

                    $refund->processPayment($request_payment);

                    // Если не прошла транзакция
                    if(isset($request_payment) and $request_payment['status'] == 'refused')
                        throw new \Exception($request_payment['error']);
                }catch (\Exception $exception) {
                    LogDrain::create([
                        'type'          => 'yandex',
                        'from_value'    => 'очередь',
                        'text'          =>  "[Массовый слив]: ({$account->account_number}): ".$exception->getMessage(),
                        'id_requisites' =>  (isset($account->id) ? $account->id : 0)
                    ]);
                }

                // Ожидание каждые 30 сек.
                sleep(30);
            }
        }
    }
}
