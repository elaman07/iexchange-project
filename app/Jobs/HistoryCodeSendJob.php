<?php

namespace App\Jobs;

use App\Mail\NotificationClient;
use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class HistoryCodeSendJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Детали транзакции
     *
     * @var \App\Models\Task
    */
    protected Task $order;

    /**
     * EXCode для клиента
     *
     * @var string
    */
    protected string $code;

    /**
     * Create a new job instance.
     *
     * @param Task $task
     * @param string $code
     */
    public function __construct(Task $task, string $code)
    {
        $this->order = $task;
        $this->code = $code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->order->user->email)
            ->send(new NotificationClient($this->order->toArray(), [
                'message_success'   =>  '<b>Code:</b> '. $this->code
            ]));
    }
}
