<?php
namespace App\Console;

use App\Console\ShortCommand\UpdateRatesShortCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Spatie\ShortSchedule\ShortSchedule;


class Kernel extends ConsoleKernel
{
    protected $commands = [
        UpdateRatesShortCommand::class
    ];

    /**
     * Определите расписание команд приложения.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Оставливаем весь процесс, если запушен установщик
        if(Config::get('setup.enabled') == 1)
            return;

        // Автоматическое обновление карты сайта (В полночь)
        if(iEXSetting('seo_enabled_sitemap_cron')) {
            $schedule->command('update:sitemap')->daily();
        }

//        $schedule->command('monitoring:process')->everyMinute()
//            ->runInBackground();

        // Обновление деталей мерчанта каждую минуту
        $schedule->command('monitoring:merchant')
            ->everyMinute()
            ->runInBackground();

        $schedule->command('callback:receive_hash')
            ->everyMinute();

        // Обновление CRON резервов
        $schedule->command('reserve:update')
            ->everyMinute();

        $match = (int)iEXSetting('cron_interval_minutes_update_rates', 0);
        $compilerTime = match ($match)
        {
            1 => 'everyTwoMinutes',
            2 => 'everyThreeMinutes',
            3 => 'everyFiveMinutes',
            default => 'everyMinute',
        };

        // Если сайт включен, обновляем каждую минуту, а в случае отключения растягиваем на час.
        if((int)iEXSetting('cron_format_update_rates') == 0)
        {
            if(isJobOffline())
            {
                $schedule
                    ->command('compiler:courses')
                    ->hourly()
                    ->runInBackground()
                    ->after(function () {
                        \Log::info('Время курсов: '. Carbon::now()->toDateTimeString() .', dr_snapshot_id: '. \Cache::get('dr_snapshot_id'));
                    });
            } else {
                $schedule
                    ->command('compiler:courses')
                    ->{$compilerTime}()
                    ->runInBackground()
                    ->after(function () {
                        \Log::info('Время курсов: '. Carbon::now()->toDateTimeString() .', dr_snapshot_id: '. \Cache::get('dr_snapshot_id'));
                    });
            }
        }

        // Обновление курсов BestChange
        $schedule->command('compiler:bestchange')->everyMinute();

        // Проверяем и обновляем файлы курсов
        if((int)iEXSetting('cron_format_update_rates') == 0) {
            $schedule->command('scheme:files')
                ->everyMinute();
        }


        // Очистка старых логов
        $schedule->command('log:clears')->dailyAt('02:00');
        // Проверяем забаненных пользователей
        $schedule->command('ban:delete-expired')->hourly();
        // Ведется часовой мониторинг данных
        $schedule->command('monitoring:hourly')->hourly();
        // Автовыплата бонусных вознаграждений
        $schedule->command('bonus:auto_withdrawal')->daily();
        // Обновление адресов proxyfilter (Каждый день в 7 часов утра)
        $schedule->command('proxyfilter:reload')->dailyAt('07:00');

        // Экпорт данных
        if(config('export-data.is_cron')) {
            $schedule->command('export:excel')->dailyAt('05:00');
        }

        //Ежедневное архивирование данных в (23:59)
        if(iEXSetting('is_auto_reserves_report')) {
            $schedule->command('generator:report')->dailyAt('23:59');
        }

        // Ежедневное обновление данных в (03:00)
        $schedule->command('common:planning')->dailyAt('03:00');

        //Каждый день в 2 часа ночи создаем бэкап базы данных
        if((int)iEXSetting('backup_manager_enable', 0) == 1) {
            $schedule->command('snapshot:create')->dailyAt('02:00');
        }

        // Каждые 15 минут делаем скриншот Horizon
        $schedule->command('horizon:snapshot')->everyFiveMinutes();

        if((int)iEXSetting('is_tg_send_stat', 0) == 1) {
            $schedule->command('iextelegram:stat')->dailyAt('23:58');
        }

        //Авто-проверка обновлений
        if(config('license.update_autocheck') == 1) {
            $schedule->command('iex:autocheckupdate')->daily(); // Каждый день
        } elseif(config('license.update_autocheck') == 2) {
            $schedule->command('iex:autocheckupdate')->weekly(); // Раз в неделю
        } elseif(config('license.update_autocheck') == 3) {
            $schedule->command('iex:autocheckupdate')->monthly(); // Раз в месяц
        }

    }

    protected function shortSchedule(ShortSchedule $shortSchedule)
    {
        $cron_interval_second_update_rates = (int)iEXSetting('cron_interval_second_update_rates');
        // Если сайт включен, обновляем каждую минуту, а в случае отключения растягиваем на час.
        if((int)iEXSetting('cron_format_update_rates') == 1)
        {
            if(isJobOffline() == 0) {
                $shortSchedule->command('compiler:courses')->everySeconds($cron_interval_second_update_rates);
            }
        }

        // Проверяем и обновляем файлы курсов
        if((int)iEXSetting('cron_format_update_rates') == 1) {
            $shortSchedule->command('scheme:files')->everySeconds($cron_interval_second_update_rates+2);
        }
    }

    /**
     * Зарегистрируйте команды для приложения.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
