<?php

namespace App\Console\Commands;


use App\Events\CoursesFileEvent;
use App\Events\StatusSiteEvent;
use App\Jobs\StatusSiteOfflineJob;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class StopModeOperation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'operation:stop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'We finish the mode of operation of operators';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        if((int)iEXSetting('type_working_mode') == 2) {
            iEXSetting(['is_working_manual' => 1])->save();
        }

        // Очистка файлов курсов
        if((int)iEXSetting('grates_static') == 1) {
            \Crypto::setSchema('xml')->delete();
        }

        if((int)iEXSetting('grates_static') == 0) {
            \Crypto::setSchema('xml')->empty();
        }

        \Crypto::setSchema('txt')->empty();
        \Crypto::setSchema('json')->delete();

        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new StatusSiteEvent(iEXContentLanguage('sitename') . ' отключились на тех. перерыв', 2));
        }
        // Если уведомление включено, отправляем сообщение об остановке сайте
        if(config('telegram.enable')) {
            broadcast(new StatusSiteOfflineJob());
        }
    }
}
