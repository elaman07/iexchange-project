<?php

namespace App\Console\Commands;

use App\Common\Packages\Pages\Models\Page;
use App\Common\Packages\Pages\Pages;
use App\Models\FaqCategory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ClearUserWalletsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallet:clear_stories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сброс всех сохраненных счетов клиентов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if((int)iEXSetting('is_cron_user_wallet_updating') != 1)
        {
            \DB::table('user_wallet_stories')->delete();
            \DB::table('user_wallet_stories')->truncate();
            iEXSetting(['is_cron_user_wallet_updating' => 1])->save();
        }
    }

}
