<?php

namespace App\Console\Commands;

use App\Models\DirectionExchangeErrorLog;
use App\Models\LogAutoPayment;
use App\Models\LogCheckPay;
use App\Models\ReferralStatistics;
use App\Models\TaskLogConfirmation;
use App\Models\TaskStatusLog;
use App\Models\VerificationCard;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class StatusRatesFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scheme:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checker files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('- Запуск обновления файла курсов');

        // Проверка, существует ли файл экспортный
        $xml_parser_file = \File::exists(public_path((iEXSetting('grates_filename').'.xml')));

        // Если включен экспорт курсов в реальном времени, то удаляем файл
        if((int)iEXSetting('grates_static') == 1 and $xml_parser_file) {
            \Crypto::setSchema('xml')->delete();
        }

        if(isJobOffline())
        {
            if((int)iEXSetting('grates_static') == 0) {
                \Crypto::setSchema('xml')->empty();
            }
            \Crypto::setSchema('txt')->empty();
            \Crypto::setSchema('json')->delete();

        } else {

            if((int)iEXSetting('grates_static') == 0) {
                \Crypto::setSchema('xml')->build();
            }
            \Crypto::setSchema('txt')->build();
            \Crypto::setSchema('json')->build();
        }


        $this->comment('+ Файл курсов успешно обновлен');
    }
}
