<?php


namespace App\Console\Commands;



use App\Events\StatusSiteEvent;
use App\Models\Task;
use App\Models\TaskConvertLog;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class TelegramStatisticsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iextelegram:stat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        // Если уведомление включено, отправляем сообщение об остановке сайте
        if(config('telegram.enable')) {
            $commonAmount = TaskConvertLog::whereDate('created_at', '=', Carbon::today());
            //Сумма обменов за сегодня
            $amount_exchanges = 0;
            if($commonAmount->count() > 0) {
                foreach($commonAmount->get() as $item) {
                    $amount_exchanges += $item->to_rub;
                }
            }

            $today_order = Task::whereDate('created_at', '=', Carbon::today())->count();
            $order_success = Task::where('status', '=', 4)->whereDate('created_at', '=', Carbon::today())->count();
            $order_waiting = Task::where('status', '=', 3)->whereDate('created_at', '=', Carbon::today())->count();
            $order_cancel = Task::where('status', '=',5)->whereDate('created_at', '=', Carbon::today())->count();
            $new_users = User::whereDate('created_at', Carbon::today())->count();

            telegram_notification('stop_service', [
                'today_order' => $today_order,
                'order_success' => $order_success,
                'order_waiting' =>  $order_waiting,
                'cancel_order' => $order_cancel,
                'new_users' => $new_users,
                'amount_exchanges' => $amount_exchanges
            ]);
        }
    }
}
