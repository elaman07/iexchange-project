<?php

namespace App\Console\Commands;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\DirectionExchangeErrorLog;
use App\Models\LogAutoPayment;
use App\Models\LogCheckPay;
use App\Models\ReferralStatistics;
use App\Models\Task;
use App\Models\TaskInfo;
use App\Models\TaskLogConfirmation;
use App\Models\TaskStatusLog;
use App\Models\VerificationCard;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class PayTransactionHashCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'callback:receive_hash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Получение hash с blockchain';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Для выплаты callback
        Task::where([
                ['is_status_pay_callback', '!=', 0],
                ['is_attempts_pay', '<=', 3]
            ])->chunk(100, function($orders)
            {
                foreach ($orders as $order)
                {
                    $this->comment('Получение заявки №'.$order->id);

                    $gateway_payment = $order->direction_exchange->currency2->pay;
                    $pay_id = $gateway_payment->id;

                    // Данные о полученном платеже
                    $driver = PaymentFacade::pay($gateway_payment->gateway->alias, $pay_id)->api();

                    try {
                        $details = $driver->findTransaction($order->is_status_pay_callback);
                        if(isset($details['id']))
                        {
                            $status_pay_api = 0;
                            if($details['status'] == 'process') {
                                $status_pay_api = 1;
                            }elseif($details['status'] == 'paid') {
                                $status_pay_api = 2;
                            }elseif(in_array($details['status'], ['error', 'cancel'])) {
                                $status_pay_api = 3;
                            }

                            $options = [
                                'status_pay_api' => $status_pay_api,
                                'is_attempts_pay' => $order->is_attempts_pay+1
                            ];

                            if($status_pay_api == 2 and $order->is_status_pay_email == 0) {
                                $driver->findTransaction($order->is_status_pay_callback, $order->email);
                                $options['is_status_pay_email'] = 1;
                            }

                            $order->update($options);

                        }

                    }catch (\Exception $exception) {

                    }
                }
            });


        TaskInfo::where('is_wait_hash_pay', 1)
            ->chunk(100, function($tasks) {

                foreach ($tasks as $info) {
                    $this->comment('Получение Transaction Hash по заявке '.$info->id_task);

                    // Если не найден Blockchain ID
                    if(!isset($info->pay_transaction_hash)) {
                        $info->update(['is_wait_hash_pay' => 0]);
                        return;
                    }

                    if(!empty($info->pay_transaction_hash->transaction_hash)) {
                        $info->update(['is_wait_hash_pay' => 0]);
                        return;
                    }

                    $gateway_payment = $info->tasks->direction_exchange->currency2->pay;
                    $pay_id = $gateway_payment->id;

                    // Данные о полученном платеже
                    $driver = PaymentFacade::pay($info->pay_transaction_hash->provider, $pay_id)->api();

                    try {
                        // Для WestWallet
                        if(\Str::lower($info->pay_transaction_hash->provider) == 'westwallet')
                        {
                            $details = $driver->findTransaction($info->pay_transaction_hash->api_transfer_id);
                            if(isset($details['blockchain_hash']) and !empty($details['blockchain_hash']))
                            {
                                $info->pay_transaction_hash->update([
                                    'transaction_hash' => $details['blockchain_hash']
                                ]);
                                $info->update(['is_wait_hash_pay' => 0]);
                            }
                        } elseif(\Str::lower($info->pay_transaction_hash->provider) == 'whitebitcrypto')
                        {
                            $details = $driver->findTransactionWithdraw($info->tasks->direction_exchange->currency2->code_currency->name, $info->id_task);

                            if(!empty($details['uniqueId']) and $details['uniqueId'] == $info->id_task)
                            {
                                if(isset($details['transactionHash']) and !empty($details['transactionHash']))
                                {
                                    $info->pay_transaction_hash->update([
                                        'transaction_hash' => $details['transactionHash']
                                    ]);

                                    $info->update(['is_wait_hash_pay' => 0]);
                                }
                            }
                        }elseif(\Str::lower($info->pay_transaction_hash->provider) == 'garantex')
                        {
                            $details = $driver->findWithdrawsTransaction($info->pay_transaction_hash->api_transfer_id);
                            if(isset($details['txid']) and !empty($details['txid']))
                            {
                                $info->pay_transaction_hash->update([
                                    'transaction_hash' => $details['txid']
                                ]);
                                $info->update(['is_wait_hash_pay' => 0]);
                            }
                        }
                    }catch (\Exception $exception) {

                    }
                }
            });
    }
}
