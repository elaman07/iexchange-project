<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class iEXTelegramWebhook extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'iex:telegram {bot?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set webhook url for telegram bot';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function handle()
    {
        $bot = $this->hasArgument('bot') ? $this->argument('bot') : null;

        if(is_null($bot)) {
            return $this->warn('Не выбран бот');
        }


        if($bot == 'user')
        {
            iEXSetting(['is_user_telegram_status' => 1])->save();
            $url = env('APP_URL').'/telegram-bot/'.env('TELEGRAM_SECURITY_PREFIX').'/client';
            $response = Telegram::bot('iex')->setWebhook([
                'url' => $url
            ]);
        } elseif($bot == 'admin')
        {
            iEXSetting(['is_admin_telegram_status' => 1])->save();

            $url = env('APP_URL'). '/telegram-bot/'.env('TELEGRAM_SECURITY_PREFIX').'/admin';
            $response = Telegram::bot('admin')->setWebhook([
                'url' => $url
            ]);
        }




        $this->info('[' . ($response == true ? 'success' : 'error') . '] Webhook handler URL: ' . $url);
    }
}
