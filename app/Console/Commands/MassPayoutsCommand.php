<?php

namespace App\Console\Commands;

use App\Common\Packages\Payment\PaymentFacade;
use App\Models\GatewayPayment;
use App\Models\Task;
use Illuminate\Console\Command;

class MassPayoutsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iex:mass_payouts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Массовые выплаты';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gateway = GatewayPayment::where([
            ['is_mass_payouts', 1], ['status', 1]
        ])->get();

        foreach ($gateway as $item)
        {
            $orders = Task::where([['status', '=', 4], ['queue_status', 1], ['id_payment_gateway', $item->id]])->orderBy('id', 'asc')->cursor();
            $amounts = $orders->implode('receiving_price', ',');
            $to_addresses = $orders->implode('to_shot', ',');

            $facade = PaymentFacade::pay($item->gateway->alias, $item->id)
                ->api();


            $options_mass = [
                'amounts' => $amounts,
                'to_address' => $to_addresses
            ];

            $response = $facade->mass_transfer_btc($options_mass, $item->priority_fee);
            foreach ($orders as $order) {
                $order->update(['queue_status' => 2]);
            }

            if(isset($response['txid'])) {
                \Log::debug(json_encode('MASS PAYOUTS: ' .$response));
            }
        }
    }
}
