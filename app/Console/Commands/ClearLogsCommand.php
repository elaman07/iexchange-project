<?php

namespace App\Console\Commands;

use App\Models\DirectionExchangeErrorLog;
use App\Models\LogAutoPayment;
use App\Models\LogCheckPay;
use App\Models\ParserExchangeHttpLog;
use App\Models\ReferralStatistics;
use App\Models\TaskLogConfirmation;
use App\Models\TaskStatusLog;
use App\Models\VerificationCard;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ClearLogsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:clears';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Очистика логов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day_confirm_log = (int)iEXSetting('settings_log_confirmation_clear');
        $day_old_status = (int)iEXSetting('settings_log_old_status_clear');
        $day_autopayment = (int)iEXSetting('settings_log_autopayment_clear');
        $day_referral_archive = (int)iEXSetting('settings_log_partners_transition_clear');
        $day_checkpayment = (int)iEXSetting('settings_log_checkpayment_clear');
        $day_direction = (int)iEXSetting('settings_log_direction_e_clear');
        $day_card = (int)iEXSetting('settings_log_unverified_card');
        $day_course_http = (int)iEXSetting('settings_log_parser_http');

        // Удаление лога запросов к источникам курсов (дней)
        if($day_course_http > 0) {
            ParserExchangeHttpLog::whereDate('created_at', '<=', Carbon::now()->subDays($day_course_http))->delete();
        }

        // Очистка логов подтверждений
        if($day_confirm_log > 0) {
            TaskLogConfirmation::whereDate('created_at', '<=', Carbon::now()->subDays($day_confirm_log))->delete();
        }

        // Удаление лога старых статусов заявок
        if($day_old_status > 0) {
            TaskStatusLog::whereDate('created_at', '<=', Carbon::now()->subDays($day_old_status))->delete();
        }

        // Удаление логов автоматических выплат
        if($day_autopayment > 0) {
            LogAutoPayment::whereDate('created_at', '<=', Carbon::now()->subDays($day_autopayment))->delete();
        }

        // Удаление логов проверки оплата
        if($day_checkpayment > 0) {
            LogCheckPay::whereDate('created_at', '<=', Carbon::now()->subDays($day_checkpayment))->delete();
        }

        // Архивация партнерских переходов
        if($day_referral_archive > 0) {
            ReferralStatistics::whereDate('created_at', '<=', Carbon::now()->subDays($day_referral_archive))->delete();
        }

        // Очистка логов направлений
        if($day_direction > 0) {
            DirectionExchangeErrorLog::whereDate('created_at', '<=', Carbon::now()->subDays($day_direction))->delete();
        }



        // Очистка неверифицированных карт
        if($day_card > 0) {
            VerificationCard::whereIn('status', [2, 3])->whereDate('created_at', '<=', Carbon::now()->subDays($day_card))->delete();
        }

    }
}
