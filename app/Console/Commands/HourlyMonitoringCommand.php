<?php

namespace App\Console\Commands;

use App\Jobs\ReserveThresholdEmailJob;
use App\Jobs\ReserveThresholdTelegramJob;
use App\Models\DirectionExchange;
use App\Models\MainEventLog;
use App\Models\ReserveAlert;
use App\Models\Task;
use App\Models\UnpaidItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Contests\Entities\ContestModel;
use Nwidart\Modules\Facades\Module;

class HourlyMonitoringCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitoring:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Часовой мониторинг данных';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /// Проверка неоплаченных заявок
        $unpaid = UnpaidItem::find(1);
        $exchange = DirectionExchange::with('tasks')->chunk(100, function ($items) use($unpaid)
        {
            foreach ($items as $item)
            {
                // Если включено индивидуальное удаление заявок то, работаем с ней
                if(!empty($item->auto_del_order_status))
                {
                    $item_tasks = Task::where('id_direction_exchange', '=', $item->id)
                        ->whereIn('status', explode(',', $item->auto_del_order_status))
                        ->where('created_at', '<', \Illuminate\Support\Carbon::now()
                        ->subDays($item->auto_del_order_day)
                        ->subHours($item->auto_del_order_hour)
                        ->subMinutes($item->auto_del_order_minute))->get();

                    if(count($item_tasks) > 0)
                    {
                        foreach ($item_tasks as $item_task) {
                            iex_order_status_log($item_task, 11);
                            $item_task->update(['status' => 11]);
                        }
                    }
                } else {
                    //Если включена автоматическое удаление
                    if($unpaid->auto_delete == 1 and $unpaid->rules_cron == 1 and !empty($unpaid->order_status))
                    {
                        $item_tasks = $item->tasks()->whereIn('status', explode(',', $unpaid->order_status))
                            ->whereDate('created_at', '<=', \Illuminate\Support\Carbon::now()
                                ->subHours($unpaid->removal_time)
                                ->subMinutes($unpaid->removal_minute))->get();

                        if(count($item_tasks) > 0)
                        {
                            foreach ($item_tasks as $item_task) {
                                iex_order_status_log($item_task, 11);
                                $item_task->update(['status' => 11]);
                            }
                        }
                    }
                }
            }

        });


        // Проверяем активные конкурсы
        if(Module::find('Contests')->isEnabled() == 1)
        {
            $contest = ContestModel::where('status', '=', 1)->first();
            if(!empty($contest))
            {
                $start_time = Carbon::now();
                $duration = Carbon::parse($contest->duration);
                $leadtime = 0;
                if($duration->gt($start_time)) {
                    $leadtime = $duration->diffInSeconds($start_time);
                }
                if($leadtime == 0) {
                    $contest->update(['status' => 2]);
                }
            }

        }

        // Проверяем, и если не активеен Telegram, включаем
        if((int)iEXSetting('is_enable_telegram_exchange') == 1 and iEXSetting('is_user_telegram_status') == 0) {
            $this->call('iex:telegram', ['bot' => 'user']);
        }

        // Проверяем, и если не активеен Telegram, включаем
        if(!empty(env('ADMIN_TELEGRAM_BOT_TOKEN')) and iEXSetting('is_admin_telegram_status') == 0) {
            $this->call('iex:telegram', ['bot' => 'admin']);
        }


        // Оповещение о заканчивающимся резерве валюты для администратора.
        if(iEXSetting('is_enabled_reserve_threshold') == 1)
        {
            ReserveAlert::chunk(100, function($reserves) {
                $delay = now()->addMinutes(10);
                foreach ($reserves as $reserve) {
                    // Пропускаем невходящие в порог резервы
                    if($reserve->reserves->summa > $reserve->threshold)
                        continue;

                    // E-mail оповещение
                    if($reserve->is_email)
                        dispatch((new ReserveThresholdEmailJob($reserve))->onQueue('low')->delay($delay));

                    // Telegram оповещение
                    if($reserve->is_telegram)
                        dispatch((new ReserveThresholdTelegramJob($reserve))->onQueue('low')->delay($delay));
                }
            });
        }

        // Удаляем старый журнал
        if(iEXSetting('event_log_cleanup_days') > 0) {
            MainEventLog::whereDate('created_at', '<', Carbon::now()->subDays(iEXSetting('event_log_cleanup_days')))->delete();
        }

        // Архивация заявок
        if(iEXSetting('is_auto_archived') == true) {
            Task::where('is_archive', '=', 0)
                ->whereDate('created_at', '<=', Carbon::today()->subDays(iEXSetting('archived_days')))
                ->whereIn('status', explode(',', iEXSetting('archived_statuses')))
                ->chunk(100, function($orders) {
                    foreach ($orders as $order) {
                        $order->update(['is_archive' => 1]);
                    }
                });
        }


        // Проверка временных прав пользователей
        User::whereNotNull('role_expired_at')->where('role_expired_at', '<=', Carbon::now())
            ->chunk(100, function($users) {
                foreach ($users as $user) {
                    \Log::info(
                        sprintf('Пользователь %s удален из группы %s', $user->name, $user->roles()->pluck('name')->implode(', '))
                    );
                    $user->update(['role_expired_at' => null]);
                    $user->roles()->detach();
                }
            });
    }
}
