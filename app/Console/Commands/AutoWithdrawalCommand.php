<?php

namespace App\Console\Commands;

use App\Models\Currency;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\WithdrawalRequest;
use Illuminate\Console\Command;

class AutoWithdrawalCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bonus:auto_withdrawal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Автовывод реферальных бонусов и кэшбэк`а';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Список пользователей которые включили систему автовывода
        $users = User::where('auto_withdrawal', '=',1)->get();

        foreach ($users as $user)
        {
            $withdrawalRequest = WithdrawalRequest::where([['id_user', $user->id], ['status', 0]]);

            // Для начало проверяем, есть ли активные
            if($withdrawalRequest->count() == 0)
            {
                // Дополнительная проверка мин. суммы перевода
                if($user->min_withdrawal < iEXSetting('min_auto_withdrawal')) {
                    return false;
                }

                $balance = ($user->user_balance->balance + $user->user_balance->balance_reward);
                if($balance > $user->min_withdrawal)
                {
                    $this->comment('Создана заявка для пользователя #'.$user->id);

                    $balance = $this->getBalance([
                        'referral'  =>  $user->withdrawal_request->referral,
                        'cashback'  =>  $user->withdrawal_request->reward
                    ], $user->withdrawal_request->id_currency, $user->user_balance);

                    WithdrawalRequest::create([
                        'id_user'               => $user->id,
                        'id_currency'           => $user->withdrawal_request->id_currency,
                        'referral'              => $user->withdrawal_request->referral,
                        'reward'                => $user->withdrawal_request->reward,
                        'score'                 => $user->withdrawal_request->score,
                        'balance_referral'      => $balance['referral'],
                        'balance_reward'        => $balance['reward'],
                        // В случае возврата
                        'base_referral'         =>  $balance['base_referral'],
                        'base_reward'           =>  $balance['base_reward'],
                        'ip'                    =>  request()->ip()
                    ]);
                }
            }
        }
    }

    /**
     * Получение баланса
     *
     * @param $purpose
     * @param $id
     * @return array
     */
    protected function getBalance($purpose, $id, $balance)
    {
        $currency = Currency::find($id);
        $price_referral = 0;
        $price_reward = 0;
        $base_referral = 0;
        $base_reward = 0;

        // Выплата реферальных бонусов
        if($purpose['referral'] == 1) {
            if(mb_strtolower($currency->code_currency->name) == 'usd') {
                $price_referral = (float)convert_to_usd('RUB', $balance->balance);
            } else {
                $price_referral = (float)$balance->balance;
            }
            $base_referral = (float)$balance->balance;
        }

        // Выплата вознаграждений
        if($purpose['cashback'] == 1) {
            if(mb_strtolower($currency->code_currency->name) == 'usd') {
                $price_reward = (float)convert_to_usd('RUB', $balance->balance_reward);
            } else {
                $price_reward = (float)$balance->balance_reward;
            }
            $base_reward = (float)$balance->balance_reward;
        }

        return [
            'referral'      =>  $price_referral,
            'reward'      =>  $price_reward,
            'combine'       =>  ($price_referral + $price_reward),
            'base_referral' =>  $base_referral,
            'base_reward'   =>  $base_reward
        ];
    }
}
