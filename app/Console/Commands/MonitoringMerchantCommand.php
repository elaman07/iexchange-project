<?php

namespace App\Console\Commands;

use App\Common\Packages\Payment\Facades\PaymentFacade;
use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Jobs\TelegramAutoPaymentFailed;
use App\Models\LogAutoPayment;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MonitoringMerchantCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitoring:merchant';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяем получаемые средства в фоновом режиме';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        // Каждую минуту проверяем
        Task::where([
            ['status', '=', 3],
            ['is_bot', '=', 1],
            ['is_hold', '=', 0],
            ['is_auto_check_pay', '=', 1]
        ])->where('next_checkout_at', '<=', Carbon::now()->toDateTimeString())
            ->chunk(100, function($tasks)
            {

                // Список актуальных заявок
                foreach ($tasks as $task) {
                    // Сразу обновляем время (Чтобы лишний раз не нагружать сервер)
                    $task->update(['next_checkout_at' => Carbon::now()->addSeconds(30)->toDateTimeString()]);
                    $this->comment('Проверка заявки №'.$task->id);

                    $transaction = TransactionFacade::init($task);
                    $transaction->setIsCron(true);
                    if($transaction->hasTimeOutAutoPayment())
                    {
                        $this->comment(sprintf('Заявка %s переведена в ручной режим', $task->id));
                        $task->update(['is_bot' => 0]);
                        continue;
                    }

                    $transaction->setCheckAccurateBalance(true);
                    $check_pay = $transaction->checkInPayment();

                    // Если средства получены
                    if($check_pay['status'] == 0 and iEXSetting('is_enabled_autopayment') == 1)
                    {
                        if($transaction->getCurrencyOut()->auto_pay_order_pay != 2)
                        {
                            try {
                                $transaction->paymentRelatedInstanceFor();
                            } catch (\Exception $e)
                            {
                                $transaction->disableIsBot();
                                $transaction->addErrorLogToSignOrder(1, $e->getMessage());

                                LogAutoPayment::create([
                                    'id_task' => $task->id,
                                    'status' => 1,
                                    'event_value' => $e->getMessage()
                                ]);

                                // Уведомлять в случае если неудачной автовыплаты
                                if(iEXSetting('is_notify_failed_autopayment')) {
                                    $delay = now()->addMinutes(1);
                                    dispatch(
                                        new TelegramAutoPaymentFailed($task, $e->getMessage())
                                    )->delay($delay)->onQueue('low');
                                }
                            }
                        }
                    }
                }
            });
    }
}
