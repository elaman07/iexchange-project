<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class IEXResetPasswordCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iex:resetpass';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сбросить пароль администратора';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::find(1);
        $this->line("Новый пароль для логина {$user->name}");

        $options = [
            'password' => Hash::make($this->secret('Пожалуйста, введите пароль для администратора'))
        ];

        $user->update($options);
        $this->info('Новый пароль успешно обновлен ⚡');
    }
}
