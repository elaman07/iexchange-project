<?php

namespace App\Console\Commands;

use App\Models\User;
use Brotzka\DotenvEditor\DotenvEditor;
use Brotzka\DotenvEditor\Exceptions\DotEnvException;
use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;
use Eloquent;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class IEXImportDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iex:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Хотите настроить iEXExchanger перед установкой?')) {
            $this->configureKey();
            $this->configureDatabase();
            $this->configureDrivers();
            $this->configureUser();
        }

        $this->line('Начинаем установку iEXExchanger...');
        $this->info('iEXExchanger установлен ⚡');
    }

    /**
     * Генерация уникального ключа
     *
     * @return void
     */
    protected function configureKey()
    {
        $this->call('key:generate');
    }


    /**
     *  Настройки админа
     *
     * @return void
     */
    protected function configureUser()
    {
        if (!$this->confirm('Вы хотите обновить пароль для администратора?')) {
            return;
        }
        // Нам нужно обновить конфигурацию, чтобы получить доступ к вновь подключенной базе данных.
        $this->getFreshConfiguration();

        Eloquent::unguard();
        DB::unprepared(File::get(database_path('/iex_data.sql')));
        $this->call('migrate');

        $user = [
            'name' => $this->ask('Пожалуйста, введите имя пользователя'),
            'email'    => $this->ask('Пожалуйста, введите E-mail'),
            'password' => Hash::make($this->secret('Пожалуйста, введите пароль'))
        ];

        User::updateOrCreate([
            'id' => 1
        ], $user);
    }

    /**
     * Boot a fresh copy of the application configuration.
     *
     * @return void
     */
    protected function getFreshConfiguration()
    {
        $app = require $this->laravel->bootstrapPath().'/app.php';
        $app->make(Kernel::class)->bootstrap();
    }

    /**
     * Конфигурация базы данных
     *
     * @param array $default
     * @return mixed
     */
    protected function configureDatabase(array $default = [])
    {
        $config = array_merge([
            'DB_HOST'   =>  null,
            'DB_DATABASE' => null,
            'DB_USERNAME' => null,
            'DB_PASSWORD' => null,
            'DB_PORT'     => null,
        ], $default);

        $config['DB_HOST'] = $this->ask("Какой хост у вашей базы данных MYSQL?", $config['DB_HOST']);
        if ($config['DB_HOST'] === 'localhost') {
            $this->warn("Использование 'localhost' приведет к использованию локального сокета Unix. Используйте 127.0.0.1, если вы хотите подключиться через TCP");
        }
        $config['DB_DATABASE'] = $this->ask('Какое имя базы данных должен использовать iEXExchanger?', $config['DB_DATABASE']);
        $config['DB_USERNAME'] = $this->ask('Укажите имя пользователя для соединения с базой данных', $config['DB_USERNAME']);
        $config['DB_PASSWORD'] = $this->secret('Укажите пароля для соединения с базой данных', $config['DB_PASSWORD']);
        $config['DB_PORT'] = 3306;
        if ($this->confirm('Ваша база данных использует нестандартный номер порта?')) {
            $config['DB_PORT'] = $this->anticipate('Какой номер порта использует ваша база данных?', [3306, 5432], $config['DB_PORT']);
        }


        $this->formatConfigsTable($config);

        if (!$this->confirm('Правильно ли указаны настройки?')) {
            return $this->configureDatabase($config);
        }

        $this->writeEnv($config);
    }


    /**
     * Configure other drivers.
     *
     * @param array $default
     * @return mixed
     */
    protected function configureDrivers(array $default = [])
    {
        $config = array_merge([
            'CACHE_DRIVER'   => null,
            'SESSION_DRIVER' => null,
            'QUEUE_DRIVER'   => null,
        ], $default);

        // Отформатируйте настройки, чтобы отобразить их в таблице.
        $this->formatConfigsTable($config);

        $config['CACHE_DRIVER'] = $this->choice('Какой драйвер Cache вы хотите использовать?', [
            'apc'       => 'APC(u)',
            'array'     => 'Array',
            'database'  => 'Database',
            'file'      => 'File',
            'memcached' => 'Memcached',
            'redis'     => 'Redis',
        ], $config['CACHE_DRIVER']);

        // Нам нужно настроить Redis.
        if ($config['CACHE_DRIVER'] === 'redis') {
            $this->configureRedis();
        }

        $config['SESSION_DRIVER'] = $this->choice('Какой драйвер для Session вы хотите использовать?', [
            'apc'       => 'APC(u)',
            'array'     => 'Array',
            'database'  => 'Database',
            'file'      => 'File',
            'memcached' => 'Memcached',
            'redis'     => 'Redis',
        ], $config['SESSION_DRIVER']);

        // Нам нужно настроить Redis.
        if ($config['SESSION_DRIVER'] === 'redis') {
            $this->configureRedis();
        }

        $config['QUEUE_DRIVER'] = $this->choice('Какой драйвер QUEUE вы хотите использовать?', [
            'null'       => 'None',
            'sync'       => 'Synchronous',
            'database'   => 'Database',
            'beanstalkd' => 'Beanstalk',
            'sqs'        => 'Amazon SQS',
            'redis'      => 'Redis',
        ], $config['QUEUE_DRIVER']);

        // Нам нужно настроить Redis, но только если драйвер кеша не был redis.
        if ($config['QUEUE_DRIVER'] === 'redis' && ($config['SESSION_DRIVER'] !== 'redis' || $config['CACHE_DRIVER'] !== 'redis')) {
            $this->configureRedis();
        }

        // Отформатируйте настройки, чтобы отобразить их в таблице.
        $this->formatConfigsTable($config);
        if (!$this->confirm('Are these settings correct?')) {
            return $this->configureDrivers($config);
        }

        $this->writeEnv($config);
    }

    /**
     * Отформатируйте конфиги в симпатичную таблицу, которую мы легко читаем.
     *
     * @param array $config
     *
     * @return void
     */
    protected function formatConfigsTable(array $config)
    {
        $configRows = [];
        foreach ($config as $setting => $value) {
            $configRows[] = compact('setting', 'value');
        }
        $this->table(['Setting', 'Value'], $configRows);
    }


    /**
     * Настройте соединение redis.
     *
     * @return void
     */
    protected function configureRedis()
    {
        $config = [
            'REDIS_HOST'     => null,
            'REDIS_DATABASE' => null,
            'REDIS_PORT'     => null,
        ];

        $config['REDIS_HOST'] = $this->ask('Какой хост вашего сервера redis?');
        $config['REDIS_DATABASE'] = $this->ask('Какое имя базы данных должен использовать iEXExchanger?');
        $config['REDIS_PORT'] = $this->ask('Какой порт должен использовать iEXExchanger?', 6379);

        $this->writeEnv($config);
    }

    /**
     * Writes to the .env file with given parameters.
     *
     * @param $options
     * @return string
     */
    protected function writeEnv($options)
    {
        try {
            $env = new DotenvEditor();
            $env->changeEnv($options);
        } catch (DotEnvException $e) {
            return $e->getMessage();
        }
    }
}
