<?php

namespace App\Console\Commands;

use App\Models\DirectionExchange;
use App\Models\Menu;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class UpdateSitemapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление карты сайта';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = Sitemap::create()
            ->add(Url::create(config('app.url'))
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                ->setPriority(0.8));

        //Список категорий
        Menu::where('status', '=', 1)
            ->whereIn('id', explode(',', iEXSetting('seo_allows_menu')))
            ->get()->each(function (Menu $item) use ($sitemap) {
                $sitemap->add(
                    Url::create(config('app.url') . $item->slug)
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                        ->setPriority(0.8));
            });

        if(iEXSetting('seo_enabled_description_exchanges')) {
            DirectionExchange::whereNotNull('parent_url')->get()->each(function($item) use($sitemap) {
                $sitemap->add(Url::create(config('app.url').'/obmen/'.$item->parent_url)
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.8));
            });
        }

        if(iEXSetting('seo_enabled_direction_exchanges')) {
            //Список направлений
            DirectionExchange::where('status', 1)->get()->each(function($item) use($sitemap) {
                $sitemap->add(Url::create(config('app.url').'/?cur_from='.$item->currency1->designation_xml.'&cur_to='.$item->currency2->designation_xml)
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                    ->setPriority(0.8));
            });
        }

        iEXSetting(['seo_sitemap_modification' => Carbon::now()->format('c')]);
        File::put(public_path('sitemap.xml'), $sitemap->render());
    }
}
