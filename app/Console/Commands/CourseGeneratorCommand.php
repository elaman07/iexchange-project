<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CourseGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:course';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генератор курсов в файл';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(isJobOffline())
        {
            \Crypto::setSchema('xml')->empty();
            \Crypto::setSchema('txt')->empty();
            \Crypto::setSchema('json')->delete();
        } else {
            \Crypto::setSchema('xml')->build();
            \Crypto::setSchema('txt')->build();
            \Crypto::setSchema('json')->build();
        }
    }

}
