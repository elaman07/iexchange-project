<?php

namespace App\Console\Commands;

use App\Common\Telegram;
use App\Mail\NotifyReportMail;
use App\Models\ArchiveReport;
use App\Models\Currency;
use App\Models\EventReserve;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class GenerationReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генератор отчетов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $filename = Carbon::now()->format('dmYHis');

        $pdf = \PDF::loadView('_parent.pdf.reserve', [
            'currencies'    =>  Currency::active()->where('status', 0)->orderBy('status', 'asc')->get(),
            'exchange'  =>  Task::where('status', '=',4)->whereDate('created_at',  Carbon::today()->toDateString())->get(),
            'totalUSD'  =>  Currency::totalAmountUSD(),
            'events'    =>  EventReserve::whereDate('created_at', Carbon::today()->toDateTimeString())->get()
        ]);

        $item = ArchiveReport::create([
            'name'  =>  $filename.'.pdf',
            'hash_id' => hash('ripemd160', $filename)
        ]);

        $pdf->save(storage_path('app/archive/'.$filename.'.pdf'));
        if(iEXSetting('report_analytics_status') == 1 and iEXSetting('report_analytics_email')) {
            Mail::to(iEXSetting('report_analytics_email'))->send(new NotifyReportMail($item->id));
        }

        return true;
    }
}
