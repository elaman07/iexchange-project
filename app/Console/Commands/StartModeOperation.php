<?php

namespace App\Console\Commands;


use App\Events\CoursesFileEvent;
use App\Events\StatusSiteEvent;
use App\Jobs\StatusSiteOnlineJob;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class StartModeOperation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'operation:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Launching the operating mode of the operators';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        // Включаем для ручного режима
        if((int)iEXSetting('type_working_mode') == 2) {
            iEXSetting(['is_working_manual' => 0])->save();
        }

        if(iEXSetting('is_enabled_module_socket') == 1) {
            broadcast(new StatusSiteEvent(iEXContentLanguage('sitename') . ' запущен', 1));
        }

        // При запуске, сразу запускаем обновление курсов
        $this->call('compiler:courses');

        // Если уведомление включено, отправляем сообщение о запуске сайте
        if(config('telegram.enable')) {
           broadcast(new StatusSiteOnlineJob());
        }
    }
}
