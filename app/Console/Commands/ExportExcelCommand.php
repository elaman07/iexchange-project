<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Modules\ExportData\Entities\ExportData;

class ExportExcelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:excel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $items = ExportData::where('is_cron', 1)->get();

        foreach ($items as $item)
        {
            $class = '\\App\\Exports\\'.$item->export_value;
            $class_collection = $class.'Collection';
            $first = Carbon::now()->format('Y_m_d').'_'. random_int(0, 999999);
            $ext = $first.'_'.Str::studly($item->export_value).'.'.mb_strtolower($item->format_export);

            // Если существует класс экспорта
            if(class_exists($class) and class_exists($class_collection))
            {
                if($item->is_filter == 1)
                    \Excel::store(new $class(true), $ext, config('export-data.driver'), excel_type($item->format_export));
                else
                    \Excel::store(new $class_collection(), $ext, config('export-data.driver'), excel_type($item->format_export));

                // Обновляем счетчик
                $item->update(['count' => $item->count + 1]);
            }
        }
    }
}
