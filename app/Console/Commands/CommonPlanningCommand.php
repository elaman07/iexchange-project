<?php

namespace App\Console\Commands;

use App\Common\Packages\Referral\Models\ReferralLink;
use App\Common\Packages\Referral\Models\ReferralLog;
use App\Common\Packages\Referral\Models\ReferralProgram;
use App\Common\Packages\Update\Facades\UpdateClientFacade;
use App\Models\AdminLogOperation;
use App\Models\Reward;
use App\Models\RewardLog;
use App\Models\RewardProgram;
use App\Models\TaskInfo;
use App\Models\TasksRatesData;
use App\Models\TaskStatusLog;
use App\Models\TaskUser;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\File;
use App\Models\DirectionExchange;
use App\Models\Menu;
use App\Models\Task;
use App\Models\UnpaidItem;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;
use MatthiasMullie\Minify\JS;
use Spatie\Analytics\Period;

class CommonPlanningCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'common:planning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Общий обработчик дополнительных задач';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        UpdateClientFacade::checkUpdates();

        // Генерируем файл конфига
        $exists_config = public_path('project_info.txt');

        if(isset($exists_config)) {
            iex_file_delete($exists_config);
        }

        file_put_contents($exists_config, json_encode([
            'product'             => 'iEXExchanger',
            'website'             => 'exchanger.iexbase.com',
            'version'             =>  config('version.current'),
            'framework_version'   =>  app()->version(),
            'developer'           => 'iEXBase group'
        ]));


        $this->deactivationUser();
        if(iEXSetting('is_enabled_google_analytics') == 1) {
            try {
                $this->analytics();
            }catch (\Exception $exception) {

            }
        }
    }

    protected function analytics()
    {
        $this->updateGraphicApplication();
        $this->graphics_x_day(Period::days(7), 7);
        $this->graphics_x_day(Period::days(30), 30);
        $this->visitors_x_day(Period::days(7), 7);
        $this->visitors_x_day(Period::days(30), 30);
        $this->fetchTotalVisitorsAndPageViews(Period::days(7),7);
    }

    /**
     * Деактивация неактивных пользователей
    */
    protected function deactivationUser()
    {
        if((int)iEXSetting('new_user_registration_cleanup_days') > 0) {
            $day = Carbon::now()->subDays(iEXSetting('new_user_registration_cleanup_days'));
            User::whereNull('email_verified_at')->where('created_at', '<', $day)->update(['deactivation' => 1]);
        }

        if((int)config('auth.max_users_day') > 0) {
            $day = Carbon::now()->subDays(config('auth.max_users_day'));
            User::where('last_activity_at', '<', $day)->update(['deactivation' => 1]);
        }
    }


    /**
     * Строим графику по заявкам за месяц
     *
     * @return void
    */
    protected function updateGraphicApplication()
    {
        $month = Carbon::now()->subDays(30)->startOfDay();

        //Список всех записей
        $all_applications = Task::where('created_at', '>=', $month)->get()
            ->groupBy(function($item) {
                return $item->created_at->format('d.m.Y');
            })->toArray();


        $array_applications = [];
        foreach ($all_applications as $key =>  $post) {
            $array_applications[] = [Carbon::parse($key)->timestamp * 1000, count($post)];
        }

        //Список выполненных записей
        $completed_requests = Task::where('created_at', '>=', $month)->where('status', 4)->get()
            ->groupBy(function($item){
                return $item->created_at->format('d.m.Y');
            })->toArray();

        $array_completed_requests = [];
        foreach ($completed_requests as $key =>  $post) {
            $array_completed_requests[] = [Carbon::parse($key)->timestamp * 1000, count($post)];
        }

        //Список отклоненных записей
        $deleted_requests = Task::where('created_at', '>=', $month)->where('status', 5)->get()
            ->groupBy(function($item) {
                return $item->created_at->format('d.m.Y');
            })->toArray();

        $array_deleted_requests = [];
        foreach ($deleted_requests as $key =>  $post) {
            $array_deleted_requests[] = [Carbon::parse($key)->timestamp * 1000, count($post)];
        }


        $view = View::make('_parent.application',[
            'array_applications'        =>      json_encode($array_applications, JSON_NUMERIC_CHECK),
            'array_completed_requests'  =>      json_encode($array_completed_requests, JSON_NUMERIC_CHECK),
            'array_deleted_requests'    =>      json_encode($array_deleted_requests, JSON_NUMERIC_CHECK),
        ]);



        $contents = $view->render();

        $minifier = new JS($contents);

        //Перед созданием нового документа, удаляем старый
        if(File::exists(public_path('js/gapp.js'))) {
            File::delete(public_path('js/gapp.js'));
        }

        ///Записываем результат в файл
        File::put(public_path('js/gapp.js'), $minifier->minify(), FILE_APPEND);

    }


    protected function fetchTotalVisitorsAndPageViews($period, $x)
    {
        $analyticsCustom =\Analytics::fetchTotalVisitorsAndPageViews($period);

        File::put(public_path('admin-assets/statistics/fetchTotalVisitorsAndPageViews_'.$x.'d.json'),
            $analyticsCustom,
            FILE_APPEND);
    }

    protected function visitors_x_day($period, $x)
    {
        //Какие страницы просматривают пользователи
        $fetchMostVisitedPages = \Analytics::fetchMostVisitedPages($period, 10);
        //С каких сайтов идут переходы
        $fetchTopReferrers     = \Analytics::fetchTopReferrers($period, 10);
        //Посещаемость
        $fetchUserTypes       =   \Analytics::fetchUserTypes($period, 10);


        File::put(public_path('admin-assets/statistics/fetchMostVisitedPages_'.$x.'d.json'),
            $fetchTopReferrers,
            FILE_APPEND);

        File::put(public_path('admin-assets/statistics/fetchTopReferrers_'.$x.'d.json'),
            $fetchMostVisitedPages,
            FILE_APPEND);

        File::put(public_path('admin-assets/statistics/fetchUserTypes_'.$x.'d.json'),
            $fetchUserTypes,
            FILE_APPEND);
    }

    protected function graphics_x_day($period, $x)
    {
        $stat_label = [
            'title' =>  'Последние '.$x.' дней',
            'link'  =>  'Пользователей'
        ];
        $period = Period::days(7);

        //$analyticsData =\Analytics::fetchTotalVisitorsAndPageViews(Period::days(7));
        $analyticsCustom =\Analytics::fetchTotalVisitorsAndPageViews($period);
        $analyticsBrowser =\Analytics::fetchTopBrowsers($period);


        $analytics = [];

        foreach ($analyticsCustom as $value) {
            $analytics[] =  [Carbon::parse($value['date'])->timestamp * 1000, $value["visitors"]];
        }

        //Статистика по браузеру
        $array_browser = [];

        foreach ($analyticsBrowser as $browser) {
            array_push($array_browser, [
                "name"  =>  $browser['browser'],
                "y"     =>  $browser['sessions']
            ]);
        }

        $view = View::make('_parent.google', [
            'analytics'                 =>      json_encode($analytics, JSON_NUMERIC_CHECK),
            'stat_label'                =>      $stat_label,
            'array_browser'             =>     json_encode($array_browser, JSON_UNESCAPED_SLASHES),
        ]);


        $contents = $view->render();

        $minifier = new JS($contents);

        if(File::exists(public_path('admin-assets/statistics/google_'.$x.'d.js'))) {
            File::delete(public_path('admin-assets/statistics/google_'.$x.'d.js'));
        }

        ///Записываем результат в файл
        File::put(public_path('admin-assets/statistics/google_'.$x.'d.js'), $minifier->minify(), FILE_APPEND);
    }
}
