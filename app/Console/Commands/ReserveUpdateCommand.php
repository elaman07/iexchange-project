<?php

namespace App\Console\Commands;

use App\Models\Currency;
use App\Models\FileParserGroup;
use App\Models\FileParserRates;
use App\Models\Reserve;
use App\Models\ReserveFile;
use App\Models\ReserveFileGroup;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\WithdrawalRequest;
use Illuminate\Console\Command;

class ReserveUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reserve:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление файлов резерва';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        if(iEXSetting('is_enabled_reserves_from_file') == 1)
        {
            // Обновление файла резервов
            ReserveFileGroup::where('status', '=', 1)->get()->map(function($item)
            {
                $handle = trim(file_get_contents($item->link));
                $yield = collect(
                    preg_split("/\r\n|\n|\r/", $handle)
                )->map(function($filter) {

                    $filter_value = explode(':', $filter);
                    return [
                        'name' => trim($filter_value[0]),
                        'amount' => trim($filter_value[1])
                    ];
                })->pluck('amount','name')->toArray();

                ReserveFile::where([
                    ['id_group', '=', $item->id],
                    ['status', '=', 1]
                ])->chunk(100, function($parsers) use($yield, $item)
                {
                    foreach ($parsers as $parser)
                    {
                        $find_parser = $yield[$parser->name];
                        if(empty($find_parser))
                            throw new \Exception('В файле '.$item->name.' не найдена пара '.$parser->name);

                        //Обновление данных в базе
                        ReserveFile::where('id', '=', $parser->id)
                            ->update([
                                'amount' => $find_parser
                            ]);
                    }
                });

            });
            // Обновление самих резервов
            $reserves = Reserve::where('id_file_reserve', '>', 0)->get();
            // Получение всех курсов из файла
            $file_reserve = ReserveFile::where('status', '=', 1)->get()->pluck('amount','id')->toArray();

            foreach ($reserves as $item) {
                if(isset($file_reserve[$item->id_file_reserve]))
                {
                    $item->update([
                        'summa' => $file_reserve[$item->id_file_reserve]
                    ]);
                }
            }
        }

        if(iEXSetting('is_enabled_reserves_from_server') == 1)
        {
            $reserves = Reserve::whereNotNull('id_server_reserve')->get()->map(function($item) {
                if((int)$item['id'] > 0) {
                    $explode =  explode('_', $item['id_server_reserve']);
                    return ['id' => $explode[0].'_'.$explode[1]];
                }
            })->groupBy('id')->keys()->toArray();

            $array_balances = [];
            foreach ($reserves as $value) {
                $find_data = explode('_', $value);
                $balances = \App\Common\Packages\Payment\PaymentFacade::pay($find_data[0], $find_data[1])->api()->getAllBalances();
                $array_balances[$value] = $balances;
            }

            $reserves_update = Reserve::whereNotNull('id_server_reserve')->get();
            foreach ($reserves_update as $item)
            {
                $explode =  explode('_', $item['id_server_reserve']);

                $item->update([
                    'summa' => $array_balances[sprintf('%s_%s',$explode[0],$explode[1])][\Str::upper($explode[2])]
                ]);
            }
        }
    }
}
