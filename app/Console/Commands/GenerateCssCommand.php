<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use MatthiasMullie\Minify\CSS;

class GenerateCssCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:css';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generator CSS file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $view = View::make('_parent.css');
        $contents = $view->render();

        $minifier = new CSS($contents);
        //Перед созданием нового документа, удаляем старый
        if(File::exists(public_path('css/colors.css'))) {
            File::delete(public_path('css/colors.css'));
        }

        iEXSetting([
            'color_style_hash' =>  Carbon::now()->format('c')
        ])->save();

        ///Записываем результат в файл
        File::put(public_path('css/colors.css'), $minifier->minify(), FILE_APPEND);
    }
}
