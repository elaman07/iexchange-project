<?php

namespace App\Console\Commands;

use App\Models\GroupParserExchange;
use App\Models\LogAutoPayment;
use App\Models\LogCheckPay;
use App\Models\ParserExchange;
use App\Models\ReferralStatistics;
use App\Models\TaskLogConfirmation;
use App\Models\TaskStatusLog;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GroupParserUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iex:parser_reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление курсов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Вы действительно хотите обновить группы курсов? (Внимание: Будут сброшены все группы парсеров)', true))
        {
            $this->comment('Выполняется обновление групп курсов...');

            $parser = GroupParserExchange::find(3);

            if((int)iEXSetting('is_group_parser_updating') != 1)
            {
                DB::table('group_parser_exchange')->delete();
                $max = DB::table('group_parser_exchange')->max('id') + 1;
                DB::statement("ALTER TABLE group_parser_exchange AUTO_INCREMENT =  $max");

                iEXSetting(['is_group_parser_updating' => 1])->save();
            }


            $parser_file = json_decode(file_get_contents(storage_path('group_parsers.json')), true);
            foreach ($parser_file as $value)
            {
                GroupParserExchange::updateOrCreate([
                    'alias' => $value['alias'],
                ], [
                    'id'    =>  $value['id'],
                    'alias' => $value['alias'],
                    'name' => $value['name'],
                    'type_amount' => $value['type_amount'],
                    'sorting' => $value['sorting'],
                    'priority' => $value['priority'] ?: 0,
                    'provider_url' => $value['provider_url']
                ]);
            }

            GroupParserExchange::find(3)->update(['provider_id' => 'coinmarketcap']);
            GroupParserExchange::find(32)->update(['provider_id' => 'fixerapi']);
            GroupParserExchange::find(33)->update(['provider_id' => 'fixerapi']);
            GroupParserExchange::find(34)->update(['provider_id' => 'fixerapi']);


            $this->comment('Группы успешно обновлены');
        }

    }
}
