<?php

namespace App\Console\Commands;

use App\Common\Packages\Transaction\Facades\TransactionFacade;
use App\Models\Task;
use Illuminate\Console\Command;

class ProcessPaymentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitoring:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяем средства в фоновом режиме';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        // Каждую минуту проверяем
        $task_where = [
            ['status', 2],
            ['is_autopay_modal', 1]
        ];
        $tasks = Task::where($task_where)->limit(5)->cursor();

        // Список актуальных заявок
        foreach ($tasks as $task)
        {
            if($task->is_autopay_limit > 1) {
                \Log::error('Отказано в повторной проверке '.$task->id);
                continue;
            }

            $this->info('Проверка поступлений по заявки №'.$task->id);
            $transaction = TransactionFacade::init($task);
            if($transaction->hasTimeOutCheckPayment()) {
                $transaction->failed();
                continue;
            }

            // Если нет проблем, проверяем данные на сервере
            if($transaction->processCheckedOrder() == true) {
                $transaction->handlerOrderFromCron();
            }

            // Очистить данные
            $transaction->clear_init();
        }
    }
}
