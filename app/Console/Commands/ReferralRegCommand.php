<?php

namespace App\Console\Commands;

use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\Referral\Models\ReferralLink;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Models\Currency;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\WithdrawalRequest;
use Illuminate\Console\Command;

class ReferralRegCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Автовывод реферальных бонусов и кэшбэк`а';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Список пользователей которые включили систему автовывода
        $users = User::all();
        foreach ($users as $user) {
            $exists = ReferralLink::where('user_id', '=', $user->id)->first();
            if (!$exists) {
                //Регистрация реферала
                ReferralSystemFacade::register($user, request()->cookie('ref'));
                //Регистрируем бонусную программу
                CashbackFacade::register($user->id);

            }
        }
    }
}
