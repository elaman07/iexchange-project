<?php

namespace App\Console\Commands;

use App\Common\Packages\Pages\Models\Page;
use App\Common\Packages\Pages\Pages;
use App\Models\FaqCategory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class GeneratorPagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:pages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация все необходимых странц';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->static_pages();
        $this->faq();
    }


    /***
     * Генерация вопросов и ответов
     *
     * @return void
    */
    protected function faq()
    {
        $faq = [];

        foreach (FaqCategory::all() as $faq_category)
        {
            $category = [];
            $category['name'] = $faq_category->name;
            $category['faq'] = [];
            foreach ($faq_category->faq as $item)
            {
                array_push($category['faq'], [
                    'id'    =>  $item->id,
                    'title' =>  $item->title,
                    'text'  =>  $item->text
                ]);
            }

            array_push($faq, $category);
        }

        $this->toFile('faq',null, $faq);
    }

    /**
     * Генерация статический страниц
     *
     * @return void
    */
    protected function static_pages()
    {
        $array_search = [
            '{sitename}',
            '{sitename_desc}',
            '{url}'
        ];
        $array_value = [
            iEXContentLanguage('sitename'),
            iEXContentLanguage('sitename_desc'),
            config('app.url')
        ];

        foreach (Page::all() as $value)
        {
            if(Pages::pageExists($value->page_slug)) {
                $pageData = Pages::getPage($value->page_slug);

                // Найти и заменить
                $content = str_replace($array_search, $array_value, $pageData->page_content);

                $this->toFile($value->page_slug,'pages', [
                    'title'    =>  $pageData->page_title,
                    'page_content'  =>  $content
                ]);

            }
        }
    }

    /**
     * Записываем результаты в файл
     *
     * @param $name
     * @param $path
     * @param $content
     * @return void
     */
    protected function toFile($name, $path, $content)
    {
        if(isset($path)) {
            File::put(public_path('elements/'.$path.'/'.$name.'.json'),  json_encode($content));
        }else {
            File::put(public_path('elements/'.$name.'.json'),  json_encode($content));
        }
    }
}
