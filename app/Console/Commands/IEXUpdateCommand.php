<?php

namespace App\Console\Commands;

use App\Common\Packages\Cashback\Facades\CashbackFacade;
use App\Common\Packages\ReferralSystem\Models\ReferralLog;
use App\Common\Packages\ReferralSystem\ReferralSystemFacade;
use App\Common\Packages\Update\Facades\UpdateClientFacade;
use App\Exports\CurrencyExport;
use App\Models\AdminDesktop;
use App\Models\AdminDesktopGadget;
use App\Models\CompetitorRates;
use App\Models\Contact;
use App\Models\Currency;
use App\Models\CurrencyAnalytics;
use App\Models\DirectionExchange;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\FileParserRates;
use App\Models\FilterCurrency;
use App\Models\Gateway;
use App\Models\GatewayMerchant;
use App\Models\GatewayPayment;
use App\Models\GeoCountryList;
use App\Models\GroupParserExchange;
use App\Models\IEXScriptConfig;
use App\Models\LinksReview;
use App\Models\LinksReviewGroup;
use App\Models\Merchant;
use App\Models\News;
use App\Models\NoticeExchange;
use App\Models\ParserApiKey;
use App\Models\ParserExchange;
use App\Models\Payment;
use App\Models\PendingOrderStatus;
use App\Models\Task;
use App\Models\TaskRejectionStatus;
use App\Models\TaskStatus;
use App\Models\TemplateTypeEvent;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\WithdrawalRequest;
use Brotzka\DotenvEditor\DotenvEditor;
use Carbon\Carbon;
use Defuse\Crypto\Crypto;
use Illuminate\Auth\Access\Gate;
use Illuminate\Console\Command;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class IEXUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iex:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function handle()
    {
        $this->comment('* Обновление данных в базе MYSQL');
        $this->call('migrate', [
            '--force' => true
        ]);
        $this->comment('-------------------------------');
        $this->comment('* Обновление данных');


        // Обновление кошельков
        $wallets = WithdrawalRequest::where('status', '=', 1)->groupBy('score')->get();
        foreach ($wallets as $wallet) {
            \App\Models\WithdrawalWallets::updateOrCreate([
                'wallet' => $wallet->score,
                'id_currency' => $wallet->id_currency
            ], [
                'id_user' => $wallet->id_user,
                'wallet' => $wallet->score,
                'id_currency' => $wallet->id_currency
            ]);
        }


        // Обновление баланса
        $balances = UserBalance::select('id', 'id_user', 'balance')->where('balance', '>', 0)->get();

        foreach ($balances as $balance) {
            $referrals_total_profit = ReferralLog::with(['user_admin','user_admin.user_balance'])
                ->select('id_user')
                ->where('referral_log.id_user', $balance->id_user)
                ->selectRaw("count(*) as count_num")
                ->selectRaw("sum(`bonus_number`) as total_amount")
                ->first();

            $total_sum = WithdrawalRequest::where('id_user', $balance->id_user)->where('status', '=', 1)->sum('base_referral');

            $balance->update([
                'referral_total_profit' => $referrals_total_profit['total_amount'],
                'referral_total_withdrawal' => $total_sum
            ]);
        }

        // Проверка папок
        $required_fields = ['advantage', 'contact', 'links', 'news', 'svg', 'uploads', 'payment_systems'];
        foreach ($required_fields as $required_field) {
            if(!\File::isDirectory(public_path('/storage/'.$required_field))) {
                File::makeDirectory(public_path('/storage/'.$required_field), 0777, true);
            }
        }

        // Перевести параметры в мультиязычность
        $default_params = DB::table('filter_currency')->get()->keyBy('id')->toArray();
        $filter_currency = FilterCurrency::all();
        foreach ($filter_currency as $item)
        {
            if(empty($item->name)) {
                $item->update([
                    'name' => ['ru' => $default_params[$item->id]->name], ['en' => $default_params[$item->id]->name]
                ]);
            }
        }


        // Обновление статусов
        $get_task_statuses = collect(json_decode(file_get_contents(storage_path('/task_status.json')), true))->keyBy('id')->toArray();
        foreach (TaskStatus::all() as $status)
        {
            if(empty($status->name) and isset($get_task_statuses[$status->id]))
            {
                $status->name = $get_task_statuses[$status->id]['name'];
                $status->save();
            }
        }

        // Обновление причин
        $task_pending_status = collect(json_decode(file_get_contents(storage_path('/task_pending_status.json')), true))->keyBy('id')->toArray();
        foreach (PendingOrderStatus::all() as $status)
        {
            if(empty($status->name) and isset($task_pending_status[$status->id]))
            {
                $status->name = $task_pending_status[$status->id]['name'];
                $status->save();
            }
        }

        // Обновление причин
        $task_reject_status = collect(json_decode(file_get_contents(storage_path('/task_reject_status.json')), true))->keyBy('id')->toArray();
        foreach (TaskRejectionStatus::all() as $status)
        {
            if(empty($status->name) and isset($task_reject_status[$status->id]))
            {
                $status->name = $task_reject_status[$status->id]['name'];
                $status->save();
            }
        }

        // Обновление настроек страницы
        if(empty(iEXSetting('admin_currency_hidden_columns'))) {
            iEXSetting(['admin_currency_hidden_columns' => 'currency,code_currency,xml,merchant,payment,reserve,receiving,sending,number_format,status'])->save();
        }

        if(empty(iEXSetting('admin_code_hidden_columns'))) {
            iEXSetting(['admin_code_hidden_columns' => 'code_currency,symbol,pegged_currencies,reserve,course,auto_currect'])->save();
        }

        if(empty(iEXSetting('admin_direction_hidden_columns'))) {
            iEXSetting(['admin_direction_hidden_columns' => 'direction_name,course,auto_currect,curs_bestchange,profit,status'])->save();
        }

        if(empty(iEXSetting('admin_user_hidden_columns'))) {
            iEXSetting(['admin_user_hidden_columns' => 'name,email,count_exchange,balance,ip_address_browser,created_at'])->save();
        }

        if(empty(iEXSetting('admin_merchant_hidden_columns'))) {
            iEXSetting(['admin_merchant_hidden_columns' => 'name,alias,settings,security,pegged_currencies,count_order,status'])->save();
        }

        // Обновление настроек страницы
        if(empty(iEXSetting('admin_parser_formula_hidden_columns'))) {
            iEXSetting(['admin_parser_formula_hidden_columns' => 'title,formula,course,pegged_currencies,created_at,updated_at,status'])->save();
        }



        DB::table('direction_exchange')->whereNull('oth_comm_percent')->update(['oth_comm_percent' => 0]);
        DB::table('direction_exchange')->whereNull('oth_comm_currency')->update(['oth_comm_currency' => 0]);
        Schema::dropIfExists('job_settings');

//        DB::table('iex_script_config')->truncate();
//        if(IEXScriptConfig::count() == 0)
//        {
//            $json_to_array = json_decode(File::get(config('setting.json.path')), true);
//            foreach ($json_to_array as $key => $value)
//            {
//                DB::table('iex_script_config')->insert([
//                    'key' => $key,
//                    'value' => $value
//                ]);
//            }
//        }


        $this->gateways();
        $this->rules();
        /// .env
        $this->initEnv();
        $this->mailTemplates();

        // Создаем пользователь "Guest"
        $is_guest = User::where('is_guest', 1)->count();
        if($is_guest == 0) {
            $user =  User::create([
                'is_guest'          =>  1,
                'name'              =>  'Guest',
                'email'             =>  'guest@test.com',
                'password'          =>  Hash::make(Str::random()),
                'ip_address'        =>  '127.0.0.1', // Local IP
                'last_login'        =>  \Illuminate\Support\Carbon::now(),
                'last_activity'     =>  Carbon::now(),
            ]);

            //Регистрация реферала
            ReferralSystemFacade::register($user, null);
            //Регистрируем бонусную программу
            CashbackFacade::register($user->id);
        }


        Permission::updateOrCreate([
            'name' => 'admin_api'
        ], [
            'name' => 'admin_api',
            'title' => 'Разрешить доступ к API в админпанели',
            'text' => 'Данная опция позволит пользователям, имеющим доступ в админпанель, конторолировать функции API',
            'id_permissions_group' => 3,
        ]);

        // Обновить тех. название
        $currencies_name = Currency::has('payment')->has('code_currency')->whereNull('tech_name')->get();
        foreach ($currencies_name as $item) {
            $item->update(['tech_name' => $item->payment->name.' '.$item->code_currency->name]);
        }


        $currentKey = $this->laravel['config']['payment.password'];
        if (strlen($currentKey) == 0)
        {
            $escaped = preg_quote('='.$this->laravel['config']['payment.password'], '/');
            file_put_contents($this->laravel->environmentFilePath(), preg_replace(
                "/^APP_PAYMENT_PASSWORD{$escaped}/m",
                'APP_PAYMENT_PASSWORD='.Str::random(20),
                file_get_contents($this->laravel->environmentFilePath())
            ));
        }

        // По умолчнию загружаем анализ сайта
        if((int)iEXSetting('is_insert_widget_perfmon_panel') != 1)
        {
            $desktops = AdminDesktop::all();
            foreach ($desktops as $desktop)
            {
                if($desktop->gadgets->where('alias', '=', 'PerfmonPanelWidget')->count() > 0) {
                    continue;
                }

                AdminDesktopGadget::updateOrCreate([
                    'id_desktop' => $desktop->id,
                    'alias' => 'PerfmonPanelWidget',
                ], [
                    'id_desktop' => $desktop->id,
                    'name' => 'Анализ сайта',
                    'alias' => 'PerfmonPanelWidget',
                    'sorting' => 0,
                    'column_id' => 'column0',
                    'id_user' => $desktop->id_user,
                    'hash_id' => 'f9404289e28f00b3b570831e666c93c03fb3f0ba'
                ]);
            }

            iEXSetting(['is_insert_widget_perfmon_panel' => 1])->save();
        }

        $competitors_exchanges = CompetitorRates::whereNull('code')->get();
        foreach ($competitors_exchanges as $rate)
        {
            if(isset($rate->competitor_link) and !empty($rate->competitor_link->alias)) {
                $rate->update([
                    'code' => '[competitors_'.\Str::lower(\Str::studly($rate->competitor_link->name)).'_'.\Str::lower($rate->exchange_in.'-'.$rate->exchange_out).']'
                ]);
            }
        }

        $file_parser_exchanges = FileParserRates::whereNull('code')->get();
        foreach ($file_parser_exchanges as $rate)
        {
            if(isset($rate->file_parser_group) and !empty($rate->file_parser_group->alias)) {
                $rate->update([
                    'code' => '[fileparser_'.\Str::lower(Str::studly($rate->file_parser_group->name)).'_'.\Str::lower($rate->exchange_in.'-'.$rate->exchange_out).']'
                ]);
            }
        }

        $parser_exchanges = ParserExchange::whereNull('code')->get();
        foreach ($parser_exchanges as $parser_exchange)
        {
            if(isset($parser_exchange->group_parse_exchange) and !empty($parser_exchange->group_parse_exchange->alias)) {
                $parser_exchange->update([
                    'code' => '['.\Str::lower($parser_exchange->group_parse_exchange->alias).'_'.\Str::lower($parser_exchange->code_in.'-'.$parser_exchange->code_out).']'
                ]);
            }
        }

        // Очистка кэша
        $this->call('view:clear');
        $this->line('Начинаем обновление iEXExchanger...');
        UpdateClientFacade::checkUpdates();
        $this->info('iEXExchanger обновление (актуальная версия '.config('version.current').') ⚡');
    }


    /**
     * env ключи
     *
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    protected function initEnv()
    {
        $envKeys =  json_decode(file_get_contents(storage_path('/default_env.txt'), true));

        $dot_env = new DotenvEditor();
        $dot_env->createBackup();
        $filter_array = [];
        foreach ($envKeys as $envKey => $envValue) {
            if(!$dot_env->keyExists($envKey)) {
                $filter_array[$envKey] = $envValue;
            }
        }

        if(!empty($filter_array)) {
            $dot_env->addData($filter_array);
        }

        $env_data = collect(json_decode($dot_env->getAsJson(), true))->pluck('', 'key');
        $formatting_data = [];
        foreach ($env_data->diffKeys($envKeys)->toArray() as $diffKey => $values) {
            $formatting_data[] = $diffKey;
        }

        $dot_env->deleteData($formatting_data);
    }

    protected function rules()
    {
        foreach (json_decode(file_get_contents(storage_path('geo_country_list.json')), true) as $value)
        {
            GeoCountryList::updateOrCreate([
                'code' => $value['code'],
            ], [
                'code' => $value['code'],
                'value' => $value['value'],
            ]);
        }

        $imports = collect(json_decode(file_get_contents(storage_path('/geo_country_list.json')), true))->pluck('value','code');

        foreach (GeoCountryList::all() as $item)
        {
            if(empty($item->value) and isset($imports[$item->code]))
            {
                $item->setTranslation('value', 'ru', $imports[$item->code]);
                $item->save();
            }
        }
    }

    protected function gateways()
    {
        $this->comment('* Добавление/Обновление мерчантов или выплат');

        foreach (json_decode(file_get_contents(storage_path('gateways.json')), true) as $value)
        {
            Gateway::updateOrCreate([
                'alias' => $value['alias'],
                'name' => $value['name'],
                'class_name' => $value['class_name']
            ], [
                'alias' => $value['alias'],
                'name' => $value['name'],
                'class_name' => $value['class_name'],
                'is_merchant' => $value['is_merchant'],
                'is_pay' => $value['is_pay'],
                'is_rpc' => $value['is_rpc'],
                'is_check_pay' => $value['is_check_pay'],
                'version' => $value['version'],
                'security_options'  =>  $value['security_options']
            ]);
        }
    }


    protected function mailTemplates()
    {
        foreach (json_decode(file_get_contents(storage_path('event_types_mail.json')), true) as $value)
        {
            TemplateTypeEvent::updateOrCreate([
                'event_name' => $value['event_name']
            ], [
                'event_name' => $value['event_name'],
                'event_type' => $value['event_type'],
                'name' => $value['name'],
                'description' => $value['description'],
                'id_user' => $value['id_user']
            ]);
        }

        foreach (json_decode(file_get_contents(storage_path('group_parsers.json')), true) as $value)
        {
            GroupParserExchange::updateOrCreate([
                'alias' => $value['alias']
            ], [
                'alias' => $value['alias'],
                'name' => $value['name'],
                'type_amount' => $value['type_amount'],
                'provider_url' => $value['provider_url'],
                'provider_id' => $value['provider_id']
            ]);
        }
    }
}
