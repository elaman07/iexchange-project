<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 14.02.2018, 23:12
 */

namespace App\Repositories;


interface RepositoryInterface {

    public function all($columns = ['*']);

    public function paginate($perPage = 15, $columns = ['*']);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function find($id, $columns = ['*']);

    public function findBy($field, $value, $columns = ['*']);
}