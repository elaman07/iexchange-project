<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 14.02.2018, 23:13
 */

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    /**
     * model property on class instances
    */
    protected $model;

    /**
     * Constructor to bind model to repo
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}