<?php
/**
 * Новый файл без названия
 * Автор: Шамсудин
 * Дата и Время: 14.02.2018, 23:15
 */

namespace App\Repositories;


use App\Models\Reserve;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Cache\Repository as Cache;

class ReserveRepository extends Repository
{
    public function __construct(Reserve $model)
    {
        parent::__construct($model);
    }

    public function all()
    {
        $array = [];
        foreach ($this->model->get() as $item)
        {
            array_push($array, [
                'id'    =>  $item->id,
                ''
            ]);
        }
    }
}