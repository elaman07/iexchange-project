<?php
/**
 * Created by PhpStorm.
 * User: steei
 * Date: 23.10.2019
 * Time: 20:21
 */

namespace App\Exports;


use App\Models\BackupCodeModel;
use App\Models\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BackupCodesExport implements FromQuery, WithHeadings
{
    use Exportable;

    /**
     * Информация о пользователе
     *
     * @var integer
    */
    protected $user;

    public function __construct(int $id) {
        $this->user = $id;
    }

    /**
     * Запрос на получение кодов
    */
    public function query() {
        return BackupCodeModel::query()->select('num', 'code')->where([
            ['id_user' , '=', $this->user]
        ]);
    }

    /**
     * Отправляем заголовки колонок
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Номер',
            'Код'
        ];
    }
}