<?php
namespace App\Exports;


use App\Models\Currency;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class CurrencyExport implements FromQuery
{
    use Exportable;

    protected $is_filter = false;

    public function __construct(bool $is_filter = false) {
        $this->is_filter = $is_filter;

        
    }

    public function filter()
    {
        return [
            ['status', 1]
        ];
    }

    public function query() {
        if($this->is_filter) {
            return Currency::query()->where($this->filter());
        }

        return Currency::query()->get();
    }
}