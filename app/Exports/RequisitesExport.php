<?php
namespace App\Exports;


use App\Models\Currency;
use App\Models\DirectionExchange;
use App\Models\Requisites;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class RequisitesExport implements FromQuery
{
    use Exportable;

    protected $is_filter = false;

    public function __construct(bool $is_filter = false) {
        $this->is_filter = $is_filter;
    }

    public function filter()
    {
        return [
            ['is_history', '!=', 1]
        ];
    }

    public function query() {
        if($this->is_filter) {
            return Requisites::query()->where($this->filter());
        }

        return Requisites::query()->get();
    }
}