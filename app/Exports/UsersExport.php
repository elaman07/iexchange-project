<?php
namespace App\Exports;


use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Spatie\Permission\Models\Role;

class UsersExport implements FromQuery
{
    use Exportable;

    protected $is_filter = false;

    public function __construct(bool $is_filter = false) {
        $this->is_filter = $is_filter;
    }

    public function filter()
    {
        $allowRoles = Role::where('is_export', 1)->get()->map(function($item) {
            return $item->name;
        })->toArray();

        return $allowRoles;
    }

    public function query() {
        if($this->is_filter) {
            return User::query()->role($this->filter());
        }
        return User::query()->get();
    }
}