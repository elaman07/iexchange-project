<?php
namespace App\Exports;


use App\Models\Requisites;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class RequisitesExportCollection implements FromCollection
{
    use Exportable;

    /**
     * @return Collection
     */
    public function collection() {
        return Requisites::all();
    }
}