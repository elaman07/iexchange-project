<?php
namespace App\Exports;


use App\Models\Currency;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class CurrencyExportCollection implements FromCollection
{
    use Exportable;

    /**
     * @return Collection
     */
    public function collection() {
        return Currency::all();
    }
}