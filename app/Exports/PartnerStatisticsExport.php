<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PartnerStatisticsExport implements FromView
{
    use Exportable;

    protected $user;

    public function __construct($id_user) {
        //$this->id_user = $id_user;
    }

    public function view(): View
    {
        return view('exports.partner_statistics');
    }
}
