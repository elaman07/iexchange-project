<?php
namespace App\Exports;


use App\Models\DirectionExchange;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class DirectionExportCollection implements FromCollection
{
    use Exportable;

    /**
     * @return Collection
     */
    public function collection() {
        return DirectionExchange::all();
    }
}