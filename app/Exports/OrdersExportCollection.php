<?php
namespace App\Exports;


use App\Models\Task;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrdersExportCollection implements FromCollection
{
    use Exportable;

    /**
     * @return Collection
     */
    public function collection() {
        return Task::all();
    }
}