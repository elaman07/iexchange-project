<?php
namespace App\Exports;


use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersFilterExport implements FromQuery, WithHeadings
{
    use Exportable;

    /**
     * Request data
     *
     * @var Request
    */
    protected $request;

    /**
     * Список колонок для экпорта
     *
     * @var array
    */
    protected $column = [
        'id' => 'ID',
        'name' => 'Имя',
        'email' => 'Email',
        'created_at' => 'Дата регистрации',
        'updated_at' => 'Дата последнего посещения'
    ];

    /**
     * Конструктор
     *
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Фильтруем данные перед экспортированием
     *
     * @return \App\Models\User
    */
    public function query() {
        return User::query()->select($this->request->get('columns'))->filter($this->request->all());
    }

    /**
     * Отправляем заголовки колонок
     *
     * @return array
    */
    public function headings(): array
    {
        return collect($this->request->get('columns'))->map(function($item) {
            return $this->column[$item];
        })->toArray();
    }
}