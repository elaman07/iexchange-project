<?php
namespace App\Exports;


use App\Models\Currency;
use App\Models\DirectionExchange;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class DirectionExport implements FromQuery
{
    use Exportable;

    protected $is_filter = false;

    public function __construct(bool $is_filter = false) {
        $this->is_filter = $is_filter;
    }

    public function filter()
    {
        return [
            ['status', '!=', 2]
        ];
    }

    public function query() {
        if($this->is_filter) {
            return DirectionExchange::query()->where($this->filter());
        }

        return DirectionExchange::query()->get();
    }
}