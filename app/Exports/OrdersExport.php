<?php
namespace App\Exports;

use App\Models\Task;
use App\Models\TaskStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class OrdersExport implements FromQuery
{
    use Exportable;

    protected $is_filter = false;

    public function __construct(bool $is_filter = false) {
        $this->is_filter = $is_filter;
    }

    public function filter()
    {
        return TaskStatus::where('is_export', 1)->get()->map(function($item) {
            return $item->id;
        })->toArray();
    }

    public function query() {
        if($this->is_filter) {
            return Task::query()->whereIn('status', $this->filter());
        }

        return Task::query()->get();
    }
}