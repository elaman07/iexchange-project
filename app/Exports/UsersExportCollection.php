<?php
namespace App\Exports;


use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExportCollection implements FromCollection
{
    use Exportable;

    /**
     * @return Collection
     */
    public function collection() {
        return User::all();
    }
}