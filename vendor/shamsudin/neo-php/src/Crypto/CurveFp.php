<?php
namespace NeoPHP\Crypto;

use ErrorException;

if (!defined("USE_EXT")) {
    if (extension_loaded('gmp')) {
        define("USE_EXT", "GMP");
    } elseif (extension_loaded('bcmath')) {
        define("USE_EXT", "BCMATH");
    }
}

class gmp_Utils {

    public static function gmp_mod2($n, $d) {
        if (extension_loaded('gmp') && USE_EXT=='GMP') {
            $res = gmp_div_r($n, $d);
            if (gmp_cmp(0, $res) > 0) {
                $res = gmp_add($d, $res);
            }
            return gmp_strval($res);
        } else {
            throw new Exception("PLEASE INSTALL GMP");
        }
    }

    public static function gmp_random($n) {
        if (extension_loaded('gmp') && USE_EXT=='GMP') {
            $random = gmp_strval(gmp_random());
            $small_rand = rand();
            while (gmp_cmp($random, $n) > 0) {
                $random = gmp_div($random, $small_rand, GMP_ROUND_ZERO);
            }

            return gmp_strval($random);
        } else {
            throw new Exception("PLEASE INSTALL GMP");
        }
    }

    public static function gmp_hexdec($hex) {
        if (extension_loaded('gmp') && USE_EXT=='GMP') {
            $dec = gmp_strval(gmp_init($hex, 16), 10);

            return $dec;
        } else {
            throw new Exception("PLEASE INSTALL GMP");
        }
    }

    public static function gmp_dechex($dec) {
        if (extension_loaded('gmp') && USE_EXT=='GMP') {
            $hex = gmp_strval(gmp_init($dec, 10), 16);

            return $hex;
        } else {
            throw new Exception("PLEASE INSTALL GMP");
        }
    }

}
    

class CurveFp
{
    protected $a = 0;

    protected $b = 0;

    protected $prime = 0;

    public function __construct($prime, $a, $b)
    {
        $this->a = $a;
        $this->b = $b;
        $this->prime = $prime;
    }

    public function contains($x, $y)
    {
        $eq_zero = null;
        if (extension_loaded('gmp') && USE_EXT == 'GMP') {
            $eq_zero = gmp_cmp(gmp_Utils::gmp_mod2(gmp_sub(gmp_pow($y, 2), gmp_add(gmp_add(gmp_pow($x, 3), gmp_mul($this->a, $x)), $this->b)), $this->prime), 0);
            if ($eq_zero == 0) {
                return true;
            } else {
                return false;
            }
        } elseif (extension_loaded('bcmath') && USE_EXT == 'BCMATH') {
            $eq_zero = bccomp(bcmod(bcsub(bcpow($y, 2), bcadd(bcadd(bcpow($x, 3), bcmul($this->a, $x)), $this->b)), $this->prime), 0);
            if ($eq_zero == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new ErrorException("Please install BCMATH or GMP");
        }
    }

    public function getA()
    {
        return $this->a;
    }

    public function getB()
    {
        return $this->b;
    }

    public function getPrime()
    {
        return $this->prime;
    }

    public static function cmp(CurveFp $cp1, CurveFp $cp2)
    {
        $same = null;
        if (extension_loaded('gmp') && USE_EXT == 'GMP') {
            if (gmp_cmp($cp1->a, $cp2->a) == 0 && gmp_cmp($cp1->b, $cp2->b) == 0 && gmp_cmp($cp1->prime, $cp2->prime) == 0) {
                return 0;
            } else {
                return 1;
            }
        } elseif (extension_loaded('bcmath') && USE_EXT == 'BCMATH') {
            if (bccomp($cp1->a, $cp2->a) == 0 && bccomp($cp1->b, $cp2->b) == 0 && bccomp($cp1->prime, $cp2->prime) == 0) {
                return 0;
            } else {
                return 1;
            }
        } else {
            throw new \ErrorException("Please install BCMATH or GMP");
        }
    }
}
