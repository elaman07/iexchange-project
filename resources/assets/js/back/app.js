import 'babel-polyfill';

window.jQuery = window.$ = require('jquery');


if (document.querySelector('blender-chart')) {
    require.ensure([], () => {
        require('./modules/chart').default();
    }, 'back.chart');
}

// if (document.querySelector('[data-editor]')) {
//     require.ensure([], () => {
//         require('./modules/editor').default();
//     }, 'back.editor');
// }