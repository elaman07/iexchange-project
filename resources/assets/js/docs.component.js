angular.module('DocsApp.Components', [])

    .component('stnThumbnail', {
        bindings: {
            id: '='
        }
    })

    .component('oauthClient', {
        templateUrl: '/partials/oauth/client.html',
        controller: ['$scope','$mdDialog','$mdToast','$http','$window',
            function ($scope, $mdDialog, $mdToast, $http, $window)
            {
                $scope.clients = [];

                $scope.createForm = {
                    errors      :   [],
                    name        :   '',
                    redirect    :   '',
                    description :   '',
                    website     :   ''
                };

                $scope.editForm = {
                    errors      :   [],
                    name        :   '',
                    redirect    :   '',
                    description :   '',
                    website     :   ''
                };

                $scope.getClient = function()
                {
                    $scope.loadingProcess = true;

                    $http.get('/oauth/clients').then(function successCallback(response)
                    {
                        $scope.clients = response.data;
                        $scope.loadingProcess = false;
                    }, function errorCallback(response) {
                        $scope.loadingProcess = false;
                    });
                }

                //init
                $scope.getClient();


                //Создание нового ключа
                $scope.showCreateClientForm = function ($event)
                {
                    $mdDialog.show
                    (
                        {
                            scope: $scope,
                            clickOutsideToClose: false,
                            preserveScope: true,
                            templateUrl:  '/partials/oauth/create_form.html',
                            targetEvent: $event,
                            parent:     angular.element(document.body),
                            controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window)
                            {
                                //Выход
                                $scope.hide = function()
                                {
                                    $mdDialog.hide();
                                };

                                $scope.cancel = function() {
                                    $mdDialog.cancel();
                                };

                                $scope.answer = function(answer) {
                                    $mdDialog.hide(answer);
                                };


                                $scope.store = function($event)
                                {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content('Пожалуйста, подождите')
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );

                                    $scope.getClient();

                                    $http({
                                        method        :       'POST',
                                        dataType      :       'JSON',
                                        url           :        '/oauth/clients',
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                                        transformRequest: function(obj)
                                        {
                                            var str = [];
                                            for(var p in obj)
                                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                            return str.join("&");
                                        },
                                        data: {
                                            name:          $scope.createForm.name,
                                            redirect:      $scope.createForm.redirect,
                                            description:   $scope.createForm.description,
                                            website:       $scope.createForm.website,
                                            errors:     []
                                        }
                                    }).then(function successCallback(response)
                                    {
                                        $window.location.reload();
                                    }, function errorCallback(response)
                                    {
                                        console.log(angular.toJson(response.data))


                                        if (typeof response.data === 'object')
                                        {
                                            $scope.createForm.errors = _.flatten(_.toArray(response.data));

                                        } else {
                                            $scope.createForm.errors = ['Что-то пошло не так. Пожалуйста, попробуйте еще раз.'];
                                        }

                                        $mdToast.show(
                                            $mdToast.simple()
                                                .content("Status: "+response.status+", Text: "+ response.statusText)
                                                .position(Config.ToastPosition)
                                                .hideDelay(Config.ToastHide)
                                        );
                                    });
                                }
                            }]
                        });
                }

                //Обновление ключа
                $scope.updateClientForm = function ($event, client)
                {
                    $scope.editForm.id = client.id;
                    $scope.editForm.name = client.name;
                    $scope.editForm.redirect = client.redirect;

                    $mdDialog.show
                    (
                        {
                            scope: $scope,
                            clickOutsideToClose: false,
                            preserveScope: true,
                            templateUrl:  '/partials/oauth/editor_form.html',
                            targetEvent: $event,
                            parent:     angular.element(document.body),
                            controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window)
                            {

                                //Выход
                                $scope.hide = function()
                                {
                                    $mdDialog.hide();
                                };

                                $scope.cancel = function() {
                                    $mdDialog.cancel();
                                };

                                $scope.answer = function(answer) {
                                    $mdDialog.hide(answer);
                                };


                                ///Обновление данных
                                $scope.update = function($event)
                                {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content('Пожалуйста, подождите')
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );

                                    $scope.getClient();


                                    $http({
                                        method        :       'PUT',
                                        url: '/oauth/clients/' + $scope.editForm.id,
                                        headers:
                                            {
                                                'Content-Type': 'application/json; charset=utf-8'
                                            },
                                        xsrfHeaderName: Config.CSRF_TOKEN,
                                        data: {
                                            name:       $scope.editForm.name,
                                            redirect:   $scope.editForm.redirect
                                        }
                                    }).then(function successCallback(response)
                                    {
                                        $window.location.reload();

                                    }, function errorCallback(response)
                                    {
                                        $mdToast.show(
                                            $mdToast.simple()
                                                .content("Status: "+response.status+", Text: "+ response.statusText)
                                                .position('bottom right')
                                                .hideDelay(2000)
                                        );

                                        $scope.createForm.error = 'Что-то пошло не так. Пожалуйста, попробуйте еще раз.';
                                    });
                                }
                            }]
                        });
                }


                ///Удаление oAuth ключа
                $scope.destroy = function($event, client)
                {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Пожалуйста, подождите')
                            .position('bottom right')
                            .hideDelay(2000)
                    );

                    $scope.getClient();

                    $http.delete('/oauth/clients/' + client.id)
                        .then(function successCallback(response)
                        {
                            $window.location.reload();

                        }, function errorCallback(response)
                        {

                            $mdToast.show(
                                $mdToast.simple()
                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                    .position(Config.ToastPosition)
                                    .hideDelay(Config.ToastHide)
                            );
                        });
                }

            }]
    })

    .component('oauthAuthorizedClients', {
        templateUrl: '/partials/oauth/authorized_clients.html',
        controller: ['$scope','$mdDialog','$mdToast','$http','$window',
            function ($scope, $mdDialog, $mdToast, $http, $window)
            {
                $scope.tokens = [];


                $scope.getTokens = function($events)
                {
                    $http.get('/oauth/tokens')
                        .then(function successCallback(response)
                        {
                            $scope.tokens = response.data;

                        }, function errorCallback(response)
                        {

                            $mdToast.show(
                                $mdToast.simple()
                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        });
                }

                $scope.getTokens();


                $scope.revoke = function($events, token)
                {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Пожалуйста, подождите')
                            .position('bottom right')
                            .hideDelay(2000)
                    );

                    $http.delete('/oauth/clients/' + client.id)
                        .then(function successCallback(response)
                        {
                            $window.location.reload();

                        }, function errorCallback(response)
                        {

                            $mdToast.show(
                                $mdToast.simple()
                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                    .position(Config.ToastPosition)
                                    .hideDelay(Config.ToastHide)
                            );
                        });
                }

            }]
    })


    .component('oauthPersonalAccessTokens', {
        templateUrl: '/partials/oauth/personal_access_tokens.html',
        controller: ['$scope','$mdDialog','$mdToast','$http','$window',
            function ($scope, $mdDialog, $mdToast, $http, $window)
            {
                $scope.accessToken = null;

                $scope.tokens = [];
                $scope.scopes = [];

                $scope.form = {
                    name: '',
                    scopes: [],
                    errors: []
                };


                $scope.getTokens = function($event)
                {
                    $http.get('/oauth/personal-access-tokens')
                        .then(function successCallback(response)
                        {
                            $scope.tokens = response.data;

                        }, function errorCallback(response)
                        {

                            $mdToast.show(
                                $mdToast.simple()
                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        });
                }

                $scope.getScopes = function($event)
                {
                    $http.get('/oauth/scopes')
                        .then(function successCallback(response)
                        {
                            $scope.scopes = response.data;

                        }, function errorCallback(response)
                        {

                            $mdToast.show(
                                $mdToast.simple()
                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        });
                }

                $scope.getTokens();
                $scope.getScopes();

                $scope.showCreateTokenForm = function($event) {

                    $mdDialog.show
                    (
                        {
                            scope: $scope,
                            clickOutsideToClose: false,
                            preserveScope: true,
                            templateUrl: '/partials/oauth/create_access_token.html',
                            targetEvent: $event,
                            parent:     angular.element(document.body),
                            controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window)
                            {
                                //Выход
                                $scope.hide = function()
                                {
                                    $mdDialog.hide();
                                };

                                $scope.cancel = function() {
                                    $mdDialog.cancel();
                                };

                                $scope.answer = function(answer) {
                                    $mdDialog.hide(answer);
                                };


                                $scope.store = function($event)
                                {
                                    $scope.accessToken = null;
                                    $scope.form.errors = [];

                                    $mdToast.show(
                                        $mdToast.simple()
                                            .content('Пожалуйста, подождите')
                                            .position('bottom right')
                                            .hideDelay(2000)
                                    );

                                    $http.post('/oauth/personal-access-tokens/', $scope.form)
                                        .then(function successCallback(response)
                                        {
                                            $scope.form.name = '';
                                            $scope.form.scopes = [];
                                            $scope.form.errors = [];

                                            $scope.tokens.push(response.data.token);
                                            //$scope.showAccessToken(response.data.accessToken);

                                            $scope.accessToken = response.data.accessToken;
                                            $mdDialog.show
                                            (
                                                {
                                                    scope: $scope,
                                                    clickOutsideToClose: false,
                                                    preserveScope: true,
                                                    templateUrl: '/partilas/oauth/access_token.html',
                                                    targetEvent: $event,
                                                    parent:     angular.element(document.body),
                                                    controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window)
                                                    {
                                                        //Выход
                                                        $scope.hide = function()
                                                        {
                                                            $mdDialog.hide();
                                                        };

                                                        $scope.cancel = function() {
                                                            $mdDialog.cancel();
                                                        };

                                                        $scope.answer = function(answer) {
                                                            $mdDialog.hide(answer);
                                                        };
                                                    }]
                                                });

                                        }, function errorCallback(response)
                                        {
                                            if (typeof response.data === 'object') {
                                                $scope.form.errors = _.flatten(_.toArray(response.data));
                                            } else {
                                                $scope.form.errors = ['Something went wrong. Please try again.'];
                                            }

                                            $mdToast.show(
                                                $mdToast.simple()
                                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                                    .position('bottom right')
                                                    .hideDelay(2000)
                                            );
                                            // $scope.form.errors = 'Что-то пошло не так. Пожалуйста, попробуйте еще раз.';
                                        });
                                }

                                $scope.toggleScope = function(scope)
                                {
                                    if ($scope.scopeIsAssigned(scope)) {
                                        $scope.form.scopes = scope;
                                    } else {
                                        $scope.form.scopes.push(scope);
                                    }
                                };

                                $scope.scopeIsAssigned = function(scope) {
                                    return $scope.form.scopes(scope) >= 0;
                                };
                            }]
                        });
                }

                $scope.showAccessToken = function(accessToken)
                {
                    $scope.accessToken = accessToken;
                }

                $scope.revoke = function(token) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Пожалуйста, подождите')
                            .position('bottom right')
                            .hideDelay(2000)
                    );




                    $http.delete('/oauth/personal-access-tokens/' + token.id)
                        .then(function successCallback(response)
                        {
                            $scope.getTokens();
                            $window.location.reload();

                        }, function errorCallback(response)
                        {
                            $mdToast.show(
                                $mdToast.simple()
                                    .content("Status: "+response.status+", Text: "+ response.statusText)
                                    .position('bottom right')
                                    .hideDelay(2000)
                            );
                        });
                }


            }]
    })