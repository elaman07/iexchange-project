'use strict';

angular.module('DocsApp', ['ngMaterial','ngAria','DocsApp.Components'])
    .config(['$interpolateProvider','$mdThemingProvider','$compileProvider','$logProvider','$qProvider','$mdAriaProvider',
        function($interpolateProvider,$mdThemingProvider,$compileProvider,$logProvider,$qProvider,$mdAriaProvider)
        {
            $logProvider.debugEnabled(true);
            $compileProvider.debugInfoEnabled(false);
            $compileProvider.commentDirectivesEnabled(false);
            $compileProvider.cssClassDirectivesEnabled(false);
            $compileProvider.preAssignBindingsEnabled(true);
            $mdAriaProvider.disableWarnings();
            $qProvider.errorOnUnhandledRejections(false);


            //Новые параметры для определение angular
            $interpolateProvider.startSymbol('[[');
            $interpolateProvider.endSymbol(']]');
            //Фиксируем тему для шаблона
            $mdThemingProvider.theme('default')
                .primaryPalette('blue')
                .accentPalette('blue');
        }])
    .controller('docsCtrl',['$scope', '$timeout', '$mdSidenav', '$log','$mdMedia', function($scope, $timeout, $mdSidenav, $log,$mdMedia) {

        $scope.toggleLeft = buildToggler('left');
        $scope.toggleRight = buildToggler('right');

        $scope.$mdMedia = $mdMedia;

        function buildToggler(componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            };
        }

    }])
    .controller('DeveloperController', ['$scope','$window','$http','$mdToast', function($scope, $window, $http, $mdToast)
    {
        var app_id = document.getElementById('developer_view').getAttribute("data-id");

        $scope.app_data = [];
        $scope.load_data    =   false;

        $http(
            {
                method        :       'POST',
                dataType      :       'JSON',
                url           :       '/apps/loadData',
                headers       :        { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                transformRequest: function(obj)
                {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                        id          :   app_id
                    }

            }).then(function successCallback(response)
        {
            if(response.data.status == 0) {
                $scope.load_data = true;
                $scope.app_data = response.data;

            }else {
                console.log(response.data.error);
            }

        }, function errorCallback(response)
        {
            console.log($scope.response.data.error.status);
        });


        $scope.pressingStatus = false;

        $scope.update = function($event)
        {
            $scope.pressingStatus = true;

            $mdToast.show(
                $mdToast.simple()
                    .content('Пожалуйста, подождите')
                    .position('bottom right')
                    .hideDelay(2000)
            );

            $http(
                {
                    method        :       'PUT',
                    dataType      :       'JSON',
                    url           :      '/apps/updateData',
                    headers       :        { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    transformRequest: function(obj)
                    {
                        var str = [];
                        for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data:
                        {
                            id          :   $scope.app_data.id,
                            name        :   $scope.app_data.name,
                            website        :   $scope.app_data.website,
                            description        :   $scope.app_data.description,
                            redirect    :   $scope.app_data.redirect
                        }

                }).then(function successCallback(response)
            {

                if(response.data.status == 0) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content(response.data.message)
                            .position('bottom right')
                            .hideDelay(2000)
                    );

                }
                else {
                    $mdToast.show(
                        $mdToast.simple()
                            .content(response.data.error)
                            .position('bottom right')
                            .hideDelay(2000)
                    );
                }

                $scope.pressingStatus = false;

            }, function errorCallback(response) {
                console.log($scope.response.data.error.status);
            });
        }

        //Удаление приложения
        $scope.delete = function ($event)
        {
            $scope.pressingStatus = true;

            $mdToast.show(
                $mdToast.simple()
                    .content('Пожалуйста, подождите')
                    .position('bottom right')
                    .hideDelay(2000)
            );

            $http.delete('/oauth/clients/' + app_id)
                .then(function successCallback(response)
                {
                    $window.location.href = '/apps';

                }, function errorCallback(response)
                {
                    $mdToast.show(
                        $mdToast.simple()
                            .content("Status: "+response.status+", Text: "+ response.statusText)
                            .position('bottom right')
                            .hideDelay(2000)
                    );
                });
        }
    }])
;


jQuery(function($) {


    if ($('.documentation-sidebar ul').length) {
        var current = $('.documentation-sidebar ul').find('li a[href="' + window.location.pathname + '"]');

        if (current.length) {
            current.parent().addClass('active-link');
            current.closest('ul').prev().toggleClass('is-active');
        }
    }

    var toggles = document.querySelectorAll('.documentation-sidebar h2'),
        togglesList = document.querySelectorAll('.documentation-sidebar h2 + ul');

    for (var i = 0; i < toggles.length; i++) {
        toggles[i].addEventListener('click', expandItem);
        toggles[i].addEventListener('keydown', expandItemKeyboard);
        toggles[i].setAttribute('tabindex', '0');
    }

    function expandItem(e) {
        var elem = e.target;

        if(elem.classList.contains('is-active')) {
            elem.classList.remove('is-active');
        } else {
            clearItems();
            elem.classList.add('is-active');
        }
    }

    function expandItemKeyboard(e) {
        clearItems();
        var elem = e.target;

        if (e.keyCode === 13) {
            elem.classList.toggle('is-active');
        }

        if (e.keyCode === 39) {
            elem.classList.add('is-active');
        }

        if (e.keyCode === 37) {
            elem.classList.remove('is-active');
        }
    }

    function clearItems() {
        for (var i = 0; i < toggles.length; i++) {
            toggles[i].classList.remove('is-active');
        }
    }

});


(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

// Sidebar
$(document).ready(function()
{
    // TODO: This is some kind of easy fix, maybe we can improve this
    var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
    };

    $SIDEBAR_MENU.find('a').on('click', function(ev) {
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }

            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');
    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
        setContentHeight();
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){
        setContentHeight();
    });

    setContentHeight();
});
// /Sidebar
