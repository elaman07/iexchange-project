angular.module('TxApp',
    [
        'ngMaterial',
        'ngclipboard',
        'ngAria'
    ])
    .config(['$interpolateProvider','$mdThemingProvider','$compileProvider','$logProvider','$qProvider','$mdAriaProvider', function($interpolateProvider,$mdThemingProvider,$compileProvider,$logProvider,$qProvider,$mdAriaProvider)
    {
        $logProvider.debugEnabled(true);
        $compileProvider.debugInfoEnabled(false);
        $compileProvider.commentDirectivesEnabled(false);
        $compileProvider.cssClassDirectivesEnabled(false);
        $compileProvider.preAssignBindingsEnabled(true);
        $mdAriaProvider.disableWarnings();
        $qProvider.errorOnUnhandledRejections(false);


        //Новые параметры для определение angular
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
        //Фиксируем тему для шаблона
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('blue');
    }])

    .controller('CommonController', ['$mdDialog','$mdMenu', function($mdDialog,$mdMenu) {

        var originatorEv;

        this.openMenu = function($mdMenu, ev) {
            originatorEv = ev;
            $mdMenu.open(ev);
        };

    }])

    .controller('BaseController',['$scope', '$timeout', '$mdSidenav', '$http','$mdMedia','$mdDialog', '$window','$interval','$mdToast', function($scope, $timeout, $mdSidenav, $http,$mdMedia,$mdDialog,$window,$interval,$mdToast) {
            $scope.confirmations = null;

            $scope.checkLivePayment = function($code, $id)
            {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Обновление данных...')
                        .position('top right')
                        .hideDelay(3000)
                );

                $http.post('/admin/private/check_receipts', {
                    'id':   $id,
                    'payment': $code
                }).then(function(response)
                {
                    if(response.data.status == 1) {
                        if(response.data.message)
                        {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent(response.data.message)
                                    .position('top right')
                                    .hideDelay(3000)
                            );
                        } else {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Средства не поступили')
                                    .position('top right')
                                    .hideDelay(3000)
                            );
                        }
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Данные успешно обновлены')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        if(response.data.redirect != null) {
                            $window.location.href = response.data.redirect;
                        }
                    }
                    $scope.confirmations = response.data.items.confirmations;
                });
            }

            $scope.enableEdit = function(item){
                item.hidden = true;
            };

            $scope.disableEdit = function(item){
                item.hidden = false;

            };

            $scope.showMobileMainHeader = true;
        $scope.toggleLeft = buildToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.status_site = false;
        $scope.isStatusSite = false;
        $scope.$mdMedia = $mdMedia;
        $scope.new_postponed = 0;
        $scope.new_task = 0;
        $scope.new_payment_bonuses = 0;

        $scope.count_user_messages = 0;
        $scope.live_data = [];
        $scope.isLiveLoading = false;
        $scope.live_deferred = {};

        function buildToggler(componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            };
        }
            $scope.searchShow = function () {
                $scope.showMobileMainHeader = false;
            };
            $scope.searchHide = function () {
                $scope.showMobileMainHeader = true;
            };


        $http({
            method: 'POST',
            url: '/admin/private/status_site',
            data: {
                active: true
            }})
            .then(function successCallback(response) {
                $scope.isStatusSite = true;
                $scope.status_site = response.data.status;
            }, function errorCallback(response) {
                //
            });

        $scope.onChange = function(cbState)
        {
            $http({
                method: 'POST',
                url: '/admin/private/status_site',
                data: {
                    status: cbState
                }
            }).then(function successCallback(response) {
                $scope.status_site = response.data.status;
            }, function errorCallback(response) {
                //
            });
        };




        $scope.refreshData = function ()
        {
            $http({
                method: 'GET',
                url: '/admin/json/live_tx'
            }).then(function successCallback(response)
            {
                $scope.live_deferred = response.data.deferred;
                $scope.live_data = response.data.task;
                $scope.isLiveLoading = true;
                $scope.status_site = response.data.status;
                $scope.new_task = response.data.other.count;
                $scope.new_payment_bonuses = response.data.other.payment_bonuses;
                $scope.new_postponed = response.data.other.new_postponed;
                $scope.count_user_messages = response.data.other.user_messages;

            }, function errorCallback(response) {
                if(response.status == 503) {
                    $window.location.href = '/';
                }
            });
        };

            $scope.refreshData();
          $interval($scope.refreshData, 20000);



        //Открываем настройки
        $scope.settings = function($event)
        {
            $scope.requisites = {};
            $scope.reserves = {};
            $scope.unlimited_reserve = {};
            $scope.direction_exchange = {};
            $scope.requisitesModel = [];
            $scope.reservesModel = [];
            $scope.unlimitedReserveModel = [];
            $scope.unlimitedBaseModel = 0;
            $scope.controlDirectionExchange = [];

            $http({
                method: 'GET',
                url: '/admin/private/operator_settings'
            })
                .then(function successCallback(response) {
                    $scope.requisites = response.data.requisites;
                    $scope.reserves = response.data.reserves;
                    $scope.unlimited_reserve = response.data.unlimited_reserve;
                    $scope.unlimitedBaseModel = response.data.unlimited_base;
                    $scope.direction_exchange = response.data.direction_exchange;
                }, function errorCallback(response) {
                    //
                });

            $mdDialog.show({
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/settings.html',
                targetEvent: $event,
                controller: ['$scope', '$mdToast', '$mdDialog', function ($scope, $mdToast, $mdDialog)
                {
                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.updateUnlimited = function($event, $id_currency)
                    {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Пожалуйста, подождите...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/operator_settings',
                            data: {
                                'type': 'unlimited',
                                'id_currency': $id_currency,
                                'detail': $scope.unlimitedReserveModel[$id_currency]
                            }
                        }).then(function successCallback(response)
                        {
                            $scope.isButtonDisabled = false;
                            if(response.data.status == 0) {
                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Данные успешно обновлены')
                                        .position('top right')
                                        .hideDelay(3000)
                                );
                            }

                        }, function errorCallback(response) {
                            //
                        });
                    };

                    $scope.updateDirectionExchange = function($event, $id_currency)
                    {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Пожалуйста, подождите...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/operator_settings',
                            data: {
                                'type': 'direction_exchange',
                                'id_currency': $id_currency,
                                'detail': $scope.controlDirectionExchange[$id_currency]
                            }
                        }).then(function successCallback(response)
                        {
                            $scope.isButtonDisabled = false;
                            if(response.data.status == 0) {
                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Данные успешно обновлены')
                                        .position('top right')
                                        .hideDelay(3000)
                                );
                            }

                        }, function errorCallback(response) {
                            //
                        });
                    };

                    $scope.updateUnlimitedBase = function($event)
                    {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Пожалуйста, подождите...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/operator_settings',
                            data: {
                                'type': 'unlimited_base',
                                'detail': $scope.unlimitedBaseModel
                            }
                        }).then(function successCallback(response)
                        {
                            $scope.isButtonDisabled = false;
                            if(response.data.status == 0) {
                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Данные успешно обновлены')
                                        .position('top right')
                                        .hideDelay(3000)
                                );
                            }

                        }, function errorCallback(response) {
                            //
                        });
                    };

                    $scope.saveReqisites = function()
                    {
                        $scope.isButtonDisabled = true;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Перестройка реквизитов...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/operator_settings',
                            data: {
                                'type': 'requisites',
                                'detail': $scope.requisitesModel
                            }
                        }).then(function successCallback(response)
                            {
                                $scope.isButtonDisabled = false;
                                if(response.data.status == 0) {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent('Реквизиты успешно перестроены...')
                                            .position('top right')
                                            .hideDelay(3000)
                                    );
                                }

                            }, function errorCallback(response) {
                                //
                            });

                    };

                    $scope.saveReserve = function() {
                        $scope.isButtonDisabled = true;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Корректировка резервов...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/operator_settings',
                            data: {
                                'type': 'reserves',
                                'detail': $scope.reservesModel
                            }
                        }).then(function successCallback(response)
                        {
                            $scope.isButtonDisabled = false;
                            if(response.data.status == 0) {
                                $mdToast.show(
                                    $mdToast.simple()
                                        .textContent('Резервы успешно скорректированы...')
                                        .position('top right')
                                        .hideDelay(3000)
                                );
                            }

                        }, function errorCallback(response) {
                            //
                        });

                    }

                }]
            });
        }

    }])

    .controller('TickerController',['$scope','$http','$mdToast','$mdDialog','$window','$interval', function($scope,$http,$mdToast, $mdDialog,$window,$interval)
    {

        $scope.onEventClipboardSuccess = function(e) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(e+' Copied')
                    .position('top right')
                    .hideDelay(1000)
            );
        }

        $scope.isButton = false;
        $scope.isButtonDisabled = false;

        $scope.disableClick = function() {
            $scope.isButton = true;

            $interval(function() {
                $scope.isButtonDisabled = true;
            }, 1000);
        };

        $scope.confirmations = null;



        $scope.checkPayment = function($code, $id)
        {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Обновление данных...')
                    .position('top right')
                    .hideDelay(3000)
            );

            $http.post('/admin/private/check_receipts', {
                'id':   $id,
                'payment': $code
            }).then(function(response)
            {
                if(response.data.status == 1)
                {
                    var message = 'Средства не поступили';
                    if(response.data.message) {
                        message = response.data.message;
                    }

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(message)
                            .position('top right')
                            .hideDelay(3000)
                    );
                } else {
                    if(response.data.redirect != null) {
                        $window.location.href = response.data.redirect;
                    }

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Данные успешно обновлены')
                            .position('top right')
                            .hideDelay(3000)
                    );
                }

                $scope.confirmations = response.data.items.confirmations;
            });
        }


        //Передать заявку другому оператору
        $scope.transferToAnother = function($event, $id)
        {
            $scope.listOperators = {};
            $scope.selectOperator = null;

            $http({
                method: 'GET',
                url: '/admin/private/list_operators?id='+$id,
            })
                .then(function successCallback(response) {
                    $scope.listOperators = response.data;
                }, function errorCallback(response) {
                    //
                });


            $mdDialog.show({
                controller: ['$scope', '$mdDialog','$window', function($scope, $mdDialog, $window) {
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };

                    $scope.selectedOperator = function($event, $id_user)
                    {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Пожалуйста, подождите...')
                                .position('top right')
                                .hideDelay(3000)
                        );

                        $http({
                            method: 'POST',
                            url: '/admin/private/change_operators',
                            data: {
                                'id': $id,
                                'id_operator': $id_user
                            }
                        })
                            .then(function successCallback(response)
                            {
                                if(response.data.status == 0) {
                                    $mdToast.show(
                                        $mdToast.simple()
                                            .textContent('Оператор успешно изменен...')
                                            .position('top right')
                                            .hideDelay(3000)
                                    );
                                    $window.location.href = response.data.redirect;
                                }
                            }, function errorCallback(response) {
                                //
                            });
                    }
                }],
                scope: $scope,
                clickOutsideToClose: false,
                preserveScope: true,
                templateUrl: '/admin-assets/partials/transfer_to_operator.html',
                targetEvent: $event,
            });
        };


        $scope.notPartner = false;
        $scope.notCashback = false;
        $scope.idTransaction = null;
        $scope.textMessageOrder = null;
        $scope.pinCode = null;
        $scope.isButtonDisabled = false;

        $scope.successPayment = function($event, $merchant, $id)
        {
            $scope.type = $merchant;
            $scope.toolbarCustom = '';
            $scope.isButtonDisabled = true;
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Пожалуйста, подождите...')
                    .position('top right')
                    .hideDelay(3000)
            );

            $scope.hasBalance = 0;
            $scope.toolbarCustom = '';
            $scope.isError = false;
            $scope.ErrorMessage = null;

            $http({
                method: 'POST',
                url: '/admin/private/success_operator_payment',
                data: {
                    'id': $id,
                    'type': $scope.type,
                    'actions': 'success',
                    'id_transaction': $scope.idTransaction,
                    'text_message_order': $scope.textMessageOrder,
                    'pin_code': $scope.pinCode,
                    'not_partner':  $scope.notPartner,
                    'not_cashback':  $scope.notCashback
                }
            }).then(function successCallback(response)
            {
                if(response.data.status == 1)
                {
                    $scope.toolbarCustom = 'toolbar-error';
                    $mdToast.show({
                        template: '<md-toast class="md-toast md-toast-error">Ошибка: ' + response.data.message + '</md-toast>',
                        hideDelay: 6000,
                        position: 'top right'
                    });

                    $scope.isError = true;
                    $scope.ErrorMessage  = response.data.message;

                    $interval(function() {
                        $scope.isButtonDisabled = false;
                    }, 1000);

                } else {
                    $mdToast.show({
                        template: '<md-toast class="md-toast md-toast-success">Заявка успешно выполнена</md-toast>',
                        hideDelay: 3000,
                        position: 'top right'
                    });

                    $window.location.href = response.data.redirect;
                }

            }, function errorCallback(response) {
                //
            });
        };

        //Отложить
        $scope.deferType = 1;
        $scope.categoryReject = 1;

        // Отложить заявку
        $scope.confirmDefer = function($event, $id)
        {
            $scope.isButtonDisabled = true;
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Процесс отложения заявки...')
                    .position('top right')
                    .hideDelay(3000)
            );

            $scope.isError = false;
            $scope.ErrorMessage = null;

            $http({
                method: 'POST',
                url: '/admin/private/success_operator_payment',
                data: {
                    'id': $id,
                    'actions': 'defer',
                    'defer_type': $scope.deferType,
                }
            }).then(function successCallback(response)
            {
                if(response.data.status == 1)
                {
                    $scope.toolbarCustom = 'toolbar-error';
                    $mdToast.show({
                        template: '<md-toast class="md-toast md-toast-error">Ошибка: ' + response.data.message + '</md-toast>',
                        hideDelay: 6000,
                        position: 'top right'
                    });

                    $scope.isError = true;
                    $scope.ErrorMessage  = response.data.message;

                    $interval(function() {
                        $scope.isButtonDisabled = false;
                    }, 1000);

                } else {
                    $mdToast.show({
                        template: '<md-toast class="md-toast md-toast-success">Заявка успешно отложена</md-toast>',
                        hideDelay: 3000,
                        position: 'top right'
                    });

                    $window.location.href = response.data.redirect;
                }

            }, function errorCallback(response) {
                //
            });
        }


        //Отклонить заявку
        $scope.confirmReject = function($event, $id)
        {
            $scope.isButtonDisabled = true;
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Процесс отклонения заявки...')
                    .position('top right')
                    .hideDelay(3000)
            );

            $scope.isError = false;
            $scope.ErrorMessage = null;

            $http({
                method: 'POST',
                url: '/admin/private/success_operator_payment',
                data: {
                    'id': $id,
                    'actions': 'failed',
                    'rejection_status': $scope.categoryReject
                }
            }).then(function successCallback(response)
            {
                if(response.data.status == 1)
                {
                    $scope.toolbarCustom = 'toolbar-error';
                    $mdToast.show({
                        template: '<md-toast class="md-toast md-toast-error">Ошибка: ' + response.data.message + '</md-toast>',
                        hideDelay: 6000,
                        position: 'top right'
                    });

                    $scope.isError = true;
                    $scope.ErrorMessage  = response.data.message;

                    $interval(function() {
                        $scope.isButtonDisabled = false;
                    }, 1000);

                } else {
                    $mdToast.show({
                        template: '<md-toast class="md-toast md-toast-success">Заявка успешно отклонена</md-toast>',
                        hideDelay: 3000,
                        position: 'top right'
                    });

                    $window.location.href = response.data.redirect;
                }

            }, function errorCallback(response) {
                //
            });
        }
    }])


    .controller('ApplicationCtrl', ['$scope','$http','$timeout', function($scope, $http, $timeout)
    {
        // $scope.live = {
        //     deferred: {},
        //     data: {}
        // };
        //
        // $scope.reload = function ()
        // {
        //     $http({
        //         method: 'GET',
        //         url: '/admin/json/live_tx'
        //     }).then(function successCallback(response) {
        //         $scope.live.deferred = response.data.deferred;
        //         $scope.live.data = response.data.task;
        //
        //
        //
        //     }, function errorCallback(response) {
        //         if(response.status == 503) {
        //             window.location.href = '/';
        //         }
        //     });
        // };
        //
        // $inteval($scope.reload, 15000);
        //
        // $scope.reload();
    }])

    .directive('htmlcompile', ['$compile', function ($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    return scope.$eval(attrs.htmlcompile);
                },
                function(value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                }
            );
        };
    }])

    .directive('compile', ['$compile', function ($compile) {
        return function(scope, element, attrs) {
            scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        };
    }])

    .filter('html', ['$sce', function($sce) {
        return function (input) {
            return $sce.trustAsHtml(input);
        }
    }]);
