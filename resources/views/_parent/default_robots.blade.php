# robots.txt for {{ iEXContentLanguage('sitename') }}
# last modification {{ iEXSetting('seo_robots_modification') }}

{!! iEXSetting('seo_robots_txt') !!}

Host: {{ config('app.url') }}
@if(iEXSetting('seo_enabled_sitemap_rule'))
Sitemap: {{ url('/sitemap.xml') }}
@endif
