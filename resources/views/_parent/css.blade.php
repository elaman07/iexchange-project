@foreach(App\Models\Currency::all() as $value)
    @if(isset($value->text_color))
        .main-menu-popup-renderer .mat-list-item.p-{{strtolower(\Str::slug($value->id))}},
        .p-{{strtolower(\Str::slug($value->id))}}
        .mat-form-field-label { color: {{$value->text_color}} !important;}

        .p-{{strtolower(\Str::slug($value->id))}}
        .mat-input-element { caret-color: {{$value->text_color}} !important;}

    .currency-action-{{strtolower(\Str::slug($value->id))}} {border-left-color: {{$value->text_color}} !important;}
    .p-{{strtolower(\Str::slug($value->id))}} .mat-form-field-underline,
    .p-{{strtolower(\Str::slug($value->id))}} .mat-form-field-ripple,
        .main-menu-popup-renderer .mat-list-item.p-{{strtolower(\Str::slug($value->id))}}:hover::before,
        .main-menu-popup-renderer .mat-list-item.active.p-{{strtolower(\Str::slug($value->id))}}::before {
    background-color: {{$value->text_color}} !important;}

    .p-{{strtolower(\Str::slug($value->id))}}.mat-form-field-appearance-outline .mat-form-field-outline-thick {
        color: rgba(0, 0, 0, 0.12) !important;
    }
    .p-{{strtolower(\Str::slug($value->id))}}.mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {
        color: {{$value->text_color}} !important;
    }
    @endif

    @if(isset($value->text_color))
        .selected-i-{{$value->id}} { background: {{$value->text_color}} !important;}
    @endif
@endforeach
