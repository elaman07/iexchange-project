<b>{{ __('telegram-bot.messages.referral_program_title') }}</b>
Вы зарабатываете {{ $current_percent }} от суммы обменов ваших рефералов. Подробнее про условия реферальной программы по ссылке (<a href="{{ url('/partners') }}">{{ url('/partners') }}</a>).

В текущей версии вы можете увидеть список ваших рефералов на сайте, кликнув на кнопку ниже.

{{ __('telegram-bot.messages.your_referral_link') }}:
<a href="{{ url('/?ref_hash='.$referral_hash) }}">{{ url('/?ref_hash='.$referral_hash) }}</a>