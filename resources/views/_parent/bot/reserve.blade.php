

@foreach($filters as $filter)
<b>💰 {{ $filter->name }}</b>
@foreach($filter->currencies as $currency)
@if(isset($currency->reserve))
- {{ $currency->payment->name }} {{ $currency->code_currency->name }}: <b>{{ iex_number_format(iex_reserve_amount($currency->reserve), $currency->number_format, true) }}</b>
@endif
@endforeach

@endforeach
