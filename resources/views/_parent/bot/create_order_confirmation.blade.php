{{ __('telegram-bot.messages.confirm_order_text') }}

💹 {{ __('telegram-bot.messages.course') }} {{ $exchange['r']['curs'] }}
➡️ {{ __('telegram-bot.messages.order_give') }} {{ $user['conversation']['fields']['send_amount_in'] }} {{ $in_detail['attributes']['name'] }}
@if(isset($user['conversation']['fields']['income_account']))
👛{{ $in_detail['attributes']['order_form_fields']['label_in'] }}: {{ $user['conversation']['fields']['income_account'] }}
@endif
⬅️ {{ __('telegram-bot.messages.order_receiving') }} {{ $user['conversation']['fields']['send_amount_out'] }} {{ $out_detail['attributes']['name'] }}
@if(isset($user['conversation']['fields']['outcome_account']))
👛{{ $out_detail['attributes']['order_form_fields']['label_out'] }}: {{ $user['conversation']['fields']['outcome_account'] }}
@endif
📨 {{ __('telegram-bot.messages.your_email') }}: {{ $user_email }}


{!! __('telegram-bot.messages.order_confirm_info') !!}
