@foreach($orders as $order)
<b>{{ __('telegram-bot.messages.my_order_title') }} {{ $order->updated_at }} № {{ $order->id }}</b>
{{ __('telegram-bot.messages.my_order_course') }}:  {{ $order->course_display }}
{{ __('telegram-bot.messages.order_give') }}: {{ $order->give_price }} {{ currency_in($order, true) }}
{{ __('telegram-bot.messages.order_receiving') }}: {{ $order->receiving_price }} {{ currency_out($order, true) }}
@if(strlen(strip_tags($order->from_shot)) > 0)
{{ __('telegram-bot.messages.my_order_from_shot') }}: {{ $order->from_shot }}
@endif
@if(strlen(strip_tags($order->to_shot)) > 0)
{{ __('telegram-bot.messages.my_order_to_shot') }}: {{ $order->to_shot }}
@endif
@switch($order->status)
@case(3)
{{ __('telegram-bot.messages.status') }}: ⏱ {{$order->task_status->name}}
@break
@case(4)
{{ __('telegram-bot.messages.status') }}: ✅ {{$order->task_status->name}}
@break
@case(5)
{{ __('telegram-bot.messages.status') }}: ❌ {{$order->task_status->name}}
@break
@default
{{ __('telegram-bot.messages.status') }}: {{$order->task_status->name}}
@break
@endswitch


@endforeach