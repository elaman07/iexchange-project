<b>Основная статистика:</b>

<b>Пользователи</b>
Всего: {{ iex_number_format($users_count->total, 0, true) }}
Верифицированных: {{ iex_number_format($users_count->verified, 0, true) }}
Новые клиенты за сегодня: {{ iex_number_format($users_count->today, 0, true) }}

<b>Заявки</b>
Всего заявок: {{ iex_number_format($order_count->total, 0, true) }}
Исполненные заявки: {{ iex_number_format($order_count->success, 0, true) }}

<b>Заявки</b>
Всего заявок: {{ iex_number_format($order_count->total, 0, true) }}
Исполненные заявки: {{ iex_number_format($order_count->success, 0, true) }}


<b>Суммарные суммы обменов:</b>

Всего получено ←
<b>{{ iex_number_format($common['in'], 0, true) }} USD</b>
Всего отправлено →
<b>{{ iex_number_format($common['out'], 0, true) }} USD</b>