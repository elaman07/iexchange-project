@foreach($items['exchanges'] as $exchange)
<b><a href="{{ url($exchange['link']) }}">{{ $exchange['_in']['name'] }} --> {{ $exchange['_out']['name'] }}</a></b>
<b>{{ __('telegram-bot.messages.my_order_course') }}:</b> {{ $exchange['_in']['rate'] }} --> {{ $exchange['_out']['rate'] }}
<b>{{ __('telegram-bot.messages.reserve') }}:</b> {{ $exchange['reserve'] }} {{ $exchange['_out']['code'] }}

@endforeach