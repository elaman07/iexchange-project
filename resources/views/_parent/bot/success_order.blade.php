<b>{{ __('telegram-bot.messages.success_order') }}</b>

{{ __('telegram-bot.messages.order_id') }}: {{ iEXSetting('client_id_type_for_order') == 1 ? $order->public_id : $order->id }}
{{ __('telegram-bot.messages.order_status') }}: «{{$order->task_status->name}}»
