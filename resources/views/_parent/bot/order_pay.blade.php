{!! __('telegram-bot.messages.order_pay_text',['id' => $order_pay['id']]) !!}

<b>{{ __('telegram-bot.messages.direction_amount') }}:</b>
{{ $in_detail['attributes']['name'] }} −−> {{ $out_detail['attributes']['name'] }}
{{ $order_pay['attributes']['income_amount']['amount'] }} {{ $order_pay['attributes']['income_amount']['currency'] }} −−> {{ $order_pay['attributes']['outcome_amount']['amount'] }} {{ $order_pay['attributes']['outcome_amount']['currency'] }}


@if($order_pay['attributes']['pay_merchant'] == false)
<b>{{ __('telegram-bot.messages.you_requisites') }}:</b>
@if(!is_null($order_pay['attributes']['income_account']))
{{ $in_detail['attributes']['order_form_fields']['label_in'] }}: {{ $order_pay['attributes']['income_account'] }}
@endif
@if(!is_null($order_pay['attributes']['outcome_account']))
{{ $out_detail['attributes']['order_form_fields']['label_out'] }}: {{ $order_pay['attributes']['outcome_account'] }}
@endif
@endif


@if($order_pay['attributes']['pay_merchant'] == false)
<b>{{ __('telegram-bot.messages.requisites_pay') }}:</b>
@if(is_null($order_pay['attributes']['payment_field']['value']))
{{ __('telegram-bot.messages.shot_undefined') }}
@else
{{ $order_pay['attributes']['payment_field']['value'] }}
@endif

@if(isset($order_pay['attributes']['custom_fields']))
{{ $order_pay['attributes']['custom_fields']['name'] }}: {{ $order_pay['attributes']['custom_fields']['prefix'] }} {{ $order_pay['attributes']['custom_fields']['value'] }}
@endif

@if(isset($order_pay['attributes']['custom_fields']) and isset($order_pay['attributes']['custom_fields']['comment']))
⚠️<b>{{ __('telegram-bot.messages.warning_title') }}</b>
{{ strip_tags($order_pay['attributes']['custom_fields']['comment']) }}
@endif

@if(!empty($order_pay['relationships']['pay_fields']))
@foreach($order_pay['relationships']['pay_fields'] as $value)
<b>{{ $value['name'] }}:</b> {{ $value['value'] }}
@endforeach
@endif
{!! __('telegram-bot.messages.order_pay_confirm') !!}
@else
{!! __('telegram-bot.messages.order_pay_confirm_merchant') !!}
@endif
