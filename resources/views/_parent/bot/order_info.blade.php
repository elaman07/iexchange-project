<b>{{ __('telegram-bot.message.detail_order_title', ['id' => iEXSetting('client_id_type_for_order') == 1 ? $detail->public_id : $detail->id]) }}</b>


📉 {{ __('telegram-bot.messages.my_order_course') }}: {{$detail->course_display}}
🕐 {{ __('telegram-bot.messages.status') }}: @if($detail->status == 3){{ $detail->task_status->name }} @elseif($detail->status == 2) {{$detail->task_status->name}} @elseif($detail->status == 6){{$detail->task_status->name}}@elseif($detail->status == 1){{$detail->task_status->name}}@elseif($detail->status == 5){{$detail->task_status->name}}@elseif($detail->status == 6){{$detail->task_status->name}}@elseif($detail->status == 4){{$detail->task_status->name}}@endif

📆 {{ __('telegram-bot.messages.created_at') }}: {{\Illuminate\Support\Carbon::parse($detail->created_at)->translatedFormat('d M Y, H:i')}}


👉 <b>{{ __('telegram-bot.messages.order_give') }}</b>
---------------
{{ __('telegram-bot.messages.currency') }}: {{ currency_in($detail, true) }}
{{ __('telegram-bot.messages.amount') }}: @if(in_array($detail->direction_exchange->currency1->code_currency->id, [1,2]))
{{ number_format($detail->give_price, 2,'.',' ')}} {{  $detail->direction_exchange->currency1->code_currency->sign }}
@else
{{ $detail->give_price }} {{  $detail->direction_exchange->currency1->code_currency->sign }}
@endif


👉 <b>{{ __('telegram-bot.messages.order_receiving') }}</b>
---------------
{{ __('telegram-bot.messages.currency') }}: {{ currency_out($detail, true) }}
{{ __('telegram-bot.messages.amount') }}: @if(in_array($detail->direction_exchange->currency2->code_currency->id, [1,2]))
{{ number_format($detail->receiving_price, 2,'.',' ')}} {{  $detail->direction_exchange->currency2->code_currency->sign }}
@else
{{ $detail->receiving_price }} {{  $detail->direction_exchange->currency2->code_currency->sign }}
@endif

