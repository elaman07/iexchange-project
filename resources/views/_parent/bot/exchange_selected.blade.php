<b>{{ __('telegram-bot.messages.your_created_direction') }}</b>
{{$in_detail['attributes']['name']}} 🔄 {{ $out_detail['attributes']['name'] }}

{{ __('telegram-bot.messages.exchange_rate') }}: {{ $response['r']['curs'] }}
{{ __('telegram-bot.messages.reserve') }}: {{ $reserve }} {{ $out_detail['attributes']['currency_iso_code'] }}

@if(!empty(strip_tags($in_detail['attributes']['income_notice'])))
⚠ {{ strip_tags($in_detail['attributes']['income_notice']) }}
@endif
