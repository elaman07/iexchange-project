$(function()
{
setTimeout(function ()
{
var chart = $('#list-tasks').highcharts();
chart.reflow();
}, 500);
});

Highcharts.getSVG = function (charts) {
var svgArr = [],
top = 0,
width = 0;

Highcharts.each(charts, function (chart) {
var svg = chart.getSVG(),
// Get width/height of SVG for export
svgWidth = +svg.match(
/^<svg[^>]*width\s*=\s*\"?(\d+)\"?[^>]*>/
)[1],
svgHeight = +svg.match(
/^<svg[^>]*height\s*=\s*\"?(\d+)\"?[^>]*>/
)[1];

svg = svg.replace(
'<svg',
'<g transform="translate(0,' + top + ')" '
);
svg = svg.replace('</svg>', '</g>');

top += svgHeight;
width = Math.max(width, svgWidth);

svgArr.push(svg);
});

return '<svg height="' + top + '" width="' + width +
                '" version="1.1" xmlns="http://www.w3.org/2000/svg">' +
    svgArr.join('') + '</svg>';
};

Highcharts.exportCharts = function (charts, options) {

// Merge the options
options = Highcharts.merge(Highcharts.getOptions().exporting, options);

// Post to export server
Highcharts.post(options.url, {
filename: options.filename || 'chart',
type: options.type,
width: options.width,
svg: Highcharts.getSVG(charts)
});
};


Highcharts.setOptions({
global: {
useUTC: false
},
lang: {
loading: 'Загрузка...',
months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
exportButtonTitle: "Экспорт",
printButtonTitle: "Печать",
rangeSelectorFrom: "С",
rangeSelectorTo: "По",
rangeSelectorZoom: "Период",
downloadPNG: 'Скачать PNG',
downloadJPEG: 'Скачать JPEG',
downloadPDF: 'Скачать PDF',
downloadSVG: 'Скачать SVG',
printChart: 'Напечатать график'
},

});


var chart1 = $('#list-tasks').highcharts({
title: {
text: ''
},

credits: {
enabled: false
},

subtitle: {
text: ""
},

xAxis: {
type: 'datetime'
},

yAxis: {
title: {
text: null
}
},

series: [{
name: "Все заявки",
color: '#107eeb',
data: {{ $array_applications  }}
},
{
name: "Выполненные заявки",
color: '#05b308',
data: {{ $array_completed_requests  }}
},
{
name: "Отклоненные заявки",
color: '#c71423',
data: {{ $array_deleted_requests  }}
}
],
exporting: {
enabled: false // hide button
}

});


$('#export-png').click(function () {
Highcharts.exportCharts([chart1]);
});

$('#export-pdf').click(function () {
Highcharts.exportCharts([chart1], {
type: 'application/pdf'
});
});
