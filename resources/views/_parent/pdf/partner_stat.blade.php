<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Отчет за {{ Carbon\Carbon::now()->format('d.m.Y') }}</title>
    @include('_parent.pdf.css')
</head>
<body>


<div style="padding-bottom: 20px;">
    <div>Переходов за месяц: {{ $transitions_month }}</div>
    <div>Обменов за месяц: {{ $exchange_month }}</div>
    <div>Регистраций за месяц: {{ $register_month }}</div>
    <hr />

    <div>Партнерский процент: {{ $referral_ref }}%</div>
</div>



<h1>Заявок за месяц {{ $orders->count() }}</h1>

<table class="table table-bordered">
    <thead>
    <tr>
        <th class="css-black">№ заявки</th>
        <th>Сумма</th>
        <th>Клиент отдает</th>
        <th>Сервис отправляет</th>
        <th>Бонус</th>
    </tr>

    </thead>
    <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ direction_name($order, true) }}</td>
                <td>{{ $order->give_price }} {{ $order->direction_exchange->currency1->code_currency->name }}</td>
                <td>{{ $order->receiving_price }} {{ $order->direction_exchange->currency2->code_currency->name }}</td>
                <td>
                    @if(isset($order->referral_log))
                        <small class="text-muted">{{ $order->referral_log->bonus }}</small>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div style="text-align: right; padding-bottom: 20px;">
    <span style="font-size: 20px;">Итого:</span>&nbsp;&nbsp;&nbsp;
    @if(config('crypto.currency_payout_position') == 'left')
        <span style="font-size: 25px;color: #0D47A1">{{ config('crypto.currency_payout_sign') }} {{ $total }}</span>
    @else
        <span style="font-size: 25px;color: #0D47A1">{{ $total }} {{ config('crypto.currency_payout_sign') }}</span>
    @endif
</div>
