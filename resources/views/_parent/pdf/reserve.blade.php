<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Отчет за {{ Carbon\Carbon::now()->format('d.m.Y') }}</title>
    @include('_parent.pdf.css')
</head>
<body>

<h1>Резервы за {{ \Illuminate\Support\Carbon::now()->format('d.m.Y') }}</h1>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="css-black">Валюта</th>
        <th>Резерв</th>
        <th>Актуальный курс</th>
        <th>Итого</th>
    </tr>

    </thead>
    <tbody>
    @foreach($currencies as $item)
        <tr>
            <th class="css-black" scope="row">{{$item->payment->name}}  {{$item->code_currency->name}}</th>
            <td>
                @if(isset($item->reserve))
                    @if(isset($item->reserve->main_reserve))
                        {{ $item->reserve->main_reserve->summa }}
                    @else
                        {{ $item->reserve->summa }}
                    @endif
                @endif
            </td>
            <td>
                ${{ number_format(convert_to_usd($item->code_currency->name, 1) , 2,'.',' ') }}
            </td>
            <td>
                @if(isset($item->reserve))
                    @if(isset($item->reserve->main_reserve))
                        Привязан к {{$item->payment->name}}  {{$item->code_currency->name}}
                    @else
                        ${{ number_format(convert_to_usd($item->code_currency->name, $item->reserve->summa), 2,'.',' ') }}
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div style="text-align: right; padding-bottom: 20px;">
    <span style="font-size: 20px;">Итого:</span>&nbsp;&nbsp;&nbsp;
    <span style="font-size: 25px;color: #0D47A1">${{ $totalUSD }}</span>
</div>

<h1>События по резервам за {{ \Illuminate\Support\Carbon::now()->format('d.m.Y') }}</h1>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="css-black">Валюта</th>
            <th>Тип</th>
            <th>Дата/Менеджер</th>
            <th>Значение до</th>
            <th>Значение после</th>
        </tr>
        </thead>
        <tbody>
        @if($events->count() > 0)
            @foreach($events as $item)
                <tr>
                    <td class="css-black" scope="row">
                        {{$item->currency->payment->name}} {{$item->currency->code_currency->name}}<br />
                    </td>
                    <td>
                        @if($item->type == 0)
                            <span style="font-size: 11px;color:red;">Уменьшен</span>
                        @else
                            <span style="font-size: 11px;color: green">Пополнен</span>
                        @endif
                    </td>

                    <td>
                        [{{ $item->user->id }}] - {{ $item->user->name }}
                        <div style="color: #555;font-size: 11px;">
                            {{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}
                        </div>
                    </td>
                    <td>{{$item->value_before}}</td>
                    <td>{{$item->value_after}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9"><p align="center"><br />Событий за 24-часа нет</p></td>
            </tr>
        @endif
        </tbody>
    </table>

<h1>Исполенные заявки за {{ \Illuminate\Support\Carbon::now()->format('d.m.Y') }}</h1>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="css-black">Направление</th>
        <th>Отдает клиент</th>
        <th>Переводит сервис</th>
        <th>Курс</th>
    </tr>

    </thead>
    <tbody>
    @foreach($exchange as $item)
        <tr>
            <th class="css-black" scope="row">
                {{ $item->direction_exchange->currency1->payment->name }} {{ $item->direction_exchange->currency1->code_currency->name }}
                ->
                {{ $item->direction_exchange->currency2->payment->name }} {{ $item->direction_exchange->currency2->code_currency->name }}
            </th>
            <td>
                {{ $item->give_price }} {{ $item->direction_exchange->currency1->code_currency->name }}
            </td>
            <td>
                {{ $item->receiving_price }} {{ $item->direction_exchange->currency2->code_currency->name }}
            </td>
            <td>
                {{$item->course_display}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>