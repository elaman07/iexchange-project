<style type="text/css">
    @font-face {
        font-family: 'Roboto';
        src: url('{{ storage_path('app/fonts/Roboto-Regular.ttf') }}') format('truetype');
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto';
        src: url('{{ storage_path('app/fonts/Roboto-Medium.ttf') }}') format('truetype');
        font-weight: 500;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto';
        src: url('{{ storage_path('app/fonts/Roboto-Thin.ttf') }}') format('truetype');
        font-weight: 100;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto';
        src: url('{{ storage_path('app/fonts/Roboto-Black.ttf') }}') format('truetype');
        font-weight: 900;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto';
        src: url('{{ storage_path('app/fonts/Roboto-Light.ttf') }}') format('truetype');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto';
        src: url('{{ storage_path('app/fonts/Roboto-Bold.ttf') }}') format('truetype');
        font-weight: bold;
        font-style: normal;
    }
</style>