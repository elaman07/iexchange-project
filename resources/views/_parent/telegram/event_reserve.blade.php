@extends('admin.layouts.app')

@section('title','Панель управления')


@section('no_background')
    @if($events->count() > 0)
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Дата/Менеджер</th>
                <th>Валюта</th>
                <th>Значение до</th>
                <th>Значение после</th>
            </tr>
            </thead>
            <tbody>
            @foreach($events as $item)
                <tr>
                    <td>
                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_user}}&sorting=id"><b>{{$item->user->name}}</b></a>
                        <div style="color: #555;font-size: 11px;">
                            {{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}
                        </div>
                    </td>
                    <td>
                        {{$item->currency->payment->name}} {{$item->currency->code_currency->name}}<br />
                        @if($item->type == 0)
                            <span class="text-danger" style="font-size: 11px">Уменьшен</span>
                        @else
                            <span class="text-success"  style="font-size: 11px">Пополнен</span>
                        @endif
                    </td>
                    <td>
                        {{$item->value_before}}
                    </td>

                    <td>
                        {{$item->value_after}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-warning text-center">Событий за 24-часа нет</div>
    @endif
@endsection