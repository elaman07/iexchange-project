<table>
    <thead>
    <tr>
        <th>Номер</th>
        <th>Код</th>
    </tr>
    </thead>
    <tbody>
    @foreach($codes as $code)
        <tr>
            <td>{{ $code->num }}</td>
            <td>{{ $code->code }}</td>
        </tr>
    @endforeach
    </tbody>
</table>