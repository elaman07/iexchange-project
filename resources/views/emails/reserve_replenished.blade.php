@component('mail::message')
# Заявка на резерв выполнена.

Здравствуйте!<br/>
Резерв валюты ***{{$name}}*** успешно пополнен.<br/>
Сумма - {{$data->price}}.

@component('mail::button', ['url' => config('app.url')])
Сделать обмен
@endcomponent

@endcomponent
