@component('mail::message')

@if($part == false)
# Заявка на выплату выполнено
@else
# Заявка на выплату (частичная)
@endif
Здравствуйте {{$item->user->name}}!<br/>

@if($part == true)
{!! $log->text !!}<br/>
@else
Выплата в полном объему произведена успешна.
@endif

@component('mail::button', ['url' => config('app.url')])
    Сделать обмен
@endcomponent

@endcomponent
