@component('mail::message')
# {{ $options['title'] }}

<!-- Если заявка выполнена успешно -->
@if($order->status == 4)
<p>{!! __('email.you_order_success_desc', ['id' => iEXSetting('client_id_type_for_order') == 1 ? $order->public_id : $order->id]) !!}</p>
@component('mail::button', ['url' => url('/check/'.$order->transaction->transaction)])
    {{ __('email.link_check') }}
@endcomponent

<p>{!! __('email.order_status_message') !!}</p>
<br/>
@if(count($links) > 0)
@component('mail::panel')

@if(count($socials) > 0)
@component('mail::table')
|        |         |
| ------------- |:-------------|
|<b>{{ __('email.sites_link_title') }}:</b> | <b>{{ __('email.social_link_title') }}:</b>
| {!! $content_link !!} | {!! $content_social !!}
@endcomponent
@else
<b>{{ __('email.sites_link_title') }}:</b>
@foreach($links as $link)
* <a target="_blank" href="{{$link->url}}">{{$link->name}}</a><br />
@endforeach
@endif
@endcomponent
@endif

<!--Приветствие перед действиями -->
@elseif(in_array($order->status, [5, 8]))
<p>{{ __('email.create_order_desc') }} №{{ iEXSetting('client_id_type_for_order') == 1 ? $order->public_id : $order->id }}.</p>
<p>{{ __('email.status_your_order') }}: «@if($order->status == 5)Удалена@else{{$order->task_status->name}} @endif»</p>
@endif

@if($order->tasks_info and !empty($order->tasks_info->text_message_order))
<p>{{ $order->tasks_info->text_message_order }}</p> 
@endif

<!-- Причина отклонения заявки -->
@if($order->status == 5 and isset($order->tasks_rejection_status))
@component('mail::panel')
<b>{{ __('email.cause') }}:</b>
<p>{{ ($order->tasks_rejection_status->id == 5 ? __('email.fraudulent_activities') : $order->tasks_rejection_status->name) }}</p>
@endcomponent

@if($order->tasks_rejection_status->id == 1)
<p>{{ __('email.order_status_desc') }} {{ iEXSetting('project_email_support') }}</p>
@endif

@component('mail::button', ['url' => config('app.url')])
    {{ __('email.connect_operator') }}
@endcomponent
@endif

<!-- В случае если заявка отложено -->
@if($order->status == 8 and isset($order->pending_order_status))
@component('mail::panel')
<b>{{ __('email.cause') }}:</b>
@if($order->pending_order_status->id == 5)
<p>{{ __('email.order_status_debtors') }}.</p>
@else
<p>{{$order->pending_order_status->name}}</p>
@endif
@endcomponent
@endif
@endcomponent
