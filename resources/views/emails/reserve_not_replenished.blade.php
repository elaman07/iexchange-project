@component('mail::message')
# {{config('app.name')}} - Заявка на резерв не выполнена.

Здравствуйте!<br/>
Резерв валюты ***{{$name}}*** не пополнен.<br/>
@endcomponent
