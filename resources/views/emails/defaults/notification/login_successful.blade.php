Здравствуйте {{ $user->name }}, мы обнаружили успешную авторизацию вашей учетной записи на {{ iEXContentLanguage('sitename') }}, в {{ Carbon\Carbon::now()->toDateTimeString() }}.
<br /><br />
* IP Адрес: {{ $user->ip_address }}
* Страна/Город: {{ $geo_string }}
<br /><br />


Если вы думаете, что кто-то вошел в ваш аккаунт против вашей воли, вам необходимо срочно зайти на сервис и сменить пароль.