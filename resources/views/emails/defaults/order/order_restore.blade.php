<p>{{ __('email.hello_user', ['name' => $order->user->name]) }}<br/>
{!! __('email.restore_desc', ['id' => iEXSetting('client_id_type_for_order') == 1 ? $order->public_id : $order->id]) !!}</p>

<p>{{ __('email.status_your_order') }}: «{{$order->task_status->name}}»<br/>
