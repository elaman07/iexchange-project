<p>{{ __('email.hello_user', ['name' => $order->user->name]) }}</b><br/>
{{ __('email.create_order_desc') }} <b>№{{iEXSetting('client_id_type_for_order') == 1 ? $order->public_id : $order->id}}</b></p>

<div class="order-head__title">{{ __('email.order_info_title') }}</div>
 
<x-mail::panel>
<div class="order-title__block">{{ __('email.order_status') }}</div>
<div>{{$order->task_status->name}}</div>
</x-mail::panel>

<x-mail::panel>
 <div class="order-tr__wrap">
  <div class="order-tr__title">{{ __('email.order_rate') }}</div>
  <div class="order-tr__value">{{ $order->course_display }}</div>
 </div>

 @if(!empty($order->task_info->country_name) and !empty($order->task_info->city_name))
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('email.city_country') }}</div>
   <div class="order-tr__value">{{ $order->task_info->country_name  }} /{{ $order->task_info->city_name  }}</div>
  </div>
 @endif
</x-mail::panel>

@if(!empty($order->transfer_to_account))
<x-mail::panel>
 <div class="order-tr__wrap">
  <div class="order-tr__title">{{ __('email.transfer_to') }}</div>
  <div class="order-tr__value">{{ $order->transfer_to_account }}</div>
 </div>
 @if(count($tasks_fields['requisites_info_field']) > 0)
  <div class="order-tr__wrap">
   @foreach($tasks_fields['requisites_info_field'] as $value)
    <div class="order-tr__title">{{ $value->key_name }}</div>
    <div class="order-tr__value">{{ $value->value_name }}</div>
   @endforeach
  </div>
 @endif

 @if(count($tasks_fields['many']) > 0)
  <div class="order-tr__wrap">
   @foreach($tasks_fields['many'] as $value)
    <div class="order-tr__title">{{ $value->field_name }}</div>
    <div class="order-tr__value">{{ $value->field_value }}</div>
   @endforeach
  </div>
 @endif
</x-mail::panel>
@endif


<x-mail::panel>
 <div class="order-tr__wrap">
  <div class="order-tr__title">{{ __('email.in') }}</div>
  <div class="order-tr__value">{{$order->give_price}} {{ $order->direction_exchange->currency1->code_currency->name }}</div>
 </div>

 @if(isset($order->from_shot))
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('email.account_number') }}</div>
   <div class="order-tr__value">{{ $order->from_shot }}</div>
  </div>
 @endif

 @if(isset($tasks_fields) and isset($tasks_fields['currency_in']) and count($tasks_fields['currency_in']) > 0)
  <div class="order-tr__wrap">
   @foreach($tasks_fields['currency_in'] as $item)
    <div class="order-tr__title">{{ $item->field_name }}</div>
    <div class="order-tr__value">@if(!is_null($item->field_value)){{ $item->field_value }}@else<span class="text-danger">{{ __('email.empty_shot') }}</span>@endif</div>
   @endforeach
  </div>
 @endif
</x-mail::panel>

<x-mail::panel>
 <div class="order-tr__wrap">
  <div class="order-tr__title">{{ __('email.out') }}</div>
  <div class="order-tr__value">{{$order->receiving_price}} {{ $order->direction_exchange->currency2->code_currency->name }}</div>
 </div>

 @if(isset($order->to_shot))
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('email.account_number') }}</div>
   <div class="order-tr__value">{{ $order->to_shot }}</div>
  </div>
 @endif

 @if(isset($tasks_fields) and isset($tasks_fields['currency_out']) and count($tasks_fields['currency_out']) > 0)
  <div class="order-tr__wrap">
   @foreach($tasks_fields['currency_out'] as $item)
    <div class="order-tr__title">{{ $item->field_name }}</div>
    <div class="order-tr__value">@if(!is_null($item->field_value)){{ $item->field_value }}@else<span class="text-danger">{{ __('email.empty_shot') }}</span>@endif</div>
   @endforeach
  </div>
 @endif
</x-mail::panel>

<x-mail::panel>
 <div class="order-tr__wrap">
  <div class="order-tr__title">{{ __('email.created_at') }}</div>
  <div class="order-tr__value">{{$order->created_at}}</div>
 </div>
 <div class="order-tr__wrap">
  <div class="order-tr__title">{{ __('email.direction_exchange') }}</div>
  <div class="order-tr__value">{{ direction_name($order, true) }}</div>
 </div>
</x-mail::panel>

@if($instruction_currency_in)
 <x-mail::panel>
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('email.description') }}</div>
   <div class="order-tr__value">{!! $instruction_currency_in !!}</div>
  </div>
 </x-mail::panel>
@endif

@if(!is_null($instruction_direction))
 <x-mail::panel>
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('Инструкция по оплате') }}</div>
   <div class="order-tr__value">{!! $instruction_direction !!}</div>
  </div>
 </x-mail::panel>
@endif

@if($order->direction_exchange->is_email_desc_exchange_dop == 1)
 <x-mail::panel>
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('Дополнительное описание обмена') }}</div>
   <div class="order-tr__value">{!! $order->direction_exchange->desc_exchange_dop !!}</div>
  </div>
 </x-mail::panel>
@endif

@if($order->direction_exchange->is_email_other_docs == 1)
 <x-mail::panel>
  <div class="order-tr__wrap">
   <div class="order-tr__title">{{ __('Дополнительный текст') }}</div>
   <div class="order-tr__value">{!! $order->direction_exchange->other_docs !!}</div>
  </div>
 </x-mail::panel>
@endif

<x-mail::button :url="url('/orders/'.$order->public_id)" color="primary">
 {{ __('email.open_order') }}
</x-mail::button>