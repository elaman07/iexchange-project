Менеджер **{{ $reserve->user->name }}** изменил резерв вручную.

**Детали:**<br/>
Дата создания: {{ $reserve->created_at }}<br />
Тип: @if($reserve->type == 0) Уменьшен @else Пополнен @endif <br />
События: {{ $reserve->text }} <br />
@if(!is_null($reserve->comment))
Комментарий: {{ $reserve->comment }}
@endif