@component('mail::message')
# {{ __('email.verify_title') }}

{{ __('email.hello_user', ['name' => $user_name]) }}<br/>
{{ __('email.verify_text') }} {{ iEXContentLanguage('sitename') }}.

@component('mail::panel')
<b>{{ __('email.access_account') }}:</b><br />
{!! __('email.verify_desc') !!}
@endcomponent

{{ __('email.verify_notify_account') }} {{ iEXContentLanguage('sitename') }}:

* 	{{ __('email.track_status') }}
*  {{ __('email.referral_discount') }}

{{ __('email.verify_button_text') }}
@component('mail::button', ['url' => $verify])
    {{ __('email.verify_button') }}
@endcomponent
@endcomponent
