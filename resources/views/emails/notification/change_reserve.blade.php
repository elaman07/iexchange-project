@component('mail::message')
@if(empty($data))
# Корректировка резерва для валюты {{ $reserve->currency->payment->name }} {{ $reserve->currency->code_currency->name }}

@include('emails.defaults.change_reserve')
@else
# {{ $data['title'] }}

{!! $data['content'] !!}
@endif
@endcomponent
