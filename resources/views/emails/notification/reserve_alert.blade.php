@component('mail::message')
# Окончание резерва для {{ $alert->reserves->currency->payment->name.' '.$alert->reserves->currency->code_currency->name }}

Уважаемая администрация сервиса {{ iEXContentLanguage('sitename') }}, резерв валюты {{ $alert->reserves->currency->payment->name.' '.$alert->reserves->currency->code_currency->name }}
опустился ниже установленного порога.
<br />
* В резерве {{ $alert->reserves->summa }} {{ $alert->reserves->currency->code_currency->name }}
* Установленный порог {{ $alert->threshold }} {{ $alert->reserves->currency->code_currency->name }}
@endcomponent
