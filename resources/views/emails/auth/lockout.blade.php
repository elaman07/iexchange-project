@component('mail::message')
# {{ __('email.failed_step_auth') }}!<br/><br/>

{{ __('email.hello_user', ['name' => $user->name]) }}<br/>
{{ __('email.lockout_desc') }}

**{{ __('email.details') }}**<br />
* IP: {{ $details['ip'] }}
* {{ __('email.city_country') }}: ({{ $details['code_country'] }}) {{ $details['country'] }}, {{ $details['city'] }}
* {{ __('email.last_date_attempts') }} {{ now()->toDateTimeString() }}
@endcomponent
