@component('mail::message')
# {{ __('email.sign_new_device') }}<br/><br/>

{{ __('email.sign_new_device_desc', ['sitename' => iEXContentLanguage('sitename')]) }}

**Location:** {{ $log->country }},{{ $log->city }}<br />
**Account:** {{ $account->email }}<br />
**Time:** {{ $time->toCookieString() }}<br />
**IP Address:** {{ $ipAddress }}<br />
**Browser:** {{ $browser }}

@endcomponent