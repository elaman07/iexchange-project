@extends('layouts.app')

@section('title', __('E-Mail подтвержден'))

@section('top-js')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

    <style>

        body {
            background: #252a48;
        }

        .full_page {
            font-family: Roboto, sans-serif;
            max-width: 800px;
            padding-bottom: 50px;
            margin-top: 50px;
        }

        .static-body-container {
            padding: 6rem 10rem;
            margin-top: 4.6rem;
        }

        .static-body-container .data-input-left {
            padding: 0;
        }

        .static-body-container h2 {
            font-size: 26px;
            font-weight: 300;
            color: #252a48;
            margin: 0.6rem 0 4.6rem;
            padding: 0;
        }

        .currency-block-link {
            position: relative;
            bottom: 0;
            background: #252a48bf;
            left: 0;
            right: 0;
            padding: 10px 15px 15px;
        }


        .btn {
            text-align: center;
            box-shadow: 0 20px 40px rgba(0,0,0,0.1);
            outline: none;
            width: 260px;
            text-transform: uppercase;
            min-height: 49px;
            display: block;
            margin: 14px auto 0;
            border: none;
            background-color: #1c213c;
            color: #fff;
            line-height: 3;
        }

        .btn:hover, .btn:focus, .btn:active {
            outline: none;
            background-color: #1c213c !important;
            opacity: .6;
            color: #fff !important;
        }
    </style>
@endsection

@section('content')
    <div class="container full_page">

        <div class="default-page-wrap default-page-inner">
            <div class="static-body-container">
                <h2 class="text-center default-page-title">{{ __('E-Mail подтвержден') }}</h2>
                <div class="text-center">
                    <p>{{ __('Ваш e-mail адрес успешно верифицирован') }}</p>
                </div>
            </div>
        </div>

        <div class="currency-block-link">
            <a class="btn btn-default" href="/">{{ __('На главную') }}</a>
        </div>
    </div>
@endsection
