@extends('layouts.app')

@section('title', __('Переадресация'))

@section('content')
    <link nonce="tlUXuDHSkedfiqSZ74FKlrvnZFasNg9R" href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">

    <style>

        .page-center {
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            width: 100%;
        }

        .loading-merchant {
            max-width: 600px;
            margin: 0 auto;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 3px 1px -2px rgb(0 0 0 / 20%), 0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%);
            overflow: hidden;
            padding: 20px 30px;
            font-size: 16px;
            font-family: Nunito, serif;
        }

        h4 {
            margin-top: 30px;
            text-align:center;
            font-family: Nunito, serif;
        }
    </style>

    <div class="page-center">
        <div class="loading-merchant">
            {{ __('Ожидание перенаправления на платёжную систему') }}...
        </div>

        {!! $html !!}
    </div>
@endsection


@section('bottom-js')
    <script src="/js/lottie-player.js"></script>
    <script type="text/javascript">
        setTimeout(function() {
            document.getElementById("autosubmit").submit();
        }, 1000);
    </script>
@endsection
