<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="padding: 10px 20px 20px;">
        <form action="?widget_form=status_operator" method="POST"  layout="row" layout-align="center center">
            @csrf
            <select class="form-control selectpicker" name="is_user_status_operator" data-live-search="true">
                <option value="0" @if(iEXSetting('is_user_status_operator') == 0) selected @endif>{{ __('Не выбрано') }}</option>
                <option value="1" @if(iEXSetting('is_user_status_operator') == 1) selected @endif>Операторы в сети</option>
                <option value="2" @if(iEXSetting('is_user_status_operator') == 2) selected @endif>Операторы не в сети</option>
            </select>

            <md-button class="md-raised md-primary" style="padding: 0" type="submit">Применить</md-button>
        </form>
    </div>
</div>
