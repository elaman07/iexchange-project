
<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header') (В сети: {{ count($user_online) }})</h2>
    </div>

    <div class="x_content online-user-content">
        @if(count($user_online) > 0)
            <div slim-scroll="" height="200px">

                <table class="table table-border-2">
                    <tbody>
                    @foreach($user_online as $online)
                        <tr>
                            <td>
                                {{ $loop->iteration }}.
                            </td>

                            <td>
                                <a href="{{ admin_path('/account/logs_auth/?ip_address='.$online->ip_address) }}">IP {{ $online->ip_address }}</a>
                            </td>

                            <td class="text-right" style="padding-right: 20px;">
                                @if(\Illuminate\Support\Carbon::parse($online->last_activity_at)->addMinutes(1) > \Illuminate\Support\Carbon::now() )
                                    <small class="text-success">
                                        <i class="fas fa-circle"></i>
                                    </small>
                                @else
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($online->last_activity_at)->diffForHumans() }}</small>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('admin.home.nothing_found') }}
            </div>
        @endif
    </div>
</div>
