<div class="x_panel">
    <div class="x_title">
        <h2>
           @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <br />
    <div class="x_content product-details">

        <div class="product-details-label">{{ __('Проект работает на основе') }} {{ $productLicence['product-name'] }}</div>
        <hr style="margin-top: 10px; margin-bottom: 10px" />
        <form class="form-horizontal">
            <div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Версия') }} {{ $productLicence['product-name'] }}: </label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ config('version.current') }}<br />
                            <a target="_blank" class="small" href="https://exchanger.iexbase.com/news/relizy">история версий</a>
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Версия PHP') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ phpversion() }}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Разработчики продукта') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ $productLicence['product-development'] }}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Ioncube Version') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ $ioncube_version }}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Laravel Framework') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ app()->version() }}</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
