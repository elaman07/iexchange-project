<div class="x_panel">
    <div class="x_title">
        <h2>
            {{ $config['item']['name'] }}
            @include('widgets.includes._header_right')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form style="width:100%;margin-right: 10px;" method="post" action="?favorites_form=in">
            @csrf
            <table class="table table-border-2" style="margin-bottom: 0">
                <thead>
                <tr>
                    <th>{{ __('Направление') }}</th>
                    <th>{{ __('Тип курса') }}</th>
                    <th>{{ __('Позиция Bestchange') }}</th>
                    <th>{{ __('Комиссия') }}</th>
                    <th style="width: 100px;" class="not-sortable">{{ __('Статус') }}</th>
                    <th class="not-sortable"></th>
                </tr>
                </thead>
                <tbody>
                @if($favorites->count() > 0)
                    @foreach($favorites as $detail)
                        <tr @if($detail->direction_exchange->status == 0)style="background-color: rgba(255, 197, 197, 0.34);" @endif>
                            <td>
                                <a href="{{$detail->id_currency1}}/{{$detail->direction_exchange->id}}">
                                    <b>
                                        {{ $detail->direction_exchange->currency1->payment->name.' '.$detail->direction_exchange->currency1->code_currency->name }}
                                        →
                                        {{ $detail->direction_exchange->currency2->payment->name.' '.$detail->direction_exchange->currency2->code_currency->name }}
                                    </b>
                                </a>
                            </td>
                            <td>
                                {{--                                @if(isset($detail->direction_exchange->id_bestchange_rates) and $detail->direction_exchange->enable_bestchange == 1)--}}
                                {{--                                    BestChange--}}
                                {{--                                @elseif(isset($detail->direction_exchange->id_crypto_parser) and $detail->direction_exchange->id_crypto_parser > 0 and $detail->direction_exchange->enable_bestchange == 0)--}}
                                {{--                                    CoinMarketCap--}}
                                {{--                                @endif--}}

                                {{--                                <div style="font-size: 11px; color:#999;"> {{ \Converter::call($detail->direction_exchange)->in()->getCourse() }}</div>--}}
                            </td>
                            <td style="width: 80px">
                                <input type="text"
                                       style="width: 50px;text-align: center;"
                                       name="bestchange_position[{{$detail->direction_exchange->id}}]"
                                       class="form-control" value="{{$detail->direction_exchange->bestchange_position}}">

                            </td>
                            <td style="width: 80px;">
                                <input type="text" style="width: 50px;text-align: center;" name="profit[{{$detail->direction_exchange->id}}]" class="group-input-select form-control" value="{{$detail->direction_exchange->profit}}">
                            </td>
                            <td>
                                <div class="material-switch">
                                    <input id="SwitchOptionPrimary{{$detail->direction_exchange->id}}" name="status[{{$detail->direction_exchange->id}}]" type="checkbox" value="1" @if($detail->direction_exchange->status == 1) checked @endif />
                                    <label for="SwitchOptionPrimary{{$detail->direction_exchange->id}}" class="label-success"></label>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10"><p align="center"><br />{{ __('Ничего не найдено') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
            @if($favorites->count() > 0)
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
        </form>
    </div>
</div>
