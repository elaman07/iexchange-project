<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content product-details">
        <table class="table table-border-2 margin-b0">
            <thead>
            <tr>
                <th>{{ __('ID') }}</th>
                <th>{{ __('Имя') }}</th>
                <th>{{ __('E-mail') }}</th>
                <th>{{ __('Права') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($admin_users as $admin_user)
                <tr>
                    <th>{{$admin_user->id}}</th>
                    <td>
                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$admin_user->id}}&sorting=id"><b>{{$admin_user->name}}</b></a>
                        <div class="widget-color-time">
                            {{\Illuminate\Support\Carbon::parse($admin_user->last_activity_at)->diffForHumans()}}
                        </div>
                    </td>
                    <td>{{$admin_user->email}}</td>
                    <td>
                        @if($admin_user->roles()->pluck('name')->implode(', ') != null)
                            <div>{{$admin_user->roles()->pluck('name')->implode(', ')}}</div>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
