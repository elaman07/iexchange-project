
<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content" layout="column" layout-align="center center">
        <div style="color: #999; padding: 15px; font-size: 12px;">{{ __('Суммарный резерв') }}</div>
        <div style="font-size: 34px; color: #0a983d; padding-bottom: 23px;">
            <a href="{{ admin_base_path('/basic/reserves') }}">{{ iex_number_format($amount_reserve, 2, true) }} USD</a>
        </div>
    </div>
</div>
