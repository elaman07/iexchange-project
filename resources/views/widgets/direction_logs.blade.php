<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        @if($active_errors_count > 0)
            <div class="text-center alert alert-danger">
                {{ __('Проблем с курсами направлений') }} <b>{{ $active_errors_count }}</b>
                <a href="{{ admin_base_path('/basic/direction_exchange?is_error_rate=1&status[]=1') }}">
                    {{ __('Смотреть') }}
                </a>
            </div>
        @else
            <div class="text-center alert alert-success">
                {{ __('Курсы направлений работают стабильно') }}
            </div>
        @endif

        @if(count($logs) > 0)
            <div slim-scroll="" height="300px">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('Дата создания') }}</th>
                        <th>{{ __('Направление') }}</th>
                        <th>{{ __('Текст') }}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <tr>
                            <td>{{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</td>
                            <td>
                                @if(isset($log->direction_exchange))
                                    <a href="{{ admin_base_path('/basic/direction_exchange/'.$log->id_direction_exchange.'/edit') }}">{{ direction_name($log->direction_exchange) }}</a>
                                @else
                                    {{ __('Не определен') }}
                                @endif
                            </td>
                            <td>{{ $log->text }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('Ничего не найдено') }}
            </div>
        @endif
    </div>
</div>
