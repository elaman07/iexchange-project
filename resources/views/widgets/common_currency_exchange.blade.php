
<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content">
      <div layout="row" layout-align="space-around center" style="padding: 20px 10px;">
          <div>
              <div style="color: #999; padding-bottom: 10px; font-size: 12px;">{{ __('Всего получено') }}</div>
              <div style="font-size: 28px;  padding-bottom: 23px; color: #55ba81;font-weight: 500;">
                  $ {{ iex_number_format($in_amount, 2, true) }}
              </div>
          </div>
          <div>
              <div style="color: #999;  padding-bottom: 10px; font-size: 12px;">{{ __('Всего отправлено') }}</div>
              <div style="font-size: 28px; padding-bottom: 23px;color: #de6d62;font-weight: 500;">
                  $ {{ iex_number_format($out_amount, 2, true) }}
              </div>
          </div>
      </div>
    </div>
</div>
