<div class="x_panel">
    <div class="x_title">
        <h2>
            <a href="{{ admin_base_path('/analytics/reserves') }}">{{ $config['item']['name'] }}</a>
            @include('widgets.includes._header_right')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        @if(count($events) > 0)
            <div slim-scroll="" height="300px">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('Дата/Менеджер') }}</th>
                        <th>{{ __('Валюта') }}</th>
                        <th>{{ __('Значение до') }}</th>
                        <th>{{ __('Значение после') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $item)
                        <tr>
                            <td>
                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->id_user.'&sorting=id') }}"><b>{{$item->user->name}}</b></a>
                                <div class="event-reserve-time">
                                    {{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}
                                </div>
                            </td>
                            <td>

                                <a href="{{ admin_base_path('/analytics/reserves?currencies[]='.$item->currency->id) }}">
                                    <b>{{$item->currency->payment->name}} {{$item->currency->code_currency->name}}</b>
                                </a><br />
                                @if($item->type == 0)
                                    <span class="text-danger" style="font-size: 11px">{{ __('Уменьшен') }}</span>
                                @else
                                    <span class="text-success"  style="font-size: 11px">{{ __('Пополнен') }}</span>
                                @endif
                            </td>
                            <td>{{$item->value_before}}</td>
                            <td>{{$item->value_after}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('Ничего не найдено') }}
            </div>
        @endif
    </div>
</div>
