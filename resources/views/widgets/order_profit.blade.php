<style>
    .order-profit-items {
        width: 100%;text-align: center;
    }
    .order-profit-items:last-child {
        border: none;
    }

    .order-profit-label {
        text-align: center;color: #999; padding: 15px; font-size: 12px;
    }
</style>

<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content">

        <div slim-scroll="" height="500px">
            <div layout="row" layout-align="center center">
                {{ __('Фильтровать по') }}:&nbsp;
                <a href="?">{{ __('дням') }}</a>
                &nbsp;
                <a href="?widget=order_profit&action=sum_month">{{ __('месяцам') }}</a>
                &nbsp;
                <a href="?widget=order_profit&action=sum_year">{{ __('годам') }}</a>
            </div>
            <div class="clearfix"></div>
            <hr />
            <div class="clearfix"></div>
            <form action="?" method="GET" style="width: 100%">
                <input type="hidden" name="widget" value="order_profit" />
                <input type="hidden" name="action" value="avg_date_profit" />
                <div layout="row">
                    <div class="form-group col-md-5">
                        <div class="control-label-br">{{ __('Период прибыли (От)') }}</div>
                        <input type="text" name="wid_op_duration_from" class="form-control datetimepicker" value="{{ isset($request_all['wid_op_duration_from']) ?? $request_all['wid_op_duration_from'] }}">
                    </div>
                    <div class="form-group col-md-5">
                        <div class="control-label-br">{{ __('Период прибыли (До)') }}</div>
                        <input type="text" name="wid_op_duration_to" class="form-control datetimepicker" value="{{ isset($request_all['wid_op_duration_to']) ?? $request_all['wid_op_duration_to']}}">
                    </div>
                    <span flex></span>
                    <div class="form-group">
                        <div class="control-label-br">&nbsp;</div>
                        <md-button class="md-raised md-primary" style="margin-top: 0;margin-bottom: 0;padding: 3px 15px;" type="submit">{{ __('Проверить') }}</md-button>
                    </div>
                </div>
                <br />
                <hr />
            </form>

            @if($avg_date == true)
                <b style="font-size: 34px;text-align: center; width: 100%; margin: 0 auto; display: block; padding: 30px 0;">{{ $order_profit }} USD</b>
            @else
                @if(count($order_profit) > 0)
                    <div layout="row" layout-wrap="">
                        @foreach($order_profit as $value)
                            @if(\Illuminate\Support\Carbon::parse($value->created_at)->isToday())
                                <div flex="100" class="order-profit-items">
                                    <div class="order-profit-label">
                                        {{ __('Прибыль за сегодня') }}
                                    </div>
                                    <div style="font-size: 34px; color: #0a983d; padding-bottom: 23px;">
                                        {{ iex_number_format($value->usd, 2, true) }} USD
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr />
                                <div class="clearfix"></div>
                            @else
                                <div flex="50" class="order-profit-items">
                                    <div class="order-profit-label">
                                        @if(empty($action))
                                            @if(\Illuminate\Support\Carbon::parse($value->created_at)->isToday())
                                                {{ __('Прибыль за сегодня') }}
                                            @else
                                                {{ __('Прибыль за') }} {{\Illuminate\Support\Carbon::parse($value->created_at)->translatedFormat('d M Y')}}
                                            @endif
                                        @elseif($action == 'sum_month')
                                            {{ __('Прибыль за') }} {{\Illuminate\Support\Carbon::parse($value->created_at)->translatedFormat('M Y')}}
                                        @elseif($action == 'sum_year')
                                            {{ __('Прибыль за') }} {{\Illuminate\Support\Carbon::parse($value->created_at)->translatedFormat('Y')}}
                                        @endif
                                    </div>
                                    <div style="@if(\Illuminate\Support\Carbon::parse($value->created_at)->isToday())font-size: 34px; color: #0a983d; @else font-size: 28px; @endif padding-bottom: 23px;">
                                        {{ iex_number_format($value->usd, 2, true) }} USD
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @else
                    <div class="empty-content">
                        {{ __('Ничего не найдено') }}
                    </div>
                @endif
            @endif
        </div>
    </div>
</div>
