<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content">
        @if(count($news) > 0)
            <div slim-scroll="" height="300px">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-widget.title') }}</th>
                        <th>{{ __('admin-widget.created_at') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $item)
                        <tr>
                            <td>
                                <a href="{{ isset($item['attributes']['slug']) ? $item['attributes']['slug'] : null }}" target="_blank" style="font-weight: bold;">{{ isset($item['attributes']['title']) ? $item['attributes']['title'] : null }}</a>
                            </td>
                            <td>
                                {{ isset($item['attributes']['created_at']) ? $item['attributes']['created_at'] :null }}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('admin.home.nothing_found') }}
            </div>
        @endif
    </div>
</div>