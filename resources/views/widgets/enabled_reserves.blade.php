<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content">
        @if(count($reserves) > 0)
            <div slim-scroll="" height="300px">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-reserve.currency') }}</th>
                        <th>{{ __('admin-reserve.amount') }}</th>
                        <th>{{ __('admin-reserve.who_updated') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reserves as $reserv)
                        <tr>
                            <td>
                                <a href="{{ admin_base_path('basic/reserves/show/'.$reserv->id) }}" style="font-weight: bold;">{{$reserv->currency->payment->name}} {{$reserv->currency->code_currency->name}}</a>

                                @if($reserv->main_reserves->count() > 0)
                                    <div style="color: #888;margin-left: 5px;">
                                        <small>Связанные резервы</small>
                                        @foreach($reserv->main_reserves as $main_reserve)
                                            <div>
                                                <a href="{{ admin_base_path('basic/reserves/change?id='.$main_reserve->id) }}" style="font-size: 11px;font-weight: bold;">
                                                    - {{ $main_reserve->currency->payment->name }} {{ $main_reserve->currency->code_currency->name }}
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </td>
                            <td>
                                @if($reserv->id_main == 0)
                                    {{$reserv->summa}} {{$reserv->currency->code_currency->name}}
                                @else
                                    <small class="text-danger">
                                        {{ __('admin-reserve.uses_reserve') }}
                                        {{$reserv->main_reserve->currency->payment->name}}
                                        {{$reserv->main_reserve->currency->code_currency->name}}
                                    </small>
                                @endif
                            </td>

                            <td>
                                @if(isset($reserv->user))
                                    <a href="{{admin_base_path('/account/users?action=filter&id='.$reserv->user->id.'&sorting=id')}}"><b>{{$reserv->user->name}}</b></a>
                                @else
                                    <span class="text-muted">{{ __('admin-reserve.system_name') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('admin.home.nothing_found') }}
            </div>
        @endif
    </div>
</div>