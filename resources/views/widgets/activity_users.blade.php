
<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content">
        <div layout="row" layout-align="space-around center" >
            <div style="color: #999; padding: 15px; font-size: 12px;text-align: center;">
                <div style="color: #999; padding: 15px; font-size: 12px;">{{ __('Клиенты в сети') }}</div>
                <div style="font-size: 30px; color: #0a983d; padding-bottom: 23px;">
                    {{ iex_number_format($active_count_10_min, 0, true) }}
                </div>
            </div>
            <div style="color: #999; padding: 15px; font-size: 12px;text-align: center;">
                <div style="color: #999; padding: 15px; font-size: 12px;">{{ __('Гости в сети') }}</div>
                <div style="font-size: 30px; color: #e30000; padding-bottom: 23px;">
                    {{ iex_number_format($active_guest_10_min, 0, true) }}
                </div>
            </div>
        </div>
    </div>
</div>
