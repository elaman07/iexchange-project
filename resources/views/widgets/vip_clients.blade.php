<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content product-details">
        <table class="table table-border-2 margin-b0">
            <thead>
            <tr>
                <th>{{ __('admin.home.new_client_name') }}</th>
                <th>{{ __('admin.home.new_client_email') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>
                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$user->id.'&sorting=id') }}"><b>{{ $user->name }}</b></a>
                        <div style="color: #555;font-size: 11px;">
                            {{ \Illuminate\Support\Carbon::parse($user->last_activity_at)->diffForHumans() }}
                        </div>
                    </td>
                    <td>{{ $user->email }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>