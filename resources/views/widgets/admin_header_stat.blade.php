<style>

    .ws-widgets .ws-widgets-content {
        text-align: center;
    }

    .line-stat-circle {
        font-size: 5px;
        margin-left: 3px;
        margin-right: 3px;
        vertical-align: middle;
    }


    .ws-widget-color-1 .ws-widgets-title, .ws-widget-color-1 .ws-widget-image {
        color: rgba(59,130,246,1);
    }


    .ws-widget-color-2 .ws-widgets-title, .ws-widget-color-2 .ws-widget-image  {
        color: rgba(239,68,68,1);
    }

    .ws-widget-color-3 .ws-widgets-title, .ws-widget-color-3 .ws-widget-image  {
        color: rgba(245,158,11,1);
    }

    .ws-widget-color-4 .ws-widgets-title, .ws-widget-color-4 .ws-widget-image  {
        color: rgba(34,197,94,1);
        padding-right: 0;
    }

    .ws-widget-color-1 .ws-widgets-title, .ws-widget-color-1 .ws-widget-image {
        color: rgba(59,130,246,1);
    }

    .ws-widget-color-1 .ws-widget-image {
        color: rgba(59,130,246,1);
        background-color: rgb(237 244 255);
    }


    .ws-widget-color-2 .ws-widget-image  {
        color: rgba(239,68,68,1);
        background: rgb(255 239 239);
    }

    .ws-widget-color-3 .ws-widget-image  {
        color: rgba(245,158,11,1);
        background: rgb(255 242 221);
    }

    .ws-widget-color-4 .ws-widget-image  {
        color: rgba(34,197,94,1);
        background-color: rgb(224 255 236);
        padding-right: 0 !important;
        margin-right: 0 !important;
    }
    .ws-widget-image {
        padding-right: 25px;
        font-size: 30px;
    }

    .ws-widgets .ws-widget-image {
        border-radius: 50px;
        width: 75px;
        height: 75px;
        text-align: center;
        padding-top: 20px;
        padding-right: 0;
        margin-right: 20px;
    }

    .ws-widgets-stats .ws-widgets-title {
        margin-top: 0;
    }

    .ws-widgets .widget-item-user {
        position: relative;
    }

    .ws-widgets .widget-item-user .widget-item-user-count {
        font-size: 15px;
        color: #0ed20e;
        padding-top: 3px;
        font-weight: 500;
    }

    .ws-widgets-stats .ws-widgets-title {
        margin-top: 0;
    }

    .ws-widgets .ws-widgets-content .category {
        margin-bottom: 0;
        margin-top: 9px;
    }

    .ws-widgets .ws-widgets-title {
        font-size: 39px;
        font-weight: 500;
    }

    .ws-widgets-lines-top {
        border-top: 1px solid #f9f9f9;;
    }

</style>

<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <br />
    <div class="x_content">
        <div hide-xs layout="row" layout-wrap layout-align="space-between stretch">
            <div class="ws-widget-color-1" flex="50">
                <div class="ws-widgets ws-widgets-stats widget-item-user">
                    <div class="ws-widgets-content">
                        <div class="ws-widgets-title">
                            <div class="widget-item-user">
                                <span class="widget-item-user-total" data-toggle="tooltip" data-placement="top" title="{{ __('Всего зарегистрировано') }}">{{ iex_number_format($users_count->total, 0, true) }}</span>
                                @if($users_count->today > 0)
                                    <div class="widget-item-user-count">+ {{ iex_number_format($users_count->today, 0, true) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="category">{{ __('Пользователей') }}</p>
                    </div>
                </div>
            </div>
            <div class="ws-widget-color-2"  flex="50">
                <div class="ws-widgets ws-widgets-stats">
                    <div class="ws-widgets-content">
                        <div class="ws-widgets-title">
                            <span data-toggle="tooltip" data-placement="top" title="{{ __('Кол-во активных клиентов в сети') }}">{{ iex_number_format($active_count_10_min, 0, true) }}</span>
                        </div>
                        <p class="category">{{ __('Клиентов в сети') }}</p>
                    </div>
                </div>
            </div>
            <div class="ws-widget-color-3 ws-widgets-lines-top" flex="50">
                <div class="ws-widgets ws-widgets-stats">
                    <div class="ws-widgets-content">
                        <div class="ws-widgets-title">
                            <div class="widget-item-user">
                                @if(iEXSetting('is_hidden_order_count'))
                                    <span class="text-danger">[{{ __('Счетчик скрыт') }}]</span>
                                @else
                                    <span class="widget-item-user-total" data-toggle="tooltip" data-placement="top" title="{{ __('Общее количество заявок') }}">{{ iex_number_format($order_count->total, 0, true) }}</span>
                                    @if($order_count->success > 0)
                                        <div class="widget-item-user-count">+ {{ iex_number_format($order_count->success, 0, true) }}</div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="category">{{ __('Заявок') }}</p>
                    </div>
                </div>
            </div>
            <div class="ws-widget-color-4 ws-widgets-lines-top" flex="50">
                <div class="ws-widgets ws-widgets-stats">
                    <div class="ws-widgets-content">

                        <div class="ws-widgets-title">
                            <span data-toggle="tooltip" data-placement="top" title="{{ __('Всего направлений') }}">{{ $direction_count->total }}</span>
                        </div>
                        <p class="category">{{ __('Направлений') }}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
