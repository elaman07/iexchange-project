<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div hide-xs layout="row" layout-wrap="" layout-align="center stretch" class="admin-home-layout-1 ws-widgets-clients">
            <div flex="50">
                <div class="ws-widgets ws-widgets-stats">

                    <div class="ws-widgets-content">
                        <h3 class="ws-widgets-title">
                            <span>{{ number_format($user_count, 0,'.', ' ') }}</span>
                        </h3>
                        <p class="category user-widget-clients-label">{{ __('admin-account.users.clients_label') }}</p>
                    </div>
                </div>
            </div>
            <div flex="50">
                <div class="ws-widgets ws-widgets-stats">

                    <div class="ws-widgets-content">
                        <h3 class="ws-widgets-title">
                            <a href="{{ admin_base_path('/account/users?allow_admin=1') }}">{{ number_format($roles_count, 0,'.', ' ') }}</a>
                        </h3>
                        <p class="category user-widget-online-label">{{ __('admin-account.users.allow_admin_label') }}</p>
                    </div>
                </div>
            </div>
            <div class="ws-widgets-lines-top" flex="50">
                <div class="ws-widgets ws-widgets-stats">
                    <div class="ws-widgets-content">
                        <h3 class="ws-widgets-title">
                            <a href="{{ admin_base_path('/account/users?is_ban=1') }}">{{ number_format($banned_count, 0,'.', ' ') }}</a>
                        </h3>
                        <p class="category user-widget-lock-label">{{ __('admin-account.users.blocked_label') }}</p>
                    </div>
                </div>
            </div>
            <div class="ws-widgets-lines-top" flex="50">
                <div class="ws-widgets ws-widgets-stats">
                    <div class="ws-widgets-content">
                        <h3 class="ws-widgets-title">
                            <a href="{{ admin_base_path('/account/users?is_verified=1') }}">{{ number_format($verified_count, 0,'.', ' ') }}</a>
                        </h3>
                        <p class="category user-widget-verify-label">{{ __('admin-account.users.verification_label') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
