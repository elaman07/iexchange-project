<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        @if(count($buyers) > 0)
            <div slim-scroll="" height="300px">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('Клиент') }}</th>
                        <th>{{ __('Кол-во успешных') }}<br/>{{ __('сделок') }}</th>
                        <th>{{ __('Кол-во неудачных') }}<br/>{{ __('сделок') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($buyers as $item)
                            <tr>
                                <td>
                                    <div>
                                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->user->id}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                    </div>
                                    <small class="text-muted">{{$item->user->email}}</small>
                                </td>
                                <td>{{$item->total_orders}}</td>
                                <td>{{$item->user->countFailedTransaction()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('Ничего не найдено') }}
            </div>
        @endif
    </div>
</div>
