<div class="float-right icon-button-widget">
    <a href="{{ admin_base_path(sprintf('/widgets/%s/edit', $config['item']['hash_id'])) }}">
        <i class="fad fa-wrench"></i>
    </a>

    <a href="{{ admin_base_path(sprintf('/widgets/%s/delete', $config['item']['hash_id'])) }}">
        <i class="fad fa-trash" style=" color: #ff5c5c;"></i>
    </a>
</div>
