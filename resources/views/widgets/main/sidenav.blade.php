<style>
    .fuse-vertical-navigation-item-wrapper {
        margin: 24px 12px 0;
    }

    .fuse-vertical-navigation-item-title-wrapper {
        position: relative;
        padding: 10px 16px;
        font-size: 13px;
        font-weight: 500;
        line-height: 20px;
        text-decoration: none;
        border-radius: 6px;
    }

    .fuse-vertical-navigation-item-title {
        color: rgba(129,140,248, 1);
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .05em;
        text-transform: uppercase;
    }

    .fuse-vertical-navigation-item-subtitle {
        font-size: 12px;
        line-height: 1.5;
        font-weight: 400;
        opacity: .5;
        color: #fff;
    }

    .vertical-navigation-linear {
        position: relative;
        display: flex;
        flex-direction: column;
        flex: 1 0 auto;
        -webkit-user-select: none;
        user-select: none;
        margin: 24px 0;
    }

    .vertical-navigation-linear-divider {
        margin: 0 30px;
        border-top: 2px dashed #6c6c6c2e;
    }
</style>

<div class="scroll-view dark bg-gray-900 sidenav-bar">
    <div>
        <div class="w-full p-4" layout="column" layout-align="center center">
            <a href="{{ admin_base_path('/') }}" class="site_title"><span>{{ iEXContentLanguage('sitename', 'iEXExchanger') }}</span></a>
        </div>
        <div class="p-4" layout="column" layout-align="center center">
            <div class="sidenav-admin-avatar">
                <img alt="User avatar" class="img-circle" src="{{ Gravatar::get($currentUser->email, ['size' => 256]) }}">
            </div>
            <div class="w-full mt-6" layout="column" layout-align="center center">
                <div class="sidenav-admin-username">{{ $currentUser->name }}</div>
                <div class="sidenav-admin-rule">{{ $currentUser->roles()->pluck('name')->implode(', ') }}</div>
            </div>
        </div>
    </div>

    <div id="sidebar-menu" class="sidebar-default main_menu_side hidden-print main_menu">
{{--        <input type="text" id="admin-menu-filter" placeholder="Поиск разделов.." class="form-control">--}}
        <div class="menu_section">
            <ul class="nav navigation side-menu side-menu-home">
                @if(config('crypto.panel_operator') == 1)
                    <li>
                        <a md-ink-ripple="'red'" href="{{ admin_base_path('/tx') }}"><i class="fal fa-coins fa-custom-theme"></i>
                            <span class="child_menu_title"> {{ __('Панель оператора') }}</span>
                        </a>
                    </li>
                @endif
            </ul>

            <!-- Избранные пункты -->
            @if(count($favorites) > 0 and iEXSetting('is_not_favorites_left_menu') == 0)
                <div class="vertical-navigation-linear">
                    <div class="vertical-navigation-linear-divider"></div>
                </div>
                <div class="clearfix"></div>

                <ul class="nav navigation side-menu side-menu-home">

                    @foreach($favorites as $favorite)
                        <li>
                            <a md-ink-ripple="'red'" href="{{ admin_base_path($favorite->link) }}"><i class="fal fa-link fa-custom-theme"></i>
                                <span class="child_menu_title"> {{ $favorite->name }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif

            <ul class="nav navigation side-menu" id="search-side-menu" style="margin-top: 0">

                @include('admin._partials._menu')
            </ul>
        </div>
    </div>
</div>
<div class="sidenav-bar-footer" hide-xs>
    <div layout="row" layout-align="start center">
        <div class="sidenav-bar-footer-icon">
            <img alt="User avatar" class="img-circle" src="{{ Gravatar::get($currentUser->email, ['size' => 35]) }}">
        </div>

        <div class="sidenav-bar-footer-value">
            <div class="sidenav-bar-footer-value-name">{{ $currentUser->name }}</div>
            <div class="sidenav-bar-footer-value-email">{{ $currentUser->email }}</div>
        </div>
        <span flex></span>

        <div ng-controller="CommonController  as ctrl">
            <md-menu-bar class="widgets-multi-menu-bar sidenav-bar-menu-bar">
                <md-menu class="widgets-multi-menu-bar">
                    <a href="#" class="sidenav-bar-footer-menu" ng-click="ctrl.openMenu($mdMenu, $event)">
                        <i class="far fa-cog font-size-16"></i>
                    </a>

                    <md-menu-content class="widgets-multi-nested-content" width="3">
                        <md-menu-item>
                            <md-button data-toggle="modal" href="{{ admin_base_path('/account/users/'.$currentUser->id.'/edit') }}">{{ __('Просмотр профиля') }}</md-button>
                        </md-menu-item>
                        <md-divider></md-divider>
                        <md-menu-item>
                            <md-button ng-href="/logout">{{ __('Выйти из аккаунта') }}</md-button>
                        </md-menu-item>

                    </md-menu-content>
                </md-menu>
            </md-menu-bar>
        </div>
    </div>
</div>
