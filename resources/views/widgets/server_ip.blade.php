
<div class="x_panel">
    <div class="x_title">
        <h2>@include('widgets.includes._header')</h2>
    </div>

    <div class="x_content" layout="column" layout-align="center center">
        <div style="color: #999; padding: 15px; font-size: 12px;">{{ __('IP Адрес Сервера') }}</div>
        <b style="font-size: 34px; padding-bottom: 23px;">
            {{ request()->server('SERVER_ADDR') }}
        </b>
    </div>
</div>
