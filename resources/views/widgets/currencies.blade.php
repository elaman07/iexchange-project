<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        @if(count($currencies) > 0)
            <div slim-scroll="" height="300px">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-basic.course_designer.currency') }}</th>
                        <th>{{ __('admin-basic.course_designer.receiving') }} ←</th>
                        <th>{{ __('admin-basic.course_designer.give') }} →</th>
                        <th>{{ __('admin-basic.course_designer.count_order') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($currencies as $currency)
                        <tr>
                            <th>
                                <b>{{$currency->payment->name}} ({{$currency->code_currency->name}})</b>
                            </th>
                            <td>
                                @if(!iEXSetting('is_disabled_count_course_designer'))
                                    @if(isset($currency->currency_analytics))
                                        {{ iex_number_format($currency->currency_analytics->in_amount, $currency->number_format, true) }} {{ $currency->code_currency->sign }}
                                    @else
                                        0
                                    @endif
                                @else
                                    <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                                @endif
                            </td>
                            <td>
                                @if(!iEXSetting('is_disabled_count_course_designer'))
                                    @if(isset($currency->currency_analytics))
                                        {{ iex_number_format($currency->currency_analytics->out_amount, $currency->number_format, true) }} {{ $currency->code_currency->sign }}
                                    @else
                                        0
                                    @endif
                                @else
                                    <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                                @endif
                            </td>
                            <td>
                                @if(!iEXSetting('is_disabled_count_course_designer'))
                                    <div>
                                        <a href="{{route('admin.application')}}?action=filter&currencies_in={{$currency->id}}&status[]=4">
                                            @if(isset($currency->currency_analytics))
                                                Всего отдали: <b>{{ iex_number_format($currency->currency_analytics->in_order_count, 0, true) }}</b>
                                            @else
                                                Всего отдали: <b>0</b>
                                            @endif

                                        </a>
                                    </div>

                                    <div>
                                        <a href="{{route('admin.application')}}?action=filter&currencies_out={{$currency->id}}&status[]=4">
                                            @if(isset($currency->currency_analytics))
                                                Всего получили: <b>{{ iex_number_format($currency->currency_analytics->out_order_count, 0, true) }}</b>
                                            @else
                                                Всего получили: <b>0</b>
                                            @endif
                                        </a>
                                    </div>
                                @else
                                    <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="empty-content">
                {{ __('admin.home.nothing_found') }}
            </div>
        @endif
    </div>
</div>
