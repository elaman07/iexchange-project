<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="padding: 10px 20px 20px;">
        <form action="{{ admin_base_path('/applications') }}" method="GET"  layout="row" layout-align="center center">
            <input type="hidden" name="send_action" value="on">
            <input type="hidden" name="write_to_cache" value="on">
            <input class="form-control" placeholder="{{ __('Введите ID заявки') }}..." type="text" name="id" value="{{ old('id') }}">
            <md-button class="md-raised md-primary" type="submit">
                <i class="far fa-search"></i>
            </md-button>
        </form>

        @if(count($logs) > 0)
            <hr />
            <b>{{ __('История поиска') }}: {{ count($logs) }}</b>

            @can('admin_settings')
                <a href="?widget=order_id&cache_clear=true" class="text-danger pull-right small">{{ __('Очистить данные') }}</a>
            @endcan


            <br />  <br />
            @foreach($logs as $log)
                <a href="{{ admin_base_path('/applications?id='.$log['order_id']) }}">{{ $log['order_id'] }}</a> <small class="event-reserve-time">({{ \Illuminate\Support\Carbon::parse($log['created_at'])->diffForHumans() }})</small>,
            @endforeach
        @endif


    </div>
</div>
