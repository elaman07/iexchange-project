<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="padding: 10px 20px 20px;">

        @can('admin_cache')
            <form action="?widget_form=cache_control" method="POST"  layout="row" layout-align="center center">
                @csrf
                <select class="form-control selectpicker" name="wid_cache_control" data-live-search="true">
                    <option value="all">{{ __('Очистить весь кэш') }}</option>
                    <option value="clear_menu">{{ __('Очистить кэш меню') }}</option>
                    <option value="clear_news_iexexchanger">{{ __('Очистить кэш новостей') }}</option>
                    <option value="clear_reserves">{{ __('Очистить кэш суммарного резерва') }}</option>
                    <option value="clear_faq">{{ __('Очистить кэш Вопросов и ответов') }}</option>
                    <option value="clear_social_auth">{{ __('Очистить кэш Системы авторизаций') }}</option>
                    <option value="clear_cache_bestchange_rates">{{ __('Очистить кэш BestChange парсера') }}</option>
                    <option value="clear_cache_partners">{{ __('Очистить кэш Партнерам') }}</option>

                @if(\Module::find('Cloudflare')->isEnabled())
                        <option value="cloudflare">{{ __('Очистить кэш Cloudflare') }}</option>
                    @endif
                </select>

                <md-button class="md-raised md-primary" style="padding: 0" type="submit">{{ __('Очистить') }}</md-button>
            </form>
        @endcan
    </div>
</div>
