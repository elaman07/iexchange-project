<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content" style="padding: 10px 20px 20px;">
        <form action="?avg_direction_course" method="GET">
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Направление</div>
                        <select class="form-control selectpicker" name="wid_adc_direction" data-live-search="true">
                            @foreach($direction_exchange as $value)
                                <option value="{{ $value->id }}">{{ $value->tech_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="form-group col-md-12">
                            <div class="control-label-br">Период проверки (ОТ)</div>
                            <input type="text" name="wid_adc_duration_from" class="form-control datetimepicker" value="{{ old('wid_adc_duration_from') }}">
                        </div>
                        <br />
                        <div class="form-group col-md-12">
                            <div class="control-label-br">Период проверки (До)</div>
                            <input type="text" name="wid_adc_duration_to" class="form-control datetimepicker" value="{{ old('wid_adc_duration_to') }}">
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <hr />

            <md-button class="md-raised md-primary" style="padding: 0" type="submit">Проверить</md-button>

            @if($direction_exchange_result > 0)
                <div class="alert alert-success">
                    Результат: 1 = {{ $direction_exchange_result }}
                </div>
            @endif
        </form>
    </div>
</div>
