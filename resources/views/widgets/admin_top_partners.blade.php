<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
    </div>

    <div class="x_content product-details">
        <table class="table table-border-2 margin-b0">
            <thead>
            <tr>
                <th></th>
                <th>{{ __('admin.home.top_partners_partner') }}</th>
                <th>{{ __('admin.home.top_partners_exchange') }}</th>
                <th>{{ __('admin.home.top_partners_balance') }}</th>
                <th>{{ __('admin.home.top_partners_total_earned') }}</th>
            </tr>

            </thead>
            <tbody>
            @if(count($referrals_logs) > 0)
                @foreach($referrals_logs as $referrals_log)
                    <tr>
                        <td>
                            @if($referrals_log->user_admin->is_pay_referral == 1)
                                <a href="?actions=top_partners&user_id={{ $referrals_log->user_admin->id }}&type=on">
                                    <i class="fas fa-power-off color-red-alt" data-toggle="tooltip" title="Включить начисление бонусов"> </i>
                                </a>
                            @else
                                <a href="?actions=top_partners&user_id={{ $referrals_log->user_admin->id }}&type=off">
                                    <i class="fas fa-power-off color-green-alt" data-toggle="tooltip" title="Отключить начисление бонусов"> </i>
                                </a>
                            @endif
                        </td>
                        <td>
                            <div>
                                <a data-toggle="tooltip" data-placement="top" title="Последний визит: {{ \Illuminate\Support\Carbon::parse($referrals_log->user_admin->last_activity_at)->diffForHumans() }}" href="{{config('admin.directory')}}/account/users?action=filter&id={{$referrals_log->user_admin->id}}&sorting=id">
                                    <b>{{$referrals_log->user_admin->name}}</b>
                                </a>
                            </div>
                            <small>{{ $referrals_log->user_admin->email }}</small>
                        </td>
                        <td>{{ $referrals_log->count_num }} /
                            <span data-toggle="tooltip" data-placement="top" title="{{ __('admin.home.top_partners_exchange_today') }}">{{ $referrals_log->today }}</span>
                        </td>
                        <td> {{ number_format($referrals_log->user_admin->user_balance->balance, 2,'.',' ') }} {{ config('crypto.currency_payout_sign') }}</td>
                        <td> {{ number_format($referrals_log->total_amount, 2,'.',' ') }} {{ config('crypto.currency_payout_sign') }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9"><p align="center"><br />{{ __('admin.home.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
