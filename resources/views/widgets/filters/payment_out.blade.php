<div class="form-group">
    <label class="control-label col-md-6">Название ПС Получаю</label>
    <div class="col-md-6">
        {!! Form::select('payment_out[]', $payments, is_filter_search($filter, 'payment_out'),
            ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>