<div class="form-group">
    <label class="control-label col-md-6">ID заявки (От и До)</label>
    <div class="col-md-6">
        От: {{ Form::input('text', 'from_order_id', is_filter_search($filter, 'from_order_id'), ['class' => 'form-control', 'style' => 'width: 50px;display: inline-block;margin-right: 10px;text-align: center']) }}
        До: {{ Form::input('text', 'to_order_id', is_filter_search($filter, 'to_order_id'), ['class' => 'form-control', 'style' => 'width: 50px;display: inline-block;text-align: center']) }}
    </div>
</div>