<div class="form-group">
    <label class="control-label col-md-6">Код валюты Получаю (Обоз.)</label>
    <div class="col-md-6">
        {!! Form::select('designation_out[]', $designation_code, is_filter_search($filter, 'designation_out'),
         ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
