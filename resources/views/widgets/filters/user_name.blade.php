<div class="form-group">
    <label class="control-label col-md-6">Имя</label>
    <div class="col-md-6">
        {{ Form::input('text', 'user_name', is_filter_search($filter, 'user_name'), ['class' => 'form-control']) }}
        <div class="form-check">
            <input class="form-check-input" id="checkbox3" value="1" type="checkbox" name="checkbox_full_name" @if(isset($filter['checkbox_full_name'])) checked @endif>
            <label class="form-check-label" for="checkbox3">Точное имя</label>
        </div>
    </div>
</div>
