<div class="form-group">
    <label class="control-label col-md-6">E-mail партнера</label>
    <div class="col-md-6">
        {{ Form::input('text', 'partner_email', is_filter_search($filter, 'partner_email'), ['class' => 'form-control']) }}
        <div class="form-check">
            <input class="form-check-input" id="checkbox_partner_email" value="1" type="checkbox" name="checkbox_partner_email" @if(isset($filter['checkbox_partner_email'])) checked @endif>
            <label class="form-check-label" for="checkbox_partner_email">Точный e-mail</label>
        </div>
    </div>
</div>
