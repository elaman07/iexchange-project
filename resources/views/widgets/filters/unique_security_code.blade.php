
<div class="form-group">
    <label class="control-label col-md-6">Уникальный номер</label>
    <div class="col-md-6">
        {{ Form::input('text', 'unique_security_code', is_filter_search($filter, 'unique_security_code'), ['class' => 'form-control']) }}
    </div>
</div>