<div class="form-group">
    <label class="control-label col-md-6">ID партнера</label>
    <div class="col-md-6">
        {{ Form::input('text', 'partner_id', is_filter_search($filter, 'partner_id'), ['class' => 'form-control']) }}
    </div>
</div>