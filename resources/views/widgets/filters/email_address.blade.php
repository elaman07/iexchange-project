<div class="form-group">
    <label class="control-label col-md-6">E-mail адрес</label>
    <div class="col-md-6">
        {{ Form::input('text', 'email_address', is_filter_search($filter, 'email_address'), ['class' => 'form-control']) }}
        <div class="form-check">
            <input class="form-check-input" id="checkbox2" value="1" type="checkbox" name="checkbox_full_email" @if(isset($filter['checkbox_full_email'])) checked @endif>
            <label class="form-check-label" for="checkbox2">Точный email</label>
        </div>
    </div>
</div>
