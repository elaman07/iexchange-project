<div class="form-group">
    <label class="control-label col-md-6">Верифицированный</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary5" name="is_verified" type="checkbox" value="1" @if(is_filter_search($filter, 'is_verified') == 1) checked @endif />
            <label for="SwitchOptionPrimary5" class="label-primary"></label>
        </div>
    </div>
</div>