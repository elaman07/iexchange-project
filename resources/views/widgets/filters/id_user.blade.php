<div class="form-group">
    <label class="control-label col-md-6">ID пользователя</label>
    <div class="col-md-6">
        {{ Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) }}
    </div>
</div>