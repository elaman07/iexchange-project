<div class="form-group">
    <label class="control-label col-md-6">Номер телефона</label>
    <div class="col-md-6">
        {{ Form::input('text', 'phone', is_filter_search($filter, 'phone'), ['class' => 'form-control']) }}
        <div class="form-check">
            <input class="form-check-input" id="checkbox_full_phone" value="1" type="checkbox" name="checkbox_full_phone" @if(isset($filter['checkbox_full_phone'])) checked @endif>
            <label class="form-check-label" for="checkbox_full_phone">Точный номер телефона</label>
        </div>
    </div>
</div>
