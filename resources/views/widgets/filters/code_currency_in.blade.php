<div class="form-group">
    <label class="control-label col-md-6">Код валюты Отдаю</label>
    <div class="col-md-6">
        {!! Form::select('code_currency_in[]', $code_currencies, is_filter_search($filter, 'code_currency_in'),
           ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
