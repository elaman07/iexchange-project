<div class="form-group">
    <label class="control-label col-md-6">Дата закрытия (От)</label>
    <div class="col-md-6">
        {{ Form::input('text', 'from_updated_at', is_filter_search($filter, 'from_updated_at'), ['class' => 'datetimepicker form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-6">Дата закрытия (До)</label>
    <div class="col-md-6">
        {{ Form::input('text', 'to_updated_at', is_filter_search($filter, 'to_updated_at'), ['class' => 'datetimepicker form-control']) }}
    </div>
</div>