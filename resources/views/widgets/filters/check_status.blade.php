<div class="form-group">
    <label class="control-label col-md-6">Статус отправки чека</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary3" name="check_status" type="checkbox" value="1" @if(is_filter_search($filter, 'check_status') == 1) checked @endif />
            <label for="SwitchOptionPrimary3" class="label-primary"></label>
        </div>
    </div>
</div>