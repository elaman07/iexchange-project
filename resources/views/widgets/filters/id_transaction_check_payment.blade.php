<div class="form-group">
    <label class="control-label col-md-6">ID транзакции проверки оплаты</label>
    <div class="col-md-6">
        {{ Form::input('text', 'id_transaction_check_payment', is_filter_search($filter, 'id_transaction_check_payment'), ['class' => 'form-control']) }}
    </div>
</div>