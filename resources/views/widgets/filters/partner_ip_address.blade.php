<div class="form-group">
    <label class="control-label col-md-6">IP адрес партнера</label>
    <div class="col-md-6">
        {{ Form::input('text', 'partner_ip_address', is_filter_search($filter, 'partner_ip_address'), ['class' => 'form-control']) }}
    </div>
</div>