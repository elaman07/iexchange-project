<div class="form-group">
    <label class="control-label col-md-6">Страна пользователя</label>
    <div class="col-md-6">
        {!! Form::select('user_country[]', $allOrderCountries, is_filter_search($filter, 'user_country'),
         ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>