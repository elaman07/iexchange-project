<div class="form-group">
    <label class="control-label col-md-6">ID транзакции</label>
    <div class="col-md-6">
        {{ Form::input('text', 'id_transaction', is_filter_search($filter, 'id_transaction'), ['class' => 'form-control']) }}
    </div>
</div>