<div class="form-group">
    <label class="control-label col-md-6">Название валюты Отдаю</label>
    <div class="col-md-6">
        {!! Form::select('currencies_in[]', $currencies, is_filter_search($filter, 'currencies_in'),
        ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
