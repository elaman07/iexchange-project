<div class="form-group">
    <label class="control-label col-md-6">Публичный ID</label>
    <div class="col-md-6">
        {{ Form::input('text', 'id_public', is_filter_search($filter, 'id_public'), ['class' => 'form-control']) }}
    </div>
</div>
