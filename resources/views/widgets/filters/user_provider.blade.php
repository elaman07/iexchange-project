<div class="form-group">
    <label class="control-label col-md-6">Провайдер</label>
    <div class="col-md-6">
        {!! Form::select('user_provider[]', config('auth.social_providers'), is_filter_search($filter, 'user_provider'),
            ['class' => 'selectpicker', 'data-width' => '100%', 'data-live-search' => 'true', 'multiple']) !!}
    </div>
</div>
