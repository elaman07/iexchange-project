<div class="form-group">
    <label class="control-label col-md-6">Только заявки из Telegram</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="is_telegram" name="is_telegram" type="checkbox" value="1" @if(is_filter_search($filter, 'is_telegram') == 1) checked @endif />
            <label for="is_telegram" class="label-primary"></label>
        </div>
    </div>
</div>