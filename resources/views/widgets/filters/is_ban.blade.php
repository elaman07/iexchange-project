<div class="form-group">
    <label class="control-label col-md-6">Забанен</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary4" name="is_ban" type="checkbox" value="1" @if(is_filter_search($filter, 'is_ban') == 1) checked @endif />
            <label for="SwitchOptionPrimary4" class="label-primary"></label>
        </div>
    </div>
</div>