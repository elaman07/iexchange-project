<div class="form-group">
    <label class="control-label col-md-6">Имя партнера</label>
    <div class="col-md-6">
        {{ Form::input('text', 'partner_name', is_filter_search($filter, 'partner_name'), ['class' => 'form-control']) }}
        <div class="form-check">
            <input class="form-check-input" id="checkbox_partner_name" value="1" type="checkbox" name="checkbox_partner_name" @if(isset($filter['checkbox_partner_name'])) checked @endif>
            <label class="form-check-label" for="checkbox_partner_name">Точное имя</label>
        </div>
    </div>
</div>
