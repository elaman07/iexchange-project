<div class="form-group">
    <label class="control-label col-md-6">Номер денежного перевода</label>
    <div class="col-md-6">
        {{ Form::input('text', 'num_transaction', is_filter_search($filter, 'num_transaction'), ['class' => 'form-control']) }}
    </div>
</div>
