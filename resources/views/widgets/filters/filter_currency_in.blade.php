<div class="form-group">
    <label class="control-label col-md-6">Фильтр валюты Отдаю</label>
    <div class="col-md-6">
        {!! Form::select('filter_currency_in[]', $filter_currency, is_filter_search($filter, 'filter_currency_in'),
           ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
