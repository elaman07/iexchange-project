<div class="form-group">
    <label class="control-label col-md-6">Название валюты Получаю</label>
    <div class="col-md-6">
        {!! Form::select('currencies_out[]', $currencies, is_filter_search($filter, 'currencies_out'),
       ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
