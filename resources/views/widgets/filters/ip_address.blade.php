<div class="form-group">
    <label class="control-label col-md-6">IP адрес</label>
    <div class="col-md-6">
        {{ Form::input('text', 'ip_address', is_filter_search($filter, 'ip_address'), ['class' => 'form-control']) }}
    </div>
</div>