<div class="form-group">
    <label class="control-label col-md-6">Код валюты Получаю</label>
    <div class="col-md-6">
        {!! Form::select('code_currency_out[]', $code_currencies, is_filter_search($filter, 'code_currency_out'),
         ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
