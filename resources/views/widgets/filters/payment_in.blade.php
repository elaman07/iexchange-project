<div class="form-group">
    <label class="control-label col-md-6">Название ПС Отдаю</label>
    <div class="col-md-6">
        {!! Form::select('payment_in[]', $payments, is_filter_search($filter, 'payment_in'),
            ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>