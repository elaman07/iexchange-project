<div class="form-group">
    <label class="control-label col-md-6">Реферальный хэш партнера</label>
    <div class="col-md-6">
        {{ Form::input('text', 'referral_hash', is_filter_search($filter, 'referral_hash'), ['class' => 'form-control']) }}
    </div>
</div>