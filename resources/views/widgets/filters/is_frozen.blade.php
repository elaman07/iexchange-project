<div class="form-group">
    <label class="control-label col-md-6">Замороженные заявки</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary-is-frozen" name="is_frozen" type="checkbox" value="1" @if(is_filter_search($filter, 'is_frozen') == 1) checked @endif />
            <label for="SwitchOptionPrimary-is-frozen" class="label-primary"></label>
        </div>
    </div>
</div>