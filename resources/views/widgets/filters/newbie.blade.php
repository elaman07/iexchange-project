<div class="form-group">
    <label class="control-label col-md-6">Новичок</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary1" name="newbie" type="checkbox" value="1" @if(is_filter_search($filter, 'newbie') == 1) checked @endif />
            <label for="SwitchOptionPrimary1" class="label-primary"></label>
        </div>
    </div>
</div>