<div class="form-group">
    <label class="control-label col-md-6">Партнерские выплаты отключены</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary3-4" name="is_not_partner" type="checkbox" value="1" @if(is_filter_search($filter, 'is_not_partner') == 1) checked @endif />
            <label for="SwitchOptionPrimary3-4" class="label-primary"></label>
        </div>
    </div>
</div>