<div class="form-group">
    <label class="control-label col-md-6">Версия сайта</label>
    <div class="col-md-6">
        {!! Form::select('devices[]', $allDevices, is_filter_search($filter, 'devices'),
       ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>