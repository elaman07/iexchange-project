<div class="form-group">
    <label class="control-label col-md-6">Сумма платежа через мерчант</label>
    <div class="col-md-6">
        {!! Form::select('merchant_overpayment[]', $paymentMerchantStatusses, is_filter_search($filter, 'merchant_overpayment'),
            ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>