<div class="form-group">
    <label class="control-label col-md-6">Менеджер</label>
    <div class="col-md-6">
        {!! Form::select('manager[]', $allOrderManagers, is_filter_search($filter, 'manager'),
        ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>