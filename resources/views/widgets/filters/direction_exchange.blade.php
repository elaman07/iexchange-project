<div class="form-group">
    <label class="control-label col-md-6">Направление обмена</label>
    <div class="col-md-6">
        {!! Form::select('direction_exchange[]', $direction, is_filter_search($filter, 'direction_exchange'),
        ['class' => 'selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
    </div>
</div>
