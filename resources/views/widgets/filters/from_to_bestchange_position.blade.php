<div class="form-group">
    <label class="control-label col-md-6">Заявки по позициям из Bestchange</label>
    <div class="col-md-6">
        От: {{ Form::input('text', 'from_bestchange_position', is_filter_search($filter, 'from_bestchange_position'), ['class' => 'form-control', 'style' => 'width: 50px;display: inline-block;margin-right: 10px;text-align: center']) }}
        До: {{ Form::input('text', 'to_bestchange_position', is_filter_search($filter, 'to_bestchange_position'), ['class' => 'form-control', 'style' => 'width: 50px;display: inline-block;text-align: center']) }}
    </div>
</div>