<div class="form-group">
    <label class="control-label col-md-6">ID транзакции мерчанта</label>
    <div class="col-md-6">
        {{ Form::input('text', 'id_transaction_merchant', is_filter_search($filter, 'id_transaction_merchant'), ['class' => 'form-control']) }}
    </div>
</div>