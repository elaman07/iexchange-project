<div class="form-group">
    <label class="control-label col-md-6">Мошенники (Только из базы сайта)</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary-is-local-scam" name="is_local_scam" type="checkbox" value="1" @if(is_filter_search($filter, 'is_local_scam') == 1) checked @endif />
            <label for="SwitchOptionPrimary-is-local-scam" class="label-primary"></label>
        </div>
    </div>
</div>