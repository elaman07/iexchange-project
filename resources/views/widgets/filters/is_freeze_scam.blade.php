
<div class="form-group">
    <label class="control-label col-md-6">Арестованные заявки</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary-is_freeze_scam" name="is_freeze_scam" type="checkbox" value="1" @if(is_filter_search($filter, 'is_freeze_scam') == 1) checked @endif />
            <label for="SwitchOptionPrimary-is_freeze_scam" class="label-primary"></label>
        </div>
    </div>
</div>