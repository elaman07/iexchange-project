<div class="form-group">
    <label class="control-label col-md-6">Статус заявки</label>
    <div class="col-md-6">
        {!! Form::select('status[]', $allOrderStatuses, is_filter_search($filter, 'status'),
            ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
    </div>
</div>