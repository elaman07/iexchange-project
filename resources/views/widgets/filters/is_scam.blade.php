<div class="form-group">
    <label class="control-label col-md-6">Мошенники</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary2" name="is_scam" type="checkbox" value="1" @if(is_filter_search($filter, 'is_scam') == 1) checked @endif />
            <label for="SwitchOptionPrimary2" class="label-primary"></label>
        </div>
    </div>
</div>
