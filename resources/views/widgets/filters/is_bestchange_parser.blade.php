<div class="form-group">
    <label class="control-label col-md-6">Заявки по курсам из Bestchange</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary5-3" name="is_bestchange_parser" type="checkbox" value="1" @if(is_filter_search($filter, 'is_bestchange_parser') == 1) checked @endif />
            <label for="SwitchOptionPrimary5-3" class="label-primary"></label>
        </div>
    </div>
</div>