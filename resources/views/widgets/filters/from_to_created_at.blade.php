<div class="form-group">
    <label class="control-label col-md-6">Дата создания (От)</label>
    <div class="col-md-6">
        {{ Form::input('text', 'from_created_at', is_filter_search($filter, 'from_created_at'), ['class' => 'datetimepicker form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-6">Дата создания (До)</label>
    <div class="col-md-6">
        {{ Form::input('text', 'to_created_at', is_filter_search($filter, 'to_created_at'), ['class' => 'datetimepicker form-control']) }}
    </div>
</div>