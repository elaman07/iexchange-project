<div class="form-group">
    <label class="control-label col-md-6">Telegram</label>
    <div class="col-md-6">
        {{ Form::input('text', 'telegram', is_filter_search($filter, 'telegram'), ['class' => 'form-control']) }}
    </div>
</div>