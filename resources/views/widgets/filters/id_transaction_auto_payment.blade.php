<div class="form-group">
    <label class="control-label col-md-6">ID транзакции автовыплаты</label>
    <div class="col-md-6">
        {{ Form::input('text', 'id_transaction_auto_payment', is_filter_search($filter, 'id_transaction_auto_payment'), ['class' => 'form-control']) }}
    </div>
</div>