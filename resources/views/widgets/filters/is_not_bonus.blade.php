<div class="form-group">
    <label class="control-label col-md-6">Кэшбэк отключен</label>
    <div class="col-md-6">
        <div class="material-switch switch-input-1">
            <input id="SwitchOptionPrimary3-3" name="is_not_bonus" type="checkbox" value="1" @if(is_filter_search($filter, 'is_not_bonus') == 1) checked @endif />
            <label for="SwitchOptionPrimary3-3" class="label-primary"></label>
        </div>
    </div>
</div>
