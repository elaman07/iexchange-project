<div class="x_panel">
    <div class="x_title">
        <h2>
            <a href="{{ admin_base_path('/gateways/merchant') }}">@include('widgets.includes._header')</a>
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content product-details">
        <div slim-scroll="" height="300px">
            <table class="table table-border-2 margin-b0">
                <thead>
                <tr>
                    <th>{{ __('Название') }}</th>
                    <th>{{ __('Alias') }}</th>
                    <th>{{ __('Кол-во заявок') }}</th>
                    <th>{{ __('Объем в USD') }}</th>
                </tr>

                </thead>
                <tbody>
                    @if(count($merchants) > 0)
                    @foreach($merchants as $merchant)
                        <tr @if($merchant->status == 1) style="border-left: 3px solid #bcffbc" @else style="border-left: 3px solid #ffbcbc;background: #fff4f4" @endif>
                            <th>
                                <a href="{{ admin_base_path('gateways/merchant/'.$merchant->id.'/edit') }}">
                                    {{ $merchant->name }} &nbsp;<i class="fal fa-pencil"></i>
                                </a>
                            </th>
                            <td>{{ isset($merchant->gateway) ? $merchant->gateway->alias : '' }} (v{{ isset($merchant->gateway) ? $merchant->gateway->version : 0 }})</td>

                            <td>
                                {{ $merchant->tasks->where('status', '=', 4)->count() }}
                            </td>


                            <td>
                                ${{ iex_number_format($merchant->total) }}
                            </td>

                        </tr>
                    @endforeach
                @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
