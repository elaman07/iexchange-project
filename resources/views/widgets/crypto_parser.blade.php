<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <br />
    <div class="x_content product-details">

        <form class="form-horizontal">
            <div>

                @if(!empty($parser_cron))
                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{ __('Последнее обновление') }}:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">{{ \Illuminate\Support\Carbon::parse($parser_cron['last-update'])->diffForHumans() }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">{{ __('Время обновления') }}:</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">{{ $parser_cron['time'] }} sec.</p>
                            <small>
                                <a href="{{ admin_base_path('/crypto/log_update_courses') }}">{{ __('История обновлений') }}</a>
                            </small>
                        </div>
                    </div>

                    <hr />
                @endif

                    @if(!empty($bestchange_cron))
                        <div class="form-group">
                            <label class="col-sm-4 control-label">{{ __('Последнее обновление BestChange') }}:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ \Illuminate\Support\Carbon::parse($bestchange_cron['last-update'])->diffForHumans() }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">{{ __('Время обновления BestChange') }}:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bestchange_cron['time'] }} sec.</p>
                                <small>
                                    <a href="{{ admin_base_path('/crypto/log_update_courses') }}">{{ __('История обновлений') }}</a>
                                </small>
                            </div>
                        </div>

                        <hr />
                    @endif


                    <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Всего источников') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{count($all_groups)}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Активных источников') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ $active_source }}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Всего курсов') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ $all_count }}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">{{ __('Активных курсов') }}:</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">{{ $active_count }}</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
