
<div class="x_panel">
    <div class="x_title">
        <h2>
            <a href="{{ admin_base_path('/analytics/exchange') }}">{{ $config['item']['name'] }}</a>
            @include('widgets.includes._header_right')
        </h2>
    </div>

    <div class="x_content" layout="column" layout-align="space-around stretch" style="margin: 0">
        @if(count($common_amount) > 0)
            @foreach($common_amount as $item)
                <div class="home-amount-today" layout="row" layout-align="start center">
                    <div>
                        <div class="subtext">
                            @if(\Illuminate\Support\Carbon::parse($item->created_at)->isToday())
                                {{ __('admin.home.today') }}
                            @elseif(\Illuminate\Support\Carbon::parse($item->created_at)->isYesterday())
                                {{ __('admin.home.yesterday') }}
                            @else
                                {{\Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y')}}
                            @endif
                        </div>
                        $ {{ iex_number_format($item->usd, 0, true) }}
                    </div>
                    <span flex=""></span>
                    <div class="list-operators">
                        @foreach($list_roles as $user)
                            @if($user->managerHistory('count', $item->created_at) > 0)
                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$user->id) }}">{{$user->name}}</a>
                                -
                                <small>{{ __('admin.home.amount_exchange_order_label') }}: {{ $user->managerHistory('count', $item->created_at) }}</small>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        @else
            <div class="empty-content">
                {{ __('admin.home.nothing_found') }}
            </div>
        @endif
    </div>
</div>