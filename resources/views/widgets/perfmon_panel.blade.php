<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <br />
    <div class="x_content product-details">
        <style>
            .alert-base {
                color: #333;
                position: relative;
                padding-left: 20px;
                padding-right: 20px;
                -webkit-box-shadow: 0 1px 3px rgb(0 0 0 / 12%), 0 1px 2px rgb(0 0 0 / 24%);
                box-shadow: 0 1px 3px rgb(0 0 0 / 12%), 0 1px 2px rgb(0 0 0 / 24%);
            }

            .alert-base-danger {
                background-color: #FBE9E7;
                border-color: #FF5722;
            }

            .alert-base-info {
                background-color: #E0F7FA;
                border-color: #00BCD4;
            }

            .alert-base-warning {
                background-color: #FFF3E0;
                border-color: #FF9800;
            }

            .alert-base-success {
                background-color: #E8F5E9;
                border-color: #4CAF50;
            }

            .alert-perfmon-content .alert-base{
                margin-bottom: 10px;
                font-size: 13px;
                line-height: 1.4285715;
            }

            .panel-body-perfmon-text {
                font-size: 14px;
                padding: 15px 20px;
                line-height: 1.5285715;
            }
        </style>


        <div class="panel-body panel-body-perfmon-text">
            Этот виджет производит анализ настроек скрипта и настроек групп пользователей и по их результатам вы получаете рекомендации по отключению или изменению тех или иных настроек.
        </div>
        <hr />

        <div class="panel-body alert-perfmon-content">

            @if(in_array(config('admin.route_path'), ['admin', 'iexadmin']))
                <div class="alert alert-base alert-base-danger">На вашем сайте используется небезопасный путь к админ-панели "/{{ config('admin.route_path') }}". Настроятельно рекомендуем вам изменить путь, для этого зайдите в <b>Настройки » Настройки системы</b> перейдите во вкладку <b>Безопасность</b> и найдите поле <b>URL админ панели</b></div>
            @endif


            @if($memory_limit < (500 * 1024 * 1024))
                <div class="alert alert-base alert-base-danger">
                    Для корректной работы сайта, на сервере увеличьте объем памяти <code>memory_limit</code>. Рекомендуются установить <code>memory_limit</code> не ниже 500MB.
                    Если вы незнаете как установить, свяжитесь с вашим хостинг провайдером и попросите внести изменения на сервере.
                </div>
            @endif
        </div>
    </div>
</div>
