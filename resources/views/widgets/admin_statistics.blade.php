<div class="x_panel">
    <div class="x_title">
        <h2>
            @include('widgets.includes._header')
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content tab-capitalize">
        @can('admin_statistics')
            <md-tabs md-dynamic-height md-border-bottom>
                <md-tab label="{{ __('Статистика заявок') }}">
                    <md-content>
                        <table style="    border-width: 2px;" class="table table-border-2 margin-b0">
                            <tbody>
                            <tr>
                                <th>&nbsp;</th>
                                <th style="text-align: center">{{ __('Общее') }}</th>
                                <th style="text-align: center">{{ __('Выполнен') }}</th>
                                <th style="text-align: center">{{ __('Отменен') }}</th>
                            </tr>

                            @foreach($stat_result as $item)
                                <tr>
                                    <td>{{ $item['n'] }}:</td>
                                    <td align="center">
                                        @if(!is_array($item['u']))
                                            <a href="{{ admin_base_path('/applications/?updated_at='.$item['u']) }}">{{ iex_number_format($item['a'], 0, true) }}</a>
                                        @else
                                            <a href="{{ admin_base_path('/applications/?from_updated_at='.$item['u'][0].'&to_updated_at='.$item['u'][1]) }}">{{ iex_number_format($item['a'], 0, true) }}</a>
                                        @endif
                                    </td>
                                    <td align="center">
                                        @if(!is_array($item['u']))
                                            <a href="{{ admin_base_path('/applications/?status[]=4&updated_at='.$item['u']) }}">{{ iex_number_format($item['s'], 0, true) }}</a>
                                        @else
                                            <a href="{{ admin_base_path('/applications/?status[]=4&from_updated_at='.$item['u'][0].'&to_updated_at='.$item['u'][1]) }}">{{ iex_number_format($item['s'], 0, true) }}</a>
                                        @endif
                                    </td>
                                    <td align="center">
                                        @if(!is_array($item['u']))
                                            <a href="{{ admin_base_path('/applications/?status[]=5&updated_at='.$item['u']) }}">{{ iex_number_format($item['f'], 0, true) }}</a>
                                        @else
                                            <a href="{{ admin_base_path('/applications/?status[]=5&from_updated_at='.$item['u'][0].'&to_updated_at='.$item['u'][1]) }}">{{ iex_number_format($item['f'], 0, true) }}</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </md-content>

                </md-tab>

                <md-tab label="{{ __('Последние обмены') }}">
                    <md-content>
                        <table class="table table-border-2 margin-b0">
                            <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Направление') }}</th>
                                <th>{{ __('Статус') }}</th>
                                <th>{{ __('Создана') }}</th>
                            </tr>

                            </thead>
                            <tbody>
                            @if(count($last_orders) > 0)
                                @foreach($last_orders as $item)
                                    <tr>
                                        <th>{{$item->id}}</th>
                                        <td>
                                            <a href="{{config('admin.directory')}}/applications/details/{{$item->id}}?actions=arhive">
                                                <b>{{ direction_name($item, true) }}</b>
                                            </a>
                                            <div class="order-home-amount">
                                                {{$item->give_price}} {{$item->direction_exchange->currency1->code_currency->name}}
                                                →
                                                {{$item->receiving_price}}  {{$item->direction_exchange->currency2->code_currency->name}}
                                            </div>
                                        </td>
                                        <td>
                                            @if($item->status == 3)
                                                @if($item->scans > 0)
                                                    <span class="st-base-name st-handler-operator">{{ $item->operator->name }} {{ __('принял') }}</span>
                                                @else
                                                    <span class="st-base-name {{$item->task_status->class}}">{{$item->task_status->name}}</span>
                                                @endif
                                            @elseif($item->status == 5)
                                                <span class="st-base-name st-delete" data-toggle="tooltip" data-placement="top" title="{{ __('Заявка отклонена') }}">{{  config('transaction.status_reject.'.$item->category_reject) }}</span>
                                            @else
                                                <span class="st-base-name {{$item->task_status->class}}">{{$item->task_status->name}}</span>
                                            @endif
                                        </td>
                                        <td>{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                                </tr>
                            @endif
                            </tbody>

                        </table>
                    </md-content>
                </md-tab>

                <md-tab label="{{ __('admin.home.new_clients_label') }}">
                    <md-content>
                        <table class="table table-border-2 margin-b0">
                            <thead>
                            <tr>
                                <th>{{ __('admin.home.new_client_id') }}</th>
                                <th>{{ __('admin.home.new_client_name') }}</th>
                                <th>{{ __('admin.home.new_client_partner') }}</th>
                                <th>{{ __('admin.home.new_client_last_visit') }}</th>
                                <th>{{ __('admin.home.new_client_exchange') }}</th>
                            </tr>

                            </thead>
                            <tbody>
                            @if(count($last_users) > 0)
                                @foreach($last_users as $item)
                                    <tr>
                                        <th>{{$item->id}}</th>
                                        <td>
                                            <div>
                                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->id.'&sorting=id') }}">
                                                    <b style="color: {{ is_null($item->email_verified_at) ? 'red' : 'green'}};" >{{ $item->name }}</b>
                                                </a>
                                            </div>
                                            <small>{{ $item->email }}</small>
                                        </td>

                                        <td>
                                            @if(isset($item->fromReferral))
                                                <span class="text-muted">
                                                                <a href="{{ admin_base_path('/account/users/?id='.$item->fromReferral->referral_link->user->id) }}">{{ $item->fromReferral->referral_link->user->name }}</a>
                                                            </span>
                                            @else
                                                <span class="text-muted">{{ __('admin.home.not_found') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if(!is_null($item->last_activity_at))
                                                {{\Illuminate\Support\Carbon::parse($item->last_activity_at)->diffForHumans()}}
                                            @else
                                                <span class="text-muted">{{ __('admin.home.not_defined') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->order_num > 0)
                                                <a href="{{ admin_base_path('/applications?status[]=4&id_user='.$item->id) }}">
                                                    {{ $item->order_num }}
                                                    (<b>{{ iex_number_format($item->successOrderSum(), 2) }}$</b>)
                                                </a>
                                            @else
                                                <span class="text-danger">{{ __('admin.home.not_exchanges') }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4"><p align="center"><br />{{ __('admin.home.list_is_empty') }}</p></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </md-content>
                </md-tab>
                <md-tab label="{{ __('admin.home.last_activity_label') }}">
                    <md-content>
                        <table class="table table-border-2 margin-b0">
                            <thead>
                            <tr>
                                <th>{{ __('admin.home.last_activity_id') }}</th>
                                <th>{{ __('admin.home.last_activity_name') }}</th>
                                <th>{{ __('admin.home.last_activity_partner') }}</th>
                                <th>{{ __('admin.home.last_activity_visit') }}</th>
                                <th>{{ __('admin.home.last_activity_exchange') }}</th>
                            </tr>

                            </thead>
                            <tbody>
                            @if(count($last_active_users) > 0)
                                @foreach($last_active_users as $item)
                                    <tr>
                                        <th>{{$item->id}}</th>
                                        <td>
                                            <div>
                                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->id.'&sorting=id') }}">
                                                    <b style="color: {{ is_null($item->email_verified_at) ? 'red' : 'green'}};" >{{ $item->name }}</b>
                                                </a>
                                            </div>
                                            <small>{{ $item->email }}</small>
                                        </td>

                                        <td>
                                            @if(isset($item->fromReferral))
                                                <span class="text-muted">
                                                                <a href="{{ admin_base_path('/account/users/?id='.$item->fromReferral->referral_link->user->id) }}">{{ $item->fromReferral->referral_link->user->name }}</a>
                                                            </span>
                                            @else
                                                <span class="text-muted">{{ __('admin.home.not_found') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if(!is_null($item->last_activity_at))
                                                {{\Illuminate\Support\Carbon::parse($item->last_activity_at)->diffForHumans()}}
                                            @else
                                                <span class="text-muted">{{ __('admin.home.not_defined') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->order_num > 0)
                                                <a href="{{ admin_base_path('/applications?status[]=4&id_user='.$item->id) }}">
                                                    {{ $item->order_num }}
                                                    (<b>{{ iex_number_format($item->successOrderSum(), 2) }}$</b>)
                                                </a>
                                            @else
                                                <span class="text-danger">{{ __('admin.home.not_exchanges') }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4"><p align="center"><br />{{ __('admin.home.list_is_empty') }}</p></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </md-content>
                </md-tab>
                <md-tab label="{{ __('admin.home.popular_direction_label') }}">
                    <md-content>
                        <table class="table table-border-2 margin-b0">
                            <thead>
                            <tr>
                                <th>{{ __('admin.home.last_exchange_direction') }}</th>
                                <th>{{ __('admin.home.count_exchange') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($popular_exchange) > 0)
                                @foreach($popular_exchange as $item)
                                    <tr>
                                        <td>
                                            <a href="{{config('admin.directory')}}/applications/?status[]=4&currencies_in[]={{$item->id_currency1}}&currencies_out[]={{$item->id_currency2}}">
                                                <b>{{ direction_name($item) }}</b>
                                            </a>
                                        </td>

                                        <td>{{ $item->orders_count }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4"><p align="center"><br />{{ __('admin.home.list_is_empty') }}</p></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </md-content>
                </md-tab>
            </md-tabs>
        @endcan
    </div>
</div>
