@extends('layouts.app')

@section('title', 'Сброс пароля')

@section('body-class', 'class=static-body')

@section('top-js')
    <style>
        .static-body {
            background-color: #252a48;
        }

        .static-body-container {
            padding: 6rem 10rem;
            margin-top: 4.6rem;
        }

        @media (min-width: 100px) and (max-width: 599px) {
            .static-body-container {
                padding: 4rem 1.5rem;
            }
        }

        .static-body-container .data-input-left {
            padding: 0;
        }

        .static-body-container h2 {
            font-size: 26px;
            font-weight: 300;
            color: #252a48;
            margin: 0.6rem 0 4.6rem;
            padding: 0;
        }

        .static-body-button {
            width: 100%;
            color: #fff;
            font-family: inherit;
            font-weight: 400;
            border: none;
            outline: 0;
            transition: 333ms ease-in-out;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            min-height: 55px;
            padding: 10px 36px;
            font-size: 17px;
            border-radius: 5px;
            background-color: #252a48;
        }

        .static-body-button:hover {
            background-color: #111425;
        }

        .static-body-container .error-text {
            padding: 10px;
            margin-top: -15px;
        }

        .g-recaptcha > div {
            margin: 0 auto;
        }
    </style>
@endsection

@section('content')
    <div class="default-page ">
        <div class="default-page-wrap default-page-inner">
            <div class="static-body-container">
                <h2 class="text-center default-page-title">Сброс пароля</h2>

                <form class="form-horizontal" method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="email" type="email" placeholder="Укажите E-mail адрес" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                            </div>
                        </div>

                        @if ($errors->has('email'))
                            <div class="text-danger error-text">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="password" type="password" placeholder="Введите пароль" name="password" required>
                            </div>
                        </div>

                        @if ($errors->has('password'))
                            <div class="text-danger error-text">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>


                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="password-confirm" type="password" placeholder="Повторите введенный пароль" name="password_confirmation" required>
                            </div>
                        </div>

                        @if ($errors->has('password_confirmation'))
                            <div class="text-danger error-text">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </div>
                        @endif
                    </div>


                    <button type="submit" class="static-body-button">Отправить</button>
                </form>

            </div>
        </div>
    </div>
@endsection
