@extends('layouts.app')

@section('title', 'Авторизация')

@section('content')
    <div class="default-page ">
        <h2 class="default-page-title">Авторизация</h2>

        <div class="default-page-wrap default-page-inner">
            <div class="default-page-container">

                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="email" type="email" placeholder="Укажите E-mail адрес" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <div class="exchange-icon">
                                <span class="exchange-i">
                                    <i class="fa fa-fw fa-envelope"></i>
                                </span>
                            </div>
                        </div>

                        @if ($errors->has('email'))
                            <div class="text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="password" type="password" placeholder="Введите пароль" name="password" required>
                            </div>

                            <div class="exchange-icon">
                                <span class="exchange-i">
                                    <i class="fa fa-fw fa-lock"></i>
                                </span>
                            </div>
                        </div>

                        @if ($errors->has('password'))
                            <div class="text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>

                    @captcha()

                    <button type="submit" class="accent-button-shadow final-exchange-button">Войти</button>
                </form>

                <div class="recover_password">
                    <a href="{{ route('password.request') }}">Восстановление пароля</a>
                </div>

                <div class="register-to_auth">
                    <span>ИЛИ?</span>
                </div>

                <a href="/register" class="register-to_auth-b btn btn-default">Создать аккаунт</a>

            </div>

        </div>

    </div>
@endsection
