@extends('layouts.app')

@section('title', 'Регистрация')

@section('content')


    <div class="default-page ">
        <h2 class="default-page-title">Регистрация</h2>

        <div class="default-page-wrap default-page-inner">
            <div class="default-page-container">

                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="name" type="text" placeholder="Укажите Имя" name="name" value="{{ old('name') }}" required autofocus>
                            </div>
                            <div class="exchange-icon">
                                <span class="exchange-i">
                                    <i class="fa fa-fw fa-user"></i>
                                </span>
                            </div>
                        </div>

                        @if ($errors->has('name'))
                            <div class="text-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="email" placeholder="Введите E-mail адрес" type="email" name="email" value="{{ old('email') }}" required>
                            </div>

                            <div class="exchange-icon">
                                <span class="exchange-i">
                                    <i class="fa fa-fw fa-envelope"></i>
                                </span>
                            </div>
                        </div>

                        @if ($errors->has('email'))
                            <div class="text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form {{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="exchange-field">
                                <input id="password" type="password" placeholder="Введите пароль" name="password" required>
                            </div>

                            <div class="exchange-icon">
                                <span class="exchange-i">
                                    <i class="fa fa-fw fa-lock"></i>
                                </span>
                            </div>
                        </div>

                        @if ($errors->has('password'))
                            <div class="text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="data-input-left data-input-reserve">
                        <div class="exchange-form">
                            <div class="exchange-field">
                                <input id="password-confirm" type="password" placeholder="Повторите введенный пароль" name="password_confirmation" required>
                            </div>


                            <div class="exchange-icon">
                                <span class="exchange-i">
                                    <i class="fa fa-fw fa-lock"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    @captcha()

                    <button type="submit" class="accent-button-shadow final-exchange-button">Регистрация</button>
                </form>

                <div class="register-to_auth">
                    <span>Уже есть аккаунт?</span>
                </div>

                <a href="/login" class="register-to_auth-b btn btn-default">Перейти на страницу авторизации</a>

            </div>
        </div>
    </div>

@endsection
