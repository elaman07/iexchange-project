@extends('layouts.app')

@php
    $title = sprintf('%s - %s', iEXContentLanguage('sitename'), iEXContentLanguage('sitename_desc'));
@endphp

@section('angular-script')
    <script nonce="{{ csp_nonce() }}">
        iexcfg = window.iexcfg || {}; iexcfg = @json($build_config);
    </script>
@endsection

@section('title', $title)

@section('content-base')
    <!-- Start Application -->
    <portal-main></portal-main>
    <!-- Finish Application -->
@endsection

@section('angular-script-footer')
    <script nonce="{{ csp_nonce() }}">
        iexInitialGuideData = window.iexInitialGuideData || {};
        @if((int)iEXSetting('type_cached_rates') == 1)
            iexInitialGuideData = {!! \IRedis::get('exchange-iex-initial-rates_'.\Str::lower(app()->getLocale())) !!};
        @endif
    </script>
@endsection
