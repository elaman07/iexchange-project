@extends('layouts.app')

@section('title', __('check.title', ['sitename' => iEXContentLanguage('sitename'), 'id' => $detail->id]))

@section('content-base')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

    <style>
        body{background-color:#f5f3f1}.full_page{max-width:500px}.page_content{background-color:#fff;border-radius:5px;box-shadow:0 3px 1px -2px rgb(0 0 0 / 20%),0 2px 2px 0 rgb(0 0 0 / 14%),0 1px 5px 0 rgb(0 0 0 / 12%);overflow:hidden;padding:0;width:100%}.page-center{position:absolute;left:50%;top:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:100%}.row{margin:0}.wordwrap{word-wrap:break-word;text-align:right}.item-key-color{color:#8e8e8e;font-size:13px;font-weight:600}.cbs{margin-bottom:14px}.sub-header{padding:10px;background-color:#0f172a;text-align:center;font-size:15px;color:#fff;width:200px;margin:9px auto;border-radius:10px}h2{font-size:20px;padding:15px 15px;color:#000;background-color:#fff;text-align:center;margin:10px 0;font-weight:300}.st-delete{color:#d60202}.st-new{color:#2196f3}.st-handler{color:#2196f3}.st-frozen{background:#b2ba0f}.st-pending-payment{color:#7b93a3}.st-success{color:#5ca370}.st-merchant{color:#af4c88}.st-process-payment{color:#ab804b}.st-handler-operator{color:#fc6d41}.st-cancel{color:#969494}.st-error{background-color:#a70000}.line{width:100%;height:1px;background:#e9e9e9}.list-item{position:relative;display:-webkit-box;display:-webkit-flex;display:flex;-webkit-box-pack:start;-webkit-justify-content:flex-start;justify-content:flex-start;-webkit-box-align:center;-webkit-align-items:center;align-items:center;min-height:48px;height:auto}.btn{text-align:center;box-shadow:0 3px 1px -2px rgb(0 0 0 / 20%), 0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%);outline:0;width:260px;text-transform:uppercase;min-height:40px;display:block;margin:14px auto 0;border:none;background:#000;color:#fff;line-height:2.5}.btn:active,.btn:focus,.btn:hover{outline:0;background-color:#1c213c!important;color:#fff!important}.blockchain-link{padding-bottom:20px;padding-top:10px}.comment{box-shadow: none}.comment p{padding:15px;font-size:16px}.comment p a{text-decoration:underline}.blockchain-text{color:rgba(255,255,255,.75);text-align:center;width:400px;margin:0 auto;font-size:12px;padding-bottom:30px}.check-grid-wrapper{display:grid;grid-template-columns:repeat(2,1fr)}.check-grid-wrapper-flex{position:relative;z-index:0;padding:10px;border-right:1px solid #e9e9e9}.check-grid-wrapper-name{color:#afafaf;font-size:13px;margin-bottom:10px}
    </style>

    <div class="container full_page page-center">
            <div>

                @if($detail->status == 4)
                    <div class="page_content comment">
                        <p>Благодарим вас за совершенный обмен!<br />
                            Мы будем очень рады, если Вы оставите отзыв о нашем сервисе на любом удобном для Вас сайте.</p>
                        @if(count($links) > 0)
                            <div style="padding: 0 20px">
                                <ul>
                                    @foreach($links as $link)
                                        <li><a target="_blank" href="{{$link->url}}">{{$link->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                @endif


                <h2>Транзакция №{{ $detail->id }}</h2>

                <div class="row">
                    <div class="page_content">
                        <div class="list-item">
                            <div class="col-md-4 item-key-color">{{ __('check.number_order') }}</div>
                            <div class="col-md-8 wordwrap">{{$detail->id}}</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line"></div>
                        <div class="list-item">
                            <div class="col-md-4 item-key-color"> {{ __('check.course') }}</div>
                            <div class="col-md-8 wordwrap">{{$detail->course_display}}</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line"></div>
                        <div class="list-item">
                            <div class="col-md-4 item-key-color">{{ __('check.status') }}</div>
                            <div class="col-md-8 wordwrap">
                                <span class="{{ $detail->task_status->class }}">{{ $detail->task_status->name }}</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line"></div>

                        <div class="list-item">
                            <div class="col-md-4 item-key-color">Дата создания</div>
                            <div class="col-md-8 wordwrap">{{\Illuminate\Support\Carbon::parse($detail->created_at)->translatedFormat('d M Y, H:i')}}</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="line"></div>

                        @if($detail->status == 4)
                            <div class="list-item">
                                <div class="col-md-4 item-key-color">Дата выполнения</div>
                                <div class="col-md-8 wordwrap">{{\Illuminate\Support\Carbon::parse($detail->updated_at)->translatedFormat('d M Y, H:i')}}</div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="line"></div>
                            <div class="list-item">
                                <div class="col-md-4 item-key-color">Порт. времени</div>
                                <div class="col-md-8 wordwrap">{{ $leadTime }}</div>
                                <div class="clearfix"></div>
                            </div>
                        @endif
                    </div>
                    <div class="sub-header"> {{ __('check.exchange') }}</div>
                    <div class="page_content">

                        <div class="check-grid-wrapper">
                            <div class="check-grid-wrapper-flex">
                                <div class="check-grid-wrapper-name">{{ __('check.valuta') }}</div>
                                <div class="check-grid-wrapper-value"> {{ currency_in($detail, true) }}</div>
                            </div>

                            <div class="check-grid-wrapper-flex">
                                <div class="check-grid-wrapper-name">{{ __('check.amount') }}</div>
                                <div class="check-grid-wrapper-value">{{ $detail->give_price }} {{  $detail->direction_exchange->currency1->code_currency->sign }}</div>
                            </div>

                            @if(strlen(strip_tags($detail->from_shot)) > 0)
                                <div class="check-grid-wrapper-flex">
                                    <div class="check-grid-wrapper-name">{{ __('check.sender_account') }}</div>
                                    <div class="check-grid-wrapper-value">{{ rule_display_shot_check('from_shot', $detail->from_shot) }}</div>
                                </div>
                            @endif

                            @foreach($detail->tasks_fields_currency_in as $item)
                                <div class="check-grid-wrapper-flex">
                                    <div class="check-grid-wrapper-name">{{$item->field_name}}</div>
                                    <div class="check-grid-wrapper-value">
                                        @if(!is_null($item->field_value))
                                            {{ rule_display_shot_check('personal', $item->field_value) }}
                                        @else
                                            <span class="text-danger">Не указано</span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="sub-header"> {{ __('check.receive') }}</div>
                    <div class="page_content">

                        <div class="check-grid-wrapper">
                            <div class="check-grid-wrapper-flex">
                                <div class="check-grid-wrapper-name">{{ __('check.valuta') }}</div>
                                <div class="check-grid-wrapper-value">{{ currency_out($detail, true) }}</div>
                            </div>

                            <div class="check-grid-wrapper-flex">
                                <div class="check-grid-wrapper-name">{{ __('check.amount') }}</div>
                                <div class="check-grid-wrapper-value">{{ $detail->receiving_price }} {{  $detail->direction_exchange->currency2->code_currency->sign }}</div>
                            </div>

                            @if(strlen(strip_tags($detail->to_shot)) > 0)
                                <div class="check-grid-wrapper-flex">
                                    <div class="check-grid-wrapper-name">{{ __('check.receive_account') }}</div>
                                    <div class="check-grid-wrapper-value">{{ rule_display_shot_check('to_shot', $detail->to_shot) }}</div>
                                </div>
                            @endif

                            @foreach($detail->tasks_fields_currency_out as $item)
                                <div class="check-grid-wrapper-flex">
                                    <div class="check-grid-wrapper-name">{{ $item->field_name }}</div>
                                    <div class="check-grid-wrapper-value">
                                        @if(!is_null($item->field_value))
                                            {{ rule_display_shot_check('personal', $item->field_value) }}
                                        @else
                                            <span class="text-danger">Не указано</span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <!-- Ссылка на Blockchain -->
            <div class="clearfix"></div>
            @if(isset($detail->pay_transaction_hash) and isset($detail->direction_exchange->currency2->payment->explorer))
                <div class="blockchain-link">
                    <a target="_blank" class="btn btn-primary" href="{{  str_replace('{hash}', $detail->pay_transaction_hash->transaction_hash, $detail->direction_exchange->currency2->payment->explorer->link)  }}">Ссылка на Blockchain</a>
                </div>

                @if(!empty($detail->direction_exchange->currency2->payment->explorer->text))
                    <div class="blockchain-text">{!! $detail->direction_exchange->currency2->payment->explorer->text !!}</div>
                @endif
            @endif
    </div>

@endsection

@section('admin-page', ' ')
