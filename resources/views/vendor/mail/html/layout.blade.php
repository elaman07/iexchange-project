<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<style>
@media only screen and (max-width: 600px) {
.inner-body {
width: 100% !important;
}

.footer {
width: 100% !important;
}
}

@media only screen and (max-width: 500px) {
.button {
width: 100% !important;
}
}
</style>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
{{ $header ?? '' }}

<!-- Email Body -->
<tr>
<td class="body" width="100%" cellpadding="0" cellspacing="0">
<table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
<!-- Body content -->
<tr>
<td class="content-cell">
{{ Illuminate\Mail\Markdown::parse($slot) }}

{{ $subcopy ?? '' }}
</td>
</tr>
<tr valign="top" height="13">
<td align="center">
<img src="{{ 'https://ftcash.cc/images/mail/linear.png' }}" width="650" height="13" style="margin:0;display:block;font-family:Arial,'sans-serif';font-size:25px;color:#edf0f1">
</td>
</tr>
<tr valign="middle">
<td align="center" style="padding:50px 110px 50px 110px;font-family:Arial,'sans-serif';font-size:16px;font-weight:normal;line-height:24px;color:#878792">
{{ __('email.footer_email_support') }} <a href="mailto:{{ iEXSetting('project_email_support') }}" style="text-decoration:none;color:#4ea4e4" target="_blank"><span style="color:#4ea4e4">{{ iEXSetting('project_email_support') }}</span></a>

</td>
</tr>

</table>
</td>
</tr>

{{ $footer ?? '' }}
</table>
</td>
</tr>
</table>
</body>
</html>
