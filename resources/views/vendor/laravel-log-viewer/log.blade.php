@extends('admin.layouts.app')

@section('title', 'Просмотр журнала логов')

@section('breadcrumbs', Breadcrumbs::render('admin.tools.log'))

@section('custom-js-head')
    <style>
        .stack {
            font-size: 0.85em;
        }
        .date {
            min-width: 75px;
        }
        .text {
            word-break: break-all;
        }
        a.llv-active {
            z-index: 2;
            background-color: #f5f5f5;
            border-color: #777;
        }
        .list-group-item {
            word-wrap: break-word;
        }
        .folder {
            padding-top: 15px;
        }

        .nowrap {
            white-space: nowrap;
        }

        .p-3 {
            padding: 20px;
        }
    </style>
@endsection


@section('no-block-content')
    @if(config('admin.is_demo_mode') == true or config('admin.is_rent') == true)
        @if(config('admin.is_rent') == true)
            <div class="alert alert-danger">Недоступно в тарифном плане "Аренда"</div>
        @else
            <div class="alert alert-danger">{{ __('admin-tools.not_demo_version') }}</div>
        @endif
    @else
        <div class="col sidebar mb-3">
            <div class="list-group div-scroll">
                @foreach($folders as $folder)
                    <div class="list-group-item">
                        <a href="?f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}">
                            <span class="fa fa-folder"></span> {{$folder}}
                        </a>
                        @if ($current_folder == $folder)
                            <div class="list-group folder">
                                @foreach($folder_files as $file)
                                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}&f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}"
                                       class="list-group-item @if ($current_file == $file) llv-active @endif">
                                        {{$file}}
                                    </a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endforeach
                @foreach($files as $file)
                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}"
                       class="list-group-item @if ($current_file == $file) llv-active @endif">
                        {{$file}}
                    </a>
                @endforeach
            </div>
        </div>
        <div class="x_panel">
            <div class="x_title">
                <h2>Общее</h2>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="table-container">
                    @if ($logs === null)
                        <div>
                            Log file >50M, please download it.
                        </div>
                    @else
                        <table id="default-table" class="table table-border-2" data-ordering-index="{{ $standardFormat ? 2 : 0 }}">
                            <thead>
                            <tr>
                                @if ($standardFormat)
                                    <th>Уровень</th>
                                    <th>Тип</th>
                                    <th>Дата и время</th>
                                @else
                                    <th>Line number</th>
                                @endif
                                <th>Content</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($logs as $key => $log)
                                <tr data-display="stack{{{$key}}}">
                                    @if ($standardFormat)
                                        <td class="nowrap text-{{{$log['level_class']}}}" style="width: 30px;">
                                            <span class="fa fa-{{{$log['level_img']}}}" aria-hidden="true"></span>&nbsp;&nbsp;{{$log['level']}}
                                        </td>
                                        <td class="text" style="width: 20px;">{{$log['context']}}</td>
                                    @endif
                                    <td class="date" style="width: 100px;">{{{$log['date']}}}</td>
                                    <td class="text" style="width: 300px;">
                                        @if ($log['stack'])
                                            <button type="button"
                                                    class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2"
                                                    data-display="stack{{{$key}}}">
                                                <span class="fa fa-search"></span>
                                            </button>
                                        @endif
                                        {{{$log['text']}}}
                                        @if (isset($log['in_file']))
                                            <br/>{{{$log['in_file']}}}
                                        @endif
                                        @if ($log['stack'])
                                            <div class="stack" id="stack{{{$key}}}"
                                                 style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    @endif
@endsection

@section('pagination')
    @if(config('admin.is_demo_mode') == true or config('admin.is_rent') == true)
    @else
        <div class="p-3">
            @if($current_file)
                <a class="btn btn-info" id="clean-log" href="?clean={{ \Illuminate\Support\Facades\Crypt::encrypt($current_folder ? $current_folder . "/" . $current_file : $current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                    Очистить файл
                </a>
                <a class="btn btn-danger" id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_folder ? $current_folder . "/" . $current_file : $current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                    Удалить файл
                </a>
                @if(count($files) > 1)
                    <a  class="btn btn-danger" id="delete-all-log" href="?delall=true{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <span class="fa fa-trash"></span> Удалить все файлы
                    </a>
                @endif
            @endif
        </div>
    @endif
@endsection
