<b>Заявка на выплату бонусных вознаграждений.</b>

- № Заявки: {{ $detail->id }}
------------
- ПС. {{$detail->currency->payment->name}} {{$detail->currency->code_currency->name}}
- Реферальные: {{ $detail->base_referral }}  {{$detail->currency->code_currency->sign}}
- Бонусы за обмен:  {{ $detail->base_reward }}  {{$detail->currency->code_currency->sign}}
- Итоговая сумма выплаты {{ $detail->base_referral + $detail->base_reward}}  {{$detail->currency->code_currency->sign}}

<b>О пользователе:</b>
— Имя: {{ $detail->user->name }}
— E-mail: {{ $detail->user->email }}