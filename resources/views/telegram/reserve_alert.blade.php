Уважаемая администрация сервиса {{ iEXContentLanguage('sitename') }}, резерв валюты {{ $options->reserves->currency->payment->name.' '.$options->reserves->currency->code_currency->name }}
опустился ниже установленного порога.


В резерве: {{ $options->reserves->summa }} {{ $options->reserves->currency->code_currency->name }}
Установленный порог: {{ $options->threshold }} {{ $options->reserves->currency->code_currency->name }}
