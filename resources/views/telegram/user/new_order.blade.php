Здравствуйте, <b>{{$options->user->name}}</b>
Уважаемый клиент, Вы создали заявку <b>№{{$options->id}}</b>

<b>Информация по заявке:</b>
--------------------------
Курс: {{ $options->course_display  }}
Дата и время оформления: {{ $options->created_at->format('d M Y, H:i') }}
Статус заявки: «{{$options->task_status->name}}»
--------------------------

<b>Отдаете:</b>
— ПС: {{$options->direction_exchange->currency1->payment->name.' '.$options->direction_exchange->currency1->code_currency->name}}
— Сумма перевода: {{$options->give_price.' '.$options->direction_exchange->currency1->code_currency->name}}
@if(strlen(strip_tags($options->from_shot)) > 0)
— Со счета/кошелька: {{(isset($order->from_shot) ? $order->from_shot : '-') }}
@endif

<b>Получаете:</b>
— ПС: {{$options->direction_exchange->currency2->payment->name.' '.$options->direction_exchange->currency2->code_currency->name}}
— Сумма получения: {{$options->receiving_price.' '.$options->direction_exchange->currency2->code_currency->name}}
@if(strlen(strip_tags($options->to_shot)) > 0)
— На счет/кошелек: {{(isset($order->to_shot) ? $order->to_shot : '-') }}
@endif

