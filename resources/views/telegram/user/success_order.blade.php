Ваша заявка <b>№{{$options['order']->id}}</b> успешно обработана. Средства перечислены на ваши реквизиты.

Благодарим вас за совершенный обмен!
Мы будем очень рады, если Вы оставите отзыв о нашем сервисе на любом удобном для Вас сайте.

@if(count($options['links']) > 0)
@foreach($options['links'] as $link)
-- <a target="_blank" href="{{$link->url}}">{{$link->name}}</a>
@endforeach
@endif

-----------------
@if(in_array($options['order']->status, [4, 8]))
<i>При возникновении вопросов, Вы можете обратиться в службу поддержки на нашем сайте или написать нам на эл. адрес {{ iEXSetting('project_email_support') }}.</i>
@endif
