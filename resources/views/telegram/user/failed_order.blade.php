Уважаемый клиент, Вы создали заявку №{{ $options->id }}.
Статус Вашей заявки: «Удалена»

@if($options->status == 5 and isset($options->tasks_rejection_status))
-----------------
<b>Причина:</b>
{{$options->tasks_rejection_status->name}}
-----------------

@if($options->tasks_rejection_status->id == 1)
Если Вы уверены, что заявка удалена ошибочно, срочно обратитесь в службу поддержки на нашем сайте или напишите нам на почту {{ iEXSetting('project_email_support') }}
@endif

@endif
