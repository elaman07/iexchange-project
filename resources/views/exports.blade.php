<table>
    <thead>
    <tr>
        <th>Дата создания</th>
        <th>Направление</th>
        <th>E-mail</th>
        <th>IP Адрес</th>
        <td>Номер счета</td>
        <th>Ф.И.О Получателя</th>
        <td>Сумма к получению</td>
    </tr>
    </thead>
    <tbody>
    @foreach($invoices as $item)
        <tr>
            <td>{{ $item->created_at }}</td>
            <td>{{ direction_name($item, true) }}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->ip}}</td>
            <td>{{$item->to_shot}}</td>
            <td>{{$item->recipient_fullname}}</td>
            <td>{{$item->receiving_price}} руб</td>
        </tr>
    @endforeach
    </tbody>
</table>