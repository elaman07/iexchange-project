@if(!is_null(iEXSetting('google_analytics')))
    @include('layouts.headers.analytics.google')
@endif

@if(!is_null(iEXSetting('yandex_metrika')))
    @include('layouts.headers.analytics.yandex')
@endif


@if((int)iEXSetting('is_enabled_online_chat') == 1)
    @if(iEXSetting('chat_type') == 'jivosite')
        @include('layouts.headers.chats.jivosite')
    @elseif(iEXSetting('chat_type') == 'intercom')
        @include('layouts.headers.chats.intercom')
    @elseif(iEXSetting('chat_type') == 'replain')
        @include('layouts.headers.chats.replain')
    @elseif(iEXSetting('chat_type') == 'tawk')
        @include('layouts.headers.chats.tawk')
    @elseif(iEXSetting('chat_type') == 'smartsupp')
        @include('layouts.headers.chats.smartsupp')
    @elseif(iEXSetting('chat_type') == 'talkme')
        @include('layouts.headers.chats.talk-me')
    @endif
@endif
