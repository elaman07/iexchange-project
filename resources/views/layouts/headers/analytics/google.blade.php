<!-- Global site tag (gtag.js) - Google Analytics -->
<script nonce="{{ csp_nonce() }}" async src="https://www.googletagmanager.com/gtag/js?id={{iEXSetting('google_analytics')}}"></script>
<script nonce="{{ csp_nonce() }}">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{iEXSetting('google_analytics')}}');
</script>