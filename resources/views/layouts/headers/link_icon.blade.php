<link rel="shortcut icon" href="/favicon.ico?v={{ iEXSetting('last_update_icon', '655') }}" type="image/x-icon">
<link rel="icon" href="/favicon.ico?v={{ iEXSetting('last_update_icon', '655') }}" type="image/x-icon">

@foreach($metaLinkIcons as $value)
@if(isset($value['type']))
<link rel="{{ $value['rel'] }}" type="{{$value['type']}}" sizes="{{ $value['size'] }}" href="{{ url('/images/icons/'.$value['filename'].'.png') }}?v={{ iEXSetting('last_update_icon', '655') }}">
    @else
<link rel="{{ $value['rel'] }}" sizes="{{ $value['size'] }}" href="{{ url('/images/icons/'.$value['filename'].'.png') }}?v={{ iEXSetting('last_update_icon', '655') }}">
@endif
@endforeach

<link rel="manifest" href="{{config('app.url')}}/manifest.json">
<meta name="theme-color" content="#ffffff">
<meta name="apple-mobile-web-app-title" content="{{ iEXContentLanguage('sitename') }}">
<meta name="application-name" content="{{ iEXContentLanguage('sitename') }}">
<meta name="theme-color" content="#ffffff">
<meta name="referrer" content="always" />
