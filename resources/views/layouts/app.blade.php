<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage" theme-mode="iex-light-theme" data-critters-container data-iexexchanger="initProviders()"> {{-- iEXSetting('iex_interface_style_mainpage', 1) --}}
<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <base href="/">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Developers: iEXExchanger FZCO -->
    <meta name="generator" content="iEXExchanger FZCO (https://exchanger.iexbase.com) v{{ config('version.current') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!-- Custom Fonts -->
    @include('layouts.headers.fonts')

    <link href="{{ config('app.url') }}" rel="alternate" hreflang="x-default" />
    @foreach(config('app.locale_codes') as $value)
        <link rel="alternate" href="{{ url('/?locale='. $value) }}" hreflang="ru">
    @endforeach

    @yield('css')
    @if ($__env->yieldContent('admin-page'))
        @yield('admin-page')
    @else
        @include('layouts.headers.meta')
        @include('layouts.headers.link_icon')

        @include('layouts.angular-css')

        <link nonce="{{ csp_nonce() }}" rel="stylesheet" href="/dist/styles.b89a49ca821e57e8.css" media="print" onload="this.media='all'">
        <noscript><link nonce="{{ csp_nonce() }}" rel="stylesheet" href="/dist/styles.b89a49ca821e57e8.css"></noscript>

        <link nonce="{{ csp_nonce() }}" rel="stylesheet" href="{{ asset('/css/custom.css') }}">
        @if(iEXSetting('is_gradient_text_color', 0) == 1)
            <link nonce="{{ csp_nonce() }}" rel="stylesheet" href="/css/colors.css?hash={{ iEXSetting('color_style_hash', 'n') }}">
        @endif
    @endif

    @if ($__env->yieldContent('meta'))
        @yield('meta')
    @endif

    <style nonce="{{ csp_nonce() }}">

        @if(!empty(iEXSetting('iex_interface_design_exchange')))
        html[theme-mode=iex-light-theme] {
            {!! iEXSetting('iex_interface_design_exchange') !!}
        }
        @endif

        @if(!empty(iEXSetting('iex_interface_dark_design_exchange')))
            html[theme-mode=iex-dark-theme] {
                {!! iEXSetting('iex_interface_dark_design_exchange') !!}
            }
        @endif
    </style>


    @yield('angular-script')

    @yield('top-js')
</head>
<body @yield('body-class')>

@if (!$__env->yieldContent('content-base'))
    @yield('content')
@else
    @yield('content-base')
@endif


<!-- Script JS -->
@if (!$__env->yieldContent('admin-page'))
    @yield('angular-script-footer')

    <script nonce="{{ csp_nonce() }}" src="/dist/runtime.f008cc8fd6afcafc.js" type="module"></script>
    <script nonce="{{ csp_nonce() }}" src="/dist/polyfills.53b96305398967ba.js" type="module"></script>
    <script nonce="{{ csp_nonce() }}" src="/dist/scripts.5c83cbb4dbb11a83.js" defer></script>
    <script nonce="{{ csp_nonce() }}" src="/dist/main.b09eddedd1ca3ae2.js" type="module"></script>

    @include('layouts.headers.script')

@endif

@yield('bottom-js')

</body>
</html>
