@extends('layouts.app')

@section('title', __('Панель управления iEXExchanger'))

@section('admin-page')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/admin-login.css?v=1">

    <link rel="stylesheet" href="/assets/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/solid.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/brands.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/regular.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/light.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/duotone.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: Roboto, "Helvetica Neue", sans-serif;
            color: #000000;
            background-color: #ededed;
        }
        .wrapper-container {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #f3f4f5;
            display: -webkit-box;
            display: flex;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            flex-direction: column;
            -webkit-box-pack: center;
            justify-content: center;
        }

        .form-control {
            outline: none;
            border: 1px solid #b9b9b9;
            border-radius: 6px;
            background: #fff;
            width: 100%;
            display: inline-block;
            font-size: 14px;
            color: rgba(47,50,62,.8);
            box-shadow: none !important;
            height: 45px;
        }
        .panel-heading {
            border-color: #fff !important;
            background-color: #fff !important;
            padding: 20px 30px 25px;
            text-align: center;
            display: block;
            color: #2f323e !important;
            font-size: 16px;
            margin: 0;
            font-weight: 400;
            border-radius: 10px 10px 0 0;
        }

        .panel {
            min-width: 220px;
            max-width: 380px;
            width: 100%;
            border-radius: 20px;
            background-color: #fff;
            box-shadow: 0 4px 11px 0 rgba(196,198,215,.21)
        }

        .panel-body {
            width: 300px;
            margin: 0 auto;
            padding: 30px 0;
        }

        .btn {
            color: #fff;
            background-color: #000000;
            border-color: #070707;
            border-radius: 1rem;
            padding: 1rem;
            outline: none !important;
            margin-top: 30px;
            height: 48px;
            font-size: 15px;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

        .btn:hover {
            background-color: #424242;
            border-color: #323232;
        }


        a {
            color: #1E88E5;
            text-decoration: none;
        }

        a:hover, a:focus {
            color: #166dba;
            text-decoration: none;
        }

        .text-muted {
            font-size: 13px;
        }

        .panel-body h2 {
            font-size: 1.2em;
            text-align: center;
            margin: 0;
            font-weight: 400;
            margin-bottom: 40px;
            margin-top: 0;
        }


        .help-block {
            font-size: 13px;
            font-weight: 400;
            color: red;
        }

        .legitRipple {
            padding: 0;
            margin: 10px 0 0;
            width: 100%;
        }

        .form-check-label {
            font-size: 14px;
        }
    </style>
@endsection


@section('content-base')
    <div class="container">

        <div class="wrapper-container">
            <div class="panel panel-default">

                <div class="panel-body">

                    <h2>{{ __('Панель управления iEXExchanger') }}</h2>

                    <form class="separate-sections" role="form" method="POST" action="{{ route('authentication.index') }}">
                        @csrf

                        <div class="mb-3 has-feedback has-feedback-left {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input class="form-control" type="text" name="email" value="{{ old('email') }}" placeholder="{{ __('Введите ваш E-mail') }}" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="mb-3 has-feedback has-feedback-left {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input class="form-control" type="password" name="password" placeholder="{{ __('Введите ваш пароль') }}" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="exampleCheck1">{{ __('Запомнить меня') }}</label>
                        </div>

                        @if(iEXSetting('is_security_captcha_type') == 0 and !empty(config('captcha.sitekey')) and !empty(config('captcha.secret')))
                            <div class="iex-captcha">
                                {!! NoCaptcha::display(['data-theme' => config('captcha.theme', 'light')]) !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="text-danger error-text">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </div>
                                @endif
                            </div>
                        @endif

                        @if(iEXSetting('is_security_captcha_type') == 1 and !empty(config('captcha.sitekey')) and !empty(config('captcha.secret')))
                            {!! HCaptcha::display(['data-theme' => config('captcha.theme', 'light')]) !!}
                            @if ($errors->has('h-captcha-response'))
                                <div class="text-danger error-text">
                                    <strong>{{ $errors->first('h-captcha-response') }}</strong>
                                </div>
                            @endif
                        @endif

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary legitRipple">
                                {{ __('Войти') }}&nbsp;&nbsp;
                                <i class="fad fa-sign-in-alt"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-muted text-center">
                {{ config('app.cms') }}® Copyright 2019-{{ date('Y') }}<br>
                © <a target="_blank" href="https://exchanger.iexbase.com">iEXExchanger FZCO</a> All rights reserved.
            </div>
        </div>
    </div>
@endsection


@section('bottom-js')
    @if(iEXSetting('is_security_captcha_type') == 0 and !empty(config('captcha.sitekey')) and !empty(config('captcha.secret')))
        {!! NoCaptcha::renderJs() !!}
    @endif

    @if(iEXSetting('is_security_captcha_type') == 1 and !empty(config('captcha.sitekey')) and !empty(config('captcha.secret')))
        {!! HCaptcha::renderJs() !!}
    @endif
@endsection
