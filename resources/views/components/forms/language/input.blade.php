<div class="multi-language-form-group">
    <div class="control-label-br">
        <div layout="row">
            <div>{{ $label }}</div>
            <span flex></span>
            <ul class="nav nav-pills lang-tabs">
                @foreach(config('app.form_lang') as $language_form_key => $language_form_value)
                    <li @if($language_form_value['active'] == true) class="active" @endif>
                        <a class="lang-tabs-link" data-toggle="tab" href="#{{ $tab_name }}-{{ $language_form_key }}">
                            {{ $language_form_key }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @foreach(config('app.form_lang') as $language_form_key => $language_form_value)
        <div id="{{ $tab_name }}-{{ $language_form_key }}" style="padding-left: 0;" class="tab-pane fade in @if($language_form_value['active'] == true) active @endif">
            <input class="form-control" type="text" name="{{ $key_name}}{{$language_form_value['field'] }}" value="{{ iEXContentLanguage()->getTranslation($key_name, $language_form_key) }}">
        </div>
    @endforeach
</div>
<div class="clearfix"></div>
