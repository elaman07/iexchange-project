@extends('layouts.app')

@section('title', __('Забанен'))

@section('content-base')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">


    <style>
        body {
            font-family: 'Roboto', sans-serif;
            background-color: #303558;
        }
        .full_page {
            width: 500px;
            margin: auto;
        }
        .box-content {
            background-color: #ffffff;
            border-radius: 5px;
            box-shadow: 0 10px 20px rgba(0,0,0,.1);
            float: left;
            overflow: hidden;
            padding: 0;
            width: 100%;
            margin-top: 50px;
            padding: 20px;
        }

        h3 {
            text-align: center;
        }

        .blocked-info span {
            font-size: 20px;
            color: #303558;
            font-weight: 500;
        }
    </style>

    <div class="full_page">
        <div class="box-content">
            <br />
            <p class="text-center blocked-info">
                {{ __('Ваш аккаунт заблокирован до') }}<br />
                <span>{{ \Illuminate\Support\Carbon::parse($banned->expired_at)->translatedFormat('j F Y, H:i') }}.</span>
            </p>
            <hr />
            <p>
                <strong>{{ __('Причина блокировки') }}:</strong><br />
                {{($banned->description)}}
            </p>

            <hr/>
            <div class="text-center">
                {{ __('Для получения подробной информации о блокировке свяжитесь с нами') }} {{ iEXSetting('project_email_support') }}
            </div>

        </div>
    </div>

@endsection


@section('admin-page', ' ')
