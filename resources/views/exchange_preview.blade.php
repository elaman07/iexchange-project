@extends('layouts.app')

@section('body-class', 'class=exchange-preview')

@php
    $title = 'Обмен '.$exchanger->currency1->payment->name .' '.$exchanger->currency1->code_currency->name.
    ' в '. $exchanger->currency2->payment->name .' '.$exchanger->currency2->code_currency->name;

    $currency_in = $exchanger->currency1->payment->name .' '.$exchanger->currency1->code_currency->name;
    $currency_out = $exchanger->currency2->payment->name .' '.$exchanger->currency2->code_currency->name;
@endphp

@section('meta')
    <meta name="title" content="{{ $title }}" />
    <meta name="keywords" content="{{ $exchanger->seo_keywords  }}" />
    <meta name="description" content="{!! trim(preg_replace('/\s\s+/', ' ', \Str::limit(strip_tags($exchanger->description_exchange), 500))) !!}" />
    <meta name="robots" content="index, follow" />
    <meta property="og:title" content="{{ $title }}">
    <meta property="og:keywords" content="{{ $exchanger->seo_keywords  }}">
    <meta property="og:description" content="{!! trim(preg_replace('/\s\s+/', ' ', \Str::limit(strip_tags($exchanger->description_exchange), 500))) !!}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

    <style>

        body {
            background: #252a48;
        }

        .full_page {
            font-family: Roboto, sans-serif;
            max-width: 800px;
            padding-bottom: 50px;
            margin-top: 50px;
        }

        .currency-block {
            box-shadow: 0 10px 20px rgba(0,0,0,.1);
            background-color: #ffffff;
            padding: 9.2rem 12rem;
            border-radius: 4px;
            color: #4d4d4d;
            margin-bottom: 28px;
        }


        @media (min-width: 100px) and (max-width: 599px) {
            .currency-block {
                padding: 20px 10px !important;
            }
        }

        .currency-block h1 {
            margin: 0;
            text-align: center;
            font-size: 2.9rem;
            font-weight: 300;
            padding-bottom: 50px;
        }

        .currency-block p {
            color: #4d4d4d;
            font-size: 16px;
            padding: 10px;
            line-height: 1.7;
            margin-top: 0;
            margin-bottom: 2.8rem;
        }

        .currency-block ul li {
            padding-bottom: 10px;
        }

        .currency-block-link {
            position: fixed;
            bottom: 0;
            background: #252a48bf;
            left: 0;
            right: 0;
            padding: 10px 15px 15px;
        }

        .btn {
            text-align: center;
            box-shadow: 0 20px 40px rgba(0,0,0,0.1);
            outline: none;
            width: 260px;
            text-transform: uppercase;
            min-height: 49px;
            display: block;
            margin: 14px auto 0;
            border: none;
            background: #252a48;
            color: #fff;
            line-height: 3;
        }

        .btn:hover, .btn:focus, .btn:active {
            outline: none;
            background-color: #1c213c !important;
            color: #fff !important;
        }

        .img-default {
            margin: 0 auto 2rem;
            max-width: 36.6rem;
            padding-bottom: 10px;
            text-align: center;
        }

        h2 {
            line-height: 1.5;
            font-size: 1.5em;
            font-weight: 400;
            text-align: center;
            padding: 20px 0;
        }

        h2 strong {
            font-weight: inherit;
        }

        ul {
            font-size: 16px;
        }

        ul li {
            padding-top: 10px;
        }
    </style>

@endsection

@section('title', $title)

@section('content-base')


    <div class="container full_page">
        <div class="currency-block">

            <div class="img-default">
                <img src="/images/exchange.png"/>
            </div>

            <h1>{{ $title }}</h1>

            {!! $exchanger->description_exchange !!}
        </div>

        <div class="currency-block-link">
            <a href="{{config('app.url')}}/?cur_from={{ $exchanger->currency1->designation_xml }}&cur_to={{ $exchanger->currency2->designation_xml }}" class="btn btn-default">Перейти к обмену</a>
        </div>
    </div>

@endsection


@section('admin-page', ' ')
