@extends('setup.layout')

@section('title', 'Создание администратора')

@section('content')


    <form method="post" action="/setup/create_admin">
        @csrf

        <div class="form-group row">
            <label for="login" class="col-sm-3 col-form-label">Логин (мин. 3 символа)</label>
            <div class="col-sm-5">
                <input type="text" name="login" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" id="login">

                @if ($errors->has('login'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('login') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-sm-3 col-form-label">Пароль (мин. 6 символов):</label>
            <div class="col-sm-5">
                <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password">

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password_confirmation" class="col-sm-3 col-form-label">Подтверждение пароля:</label>
            <div class="col-sm-5">
                <input type="password" name="password_confirmation" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation">

                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-sm-3 col-form-label">E-Mail:</label>
            <div class="col-sm-5">
                <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email">

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>




        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-primary-custom">
                Далее
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </button>
        </div>
    </form>

@endsection