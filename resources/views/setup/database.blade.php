@extends('setup.layout')

@section('title', 'Создание базы данных')

@section('content')

    <b>Данные для доступа к MySQL серверу</b>
    <br/><br/>
    <form method="post" action="/setup/database">
    @csrf

        <div class="form-group row">
            <label for="server" class="col-sm-3 col-form-label">Сервер MySQL</label>
            <div class="col-sm-5">
                <input type="text" name="server" class="form-control" value="localhost" id="server">
            </div>
        </div>

        <div class="form-group row">
            <label for="dbname" class="col-sm-3 col-form-label">Имя базы данных:</label>
            <div class="col-sm-5">
                <input type="text" name="dbname" class="form-control" id="dbname">
            </div>
        </div>

        <div class="form-group row">
            <label for="dbuser" class="col-sm-3 col-form-label">Имя пользователя:</label>
            <div class="col-sm-5">
                <input type="text" name="dbuser" class="form-control" id="dbuser">
            </div>
        </div>

        <div class="form-group row">
            <label for="dbpassword" class="col-sm-3 col-form-label">Пароль:</label>
            <div class="col-sm-5">
                <input type="password" name="dbpassword" class="form-control" id="dbpassword">
            </div>
        </div>


        <p>
            Нажимайте на <b>Далее</b> один раз и дождите загрузки таблиц и вас автоматически переадресует на другую страницу
        </p>


        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-primary-custom">
                Далее
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </button>
        </div>
    </form>

@endsection