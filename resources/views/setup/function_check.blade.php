@extends('setup.layout')

@section('title', 'Предварительная проверка')

@section('content')
    <h3>Обязательные параметры системы</h3>

    <p>
        Ваша система должна соответствовать обязательным параметрам. Если какой-либо из этих параметров выделен красным цветом, то вам необходимо исправить его. В противном случае работоспособность сайта не гарантируется.
    </p>

    <table class="table table-striped table-xs">
        <thead>
        <tr>
            <th width="300">Параметр</th>
            <th colspan="2">Требуется</th>
            <th colspan="2">Текущее значение</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>Версия PHP</td>
            <td colspan="2">8.1 и выше</td>
            <td colspan="2">
                @if(version_compare($php_current, '8.1', '>='))
                    <span class="text-success font-weight-bold">{{ $php_current }}</span>
                @else
                    <span class="text-danger font-weight-bold">{{ $php_current }}</span>
                @endif
            </td>
        </tr>

        <tr>
            <td colspan="3"><b>Требуемые модули PHP:</b></td>
        </tr>

        @foreach($modules as $module)
            <tr>
                <td>{{ $module['value'] }}</td>
                <td colspan="2">Установлен</td>
                <td colspan="2">
                    @if($module['type']($module['name']))
                        <span class="text-success font-weight-bold">Установлен</span>
                    @else
                        <span class="text-danger font-weight-bold">Не установлен</span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h3>Проверка доступа к диску</h3>
    <p>
        Скрипты сайта должны иметь доступ на запись к файлам сайта. Это необходимо для работы модуля управления структурой сайта, загрузки файлов, а так же для работы системы обновлений, которая обновляет ядро сайта до последней версии.

    </p>


    <table class="table table-striped table-xs">
        <thead>
        <tr>
            <th width="300">Параметр</th>
            <th colspan="2">Значение</th>
        </tr>
        </thead>

        <tbody>
        @foreach($permissions['permissions'] as $permission)
            <tr>
                <td>{{ $permission['folder'] }}</td>
                <td colspan="2" class="{{ $permission['isSet'] ? 'text-success font-weight-bold' : 'text-danger font-weight-bold' }}">
                    <i class="fa fa-fw fa-{{ $permission['isSet'] ? 'check-circle-o' : 'exclamation-circle' }}"></i>
                    {{ $permission['permission'] }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h3>Рекомендуемые установки</h3>
    <p>
        Параметры, которые рекомендуется установить в соответствующие значения, чтобы обеспечить полную совместимость с требованиями продукта.
        Сайт будет работать даже в том случае, если некоторые установки не соответствуют рекомендациям. Но в этом случае часть функционала сайта может оказаться недоступной.
    </p>

    <table class="table table-striped table-xs">
        <thead>
        <tr>
            <th width="300">Параметр</th>
            <th colspan="2">Рекомендуется</th>
            <th colspan="2">Текущее значение</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>доступная память</td>
            <td colspan="2">
                не менее 64 Mb, рекомендуется не менее 256 Mb
            </td>
            <td colspan="2">
                @if($memory_limit > 300)
                    <div class="text-success font-weight-bold">{{ $memory_limit }}M</div>
                @else
                    <div class="text-danger font-weight-bold">{{ $memory_limit }}M</div>
                @endif
            </td>
        </tr>

        <tr>
            <td>позволить загрузку файлов (file_uploads)</td>
            <td colspan="2">Включено</td>
            <td colspan="2">
                @if($file_uploads == 1)
                    <div class="text-success font-weight-bold">Включено</div>
                @else
                    <div class="text-danger font-weight-bold">Отключено</div>
                @endif
            </td>
        </tr>

        <tr>
            <td>показывать ошибки (display_errors)</td>
            <td colspan="2">Включено</td>
            <td colspan="2">
                @if($display_errors != 'Off')
                    <div class="text-success font-weight-bold">Включено</div>
                @else
                    <div class="text-danger font-weight-bold">Отключено</div>
                @endif
            </td>
        </tr>
        </tbody>
    </table>


    <ul>
        <li><span class="text-success">Зеленым цветом</span> выделены значения, которые удовлетворяют требованиям системы.</li>
        <li><span class="text-danger">Красным цветом</span> выделены значения, которые <b>не</b> удовлетворяют требованиям системы.</li>
    </ul>

    <div class="modal-footer">
        <a class="btn btn-primary btn-primary-custom" href="/setup/database">
            Далее
            <i class="fa fa-angle-right" aria-hidden="true"></i>

        </a>
    </div>
@endsection
