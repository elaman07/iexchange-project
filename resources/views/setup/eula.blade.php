@extends('setup.layout')

@section('title', 'Лицензионное соглашение')

@section('content')

    <form method="post" action="/setup/eula">
        @csrf

        @if ($errors->has('eula'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('eula') }}
            </div>
        @endif

        <div style="height: 300px; border: 1px solid #76774C; padding: 20px; overflow: auto;">
            <br />
            {!! $license !!}
        </div>

        <div class="form-check" style="margin-top: 10px">
            <input type="checkbox" name="eula" class="form-check-input" id="checked-license">
            <label class="form-check-label" for="checked-license">Я принимаю лицензионное соглашение</label>
        </div>


        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-primary-custom">
                Далее
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </button>
        </div>
    </form>

@endsection
