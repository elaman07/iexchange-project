<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8">
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <title>@yield('title')</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Developers: iEXBase .group -->
    <meta name="generator" content="iEXExchanger (https://exchanger.iexbase.com) v{{config('version.current')}}" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <style>

        body {

            font-size: .9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            background-color: #ededed;
            min-height: 100%;
        }

        .bg-primary-700 {
            background-color: #1976d2;
            border-color: #1976d2;
            color: #fff;
        }

        .navbar-inverse .navbar-brand:hover, .navbar-inverse .navbar-brand:focus {
            color: #fff;
            background-color: transparent;
        }

        .navbar-inverse .navbar-brand {
            color: #fff;
            font-weight: 400;
            text-shadow: 0px 2px 3px rgba(0,0,0,.4);
            font-size: 22px;
            letter-spacing: .1em;
            margin: 0 auto;
        }

        .body-content {
            padding: 30px;
        }


        .panel {
            border-radius: 0px;
            margin-bottom: 20px;
            border-width: 0;
            color: #333333;
            -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }

        .panel-default > .panel-heading {
            color: #333333;
            background-color: #f6f6f6;
            border-color: #ddd;
        }

        .panel-heading {
            position: relative;
            padding-top: 10px;
            padding-bottom: 10px;
            border-top-right-radius: 0px;
            border-top-left-radius: 0px;
        }

        .mt-20 {
            margin-top: 20px!important;
        }

        .panel-heading {
            padding: 15px 20px;
            border-bottom: 1px solid transparent;
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
        }

        .page-content .container {
            max-width: 900px;
            margin: 0 auto;
            position: relative;
            background: #fff;
        }

        .setup-headline{
            font-weight: 100;
            color: #1c1c1c;
            font-size: 2rem;
            border-bottom: 1px solid #e9e9e9;
            padding-bottom: 2rem;
            padding-top: 2rem;
            padding-left: 1rem;
            padding-right: 1rem;
        }

        .content {
            font-size: 15px;
        }

        .btn-primary-custom {
            padding: 8px 30px;
        }

        .btn-primary-custom i {
            margin-left: 10px;
        }


        .content h3 {
            padding: 20px 0;
            font-weight: 400;
            font-size: 20px;
        }


    </style>
</head>
<body>

<div class="navbar navbar-inverse bg-primary-700">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Установка продукта iEXExchanger</a>
    </div>
</div>


<div class="page-container">
    <div class="page-content">
        <div class="container">
            <div class="container-wrapper">
                <h1 class="setup-headline">@yield('title')</h1>
                @if (flash()->message)
                    <div class="{{ flash()->class }}">
                        {{ flash()->message }}
                    </div>
                @endif
                <div class="row cols-xs-space cols-sm-space cols-md-space">
                    <div class="col content store">
                        @yield('content')
                    </div>
                </div>

                @yield('footer')
            </div>

        </div>
    </div>
</div>
</body>

</html>
