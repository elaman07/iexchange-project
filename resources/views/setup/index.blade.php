@extends('setup.layout')

@section('title', 'Установка продукта')

@section('content')


    <div class="body-content">

        <p>
            Добро пожаловать в мастер установки iEXExchanger. Данный мастер поможет вам установить скрипт всего за несколько минут. Однако, не смотря на это, мы настоятельно рекомендуем Вам ознакомиться с документацией по работе со скриптом, а также по его установке, которая поставляется вместе со скриптом.
        </p>

        <p>
            Получить более подробную информацию о программном продукте «iEXExchanger» можно на сайте: <a target="_blank" href="https://exchanger.iexbase.com">https://exchanger.iexbase.com</a>
        </p>
    </div>
@endsection

@section('footer')
    <div class="modal-footer">
        <a class="btn btn-primary btn-primary-custom" href="/setup/eula">
            Далее
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
@endsection
