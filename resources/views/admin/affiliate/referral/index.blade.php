@extends('admin.layouts.app')

@section('title', __('Реферальная программа'))

@section('top-block')
    <md-button ng-href="referral/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить программу') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral'))

@section('x_panel_class', 'new-x_panel_wrapper')

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Программа') }}</th>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Процент') }}</th>
            <th>{{ __('Дата создания') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>
        @foreach($referrals as $referral)
            <tr>
                <th>
                    <a href="{{ admin_base_path('/affiliate/referral/'.$referral->id.'/edit') }}">
                        {{ $referral->name }} &nbsp;<i class="fal fa-pencil"></i>
                    </a>

                    @if($referral->is_reg)
                       <div>
                           <small class="text-success font-weight-normal">{{ __('Выдается новым пользователям') }}</small>
                       </div>
                    @endif
                </th>
                <td>{{ $referral->title }}</td>
                <td>{{ $referral->percent }}%</td>
                <td>
                    <div class=""> {{ \Illuminate\Support\Carbon::parse($referral->created_at)->translatedFormat('d M Y H:i') }}</div>
                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($referral->created_at)->diffForHumans() }}</small>
                </td>
                <td>
                    <div class=""> {{ \Illuminate\Support\Carbon::parse($referral->updated_at)->translatedFormat('d M Y H:i') }}</div>
                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($referral->updated_at)->diffForHumans() }}</small>
                </td>
                <td style="width: 100px;">

                    <md-button class="md-icon-button" onclick="event.preventDefault(); document.getElementById('referral-delete-{{$referral->id}}').submit();">
                        <i class="far fa-trash text-danger"></i>
                    </md-button>

                    {!! Form::open(['id' => 'referral-delete-'.$referral->id, 'method' => 'DELETE', 'route' => ['referral.destroy', $referral->id] ]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-9">{{ __('Отключить в Личном кабинете вкладку История операций') }}</label>
                            <div class="col-md-3">
                                <div class="material-switch switch-input-1">
                                    <input id="is_referral_disable_history_orders" name="is_referral_enable_history_orders" @if(iEXSetting('is_referral_disable_history_orders') == 1) checked @endif type="checkbox" value="1" />
                                    <label for="is_referral_disable_history_orders" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-9">{{ __('Отключить в Личном кабинете вкладку Заработано за рефералы') }}</label>
                            <div class="col-md-3">
                                <div class="material-switch switch-input-1">
                                    <input id="is_referral_disable_profit_ref" name="is_referral_disable_profit_ref" @if(iEXSetting('is_referral_disable_profit_ref') == 1) checked @endif type="checkbox" value="1" />
                                    <label for="is_referral_disable_profit_ref" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-9">{{ __('Отключить в Личном кабинете вкладку Привлечено клиентов') }}</label>
                            <div class="col-md-3">
                                <div class="material-switch switch-input-1">
                                    <input id="is_referral_disable_involved_clients" name="is_referral_disable_involved_clients" @if(iEXSetting('is_referral_disable_involved_clients') == 1) checked @endif type="checkbox" value="1" />
                                    <label for="is_referral_disable_involved_clients" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-9">{{ __('Отключить в Личном кабинете вкладку Заработанные средства') }}</label>
                            <div class="col-md-3">
                                <div class="material-switch switch-input-1">
                                    <input id="is_referral_disable_profit_money" name="is_referral_disable_profit_money" @if(iEXSetting('is_referral_disable_profit_money') == 1) checked @endif type="checkbox" value="1" />
                                    <label for="is_referral_disable_profit_money" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отменить') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
