@extends('admin.layouts.app')

@section('title', __('Лог реферальной программы'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.logs'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Дата добавления') }}</th>
            <th>{{ __('Партнер') }}</th>
            <th>{{ __('Реферал') }}</th>
            <th>{{ __('Текст') }}</th>
            <th>{{ __('Тип') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>
                        @if(isset($log->referral))
                            <div>
                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$log->referral->id.'&sorting=id') }}">
                                    <b>{{$log->referral->name}}</b>
                                </a>
                            </div>
                            <small>{{ $log->referral->email }}</small>
                        @else
                            <small>{{ __('Не определен') }}</small>
                        @endif
                    </td>

                    <td>
                        @if(isset($log->user))
                            <div>
                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$log->user->id.'&sorting=id') }}">
                                    <b>{{$log->user->name}}</b>
                                </a>
                            </div>
                            <small>{{ $log->user->email }}</small>
                        @else
                            <small>{{ __('Не определен') }}</small>
                        @endif
                    </td>
                    <td style="width: 30%">
                        {!! $log->text !!}
                    </td>
                    <td>
                        @if($log->type == 0)
                            <label class="label label-success">{{ __('Не определен') }}</label>
                        @elseif($log->type == 1)
                            <label class="label label-primary">{{ __('Новый реферал') }}</label>
                        @elseif($log->type == 2)
                            <label class="label label-danger">{{ __('Бонус не получен') }}</label>
                        @elseif($log->type == 3)
                            <label class="label label-success">{{ __('Бонус получен') }}</label>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    {!! $logs->links() !!}
@endsection
