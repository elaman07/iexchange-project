@extends('admin.layouts.app')

@section('title', __('Партнерские обмены'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.exchanges'))

@section('top-block')
    <md-button ng-href="exchanges_group" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-exchange text-info"></md-icon>
        <span>{{ __('admin.affiliate.exchanges.groups') }}</span>
    </md-button>
@endsection

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>

        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('ID заявки') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::input('text', 'id_task', is_filter_search($filter, 'id_task'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('ID заявки (От и До)') }}</label>
                                    <div class="col-md-6">
                                        От: {{ Form::input('text', 'from_id_task', is_filter_search($filter, 'from_id_task'), ['class' => 'form-control', 'style' => 'width: 50px;display: inline-block;margin-right: 10px;text-align: center']) }}
                                        До: {{ Form::input('text', 'to_id_task', is_filter_search($filter, 'to_id_task'), ['class' => 'form-control', 'style' => 'width: 50px;display: inline-block;text-align: center']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Пользователь') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::input('text', 'id_referral', is_filter_search($filter, 'id_referral'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Реферал') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Дата создания') }}</label>
                                    <div class="col-md-6">
                                        От: <input  value="{{ is_filter_search($filter, 'from_created_at') }}" name="from_created_at" style="width: 110px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        До: <input  value="{{ is_filter_search($filter, 'to_created_at') }}" name="to_created_at" style="width: 110px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Применить фильтры') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Партнерские обмены') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('Дата создания') }}</th>
                    <th>{{ __('Пользователь') }}</th>
                    <th>{{ __('Реферал') }}</th>
                    <th>{{ __('ID заявки') }}</th>
                    <th>{{ __('Событие') }}</th>
                    <th>{{ __('Партнер заработал') }}</th>
                    <th>{{ __('Партнерский процент') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <td>
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->user->id.'&sorting=id') }}">
                                        <b>{{$item->user->name}}</b>
                                    </a>
                                </div>
                                <small>{{ $item->user->email }}</small>
                            </td>
                            <td>
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->user_admin->id.'&sorting=id') }}">
                                        <b>{{$item->user_admin->name}}</b>
                                    </a>
                                </div>
                                <small>{{ $item->user_admin->email }}</small>
                            </td>
                            <td>
                                <a href="{{ admin_base_path('/applications?id='.$item->id_task) }}">{{ $item->id_task }}</a>
                            </td>

                            <td>{{ $item->text }}</td>
                            <td> {{ $item->bonus }}</td>
                            <td>
                                {{ $item->current_percent }}%
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
