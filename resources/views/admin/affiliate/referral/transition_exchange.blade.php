@extends('admin.layouts.app')

@section('title', __('Реферальные направления') )

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.transitions_exchange'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Реферал') }}</th>
            <th>{{ __('Хэш') }}</th>
            <th>{{ __('Направление') }}</th>
            <th>{{ __('Переходов') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($transitions) > 0)
            @foreach($transitions as $item)
                <tr>
                    <td>
                        <div>
                            <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->referral_user_id) }}">
                                <b>{{$item->referral_user_name}}</b>
                            </a>
                        </div>
                        <small class="text-muted">{{ $item->referral_user_email }}</small>
                    </td>
                    <td>{{ $item->ref_hash }}</td>

                    <td>{{ $item->from_and_to }}</td>
                    <td>{{ $item->total }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $transitions->links() !!}
    </div>
@endsection
