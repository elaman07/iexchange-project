@extends('admin.layouts.app')

@section('title', __('Список рефералов'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.referrals'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Дата создания') }}</th>
            <th>{{ __('Пользователь') }}</th>
            <th>{{ __('Реферал') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($referrals) > 0)
            @foreach($referrals as $referral)
                <tr>
                    <th>
                        <div>{{$referral->created_at}}</div>
                        <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($referral->created_at)->diffForHumans() }}</small>
                    </th>
                    <td>
                        <div>
                            <a href="{{ config('admin.directory') }}/account/users?action=filter&id={{$referral->user->id}}&sorting=id">
                                <b>{{$referral->user->name}}</b>
                            </a>
                        </div>
                        <small>{{ $referral->user->email }}</small>
                    </td>
                    <td>
                        <div>
                            <a href="{{ config('admin.directory') }}/account/users?action=filter&id={{$referral->referral_link->user->id}}&sorting=id">
                                <b>{{$referral->referral_link->user->name}}</b>
                            </a>
                        </div>
                        <small>{{ $referral->referral_link->user->email }}</small>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $referrals->links() !!}
    </div>
@endsection
