@extends('admin.layouts.app')

@section('title', __('Сгруппированные партнерские обмены'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.exchanges_group'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('Сгруппированные партнерские обмены') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('Партнер') }}</th>
                    <th>{{ __('Количество успешных обменов') }}</th>
                    <th>{{ __('Всего заработал партнер') }}</th>
                    <th></th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <td>
                                <div>
                                    <a href="{{ config('admin.directory') }}/account/users?action=filter&id={{$item->user_admin->id}}&sorting=id">
                                        <b>{{$item->user_admin->name}}</b>
                                    </a>
                                </div>
                                <small>{{ $item->user_admin->email }}</small>
                            </td>
                            <td>{{ $item->count_num }}</td>
                            <td> {{ number_format($item->total_amount, 2,'.',' ') }} {{ config('crypto.currency_payout_sign') }}</td>
                            <td style="width: 150px">
                                <md-button class="md-button md-primary" href="{{ admin_base_path('affiliate/referral/exchanges?id_user='.$item->id_user) }}">{{ __('Подробнее') }}</md-button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
