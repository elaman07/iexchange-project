@extends('admin.layouts.app')

@section('title', __('Реферальные переходы'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.transitions'))

@section('top-block')
    <md-button ng-href="transitions_exchange" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-info"></md-icon>
        <span>{{ __('Реф. направления') }}</span>
    </md-button>

    <md-button ng-href="?clear" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">{{ __('Очистить переходы') }}</span>
    </md-button>
@endsection

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>

        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Пользователь') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('IP Адрес') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::input('text', 'ip_address', is_filter_search($filter, 'ip_address'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Применить фильтры') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Реферальные переходы') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('Дата создания') }}</th>
                    <th>{{ __('Пользователь') }}</th>
                    <th>{{ __('IP Адрес') }}</th>
                    <th>{{ __('User Agent') }}</th>
                    <th>{{ __('Реферал') }}</th>
                    <th>{{ __('Хэш') }}</th>
                    <th>{{ __('Переходов') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($transitions) > 0)
                    @foreach($transitions as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>

                            <td>
                                @if(isset($item->user))
                                    <div>
                                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_user}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                    </div>
                                    <small>{{$item->user->email}}</small>
                                @else
                                    <span class="text-muted">{{ __('admin-affiliate.guest') }}</span>
                                @endif
                            </td>

                            <td>
                                @if(isset($item->ip_address))
                                    <a href="{{ admin_base_path('/account/logs_auth?ip_address='.$item->ip_address) }}">{{ $item->ip_address }}</a>
                                @else
                                    <span class="text-muted">{{ __('Не определен') }}</span>
                                @endif
                            </td>

                            <td style="width: 400px">
                                @if(isset($item->user_agent))
                                    {{ $item->user_agent }}
                                @else
                                    <span class="text-muted">{{ __('Не определен') }}</span>
                                @endif
                            </td>

                            <td>
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->referral_user_id) }}">
                                        <b>{{$item->referral_user_name}}</b>
                                    </a>
                                </div>
                                <small class="text-muted">{{ $item->referral_user_email }}</small>
                            </td>
                            <td>
                                {{ $item->ref_hash }}
                            </td>
                            <td>
                                {{ $item->total }}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $transitions->links() !!}
    </div>
@endsection
