@extends('admin.layouts.app')

@section('title', __('Изменить программу').' '.$referral->name)

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.edit', $referral))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/affiliate/referral/'.$referral->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Изменить программу') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Программа') }}</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $referral->name }}" required>
                        <div class="input-p-text">{{ __('Напишите название программы на английском') }}</div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Программа доступа всем пользователям') }}</div>
                        <select name="type" class="form-control selectpicker">
                            <option value="1" @if($referral->type == 1) selected @endif>{{ __('Да') }}</option>
                            <option value="2" @if($referral->type == 2) selected @endif>{{ __('Нет') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Выдать сразу после регистрации') }}</div>
                        <select name="is_reg" class="form-control selectpicker">
                            <option value="0" @if($referral->is_reg == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if($referral->is_reg == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}"  placeholder="{{ __('Заголовок программы: Пример(Новичок,Серебряный партнёр ..)') }}" class="form-control" id="title{{$form_lang['field']}}" value="{{ $referral->getTranslation('title', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#description-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="description-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="description{{$form_lang['field']}}" class="form-control" id="description{{$form_lang['field']}}" value="{{ $referral->getTranslation('description', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Процент с обмена') }}</div>
                        <input type="text" name="percent" class="form-control" id="percent" value="{{ $referral->percent }}" placeholder="{{ __('Укажите процент с каждого обмена') }}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Количество рефералов') }}</div>
                        <input type="text" name="count" class="form-control" id="count" value="{{ $referral->count }}" placeholder="{{ __('Укажите макс. количество рефералов процентной программы') }}" required>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Оформление') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Ширина фона процента') }}</div>
                        <input type="text" name="style_width" class="form-control" id="style_width" value="{{ $referral->style_width }}">
                    </div>
                </div>
            </div>
        </div>


        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Изменить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/affiliate/referral') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
