@extends('admin.layouts.app')

@section('title', __('Реферальная статистика'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.statistics'))

@section('content')

    <md-list>
        <md-list-item>
            <div flex="30">{{ __('Партнерские переходы') }}</div>
            <div flex="30" class="font-bold">{{ $transitions }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских переходов (за сегодня)') }}</div>
            <div flex="30" class="font-bold">{{ $transitions_today }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских переходов (за неделю)') }}</div>
            <div flex="30" class="font-bold">{{ $transitions_week }}</div>
        </md-list-item>


        <md-list-item>
            <div flex="30">{{ __('Партнерских переходов (за месяц)') }}</div>
            <div flex="30" class="font-bold">{{ $transitions_month }}</div>
        </md-list-item>

        <md-divider></md-divider>

        <md-list-item>
            <div flex="30">{{ __('Партнерских регистраций') }}</div>
            <div flex="30" class="font-bold">{{ $register }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских регистраций (за сегодня)') }}</div>
            <div flex="30" class="font-bold">{{ $register_today }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских регистраций (за неделю)') }}</div>
            <div flex="30" class="font-bold">{{ $register_week }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских регистраций (за месяц)') }}</div>
            <div flex="30" class="font-bold">{{ $register_month }}</div>
        </md-list-item>

        <md-divider></md-divider>

        <md-list-item>
            <div flex="30">{{ __('Партнерских обменов') }}</div>
            <div flex="30" class="font-bold">{{ $exchange }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских обменов (за сегодня)') }}</div>
            <div flex="30" class="font-bold">{{ $exchange_today }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских обменов (за неделю)') }}</div>
            <div flex="30" class="font-bold">{{ $exchange_week }}</div>
        </md-list-item>

        <md-list-item>
            <div flex="30">{{ __('Партнерских обменов (за месяц)') }}</div>
            <div flex="30" class="font-bold">{{ $exchange_month }}</div>
        </md-list-item>
    </md-list>
@endsection
