@extends('admin.layouts.app')

@section('title', __('Добавить программу'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.referral.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/affiliate/referral') }}">
        @csrf
        <div class="default-panel-body">


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Новая программа') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Программа') }}</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name')  }}" required>
                        <div class="input-p-text">{{ __('Напишите название программы на английском') }}</div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Программа доступа всем пользователям') }}</div>
                        <select name="type" class="form-control selectpicker">
                            <option value="1" @if(old('type') == 1) selected @endif>{{ __('Да') }}</option>
                            <option value="2" @if(old('type') == 2) selected @endif>{{ __('Нет') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Выдать сразу после регистрации') }}</div>
                        <select name="is_reg" class="form-control selectpicker">
                            <option value="0" @if(old('is_reg') == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if(old('is_reg') == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}"  placeholder="{{ __('Заголовок программы: Пример(Новичок,Серебряный партнёр ..)') }}" class="form-control" id="title{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-3 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#description-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="description-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="description{{$form_lang['field']}}" class="form-control" id="description{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Процент с обмена') }}</div>
                        <input type="text" name="percent" class="form-control" id="percent" value="{{ old('percent', 0) }}" placeholder="{{ __('Укажите процент с каждого обмена') }}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Количество рефералов') }}</div>
                        <input type="text" name="count" class="form-control" id="count" value="{{ old('count', 0) }}" placeholder="{{ __('Укажите макс. количество рефералов процентной программы') }}" required>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/affiliate/referral') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
