@extends('admin.layouts.app')

@section('title', __('Условия для мониторингов'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.conditions.monitoring'))

@section('content')

    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="partners_text" class="col-sm-2 control-label">{{ __('Информация во вкладке Мониторингам') }}</label>
                <div class="col-sm-10">
                    <x-forms.language.textarea-ckeditor ckeditorId="ckeditor" tabName="monitoring_text" keyName="monitoring_text" />
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
        </div>
    </form>
@endsection
