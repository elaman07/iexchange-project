@extends('admin.layouts.app')

@section('title', __('Условия кэшбэка'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.conditions.cashback'))

@section('content')

    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="partners_text" class="col-sm-2 control-label">{{ __('Информация во вкладке Cashback') }}</label>
                <div class="col-sm-10">
                    <x-forms.language.textarea-ckeditor ckeditorId="ckeditor" tabName="cashback_text" keyName="cashback_text" />
                </div>
            </div>

            <div class="form-group">
                <label for="account_text" class="col-sm-2 control-label">{{ __('Информация во вкладке Cashback') }} ({{ __('внизу') }})</label>
                <div class="col-sm-10">
                    <x-forms.language.textarea-ckeditor ckeditorId="ckeditor" tabName="cashback_footer" keyName="cashback_footer" />
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
        </div>
    </form>

@endsection
