@extends('admin.layouts.app')

@section('title', __('admin-affiliate.bonus.bonus_program'))

@section('top-block')
    <md-button ng-href="bonus/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-affiliate.bonus.add_program_title') }}</span>
    </md-button>



    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.bonus'))

@section('x_panel_class', 'new-x_panel_wrapper')
@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-affiliate.bonus.name') }}</th>
            <th>{{ __('admin-affiliate.bonus.summa_plus') }}</th>
            <th>{{ __('admin-affiliate.bonus.percent') }}</th>
            <th>{{ __('admin-affiliate.bonus.created_at') }}</th>
            <th>Посл. обновление</th>
            <th></th>
        </tr>

        </thead>
        <tbody>
        @foreach($bonus as $value)
            <tr>
                <th>
                    <a href="{{ admin_base_path('/affiliate/bonus/'.$value->id.'/edit') }}">
                        {{ $value->name }} &nbsp;<i class="fal fa-pencil"></i>
                    </a>
                </th>
                <td> > {{ $value->amount }}</td>
                <td> {{ $value->percent }}%</td>
                <td>
                    <div class=""> {{ \Illuminate\Support\Carbon::parse($value->created_at)->translatedFormat('d M Y H:i') }}</div>
                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($value->created_at)->diffForHumans() }}</small>
                </td>
                <td>
                    <div class=""> {{ \Illuminate\Support\Carbon::parse($value->updated_at)->translatedFormat('d M Y H:i') }}</div>
                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($value->updated_at)->diffForHumans() }}</small>
                </td>
                <td style="width: 100px;">
                    <md-button class="md-icon-button" onclick="event.preventDefault(); document.getElementById('bonus-delete-{{$value->id}}').submit();">
                        <i class="far fa-trash text-danger"></i>
                    </md-button>

                    {!! Form::open(['id' => 'bonus-delete-'.$value->id, 'method' => 'DELETE', 'route' => ['bonus.destroy', $value->id] ]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-9">{{ __('Отключить систему Cashback') }}</label>
                            <div class="col-md-3">
                                <div class="material-switch switch-input-1">
                                    <input id="is_cashback_disabled" name="is_cashback_disabled" @if(iEXSetting('is_cashback_disabled') == 1) checked @endif type="checkbox" value="1" />
                                    <label for="is_cashback_disabled" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отменить') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
