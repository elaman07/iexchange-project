@extends('admin.layouts.app')

@section('title', __('admin-affiliate.bonus_logs.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.bonus.logs'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-affiliate.bonus_logs.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-affiliate.bonus_logs.created_at') }}</th>
                    <th>{{ __('admin-affiliate.bonus_logs.user') }}</th>
                    <th>{{ __('admin-affiliate.bonus_logs.id_order') }}</th>
                    <th>{{ __('admin-affiliate.bonus_logs.event') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <td>
                                @if(isset($item->user))
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->user->id.'&sorting=id') }}">
                                        <b>{{$item->user->name}}</b>
                                    </a>
                                </div>
                                <small>{{ $item->user->email }}</small>
                                @else
                                    <small class="text-danger">Пользователь не определен</small>
                                @endif
                            </td>
                            <td>
                                <a href="{{ admin_base_path('/applications?id='.$item->id_task) }}">{{ $item->id_task }}</a>
                            </td>

                            <td>{{ $item->description }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-affiliate.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
