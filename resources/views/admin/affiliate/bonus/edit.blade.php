@extends('admin.layouts.app')

@section('title', __('admin-affiliate.bonus.edit_program_title', ['name' => $bonus->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.bonus.edit', $bonus))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/affiliate/bonus/'.$bonus->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Настройка программы</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-affiliate.bonus.program_name') }}</div>
                        <input type="text" name="name" value="{{ $bonus->name }}" class="form-control" id="name" placeholder="{{ __('admin-affiliate.bonus.name_program_hint') }}" required>
                        <div class="input-p-text">{{ __('admin-affiliate.bonus.write_english') }}</div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-affiliate.bonus.output_register') }}</div>
                        <select name="is_reg" class="form-control selectpicker">
                            <option value="0" @if($bonus->is_reg == 0) selected @endif>{{ __('admin-affiliate.no') }}</option>
                            <option value="1" @if($bonus->is_reg == 1) selected @endif>{{ __('admin-affiliate.yes') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-affiliate.bonus.summa_plus') }}</div>
                        <input type="text" name="amount" value="{{ $bonus->amount }}" class="form-control" id="amount" required>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-affiliate.bonus.percent') }}</div>
                        <input type="text" name="percent" class="form-control" value="{{ $bonus->percent }}" id="percent" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}" class="form-control" id="title{{$form_lang['field']}}" value="{{ $bonus->getTranslation('title', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#description-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="description-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="description{{$form_lang['field']}}" class="form-control" id="description{{$form_lang['field']}}" value="{{ $bonus->getTranslation('description', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Оформление') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Ширина фона процента') }}</div>
                        <input type="text" name="style_width" class="form-control" id="style_width" value="{{ $bonus->style_width }}">
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>


        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-affiliate.edit') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/affiliate/bonus') }}">{{ __('admin-affiliate.back') }}</md-button>
        </div>
    </form>

@endsection
