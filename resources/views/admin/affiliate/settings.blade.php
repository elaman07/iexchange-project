@extends('admin.layouts.app')

@section('title', __('admin-affiliate.settings.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.affiliate.settings'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-affiliate.settings.title') }}</h2>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <form action="?" method="POST" class="form-horizontal">
                @csrf

                <div class="default-panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Общие настройки</label>
                        <div class="col-sm-10">
                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.settings.code_currency') }}</div>
                                <select class="form-control selectpicker" name="currency_id" data-live-search="true" data-size="8">
                                    @foreach($codes as $key => $value )
                                        <option value="{{ $key }}" @if($key == config('crypto.currency_id')) selected @endif>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.settings.name_currency') }}</div>
                                <input type="text" class="form-control" id="currency_payout_sign" name="currency_payout_sign" value="{{ config('crypto.currency_payout_sign') }}">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.settings.symbol_currency') }}</div>
                                <input type="text" class="form-control" id="currency_payout_sign_alt" name="currency_payout_sign_alt" value="{{ config('crypto.currency_payout_sign_alt') }}">
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.settings.position_view_symbol') }}</div>
                                <select class="form-control selectpicker" name="currency_payout_position">
                                    <option value="right" @if(config('crypto.currency_payout_position') == 'right') selected @endif>{{ __('admin-affiliate.settings.default') }}</option>
                                    <option value="left" @if(config('crypto.currency_payout_position') == 'left') selected @endif>{{ __('admin-affiliate.settings.left') }}</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('Оставить отступ между символом и суммой?') }}</div>
                                <select class="form-control selectpicker" name="enabled_referral_system_space_indent">
                                    <option value="0" @if(iEXSetting('enabled_referral_system_space_indent') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1" @if(iEXSetting('enabled_referral_system_space_indent') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.minimum_withdraw') }}</div>
                                <input type="text" class="form-control" name="minimum_bonus_payout" value="{{ iEXSetting('minimum_bonus_payout', 100) }}">
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.notify_withdraw') }}</div>
                                <select class="form-control selectpicker" name="is_on_notify_referral_system">
                                    <option value="0" @if(iEXSetting('is_on_notify_referral_system') == 0) selected @endif>{{ __('admin-affiliate.yes') }}</option>
                                    <option value="1" @if(iEXSetting('is_on_notify_referral_system') == 1) selected @endif>{{ __('admin-affiliate.no') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Реферальные настройки</label>
                        <div class="col-sm-10">


                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('Сумма обмена это') }}</div>
                                <select class="form-control selectpicker" name="referral_system_type_exchange">
                                    <option value="0" @if(iEXSetting('referral_system_type_exchange') == 0) selected @endif>{{ __('Сумма отдаю с доп. комиссией') }}</option>
                                    <option value="1" @if(iEXSetting('referral_system_type_exchange') == 1) selected @endif>{{ __('Сумма отдаю с доп. комиссией и комиссией ПС') }}</option>
                                    <option value="2" @if(iEXSetting('referral_system_type_exchange') == 2) selected @endif>{{ __('Сумма отдаю без комиссией') }}</option>
                                    <option value="3" @if(iEXSetting('referral_system_type_exchange') == 3) selected @endif>{{ __('Сумма получаю с доп. комиссией') }}</option>
                                    <option value="4" @if(iEXSetting('referral_system_type_exchange') == 4) selected @endif>{{ __('Сумма получаю с доп. комиссией и комиссией ПС') }}</option>
                                    <option value="5" @if(iEXSetting('referral_system_type_exchange') == 5) selected @endif>{{ __('Сумма получаю без комиссией') }}</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.enable_referral_program') }}</div>
                                <select class="form-control selectpicker" name="enabled_referral_system">
                                    <option value="0" @if(iEXSetting('enabled_referral_system') == 0) selected @endif>{{ __('admin-affiliate.yes') }}</option>
                                    <option value="1" @if(iEXSetting('enabled_referral_system') == 1) selected @endif>{{ __('admin-affiliate.no') }}</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">Включить лог действий</div>
                                <select class="form-control selectpicker" name="is_enabled_referral_logs">
                                    <option value="0" @if(iEXSetting('is_enabled_referral_logs') == 0) selected @endif>{{ __('admin-affiliate.no') }}</option>
                                    <option value="1" @if(iEXSetting('is_enabled_referral_logs') == 1) selected @endif>{{ __('admin-affiliate.yes') }}</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">Начислять партнерское вознаграждение</div>
                                <select class="form-control selectpicker" name="type_partner_deductions">
                                    <option value="0" @if(iEXSetting('type_partner_deductions') == 0) selected @endif>От Суммы обмена</option>
                                    <option value="1" @if(iEXSetting('type_partner_deductions') == 1) selected @endif>От прибыли в настройках направлений</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.withdraw_bonus') }}</div>
                                <select class="form-control selectpicker" name="is_enabled_not_referral_bonus">
                                    <option value="0" @if(iEXSetting('is_enabled_not_referral_bonus') == 0) selected @endif>{{ __('admin-affiliate.yes') }}</option>
                                    <option value="1" @if(iEXSetting('is_enabled_not_referral_bonus') == 1) selected @endif>{{ __('admin-affiliate.no') }}</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.withdraw_timetable') }}</div>
                                <select class="form-control selectpicker" name="referral_bonus_range">
                                    <option value="0" @if(iEXSetting('referral_bonus_range') == 0) selected @endif>{{ __('admin-affiliate.no') }}</option>
                                    <option value="1" @if(iEXSetting('referral_bonus_range') == 1) selected @endif>{{ __('admin-affiliate.yes') }}</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.period_withdraw_bonus') }} (От)</div>
                                <input type="text" name="referral_bonus_from" class="form-control timepicker" value="{{ iEXSetting('referral_bonus_from') }}">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.period_withdraw_bonus') }} (До)</div>
                                <input type="text" name="referral_bonus_to" class="form-control timepicker" value="{{ iEXSetting('referral_bonus_to') }}">
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.minimum_amount_bonus') }}</div>
                                <input type="text" class="form-control" name="referral_min_bonus" value="{{ iEXSetting('referral_min_bonus', 0) }}">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.maximum_amount_bonus') }}</div>
                                <input type="text" class="form-control" name="referral_max_bonus" value="{{ iEXSetting('referral_max_bonus', 0) }}">
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.day_limit_bonus') }}</div>
                                <input type="text" class="form-control" name="referral_bonus_limit_day" value="{{ iEXSetting('referral_bonus_limit_day', 0) }}">
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.month_limit_bonus') }}</div>
                                <input type="text" class="form-control" name="referral_bonus_limit_month" value="{{ iEXSetting('referral_bonus_limit_month', 0) }}">
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.life_span_referral') }}</div>
                                <select class="form-control selectpicker" name="referral_storage_periods">
                                    <option value="0"  @if(iEXSetting('referral_storage_periods') == 0) selected @endif>{{ __('admin-affiliate.referral.forever') }}</option>
                                    <option value="1"  @if(iEXSetting('referral_storage_periods') == 1) selected @endif>{{ __('admin-affiliate.referral.cookie_limit') }}</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-affiliate.referral.time_life_span_day') }}</div>
                                <input type="text" class="form-control" name="referral_lifetime_day" value="{{ iEXSetting('referral_lifetime_day', 7) }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="default-panel-footer default-panel-footer-right">
                    <md-button class="md-raised md-primary" type="submit">{{ __('admin-affiliate.save') }}</md-button>
                </div>
            </form>
        </div>
    </div>
@endsection
