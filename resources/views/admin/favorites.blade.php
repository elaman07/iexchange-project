@extends('admin.layouts.app')

@section('title', __('Управление избранными страницами'))

@section('breadcrumbs', Breadcrumbs::render('admin.favorites.control'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection


@section('custom-js-head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        #sortable {
            list-style-type: none;
            width: 50%;
            margin: 0 auto;
            padding: 25px 0 30px;
        }
        #sortable li {
            margin: 0 3px 3px 3px;
            padding: 0.4em 0.4em 0.4em 1.5em;
            font-size: 1.4em;
            height: 4rem;
            border: 1px dashed #dadada;
        }


        .ui-state-highlight {  height: 4rem; line-height: 1.2em; }
    </style>
@endsection


@section('content')
    @if(count($favorites) > 0)
    <ul id="sortable">
        @foreach($favorites as $key => $value)
            <li class="ui-state-default" id="item-{{$value->id}}" data-position="{{$key}}" layout="row">
                <span>{{ $value->name }}</span>
                <span flex></span>
                <span>
                     <a class="text-danger" href="{{ admin_base_path('/favorites/'.$value->id.'/delete') }}">
                        <i class="fa fa-fw fa-trash"></i>
                    </a>
                </span>
            </li>
        @endforeach
    </ul>
    @else
        <x-admin-empty-page :message="'Ничего не найдено'"></x-admin-empty-page>
    @endif


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">{{ __('Выводить разделы в левое меню') }}</div>
                                    <select class="form-control selectpicker" name="is_not_favorites_left_menu">
                                        <option value="0"  @if(iEXSetting('is_not_favorites_left_menu') == 0) selected @endif>{{ __('Да') }}</option>
                                        <option value="1"  @if(iEXSetting('is_not_favorites_left_menu') == 1) selected @endif>{{ __('Нет') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отмена') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js_bottom')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#sortable" ).sortable();
            $( "#sortable" ).disableSelection();
        } );
    </script>

    <script>
        $( function()
        {
            var ul_sortable = $('#sortable');
            ul_sortable.sortable({
                cursor: 'move',
                placeholder: "ui-state-highlight",
                update: function()
                {
                    var sortable_data = ul_sortable.sortable('serialize');
                    $.ajax({
                        data: sortable_data,
                        type: 'POST',
                        url: '{{ admin_base_path('/favorites/sorting') }}',
                        success: function (result) {
                            console.log(result);
                        }
                    });
                }
            });
            ul_sortable.disableSelection();
        } );
    </script>
@endsection
