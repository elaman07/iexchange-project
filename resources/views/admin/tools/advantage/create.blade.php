@extends('admin.layouts.app')

@section('title', __('Добавить новое преимущество'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.advantage.add'))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/advantage') }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Информация') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}"  placeholder="{{ __('Заголовок (Пример: Выгодные курсы)') }}" class="form-control" id="title{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Ссылка') }}</div>
                        <input type="url" name="link" class="form-control">
                        <div class="form-check">
                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="is_target">
                            <label class="form-check-label" for="checkbox1">{{ __('Открыть в новом окне') }}</label>
                        </div>
                    </div>


                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0">{{ __('Отключено') }}</option>
                            <option value="1">{{ __('Включено') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#content-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="content-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea rows="5" id="content{{$form_lang['field']}}" name="content{{$form_lang['field']}}" class="form-control"></textarea>
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Иконка') }}</div>
                        <input type="file" name="icon" class="form-control" required>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/advantage') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
