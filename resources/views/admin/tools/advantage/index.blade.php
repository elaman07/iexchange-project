@extends('admin.layouts.app')

@section('title', __('Преимущества'))

@section('video__instruction', 'https://www.youtube.com/embed/gQOZa6Zz02U?autoplay=1&amp;mute=0')


@section('top-block')
    <md-button ng-href="advantage/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить пункт') }}</span>
    </md-button>


    <md-button ng-href="advantage/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-filter text-primary"></md-icon>
        <span>{{ __('Сортировка') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.advantage'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Кто изменил') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($advantages) > 0)
            @foreach($advantages as $advantage)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/advantage/'.$advantage->id.'/edit') }}">
                            {{ $advantage->title }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>
                        @if($advantage->status == 0)
                            <span class="text-danger">{{ __('Отключено') }}</span>
                        @else
                            <span class="text-success">{{ __('Включено') }}</span>
                        @endif
                    </td>
                    <td>
                        @if($advantage->id_user == 0)
                            <span class="text-muted">{{ __('Система') }}</span>
                        @else
                            {{ $advantage->user->name }}
                        @endif
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($advantage->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($advantage->updated_at)->diffForHumans() }}</small>
                    </td>

                    <td style="width: 100px;">
                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/advantage/'.$advantage->id.'/destroy')  }}">
                            <i class="fa fa-fw fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
