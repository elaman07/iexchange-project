@extends('admin.layouts.app')

@section('title', __('Изменить преимущество'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.advantage.edit', $item))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/advantage/'.$item->id) }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf
            @method('PUT')


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Информация') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}"  placeholder="{{ __('Заголовок (Пример: Выгодные курсы)') }}" class="form-control" id="title{{$form_lang['field']}}" value="{{ $item->getTranslation('title', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Ссылка') }}</div>
                        <input type="url" name="link" class="form-control" value="{{ $item->link }}">
                        <div class="form-check">
                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="is_target"  @if($item->is_target) checked @endif>
                            <label class="form-check-label" for="checkbox1">{{ __('Открыть в новом окне') }}</label>
                        </div>
                    </div>


                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Отключено') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Включено') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#content-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="content-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea rows="5" id="content{{$form_lang['field']}}" name="content{{$form_lang['field']}}" class="form-control">{{ $item->getTranslation('content', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Иконка') }}</div>
                        <input type="file" name="icon" class="form-control">
                        <div class="input-p-text">
                            @if(!is_null($item->icon))
                                {{ __('Текущая иконка') }}:
                                <a target="_blank" href="/storage/advantage/{{ $item->icon }}">{{ $item->icon }}</a>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/advantage') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
