@extends('admin.layouts.app')

@section('title','Документация - Список статей')


@section('top-block')
    <md-button ng-href="docs/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-file-text-o text-primary"></md-icon>
        <span>Добавить статью</span>
    </md-button>

    <md-button ng-href="#add_category" data-toggle="modal" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить категорию</span>
    </md-button>

    <md-button ng-href="#add_type_category" data-toggle="modal" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить тип категории</span>
    </md-button>
@endsection


@section('breadcrumbs', Breadcrumbs::render('admin.tools.docs'))

@section('content')

    <form action="?form=add_category" method="post">
        {{csrf_field()}}
        <div class="modal fade" id="add_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Новая категория</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_name">Название</label>
                            <div class="clearfix"></div>
                            <input type="text" name="name" class="form-control" id="input_name" required>
                        </div>

                        <div class="form-group">
                            <label for="select">Выберите категорию</label>
                            <select name="category" class="form-control">
                                <option value="0"> Основная </option>
                                {!! $multiple_cat !!}
                            </select>

                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Выберите тип категории</label>
                            <select name="type" class="form-control">
                                @foreach($type as $option)
                                    <option value="{{$option->id}}">{{$option->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Добавить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="?form=add_type_category" method="post">
        {{csrf_field()}}
        <div class="modal fade" id="add_type_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Добавить новый тип категорий</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input_name">Название типа</label>
                            <div class="clearfix"></div>
                            <input type="text" name="name" class="form-control" id="input_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Добавить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <table class="table table-border-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Категория</th>
            <th>Дата публикации</th>
            <th>Действие</th>
        </tr>

        </thead>
        <tbody>

        @foreach($docs as $item)
            <tr>
                <th>{{$item->id}}</th>
                <td>{{$item->name}}</td>
                <td>
                    {{ $item->docs_category->name  }}

                </td>
                <td>{{$item->created_at}}</td>
                <td>
                    <div class="btn-margin-default">
                        <a href="{{config('admin.directory')}}/tools/docs/{{$item->id}}/edit" class="btn btn-info btn-sm">Изменить</a>

                        <form method="POST" action="{{config('admin.directory')}}/tools/docs/{{$item->id}}">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE" autocomplete="off">
                            <input class="btn btn-danger btn-sm" type="submit" value="Удалить">
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@endsection