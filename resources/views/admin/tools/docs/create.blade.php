@extends('admin.layouts.app')

@section('title','Добавить статью')

@section('breadcrumbs', Breadcrumbs::render('admin.tools.docs.add'))

@section('content')
    <form id="create" action="?form=create" class="row" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="col-md-9">
            <div class="form-group">
                <label for="title_news">Название</label>
                <input type="text" name="name" class="form-control" id="title_news" placeholder="Введите название" required>
            </div>

            <div class="form-group">
                <label for="title_news">Ссылка</label>
                <input type="text" name="parent_url" class="form-control" id="title_news" placeholder="Введите путь на английском">
            </div>

            <div class="form-group">
                <label for="exampleSelect1">Выберите категорию</label>

                <select class="form-control selectpicker" id="exampleSelect1" name="category" data-live-search="true" data-max-options="1" required data-size="100%">
                    @foreach($type as $value)
                        @if($value->docs_category->count() > 0)
                            <optgroup label="{{$value['name']}}">
                                @foreach($value->docs_category as $category)
                                    <option value="{{$category['id']}}">{{$category['name']}}</option>
                                @endforeach
                            </optgroup>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="title_news">Дополнительные параметры</label>

                <div class="checkbox checkbox-primary">
                    <input id="important" name="code" value="1" class="styled" type="checkbox">
                    <label for="important">
                        Програмный код
                    </label>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="form-group" style="padding: 10px">
            <label for="video">Текст</label>
            <textarea name="text" id="ckeditor_textarea"></textarea>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Добавить</md-button>
            <md-button ng-href="{{config('app.url')}}{{config('admin.directory')}}/tools/faq">Назад</md-button>
        </div>
    </form>
@endsection

@section('js_bottom')
    <script src="/public/admin-assets/admin/components/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'ckeditor_textarea', {
                height: 500,
                filebrowserBrowseUrl: '/public/assets/admin/library/ckeditor/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/public/assets/admin/library/ckeditor/ckfinder/ckfinder.html?type=Images',
                filebrowserUploadUrl: '/public/assets/admin/library/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '/public/assets/admin/library/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                codeSnippet_theme: 'monokai_sublime',

                on: {
                    instanceReady: function () {
                        this.dataProcessor.htmlFilter.addRules({
                            elements: {
                                img: function (el) {
                                    // Add an attribute.
                                    if (!el.attributes.alt)
                                        el.attributes.alt = $('#title_news').val();

                                    el.addClass('responsive-img preview--img__default');
                                }
                            }
                        });
                    }
                }


            }
        );


        // разрешить теги <style>
        CKEDITOR.config.protectedSource.push(/<(style)[^>]*>.*<\/style>/ig);
        // разрешить теги <script>
        CKEDITOR.config.protectedSource.push(/<(script)[^>]*>.*<\/script>/ig);
        // разрешить php-код
        CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
        // разрешить любой код: <!--dev-->код писать вот тут<!--/dev-->
        CKEDITOR.config.protectedSource.push(/<!--dev-->[\s\S]*<!--\/dev-->/g);
        CKEDITOR.config.allowedContent = true;
    </script>

@endsection
