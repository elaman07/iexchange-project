@extends('admin.layouts.app')

@section('title', __('admin-tools.statistics.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.statistics'))

@section('no-block-content')
    @can('admin_statistics')
        <div hide-xs layout="row" layout-align="space-between stretch" layout-wrap="" class="admin-home-layout">
            <div style="padding-left: 0" flex="50">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ __('admin-tools.statistics.where_user_come') }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="x_content">
                        <table class="table table-border-2">
                            <thead>
                            <tr>
                                <th>{{ __('admin-tools.statistics.website') }}</th>
                                <th>{{ __('admin-tools.statistics.page_views') }}</th>
                                <th></th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($fetchTopReferrers as $value)
                                <tr>
                                    <td>{{str_limit($value['url'], 50)}}</td>
                                    <td>{{$value['pageViews']}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <div style="padding-right: 0"  flex="50">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ __('admin-tools.statistics.what_pages_do_users') }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="x_content">
                        <table class="table table-border-2">
                            <thead>
                            <tr>
                                <th>{{ __('admin-tools.statistics.webpage') }}</th>
                                <th>{{ __('admin-tools.statistics.view') }}</th>
                                <th></th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($fetchMostVisitedPages as $value)
                                <tr>
                                    <td>{{$value['url']}}</td>
                                    <td>{{$value['pageViews']}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <div  style="padding-left: 0"  flex="50">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ __('admin-tools.statistics.activity_user') }}</h2>
                        <div class="heading-right-button">
                            <md-button href="?"  @if(!isset($stat_google)) disabled @endif >{{ __('admin-tools.statistics.7day') }}.</md-button>
                            <md-button href="?stat_google=30d" @if(isset($stat_google) and $stat_google == '30d') disabled @endif>{{ __('admin-tools.statistics.30day') }}.</md-button>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="x_content">
                        <div id="google-analytics" style="width: 100%"></div>
                    </div>
                </div>
            </div>
            <div style="padding-right: 0"  flex="50">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ __('admin-tools.statistics.browser') }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="x_content">
                        <div id="statistics-browser" style="width: 100%;display: block"></div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

@endsection

@section('js_bottom')

    @if($stat_google == '30d')
        <script src="/public/admin-assets/statistics/google_30d.js"></script>
    @else
        <script src="/public/admin-assets/statistics/google_7d.js"></script>
    @endif
@endsection