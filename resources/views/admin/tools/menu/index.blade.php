@extends('admin.layouts.app')

@section('title', __('admin-tools.menu.title'))


@section('top-block')
    <md-button ng-href="menu/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-tools.menu.add_link') }}</span>
    </md-button>
@endsection


@section('breadcrumbs', Breadcrumbs::render('admin.tools.menu'))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-tools.menu.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-tools.menu.name') }}</th>
                    <th>{{ __('admin-tools.menu.link_page') }}</th>
                    <th>{{ __('admin-tools.menu.status') }}</th>
                    <th>Посл. обновление</th>
                    <th></th>
                </tr>

                </thead>
                <tbody>

                @foreach($menus as $menu)
                    <tr>
                        <th>
                            @if(empty($menu->name))
                                <a href="menu/{{$menu->id}}/edit">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="menu/{{$menu->id}}/edit">
                                    {{ $menu->name }} &nbsp;<i class="fal fa-pencil"></i>
                                </a>
                            @endif
                        </th>
                        <td>{{ $menu->slug }}</td>
                        <td>
                            @if($menu->status == 1)
                                <span class="text-success">{{ __('admin-tools.menu.enable') }}</span>
                            @else
                                <span class="text-danger">{{ __('admin-tools.menu.disable') }}</span>
                            @endif
                        </td>
                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($menu->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($menu->updated_at)->diffForHumans() }}</small>
                        </td>
                        <td style="width: 50px;">
                            {!! Form::open(['id' => 'menu_destroy_'.$menu->id, 'method' => 'DELETE', 'route' => ['menu.destroy', $menu->id] ]) !!}
                            {!! Form::close() !!}

                            <a href="javascript:void(0)" onclick="document.getElementById('menu_destroy_{{ $menu->id }}').submit()">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
