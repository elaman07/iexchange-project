@extends('admin.layouts.app')

@section('title', __('admin-tools.menu.add_link'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.menu.add'))

@section('content')
        <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/menu') }}">
            <div class="default-panel-body">
                @csrf
                <div class="form-group">
                    <label for="profit" class="col-sm-2 control-label">Добавить меню</label>
                    <div class="col-sm-10">
                        <div class="tab-content">

                            <div class="form-group col-md-6 multi-language-form-group">
                                <div class="control-label-br">
                                    <div layout="row">
                                        <div>{{ __('admin-tools.menu.name') }}</div>
                                        <span flex></span>
                                        <ul class="nav nav-pills lang-tabs">
                                            @foreach(config('app.form_lang') as $key => $item)
                                                <li @if($item['active'] == true) class="active" @endif>
                                                    <a class="lang-tabs-link" data-toggle="tab" href="#{{ $key }}">
                                                        {{ $key }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @foreach(config('app.form_lang') as $key => $item)
                                    <div id="{{ $key }}" style="padding-left: 0;" class="tab-pane fade in @if($item['active'] == true) active @endif">
                                        <input type="text" name="name{{$item['field']}}" class="form-control">
                                    </div>
                                @endforeach
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">Путь</div>
                                <input type="text" name="slug" class="form-control" id="slug" required>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-tools.menu.position') }}</div>
                                <input type="text" name="sorting" class="form-control" id="sorting" value="{{ old('sorting', 0) }}" required>
                            </div>


                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-tools.menu.status') }}</div>
                                <select name="status" class="form-control selectpicker">
                                    <option value="0">{{ __('admin-tools.menu.un_active') }}</option>
                                    <option value="1">{{ __('admin-tools.menu.active') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <hr />
                <div class="form-group">
                    <label for="profit" class="col-sm-2 control-label">Оформление</label>
                    <div class="col-sm-10">

                        <div class="form-group col-md-6">
                            <div class="control-label-br">{{ __('Цвет текста') }}</div>
                            <div id="cp1" class="input-group colorpicker-component">
                                <input type="text" name="text_color" class="form-control" value="{{ old('text_color') }}" />
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="default-panel-footer default-panel-footer-right">
                <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.menu.save') }}</md-button>
                <md-button ng-href="{{ admin_base_path('/tools/menu') }}">{{ __('admin-tools.menu.back') }}</md-button>
            </div>
        </form>
@endsection
