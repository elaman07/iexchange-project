@extends('admin.layouts.app')

@section('title', __('admin-tools.contact.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.contacts.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/contacts/'.$item->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новый контакт</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.contact.type_contact') }}</div>
                        <select class="form-control selectpicker" name="type" data-live-search="true">
                            @foreach($listContactsTypes as $key => $value)
                                <option value="{{$key}}" @if($item->type == $key) selected @endif>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Иконка') }}</div>
                        <input type="file" name="icon" class="form-control" accept="image/*">
                        <div class="input-p-text">
                            @if(!is_null($item->icon))
                                {{ __('Текущая иконка') }}:
                                <a target="_blank" href="/storage/contact/{{ $item->icon }}">{{ $item->icon }}</a>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.contact.name_system') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{ $form_lang['field'] }}" id="title{{  $form_lang['field'] }}" value="{{ $item->getTranslation('name', $form_lang_key) }}" placeholder="{{ __('admin-tools.contact.name_system_hint') }}" class="form-control">
                            </div>
                        @endforeach
                    </div>


                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Контактные данные') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#value-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="value-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="value{{ $form_lang['field'] }}" id="value{{  $form_lang['field'] }}" value="{{ $item->getTranslation('value', $form_lang_key) }}" placeholder="{{ __('admin-tools.contact.contact_data_hint') }}" class="form-control">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.contact.link_contact') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#url-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="url-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="url{{ $form_lang['field'] }}" id="url{{  $form_lang['field'] }}" placeholder="{{ __('admin-tools.contact.link_contact_hint') }}"  value="{{ $item->getTranslation('url', $form_lang_key) }}" class="form-control">
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.contact.make_button') }}</div>
                        <select class="form-control selectpicker" name="is_button">
                            <option value="0" @if($item->is_button == 0) selected @endif>{{ __('admin-tools.contact.no') }}</option>
                            <option value="1" @if($item->is_button == 1) selected @endif>{{ __('admin-tools.contact.yes') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.contact.visible_home') }}</div>
                        <select class="form-control selectpicker" name="is_home">
                            <option value="0" @if($item->is_home == 0) selected @endif>{{ __('admin-tools.contact.no') }}</option>
                            <option value="1"@if($item->is_home == 1) selected @endif>{{ __('admin-tools.contact.yes') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Оформление</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Цвет текста') }}</div>
                        <div id="cp1" class="input-group colorpicker-component">
                            <input type="text" name="text_color" class="form-control" value="{{ $item->text_color }}" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.contact.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/contacts') }}">{{ __('admin-tools.contact.back') }}</md-button>
        </div>
    </form>
@endsection
