@extends('admin.layouts.app')

@section('title', __('admin-tools.contact.title'))

@section('video__instruction', 'https://www.youtube.com/embed/ve8aeA3ZfUg?autoplay=1&amp;mute=0')


@section('top-block')
    <md-button ng-href="contacts/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.contact.add_contact') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-tools.contact.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.contacts'))

@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.contact.name') }}</th>
                <th>{{ __('admin-tools.contact.contact') }}</th>
                <th>{{ __('admin-tools.contact.link') }}</th>
                <th>{{ __('admin-tools.contact.visible_home') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($contacts) > 0)
                @foreach($contacts as $contact)
                    <tr>

                        <th>

                            @if(empty($contact->name))
                                <a href="contacts/{{ $contact->id }}/edit">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="{{ admin_base_path('/tools/contacts/'.$contact->id.'/edit') }}">
                                    {{ $contact->name }} &nbsp;<i class="fal fa-pencil"></i>
                                </a>
                            @endif
                        </th>
                        <td>{{$contact->value}}</td>
                        <td>
                            @if(empty($contact->url))
                                <small class="text-warning">{{ __('admin-tools.contact.link_not_specified') }}</small>
                            @else
                                {{$contact->url}}
                            @endif
                        </td>

                        <td>
                            <input type="hidden" name="item_id[]" value="{{$contact->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$contact->id}}" name="is_home[{{$contact->id}}]" type="checkbox" value="1" @if($contact->is_home == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$contact->id}}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width: 100px;">
                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/contacts/'.$contact->id.'/delete')  }}">
                                <i class="fa fa-fw fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-tools.contact.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.contact.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-tools.contact.settings') }}</span>
                    </div>
                    <div class="modal-body">
                        <x-forms.language.textarea tabName="description_contact" keyName="description_contact" />
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.contact.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-tools.contact.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
