@extends('admin.layouts.app')

@section('title', __('admin-basic.partners.title'))

@section('top-block')
    <md-button ng-href="partners/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-basic.partners.add_partner') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.partners'))

@section('x_panel_class', 'new-x_panel_wrapper')
@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{  __('admin-basic.partners.name') }}</th>
            <th>{{  __('admin-basic.partners.link') }}</th>
            <th>{{  __('admin-basic.partners.logotype') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($partners) > 0)
            @foreach($partners as $item)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/partners/'.$item->id.'/edit') }}">
                            {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>{{ $item->link }}</td>
                    <td><img src="/storage/{{$item->logo}}" alt="{{ $item->name }}" style="    max-width: 100px;"></td>
                    <td style="width:100px;">
                        <md-button href="{{ admin_base_path('/tools/partners/'.$item->id.'/destroy') }}" class="md-icon-button">
                            <i class="text-danger fas fa-trash"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>

    </table>



    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Вид отображения логотипов партнеров (на сайте)') }}</label>
                            <div class="col-md-6">
                                <select class="form-control selectpicker" name="is_view_logotype_partner">
                                    <option value="0" @if(iEXSetting('is_view_logotype_partner') == 0) selected @endif>{{ __('Компактный') }}</option>
                                    <option value="1" @if(iEXSetting('is_view_logotype_partner') == 1) selected @endif>{{ __('Свой размер') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
