@extends('admin.layouts.app')

@section('title', __('admin-tools.admin-log.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.admin-logs'))

@section('content')
    @if(config('admin.is_demo_mode') == true or config('admin.is_rent') == true)
        @if(config('admin.is_rent') == true)
            <div class="alert alert-danger">Недоступно в тарифном плане "Аренда"</div>
        @else
        <div class="alert alert-danger">{{ __('admin-tools.not_demo_version') }}</div>
        @endif
    @else
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.admin-log.created_at') }}</th>
                <th>{{ __('admin-tools.admin-log.admin') }}</th>
                <th>{{ __('admin-tools.admin-log.method') }}</th>
                <th>{{ __('admin-tools.admin-log.path') }}</th>
                <th>{{ __('admin-tools.admin-log.ip_address') }}</th>
                <th>{{ __('admin-tools.admin-log.input_params') }}</th>
            </tr>

            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>
                        <div>
                            <a href="{{ admin_path() }}/account/users?action=filter&id={{$log->id_user}}&sorting=id"><b>{{$log->user->name}}</b></a>
                        </div>
                        <small>{{$log->user->email}}</small>
                    </td>
                    <td>{{ $log->method }}</td>
                    <td>{{ $log->path }}</td>
                    <td>
                        <span class="label label-primary">{{ $log->ip }}</span>
                    </td>
                    <td style="width: 500px;">
                        <a class="btn btn-default btn-sm" role="button" data-toggle="collapse" href="#id{{$log->id}}" aria-expanded="false" aria-controls="collapseExample">
                            {{ __('admin-tools.admin-log.show_input_params') }}
                        </a>

                        <div style="margin-top: 10px" class="collapse" id="id{{$log->id}}">
                            <pre>{!! $log['input'] !!}</pre>
                        </div>
                    </td>

                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection


@section('pagination')
    {!! $logs->links() !!}
@endsection
