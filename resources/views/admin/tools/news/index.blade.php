@extends('admin.layouts.app')

@section('title', __('admin-tools.news.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.news'))

@section('top-block')
    <md-button ng-href="news/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-tools.news.add_news') }}</span>
    </md-button>
@endsection

@section('x_panel_class', 'new-x_panel_wrapper')
@section('content')

    <table  class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-tools.news.name') }}</th>
            <th>{{ __('admin-tools.news.views') }}</th>
            <th>{{ __('admin-tools.news.created_at') }}</th>
            <th>Посл. обновление</th>
            <th class="not-sortable"></th>
        </tr>

        </thead>
        <tbody>

        @if(count($news) > 0)
            @foreach($news as $item)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/news/'.$item->id.'/edit') }}">
                            {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>{{ $item->views }}</td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                    </td>

                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                    </td>
                    <td style="width: 50px;">
                            {!! Form::open(['id' => 'news_destroy', 'method' => 'DELETE', 'route' => ['news.destroy', $item->id] ]) !!}
                            {!! Form::close() !!}

                        <a href="javascript:void(0)" onclick="document.getElementById('news_destroy').submit()">
                            <i class="fas fa-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>

    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $news->links() !!}
    </div>
@endsection
