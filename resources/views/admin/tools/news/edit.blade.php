@extends('admin.layouts.app')

@section('title', __('admin-tools.news.edit_title', ['name' => '№'.$news->id]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.news.edit', $news->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/news/'.$news->id) }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group col-md-6 multi-language-form-group">
                <div class="control-label-br">
                    <div layout="row">
                        <div>{{ __('Название новости') }}</div>
                        <span flex></span>
                        <ul class="nav nav-pills lang-tabs">
                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                <li @if($form_lang['active'] == true) class="active" @endif>
                                    <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                        {{ $form_lang_key }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                    <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                        <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}" value="{{ $news->getTranslation('name', $form_lang_key) }}">
                    </div>
                @endforeach
            </div>

            <div class="clearfix"></div>

            <div class="form-group col-md-6">
                <div class="control-label-br">
                    <div>{{ __('admin-tools.news.photo') }}</div>
                </div>
                <input type="file" name="image" class="form-control" accept="image/*">
                <div class="input-p-text">
                    @if(!empty($news->image))
                        @if(!empty(iEXSetting('storage_disk_news')) and $news->is_local_image == 1)
                            <a target="_blank" href="{{ iex_dynamic_read_view_image('/news/'.$news->image, false) }}">{{  $news->image }}</a>
                        @else
                            <a target="_blank" href="/storage/news/{{$news->image}}">{{$news->image}}</a>
                        @endif

                    @endif
                </div>
            </div>
            <div class="clearfix"></div>


            <div class="form-group col-md-12 multi-language-form-group">
                <div class="control-label-br">
                    <div layout="row">
                        <div>{{ __('admin-tools.faq.description') }}</div>
                        <span flex></span>
                        <ul class="nav nav-pills lang-tabs">
                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                <li @if($form_lang['active'] == true) class="active" @endif>
                                    <a class="lang-tabs-link" data-toggle="tab" href="#text-{{ $form_lang_key }}">
                                        {{ $form_lang_key }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                    <div id="text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                        <textarea class="form-control" id="{{ $form_lang['text'] }}" name="text{{$form_lang['field']}}" rows="5">
                            {!! $news->getTranslation('text', $form_lang_key) !!}
                        </textarea>
                    </div>
                @endforeach
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.news.update') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/news') }}">{{ __('admin-tools.news.back') }}</md-button>
        </div>
    </form>


@endsection
