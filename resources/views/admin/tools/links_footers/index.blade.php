@extends('admin.layouts.app')

@section('title', 'Ссылки для Footer')


@section('video__instruction', 'https://www.youtube.com/embed/RfwCvFQ81m0?autoplay=1&amp;mute=0')


@section('top-block')
    <md-button ng-href="links_footers/create" class="btn-float" layout="column">
        <md-icon md-font-icon="far fa-plus add-item-button-color"></md-icon>
        <span class="add-item-button-color">{{ __('admin-tools.links_reviews.add_link') }}</span>
    </md-button>

    <md-button ng-href="links_footers_group" class="btn-float" layout="column">
        <md-icon md-font-icon="far fa-layer-group text-primary"></md-icon>
        <span>Группы</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-tools.contact.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.links_footers'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-tools.links_reviews.name') }}</th>
            <th>Группа</th>
            <th>{{ __('admin-tools.links_reviews.link') }}</th>
            <th>{{ __('admin-tools.links_reviews.last_update') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($links) > 0)
            @foreach($links as $link)
                <tr>
                    <th>
                        @if(empty($link->name))
                            <a href="links_footers/{{ $link->id }}/edit">
                                <small class="text-danger">
                                    <i class="far fa-exclamation-circle"></i>
                                    Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                </small>
                            </a>
                        @else
                            <a href="{{ admin_base_path('/tools/links_footers/'.$link->id.'/edit') }}">
                                {{ $link->name }} &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        @endif
                    </th>
                    <td>{{isset($link->group) ? $link->group->name : ''}}</td>
                    <td>{{$link->url}}</td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($link->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($link->updated_at)->diffForHumans() }}</small>
                    </td>


                    <td style="width: 50px;">
                        {!! Form::open(['id' => 'link-footer_destroy_'.$link->id, 'method' => 'DELETE', 'route' => ['links_footers.destroy', $link->id] ]) !!}
                        {!! Form::close() !!}

                        <a href="javascript:void(0)" onclick="document.getElementById('link-footer_destroy_{{ $link->id }}').submit()">
                            <i class="fas fa-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form method="post" action="?" enctype="multipart/form-data">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group col-md-12">
                            <label class="control-label control-label-br">{{ __('Тип отображения') }}</label>
                            <select class="form-control selectpicker" name="input_footer_select">
                                <option value="0" @if(iEXSetting('input_footer_select') == 0) selected @endif>Текст</option>
                                <option value="1" @if(iEXSetting('input_footer_select') == 1) selected @endif>Изображение</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <x-forms.language.input label="{{ 'Название' }}" tabName="input_footer_title" keyName="input_footer_title" />
                        </div>

                        <div class="form-group col-md-12">
                            <x-forms.language.textarea label="{{ 'Описание' }}" tabName="description_footer_text" keyName="description_footer_text" />
                        </div>
                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group col-md-12">
                            <label class="control-label control-label-br">{{ __('Изображение') }}</label>
                            <input type="file" name="footer_image" class="form-control" accept="image/*">

                            @if(!empty(iEXSetting('input_footer_image')))
                                <div class="input-p-text">
                                    {{ __('Выбран') }}: <a target="_blank" href="{{ url('/storage/'.iEXSetting('input_footer_image')) }}">{{  iEXSetting('input_footer_image') }}</a>
                                </div>
                            @endif
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.contact.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-tools.contact.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
