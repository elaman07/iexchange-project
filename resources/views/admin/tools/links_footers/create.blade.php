@extends('admin.layouts.app')

@section('title', __('admin-tools.links_reviews.add_link'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.links_footers.create'))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/links_footers') }}" enctype="multipart/form-data">
        @csrf

        <div class="default-panel-body">
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Информация</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.links_reviews.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}" placeholder="Название ссылки">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.links_reviews.link') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#url-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="url-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="url{{ $form_lang['field'] }}" class="form-control" id="url{{  $form_lang['field'] }}" placeholder="Ссылка на страницу">
                            </div>
                        @endforeach

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" style="margin-top: 0" name="is_blank" value="1"> {{ __('Откройте в новой вкладке') }}
                            </label>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.links_reviews.group') }}</div>
                        {{ Form::select('id_group', $groups, old('id_group'), ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true']) }}
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.links_reviews.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/links_footers') }}">{{ __('admin-tools.links_reviews.back') }}</md-button>
        </div>
    </form>
@endsection
