@extends('admin.layouts.app')

@section('title', __('Список групп'))

@section('top-block')
    <md-button ng-href="links_footers_group/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить группу') }}</span>
    </md-button>

@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.links_footers_group'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($links) > 0)
            @foreach($links as $link)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/links_footers_group/'.$link->id.'/edit') }}">
                            {{ $link->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td style="width: 100px;">
                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['links_footers_group.destroy', $link->id] ]) !!}
                            {!! Form::submit(__('Удалить'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
