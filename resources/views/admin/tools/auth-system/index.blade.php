@extends('admin.layouts.app')

@section('title', __('admin-tools.auth-system.title'))

@section('top-block')
    <md-button ng-href="auth-system/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.auth-system.add_system') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.auth-system'))

@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.auth-system.name') }}</th>
                <th>{{ __('admin-tools.auth-system.client_id') }}</th>
                <th>{{ __('admin-tools.auth-system.client_secret') }}</th>
                <th>{{ __('admin-tools.auth-system.status') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($auth_systems) > 0)
                @foreach($auth_systems as $auth_system)
                    <tr>
                        <th>
                            <a href="auth-system/{{ $auth_system->id }}/edit">
                                {{ $auth_system->name }}  &nbsp;<i class="fal fa-pencil"></i>
                            </a>

                        </th>
                        <td>
                            <input type="text" placeholder="{{ __('admin-tools.auth-system.client_id_hint') }}" name="client_id[{{$auth_system->id}}]" class="form-control" value="{{ $auth_system->client_id }}">
                        </td>
                        <td>
                            <input type="text" placeholder="{{ __('admin-tools.auth-system.client_secret_hint') }}" name="client_secret[{{$auth_system->id}}]" class="form-control" value="{{ $auth_system->client_secret }}">
                        </td>

                        <td>
                            <input type="hidden" name="item_id[]" value="{{$auth_system->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$auth_system->id}}" name="status[{{$auth_system->id}}]" type="checkbox" value="1" @if($auth_system->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$auth_system->id}}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width: 50px;">

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/auth-system/'.$auth_system->id.'/delete') }}">
                                <i class="fad fa-trash text-danger"></i>
                            </md-button>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-tools.auth-system.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.auth-system.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
