@extends('admin.layouts.app')

@section('title', __('admin-tools.auth-system.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.auth-system.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/auth-system/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.auth-system.name') }}</label>
                <div class="col-sm-3">
                    <input type="text" name="name" placeholder="{{ __('admin-tools.auth-system.name_hint') }}" value="{{$item->name}}" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.auth-system.available_services') }}</label>
                <div class="col-sm-3">
                    <select class="form-control selectpicker" name="alias" data-live-search="true" data-size="8">
                        @foreach($listAuthSystems as $key => $value)
                            <option value="{{$key}}" @if($item->alias == $key) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.auth-system.status') }}</label>
                <div class="col-sm-1">
                    <select class="selectpicker" name="status">
                        <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-tools.auth-system.disable') }}</option>
                        <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-tools.auth-system.enable') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.auth-system.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/auth-system') }}">{{ __('admin-tools.auth-system.cancel_button') }}</md-button>
        </div>
    </form>
@endsection
