@extends('admin.layouts.app')

@section('title', __('admin-tools.notification.edit_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.notification.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/notification/'.$item->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="default-panel-body">


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Добавить уведомление</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.notification.status') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-tools.notification.off') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-tools.notification.on') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-8 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>Текст уведомления</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea id="text{{$form_lang['field']}}" rows="6" class="form-control" name="text{{$form_lang['field']}}">{{ $item->getTranslation('text', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <label for="sign" class="control-label-br">{{ __('admin-tools.notification.link') }}</label>
                        <input type="text" name="link" class="form-control" value="{{ $item->link }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">С новой вкладки</div>
                        <select class="selectpicker form-control" name="is_blank">
                            <option value="0" @if($item->is_blank == 0) selected @endif>{{ __('admin-tools.notification.no') }}</option>
                            <option value="1" @if($item->is_blank == 1) selected @endif>{{ __('admin-tools.notification.yes') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Настройка дизайна</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Стиль фона для уведомлений</div>
                        <div id="cp1" class="input-group colorpicker-component">
                            <input type="text" value="{{ $item->bg_color }}" name="bg_color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Стиль текста для уведомлений</div>
                        <div id="cp2" class="input-group colorpicker-component">
                            <input type="text" value="{{ $item->text_color }}" name="text_color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Размер шрифта</div>
                        <input type="text" value="{{ $item->text_size }}" name="text_size" class="form-control" />
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Иконка') }}</div>
                        <input type="file" name="icon_notice" class="form-control" accept="image/*">
                        <div class="input-p-text">
                            @if(!is_null($item->icon_notice))
                                {{ __('Текущая иконка') }}:
                                <a target="_blank" href="/storage/notices/{{ $item->icon_notice }}">{{ $item->icon_notice }}</a>
                                [<a class="text-danger" href="?type=delete">удалить</a>]
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>


                </div>

            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Настройка по расписанию</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.notification.scheduled_enable') }}</div>
                        <select class="selectpicker form-control" name="is_enabled_schedule">
                            <option value="0" @if($item->is_enabled_schedule == 0) selected @endif>{{ __('admin-tools.notification.no') }}</option>
                            <option value="1" @if($item->is_enabled_schedule == 1) selected @endif>{{ __('admin-tools.notification.yes') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.notification.period_view') }} (От)</div>
                        <input type="text" name="from_time" class="form-control timepicker" value="{{ $item->from_time }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-tools.notification.period_view') }} (До)</div>
                        <input type="text" name="to_time" class="form-control timepicker" value="{{  $item->to_time }}">
                    </div>

                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.notification.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/notification') }}">{{ __('admin-tools.notification.back') }}</md-button>
        </div>
    </form>

@endsection
