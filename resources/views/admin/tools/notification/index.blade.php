@extends('admin.layouts.app')

@section('title', __('admin-tools.notification.title'))


@section('video__instruction', 'https://www.youtube.com/embed/7xC213NoLVE?autoplay=1&amp;mute=0')


@section('top-block')
    <md-button ng-href="notification/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.notification.add') }}</span>
    </md-button>

    <md-button ng-href="notification/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-filter text-primary"></md-icon>
        <span>{{ __('admin-tools.notification.sorting') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.notification'))

@section('content')
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.notification.text') }}</th>
                <th>{{ __('admin-tools.notification.color') }}</th>
                <th>{{ __('admin-tools.notification.status') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @foreach($notifications as $notification)
                <tr>
                    <td>{{$notification->text}}</td>
                    <td>
                        @if($notification->color == 0)
                            <span class="text-success">{{ __('admin-tools.notification.green') }}</span>
                        @elseif($notification->color == 1)
                            <span class="text-warning">{{ __('admin-tools.notification.yellow') }}</span>
                        @elseif($notification->color == 2)
                            <span class="text-danger">{{ __('admin-tools.notification.red') }}</span>
                        @elseif($notification->color == 3)
                            <span class="text-primary">{{ __('admin-tools.notification.priority') }}</span>
                        @endif
                    </td>
                    <td>

                        @if($notification->status == 0)
                            <a href="notification/{{$notification->id}}/on">
                                <i class="fa fa-fw fa-power-off text-danger" data-toggle="tooltip" title="{{ __('admin-tools.notification.disable') }}"> </i>
                            </a>
                        @else
                            <a href="notification/{{$notification->id}}/off">
                                <i class="fa fa-fw fa-power-off text-success" data-toggle="tooltip" title="{{ __('admin-tools.notification.enable') }}"> </i>
                            </a>
                        @endif
                    </td>

                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a  href="notification/{{$notification->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-tools.notification.edit_button') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['notification.destroy', $notification->id] ]) !!}
                            {!! Form::submit(__('admin-tools.notification.delete_button'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection
