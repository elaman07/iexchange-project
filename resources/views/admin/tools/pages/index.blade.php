@extends('admin.layouts.app')

@section('title', __('Список страниц'))

@section('top-block')

    <md-button ng-href="pages/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить страницу') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.pages'))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>@yield('title')</h2>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('Название страницы') }}</th>
                    <th>{{ __('Ссылка на страницу') }}</th>
                    <th>{{ __('Дата создания') }}</th>
                    <th>{{ __('Дата обновления') }}</th>
                    <th>{{ __('Автор') }}</th>
                    <th></th>
                </tr>

                </thead>
                <tbody>

                @foreach($pages as $page)
                    <tr>
                        <th>
                            @if(empty($page->page_title))
                                <a href="pages/{{$page->page_id}}/edit">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        {{ __('Нет названия') }} &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="pages/{{$page->page_id}}/edit">
                                    {{ $page->page_title }} &nbsp;<i class="fal fa-pencil"></i>
                                </a>
                            @endif
                        </th>
                        <td>
                            @if($page->page_slug == 'pravila-obmena')
                                <div>
                                    <a target="_blank" href="{{ url('/rules') }}">
                                        <i>{{ $page->page_slug }}</i>&nbsp;
                                        <i class="fad fa-link"></i></a>
                                </div>

                                @if($page->page_slug == 'pravila-obmena')
                                    <small class="text-muted" style="width: 150px; display: block;">{{ __('Данная страница переадресовывается на') }} {{ url('/rules') }}</small>
                                @endif
                            @else
                               <div>
                                   <a target="_blank" href="{{ url('/pages/'.$page->page_slug) }}">
                                       <i>{{ $page->page_slug }}</i>&nbsp;
                                       <i class="fad fa-link"></i>
                                   </a>
                               </div>
                            @endif
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($page->created_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($page->created_at)->diffForHumans() }}</small>
                        </td>
                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($page->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($page->updated_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            @if(isset($page->user))
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$page->user->id.'&sorting=id') }}"><b>{{$page->user->name}}</b></a>
                                </div>
                                <small>{{$page->user->email}}</small>
                            @else
                                <small class="text-danger">{{ __('Не определен') }}</small>
                            @endif
                        </td>

                        <td style="width: 50px;">
                            @if($page->page_slug != 'pravila-obmena')
                                {!! Form::open(['id' => 'pages_destroy', 'method' => 'DELETE', 'route' => ['pages.destroy', $page->page_id] ]) !!}
                                {!! Form::close() !!}

                                <a href="javascript:void(0)" onclick="document.getElementById('pages_destroy').submit()">
                                    <i class="fas fa-trash text-danger"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('Ссылка на страницу с правилами обмена') }}</label>
                            <div class="col-md-8">
                                <input type="text" name="exchange_rules_link" class="form-control" value="{{ iEXSetting('exchange_rules_link', '/rules') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('Ссылка на страницу с AML соглашением') }}</label>
                            <div class="col-md-8">
                                <input type="text" name="exchange_kyc_link" class="form-control" value="{{ iEXSetting('exchange_kyc_link', '/pages/kyc') }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отмена') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {{ $pages->links() }}
    </div>
@endsection
