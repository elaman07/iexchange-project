@extends('admin.layouts.app')

@section('title', __('Изменить страницу :name', ['name' => $page->page_title]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.pages.change', $page))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title" layout="row" layout-align="start center">
            <h2>@yield('title')</h2>
            <span flex></span>
            <div class="heading-right-button">
                <ul class="nav nav-pills lang-tabs">
                    @foreach(config('app.form_lang') as $key => $item)
                        <li @if($item['active'] == true) class="active" @endif>
                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $key }}">
                                @if(!empty($item['img']))
                                    <img src="/images/flags/{{ $item['img'] }}" alt="{{ $key }}"/>
                                @else
                                    {{ $item['name'] }}
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="x_content">
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/pages/'.$page->page_id) }}">
                <div class="default-panel-body">
                    @csrf
                    @method('PUT')

                    <div class="tab-content">
                        @foreach(config('app.form_lang') as $key => $item)
                            <div id="{{ $key }}" class="tab-pane fade in @if($item['active'] == true) active @endif">

                                <div class="form-group">
                                    <label for="page_title{{$item['field']}}" class="col-sm-2 control-label">{{ __('Название страницы') }}</label>
                                    <div class="col-sm-5">
                                        <input type="text" name="page_title{{$item['field']}}" value="{{ $page->getTranslation('page_title', $key) }}" class="form-control" id="page_title{{$item['field']}}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="page_content{{$item['field']}}" class="col-sm-2 control-label">{{ __('Контент') }}</label>
                                    <div class="col-sm-10">
                                        <textarea id="{{$item['text']}}" name="page_content{{$item['field']}}">{{ $page->getTranslation('page_content', $key) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group">
                            <label for="page_slug" class="col-sm-2 control-label">{{ __('Ссылка на страницу') }}</label>
                            <div class="col-sm-3">
                                <input type="text" name="page_slug" value="{{ $page->page_slug }}" class="form-control" id="page_slug" @if($page->page_slug == 'pravila-obmena') readonly @endif>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="default-panel-footer default-panel-footer-right">
                    <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
                    <md-button ng-href="{{ admin_base_path('/tools/pages') }}">{{ __('Назад') }}</md-button>
                </div>
            </form>
        </div>
    </div>

@endsection
