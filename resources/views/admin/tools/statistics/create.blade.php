@extends('admin.layouts.app')

@section('title', __('admin-tools.stat.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.info-statistics.create'))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/statistics') }}">
        @csrf
        <div class="default-panel-body">
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Информация</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.stat.type') }}</div>
                        <select class="form-control selectpicker" name="type" data-live-search="true" data-size="8">
                            @foreach($listStatisticsTypes as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.stat.name') }}</div>
                        <input type="text" name="name" placeholder="{{ __('admin-tools.stat.name_hint') }}" class="form-control" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.stat.value') }}</div>
                        <input type="text" name="value" placeholder="{{ __('admin-tools.stat.value_hint') }}" class="form-control">
                        <div class="form-check">
                            <input class="form-check-input" id="checkbox2" value="1" type="checkbox" name="is_automatic">
                            <label class="form-check-label" for="checkbox2">{{ __('admin-tools.stat.value_automatic') }}</label>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.stat.number_shot') }}</div>
                        <input type="text" name="account_number" placeholder="{{ __('admin-tools.stat.number_shot_hint') }}" class="form-control">
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.stat.link') }}</div>
                        <input type="text" name="link" placeholder="{{ __('admin-tools.stat.link_hint') }}" class="form-control">
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.stat.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/statistics') }}">{{ __('admin-tools.stat.back') }}</md-button>
        </div>
    </form>
@endsection
