@extends('admin.layouts.app')

@section('title', __('admin-tools.stat.title'))

@section('top-block')
    <md-button ng-href="statistics/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.stat.add_stat') }}</span>
    </md-button>

    <md-button ng-href="statistics/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-filter text-primary"></md-icon>
        <span>{{ __('admin-tools.stat.sorting') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.info-statistics'))

@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.stat.name') }}</th>
                <th>{{ __('admin-tools.stat.value') }}</th>
                <th>Дата создания</th>
                <th>{{ __('admin-tools.stat.last_update') }}</th>
                <th>{{ __('admin-tools.stat.status') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($statistics) > 0)
                @foreach($statistics as $statistic)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('/tools/statistics/'.$statistic->id.'/edit') }}">
                                {{ $statistic->name }} &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>
                            @if($statistic->is_automatic == 1)
                                <small class="text-success">{{ __('admin-tools.stat.auto-detect') }}</small>
                            @else
                                {{$statistic->value}}
                            @endif
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($statistic->created_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($statistic->created_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($statistic->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($statistic->updated_at)->diffForHumans() }}</small>
                        </td>


                        <td>
                            <input type="hidden" name="item_id[]" value="{{$statistic->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$statistic->id}}" name="status[{{$statistic->id}}]" type="checkbox" value="1" @if($statistic->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$statistic->id}}" class="label-success"></label>
                            </div>
                        </td>

                        <td style="width: 50px">
                            <a class="md-icon-button" href="{{ admin_base_path('/tools/statistics/'.$statistic->id.'/destroy') }}">
                                <i class="fa fa-fw fa-trash text-danger" ></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>

        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-tools.stat.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.stat.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
