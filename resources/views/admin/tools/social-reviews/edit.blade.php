@extends('admin.layouts.app')

@section('title', __('admin-tools.social-reviews.edit_title', ['name' => $link->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.social-reviews.edit', $link))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/social-reviews/'.$link->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Информация</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.social-reviews.type') }}</div>
                        <select class="form-control selectpicker" name="type" data-live-search="true">
                            @foreach($listSocialReviews as $key => $value)
                                <option value="{{$key}}" @if($link->type == $key) selected @endif>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.social-reviews.name') }}</div>
                        <input type="text" name="name" placeholder="{{ __('admin-tools.social-reviews.name_hint') }}" class="form-control" value="{{ $link->name }}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-tools.social-reviews.link') }}</div>
                        <input type="url" name="link" placeholder="{{ __('admin-tools.social-reviews.link_hint') }}"  value="{{ $link->link }}" class="form-control">
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.social-reviews.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/social-reviews') }}">{{ __('admin-tools.social-reviews.back') }}</md-button>
        </div>
    </form>
@endsection
