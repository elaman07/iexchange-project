@extends('admin.layouts.app')

@section('title', __('admin-tools.social-reviews.title'))

@section('top-block')
    <md-button ng-href="social-reviews/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-tools.social-reviews.add_link') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.social-reviews'))

@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.social-reviews.name') }}</th>
                <th>{{ __('admin-tools.social-reviews.link') }}</th>
                <th>Дата создания</th>
                <th>{{ __('admin-tools.social-reviews.last_update') }}</th>
                <th>{{ __('admin-tools.social-reviews.status') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($links) > 0)
                @foreach($links as $link)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('/tools/social-reviews/'.$link->id.'/edit') }}">
                                {{ $link->name }} &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>{{$link->link}}</td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($link->created_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($link->created_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($link->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($link->updated_at)->diffForHumans() }}</small>
                        </td>


                        <td>
                            <input type="hidden" name="item_id[]" value="{{$link->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$link->id}}" name="status[{{$link->id}}]" type="checkbox" value="1" @if($link->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$link->id}}" class="label-success"></label>
                            </div>
                        </td>

                        <td style="width: 100px">
                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/social-reviews/'.$link->id.'/delete')  }}">
                                <i class="fas fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-tools.social-reviews.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.social-reviews.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
