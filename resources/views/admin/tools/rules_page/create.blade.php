@extends('admin.layouts.app')

@section('title', __('Добавить ссылку'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.rules_pages.create'))

@section('content')
        <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/rules_pages') }}">
            <div class="default-panel-body">
                @csrf
                <div class="form-group">
                    <label for="profit" class="col-sm-2 control-label">{{ __('Добавить ссылку') }}</label>
                    <div class="col-sm-10">
                        <div class="tab-content">

                            <div class="form-group col-md-6 multi-language-form-group">
                                <div class="control-label-br">
                                    <div layout="row">
                                        <div>{{ __('Название ссылки') }}</div>
                                        <span flex></span>
                                        <ul class="nav nav-pills lang-tabs">
                                            @foreach(config('app.form_lang') as $key => $item)
                                                <li @if($item['active'] == true) class="active" @endif>
                                                    <a class="lang-tabs-link" data-toggle="tab" href="#{{ $key }}">
                                                        {{ $key }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @foreach(config('app.form_lang') as $key => $item)
                                    <div id="{{ $key }}" style="padding-left: 0;" class="tab-pane fade in @if($item['active'] == true) active @endif">
                                        <input type="text" name="title{{$item['field']}}" class="form-control">
                                    </div>
                                @endforeach
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group col-md-6">
                                <div class="control-label-br">Страница</div>
                                <select class="form-control selectpicker" name="id_page" data-size="8">
                                    @foreach($pages as $page)
                                        <option value="{{ $page->page_id }}">{{ $page->page_title }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <div class="control-label-br">{{ __('admin-tools.menu.status') }}</div>
                                <select name="status" class="form-control selectpicker">
                                    <option value="0">{{ __('admin-tools.menu.un_active') }}</option>
                                    <option value="1">{{ __('admin-tools.menu.active') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="default-panel-footer default-panel-footer-right">
                <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
                <md-button ng-href="{{ admin_base_path('/tools/rules_pages') }}">{{ __('Назад') }}</md-button>
            </div>
        </form>
@endsection
