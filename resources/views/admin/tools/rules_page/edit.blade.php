@extends('admin.layouts.app')

@section('title', __('Изменить ссылку :name', ['name' => $item->title]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.rules_pages.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/rules_pages/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Обновить ссылку</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.menu.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}" class="form-control" value="{{ $item->getTranslation('title', $form_lang_key) }}" id="title{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group col-md-6">
                            <div class="control-label-br">Страница</div>
                            <select class="form-control selectpicker" name="id_page" data-size="8">
                                @foreach($pages as $page)
                                    <option value="{{ $page->page_id }}" @if($item->id_page == $page->page_id) selected @endif>{{ $page->page_title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-6">
                            <div class="control-label-br">{{ __('admin-tools.menu.status') }}</div>
                            <select name="status" class="form-control selectpicker">
                                <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-tools.menu.un_active') }}</option>
                                <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-tools.menu.active') }}</option>
                            </select>
                        </div>
                    </div>
            </div>
        </div>


        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.menu.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/rules_pages') }}">{{ __('admin-tools.menu.back') }}</md-button>
        </div>
    </form>

@endsection
