@extends('admin.layouts.app')

@section('title', __('Список правил'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.rules_pages'))

@section('top-block')
    <md-button ng-href="rules_pages/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить ссылку') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection


@section('breadcrumbs', Breadcrumbs::render('admin.tools.menu'))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Список правил') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('Название') }}</th>
                    <th>{{ __('Название страницы') }}</th>
                    <th>{{ __('Ссылка на страницу') }}</th>
                    <th>{{ __('Статус') }}</th>
                    <th>{{ __('Посл. обновление') }}</th>
                    <th></th>
                </tr>

                </thead>
                <tbody>

                @foreach($items as $item)
                    <tr>
                        <th>
                            @if(empty($item->title))
                                <a href="rules_pages/{{$item->id}}/edit">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="rules_pages/{{$item->id}}/edit">
                                    {{ $item->title }} &nbsp;<i class="fal fa-pencil"></i>
                                </a>
                            @endif
                        </th>
                        <td>

                            {{ $item->page->page_title }}
                        </td>
                        <td>
                            <div>
                                <a target="_blank" href="{{ url('/rules-page/'.$item->page->page_slug) }}">
                                    <i>{{  $item->page->page_slug }}</i>&nbsp;
                                    <i class="fad fa-link"></i>
                                </a>
                            </div>
                        </td>
                        <td>
                            @if($item->status == 1)
                                <span class="text-success">{{ __('Включено') }}</span>
                            @else
                                <span class="text-danger">{{ __('Отключено') }}</span>
                            @endif
                        </td>
                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                        </td>
                        <td style="width: 50px;">
                            {!! Form::open(['id' => 'rules_pages_destroy_'.$item->id, 'method' => 'DELETE', 'route' => ['rules_pages.destroy', $item->id] ]) !!}
                            {!! Form::close() !!}

                            <a href="javascript:void(0)" onclick="document.getElementById('rules_pages_destroy_{{ $item->id }}').submit()">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">
                        <x-forms.language.input tabName="title_rules_page" keyName="title_rules_page" label="{{ __('Название страницы') }}" />
                        <div class="clearfix"></div>
                        <hr />
                        <x-forms.language.textarea tabName="description_rules_page" keyName="description_rules_page"  label="{{ __('Описание страницы') }}" />
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
