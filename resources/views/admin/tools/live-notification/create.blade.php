@extends('admin.layouts.app')

@section('title', __('admin-tools.live.add_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.live-notification.add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/live-notification') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.live.fon') }}</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="color" data-width="150">
                        <option value="0">{{ __('admin-tools.live.green') }}</option>
                        <option value="1">{{ __('admin-tools.live.yellow') }}</option>
                        <option value="2">{{ __('admin-tools.live.red') }}</option>
                        <option value="3">{{ __('admin-tools.live.priority') }}</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">\
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.live.text_notification') }}</label>
                <div class="col-sm-10">
                    <textarea rows="5" placeholder="{{ __('admin-tools.live.text_notification_hint') }}" name="text" class="form-control" required></textarea>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.live.text_notification_old') }}</label>
                <div class="col-sm-10">
                    <textarea rows="5" placeholder="{{ __('admin-tools.live.text_notification_hint') }}" name="text_alt" class="form-control" required></textarea>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.live.hide_across') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="timeout" value="0" placeholder="{{ __('admin-tools.live.hide_across_hint') }}" class="form-control" required>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.live.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/live-notification') }}">{{ __('admin-tools.live.back') }}</md-button>
        </div>
    </form>

@endsection
