@extends('admin.layouts.app')

@section('title', __('admin-tools.live.title'))

@section('top-block')
    <md-button ng-href="live-notification/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.live.add') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.live-notification'))

@section('content')
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.live.text') }}</th>
                <th>{{ __('admin-tools.live.color') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($notifications) > 0)
                @foreach($notifications as $notification)
                    <tr>
                        <td style="width: 50%">{{$notification->text}}</td>
                        <td>
                            @if($notification->color == 0)
                                <span class="text-success">{{ __('admin-tools.live.green') }}</span>
                            @elseif($notification->color == 1)
                                <span class="text-warning">{{ __('admin-tools.live.yellow') }}</span>
                            @elseif($notification->color == 2)
                                <span class="text-danger">{{ __('admin-tools.live.red') }}</span>
                            @elseif($notification->color == 3)
                                <span class="text-primary">{{ __('admin-tools.live.priority') }}</span>
                            @endif
                        </td>
                        <td style="width: 200px;">
                            <div class="col-md-6">
                                <a  href="live-notification/{{$notification->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-tools.live.edit_button') }}</a>
                            </div>

                            <div class="col-md-6">
                                {!! Form::open(['method' => 'DELETE', 'route' => ['live-notification.destroy', $notification->id] ]) !!}
                                {!! Form::submit(__('admin-tools.live.delete_button'), ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
@endsection
