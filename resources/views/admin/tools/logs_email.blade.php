@extends('admin.layouts.app')

@section('title', __('admin-tools.logs_email.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.logs-email'))

@section('content')
    @if(config('admin.is_demo_mode') == true)
        <div class="alert alert-danger">{{ __('admin-tools.not_demo_version') }}</div>
    @else
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.logs_email.created_at') }}</th>
                <th>{{ __('admin-tools.logs_email.from') }}</th>
                <th>{{ __('admin-tools.logs_email.to') }}</th>
                <th>{{ __('admin-tools.logs_email.headline') }}</th>
            </tr>

            </thead>
            <tbody>
            @if(count($logs) > 0)
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->created_at }}</td>
                        <td>
                            @if(!empty($log->from_mail))
                                {{ $log->from_mail }}
                            @else
                                <span class="text-muted">{{ __('admin-tools.logs_email.not_specified') }}</span>
                            @endif
                        </td>
                        <td>
                            @if(!empty($log->to_mail))
                                {{ $log->to_mail }}
                            @else
                                <span class="text-muted">{{ __('admin-tools.logs_email.not_specified') }}</span>
                            @endif
                        </td>
                        <td>
                            {!! $log->subject !!}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
    @endif
@endsection


@section('pagination')
    {!! $logs->links() !!}
@endsection
