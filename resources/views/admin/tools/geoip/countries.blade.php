@extends('admin.layouts.app')

@section('title', __('Страны'))

@section('top-block')

@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.geoip.countries'))

@section('content')
    <form style="width:100%;margin-right: 10px;" method="POST" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th >{{ __('Страна') }}</th>
            </tr>
            </thead>
            <tbody>

            @if(count($countries) > 0)
                @foreach($countries as $country)
                    <tr>
                        <td>
                            <input type="hidden" name="item_id[]" value="{{ $country->id }}">
                            <div class="multi-language-form-group-input">
                                    <ul class="nav nav-pills lang-tabs">
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                <a class="lang-tabs-link" data-toggle="tab" href="#value-{{ $country->id }}-{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                    <div id="value-{{ $country->id }}-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                        <input type="text" class="form-control"  name="value{{$form_lang['field']}}[{{ $country->id }}]" value="{{ $country->getTranslation('value', $form_lang_key) }}">
                                    </div>
                                @endforeach
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="0">{{ __('Действие') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
