@extends('admin.layouts.app')

@section('title', __('admin-tools.geoip.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.geoip.settings'))

@section('content')

    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">{{ __('admin-tools.geoip.driver') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-sm-3">
                        <div class="control-label-br">{{ __('admin-tools.geoip.select_driver') }}</div>
                        <select class="selectpicker form-control" name="GEOIP_DRIVER" data-live-search="true">
                            @foreach(config('geoip.list_drivers') as $key => $driver)
                                <option value="{{ $key }}" @if(config('geoip.service') == $key) selected @endif>{{ $driver }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="form-group">
                <label for="exchange_rate1" class="col-sm-2 control-label">Maxind API</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">User ID</div>
                        <input type="text" name="MAXMIND_USER_ID" class="form-control" id="MAXMIND_USER_ID" value="{{ config('geoip.services.maxmind_api.user_id') }}">
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">License key</div>
                        <input type="text" name="MAXMIND_LICENSE_KEY" class="form-control" id="MAXMIND_LICENSE_KEY" value="{{ config('geoip.services.maxmind_api.license_key') }}">
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label for="exchange_rate1" class="col-sm-2 control-label">IpApi</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">Key</div>
                        <input type="text" name="IPAPI_KEY" class="form-control" id="IPAPI_KEY" value="{{ config('geoip.services.ipapi.key') }}">
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label for="exchange_rate1" class="col-sm-2 control-label">IP GeoLocation</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">Key</div>
                        <input type="text" name="IPGEOLOCATION_KEY" class="form-control" id="IPGEOLOCATION_KEY" value="{{ config('geoip.services.ipgeolocation.key') }}">
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label for="exchange_rate1" class="col-sm-2 control-label">IP Data</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">Key</div>
                        <input type="text" name="IPDATA_API_KEY" class="form-control" id="IPDATA_API_KEY" value="{{ config('geoip.services.ipdata.key') }}">
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.geoip.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/your_course') }}">{{ __('admin-tools.geoip.back') }}</md-button>
        </div>
    </form>

@endsection
