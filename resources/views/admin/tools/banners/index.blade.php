@extends('admin.layouts.app')

@section('title', __('Баннеры'))

@section('top-block')
    <md-button ng-href="banners/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить баннер') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.banners'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($banners) > 0)
            @foreach($banners as $banner)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/banners/'.$banner->id.'/edit') }}">
                            {{ $banner->title }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>
                        @if($banner->status == 0)
                            <div class="text-success">{{ __('Включено') }}</div>
                        @else
                            <div class="text-danger">{{ __('Отключено') }}</div>
                        @endif
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($banner->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($banner->updated_at)->diffForHumans() }}</small>
                    </td>

                    <td style="width: 100px;">
                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/banners/'.$banner->id.'/destroy')  }}">
                            <i class="fa fa-fw fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form method="post" action="?" enctype="multipart/form-data">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group col-md-6">
                            <label class="control-label control-label-br">{{ __('Автопролистывание баннеров') }}</label>
                            <select class="form-control selectpicker" name="is_banner_autoplay">
                                <option value="0" @if(iEXSetting('is_banner_autoplay') == 0) selected @endif>{{ __('Отключить') }}</option>
                                <option value="1" @if(iEXSetting('is_banner_autoplay') == 1) selected @endif>{{ __('Включить') }}</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label control-label-br">{{ __('Время задержки каждого баннера (в сек.)') }}</label>
                            <input type="number" name="number_banner_autoplay_timeout" class="form-control" value="{{ iEXSetting('number_banner_autoplay_timeout', 2) }}">
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group col-md-12">
                            <label class="control-label control-label-br">{{ __('Скрыть навигацию') }}</label>
                            <select class="form-control selectpicker" name="is_banner_hidden_nav">
                                <option value="0" @if(iEXSetting('is_banner_hidden_nav') == 0) selected @endif>{{ __('Нет') }}</option>
                                <option value="1" @if(iEXSetting('is_banner_hidden_nav') == 1) selected @endif>{{ __('Да') }}</option>
                            </select>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отменить') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
