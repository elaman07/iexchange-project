@extends('admin.layouts.app')

@section('title', __('Добавить баннер'))


@section('breadcrumbs', Breadcrumbs::render('admin.tools.banners.create'))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/banners') }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Информация') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}" class="form-control" id="title{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Цвет текста') }}</div>
                        <div id="cp1" class="input-group colorpicker-component">
                            <input type="text" name="color_title" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea rows="5" id="text{{$form_lang['field']}}" name="text{{$form_lang['field']}}" class="form-control"></textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Цвет текста') }}</div>
                        <div id="cp2" class="input-group colorpicker-component">
                            <input type="text" name="color_text" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Кнопки') }}</div>
                        {{ Form::select('ids_buttons[]', $buttons, null, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '8']) }}
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фотография') }}</div>
                        <input type="file" name="images" class="form-control" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr/>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фон баннера (не обязательно)') }}</div>
                        <input type="file" name="images_banner" class="form-control">
                    </div>

                    <div class="clearfix"></div>
                    <hr/>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if(old('status') == 0) selected @endif>{{ __('Включено') }}</option>
                            <option value="1" @if(old('status') == 1) selected @endif>{{ __('Отключено') }}</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/banners') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
