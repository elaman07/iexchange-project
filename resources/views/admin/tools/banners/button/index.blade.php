@extends('admin.layouts.app')

@section('title', __('Кнопки для баннеров'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.banners.button'))

@section('top-block')
    <md-button ng-href="banners-button/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить кнопку') }}</span>
    </md-button>
@endsection

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Ссылка') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($banners) > 0)
            @foreach($banners as $banner)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/banners-button/'.$banner->id.'/edit') }}">
                            {{ $banner->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>
                        {{ $banner->link }}
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($banner->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($banner->updated_at)->diffForHumans() }}</small>
                    </td>

                    <td style="width: 100px;">
                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/banners-button/'.$banner->id.'/destroy')  }}">
                            <i class="fa fa-fw fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
