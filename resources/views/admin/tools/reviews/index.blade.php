@extends('admin.layouts.app')

@section('title', __('admin-tools.reviews.title'))

@section('top-block')
    <md-button ng-href="reviews/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.reviews.add_review') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-tools.reviews.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.reviews'))

@section('no-block-content')

    <div class="x_panel">

        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-tools.reviews.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-tools.reviews.name') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'name', is_filter_search($filter, 'name'), ['class' => 'form-control']) }}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_full_name" @if(isset($filter['checkbox_full_name'])) checked @endif>
                                            <label class="form-check-label" for="checkbox1">{{ __('admin-tools.reviews.exact_name') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-tools.reviews.ip_address') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'ip_address', is_filter_search($filter, 'ip_address'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-tools.reviews.status') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('status[]', $statuses, is_filter_search($filter, 'status'),
                                            ['class' => 'selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-tools.reviews.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-tools.reviews.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-tools.reviews.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <form method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('Автор') }}</th>
                        <th>{{ __('Заявка №') }}</th>
                        <th>{{ __('Дата публикации') }}</th>
                        <th>{{ __('Общая оценка') }}</th>
                        <th>{{ __('IP Адрес') }}</th>
                        <th>{{ __('Текст отзыва') }}</th>
                        <th>{{ __('Статус') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(count($reviews) > 0)
                        @foreach($reviews as $review)
                            <tr>
                                <th>
                                    <a ng-href="{{ admin_base_path('/tools/reviews/'.$review->id.'/edit') }}">
                                        <div>{{$review->name}} &nbsp;<i class="fal fa-pencil"></i></div>
                                    </a>
                                </th>
                                <td>
                                    @if($review->id_task > 0 and isset($review->tasks))
                                        <a href="{{ admin_base_path('/tx/'.$review->id_task) }}?actions=arhive">№{{ $review->id_task }} ({{ $review->tasks->public_id }})</a>
                                    @endif
                                </td>
                                <td>{{ $review->created_at }}</td>

                                <td>
                                    @if($review->rate_speed != 0)
                                        @for($i = 0; $i < $review->rate_speed; $i++)
                                            <i class="text-warning fa fa-star"></i>
                                        @endfor
                                    @else
                                        <small class="text-danger">{{ __('нет оценки') }}</small>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($review->ip_address))
                                        {{ $review->ip_address }}
                                    @else
                                        <small class="text-muted">{{ __('не определен') }}</small>
                                    @endif
                                </td>
                                <td style="width: 400px">{{ $review->text }}</td>
                                <td>
                                    @if($review->status == 0)
                                        <input type="hidden" name="item_id[]" value="{{ $review->id }}">
                                        <select class="selectpicker" name="status[{{$review->id}}]">
                                            <option value="0" @if($review->status == 0) selected @endif>{{ __('admin-tools.reviews.on_check') }}</option>
                                            <option value="1" @if($review->status == 1) selected @endif>{{ __('admin-tools.reviews.published') }}</option>
                                            <option value="2" @if($review->status == 2) selected @endif>{{ __('admin-tools.reviews.blocked') }}</option>
                                        </select>
                                    @elseif($review->status == 1)
                                        <span class="label label-success">{{ __('admin-tools.reviews.published') }}</span>
                                    @else
                                        <span class="label label-danger">{{ __('admin-tools.reviews.blocked') }}</span>
                                    @endif
                                </td>
                                <td style="width:100px">
                                    <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/reviews/'.$review->id.'/delete')  }}">
                                        <i class="fa fa-fw fa-trash text-danger"></i>
                                    </md-button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('admin-tools.reviews.save') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.reviews.run') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>

        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-tools.reviews.settings_review') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-8">{{ __('admin-tools.reviews.allow_add_review') }}</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_reviews" name="is_enabled_reviews" type="checkbox" value="1" @if(iEXSetting('is_enabled_reviews') == 1) checked @endif />
                                    <label for="is_enabled_reviews" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">{{ __('admin-tools.reviews.allow_add_review_one_ip') }}</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="checked_ip_address_reviews" name="checked_ip_address_reviews" type="checkbox" value="1" @if(iEXSetting('checked_ip_address_reviews') == 1) checked @endif />
                                    <label for="checked_ip_address_reviews" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <hr />


                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-tools.reviews.count_review_one_ip') }}</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="input_num_reviews" style="margin: 0 auto;width: 150px;text-align: center" value="{{ iEXSetting('input_num_reviews', 1) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-tools.reviews.count_review_page') }}</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="count_reviews_exchange" style="margin: 0 auto;width: 150px;text-align: center" value="{{ iEXSetting('count_reviews_page', 20) }}">
                            </div>
                        </div>


                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-tools.reviews.who_enable_review') }}</label>
                            <div class="col-md-6">
                                <select class="form-control selectpicker" name="is_auth_type_reviews">
                                    <option value="0" @if(iEXSetting('is_auth_type_reviews') == 0) selected @endif>{{ __('admin-tools.reviews.all') }}</option>
                                    <option value="1" @if(iEXSetting('is_auth_type_reviews') == 1) selected @endif>{{ __('admin-tools.reviews.only_auth') }}</option>
                                    <option value="2" @if(iEXSetting('is_auth_type_reviews') == 2) selected @endif>{{ __('admin-tools.reviews.only_client_first_order') }}</option>
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.reviews.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-tools.reviews.back') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
