@extends('admin.layouts.app')

@section('title', __('admin-tools.reviews.edit_title', ['id' => $item->id]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.reviews.edit', $item->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/reviews/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.reviews.name') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="name" placeholder="{{ __('admin-tools.reviews.name') }}" value="{{$item->name}}" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.reviews.email') }}</label>
                <div class="col-sm-5">
                    <input type="email" name="email" placeholder="{{ __('admin-tools.reviews.email_hint') }}" value="{{$item->email}}" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.reviews.type_review') }}</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" name="type">
                        <option value="0" @if($item->type == 0) selected @endif>{{ __('admin-tools.reviews.positive') }}</option>
                        <option value="1" @if($item->type == 1) selected @endif>{{ __('admin-tools.reviews.negative') }}</option>
                    </select>
                </div>
            </div>


            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.reviews.text_review') }}</label>
                <div class="col-sm-5">
                    <textarea class="form-control" rows="5" name="text">{{$item->text}}</textarea>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.reviews.status') }}</label>
                <div class="col-sm-5">
                    <select class="selectpicker" name="status">
                        <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-tools.reviews.on_check') }}</option>
                        <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-tools.reviews.published') }}</option>
                        <option value="2" @if($item->status == 2) selected @endif>{{ __('admin-tools.reviews.blocked') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.reviews.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/reviews') }}">{{ __('admin-tools.reviews.back') }}</md-button>
        </div>
    </form>
@endsection
