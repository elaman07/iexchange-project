@extends('admin.layouts.app')

@section('title', __('Экспорт курсов'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.generator_currency'))

@section('content')
    <div class="systemsettings">
        <div class="table-responsives">
            <form action="?" method="POST" class="form-horizontal">
                @csrf
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <table class="table table-2 table-striped">
                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Название файла') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Укажите название для экспортного курса файлов') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <input type="text" class="form-control" style="width: 300px" id="grates_filename" name="grates_filename" value="{{ iEXSetting('grates_filename') }}">
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Тип отображения знаков после запятой') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите из доступного списка тип отображения знаков после запятой') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <select class="form-control selectpicker" name="grates_type_number_format" data-width="300px">
                                        <option value="0" @if(iEXSetting('grates_type_number_format') == 0) selected @endif>{{ __('От настроек валюты (Для сайта)') }}</option>
                                        <option value="1" @if(iEXSetting('grates_type_number_format') == 1) selected @endif>{{ __('Принудительно') }}</option>
                                        <option value="2" @if(iEXSetting('grates_type_number_format') == 2) selected @endif>{{ __('От настроек валюты (Для XML)') }}</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Количество знаков после запятой (принудительно)') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Укажите принудительно количество знаков после запятой для экспортного файла курсов') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <input type="text" class="form-control" style="width: 300px;text-align: center" id="grates_number_format" name="grates_number_format" value="{{ iEXSetting('grates_number_format') }}">
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Разрешить отображение неактивных валют') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Если функция включена, то все отключенные направления будут отображены в экспортном файле') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <div class="material-switch">
                                        <input id="grates_inactive_currencies" name="grates_inactive_currencies" type="checkbox" value="1" @if(iEXSetting('grates_inactive_currencies') == 1) checked @endif />
                                        <label for="grates_inactive_currencies" class="label-primary"></label>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Отключить направление, если резерв меньше чем мин. сумма обмена') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Если функция включена, то в экспортном файле будут отключены направления, резерв которых меньше чем установленна минимальная цена') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <div class="material-switch">
                                        <input id="grates_inactive_min_reserve" name="grates_inactive_min_reserve" type="checkbox" value="1" @if(iEXSetting('grates_inactive_min_reserve') == 1) checked @endif />
                                        <label for="grates_inactive_min_reserve" class="label-primary"></label>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Отключить файлы мгновенное, если обменник на тех. работах') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Если функция включена, то в случае если обменник переводится на тех.работы, экспортные файлы мгновенно будут очищены') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <div class="material-switch">
                                        <input id="grates_is_disabled_export_file" name="grates_is_disabled_export_file" type="checkbox" value="1" @if(iEXSetting('grates_is_disabled_export_file') == 1) checked @endif />
                                        <label for="grates_is_disabled_export_file" class="label-primary"></label>
                                    </div>
                                </td>
                            </tr>


                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Разрешенные типы файлов') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите из списка типы файлов, которые хотите активировать') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <div class="form-check">
                                        <input class="form-check-input" id="grates_format_xml" type="checkbox" value="1" name="grates_format_xml" @if(iEXSetting('grates_format_xml') == 1) checked @endif>
                                        <label class="form-check-label" for="grates_format_xml">XML <a target="_blank" href="{{ url(iEXSetting('grates_filename').'.xml') }}">{{ __('Открыть') }}</a></label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" id="grates_format_txt" type="checkbox" value="1" name="grates_format_txt" @if(iEXSetting('grates_format_txt') == 1) checked @endif>
                                        <label class="form-check-label" for="grates_format_txt">TXT <a target="_blank" href="{{ url(iEXSetting('grates_filename').'.txt') }}">{{ __('Открыть') }}</a></label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" id="grates_format_json" type="checkbox" value="1" name="grates_format_json" @if(iEXSetting('grates_format_json') == 1) checked @endif>
                                        <label class="form-check-label" for="grates_format_json">JSON <a target="_blank" href="{{ url(iEXSetting('grates_filename').'.json') }}">{{ __('Открыть') }}</a></label>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Способ работы курсов') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите способ получения курсов в экспортный файл') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <select class="form-control selectpicker" name="grates_static" data-width="300px">
                                        <option value="0" @if(iEXSetting('grates_static') == 0) selected @endif>{{ __('Статический (Рекомендуется)') }}</option>
                                        <option value="1" @if(iEXSetting('grates_static') == 1) selected @endif>{{ __('В реальном времени') }}</option>
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Параметр для fromfee') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите из списка тип передачи дополнительных комиссий в файл курсов') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <select class="form-control selectpicker" name="is_in_type_fromfee" data-width="300px">
                                        <option value="0" @if(iEXSetting('is_in_type_fromfee') == 0) selected @endif>{{ __('Дополнительная комиссия (Отдаю)') }}</option>
                                        <option value="1" @if(iEXSetting('is_in_type_fromfee') == 1) selected @endif>{{ __('Комиссия платежной системы (Отдаю)') }}</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Параметр для tofee') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите из списка тип передачи дополнительных комиссий в файл курсов') }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <select class="form-control selectpicker" name="is_in_type_tofee" data-width="300px">
                                        <option value="0" @if(iEXSetting('is_in_type_tofee') == 0) selected @endif>{{ __('Дополнительная комиссия (Получаю)') }}</option>
                                        <option value="1" @if(iEXSetting('is_in_type_tofee') == 1) selected @endif>{{ __('Комиссия платежной системы (Получаю)') }}</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Включить Мин. сумму для файлов курса') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Минимальная возможная к разовому обмену сумма валюты, которую обменный пункт принимает от клиента') }}: {{ "<minamount>4.1 USD</minamount>" }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <div class="material-switch">
                                        <input id="grates_is_minamount" name="grates_is_minamount" type="checkbox" value="1" @if(iEXSetting('grates_is_minamount') == 1) checked @endif />
                                        <label for="grates_is_minamount" class="label-primary"></label>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('Включить Макс. сумму для файлов курса') }}:</h6>
                                    <span class="text-muted text-size-small hidden-xs">{{ __('Максимальная возможная к разовому обмену сумма валюты, которую обменный пункт принимает от клиента') }}: {{ "<maxamount>5000 USD</maxamount>" }}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <div class="material-switch">
                                        <input id="grates_is_maxamount" name="grates_is_maxamount" type="checkbox" value="1" @if(iEXSetting('grates_is_maxamount') == 1) checked @endif />
                                        <label for="grates_is_maxamount" class="label-primary"></label>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Сохранить') }}</md-button>
                </div>
            </form>
        </div>
    </div>
@endsection
