@extends('admin.layouts.app')

@section('title', 'Изменить хранилище '.$item->name)


@section('breadcrumbs', Breadcrumbs::render('admin.tools.file-storages', $item))

@section('content')

    <form action="{{ admin_base_path('/tools/file-storages/'.$item->id) }}" method="POST" class="form-horizontal">
        @csrf
        @method('PUT')

        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Основное</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $item->name }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Хранилище</div>
                        {{ Form::select('disk_type', $storages, $item->disk, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '15', 'disabled']) }}
                    </div>


                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>Отключен</option>
                            <option value="1" @if($item->status == 1) selected @endif>Включен</option>
                        </select>
                    </div>


                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">Настройки хранилища</label>
                <div class="col-sm-10">
                    @if(view()->exists('admin.tools.file_storages.include.'.$item->disk_type))
                        @include('admin.tools.file_storages.include.'.$item->disk_type)
                    @else
                        <div class="merchant-alert-notify text-center">Не найдены конфигурационные данные</div>
                    @endif
                </div>
            </div>

            <div class="clearfix"></div>

        </div>


        <div class="clearfix"></div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/file-storages') }}">Назад</md-button>
        </div>
    </form>
@endsection
