<div class="form-group col-md-6">
    <div class="control-label-br">Адрес FTP сервера</div>
    <input type="text" name="host" class="form-control" id="host" placeholder="{{ security_pay_data($data, 'host') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Логин для подключения к FTP серверу</div>
    <input type="text" name="username" class="form-control" id="username" placeholder="{{ security_pay_data($data, 'username') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Пароль для подключения к FTP серверу</div>
    <input type="text" name="password" class="form-control" id="password" placeholder="{{ security_pay_data($data, 'password') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Порт FTP сервера</div>
    <input type="text" name="port" class="form-control" id="port" placeholder="{{ security_pay_data($data, 'port') }}">
</div>
