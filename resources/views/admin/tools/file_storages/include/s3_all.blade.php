<div class="form-group col-md-6">
    <div class="control-label-br">Key</div>
    <input type="text" name="key" class="form-control" id="client_id" placeholder="{{ security_pay_data($data, 'key') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Secret</div>
    <input type="text" name="secret" class="form-control" id="secret" placeholder="{{ security_pay_data($data, 'secret') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Endpoint</div>
    <input type="text" name="endpoint" class="form-control" id="endpoint" placeholder="{{ security_pay_data($data, 'endpoint') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Url</div>
    <input type="text" name="url" class="form-control" id="url" placeholder="{{ security_pay_data($data, 'url') }}">
</div>


<div class="form-group col-md-6">
    <div class="control-label-br">Region</div>
    <input type="text" name="region" class="form-control" id="region" placeholder="{{ security_pay_data($data, 'region') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Bucket</div>
    <input type="text" name="bucket" class="form-control" id="bucket" placeholder="{{ security_pay_data($data, 'bucket') }}">
</div>
