@extends('admin.layouts.app')

@section('title', __('Список хранилищ'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить хранилище') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.file-storages'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название хранилища') }}</th>
            <th>{{ __('Тип') }}</th>
            <th>{{ __('Статус') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/file-storages/'.$item->id.'/edit') }}">
                            {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>

                    <td>
                        {{ $storages[$item->disk_type] ?? null }}
                    </td>

                    <td>
                        @if($item->status == 0)
                            <div class="label label-danger">{{ __('Отключен') }}</div>
                        @else
                            <div class="label label-success">{{ __('Включен') }}</div>
                        @endif
                    </td>

                    <td style="width: 100px;">
                        <md-button title="{{ __('Удалить') }}" href="file-storages/{{$item->id}}/delete" class="md-icon-button">
                            <i class="fas fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>


    <!-- Модальное окно для создания хранилище -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/file-storages') }}">
                <input type="hidden" name="action" value="create">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Добавить хранилище') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Тип хранилища') }}</label>
                            <div class="col-md-6">
                                {!! Form::select('disk', $storages, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Создать') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка загрузки файлов на сервер') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить модуль') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_module_storage_disk" data-width="250px">
                                    <option value="0" @if(iEXSetting('is_module_storage_disk') == 0) selected @endif>Отключен</option>
                                    <option value="1" @if(iEXSetting('is_module_storage_disk') == 1) selected @endif>Включен</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

