@extends('admin.layouts.app')

@section('title', __('admin-tools.pr.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.collaboration-pr.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/collaboration-pr/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.pr.type_contact') }}</label>
                <div class="col-sm-5">
                    <select class="selectpicker" name="type" data-live-search="true">
                        @foreach($listContactsTypes as $key => $value)
                            <option value="{{$key}}" @if($item->type == $key) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.pr.name_system') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="name" placeholder="{{ __('admin-tools.pr.name_system_hint') }}" value="{{$item->name}}" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.pr.contact_data') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="value" placeholder="{{ __('admin-tools.pr.contact_data_hint') }}" value="{{$item->value}}" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.pr.link_contact') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="url" value="{{$item->url}}" placeholder="{{ __('admin-tools.pr.link_contact_hint') }}" class="form-control">
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-tools.pr.make_button') }}</label>
                <div class="col-sm-2">
                    <select class="selectpicker" name="is_button">
                        <option value="0" @if($item->is_button == 0) selected @endif>{{ __('admin-tools.pr.no') }}</option>
                        <option value="1" @if($item->is_button == 1) selected @endif>{{ __('admin-tools.pr.yes') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.pr.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/collaboration-pr') }}">{{ __('admin-tools.pr.back') }}</md-button>
        </div>
    </form>
@endsection
