@extends('admin.layouts.app')

@section('title', __('admin-tools.pr.title'))

@section('top-block')
    <md-button ng-href="collaboration-pr/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.pr.add_contact') }}</span>
    </md-button>

    <md-button ng-href="collaboration-pr/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-sort text-primary"></md-icon>
        <span>Сортировка</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-tools.pr.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.collaboration-pr'))

@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.pr.name') }}</th>
                <th>{{ __('admin-tools.pr.contact') }}</th>
                <th>{{ __('admin-tools.pr.link') }}</th>
                <th>{{ __('admin-tools.pr.status') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($contacts) > 0)
                @foreach($contacts as $contact)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('/tools/collaboration-pr/'.$contact->id.'/edit') }}">
                                {{ $contact->name }} &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>{{$contact->value}}</td>
                        <td>
                            @if(empty($contact->url))
                                <small class="text-warning">{{ __('admin-tools.pr.link_not_specified') }}</small>
                            @else
                                {{$contact->url}}
                            @endif
                        </td>
                        <td>
                            <input type="hidden" name="item_id[]" value="{{$contact->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$contact->id}}" name="status[{{$contact->id}}]" type="checkbox" value="1" @if($contact->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$contact->id}}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width: 100px;">

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/collaboration-pr/'.$contact->id.'/delete')  }}">
                                <i class="fa fa-fw fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-tools.pr.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.pr.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-tools.pr.settings') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-tools.pr.description') }}</label>
                            <div class="col-md-8">
                                <x-forms.language.textarea tabName="description_pr" keyName="description_pr" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.pr.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-tools.pr.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
