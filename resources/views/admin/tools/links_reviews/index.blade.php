@extends('admin.layouts.app')

@section('title', __('admin-tools.links_reviews.title'))

@section('top-block')
    <md-button ng-href="links_reviews/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.links_reviews.add_link') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-tools.links_reviews.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.links_reviews'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-tools.links_reviews.name') }}</th>
            <th>{{ __('admin-tools.links_reviews.link') }}</th>
            <th>{{ __('admin-tools.links_reviews.last_update') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($links) > 0)
            @foreach($links as $link)
                <tr>
                    <th>
                        @if(empty($link->name))
                            <a href="links_reviews/{{ $link->id }}/edit">
                                <small class="text-danger">
                                    <i class="far fa-exclamation-circle"></i>
                                    Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                </small>
                            </a>
                        @else
                            <a href="{{ admin_base_path('/tools/links_reviews/'.$link->id.'/edit') }}">
                                {{ $link->name }} &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        @endif
                    </th>
                    <td>{{$link->url}}</td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($link->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($link->updated_at)->diffForHumans() }}</small>
                    </td>


                    <td style="width: 50px;">
                        {!! Form::open(['id' => 'link-review_destroy_'.$link->id, 'method' => 'DELETE', 'route' => ['links_reviews.destroy', $link->id] ]) !!}
                        {!! Form::close() !!}

                        <a href="javascript:void(0)" onclick="document.getElementById('link-review_destroy_{{ $link->id }}').submit()">
                            <i class="fas fa-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-tools.links_reviews.settings') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-tools.links_reviews.description') }}</label>
                            <div class="col-md-8">
                                <x-forms.language.textarea tabName="description_review" keyName="description_review" />
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Отключить возможность оставить отзывы в карточке заявки') }}</label>
                            <div class="col-md-6" id="is_disabled_writing_links_reviews">
                                <select class="selectpicker" name="is_disabled_writing_links_reviews" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_disabled_writing_links_reviews') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_disabled_writing_links_reviews') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.links_reviews.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-tools.links_reviews.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
