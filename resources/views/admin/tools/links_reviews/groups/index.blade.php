@extends('admin.layouts.app')

@section('title', __('admin-tools.links_reviews_group.title'))

@section('top-block')
    <md-button ng-href="links_reviews_group/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.links_reviews_group.add_group') }}</span>
    </md-button>

@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.links_reviews_group'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-tools.links_reviews_group.name') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($links) > 0)
            @foreach($links as $link)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/links_reviews_group/'.$link->id.'/edit') }}">
                            {{ $link->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td style="width: 100px;">
                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['links_reviews_group.destroy', $link->id] ]) !!}
                            {!! Form::submit(__('admin-tools.links_reviews_group.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
