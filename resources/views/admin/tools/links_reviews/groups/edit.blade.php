@extends('admin.layouts.app')

@section('title', __('admin-tools.links_reviews_group.edit_title', ['name' => $link->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.links_reviews_group.edit', $link))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/links_reviews_group/'.$link->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Обновить группу</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.links_reviews_group.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}" value="{{ $link->getTranslation('name', $form_lang_key) }}" placeholder="{{ __('admin-tools.links_reviews_group.name_hint') }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Сортировка</div>
                        <input type="number" name="sorting" class="form-control" value="{{ $link->sorting }}" required>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.links_reviews_group.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/links_reviews_group') }}">{{ __('admin-tools.links_reviews_group.back') }}</md-button>
        </div>
    </form>
@endsection
