@extends('admin.layouts.app')

@section('title', __('Добавить расписание'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/job-schedule') }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Информация') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" class="form-control">
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Статус работы') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0">{{ __('В сети') }}</option>
                            <option value="1">{{ __('Не в сети') }}</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Рабочее время (От)') }}</div>
                        <input type="text" name="from_time" class="form-control timepicker">
                    </div>

                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Рабочее время (До)') }}</div>
                        <input type="text" name="to_time" class="form-control timepicker">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Рабочие дни') }}</div>
                        {{ Form::select('work_days[]', $dayNames, null, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '8', 'multiple']) }}
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/job-schedule') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
