@extends('admin.layouts.app')

@section('title', __('Изменить расписание'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/job-schedule/'.$item->id) }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Информация') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" class="form-control" value="{{ $item->name }}">
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Статус работы') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('В сети') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Не в сети') }}</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Рабочее время (От)') }}</div>
                        <input type="text" name="from_time" class="form-control timepicker" value="{{ $item->from_time }}">
                    </div>

                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Рабочее время (До)') }}</div>
                        <input type="text" name="to_time" class="form-control timepicker" value="{{ $item->to_time }}">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Рабочие дни') }}</div>
                        {{ Form::select('work_days[]', $dayNames, explode(',', $item->work_days), ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '8', 'multiple']) }}
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/job-schedule') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
