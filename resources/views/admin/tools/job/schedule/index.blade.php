@extends('admin.layouts.app')

@section('title', __('Расписание работы'))

@section('video__instruction', 'https://www.youtube.com/embed/gQOZa6Zz02U?autoplay=1&amp;mute=0')


@section('top-block')
    <md-button ng-href="job-schedule/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить расписание') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.advantage'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Рабочее время') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/tools/job-schedule/'.$item->id.'/edit') }}">
                            {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>
                        @if($item->status == 0)
                            <span class="text-success">{{ __('В сети') }}</span>
                        @else
                            <span class="text-warning">{{ __('Не в сети') }}</span>
                        @endif
                    </td>

                    <td>
                       {{ $item->from_time }}-{{ $item->to_time }}
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                    </td>

                    <td style="width: 100px;">
                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/job-schedule/'.$item->id.'/destroy')  }}">
                            <i class="fa fa-fw fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
