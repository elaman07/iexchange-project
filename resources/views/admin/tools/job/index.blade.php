@extends('admin.layouts.app')

@section('title',  __('Статус работы сервиса'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.job'))

@section('no-block-content')
    <form action="?action=base" method="POST" class="form-horizontal">
        @csrf

        <div layout="row">
            <div style="margin-right: 10px" class="x_panel">
                <div class="x_title">
                    <h2>{{ __('Настройка режимов') }}</h2>
                    <div class="clearfix"></div>
                </div>
                <br />
                <div class="x_content">
                    <div class="default-panel-body">
                        <div class="form-group">
                            <label for="type_job" class="col-sm-3 control-label">{{ __('Выберите режим работы') }}</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" name="type_working_mode">
                                    <option value="1" @if(iEXSetting('type_working_mode') == 1) selected @endif>{{ __('По расписанию') }}</option>
                                    <option value="2" @if(iEXSetting('type_working_mode') == 2) selected @endif>{{ __('Вручную') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ __('Настройка вручную') }}</h2>
                    <div class="clearfix"></div>
                </div>
                <br />
                <div class="x_content">
                    <div class="default-panel-body">
                        <div class="form-group">
                            <label for="cron_status" class="col-sm-3 control-label">{{ __('admin-tools.job.job_status') }}</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" name="is_working_manual">
                                    <option value="0" @if(iEXSetting('is_working_manual') == 0) selected @endif>{{ __('admin-tools.job.online') }}</option>
                                    <option value="1" @if(iEXSetting('is_working_manual') == 1) selected @endif>{{ __('admin-tools.job.offline') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>{{ __('Общие настройки') }}</h2>
                <div class="clearfix"></div>
            </div>
            <br />
            <div class="x_content">

                <table class="table table-2 table-striped">
                    <tr>
                        <td class="col-xs-6 col-sm-6 col-md-7">
                            <h6 class="media-heading font-size-14 text-semibold">{{ __('Разрешить отображение статуса оператора на сайте') }}:</h6>
                            <span class="text-muted font-size-12 hidden-xs">{{ __("Если выбрано 'Да', то в шапке сайте будет отображаться статус оператора") }}</span>
                        </td>
                        <td class="col-xs-6 col-sm-6 col-md-5">
                            <select class="form-control selectpicker" name="is_working_view_header" data-width="300px">
                                <option value="0" @if(iEXSetting('is_working_view_header') == 0) selected @endif>{{ __('Нет') }}</option>
                                <option value="1" @if(iEXSetting('is_working_view_header') == 1) selected @endif>{{ __('Да') }}</option>
                            </select>
                        </td>
                    </tr>


                    <tr>
                        <td class="col-xs-6 col-sm-6 col-md-7">
                            <h6 class="media-heading font-size-14 text-semibold">{{ __('Текст, когда оператор в сети') }}:</h6>
                            <span class="text-muted font-size-12 hidden-xs">{{ __('Укажите текст, определяющий статус оператора. Например: Оператор: В сети') }}</span>
                        </td>
                        <td class="col-xs-6 col-sm-6 col-md-5">
                            <x-forms.language.input tabName="working_online_text" keyName="working_online_text" />
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-6 col-sm-6 col-md-7">
                            <h6 class="media-heading font-size-14 text-semibold">{{ __('Текст, когда оператор не в сети') }}:</h6>
                            <span class="text-muted font-size-12 hidden-xs">{{ __('Укажите текст, определяющий статус оператора. Например: Оператор: Не в сети') }}</span>
                        </td>
                        <td class="col-xs-6 col-sm-6 col-md-5">
                            <x-forms.language.input tabName="working_offline_text" keyName="working_offline_text" />
                        </td>
                    </tr>

{{--                    <tr>--}}
{{--                        <td class="col-xs-6 col-sm-6 col-md-7">--}}
{{--                            <h6 class="media-heading font-size-14 text-semibold">{{ __('Текст уведомления на всех страницах, когда оператор не в сети') }}:</h6>--}}
{{--                            <span class="text-muted font-size-12 hidden-xs">{{ __('Укажите текст уведомления, которая будет выводиться на всех страницах') }}</span>--}}
{{--                        </td>--}}
{{--                        <td class="col-xs-6 col-sm-6 col-md-5">--}}
{{--                            <x-forms.language.input tabName="working_offline_notify" keyName="working_offline_notify" />--}}
{{--                        </td>--}}
{{--                    </tr>--}}
                </table>
            </div>
        </div>

        <div class="modal-footer">
            <md-button type="submit" class="md-raised md-primary">{{ __('admin-tools.job.save') }}</md-button>
        </div>
    </form>

@endsection
