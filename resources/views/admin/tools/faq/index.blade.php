@extends('admin.layouts.app')

@section('title', __('admin-tools.faq.title'))

@section('video__instruction', 'https://www.youtube.com/embed/166TEwIW0cI?autoplay=1&amp;mute=0')


@section('top-block')


    <md-button ng-href="faq/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-tools.faq.add_faq') }}</span>
    </md-button>


    <md-button ng-href="faq-category" class="btn-float" layout="column">
        <i class="far fa-folders"></i>
        <span >{{ __('admin-tools.faq.categories') }}</span>
    </md-button>
@endsection


@section('breadcrumbs', Breadcrumbs::render('admin.tools.faq'))

@section('no-block-content')

    <div class="x_panel">

        <div class="x_title x_title_filter">
            <a class="md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-tools.faq.filters_label') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-tools.faq.categories') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('categories[]', $categories, is_filter_search($filter, 'categories'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple', 'data-live-search' => 'true']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-tools.faq.status') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('status[]', $statusses, is_filter_search($filter, 'status'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple', 'data-live-search' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-tools.faq.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-tools.faq.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-tools.faq.title') }}</h2>
            <div class="clearfix"></div>
        </div>


        <div class="x_content">
            <form method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-tools.faq.name') }}</th>
                        <th>{{ __('admin-tools.faq.category') }}</th>
                        <th>{{ __('admin-tools.faq.status') }}</th>
                        <th>{{ __('admin-tools.faq.published_at') }}</th>
                        <th>Посл. обновление</th>
                        <th>{{ __('admin-tools.faq.action') }}</th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($faq) > 0)
                        @foreach($faq as $item)
                            <tr>
                                <th>
                                    @if(empty($item->title))
                                        <a href="links_reviews/{{ $item->id }}/edit">
                                            <small class="text-danger">
                                                <i class="far fa-exclamation-circle"></i>
                                                Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                            </small>
                                        </a>
                                    @else
                                        <a href="{{ admin_base_path('/tools/faq/'.$item->id.'/edit') }}">
                                            {{ $item->title }} &nbsp;<i class="fal fa-pencil"></i>
                                        </a>
                                    @endif


                                </th>
                                <td>{{ $item->faq_category['name']  }}</td>
                                <td>
                                    <input type="hidden" name="item_id[]" value="{{$item->id}}">

                                    <div class="material-switch">
                                        <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="1" @if($item->status == 1) checked @endif />
                                        <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                </td>

                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                                </td>

                                <td style="width: 100px">
                                    <md-button class="md-icon-button" ng-href="{{ admin_base_path('/tools/faq/'.$item->id.'/delete')  }}">
                                        <i class="fas fa-trash text-danger"></i>
                                    </md-button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                        </tr>
                    @endif
                    </tbody>

                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('admin-tools.faq.save') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.faq.run') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {{ $faq->links() }}
    </div>
@endsection
