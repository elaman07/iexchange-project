@extends('admin.layouts.app')

@section('title', __('admin-tools.faq.add_faq'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.faq.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/faq') }}">
        @csrf
        <div class="default-panel-body">

            <label class="col-sm-2 control-label">Новая запись</label>
            <div class="col-sm-10">
                <div class="form-group col-md-6 multi-language-form-group">
                    <div class="control-label-br">
                        <div layout="row">
                            <div>{{ __('admin-tools.faq-category.name') }}</div>
                            <span flex></span>
                            <ul class="nav nav-pills lang-tabs">
                                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                    <li @if($form_lang['active'] == true) class="active" @endif>
                                        <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                            {{ $form_lang_key }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                        <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                            <input type="text" name="title{{$form_lang['field']}}" class="form-control" id="title{{$form_lang['field']}}" placeholder="{{ __('admin-tools.faq-category.name_hint') }}">
                        </div>
                    @endforeach
                </div>

                <div class="clearfix"></div>

                <div class="form-group col-md-6">
                    <div class="control-label-br">{{ __('admin-tools.faq.category') }}</div>
                    <select class="form-control selectpicker" name="id_group">
                        @foreach($category as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="clearfix"></div>
                <hr />

                <div class="form-group col-md-12 multi-language-form-group">
                    <div class="control-label-br">
                        <div layout="row">
                            <div>{{ __('admin-tools.faq.description') }}</div>
                            <span flex></span>
                            <ul class="nav nav-pills lang-tabs">
                                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                    <li @if($form_lang['active'] == true) class="active" @endif>
                                        <a class="lang-tabs-link" data-toggle="tab" href="#desc_exchange-{{ $form_lang_key }}">
                                            {{ $form_lang_key }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                        <div id="desc_exchange-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                            <textarea class="form-control" id="{{ $form_lang['text'] }}" name="text{{$form_lang['field']}}" rows="5"></textarea>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>


        <div class="clearfix"></div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.faq.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/faq') }}">{{ __('admin-tools.faq.back') }}</md-button>
        </div>
    </form>
@endsection
