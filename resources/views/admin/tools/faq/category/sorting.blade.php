@extends('admin.layouts.app')

@section('title', __('admin-tools.faq-category.sorting_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.faq.category.sorting'))

@section('custom-js-head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 50%; margin: 0 auto;text-align: center; }
        #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em;  height: 4rem;    text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        #sortable li span { position: absolute; margin-left: -1.3em; }
        .ui-state-highlight { min-height: 4rem; height: auto; line-height: 1.2em; }
    </style>
@endsection


@section('content')
    <ul id="sortable">
        @foreach($categories as $key => $value)
            <li class="ui-state-default" id="item-{{$value->id}}" data-position="{{$key}}">{{$value->name}}</li>
        @endforeach
    </ul>
@endsection

@section('js_bottom')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#sortable" ).sortable();
            $( "#sortable" ).disableSelection();
        } );
    </script>

    <script>
        $( function()
        {
            var ul_sortable = $('#sortable');
            ul_sortable.sortable({
                cursor: 'move',
                placeholder: "ui-state-highlight",
                update: function()
                {
                    var sortable_data = ul_sortable.sortable('serialize');
                    $.ajax({
                        data: sortable_data,
                        type: 'POST',
                        url: '/admin/private/sorting/faq_category',
                        success: function (result) {
                            console.log(result);
                        }
                    });
                }
            });
            ul_sortable.disableSelection();
        } );
    </script>
@endsection