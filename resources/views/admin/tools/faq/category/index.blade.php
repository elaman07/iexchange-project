@extends('admin.layouts.app')

@section('title', __('admin-tools.faq-category.title'))

@section('top-block')
    <md-button ng-href="{{ admin_base_path('/tools/faq-category/create') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-tools.faq-category.add') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.faq.category'))

@section('content')
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-tools.faq-category.name') }}</th>
                <th>{{ __('admin-tools.faq-category.count_item') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($categories) > 0)
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td>
                            {{ $category->faq->count() }}
                        </td>
                        <td style="width: 200px;">
                            <div class="col-md-6">
                                <a href="faq-category/{{$category->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-tools.faq-category.edit_button') }}</a>
                            </div>

                            <div class="col-md-6">
                                {!! Form::open(['method' => 'DELETE', 'route' => ['faq-category.destroy', $category->id] ]) !!}
                                {!! Form::submit(__('admin-tools.faq-category.delete_button'), ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
@endsection