@extends('admin.layouts.app')

@section('title', __('admin-tools.faq-category.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.faq.category.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/tools/faq-category/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Обновить группу</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.faq-category.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}" value="{{ $item->getTranslation('name', $form_lang_key) }}" placeholder="{{ __('admin-tools.faq-category.name_hint') }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.faq.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/tools/faq-category') }}">{{ __('admin-tools.faq.back') }}</md-button>
        </div>
    </form>

@endsection
