@extends('admin.layouts.app')

@section('title', 'API')


@section('top-block')
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.api'))

@section('content')

    <style>
        .user-api__wrap {

        }

        .user-api__list {
            background: #fbfbfb;
            border-radius: 5px;
            margin-bottom: 10px;
        }

        .user-api__list-item {
            border-bottom: 1px solid #e5e5e5;
            padding: 13px 15px;
        }

        .user-api__list-item-name {
            font-size: 15px;
        }

        .user-api__list-item-name span {
            background: #efefef;
            padding: 10px;
            border-radius: 5px;
            margin-left: 8px;
            font-weight: 500;
        }

        .user-api__list-item:last-child {
            border-bottom: none;
        }

    </style>

    @csrf

    <form style="width:100%;margin-right: 10px;" method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Пользователь') }}</th>
                <th>{{ __('Ключи и Доступы') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($users) > 0)
                @foreach($users as $user)
                    <tr>
                        <td>
                            <input type="hidden" name="item_id[{{ $user->id }}]" value="{{ $user->id }}">
                            <div>
                                <a href="{{ admin_base_path('/account/users?action=filter&id='.$user->id.'&sorting=id') }}"><b>{{ $user->name }}</b></a>
                            </div>
                            <small>{{ $user->email }}</small>
                        </td>
                        <td>
                            @foreach($user->tokens as $token)
                                <div class="user-api__list">
                                    <div class="user-api__list-item">
                                        <div class="user-api__list-item-name">
                                            API Ключ:  <span>{{ $token->id }}|{{ $token->token_code }}</span>
                                        </div>
                                    </div>
                                    <div class="user-api__list-item">
                                        <b>Разрешения:</b>

                                        {{ Form::select('abilities['.$user->id.']['.$token->id.'][]', $abilities, $token->abilities,
                                        ['multiple', 'class' => 'selectpicker', 'data-width'=>'300', 'data-live-search' => 'true']) }}
                                    </div>
                                    <div class="user-api__list-item">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{ $token->id}}" name="check[{{$user->id}}][{{ $token->id}}]" id="checkbox{{ $token->id }}" autocomplete="off">
                                            <label class="form-check-label" for="checkbox{{  $token->id }}">
                                                Выбрать
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('Сохранить') }}</option>
                        <option value="trashed">{{ __('Удалить') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>


    <div class="pagination-right">
        {!! $users->appends($filter)->links() !!}
    </div>
@endsection
