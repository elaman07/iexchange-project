@extends('admin.layouts.app')

@section('title', __('Работа парсеров'))

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser'))

@section('video__instruction', 'https://www.youtube.com/embed/wV2mB9ILgJE?autoplay=1&amp;mute=0')
@section('top-block')
    {{--    <md-button data-toggle="modal" data-target="#template" class="btn-float" layout="column">--}}
    {{--        <i class="fad fa-upload text-muted"></i>--}}
    {{--        <span class="text-muted">Готовые данные</span>--}}
    {{--    </md-button>--}}

    <md-button ng-href="parser/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить пару') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#cron" class="btn-float" layout="column">
            <i class="far fa-sync-alt"></i>
            <span>{{ __('Настройка CRON') }}</span>
        </md-button>
    @endcan


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <i class="far fa-cog"></i>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan

    <style>

        .parser-group-item {
            padding: 30px;
            height: 270px;
            position: relative;
            box-shadow: 0 1px 20px 0 rgb(69 90 100 / 8%);
            background: #fff;
            margin-right: 10px;
            margin-bottom: 10px;
            border-radius: 10px;
        }
        .parser-group-title {
            font-size: 21px;
            font-weight: 300;
        }

        .parser-group-image {
            display: block;
            width: auto;
            height: 50px;
            padding-right: 10px;
        }

        .parser-group-image img {
            display: block;
            width: auto;
            height: 100%;
        }

        .parser-group-item-row {
            padding-bottom: 30px;
        }

        .parser-group-item-list {
            margin-bottom: 15px;
            list-style-type: none;
            padding: 0;
            color: #adadad;
        }

        .parser-group-item-list li {
            padding-top: 10px;
            padding-bottom: 10px;
            border-bottom: 1px solid #e9e9e9;
        }

        .parser-group-item-list li:last-child {
            border: none;
        }

        .parser-group-item-button-wrap {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
        .parser-group-item-button.md-primary {
            width: 100%;
            padding: 8px 0;
            background: #f7f7f7;
            color: #6c6c6c;
            font-size: 14px;
            margin: 0 !important;
            /* box-shadow: 0 0 #0003, 0 0 #00000024, 0 0 #0000001f; */
            /* border: 1px solid #0000001f; */
            border-radius: 0px 0 10px 10px;
        }


        .parser-group-count {
            text-align: right;
            float: right;
            font-size: 18px;
            color: #21b921;
        }

        .parser-group-count-red {
            color: #eb1f1f;
        }

        .parser-group-item .parser-group-item-header {
            opacity: 0;
            -webkit-transition: opacity 0.2s ease-in-out;
            -moz-transition: opacity 0.2s ease-in-out;
            -ms-transition: opacity 0.2s ease-in-out;
            -o-transition: opacity 0.2s ease-in-out;
            transition: opacity 0.2s ease-in-out;
            z-index: 999999999;
            position: absolute;
            right: 20px;
            top: 20px;
            background: #000000;
            color: #fff;
            padding: 5px 10px;
            border-radius: 5px;
            font-size: 12px;
        }

        .parser-group-item:hover .parser-group-item-header {
            opacity: 1;
        }

        .parser-group-item:hover .parser-group-text {
            opacity: 0;
        }


        .parser-group-item-disabled .parser-group-item-row, .parser-group-item-disabled .parser-group-item-list, .parser-group-item-disabled  .parser-group-text {
            opacity: .4;
        }


        .parser-h2 {
            font-family: Nunito,sans-serif;
            padding: 0;
            margin: 0;
            font-weight: 500;
            font-size: 2.5rem;
            padding-top: 20px;
            padding-bottom: 30px;
        }

        .parser-content-body {
        }


        .parser-group-item-green {
            background: #21b921 !important;
        }

        .parser-group-item-red {
            background: #eb1f1f !important;
        }

        .parser-group-time {
            color: #646464;
            font-size: 13px;
            float: right;
        }

        .parser-group-time-none {
            color: #cdcdcd;
        }


        .parser-group-text {
            position: absolute;
            top: 0;
            right: 0;
            color: #fff;
            padding: 5px 12px;
            font-size: 13px;
            border-radius: 0 10px 0 10px;

            -webkit-transition: opacity 0.3s ease-in-out;
            -moz-transition: opacity 0.3s ease-in-out;
            -ms-transition: opacity 0.3s ease-in-out;
            -o-transition: opacity 0.3s ease-in-out;
            transition: opacity 0.3s ease-in-out;
        }
        .parser-group-text-green {
            background: #5cb85c;
        }

        .parser-group-text-red {
            background: #720f07;
        }


    </style>

@endsection

@section('no-block-content')
    @if($group_parser_v2_count > 0)
        <h2 class="parser-h2">{{ __('Активные парсеры') }} {{ count($group_parser_v2_on) }}</h2>

        <div class="parser-content-body row">
            @foreach($group_parser_v2_on as $value)
                <div class="col-lg-4">
                    <div class="parser-group-item {{ $value->status == 0 ? 'parser-group-item-disabled' : '' }}" layout="column">
                        <div class="parser-group-item-row" layout="row" layout-align="start center">
                            <div class="parser-group-image">
                                <img class="img-circle" src="{{ url('/images/parsers/'.\Str::lower($value->alias).'.png') }}"/>
                            </div>
                            <div class="parser-group-title">{{ $value->name }}</div>
                        </div>

                        <ul class="parser-group-item-list">
                            <li>
                                <div class="parser-group-active_rates">{{ __('Доступных курсов') }}: <span class="parser-group-count {{ $value->parserExchange->count() == 0 ? 'parser-group-count-red' : ''}}">{{ iex_number_format($value->parserExchange->count(), 0, true) }}</span></div>
                            </li>
                            <li>
                                <div class="parser-group-active_rates">{{ __('Посл. обновление') }}: <span class="@if(is_null($value->last_updated_at)) parser-group-time-none @endif parser-group-time">{{ !is_null($value->last_updated_at) ? $value->last_updated_at->diffForHumans() : 'Не обновлен'}}</span></div>
                            </li>
                        </ul>

                        @if($value->status == 1)
                            <a href="?actions=disabled&id={{ $value->id }}" class="parser-group-item-header  parser-group-item-red">{{ __('Отключить') }}</a>
                        @else
                            <a href="?actions=enabled&id={{ $value->id }}" class="parser-group-item-header parser-group-item-green">{{ __('Включить') }}</a>
                        @endif

                        <div>
                            @if($value->status == 1)
                                <div class="parser-group-text parser-group-text-green">{{ __('Активен') }}</div>
                            @else
                                <div class="parser-group-text parser-group-text-red">{{ __('Отключен') }}</div>
                            @endif
                        </div>

                        <div class="parser-group-item-button-wrap">
                            <a href="{{ admin_base_path('/crypto/parser/'.$value->id) }}" class="md-button  md-primary parser-group-item-button">{{ __('Список курсов') }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <h2 class="parser-h2">{{ __('Отключенные парсеры') }} {{ count($group_parser_v2_off) }}</h2>

        <div class="parser-content-body row">
            @foreach($group_parser_v2_off as $value)
                <div class="col-lg-4">
                    <div class="parser-group-item {{ $value->status == 0 ? 'parser-group-item-disabled' : '' }}" layout="column">
                        <div class="parser-group-item-row" layout="row" layout-align="start center">
                            <div class="parser-group-image">
                                <img class="img-circle" src="{{ url('/images/parsers/'.\Str::lower($value->alias).'.png') }}"/>
                            </div>
                            <div class="parser-group-title">{{ $value->name }}</div>
                        </div>

                        <ul class="parser-group-item-list">
                            <li>
                                <div class="parser-group-active_rates">{{ __('Доступных курсов') }}: <span class="parser-group-count {{ $value->parserExchange->count() == 0 ? 'parser-group-count-red' : ''}}">{{ iex_number_format($value->parserExchange->count(), 0, true) }}</span></div>
                            </li>
                            <li>
                                <div class="parser-group-active_rates">{{ __('Посл. обновление') }}: <span class="@if(is_null($value->last_updated_at)) parser-group-time-none @endif parser-group-time">{{ !is_null($value->last_updated_at) ? $value->last_updated_at->diffForHumans() : 'Не обновлен'}}</span></div>
                            </li>
                        </ul>

                        @if($value->status == 1)
                            <a href="?actions=disabled&id={{ $value->id }}" class="parser-group-item-header  parser-group-item-red">{{ __('Отключить') }}</a>
                        @else
                            <a href="?actions=enabled&id={{ $value->id }}" class="parser-group-item-header parser-group-item-green">{{ __('Включить') }}</a>
                        @endif


                        <div>
                            @if($value->status == 1)
                                <div class="parser-group-text parser-group-text-green">{{ __('Активен') }}</div>
                            @else
                                <div class="parser-group-text parser-group-text-red">{{ __('Отключен') }}</div>
                            @endif
                        </div>

                        <div class="parser-group-item-button-wrap">
                            <a href="{{ admin_base_path('/crypto/parser/'.$value->id) }}" class="md-button  md-primary parser-group-item-button">{{ __('Список курсов') }}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="alert alert-danger text-center">{{ __('Нет данных') }}</div>
    @endif

    <!-- Модальное окно группы курсов -->
    <div class="modal fade" id="group_courses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="courses_group">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки источников курсов</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="profit" class="col-sm-2 control-label">Источники курсов</label>
                            <div class="col-sm-10">

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки работы парсера</span>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Макс. кол-во знаков после запятой в подсчетах для курсов обмена валют</div>
                                    <input type="text" class="form-control" name="max_number_format_parser" value="{{ iEXSetting('max_number_format_parser', 10) }}">
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Записывать историю курсов</div>
                                    <select class="form-control selectpicker" name="record_course_history">
                                        <option value="0"  @if(iEXSetting('record_course_history') == 0) selected @endif>Нет</option>
                                        <option value="1"  @if(iEXSetting('record_course_history') == 1) selected @endif>Да</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Записывать историю запросов</div>
                                    <select class="form-control selectpicker" name="is_record_parser_http_log">
                                        <option value="0"  @if(iEXSetting('is_record_parser_http_log') == 0) selected @endif>Нет</option>
                                        <option value="1"  @if(iEXSetting('is_record_parser_http_log') == 1) selected @endif>Да</option>
                                    </select>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Сортировка источники</div>
                                    <select class="form-control selectpicker" name="crypto_parser_sorting">
                                        <option value="0"  @if(iEXSetting('crypto_parser_sorting') == 0) selected @endif>По умолчанию</option>
                                        <option value="1"  @if(iEXSetting('crypto_parser_sorting') == 1) selected @endif>По количеству пар</option>
                                    </select>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Режим тестирования</div>
                                    <select class="form-control selectpicker" name="is_crypto_parser_test">
                                        <option value="0"  @if(iEXSetting('is_crypto_parser_test') == 0) selected @endif>Отключено</option>
                                        <option value="1"  @if(iEXSetting('is_crypto_parser_test') == 1) selected @endif>Включить</option>
                                    </select>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Разрешить парсинг курсов по API</div>
                                    <select class="form-control selectpicker" name="is_crypto_partner_parser_api">
                                        <option value="0"  @if(iEXSetting('is_crypto_partner_parser_api') == 0) selected @endif>Нет</option>
                                        <option value="1"  @if(iEXSetting('is_crypto_partner_parser_api') == 1) selected @endif>Да</option>
                                    </select>
                                    <p class="input-p-text">После включения данной функции, у пользователей iEXExchanger будет возможность парсить ваши курсы</p>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек CRON -->
    <div class="modal fade" id="cron" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings_cron">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройка обновления курсов</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{{ __('Основное') }}</label>
                            <div class="col-sm-9">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">Таймер обновления курсов</div>
                                    <select class="form-control selectpicker" name="cron_format_update_rates">
                                        <option value="0" @if(iEXSetting('cron_format_update_rates') == 0) selected @endif>Стандартное</option>
                                        <option value="1" @if(iEXSetting('cron_format_update_rates') == 1) selected @endif>Ускоренное</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="text-warning">
                                    При установке таймера "Ускоренное", убедитесь что у вас курсы обновляются меньше 10/15 секунд
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{{ __('Стандартное') }}</label>
                            <div class="col-sm-9">
                                <div class="form-group col-md-8">
                                    <div class="control-label-br">Выберите время обновления курсов</div>
                                    <select class="form-control selectpicker" name="cron_interval_minutes_update_rates">
                                        <option value="0" @if(iEXSetting('cron_interval_minutes_update_rates') == 0) selected @endif>Каждую минуту (рекомендуется)</option>
                                        <option value="1" @if(iEXSetting('cron_interval_minutes_update_rates') == 1) selected @endif>Каждые 2 минуты</option>
                                        <option value="2" @if(iEXSetting('cron_interval_minutes_update_rates') == 2) selected @endif>Каждые 3 минуты</option>
                                        <option value="3" @if(iEXSetting('cron_interval_minutes_update_rates') == 3) selected @endif>Каждые 5 минут</option>
                                    </select>
                                </div>

                                <div class="clearfix"></div>
                                <hr />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{{ __('Ускоренное') }}</label>
                            <div class="col-sm-9">

                                <div class="form-group col-md-8">
                                    <div class="control-label-br">Обновление курсов (в секундах)</div>
                                    <input type="text" class="form-control" name="cron_interval_second_update_rates" value="{{ iEXSetting('cron_interval_second_update_rates', 0) }}">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
