@extends('admin.layouts.app')

@section('title','Курсы источника '.$group_parser->name)

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser.show', $group_parser))


@section('top-block')
    <md-button ng-href="create?id_group={{ $group_parser->id }}" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">Добавить пару</span>
    </md-button>

@endsection
@section('no-block-content')

    <style>
        .crypto-parser-show__import {
            text-align: center;
        }

        .crypto-parser-show__item {

        }
        .crypto-parser-show__item-image img{
            width: auto;
            display: block;
            height: 150px;
            text-align: center;
            margin: 0 auto;
        }

        .crypto-parser-show__item-headline {
            border-bottom: 1px solid #e9e9e9;
        }
        .crypto-parser-show__item-headline .crypto-parser-show__item-title {
            font-size: 17px;
            text-align: center;
            padding: 15px 0;
        }


        .crypto-parser-show__item-body {
            padding: 10px 20px;
        }

        .crypto-parser-show__item-ul {
            margin-bottom: 15px;
            list-style-type: none;
            padding: 0;
            color: #adadad;
        }

        .crypto-parser-show__item-ul li {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .crypto-parser-show__item-ul li:last-child {
            border: none;
        }

        .parser-group-count {
            text-align: right;
            float: right;
            font-size: 18px;
            color: #21b921;
        }

        .parser-group-count-red {
            color: #eb1f1f;
        }

        .parser-group-time {
            color: #646464;
            font-size: 13px;
            float: right;
        }

        .parser-group-time-none {
            color: #cdcdcd;
        }

        .crypto-parser-show__item-linear {
            background: #e9e9e9;
            height: 1px;
            width: 100%;
        }

        .crypto-parser-show__item-status1 {
            color: rgba(239,68,68,1);
        }

        .crypto-parser-show__item-status2 {
            color: rgba(34,197,94,1);
        }

        .crypto-parser-show__import-label {
            padding: 10px;
            color: #a7a7a7;
        }

    </style>

    <div layout="row" layout-padding class="padding-lr-0">
        <div flex="85" class="padding-l-0">
            <div class="x_panel">
                <div class="x_title x_title_filter">
                    <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                        <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                        <div flex=""></div>
                        <div class="heading-right-button">
                            <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                            <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                        </div>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
                    <form class="form-horizontal" method="get" action="?">
                        <input type="hidden" name="send_action" value="on">

                        <div class="row task-filter-body">
                            <div class="padding-20">
                                <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                                    <div flex="50">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Название пары</label>
                                            <div class="col-md-6">
                                                {{ Form::input('text', 'name', is_filter_search($filter, 'name'), ['class' => 'form-control']) }}
                                                <div class="form-check">
                                                    <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_name" @if(isset($filter['checkbox_name'])) checked @endif>
                                                    <label class="form-check-label" for="checkbox1">Точное совпадение пары</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div flex="50">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-primary">{{ __('Применить фильтры') }}</md-button>
                            <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="pagination-right">
                {!! $parser_exchange->appends($filter)->links() !!}
            </div>

            <div class="x_panel new-x_panel_wrapper">
                <div class="x_title" layout="row">
                    <h2>Список парсеров</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <form method="post" action="{{ admin_base_path('/crypto/parser?id_group='.$group_parser->id.'&form=update') }}">
                        @csrf
                        <table class="manager-table-2 table table-border-2">
                            <thead>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input class="form-check-input checkAll" id="checkbox-{{  \Str::slug($group_parser->name) }}" type="checkbox">
                                        <label class="form-check-label" for="checkbox-{{  \Str::slug($group_parser->name) }}"></label>
                                    </div>
                                </th>
                                <th style="width: 30%">Пара</th>
                                <th style="width: 20%">Курс</th>
                                <th>Тип</th>
                                <th>Привязанные направления</th>
                                <th>Посл. обновление</th>
                                <th>Статус</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($parser_exchange__count > 0)
                                @foreach($parser_exchange as $item)
                                    <tr>
                                        <th>
                                            <div class="form-check">
                                                <input class="form-check-input" id="checkbox{{$item->id}}" value="{{$item->id}}" type="checkbox" name="check[]">
                                                <label class="form-check-label" for="checkbox{{$item->id}}"></label>
                                            </div>
                                        </th>
                                        <th>
                                            <a href="{{ admin_base_path('/crypto/parser/'.$item->id.'/edit') }}">
                                                {{ $item->name }}  &nbsp;<i class="fal fa-pencil"></i>
                                            </a>

                                            <br />

                                            <div class="parser-code__item">
                                                {{ __('Код пары') }}:
                                                <span class="iex-item__copied" ngclipboard data-clipboard-text="{{ $item->code }}" ngclipboard-success="onEventClipboardSuccess('Code');">
                                                    {{ $item->code }}&nbsp;&nbsp;<i class="fad fa-copy"></i>
                                                </span>
                                            </div>
                                        </th>
                                        <td>

                                            @if($item->summa == null)
                                                <span class="text-danger">Не указан</span>
                                            @else
                                                {{  $item->value_default }}  →	 {{  $item->summa_default }}
                                                @if(!empty($item->type_price))
                                                    <br />
                                                <small class="text-muted">Тип: {{ $item->type_price }}</small>
                                                    @endif
                                            @endif

                                        </td>
                                        <td>
                                            <select name="type[{{$item->id}}]" class="selectpicker form-control">
                                                <option value="0" @if($item->type == 0) selected @endif>Серверный</option>
                                                <option value="1" @if($item->type == 1) selected @endif>Внутренний</option>
                                            </select>
                                        </td>
                                        <td>
                                            @if($item->direction_exchange->count() > 0)
                                                <ul class="list-unstyled"  height="100px" slim-scroll>
                                                    @foreach($item->direction_exchange as $value)
                                                        <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ $value->tech_name }}</a></li>
                                                    @endforeach
                                                </ul>
                                            @else
                                                <small class="text-danger">Не найдено</small>
                                            @endif
                                        </td>

                                        <td>
                                            <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                                            @if($item->is_not_update == 1)
                                                <div class="text-danger" style="font-size: 10px;margin-top:3px;">
                                                    <i class="far fa-exclamation-circle"></i>
                                                    Курс не обновлен
                                                </div>
                                            @endif
                                        </td>

                                        <td>
                                            <div class="material-switch">
                                                <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="1" @if($item->status == 1) checked @endif />
                                                <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                                            </div>
                                        </td>

                                        <td style="width: 50px">
                                            <input type="hidden" name="item_id[{{ $item->id }}]">

                                            <ul class="breadcrumb-elements" hide-xs layout="row" layout-align="center center">

                                                <li class="dropdown">
                                                    <md-button href="#" class="md-icon-button" data-toggle="dropdown">
                                                        <i class="far fa-ellipsis-v font-size-16"></i>
                                                    </md-button>
                                                    <ul class="dropdown-menu dropdown-menu-left">
                                                        <li><a ng-href="{{ admin_base_path('/crypto/parser/history/'.$item->id) }}">{{ __('История курсов') }}</a></li>
                                                        <li class="divider"></li>
                                                        <li><a class="text-danger" ng-href="{{ admin_base_path('/crypto/parser/delete/'.$item->id) }}">{{ __('Удалить') }}</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="9"><p align="center"><br />Список пуст</p></td>
                                </tr>
                            @endif
                            </tbody>

                        </table>

                        <div class="clearfix"></div>
                        <div class="default-panel-footer">
                            <div class="pull-right">

                                <div style="display: inline-block">
                                    <select name="actions" class="form-control selectpicker" data-width="200px">
                                        <option value="save">Сохранить</option>
                                        <option data-divider="true"></option>
                                        <option value="activation">Активировать</option>
                                        <option value="deactivation">Деактивировать</option>
                                        <option data-divider="true"></option>
                                        <option value="delete">Удалить</option>
                                    </select>
                                </div>
                                <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="pagination-right">
                {!! $parser_exchange->appends($filter)->links() !!}
            </div>
        </div>

        <div  class="padding-r-0" flex="15">
            <div class="x_panel new-x_panel_wrapper">
                <div class="x_title">
                    <h2>Информация источника</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div>
                        <div class="crypto-parser-show__item-image">
                            <img class="img-circle" src="{{ url('/images/parsers/'.\Str::lower($group_parser->alias).'.png') }}"/>
                        </div>
                        <div class="crypto-parser-show__item-headline">
                            <div class="crypto-parser-show__item-title">{{ $group_parser->name }}</div>
                        </div>

                        <div class="crypto-parser-show__item-body">
                            <ul class="crypto-parser-show__item-ul">
                                <li>
                                    <div class="parser-group-active_rates">Доступных курсов: <span class="parser-group-count {{ $parser_exchange__count == 0 ? 'parser-group-count-red' : ''}}">{{ $parser_exchange__count }}</span></div>
                                </li>
                                <li>
                                    <div class="parser-group-active_rates">Посл. обновление: <span class="@if(is_null($group_parser->last_updated_at)) parser-group-time-none @endif parser-group-time">{{ !is_null($group_parser->last_updated_at) ? $group_parser->last_updated_at->diffForHumans() : 'Не обновлен'}}</span></div>
                                </li>
                            </ul>
                        </div>
                        <div class="crypto-parser-show__item-linear"></div>
                        <div class="crypto-parser-show__item-body">
                            <ul class="crypto-parser-show__item-ul">
                                <li>
                                    <div class="crypto-parser-show__item-body-status">
                                        Статус:
                                        <span class="float-right">
                                             @if($group_parser->status == 1)
                                                <a href="?actions=disabled&id={{ $group_parser->id }}" class="crypto-parser-show__item-status1">Отключить</a>
                                            @else
                                                <a href="?actions=enabled&id={{ $group_parser->id }}" class="crypto-parser-show__item-status2">Включить</a>
                                            @endif
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="crypto-parser-show__import">
                @if(file_exists( storage_path('templates/rates/'. $group_parser->alias.'.json')))
                    <a href="?actions=import&id={{ $group_parser->id }}" class="md-primary md-raised md-button">Загрузить все курсы</a>
                @endif
                <a href="?actions=import_trashed&id={{ $group_parser->id }}" class="md-danger md-raised md-button">Очистить курсы</a>
            </div>
        </div>
    </div>
@endsection
