@extends('admin.layouts.app')

@section('title','Добавить новый курс')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser.add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/parser') }}">
        @csrf
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новая пара</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Источник</div>
                        <select class="form-control selectpicker" name="id_group" data-live-search="true">
                            @foreach($group_parser as $parser)
                                <option value="{{ $parser->id }}" @if($parser->id == old('id_group', $id_group)) selected @endif >{{ $parser->name }} ({{ $parser->parserExchange->count() }})</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Код Отдаю</div>
                        <input type="text" name="code_in" class="form-control" id="name" value="{{ old('code_in') }}">
                        <div class="input-p-text">Пример: <b>BTC</b></div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Код Получаю</div>
                        <input type="text" name="code_out" class="form-control" id="name" value="{{ old('code_out') }}">
                        <div class="input-p-text">Пример: <b>USD</b></div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Разрешить добавить обратную парю</div>
                        <div class="material-switch switch-input-1">
                            <input id="is_allow_to_backspace" name="is_allow_to_backspace" type="checkbox" value="1" />
                            <label for="is_allow_to_backspace" class="label-primary"></label>
                        </div>
                        <div class="input-p-text">Пример: Если вы добавляете пару BTC - USD, то после включения данной функции автоматически будет добавлено и USD - BTC</div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаки после, запятой</div>
                        <input type="number" name="number_format" class="form-control" id="number_format" value="{{ old('number_format', 10) }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Тип парсера</div>

                        <select name="type" class="form-control selectpicker" data-width="100%">
                            <option value="0" @if(old('type') == 0) selected @endif>Серверный</option>
                            <option value="1" @if(old('type') == 1) selected @endif>Внутренний</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>

                        <select name="status" class="form-control selectpicker" data-width="100%">
                            <option value="0" @if(old('status') == 0) selected @endif>Не активен</option>
                            <option value="1" @if(old('status') == 1) selected @endif>Активен</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/parser') }}">Назад</md-button>
        </div>
    </form>

@endsection
