@extends('admin.layouts.app')

@section('title', 'Все курсы из источников')


@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="far fa-filter"></i>&nbsp;&nbsp;&nbsp;{{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="font-size-16 far fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="font-size-16 far fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Пара') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::input('text', 'name', is_filter_search($filter, 'name'), ['class' => 'form-control']) !!}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_name" @if(isset($filter['checkbox_name'])) checked @endif>
                                            <label class="form-check-label" for="checkbox1">{{ __('Точное совпадение') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Источник') }}</label>
                                    <div class="col-md-8">
                                        <select class="form-control selectpicker" name="id_group" data-live-search="true" data-size="8">
                                            @foreach($groups as $group)
                                                <option @if(is_filter_search($filter, 'id_group') == $group->id) selected @endif value="{{ $group->id }}" data-subtext="{{ $group->status == 1 ? 'Активен' : 'Не активен'}}">{{ $group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-basic.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-basic.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form style="width:100%;margin-right: 10px;" method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th class="not-sortable">
                            <div class="form-check-v2">
                                <input id="checkbox-0" type="checkbox" class="form-check-input checkAll">
                                <label class="form-check-label" for="checkbox-0"></label>
                            </div>
                        </th>
                        <th>Пара</th>
                        <th>Код пары</th>
                        <th>Источник</th>
                        <th>Курс</th>
                        <th>Привязанные направления</th>
                        <th>Посл. обновление</th>
                        <th class="not-sortable"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($rates) > 0)
                        @foreach($rates as $item)
                            <tr>
                                <th>
                                    <div class="form-check-v2">
                                        <input class="form-check-input" id="checkbox{{ $item->id }}"
                                               value="{{ $item->id }}" type="checkbox" name="bestchange_checkbox[{{ $item->id }}]">
                                        <label class="form-check-label" for="checkbox{{$item->id}}"></label>
                                    </div>
                                </th>
                                <th>
                                    <a href="{{ admin_base_path('/crypto/parser/'.$item->id.'/edit') }}">
                                        {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                                    </a>

                                    @if($item->is_not_pair == 1)
                                        <div>
                                            <small class="text-danger font-weight-bold">
                                                <a href="{{ admin_base_path('/crypto/bestchange/error_log/'.$item->id) }}" class="text-danger"  data-toggle="tooltip" title="В Bestchange отсутствуют курсы по направлению {{$item->name}}">
                                                    <i class="far fa-exclamation-circle text-danger"></i>
                                                    ошибка обновлении курсов
                                                </a>
                                            </small>
                                        </div>
                                    @endif
                                </th>
                                <td>
                                    <div class="parser-code__item">
                                        <span class="iex-item__copied" ngclipboard data-clipboard-text="{{ $item->code }}" ngclipboard-success="onEventClipboardSuccess('Code');">
                                                    {{ $item->code }}&nbsp;&nbsp;<i class="fad fa-copy"></i>
                                                </span>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ admin_base_path('/crypto/parser/'.$item->id_group) }}">{{ $item->group_parse_exchange->name }}</a>
                                </td>
                                <td>
                                    @if($item->status == 1)
                                        @if($item->summa == null)
                                            <span class="text-danger">Не указан</span>
                                        @elseif($item->summa == 0)
                                            <small class="text-danger">
                                                <i class="far fa-exclamation-triangle"></i>
                                                Требуется обновление курсов
                                            </small>
                                        @else
                                            <div>{{$item->value}} → {{$item->summa}}</div>
                                        @endif

                                        @if(!empty($item->source_name))
                                            <div style="font-size: 11px" class="text-muted">Источник: <b>{{ $item->source_name }}</b></div>
                                        @endif
                                    @else
                                        <span class="text-danger">Парсер отключен</span>
                                    @endif
                                </td>
                                <td style="width: 300px">
                                    @if(count($item->direction_exchange) > 0)
                                        <ul class="list-unstyled">
                                            @foreach($item->direction_exchange as $value)
                                                <li>
                                                    - <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ $value->tech_name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <small class="text-danger">Не найдено</small>
                                    @endif
                                </td>
                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                                </td>
                                <td style="width: 50px">

                                    <ul class="breadcrumb-elements" hide-xs layout="row" layout-align="center center">

                                        <li class="dropdown">
                                            <md-button href="#" class="md-icon-button" data-toggle="dropdown">
                                                <i class="far fa-ellipsis-v font-size-16"></i>
                                            </md-button>
                                            <ul class="dropdown-menu dropdown-menu-left">
                                                <li><a ng-href="{{ admin_base_path('/crypto/parser/history/'.$item->id) }}">{{ __('История курсов') }}</a></li>
                                                <li class="divider"></li>
                                                <li><a class="text-danger" ng-href="{{ admin_base_path('/crypto/parser/delete/'.$item->id) }}">{{ __('Удалить') }}</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                        </tr>
                    @endif
                    </tbody>

                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">

                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="0">Действия</option>
                                <option value="activation">Активировать</option>
                                <option value="deactivation">Деактивировать</option>
                                <option value="delete">Удалить</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $rates->links() !!}
    </div>
@endsection

