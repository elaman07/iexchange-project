@extends('admin.layouts.app')

@section('title','Изменить пару '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser.change', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/parser/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новая пара</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Источник</div>
                        <select class="form-control selectpicker" name="id_group" data-live-search="true">
                            @foreach($group_parser as $parser)
                                <option value="{{ $parser->id }}" @if($parser->id == $item->id_group) selected @endif >{{ $parser->name }} ({{ $parser->parserExchange->count() }})</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Код Отдаю</div>
                        <input type="text" name="code_in" class="form-control" id="name" value="{{ $item->code_in }}">
                        <div class="input-p-text">Пример: <b>BTC</b></div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Код Получаю</div>
                        <input type="text" name="code_out" class="form-control" id="name" value="{{ $item->code_out }}">
                        <div class="input-p-text">Пример: <b>USD</b></div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаки после, запятой</div>
                        <input type="number" name="number_format" class="form-control" id="number_format" value="{{ $item->number_format }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Тип парсера</div>

                        <select name="type" class="form-control selectpicker" data-width="100%">
                            <option value="0" @if($item->type == 0) selected @endif>Серверный</option>
                            <option value="1" @if($item->type == 1) selected @endif>Внутренний</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>

                        <select name="status" class="form-control selectpicker" data-width="100%">
                            <option value="0" @if($item->status == 0) selected @endif>Не активен</option>
                            <option value="1" @if($item->status == 1) selected @endif>Активен</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer">
            <div class="pull-right">
                <md-button name="success_and_exit" class="md-raised md-primary" type="submit">Сохранить</md-button>
                <md-button ng-href="{{ admin_base_path('/crypto/parser') }}">Назад</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
