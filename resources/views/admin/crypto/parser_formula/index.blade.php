@extends('admin.layouts.app')

@section('title', __('Курсы по формуле'))

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser-formula'))

@section('top-block')
    <md-button ng-href="parser-formula/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить формулу') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings_columns" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-table"></md-icon>
            <span>{{ __('Настройка страницы') }}</span>
        </md-button>
    @endcan

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <i class="far fa-cog"></i>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="far fa-filter"></i>&nbsp;&nbsp;&nbsp;{{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="font-size-16 far fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="font-size-16 far fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Заголовок') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::input('text', 'title', is_filter_search($filter, 'title'), ['class' => 'form-control']) !!}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_full_name" @if(isset($filter['checkbox_full_name'])) checked @endif>
                                            <label class="form-check-label" for="checkbox1">{{ __('Точное совпадение заголовка') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-basic.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-basic.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form style="width:100%;margin-right: 10px;" method="post" action="?">
                @csrf
                <table class="manager-table-2 table table-border-2">
                    <thead>
                    <tr>
                        @if(in_array('title', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Заголовок') }}</th>
                        @endif
                        @if(in_array('formula', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Формула') }}</th>
                        @endif

                        @if(in_array('course', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Курс') }}</th>
                        @endif
                        @if(in_array('pegged_currencies', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Привязанные направления') }}</th>
                        @endif
                        @if(in_array('created_at', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Дата создания') }}</th>
                        @endif
                        @if(in_array('updated_at', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Посл. обновление') }}</th>
                        @endif
                        @if(in_array('status', $admin_parser_formula_hidden_columns))
                            <th>{{ __('Статус') }}</th>
                        @endif
                        <th class="not-sortable"></th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($parsers) > 0)
                        @foreach($parsers as $rate)
                            <tr>
                                @if(in_array('title', $admin_parser_formula_hidden_columns))
                                    <th>
                                        <a href="{{ admin_base_path('/crypto/parser-formula/'.$rate->id.'/edit') }}">
                                            {{ $rate->title }}  &nbsp;<i class="fal fa-pencil"></i>
                                        </a>
                                    </th>
                                @endif
                                @if(in_array('formula', $admin_parser_formula_hidden_columns))
                                    <td>
                                        <input class="form-control" type="text" name="formula[{{ $rate->id }}]" value="{{ $rate->name }}">
                                    </td>
                                @endif
                                @if(in_array('course', $admin_parser_formula_hidden_columns))
                                    <td>
                                        @if($rate->status == 1)
                                            @if($rate->summa == null)
                                                <span class="text-danger">{{ __('Не указан') }}</span>
                                            @else
                                                {{ $rate->value }} → {{ iex_number_format($rate->summa, $rate->number_format) }}
                                            @endif
                                        @else
                                            <span class="text-danger">{{ __('Парсер отключен') }}</span>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('pegged_currencies', $admin_parser_formula_hidden_columns))
                                    <td>
                                        @if($rate->direction_exchange->count() > 0)
                                            <ul class="list-unstyled" height="100px" slim-scroll>
                                                @foreach($rate->direction_exchange as $value)
                                                    <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ direction_name($value) }}</a></li>
                                                @endforeach
                                            </ul>
                                        @else
                                            <small class="text-danger">{{ __('Не найдено') }}</small>
                                        @endif
                                    </td>
                                @endif

                                @if(in_array('created_at', $admin_parser_formula_hidden_columns))
                                    <td>
                                        <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->created_at)->translatedFormat('d M Y H:i') }}</div>
                                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->created_at)->diffForHumans() }}</small>
                                    </td>
                                @endif

                                @if(in_array('updated_at', $admin_parser_formula_hidden_columns))
                                    <td>
                                        <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->updated_at)->diffForHumans() }}</small>
                                    </td>
                                @endif
                                @if(in_array('status', $admin_parser_formula_hidden_columns))
                                    <td>
                                        @if($rate->status == 1)
                                            <span class="label label-success">{{ __('Активен') }}</span>
                                        @else
                                            <span class="label label-danger">{{ __('Отключен') }}</span>
                                        @endif
                                    </td>
                                @endif
                                <td style="width: 50px">
                                    <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/parser-formula/'.$rate->id.'/delete')  }}">
                                        <i class="fad fa-trash text-danger"></i>
                                    </md-button>
                                    <input type="hidden" name="item_id[]" value="{{ $rate->id }}">
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="default-panel-footer">
                    <div class="pull-right">

                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </form>
            <!-- Модальное окно для настроек таблицы -->
            <div class="modal fade" id="settings_columns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" >
                    <form class="form-horizontal" method="post" action="?">
                        <input type="hidden" name="action" value="settings_columns">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header ui-dialog-titlebar">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка страницы') }}</span>
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                    <label class="control-label col-md-6">{{ __('Колонки') }}</label>
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[title]" value="title" type="checkbox" name="admin_parser_formula_hidden_columns[title]" @if(in_array('title', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[title]">{{ __('Заголовок') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[formula]" value="formula" type="checkbox" name="admin_parser_formula_hidden_columns[formula]" @if(in_array('formula', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[formula]">{{ __('Формула') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[course]" value="course" type="checkbox" name="admin_parser_formula_hidden_columns[course]" @if(in_array('course', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[course]">{{ __('Курс') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[pegged_currencies]" value="pegged_currencies" type="checkbox" name="admin_parser_formula_hidden_columns[pegged_currencies]" @if(in_array('pegged_currencies', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[pegged_currencies]">{{ __('Привязанные направления') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[created_at]" value="created_at" type="checkbox" name="admin_parser_formula_hidden_columns[created_at]" @if(in_array('created_at', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[created_at]">{{ __('Дата создания') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[updated_at]" value="updated_at" type="checkbox" name="admin_parser_formula_hidden_columns[updated_at]" @if(in_array('updated_at', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[updated_at]">{{ __('Посл. обновление') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="admin_parser_formula_hidden_columns[status]" value="status" type="checkbox" name="admin_parser_formula_hidden_columns[status]" @if(in_array('status', $admin_parser_formula_hidden_columns)) checked @endif>
                                            <label class="form-check-label" for="admin_parser_formula_hidden_columns[status]">{{ __('Статус') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label for="admin_parser_formula_paginate" class="control-label col-md-6">{{ __('Кол-во записей на странице') }}</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="admin_parser_formula_paginate" class="form-control" value="{{ iEXSetting('admin_parser_formula_paginate', 20) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                                <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <!-- Модальное окно для настроек -->
            <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" >
                    <form class="form-horizontal" method="post" action="?">
                        <input type="hidden" name="action" value="settings">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header ui-dialog-titlebar">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <span class="ui-dialog-title" id="myModalLabel">Настройки</span>
                            </div>
                            <div class="modal-body">


                                <div class="form-group">
                                    <div class="col-sm-12">

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">Записывать историю курсов</div>
                                            <select class="form-control selectpicker" name="record_course_formula_history">
                                                <option value="0"  @if(iEXSetting('record_course_formula_history') == 0) selected @endif>Нет</option>
                                                <option value="1"  @if(iEXSetting('record_course_formula_history') == 1) selected @endif>Да</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>
                            <div class="modal-footer">
                                <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                                <md-button type="button" data-dismiss="modal">Отмена</md-button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $parsers->links() !!}
    </div>
@endsection
