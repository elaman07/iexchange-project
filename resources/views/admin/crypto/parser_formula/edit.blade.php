@extends('admin.layouts.app')

@section('title','Обновить формулу')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser-formula.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/parser-formula/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Обновить формулу</label>
                <div class="col-sm-10">


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Заголовок</div>
                        <input type="text" name="title" class="form-control" id="title" value="{{ $item->title }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-12">
                        <div class="control-label-br">Формула</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $item->name  }}" required>
                        <div class="input-p-text">Здесь вы можете указать коды для создания курса. <br/> <b>Например:</b> [binance_1inch-btc]*[binance_1inch-busd].</div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if($item->status == 0) selected @endif>Не активный парсер</option>
                            <option value="1" @if($item->status == 1) selected @endif>Активный парсер</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаки после, запятой</div>
                        <input type="text" name="number_format" class="form-control" id="number_format" value="{{ $item->number_format }}" required>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/parser-formula') }}">Назад</md-button>
        </div>
    </form>

@endsection
