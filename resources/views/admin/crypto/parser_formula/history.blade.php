@extends('admin.layouts.app')

@section('title')
    @if(!is_null($item))
        {{ __('История курсов')  }} {{ $item->name }}
    @else
        {{ __('История курсов') }}
    @endif
@endsection

@section('top-block')
    <md-button ng-href="?clear" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">{{ __('Очистить историю') }}</span>
    </md-button>
@endsection

@section('breadcrumbs')
    @if(!is_null($item))
        {{ Breadcrumbs::render('admin.crypto.parser.history_id', $item) }}
    @else
        {{ Breadcrumbs::render('admin.crypto.parser.history') }}
    @endif
@endsection

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Дата создания') }}</th>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Формула') }}</th>
            <th>{{ __('Курс') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <th>
                        <div>{{ $log->created_at }}</div>
                        <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</small>
                    </th>
                    <td>
                        {{ $log->name }}
                    </td>
                    <td>
                        {{ $log->formula }}
                    </td>
                    <td>
                        {{ $log->amount }}
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
