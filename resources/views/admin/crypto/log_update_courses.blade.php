@extends('admin.layouts.app')

@section('title', 'Лог обновлении курсов')


@section('top-block')
    <md-button ng-href="?clear_logs" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">{{ __('Очистить лог') }}</span>
    </md-button>
@endsection

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Лог обновлении курсов') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th> {{ __('Дата создания') }}</th>
                    <th> {{ __('Время обновления') }}</th>
                    <th> {{ __('Тип обновления') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <th>
                                {{ $item->time }} sec
                            </th>
                            <td>
                               {{ $item->type_rate }}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br /> {{ __('Список пуст') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
