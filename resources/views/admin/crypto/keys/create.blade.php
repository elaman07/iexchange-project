@extends('admin.layouts.app')

@section('title','Добавить API Ключ')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser.api_keys.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/api-keys') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Ключи (с новой строки)</label>
                <div class="col-sm-5">
                    <textarea rows="5" class="form-control" name="keys"></textarea>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Статус</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="status" data-width="150">
                        <option value="0">Отключено</option>
                        <option value="1">Включено</option>
                    </select>
                </div>
            </div>


            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Источник курсов</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="provider_id" data-width="300">
                        <option value="coinmarketcap">CoinMarketCap</option>
                        <option value="fixerapi">Fixer API</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/api-keys') }}">Назад</md-button>
        </div>
    </form>

@endsection
