@extends('admin.layouts.app')

@section('title','API Ключи источников')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser.api_keys'))

@section('video__instruction', 'https://www.youtube.com/embed/fjwbAo0Rhh4?autoplay=1&amp;mute=0')

@section('top-block')
    <md-button ng-href="api-keys/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить ключ</span>
    </md-button>

    <md-button ng-href="api-keys/delete_all" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">Удалить ключи</span>
    </md-button>
@endsection

@section('content')
    <form style="width:100%;margin-right: 10px;" method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Дата добавления</th>
                <th>Ключ</th>
                <th>Источник</th>
                <th>Просмотров</th>
                <th>Статус</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($items) > 0)
                @foreach($items as $item)
                    <tr>
                        <th>{{$item->created_at}}</th>
                        <td>{{$item->api_key}}</td>
                        <td>{{$item->provider_id}}</td>
                        <th>{{ iex_number_format($item->view_count, 0, true) }}</th>
                        <td>
                            <input type="hidden" name="key_id[]" value="{{$item->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="1" @if($item->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width:150px;">

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/api-keys/'.$item->id.'/edit') }}">
                                <i class="far fa-pencil"></i>
                            </md-button>

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/api-keys/'.$item->id.'/delete')  }}">
                                <i class="far fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />Список пуст</p></td>
                </tr>
            @endif
            </tbody>
        </table>

        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">Сохранить</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection

@section('pagination')
    <div class="pagination-right">
        {{ $items->links() }}
    </div>
@endsection
