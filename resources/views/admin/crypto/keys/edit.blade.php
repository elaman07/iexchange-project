@extends('admin.layouts.app')

@section('title','Изменить ключ '. $item->api_key)

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.parser.api_keys.edit', $item))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/api-keys/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Ключ</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="key" value="{{$item->api_key}}" required>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Статус</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="status" data-width="150">
                        <option value="0" @if($item->status == 0) selected @endif>Отключено</option>
                        <option value="1" @if($item->status == 1) selected @endif>Включено</option>
                    </select>
                </div>
            </div>


            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Источник курсов</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="provider_id" data-width="300">
                        <option value="coinmarketcap" @if($item->provider_id == 'coinmarketcap') selected @endif>CoinMarketCap</option>
                        <option value="fixerapi" @if($item->provider_id == 'fixerapi') selected @endif>Fixer API</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/api-keys') }}">Назад</md-button>
        </div>
    </form>

@endsection
