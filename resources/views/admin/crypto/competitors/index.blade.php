@extends('admin.layouts.app')

@section('title', 'Курсы конкурентов')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.competitors-parser'))

@section('top-block')
    <md-button ng-href="competitors-parser/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить</span>
    </md-button>

    <md-button ng-href="competitors-parser/history" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-history text-muted"></md-icon>
        <span class="text-muted">История курсов</span>
    </md-button>

    <md-button ng-href="competitors-link" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-database"></md-icon>
        <span>Источники</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройки</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')

    @if(count($links) > 0)
        <div class="panel panel-default">

            <div class="panel-heading nav-tabs-solid-header">
                <ul class="nav nav-tabs nav-tabs-solid">
                    @foreach($links as $key => $value)
                        <li class="@if($key == 0) active @endif" style="margin-bottom: 0;">
                            <a md-ink-ripple="'#000'" href="#crypto-{{$value->id}}" data-toggle="tab" aria-expanded="false">
                                {{$value->name}} ({{$value->rates->count()}})
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="panel-tab-content tab-content">
                @foreach($links as $key => $value)
                    <div class="tab-pane @if($key == 0) active @endif" id="crypto-{{$value->id}}">
                        <form style="width:100%;margin-right: 10px;" method="post" action="?">
                            @csrf
                            <table class="manager-table-2 table table-border-2">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Курс</th>
                                    <th>Корректировать курс</th>
                                    <th>Дата создания</th>
                                    <th>Посл. обновление</th>
                                    <th>Статус</th>
                                    <th class="not-sortable"></th>
                                </tr>

                                </thead>
                                <tbody>

                                @if(count($value->rates) > 0)
                                    @foreach($value->rates as $rate)
                                        <tr @if($rate->duplicate_count() > 1) class="isset-table-td" @endif>
                                            <th>
                                                <a href="{{ admin_base_path('/crypto/competitors-parser/'.$rate->id.'/edit') }}">
                                                    {{ $rate->name }}  &nbsp;<i class="fal fa-pencil"></i>
                                                </a>
                                                <br />
                                                <div class="parser-code__item">
                                                    {{ __('Код пары') }}:
                                                    <span class="iex-item__copied" ngclipboard data-clipboard-text="{{ $rate->code }}" ngclipboard-success="onEventClipboardSuccess('Code');">
                                                    {{ $rate->code }}&nbsp;&nbsp;<i class="fad fa-copy"></i>
                                                </span>
                                                </div>
                                            </th>
                                            <td>
                                                @if($rate->status == 1)
                                                    @if($rate->summa == null)
                                                        <span class="text-danger">Не указан</span>
                                                    @else
                                                        {{$rate->value}}  →	 {{$rate->summa}}
                                                    @endif
                                                @else
                                                    <span class="text-danger">Парсер отключен</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($rate->type == 0)
                                                    По "Отдаете"
                                                @elseif($rate->type == 1)
                                                    По "Получаете"
                                                @endif
                                            </td>
                                            <td>
                                                <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->created_at)->translatedFormat('d M Y H:i') }}</div>
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->created_at)->diffForHumans() }}</small>
                                            </td>


                                            <td>
                                                <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->updated_at)->diffForHumans() }}</small>
                                            </td>
                                            <td>
                                                @if($rate->status == 1)
                                                    <span class="label label-success">Активен</span>
                                                @else
                                                    <span class="label label-danger">Отключен</span>
                                                @endif
                                            </td>
                                            <td style="width: 150px">

                                                <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/competitors-parser/'.$rate->id.'/history')  }}">
                                                    <i class="fad fa-history text-info"></i>
                                                </md-button>

                                                <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/competitors-parser/'.$rate->id.'/delete')  }}">
                                                    <i class="fad fa-trash text-danger"></i>
                                                </md-button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10"><p align="center"><br />Список пуст</p></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                            <div class="default-panel-footer">
                                <div class="pull-right">

                                    <div style="display: inline-block">
                                        <select name="actions" class="form-control selectpicker" data-width="200px">
                                            <option value="0">Действия</option>
                                            <option value="change_type">Изменить тип парсинга</option>
                                            <option value="activation">Активировать</option>
                                            <option value="deactivation">Деактивировать</option>
                                            <option value="delete">Удалить</option>
                                        </select>
                                    </div>
                                    <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="alert alert-danger text-center">Нет данных</div>
    @endif


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки Bestchange парсинга</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">Записывать историю курсов</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="record_course_history_competitors" data-width="250px">
                                    <option value="0"  @if(iEXSetting('record_course_history_competitors') == 0) selected @endif>Нет</option>
                                    <option value="1"  @if(iEXSetting('record_course_history_competitors') == 1) selected @endif>Да</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить округление в парсинге</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_competitors_rounding" name="is_enabled_competitors_rounding" type="checkbox" value="1" @if(iEXSetting('is_enabled_competitors_rounding') == 1) checked @endif />
                                    <label for="is_enabled_competitors_rounding" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
