@extends('admin.layouts.app')

@section('title','Добавить ссылку')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.competitors-link.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/competitors-link') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новый источник</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Ссылка на XML файл</div>
                        <input type="text" name="link" class="form-control" id="link" value="{{ old('link') }}" placeholder="Укажите ссылку на XML файл курсов" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-2">
                        <div class="control-label-br">Статус</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if(old('status') == 0) selected @endif>Отключено</option>
                            <option value="1" @if(old('status') == 1) selected @endif>Включено</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/competitors-link') }}">Назад</md-button>
        </div>
    </form>

@endsection
