@extends('admin.layouts.app')

@section('title','Добавить пару')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.file-parser.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/file-parser') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новая пара</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" required>
                        <div class="input-p-text">Формат названия: <b>BTC - SBERRUB</b></div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Источник</div>
                        @if(count($links) == 0)
                            <div class="text-danger">Нет доступных источников курсов</div>
                            <small>
                                <a href="{{ admin_base_path('/crypto/file-parser-group') }}">Добавить источник курсов</a>
                            </small>
                        @else
                            <select class="form-control selectpicker" name="id_group" id="id_group" data-live-search="true" required>
                                @foreach($links as $link)
                                    <option value="{{ $link->id }}" @if(old('id_group') == $link->id) selected @endif>{{ $link->name }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if(old('status') == 0) selected @endif>Не активный парсер</option>
                            <option value="1" @if(old('status') == 1) selected @endif>Активный парсер</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Корректировать курс</div>
                        <select name="type" class="form-control selectpicker">
                            <option value="0" @if(old('type') == 0) selected @endif>Отдаете</option>
                            <option value="1" @if(old('type') == 1) selected @endif>Получаете</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаки после, запятой</div>
                        <input type="text" name="number_format" class="form-control" id="number_format" value="{{ old('number_format', 0) }}" required>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/file-parser') }}">Назад</md-button>
        </div>
    </form>

@endsection
