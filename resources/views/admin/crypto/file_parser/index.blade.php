@extends('admin.layouts.app')

@section('title', 'Курсы обмена из файлов')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.file-parser'))

@section('top-block')
    <md-button ng-href="file-parser/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить пару</span>
    </md-button>
@endsection

@section('no-block-content')

    @if(count($links) > 0)
        <div class="panel panel-default">

            <div class="panel-heading nav-tabs-solid-header group-parser-header">
                <ul class="nav nav-tabs nav-tabs-solid group-parser-nav">
                    @foreach($links as $key => $value)
                        <li class="@if($key == 0) active @endif">
                            <a md-ink-ripple="'#000'" href="#crypto-{{ $value->id }}" data-toggle="tab" aria-expanded="false">
                                {{ $value->name }}
                                <span class="badge badge-light">{{ $value->rates->count() }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="panel-tab-content tab-content">
                @foreach($links as $key => $value)
                    <div class="tab-pane @if($key == 0) active @endif" id="crypto-{{$value->id}}">
                        <form style="width:100%;margin-right: 10px;" method="post" action="?">
                            @csrf
                            <table class="manager-table-2 table table-border-2">
                                <thead>
                                <tr>
                                    <th>Пара</th>
                                    <th>Курс</th>
                                    <th>Привязанные направления</th>
                                    <th>Дата создания</th>
                                    <th>Посл. обновление</th>
                                    <th>Статус</th>
                                    <th class="not-sortable"></th>
                                </tr>

                                </thead>
                                <tbody>

                                @if(count($value->rates) > 0)
                                    @foreach($value->rates as $rate)
                                        <tr >

                                            <th>
                                                <a href="{{ admin_base_path('/crypto/file-parser/'.$rate->id.'/edit') }}">
                                                    {{ $rate->name }}  &nbsp;<i class="fal fa-pencil"></i>
                                                </a>

                                                <br />
                                                <div class="parser-code__item">
                                                    {{ __('Код пары') }}:
                                                    <span class="iex-item__copied" ngclipboard data-clipboard-text="{{ $rate->code }}" ngclipboard-success="onEventClipboardSuccess('Code');">
                                                    {{ $rate->code }}&nbsp;&nbsp;<i class="fad fa-copy"></i>
                                                </span>
                                                </div>
                                            </th>
                                            <td>
                                                @if($rate->status == 1)
                                                    @if($rate->summa == null)
                                                        <span class="text-danger">Не указан</span>
                                                    @else
                                                        {{ $rate->value }}  →	 {{ $rate->summa }}
                                                    @endif
                                                @else
                                                    <span class="text-danger">Парсер отключен</span>
                                                @endif
                                            </td>

                                            <td>
                                                @if($rate->direction_exchange->count() > 0)
                                                    <ul class="list-unstyled">
                                                        @foreach($rate->direction_exchange as $value)
                                                            <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ direction_name($value) }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <small class="text-danger">Не найдено</small>
                                                @endif
                                            </td>

                                            <td>
                                                <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->created_at)->translatedFormat('d M Y H:i') }}</div>
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->created_at)->diffForHumans() }}</small>
                                            </td>


                                            <td>
                                                <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->updated_at)->diffForHumans() }}</small>
                                            </td>

                                            <td>
                                                @if($rate->status == 1)
                                                    <span class="label label-success">Активен</span>
                                                @else
                                                    <span class="label label-danger">Отключен</span>
                                                @endif
                                            </td>
                                            <td style="width: 100px">
                                                <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/file-parser/'.$rate->id.'/delete')  }}">
                                                    <i class="fad fa-trash text-danger"></i>
                                                </md-button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10"><p align="center"><br />Список пуст</p></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                            <div class="default-panel-footer">
                                <div class="pull-right">

                                    <div style="display: inline-block">
                                        <select name="actions" class="form-control selectpicker" data-width="200px">
                                            <option value="0">Действия</option>
                                            <option value="change_type">Изменить тип парсинга</option>
                                            <option value="activation">Активировать</option>
                                            <option value="deactivation">Деактивировать</option>
                                            <option value="delete">Удалить</option>
                                        </select>
                                    </div>
                                    <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="alert alert-danger text-center">Нет данных</div>
    @endif
@endsection
