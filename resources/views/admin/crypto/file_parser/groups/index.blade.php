@extends('admin.layouts.app')

@section('title', 'Группы курсов из файла')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.file-parser-group'))

@section('top-block')
    <md-button ng-href="file-parser-group/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить источник</span>
    </md-button>

    <md-button ng-href="file-parser-group/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-filter text-primary"></md-icon>
        <span>Сортировка</span>
    </md-button>
@endsection

@section('content')
    <form style="width:100%;margin-right: 10px;" method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Название</th>
                <th>Кол-во курсов</th>
                <th>Адрес файла</th>
                <th>Дата добавления</th>
                <th>Статус</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($links) > 0)
                @foreach($links as $link)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('/crypto/file-parser-group/'.$link->id.'/edit') }}">
                                {{ $link->name }}  &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>
                            {{ $link->rates->count() }}
                        </td>
                        <td>
                            <input type="text" class="form-control" name="link[{{$link->id}}]" value="{{ $link->link }}">
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($link->created_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($link->created_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$link->id}}" name="status[{{$link->id}}]" type="checkbox" value="1" @if($link->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$link->id}}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width:150px;">

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/file-parser-group?id='.$link->id.'&duplicate=true') }}">
                                <i class="fad fa-copy"></i>
                            </md-button>

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/file-parser-group/'.$link->id.'/delete')  }}">
                                <i class="fad fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />Список пуст</p></td>
                </tr>
            @endif
            </tbody>
        </table>

        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">Сохранить</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection

@section('pagination')
    <div class="pagination-right">
        {{ $links->links() }}
    </div>
@endsection
