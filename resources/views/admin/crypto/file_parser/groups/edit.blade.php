@extends('admin.layouts.app')

@section('title','Изменить источник '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.file-parser-group.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/file-parser-group/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('put')


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Изменить источник</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" placeholder="Название источника" value="{{$item->name}}" class="form-control" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Ссылка на файл</div>
                        <input type="text" name="link"  placeholder="Укажите ссылку на файл курсов" value="{{$item->link}}" class="form-control" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-2">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status" data-width="150">
                            <option value="0" @if($item->status == 0) selected @endif>Отключено</option>
                            <option value="1" @if($item->status == 1) selected @endif>Включено</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/file-parser-group') }}">Назад</md-button>
        </div>
    </form>

@endsection
