@extends('admin.layouts.app')

@section('title','Добавить коэффициент для формулы')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.formula-coefficient.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/formula-coefficient') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Добавить коэффициент</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" required>
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Сумма</div>
                        <input type="text" name="summa" class="form-control" id="summa" value="{{ old('summa', 0) }}" required>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/formula-coefficient') }}">Назад</md-button>
        </div>
    </form>

@endsection
