@extends('admin.layouts.app')

@section('title', 'Коэффициенты для формул')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.formula-coefficient'))

@section('top-block')
    <md-button ng-href="formula-coefficient/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить коэффициент</span>
    </md-button>
@endsection

@section('content')


        <table class="manager-table-2 table table-border-2">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Код</th>
                        <th>Сумма</th>
                        <th>Дата создания</th>
                        <th>Посл. обновление</th>
                        <th class="not-sortable"></th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($parsers) > 0)
                        @foreach($parsers as $rate)
                            <tr >

                                <th>
                                    <a href="{{ admin_base_path('/crypto/formula-coefficient/'.$rate->id.'/edit') }}">
                                        {{ $rate->name }}  &nbsp;<i class="fal fa-pencil"></i>
                                    </a>
                                </th>
                                <td>
                                    {{ $rate->alias }}
                                </td>
                                <td>
                                    {{ $rate->summa }}
                                </td>


                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->created_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->created_at)->diffForHumans() }}</small>
                                </td>


                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($rate->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($rate->updated_at)->diffForHumans() }}</small>
                                </td>

                                <td style="width: 100px">
                                    <md-button class="md-icon-button" ng-href="{{ admin_base_path('crypto/formula-coefficient/'.$rate->id.'/delete')  }}">
                                        <i class="fad fa-trash text-danger"></i>
                                    </md-button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />Список пуст</p></td>
                        </tr>
                    @endif
                    </tbody>
        </table>
@endsection
