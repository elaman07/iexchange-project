@extends('admin.layouts.app')

@section('title','Изменить коэффициент для формулы')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.formula-coefficient.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/formula-coefficient/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Обновить коэффициент</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Сумма</div>
                        <input type="text" name="summa" class="form-control" id="number_format" value="{{ $item->summa }}" required>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/formula-coefficient') }}">Назад</md-button>
        </div>
    </form>

@endsection
