@extends('admin.layouts.app')

@section('title','Добавить партнера для парсинга')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.partner-parser-group.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/partner-parser-group') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новый партнер</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">Название партнера</div>
                        <select class="form-control selectpicker" name="name"></select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-2">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status" data-width="150">
                            <option value="0" @if(old('status') == 0) selected @endif>Отключено</option>
                            <option value="1" @if(old('status') == 1) selected @endif>Включено</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/partner-parser-group') }}">Назад</md-button>
        </div>
    </form>

@endsection
