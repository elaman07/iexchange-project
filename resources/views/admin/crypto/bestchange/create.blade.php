@extends('admin.layouts.app')

@section('title','Добавить новый курс')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.bestchange.add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/bestchange') }}">

        @csrf
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Детали курса</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Отдаете</div>
                        {{ Form::select('give', $rates, old('exchanger_id1'), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10']) }}
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Получаете</div>
                        {{ Form::select('receiving', $rates, old('exchanger_id2'), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10']) }}
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаков, после запятой</div>
                        <input type="number" name="number_format" class="form-control" id="number_format" value="{{ old('number_format', 10) }}" required @if((int)iEXSetting('is_enabled_bestchange_rounding') == 0)readonly @endif>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select name="status" class="form-control selectpicker">
                            <option value=0"  @if(old('status', 1) == 0) selected @endif>Не активный парсер</option>
                            <option value="1"  @if(old('status', 1) == 1) selected @endif>Активный парсер</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/bestchange') }}">Назад</md-button>
        </div>
    </form>

@endsection
