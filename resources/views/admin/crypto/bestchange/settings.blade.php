@extends('admin.layouts.app')

@section('title','Настроки парсинга')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.bestchange.settings'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/bestchange/settings') }}">
        <div class="default-panel-body">
            @csrf


            <div class="form-group">
                <label class="col-sm-2 control-label">Настройка парсинга</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Включить BestChange парсер</div>
                        <select class="form-control selectpicker" name="is_enable_parser_bs_console">
                            <option value="0" @if(iEXSetting('is_enable_parser_bs_console') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('is_enable_parser_bs_console') == 1) selected @endif>Да</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Сервер BestChange</div>
                        <select class="form-control selectpicker" name="public_server_bestchange">
                            <option value="0" @if(iEXSetting('public_server_bestchange') == 0) selected @endif>api.bestchange.ru</option>
                            <option value="1" @if(iEXSetting('public_server_bestchange') == 1) selected @endif>api.bestchange.net</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Таймер</div>
                        <input type="text" name="bestchange_timeout" class="form-control" id="bestchange_timeout" value="{{ iEXSetting('bestchange_timeout', 30) }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Включить режим тестирования</div>
                        <select class="form-control selectpicker" name="is_crypto_parser_test">
                            <option value="0" @if(iEXSetting('is_crypto_parser_test') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('is_crypto_parser_test') == 1) selected @endif>Да</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Черный список ID обменников</div>
                        {!! Form::select('bestchange_exchange_black_list[]', $exchangers, explode(',', iEXSetting('bestchange_exchange_black_list')), ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '10']) !!}
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Исключить записи обменников (Из списка мошенников)</div>
                        {!! Form::select('bestchange_exc_scam[]', $exchangers_2, explode(',', iEXSetting('bestchange_exc_scam')), ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '10']) !!}
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Парсить с позиции</div>
                        <input type="text" name="bestchange_position" class="form-control" id="BESTCHANGE_POSITION" value="{{ iEXSetting('bestchange_position', 10) }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Выбранные валюты для парсинга</div>
                        {!! Form::select('bestchange_currencies_parsers[]', $allowed_currencies, explode(',', iEXSetting('bestchange_currencies_parsers')), ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '10']) !!}
                        <div class="input-p-text">
                            <a href="?wid_cache_control=clear_cache_bestchange_rates">Очистить кэш</a>
                        </div>
                    </div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Выбранные города для парсинга</div>
                        {!! Form::select('bestchange_cities_parsers[]', $cities, explode(',', iEXSetting('bestchange_cities_parsers')), ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '10']) !!}
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label class="col-sm-2 control-label">Опции</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Включить Bestchange лог</div>
                        <select class="selectpicker form-control" name="is_enabled_bestchange_log">
                            <option value="0" @if(iEXSetting('is_enabled_bestchange_log') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('is_enabled_bestchange_log') == 1) selected @endif>Да</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Записывать лог ошибок</div>
                        <select class="selectpicker form-control" name="is_enable_bs_log_error">
                            <option value="0" @if(iEXSetting('is_enable_bs_log_error') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('is_enable_bs_log_error') == 1) selected @endif>Да</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Записывать историю курсов</div>
                        <select class="selectpicker form-control" name="record_course_history_bestchange">
                            <option value="0" @if(iEXSetting('record_course_history_bestchange') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('record_course_history_bestchange') == 1) selected @endif>Да</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Включить округление в парсинге</div>
                        <select class="selectpicker form-control" name="is_enabled_bestchange_rounding">
                            <option value="0" @if(iEXSetting('is_enabled_bestchange_rounding') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('is_enabled_bestchange_rounding') == 1) selected @endif>Да</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Уведомлять в Telegram, если курс отсутствует в Bestchange</div>
                        <select class="selectpicker form-control" name="is_enable_bs_undefined_pair">
                            <option value="0" @if(iEXSetting('is_enable_bs_undefined_pair') == 0) selected @endif>Нет</option>
                            <option value="1" @if(iEXSetting('is_enable_bs_undefined_pair') == 1) selected @endif>Да</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/bestchange') }}">Назад</md-button>
        </div>
    </form>

@endsection
