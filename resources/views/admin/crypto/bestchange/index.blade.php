@extends('admin.layouts.app')

@section('title', 'BestChange парсер')

@section('top-block')

    <md-button ng-href="?update_rates" class="btn-float" layout="column">
        <i class="far fa-refresh add-item-button-color"></i>
        <span class="add-item-button-color">Обновить курсы</span>
    </md-button>

    <md-button ng-href="bestchange/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">Добавить пару</span>
    </md-button>
@endsection

@section('video__instruction', 'https://www.youtube.com/embed/KZ1ZSFZBPv0?autoplay=1&amp;mute=0')


@section('breadcrumbs', Breadcrumbs::render('admin.crypto.bestchange'))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form style="width:100%;margin-right: 10px;" method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th class="not-sortable">
                            <div class="form-check-v2">
                                <input id="checkbox-0" type="checkbox" class="form-check-input checkAll">
                                <label class="form-check-label" for="checkbox-0"></label>
                            </div>
                        </th>
                        <th>Пара</th>
                        <th>Курс</th>
                        <th>Привязанные направления</th>
                        <th>Дата создания</th>
                        <th>Посл. обновление</th>
                        <th class="not-sortable"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($rates) > 0)
                        @foreach($rates as $item)
                            <tr @if($item->duplicate()->count() > 1) class="table-gateway-offline-tr" @endif>
                                <th>
                                    <div class="form-check-v2">
                                        <input class="form-check-input" id="checkbox{{ $item->id }}"
                                               value="{{ $item->id }}" type="checkbox" name="bestchange_checkbox[{{ $item->id }}]">
                                        <label class="form-check-label" for="checkbox{{$item->id}}"></label>
                                    </div>
                                </th>
                                <th>
                                    <a href="{{ admin_base_path('/crypto/bestchange/'.$item->id.'/edit') }}">
                                        {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                                    </a>

                                    @if($item->is_not_pair == 1)
                                        <div>
                                            <small class="text-danger font-weight-bold">
                                                <a href="{{ admin_base_path('/crypto/bestchange/error_log/'.$item->id) }}" class="text-danger"  data-toggle="tooltip" title="В Bestchange отсутствуют курсы по направлению {{$item->name}}">
                                                    <i class="far fa-exclamation-circle text-danger"></i>
                                                    ошибка обновлении курсов
                                                </a>
                                            </small>
                                        </div>
                                    @endif
                                </th>
                                <td>
                                    @if($item->status == 1)
                                        @if($item->summa == null)
                                            <span class="text-danger">Не указан</span>
                                        @elseif($item->summa == 0)
                                            <small class="text-danger">
                                                <i class="far fa-exclamation-triangle"></i>
                                                Требуется обновление курсов
                                            </small>
                                        @else
                                            <div>{{$item->value}} → {{$item->summa}}</div>
                                        @endif

                                        @if(!empty($item->source_name))
                                            <div style="font-size: 11px" class="text-muted">Источник: <b>{{ $item->source_name }}</b></div>
                                        @endif
                                    @else
                                        <span class="text-danger">Парсер отключен</span>
                                    @endif
                                </td>
                                <td style="width: 300px">
                                    @if(count($item->direction_exchange) > 0)
                                        <ul class="list-unstyled">
                                            @foreach($item->direction_exchange as $value)
                                                <li>
                                                    - <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ $value->tech_name }}</a>

                                                    <a href="?actions=delete&id={{ $item->id }}&id_direction={{ $value->id }}">
                                                        &nbsp;
                                                        <small>
                                                            <i class="text-danger far fa-trash"></i>
                                                        </small>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <small class="text-danger">Не найдено</small>
                                    @endif
                                </td>
                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                </td>
                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                                </td>
                                <td style="width: 150px">

                                    <md-button class="md-icon-button" ng-href="{{config('admin.directory')}}/crypto/bestchange/history/{{$item->id}}">
                                        <i class="far fa-history text-info"></i>
                                    </md-button>

                                    <md-button class="md-icon-button" ng-href="{{ admin_base_path('/crypto/bestchange/?id='.$item->id.'&delete') }}">
                                        <i class="far fa-trash text-danger"></i>
                                    </md-button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                        </tr>
                    @endif
                    </tbody>

                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">

                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="0">Действия</option>
                                <option value="activation">Активировать</option>
                                <option value="deactivation">Деактивировать</option>
                                <option value="delete">Удалить</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
