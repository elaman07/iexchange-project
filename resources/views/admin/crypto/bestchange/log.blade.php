@extends('admin.layouts.app')

@section('title', 'Bestchange лог')
@section('top-block')

    <md-button ng-href="?clear" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">Очистить лог</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.bestchange.log'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Дата создания</th>
            <th>Тип</th>
            <th>Текст</th>
            <th>Откуда</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <th>
                        <div>{{$log->created_at}}</div>
                        <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</small>
                    </th>
                    <td>
                        @if($log->type_log == 0)
                            <label class="label label-success">Успешно</label>
                        @elseif($log->type_log == 1)
                            <label class="label label-danger">Ошибка</label>
                        @endif
                    </td>
                    <td style="width: 30%">
                        @if(empty($log->message))
                            <span class="text-danger">Нет текста</span>
                        @else
                            {{ $log->message }}
                        @endif
                    </td>
                    <td style="width: 200px">
                        @if($log->where_from == 1)
                            Добавить в черный список
                        @elseif($log->where_from == 2)
                            Поиск в черном списке
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />Список пуст</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
