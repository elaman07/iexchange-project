@extends('admin.layouts.app')

@section('title', 'Bestchange: Лог ошибок '. $item->name)
@section('top-block')

    <md-button ng-href="?clear" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">Очистить лог</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.bestchange.error_log', $item))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Дата создания</th>
            <th>Пара</th>
            <th>Направление</th>
            <th>Ошибка</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <th>
                        <div>{{$log->created_at}}</div>
                        <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</small>
                    </th>
                    <td>
                        {{ $log->bestchange_rates->name }}
                    </td>
                    <td style="width: 200px">
                        @if(isset($log->direction_exchange))
                        <a href="{{ admin_base_path('/basic/direction_exchange/'.$log->id_direction_exchange.'/edit') }}">{{ direction_name($log->direction_exchange) }}</a>
                        @else
                            <span class="text-danger">Не найден</span>
                        @endif
                    </td>
                    <td style="width: 30%">
                        @if(empty($log->description))
                            <span class="text-danger">Нет текста</span>
                        @else
                            {{ $log->description }}
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />Список пуст</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
