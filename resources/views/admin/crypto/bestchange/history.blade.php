@extends('admin.layouts.app')

@section('title')
    @if(!is_null($item))
        История курсов {{$item->name}}
    @else
        История курсов
    @endif
@endsection

@section('top-block')

    <md-button ng-href="?clear" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">Очистить историю</span>
    </md-button>
@endsection

@section('breadcrumbs')
    @if(!is_null($item))
        {{ Breadcrumbs::render('admin.crypto.bestchange.history_id', $item) }}
    @else
        {{ Breadcrumbs::render('admin.crypto.bestchange.history') }}
    @endif
@endsection

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Дата создания</th>
            <th>Пара</th>
            <th>Курс</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <th>
                        <div>{{$log->created_at}}</div>
                        <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</small>
                    </th>
                    <td>{{ $log->bestchange_rates->name }}</td>
                    <td>
                        @if($log->out_price == null)
                            <span class="text-danger">Не указан</span>
                        @else
                            {{$log->in_price}} → {{$log->out_price}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
