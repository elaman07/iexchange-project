@extends('admin.layouts.app')

@section('title','Изменить курс')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.bestchange.change', $item->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/crypto/bestchange/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Детали курса</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Отдаете</div>
                        {{ Form::select('give', $rates, $item->exchanger_id1, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10']) }}
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Получаете</div>
                        {{ Form::select('receiving', $rates, $item->exchanger_id2, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10']) }}
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаков, после запятой</div>
                        <input type="number" name="number_format" class="form-control" id="number_format" value="{{ $item->number_format }}" required @if((int)iEXSetting('is_enabled_bestchange_rounding') == 0)readonly @endif>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select name="status" class="form-control selectpicker">
                            <option value=0"  @if($item->status == 0) selected @endif>Не активный парсер</option>
                            <option value="1"  @if($item->status == 1) selected @endif>Активный парсер</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button name="success_and_exit" class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/crypto/bestchange') }}">Назад</md-button>
        </div>
    </form>
@endsection
