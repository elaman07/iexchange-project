@extends('admin.layouts.app')

@section('title','Группы')

@section('breadcrumbs', Breadcrumbs::render('admin.crypto.group'))

@section('content')
    <form method="post" action="?form=home">
       @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Источник</th>
                <th>Статус</th>
            </tr>

            </thead>
            <tbody>

            @foreach($group as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>
                        <div class="material-switch">
                            <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="1" @if($item->status == 1) checked @endif />
                            <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

    </form>
@endsection