@extends('layouts.app')

@section('title', __('admin.google2fa.title'))

@section('admin-page')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css?v=1">
@endsection

@section('content-base')
    <div class="container">
        <div class="col-md-4 col-md-offset-4">
            <div class="page-container" style="min-height:385px">
                <!--MAIN area-->
                <div class="panel panel-default" style="margin-top: 100px;">

                    <div class="panel-heading">
                        {{ __('admin.google2fa.title_label') }}
                    </div>

                    <div class="panel-body">

                        <form  class="separate-sections" role="form" method="POST" action="{{ route('2faGoogleVerify') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('one_time_password-code') ? ' has-error' : '' }}">
                                <input id="one_time_password" type="number" class="form-control" placeholder="{{ __('admin.google2fa.write_code') }}" name="one_time_password" autofocus required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-raised btn-block legitRipple">{{ __('admin.google2fa.save') }} <i class="fa fa-sign-in"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-muted text-size-small text-center">Copyright 2018<br></div>
                <!--MAIN area-->
            </div>
        </div>
    </div>
@endsection