<table class="table table-xs empty-page-table">
    <tbody>
    <tr>
        <td class="no-border-top">
            <div align="center">
                <br><br>{{ !is_null($message) ? $message : 'Нет данных' }}<br><br><br>
            </div>
        </td>
    </tr>
    </tbody>
</table>