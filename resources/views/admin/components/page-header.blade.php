@if ($__env->yieldContent('hide-admin-header'))
    <!-- Empty -->

@else
    <div class="row align-items-center">
        <div class="page-header-wrapper">
            <div class="page-header-content" layout="row" layout-align="start center" layout-xs="column">
                <div class="page-header-title">
                    @if (!$__env->yieldContent('custom-nav-title'))
                        <h5>
                            @if(isset($current_url))
                                <a class="page-header-star" data-toggle="tooltip" title="Удалить из списка избранных" href="{{ admin_base_path('/favorites/'.$current_url->id.'/delete?redirect_page='.\Request::getRequestUri()) }}"><i class="fas fa-star"></i></a>
                            @else
                                <a class="page-header-star" data-toggle="tooltip" title="Добавить в избранное" href="{{ admin_base_path('/favorites/add?url='.\Request::getRequestUri().'&name='.$__env->yieldContent('title')) }}"><i class="far fa-star"></i></a>
                            @endif
                            @yield('title')
                        </h5>
                    @else
                        @yield('custom-nav-title')
                    @endif
                </div>
                <div flex></div>
                @if (!$__env->yieldContent('main-top-block'))

                    <div class="top-block-header">
                        <div class="heading-btn-group" layout="row">
                            @yield('top-block')
                        </div>
                    </div>
                @else
                    @yield('main-top-block')
                @endif
            </div>

            <div class="breadcrumb-line" layout="row" layout-align="start center">
                @yield('breadcrumbs')
                <div flex></div>
                <ul class="breadcrumb-elements" hide-xs layout="row" layout-align="center center">
                    <li class="dropdown">

                        <a href="#" md-ink-ripple class="dropdown-toggle breadcrumb-link__text" data-toggle="dropdown">
                            <i class="fas fa-cog"></i>
                            Настройки&nbsp;&nbsp;<i class="far fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a md-ink-ripple data-toggle="modal" href="#style_switcher_toggle"><i class="far fa-palette"></i>Настройки панели</a>
                            </li>
                            @can('admin_settings')
                                <li><a md-ink-ripple href="{{ admin_base_path('/account/users/'.auth()->id().'/edit') }}"><i class="far fa-users-cog"></i>Настройки профиля</a></li>
                                <li class="divider"></li>
                                @foreach(config('admin-settings.categories_breadcrumb') as $key => $value)
                                <li>
                                    <a md-ink-ripple href="{{ admin_base_path('/settings/?mid='.$key) }}"><i class="{{ $value['type'] ?? 'far' }} {{ $value['icon'] }}"></i>{{ $value['title'] }}</a>
                                </li>
                                @endforeach
                            @endcan
                        </ul>
                    </li>

                    @if ($__env->yieldContent('video__instruction'))
                        <li class="dropdown">
                            <a data-toggle="modal" data-target="#modalVideoScript" data-url=" @yield('video__instruction')" class="play-button__video video-instruction" md-no-ink title="{{ __('admin-layout.view_site') }}">
                                <i class="fad fa-film"></i>
                                Инструкция
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
