<style>
    #notifications-btn .md-button.md-icon-button {
        border-radius: 0;
    }

</style>

<md-toolbar class="top-toolbar md-hue-2 @yield('md-toolbar-class')">
    <div class="md-toolbar-tools">
        <md-button ng-click="toggleLeft()" class="show-button-toggle md-icon-button">
            <md-icon md-font-icon="fa fa-fw fa-bars"></md-icon>
        </md-button>
        <div class="dropdown" id="notifications-btn">
            @can('admin_tasks')
                <md-button data-toggle="dropdown" class="md-icon-button" href="#" title="{{ __('admin-layout.notification_new_order') }}">
                    <span ng-if="new_task > 0" class="small-badge badge-green small-badge-text" htmlcompile="new_task"></span>
                    <md-icon md-font-icon="fal fa-bell"></md-icon>
                </md-button>

                <md-button ng-if="new_postponed > 0" class="md-icon-button" href="{{ admin_base_path('/applications/postponed') }}" title="{{ __('admin-layout.list_postponed_order') }}">
                    <span class="small-badge bg-red small-badge-text" htmlcompile="new_postponed"></span>
                    <md-icon style="color: #b2ba0f" md-font-icon="fal fa-bell-exclamation"></md-icon>
                </md-button>
            @endcan

            @can('claims_payment')
                <md-button ng-if="new_payment_bonuses > 0" hide-xs class="md-icon-button" href="{{ admin_base_path('/applications/payment_bonuses') }}" title="{{ __('admin-layout.list_payout_bonuses') }}">
                    <span class="small-badge bg-red small-badge-text" htmlcompile="new_payment_bonuses"></span>
                    <md-icon md-font-icon="fal fa-receipt"></md-icon>
                </md-button>
            @endcan

            @can('admin_tasks')
                <md-button ng-if="reserve_request > 0" hide-xs class="md-icon-button" href="{{ admin_base_path('/basic/reserve_request') }}" title="{{ __('admin-layout.notification_request_reserve') }}">
                    <span class="small-badge bg-red small-badge-text" htmlcompile="reserve_request"></span>
                    <md-icon md-font-icon="fal fa-university"></md-icon>
                </md-button>
            @endcan

            @can('admin_verification_card')
                <md-button ng-if="verification_card > 0" hide-xs class="md-icon-button" href="{{ admin_base_path('/verifications/cards') }}" title="{{ __('admin-layout.verification_wallet') }}">
                    <span class="small-badge bg-red small-badge-text" htmlcompile="verification_card"></span>
                    <md-icon md-font-icon="fal fa-credit-card"></md-icon>
                </md-button>
            @endcan

            @can('admin_reviews')
                <md-button ng-if="reviews_count > 0" hide-xs class="md-icon-button" href="{{ admin_base_path('/tools/reviews') }}" title="{{ __('admin-layout.review_clients') }}">
                    <span class="small-badge bg-red small-badge-text" htmlcompile="reviews_count"></span>
                    <md-icon md-font-icon="fal fa-comments-alt"></md-icon>
                </md-button>
            @endcan

            <div class="live-popover dropdown-menu box-md float-left">
                <div class="popover-title popover-title-task" layout="row">
                    <div>{{ __('admin-layout.new_order') }}</div>
                </div>

                <div class="scrollable-content scrollable-slim-box" slim-scroll color="rgb(141, 160, 170)"  width="100%" size="6" height="400px">

                    <md-list class="live-md-list" style="padding: 0">
                        <md-list-item class="md-3-line" ng-repeat="item in live.data" ng-href="{{ admin_base_path('/tx/[[item.id]]') }}" ng-class="{'live-md-list-scam': item.is_scam == 1}">
                            <div class="md-list-item-text" layout="column">
                                <div class="id">№[[item.id]]</div>
                                <h3 class="name" style="color:#af4c88;" ng-if="item.status_int == 7">[[item.name | html]]</h3>
                                <h3 class="name" ng-if="item.status_int != 7">[[item.name | html]]</h3>
                                <p class="amount">[[item.amount | html]]</p>
                            </div>
                            <span class="md-secondary" layout="column">
                                                    <span style="line-height: 1.5; color: #999999;font-size: 12px;" ng-bind="item.created_at"></span>
                                                    <span style="line-height: 1.5; color: #721c24;font-size: 11px;text-align: right" ng-if="item.is_scam == 1">{{ __('admin-layout.scam') }}</span>
                                                </span>
                        </md-list-item>
                    </md-list>
                </div>
            </div>

        </div>
        <span flex></span>

        @if (!$__env->yieldContent('custom-toolbar-right'))
            <x-admin-breadcrumb-elements></x-admin-breadcrumb-elements>
        @else
            @yield('custom-toolbar-right')
        @endif
    </div>


</md-toolbar>
