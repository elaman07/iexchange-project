<ul class="breadcrumb-elements" hide-xs layout="row" layout-align="center center">
    @if(count($links) > 0)
        <li class="dropdown">
            <md-button href="#" md-ink-ripple class="dropdown-toggle md-icon-button" data-toggle="dropdown">
                <i class="fal fa-star font-size-16"></i>
            </md-button>

            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a md-ink-ripple href="{{ admin_base_path('/favorites/control') }}"><i class="far fa-wrench"></i> Управление</a>
                </li>
                <li class="divider"></li>
                @foreach($links as $link)
                    <li>
                        <a md-ink-ripple data-toggle="modal" href="{{ admin_base_path($link->link) }}">
                            <i class="fal fa-chevron-right"></i>  {{ $link->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    @endif

    <li>
        <md-button target="_blank" href="{{ config('app.url') }}" class="md-icon-button" id="notify-btn" title="{{ __('admin-layout.view_site') }}">
            <i class="fad fa-globe font-size-16" style="color: #303f9f"></i>
        </md-button>
    </li>
</ul>
