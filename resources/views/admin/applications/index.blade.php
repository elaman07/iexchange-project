@extends('admin.layouts.app')

@section('title','Заявки')

@section('breadcrumbs', Breadcrumbs::render('admin.applications'))

@section('top-block')

    <md-button ng-href="applications/status-log" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-history"></md-icon>
        <span>{{ __('Лог статусов') }}</span>
    </md-button>

    <md-button ng-href="applications/postponed" class="btn-float" layout="column">
        <md-icon md-font-icon="far fa-clock" style="color: #b2ba0f"></md-icon>
        <span style="color: #b2ba0f">{{ __('Отложенные') }}</span>
    </md-button>

    @if(iEXSetting('is_enabled_favorites_order') == 1)
        <md-button ng-href="applications/favorites" class="btn-float" layout="column">
            <md-icon md-font-icon="far fa-star" style="color: #ff8100"></md-icon>
            <span style="color: #ff8100">{{ __('Избранные') }}</span>
        </md-button>
    @endif

    @can('admin_order_trashed')
        @if(iEXSetting('is_enabled_delete_order') == 1)
            <md-button ng-href="applications/trashed" class="btn-float" layout="column">
                <md-icon md-font-icon="far fa-trash" style="color: #a0a0a0"></md-icon>
                <span style="color: #a0a0a0">{{ __('Удаленные') }}</span>
            </md-button>
        @endif
    @endcan

    @if(iEXSetting('is_enabled_spam_order') == 1)
        <md-button ng-href="applications/spam" class="btn-float" layout="column">
            <md-icon md-font-icon="far fa-thumbs" style="color: #9694ff"></md-icon>
            <span style="color: #9694ff">{{ __('Спам') }}</span>
        </md-button>
    @endif

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float  iex-settings-color" layout="column">
            <md-icon  md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan

@endsection

@section('no-block-content')


    <div class="alert alert-danger text-danger">
        {{ __('Для работы с заявками перейдите в новый раздел Панель оператора в левом меню') }}
    </div>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?action=settings">
                <input type="hidden" name="export_users" value="enable">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки заявок</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">Кол. заявок на странице</label>
                            <div class="col-md-6">
                                <input type="text" name="show_task_page" class="form-control" value="{{ iEXSetting('show_task_page') }}">
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Укажите макс. кол-во раз, когда заявки может смениться оператор</label>
                            <div class="col-md-6">
                                <input type="text" name="count_change_operator" class="form-control" value="{{ iEXSetting('count_change_operator', 0) }}">
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Отображать спам заявки</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="order_is_allow_spam" name="order_is_allow_spam" type="checkbox" value="1" @if(iEXSetting('order_is_allow_spam') == 1) checked @endif />
                                    <label for="order_is_allow_spam" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Отображаемые статусы заявок</label>
                            <div class="col-md-6" id="requisites-list">
                                {!! Form::select('order_priority[]', $allOrderStatuses, explode(',', iEXSetting('order_priority', null)),
                                ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '100%']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Определить новичка</label>
                            <div class="col-md-6" id="requisites-list">
                                {!! Form::select('order_identify_newbie[]', $typeDefineBeginner, explode(',', iEXSetting('order_identify_newbie', null)),
                                ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '100%']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Время актуальности заявки</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="max_time_task" data-width="250px">
                                    <option value="10"  @if(iEXSetting('max_time_task') == 10) selected @endif>10 секунд</option>
                                    <option value="300"  @if(iEXSetting('max_time_task') == 300) selected @endif>5 минут</option>
                                    <option value="600"  @if(iEXSetting('max_time_task') == 600) selected @endif>10 минут</option>
                                    <option value="900"  @if(iEXSetting('max_time_task') == 900) selected @endif>15 минут</option>
                                    <option value="1020"  @if(iEXSetting('max_time_task') == 1020) selected @endif>17 минут</option>
                                    <option value="1200"  @if(iEXSetting('max_time_task') == 1200) selected @endif>20 минут</option>
                                    <option value="1800"  @if(iEXSetting('max_time_task') == 1800) selected @endif>30 минут</option>
                                    <option value="1800"  @if(iEXSetting('max_time_task') == 1800) selected @endif>30 минут</option>
                                    <option value="3600"  @if(iEXSetting('max_time_task') == 3600) selected @endif>1 час</option>
                                    <option value="7200"  @if(iEXSetting('max_time_task') == 7200) selected @endif>2 часа</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Мин. сумма зачисления в резерв</label>
                            <div class="col-md-6" id="requisites-list">
                                <input type="text" class="form-control" style="text-align: center;" name="give_transfer_reserve" value="{{iEXSetting('give_transfer_reserve', 5000)}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить фильтры</label>
                            <div class="col-md-6" id="requisites-list">
                                <select name="is_allow_order_filter" class="form-control selectpicker">
                                    <option value="0" @if(iEXSetting('is_allow_order_filter') == 0) selected @endif>Да</option>
                                    <option value="1" @if(iEXSetting('is_allow_order_filter') == 1) selected @endif>Нет</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Использовать стандартную систему фильтров</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_order_all_filters" name="is_order_all_filters" type="checkbox" value="1" @if(iEXSetting('is_order_all_filters') == 1) checked @endif />
                                    <label for="is_order_all_filters" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить избранные заявки</label>
                            <div class="col-md-6" id="requisites-list">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_favorites_order" name="is_enabled_favorites_order" type="checkbox" value="1" @if(iEXSetting('is_enabled_favorites_order') == 1) checked @endif />
                                    <label for="is_enabled_favorites_order" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить спам заявки</label>
                            <div class="col-md-6" id="requisites-list">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_spam_order" name="is_enabled_spam_order" type="checkbox" value="1" @if(iEXSetting('is_enabled_spam_order') == 1) checked @endif />
                                    <label for="is_enabled_spam_order" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        @can('admin_order_trashed')
                            <div class="form-group">
                                <label class="control-label col-md-6">Включить удаленные заявки</label>
                                <div class="col-md-6" id="requisites-list">
                                    <div class="material-switch switch-input-1">
                                        <input id="is_enabled_delete_order" name="is_enabled_delete_order" type="checkbox" value="1" @if(iEXSetting('is_enabled_delete_order') == 1) checked @endif />
                                        <label for="is_enabled_delete_order" class="label-primary"></label>
                                    </div>
                                </div>
                            </div>
                        @endcan


                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить добавление комментарий к заявкам</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_add_comment_task" name="is_enabled_add_comment_task" type="checkbox" value="1" @if(iEXSetting('is_enabled_add_comment_task') == 1) checked @endif />
                                    <label for="is_enabled_add_comment_task" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить отображение последних комментарий к заявкам</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_last_comment_task" name="is_enabled_last_comment_task" type="checkbox" value="1" @if(iEXSetting('is_enabled_last_comment_task') == 1) checked @endif />
                                    <label for="is_enabled_last_comment_task" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">В заявках включить отображение популярных валют</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_currencies_fire_angular" name="is_currencies_fire_angular" type="checkbox" value="1" @if(iEXSetting('is_currencies_fire_angular') == 1) checked @endif />
                                    <label for="is_currencies_fire_angular" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

