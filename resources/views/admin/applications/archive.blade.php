@extends('admin.layouts.app')


@section('title','Архивированные заявки')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.archive'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title">
            <h2>Архивированные заявки
                <span class="pull-right font-size-13">
                    Всего заявок: {{ $count }}
                </span>
            </h2>
            <div class="clearfix"></div>
        </div>
        <br />
        <div class="x_content">
            <form style="width:100%;margin-right: 10px;" method="post" action="?form=update">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Направление обмена</th>
                        <th>Клиент</th>
                        <th>Статус</th>
                        <th>Оператор</th>
                        <th>Время исполнения</th>
                        <th>Дата создания</th>
                        <th>Информация</th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($orders) > 0)
                        @foreach($orders as $item)
                            <tr @if(isset($item->task_info) and $item->task_info->is_scam == 1 and $item->status != 4) style="background: #ffe7e9;"  @endif>
                                <th>
                                    <div class="checkbox" style="padding-top: 10px;">
                                        <input class="checkbox-input" id="checkbox{{$item->id}}"
                                               value="{{$item->id}}" type="checkbox" name="archives[]">
                                        <label for="checkbox{{$item->id}}"></label>
                                    </div>
                                </th>
                                <th>{{$item->id}}</th>
                                <td>
                                    <a href="{{config('admin.directory')}}/applications/details/{{$item->id}}?actions=arhive">
                                        <b>{{ direction_name($item, true) }}</b>
                                    </a>
                                    <div style="color: #666;">
                                        {{$item->give_price}} {{$item->direction_exchange->currency1->code_currency->name}}
                                        →
                                        {{$item->receiving_price}}  {{$item->direction_exchange->currency2->code_currency->name}}
                                    </div>
                                </td>
                                <td>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->user->id}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                    @if(!is_null($item->user->last_activity_at))
                                        <div style="color: #999;font-size: 11px;" data-toggle="tooltip" data-placement="top" title="Последняя активность">
                                            {{\Illuminate\Support\Carbon::parse($item->user->last_activity_at)->diffForHumans()}}
                                        </div>
                                    @endif
                                </td>
                                <td>
                                    @if($item->status == 3)
                                        @if($item->scans > 0)
                                            <span class="st-base-name st-handler-operator">{{ $item->operator->name }} принял</span>
                                        @else
                                            <span class="st-base-name {{$item->task_status->class}}">{{$item->task_status->name}}</span>
                                        @endif
                                    @elseif($item->status == 5)
                                        <span class="st-base-name st-delete" data-toggle="tooltip" data-placement="top" title="Заявка отклонена">
                                    {{config('transaction.status_reject.'.$item->category_reject)}}
                                </span>
                                    @else
                                        <span class="st-base-name {{$item->task_status->class}}">{{$item->task_status->name}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($item->id_manager))
                                        <div>
                                            <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_manager}}&sorting=id"><b>{{$item->manager->name}}</b></a>
                                        </div>
                                        <small>{{$item->manager->email}}</small>
                                    @else
                                        @if(isset($item->task_info) and $item->task_info->is_scam == 1)
                                            <div style="color: red;">
                                                Привлекался за<br /> мошенничество
                                            </div>
                                        @else
                                            <span style="color: #999">Нет данных</span>
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($item->started_at != null and $item->status == 4)
                                        {{ \Illuminate\Support\Carbon::parse($item->started_at)->diffForHumans(\Illuminate\Support\Carbon::parse($item->updated_at), true)  }}
                                    @else
                                        <span style="color: #999;">Не определено</span>
                                    @endif
                                </td>
                                <td>{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>

                                <td>
                                    @if(isset($item->task_info->device))
                                        @if($item->task_info->device == 'mobile')
                                            <i data-toggle="tooltip" data-placement="left" title="Мобильная версия" class="fa fa-fw fa-mobile"></i>
                                        @elseif($item->task_info->device == 'desktop')
                                            <i data-toggle="tooltip" data-placement="left" title="Компьютерная версия" class="fa fa-fw fa-desktop"></i>
                                        @elseif($item->task_info->device == 'tablet')
                                            <i data-toggle="tooltip" data-placement="left" title="Планшет" class="fa fa-fw fa-tablet"></i>
                                        @endif
                                    @endif

                                    @if(!empty($item->task_info->code_country))
                                        <img data-toggle="tooltip" data-placement="left" title="Страна" src="/images/country/{{ $item->task_info->code_country }}.svg">
                                    @endif

                                    @if(!empty($item->task_info->newbie))
                                        <i class="fa fa-user" data-toggle="tooltip" data-placement="left" title="Обмен делает новичок"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />Список пуст</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="0">Действия</option>
                                <option value="cancel_archiving">Вернуть</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $orders->links() !!}
    </div>
@endsection
