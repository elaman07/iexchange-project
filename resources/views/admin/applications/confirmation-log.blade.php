@extends('admin.layouts.app')

@section('title','Лог подтверждений')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.confirmation-log'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-order.filter') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ID Заявки</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_task', is_filter_search($filter, 'id_task'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">Применить фильтры</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">Очистить фильтры</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Лог подтверждений</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>ID заявки</th>
                    <th>Номер подтверждения</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <th>
                                <a href="{{config('admin.directory')}}/applications?action=filter&id={{$item->id_task}}">{{$item->id_task}}</a>
                            </th>
                            <td>{{ $item->confirmation }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />Список пуст</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
