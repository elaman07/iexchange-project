@extends('admin.layouts.app')

@section('title','Список частых клиентов')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.buyers'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title">
            <h2>Список частых клиентов
                <span class="pull-right font-size-13">
                    Всего клиентов: {{ $buyers->total() }}
                </span>
            </h2>
            <div class="clearfix"></div>
        </div>
        <br />
        <div class="x_content">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>Клиент</th>
                        <th>E-mail</th>
                        <th>Дата регистрации</th>
                        <th>Последний визит</th>
                        <th>Кол-во успешных<br/> сделок</th>
                        <th>Кол-во неудачных<br/> сделок</th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($buyers) > 0)
                        @foreach($buyers as $item)
                            <tr>
                                <td>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->user->id}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                </td>
                                <td>{{$item->user->email}}</td>
                                <td>{{\Illuminate\Support\Carbon::parse($item->user->created_at)->translatedFormat('d M Y H:i')}}</td>
                                <td>
                                    @if($item->user->last_activity_at != null)
                                        {{ \Illuminate\Support\Carbon::parse($item->user->last_activity_at)->diffForHumans() }}
                                    @else
                                        <span class="text-muted">Не определена</span>
                                    @endif
                                </td>
                                <td>{{$item->total_orders}}</td>
                                <td>{{$item->user->countFailedTransaction()}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />Список пуст</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $buyers->links() !!}
    </div>
@endsection