
@extends('admin.applications.tx.layout')

@section('title','Заявки на выплату')

@section('search')
    <form action="{{config('admin.directory')}}/tx" method="GET">
        <input type="hidden" name="action" value="filter" autocomplete="off">
        <div class="live-search" layout="row" layout-align="start center">
            <div>
                <input type="search" name="q" placeholder="Найти заявку..." ng-model="searchFish">
            </div>
        </div>
    </form>
@endsection
@section('toolbar')
    <md-toolbar class="top-toolbar">
        <div class="md-toolbar-tools">
            <md-button ng-click="toggleLeft()" class="md-icon-button">
                <md-icon>menu</md-icon>
            </md-button>
            <h2 flex md-truncate>Список заявок на выплату</h2>
            <span flex=""></span>
        </div>
    </md-toolbar>
@endsection

@section('content')
    <md-content style="height: 100%;">
        <div class="widget-application-layout" layout="row" layout-xs="column" layout-align="space-around stretch">
            <div flex="25" flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">Заявка создана</div>
                        <div class="widget-application-title widget-application-datetime">
                            {{\Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y, H:i')}}
                        </div>
                        <div class="widget-application-foot">
                            {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}
                        </div>
                    </div>
                </div>
            </div>

            <div flex="25"  flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">Номер выплаты</div>
                        <div class="widget-application-title widget-application-id">
                            {{$item->id}}
                        </div>
                    </div>
                </div>
            </div>

            <div flex="25"  flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">Статус выплаты</div>
                        <div class="widget-application-title">
                            @if($item->status == 0)
                                @if(is_null($item->verified_at) and iEXSetting('is_verified_payouts_bonus') == 1)
                                    <span class="st-error-color">Не подтвержден вывод</span>
                                @else
                                    <span class="st-handler-color">Ожидает обработки</span>
                                @endif
                            @elseif($item->status == 1)
                                <span class="st-success-color">Успешно выплачено</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div flex="25"  flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">IP Адрес</div>
                        <div class="widget-application-title widget-application-ip">
                            {{$item->ip}}
                        </div>
                        <div class="widget-application-foot">
                            <a data-toggle="modal" data-target="#info_ip">Информация по IP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="application-detail-data">
            <div>
                <div class="x_panel">
                    <div class="x_content">
                        <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                            <md-tab label="О Клиенте">
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        E-mail
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$item->user->email}}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Имя
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$item->user->name}}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Последний визит
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{ \Illuminate\Support\Carbon::parse($item->user->last_activity)->diffForHumans() }}
                                    </div>
                                </div>
                            </md-tab>
                        </md-tabs>
                    </div>
                </div>
            </div>
            <div layout="row" layout-xs="column" layout-align="space-around stretch">
                <div flex="50" flex-xs="100" class="padding-right">
                    <div class="x_panel">
                        <div class="x_content">
                            <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                                <md-tab label="Детали выплаты">
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Платежная система
                                        </div>

                                        <div class="application-detail-value col-md-6">
                                            {{$item->currency->payment->name}} {{$item->currency->code_currency->name}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Счет
                                        </div>

                                        <div class="application-detail-value col-md-6">
                                            {{$item->score}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Реферальные
                                        </div>

                                        <div class="application-detail-value col-md-6">
                                            {{ $item->balance_referral }}  {{$item->currency->code_currency->sign}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Бонусы за обмен
                                        </div>

                                        <div class="application-detail-value col-md-6">
                                            {{ $item->balance_reward }}  {{$item->currency->code_currency->sign}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Итоговая сумма выплаты
                                        </div>

                                        <div class="application-detail-value col-md-6" style="text-align: center;font-size: 14px;font-weight: 500">
                                            {{ $item->balance_referral + $item->balance_reward}}  {{$item->currency->code_currency->sign}}
                                        </div>
                                    </div>
                                </md-tab>
                            </md-tabs>
                        </div>
                    </div>
                </div>
                <div flex="50" flex-xs="100">
                    <div class="x_panel">
                        <div class="x_content">
                            <md-tabs class="application-detail-tabs" md-border-bottom>
                                <md-tab label="История частичных выплат">

                                    @if($histories->count() == 0)
                                        <div layout="column" layout-align="center center" style="    height: 100%;">
                                            <div class="not-data" style="color: #999999">
                                                Ничего не найдено
                                            </div>
                                        </div>
                                    @else
                                        <table class="table table-border-2">
                                            <thead>
                                            <tr>
                                                <th>Событие</th>
                                                <th>Выплачено</th>
                                                <th>Остаток</th>
                                                <th>Менеджер</th>
                                                <th>Изменено</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($histories as $history)
                                                <tr>
                                                    <td style="width: 200px">
                                                        {!! $history->text !!}
                                                    </td>
                                                    <td>
                                                        {{$history->amount}} {{$item->currency->code_currency->sign}}
                                                    </td>
                                                    <td>
                                                        {{$history->remainder}} {{$item->currency->code_currency->sign}}
                                                    </td>
                                                    <td>
                                                        @if(!empty($history->id_manager))
                                                            <div>
                                                                <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$history->id_manager}}&sorting=id"><b>{{$history->manager->name}}</b></a>
                                                            </div>
                                                            <small>{{$history->manager->email}}</small>
                                                        @else
                                                            <span style="color: #999">Нет данных</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$history->created_at->format('d m Y, H:i')}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </md-tab>
                            </md-tabs>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </md-content>

    @if($item->status == 0 and $item->ip != null)
        <div class="footer-detail">
            <div layout="row" layout-xs="column">
                @if(!is_null($item->verified_at) or iEXSetting('is_verified_payouts_bonus') == 0)
                    <md-button class="md-raised md-primary" type="button" data-toggle="modal" data-target="#parts" ng-disabled="isButtonDisabled">
                        Оплатить половину
                    </md-button>
                @endif
                <span flex=""></span>
                <div>
                    @if(!is_null($item->verified_at) or iEXSetting('is_verified_payouts_bonus') == 0)
                        <md-button ng-href="?actions=success" type="button" class="md-raised md-success-2">
                            Выплата произведена
                        </md-button>
                    @endif
                    <md-button ng-href="?actions=delete" class="md-raised md-danger">
                        Удалить
                    </md-button>
                </div>
            </div>
        </div>

        <form action="?actions=handler" method="POST" class="form-horizontal">
            @csrf
            <div class="modal fade" id="parts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header ui-dialog-titlebar">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <span class="ui-dialog-title" id="myModalLabel">Оплатить часть от общей суммы</span>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="message_success" class="col-sm-4 control-label">Тип баланса</label>
                                <div class="col-sm-8">
                                    <select class="selectpicker" name="type">
                                        <option value="">-- Не выбрано --</option>
                                        <option @if($item->balance_referral == 0) disabled @endif value="1">Реферальный баланс ({{$item->balance_referral}})</option>
                                        <option @if($item->balance_reward == 0) disabled @endif value="2">Бонусный баланс  ({{$item->balance_reward}})</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="message_success" class="col-sm-4 control-label">Уменьшить на сумму</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="balance">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="message_success" class="col-sm-4 control-label">Уведомлять клиента о частичной выплаты</label>
                                <div class="col-sm-8">
                                    <div class="material-switch switch-input-1">
                                        <input id="SwitchOptionPrimary1" name="notification" type="checkbox" value="1"/>
                                        <label for="SwitchOptionPrimary1" class="label-primary"></label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <br />
                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-primary">Отправить чек</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endif

@endsection