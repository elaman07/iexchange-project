
@if(count($adminFiltersHeaders) > 0)
    <div class="x_panel">
        <div class="x_title x_title_filter">

            <ul class="nav nav-tabs nav-tabs-solid" id="orderFilterTab">
                @foreach($adminFiltersHeaders as $key => $value)
                    <li class="{{ ($key == 0) ? 'active' : '' }}">
                        <a md-ink-ripple="'#000'" href="#filter-{{ $value['id'] }}" data-toggle="tab" aria-expanded="false">
                            {{ $value['name'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="x_content">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="lite_send_action" value="on">
                <div class="tab-content">
                    @foreach($adminFiltersHeaders as $header_key =>  $header_value)
                        <div style="padding: 0;" class="tab-pane {{ $header_key == 0 ? 'in active': '' }}" id="filter-{{ $header_value['id'] }}">
                            @foreach($header_value['admin_filters_user'] as $key => $value)
                                <div class="padding-lr-20">
                                    <div class="task-filter-content" layout="column" layout-xs="column" layout-padding="" layout-wrap>
                                        @foreach($value->options as $option)
                                            <div layout="row" layout-align="start center">
                                                <div flex="95">
                                                    @widget('filters.'.$option, ['request' => $filter])
                                                </div>
                                                <div flex=""></div>
                                                <a href="?actions=delete_filter_key&key_id={{ $option }}&id_filter={{ $header_value['id'] }}">
                                                    <i class="far fa-minus"></i>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach

                            <div class="filter-footer">
                                <div class="pull-left">
                                    <md-button type="submit" class="md-raised md-success">Найти</md-button>
                                    <md-button ng-href="?" class="md-raised">Очистить</md-button>
                                </div>
                                <div class="pull-right" ng-controller="CommonController  as ctrl">
                                    <md-menu md-position-mode="target-right target">
                                        <md-button aria-label="Open demo menu" class="md-icon-button md-raised" ng-click="ctrl.openMenu($mdMenu, $event)">
                                            <i class="fa fa-fw fa-cog"></i>
                                        </md-button>
                                        <md-menu-content width="2">
                                            <md-menu-item class="menu-items-list">
                                                <md-button href="{{ admin_base_path('/tx?actions=delete_filter&id='.$header_value['id']) }}">
                                                    <span md-menu-align-target> Удалить фильтр</span>
                                                </md-button>
                                            </md-menu-item>
                                        </md-menu-content>
                                    </md-menu>

                                    <md-button data-toggle="modal" href="#add_filter-{{ $header_value['id'] }}"   class="md-icon-button md-raised">
                                        <i class="fa fa-fw fa-plus"></i>
                                    </md-button>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </form>
        </div>
    </div>
@endif


<!-- Создаем фильтр -->
<div class="modal fade" id="create_filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" >
        <form class="form-horizontal" method="post" action="?actions=create_filter">
            <input type="hidden" name="export_users" value="enable">
            @csrf
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="ui-dialog-title" id="myModalLabel">Сохранить фильтр</span>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-md-6">Название фильтра</label>
                        <div class="col-md-6" id="requisites-list">
                            <input type="text" class="form-control" name="name_filter" value="Новый фильтр">
                        </div>
                    </div>

                    @can('admin_access_global_control')
                        <div class="form-group">
                            <label class="control-label col-md-6">Доступен всем</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="form-control selectpicker" name="is_common">
                                    <option value="0">Нет</option>
                                    <option value="1">Да</option>
                                </select>
                            </div>
                        </div>
                    @endcan
                </div>

                <div class="modal-footer">
                    <md-button type="submit" class="md-success md-raised">Сохранить</md-button>
                    <md-button type="button" data-dismiss="modal">Отмена</md-button>
                </div>
            </div>
        </form>
    </div>
</div>

@foreach($adminFiltersHeaders as $header_key =>  $header_value)
    @foreach($header_value['admin_filters_user'] as $key => $value)
        <div class="modal fade" id="add_filter-{{ $header_value['id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" >
                <form class="form-horizontal" method="post" action="?actions=admin_filter&id_filter={{ $header_value['id'] }}">
                    <input type="hidden" name="export_users" value="enable">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header ui-dialog-titlebar">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <span class="ui-dialog-title" id="myModalLabel">Настройка фильтров</span>
                        </div>
                        <div class="modal-body">
                            {!! Form::select('adminFilters[]', config('admin-filters.orders.fields'), $value['options'],
                            ['class' => 'selectpicker', 'multiple', 'data-actions-box' => 'true', 'data-live-search' => 'true', 'data-width' => '100%', 'data-size' => 10]) !!}
                        </div>

                        <div class="modal-footer">
                            <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
@endforeach



<style>
    .form-horizontal-custom .form-group{
        margin-bottom: 15px;
    }
</style>

<!-- Создаем фильтр -->
<div class="modal fade" id="settings_style" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" >
        <form class="form-horizontal form-horizontal-custom" method="post" action="?actions=custom_style">
            <input type="hidden" name="edit_style" value="enable">
            @csrf
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="ui-dialog-title" id="myModalLabel">Настройка страницы заявок</span>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-md-6">Кол. заявок на странице</label>
                        <div class="col-md-6">
                            <input type="text" name="show_task_page" class="form-control" value="{{ iEXSetting('show_task_page') }}">
                        </div>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="control-label col-md-6">Заявки для операторов</label>
                        <div class="col-md-6" id="requisites-list">
                            <select name="operator_panel_handler_orders" class="form-control selectpicker">
                                <option value="0" @if(iEXSetting('operator_panel_handler_orders') == 0) selected @endif>Нет ограничений</option>
                                <option value="1" @if(iEXSetting('operator_panel_handler_orders') == 1) selected @endif>1 оператор = 1 заявка</option>
                                <option value="2" @if(iEXSetting('operator_panel_handler_orders') == 2) selected @endif>По параметру "Укажите макс. кол-во раз, когда заявки может смениться оператор"</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Укажите макс. кол-во раз, когда заявки может смениться оператор</label>
                        <div class="col-md-6">
                            <input type="text" name="count_change_operator" class="form-control" value="{{ iEXSetting('count_change_operator', 0) }}">
                        </div>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="control-label col-md-6">Отображать спам заявки</label>
                        <div class="col-md-6">
                            <div class="material-switch switch-input-1">
                                <input id="order_is_allow_spam" name="order_is_allow_spam" type="checkbox" value="1" @if(iEXSetting('order_is_allow_spam') == 1) checked @endif />
                                <label for="order_is_allow_spam" class="label-primary"></label>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="control-label col-md-6">Определить новичка</label>
                        <div class="col-md-6" id="requisites-list">
                            {!! Form::select('order_identify_newbie[]', $typeDefineBeginner, explode(',', iEXSetting('order_identify_newbie', null)),
                            ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '100%']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Время актуальности заявки</label>
                        <div class="col-md-6" id="requisites-list">
                            <select class="selectpicker" name="max_time_task" data-width="250px">
                                <option value="10"  @if(iEXSetting('max_time_task') == 10) selected @endif>10 секунд</option>
                                <option value="300"  @if(iEXSetting('max_time_task') == 300) selected @endif>5 минут</option>
                                <option value="600"  @if(iEXSetting('max_time_task') == 600) selected @endif>10 минут</option>
                                <option value="900"  @if(iEXSetting('max_time_task') == 900) selected @endif>15 минут</option>
                                <option value="1020"  @if(iEXSetting('max_time_task') == 1020) selected @endif>17 минут</option>
                                <option value="1200"  @if(iEXSetting('max_time_task') == 1200) selected @endif>20 минут</option>
                                <option value="1800"  @if(iEXSetting('max_time_task') == 1800) selected @endif>30 минут</option>
                                <option value="1800"  @if(iEXSetting('max_time_task') == 1800) selected @endif>30 минут</option>
                                <option value="3600"  @if(iEXSetting('max_time_task') == 3600) selected @endif>1 час</option>
                                <option value="7200"  @if(iEXSetting('max_time_task') == 7200) selected @endif>2 часа</option>
                            </select>
                        </div>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="control-label col-md-6">Мин. сумма зачисления в резерв</label>
                        <div class="col-md-6" id="requisites-list">
                            <input type="text" class="form-control" style="text-align: center;" name="give_transfer_reserve" value="{{iEXSetting('give_transfer_reserve', 5000)}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Включить фильтры</label>
                        <div class="col-md-6" id="requisites-list">
                            <select name="is_allow_order_filter" class="form-control selectpicker">
                                <option value="0" @if(iEXSetting('is_allow_order_filter') == 0) selected @endif>Да</option>
                                <option value="1" @if(iEXSetting('is_allow_order_filter') == 1) selected @endif>Нет</option>
                            </select>
                        </div>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="control-label col-md-6">Продублировать верификацию карт в карточках</label>
                        <div class="col-md-6">
                            <div class="material-switch switch-input-1">
                                <input id="is_allow_panel_operation_verification" name="is_allow_panel_operation_verification" type="checkbox" value="1" @if(iEXSetting('is_allow_panel_operation_verification') == 1) checked @endif />
                                <label for="is_allow_panel_operation_verification" class="label-primary"></label>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group">
                        <label class="control-label col-md-6">Включить добавление комментарий к заявкам</label>
                        <div class="col-md-6">
                            <div class="material-switch switch-input-1">
                                <input id="is_enabled_add_comment_task" name="is_enabled_add_comment_task" type="checkbox" value="1" @if(iEXSetting('is_enabled_add_comment_task') == 1) checked @endif />
                                <label for="is_enabled_add_comment_task" class="label-primary"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Включить отображение последних комментарий к заявкам</label>
                        <div class="col-md-6">
                            <div class="material-switch switch-input-1">
                                <input id="is_enabled_last_comment_task" name="is_enabled_last_comment_task" type="checkbox" value="1" @if(iEXSetting('is_enabled_last_comment_task') == 1) checked @endif />
                                <label for="is_enabled_last_comment_task" class="label-primary"></label>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <div class="form-group">
                        <label class="control-label col-md-6">В заявках включить отображение популярных валют</label>
                        <div class="col-md-6">
                            <div class="material-switch switch-input-1">
                                <input id="is_currencies_fire_angular" name="is_currencies_fire_angular" type="checkbox" value="1" @if(iEXSetting('is_currencies_fire_angular') == 1) checked @endif />
                                <label for="is_currencies_fire_angular" class="label-primary"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">В заявках скрыть галочки "Не выплачивать партнерские и cashback бонусы"</label>
                        <div class="col-md-6">
                            <div class="material-switch switch-input-1">
                                <input id="is_currencies_order_hide_cashback_bonus" name="is_currencies_order_hide_cashback_bonus" type="checkbox" value="1" @if(iEXSetting('is_currencies_order_hide_cashback_bonus') == 1) checked @endif />
                                <label for="is_currencies_order_hide_cashback_bonus" class="label-primary"></label>
                            </div>
                        </div>
                    </div>


                    <br />
                    <hr />
                    <h4>Главная страница</h4>
                    <br />
                    <div class="form-group">
                        <label class="control-label col-md-6">Отключить блок "Отдает клиент"</label>
                        <div class="col-md-6" id="requisites-list">
                            <select class="form-control selectpicker" name="is_hide_block_give_service">
                                <option value="0" @if($style->is_hide_block_give_service == 0) selected @endif>Нет</option>
                                <option value="1" @if($style->is_hide_block_give_service == 1) selected @endif>Да</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Отключить блок "Переводит Сервис"</label>
                        <div class="col-md-6" id="requisites-list">
                            <select class="form-control selectpicker" name="is_hide_block_receive_service">
                                <option value="0" @if($style->is_hide_block_receive_service == 0) selected @endif>Нет</option>
                                <option value="1" @if($style->is_hide_block_receive_service == 1) selected @endif>Да</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Отключить блок "О клиенте"</label>
                        <div class="col-md-6" id="requisites-list">
                            <select class="form-control selectpicker" name="is_hide_block_client">
                                <option value="0" @if($style->is_hide_block_client == 0) selected @endif>Нет</option>
                                <option value="1" @if($style->is_hide_block_client == 1) selected @endif>Да</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-6">Кол-во блоков для главной страницы (в один ряд)</label>
                        <div class="col-md-6" id="requisites-list">
                            <select class="form-control selectpicker" name="count_block_home">
                                <option value="1" @if($style->count_block_home == 1) selected @endif>1</option>
                                <option value="2" @if($style->count_block_home == 2) selected @endif>2</option>
                                <option value="3" @if($style->count_block_home == 3) selected @endif>3</option>
                                <option value="4" @if($style->count_block_home == 4) selected @endif>4</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <md-button type="submit" class="md-success md-raised">Сохранить</md-button>
                    <md-button type="button" data-dismiss="modal">Отмена</md-button>
                </div>
            </div>
        </form>
    </div>
</div>
