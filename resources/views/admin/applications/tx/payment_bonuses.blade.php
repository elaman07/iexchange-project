@extends('admin.applications.tx.layout')

@section('title','Заявки на выплату')

@section('search')
    <form action="{{config('admin.directory')}}/tx" method="GET">
        <input type="hidden" name="action" value="filter" autocomplete="off">
        <div class="live-search" layout="row" layout-align="start center">
            <div>
                <input type="search" name="q" placeholder="Найти заявку..." ng-model="searchFish">
            </div>
        </div>
    </form>
@endsection
@section('toolbar')
    <md-toolbar class="top-toolbar">
        <div class="md-toolbar-tools">
            <md-button ng-click="toggleLeft()" class="md-icon-button">
                <md-icon>menu</md-icon>
            </md-button>
            <h2 flex md-truncate>Список заявок на выплату</h2>
            <span flex=""></span>
        </div>
    </md-toolbar>
@endsection

@section('content')

    <md-content style="height: 100%;">
        <div class="pagination-right">
            {!! $withdrawal->links() !!}
        </div>
        <table  class="table table-border-2">
            <thead>
            <tr>
                <th>ID</th>
                <th>Направление обмена</th>
                <th>Клиент</th>
                <th>Статус</th>
                <th>Выполнил</th>
                <th>Дата создания</th>
            </tr>

            </thead>
            <tbody>

            @foreach($withdrawal as $item)
                <tr @if($item->status == 0) class="important-error" @endif>
                    <td>{{$item->id}}</td>
                    <td>
                        <a href="{{config('admin.directory')}}/tx/bonuses/{{$item->id}}">
                            <b>{{config('app.name')}} → {{$item->currency->payment->name}} {{$item->currency->code_currency->name}}</b>
                        </a>
                    </td>

                    <td>
                        {{$item->user->name}}
                    </td>
                    <td>
                        @if($item->status == 0)
                            @if(is_null($item->verified_at) and iEXSetting('is_verified_payouts_bonus') == 1)
                                <span class="st-base-name st-error">Не подтвержден вывод</span>
                            @else
                                <span class="st-base-name st-handler">Ожидает обработки</span>
                            @endif
                        @else
                            <span class="st-base-name st-success">Выплачено</span>
                        @endif
                    </td>
                    <td>
                        @if(!empty($item->id_manager))
                            <div>
                                <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_manager}}&sorting=id"><b>{{$item->manager->name}}</b></a>
                            </div>
                            <small>{{$item->manager->email}}</small>
                        @else
                            <span style="color: #999">Нет данных</span>
                        @endif
                    </td>

                    <td>{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="pagination-right">
            {!! $withdrawal->links() !!}
        </div>
    </md-content>
@endsection