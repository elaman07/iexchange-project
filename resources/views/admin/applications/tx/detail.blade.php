@extends('admin.applications.tx.layout')

@section('ng-controller','ng-controller=TickerController')

@section('title', 'Заявка №'.$detail->id)

@section('search')
    <form action="{{ admin_base_path('/tx') }}" method="GET">
        <input type="hidden" name="send_action" value="on">
        <input type="hidden" name="action" value="filter" autocomplete="off">
        <div class="live-search" layout="row" layout-align="start center">
            <div>
                <input type="search" name="q" placeholder="Найти заявку..." ng-model="searchFish">
            </div>
        </div>
    </form>
@endsection


@section('content')
    <md-content style="height: 100%;background-color: transparent;">

        <div ng-if="isButton">
            <div class="progress-please-wait"></div>
            <div layout="column" layout-align="center center" class="progress-please-wait-scroll">
                <md-progress-circular class="md-hue-2" md-diameter="40px"></md-progress-circular>
            </div>
        </div>

        @if(isset($taskInfo) and $taskInfo->is_scam == 1)
            @if($taskInfo->is_local_scam == 1)
                <div class="isScam">
                    <b>Внимание:</b> Ранее клиент привлекался за мошенничество
                </div>
            @else
                <div class="isScam">
                    <b>Внимание:</b>  Данные клиента найдены в черном списке BestChange
                </div>
            @endif
        @endif

        <div class="clearfix"></div>

        @if($detail->is_autopay_off == 1 and !in_array($detail->status, [4,5]))
            <div class="alert alert-warning" style="margin-top: 10px; margin-bottom: 5px; text-align: center; line-height: 2; padding: 10px;">
                <div>Автовыплата отключена</div>
                <a href="{{ admin_base_path('/applications/autopayment-log') }}" style="font-size: 12px;">Открыть лог событий</a>
            </div>
        @endif

        @if($is_hold)
            <div class="two-column-toolbar two-column-toolbar-hold">
                <div class="item-detail item-detail-error">
                    Транзакция заморожена на 25 часов, осталось {{ $hold_timeout }}
                </div>
            </div>
        @endif

        @if($detail->double_withdrawal == 1 and $detail->status != 4)
            <div class="two-column-toolbar" style="background-color: #ffd7d7;">
                <div class="item-detail item-detail-error">
                    Попытка двойного снятия при автовыплате.<br /> Проверьте транзакцию, возможно средства уже отправлены клиенту.
                </div>
            </div>
        @endif

        @if(config('admin.is_demo_mode') == true)
            <div class="two-column-toolbar">
                <div class="item-detail item-detail-error">
                    Выполнение недоступно в демо режиме
                </div>
            </div>
        @endif

        <div class="application-detail-data">
            <div layout="row" layout-xs="column">

                <!-- Платежные данные -->
                <div flex="80" flex-xs="100">
                    <div class="toolbar-order-detail" layout="row" layout-xs="column">
                        <h2>
                            <md-button ng-click="toggleLeft()" class="show-button-toggle md-icon-button float-left" show-xs>
                                <i class="far fa-bars"></i>
                            </md-button>

                            Заявка <i class="far fa-horizontal-rule"></i> <span>№{{ $detail->id }}</span>
                            @if(iEXSetting('client_id_type_for_order') == 1 and $detail->public_id > 0)
                                 &nbsp;| {{ $detail->public_id }}
                                @if(iEXSetting('is_order_user_no_copydata') == 1)
                                    <div class="iex-order__copied iex-order__copied-head" ngclipboard data-clipboard-text="{{$detail->public_id}}" ngclipboard-success="onEventClipboardSuccess('Public ID');" hide-xs>
                                        <i class="fad fa-copy"></i>
                                    </div>
                                @endif
                            @endif
                        </h2>

                        <div class="status-item st-base-name {{ $detail->task_status->class }}">{{ $detail->task_status->name }}</div>
                        <div class="status-item-tooltip @if($detail->status == 8) status-item-tooltip-frozen @endif">

                            @if($detail->status == 8 and isset($detail->pending_order_status))
                                <i class="far fa-question-circle" data-toggle="tooltip" data-placement="bottom" data-title="Причина: {{ $detail->pending_order_status->name }}"></i>
                            @endif

                            @if($detail->status == 5 and isset($detail->tasks_rejection_status))
                                <i class="far fa-question-circle" data-toggle="tooltip" data-placement="bottom" data-title="Причина: {{ $detail->tasks_rejection_status->name }}"></i>
                            @elseif(isset($validityOrder) and in_array($validityOrder['level'], [3]))
                                @if($validityOrder['level'] == 3)
                                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="bottom" data-title="Внимание: Выполнив заявку вы будете в убытке на {{ $validityOrder['amount'] }} {{ $codeOut->name }}"></i>
                                @endif
                            @endif
                        </div>

                        @if($bonus > 0)
                        <div class="toolbar-bonus">
                            @if($bonus > 0)
                                Cashback: {{ $bonus }}
                            @endif
                        </div>
                        @endif

                        <div flex></div>
                        <div class="toolbar-nav" hide-xs></div>
                    </div>
                    <div class="order-detail-content-left">
                        <div class="widget-application-layout" layout="row" layout-xs="column" layout-sm="column" layout-align="space-around stretch" ng-cloak="">
                            <div class="widget-app-item" flex="33" flex-sm="100" flex-xs="100">
                                <div class="widget-application">
                                    <div class="widget-application-wrap">
                                        <div class="widget-application-head">Дата создания</div>
                                        <div class="widget-application-title widget-application-datetime">
                                            {{\Illuminate\Support\Carbon::parse($detail->created_at)->translatedFormat('d M Y, H:i')}}
                                        </div>
                                        <div class="widget-application-foot">
                                            {{ \Illuminate\Support\Carbon::parse($detail->created_at)->diffForHumans() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-app-item" flex="33"  flex-sm="100" flex-xs="100">
                                <div class="widget-application">
                                    <div class="widget-application-wrap">
                                        <div class="widget-application-head">ID Заявки</div>
                                        <div class="widget-application-title widget-application-id">
                                            {{ $detail->id }}
                                            @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                <div class="iex-order__copied iex-order__copied-head" ngclipboard data-clipboard-text="{{$detail->id}}" ngclipboard-success="onEventClipboardSuccess('ID');">
                                                    <i class="fad fa-copy"></i>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="widget-application-foot">
                                            @if(!empty($detail->public_id))
                                                Public ID: <a target="_blank" href="{{ url('/check/'.$detail->public_id) }}">{{  $detail->public_id }}</a>
                                            @elseif(isset($detail->transaction))
                                                <a target="_blank" href="{{ url('/check/'.$detail->transaction->transaction) }}">{{$detail->transaction->transaction}}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-app-item" flex="33" flex-xs="100" flex-sm="100">
                                <div class="widget-application">
                                    <div class="widget-application-wrap">

                                        @if(is_null($detail->deleted_at) and in_array($detail->status, config('order.status_pending')))
                                            <div class="widget-application-head">Изменить статус заявки</div>
                                            <div class="widget-application-title text-center" style="margin-top:10px">
                                                <a class="order-detail-restore-button" href="?actions=arhive&change_status=3">Восстановить заявку</a>
                                                @if($detail->status != 5)
                                                    <a class="order-detail-reject-button" href="?actions=arhive&change_status=5">Отклонить заявку</a>
                                                @endif
                                            </div>
                                        @else
                                            @if($detail->status == 8)
                                                <div class="widget-application-head">Изменить статус заявки</div>
                                                <div class="widget-application-title text-center" style="margin-top:10px">
                                                    <select class="selectpicker" onchange="if (this.value) window.location.href=this.value"  data-width="250px">
                                                        <option value="">--- Изменить статус заявки...</option>
                                                        <option value="?st=postponed&act=expectation&is_reserve=0" data-subtext="резервы снимаются сразу">Ожидает обработки</option>
                                                        <option value="?st=postponed&act=expectation&is_reserve=1" data-subtext="резервы снимаются после завершения">Ожидает обработки</option>
                                                        <option value="?st=postponed&act=reject">Отклонить заявку</option>
                                                    </select>
                                                </div>
                                            @else

                                                <div class="widget-application-head">Статус заявки</div>
                                                <div class="widget-application-title">
                                                    <div class="st-base-name st-basename-custom {{$detail->task_status->class}}">
                                                        {{$detail->task_status->name}}
                                                    </div>
                                                </div>
                                                <div class="widget-application-foot" hide-xs>
                                                    @if($detail->status != 4)
                                                        @if($detail->scans > 0)
                                                            @if(!$preview)
                                                                @if(iEXSetting('is_allow_transfer_operator'))
                                                                    <a ng-click="transferToAnother($event,'{{$detail->id}}')">{{\App\Models\User::find($detail->scans)->name}} работает с заявкой</a>
                                                                @else
                                                                    {{\App\Models\User::find($detail->scans)->name}} работает с заявкой
                                                                @endif
                                                            @else
                                                                <span>{{\App\Models\User::find($detail->scans)->name}} работает с заявкой</span>
                                                            @endif
                                                        @else
                                                            <span>Операторы не открывали заявку</span>
                                                        @endif
                                                    @else
                                                        @if(isset($detail->manager))
                                                            <span>{{$detail->manager->name}} выполнил заявку</span>
                                                        @endif
                                                    @endif


                                                    <div style="position: absolute;bottom: 10px;right: 15px;">
                                                        <a data-toggle="modal" data-target="#all_operator" href="">Операторы</a>
                                                    </div>

                                                    @if(isset($detail->main_operator))
                                                        <div style="position: absolute;top: 10px;right: 10px;text-align: center;">
                                                            <div>
                                                                <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$detail->main_operator->id}}&sorting=id"><b>{{$detail->main_operator->name}}</b></a>
                                                            </div>
                                                            <small>Главный оператор</small>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(isset($detail->direction_exchange->currency2->payment->explorer) and isset($detail->pay_transaction_hash) and !empty($detail->pay_transaction_hash->transaction_hash))
                            <div class="order-pay-blockchain__link">
                                <b>Ссылка на транзакцию</b><br />
                                <a target="_blank" href="{{ str_replace('{hash}', $detail->pay_transaction_hash->transaction_hash, $detail->direction_exchange->currency2->payment->explorer->link) }}">
                                    {{ str_replace('{hash}', $detail->pay_transaction_hash->transaction_hash, $detail->direction_exchange->currency2->payment->explorer->link) }}
                                </a>
                            </div>
                        @endif

                        <div layout="row" layout-xs="column" layout-sm="column" layout-align="space-around stretch">
                            <div flex="50" flex-xs="100" flex-sm="100" class="x_panel x_panel-tx margin-right-10">

                                <div class="order-detail-title" layout="row">
                                    <h2>Отдает клиент</h2>
                                    <span flex></span>
                                    <div class="order-detail-title-right">
                                        @if($checkPayment)
                                            <div class="two-column-toolbar">
                                                @if($walletInfoIn != null)
                                                    <div class="item-detail">
                                                        <div class="amount">
                                                            @if(in_array($detail->status, [3, 7]))
                                                                @if($walletInfoIn->amount < $detail->give_price and empty($detail->income_code))
                                                                    <a href="#" data-toggle="modal" data-target="#recount2">
                                                                        {{ iex_number_format($walletInfoIn->amount, 8) }} {{ $codeIn->name }}
                                                                    </a>
                                                                @else
                                                                    @if(empty($detail->income_code))
                                                                        {{ iex_number_format($walletInfoIn->amount, 8) }} {{ $codeIn->name }}
                                                                    @else
                                                                        {{ $walletInfoIn->amount }} {{ $walletInfoIn->currency }}
                                                                    @endif
                                                                @endif
                                                            @else
                                                                {{ iex_number_format($walletInfoIn->amount, 8) }} {{ $codeIn->name }}
                                                            @endif
                                                        </div>

                                                        @if($walletInfoIn->amount >= get_amount_credit_merchant_from_order($detail))
                                                            @if(empty($detail->income_code))
                                                                <div ng-if="confirmations != null" style="color: #1dc569">
                                                                    Кол. подтверждений: <span ng-bind="confirmations"></span>
                                                                </div>
                                                                <div ng-if="confirmations == null" style="color: #1dc569">Кол. подтверждений: {{ $walletInfoIn->confirmations }}</div>
                                                            @endif
                                                        @else
                                                            <div class="text-danger">
                                                                Оплата не в полном объеме: <br />
                                                                @if(empty($detail->income_code))
                                                                    <b>Нехватает:</b> {{number_format(get_amount_credit_merchant_from_order($detail) - $walletInfoIn->amount, 8,'.','')}}
                                                                    {{$codeIn->name}}
                                                                @else
                                                                    <b>Нехватает:</b> {{ get_amount_credit_merchant_from_order($detail) - $walletInfoIn->amount }}
                                                                    {{ $codeIn->name }}
                                                                @endif
                                                            </div>
                                                        @endif
                                                    </div>
                                                @else
                                                    @if(!$preview or $detail->status == 8)
                                                        <div class="check-payment-button">
                                                            <md-button class="md-warn md-raised check-payment-btn" ng-click="checkPayment('{{$paymentIn->name}}','{{$detail->id}}')">
                                                                Проверить оплату
                                                            </md-button>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="x_content application-detail-tabs">
                                    <div class="order-detail-items" layout="row"  layout="column" layout-wrap>
                                        <div class="order-detail-item" flex="50" flex-xs="100">
                                            <div class="order-detail-item-name">Валюта</div>
                                            <div class="order-detail-item-value">{{ $paymentIn->name }} {{ $codeIn->name }}</div>
                                        </div>

                                        <div class="order-detail-item" flex="50" flex-xs="100">

                                            <div>
                                                <div class="order-detail-item-name">Сумма</div>
                                                <div class="order-detail-item-value order-detail-item-value-amount">
                                                    @if(in_array($detail->status, [3, 7]))
                                                        @if($detail->skip_reserve == 0)
                                                            <a href="#" data-toggle="modal" data-target="#recount">{{ $display_in_price }} {{  $codeIn->name }}</a>
                                                        @else
                                                            {{ display_in_price_auto($detail, 'give_price') }} {{  $codeIn->name }}
                                                        @endif
                                                    @else
                                                        <a href="#" data-toggle="modal" data-target="#history_recalculation">{{ $display_in_price }} {{  $codeIn->name }}</a>
                                                    @endif

                                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ $display_in_price }}" ngclipboard-success="onEventClipboardSuccess('Сумма отдаю');">
                                                            <i class="fad fa-copy"></i>
                                                        </div>
                                                    @endif

                                                    <div class="clearfix"></div>
                                                    <a class="order-detail-card-info" href="#inPriceInfo" data-toggle="collapse" style="font-size: 11px; margin-top: 5px; margin-left: 0;">
                                                        Информация по сумме
                                                    </a>
                                                </div>
                                            </div>

                                            <div id="inPriceInfo" class="collapse">
                                                <br />
                                                <md-divider class="order-detail-divider"></md-divider>
                                                <br />
                                                <div class="order-detail-item__padding">
                                                    <div class="order-detail-item-name">Сумма (без комиссий)</div>
                                                    <div class="order-detail-item-value order-detail-item-value-amount">
                                                        {{ display_in_price_auto($detail, 'give_price_default') }}   {{ $codeIn->name }}
                                                    </div>
                                                </div>

                                                <div class="order-detail-item__padding">
                                                    <div class="order-detail-item-name">Доп. комиссия</div>
                                                    <div class="order-detail-item-value order-detail-item-value-amount">
                                                        {{ display_in_price_auto($detail, 'give_price_fee_comm', false) }}   {{ $codeIn->name }}
                                                    </div>
                                                </div>

                                                <div class="order-detail-item__padding">
                                                    <div class="order-detail-item-name">Сумма (с доп. комиссией)</div>
                                                    <div class="order-detail-item-value order-detail-item-value-amount">
                                                        {{ display_in_price_auto($detail, 'give_price_with_comm') }}   {{ $codeIn->name }}
                                                    </div>
                                                </div>

                                                <div class="order-detail-item__padding">
                                                    <div class="order-detail-item-name">Комиссия ПС</div>
                                                    <div class="order-detail-item-value order-detail-item-value-amount">
                                                        {{ display_in_price_auto($detail, 'give_price_fee_pay', false) }}   {{ $codeIn->name }}
                                                    </div>
                                                </div>

                                                <div class="order-detail-item__padding">
                                                    <div class="order-detail-item-name">Сумма (с комиссией доп. и ПС)</div>
                                                    <div class="order-detail-item-value order-detail-item-value-amount">
                                                        {{ display_in_price_auto($detail, 'give_price_with_comm_pay') }}   {{ $codeIn->name }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @if(mb_strlen($detail->from_shot) > 0)
                                            <div class="order-detail-item" flex="50" flex-xs="100">
                                                <div class="order-detail-item-name">Со счета</div>
                                                <div class="order-detail-item-value">
                                                    {{ $detail->from_shot }}
                                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ remove_all_spaces($detail->from_shot) }}" ngclipboard-success="onEventClipboardSuccess('Со счета');">
                                                            <i class="fad fa-copy"></i>
                                                        </div>
                                                    @endif

                                                    @if($isVerifiedCard)
                                                        <div class="display-initial order-detail-is-verified" data-toggle="tooltip" data-title="Этот номер счета верифицирован">
                                                            <i class="fas fa-badge-check"></i>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif

                                        @if(isset($detail->direction_exchange->currency1->payment->explorer) and isset($detail->merchant_transaction_hash) and !empty($detail->merchant_transaction_hash->transaction_hash))
                                            <div class="order-detail-item" flex="50" flex-xs="100">
                                                <div class="order-detail-item-name">Ссылка на входящую транзакцию</div>
                                                <div class="order-detail-item-value">
                                                    <a target="_blank" href="{{ str_replace('{hash}', $detail->merchant_transaction_hash->transaction_hash, $detail->direction_exchange->currency1->payment->explorer->link) }}">Перейдите в Blockchain</a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <!-- Доп. поля (Отдаю) -->
                                    @if(count($tasks_fields['currency_in']) > 0)
                                        <md-divider class="order-detail-divider"></md-divider>

                                        <div class="order-detail-items" layout="row" layout-xs="column" layout-wrap>
                                            @foreach($tasks_fields['currency_in'] as $item)
                                                <div class="order-detail-item" flex="50"  flex-xs="100">
                                                    <div class="order-detail-item-name">{{ $item->field_name }}</div>
                                                    <div class="order-detail-item-value">
                                                        @if(!is_null($item->field_value))
                                                            {{ $item->field_value }}
                                                        @else
                                                            <span class="text-danger">Не указано</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif

                                    <!-- Прикрепленные чеки -->
                                    @if($detail->tasks_check_images)
                                        <a href="{{ url('/images/checks/'.$detail->tasks_check_images->image) }}" target="_blank" class="check-image__item">
                                            <img src="/images/checks/{{ $detail->tasks_check_images->image }}"/>
                                        </a>
                                    @endif

                                </div>
                            </div>
                            <div flex="50" flex-xs="100" flex-sm="100" class="x_panel x_panel-tx margin-right-10">
                                <div class="order-detail-title" layout="row" layout-align="start center">
                                    <h2>Переводит сервис</h2>
                                    <span flex></span>
                                    <div class="order-detail-title-right order-detail-items-footer">
                                        @if($preview == false)
                                            <a data-toggle="modal" href="#edit_data" class="edit-pencil-link">
                                                <i class="fad fa-pencil-alt"></i>
                                                <span hide-xs>Изменить</span>
                                            </a>
                                        @endif

                                        @if(iEXSetting('telegram_to_shot_notification'))
                                            <a hide-xs href="?actions=send_to_shot_telegram" class="order-detail-telegram" data-toggle="tooltip" data-title="Отправить счет получателя в Telegram">
                                                <i class="fab fa-telegram-plane"></i>
                                            </a>
                                        @endif
                                    </div>
                                </div>

                                <div class="x_content application-detail-tabs">
                                    <div class="order-detail-column">
                                        @if($taskInfo->is_aml_high_risk == 1 and isset($detail->direction_exchange->currency1->aml_service_model))
                                            <div class="big-alert__aml-notify">
                                                ВНИМАНИЕ!!! У ТРАНЗАКЦИИ ВЫСОКАЯ СТЕПЕНЬ ОПАСНОСТИ ПО РЕЗУЛЬТАТАМ AML
                                                <div class="big-alert__aml-notify-num">{{  $taskInfo->aml_result_value }}</div>
                                            </div>
                                        @endif

                                        @if($uniqueToShot == false)
                                            <div class="order-detail-error">
                                                <i class="fa fa-fw fa-warning"></i>
                                                @if(isset($detail->edit_data_manager))
                                                    Номер счета был изменен оператором <a href="{{ admin_base_path('/account/users/?id='.$detail->edit_data_manager->id) }}">
                                                        <b>{{ $detail->edit_data_manager->name }}</b>
                                                    </a>
                                                @else
                                                    Номер счета был изменен
                                                @endif
                                            </div>
                                        @endif

                                        <div class="order-detail-items" layout="row" layout-xs="column" layout-wrap>
                                            <div class="order-detail-item" flex="50" flex-xs="100">
                                                <div class="order-detail-item-name">Валюта</div>
                                                <div class="order-detail-item-value">{{ $paymentOut->name }} {{ $codeOut->name }}  {{ !empty($detail->task_info->network_name) ? $detail->task_info->network_name : '' }}</div>
                                            </div>

                                            <div class="order-detail-item" flex="50" flex-xs="100">
                                                <div>
                                                    <div class="order-detail-item-name">Сумма</div>
                                                    <div class="order-detail-item-value order-detail-item-value-amount">
                                                        {{ $display_out_price }}  {{ $codeOut->name }}
                                                        @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                            <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ $display_out_price }}" ngclipboard-success="onEventClipboardSuccess('Сумма получаю');">
                                                                <i class="fad fa-copy"></i>
                                                            </div>
                                                        @endif

                                                        <div style="font-size: 10px;margin-top: 5px;" class="text-muted">Разница: {{ ($percent_diff > 0 ? $percent_diff : 0) }}%
                                                            @if($percent_diff > 0)
                                                                <span class="text-danger">({{ $new_out_amount }} {{  $codeOut->name }}) </span>
                                                            @endif
                                                        </div>

                                                        <a class="order-detail-card-info" href="#outPriceInfo" data-toggle="collapse" style="font-size: 11px; margin-top: 5px; margin-left: 0;">
                                                            Информация по сумме
                                                        </a>
                                                    </div>
                                                </div>

                                                <div id="outPriceInfo" class="collapse">
                                                    <br />
                                                    <md-divider class="order-detail-divider"></md-divider>
                                                    <br />
                                                    <div class="order-detail-item__padding">
                                                        <div class="order-detail-item-name">Сумма (без комиссий)</div>
                                                        <div class="order-detail-item-value order-detail-item-value-amount">
                                                            {{ display_out_price_auto($detail, 'receiving_price_default') }}   {{ $codeOut->name }}
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item__padding">
                                                        <div class="order-detail-item-name">Доп. комиссия</div>
                                                        <div class="order-detail-item-value order-detail-item-value-amount">
                                                            {{ display_out_price_auto($detail, 'receiving_price_fee_comm', false) }}   {{ $codeOut->name }}
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item__padding">
                                                        <div class="order-detail-item-name">Сумма (с доп. комиссией)</div>
                                                        <div class="order-detail-item-value order-detail-item-value-amount">
                                                            {{ display_out_price_auto($detail, 'receiving_price_with_comm') }}   {{ $codeOut->name }}
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item__padding">
                                                        <div class="order-detail-item-name">Комиссия ПС</div>
                                                        <div class="order-detail-item-value order-detail-item-value-amount">
                                                            {{ display_out_price_auto($detail, 'receiving_price_fee_pay', false) }}   {{ $codeOut->name }}
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item__padding">
                                                        <div class="order-detail-item-name">Сумма (с комиссией доп. и ПС)</div>
                                                        <div class="order-detail-item-value order-detail-item-value-amount">
                                                            {{ display_out_price_auto($detail, 'receiving_price_with_comm_pay') }}   {{ $codeOut->name }}
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            @if(mb_strlen($detail->to_shot) > 0)
                                                <div class="order-detail-item" flex="50" flex-xs="100">
                                                    <div class="order-detail-item-name">На счет</div>
                                                    <div class="order-detail-item-value" layout="row">
                                                        <div>
                                                            {{ $detail->to_shot }}
                                                            @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                                <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ remove_all_spaces($detail->to_shot) }}" ngclipboard-success="onEventClipboardSuccess('На счет');">
                                                                    <i class="fad fa-copy"></i>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        @if(!empty($cardDetails))
                                                            <a class="order-detail-card-info" href="#cardInfo" data-toggle="collapse" style="font-size: 11px; margin-top: 5px; margin-left: 0;">
                                                                Информация о карте
                                                            </a>
                                                        @endif
                                                    </div>
{{--                                                    <!-- Проверка, включен ли модуль GetBlock -->--}}
{{--                                                    @if(\Module::find('GetBlockBot')->isEnabled() and $detail->task_info->aml_address_risk_out > 0)--}}
{{--                                                        <div @if((float)$detail->task_info->aml_address_risk_out >= \GetBlockBotFacade::getLevelRisk()) class="text-danger" @else class="text-success" @endif style="margin-top: 5px;font-size: 11px;font-weight: 500;">--}}
{{--                                                            @if((float)$detail->task_info->aml_address_risk_out >= \GetBlockBotFacade::getLevelRisk()) <i class="far fa-exclamation-triangle"></i> @endif--}}
{{--                                                            GetBlock Risk:--}}
{{--                                                            {{ $detail->task_info->aml_address_risk_out }}%--}}
{{--                                                        </div>--}}
{{--                                                    @endif--}}
                                                </div>
                                            @endif
                                        </div>

                                        <!-- Доп. поля (Отдаю) -->
                                        @if(count($tasks_fields['currency_out']) > 0)
                                            <md-divider class="order-detail-divider"></md-divider>

                                            <div class="order-detail-items" layout="row" layout-xs="column" layout-wrap>
                                                @foreach($tasks_fields['currency_out'] as $item)
                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">{{ $item->field_name }}</div>
                                                        <div class="order-detail-item-value">
                                                            @if(!is_null($item->field_value))
                                                                {{ $item->field_value }}
                                                                @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                                    <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$item->field_value}}" ngclipboard-success="onEventClipboardSuccess('{{ $item->field_name }}');">
                                                                        <i class="fad fa-copy"></i>
                                                                    </div>
                                                                @endif
                                                            @else
                                                                <span class="text-danger">Не указано</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif


                                        <!-- Информация о карте -->
                                        @if(!empty($cardDetails))
                                            <div id="cardInfo" class="collapse">
                                                <md-divider class="order-detail-divider"></md-divider>
                                                <div class="order-detail-items" layout="row" layout-xs="column" layout-wrap>

                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">Номер карты</div>
                                                        <div class="order-detail-item-value">
                                                            {{ $cardDetails->card_number }}
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">Система</div>
                                                        <div class="order-detail-item-value">
                                                            @if(!empty($cardDetails->payment_system))
                                                                {{ $cardDetails->payment_system }}
                                                            @else
                                                                <small class="text-danget">Не определен</small>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">  Тип карты</div>
                                                        <div class="order-detail-item-value">
                                                            @if(!empty($cardDetails->type_card))
                                                                {{ $cardDetails->type_card }}
                                                            @else
                                                                <small class="text-danget">Не определен</small>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">Бренд карты</div>
                                                        <div class="order-detail-item-value">
                                                            @if(!empty($cardDetails->brand_card))
                                                                {{ $cardDetails->brand_card }}
                                                            @else
                                                                <small class="text-danget">Не определен</small>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">Страна</div>
                                                        <div class="order-detail-item-value">
                                                            @if(!empty($cardDetails->country_name))
                                                                {{ $cardDetails->country_name }} ({{ $cardDetails->country_currency }})
                                                            @else
                                                                <small class="text-danget">Не определен</small>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="order-detail-item" flex="50" flex-xs="100">
                                                        <div class="order-detail-item-name">Название банка</div>
                                                        <div class="order-detail-item-value">
                                                            @if(!empty($cardDetails->bank_name))
                                                                <div>{{ $cardDetails->bank_name }}</div>
                                                                @if($cardDetails->bank_url)
                                                                    <small><a target="_blank" href="{{ $cardDetails->bank_url }}">{{ $cardDetails->bank_url }}</a></small>
                                                                    <br />
                                                                @endif

                                                                @if($cardDetails->bank_phone)
                                                                    <small>Контактный номер: {{ $cardDetails->bank_phone }}</small>
                                                                @endif
                                                            @else
                                                                <small class="text-danget">Не определен</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- Для моб. отключаем -->
                        <div class="footer-detail">
                            <div layout="row" layout-xs="column">
                                @if(!$preview and $detail->status != 4)
                                    @if(isset($validityOrder) and $validityOrder['level'] == 3)
                                        {{ 'ss' }}
                                    @else
                                        @if($detail->skip_reserve == 0)
                                            <md-button type="submit" class="md-raised md-frozen" data-toggle="modal" href="#button-frozen" ng-disabled="isButtonDisabled">Отложить</md-button>
                                        @endif
                                    @endif
                                @endif
                                <span flex></span>
                                <div>
                                    @if($preview == false and $detail->status != 4)
                                        <div layout="row" layout-xs="column" layout-align="start center" layout-align-xs="space-between stretch">
                                            @if(isset($validityOrder) and $validityOrder['level'] == 3)
                                                <span class="text-danger">Заявку нельзя выполнить</span>
                                            @else
                                                @if(isset($payOut))
                                                    @if(in_array($payOut->gateway->alias, ['binance']))

                                                        @if($hasBalance == 1)
                                                            <div class="text-danger">
                                                                Недостаточно средств для выплаты. <br />
                                                                На счету: <b>{{ $apiBalance }} {{ $codeOut->name }}</b>
                                                            </div>
                                                        @endif
                                                        <md-button type="submit" class="md-raised md-success-3" data-toggle="modal" href="#button-success-autopay" ng-disabled="isButtonDisabled">
                                                            @if($payOut->exchange_buy == 0)
                                                                Докупить и выплатить
                                                            @elseif($payOut->exchange_buy == 2)
                                                                Докупить, и не выводить
                                                            @elseif($payOut->exchange_buy == 3)
                                                                Рыночная покупка и выплата
                                                            @else
                                                                Оплатить и закрыть
                                                            @endif
                                                        </md-button>
                                                    @else
                                                        @if($hasBalance == 1)
                                                            <div class="text-danger">
                                                                Недостаточно средств для выплаты. <br />
                                                                На счету: <b>{{ $apiBalance }} {{ $codeOut->name }}</b>
                                                            </div>
                                                        @elseif($hasBalance == 0)
                                                            <span class="text-muted">На балансе: {{ $apiBalance }} {{ $codeOut->name }}</span>
                                                            <md-button type="submit" class="md-raised md-success-3" data-toggle="modal" href="#button-success-autopay" ng-disabled="isButtonDisabled">Оплатить и завершить</md-button>
                                                        @endif
                                                    @endif
                                                @endif

                                                @if($disable_manual_button == 0)
                                                    <md-button type="submit" class="md-raised md-success-2" data-toggle="modal" href="#button-success-payment" ng-disabled="isButtonDisabled">Заявка выполнена</md-button>
                                                @endif
                                            @endif

                                            <md-button type="submit" class="md-raised md-danger" data-toggle="modal" href="#button-reject" ng-disabled="isButtonDisabled">Отклонить</md-button>
                                        </div>
                                    @else
                                        <md-button href="{{config('admin.directory')}}/tx" class="md-raised">К заявкам</md-button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div flex="30" flex-xs="100">
                    <div class="toolbar-order-detail-right" layout="row" layout-align="end center">
                        <div>
                            @if(!$preview and $detail->status != 4)
                                <md-button class="md-raised md-primary" type="button" data-toggle="modal" data-target="#sendCheck" ng-disabled="isButtonDisabled">
                                    @if($detail->check_status == 0)
                                        Отправить чек
                                    @else
                                        Повторно отправить чек
                                    @endif
                                </md-button>
                            @endif
                        </div>
                    </div>
                    <div class="order-detail-content-right">

                        @if($detail->status == 4 and isset($reserves_log))
                            <div class="x_panel x_panel-tx">
                                <div class="order-detail-title">
                                    <h2>Корректировка резерва</h2>
                                </div>
                                <div class="x_content x_content-custom">
                                    <div class="order-detail-items" layout="row">
                                        @foreach($reserves_log as $item)
                                            @if($item->type == 'minus')
                                                <div class="order-detail-item order-reserve-history-minus" flex="50">
                                                    <div class="order-detail-item-layout" layout="row" layout-align="start center">
                                                        <div class="order-detail-item-icon">
                                                            <i class="fad fa-level-down"></i>
                                                        </div>
                                                        <div class="order-detail-item-data">
                                                            <div class="order-detail-item-name">
                                                                <span>{{ $paymentOut->name }} {{ $codeOut->name }}</span>
                                                            </div>
                                                            <div class="order-detail-item-value">
                                                                <span>-{{ $item->summa }} {{ $codeOut->name }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="order-detail-item order-reserve-history-plus" flex="50">
                                                    <div class="order-detail-item-layout" layout="row" layout-align="start center">
                                                        <div class="order-detail-item-icon">
                                                            <i class="fad fa-level-up"></i>
                                                        </div>
                                                        <div class="order-detail-item-data">
                                                            <div class="order-detail-item-name">
                                                                <span>{{ $paymentIn->name }} {{ $codeIn->name }}</span>
                                                            </div>
                                                            <div class="order-detail-item-value">
                                                                <span>+{{ $item->summa }} {{ $codeIn->name }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        @endif

                        <div class="x_panel x_panel-tx">
                            <div class="order-detail-title">
                                <h2>Детали транзакции</h2>
                            </div>
                            <div class="x_content x_content-custom">

                                @if((int)iEXSetting('is_enabled_module_order_step') == 1)
                                    <div class="application-detail-item row">

                                        <div class="order-detail-item">
                                            <div class="order-detail-item-name">{{ __('Этап') }}
                                                <a data-toggle="modal" href="#steps_logs" class="pull-right">{{ __('Историй') }}: {{ isset($detail->steps_logs) ? count($detail->steps_logs) : 0 }}</a>
                                            </div>
                                            <div class="order-detail-item-value">
                                                <select class="form-control selectpicker" name="order_step" onchange="if (this.value) window.location.href='?order_step='+this.value">
                                                    <option value="0">-- {{ __('Не выбрано') }} --</option>
                                                    @foreach($order_steps as $key => $value)
                                                        <option value="{{ $key }}" @if($detail->id_order_step == $key) selected @endif>{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <md-divider></md-divider>
                                @endif

                                @if(!is_array($receive_wallet) and \Str::lower($receive_wallet) == '[request_payment]')
                                    <div class="application-detail-item row">

                                        <div class="order-detail-item">
                                            <div class="order-detail-item-name font-weight-500">{{ __('Добавление реквизита') }}</div>
                                            <div class="order-detail-item-value">



                                                <form method="post" action="?actions=add_requisites_payment">
                                                    <div class="form-group">

                                                        <select id="id_request_payments" data-live-search="true" name="id_request_payments" class="form-control selectpicker" data-size="10">
                                                            <option value="0">-- {{ __('Ничего не выбрано') }} --</option>
                                                            <option value="add">-- {{ __('Добавить реквизит вручную') }} --</option>
                                                            @foreach($transit_requisites as $value)
                                                                <option value="{{ $value->id }}" data-subtext="(Использований: {{ $value->view }})">{{$value->account}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="show_detail_request_payment">
                                                        <div class="form-group">
                                                            <div class="clearfix"></div>
                                                            <input type="text" class="form-control" name="requisites_field" placeholder="Укажите реквизит">
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="clearfix"></div>
                                                            <textarea class="form-control" rows="5" name="requisites_description" placeholder="Добавьте описание"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="text-right">
                                                        <button type="submit" class="btn btn-default">Привязать</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <md-divider></md-divider>
                                @endif



                                @if($detail->status == 4 and !is_null($detail->started_at))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">Время выполнения заявки</div>

                                        <div class="application-detail-value col-md-7 text-right">{{ $leadTime }}</div>
                                    </div>

                                    <md-divider></md-divider>
                                @endif

                                    @if(isset($detail->task_info->cities) and \Module::find('Cities')->isEnabled())
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">Город</div>

                                            <div class="application-detail-value col-md-7 text-right">
                                                <div style="font-weight: 500">{{  $detail->task_info->cities->designation_xml }} ({{ $detail->task_info->country_name }} - {{ $detail->task_info->city_name }})</div>
                                            </div>
                                        </div>
                                    @endif


                                    <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">Курс</div>

                                    <div class="application-detail-value col-md-7 text-right">
                                        <div style="font-weight: 500">Фиксированный: {{$detail->course_display ?? 'Не определена'}}</div>
                                        <small class="text-muted">Актуальный: {{ $newCourse['curs'] }}</small>
                                    </div>
                                </div>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Код заявки
                                        </div>

                                        <div class="application-detail-value col-md-7 text-right">{!! $detail->unique_security_code ?? 'не найден' !!}</div>
                                    </div>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Направление
                                        </div>

                                        <div class="application-detail-value col-md-7 text-right">
                                            <a href="{{ admin_base_path('/basic/direction_exchange/'.$detail->id_direction_exchange.'/edit') }}">
                                                <b>{{ $detail->direction_exchange->tech_name }}</b>
                                            </a>
                                        </div>
                                    </div>

                                <!-- Дата пересчета -->
                                @if(!is_null($lastRecalcDate))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Дата посл. пересчета
                                        </div>

                                        <div class="application-detail-value col-md-7 text-right">
                                            {{ \Illuminate\Support\Carbon::parse($lastRecalcDate)->translatedFormat('j F Y, H:i') }}
                                        </div>
                                    </div>
                                @endif

                                @if(is_array($receive_wallet))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            @if(isset($account_number_field))
                                                {{ $account_number_field }}
                                            @else
                                                Кошелек
                                            @endif
                                        </div>

                                        <div class="application-detail-value col-md-7 text-right">
                                            {{ $receive_wallet['address'] }}
                                        </div>
                                    </div>
                                @else
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            @if(isset($account_number_field))
                                                {{ $account_number_field }}
                                            @else
                                                Кошелек
                                            @endif
                                        </div>
                                        <div class="application-detail-value col-md-7 text-right">
                                            {{ $receive_wallet }}
                                        </div>
                                    </div>
                                @endif

                            <!-- Приватные ключи доступны только разрабочикам -->
                                @role('Разработчик')
                                @if(isset($privateKeyAddress) and config('admin.is_demo_mode') == false)
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Приватный ключ
                                        </div>

                                        <div class="application-detail-value col-md-7 text-right">
                                            {{ $privateKeyAddress }}
                                        </div>
                                    </div>
                                @endif
                                @endrole

                                    @if($detail->is_bot == 1)
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                Автовыплата
                                            </div>

                                            <div class="application-detail-value col-md-7 text-right">
                                                <a href="?actions=switch_to_manual_mode">Перевести в ручной режим</a>
                                            </div>
                                        </div>
                                    @endif

                                    @if(count($tasks_fields['requisites_info_field']) > 0)
                                        @foreach($tasks_fields['requisites_info_field'] as $value)
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-5">{{ $value->key_name }}</div>
                                                <div class="application-detail-value col-md-7 text-right">{{ $value->value_name }}</div>
                                            </div>
                                        @endforeach
                                    @endif

                                    @if(count($tasks_fields['many']) > 0)
                                        @foreach($tasks_fields['many'] as $value)
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-5">{{ $value->field_name }}</div>
                                                <div class="application-detail-value col-md-7 text-right">{{ $value->field_value }}</div>
                                            </div>
                                        @endforeach
                                    @endif

                                    @if(!empty($taskInfo->num_transaction))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Номер перевода
                                        </div>
                                        <div class="application-detail-value col-md-7 text-right">
                                            {{ $taskInfo->num_transaction }}
                                        </div>
                                    </div>
                                @endif

                                @if(!empty($taskInfo->note_tx))
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Примечание к транзакции
                                        </div>

                                        <div class="application-detail-value col-md-7 text-right">
                                            {{ $taskInfo->note_tx }}
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="clearfix"></div>

                            @include('admin.applications.tx.component.aml')

                            <div class="x_panel x_panel-tx">
                            <div class="order-detail-title order-detail-title-collapse">
                                <a class="collapsed md-icon-button" data-toggle="collapse" href="#userInfo" layout="row">
                                    <h2>
                                        <i class="fad fa-user-alt"></i>&nbsp;&nbsp;Информация о клиенте
                                    </h2>
                                    <div flex=""></div>
                                    <div class="heading-right-button">
                                        <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                                        <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                                    </div>
                                </a>
                                <div class="clearfix"></div>
                            </div>
                            <div id="userInfo" class="x_content x_content-custom collapse">
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        ID
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        <a href="{{ admin_base_path('/account/users/?id='.$detail->user->id) }}">{{$detail->user->id}}</a>
                                        @if(iEXSetting('is_order_user_no_copydata') == 1)
                                            <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$detail->user->id}}" ngclipboard-success="onEventClipboardSuccess('ID');">
                                                <i class="fad fa-copy"></i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        E-mail
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->user->email}}
                                        @if(iEXSetting('is_order_user_no_copydata') == 1)
                                            <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$detail->user->email}}" ngclipboard-success="onEventClipboardSuccess('E-mail');">
                                                <i class="fad fa-copy"></i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @if(!empty($detail->phone))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Номер телефона
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$detail->phone}}
                                            @if(iEXSetting('is_order_user_no_copydata') == 1)
                                                <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$detail->phone}}" ngclipboard-success="onEventClipboardSuccess('Phone');">
                                                    <i class="fad fa-copy"></i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Имя
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->user->name}}
                                        @if(iEXSetting('is_order_user_no_copydata') == 1)
                                            <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$detail->user->name}}" ngclipboard-success="onEventClipboardSuccess('Name');">
                                                <i class="fad fa-copy"></i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Всего обменов
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{ \App\Models\Task::where('id_user', $detail->user->id)->whereIn('status',[4,5])->count() }}
                                    </div>
                                </div>

                                @if(isset($detail->user->fromReferral))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            От партнера
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            <div class="text-muted">
                                                <a href="{{ admin_base_path('/account/users/?id='.$detail->user->fromReferral->referral_link->user->id) }}">{{ $detail->user->fromReferral->referral_link->user->name }}</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Дата регистрации
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->user->created_at}}
                                    </div>
                                </div>

                                @if(!is_null($detail->user->last_activity_at))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Последний визит
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ \Illuminate\Support\Carbon::parse($detail->user->last_activity_at)->diffForHumans() }}
                                        </div>
                                    </div>
                                @endif

                                <div class="user-info-button-ban">
                                    @if($detail->is_ban_order_data == 0)
                                        <a data-toggle="tooltip" data-title="Внести данные клиента в черный список" href="?ban_order={{ $detail->id }}" class="text-danger">
                                            Внести данные в черный список
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>



                        @if((int)iEXSetting('is_enabled_last_comment_task') == 1)

                            <div class="application-comment-title">
                                <div layout="row">
                                    <div>
                                        Комментарий: <span>{{ $detail->task_comment->count() }}</span>
                                    </div>
                                    <span flex></span>
                                    <a  data-toggle="modal" href="#orderComment-{{ $detail->id }}">Добавить</a>
                                </div>
                            </div>


                                @if($detail->task_comment->count() > 0)
                                    <div class="application-comment">
                                        @foreach($detail->task_comment as $comment)
                                            <div class="application-comment-item {{ !is_null($comment->class_style) ? 'comment-style-'.$comment->class_style : 'comment-style-orange' }}">
                                                <div class="application-comment-item-author">
                                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$comment->user->id) }}"><b>{{$comment->user->name}}</b></a>
                                                    <span class="application-comment-item-time">{{ \Illuminate\Support\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                                                </div>
                                                <p class="application-comment-data">{{ $comment->message }}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="application-comment-empty">No comment</div>
                                @endif

                        @endif


                            <!-- Сообщений в чате -->
                            @if((int)iEXSetting('is_enable_order_online_chat') == 1)
                                <div class="application-comment-title">
                                    <div layout="row" layout-align="start center">
                                        <div>
                                            Сообщений от клиента: <span>{{ $detail->task_messages->count() }}</span>
                                        </div>
                                        <span flex></span>
                                        @if($detail->task_info->is_blocked_chat == 0)
                                            <a style="font-size: 13px; color: #999;" data-toggle="modal" href="?is_blocked_chat">Заблокировать чат</a>
                                        @else
                                            <a style="font-size: 13px; color: #999;" data-toggle="modal" href="?is_unblocked_chat">Разблокировать чат</a>
                                        @endif
                                    </div>
                                </div>
                                @if($detail->task_messages->count() > 0)
                                    <form action="?actions=notification" method="POST">
                                        @csrf
                                        <input type="hidden" name="type_send_message" value="1">
                                        <div layout="row" layout-align="space-between center">
                                            <textarea class="form-control" rows="3" name="message_success"></textarea>
                                            <span flex></span>
                                            <md-button type="submit" class="md-primary md-raised">Отправить</md-button>
                                        </div>
                                    </form>

                                    <div class="application-comment new-event__order-chats" layout="column">
                                        @foreach($detail->task_messages as $message)
                                            <div class="new-event__order-chat__item @if($message->type_user == 1) new-event__order-chat__white @endif">
                                                <div class="application-comment-item-author">
                                                    <span class="new-event__order-chat__item-time">{{ \Illuminate\Support\Carbon::parse($message->created_at)->diffForHumans() }}</span>
                                                </div>
                                                <p class="application-comment-data">{{ $message->message }}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="application-comment-empty">Нет сообщений</div>
                                @endif
                            @endif



                            <form action="{{ admin_base_path('/tx') }}?actions=add_comment&id={{ $detail->id }}" method="post" class="form-horizontal">
                                <div class="modal fade" id="orderComment-{{ $detail->id }}" tabindex="-1" role="dialog" aria-labelledby="orderComment-{{ $detail->id }}-Label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header ui-dialog-titlebar">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <span class="ui-dialog-title" id="myModalLabel">Комментарий к заявке</span>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="from_shot" class="col-sm-3 control-label">Текста</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" rows="5" name="message"></textarea>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="from_shot" class="col-sm-3 control-label">Стиль</label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control selectpicker" name="class_style">
                                                            <option value="blue">Голубой</option>
                                                            <option value="orange">Оранжевый</option>
                                                            <option value="green">Зеленый</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br />

                                                @if($detail->task_comment->count() == 0)
                                                    <div class="text-center" style="color: red;">
                                                        Комментарий не найдено
                                                    </div>
                                                @else
                                                    <table class="table table-border-2">
                                                        <thead>
                                                        <tr>
                                                            <th>Менеджер</th>
                                                            <th>Сообщение</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($detail->task_comment as $comment)
                                                            <tr>
                                                                <td style="width: 150px;">
                                                                    <div>
                                                                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$comment->user->id}}&sorting=id"><b>{{$comment->user->name}}</b></a>
                                                                    </div>
                                                                    <small>{{ $comment->created_at->translatedFormat('d M Y H:i') }}</small>
                                                                </td>
                                                                <td>
                                                                    {{ $comment->message }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>

                                                    </table>
                                                @endif

                                                <br />

                                            </div>
                                            <div class="modal-footer">
                                                <div class="float-right">
                                                    <md-button type="submit" class="md-primary md-raised">Добавить</md-button>
                                                    <md-button type="button" data-dismiss="modal">Отмена</md-button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>


                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </md-content>


    <!-- Отправить уведомление клиенту -->
    <form action="?actions=notification" method="POST">
        @csrf
        <div class="modal fade" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Отправить сообщение на почту</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="message_success" class="col-sm-2 control-label">Сообщение</label>
                            <div class="col-sm-10">
                                <textarea rows="5" class="form-control" name="message_success" placeholder="Напишите сообщение для клиента"></textarea>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="collapse" id="chatHistory">
                            <hr />
                            <h4>История чата</h4>
                            @if($historyChat->count() == 0)
                                <div class="text-center" style="color: red;">
                                    Сообщений не найдено
                                </div>
                            @else
                                <table class="table table-border-2">
                                    <thead>
                                    <tr>
                                        <th>Дата создания</th>
                                        <th>Кто создал</th>
                                        <th>Сообщение</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($historyChat as $item)
                                        <tr>
                                            <td>{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</td>
                                            <td>{{ $item->user->name }}</td>
                                            <td>{{ $item->message }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br />
                    <div class="modal-footer">
                        <div class="float-left">
                            <md-button type="button" role="button" data-toggle="collapse" href="#chatHistory" class="md-warn md-raised">История</md-button>
                        </div>

                        <div class="float-right">
                            <md-button type="submit" class="md-primary md-raised">Отправить</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Отправляем чек клиенту -->
    <form action="?actions=check" method="POST">
        @csrf
        <div class="modal fade" id="sendCheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Отправить чек</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="message_success" class="col-sm-2 control-label">Сообщение</label>
                            <div class="col-sm-10">
                                <textarea rows="5" class="form-control" name="message_success" placeholder="Напишите информацию для клиента"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br />
                    <div class="modal-footer">
                        <md-button type="submit" class="md-raised md-primary">Отправить чек</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- История этапов -->
    @if((int)iEXSetting('is_enabled_module_order_step') == 1)
        <div class="modal fade" id="steps_logs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">История этапов заявок</span>
                    </div>
                    <div class="modal-body">
                        @if(!isset($detail->steps_logs) or count($detail->steps_logs) == 0)
                            <div class="text-center" style="color: red;">
                                Ничего не найдено
                            </div>
                        @else
                            <table class="table table-border-2">
                                <thead>
                                <tr>
                                    <th>Менеджер</th>
                                    <th>Этап</th>
                                    <th>Статус заявки</th>
                                    <th>Изменено</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($detail->steps_logs as $item)
                                    <tr>
                                        <td>
                                            {{ isset($item->manager) ? $item->manager->name : ''}}
                                        </td>
                                        <td>
                                            {{ isset($item->order_step) ? $item->order_step->name : '' }}
                                        </td>
                                        <td>
                                            {{ isset($item->order_status) ? $item->order_status->name : ''}}
                                        </td>
                                        <td>{{  $item->created_at->format('d m Y, H:i') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <!-- История пересчитываний -->
    <div class="modal fade" id="history_recalculation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="ui-dialog-title" id="myModalLabel">История изменений</span>
                </div>
                <div class="modal-body">
                    @if($historyRecalculation->count() == 0)
                        <div class="text-center" style="color: red;">
                            Изменений не зафиксировано
                        </div>
                    @else
                        <table class="table table-border-2">
                            <thead>
                            <tr>
                                <th>Было</th>
                                <th>Стало</th>
                                <th>Тип</th>
                                <th>Изменено</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($historyRecalculation as $item)
                                <tr>
                                    <td>
                                        {{$item->old_amount}} {{$codeIn->name}}
                                    </td>
                                    <td>
                                        {{$item->amount}} {{$codeIn->name}}
                                    </td>
                                    <td>
                                        @if($item->type == 0)
                                            По станд. курсу
                                        @elseif($item->type == 1)
                                            По новому курсу
                                        @endif
                                        <div style="font-size: 10px; color: #999;">{{$item->course}}</div>
                                    </td>
                                    <td>{{$item->created_at->format('d m Y, H:i')}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    @endif
                </div>
                <div class="modal-footer">
                    <md-button type="button" data-dismiss="modal">Отмена</md-button>
                </div>
            </div>
        </div>
    </div>

    <!-- Пересчитать -->
    <form action="?actions=recount" method="post"  class="form-horizontal">
        @csrf
        <div class="modal fade" id="recount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Пересчитать</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Клиент отдал</label>
                            <div class="col-sm-9">
                                <input type="text" name="give" class="form-control" id="give" placeholder="Сумму которую отдал клиент" value="{{$detail->give_price}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Пересчитать по</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" name="at_rate">
                                    <option value="0">курсу которую создал клиент</option>
                                    <option value="1">по актуальному курсу</option>
                                </select>
                            </div>
                        </div>

                        <div class="collapse" id="collapseExample">
                            <hr />
                            <h4>История изменений</h4>
                            <hr />
                            @if($historyRecalculation->count() == 0)
                                <div class="text-center" style="color: red;">
                                    Изменений не зафиксировано
                                </div>
                            @else
                                <table class="table table-border-2">
                                    <thead>
                                    <tr>
                                        <th>Было</th>
                                        <th>Стало</th>
                                        <th>Тип</th>
                                        <th>Изменено</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($historyRecalculation as $item)
                                        <tr>
                                            <td>
                                                {{$item->old_amount}} {{$codeIn->name}}
                                            </td>
                                            <td>
                                                {{$item->amount}} {{$codeIn->name}}
                                            </td>
                                            <td>
                                                @if($item->type == 0)
                                                    По станд. курсу
                                                @elseif($item->type == 1)
                                                    По новому курсу
                                                @endif
                                                <div style="font-size: 10px; color: #999;">{{$item->course}}</div>
                                            </td>
                                            <td>{{$item->created_at->format('d m Y, H:i')}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="float-left">
                            <md-button type="button" role="button" data-toggle="collapse" href="#collapseExample" class="md-warn md-raised">История</md-button>
                        </div>

                        <div class="float-right">
                            <md-button type="submit" class="md-primary md-raised">Пересчитать</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Пересчитать (По сумме которую клиент оплатил)-->
    @if($walletInfoIn != null)
        <form action="?actions=recount" method="post"  class="form-horizontal">
            @csrf
            <div class="modal fade" id="recount2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Пересчитать</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Клиент отправил</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly name="give" class="form-control" id="give" placeholder="Сумму которую отдал клиент" value="{{  $walletInfoIn->amount }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary">Пересчитать</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endif



    <!-- Редактирование рекзвитов клиента -->

    <form action="?actions=edit_data" method="post"  class="form-horizontal">
        @csrf
        <div class="modal fade" id="edit_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Изменение реквизитов</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="from_shot" class="col-sm-3 control-label">Со счета</label>
                            <div class="col-sm-9">
                                <input type="text" name="from_shot" class="form-control" id="from_shot" value="{{$detail->from_shot}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="to_shot" class="col-sm-3 control-label">На счет</label>
                            <div class="col-sm-9">
                                <input type="text" name="to_shot" class="form-control" id="to_shot" value="{{$detail->to_shot}}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="float-right">
                            <md-button type="submit" class="md-primary md-raised">Обновить</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade" id="all_operator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="ui-dialog-title" id="myModalLabel">Все операторы</span>
                </div>
                <div class="modal-body">
                    @if($historyOperators->count() == 0)
                        <div class="text-center" style="color: red;">
                            Операторов нет
                        </div>
                    @else
                        <table class="table table-border-2">
                            <thead>
                            <tr>
                                <th>Дата создания</th>
                                <th>Смена</th>
                                <th>Статус заявки</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($historyOperators as $item)
                                <tr>
                                    <td>{{$item->created_at->format('d m Y, H:i')}}</td>
                                    <td>
                                        Оператор
                                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->from_manager->id.'&sorting=id') }}"><b> {{ $item->from_manager->name }}</b></a>
                                        взял заявку
                                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->to_manager->id.'&sorting=id') }}"><b> {{ $item->to_manager->name }}</b></a>
                                    </td>
                                    <td>{{ $item->tasks->task_status->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <!-- Оплатить и закрыть заявку -->
    <div class="modal fade" id="button-success-autopay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="ui-dialog-title" id="myModalLabel">Выполнение заявки</span>
                </div>
                <div class="modal-body">
                    <div class="md_simple_form_description_com" @if((int)iEXSetting('is_currencies_order_hide_cashback_bonus') == 1) style="display: none" @endif>
                        <md-switch name="notPartner" ng-model="notPartner" ng-change="notPartnerChange()" ng-true-value="true" ng-false-value="false">Не выплачивать партнерские бонусы</md-switch>
                        <md-switch name="notCashback" ng-model="notCashback" ng-change="notCashbackChange()" ng-true-value="true" ng-false-value="false">Не выплачивать cashback</md-switch>
                    </div>

                    @if((int)iEXSetting('is_currencies_order_hide_cashback_bonus') == 0)
                        <hr />
                    @endif
                    <div class="md_simple_form_description_com">
                        <p>Вставьте ID Транзакции (Не ссылку):</p>
                        <input name="id_transaction" ng-model="idTransaction" class="form-control" placeholder="ID Транзакции">
                    </div>

                    @if(!empty(config('security-codes.order-confirm')))
                        <hr />
                        <div class="md_simple_form_description_com">
                            <p>Ключ безопасности</p>
                            <input name="pin_code" ng-model="pinCode" class="form-control" placeholder="Введите ключ безопасности">
                        </div>
                    @endif

                    <div class="md_simple_form_description_com">
                        <p>Комментарий для клиента</p>
                        <textarea class="form-control" name="text_message_order" rows="5"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="float-right">
                        <md-button ng-click="successPayment($event, 'merchant', '{{$detail->id}}')" class="md-raised md-success-2" ng-disabled="isButtonDisabled">
                            <span ng-if="isButtonDisabled == false">Завершить</span>
                            <span ng-if="isButtonDisabled == true">Пожалуйста, подождите...</span>
                        </md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Выполнить заявку -->
    <div class="modal fade" id="button-success-payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog order-detail-modal" role="document">
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="ui-dialog-title" id="myModalLabel">Выполнение заявки</span>
                </div>
                <div class="modal-body">

                    <div class="md_simple_form_description_com" @if((int)iEXSetting('is_currencies_order_hide_cashback_bonus') == 1) style="display: none" @endif>
                        <md-switch name="notPartner" ng-model="notPartner" ng-change="notPartnerChange()" ng-true-value="true" ng-false-value="false">Не выплачивать партнерские бонусы</md-switch>
                        <md-switch name="notCashback" ng-model="notCashback" ng-change="notCashbackChange()" ng-true-value="true" ng-false-value="false">Не выплачивать cashback</md-switch>
                    </div>

                    @if((int)iEXSetting('is_currencies_order_hide_cashback_bonus') == 0)
                        <hr />
                    @endif
                    <div class="md_simple_form_description_com">
                        <p>Вставьте ID Транзакции (Не ссылку):</p>
                        <input name="id_transaction" ng-model="idTransaction" class="form-control" placeholder="ID Транзакции">
                        <div class="input-p-text">Этот пункт необходим в случае, если вы хотите чтобы клиент в чеке мог открыть транзакцию в BlockChain</div>
                    </div>

                    <div class="md_simple_form_description_com">
                        <p>Комментарий для клиента</p>
                        <textarea class="form-control" ng-model="textMessageOrder" name="text_message_order" rows="5"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="float-right">
                        <md-button ng-click="successPayment($event, 'default', '{{$detail->id}}')" class="md-raised md-success-2" ng-disabled="isButtonDisabled">
                            <span ng-if="isButtonDisabled == false">Завершить</span>
                            <span ng-if="isButtonDisabled == true">Пожалуйста, подождите...</span>
                        </md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Отклонить заявку -->
    <div class="modal fade" id="button-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog order-detail-modal" role="document">
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="ui-dialog-title" id="myModalLabel">Отклонение заявки</span>
                </div>
                <div class="modal-body">
                    <div class="md-dialog-content">
                        <div class="md_simple_form_description_com">

                            <h4>Причина отклонения заявки</h4>
                            <p>Укажите причину по которой вы хотите отклонить заявку:</p>

                            <br />
                            <md-radio-group ng-model="categoryReject">
                                @foreach($reasonRejection as $key => $value)
                                    <md-radio-button value="{{ $key }}">{{ $value }}</md-radio-button>
                                @endforeach
                            </md-radio-group>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="float-right">
                        <md-button ng-click="confirmReject($event, '{{ $detail->id }}')" class="md-raised md-warn" ng-disabled="isButtonDisabled">
                            <span ng-if="isButtonDisabled == false">Отклонить</span>
                            <span ng-if="isButtonDisabled == true">Пожалуйста, подождите...</span>
                        </md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Отложить заявку -->
    <div class="modal fade" id="button-frozen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog order-detail-modal" role="document">
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="ui-dialog-title" id="myModalLabel">Отложить заявку</span>
                </div>
                <div class="modal-body">
                    <div class="md-dialog-content">
                        <div class="md_simple_form_description_com">

                            <h4>Причина</h4>
                            <p>Укажите причину по которой вы хотите отложить заявку:</p>
                            <br />

                            <md-radio-group ng-model="deferType">
                                @foreach($pendingOrderStatus as $key => $value)
                                    <md-radio-button value="{{ $key }}">{{ $value }}</md-radio-button>
                                @endforeach
                            </md-radio-group>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <div class="float-right">
                        <md-button ng-click="confirmDefer($event, '{{ $detail->id }}')" class="md-raised md-frozen" ng-disabled="isButtonDisabled">
                            <span ng-if="isButtonDisabled == false">Отложить</span>
                            <span ng-if="isButtonDisabled == true">Пожалуйста, подождите...</span>
                        </md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_bottom')
    <script type="text/javascript">
        $(function() {
            $('.show_detail_request_payment').hide();
            $('#id_request_payments').on('change', function () {
                var id = $(this).val();
                if (id == "add") {
                    $('.show_detail_request_payment').show();
                } else {
                    $('.show_detail_request_payment').hide();
                }
            });
        });
    </script>
@endsection
