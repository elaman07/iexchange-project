@foreach($items as $item)
    <div layout="row" layout-align="start end">
        <div class="new-exchange-panel__status">
            @if($item->status == 2)

                @if($item->int_status_verification_card == 1)
                    @if(isset($item->verification_card))
                        <div class="st-base-name st-verification-pending-payment">
                            {{ __('Ожидается подтверждение верификации менеджером') }}
                        </div>
                    @else
                        <div class="st-base-name st-verification-pending-payment">
                            {{ __('Верификация счета') }}
                        </div>
                    @endif
                @else
                    <div class="st-base-name {{ $item->task_status->class }}">
                        {{ $item->task_status->name }}
                    </div>
                @endif
            @elseif($item->status == 3)
                @if($item->scans > 0)
                    <div class="st-base-name st-handler-operator">
                        {{ $item->operator->name }} {{ __('принял') }}<br />
                        @if(!is_null($item->operator_started_at))
                            <div class="font-size-11">{{ \Illuminate\Support\Carbon::parse($item->operator_started_at)->diffForHumans() }}</div>
                        @endif
                    </div>
                @else
                    <div class="st-base-name {{ $item->task_status->class }}">{{ $item->task_status->name }}
                        @if($item->status == 3 and isset($item->task_single_log_confirm) and !empty($item->task_single_log_confirm))
                            <div class="order-list-wasted-time" style="color:green; " data-toggle="tooltip" data-title="{{ __('Кол-во Подтверждений от сети') }}">{{ __('Подтверждений от сети') }} <span class="text-danger">{{ $item->task_single_log_confirm->received_confirm }}</span>/<b class="text-success">{{ $item->task_single_log_confirm->needed_confirm }}</b></div>
                        @endif
                    </div>
                @endif
            @elseif($item->status == 5)
                <div class="st-base-name st-delete" data-toggle="tooltip" data-placement="top" title="{{ __('Заявка отклонена') }}">
                    @if(isset($item->tasks_rejection_status))
                        {{ $item->tasks_rejection_status->name }}
                    @else
                        {{ __('Заявка отклонена') }}
                    @endif
                </div>
            @elseif($item->status == 8)
                <div class="st-base-name st-frozen" data-toggle="tooltip" data-placement="top" title="{{ __('Заявка отложено') }}">
                    @if(isset($item->pending_order_status))
                        {{ $item->pending_order_status->name }}
                    @else
                        {{ __('Заявка отложено') }}
                    @endif
                </div>

                @if($item->task_info->is_freeze_scam == 1)
                    @if($item->task_info->is_freeze_local == 1)
                        <div class="text-danger" style="font-size: 11px; margin-top: 5px;">{{ __('Внутренний список должников') }}</div>
                    @else
                        <div class="text-danger" style="font-size: 11px; margin-top: 5px;">{{ __('Список Bestchange') }}</div>
                    @endif
                @endif
            @else
                <div class="st-base-name {{ $item->task_status->class }}">
                    @if($item->queue_status == 1)
                        <div>{{ __('Заявка в очереди на выплату') }}...</div>
                    @else
                        {{ $item->task_status->name }}
                    @endif
                </div>
            @endif

            @if($item->status == 4 and $item->type_finished_order != 0)
                @if($item->type_finished_order == 1)
                    <small class="text-warning" style="margin-top: 5px;">{{ __('Перевод в ручном режиме') }}</small>
                @else
                    <small class="text-success" style="margin-top: 5px;">{{ __('Перевод в автоматическом режиме') }}</small>
                @endif
            @endif
        </div>

        @if(isset($item->referral_link) and $item->status != 4)
            <div class="new-exchange-panel__partner">
                <div class="new-exchange-panel__partner-value">
                    {{ __('Партнер') }}:
                    <a href="{{ admin_base_path('/account/users/?id='.$item->referral_link->user->id) }}">
                        <b>{{ $item->referral_link->user->name }}</b>
                    </a>:
                </div>
            </div>
        @endif

        @if($item->int_error_type > 0)
            <div class="new-exchange-panel__partner">
                <div class="new-exchange-panel__event-value">
                    @if($item->int_error_type == 1)
                        {{ __('Ошибка при автовыплате') }}
                    @endif
                </div>
            </div>
        @endif
    </div>

    <div class="row new-exchange-panel-list__item row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-{{ $style->count_block_home }}">
        <div class="col">
            <div class="new-exchange-panel-list__head">
                <div layout="row" layout-align="start center">
                    @if($item->queue_status == 1)
                        <i class="far fa-alarm-clock" style="font-size: 18px; float: left; margin-right: 5px; margin-top: -2px;"></i>
                    @endif
                    @if($item->is_bot == 1 and $item->status == 3)
                        <i class="fad fa-robot new-exchange-monitoring__bot" data-toggle="tooltip" data-title="{{ __('Заявку мониторит бот') }}" data-placement="right"></i>
                    @endif

                    <div class="order-id">
                        <span>#{{ $item->id }}</span>
                        @if($item->public_id > 0)
                            @if((int)iEXSetting('disable_check_display') == 0)
                                |&nbsp;<a target="_blank" href="{{ url('/order/'.$item->public_id) }}">{{ $item->public_id }}</a>
                            @else
                                |&nbsp;{{ $item->public_id }}
                            @endif
                        @endif
                    </div>
                </div>
                <div class="order-direction-name">
                    @if(isset($item->edit_data_manager))
                        <a href="{{ admin_base_path('/tx/'.$item->id.'?actions=arhive') }}">
                            <b class="text-danger">{{ direction_name($item, true) }}</b>
                        </a>
                        <i class="fad fa-exclamation-triangle text-danger"></i>
                    @else
                        <a href="{{ admin_base_path('/tx/'.$item->id.'?actions=arhive') }}">
                            <b>{{ direction_name($item, true) }}</b>
                        </a>
                    @endif
                </div>
            </div>

            <div class="new-exchange-list__ul-info">

                @if($item->task_info->is_aml_analysis == 1)
                    @include('admin.applications.tx.aml')
                @endif

                @if(isset($item->referral_link) and $item->status == 4)
                    <div class="new-exchange-list__li-info new-exchange-list__li-info-bonus" layout="row" layout-align="start center">
                        <div class="new-exchange-list__li-key-info">
                            {{ __('Бонус для') }}
                            <a href="{{ admin_base_path('/account/users/?id='.$item->referral_link->user->id) }}">
                                <b>{{ $item->referral_link->user->name }}</b>
                            </a>:
                        </div>
                        <div class="new-exchange-list__li-value-info">
                            @if(isset($item->referral_log))
                                <span class="text-success">+{{ $item->referral_log->bonus }} <small class="text-muted">({{ $item->referral_log->current_percent }}%)</small></span>
                            @else
                                <small class="text-danger">{{ __('не начислен') }}</small>
                            @endif
                        </div>
                    </div>
                @endif

                    <div class="new-exchange-list__li-info" layout="row">
                        <div class="new-exchange-list__li-key-info">{{ __('Курс обмена') }}:</div>
                        <div class="new-exchange-list__li-value-info">
                            <div class="">
                                {{ $item->course_display }}
                            </div>
                        </div>
                    </div>

                    <div class="new-exchange-list__li-grid" layout="row" layout-align="space-between stretch">
                        <div class="new-exchange-list__li-info" flex="50">
                            <div class="new-exchange-list__li-key-info">{{ __('Заявка создана') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div class="">
                                    {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}
                                </div>
                                <div class="new-exchange__recalculation-value">
                                    {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" flex="50">
                            <div class="new-exchange-list__li-key-info">{{ __('Заявка обновлена') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div class="">
                                    {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}
                                </div>
                                <div class="new-exchange__recalculation-value">
                                    {{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(!empty($item->task_info->country_name) and !empty($item->task_info->city_name) and isset($item->task_info->cities))
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Город/Страна') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div class="">
                                    ({{ $item->task_info->cities->designation_xml }}) {{ $item->task_info->country_name }} - {{ $item->task_info->city_name }}
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($item->task_info->recalculated_at != null)
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Дата последнего пересчета') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info new-exchange-list__li-info-history" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('История курсов') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                @foreach($item->history_recalculation as $history_recalculation)
                                   <div class="new-exchange-list__li-linear">
                                       <div>
                                           {{ $history_recalculation->course }}
                                       </div>
                                       <div class="new-exchange__recalculation-value">
                                           {{ \Illuminate\Support\Carbon::parse($history_recalculation->created_at)->diffForHumans() }}
                                       </div>
                                   </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if($item->started_at != null and $item->status == 4)
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Заявка выполнена за') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                {{ \Illuminate\Support\Carbon::parse($item->started_at)->diffForHumans(\Illuminate\Support\Carbon::parse($item->updated_at), true)  }}
                            </div>
                        </div>
                    @endif

                @if((int)iEXSetting('is_enable_order_online_chat') == 1)
                    @if($item->task_messages->where('is_view', 0)->count() > 0)
                        <div class="order-list__chats">
                            <h3 class="new-exchange-list__order-title">{{ __('Новых сообщений в чате') }}</h3>
                            <div class="d-flex justify-content-center">
                                @foreach($item->task_messages->where('is_view', 0) as $message)
                                    <div class="p-2 flex-fill bd-highlight">
                                        <div class="application-comment-item new-chat-orderid__event" style="word-break: break-all; margin-bottom: 6px;border-radius: 5px; padding: 10px;">
                                            <div class="comment-headline">{{ \Illuminate\Support\Carbon::parse($message->created_at)->diffForHumans() }}</div>
                                            <div class="comment-message">{{ $message->message }}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                @endif
            </div>

            @if($item->status == 3)
                @if($item->in_flow_funds == 1)
                    <div class="new-exchange__status-bot">
                        <small style="color: #d20000">{{ __('Заявка переведена в ручной режим') }}</small>
                    </div>
                @else
                    @if($item->is_bot == 1)
                        <div class="@if($item->register_tx == 1) new-exchange__status-bot-success @else new-exchange__status-bot-unred @endif">
                            {!! scheduler_order_blockchain($item) !!}
                        </div>
                    @endif
                @endif
            @endif

            @if(isset($item->edit_data_manager))
                <div class="new-event__edit-manager-order">
                    {{ __('Внимание') }}, <a href="{{ admin_base_path('/account/users/?id='.$item->edit_data_manager->id) }}">
                        <b>{{ $item->edit_data_manager->name }}</b>
                    </a> {{ __('изменил реквизиты в заявке') }}.
                </div>
            @endif

            @if($item->status == 2 and $item->requisites_receive == '[request_payment]')
                <div class="new-event__waiting-requisites">
                    {{ __('По данной заявки ожидается реквизиты от оператора') }}
                </div>
            @endif

            @if($item->task_comment->count() > 0 and (int)iEXSetting('is_enabled_last_comment_task') == 1)
                <div class="d-flex justify-content-center">
                    @foreach($item->task_comment as $comment)
                        <div class="p-2 flex-fill bd-highlight">
                            <div class="application-comment-item  {{ !is_null($comment->class_style) ? 'comment-style-'.$comment->class_style : 'comment-style-orange' }}" style="word-break: break-all; margin-bottom: 6px;border-radius: 5px; padding: 10px;">
                                <div class="comment-headline">{{ \Illuminate\Support\Carbon::parse($comment->created_at)->diffForHumans() }}</div>
                                <div class="comment-message">{{ $comment->message }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

        </div>

        @if($style->is_hide_block_give_service == 0)
            <div class="col">
                <h3 class="new-exchange-list__order-title">{{ __('Отдает Клиент') }}</h3>
                <div class="new-exchange-list__ul-info">

                    @if($item->is_file_check == 1)
                        <div class="order-check__bg">Чек оплаты прикреплен</div>
                    @endif

                    <div class="new-exchange-list__li-info" layout="row">
                        <div class="new-exchange-list__li-key-info" flex="33">{{ __('Валюта') }}:</div>
                        <div class="new-exchange-list__li-value-info">
                            <a href="{{ admin_base_path('/basic/currency/'.$item->direction_exchange->id_currency1.'/edit') }}">{{ currency_in($item, true) }}</a>
                            @if(iEXSetting('is_order_user_no_copydata') == 1)
                                <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ currency_in($item, true) }}" ngclipboard-success="onEventClipboardSuccess('Валюта');">
                                    <i class="fad fa-copy"></i>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="new-exchange-list__li-info" layout="row" layout-align="start center">
                        <div class="new-exchange-list__li-key-info" flex="33">{{ __('Сумма') }} <br /><small class="order-item__label-sub">{{ __('с доп. комиссией') }}</small>:</div>
                        <div class="new-exchange-list__li-value-info">
                            <div>
                                {{ display_in_price_auto($item, 'give_price_with_comm') }} {{ $item->direction_exchange->currency1->code_currency->name }}

                                @if(iEXSetting('is_order_user_no_copydata') == 1)
                                    <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_with_comm') }}" ngclipboard-success="onEventClipboardSuccess('Сумма отдаю (с доп. комиссией)');">
                                        <i class="fad fa-copy"></i>
                                    </div>
                                @endif

                                @if(isset($item->wallet_transaction) and isset($item->merchant))
                                    @if($item->wallet_transaction->amount < get_amount_credit_merchant_from_order($item))
                                        <div class="text-danger">
                                            {{ __('Оплата не в полном объеме') }}: <br />
                                            <b>{{ __('Нехватает') }}:</b> {{ get_amount_credit_merchant_from_order($item) - $item->wallet_transaction->amount }}
                                            {{ $item->direction_exchange->currency1->code_currency->name }}
                                        </div>
                                    @endif
                                @endif

                                <small class="new-exchange__recalculation-value">
                                    @if(isset($item->history_recalculation) and $item->history_recalculation->count() > 0)
                                        {{ __('Пересчет с') }} {{ $item->history_recalculation->last()->old_amount }} {{ $item->direction_exchange->currency1->code_currency->name }} на {{ $item->history_recalculation->last()->amount }} {{ $item->direction_exchange->currency1->code_currency->name }}
                                    @endif
                                </small>
                            </div>

                            <a class="order-detail-card-info" href="#inPriceDetail-{{ $item->id }}" data-toggle="collapse" style="font-size: 11px; margin-top: 5px; margin-left: 0;">
                                {{ __('Подробнее о суммах') }}
                            </a>
                        </div>
                    </div>

                    @if(display_in_price_auto($item, 'give_price_fee_pay', false) > 0)
                        <div class="new-exchange-list__li-info" layout="row" layout-align="start center">
                            <div class="new-exchange-list__li-key-info" flex="33">{{ __('Сумма') }}<br /><small class="order-item__label-sub">{{ __('с комиссией доп. и ПС') }}</small>:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_in_price_auto($item, 'give_price_with_comm_pay') }} {{ $item->direction_exchange->currency1->code_currency->name }}

                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_with_comm_pay') }}" ngclipboard-success="onEventClipboardSuccess('Сумма отдаю (с комиссией доп. и ПС)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif


                    <div id="inPriceDetail-{{ $item->id }}" class="collapse">
                        <h3 class="new-exchange-list__order-detail-title">{{ __('Детали суммы Отдаю') }}</h3>
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Сумма (без комиссий)') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_in_price_auto($item, 'give_price_default') }}   {{ $item->direction_exchange->currency1->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_default') }}" ngclipboard-success="onEventClipboardSuccess('Сумма отдаю (без комиссий)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Доп. комиссия') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_in_price_auto($item, 'give_price_fee_comm', false) }}   {{ $item->direction_exchange->currency1->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_fee_comm', false) }}" ngclipboard-success="onEventClipboardSuccess('Доп. комиссия Отдаю');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Сумма (с доп. комиссией)') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_in_price_auto($item, 'give_price_with_comm') }}   {{ $item->direction_exchange->currency1->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_with_comm') }}" ngclipboard-success="onEventClipboardSuccess('Сумма отдаю (с доп. комиссией)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Комиссия ПС') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_in_price_auto($item, 'give_price_fee_pay', false) }}   {{ $item->direction_exchange->currency1->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_fee_pay', false) }}" ngclipboard-success="onEventClipboardSuccess('Комиссия ПС Отдаю');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Сумма (с комиссией доп. и ПС)') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_in_price_auto($item, 'give_price_with_comm_pay') }}   {{ $item->direction_exchange->currency1->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_in_price_auto($item, 'give_price_with_comm_pay') }}" ngclipboard-success="onEventClipboardSuccess('Сумма отдаю (с комиссией доп. и ПС)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>


                    @if(!empty($item->from_shot) and \Str::length($item->from_shot) > 0)
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Со счета') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                {{ $item->from_shot }}
                                @if(is_verified_order_card_collect($item))
                                    <div class="display-initial order-detail-is-verified" data-toggle="tooltip" data-title="{{ __('Этот номер счета верифицирован') }}">
                                        <i class="fas fa-badge-check"></i>
                                    </div>
                                @endif

                                @if(iEXSetting('is_order_user_no_copydata') == 1)
                                    <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ remove_all_spaces($item->from_shot) }}" ngclipboard-success="onEventClipboardSuccess('Номер счета');">
                                        <i class="fad fa-copy"></i>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif

                    <!-- Доп. поля (Получаю) -->
                    @if(count(get_order_fields_tx($item, 'currency_in')) > 0)
                        @foreach(get_order_fields_tx($item, 'currency_in') as $currency_in)
                            <div class="new-exchange-list__li-info" layout="row">
                                <div class="new-exchange-list__li-key-info">{{ $currency_in->field_name }}:</div>
                                <div class="new-exchange-list__li-value-info">
                                    @if(!is_null($currency_in->field_value))
                                        {{ $currency_in->field_value }}
                                        @if(iEXSetting('is_order_user_no_copydata') == 1)
                                            <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$currency_in->field_value}}" ngclipboard-success="onEventClipboardSuccess('{{ $currency_in->field_name }}');">
                                                <i class="fad fa-copy"></i>
                                            </div>
                                        @endif
                                    @else
                                        <span class="text-danger">{{ __('Не указано') }}</span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif


                    @if(isset($item->direction_exchange->currency1->payment->explorer) and isset($item->merchant_transaction_hash) and !empty($item->merchant_transaction_hash->transaction_hash))
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">{{ __('Входящая транзакция') }}:</div>
                            <div class="new-exchange-list__li-value-info">
                                <a target="_blank" href="{{ str_replace('{hash}', $item->merchant_transaction_hash->transaction_hash, $item->direction_exchange->currency1->payment->explorer->link) }}">{{ __('Перейдите в Blockchain') }}</a>
                            </div>
                        </div>
                    @endif


                    @if($item->status == 2)
                        @if($item->int_status_verification_card == 1)
                            @if(isset($item->verification_card))
                                <div class="new-event-verification__card">
                                    <a data-toggle="modal" href="#verificationCardInfo-{{ $item->id }}">{{ __('Проверить верификацию счета') }}</a>
                                </div>
                                <div class="modal fade" id="verificationCardInfo-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="orderComment-{{ $item->id }}-Label" aria-hidden="true">
                                    <div class="modal-dialog " role="document">

                                        <form class="form-horizontal" action="{{ admin_base_path('/verifications/cards') }}" method="POST">
                                            <input type="hidden" name="actions" value="update">
                                            <input type="hidden" name="redirect" value="tx">
                                            <input type="hidden" name="actions" value="save">
                                            <input type="hidden" name="verification_id" value="{{  $item->verification_card->id }}">
                                            @csrf

                                            <div class="modal-content">
                                                <div class="modal-header ui-dialog-titlebar">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <span class="ui-dialog-title" id="myModalLabel">{{ __('Верификация счета') }}</span>
                                                </div>
                                                <div class="modal-body2">

                                                    <div class="col-md-12">
                                                        <div class="card-verification-items">
                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('Дата создания') }}</div>
                                                                <div class="col-md-8">{{ $item->verification_card->created_at->format('d.m.Y H:i:s') }}</div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('Валюта') }}</div>
                                                                <div class="col-md-8">
                                                                    {{$item->verification_card->currency->payment->name}} {{ $item->verification_card->currency->code_currency->name }}
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('Номер счета') }}</div>
                                                                <div class="col-md-8">
                                                                    {{  (!empty($item->verification_card->card_number_string) ? $item->verification_card->card_number_string : $item->verification_card->card_number) }}
                                                                    @if(!empty($item->verification_card->name))
                                                                        {{ __('ФИО Держателя') }}: {{ $item->verification_card->name }}
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('IP Адрес') }}</div>
                                                                <div class="col-md-8">{{$item->verification_card->ip_address}}</div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('User Agent') }}</div>
                                                                <div class="col-md-8">{{$item->verification_card->user_agent}}</div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('Статус') }}</div>
                                                                <div class="col-md-8">

                                                                    <select class="selectpicker" name="status">
                                                                        <option value="0" @if($item->verification_card->status == 0) selected @endif>{{ __('На проверке') }}</option>
                                                                        <option value="1" @if($item->verification_card->status == 1) selected @endif>{{ __('Верифицирован') }}</option>
                                                                        <option value="3" @if($item->verification_card->status == 3) selected @endif>{{ __('Отклонить') }}</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-3">{{ __('Сообщение (при отклонении верификации)') }}</div>
                                                                <div class="col-md-8">
                                                                    <textarea class="form-control" name="verification_message" rows="4"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        @if((int)iEXSetting('is_module_storage_disk') == 1 and !empty(iEXSetting('storage_disk_verification_card')) and $item->verification_card->is_local_image == 1)
                                                            <a href="{{ iex_dynamic_read_view_image('verifications/'.$item->verification_card->image, false, 'verifications_card') }}" target="_blank">
                                                                <img src="{{ iex_dynamic_read_view_image('verifications/'.$item->verification_card->image, false, 'verifications_card') }}"/>
                                                            </a>
                                                        @else
                                                            <a target="_blank" href="/images/verifications/{{  $item->verification_card->image }}" class="card-verification-image">
                                                                <img src="/images/verifications/{{  $item->verification_card->image }}" />
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary" type="submit">Выполнить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endif

                </div>
            </div>
        @endif

        @if($style->is_hide_block_receive_service == 0)
            <div class="col">
                <h3 class="new-exchange-list__order-title">Переводит Сервис</h3>
                <div class="new-exchange-list__ul-info">
                    <div class="new-exchange-list__li-info" layout="row">
                        <div class="new-exchange-list__li-key-info" flex="33">Валюта:</div>
                        <div class="new-exchange-list__li-value-info">
                            <a href="{{ admin_base_path('/basic/currency/'.$item->direction_exchange->id_currency2.'/edit') }}">
                                {{ currency_out($item, true) }} {{ !empty($item->task_info->network_name) ? $item->task_info->network_name : '' }}
                            </a>
                            @if(iEXSetting('is_order_user_no_copydata') == 1)
                                <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ currency_out($item, true) }} {{ !empty($item->task_info->network_name) ? $item->task_info->network_name : '' }}" ngclipboard-success="onEventClipboardSuccess('Валюта');">
                                    <i class="fad fa-copy"></i>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="new-exchange-list__li-info" layout="row" layout-align="start center">
                        <div class="new-exchange-list__li-key-info" flex="33">Сумма <br /><small class="order-item__label-sub">с доп. комиссией</small>:</div>
                        <div class="new-exchange-list__li-value-info">
                            <div>
                                {{ display_out_price_auto($item, 'receiving_price_with_comm') }}  {{ $item->direction_exchange->currency2->code_currency->name }}
                                @if(iEXSetting('is_order_user_no_copydata') == 1)
                                    <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_with_comm') }}" ngclipboard-success="onEventClipboardSuccess('Сумма получаю (с доп. комиссией)');">
                                        <i class="fad fa-copy"></i>
                                    </div>
                                @endif
                            </div>

                            <a class="order-detail-card-info" href="#outPriceDetail-{{ $item->id }}" data-toggle="collapse" style="font-size: 11px; margin-top: 5px; margin-left: 0;">
                                Подробнее о суммах
                            </a>
                        </div>
                    </div>

                    @if(display_in_price_auto($item, 'receiving_price_fee_pay', false) > 0)
                        <div class="new-exchange-list__li-info" layout="row" layout-align="start center">
                            <div class="new-exchange-list__li-key-info" flex="33">Сумма <br /><small class="order-item__label-sub">с комиссией доп. и ПС</small>:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_out_price_auto($item, 'receiving_price_with_comm_pay') }}  {{ $item->direction_exchange->currency2->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_with_comm_pay') }}" ngclipboard-success="onEventClipboardSuccess('Сумма получаю (с комиссией доп. и ПС)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif

                    <div id="outPriceDetail-{{ $item->id }}" class="collapse">
                        <h3 class="new-exchange-list__order-detail-title">Детали суммы "Получаю"</h3>
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Сумма (без комиссий):</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_out_price_auto($item, 'receiving_price_default') }}   {{ $item->direction_exchange->currency2->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_default') }}" ngclipboard-success="onEventClipboardSuccess('Сумма получаю (без комиссий)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Доп. комиссия:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_out_price_auto($item, 'receiving_price_fee_comm', false) }}   {{ $item->direction_exchange->currency2->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_fee_comm', false) }}" ngclipboard-success="onEventClipboardSuccess('Доп. комиссия получаю');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Сумма (с доп. комиссией):</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_out_price_auto($item, 'receiving_price_with_comm') }}   {{ $item->direction_exchange->currency2->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_with_comm') }}" ngclipboard-success="onEventClipboardSuccess('Сумма получаю (с доп. комиссией)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Комиссия ПС:</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_out_price_auto($item, 'receiving_price_fee_pay', false) }}   {{ $item->direction_exchange->currency2->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_fee_pay', false) }}" ngclipboard-success="onEventClipboardSuccess('Комиссия ПС получаю');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Сумма (с комиссией доп. и ПС):</div>
                            <div class="new-exchange-list__li-value-info">
                                <div>
                                    {{ display_out_price_auto($item, 'receiving_price_with_comm_pay') }}   {{ $item->direction_exchange->currency2->code_currency->name }}
                                    @if(iEXSetting('is_order_user_no_copydata') == 1)
                                        <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ display_out_price_auto($item, 'receiving_price_with_comm_pay') }}" ngclipboard-success="onEventClipboardSuccess('Сумма получаю (с комиссией доп. и ПС)');">
                                            <i class="fad fa-copy"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(!empty($item->to_shot) and \Str::length($item->to_shot) > 0)
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">На счет:</div>
                            <div class="new-exchange-list__li-value-info">
                                {{ \Str::limit($item->to_shot, 60) }}
                                @if(iEXSetting('is_order_user_no_copydata') == 1)
                                    <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ remove_all_spaces($item->to_shot) }}" ngclipboard-success="onEventClipboardSuccess('Номер счета получаю');">
                                        <i class="fad fa-copy"></i>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif

                    <!-- Доп. поля (Получаю) -->
                    @if(count(get_order_fields_tx($item, 'currency_out')) > 0)
                        @foreach(get_order_fields_tx($item, 'currency_out') as $currency_out)
                            <div class="new-exchange-list__li-info" layout="row">
                                <div class="new-exchange-list__li-key-info">{{ $currency_out->field_name }}:</div>
                                <div class="new-exchange-list__li-value-info">
                                    @if(!is_null($currency_out->field_value))
                                        {{ $currency_out->field_value }}
                                        @if(iEXSetting('is_order_user_no_copydata') == 1)
                                            <div class="iex-order__copied" ngclipboard data-clipboard-text="{{$currency_out->field_value}}" ngclipboard-success="onEventClipboardSuccess('{{ $currency_out->field_name }}');">
                                                <i class="fad fa-copy"></i>
                                            </div>
                                        @endif
                                    @else
                                        <span class="text-danger">Не указано</span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif


                    @if($item->status == 4 and isset($item->pay_transaction_hash))
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Исходящая транзакция:</div>
                            <div class="new-exchange-list__li-value-info">
                                @if($item->task_info->is_wait_hash_pay == 1)
                                    <div>
                                        <small class="text-warning"><i class="far fa-clock"></i>Ожидается ссылка на blockchain</small>
                                    </div>
                                @elseif(!empty($item->pay_transaction_hash->transaction_hash) and isset($item->direction_exchange->currency2->payment->explorer))
                                    <a target="__blank" href="{{ str_replace('{hash}', $item->pay_transaction_hash->transaction_hash, $item->direction_exchange->currency2->payment->explorer->link) }}">Перейти в Blockchain</a>
                                @endif
                            </div>
                        </div>
                    @endif

                    @if($item->status == 4 and $item->is_status_pay_callback > 0)
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Статус выплаты:</div>
                            <div class="new-exchange-list__li-value-info">
                                @if($item->status_pay_api == 1)
                                    <div>
                                        <small class="text-warning">Ожидается подтверждение</small>
                                    </div>
                                @elseif($item->status_pay_api == 2)
                                    <div>
                                        <small class="text-success">Успешная выплата
                                            @if($item->is_status_pay_email == 1)
                                                (Чек отправлен)
                                            @endif
                                        </small>
                                    </div>
                                @elseif($item->status_pay_api == 3)
                                    <div>
                                        <small class="text-danger">Ошибка выплаты</small>
                                    </div>
                                @else
                                    <div>
                                        <small class="text-muted">Ожидается ответ</small>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endif
        @if($style->is_hide_block_client == 0)
            <div class="col">
                <h3 class="new-exchange-list__order-title">О клиенте</h3>

                <div class="new-exchange-list__ul-info">
                    <div class="new-exchange-list__li-info" layout="row" layout-align="start center">
                        <div class="new-exchange-list__li-key-info">Имя:</div>
                        <div class="new-exchange-list__li-value-info">
                            <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->user->id) }}">
                                @if($item->user->order_num > 0)
                                    <b>
                                        @if($item->user->is_vip_client)
                                            <span style="color: #bd258e">{{ $item->user->name }}</span>
                                        @else
                                            {{ $item->user->name }}
                                        @endif
                                    </b>
                                @else
                                    <b data-toggle="tooltip" data-placement="top" title="Нет успешных обменов">
                                        @if($item->user->is_vip_client)
                                            <span style="color: #bd258e">{{ $item->user->name }}</span>
                                        @else
                                            {{ $item->user->name }}
                                        @endif
                                    </b>
                                @endif
                            </a>
                            @if(!empty($item->task_info->code_country))
                                <span class="order-list-country-icon">
                                    <md-icon data-toggle="tooltip" data-placement="left" title="Страна: {{ $item->task_info->country }}" md-svg-src="/images/country/{{ $item->task_info->code_country }}.svg"></md-icon>
                                </span>
                            @endif
                            @if($item->user->isBanned())
                                <small class="text-danger">Забанен</small>
                            @endif


                            @if($item->user->is_verify_account == 1)
                                <div style="font-weight: normal;">
                                    <small class="text-success">{{ __('Личность подтверждена') }}</small>
                                </div>
                            @else
                                <div style="font-weight: normal;">
                                    <small class="text-danger">{{ __('Личность не подтверждена') }}</small>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="new-exchange-list__li-info" layout="row">
                        <div class="new-exchange-list__li-key-info">E-mail:</div>
                        <div class="new-exchange-list__li-value-info" style="color: {{ !is_null($item->user->email) && is_null($item->user->email_verified_at) ? 'red' : 'green'}};">
                            {{ $item->user->email }}
                            @if(iEXSetting('is_order_user_no_copydata') == 1)
                                <div class="iex-order__copied" ngclipboard data-clipboard-text="{{ $item->user->email }}" ngclipboard-success="onEventClipboardSuccess('E-mail');">
                                    <i class="fad fa-copy"></i>
                                </div>
                            @endif
                        </div>
                    </div>

                    @if(!is_null($item->user->last_activity_at))
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Последний визит:</div>
                            <div class="new-exchange-list__li-value-info">
                                @if(\Illuminate\Support\Carbon::parse($item->user->last_activity_at)->addMinutes(3) > \Illuminate\Support\Carbon::now() )
                                    <b class="text-success">Online</b>
                                @else
                                    {{ \Illuminate\Support\Carbon::parse($item->user->last_activity_at)->diffForHumans() }}
                                @endif
                            </div>
                        </div>
                    @endif

                    @if($item->user->order_num > 1)
                        <div class="new-exchange-list__li-info" layout="row">
                            <div class="new-exchange-list__li-key-info">Всего заявок:</div>
                            <div class="new-exchange-list__li-value-info">
                                <a href="?id_user={{$item->user->id}}">{{$item->user->order_num}}  (на сумму ${{ isset($item->task_user_sum_convert_to_usd) ? iex_number_format($item->task_user_sum_convert_to_usd, 2, true) : 0 }})</a>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="new-exchange-list__details-icons">

                    <div layout="row" layout-align="end center">

                        <a data-toggle="modal" href="#orderFile-{{ $item->id }}">
                            <div class="order-list-newbie-icon">
                                <i class="fad fa-image" title="Прикрепить фото"></i> {{ $item->task_file->count() }}
                            </div>
                        </a>
                        <form action="?actions=add_attach_file&id={{ $item->id }}" enctype="multipart/form-data" method="post" class="form-horizontal">
                            <div class="modal fade" id="orderFile-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="orderFile-{{ $item->id }}-Label" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header ui-dialog-titlebar">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <span class="ui-dialog-title" id="myModalLabel">Прикрепить фото к заявке</span>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="from_shot" class="col-sm-3 control-label">Текст</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" rows="5" name="file_message"></textarea>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <label for="from_shot" class="col-sm-3 control-label">Файл</label>
                                                <div class="col-sm-9">
                                                    <input type="file" name="file_order" class="form-control">
                                                </div>
                                            </div>
                                            <hr />

                                            @if($item->task_file->count() == 0)
                                                <div class="text-center" style="color: red;">
                                                    Комментарий не найдено
                                                </div>
                                            @else
                                                <table class="table table-border-2">
                                                    <thead>
                                                    <tr>
                                                        <th>Менеджер</th>
                                                        <th>Сообщение</th>
                                                        <th>Файл</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($item->task_file as $comment)
                                                        <tr>
                                                            <td style="width: 150px;">
                                                                <div>
                                                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$comment->user->id}}&sorting=id"><b>{{$comment->user->name}}</b></a>
                                                                </div>
                                                                <small>{{ $comment->created_at->translatedFormat('d M Y H:i') }}</small>
                                                            </td>
                                                            <td>
                                                                {{ $comment->text }}
                                                            </td>
                                                            <td>
                                                                <a href="/storage/orders/{{ $comment->file }}" target="_blank">Открыть файл</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>

                                                </table>
                                            @endif

                                            <br />

                                        </div>
                                        <div class="modal-footer">
                                            <div class="float-right">
                                                <md-button type="submit" class="md-primary md-raised">Добавить</md-button>
                                                <md-button type="button" data-dismiss="modal">Отмена</md-button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        @if((int)iEXSetting('is_enabled_add_comment_task') == 1)

                            <a data-toggle="modal" href="#orderCommentUser-{{ $item->id }}">
                                <div class="order-list-newbie-icon">
                                    <i class="fad fa-user-md-chat" title="Комментарий для пользователя"></i> {{ $item->task_comment_user->count() }}
                                </div>
                            </a>

                            <a data-toggle="modal" href="#orderComment-{{ $item->id }}">
                                <div class="order-list-newbie-icon">
                                    <i class="fad fa-comment" title="Комментарии"></i> {{ $item->task_comment->count() }}
                                </div>
                            </a>

                            <form action="?actions=add_comment&id={{ $item->id }}" method="post" class="form-horizontal">
                                <div class="modal fade" id="orderComment-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="orderComment-{{ $item->id }}-Label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header ui-dialog-titlebar">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <span class="ui-dialog-title" id="myModalLabel">Комментарий к заявке</span>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="from_shot" class="col-sm-3 control-label">Текст</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" rows="5" name="message"></textarea>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="form-group">
                                                    <label for="from_shot" class="col-sm-3 control-label">Стиль</label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control selectpicker" name="class_style">
                                                            <option value="blue">Голубой</option>
                                                            <option value="orange">Оранжевый</option>
                                                            <option value="green">Зеленый</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br />

                                                @if($item->task_comment->count() == 0)
                                                    <div class="text-center" style="color: red;">
                                                        Комментарий не найдено
                                                    </div>
                                                @else
                                                    <table class="table table-border-2">
                                                        <thead>
                                                        <tr>
                                                            <th>Менеджер</th>
                                                            <th>Сообщение</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($item->task_comment as $comment)
                                                            <tr>
                                                                <td style="width: 150px;">
                                                                    <div>
                                                                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$comment->user->id}}&sorting=id"><b>{{$comment->user->name}}</b></a>
                                                                    </div>
                                                                    <small>{{ $comment->created_at->translatedFormat('d M Y H:i') }}</small>
                                                                </td>
                                                                <td>
                                                                    {{ $comment->message }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>

                                                    </table>
                                                @endif

                                                <br />

                                            </div>
                                            <div class="modal-footer">
                                                <div class="float-right">
                                                    <md-button type="submit" class="md-primary md-raised">Добавить</md-button>
                                                    <md-button type="button" data-dismiss="modal">Отмена</md-button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form action="?actions=add_comment_user&id={{ $item->id }}" method="post" class="form-horizontal">
                                <div class="modal fade" id="orderCommentUser-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="orderCommentUser-{{ $item->id }}-Label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header ui-dialog-titlebar">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <span class="ui-dialog-title" id="myModalLabel">Комментарий для пользователя</span>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="from_shot" class="col-sm-3 control-label">Текст</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" rows="5" name="message"></textarea>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <br />

                                                @if($item->task_comment_user->count() == 0)
                                                    <div class="text-center" style="color: red;">
                                                        Комментарий не найдено
                                                    </div>
                                                @else
                                                    <table class="table table-border-2">
                                                        <thead>
                                                        <tr>
                                                            <th>Менеджер</th>
                                                            <th>Сообщение</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($item->task_comment_user as $comment)
                                                            <tr>
                                                                <td style="width: 150px;">
                                                                    <div>
                                                                        <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$comment->user->id}}&sorting=id"><b>{{$comment->user->name}}</b></a>
                                                                    </div>
                                                                    <small>{{ $comment->created_at->translatedFormat('d M Y H:i') }}</small>
                                                                </td>
                                                                <td style="width: 150px;word-break: break-all">
                                                                    {{ $comment->message }}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>

                                                    </table>
                                                @endif

                                                <br />

                                            </div>
                                            <div class="modal-footer">
                                                <div class="float-right">
                                                    <md-button type="submit" class="md-primary md-raised">Добавить</md-button>
                                                    <md-button type="button" data-dismiss="modal">Отмена</md-button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif



                        @if($item->task_info->is_aml_analysis == 1)
                            @if($item->task_info->aml_riskscore == 0)
                                <i class="fad fa-shield-check order-list-aml-success-icon"></i>
                            @elseif($item->task_info->aml_riskscore > 0 and $item->task_info->aml_riskscore <= 30)
                                <i class="fad fa-check-circle order-list-aml-success-icon"></i>
                            @elseif($item->task_info->aml_riskscore >= 31 and $item->task_info->aml_riskscore <= 69)
                                <i class="fad fa-engine-warning order-list-aml-warning-icon"></i>
                            @elseif($item->task_info->aml_riskscore >= 70 and $item->task_info->aml_riskscore <= 100)
                                <i class="fad fa-bug order-list-aml-danger-icon"></i>
                            @endif
                        @endif

                        @if($item->telegram_id > 0)
                            <i data-toggle="tooltip" data-placement="left" data-title="Заявка создана через Telegram" class="fab fa-telegram-plane order-list-telegram-icon"></i>
                        @endif

                        @if($item->is_favorites)
                            <i class="fas fa-star order-list-favorites-icon"  data-toggle="tooltip" data-placement="left" data-title="Избранная заявка"></i>
                        @endif

                        @if(isset($item->task_info->device))
                            @if($item->task_info->device == 'mobile')
                                <i  data-toggle="tooltip" data-placement="left" title="Мобильная версия" class="fad fa-mobile order-list-device-icon"></i>
                            @elseif($item->task_info->device == 'desktop')
                                <i  data-toggle="tooltip" data-placement="left" title="Основная версия" class="fad fa-desktop-alt order-list-device-icon"></i>
                            @elseif($item->task_info->device == 'tablet')
                                <i  data-toggle="tooltip" data-placement="left" title="Планшет" class="fad fa-tablet order-list-device-icon"></i>
                            @endif
                        @endif

                        @if(empty($item->task_info->newbie))
                            <i class="fad fa-user order-list-newbie-icon" title="Обмен делает новичок"></i>
                        @endif
                    </div>
                </div>

                @if(isset($item->task_info) and $item->task_info->is_scam == 1)
                    @if($item->task_info->is_local_scam == 1)
                        <div class="order-list-black-list">
                            Привлекался за<br /> мошенничество
                        </div>
                    @else
                        <div class="order-list-black-list">
                            Данные найдены<br /> в черном списке BestChange
                        </div>
                    @endif
                @endif

            </div>
        @endif
    </div>
@endforeach
