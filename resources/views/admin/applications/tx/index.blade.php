@extends('admin.applications.tx.layout')

@section('search')
    <form action="{{ admin_base_path('/tx') }}" method="GET">
        <input type="hidden" name="send_action" value="on">
        <input type="hidden" name="action" value="filter" autocomplete="off">
        <div class="live-search" layout="row" layout-align="start center">
            <div>
                <input type="search" name="q" placeholder="{{ __('Найти заявку') }}..." ng-model="searchFish">
            </div>
        </div>
    </form>

@endsection

@section('title', __('Панель оператора'))

@section('content')

    <style>

        .new-exchange-list__li-info-history {
            max-height: 200px;
            overflow-y: scroll;
        }

        .new-exchange-list__li-info-history::-webkit-scrollbar {
            width: 5px;
        }

        .new-exchange-list__li-info-history::-webkit-scrollbar-thumb {
            background-color: #bebebe;
            border-radius: 9px;
        }

        .new-exchange-list__li-linear {
            border-bottom: 1px solid #e9e9e9;
            margin-bottom: 5px;
            padding-bottom: 5px;
        }

        .new-exchange-list__li-linear:last-child {
            border:none
        }

        .new-exchange-list__li-grid .new-exchange-list__li-info {
            margin-right: 5px;
        }

        .new-exchange-list__li-grid .new-exchange-list__li-info:last-child {
            margin-right: 0;
        }

        .new-exchange-list__li-grid .new-exchange-list__li-key-info {
            margin-bottom: 5px;
        }

        .new-exchange-panel__partner {
            margin-left: 5px;
        }

        .new-exchange-panel__partner-value {
            border-radius: 10px 10px 0 0;
            background: #f1f1f161;
            color: #787878;
            padding: 7px 15px;
            width: 140px;
            display: block;
            text-align: center;
            font-weight: 500;
            font-size: 13px;
        }

        .new-exchange-panel__event-value {
            border-radius: 10px 10px 0 0;
            background: #ffaaaa61;
            color: #ff1d1d;
            padding: 7px 15px;
            width: 140px;
            display: block;
            text-align: center;
            font-weight: 500;
            font-size: 13px;
        }
    </style>

    <style>
        .pagination li.disabled span {
            opacity: .6;
        }
        .pagination li span,
        .pagination li a{
            box-shadow: 0 2px 1px -1px #0003, 0 1px 1px #00000024, 0 1px 3px #0000001f;
            margin-right: 5px;
        }

        .pagination li:last-child a {
            margin-right: 0;
        }

        .filter-footer {
            padding: 8px;
            border: none;
        }

        /*.pagination>li span {*/
        /*    border-right: 2px solid #e9e9e9;*/
        /*}*/

        /*.pagination>li span:first-child {*/
        /*    border: none;*/
        /*}*/

        /*.pagination>li span:hover {*/
        /*    border-right: 2px solid #e9e9e9;*/
        /*}*/
    </style>

    <md-content>

        <div class="toolbar-order-list" layout="row" layout-align="start center">
            <div style="width:100%">
                <md-button ng-click="toggleLeft()" class="show-button-toggle md-icon-button float-left">
                    <i class="far fa-bars"></i>
                </md-button>
                <h2>{{ __('Список заявок') }}</h2>
{{--                @if(!iEXSetting('is_hidden_order_count'))--}}
{{--                    <div class="tx-abs-count-orders">Всего заявок: {{ iex_number_format($count, 0, true) }}</div>--}}
{{--                @endif--}}

                @if(isset($filter['is_new_messages']) and $filter['is_new_messages'] == 1)
                    <a class="md-button md-raised md-primary" href="?clear_messages">{{ __('Отметить все сообщения как Прочитанное') }}</a>
                @endif
            </div>

            <span flex></span>
            <div class="toolbar-filter-icon show-button-toggle-filter">
                <div layout="row" layout-align="start center">

                    <a data-toggle="modal" href="#create_filter" class="md-button md-primary">
                        <i class="far fa-filter"></i>
                        {{ __('Добавить фильтр') }}
                    </a>
                    <a data-toggle="modal" href="#settings_style" class="md-button md-primary md-raised">
                        {{ __('Настройки') }}
                    </a>
                </div>
            </div>
        </div>

        @if($error == 1)
            <div class="error-task">
                <span class="material-icons">error_outline</span>
                <div>{{ __('С выбранной заявкой работает другой оператор') }}</div>
            </div>
        @else

            <div class="x_panel__padding">
                <div class="x_content_filters col-md-5 col-xs-12" style="margin-top: 0;">
                    @include('admin.applications.tx.filter')
                </div>
                <div class="clearfix"></div>
                <div class="x_content" ng-controller=TickerController>
                    <div class="pagination-right pagination-right-top pagination-sm">
                        {!! $items->appends($conditions)->links() !!}
                    </div>

                    @include('admin.applications.tx.orders')
                </div>
            </div>

            <div class="pagination-right">
                {!! $items->appends($conditions)->links() !!}
            </div>
        @endif
    </md-content>
@endsection
