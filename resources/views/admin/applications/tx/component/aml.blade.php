<!----------------------------------------------------->
<!------------ Блок для AML Результатов --------------->
<!----------------------------------------------------->
@if(!empty($taskInfo->aml_result_value))
    <div class="x_panel x_panel-tx">
        <div class="order-detail-title">
            <h2>AML Risk</h2>
        </div>
        <div class="x_content" style="text-align: center;padding: 30px;font-size: 34px;">

            <!-- Для строковых результатов -->
            @if(is_string($taskInfo->aml_result_value))
                @if($taskInfo->aml_result_value == 'undefined')
                    <div class="text-muted">
                        <b>{{ $taskInfo->aml_result_value }}</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Риск не известен</span>
                    </div>
                @elseif($taskInfo->aml_result_value == 'low')
                    <div class="text-success">
                        <b>{{ $taskInfo->aml_result_value }}</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Низкий уровень риска</span>
                    </div>
                @elseif($taskInfo->aml_result_value == 'medium')
                    <div class="text-warning">
                        <b>{{ $taskInfo->aml_result_value }}</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Средний уровень риска</span>
                    </div>
                @elseif(in_array($taskInfo->aml_result_value, ['high', 'severe']))
                    <div class="text-danger">
                        <b>{{ $taskInfo->aml_result_value }}</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Высокий уровень риска</span>
                    </div>
                @endif
            @endif

            <!-- Для процентных значений -->
            @if(is_numeric($taskInfo->aml_result_value))
                @if($taskInfo->aml_result_value > 0 and $taskInfo->aml_result_value <= 30)
                    <div class="text-success">
                        <b>{{ $taskInfo->aml_result_value }}%</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Низкий уровень риска</span>
                    </div>
                @elseif($taskInfo->aml_result_value > 31 and $taskInfo->aml_result_value <= 69)
                    <div class="text-warning">
                        <b>{{ $taskInfo->aml_result_value }}%</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Средний уровень риска</span>
                    </div>
                @elseif($taskInfo->aml_result_value > 70 and $taskInfo->aml_result_value <= 100)
                    <div class="text-danger">
                        <b>{{ $taskInfo->aml_result_value }}%</b>
                        <span style="display: block; font-size: 14px; margin-top: 5px;">Высокий уровень риска</span>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endif
