@if(isset($item->direction_exchange->currency1->aml_service_model))
    <!-- Для номерных значений -->
    @if(is_custom_numeric($item->task_info->aml_result_value))
        <div class="new-exchange-list__li-info @if($item->task_info->aml_result_value >= 70 and $item->task_info->aml_result_value <= 100) new-exchange__bg-red @endif" layout="row" layout-align="start center">
            <div class="new-exchange-list__li-key-info">{{ __('AML Результат') }}:</div>
            <div class="new-exchange-list__li-value-info">
                @if($item->task_info->aml_result_value == 0)
                    <small class="aml-result__zero text-success">
                        <b>{{ __('Безопасная сделка, 0% риска') }}</b>
                    </small>
                @elseif($item->task_info->aml_result_value > 0 and $item->task_info->aml_result_value <= 30)
                    <div class="aml-result__percent text-success">
                        <b>{{ $item->task_info->aml_result_value }}%</b>
                    </div>
                @elseif($item->task_info->aml_result_value >= 31 and $item->task_info->aml_result_value <= 69)
                    <div class="aml-result__percent text-warning">
                        <b>{{ $item->task_info->aml_result_value }}%</b>
                    </div>
                @elseif($item->task_info->aml_result_value >= 69 and $item->task_info->aml_result_value <= 100)
                    <div class="aml-result__percent text-danger">
                        <b>{{ $item->task_info->aml_result_value }}%</b>
                    </div>
                @endif

                <!-- Доп. статистика для AML анализа -->
                @if(isset($item->aml_analytics_log_getblock_success))
                    @if($item->direction_exchange->currency1->aml_service_model->aml_type == 'getblock' and count(get_aml_required_keys($item, 'getblock')) > 0)
                        <div class="aml-detail__analytics">
                            @foreach(get_aml_required_keys($item, 'getblock') as $key => $value)
                                {{ $key }} : {{ $value * 100 }}%<br />
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
        </div>
    @else
        <!-- Для остальных -->
        <div class="new-exchange-list__li-info" layout="row" layout-align="start center">
            <div class="new-exchange-list__li-key-info">{{ __('AML Результат') }}:</div>
            <div class="new-exchange-list__li-value-info">
                @if($item->task_info->aml_result_value == 'undefined')
                    <small class="aml-result__zero text-muted">
                        <b>{{ __('Риск не определен') }}</b>
                    </small>
                @elseif($item->task_info->aml_result_value == 'none')
                    <small class="aml-result__zero text-success">
                        <b>{{ __('Безопасная сделка, 0% риска') }}</b>
                    </small>
                @elseif($item->task_info->aml_result_value === 'low')
                    <div class="aml-result__percent text-success">
                        <b>{{ $item->task_info->aml_result_value }}</b>
                    </div>
                @elseif($item->task_info->aml_result_value === 'medium')
                    <div class="aml-result__percent text-warning">
                        <b>{{ $item->task_info->aml_result_value }}</b>
                    </div>
                @elseif(in_array($item->task_info->aml_result_value, ['high', 'severe']))
                    <div class="aml-result__percent text-danger">
                        <b>{{ $item->task_info->aml_result_value }}</b>
                    </div>
                @endif
            </div>
        </div>
    @endif
@endif
