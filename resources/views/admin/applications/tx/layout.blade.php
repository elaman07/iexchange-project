<!DOCTYPE html>
<html lang="ru" ng-app="TxApp" ng-csp="" ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', 'Администраторская')</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap-grid.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin-assets/compilers/tx/tx-20vcCQ1.css?v={{config('crypto.full_version')}}">

    <link rel="stylesheet" href="/assets/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/solid.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/brands.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/regular.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/light.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/duotone.min.css">


    <style>

        @media screen and (max-width: 800px)
        {
            .modal-open .modal-backdrop {
                display: none;
            }
        }

        .new-exchange-list__order-detail-title {
            text-align: center;
            padding: 15px;
            font-size: 15px;
            margin: 0;
        }

        .order-item__label-sub {
            color: #999;
            font-weight: 400;
            font-size: 11px;
        }

        .order-detail-item__padding {
            padding: 10px 0;
        }
        md-toolbar {
            background: transparent !important;
        }

        .top-toolbar-sidenav a:hover {
            color: #000 !important;
        }

        .md-toolbar-tools  {
            color: #3e4855;
        }

        md-content.md-default-theme, md-content {
            background-color: #f9fafe;
        }
        .new-sidenav-live__orders {

        }

        .new-sidenav-live__orders_item {
            padding: 20px 15px;
            margin: 10px;
            background: #fff;
            border-radius: 10px;
            display: flex;
            color: #3e4855;
        }

        .new-sidenav-live__orders_item-left {

        }

        .new-sidenav-live__orders_item .live-item__id {

        }
        .new-sidenav-live__orders_item .live-item__title {
            padding: 10px 0;
            font-size: 14px;
            font-weight: 600;
        }
        .new-sidenav-live__orders_item .live-item__amount{

        }

        .new-sidenav-live__orders_item:hover {
            box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px
        }

        .new-sidenav-live__orders_item-left {
            width: 100%;
        }
        .live-item__status {
            text-align: right;
        }

        .new-sidenav-live__orders md-progress-circular.md-default-theme path, md-progress-circular path {
            stroke-width: 2px !important;
        }

        .new-sidenav__preloading {
            text-align: center;
            width: 100%;
            padding: 20px;
        }

        .new-sidenav__preloading > md-progress-circular{
            margin: 0 auto;
        }
        .new-sidenav__preloading > div {
            margin-top: 10px;
            color: #999;
        }

        .live-item__time {
            text-align: right;
            font-size: 12px;
        }

        .live-item__time span {
            color: #999;
        }
        .new-sidenav__postoned__orders {
            padding: 10px;
            background: #ffddb659 !important;
            color: #f19021;
            margin: 10px;
            border-radius: 10px;
            text-align: center;
            display: block;
        }

        .new-sidenav__withdrawals__orders {
            padding: 10px;
            background: #c2ffc2 !important;
            color: #0b8b09;
            margin: 10px;
            border-radius: 10px;
            text-align: center;
            display: block;
        }

        .new-sidenav__new_messages {
            padding: 10px;
            background: #810c0cf7 !important;
            color: #ffffff;
            margin: 10px;
            border-radius: 10px;
            text-align: center;
            display: block;
        }

        .new-chat-orderid__event {
            background: #ffcccc;
            padding: 8px 10px;
            margin-top: 15px;
            border-radius: 5px;
            color: #a11616;
        }

        .order-list__chats {
            padding-top: 10px;
        }


        .new-exchange-panel-list__item {
            border-radius: 5px;
            box-shadow: 0 2px 1px -1px #0003, 0 1px 1px #00000024, 0 1px 3px #0000001f;
        }

        .order-check__bg {
            display: block;
            background: #c9ffc9;
            padding: 8px;
            margin-bottom: 10px;
            border-radius: 5px;
            color: #029916;
        }

        .check-image__item {
            display: block;
            width: 200px;
            height: auto;
            padding: 10px;
        }

        .check-image__item img {
            display: block;
            width: 100%;
            height: auto;
            border: 1px solid #e9e9e9;
            border-radius: 10px;
        }

    </style>
    <style>
        .card-verification-image {
            width: 100%;
            height: auto;
            display: block;
            text-align: center;
            margin: 0 auto;
            padding: 20px 0;
        }

        .card-verification-image img {
            width: 100%;
            display: block;
            height: auto;
        }

        .card-verification-items .row{
            padding: 15px 0;
            border-bottom: 1px solid #e9e9e9;
        }

        .card-verification-status {
            padding: 20px;
            text-align: center;
            padding-top: 0;
        }


        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>

    <style>
        .application-comment-empty {
            text-align: center;
            padding: 15px 10px;
            color: #8f8f8f;
        }

        .order-detail-item-value {
            word-wrap: break-word;
            display: block;
        }

        @media (min-width: 100px) and (max-width: 599px)
        {
            .left-sidenav .sidenav-footer {
                position: relative !important;
            }
        }
    </style>
</head>

<body class="nav-md" layout="column"> <!-- ng-cloak -->

<section ng-controller="BaseController" class="main_container" layout="row" flex style="    min-height: inherit;">
    <md-sidenav class="left-sidenav md-sidenav-left scroll-body" style="width: 600px !important; " md-component-id="left" md-is-locked-open="$mdMedia('gt-md')">
        <md-content>
            <md-toolbar class="top-toolbar-sidenav">

                <div class="md-toolbar-tools" ng-if="showMobileMainHeader == true">
                    <h2 md-truncate>
                        <a href="{{ admin_base_path('/') }}">
                            <i class="fad fa-home tx-button-home"></i>
                        </a>
                        <a style="margin-top: 5px;" href="{{ admin_base_path('/tx') }}">
                            <span ng-if="new_task > 0">
                                Активных заявок [[new_task]]
                            </span>

                            <span ng-if="new_task == 0">
                                Панель оператора
                            </span>
                        </a>
                    </h2>
                    <span flex=""></span>
                    <md-button class="md-icon-button" aria-label="Search" ng-click="searchShow()">
                        <md-tooltip>Поиск заявок</md-tooltip>
                        <i class="fad fa-search"></i>
                    </md-button>
                </div>


                <div class="md-toolbar-tools" ng-if="!showMobileMainHeader">
                    <md-button class="md-icon-button" aria-label="Back" ng-click="searchHide()">
                        <i class="fad fa-angle-left font-size-16"></i>
                    </md-button>
                    <div flex="90">
                        @yield('search')
                    </div>
                </div>
            </md-toolbar>

            <div class="new-sidenav-live__orders">


                <a ng-if="count_user_messages > 0" class="new-sidenav__new_messages" href="{{ admin_base_path('/tx?is_new_messages=1') }}">
                    Новых сообщений в чате: [[count_user_messages]]
                </a>

                <a ng-if="new_postponed > 0" class="new-sidenav__postoned__orders" href="{{ admin_base_path('/tx?status[]=8') }}">
                    Отложенных заявок найдено: [[new_postponed]]
                </a>

                <a ng-if="new_payment_bonuses > 0" class="new-sidenav__withdrawals__orders" href="{{ admin_base_path('/applications/payment_bonuses') }}">
                    Заявок на выплату вознаграждений: [[new_payment_bonuses]]
                </a>


                <div class="new-sidenav__preloading" ng-if="isLiveLoading == false">
                    <md-progress-circular md-mode="indeterminate" md-diameter="50"></md-progress-circular>
                    <div>Идет загрузка заявок...</div>
                </div>


                <div class="new-sidenav__preloading" ng-if="isLiveLoading == true && live_data.length == 0">
                    <div>Актуальных заявок нет</div>
                </div>
                <a class="new-sidenav-live__orders_item" ng-repeat="item in live_data" ng-href="{{config('admin.directory')}}/tx/[[item.id]]"
                   ng-mouseover="enableEdit(item)" ng-mouseleave="disableEdit(item)" ng-class="{'live-line-[[item.status_int]]': true, 'live-operator': item.scans > 0 && item.status_int != 7}">

                    <div class="new-sidenav-live__orders_item-left">
                        <div class="live-item__id" layout="row">
                            <div>
                                <i class="fad fa-circle icon-status__event"></i>
                                @if(iEXSetting('client_id_type_for_order') == 1)
                                    №[[item.public_id]]
                                @else
                                    №[[item.id]]
                                @endif
                            </div>
                            <span flex></span>
                            <div class="live-item__status">
                                <ng-container ng-if="!item.auth_user_validate">
                                    <span style="margin-left: 5px;" class="live-operator-color status" ng-if="item.scans > 0">[[item.scans_name]] принял</span>
                                    <span style="margin-left: 5px;" class="status" ng-if="item.scans == 0" ng-bind="item.status"></span>
                                </ng-container>

                                <ng-container ng-if="item.auth_user_validate">
                                    <span style="margin-left: 5px;" class="status" ng-bind="item.status"></span>
                                </ng-container>
                            </div>
                        </div>

                        <div class="live-item__content" layout="row" layout-xs="column" layout-align="end end" layout-align-xs="start start">
                            <div>
                                <div class="live-item__title">[[item.name | html]]</div>
                                <div  class="live-item__amount">[[item.amount | html]]</div>
                            </div>

                            <span flex></span>
                            <div class="live-item__time">
                                <span ng-bind="item.created"></span>
                                <div class="live-operator-color status" ng-if="item.scans > 0">
                                    [[item.scans_name]] принял
                                </div>
                                <div class="status" style="color: #ff5939" ng-if="item.is_hold == 1" ng-bind="item.hold_timeout"></div>
                                <div class="status" style="color: #b2ce1b" ng-if="item.in_flow_funds == 1">Ручной режим</div>
                            </div>
                        </div>

                        <div class="new-chat-orderid__event" ng-if="item.new_message > 0">
                            Новых сообщений в заявке: [[item.new_message]]
                        </div>

                    </div>
                </a>
            </div>

            <div class="sidenav-footer" ng-if="isStatusSite == true">
                <div layout="row" layout-align="center center">
                    @if((int)iEXSetting('type_working_mode') == 2)
                        <md-switch ng-change="onChange(status_site)" ng-model="status_site" class="md-primary md-switch-success">
                            <span ng-if="status_site == true">Рабочий режим</span>
                            <span ng-if="status_site == false">Технический перерыв</span>
                        </md-switch>
                    @else
                        <div style="padding: 10px;color: red;">
                            Активен режим работы "По расписанию"
                        </div>
                    @endif
                </div>
            </div>
        </md-content>
    </md-sidenav>
    <md-content layout="column" flex id="content" flex class="body-background" @yield('ng-controller')>

        @if(config('crypto.panel_operator') == 1)
            @if ($__env->yieldContent('toolbar'))
                @yield('toolbar')
            @endif

                @if (flash()->message)
                    <div class="{{ flash()->class }}">
                        {{ flash()->message }}
                    </div>
                @endif

            @yield('content')
        @else
            <div class="alert alert-danger text-center">Панель оператора отключен администратором.</div>
        @endif

    </md-content>

</section>


<script src="/admin-assets/compilers/tx/tx-20vcCQ1.js?v={{config('crypto.full_version')}}"></script>
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

@yield('js_bottom')
</body>

</html>
