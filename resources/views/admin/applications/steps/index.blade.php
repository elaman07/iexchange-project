@extends('admin.layouts.app')

@section('title', __('Этапы'))


@section('top-block')
    <md-button ng-href="application-steps/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить этап') }}</span>
    </md-button>


    <md-button ng-href="application-steps/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-filter text-primary"></md-icon>
        <span>{{ __('Сортировка') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <i class="far fa-cog"></i>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/applications/application-steps/'.$item->id.'/edit') }}">
                            {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>
                        @if($item->status == 0)
                            <span class="text-danger">{{ __('Отключено') }}</span>
                        @else
                            <span class="text-success">{{ __('Включено') }}</span>
                        @endif
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                    </td>

                    <td style="width: 100px;">
                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('/applications/application-steps/'.$item->id.'/destroy')  }}">
                            <i class="fa fa-fw fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки модуля') }}</span>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">{{ __('Включить модуль') }}</div>
                                    <select class="form-control selectpicker" name="is_enabled_module_order_step">
                                        <option value="0"  @if(iEXSetting('is_enabled_module_order_step') == 0) selected @endif>{{ __('Нет') }}</option>
                                        <option value="1"  @if(iEXSetting('is_enabled_module_order_step') == 1) selected @endif>{{ __('Да') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отмена') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

