@extends('admin.layouts.app')

@section('title','Статусы заявок')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.settings'))

@section('top-block')

    <md-button ng-href="reasons-rejection" class="btn-float" layout="column">
        <i class="text-danger far fa-times-circle"></i>
        <span class="text-danger">Отклоненные причины</span>
    </md-button>

    <md-button ng-href="reasons-defer" class="btn-float" layout="column">
        <i class="text-warning far fa-clock"></i>
        <span class="text-warning">Отложенные причины</span>
    </md-button>
@endsection

@section('content')
    <form action="?" method="post">

        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Наименование статуса</th>
                <th>Класс</th>
                <th>Цвет</th>
                <th>Экcпорт заявок</th>
                <th>Кол-во заявок</th>
                <th>Предосмотр</th>
            </tr>

            </thead>
            <tbody>
            @foreach($statuses as $status)
                <tr>
                    <td>
                        <input type="hidden" name="item_id[]" value="{{ $status->id }}">
                        <div class="multi-language-form-group-input">
                            <ul class="nav nav-pills lang-tabs">
                                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                    <li @if($form_lang['active'] == true) class="active" @endif>
                                        <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $status->id }}-{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                <div id="name-{{ $status->id }}-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                    <input type="text" class="form-control"  name="name{{$form_lang['field']}}[{{ $status->id }}]" value="{{ $status->getTranslation('name', $form_lang_key) }}">
                                </div>
                            @endforeach
                        </div>
                    </td>

                    <td>
                        <input name="class[{{$status->id}}]"  type="text" class="form-control" value="{{$status->class}}">
                    </td>

                    <td>
                        <input id="status-js{{$status->id}}" name="color[{{$status->id}}]" type="text" class="form-control" value="{{$status->color}}">
                    </td>

                    <td>
                        <select name="is_export[{{$status->id}}]" class="form-control selectpicker" data-width="100">
                            <option value="0" @if($status->is_export == 0) selected @endif>Нет</option>
                            <option value="1" @if($status->is_export == 1) selected @endif>Да</option>
                        </select>
                    </td>

                    <td>
                        {{ \App\Models\Task::where('status', $status->id)->count() }}
                    </td>

                    <td>
                        <span class="st-base-name {{ $status->class }}">{{ $status->name }}</span>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

        {{--<md-list class="requisites-dialog">--}}
            {{--@foreach($statuses as $status)--}}
                {{--<md-list-item >--}}
                    {{--<input name="status[{{$status->id}}]" flex="40" type="text" class="form-control" value="{{$status->name}}">--}}
                    {{--<md-divider></md-divider>--}}
                {{--</md-list-item>--}}
            {{--@endforeach--}}
        {{--</md-list>--}}

        <div class="modal-footer">
            <md-button class="md-raised md-success" type="submit">Сохранить</md-button>
        </div>
    </form>
@endsection
