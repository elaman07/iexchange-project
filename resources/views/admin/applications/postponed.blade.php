@extends('admin.layouts.app')

@section('title','Отложенные заявки')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.postponed'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>Направление обмена</th>
            <th>Статус</th>
            <th>Дата создания</th>
            <th>Действие</th>
            <th></th>
        </tr>

        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <th>{{$item->id}}</th>
                <td>
                    <a href="{{config('admin.directory')}}/applications/details/{{$item->id}}?actions=arhive">
                        <b>{{ direction_name($item, true)  }}</b>
                    </a>
                    <div style="color: #666;">
                        {{$item->give_price}} {{$item->direction_exchange->currency1->code_currency->name}}
                        →
                        {{$item->receiving_price}}  {{$item->direction_exchange->currency2->code_currency->name}}
                    </div>
                </td>
                <td>
                    @if($item->status == 3)
                        @if($item->scans > 0)
                            <span class="st-base-name st-handler-operator">{{ $item->operator->name}} работает с заявкой</span>
                        @else
                            <span class="st-base-name st-handler">{{$item->task_status->name}}</span>
                        @endif
                    @elseif($item->status == 2)
                        <span class="st-base-name st-pending-payment">{{$item->task_status->name}}</span>
                    @elseif($item->status == 6)
                        <span class="st-base-name st-cancel">{{$item->task_status->name}}</span>
                    @elseif($item->status == 1)
                        <span class="st-base-name st-delete">{{$item->task_status->name}}</span>
                    @elseif($item->status == 5)
                        <span class="st-base-name st-delete">{{$item->task_status->name}}</span>
                    @elseif($item->status == 4)
                        <span class="st-base-name st-success">{{$item->task_status->name}}</span>
                    @elseif($item->status == 7)
                        <span class="st-base-name st-merchant">{{$item->task_status->name}}</span>
                    @elseif($item->status == 8)
                        <span class="st-base-name st-frozen">{{$item->task_status->name}}</span>
                    @endif

                </td>
                <td>{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                <td>
                    <select class="selectpicker" onchange="if (this.value) window.location.href=this.value"  data-width="75%">
                        <option value="">--- Изменить статус заявки...</option>
                        <option value="?id={{$item->id}}&change_status=4&is_reserve=0" data-subtext="резервы снимаются сразу">Ожидает обработки</option>
                        <option value="?id={{$item->id}}&change_status=4&is_reserve=1" data-subtext="резервы снимаются после завершения">Ожидает обработки</option>
                    </select>

                    {{--@if(Transaction::call($item->id)->getReserveOut()->summa < $item->receiving_price)--}}
                        {{--<span style="color: orangered">Недостаточно резервов <br />для выполнения заявки</span>--}}
                    {{--@else--}}
                        {{--<select class="selectpicker" onchange="if (this.value) window.location.href=this.value"  data-width="75%">--}}
                           {{--<option value="">--- Изменить статус заявки...</option>--}}
                            {{--<option value="?id={{$item->id}}&change_status=4&is_reserve=0" data-subtext="резервы снимаются сразу">Ожидает обработки</option>--}}
                            {{--<option value="?id={{$item->id}}&change_status=4&is_reserve=1" data-subtext="резервы снимаются после завершения">Ожидает обработки</option>--}}
                       {{--</select>--}}
                    {{--@endif--}}
                </td>

                <td>
                    <md-button href="?actions=reject&id={{$item->id}}" class="md-raised md-danger">
                        <i class="fa fa-fw fa-trash"></i> Отклонить заявку
                    </md-button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection