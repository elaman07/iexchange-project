@extends('admin.layouts.app')

@section('title','Лог статусов заявок')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.status-log'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройки</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-order.filter') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ID пользователя</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Email пользователя</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'email_address', is_filter_search($filter, 'email_address'), ['class' => 'form-control']) }}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox2" value="1" type="checkbox" name="checkbox_full_email" @if(isset($filter['checkbox_full_email'])) checked @endif>
                                            <label class="form-check-label" for="checkbox2">Точный email</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">IP адрес</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'ip_address', is_filter_search($filter, 'ip_address'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ID заявки</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_order', is_filter_search($filter, 'id_order'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">Применить фильтры</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">Очистить фильтры</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Лог статусов заявок</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>ID заявки</th>
                    <th>Дата обновления</th>
                    <th>Пользователь</th>
                    <th>Место изменения</th>
                    <th>Старый статус</th>
                    <th>Новый статус</th>
                    <th>Курс Отдаете</th>
                    <th>Курс Получаете</th>
                    <th>Курс обмена</th>
                </tr>

                </thead>
                <tbody>
                @foreach($status_logs as $item)
                    <tr>
                        <th>
                            <a href="{{config('admin.directory')}}/applications?action=filter&id={{$item->id_task}}">{{$item->id_task}}</a>
                        </th>
                        <td>
                            <div>{{$item->created_at}}</div>
                            <small class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                        </td>
                        <td>
                            @if(isset($item->user))
                                <div>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->user_id}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                </div>
                                <small class="text-muted">{{$item->user->email}}</small>
                            @else
                                <div>
                                    Бот
                                </div>
                            @endif
                        </td>

                        <td>
                            @if(is_null($item->place_change))
                                <span class="text-muted">Не определено</span>
                            @else
                                @if($item->place_change == 1)
                                    В Админпанели
                                @elseif($item->place_change == 2)
                                    В мерчанте
                                @else
                                    На сайте
                                @endif
                            @endif
                        </td>

                        <td>
                            @if($item->old_status == 0)
                                <div class="st-base-name st-not-status">Несозданная заявка</div>
                            @else
                                <span class="st-base-name {{$item->oldStatus->class}}">{{$item->oldStatus->name}}</span>
                            @endif
                        </td>
                        <td>
                            @if($item->new_status == 0)
                                <div class="st-base-name st-not-status">Несозданная заявка</div>
                            @else
                                <span class="st-base-name {{$item->newStatus->class}}">{{$item->newStatus->name}}</span>
                            @endif
                        </td>
                        <td>
                            @if(is_null($item->in_price))
                                <span class="text-muted">Не определен</span>
                            @else
                                {{ $item->in_price }}
                            @endif
                        </td>

                        <td>
                            @if(is_null($item->out_price))
                                <span class="text-muted">Не определен</span>
                            @else
                                {{ $item->out_price }}
                            @endif
                        </td>

                        <td>
                            @if(is_null($item->course_display))
                                <span class="text-muted">Не определен</span>
                            @else
                                {{ $item->course_display }}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки логов статусов</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">Включить лог статусов</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_enabled_log_status" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_enabled_log_status') == 0) selected @endif>Нет</option>
                                    <option value="1"  @if(iEXSetting('is_enabled_log_status') == 1) selected @endif>Да</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $status_logs->links() !!}
    </div>
@endsection
