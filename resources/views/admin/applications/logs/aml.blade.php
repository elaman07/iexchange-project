@extends('admin.layouts.app')

@section('title', 'AML Log')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.checkpay-log'))

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>AML Log</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>Провайдер</th>
                    <th>ID Транзакции</th>
                    <th>ID Заявки</th>
                    <th>Код валюты</th>
                    <th>Процент риска %</th>
                    <th>Статус</th>
                    <th>Тип событии</th>
                    <th>Событие</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <th>
                                {{ $item->provider_name }}
                            </th>
                            <th>
                                <a target="_blank" href="https://www.blockchain.com/explorer/addresses/{{ \Str::lower($item->code_name) }}/{{ $item->tx_id }}">{{$item->tx_id}}</a>
                            </th>
                            <th>
                                <a href="{{config('admin.directory')}}/applications?action=filter&id={{$item->id_task}}">{{ $item->id_task }}</a>
                            </th>
                            <td>
                                {{ $item->code_name }}
                            </td>
                            <td>{{ $item->riskscore  }}%</td>
                            <td>{{ $item->status  }}</td>
                            <td>{{ $item->event_type  }}</td>
                            <td>{{ $item->event_message  }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-order.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
