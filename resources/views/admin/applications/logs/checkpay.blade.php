@extends('admin.layouts.app')

@section('title', __('admin-order.logs.log_check_payment'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.checkpay-log'))

@section('no-block-content')

    <div class="x_panel ">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-order.filter') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-order.logs.id_order') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_order', is_filter_search($filter, 'id_order'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-order.logs.event_type') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::select('event_type[]', [0 => 'Ошибка', 1 => 'Успешно'], is_filter_search($filter, 'event_type'), ['class' => 'form-control selectpicker', 'multiple' => true]) }}
                                    </div>
                                </div>

                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-order.logs.provider') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'provider', is_filter_search($filter, 'provider'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-order.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-order.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-order.logs.log_check_payment') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-order.logs.created_at') }}</th>
                    <th>{{ __('admin-order.logs.id_order') }}</th>
                    <th>{{ __('admin-order.logs.type') }}</th>
                    <th>{{ __('admin-order.logs.event') }}</th>
                    <th>{{ __('admin-order.logs.provider') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <th>
                                <a href="{{config('admin.directory')}}/applications?action=filter&id={{$item->id_task}}">{{$item->id_task}}</a>
                            </th>
                            <td>
                                @if($item->status == 1)
                                    {{ __('admin-order.logs.error') }}
                                @else
                                    {{ __('admin-order.logs.done') }}
                                @endif
                            </td>
                            <td style="width: 500px;display: block;">{{ $item->event_value  }}</td>
                            <td>
                                @if(!is_null($item->provider))
                                    {{ $item->provider }}
                                @else
                                    <span>~</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-order.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
