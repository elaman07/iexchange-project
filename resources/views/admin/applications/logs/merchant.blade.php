@extends('admin.layouts.app')

@section('title', __('admin-order.logs.log_merchant'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.merchant-log'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f"> {{ __('admin-order.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-order.filter') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3"> {{ __('admin-order.logs.id_order') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_order', is_filter_search($filter, 'id_order'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"> {{ __('admin-order.logs.event_type') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::select('event_type[]', [1 => 'Переадресация', 2 => 'Успешно', 4 => 'Отмена'], is_filter_search($filter, 'event_type'), ['class' => 'form-control selectpicker', 'multiple' => true]) }}
                                    </div>
                                </div>

                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3"> {{ __('admin-order.logs.provider') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'provider', is_filter_search($filter, 'provider'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary"> {{ __('admin-order.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn"> {{ __('admin-order.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-order.logs.log_merchant') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th> {{ __('admin-order.logs.created_at') }}</th>
                    <th> {{ __('admin-order.logs.id_order') }}</th>
                    <th> {{ __('admin-order.logs.type') }}</th>
                    <th> {{ __('admin-order.logs.event') }}</th>
                    <th> {{ __('admin-order.logs.provider') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <th>
                                <a href="{{config('admin.directory')}}/applications?action=filter&id={{$item->id_order}}">{{$item->id_order}}</a>
                            </th>
                            <td>
                                @if($item->event_type == 1)
                                    {{ __('admin-order.logs.redirect') }}
                                @elseif($item->event_type == 2)
                                    {{ __('admin-order.logs.done') }}
                                @elseif($item->event_type == 3)
                                    {{ __('admin-order.logs.done') }}
                                @elseif($item->event_type == 4)
                                    {{ __('admin-order.logs.cancel') }}
                                @endif
                            </td>
                            <td>{!! $item->event_value !!}</td>
                            <td>
                                @if(!is_null($item->provider))
                                    {{ $item->provider }}
                                @else
                                    <span>~</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br /> {{ __('admin-order.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-order.logs.settings_log_merchant') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-order.logs.enable_log_merchant') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_enabled_log_merchant" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_enabled_log_merchant') == 0) selected @endif>{{ __('admin-order.no') }}</option>
                                    <option value="1"  @if(iEXSetting('is_enabled_log_merchant') == 1) selected @endif>{{ __('admin-order.yes') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-order.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-order.back') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
