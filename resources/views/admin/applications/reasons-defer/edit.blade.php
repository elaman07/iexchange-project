@extends('admin.layouts.app')

@section('title','Изменить причину')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.reasons-defer.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/applications/reasons-defer/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group">
                <div class="col-md-8">
                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название причины') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}" value="{{ $item->getTranslation('name', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/applications/reasons-defer') }}">Назад</md-button>
        </div>
    </form>

@endsection
