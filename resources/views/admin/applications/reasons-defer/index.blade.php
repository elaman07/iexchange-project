@extends('admin.layouts.app')

@section('title','Причины отложенных заявок')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.reasons-defer'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#add" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить причину</span>
    </md-button>
@endsection

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Название причины</th>
            <th>Посл. изменение</th>
            <th></th>
        </tr>

        </thead>
        <tbody>
        @foreach($items as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td>{{ $item->updated_at }}</td>
                <td style="width: 200px;">
                    @if(in_array($item->id, [1,2,3,4,5]))
                        <div class="col-md-12">
                            <a href="reasons-defer/{{$item->id}}/edit" class="btn btn-info btn-sm">Изменить</a>
                        </div>

                    @else
                        <div class="col-md-6">
                            <a href="reasons-defer/{{$item->id}}/edit" class="btn btn-info btn-sm">Изменить</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['reasons-defer.destroy', $item->id] ]) !!}
                            {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Модальное окно для добавления ссылки -->
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form action="?" method="POST" class="form-horizontal">
                <input type="hidden" name="export_users" value="enable">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Добавить новую причину</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group col-md-12 multi-language-form-group">
                            <div class="control-label-br">
                                <div layout="row">
                                    <div>{{ __('Название причины') }}</div>
                                    <span flex></span>
                                    <ul class="nav nav-pills lang-tabs">
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                    {{ $form_lang_key }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                    <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}">
                                </div>
                            @endforeach
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Добавить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
