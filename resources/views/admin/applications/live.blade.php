@extends('admin.layouts.app')

@section('title','Live заявки')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.live'))

@section('content')
    <div ng-controller="LiveTaskController">

        <div class="alert alert-info text-center" ng-if="live.data.length == 0">
            Не найдено активных заявок
        </div>
        <table class="table table-border-2" ng-if="live.data.length > 0">
            <thead>
            <tr>
                <th>ID</th>
                <th>Направление</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Действие</th>
            </tr>

            </thead>
            <tbody>
            <tr ng-repeat="item in live.data">
                <td ng-bind="item.id"></td>
                <td>
                    <b>[[item.name | html]]</b>
                    <div style="color: #666;">[[item.amount | html]]</div>

                </td>
                <td>
                    <span ng-if="item.scans == 0 && item.status_int == 3" class="st-base-name st-handler">[[item.status]]</span>
                    <span ng-if="item.scans == 0 && item.status_int == 7" class="st-base-name st-merchant">[[item.status]]</span>

                    <div ng-if="item.scans > 0">
                        <span class="st-base-name st-handler-operator">
                             [[item.scans_name]] принял
                        </span>
                    </div>

                </td>
                <td ng-bind="item.created"></td>
                <td style="width: 150px;">
                    <div ng-if="item.scans == item.auth_id">
                        <md-button class="md-raised" href="{{config('admin.directory').'/applications/details/'}}[[item.id]]">Продолжить</md-button>
                    </div>

                    <div ng-if="item.scans != item.auth_id">
                        <md-button class="md-raised md-primary" href="{{config('admin.directory').'/applications/details/'}}[[item.id]]">Выполнить</md-button>
                    </div>
                </td>
            </tr>
            </tbody>

        </table>

    </div>
@endsection