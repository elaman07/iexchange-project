@extends('admin.layouts.app')

@section('title','Заявки на выплату')

@section('breadcrumbs', Breadcrumbs::render('admin.applications.payment_bonuses'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>Настройки</span>
        </md-button>
    @endcan

@endsection

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-order.filter') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ID Пользователя</label>
                                    <div class="col-md-6">
                                        {!! Form::input('text', 'from_value', is_filter_search($filter, 'from_value'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">E-mail адрес</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'email_address', is_filter_search($filter, 'email_address'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Имя</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'user_name', is_filter_search($filter, 'user_name'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                            </div>

                            <div flex="50">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Номер счета</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'score', is_filter_search($filter, 'score'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Валюта</label>
                                    <div class="col-md-6">
                                        {!! Form::select('currencies[]', $currencies, is_filter_search($filter, 'currencies'),
                                            ['class' => 'selectpicker', 'data-width' => '100%', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Статус</label>
                                    <div class="col-md-6">
                                        {!! Form::select('status[]', $statuses, is_filter_search($filter, 'status'),
                                            ['class' => 'selectpicker', 'data-width' => '100%', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Дата создания</label>
                                    <div class="col-md-6">
                                        От: <input  value="{{ is_filter_search($filter, 'from_created_at') }}" name="from_created_at" style="width: 100px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        До: <input  value="{{ is_filter_search($filter, 'to_created_at') }}" name="to_created_at" style="width: 100px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">Применить фильтры</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">Очистить фильтры</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="pagination-right">
        {!! $withdrawal->links() !!}
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Заявки на выплату</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table  class="table table-border-2">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Валюта</th>
                    <th>Клиент</th>
                    <th>Статус</th>
                    <th>Выполнил</th>
                    <th>Дата создания</th>
                </tr>

                </thead>
                <tbody>

                @foreach($withdrawal as $item)
                    <tr @if($item->status == 0) class="important-error" @endif>
                        <td>{{$item->id}}</td>
                        <td>
                            <a href="{{config('admin.directory')}}/applications/payment_bonuses/{{$item->id}}">
                                <b>{{$item->currency->payment->name}} {{$item->currency->code_currency->name}}</b>
                            </a>

                            @if($item->is_black_list == 1)
                                <div style="font-size: 11px;max-width: 300px" class="text-danger">{{ $item->black_list_text }}</div>
                            @endif
                        </td>

                        <td>
                            {{$item->user->name}}
                        </td>
                        <td>
                            @if($item->status == 0)
                                @if(is_null($item->verified_at) and iEXSetting('is_verified_payouts_bonus') == 1)
                                    <span class="st-base-name st-error">Не подтвержден вывод</span>
                                @else
                                    <span class="st-base-name st-handler">Ожидает обработки</span>
                                @endif
                            @else
                                <span class="st-base-name st-success">Выплачено</span>
                            @endif
                        </td>
                        <td>
                            @if(!empty($item->id_manager))
                                <div>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_manager}}&sorting=id"><b>{{$item->manager->name}}</b></a>
                                </div>
                                <small>{{$item->manager->email}}</small>
                            @else
                                <span style="color: #999">Нет данных</span>
                            @endif
                        </td>

                        <td>{{\Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans()}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <div class="pagination-right">
        {!! $withdrawal->links() !!}
    </div>



    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">Количество записей на страницу</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="payment_bonuses_paginate" value="{{ iEXSetting('payment_bonuses_paginate', 20) }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
