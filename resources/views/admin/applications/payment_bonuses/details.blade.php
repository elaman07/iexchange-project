@extends('admin.layouts.app')

@section('title','Информация о выплате №'.$item->id)

@section('breadcrumbs', Breadcrumbs::render('admin.applications.payment_bonuses'))
@section('add-to-toolbar')
    @if($item->is_black_list == 1)
        <div class="two-column-toolbar">
            <div class="item-detail item-detail-error">
                {{ $item->black_list_text }}
            </div>
        </div>
    @endif
@endsection

@section('custom-md-content')
    <md-content style="height: 100%;">
    <div class="widget-application-layout" layout="row" layout-xs="column" layout-align="space-around stretch">
        <div flex="25" flex-xs="100">
            <div class="widget-application">
                <div class="widget-application-wrap">
                    <div class="widget-application-head">Заявка создана</div>
                    <div class="widget-application-title widget-application-datetime">
                        {{\Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y, H:i')}}
                    </div>
                    <div class="widget-application-foot">
                        {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}
                    </div>
                </div>
            </div>
        </div>

        <div flex="25"  flex-xs="100">
            <div class="widget-application">
                <div class="widget-application-wrap">
                    <div class="widget-application-head">Номер выплаты</div>
                    <div class="widget-application-title widget-application-id">
                        {{$item->big_id}}
                    </div>
                </div>
            </div>
        </div>

        <div flex="25"  flex-xs="100">
            <div class="widget-application">
                <div class="widget-application-wrap">
                    <div class="widget-application-head">Статус выплаты</div>
                    <div class="widget-application-title">
                        @if($item->status == 0)
                            @if(is_null($item->verified_at) and iEXSetting('is_verified_payouts_bonus') == 1)
                                <span class="st-error-color">Не подтвержден вывод</span>
                            @else
                                <span class="st-handler-color">Ожидает обработки</span>
                            @endif
                        @elseif($item->status == 1)
                            <span class="st-success-color">Успешно выплачено</span>
                       @endif
                    </div>
                </div>
            </div>
        </div>

        <div flex="25"  flex-xs="100">
            <div class="widget-application">
                <div class="widget-application-wrap">
                    <div class="widget-application-head">IP Адрес</div>
                    <div class="widget-application-title widget-application-ip">
                        {{$item->ip}}
                    </div>
                    <div class="widget-application-foot">
                        <a data-toggle="modal" data-target="#info_ip">Информация по IP</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


    <div class="application-detail-data">
        <!-- В случае возникнования проблем -->
        @if(flash()->message)
            <div class="text-center {{ flash()->class }}">
                {{ flash()->message }}
            </div>
        @endif

        <div layout="row" layout-xs="column" layout-align="space-around stretch">
            <div flex="33" flex-xs="100">
                <div class="x_panel">
                    <div class="x_content">
                        <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                            <md-tab label="О Клиенте">
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        E-mail
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$item->user->email}}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Имя
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$item->user->name}}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Последний визит
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{ \Illuminate\Support\Carbon::parse($item->user->last_activity)->diffForHumans() }}
                                    </div>
                                </div>
                            </md-tab>
                        </md-tabs>
                    </div>
                </div>
            </div>
            <div flex="33" flex-xs="100" class="padding-right">
                <div class="x_panel">
                    <div class="x_content">
                        <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                            <md-tab label="Детали выплаты">
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-6">
                                        Платежная система
                                    </div>

                                    <div class="application-detail-value col-md-6">
                                        {{$item->currency->payment->name}} {{$item->currency->code_currency->name}}
                                    </div>
                                </div>
                                <md-divider></md-divider>

                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-6">
                                        Счет
                                    </div>

                                    <div class="application-detail-value col-md-6">
                                        {{$item->score}}
                                    </div>
                                </div>
                                <md-divider></md-divider>

                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-6">
                                        Реферальные
                                    </div>

                                    <div class="application-detail-value col-md-6">
                                        {{ $item->balance_referral }}  {{$item->currency->code_currency->sign}}
                                    </div>
                                </div>
                                <md-divider></md-divider>

                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-6">
                                        Бонусы за обмен
                                    </div>

                                    <div class="application-detail-value col-md-6">
                                        {{ $item->balance_reward }}  {{$item->currency->code_currency->sign}}
                                    </div>
                                </div>
                                <md-divider></md-divider>

                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-6">
                                        Итоговая сумма выплаты
                                    </div>

                                    <div class="application-detail-value col-md-6" style="text-align: center;font-size: 14px;font-weight: 500">
                                        {{ $item->balance_referral + $item->balance_reward}}  {{$item->currency->code_currency->sign}}
                                    </div>
                                </div>
                            </md-tab>
                        </md-tabs>
                    </div>
                </div>
            </div>
            <div flex="33" flex-xs="100">
                <div class="x_panel">
                    <div class="x_content">
                        <md-tabs class="application-detail-tabs" md-border-bottom>
                            <md-tab label="История выплат">

                                @if($histories->count() == 0)
                                    <div layout="column" layout-align="center center" style="    height: 100%;">
                                        <div class="not-data" style="color: #999999">
                                            Ничего не найдено
                                        </div>
                                    </div>
                                @else
                                    <table class="table table-border-2">
                                        <thead>
                                        <tr>
                                            <th>Событие</th>
                                            <th>Выплачено</th>
                                            <th>Остаток</th>
                                            <th>Менеджер</th>
                                            <th>Изменено</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($histories as $history)
                                            <tr>
                                                <td style="width: 200px">
                                                    {!! $history->text !!}
                                                </td>
                                                <td>
                                                    {{$history->amount}} {{$item->currency->code_currency->sign}}
                                                </td>
                                                <td>
                                                    {{$history->remainder}} {{$item->currency->code_currency->sign}}
                                                </td>
                                                <td>
                                                    @if(!empty($history->id_manager))
                                                        <div>
                                                            <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$history->id_manager}}&sorting=id"><b>{{$history->manager->name}}</b></a>
                                                        </div>
                                                        <small>{{$history->manager->email}}</small>
                                                    @else
                                                        <span style="color: #999">Нет данных</span>
                                                    @endif
                                                </td>
                                                <td>{{$history->created_at->format('d m Y, H:i')}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </md-tab>
                        </md-tabs>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </md-content>

    @if($item->status == 0 and $item->ip != null)
        <div class="footer-detail">
            <div layout="row" layout-xs="column">
                <span flex=""></span>
                    <div>
                        <div layout="row" layout-align="start center">

                            @if($hasBalance == 1)
                                <div class="text-danger">
                                    Недостаточно средств для выплаты. <br />
                                    На счету: <b>{{ $apiBalance }} {{ $item->currency->code_currency->name }}</b>
                                </div>
                            @elseif($hasBalance == 0)
                                <span class="text-muted">На балансе: {{ $apiBalance }} {{ $item->currency->code_currency->name }}</span>
                                <md-button class="md-raised md-success-3" type="button" data-toggle="modal" data-target="#success_merchant" ng-disabled="isButtonDisabled">
                                    Оплатить и завершить
                                </md-button>
                            @endif

                            @if(!is_null($item->verified_at) or iEXSetting('is_verified_payouts_bonus') == 0)
                                <md-button class="md-raised md-success-2" type="button" data-toggle="modal" data-target="#success" ng-disabled="isButtonDisabled">
                                    Выплата произведена
                                </md-button>
                            @endif

                            <md-button ng-href="?actions=failed" class="md-raised md-danger">
                                Удалить
                            </md-button>
                        </div>
                    </div>
            </div>
        </div>

        <form action="?actions=success" method="POST" class="form-horizontal">
            @csrf
            <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header ui-dialog-titlebar">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <span class="ui-dialog-title" id="myModalLabel">Оплатить бонусы</span>
                        </div>
                        <div class="modal-body">

                            <div class="alert alert-success">Вы действительно хотите выполнить заявку на выплату вознаграждений?</div>

                        </div>

                        <div class="clearfix"></div>
                        <br />
                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-primary">Выполнить</md-button>
                            <md-button type="button" data-dismiss="modal">Отменить</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form action="?actions=success" method="POST" class="form-horizontal">
            @csrf
            <input type="hidden" name="type" value="merchant">
            <div class="modal fade" id="success_merchant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header ui-dialog-titlebar">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <span class="ui-dialog-title" id="myModalLabel">Оплатить и завершить</span>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-success">Вы действительно хотите оплатить заявку в автоматическом режиме?</div>
                        </div>

                        <div class="clearfix"></div>
                        <br />
                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-primary">Оплатить</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    @endif
@endsection
