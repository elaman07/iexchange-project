@extends('admin.layouts.app')

@section('title','Информация о заявке №'.$detail->id)

@section('custom-toolbar-right')

    @if(is_string($bonus))
        <div class="bonus-user">Бонус: {{$bonus}}</div>
    @endif

    <div class="toolbar-status">
        {{$detail->task_status->name}}
    </div>

@endsection

@section('add-to-toolbar')

    @if($is_hold)
        <div class="two-column-toolbar">
            <div class="item-detail item-detail-error">
                Транзакция заморожена на 25 часов, осталось {{ $hold_timeout }}
            </div>
        </div>
    @endif

    @if($detail->double_withdrawal == 1 and $detail->status != 4)
        <div class="two-column-toolbar" style="background-color: #ffd7d7;">
            <div class="item-detail item-detail-error">
                Попытка двойного снятия при автовыплате.<br /> Проверьте транзакцию, возможно средства уже отправлены клиенту.
            </div>
        </div>
    @endif

    @if(config('admin.is_demo_mode') == true)
        <div class="two-column-toolbar">
            <div class="item-detail item-detail-error">
               Выполнение недоступно в демо режиме
            </div>
        </div>
    @endif

    @if($detail->status == 5 and isset($detail->tasks_rejection_status))
        <div class="two-column-toolbar">
            <div class="item-detail item-detail-error">
                <b>Причина:</b>
                {{ $detail->tasks_rejection_status->name }}
            </div>
        </div>
    @elseif(isset($validityOrder) and in_array($validityOrder['level'], [3]))
        @if($validityOrder['level'] == 1)
            <div class="two-column-toolbar">
                <div class="item-detail item-detail-error">
                    <b>Внимание</b>:
                    Выполнив заявку вы будете в убытке на {{ $validityOrder['amount'] }} {{ $codeOut->name }}
                </div>
            </div>
        @endif
    @endif

    @if($checkPayment)
        <div class="two-column-toolbar">
            @if($walletInfoIn != null)
                <div class="item-detail">
                    <div class="amount">
                        @if($detail->status == 3 or $detail->status == 7)
                            @if($walletInfoIn->amount < $detail->give_price and empty($detail->excode))
                                <a href="#" data-toggle="modal" data-target="#recount2">
                                    {{iex_number_format($walletInfoIn->amount, 8)}} {{$codeIn->name}}
                                </a>
                            @else
                                @if(empty($detail->excode))
                                    {{iex_number_format($walletInfoIn->amount, 8)}} {{$codeIn->name}}
                                @else
                                    {{ $walletInfoIn->amount }} {{$walletInfoIn->currency}}
                                @endif
                            @endif
                        @else
                            {{iex_number_format($walletInfoIn->amount, 8)}} {{$codeIn->name}}
                        @endif
                    </div>

                    @if($walletInfoIn->amount >= $detail->give_price)
                        @if(empty($detail->excode))
                            <div ng-if="confirmations != null" style="color: #1dc569">
                                Кол. подтверждений: <span ng-bind="confirmations"></span>
                            </div>
                            <div ng-if="confirmations == null" style="color: #1dc569">Кол. подтверждений: {{$walletInfoIn->confirmations}}</div>
                        @endif
                    @else
                        <div class="text-danger">
                            Оплата не в полном объеме: <br />
                            @if(empty($detail->excode))
                                <b>Нехватает:</b> {{number_format($detail->give_price - $walletInfoIn->amount, 8,'.','')}}
                                {{$codeIn->name}}
                            @else
                                <b>Нехватает:</b> {{ $detail->give_price - $walletInfoIn->amount }}
                                {{ $codeIn->name }}
                            @endif
                        </div>
                    @endif
                </div>
            @else
                @if(!$preview or $detail->status == 8)
                    <div class="check-payment-button">
                        <md-button class="md-warn md-raised" ng-click="checkPayment('{{$paymentIn->name}}','{{$detail->id}}')">Проверить оплату</md-button>
                    </div>
                @endif
            @endif
        </div>
    @endif
@endsection

@section('ng-controller','ng-controller=TickerController')

@section('custom-md-content')
    <md-content style="height: 100%;">

        <div ng-if="isButton">
            <div class="progress-please-wait"></div>
            <div layout="column" layout-align="center center" class="progress-please-wait-scroll">
                <md-progress-circular class="md-hue-2" md-diameter="40px"></md-progress-circular>
            </div>
        </div>

        @if(isset($taskInfo) and $taskInfo->is_scam == 1)
            @if($taskInfo->is_local_scam == 1)
                <div class="isScam">
                    <b>Внимание:</b> Ранее клиент привлекался за мошенничество
                </div>
            @else
                <div class="isScam">
                    <b>Внимание:</b>  Данные клиента найдены в черном списке BestChange
                </div>
            @endif
        @endif

        @if($detail->status == 4 and $detail->started_at != null)
            <div class="speed-notification-order">
                Время выполнения:
                <span> {{$leadTime}}</span>
            </div>
        @endif

        <div class="widget-application-layout" layout="row" layout-xs="column" layout-align="space-around stretch" ng-cloak="">
            <div flex="25" flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">Заявка создана</div>
                        <div class="widget-application-title widget-application-datetime">
                            {{\Illuminate\Support\Carbon::parse($detail->created_at)->translatedFormat('d M Y, H:i')}}
                        </div>
                        <div class="widget-application-foot">
                            {{ \Illuminate\Support\Carbon::parse($detail->created_at)->diffForHumans() }}
                        </div>
                    </div>
                </div>
            </div>

            <div flex="25"  flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">Номер заявки</div>
                        <div class="widget-application-title widget-application-id">
                            {{$detail->id}}
                        </div>
                        <div class="widget-application-foot">
                            @if(isset($detail->transaction))
                                <a target="_blank" href="{{config('app.url')}}/check/{{$detail->transaction->transaction}}">{{$detail->transaction->transaction}}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div flex="25"  flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        @if(is_null($detail->deleted_at) and in_array($detail->status, explode(',', iEXSetting('inactive_order_status_pending'))))
                            <div class="widget-application-head">Изменить статус заявки</div>
                            <div class="widget-application-title text-center" style="margin-top:10px">
                                <select class="selectpicker" onchange="if (this.value) window.location.href=this.value"  data-width="100%">
                                    <option value="">--- Изменить статус заявки ---</option>
                                    @foreach(\App\Models\TaskStatus::whereIn('id', [3, 5])->get() as $item)
                                        <option value="?actions=arhive&change_status={{$item->id}}" @if($item->id == 2) disabled @endif>
                                            {{$item->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @else
                            @if($detail->status == 8)
                                <div class="widget-application-head">Изменить статус заявки</div>
                                <div class="widget-application-title text-center" style="margin-top:10px">
                                    <select class="selectpicker" onchange="if (this.value) window.location.href=this.value" data-width="100%">
                                        <option value="">--- Изменить статус заявки...</option>
                                        <option value="?st=postponed&act=expectation&is_reserve=0" data-subtext="резервы снимаются сразу">Ожидает обработки</option>
                                        <option value="?st=postponed&act=expectation&is_reserve=1" data-subtext="резервы снимаются после завершения">Ожидает обработки</option>
                                        <option value="?st=postponed&act=reject">Отклонить заявку</option>
                                    </select>
                                </div>
                            @else

                                <div class="widget-application-head">Статус заявки</div>
                                <div class="widget-application-title">
                                    <span style="color: {{$detail->task_status->color}}">{{ $detail->task_status->name }}</span>
                                </div>
                                <div class="widget-application-foot">
                                    @if($detail->status != 4)
                                        @if($detail->scans > 0)
                                            @if(!$preview)
                                                @if(iEXSetting('is_allow_transfer_operator'))
                                                    <a ng-click="transferToAnother($event,'{{$detail->id}}')">{{$detail->operator->name}} работает с заявкой</a>
                                                @else
                                                    {{$detail->operator->name}} работает с заявкой
                                                @endif
                                            @else
                                                <span>{{$detail->operator->name}} работает с заявкой</span>
                                            @endif
                                        @else
                                            <span>Операторы не открывали заявку</span>
                                        @endif
                                    @else
                                        @if(isset($detail->manager))
                                            <span>{{$detail->manager->name}} выполнил заявку</span>
                                        @endif
                                    @endif
                                </div>

                            @endif
                        @endif
                    </div>
                </div>
            </div>

            <div flex="25"  flex-xs="100">
                <div class="widget-application">
                    <div class="widget-application-wrap">
                        <div class="widget-application-head">IP Адрес</div>
                        <div class="widget-application-title widget-application-ip">
                            {{$detail->ip}}
                        </div>
                        <div class="widget-application-foot">
                            <a data-toggle="modal" data-target="#info_ip">Информация по IP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        @if($detail->is_autopay_off == 1 and !in_array($detail->status, [4,5]))
            <div class="alert alert-warning" style="margin-top: 10px; margin-bottom: 5px; text-align: center; line-height: 2; padding: 10px;">
                <div>Автовыплата отключена</div>
                <a href="{{ admin_base_path('/applications/autopayment-log') }}" style="font-size: 12px;">Открыть лог событий</a>
            </div>
        @endif

        <div class="application-detail-data">
            <div>
                <div class="x_panel" style="margin-bottom: 3px">
                    <div class="x_content">

                        <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                            @if($detail->status == 4)
                                <md-tab label="История резерва">
                                    @foreach(\App\Models\ReserveLog::where('id_task', $detail->id)->get() as $item)
                                        @if($item->type == 'minus')
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-7">
                                                    <span style="color: red">Уменьшен резерв <b>{{$paymentOut->name}}</b>:</span>
                                                </div>
                                                <div class="application-detail-value col-md-5">
                                                    <span style="color: red;">-{{$item->summa}} {{$codeOut->name}}</span>
                                                </div>
                                            </div>
                                        @else
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-7">
                                                    <span style="color: green;">Пополнен резерв <b>{{$paymentIn->name}}</b>:</span>
                                                </div>
                                                <div class="application-detail-value col-md-5">
                                                    <span  style="color: green;">+{{$item->summa}} {{$codeIn->name}}</span>
                                                </div>
                                            </div>
                                        @endif
                                        <md-divider></md-divider>
                                    @endforeach
                                </md-tab>
                            @endif
                            @if(!empty($transactions))
                                <md-tab label="О транзакции">

                                    @if(isset($transactions['codepro']) and $transactions['codepro'] == true)
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                Код протекции
                                            </div>

                                            <div class="application-detail-value col-md-7 text-danger">
                                                Перевод защищен кодом протекции.
                                                Чтобы получить такой перевод, пользователю необходимо ввести код протекции.
                                            </div>
                                        </div>
                                    @endif

                                    @if(isset($transactions['unaccepted']) and $transactions['unaccepted'] == true)
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                unaccepted
                                            </div>

                                            <div class="application-detail-value col-md-7 text-danger">
                                                перевод еще не зачислен на счет получателя.
                                                Чтобы его принять, получателю нужно совершить дополнительные действия.
                                                Например, освободить место на счете, если достигнут лимит доступного остатка.
                                                Или указать код протекции, если он необходим для получения перевода.
                                            </div>
                                        </div>
                                    @endif


                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Дата создания
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$transactions['date']}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Сумма транзакции
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$transactions['amount']}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Валюта перевода
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$transactions['currency']}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Получатель
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$transactions['to']}}
                                        </div>
                                    </div>
                                </md-tab>
                            @endif

                            <!-- Для Exmo  и kuna -->
                            @if($detail->check_excode == 2)
                                <md-tab label="О транзакции">
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Дата создания
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$detail->history_excode->created_at}}
                                        </div>
                                    </div>

                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Операция
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            @if($detail->history_excode->type == 'load')
                                                <span style="color: #1dc569">+ Ввод</span>
                                            @else
                                                <span class="text-danger">- Вывод</span>
                                            @endif
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Сумма
                                        </div>

                                        <div class="application-detail-value col-md-7 price">
                                            @if($detail->history_excode->type == 'load')
                                                {{$detail->history_excode->amount}}
                                            @else
                                                - {{$detail->history_excode->amount}}
                                            @endif
                                        </div>
                                    </div>

                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Валюта
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$detail->history_excode->currency}}
                                        </div>
                                    </div>

                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Статус
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            @if($detail->history_excode->amount >= $detail->give_price)
                                                <span style="color: #1dc569">Зачислено</span>
                                            @else
                                                <span class="text-danger">Оплата не в полном объеме<br />

                                                    <b>Нехватает:</b> {{number_format($detail->give_price - $detail->history_excode->amount, 4)}}
                                                    {{$detail->history_excode->currency}}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            EX-Code
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$detail->history_excode->code}}
                                        </div>
                                    </div>
                                </md-tab>
                            @endif
                            <md-tab label="Детали">
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Курс
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->course_display ?? 'Не определена'}}
                                    </div>
                                </div>

                                <md-divider></md-divider>

                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Код заявки
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {!! $detail->unique_security_code ?? '<small class="text-danger">Не указан</small>' !!}
                                    </div>
                                </div>
                                <md-divider></md-divider>

                                <!-- Дата пересчета -->
                                @if(!is_null($lastRecalcDate))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Дата посл. пересчета
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ \Illuminate\Support\Carbon::parse($lastRecalcDate)->translatedFormat('j F Y, H:i') }}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>
                                @endif

                                @if(is_array($receive_wallet))
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            @if(isset($account_number_field))
                                                {{ $account_number_field }}
                                            @else
                                                Кошелек
                                            @endif
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ $receive_wallet['address'] }}
                                        </div>
                                    </div>
                                @else
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            @if(isset($account_number_field))
                                                {{ $account_number_field }}
                                            @else
                                                Кошелек
                                            @endif
                                        </div>
                                        <div class="application-detail-value col-md-7">
                                            {{ $receive_wallet }}
                                        </div>
                                    </div>
                                @endif

                                <!-- Приватные ключи доступны только разрабочикам -->
                                @role('Разработчик')
                                @if(isset($privateKeyAddress) and config('admin.is_demo_mode') == false)
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Приватный ключ
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ $privateKeyAddress }}
                                        </div>
                                    </div>
                                @endif
                                @endrole

                                @if($detail->is_bot == 1)
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Автовыплата
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            <a href="?actions=switch_to_manual_mode">Перевести в ручной режим</a>
                                        </div>
                                    </div>
                                @endif

                                @if(count($tasks_fields['many']) > 0)
                                    @foreach($tasks_fields['many'] as $value)
                                        <md-divider></md-divider>
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">{{ $value->field_name }}</div>
                                            <div class="application-detail-value col-md-7">{{ $value->field_value }}</div>
                                        </div>
                                    @endforeach
                                @endif

                                @if(!empty($taskInfo->num_transaction))
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Номер перевода
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ $taskInfo->num_transaction }}
                                        </div>
                                    </div>
                                @endif

                            @if(!empty($taskInfo->note_tx))
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Примечание к транзакции
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ $taskInfo->note_tx }}
                                        </div>
                                    </div>
                                @endif
                            </md-tab>
                            <md-tab label="О Клиенте">
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        ID
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        <a href="{{ admin_base_path('/account/users/?id='.$detail->user->id) }}">{{$detail->user->id}}</a>
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        E-mail
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->user->email}}
                                    </div>
                                </div>
                                @if(!empty($detail->phone))
                                    <md-divider></md-divider>
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Номер телефона
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$detail->phone}}
                                        </div>
                                    </div>
                                @endif
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Имя
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->user->name}}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Всего обменов
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{ \App\Models\Task::where('id_user', $detail->user->id)->whereIn('status',[4,5])->count() }}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        От партнера
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        @if(isset($detail->user->fromReferral))
                                            <div class="text-muted">
                                                <a href="{{ admin_base_path('/account/users/?id='.$detail->user->fromReferral->referral_link->user->id) }}">{{ $detail->user->fromReferral->referral_link->user->name }}</a>
                                            </div>
                                        @else
                                            <div class="text-muted">Не найдено</div>
                                        @endif
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Дата регистрации
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{$detail->user->created_at}}
                                    </div>
                                </div>
                                <md-divider></md-divider>
                                <div class="application-detail-item row">
                                    <div class="application-detail-key col-md-5">
                                        Последний визит
                                    </div>

                                    <div class="application-detail-value col-md-7">
                                        {{ \Illuminate\Support\Carbon::parse($detail->user->last_activity_at)->diffForHumans() }}
                                    </div>
                                </div>
                            </md-tab>
                            @if(!is_null($taskReport) and $taskReport->profit_rub > 0)
                                <md-tab label="Прибыль">
                                    @if($taskReport->profit > 0)
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                Указанный % прибыли
                                            </div>

                                            <div class="application-detail-value col-md-7">
                                                {{ $taskReport->profit }}%
                                            </div>
                                        </div>
                                        <md-divider></md-divider>
                                    @endif
                                    @if($taskReport->profit_s > 0)
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                Указанная сумма прибыли
                                            </div>

                                            <div class="application-detail-value col-md-7">
                                                {{ $taskReport->profit_s }} {{ $codeIn->name }}
                                            </div>
                                        </div>
                                        <md-divider></md-divider>
                                    @endif
                                        @if($taskReport->profit_rub > 0)
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-5">
                                                    Прибыль от заявки (Без учета бонусов)
                                                </div>

                                                <div class="application-detail-value col-md-7">
                                                    {{ $taskReport->profit_rub }} руб.
                                                </div>
                                            </div>
                                            <md-divider></md-divider>
                                        @endif

                                        @if($totalProfit > 0 and $detail->status == 4)
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-5">
                                                    Прибыль от заявки (С вычетом бонусов)
                                                </div>

                                                <div class="application-detail-value col-md-7">
                                                    {{ $totalProfit }} руб.
                                                </div>
                                            </div>
                                            <md-divider></md-divider>
                                        @endif
                                </md-tab>
                            @endif
                            @if($referral == true)
                                <md-tab label="Реферальное начисление">
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            ID
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$referral->user_admin->id}}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            E-mail
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{$referral->user_admin->email}}
                                        </div>
                                    </div>

                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-5">
                                            Начисление
                                        </div>

                                        <div class="application-detail-value col-md-7">
                                            {{ $referral_bonus }}
                                        </div>
                                    </div>

                                    @if(isset($referral->current_percent) and $referral->current_percent > 0)
                                        <md-divider></md-divider>
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                Процент от заявки
                                            </div>

                                            <div class="application-detail-value col-md-7">
                                                {{ $referral->current_percent }} %
                                            </div>
                                        </div>
                                    @endif

                                    @if(isset($referral->fixed_bonus) and $referral->fixed_bonus > 0)
                                        <md-divider></md-divider>
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-5">
                                                Фиксированный бонус
                                            </div>

                                            <div class="application-detail-value col-md-7">
                                                {{ $referral->fixed_bonus }} руб
                                            </div>
                                        </div>
                                    @endif
                                </md-tab>
                            @endif
                        </md-tabs>
                    </div>
                </div>
            </div>
            <div layout="row" layout-xs="column" layout-align="space-around stretch">
                <div flex="50" flex-xs="100" class="padding-right">
                    <div class="x_panel">
                        <div class="x_content">
                            <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                                <md-tab label="Отдает клиент">
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Платежная система
                                        </div>

                                        <div class="application-detail-value col-md-6">
                                            {{ $paymentIn->name }} {{ $codeIn->name }}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Сумма
                                        </div>

                                        <div class="application-detail-value col-md-6 price">
                                            @if($detail->status == 3 or $detail->status == 7)
                                                @if($detail->skip_reserve == 0)
                                                    <a href="#" data-toggle="modal" data-target="#recount">
                                                        {{ $display_in_price }} {{  $codeIn->name }}
                                                    </a>
                                                @else
                                                    {{ $display_in_price }} {{  $codeIn->name }}
                                                @endif
                                            @else
                                                <a href="#" data-toggle="modal" data-target="#history_recalculation">
                                                    {{ $display_in_price }} {{  $codeIn->name }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>

                                    @if($isAllowTransferReserve)
                                        <md-divider></md-divider>
                                        <div class="detail-add-reserve text-center">
                                            @if(convert_to_rub($codeIn->name, $detail->give_price) > iEXSetting('give_transfer_reserve'))
                                                <md-button class="md-warn" ng-href="?actions=skip_reserve">Зачислить
                                                    <b>{{ $display_in_price }} {{  $codeIn->name }}</b> в резерв
                                                </md-button>
                                            @else
                                                <div style="padding: 10px;color: #797979; font-size: 13px;">В резерв разрешено зачислять от {{ iEXSetting('give_transfer_reserve') }} руб.</div>
                                            @endif
                                        </div>
                                    @endif

                                    @if(strlen(strip_tags($detail->from_shot)) > 0)
                                        <md-divider></md-divider>
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Со счета
                                            </div>

                                            <div class="application-detail-value col-md-6">
                                                <div>{{$detail->from_shot}}</div>
                                                @if($isVerifiedCard)
                                                    <small style="color:#3c763d;">Верифицирован</small>
                                                @endif
                                            </div>
                                        </div>
                                    @endif

                                    @if(count($tasks_fields['currency_in']) > 0)
                                        @foreach($tasks_fields['currency_in'] as $item)
                                            <md-divider></md-divider>
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-6">
                                                    {{ $item->field_name }}
                                                </div>
                                                <div class="application-detail-value col-md-6">
                                                    @if(!is_null($item->field_value))
                                                        {{ $item->field_value }}
                                                    @else
                                                        <span class="text-danger">Не указано</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </md-tab>

                                @if($walletInfoIn != null and empty($detail->excode))
                                    <md-tab label="О транзакции">
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Адрес
                                            </div>

                                            <div class="application-detail-value col-md-6">
                                                {{ $walletInfoIn->address }}
                                            </div>
                                        </div>
                                        <md-divider></md-divider>

                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Подтверждений
                                            </div>

                                            <div class="application-detail-value col-md-6">
                                                <div ng-if="confirmations != null" ng-bind="confirmations"></div>
                                                <div ng-if="confirmations == null">{{ $walletInfoIn->confirmations }}</div>

                                                <a style="position: absolute; right: 10px; top: -5px;" class="abs-payment" ng-click="checkPayment('{{$paymentIn->name}}','{{$detail->id}}')">
                                                    <span class="material-icons">refresh</span>
                                                </a>
                                            </div>
                                        </div>

                                        @if($detail->skip_reserve == 0 and $detail->start == 1 and $detail->status == 3)
                                            <md-divider></md-divider>
                                            <div class="detail-add-reserve text-center">
                                                <md-button class="md-warn" ng-href="?actions=skip_reserve">Зачислить <b>{{$detail->give_price.' '.$codeIn->name }}</b> в резерв</md-button>
                                            </div>
                                            {{--<a class="a-click" href="?actions=skip_reserve">Зачислить в резерв</a>--}}
                                        @endif

                                        @if(strlen(strip_tags($detail->from_shot)) > 0)
                                            <md-divider></md-divider>
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-6">
                                                    Со счета
                                                </div>

                                                <div class="application-detail-value col-md-6">
                                                    {{$detail->from_shot}}
                                                </div>
                                            </div>
                                        @endif

                                        @if(strlen(strip_tags($detail->sender_fullname)) > 0)
                                            <md-divider></md-divider>
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-6">
                                                    Ф.И.О Отправителя
                                                </div>

                                                <div class="application-detail-value col-md-6">
                                                    {{$detail->sender_fullname}}
                                                </div>
                                            </div>
                                        @endif

                                        @if(strlen(strip_tags($detail->description_give)) > 0)
                                            <md-divider></md-divider>
                                            <div class="application-detail-item row">

                                                <div class="application-detail-description">
                                                    <h4>Описание</h4>
                                                    <div>
                                                        {!! $detail->description_give !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </md-tab>
                                @endif
                            </md-tabs>
                        </div>

                    </div>
                </div>

                <div flex="50" flex-xs="100">
                    <div class="x_panel">
                        <div class="x_content">
                            <md-tabs class="application-detail-tabs" md-dynamic-height md-border-bottom>
                                <md-tab label="Переводит сервис">
                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Платежная система
                                        </div>

                                        <div class="application-detail-value col-md-6">
                                            {{ $paymentOut->name }} {{ $codeOut->name }}
                                        </div>
                                    </div>
                                    <md-divider></md-divider>

                                    <div class="application-detail-item row">
                                        <div class="application-detail-key col-md-6">
                                            Сумма
                                        </div>

                                        <div class="application-detail-value col-md-6 price">
                                            {{ $display_out_price }} {{  $codeOut->name }}
                                            <div style="font-size: 10px;margin-top: 5px;" class="text-muted">Разница: {{ ($percent_diff > 0 ? $percent_diff : 0) }}%
                                                @if($percent_diff > 0)
                                                    <span class="text-danger">({{ $new_out_amount }} {{  $codeOut->name }}) </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    @if(strlen(strip_tags($detail->to_shot)) > 0)
                                        <md-divider></md-divider>
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                На счет
                                            </div>

                                            <div class="application-detail-value col-md-6">
                                                {{$detail->to_shot}}
                                                @if($uniqueToShot == false)
                                                    <br />
                                                    <small class="text-danger">Номер счета не уникальный</small>
                                                @endif
                                                @if(iEXSetting('telegram_to_shot_notification'))
                                                    <br />
                                                    <small>
                                                        <a href="?actions=send_to_shot_telegram">Отправить счет в telegram</a>
                                                    </small>
                                                @endif
                                            </div>
                                        </div>
                                    @endif

                                    @if(count($tasks_fields['currency_out']) > 0)
                                        @foreach($tasks_fields['currency_out'] as $item)
                                            <md-divider></md-divider>
                                            <div class="application-detail-item row">
                                                <div class="application-detail-key col-md-6">
                                                    {{ $item->field_name }}
                                                </div>
                                                <div class="application-detail-value col-md-6">
                                                    @if(!is_null($item->field_value))
                                                        {{ $item->field_value }}
                                                    @else
                                                        <span class="text-danger">Не указано</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </md-tab>
                                @if(!empty($cardDetails))
                                    <md-tab label="Информация по карте">
                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Номер карты
                                            </div>

                                            <div class="application-detail-value col-md-6">
                                                {{ $cardDetails->card_number }}
                                            </div>
                                        </div>
                                        <md-divider></md-divider>

                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Система
                                            </div>

                                            <div class="application-detail-value col-md-6 price">
                                                @if(!empty($cardDetails->payment_system))
                                                    {{ $cardDetails->payment_system }}
                                                @else
                                                    <small class="text-danget">Не определен</small>
                                                @endif
                                            </div>
                                        </div>

                                        <md-divider></md-divider>

                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Тип карты
                                            </div>

                                            <div class="application-detail-value col-md-6 price">
                                                @if(!empty($cardDetails->type_card))
                                                    {{ $cardDetails->type_card }}
                                                @else
                                                    <small class="text-danget">Не определен</small>
                                                @endif
                                            </div>
                                        </div>

                                        <md-divider></md-divider>

                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Брэнд карты
                                            </div>

                                            <div class="application-detail-value col-md-6 price">
                                                @if(!empty($cardDetails->brand_card))
                                                    {{ $cardDetails->brand_card }}
                                                @else
                                                    <small class="text-danget">Не определен</small>
                                                @endif
                                            </div>
                                        </div>

                                        <md-divider></md-divider>

                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Страна
                                            </div>

                                            <div class="application-detail-value col-md-6 price">
                                                @if(!empty($cardDetails->country_name))
                                                    {{ $cardDetails->country_name }} ({{ $cardDetails->country_currency }})
                                                @else
                                                    <small class="text-danget">Не определен</small>
                                                @endif
                                            </div>
                                        </div>

                                        <md-divider></md-divider>

                                        <div class="application-detail-item row">
                                            <div class="application-detail-key col-md-6">
                                                Название банка
                                            </div>

                                            <div class="application-detail-value col-md-6 price">
                                                @if(!empty($cardDetails->bank_name))
                                                    <div>{{ $cardDetails->bank_name }}</div>
                                                    @if($cardDetails->bank_url)
                                                        <small><a target="_blank" href="{{ $cardDetails->bank_url }}">{{ $cardDetails->bank_url }}</a></small>
                                                        <br />
                                                    @endif

                                                    @if($cardDetails->bank_phone)
                                                        <small>Контактный номер: {{ $cardDetails->bank_phone }}</small>
                                                    @endif
                                                @else
                                                    <small class="text-danget">Не определен</small>
                                                @endif
                                            </div>
                                        </div>

                                    </md-tab>
                                @endif
                            </md-tabs>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </md-content>
    <div class="footer-detail">
        <div layout="row" layout-xs="column">
            @if(!$preview and $detail->start == 1 and $detail->status != 4)
                <div>
                    <md-button class="md-raised md-primary" type="button" data-toggle="modal" data-target="#sendCheck" ng-disabled="isButtonDisabled">
                        @if($detail->check_status == 0)
                            Отправить чек
                        @else
                            Повторно отправить чек
                        @endif
                    </md-button>

                    <md-button class="md-raised md-primary" type="button" data-toggle="modal" data-target="#sendMessage" ng-disabled="isButtonDisabled">
                        Чат
                    </md-button>

                    @if(isset($validityOrder) and $validityOrder['level'] == 3)

                    @else
                        @if($detail->skip_reserve == 0)
                            <md-button type="submit" class="md-raised md-warn"
                                       ng-click="postpone($event, '{{$detail->id}}', '{{ $pendingOrderStatus }}')" ng-disabled="isButtonDisabled">
                                Отложить
                            </md-button>
                        @endif
                    @endif
                </div>
            @endif

            <span flex=""></span>
                <div>
                    @if($preview == false and $detail->status != 4)
                        @if($detail->start == 0)
                            <md-button href="?actions=run_process" class="md-raised md-primary">Начать выполнение</md-button>
                        @endif
                            @if($detail->start == 1)
                                @if(isset($validityOrder) and $validityOrder['level'] == 3)
                                    <span class="text-danger">Заявку нельзя выполнить</span>
                                @else
                                    @if($hasBalance == 1)
                                        <span class="text-danger">Недостаточно средств для автовыплаты</span>
                                    @elseif($hasBalance == 0)
                                        <md-button ng-disabled="isButtonDisabled"
                                                ng-click="successPayment($event, '{{ mb_strtolower(\Str::slug($paymentOut->name)) }}','merchant', '{{$transferCommission}}','{{$detail->id}}')" type="button" class="md-raised md-success-3">
                                            Оплатить и завершить
                                        </md-button>
                                    @endif

                                    <md-button ng-disabled="isButtonDisabled"
                                               ng-click="successPayment($event, '{{mb_strtolower(\Str::slug($paymentOut->name))}}', 'default', '{{$transferCommission}}','{{$detail->id}}')" type="button" class="md-raised md-success-2">
                                        Заявка выполнена
                                    </md-button>
                                @endif


                                <md-button class="md-raised md-danger" ng-click="reject($event, '{{$detail->id}}', '{{ $reasonRejection }}')">
                                    Отклонить
                                </md-button>

                                {{--@if($detail->skip_reserve == 0)--}}
                                {{--<md-button class="md-raised md-danger" ng-click="reject($event, '{{$detail->id}}')">--}}
                                {{--Отклонить--}}
                                {{--</md-button>--}}
                                {{--@endif--}}
                            @endif
                        @if($detail->start == 0)
                            <md-button href="?actions=logout" class="md-raised md-warn" ng-click="close()">
                                Назад
                            </md-button>
                        @endif
                    @else
                        <md-button href="{{config('admin.directory')}}/tx" class="md-raised">
                            К заявкам
                        </md-button>
                    @endif
                </div>
        </div>
    </div>


    <!-- Отправить уведомление клиенту -->
    <form action="?actions=notification" method="POST">
        @csrf
        <div class="modal fade" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Комментарии к транзакции</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="message_success" class="col-sm-2 control-label">Текст сообщения</label>
                            <div class="col-sm-10">
                                <textarea rows="5" class="form-control" name="message_success" placeholder="Напишите сообщение для клиента"></textarea>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label for="message_success" class="col-sm-2 control-label">Тип уведомления</label>
                            <div class="col-sm-10">
                                <select class="form-control selectpicker" name="type_send_message">
                                    <option value="0">Отправить клиенту на почту</option>
                                    <option value="1">Отправить клиенту на сайт в режиме реального времени</option>
                                    <option value="3">Не отправлять</option>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="collapse" id="chatHistory">
                            <hr />
                            <h4>История чата</h4>
                            @if($historyChat->count() == 0)
                                <div class="text-center" style="color: red;">
                                    Сообщений не найдено
                                </div>
                            @else
                                <table class="table table-border-2">
                                    <thead>
                                    <tr>
                                        <th>Дата создания</th>
                                        <th>Кто создал</th>
                                        <th>Сообщение</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($historyChat as $item)
                                        <tr>
                                            <td>{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</td>
                                            <td>{{ $item->user->name }}</td>
                                            <td>{{ $item->message }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br />
                    <div class="modal-footer">
                        <div class="float-left">
                            <md-button type="button" role="button" data-toggle="collapse" href="#chatHistory" class="md-warn md-raised">История</md-button>
                        </div>

                        <div class="float-right">
                            <md-button type="submit" class="md-primary md-raised">Отправить</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Отправляем чек клиенту -->
    <form action="?actions=check" method="POST">
        @csrf
        <div class="modal fade" id="sendCheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Отправить чек</span>
                    </div>
                    <div class="modal-body">
                        @if(mb_strtolower($detail->direction_exchange->currency2->payment->name) == 'decred')
                            <div class="form-group">
                                <label for="message_success" class="col-sm-2 control-label">TransactionHash</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="transaction_hash" placeholder="Укажите TransactionHash" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br />
                        @endif
                        <div class="form-group">
                            <label for="message_success" class="col-sm-2 control-label">Сообщение</label>
                            <div class="col-sm-10">
                                <textarea rows="5" class="form-control" name="message_success" placeholder="Напишите информацию для клиента"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <br />
                    <div class="modal-footer">
                        <md-button type="submit" class="md-raised md-primary">Отправить чек</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- История пересчитываний -->
    <div class="modal fade" id="history_recalculation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">История изменений</span>
                    </div>
                    <div class="modal-body">
                        @if($historyRecalculation->count() == 0)
                            <div class="text-center" style="color: red;">
                                Изменений не завиксировано
                            </div>
                        @else
                            <table class="table table-border-2">
                                <thead>
                                <tr>
                                    <th>Было</th>
                                    <th>Стало</th>
                                    <th>Тип</th>
                                    <th>Изменено</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($historyRecalculation as $item)
                                    <tr>
                                        <td>
                                            {{$item->old_amount}} {{$codeIn->name}}
                                        </td>
                                        <td>
                                            {{$item->amount}} {{$codeIn->name}}
                                        </td>
                                        <td>
                                            @if($item->type == 0)
                                                По станд. курсу
                                            @elseif($item->type == 1)
                                                По новому курсу
                                            @endif
                                            <div style="font-size: 10px; color: #999;">{{$item->course}}</div>
                                        </td>
                                        <td>{{$item->created_at->format('d m Y, H:i')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </div>
        </div>

    <!-- Пересчитать -->
    <form action="?actions=recount" method="post"  class="form-horizontal">
        @csrf
        <div class="modal fade" id="recount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="ui-dialog-title" id="myModalLabel">Пересчитать</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Клиент отдал</label>
                            <div class="col-sm-9">
                                <input type="text" name="give" class="form-control" id="give" placeholder="Сумму которую отдал клиент" value="{{$detail->give_price}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Пересчитать по</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" name="at_rate">
                                    <option value="0">курсу которую создал клиент</option>
                                    <option value="1">по актуальному курсу</option>
                                </select>
                            </div>
                        </div>

                        <div class="collapse" id="collapseExample">
                            <hr />
                            <h4>История изменений</h4>
                            <hr />
                            @if($historyRecalculation->count() == 0)
                                <div class="text-center" style="color: red;">
                                    Изменений не завиксировано
                                </div>
                            @else
                                <table class="table table-border-2">
                                    <thead>
                                    <tr>
                                        <th>Было</th>
                                        <th>Стало</th>
                                        <th>Тип</th>
                                        <th>Изменено</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($historyRecalculation as $item)
                                        <tr>
                                            <td>
                                                {{$item->old_amount}} {{$codeIn->name}}
                                            </td>
                                            <td>
                                                {{$item->amount}} {{$codeIn->name}}
                                            </td>
                                            <td>
                                                @if($item->type == 0)
                                                    По станд. курсу
                                                @elseif($item->type == 1)
                                                    По новому курсу
                                                @endif
                                                <div style="font-size: 10px; color: #999;">{{$item->course}}</div>
                                            </td>
                                            <td>{{$item->created_at->format('d m Y, H:i')}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="float-left">
                            <md-button type="button" role="button" data-toggle="collapse" href="#collapseExample" class="md-warn md-raised">История</md-button>
                        </div>

                        <div class="float-right">
                            <md-button type="submit" class="md-primary md-raised">Пересчитать</md-button>
                            <md-button type="button" data-dismiss="modal">Отмена</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Пересчитать (По сумме которую клиент оплатил)-->
    @if($walletInfoIn != null)
        <form action="?actions=recount" method="post"  class="form-horizontal">
            @csrf
            <div class="modal fade" id="recount2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Пересчитать</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Клиент отправил</label>
                                <div class="col-sm-9">
                                    <input type="text" readonly name="give" class="form-control" id="give" placeholder="Сумму которую отдал клиент" value="{{$walletInfoIn->amount}}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary">Пересчитать</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endif

@endsection
