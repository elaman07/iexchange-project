@extends('admin.layouts.app')

@section('title', __('admin-order.level.edit_limit'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.levels.edit', $item))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/applications/levels/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.manager') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="id_operator" data-live-search="true">
                        @foreach($users as $user)
                            <option value="{{$user->id}}" @if($item->id_operator == $user->id) selected @endif>{{ $user->name}} ({{$user->email}})</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.groups') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="id_level_group" data-live-search="true">
                        @foreach($levels as $level)
                            <option value="{{$level->id}}" @if($item->id_level_group == $level->id) selected @endif>{{ $level->name}} ({{$level->from_limit}} - {{ $level->to_limit }})</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.status') }}</label>
                <div class="col-sm-2">
                    <select class="form-control selectpicker" name="status">
                        <option value="0" @if($item->status == 0)selected @endif>{{ __('admin-order.enable') }}</option>
                        <option value="1" @if($item->status == 1)selected @endif>{{ __('admin-order.disable') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-order.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/applications/levels') }}">{{ __('admin-order.back') }}</md-button>
        </div>
    </form>

@endsection
