@extends('admin.layouts.app')

@section('title', __('admin-order.level.add_limit'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.levels.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/applications/levels/') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.manager') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="id_operator" data-live-search="true">
                        @foreach($users as $item)
                            <option value="{{$item->id}}" @if(old('id_operator') == $item->id) selected @endif>{{ $item->name}} ({{$item->email}})</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.groups') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="id_level_group" data-live-search="true">
                        @foreach($levels as $item)
                            <option value="{{$item->id}}" @if(old('id_level_group') == $item->id) selected @endif>{{ $item->name}} ({{$item->from_limit}} - {{ $item->to_limit }})</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.status') }}</label>
                <div class="col-sm-2">
                    <select class="form-control selectpicker" name="status">
                        <option value="0">{{ __('admin-order.enable') }}</option>
                        <option value="1">{{ __('admin-order.disable') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-order.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/applications/levels') }}">{{ __('admin-order.back') }}</md-button>
        </div>
    </form>
@endsection
