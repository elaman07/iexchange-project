@extends('admin.layouts.app')

@section('title', __('admin-order.level.limit_operator'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.levels'))

@section('top-block')
    <md-button ng-href="levels-group" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-object-group text-info"></md-icon>
        <span>{{ __('admin-order.level.group_limit') }}</span>
    </md-button>

    <md-button ng-href="levels/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-order.level.add_limit') }}</span>
    </md-button>
@endsection

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-order.level.created_at') }}</th>
            <th>{{ __('admin-order.level.manager') }}</th>
            <th>{{ __('admin-order.level.groups') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($lists) > 0)
            @foreach($lists as $list)
                <tr>
                    <th>{{ $list->created_at }}</th>
                    <td>{{ $list->manager->name }}</td>
                    <td>{{ $list->level_group->name }}</td>
                    <td style="width:200px;">

                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('applications/levels/'.$list->id.'/edit') }}">
                            <i class="fa fa-fw fa-pencil"></i>
                        </md-button>

                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('applications/levels/'.$list->id.'/delete')  }}">
                            <i class="fa fa-fw fa-close text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-order.list_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {{ $lists->links() }}
    </div>
@endsection