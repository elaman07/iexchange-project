@extends('admin.layouts.app')

@section('title', __('admin-order.level.edit_group'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.levels-group.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/applications/levels-group/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.name') }}</label>
                <div class="col-sm-4">
                    <input type="text" name="name" class="form-control" value="{{ $item->name }}">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.from_limit') }}</label>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" name="from_limit" class="form-control" value="{{ $item->from_limit }}">
                        <span class="input-group-addon" id="basic-addon3">USD</span>
                    </div>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-order.level.to_limit') }}</label>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" name="to_limit" class="form-control" value="{{ $item->to_limit }}">
                        <span class="input-group-addon" id="basic-addon3">USD</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-order.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/applications/levels-group') }}">{{ __('admin-order.back') }}</md-button>
        </div>
    </form>

@endsection