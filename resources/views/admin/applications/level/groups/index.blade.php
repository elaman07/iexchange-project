@extends('admin.layouts.app')

@section('title', __('admin-order.level.group_limit'))

@section('breadcrumbs', Breadcrumbs::render('admin.applications.levels-group'))

@section('top-block')
    <md-button ng-href="levels-group/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-order.level.add_group') }}</span>
    </md-button>
@endsection

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-order.level.created_at') }}</th>
            <th>{{ __('admin-order.level.name') }}</th>
            <th>{{ __('admin-order.level.from_limit') }}</th>
            <th>{{ __('admin-order.level.to_limit') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($groups) > 0)
            @foreach($groups as $group)
                <tr>
                    <th>{{ $group->created_at }}</th>
                    <td>{{ $group->name }}</td>
                    <td>{{ $group->from_limit }}</td>
                    <td>{{ $group->to_limit }}</td>
                    <td style="width:200px;">

                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('applications/levels-group?id='.$group->id.'&duplicate=true') }}">
                            <i class="fa fa-fw fa-copy"></i>
                        </md-button>

                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('applications/levels-group/'.$group->id.'/edit') }}">
                            <i class="fa fa-fw fa-pencil"></i>
                        </md-button>

                        <md-button class="md-icon-button" ng-href="{{ admin_base_path('applications/levels-group/'.$group->id.'/delete')  }}">
                            <i class="fa fa-fw fa-close text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-order.list_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {{ $groups->links() }}
    </div>
@endsection