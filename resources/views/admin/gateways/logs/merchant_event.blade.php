@extends('admin.layouts.app')

@section('title', 'Лог запросов к мерчанту')

@section('breadcrumbs', Breadcrumbs::render('admin.gateways.autopayment.event-merchant'))

@section('top-block')
    <md-button ng-href="?clear_logs" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">{{ __('Очистить лог') }}</span>
    </md-button>
@endsection


@section('no-block-content')

    <div class="x_panel">

        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('ID Заявки') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="id_order" value="{{ is_filter_search($filter, 'id_order') }}" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('Провайдер') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="provider" value="{{ is_filter_search($filter, 'provider') }}" >
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Сохранить фильтры') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Лог запросов к мерчанту') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th> {{ __('Дата создания') }}</th>
                    <th> {{ __('ID Заявки') }}</th>
                    <th> {{ __('Провайдер') }}</th>
                    <th> {{ __('Информация') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $item)
                        <tr>
                            <th>
                                <div>{{$item->created_at}}</div>
                                <small style="font-weight: normal" class="text-muted"> {{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </th>
                            <th>
                                {{$item->id_order}}
                            </th>
                            <td>
                                @if(!is_null($item->provider))
                                    {{ $item->provider }}
                                @else
                                    <span>~</span>
                                @endif
                            </td>
                            <td style="max-width: 300px">
                                <div>
                                    <b>URL:</b> {{ $item->url }}
                                </div>
                                <hr />
                                <div>
                                    <b>Заголовок:</b> {{ $item->headers }}
                                </div>
                                <hr />
                                <div>
                                    <b>Параметры:</b> {{ $item->content }}
                                </div>
                                <hr />
                                <div>
                                    <b>Ответ от сервера:</b> {{ $item->response }}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br /> {{ __('Список пуст') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
