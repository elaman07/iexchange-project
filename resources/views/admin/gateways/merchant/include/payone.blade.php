<div class="form-group col-md-6">
    <div class="control-label-br">API Host</div>
    <input type="text" name="api_host" class="form-control" id="api_host" placeholder="{{ security_pay_data($data, 'api_host') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">API Port</div>
    <input type="text" name="api_port" class="form-control" id="api_port" placeholder="{{ security_pay_data($data, 'api_port') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">API IP</div>
    <input type="text" name="api_ip" class="form-control" id="api_ip" placeholder="{{ security_pay_data($data, 'api_ip') }}">
</div>
