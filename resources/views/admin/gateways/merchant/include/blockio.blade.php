<div class="form-group col-md-3">
    <div class="control-label-br">Secret PIN</div>
    <input type="text" name="secret_pin" class="form-control" id="secret_pin" placeholder="{{ security_pay_data($data, 'secret_pin') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-3">
    <div class="control-label-br">Bitcoin API Key</div>
    <input type="text" name="api_key_btc" class="form-control" id="api_key_btc" placeholder="{{ security_pay_data($data, 'api_key_btc') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Litecoin API Key</div>
    <input type="text" name="api_key_ltc" class="form-control" id="api_key_ltc" placeholder="{{ security_pay_data($data, 'api_key_ltc') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Dogecoin API Key</div>
    <input type="text" name="api_key_doge" class="form-control" id="api_key_doge" placeholder="{{ security_pay_data($data, 'api_key_doge') }}">
</div>


<div class="clearfix"></div>
<hr />

<div class="form-group col-md-3">
    <div class="control-label-br">Кол-во подтверждений платежа для Bitcoin</div>
    <input type="text" name="confirm_btc" class="form-control" id="confirm_btc" placeholder="{{ security_pay_data($data, 'confirm_btc') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Кол-во подтверждений платежа для Litecoin</div>
    <input type="text" name="confirm_ltc" class="form-control" id="confirm_ltc" placeholder="{{ security_pay_data($data, 'confirm_ltc') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Кол-во подтверждений платежа для Dogecoin</div>
    <input type="text" name="confirm_doge" class="form-control" id="confirm_doge" placeholder="{{ security_pay_data($data, 'confirm_doge') }}">
</div>
