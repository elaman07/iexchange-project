<div class="form-group col-md-3">
    <div class="control-label-br">ID Магазина</div>
    <input type="text" name="id_shop" class="form-control" id="id_shop" placeholder="{{ security_pay_data($data, 'id_shop') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Секретный ключ</div>
    <input type="text" name="secret_key" class="form-control" id="secret_key" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>
