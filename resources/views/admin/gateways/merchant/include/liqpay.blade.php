<div class="form-group col-md-3">
    <div class="control-label-br">Public Key</div>
    <input type="text" name="public_key" class="form-control" id="public_key" placeholder="{{ security_pay_data($data, 'public_key') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Private Key</div>
    <input type="text" name="private_key" class="form-control" id="private_key" placeholder="{{ security_pay_data($data, 'private_key') }}">
</div>
