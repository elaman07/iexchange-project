<div class="form-group col-md-3">
    <div class="control-label-br">Публичный ключ</div>
    <input type="text" name="public_key" class="form-control" id="public_key" placeholder="{{ security_pay_data($data, 'public_key') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Приватный ключ</div>
    <input type="text" name="private_key" class="form-control" id="private_key" placeholder="{{ security_pay_data($data, 'private_key') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Service ID</div>
    <input type="text" name="service_id" class="form-control" id="service_id" placeholder="{{ security_pay_data($data, 'service_id') }}">
</div>

