<div class="form-group col-md-3">
    <div class="control-label-br">API Key</div>
    <input type="text" name="api_key" class="form-control" id="api_key" placeholder="{{ security_pay_data($data, 'api_key') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Public Key</div>
    <input type="text" name="public_key" class="form-control" id="public_key" placeholder="{{ security_pay_data($data, 'public_key') }}">
</div>
