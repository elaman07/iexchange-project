<div class="form-group col-md-3">
    <div class="control-label-br">Merchant UUID</div>
    <input type="text" name="merchant_uuid" class="form-control" id="merchant_uuid" placeholder="{{ security_pay_data($data, 'merchant_uuid') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Secret Key</div>
    <input type="text" name="secret_key" class="form-control" id="secret_key" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>
