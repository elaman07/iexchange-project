<div class="form-group col-md-6">
    <div class="control-label-br">RPC Host</div>
    <input type="text" name="rpc_host" class="form-control" id="rpc_host" placeholder="{{ security_pay_data($data, 'rpc_host') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">RPC Port</div>
    <input type="text" name="rpc_port" class="form-control" id="rpc_port" placeholder="{{ security_pay_data($data, 'rpc_port') }}">
</div>

<div class="clearfix"></div>

<div class="form-group col-md-6">
    <div class="control-label-br">RPC User</div>
    <input type="text" name="rpc_username" class="form-control" id="rpc_username" placeholder="{{ security_pay_data($data, 'rpc_username') }}">
</div>


<div class="form-group col-md-6">
    <div class="control-label-br">RPC Password</div>
    <input type="text" name="rpc_password" class="form-control" id="rpc_password" placeholder="{{ security_pay_data($data, 'rpc_password') }}">
</div>
