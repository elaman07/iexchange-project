<div class="form-group col-md-3">
    <div class="control-label-br">E-mail аккаунта</div>
    <input type="text" name="sci_account_email" class="form-control" id="sci_account_email" placeholder="{{ security_pay_data($data, 'sci_account_email') }}">
</div>

<div class="clearfix"></div>


<div class="form-group col-md-3">
    <div class="control-label-br">Название SCI</div>
    <input type="text" name="sci_name" class="form-control" id="sci_name" placeholder="{{ security_pay_data($data, 'sci_name') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Пароль от SCI</div>
    <input type="text" name="sci_password" class="form-control" id="sci_password" placeholder="{{ security_pay_data($data, 'sci_password') }}">
</div>

<div class="clearfix"></div>
<hr/>

<div class="form-group col-md-3">
    <div class="control-label-br">Название API</div>
    <input type="text" name="api_name" class="form-control" id="api_name" placeholder="{{ security_pay_data($data, 'api_name') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Пароль от API</div>
    <input type="text" name="api_secret" class="form-control" id="api_secret" placeholder="{{ security_pay_data($data, 'api_secret') }}">
</div>

