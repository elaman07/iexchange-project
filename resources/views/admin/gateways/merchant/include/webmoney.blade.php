@php
    $purses = ['wmr', 'wmz', 'wme', 'wmu', 'wmb', 'wmy', 'wmx', 'wmk', 'wml', 'wmh'];
@endphp

@foreach($purses as $value)
    <div class="form-group col-md-3">
        <div class="control-label-br">{{ mb_strtoupper($value) }} кошелек</div>
        <input type="text" name="{{ $value.'_purse' }}" class="form-control" id="{{ $value.'_purse' }}" placeholder="{{ security_pay_data($data, $value.'_purse') }}">
    </div>
@endforeach

<div class="clearfix"></div>
<hr/>
@foreach($purses as $value)
    <div class="form-group col-md-3">
        <div class="control-label-br">Secret Key {{ mb_strtoupper($value) }} кошелька</div>
        <input type="text" name="{{ $value.'_secret_key' }}" class="form-control" id="{{ $value.'_secret_key' }}" placeholder="{{ security_pay_data($data, $value.'_secret_key') }}">
    </div>
@endforeach

<div class="clearfix"></div>
<hr/>
@foreach($purses as $value)
    <div class="form-group col-md-3">
        <div class="control-label-br">Secret Key X20 {{ mb_strtoupper($value) }} кошелька</div>
        <input type="text" name="{{ $value.'_x20_secret_key' }}" class="form-control" id="{{ $value.'_x20_secret_key' }}" placeholder="{{ security_pay_data($data, $value.'_x20_secret_key') }}">
    </div>
@endforeach
