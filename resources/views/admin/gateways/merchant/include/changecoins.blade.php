<div class="form-group col-md-6">
    <div class="control-label-br">API Key</div>
    <input type="text" name="api_key" class="form-control" id="api_key" placeholder="{{ security_pay_data($data, 'api_key') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">API Secret</div>
    <input type="text" name="api_secret" class="form-control" id="api_secret" placeholder="{{ security_pay_data($data, 'api_secret') }}">
</div>
