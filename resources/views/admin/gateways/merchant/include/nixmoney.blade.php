<div class="form-group">
    <label for="account_email" class="col-sm-2 control-label">E-mail от аккаунта NixMoney</label>
    <div class="col-sm-4">
        <input type="text" name="account_email" class="form-control" id="account_email" placeholder="{{ security_pay_data($data, 'account_email') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_password" class="col-sm-2 control-label">Пароль от аккаунта NixMoney</label>
    <div class="col-sm-4">
        <input type="text" name="account_password" class="form-control" id="account_password" placeholder="{{ security_pay_data($data, 'account_password') }}">
    </div>
</div>


<hr />

<div class="form-group">
    <label for="account_usd" class="col-sm-2 control-label">USD номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_usd" class="form-control" id="account_usd" placeholder="{{ security_pay_data($data, 'account_usd') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_eur" class="col-sm-2 control-label">EUR номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_eur" class="form-control" id="account_eur" placeholder="{{ security_pay_data($data, 'account_eur') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_btc" class="col-sm-2 control-label">BTC номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_btc" class="form-control" id="account_btc" placeholder="{{ security_pay_data($data, 'account_btc') }}">
    </div>
</div>


<div class="form-group">
    <label for="account_ltc" class="col-sm-2 control-label">LTC номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_ltc" class="form-control" id="account_ltc" placeholder="{{ security_pay_data($data, 'account_ltc') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_doge" class="col-sm-2 control-label">DOGE номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_doge" class="form-control" id="account_doge" placeholder="{{ security_pay_data($data, 'account_doge') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_ppc" class="col-sm-2 control-label">PPC номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_ppc" class="form-control" id="account_ppc" placeholder="{{ security_pay_data($data, 'account_ppc') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_ftc" class="col-sm-2 control-label">FTC номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_ftc" class="form-control" id="account_ftc" placeholder="{{ security_pay_data($data, 'account_ftc') }}">
    </div>
</div>

<div class="form-group">
    <label for="account_crt" class="col-sm-2 control-label">CRT номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_crt" class="form-control" id="account_crt" placeholder="{{ security_pay_data($data, 'account_crt') }}">

    </div>
</div>

<div class="form-group">
    <label for="account_gbc" class="col-sm-2 control-label">GBC номер кошелька</label>
    <div class="col-sm-4">
        <input type="text" name="account_gbc" class="form-control" id="account_gbc" placeholder="{{ security_pay_data($data, 'account_gbc') }}">
    </div>
</div>


<div class="clearfix"></div>
<hr />

<div class="form-group">
    <label for="account_gbc" class="col-sm-2 control-label">Имя продавца (произвольное)</label>
    <div class="col-sm-4">
        <input type="text" name="account_name" class="form-control" id="account_name" placeholder="{{ security_pay_data($data, 'account_name') }}">
    </div>
</div>

