@extends('admin.layouts.app')

@section('title', 'Изменить мерчант '.$item->name)


@section('breadcrumbs', Breadcrumbs::render('admin.gateways.merchant.edit', $item))

@section('content')

    <style>
        .merchant-edit-url-code {
            border: 1px solid #e9e9e9;
            margin-bottom: 10px;
            border-radius: 5px;
        }

        .merchant-edit-url-code > b {
            margin-right: 10px;
            padding: 9px 10px 10px;
            background: #f9f9f9;
            width: 140px;
            display: inline-block;
        }
    </style>

    <style>

        .bbcode-editor {
            float: left;
            padding: 5px 10px;
            border-radius: 3px;
            border: 1px solid #e0e0e0;
            box-shadow: 0 1px 2px #e0e0e0;
            margin: 0 5px 5px 0;
            color: #333;
            font-size: 12px;
        }

        .bbcode-editor:hover {
            background: #fafafa;
            border: 1px solid #d9d9d9;
        }
    </style>


    <form action="{{ admin_base_path('/gateways/merchant/'.$item->id) }}" method="POST" class="form-horizontal">
        @csrf
        @method('PUT')

        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Основное</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $item->name }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Провайдер</div>
                        {{ Form::select('id_gateways', $gateways, $item->id_gateways, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '15', 'disabled']) }}
                    </div>


                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>Отключен</option>
                            <option value="1" @if($item->status == 1) selected @endif>Включен</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Привязать к валютам</div>
                        {{ Form::select('currencies[]', $currencies, $item->currencies, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '8']) }}
                    </div>


                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">Настройки модуля</label>
                <div class="col-sm-10">

                    @if(is_array($options_render) and !empty($options_render))
                        <div class="alert alert-success">
                            Address: <b>{{ $options_render['address'] ?? null }}</b>
                            <hr />
                            Private key: <b>{{ $options_render['private_key'] ?? null }}</b>
                        </div>
                    @endif

                    @if(view()->exists('admin.gateways.merchant.include.'.$item->gateway->alias))
                        @include('admin.gateways.merchant.include.'.$item->gateway->alias)
                    @else
                        <div class="merchant-alert-notify text-center">Не найдены конфигурационные данные</div>
                    @endif
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />


            <div class="form-group">
                <label class="col-sm-2 control-label">Настройки</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Инструкция к оплате') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="gateway-instruction-pay-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                @foreach(config('admin-settings.editor') as $key => $value)
                                    <a class="bbcode-editor" onclick='insertTextareaBBCode(this, "{{ $key }}", "gateway-instruction-pay{{$form_lang['field']}}")' href='#'>{{ $value }}</a>
                                @endforeach
                                <textarea rows="5" class="form-control" id="gateway-instruction-pay{{$form_lang['field']}}" name="instruction_payment{{$form_lang['field']}}">{{ $item->getTranslation('instruction_payment', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>



                    <div class="clearfix"></div>

                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Примечание для платежа') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#comment-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="comment-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                @foreach(config('admin-settings.editor') as $key => $value)
                                    <a class="bbcode-editor" onclick='insertTextareaBBCode(this, "{{ $key }}", "gateway-editors{{$form_lang['field']}}")' href='#'>{{ $value }}</a>
                                @endforeach
                                <textarea rows="5" class="form-control" id="gateway-editors{{$form_lang['field']}}" name="comment{{$form_lang['field']}}">{{ $item->getTranslation('comment', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Дневной лимит мерчанта</div>
                        <input type="text" name="day_limit_amount_merchant" class="form-control" value="{{ $item->day_limit_amount_merchant }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Погрешность суммы платежа</div>
                        <input type="text" name="amount_fault" class="form-control" value="{{ $item->amount_fault }}">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Дневной лимит заявок (для мерчанта)</div>
                        <input type="text" name="day_limit_merchant" class="form-control" value="{{ $item->day_limit_merchant }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Макс. сумма платежа для одной заявки</div>
                        <input type="text" name="day_limit_merchant" class="form-control" value="{{ $item->day_limit_merchant }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Включить проверку платежа по логам</div>
                        <div class="material-switch switch-input-1">
                            <input id="is_merchant_log" name="is_merchant_log" type="checkbox" @if($item->is_merchant_log == 1) checked @endif value="1" />
                            <label for="is_merchant_log" class="label-primary"></label>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Включить кнопку в мерчанте "Отменить заявку"</div>
                        <div class="material-switch switch-input-1">
                            <input id="is_enable_merchant_button" name="is_enable_merchant_button" type="checkbox" @if($item->is_enable_merchant_button == 1) checked @endif value="1" />
                            <label for="is_enable_merchant_button" class="label-primary"></label>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Запретить управлять заявкой с другого IP адреса</div>
                        <div class="material-switch switch-input-1">
                            <input id="is_deny_ip_address" name="is_deny_ip_address" type="checkbox" @if($item->is_deny_ip_address == 1) checked @endif value="1" />
                            <label for="is_deny_ip_address" class="label-success"></label>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Разрешенные IP адреса (через запятую, с новой строки)</div>
                        <textarea rows="5" class="form-control" id="allow_ip_address" name="allow_ip_address">{{ $item->allow_ip_address }}</textarea>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Сумма оплаты</div>
                        <select class="selectpicker form-control" name="pay_amount" data-width="300px">
                            <option value="0" @if($item->pay_amount == 0) selected @endif>Сумма (с доп. комиссией)</option>
                            <option value="1" @if($item->pay_amount == 1) selected @endif>Сумма (с комиссией доп. и ПС)</option>
                            <option value="2" @if($item->pay_amount == 2) selected @endif>Сумма (без комиссий)</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Сумма зачисления</div>
                        <select class="selectpicker form-control" name="credit_amount" data-width="300px">
                            <option value="0" @if($item->credit_amount == 0) selected @endif>Сумма (с доп. комиссией)</option>
                            <option value="1" @if($item->credit_amount == 1) selected @endif>Сумма (с комиссией доп. и ПС)</option>
                            <option value="2" @if($item->credit_amount == 2) selected @endif>Сумма (без комиссий)</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <style>
                        .merchant-secret__key {
                            font-size: 20px;
                            font-weight: 500;
                            margin-bottom: 20px;
                        }
                    </style>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Секретный ключ</div>
                        <div class="merchant-secret__key ">
                            {{ $item->security_hash }}
                            @if(!empty($item->security_hash))
                                <i class="text-success far fa-check"></i>
                            @else
                                <a class="btn btn-primary" href="?random_secret">Сгенерировать ключ</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            @includeIf('admin.gateways.merchant.settings.'.$item->gateway->alias)
            <!-- Информативный раздел -->
            @includeIf('admin.gateways.merchant.docs.'.$item->gateway->alias)

        </div>


        <div class="clearfix"></div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/gateways/merchant') }}">Назад</md-button>
        </div>
    </form>
@endsection
