@extends('admin.layouts.app')

@section('title', __('Список мерчантов'))

@section('top-block')
    <md-button ng-href="merchant/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить мерчант') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings_columns" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-table"></md-icon>
            <span>{{ __('Настройка страницы') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.gateways.merchant'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="far fa-filter"></i>&nbsp;&nbsp;&nbsp;{{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="font-size-16 far fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="font-size-16 far fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Имя') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::input('text', 'name', is_filter_search($filter, 'name'), ['class' => 'form-control']) !!}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_full_name" @if(isset($filter['checkbox_full_name'])) checked @endif>
                                            <label class="form-check-label" for="checkbox1">{{ __('Точное совпадение имени') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Статус') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::select('status[]', $status, is_filter_search($filter, 'status'),
                                            ['class' => 'form-control selectpicker', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-basic.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-basic.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form style="width:100%;margin-right: 10px;" method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        @if(in_array('name', $admin_merchant_hidden_columns))
                            <th>{{ __('Название') }}</th>
                        @endif
                        @if(in_array('alias', $admin_merchant_hidden_columns))
                            <th>{{ __('Алиас') }}</th>
                        @endif
                        @if(in_array('settings', $admin_merchant_hidden_columns))
                            <th>{{ __('Настройки') }}</th>
                        @endif
                        @if(in_array('security', $admin_merchant_hidden_columns))
                            <th>{{ __('Безопасность') }}</th>
                        @endif
                        @if(in_array('pegged_currencies', $admin_merchant_hidden_columns))
                            <th>{{ __('Привязанные валюты') }}</th>
                        @endif
                        @if(in_array('count_order', $admin_merchant_hidden_columns))
                            <th>{{ __('Кол-во заявок') }}</th>
                        @endif
                        @if(in_array('summary_usd', $admin_merchant_hidden_columns))
                            <th>{{ __('Объем в USD') }}</th>
                        @endif
                        @if(in_array('created_at', $admin_merchant_hidden_columns))
                            <th>{{ __('Дата создания') }}</th>
                        @endif
                        @if(in_array('updated_at', $admin_merchant_hidden_columns))
                            <th>{{ __('Посл. обновление') }}</th>
                        @endif
                        @if(in_array('status', $admin_merchant_hidden_columns))
                            <th>{{ __('Статус') }}</th>
                        @endif
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(count($merchants) > 0)
                        @foreach($merchants as $merchant)
                            <tr @if($merchant->status == 0) class="table-gateway-offline-tr" @endif>
                                @if(in_array('name', $admin_merchant_hidden_columns))
                                    <th>
                                        <a href="{{ admin_base_path('gateways/merchant/'.$merchant->id.'/edit') }}">
                                            {{ $merchant->name }} &nbsp;<i class="fal fa-pencil"></i>
                                        </a>
                                    </th>
                                @endif
                                @if(in_array('alias', $admin_merchant_hidden_columns))
                                    <td>{{ isset($merchant->gateway) ? $merchant->gateway->alias : '' }} (v{{ isset($merchant->gateway) ? $merchant->gateway->version : 0 }})</td>
                                @endif
                                @if(in_array('settings', $admin_merchant_hidden_columns))
                                    <td>
                                        @if($merchant->is_config_done == 1)
                                            <span class="text-success">
                                    <i class="fad fa-check-circle"></i> Интегрирован
                                </span>
                                        @else
                                            <b class="text-danger">
                                                Шлюз не настроен
                                            </b>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('security', $admin_merchant_hidden_columns))
                                    <td>
                                        @if(!empty($merchant->gateway->security_options))
                                            @php
                                                $i = 0;
                                            @endphp

                                            @foreach($merchant->gateway->security_options['fields'] as $key => $fields)
                                                @if(empty($merchant->{$fields}))
                                                    <div class="@if(config('payment.providers.validators.levels.'.$fields) == 5) text-danger font-weight-bold @elseif(config('payment.providers.validators.levels.'.$fields) == 3) text-security-medium @else text-danger @endif">
                                                        - {{ config('payment.providers.validators.errors.'.$fields) }}
                                                    </div>
                                                    <div style="display:none;">{{ $i++ }}</div>
                                                @endif
                                            @endforeach

                                            @if($i == 0)
                                                <div class="text-success">
                                                    <i class="far fa-shield-check"></i> Мерчант безопасен
                                                </div>
                                            @endif
                                        @else
                                            <div class="text-success">Защита активна</div>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('pegged_currencies', $admin_merchant_hidden_columns))
                                    <td>
                                        {{ Form::select('currencies['.$merchant->id.'][]', $currencies, $merchant->currencies, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15', 'data-width' => '300px']) }}
                                        {{--                            @if($merchant->currencies->count() > 0)--}}
                                        {{--                                <ul class="list-unstyled">--}}
                                        {{--                                    @foreach($merchant->currencies as $currency)--}}
                                        {{--                                        <li>- <a style="font-size: 11px;color: @if($currency->status == 0)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/currency/'.$currency->id.'/edit') }}">--}}
                                        {{--                                                {{ $currency->payment->name }} {{ $currency->code_currency->name }}--}}
                                        {{--                                            </a>--}}
                                        {{--                                        </li>--}}
                                        {{--                                    @endforeach--}}
                                        {{--                                </ul>--}}
                                        {{--                            @else--}}
                                        {{--                                <small class="text-danger">Не найдено</small>--}}
                                        {{--                            @endif--}}
                                    </td>
                                @endif
                                @if(in_array('count_order', $admin_merchant_hidden_columns))
                                    <td>
                                        {{ $merchant->order_num }}
                                    </td>
                                @endif
                                @if(in_array('summary_usd', $admin_merchant_hidden_columns))
                                    <td>
                                        ${{ iex_number_format($merchant->total_usd) }}
                                    </td>
                                @endif

                                @if(in_array('created_at', $admin_merchant_hidden_columns))
                                    <td>
                                        <div class=""> {{ \Illuminate\Support\Carbon::parse($merchant->created_at)->translatedFormat('d M Y H:i') }}</div>
                                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($merchant->created_at)->diffForHumans() }}</small>
                                    </td>
                                @endif

                                @if(in_array('updated_at', $admin_merchant_hidden_columns))
                                    <td>
                                        <div class=""> {{ \Illuminate\Support\Carbon::parse($merchant->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($merchant->updated_at)->diffForHumans() }}</small>
                                    </td>
                                @endif

                                @if(in_array('status', $admin_merchant_hidden_columns))
                                    <td>
                                        <input type="hidden" name="key_id[]" value="{{ $merchant->id }}">

                                        <div class="material-switch">
                                            <input id="SwitchOptionPrimary{{$merchant->id}}" name="status[{{$merchant->id}}]" type="checkbox" value="1" @if($merchant->status == 1) checked @endif />
                                            <label for="SwitchOptionPrimary{{$merchant->id}}" class="label-success"></label>
                                        </div>
                                    </td>
                                @endif
                                <td style="width: 100px;">
                                    <md-button title="{{ __('Удалить') }}" href="merchant/{{$merchant->id}}/delete" class="md-icon-button">
                                        <i class="fas fa-trash text-danger"></i>
                                    </md-button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">

                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек таблицы -->
    <div class="modal fade" id="settings_columns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings_columns">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка страницы') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Колонки') }}</label>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[name]" value="name" type="checkbox" name="admin_merchant_hidden_columns[name]" @if(in_array('name', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[name]">{{ __('Название') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[alias]" value="alias" type="checkbox" name="admin_merchant_hidden_columns[alias]" @if(in_array('alias', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[alias]">{{ __('Алиас') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[settings]" value="settings" type="checkbox" name="admin_merchant_hidden_columns[settings]" @if(in_array('settings', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[settings]">{{ __('Настройки') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[security]" value="security" type="checkbox" name="admin_merchant_hidden_columns[security]" @if(in_array('security', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[security]">{{ __('Безопасность') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[pegged_currencies]" value="pegged_currencies" type="checkbox" name="admin_merchant_hidden_columns[pegged_currencies]" @if(in_array('pegged_currencies', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[pegged_currencies]">{{ __('Привязанные валюты') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[count_order]" value="count_order" type="checkbox" name="admin_merchant_hidden_columns[count_order]" @if(in_array('count_order', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[count_order]">{{ __('Кол-во заявок') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[summary_usd]" value="summary_usd" type="checkbox" name="admin_merchant_hidden_columns[summary_usd]" @if(in_array('summary_usd', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[summary_usd]">{{ __('Объем в USD') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[created_at]" value="created_at" type="checkbox" name="admin_merchant_hidden_columns[created_at]" @if(in_array('created_at', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[created_at]">{{ __('Дата создания') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[updated_at]" value="updated_at" type="checkbox" name="admin_merchant_hidden_columns[updated_at]" @if(in_array('updated_at', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[updated_at]">{{ __('Посл. обновление') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_merchant_hidden_columns[status]" value="status" type="checkbox" name="admin_merchant_hidden_columns[status]" @if(in_array('status', $admin_merchant_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_merchant_hidden_columns[status]">{{ __('Статус') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group">
                            <label for="admin_code_pagination" class="control-label col-md-6">{{ __('Кол-во записей на странице') }}</label>
                            <div class="col-sm-6">
                                <input type="text" name="admin_merchant_pagination" class="form-control" value="{{ iEXSetting('admin_merchant_pagination', 20) }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $merchants->links() !!}
    </div>
@endsection
