<div class="clearfix"></div>

<div class="form-group">
    <label class="col-sm-2 control-label">Дополнительные опция</label>
    <div class="col-sm-10">

        <div class="form-group col-md-3">
            <div class="control-label-br">Включить проверку платежа по API</div>
            <div class="material-switch switch-input-1">
                <input id="is_check_api" name="is_check_api" type="checkbox" @if($item->is_check_api == 1) checked @endif value="1" />
                <label for="is_check_api" class="label-primary"></label>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group col-md-3">
            <div class="control-label-br">Включить проверку счетов с которого была сделана оплата</div>
            <div class="material-switch switch-input-1">
                <input id="is_check_from_shot" name="is_check_from_shot" type="checkbox" @if($item->is_check_from_shot == 1) checked @endif value="1" />
                <label for="is_check_from_shot" class="label-primary"></label>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group">
    <label class="col-sm-2 control-label">Ссылки для мерчантов</label>
    <div class="col-sm-10">

        <div class="form-group col-md-6">
            <div>
                <div class="merchant-edit-ul">
                    <div class="merchant-edit-url-code">
                        <b>Return URL:</b> <code>{{ route('merchant.receive_money', [$item->gateway->alias, $item->security_hash])  }}</code>
                    </div>
                    <div class="merchant-edit-url-code">
                        <b>Success URL:</b> <code>{{ url('/payment_status/success')  }}</code>
                    </div>
                    <div class="merchant-edit-url-code">
                        <b>Failed URL:</b> <code>{{ url('/payment_status/fail')  }}</code>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>
        <hr />
        <div class="form-group col-md-3">
            <div class="control-label-br">IP-адреса серверов для отправки уведомлений</div>
            <p class="col-sm-2 form-control-static"><b>185.71.65.189<br/> 185.71.65.92<br/> 149.202.17.210</b></p>
        </div>

    </div>
</div>
