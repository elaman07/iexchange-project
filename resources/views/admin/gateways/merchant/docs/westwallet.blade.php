<div class="clearfix"></div>
<hr />

<div class="form-group">
    <label class="col-sm-2 control-label">Ссылки для мерчантов</label>
    <div class="col-sm-10">

        <div class="form-group col-md-6">
            <div>
                <div class="merchant-edit-ul">
                    <div class="merchant-edit-url-code">
                        <b>Callback URL:</b> <code>{{ route('merchant.ipn', [$item->gateway->alias, $item->security_hash])  }}</code>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
