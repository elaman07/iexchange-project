@extends('admin.layouts.app')

@section('title', 'Добавить мерчант')


@section('breadcrumbs', Breadcrumbs::render('admin.gateways.merchant.create'))

@section('content')
    <form action="{{ admin_base_path('/gateways/merchant') }}" method="POST" class="form-horizontal">
        @csrf
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Новый мерчант</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Провайдер</div>
                        {{ Form::select('id_gateways', $gateways, null, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '10']) }}
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0">Отключен</option>
                            <option value="1" selected>Включен</option>
                        </select>
                    </div>
                </div>
            </div>

{{--            <div class="clearfix"></div>--}}
{{--            <hr />--}}

{{--            <div class="form-group">--}}
{{--                <label for="profit" class="col-sm-2 control-label">Дополнительные опция</label>--}}
{{--                <div class="col-sm-10">--}}
{{--                    @can('admin_roles')--}}
{{--                        <div class="form-group col-md-2">--}}
{{--                            <div class="control-label-br">Роли, которые могут вносить изменения</div>--}}
{{--                            {{ Form::select('roles[]', $roles->pluck('name', 'id'), null,--}}
{{--                                     ['multiple', 'class' => 'selectpicker', 'data-width'=>'300', 'data-live-search' => 'true']) }}--}}
{{--                        </div>--}}
{{--                    @endcan--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/gateways/merchant') }}">Назад</md-button>
        </div>
    </form>
@endsection
