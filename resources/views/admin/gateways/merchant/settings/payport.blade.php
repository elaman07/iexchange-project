<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="type_pay" data-width="300px">
            <option value="0" @if($item->type_pay == 0) selected @endif>Переадресация на (платежную форму)</option>
            <option value="1" @if($item->type_pay == 1) selected @endif>Выдача реквизитов</option>
        </select>
    </div>
</div>


<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Название банка</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="bank_name" data-width="300px" data-size="8" data-live-search="true">
            <optgroup label="RU Банки">
                @foreach(['Sberbank', 'Tinkoff Bank', 'Другой банк РФ', 'Raiffeisen'] as $value)
                    <option value="{{$value}}" @if($item->bank_name == $value) selected @endif>{{ $value }}</option>
                @endforeach
            </optgroup>
            <optgroup label="KZ Банки">
                @foreach(['Halyk Bank', 'Eurasian bank', 'Jusan Bank', 'Kaspi Bank'] as $value)
                    <option value="{{$value}}" @if($item->bank_name == $value) selected @endif>{{ $value }}</option>
                @endforeach
            </optgroup>

            <optgroup label="UAH Банки">
                @foreach(['PrivatBank', 'Monobank', 'Будь-який український банк'] as $value)
                    <option value="{{$value}}" @if($item->bank_name == $value) selected @endif>{{ $value }}</option>
                @endforeach
            </optgroup>
        </select>фв
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Опции</label>
    <div class="col-sm-10">

        <div class="form-group col-md-3">
            <div class="control-label-br">Мин. время ожидании поступлений</div>
            <input type="number" name="max_register_blockchain" value="{{ $item->max_register_blockchain }}" class="form-control">
        </div>

        <div class="form-group col-md-3">
            <div class="control-label-br">Макс. время ожидании поступлений</div>
            <input type="number" name="max_first_confirm_blockchain" value="{{ $item->max_first_confirm_blockchain }}" class="form-control">
        </div>
    </div>
</div>
