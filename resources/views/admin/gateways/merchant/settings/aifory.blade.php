<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="type_pay" data-width="300px">
            <option value="0" @if($item->type_pay == 0) selected @endif>P2P form</option>
            <option value="1" @if($item->type_pay == 1) selected @endif>H2H form</option>
        </select>
    </div>
</div>


@php
    $currencies_options = json_decode(file_get_contents(storage_path('/gateways/options/aifory_currencies.json')), true);
    $codes_options = json_decode(file_get_contents(storage_path('/gateways/options/aifory_codes.json')), true);
@endphp


<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Код валюты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px" data-size="8" data-live-search="true">
            @foreach($codes_options as $value)
                <option value="{{ $value['ID'] }}" @if($item->method_pay == $value['ID']) selected @endif>{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
</div>



<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Название банка</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="bank_name" data-width="300px" data-size="8" data-live-search="true">
            @foreach($currencies_options as $value)
                <option value="{{ $value['ID'] }}" @if($value['ID'] == $item->bank_name) selected @endif>{{ $value['name'] }} ({{ $value['currencyName'] }})</option>
            @endforeach
        </select>
    </div>
</div>

<div class="clearfix"></div>
<hr />
<div class="form-group">
    <label class="col-sm-2 control-label">Опции</label>
    <div class="col-sm-10">

        <div class="form-group col-md-3">
            <div class="control-label-br">Мин. время ожидании поступлений</div>
            <input type="number" name="max_register_blockchain" value="{{ $item->max_register_blockchain }}" class="form-control">
        </div>

        <div class="form-group col-md-3">
            <div class="control-label-br">Макс. время ожидании поступлений</div>
            <input type="number" name="max_first_confirm_blockchain" value="{{ $item->max_first_confirm_blockchain }}" class="form-control">
        </div>
    </div>
</div>
