@php
    $codes_options = json_decode(file_get_contents(storage_path('/gateways/options/payone_codes.json')), true);
@endphp


<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Способ оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px" data-size="4" data-live-search="true">
            @foreach($codes_options as $value)
                <option value="{{ $value['UUID'] }}" @if($value['UUID'] == $item->method_pay) selected @endif>{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group">
    <label class="col-sm-2 control-label">Опции</label>
    <div class="col-sm-10">

        <div class="form-group col-md-6">
            <div class="control-label-br">Мин. время ожидании поступлений</div>
            <input type="number" name="max_register_blockchain" value="{{ $item->max_register_blockchain }}" class="form-control">
        </div>

        <div class="form-group col-md-6">
            <div class="control-label-br">Макс. время ожидании поступлений</div>
            <input type="number" name="max_first_confirm_blockchain" value="{{ $item->max_first_confirm_blockchain }}" class="form-control">
        </div>
    </div>
</div>
