<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Название банка</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="bank_name" data-width="300px" data-size="8">
            <option value="0" @if($item->bank_name == 0) selected @endif>Случайный</option>
            <option value="1" @if($item->bank_name == 1) selected @endif>visa</option>
            <option value="2" @if($item->bank_name == 2) selected @endif>mastercard</option>
            <option value="3" @if($item->bank_name == 3) selected @endif>maestro</option>
            <option value="4" @if($item->bank_name == 4) selected @endif>mir</option>
            <optgroup label="RU Банки">
                <option value="5" @if($item->bank_name == 5) selected @endif>sberbank</option>
                <option value="6" @if($item->bank_name == 6) selected @endif>otkritie</option>
                <option value="7" @if($item->bank_name == 7) selected @endif>tinkoff</option>
                <option value="8" @if($item->bank_name == 8) selected @endif>raiffeisenbank</option>
                <option value="9" @if($item->bank_name == 9) selected @endif>alfa</option>
                <option value="10" @if($item->bank_name == 10) selected @endif>vtb</option>
                <option value="11" @if($item->bank_name == 11) selected @endif>mkb</option>
                <option value="12" @if($item->bank_name == 12) selected @endif>sovcombank</option>
            </optgroup>
            <optgroup label="KZ Банки">
                <option value="13" @if($item->bank_name == 13) selected @endif>kaspi</option>
                <option value="14" @if($item->bank_name == 14) selected @endif>jusan</option>
                <option value="15" @if($item->bank_name == 15) selected @endif>eurasian</option>
            </optgroup>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Опции</label>
    <div class="col-sm-10">

        <div class="form-group col-md-3">
            <div class="control-label-br">Мин. время ожидании поступлений</div>
            <input type="number" name="max_register_blockchain" value="{{ $item->max_register_blockchain }}" class="form-control">
        </div>

        <div class="form-group col-md-3">
            <div class="control-label-br">Макс. время ожидании поступлений</div>
            <input type="number" name="max_first_confirm_blockchain" value="{{ $item->max_first_confirm_blockchain }}" class="form-control">
        </div>
    </div>
</div>
