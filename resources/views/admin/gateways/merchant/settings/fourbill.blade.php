<div class="form-group">
    <label for="is_pay_commission" class="col-sm-2 control-label">Кто платит комиссию?</label>
    <div class="col-sm-2">
        <select class="selectpicker form-control" name="is_pay_commission">
            <option value="0" @if($item->is_pay_commission == 0) selected @endif>Обменник</option>
            <option value="1" @if($item->is_pay_commission == 1) selected @endif>Клиент</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="fixed_fee" class="col-sm-2 control-label">Комиссия</label>
    <div class="col-sm-1">
        <input type="text" name="fixed_fee" class="form-control" value="{{ $item->fixed_fee }}">
    </div>
</div>

<div class="form-group">
    <label for="is_pay_commission" class="col-sm-2 control-label">Включить проверку антифрода</label>
    <div class="col-sm-2">
        <select class="selectpicker form-control" name="is_card_found">
            <option value="0" @if($item->is_card_found == 0) selected @endif>Нет</option>
            <option value="1" @if($item->is_card_found == 1) selected @endif>Да</option>
        </select>
    </div>
</div>
