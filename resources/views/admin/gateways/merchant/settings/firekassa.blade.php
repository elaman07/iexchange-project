<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Способ оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px">
            <option value="0" @if($item->method_pay == 0) selected @endif>Bank wallet (Card) / Qiwi</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>Qiwi (Карты)</option>
            <option value="2" @if($item->method_pay == 2) selected @endif>ЮMoney</option>
        </select>
    </div>
</div>


<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="site_account" data-width="300px">
            <option value="0" @if($item->site_account == 0) selected @endif>QIWI P2P (Wallet)</option>
            <option value="1" @if($item->site_account == 1) selected @endif>Sber</option>
            <option value="2" @if($item->site_account == 2) selected @endif>Tinkoff</option>
            <option value="3" @if($item->site_account == 3) selected @endif>Visa\MasterCard\Mir RUB</option>
            <option value="4" @if($item->site_account == 4) selected @endif>ПриватБанк UAH</option>
            <option value="5" @if($item->site_account == 5) selected @endif>МоноБанк UAH</option>
            <option value="6" @if($item->site_account == 6) selected @endif>Visa\MasterCard UAH</option>


        </select>
    </div>
</div>
<hr />


<div class="form-group">
    <label class="col-sm-2 control-label">Опции</label>
    <div class="col-sm-10">

        <div class="form-group col-md-3">
            <div class="control-label-br">Мин. время ожидании поступлений</div>
            <input type="number" name="max_register_blockchain" value="{{ $item->max_register_blockchain }}" class="form-control">
        </div>

        <div class="form-group col-md-3">
            <div class="control-label-br">Макс. время ожидании поступлений</div>
            <input type="number" name="max_first_confirm_blockchain" value="{{ $item->max_first_confirm_blockchain }}" class="form-control">
        </div>
    </div>
</div>

