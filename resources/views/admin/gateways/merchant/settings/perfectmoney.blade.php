<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Способ оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px">
            <option value="0" @if($item->method_pay == 0) selected @endif>Счет</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>e-Voucher</option>
        </select>
    </div>
</div>
