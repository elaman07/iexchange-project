<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Способ оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px">
            <option value="0" @if($item->method_pay == 0) selected @endif>AliKassa</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>Банковские карты (RUB/EUR/KZT/UAH)</option>
            <option value="2" @if($item->method_pay == 2) selected @endif>Bitcoin</option>
            <option value="3" @if($item->method_pay == 3) selected @endif>Litecoin</option>
            <option value="4" @if($item->method_pay == 4) selected @endif>Dash</option>
            <option value="5" @if($item->method_pay == 5) selected @endif>ZCash</option>

        </select>
    </div>
</div>
