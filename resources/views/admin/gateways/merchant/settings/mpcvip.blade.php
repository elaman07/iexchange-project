<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="type_pay" data-width="300px">
            <option value="0" @if($item->type_pay == 0) selected @endif>MPCVip</option>
            <option value="1" @if($item->type_pay == 1) selected @endif>P2P</option>
        </select>
    </div>
</div>


<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Способ оплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px">
            <option value="0" @if($item->method_pay == 0) selected @endif>Qiwi</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>Qiwi (Карты)</option>
        </select>
    </div>
</div>


<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Название банка</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="bank_name" data-width="300px" data-size="8">
            <option value="1" @if($item->bank_name == 1) selected @endif>Случайный</option>
            <option value="2" @if($item->bank_name == 2) selected @endif>TCSBRUB</option>
            <option value="3" @if($item->bank_name == 3) selected @endif>SBERRUB</option>
            <option value="4" @if($item->bank_name == 4) selected @endif>RFBRUB</option>
            <option value="5" @if($item->bank_name == 5) selected @endif>OPNBRUB</option>
            <option value="6" @if($item->bank_name == 6) selected @endif>ALFARUB</option>
            <option value="7" @if($item->bank_name == 7) selected @endif>VTBRUB</option>
            <option value="8" @if($item->bank_name == 8) selected @endif>MKBRUB</option>
            <option value="9" @if($item->bank_name == 9) selected @endif>SOVKRUB</option>
        </select>
    </div>
</div>
