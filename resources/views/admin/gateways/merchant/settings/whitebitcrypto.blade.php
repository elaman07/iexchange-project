<hr />


<div class="form-group">
    <label class="col-sm-2 control-label">Опции</label>
    <div class="col-sm-10">


        <div class="form-group col-md-3">
            <div class="control-label-br">Макс. время регистрации в блокчейне (в мин.)</div>
            <input type="number" name="max_register_blockchain" value="{{ $item->max_register_blockchain }}" class="form-control">
        </div>

        <div class="form-group col-md-3">
            <div class="control-label-br">Макс. время получения 1-го подтверждения (в часах.)</div>
            <input type="number" name="max_first_confirm_blockchain" value="{{ $item->max_first_confirm_blockchain }}" class="form-control">
        </div>

        <div class="clearfix"></div>
        <hr />
        <div class="form-group col-md-6">
            <div class="control-label-br">Код валюты</div>
            <input type="text" name="code_currency" value="{{ $item->code_currency }}" class="form-control">
        </div>
    </div>
</div>
