<div class="form-group col-md-6">
    <div class="control-label-br">ID Кассы</div>
    <input type="text" name="kassa_id" class="form-control" id="kassa_id" placeholder="{{ security_pay_data($data, 'kassa_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Секретный ключ</div>
    <input type="text" name="secret_key" class="form-control" id="secret_key" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>
