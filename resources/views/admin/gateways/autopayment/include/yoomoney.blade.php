<div class="form-group col-md-6">
    <div class="control-label-br">Номер кошелька</div>
    <input type="text" name="account_id" class="form-control" id="account_id" placeholder="{{ security_pay_data($data, 'account_id') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">Client ID</div>
    <input type="text" name="client_id" class="form-control" id="client_id" placeholder="{{ security_pay_data($data, 'client_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Client Secret</div>
    <input type="text" name="client_secret" class="form-control" id="client_secret" placeholder="{{ security_pay_data($data, 'client_secret') }}">
</div>
