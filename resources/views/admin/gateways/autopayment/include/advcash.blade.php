<div class="form-group col-md-6">
    <div class="control-label-br">E-mail аккаунта</div>
    <input type="text" name="api_account_email" class="form-control" id="api_account_email" placeholder="{{ security_pay_data($data, 'api_account_email') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">Название API</div>
    <input type="text" name="api_name" class="form-control" id="api_name" placeholder="{{ security_pay_data($data, 'api_name') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Пароль от API</div>
    <input type="text" name="api_secret" class="form-control" id="api_secret" placeholder="{{ security_pay_data($data, 'api_secret') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">R номер кошелька</div>
    <input type="text" name="api_r_wallet" class="form-control" id="api_r_wallet" placeholder="{{ security_pay_data($data, 'api_r_wallet') }}">
    <div class="input-p-text">Укажите номер кошелька без пробелов</div>
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">U номер кошелька</div>
    <input type="text" name="api_u_wallet" class="form-control" id="api_u_wallet" placeholder="{{ security_pay_data($data, 'api_u_wallet') }}">
    <div class="input-p-text">Укажите номер кошелька без пробелов</div>
</div>

<div class="clearfix"></div>

<div class="form-group col-md-6">
    <div class="control-label-br">E номер кошелька</div>
    <input type="text" name="api_e_wallet" class="form-control" id="api_e_wallet" placeholder="{{ security_pay_data($data, 'api_e_wallet') }}">
    <div class="input-p-text">Укажите номер кошелька без пробелов</div>
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">H номер кошелька</div>
    <input type="text" name="api_h_wallet" class="form-control" id="api_h_wallet" placeholder="{{ security_pay_data($data, 'api_h_wallet') }}">
    <div class="input-p-text">Укажите номер кошелька без пробелов</div>
</div>

