<div class="form-group col-md-6">
    <div class="control-label-br">Secret Key</div>
    <input type="text" name="secret_key" class="form-control" id="client_id" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Название сайта</div>
    <input type="text" name="site_name" class="form-control" id="site_name" placeholder="{{ security_pay_data($data, 'site_name') }}">
</div>


<div class="form-group col-md-6">
    <div class="control-label-br">Адрес сайта</div>
    <input type="text" name="site_url" class="form-control" id="site_url" placeholder="{{ security_pay_data($data, 'site_url') }}">
</div>
