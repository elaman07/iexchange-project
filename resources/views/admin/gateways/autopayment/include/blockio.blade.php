<div class="form-group col-md-6">
    <div class="control-label-br">Secret PIN</div>
    <input type="text" name="secret_pin" class="form-control" id="secret_pin" placeholder="{{ security_pay_data($data, 'secret_pin') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">Bitcoin API Key</div>
    <input type="text" name="api_key_btc" class="form-control" id="api_key_btc" placeholder="{{ security_pay_data($data, 'api_key_btc') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Litecoin API Key</div>
    <input type="text" name="api_key_ltc" class="form-control" id="api_key_ltc" placeholder="{{ security_pay_data($data, 'api_key_ltc') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Dogecoin API Key</div>
    <input type="text" name="api_key_doge" class="form-control" id="api_key_doge" placeholder="{{ security_pay_data($data, 'api_key_doge') }}">
</div>
