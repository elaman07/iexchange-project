<div class="form-group col-md-6">
    <div class="control-label-br">API KEY</div>
    <input type="text" name="api_key" class="form-control" id="api_key" placeholder="{{ security_pay_data($data, 'api_key') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Master Key</div>
    <input type="text" name="master_key" class="form-control" id="private_key" placeholder="{{ security_pay_data($data, 'master_key') }}">
</div>
