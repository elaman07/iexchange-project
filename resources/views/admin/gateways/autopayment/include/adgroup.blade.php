<div class="form-group col-md-6">
    <div class="control-label-br">Client ID</div>
    <input type="text" name="client_id" class="form-control" id="client_id" placeholder="{{ security_pay_data($data, 'client_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Client Secret</div>
    <input type="text" name="client_secret" class="form-control" id="client_secret" placeholder="{{ security_pay_data($data, 'client_secret') }}">
</div>

<div class="clearfix"></div>

<div class="form-group col-md-6">
    <div class="control-label-br">User ID</div>
    <input type="text" name="user_id" class="form-control" id="user_id" placeholder="{{ security_pay_data($data, 'user_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">PIN</div>
    <input type="text" name="pin" class="form-control" id="pin" placeholder="{{ security_pay_data($data, 'pin') }}">
</div>

