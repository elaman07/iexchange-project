<div class="form-group col-md-6">
    <label for="wmid" class="col-sm-2 control-label">WMID</label>
    <div class="col-sm-4">
        <input type="text" name="wmid" class="form-control" id="wmid" placeholder="{{ security_pay_data($data, 'wmid') }}">
    </div>
</div>

<div class="form-group col-md-6">
    <label for="key_pass" class="col-sm-2 control-label">Пароль от файла ключей .kwm</label>
    <div class="col-sm-4">
        <input type="text" name="key_pass" class="form-control" id="key_pass" placeholder="{{ security_pay_data($data, 'key_pass') }}">
    </div>
</div>

<hr />

@php
    $purses = ['wmr', 'wmz', 'wme', 'wmu', 'wmb', 'wmy', 'wmx', 'wmk', 'wml', 'wmh'];
@endphp

@foreach($purses as $value)
<div class="form-group">
    <label for="{{ $value.'_purse' }}" class="col-sm-2 control-label">{{ mb_strtoupper($value) }} кошелек</label>
    <div class="col-sm-4">
        <input type="text" name="{{ $value.'_purse' }}" class="form-control" id="{{ $value.'_purse' }}" placeholder="{{ security_pay_data($data, $value.'_purse') }}">
    </div>
</div>
@endforeach
