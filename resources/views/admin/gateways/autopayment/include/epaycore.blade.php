<div class="form-group col-md-3">
    <div class="control-label-br">API ID</div>
    <input type="text" name="api_id" class="form-control" id="api_id" placeholder="{{ security_pay_data($data, 'api_id') }}">
</div>


<div class="form-group col-md-3">
    <div class="control-label-br">API Secret</div>
    <input type="text" name="api_secret" class="form-control" id="api_secret" placeholder="{{ security_pay_data($data, 'api_secret') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-3">
    <div class="control-label-br">Номер кошелька (USD)</div>
    <input type="text" name="account_usd" class="form-control" id="account_usd" placeholder="{{ security_pay_data($data, 'account_usd') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Номер кошелька (UAH)</div>
    <input type="text" name="account_uah" class="form-control" id="account_uah" placeholder="{{ security_pay_data($data, 'account_uah') }}">
</div>

<div class="form-group col-md-3">
    <div class="control-label-br">Номер кошелька (RUB)</div>
    <input type="text" name="account_rub" class="form-control" id="account_rub" placeholder="{{ security_pay_data($data, 'account_rub') }}">
</div>
