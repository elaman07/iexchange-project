<div class="form-group col-md-6">
    <div class="control-label-br">Token (Merchant)</div>
    <input type="text" name="token_merchant" class="form-control" id="token_merchant" placeholder="{{ security_pay_data($data, 'token_merchant') }}">
</div>


<div class="form-group col-md-6">
    <div class="control-label-br">Token (Pay)</div>
    <input type="text" name="token_pay" class="form-control" id="token_pay" placeholder="{{ security_pay_data($data, 'token_pay') }}">
</div>
