<div class="form-group col-md-6">
    <div class="control-label-br">ID аккаунта</div>
    <input type="text" name="account_id" class="form-control" id="account_id" placeholder="{{ security_pay_data($data, 'account_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Пароль от аккаунта</div>
    <input type="text" name="account_password" class="form-control" id="account_password" placeholder="{{ security_pay_data($data, 'account_password') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-4">
    <div class="control-label-br">USD счет</div>
    <input type="text" name="account_usd" class="form-control" id="account_usd" placeholder="{{ security_pay_data($data, 'account_usd') }}">
</div>

<div class="form-group col-md-4">
    <div class="control-label-br">EUR счет</div>
    <input type="text" name="account_eur" class="form-control" id="account_eur" placeholder="{{ security_pay_data($data, 'account_eur') }}">
</div>

<div class="form-group col-md-4">
    <div class="control-label-br">BTC счет</div>
    <input type="text" name="account_btc" class="form-control" id="account_btc" placeholder="{{ security_pay_data($data, 'account_btc') }}">
</div>
