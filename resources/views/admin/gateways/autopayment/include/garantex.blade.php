<div class="form-group col-md-6">
    <div class="control-label-br">Private Key</div>
    <input type="text" name="private_key" class="form-control" id="private_key" placeholder="{{ security_pay_data($data, 'private_key') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">UUID</div>
    <input type="text" name="uuid" class="form-control" id="uuid" placeholder="{{ security_pay_data($data, 'uuid') }}">
</div>
