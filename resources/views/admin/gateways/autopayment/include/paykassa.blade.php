<div class="form-group col-md-6">
    <div class="control-label-br">ID Магазина</div>
    <input type="text" name="id_shop" class="form-control" id="id_shop" placeholder="{{ security_pay_data($data, 'id_shop') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Секретный ключ магазина</div>
    <input type="text" name="secret_key" class="form-control" id="secret_key" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">API ID</div>
    <input type="text" name="api_id" class="form-control" id="api_id" placeholder="{{ security_pay_data($data, 'api_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">API Secret</div>
    <input type="text" name="api_secret" class="form-control" id="api_secret" placeholder="{{ security_pay_data($data, 'api_secret') }}">
</div>

