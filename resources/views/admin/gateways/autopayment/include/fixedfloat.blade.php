<div class="form-group col-md-6">
    <div class="control-label-br">Api Key</div>
    <input type="text" name="api_key" class="form-control" id="api_key" placeholder="{{ security_pay_data($data, 'api_key') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Secret Key</div>
    <input type="text" name="secret_key" class="form-control" id="secret_key" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>
