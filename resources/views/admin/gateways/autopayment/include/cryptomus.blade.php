<div class="form-group col-md-6">
    <div class="control-label-br">Merchant ID</div>
    <input type="text" name="merchant_id" class="form-control" id="merchant_id" placeholder="{{ security_pay_data($data, 'merchant_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Secret Key</div>
    <input type="text" name="secret_key" class="form-control" id="secret_key" placeholder="{{ security_pay_data($data, 'secret_key') }}">
</div>

<div class="clearfix"></div>
<div class="form-group col-md-6">
    <div class="control-label-br">Payout Key</div>
    <input type="text" name="payout_key" class="form-control" id="payout_key" placeholder="{{ security_pay_data($data, 'payout_key') }}">
</div>
