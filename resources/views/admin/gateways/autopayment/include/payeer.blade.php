<div class="form-group col-md-6">
    <div class="control-label-br">Номер кошелька</div>
    <input type="text" name="account_id" class="form-control" id="account_id" placeholder="{{ security_pay_data($data, 'account_id') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">API ID</div>
    <input type="text" name="api_id" class="form-control" id="api_id" placeholder="{{ security_pay_data($data, 'api_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">API Ключ</div>
    <input type="text" name="api_key" class="form-control" id="api_key" placeholder="{{ security_pay_data($data, 'api_key') }}">
</div>


