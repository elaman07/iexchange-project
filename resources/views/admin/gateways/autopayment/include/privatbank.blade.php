<div class="form-group col-md-6">
    <div class="control-label-br">ID мерчанта</div>
    <input type="text" name="merchant_id" class="form-control" id="merchant_id" placeholder="{{ security_pay_data($data, 'merchant_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Пароль</div>
    <input type="text" name="password" class="form-control" id="password" placeholder="{{ security_pay_data($data, 'password') }}">
</div>

<div class="clearfix"></div>

<div class="form-group col-md-6">
    <div class="control-label-br">Номер карты</div>
    <input type="text" name="card" class="form-control" id="card" placeholder="{{ security_pay_data($data, 'card') }}">
</div>

