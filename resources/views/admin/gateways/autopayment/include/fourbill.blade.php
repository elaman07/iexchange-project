<div class="form-group col-md-6">
    <div class="control-label-br">Point ID</div>
    <input type="text" name="point_id" class="form-control" id="point_id" placeholder="{{ security_pay_data($data, 'point_id') }}">
</div>

<div class="form-group col-md-6">
    <div class="control-label-br">Account ID</div>
    <input type="text" name="acc_id" class="form-control" id="acc_id" placeholder="{{ security_pay_data($data, 'acc_id') }}">
</div>


<div class="clearfix"></div>

<div class="form-group col-md-6">
    <div class="control-label-br">Wallet ID</div>
    <input type="text" name="wallet_id" class="form-control" id="wallet_id" placeholder="{{ security_pay_data($data, 'wallet_id') }}">
</div>


<div class="form-group col-md-6">
    <div class="control-label-br">Service ID</div>
    <input type="text" name="service_id" class="form-control" id="service_id" placeholder="{{ security_pay_data($data, 'service_id') }}">
</div>


<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">Token</div>
    <input type="text" name="token_id" class="form-control" id="token_id" placeholder="{{ security_pay_data($data, 'token_id') }}">
</div>
