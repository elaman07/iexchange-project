<div class="form-group col-md-6">
    <div class="control-label-br">API key</div>
    <input type="text" name="api_key" class="form-control" id="api_key" placeholder="{{ security_pay_data($data, 'api_key') }}">
</div>

<div class="clearfix"></div>
<hr />

<div class="form-group col-md-6">
    <div class="control-label-br">Точка входа (URL)</div>
    <input type="text" name="user_url" class="form-control" id="user_url" placeholder="{{ security_pay_data($data, 'user_url') }}">
</div>
