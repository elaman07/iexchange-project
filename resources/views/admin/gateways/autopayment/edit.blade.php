@extends('admin.layouts.app')

@section('title', 'Изменить автовыплату '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.gateways.autopayment.edit', $item))


@section('content')

    <form action="{{ admin_base_path('/gateways/autopayment/'.$item->id) }}" method="POST" class="form-horizontal">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Основное</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Название</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $item->name }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Провайдер</div>
                        {{ Form::select('id_gateways', $gateways, $item->id_gateways, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '15', 'disabled']) }}
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <div class="material-switch switch-input-1">
                            <input id="status" name="status" type="checkbox" @if($item->status == 1) checked @endif value="1" />
                            <label for="status" class="label-success"></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">Данные выплат</label>
                <div class="col-sm-10">
                    @if(view()->exists('admin.gateways.autopayment.include.'.$item->gateway->alias))
                        @include('admin.gateways.autopayment.include.'.$item->gateway->alias)
                    @else
                        <div class="merchant-alert-notify text-center">Не найдены конфигурационные данные</div>
                    @endif
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">Настройки</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Примечание для платежа') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#comment-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @include('admin._partials.editors.gateway_style')
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="comment-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                @foreach(config('admin-settings.editor') as $key => $value)
                                    <a class="bbcode-editor" onclick='insertTextareaBBCode(this, "{{ $key }}", "gateway-editors{{$form_lang['field']}}")' href='#'>{{ $value }}</a>
                                @endforeach
                                <textarea rows="5" class="form-control" id="gateway-editors{{$form_lang['field']}}" name="comment{{$form_lang['field']}}">{{ $item->getTranslation('comment', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>



                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Ручная выплата (кнопка)</div>
                        <select class="form-control selectpicker" name="manual_pay_order" data-width="300px">
                            <option value="0" @if($item->manual_pay_order == 0) selected @endif>Включена</option>
                            <option value="1" @if($item->manual_pay_order == 1) selected @endif>Отключена</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Отключить проверку баланса</div>
                        <select class="form-control selectpicker" name="hide_check_balance" data-width="300px">
                            <option value="0" @if($item->hide_check_balance == 0) selected @endif>Нет</option>
                            <option value="1" @if($item->hide_check_balance == 1) selected @endif>Да</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Суммы выплаты</div>
                        <select class="form-control selectpicker" name="pay_amount_type" data-width="300px">
                            <option value="0" @if($item->pay_amount_type == 0) selected @endif>Сумма Получаю (с комиссией доп. и ПС)</option>
                            <option value="1" @if($item->pay_amount_type == 1) selected @endif>Сумма Получаю (с доп. комиссией)</option>
                            <option value="2" @if($item->pay_amount_type == 2) selected @endif>Сумма Получаю (без комиссий)</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            @includeIf('admin.gateways.autopayment.settings.'.$item->gateway->alias)

            @if(\Module::find('ProxyManager')->isEnabled())
                <hr />
                <div class="form-group">
                    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Привязать Proxy</label>
                    <div class="col-sm-2">
                        <select class="form-control selectpicker" name="id_proxy">
                            <option value="0">-- Не выбрано --</option>
                            @foreach($proxies as $proxy)
                                <option value="{{$proxy->id}}"  @if($item->id_proxy == $proxy->id) selected @endif>{{ $proxy->address }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/gateways/autopayment') }}">Назад</md-button>
        </div>
    </form>
@endsection
