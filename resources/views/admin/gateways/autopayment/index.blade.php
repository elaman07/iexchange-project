@extends('admin.layouts.app')

@section('title', 'Список автовыплат')

@section('top-block')
    <md-button ng-href="autopayment/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">Добавить автовыплату</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.gateways.autopayment'))

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>@yield('title')</h2>
        </div>
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Название</th>
                <th>Алиас</th>
                <th>Привязанные валюты</th>
                <th>Всего заявок</th>
                <th>Объем в USD</th>
                <th>Дата создания</th>
                <th>Посл. обновление</th>
                <th>Статус</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @if(count($payments) > 0)
                @foreach($payments as $payment)
                    <tr>
                        <th>
                            <a  href="{{ admin_base_path('gateways/autopayment/'.$payment->id.'/edit') }}">
                                {{ $payment->name }} &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>{{ isset($payment->gateway) ? $payment->gateway->alias : '' }} (v{{ isset($payment->gateway) ? $payment->gateway->version : 0 }})</td>

                        <td>
                            @if($payment->currency->count() > 0)
                                <ul class="list-unstyled">
                                    @foreach($payment->currency as $value)
                                        <li>- <a style="font-size: 11px;color: @if($value->status == 0)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/currency/'.$value->id.'/edit') }}">{{ $value->payment->name }}</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <small class="text-danger">Не найдено</small>
                            @endif
                        </td>
                        <td>
                            {{ iex_number_format($payment->order_count, 0, true) }}
                            @if($payment->last_order_id > 0)
                                <div class="input-p-text font-size-11">Посл. заявка: <a href="{{ admin_base_path('/tx/'.$payment->last_order_id.'?actions=arhive') }}">№{{ $payment->last_order_id }}</a></div>
                            @endif
                        </td>

                        <td>
                            ${{ iex_number_format($payment->volume_to_usd, 2, true) }}
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($payment->created_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($payment->created_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($payment->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($payment->updated_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <input type="hidden" name="key_id[]" value="{{$payment->id}}">
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$payment->id}}" name="status[{{$payment->id}}]" type="checkbox" value="1" @if($payment->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$payment->id}}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width: 100px;">
                            @if($payment->currency->count() > 0)
                                    <i style="padding-left: 20px;" class="fas fa-lock text-muted" data-toggle="tooltip" data-title="Перед удалением {{ $payment->name }}, отвяжите от валют"></i>
                            @else
                                <md-button title="Удалить" href="{{ admin_base_path('/gateways/autopayment/'.$payment->id.'/delete') }}" class="md-icon-button">
                                    <i class="fas fa-trash text-danger"></i>
                                </md-button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />Список пуст</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">Сохранить</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
    </div>
@endsection
