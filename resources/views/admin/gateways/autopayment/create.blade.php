@extends('admin.layouts.app')

@section('title', 'Добавить автовыплату')

@section('breadcrumbs', Breadcrumbs::render('admin.gateways.autopayment.create'))


@section('content')
    <form action="{{ admin_base_path('/gateways/autopayment') }}" method="POST" class="form-horizontal">
        @csrf
        <div class="default-panel-body">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Название</label>
                <div class="col-sm-6">
                    <input type="text" name="name" class="form-control" id="name">
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Провайдер</label>
                <div class="col-sm-6">
                    {{ Form::select('id_gateways', $gateways, null,
                ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '15']) }}
                </div>
            </div>

            <hr />
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Статус</label>
                <div class="col-sm-6">
                    <div class="material-switch switch-input-1">
                        <input id="status" name="status" type="checkbox" value="1" />
                        <label for="status" class="label-success"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/gateways/autopayment') }}">Назад</md-button>
        </div>
    </form>
@endsection
