<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип выплаты</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="method_pay">
            <option value="0" @if($item->method_pay == 0) selected @endif>Qiwi</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>Карта</option>
            <option value="2" @if($item->method_pay == 2) selected @endif>ЮMoney</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Способ выплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="site_account" data-width="300px">
            <option value="0" @if($item->site_account == 0) selected @endif>QIWI P2P (Wallet)</option>
            <option value="1" @if($item->site_account == 1) selected @endif>Sber</option>
            <option value="2" @if($item->site_account == 2) selected @endif>Tinkoff</option>
            <option value="3" @if($item->site_account == 3) selected @endif>Visa\MasterCard\Mir RUB</option>
            <option value="4" @if($item->site_account == 4) selected @endif>ПриватБанк UAH</option>
            <option value="5" @if($item->site_account == 5) selected @endif>МоноБанк UAH</option>
            <option value="6" @if($item->site_account == 6) selected @endif>Visa\MasterCard UAH</option>
        </select>
    </div>
</div>
