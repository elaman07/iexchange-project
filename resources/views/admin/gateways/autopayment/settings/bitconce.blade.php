<div class="form-group">
    <label class="col-sm-2 control-label">Направление</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="direction">
            <option value="0" @if($item->direction == 0) selected @endif>banks</option>
            <option value="1" @if($item->direction == 1) selected @endif>qiwi</option>
            <option value="2" @if($item->direction == 2) selected @endif>yandex</option>
        </select>
    </div>
</div>
