<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Отключить проверку баланса</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="hide_check_balance">
            <option value="0" @if($item->hide_check_balance == 0) selected @endif>Нет</option>
            <option value="1" @if($item->hide_check_balance == 1) selected @endif>Да</option>
        </select>
    </div>
</div>
