<div class="form-group">
    <label for="min_confirm" class="col-sm-2 control-label">Кол-во подтверждений, чтобы считать платеж действительным</label>
    <div class="col-sm-1">
        <input type="number" name="min_confirm" class="form-control" value="{{ $item->min_confirm }}">
    </div>
</div>
