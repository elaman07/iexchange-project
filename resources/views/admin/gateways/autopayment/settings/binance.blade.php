<div class="clearfix"></div>
<hr />

<!-- Код валюты -->
<div class="form-group">
    <label class="col-sm-2 control-label">{{ __('Опции для Binance') }}</label>
    <div class="col-sm-10">
        <div class="form-group col-md-6">
            <div class="control-label-br">{{ __('Разрешить докупить недостающую сумму для вывода') }}</div>
            <select class="form-control selectpicker" name="exchange_buy" data-width="300px">
                <option value="0" @if($item->exchange_buy == 0) selected @endif>Да</option>
                <option value="1" @if($item->exchange_buy == 1) selected @endif>Нет</option>
                <option value="2" @if($item->exchange_buy == 2) selected @endif>Купить и не выводить</option>
                <option value="3" @if($item->exchange_buy == 3) selected @endif>Купить на всю сумму</option>
            </select>
            <div class="form-check">
                <input class="form-check-input" id="is_auto_take_fee" value="1" type="checkbox" name="is_auto_take_fee" @if($item->is_auto_take_fee == 1) checked @endif>
                <label class="form-check-label" for="is_auto_take_fee">Автоматически учитывать комиссию биржи</label>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group col-md-6">
            <div class="control-label-br">{{ __('Комиссия за торговую операцию на бирже') }}</div>
            <div class="input-group">
                <input type="text" name="exchange_fee" class="form-control" value="{{ $item->exchange_fee }}">
                <span class="input-group-addon" id="basic-addon3">%</span>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group col-md-6">
            <div class="control-label-br">{{ __('Тип ордера') }}</div>
            <select class="form-control selectpicker" name="exchange_buy_type">
                <option value="0" @if($item->exchange_buy_type == 0) selected @endif>Market</option>
                <option value="1" @if($item->exchange_buy_type == 1) selected @endif>Limit</option>
            </select>

            <div class="input-options-alert-1">
                <ul>
                    <li><b>Market</b> - ордер будет выполнен мгновенно, по рыночной цене.</li>
                    <li><b>Limit</b> - ордер будет размещен на бирже по цене которую вы установили. (Для этого пункта, настройте параметр "Время исполнения ордера".)</li>
                </ul>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="form-group col-md-6">
            <div class="control-label-br">{{ __('Время исполнения ордера (Для Limit)') }}</div>
            <select class="form-control selectpicker" name="time_in_force">
                <option value="GTC" @if($item->time_in_force == 'GTC') selected @endif>GTC</option>
                <option value="IOC" @if($item->time_in_force == 'IOC') selected @endif>IOC</option>
                <option value="FOK" @if($item->time_in_force == 'FOK') selected @endif>FOK</option>
            </select>

            <div class="input-options-alert-1">
                <ul>
                    <li>GTC (Good-Til-Canceled) - ордер действителен до тех пор, пока он не будут выполнен или отменен.</li>
                    <li>IOC (Immediate or Cancel) - ордер заполняется полностью или частично и отменяется оставшаяся часть ордера.</li>
                    <li>FOK (Fill or Kill) - ордер выполняется весь целиком, в противном случае весь ордер будет отменен.</li>
                </ul>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="form-group col-md-6">
            <div class="control-label-br">{{ __('Код торговой операции') }}</div>
            <select class="form-control selectpicker" name="exchange_buy_curr">
                <option value="USDT" @if($item->exchange_buy_curr == 'USDT') selected @endif>USDT</option>
                <option value="BTC" @if($item->exchange_buy_curr == 'BTC') selected @endif>BTC</option>
                <option value="USDC" @if($item->exchange_buy_curr == 'USDC') selected @endif>USDC</option>
                <option value="TUSD" @if($item->exchange_buy_curr == 'TUSD') selected @endif>TUSD</option>
            </select>
        </div>
    </div>
</div>


