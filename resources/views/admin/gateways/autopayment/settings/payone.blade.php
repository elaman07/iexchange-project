@php
    $codes_options = json_decode(file_get_contents(storage_path('/gateways/options/payone_codes.json')), true);
@endphp

<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип выплаты</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="method_pay">
            @foreach($codes_options as $value)
                <option value="{{ $value['UUID'] }}" @if($value['UUID'] == $item->method_pay) selected @endif>{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
</div>
