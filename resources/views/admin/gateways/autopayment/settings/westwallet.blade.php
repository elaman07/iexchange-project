<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Комиссия за транзакцию</label>
    <div class="col-sm-1">
        <select class="form-control selectpicker" name="priority_fee">
            <option value="low" @if($item->priority_fee == 'low') selected @endif>low</option>
            <option value="medium" @if($item->priority_fee == 'medium') selected @endif>medium</option>
            <option value="high" @if($item->priority_fee == 'high') selected @endif>high</option>
        </select>
    </div>
</div>
