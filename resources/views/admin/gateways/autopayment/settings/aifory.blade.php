@php
    $currencies_options = json_decode(file_get_contents(storage_path('/gateways/options/aifory_out.json')), true);
    $currencies_codes = json_decode(file_get_contents(storage_path('/gateways/options/aifory_codes.json')), true);
@endphp

<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип выплаты</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="method_pay">
            @foreach($currencies_options as $value)
                <option value="{{ $value['ID'] }}" @if($item->method_pay == $value['ID']) selected @endif>{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Код валюты</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="code_currency">
            @foreach($currencies_codes as $value)
                <option value="{{ $value['ID'] }}" @if($item->code_currency == $value['ID']) selected @endif>{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
</div>
