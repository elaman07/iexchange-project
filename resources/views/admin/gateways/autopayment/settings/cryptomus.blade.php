<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Комиссия за вывод средств </label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="is_subtract">
            <option value="0" @if($item->is_subtract == 0) selected @endif>С баланса</option>
            <option value="1" @if($item->is_subtract == 1) selected @endif>С суммы выплаты</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Валюта для выплаты</label>
    <div class="col-sm-2">
        <input type="text" class="form-control" name="currency_code" value="{{ $item->currency_code }}">
        <div class="input-p-text">
            Используйте код валюты, если хотите выплачивать через конвертер Fiat -> Coin<br />
            Доступен: USDT
        </div>
    </div>
</div>
