<div class="form-group">
    <label for="payment_blockio_priority" class="col-sm-2 control-label">Приоритет транзакции</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="priority_fee">
            <option value="">По умолчанию</option>
            <option value="low" @if($item->priority_fee == 'low') selected @endif>low</option>
            <option value="medium" @if($item->priority_fee == 'medium') selected @endif>medium</option>
            <option value="high" @if($item->priority_fee == 'high') selected @endif>high</option>
            <option value="custom" @if($item->priority_fee == 'custom') selected @endif>Вручную</option>
        </select>
        <div style="margin-top: 5px; font-size: 12px;" class="text-muted">Приоритет транзакции - low, medium или high</div>
    </div>
</div>


@if(iEXSetting('blockio_mass_payouts') == 1)
<div class="form-group">
    <label for="is_mass_payouts" class="col-sm-2 control-label">Массовые выплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="is_mass_payouts">
            <option value="0" @if($item->is_mass_payouts == 0) selected @endif>Отключено</option>
            <option value="1" @if($item->is_mass_payouts == 1) selected @endif>Включено</option>
        </select>
    </div>
</div>
@else
    <hr />
    <div class="form-group">
        <label for="is_mass_payouts" class="col-sm-2 control-label">Массовые выплаты</label>
        <div class="col-sm-4">
            <small class="text-danger">Модуль массовых выплат не установлен, напишите в службу поддержки для получения детальной информации</small>
        </div>
    </div>
@endif
