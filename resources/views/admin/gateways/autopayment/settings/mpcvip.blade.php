<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип выплаты</label>
    <div class="col-sm-2">
        <select class="form-control selectpicker" name="method_pay">
            <option value="0" @if($item->method_pay == 0) selected @endif>Qiwi</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>Карта</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Код страны</label>
    <div class="col-sm-1">
        <input type="text" name="country_code" class="form-control" value="{{ $item->country_code }}">
    </div>
</div>
