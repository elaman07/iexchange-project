<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Тип выплаты</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="method_pay" data-width="300px">
            <option value="0" @if($item->method_pay == 0) selected @endif>ADV-кошелек</option>
            <option value="1" @if($item->method_pay == 1) selected @endif>Карта Visa/MC</option>
            <option value="2" @if($item->method_pay == 2) selected @endif>E-mail</option>
            <option value="3" @if($item->method_pay == 3) selected @endif>ADVCash Card (Plastic)</option>
            <option value="4" @if($item->method_pay == 4) selected @endif>ADVCash Card (Virtual)</option>
            <option value="5" @if($item->method_pay == 5) selected @endif>Qiwi</option>
            <option value="6" @if($item->method_pay == 6) selected @endif>Payeer</option>
            <option value="7" @if($item->method_pay == 7) selected @endif>PerfectMoney</option>
        </select>
    </div>
</div>
