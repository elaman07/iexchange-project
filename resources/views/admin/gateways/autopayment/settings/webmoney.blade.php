<hr />
<div class="form-group">
    <label for="num_request" class="col-sm-2 control-label">Текущий ID платежа</label>
    <div class="col-sm-2">
        <input type="text" name="num_request" class="form-control" id="num_request" value="{{ $item->num_request }}">
        <div style="margin-top: 5px; font-size: 12px;" class="text-muted">
            Здесь вы можете задать текущий номер платежа, который передается в платежную систему при запросе автоматической выплаты.
            <br/><br/>
            <ul>
                <li><b>[order_id]</b> - Номер заявки</li>
                <li><b>[code]</b> - Уникальные коды</li>
            </ul>
        </div>
    </div>
</div>
