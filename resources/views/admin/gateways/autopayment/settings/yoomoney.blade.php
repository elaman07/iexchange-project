@if(empty(security_pay_data($data, 'access_token', true)))
    <small class="text-danger">Приложение не авторизовано. перейдите по ссылке для <a target="_blank" href="/merchant-verify/{{ $item->gateway->alias }}/{{ $item->id }}">авторизации</a>.</small>
@else
    <small class="text-success">Приложение авторизовано.</small>
@endif
