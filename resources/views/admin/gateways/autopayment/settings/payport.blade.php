<div class="form-group">
    <label for="max_first_confirm_blockchain" class="col-sm-2 control-label">Название банка</label>
    <div class="col-sm-4">
        <select class="form-control selectpicker" name="bank_name" data-width="300px" data-size="8" data-live-search="true">
            <optgroup label="RU Банки">
                <option value="16" @if($item->bank_name == 16) selected @endif>Alfa-Bank</option>
                <option value="24" @if($item->bank_name == 24) selected @endif>VTB</option>
                <option value="126" @if($item->bank_name == 126) selected @endif>QIWI</option>
                <option value="15" @if($item->bank_name == 15) selected @endif>Sberbank</option>
                <option value="469" @if($item->bank_name == 469) selected @endif>Другой банк РФ</option>
                <option value="516" @if($item->bank_name == 516) selected @endif>Raiffeisen</option>
                <option value="514" @if($item->bank_name == 514) selected @endif>Tinkoff Bank</option>
            </optgroup>
{{--            <optgroup label="KZ Банки">--}}
{{--                <option value="581" @if($item->bank_name == 581) selected @endif>Kaspi Bank</option>--}}
{{--                <option value="463" @if($item->bank_name == 463) selected @endif>Jusan Bank</option>--}}
{{--            </optgroup>--}}

{{--            <optgroup label="UAH Банки">--}}
{{--                <option value="27" @if($item->bank_name == 27) selected @endif>Neteller</option>--}}
{{--                <option value="477" @if($item->bank_name == 477) selected @endif>Monobank</option>--}}
{{--                <option value="28" @if($item->bank_name == 28) selected @endif>PrivatBank</option>--}}
{{--                <option value="370" @if($item->bank_name == 370) selected @endif>Будь-який український банк</option>--}}
{{--            </optgroup>--}}
        </select>
    </div>
</div>
