@extends('admin.layouts.app')

@section('title', __('Верификация личности'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.account.verification_account'))

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Верификация личности') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">


            <form method="post" action="?">
                <input type="hidden" name="actions" value="update">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('Дата создания') }}</th>
                        <th>{{ __('Пользователь') }}</th>
                        <th>{{ __('ФИО Держателя') }}</th>
                        <th>{{ __('Скан паспорта') }}</th>
                        <th>{{ __('Скан обратной стороны') }}</th>
                        <th>{{ __('Статус') }}</th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($cards) > 0)
                        @foreach($cards as $card)
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input id="checkbox{{$card->id}}" value="{{$card->id}}" type="checkbox" name="check[]">
                                        <label for="checkbox{{$card->id}}"></label>
                                    </div>
                                </th>
                                <th>
                                    {{$card->created_at}}
                                    <div class="form-subtext-2-line">
                                        {{\Illuminate\Support\Carbon::parse($card->created_at)->diffForHumans()}}
                                    </div>
                                </th>
                                <td>
                                    <div>
                                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$card->user_id) }}"><b>{{$card->user->name}}</b></a>
                                    </div>
                                    <div class="form-subtext-2-line">{{  $card->user->email }}</div>
                                </td>
                                <td>{{ $card->fio_user }}</td>
                                <td>
                                    <a href="/storage/user_verify/{{  $card->file_one }}" target="_blank">{{ __('Открыть') }}</a>
                                </td>
                                <td>
                                    <a href="/storage/user_verify/{{  $card->file_two }}" target="_blank">{{ __('Открыть') }}</a>
                                </td>
                                <td>
                                    @if($card->status == 1)
                                        <label class="label label-success">{{ __('Верифицирован') }}</label>
                                    @elseif($card->status == 2)
                                        <label class="label label-danger">{{ __('Отклонена') }}</label>
                                    @else
                                        <select class="selectpicker" name="status[{{$card->id}}]">
                                            <option value="0" @if($card->status == 0) selected @endif>{{ __('На проверке') }}</option>
                                            <option value="1" @if($card->status == 1) selected @endif>{{ __('Верифицировать') }}</option>
                                            <option value="2" @if($card->status == 2) selected @endif>{{ __('Отклонить') }}</option>
                                        </select>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                                <option value="delete">{{ __('Удалить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить модуль Верификации личности') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_exchange_verify_account" name="is_enabled_exchange_verify_account" type="checkbox" value="1" @if(iEXSetting('is_enabled_exchange_verify_account') == 1) checked @endif />
                                    <label for="is_enabled_exchange_verify_account" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {{ $cards->links() }}
    </div>
@endsection
