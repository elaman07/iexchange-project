@extends('admin.layouts.app')

@section('title', __('Список категорий'))

@section('top-block')
    <md-button ng-href="card-category/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.verification-card-category'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Сортировка') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Посл. обновление') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->sorting }}</td>
                    <td>
                        @if($item->status == 0)
                            <label class="label label-danger">{{ __('Отключен') }}</label>
                        @else
                            <label class="label label-success">{{ __('Включен') }}</label>
                        @endif
                    </td>
                    <td>{{$item->updated_at}}</td>

                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="card-category/{{$item->id}}/edit" class="btn btn-info btn-sm">{{ __('Изменить') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['card-category.destroy', $item->id] ]) !!}
                            {!! Form::submit(__('Удалить'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
