@extends('admin.layouts.app')

@section('title', __('Изменить категорию'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.verification-card-category.edit', $item))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/verifications/card-category/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Обновить категорию') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" value="{{ $item->getTranslation('name', $form_lang_key) }}"  class="form-control" id="name{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Отключен') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Включен') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Сортировка') }}</div>
                        <input type="number" name="sorting" class="form-control" value="{{ $item->sorting }}">
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Обновить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/verifications/card-category') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
