@extends('admin.layouts.app')

@section('title', __('admin-tools.card.show_title', ['id' => $card->id]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.verification_card.show', $card))

@section('content')

    <style>
        .card-verification-image {
            width: 300px;
            height: auto;
            display: block;
            text-align: center;
            margin: 0 auto;
        }

        .card-verification-image img {
            width: 100%;
            display: block;
            height: auto;
        }

        .card-verification-items .row{
            padding: 15px 0;
            border-bottom: 1px solid #e9e9e9;
        }

        .card-verification-status {
            padding: 20px;
            text-align: center;
            padding-top: 0;
        }
    </style>


<div class="row">
    <div class="col-md-8">
        <div class="card-verification-items">
            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.created_at') }}</div>
                <div class="col-md-8">{{ $card->created_at->format('d.m.Y H:i:s') }}</div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.user') }}</div>
                <div class="col-md-8">
                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$card->id_user) }}"><b>{{$card->user->name}}</b></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.currency') }}</div>
                <div class="col-md-8">
                    {{$card->currency->payment->name}} {{ $card->currency->code_currency->name }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.fio') }}</div>
                <div class="col-md-8">{{$card->name}}</div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.card_number') }}</div>
                <div class="col-md-8">{{(!empty($card->card_number_string) ? $card->card_number_string : $card->card_number)}}</div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.ip_address') }}</div>
                <div class="col-md-8">{{$card->ip_address}}</div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.user_agent') }}</div>
                <div class="col-md-8">{{$card->user_agent}}</div>
            </div>

            <div class="row">
                <div class="col-md-3">{{ __('admin-tools.card.status') }}</div>
                <div class="col-md-8">
                    @if($card->status == 0)
                        <label class="label label-info">{{ __('admin-tools.card.on_check') }}</label>
                    @elseif($card->status == 1)
                        <label class="label label-success">{{ __('admin-tools.card.verified') }}</label>
                    @else
                        <label class="label label-danger">{{ __('admin-tools.card.no_verified') }}</label>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        @if((int)iEXSetting('is_module_storage_disk') == 1 and !empty(iEXSetting('storage_disk_verification_card')) and $card->is_local_image == 1)
            <a href="{{ iex_dynamic_read_view_image('verifications/'.$card->image, false, 'verifications_card') }}" target="_blank">
                <img src="{{ iex_dynamic_read_view_image('verifications/'.$card->image, false, 'verifications_card') }}"/>
            </a>
        @else
            <a target="_blank" href="/images/verifications/{{$card->image}}" class="card-verification-image">
                <img src="/images/verifications/{{$card->image}}" />
            </a>
        @endif
    </div>
</div>

<hr />

    <div class="card-verification-status">
        <a class="btn btn-success" href="?status=1" @if($card->status == 1) disabled @endif>{{ __('admin-tools.card.verified') }}</a>
        <a class="btn btn-danger" href="?status=2" @if($card->status == 2) disabled @endif>{{ __('admin-tools.card.no_verified_update') }}</a>
        <a class="btn btn-danger" href="?status=3" @if($card->status == 3) disabled @endif>{{ __('admin-tools.card.no_verified_reject') }}</a>
    </div>


@endsection
