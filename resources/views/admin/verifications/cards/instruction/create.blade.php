@extends('admin.layouts.app')

@section('title', __('Добавить инструкцию'))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.verification-card-instructions.create'))


@section('content')
    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ admin_base_path('/verifications/card-instructions') }}">
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Новая инструкция') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Выберите категорию') }}</div>
                        <select name="id_category" class="form-control selectpicker" data-size="8">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}">
                                <p class="input-p-text">{{ __('К примеру: Первый способ') }}</p>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Текст инструкции') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea id="ckedtor-multieditor-i{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-i{{ $form_lang_key }}" name="text{{$form_lang['field']}}" rows="5"></textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Уведомление (жирный текст)') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#notice_text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="notice_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="notice_text{{$form_lang['field']}}" class="form-control" id="notice_text{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фотография (пример)') }}</div>
                        <input type="file" name="image" class="form-control" id="inputFile" required>
                    </div>


                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if(old('status') == 0) selected @endif>{{ __('Отключен') }}</option>
                            <option value="1" @if(old('status') == 1) selected @endif>{{ __('Включен') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Сортировка') }}</div>
                        <input type="number" name="sorting" class="form-control" value="{{ old('sorting', 0) }}">
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/verifications/card-instructions') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
