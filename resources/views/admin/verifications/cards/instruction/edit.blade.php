@extends('admin.layouts.app')

@section('title',  __('Изменить инструкцию :id', ['id' => $item->id]))

@section('breadcrumbs', Breadcrumbs::render('admin.tools.verification-card-instructions.edit', $item))

@section('content')
    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ admin_base_path('/verifications/card-instructions/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Обновить инструкцию') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Выберите категорию') }}</div>
                        <select name="id_category" class="form-control selectpicker" data-size="8">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($item->id_category == $category->id) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" id="name{{$form_lang['field']}}" value="{{ $item->getTranslation('name', $form_lang_key) }}" >
                                <p class="input-p-text">{{ __('К примеру: Первый способ') }}</p>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Текст инструкции') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea id="ckedtor-multieditor-i{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-i{{ $form_lang_key }}" name="text{{$form_lang['field']}}" rows="5">{!! $item->getTranslation('text', $form_lang_key) !!}</textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Уведомление (жирный текст)') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#notice_text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="notice_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="notice_text{{$form_lang['field']}}" class="form-control" id="notice_text{{$form_lang['field']}}" value="{!! $item->getTranslation('notice_text', $form_lang_key) !!}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фотография (пример)') }}</div>
                        <input type="file" name="image" class="form-control" id="inputFile">
                        <div class="input-p-text">
                            @if(isset($item->image))
                                {{ __('Пример загружен') }}: <a target="_blank" href="/storage/{{ $item->image }}">{{ $item->image }}</a>
                            @endif
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Отключен') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Включен') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Сортировка') }}</div>
                        <input type="number" name="sorting" class="form-control" value="{{ $item->sorting ?? 0}}">
                    </div>


                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Обновить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/verifications/card-instructions') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
