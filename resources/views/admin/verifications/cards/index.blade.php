@extends('admin.layouts.app')

@section('title', __('Верификация карт'))

@section('top-block')

    <md-button ng-href="{{ admin_base_path('verifications/card-category') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-handshake text-primary"></md-icon>
        <span>{{ __('Категории') }}</span>
    </md-button>

    <md-button ng-href="{{ admin_base_path('verifications/card-instructions') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-text text-primary"></md-icon>
        <span>{{ __('Инструкция') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.tools.verification_card'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('ID Пользователя') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('IP Адрес') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'ip_address', is_filter_search($filter, 'ip_address'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Статус') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('status[]', $statuses, is_filter_search($filter, 'status'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Валюта') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('currencies[]', $currencies, is_filter_search($filter, 'currencies'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Номер счета') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'card_number', is_filter_search($filter, 'card_number'), ['class' => 'form-control']) }}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_card_number" @if(isset($filter['checkbox_card_number'])) checked @endif>
                                            <label class="form-check-label" for="checkbox1">{{ __('Точное совпадение счета') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Применить фильтры') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Верификация карт') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">


            <form method="post" action="?">
                <input type="hidden" name="actions" value="update">
                @csrf
                <table class="table table-border-2 iex-adaptive__table">
                    <thead>
                    <tr>
                        <th>
                            <div class="form-check">
                                <input class="form-check-input iex-select-all" id="iex-checkbox-all" type="checkbox" name="iex-select-all">
                                <label class="form-check-label" for="iex-checkbox-all"></label>
                            </div>
                        </th>
                        <th>{{ __('Дата создания') }}</th>
                        <th>{{ __('Пользователь') }}</th>
                        <th>{{ __('IP Адрес') }}</th>
                        <th>{{ __('Валюта') }}</th>
                        <th>{{ __('Номер счета') }}</th>
                        <th>{{ __('Фото') }}</th>
                        <th>{{ __('Статус') }}</th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($cards) > 0)
                        @foreach($cards as $card)
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input class="form-check-input iex-select-item" id="checkbox{{$card->id}}" value="{{$card->id}}" type="checkbox" name="check[]">
                                        <label class="form-check-label" for="checkbox{{$card->id}}"></label>
                                    </div>
                                </th>

                                <td data-label="{{ __('Дата создания') }}">
                                    {{  $card->created_at }}
                                    <div class="form-subtext-2-line">
                                        {{  \Illuminate\Support\Carbon::parse($card->created_at)->diffForHumans() }}
                                    </div>
                                </td>
                                <td data-label="{{ __('Пользователь') }}">
                                    <div>
                                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$card->id_user) }}"><b>{{$card->user->name}}</b></a>
                                    </div>
                                    <div class="form-subtext-2-line">{{  $card->user->email }}</div>
                                </td>
                                <td data-label="{{ __('IP Адрес') }}">
                                    @if(!is_null($card->ip_address))
                                        <a href="{{ admin_base_path('account/logs_auth/?ip_address='.$card->ip_address) }}">{{ $card->ip_address }}</a><br />
                                    @else
                                        <span class="text-muted">{{ __('admin-tools.card.undefined') }}</span>
                                    @endif
                                </td>
                                <td data-label="{{ __('Валюта') }}">
                                    {{$card->currency->payment->name}} {{ $card->currency->code_currency->name }}
                                </td>
                                <td data-label="{{ __('Номер счета') }}">
                                    {{(!empty($card->card_number_string) ? $card->card_number_string : $card->card_number)}}
                                    @if(!empty($card->name))
                                        {{ __('ФИО Держателя') }}: {{ $card->name }}
                                    @endif
                                </td>
                                <td data-label="{{ __('Фото') }}">
                                    @if((int)iEXSetting('is_module_storage_disk') == 1 and !empty(iEXSetting('storage_disk_verification_card')) and $card->is_local_image == 1)
                                        <a href="{{ iex_dynamic_read_view_image('verifications/'.$card->image, false, 'verifications_card') }}" target="_blank">{{ __('Открыть') }}</a>
                                    @else
                                        <a href="/images/verifications/{{  $card->image }}" target="_blank">{{ __('Открыть') }}</a>
                                    @endif
                                </td>
                                <td data-label="{{ __('Статус') }}">
                                    @if($card->status == 1)
                                        <label class="label label-success">{{ __('Верифицирован') }}</label>
                                    @elseif($card->status == 3)
                                        <label class="label label-danger">{{ __('Отклонена') }}</label>
                                    @else
                                        <select class="selectpicker" name="status[{{$card->id}}]">
                                            <option value="0" @if($card->status == 0) selected @endif>{{ __('На проверке') }}</option>
                                            <option value="1" @if($card->status == 1) selected @endif>{{ __('Верифицирован') }}</option>
                                            <option value="3" @if($card->status == 3) selected @endif>{{ __('Отклонить') }}</option>
                                        </select>
                                    @endif

                                        @if(!empty($card->text_message))
                                            <div class="input-p-text">{{ __('Комментарий') }}: {{ $card->text_message }}</div>
                                        @endif

                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                                <option value="delete">{{ __('Удалить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка верификации') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Отсылать Telegram уведомление при получении новых заявок на верификацию') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <div class="material-switch switch-input-1">
                                    <input id="telegram_order_verification_card" name="telegram_order_verification_card" type="checkbox" value="1" @if(iEXSetting('telegram_order_verification_card') == 1) checked @endif />
                                    <label for="telegram_order_verification_card" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Отсылать E-mail уведомление клиентам о статусе верификации') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <div class="material-switch switch-input-1">
                                    <input id="email_verification_card" name="email_verification_card" type="checkbox" value="1" @if(iEXSetting('email_verification_card') == 1) checked @endif />
                                    <label for="email_verification_card" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить автоматическую верификацию') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <div class="material-switch switch-input-1">
                                    <input id="enabled_auto_verification_card" name="enabled_auto_verification_card" type="checkbox" value="1" @if(iEXSetting('enabled_auto_verification_card') == 1) checked @endif />
                                    <label for="enabled_auto_verification_card" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Отклонить заявку если не пройдена верификация') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <div class="material-switch switch-input-1">
                                    <input id="reject_failed_verification_card" name="reject_failed_verification_card" type="checkbox" value="1" @if(iEXSetting('reject_failed_verification_card') == 1) checked @endif />
                                    <label for="reject_failed_verification_card" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Кол-во неудачных попыток верификации после которого клиенту будет ограничен доступ к направлении') }}</label>
                            <div class="col-md-6">
                                <input type="text" name="num_count_failed_verification" value="{{ iEXSetting('num_count_failed_verification', 0) }}" class="form-control" style="width: 200px;text-align: center">
                                <div class="input-p-text">0 - {{ __('отключает ограничение') }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Доп. кэшбэк для верифицированных пользователей (%)') }} (%)</label>
                            <div class="col-md-6">
                                <input type="text" name="other_cashback_user_verification" value="{{ iEXSetting('other_cashback_user_verification', 0) }}" class="form-control" style="width: 200px;text-align: center">
                                <div class="input-p-text">% {{ __('будет прибавлен к действующему cashback') }}</div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <div class="form-group">
                            <div class="col-md-12">
                                <x-forms.language.textarea label="{{ 'Информация на странице верификации' }}" tabName="description_verification_card" keyName="description_verification_card" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <x-forms.language.textarea label="{{ 'Описание (В случае ошибки)' }}" tabName="error_verification_card" keyName="error_verification_card" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {{ $cards->links() }}
    </div>
@endsection
