@if(flash()->message)
    <div class="{{ flash()->class }}">
        @if(flash()->class == 'alert alert-success')
            <div class="flash-alert-row" layout="row" layout-align="start center">
                <i class="fal fa-check-circle"></i>
                <div class="flash-alert-column">
                    <div class="flash-alert-title">{{ __('Успешно') }}</div>
                    <div class="flash-alert-content"> {{ flash()->message }}</div>
                </div>
            </div>
        @elseif(flash()->class == 'alert alert-warning')
            <div class="flash-alert-row" layout="row" layout-align="start center">
                <i class="fal fa-exclamation-triangle"></i>
                <div class="flash-alert-column">
                    <div class="flash-alert-title">{{ __('Предупреждение') }}</div>
                    <div class="flash-alert-content"> {{ flash()->message }}</div>
                </div>
            </div>
        @elseif(flash()->class == 'alert alert-danger')
            <div class="flash-alert-row" layout="row" layout-align="start center">
                <i class="fal fa-times-circle"></i>
                <div class="flash-alert-column">
                    <div class="flash-alert-title">{{ __('Ошибка') }}</div>
                    <div class="flash-alert-content"> {{ flash()->message }}</div>
                </div>
            </div>
        @else
            {{ flash()->message }}
        @endif
    </div>
@endif



<div class="clearfix"></div>

@include('admin._partials.security')
<div class="clearfix"></div>

@if ($__env->yieldContent('nav'))
    @yield('nav')
@endif
@if (!$__env->yieldContent('no-block-content'))
    <div class="x_panel @yield('x_panel_class')">
        <div class="x_title">
            @if (!$__env->yieldContent('custom-nav-title'))
                <h2>@yield('title')
                    @if($__env->yieldContent('title-right-button'))
                        <div class="heading-right-button">
                            @yield('title-right-button')
                        </div>
                    @endif
                </h2>
            @else
                <h2>@yield('custom-nav-title')</h2>
            @endif

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            @yield('content')
        </div>
    </div>

    <div class="pagination-left">
        @yield('pagination')
    </div>

    @yield('footer-notify')
@else
    @yield('no-block-content')

    <div class="pagination-left">
        @yield('pagination')
    </div>
@endif
