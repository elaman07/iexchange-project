<!DOCTYPE html>
<html lang="ru" ng-app="adminPanel" ng-csp="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ iEXSetting('sitename', 'iEXExchanger') }}  - @yield('title', __('admin-layout.title'))</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="/admin-assets/compilers/css/application.css?v={{config('crypto.full_version')}}">
    <link nonce="{{ csp_nonce() }}" href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap-grid.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="/admin-assets/admin/css/base.css">--}}
    {{--<link rel="stylesheet" href="/admin-assets/admin/css/custom.css">--}}

    <link rel="stylesheet" href="/assets/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/solid.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/brands.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/regular.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/light.min.css">
    <link rel="stylesheet" href="/assets/fontawesome/css/duotone.min.css">
    @include('layouts.headers.link_icon')
    @yield('custom-js-head')

    <style>

        .form-subtext-2-line {
            color: #555;font-weight:normal;font-size: 11px;
        }

        .parser-code__item {
            color: #707070;
            font-weight: normal;
            font-size: 13px;
        }

        .parser-code__item span {
            cursor: pointer;
            font-weight: 500;
        }

        /*.iex-custom__scrollbar::-webkit-scrollbar {*/
        /*    width: 3px;*/
        /*    background-color: #f3f4f8;*/
        /*}*/

        /*.iex-custom__scrollbar::-webkit-scrollbar-thumb {*/
        /*    border-radius: 10px;*/
        /*    background-color: #DFE1E5;*/
        /*}*/

        /*.iex-custom__scrollbar::-webkit-scrollbar-track {*/
        /*    border-radius: 10px;*/
        /*    background-color: #f3f4f8;*/
        /*}*/

        /*.iex--scroll-height-100 {*/
        /*    height: 100px;*/
        /*    overflow-y: auto;*/
        /*}*/


        .mt-7 {
            margin-top: 7px;
        }
        .btb-style-outline-null {
            padding: 8px 15px;
            outline: none !important;
        }
        .form-check-ckeditor-input {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .content {
            min-height: 100vh;
        }

        .currency-aml-enabled {
            background: #e2ffe2;
            width: 100%;
            display: block;
            padding: 10px;
            border-radius: 5px;
            color: #067806;
            font-size: 12px;
        }

        .multi-language-form-group-input {
            position: relative;
        }

        .multi-language-form-group-input .nav {
            position: absolute;
            right: 20px;
            top: 21px;
        }

        .multi-language-form-group-input .lang-tabs .lang-tabs-link {
            padding: 3px 6px;
        }
    </style>
    <style>

        .padding-tb-5 {
            padding: 5px 0px;
        }

        .label-small {
            padding: 4px 10px;
            font-size: 11px;
        }

        .form-block__end {
            margin-block-end: 0;
        }

        .text-decoration-underline-hover:hover {
            text-decoration: underline;
        }

        .label {
            border-radius: 30px;
        }

        .link-danger-unactive {
            color: #F44336;
        }
        .link-danger-unactive:hover {
            color: #720f07;
        }


    </style>
    <style>
        .iex-content__bootstrap5__is {
            margin-top: 10px;
            font-size: 11px;
            color: #999;
        }

        .iex-content__bootstrap5-table {
            padding: 20px 10px;
            position: relative;
            border: 1px solid #e9e9e9;
            margin: 8px 10px !important;
            border-radius: 10px;
        }

        .iex-content__bootstrap5-table:last-child {
            border-bottom: none;
        }

        .iex-content__bootstrap5-table__title {
            font-size: 14px;
            font-weight: 600;
            margin: 0;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .iex-content__bootstrap5-table__wrap {
            margin-bottom: 10px;
        }

        .iex-content__bootstrap5-table__wrap:last-child {
            margin-bottom: 0;
        }
        .iex-content__bootstrap5-start-center {
            height: 100%;
            flex-direction: column;
            box-sizing: border-box;
            display: flex;
            place-content: flex-start center;
            align-items: flex-start;
        }

        .iex-content__bootstrap5-table__header .iex-content__bootstrap5-table__ft {
            font-size: 15px;
        }

        .iex-content__bootstrap5-table__header .iex-content__bootstrap5-table__li {
            padding: 2px 0;
        }

        .iex-content__bootstrap5-table__link {
            margin-bottom: 10px;
        }

        .n-mt-5 {
            margin-top: 5px;
        }

        @media (min-width: 100px) and (max-width: 599px)
        {
            .iex-content__bootstrap5-table__header {
                padding-bottom: 30px;
            }

            .content {
                padding: 0 1px 60px 1px;
            }

            .page-header-content {
                padding: 0
            }
        }

    </style>
    <style>
        .height-100 {
            height: 100%;
        }

        .layout-base-body {
            min-height: 100%;
            flex-direction: column;
            box-sizing: border-box;
            display: flex;
            place-content: stretch space-between;
            align-items: stretch;
            max-width: 100%;
        }

        .iex-copyright {
            padding: 20px;
            color: #777777;
        }
    </style>
</head>

<body class="nav-md @yield('custom-body-class')" ng-cloak layout="column"> <!-- ng-cloak -->
@if (!$__env->yieldContent('no_background'))
        <section ng-controller="BaseController" class="main_container" layout="row" flex>
            <md-sidenav class="left-sidenav md-sidenav-left" md-component-id="left" md-is-locked-open="$mdMedia('gt-md')">
                @widget('Main\AdminSidenavWidget')
            </md-sidenav>
            <md-content layout="column" flex id="content" flex class="body-background" @yield('ng-controller')>


                <div  class="height-100">
                    <div class="layout-base-body">
                        <div>
                            <x-admin-toolbar></x-admin-toolbar>

                            @yield('add-to-toolbar')

                            {{--@include('admin.layouts.top')--}}

                            <div class="clearfix"></div>


                            @if (!$__env->yieldContent('custom-md-content'))
                                <div id="page-header">
                                    <div id="header-nav-left"></div>
                                    <div id="header-nav-right">
                                        {{--<div class="nav toggle">--}}
                                        {{--<a id="menu_toggle"><i class="fa fa-bars"></i></a>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <x-admin-page-header></x-admin-page-header>
                                <div class="clearfix"></div>

                                @if (!$__env->yieldContent('disable-body-wrapper'))
                                    <div class="iex-body-panel-wrapper">
                                        <div class="content">
                                            <div>
                                                @include('admin.layouts.content')
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @include('admin.layouts.content')
                                @endif
                            @else
                                @yield('custom-md-content')
                            @endif
                        </div>

                        <div class="iex-copyright" layout="row">
                            <div>
                                <a target="_blank" href="https://exchanger.iexbase.com">iEXExchanger v.{{ config('version.current') }}</a>. © iEXExchanger FZCO, 2019-{{ date('Y') }} All rights reserved
                            </div>
                            <div flex></div>
                            <a target="_blank" href="https://iexexchanger.gitbook.io/iexexchanger/">Documentation</a>
                        </div>

                    </div>
                </div>
            </md-content>
        </section>
@else
    <div style="background-color: #fff">
        @yield('no_background')
    </div>
@endif


<!-- Модальное окно для настроек -->
<div class="modal fade" id="style_switcher_toggle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xs" role="document" style="width: 450px">
        <form class="form-horizontal" method="post" action="?">
            <input type="hidden" name="action" value="settings_logs">
            @csrf
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="ui-dialog-title" id="myModalLabel">@lang('admin.select_theme_title')</span>
                </div>
                <div class="modal-body" id="style_switcher">
                    <h5>@lang('admin.colors')</h5>
                    <ul class="switcher_app_themes" id="theme_switcher">
                        <li class="app_style_default" data-app-theme="">
                            <span class="app_color_main"></span>
                            <span class="app_color_accent"></span>
                        </li>
                        <li class="switcher_theme_dark" data-app-theme="iex-black-theme">
                            <span class="app_color_main"></span>
                            <span class="app_color_accent"></span>
                        </li>
                    </ul>

                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal fade youtube-video" id="modalVideoScript" tabindex="-1" role="dialog" aria-labelledby="modalVideoScriptLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button id="close-video" type="button" class="button btn btn-default text-right" data-dismiss="modal">
                <i class="far fa-times"></i>
            </button>
            <div class="modal-body">
                <div id="video-container" class="video-container">
                    <iframe id="youtubevideo" src="" width="640" height="360" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<script src="/admin-assets/admin/components/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/admin-assets/compilers/js/js-version.js?v={{ config('crypto.full_version') }}"></script>
<script type="text/javascript" src="/admin-assets/compilers/js/ng-version.js?v={{config('crypto.full_version')}}"></script>

<script src="/admin-assets/admin/js/angular.js?v={{ config('crypto.full_version') }}"></script>



@yield('js_bottom')

<script type="text/javascript">

    var iEXGeneratorPassword = {
        _pattern : /[a-zA-Z0-9]/,
        _getRandomByte : function()
        {
            // http://caniuse.com/#feat=getrandomvalues
            if(window.crypto && window.crypto.getRandomValues)
            {
                var result = new Uint8Array(1);
                window.crypto.getRandomValues(result);
                return result[0];
            }
            else if(window.msCrypto && window.msCrypto.getRandomValues)
            {
                var result = new Uint8Array(1);
                window.msCrypto.getRandomValues(result);
                return result[0];
            }
            else
            {
                return Math.floor(Math.random() * 256);
            }
        },

        generate : function(length)
        {
            return Array.apply(null, {'length': length})
                .map(function()
                {
                    var result;
                    while(true)
                    {
                        result = String.fromCharCode(this._getRandomByte());
                        if(this._pattern.test(result))
                        {
                            return result;
                        }
                    }
                }, this)
                .join('');
        }

    };

    function insertTextareaBBCode(a, key, id) {
        var ta = document.getElementById(id);
        ta.value += key;
        return false;
    }

    function insertTextareaCkeditorBBCode(a, key, id) {
        CKEDITOR.instances.ckeditor.insertHtml(key)
        return false;
    }

    //YOUTUBE VIDEO
    $('.play-button__video').click(function(e){
        var iframeEl = $('<iframe>', { src: $(this).data('url') });
        $('#youtubevideo').attr('src', $(this).data('url'));
    })

    $('#close-video').click(function(e){
        $('#youtubevideo').attr('src', '');
    });

    $(document).on('hidden.bs.modal','#myModal', function () {
        $('#youtubevideo').attr('src', '');
    });
</script>

<script>
    $(document).ready(function () {
        $("#admin-menu-filter").keyup(function () {
            var filter = $(this).val(),
                count = 0;
            $("#search-side-menu li").each(function () {

                $("#search-side-menu > li").addClass('active');
                $("#search-side-menu .single_child_menu").addClass('active');
                $("#search-side-menu li .child_menu").css('display', 'block');

                if (filter == "") {
                    $("#search-side-menu > li").removeClass('active');
                    $("#search-side-menu .single_child_menu").removeClass('active');
                    $("#search-side-menu li .child_menu").css('display', 'none');

                    $(this).css("visibility", "visible");
                    $(this).fadeIn();
                } else if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).css("visibility", "hidden");
                    $(this).fadeOut();
                } else {
                    $(this).css("visibility", "visible");
                    $(this).fadeIn();
                }
            });
        });
    });

    $('#cp4').colorpicker({
        customClass: 'colorpicker-2x',
        sliders: {
            saturation: {
                maxLeft: 200,
                maxTop: 200
            },
            hue: {
                maxTop: 200
            },
            alpha: {
                maxTop: 200
            }
        }
    });
</script>

<script>
    $(function(){
        //button select all or cancel
        $("#iex-select-all").click(function () {
            var all = $("input.iex-select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.iex-select-item").each(function (index,item) {
                item.checked = checked;
            });
        });
        //column checkbox select all or cancel
        $("input.iex-select-all").click(function () {
            var checked = this.checked;
            $("input.iex-select-item").each(function (index,item) {
                item.checked = checked;
            });
        });
        //check selected items
        $("input.iex-select-item").click(function () {
            var checked = this.checked;
            var all = $("input.iex-select-all")[0];
            var total = $("input.iex-select-item").length;
            var len = $("input.iex-select-item:checked:checked").length;
            all.checked = len===total;
        });

    });
</script>
</body>
