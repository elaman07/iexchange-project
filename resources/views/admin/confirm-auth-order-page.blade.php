@extends('layouts.app')

@section('title', __('admin.backup_codes.title'))

@section('admin-page')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css?v=1">
@endsection

@section('content-base')
    <div class="container">
        <div class="col-md-5 col-md-offset-3">
            <div class="page-container" style="min-height:385px">
                <!--MAIN area-->
                <div class="panel panel-default" style="margin-top: 100px;">

                    <div class="panel-heading">
                        {{ 'Введите код для авторизации' }}
                    </div>

                    <div class="panel-body">

                        @if (flash()->message)
                            <div class="{{ flash()->class }}">
                                {{ flash()->message }}
                            </div>
                        @endif

                        <form class="separate-sections" role="form" method="POST" action="/confirmAuthOrderPage">
                            @csrf
                            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                <input id="code" type="text" class="form-control" placeholder="Введите код" name="code" autofocus required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-raised btn-block legitRipple">{{ __('admin.backup_codes.save') }} <i class="fa fa-sign-in"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-muted text-size-small text-center">Copyright {{ date('Y') }}<br></div>
                <!--MAIN area-->
            </div>
        </div>
    </div>
@endsection
