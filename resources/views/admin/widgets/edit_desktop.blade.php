@extends('admin.layouts.app')

@section('title', $item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.widgets.edit_desktop', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/widgets/update_desktop/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="code" class="col-sm-2 control-label">Название рабочего стола</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control" id="name" value="{{$item->name}}" placeholder="{{ __('admin-basic.filter_currency.name_hint') }}" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="code" class="col-sm-2 control-label">Столбцов</label>
                <div class="col-md-5">
                    <select name="columns" class="form-control selectpicker">
                        <option value="2" @if($item->columns == 2) selected @endif>2</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/widgets/config_desktops') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection