@extends('admin.layouts.app')

@section('title', $item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.widgets.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/widgets/'.$item->hash_id.'/edit') }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="code" class="col-sm-2 control-label">Название гаджета</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control" id="name" value="{{$item->name}}" placeholder="{{ __('admin-basic.filter_currency.name_hint') }}" required>
                </div>
            </div>


            @if(isset($forms['inputs']) and count($forms['inputs']) > 0)
                <hr />
                @foreach($forms['inputs'] as $value)
                    <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                        <label for="code" class="col-sm-2 control-label">{{ $value['name'] }}</label>
                        <div class="col-sm-5">
                            @if($value['type'] == 'select')
                                <select class="selectpicker" name="{{ $value['key'] }}">
                                    @foreach($value['list'] as $select)
                                        <option value="{{ $select['key'] }}" @if(iEXSetting($value['key']) == $select['key']) selected @endif>{{ $select['name'] }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/?page_id='.$item->id_desktop) }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection