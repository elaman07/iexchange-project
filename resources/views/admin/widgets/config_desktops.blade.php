@extends('admin.layouts.app')

@section('title', 'Управление рабочими столами')

@section('breadcrumbs', Breadcrumbs::render('admin.widgets.config_desktop'))

@section('custom-js-head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        #sortable {
            list-style-type: none;
            width: 50%;
            margin: 0 auto;
            padding: 25px 0 30px;
        }
        #sortable li {
            margin: 0 3px 3px 3px;
            padding: 0.4em 0.4em 0.4em 1.5em;
            font-size: 1.4em;
            height: 4rem;
            border: 1px dashed #dadada;
        }

        .ui-state-highlight {  height: 4rem; line-height: 1.2em; }
    </style>
@endsection


@section('content')
    <ul id="sortable">
        @foreach($desktops as $key => $value)
            <li class="ui-state-default" id="item-{{$value->id}}" data-position="{{$key}}" layout="row">
                <span>{{ $value->name }}</span>
                <span flex></span>
                <span>
                    <a href="{{ admin_base_path('/widgets/edit_desktop/'.$value->id) }}">
                        <i class="fa fa-fw fa-cog"></i>
                    </a>
                     <a class="text-danger" href="{{ admin_base_path('/widgets/destroy_desktop/'.$value->id) }}">
                        <i class="fa fa-fw fa-trash"></i>
                    </a>
                </span>
            </li>
        @endforeach
    </ul>
@endsection

@section('js_bottom')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#sortable" ).sortable();
            $( "#sortable" ).disableSelection();
        } );
    </script>

    <script>
        $( function()
        {
            var ul_sortable = $('#sortable');
            ul_sortable.sortable({
                cursor: 'move',
                placeholder: "ui-state-highlight",
                update: function()
                {
                    var sortable_data = ul_sortable.sortable('serialize');
                    $.ajax({
                        data: sortable_data,
                        type: 'POST',
                        url: '{{ admin_base_path('/widgets/desktop/sorting') }}',
                        success: function (result) {
                            console.log(result);
                        }
                    });
                }
            });
            ul_sortable.disableSelection();
        } );
    </script>
@endsection