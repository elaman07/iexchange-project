@extends('admin.layouts.app')

@section('title', __('admin-account.settings.title'))

@section('content')
    <form action="?" method="POST" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="sitename" class="col-sm-2 control-label">{{ __('admin-account.settings.type_balance') }}</label>
            <div class="col-sm-5">
                <select class="form-control selectpicker" multiple data-actions-box="true" data-selected-text-format="count" data-live-search="true" name="type_balance[]">
                    @foreach(\App\Models\CodeCurrency::all() as $value)
                        <option value="{{ $value->id  }}"  @if($value->balance == 1) selected @endif>{{$value->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn btn-success">{{ __('admin-account.settings.save') }}</button>
        </div>
    </form>
@endsection
