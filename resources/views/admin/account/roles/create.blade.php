@extends('admin.layouts.app')

@section('title', __('admin-account.roles.title_add'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.roles.add'))


@section('content')
    {{ Form::open(array('url' => admin_base_path('/account/roles'),'id' => 'create', 'class' => 'form-horizontal')) }}
    <div class="default-panel-body">


        <div class="form-group">
            <label class="col-sm-2 control-label">{{ __('admin-account.roles.name') }}</label>
            <div class="col-sm-10">
                {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label">{{ __('admin-account.roles.permission_list') }}</label>
            <div class="col-sm-10">
                @foreach ($permissions as $permission)
                    <div class="form-check" style="margin-top: 5px;">
                        {{ Form::checkbox('permissions[]',  $permission->id, null ,['id' => str_slug($permission->name), 'class' => 'form-check-input']) }}
                        {{ Form::label(str_slug($permission->name), ucfirst($permission->title),['class' => 'form-check-label', 'for' => 'checkbox1']) }}
                        <br>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    <div class="default-panel-footer default-panel-footer-right">
        <md-button class="md-raised md-primary" name="save" type="submit">
            <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp; {{ __('admin-account.roles.add_button') }}</md-button>
        <md-button ng-href="{{ admin_base_path('/account/roles') }}">{{ __('admin-account.roles.back') }}</md-button>
    </div>
    {{ Form::close() }}


@endsection
