@extends('admin.layouts.app')

@section('title', __('admin-account.roles.title_edit'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.roles.change',$role->id))

@section('custom-js-head')
    <style>
        md-tabs [role="tabpanel"] {transition: none;}
        md-tabs md-ink-bar {transition: none;}
    </style>
@endsection

@section('no-block-content')


    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT','id' => 'update')) }}
    <div class="custom-card">

        <div class="x_panel">
            <div class="x_content">
                <md-tabs class="custom-md-tabs" md-dynamic-height md-border-bottom md-autoselect md-swipe-content>
                    <md-tab label="Общие настройки">
                        <md-content>
                            <table class="table table-2 table-striped">
                                <tr>
                                    <td class="col-xs-6 col-sm-6 col-md-7">
                                        <h6 class="media-heading text-semibold">{{ __('admin-account.roles.name') }}</h6>
                                        <span class="text-muted text-size-small hidden-xs">{{ __('admin-account.roles.name_hint') }}</span>
                                    </td>
                                    <td class="col-xs-6 col-sm-6 col-md-5">
                                        <input type="text" class="form-control" name="name" value="{{$role->name}}">
                                    </td>
                                </tr>

                                <tr>
                                    <td class="col-xs-6 col-sm-6 col-md-7">
                                        <h6 class="media-heading text-semibold">{{ __('admin-account.roles.allow_export') }}</h6>
                                        <span class="text-muted text-size-small hidden-xs">{{ __('admin-account.roles.allow_export_hint') }}</span>
                                    </td>
                                    <td class="col-xs-6 col-sm-6 col-md-5">
                                        <div class="material-switch">
                                            {{ Form::checkbox('is_export',  1, $role->is_export ,['id' => 'is_export'])}}
                                            <label for="is_export" class="label-success"></label>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </md-content>
                    </md-tab>

                    @foreach($permissions as $group)
                        <md-tab label="{{$group->name}}">
                            <md-content>
                                <table class="table table-2 table-striped">
                                    @foreach($group->permissions as $permission)
                                        <tr>
                                            <td class="col-xs-6 col-sm-6 col-md-7">
                                                <h6 class="media-heading text-semibold">{{$permission->title}}</h6>
                                                <span class="text-muted text-size-small hidden-xs">{{$permission->text}}</span>
                                            </td>
                                            <td class="col-xs-6 col-sm-6 col-md-5">
                                                <div class="material-switch">
                                                    {{ Form::checkbox('permissions[]',  $permission->id, $role->permissions ,['id' => str_slug($permission->name)])}}
                                                    <label for="{{str_slug($permission->name)}}" class="label-success"></label>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>

                            </md-content>
                        </md-tab>
                    @endforeach
                </md-tabs>
            </div>
        </div>

        <div class="form-bottom">
            <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.roles.edit_button') }}</md-button>
            <md-button ng-href="{{config('admin.directory')}}/account/roles" class="md-raised">{{ __('admin-account.roles.back') }}</md-button>
        </div>
    </div>

    {{ Form::close() }}
@endsection