@extends('admin.layouts.app')

@section('title', __('Группы пользователей'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.roles'))

@section('content')

    <div class="table-responsive" ng-controller="RolesController as ctrl">
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Название группы') }}</th>
                <th>{{ __('Пользователей') }}</th>
                <th class="not-sortable"></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($roles as $role)
                <tr>
                    <td>
                        <a href="{{ admin_base_path('/account/roles/'.$role->id.'/edit') }}" ><b>{{ $role->name }}</b></a>
                        <p>
                            @if($role->permissions()->where('name', 'allow_admin')->count() > 0)
                                <span class="text-danger">{{ __('имеет доступ к админ панели') }}</span>
                            @endif
                        </p>
                    </td>
                    <td  style="width: 100px">
                        @if($role->users()->count() > 0)
                            <ul>
                                @foreach($role->users()->get() as $user)
                                    <li>
                                        <a href="{{ admin_base_path('/account/users?id='.$user->id) }}">
                                            {{ $user->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <span class="text-danger">{{ __('Никому не выдано') }}</span>
                        @endif
{{--                            <span class="label label-primary">--}}
{{--                                {!! $role->permissions()->pluck('title')->implode("</span>, <span class=\"label label-primary\">") !!}--}}
{{--                            </span>--}}
                    </td>
                    <td style="width: 100px">
                        @if($role->users()->count() > 0)
                            <md-button class="float-right md-icon-button" disabled>
                                <i class="fas fa-lock text-warning"></i>
                            </md-button>
                        @else
                            <md-button title="{{ __('admin-account.roles.delete') }}" href="{{ admin_base_path('/account/roles/'.$role->id.'/delete') }}"
                                       class="float-right md-warn md-icon-button" >
                                <i class="fa fa-trash"></i>
                            </md-button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-left">
                <md-button ng-href="roles/create" class="md-primary md-raised">
                    <md-icon md-font-icon="fa fa-plus-circle text-primary"></md-icon>
                    &nbsp;
                    {{ __('Добавить роль') }}
                </md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

@endsection
