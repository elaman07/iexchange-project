@extends('admin.layouts.app')

@section('title', __('admin-account.internal_account.title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.internal_account', $user))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-account.internal_account.code_currency') }}</th>
            <th>{{ __('admin-account.internal_account.balance') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(count($codes) > 0)
            @foreach($codes as $code)
                <tr>
                    <th>{{ $code->name }}</th>
                    <td>{{ (isset($code->internal_account) ? $code->internal_account->balance : 0) }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4"><p align="center"><br />{{ __('admin-account.list_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection