@extends('admin.layouts.app')

@section('title', __('admin-account.referral.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.referral'))

@section('content')
    <form action="?" method="post">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-account.referral.exchange_at') }}</th>
                <th>{{ __('admin-account.referral.email') }}</th>
                <th>{{ __('admin-account.referral.exchange') }}</th>
                <th>{{ __('admin-account.referral.last_exchange') }}</th>
                <th>{{ __('admin-account.referral.action') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        {{$item->user->updated_at->format('d.m.Y H:i')}}
                    </td>
                    <td>
                        <a href="{{config('admin.directory')}}/account/users?action=filter&email={{$item->user['email']}}&sorting=id">
                            {{$item->user['email']}}
                        </a>
                    </td>
                    <td>
                        {{ $item->user->order_num }}
                    </td>
                    <td>
                        @if($item->user->last_activity_at != null)
                            {{ \Illuminate\Support\Carbon::parse($item->user->last_activity_at)->diffForHumans() }}
                        @else
                            <span class="text-muted">{{ __('admin-account.referral.undefined') }}</span>
                        @endif
                    </td>
                    <td style="width: 200px">
                        <select class="selectpicker" name="redirects[{{$item->id}}]">
                            <option value="0">-- </option>
                            @foreach($follow_referrals as $follow_referral)
                                <option value="{{$follow_referral->id}}">{{ __('admin-account.referral.throw_on', ['name' => $follow_referral->name]) }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="0">{{ __('admin-account.referral.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-account.referral.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>

        <div class="pagination-right">
            {!! $items->links() !!}
        </div>
    </div>

    <form action="?" method="post">
        @csrf
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('admin-account.referral.add_referral') }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputPassword1">{{ __('admin-account.referral.email') }}</label>
                            <input type="email" class="form-control" name="email" id="exampleInputPassword1" placeholder="{{ __('admin-account.referral.placeholder') }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin-account.referral.close') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('admin-account.referral.save') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('pagination-right')
    <div class="pagination-right">
        {!! $items->links() !!}
    </div>
@endsection