@extends('admin.layouts.app')

@section('title', __('admin-account.banned_histories.title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.banned_history', $user))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-account.banned_histories.id') }}</th>
            <th>{{ __('admin-account.banned_histories.start_ban') }}</th>
            <th>{{ __('admin-account.banned_histories.end_ban') }}</th>
            <th>{{ __('admin-account.banned_histories.comment') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($histories) > 0)
            @foreach($histories as $history)
                <tr>
                    <th>{{$history->id}}</th>
                    <td>
                        {{ $history->created_at }}
                    </td>
                    <td>
                        {{ $history->expired_at }}
                    </td>
                    <td>
                        {{ $history->comment }}
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-account.list_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $histories->links() !!}
    </div>
@endsection