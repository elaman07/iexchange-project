@extends('admin.layouts.app')

@section('title', __('admin-account.ban.title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.ban', $user))

@section('content')
    <form action="?" method="post" class="form-horizontal">
        @csrf
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('admin-account.ban.expired_at') }}</label>
                <div class="col-sm-3">
                    <input  value="" name="expired_at" type="text" class="form-control datetimepicker">
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('admin-account.ban.comment') }}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="comment" placeholder="{{ __('admin-account.ban.comment_hint') }}"></textarea>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.ban.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/account/users') }}" class="md-raised">{{ __('admin-account.ban.cancel') }}</md-button>
        </div>
    </form>
@endsection