@extends('admin.layouts.app')

@section('title', __('admin-account.balance.title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.balance', $user))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <form action="?" method="post" class="form-horizontal">
                @csrf
                <div class="default-panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ __('admin-account.balance.cashback') }}</label>
                        <div class="col-sm-3">
                            <input value="{{ $user->user_balance->balance_reward }}" name="cashback" type="text" class="form-control" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{ __('admin-account.balance.referral') }}</label>
                        <div class="col-sm-3">
                            <input value="{{ $user->user_balance->balance }}" name="referral" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="default-panel-footer default-panel-footer-right">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.balance.save') }}</md-button>
                    <md-button ng-href="{{ admin_base_path('/account/users') }}" class="md-raised">{{ __('admin-account.balance.cancel') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-account.balance.history_balance', ['count' => $logs->total()]) }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-account.balance.created_at') }}</th>
                    <th>{{ __('admin-account.balance.manager') }}</th>
                    <th>{{ __('admin-account.balance.type_balance') }}</th>
                    <th>{{ __('admin-account.balance.actions') }}</th>
                    <th>{{ __('admin-account.balance.before') }}</th>
                    <th>{{ __('admin-account.balance.after') }}</th>
                    <th>{{ __('admin-account.balance.text') }}</th>
                </tr>
                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $log)
                        <tr>
                            <th>{{ $log->created_at }}</th>
                            <td>
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$log->id_manager.'&sorting=id') }}"><b>{{$log->manager->name}}</b></a>
                                </div>
                                <small>{{$log->manager->email}}</small>
                            </td>
                            <td>
                                @if($log->route_type == 0)
                                    {{ __('admin-account.balance.cashback_label') }}
                                @else
                                    {{ __('admin-account.balance.referral_bonus') }}
                                @endif
                            </td>
                            <td>
                                @if($log->route_type == 0)
                                    <div class="label label-success">{{ __('admin-account.balance.deposit') }}</div>
                                @else
                                    <div class="label label-danger">{{ __('admin-account.balance.withdraw') }}</div>
                                @endif
                            </td>
                            <td>{{ $log->from_balance }}</td>
                            <td>{{ $log->to_balance }}</td>
                            <td style="width: 350px">{{ $log->text }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10"><p align="center"><br />{{ __('admin-account.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection