@extends('admin.layouts.app')

@section('title', __('admin-account.google.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.google', $user->id))

@section('content')

    <div style="padding: 30px;text-align: center">
        <p>{{ __('admin-account.google.description') }}</p>
        <b style="font-size: 20px;"> {{ $secret }}</b>
        <hr />
        <div>
            <img src="{{ $QR_Image }}">
        </div>
    </div>
@endsection