@extends('admin.layouts.app')

@section('title', __('admin-account.control.edit_title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.change', $user))


@section('custom-js-head')
    <style>.content{ padding: 0;}</style>
@endsection
@section('no-block-content')

    <div class="row-blog">
        <div class="col-md-8">
            <div class="x_panel">
                <div class="x_title">
                    <h2>@yield('title')</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}

                    <div class="default-panel-body">

                        <div class="form-group">
                            {{ Form::label('email', __('admin-account.control.email'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::email('email', null, array('class' => 'form-control')) }}
                                @if(is_null($user->email_verified_at))
                                    <div style="margin-top: 3px; font-size: 12px;" class="text-danger">
                                        {{ __('admin-account.control.not_verified') }}
                                        (<a href="?verified">{{ __('admin-account.control.verified') }}</a>)
                                    </div>
                                @endif
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            {{ Form::label('password', __('admin-account.control.new_password'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" name="password" class="form-control" id="generator-password">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btb-style-outline-null" type="button" onclick='document.getElementById("generator-password").value = iEXGeneratorPassword.generate(16)'>
                                         <i class="fas fa-sync-alt"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <hr />

                        @can('admin_roles')
                            <div class='form-group'>
                                {{ Form::label('email', __('admin-account.control.group_label'), ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('roles[]', $roles->pluck('name', 'id'), $user->roles,
                                    ['multiple', 'class' => 'selectpicker', 'data-width'=>'300', 'data-live-search' => 'true']) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('password', __('admin-account.control.group_until'), ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    <input value="{{$user->role_expired_at}}" name="role_expired_at" type="text" class="form-control datetimepicker width-300">
                                </div>
                            </div>
                        @endcan

                        <hr />


                        <div class="form-group">
                            {{ Form::label('name', __('admin-account.control.name'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name', null, array('class' => 'form-control width-300')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('phone', __('admin-account.control.phone'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('phone', null, array('class' => 'form-control width-300')) }}
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('deactivation', __('admin-account.control.status_account'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="deactivation" data-width="300">
                                    <option value="0" @if($user->deactivation == 0) selected @endif>{{ __('admin-account.control.enable') }}</option>
                                    <option value="1" @if($user->deactivation == 1) selected @endif>{{ __('admin-account.control.disable') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">{{ __('Статус личности') }}</label>
                            <div class="col-sm-5">
                                <select class="selectpicker" name="is_verify_account">
                                    <option value="0" @if($user->is_verify_account == 0) selected @endif>{{ __('Не верифицирован') }}</option>
                                    <option value="1" @if($user->is_verify_account == 1) selected @endif>{{ __('Верифицирован') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            {{ Form::label('personal_discount', 'Персональная скидка', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('personal_discount', null, array('class' => 'form-control width-300')) }}
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">{{ __('admin-account.control.notify_email_auth') }}</label>
                            <div class="col-sm-5">
                                <select class="selectpicker" name="is_notify_email">
                                    <option value="0" @if($user->is_notify_email == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_notify_email == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">{{ __('admin-account.control.enable_reset_password') }}</label>
                            <div class="col-sm-5">
                                <select class="selectpicker" name="is_password_reset">
                                    <option value="0" @if($user->is_password_reset == 0) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                    <option value="1" @if($user->is_password_reset == 1) selected @endif>{{ __('admin-account.control.no') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            {{ Form::label('personal_ref_discount', 'Персональная партнерский процент', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('personal_ref_discount', $user->personal_ref_discount ?? 0, array('class' => 'form-control width-300')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('max_ref_discount', 'Макс. партнерский процент', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('max_ref_discount', $user->max_ref_discount ?? 0, array('class' => 'form-control width-300')) }}
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">{{ __('admin-account.control.referral_program') }}</label>
                            <div class="col-sm-5" >
                                <select class="selectpicker" name="referral">
                                    @foreach($programs as $program)
                                        <option value="{{$program->id}}" @if($link->referral_program_id == $program->id) selected @endif>
                                            {{$program->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('is_follow_referral', __('admin-account.control.favorite_referral'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-5">
                                <select class="selectpicker" name="is_follow_referral">
                                    <option value="0" @if($user->is_follow_referral == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_follow_referral == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Разрешить отправить отчет по реферальным данным</label>
                            <div class="col-sm-5">
                                <select class="selectpicker" name="is_report_referral_data">
                                    <option value="0" @if($user->is_report_referral_data == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_report_referral_data == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">{{ __('admin-account.control.limit_create_order') }}</label>
                            <div class="col-sm-5">
                                <select class="selectpicker" name="restriction">
                                    <option value="0" @if($user->restriction == 0) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                    <option value="1" @if($user->restriction == 1) selected @endif>{{ __('admin-account.control.no') }}</option>
                                </select>
                            </div>
                        </div>


                        <div class='form-group'>
                            {{ Form::label('is_unique_user', __('admin-account.control.unique_payment_system_payout'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-5">
                                <select class="selectpicker" name="is_unique_user">
                                    <option value="0" @if($user->is_unique_user == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_unique_user == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class='form-group'>
                            {{ Form::label('is_horizon', __('admin-account.control.allow_horizon'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_horizon">
                                    <option value="0" @if($user->is_horizon == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_horizon == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('is_backup', __('admin-account.control.allow_backup'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_backup">
                                    <option value="0" @if($user->is_backup == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_backup == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('is_order', __('admin-account.control.allow_create_order'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_order">
                                    <option value="0" @if($user->is_order == 0) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                    <option value="1" @if($user->is_order == 1) selected @endif>{{ __('admin-account.control.no') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('is_order', __('Скрыть IP адрес?'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_hidden_ip_address">
                                    <option value="0" @if($user->is_hidden_ip_address == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1" @if($user->is_hidden_ip_address == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class='form-group'>
                            {{ Form::label('is_pay_referral', __('admin-account.control.pay_referral_bonus'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_pay_referral">
                                    <option value="0" @if($user->is_pay_referral == 0) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                    <option value="1" @if($user->is_pay_referral == 1) selected @endif>{{ __('admin-account.control.no') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('is_pay_cashback', __('admin-account.control.pay_cashback'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_pay_cashback">
                                    <option value="0" @if($user->is_pay_cashback == 0) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                    <option value="1" @if($user->is_pay_cashback == 1) selected @endif>{{ __('admin-account.control.no') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class='form-group'>
                            {{ Form::label('is_verification', __('admin-account.control.active_verification_shot'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_verification" id="is_verification">
                                    <option value="0" @if($user->is_verification == 0) selected @endif>{{ __('admin-account.control.disable') }}</option>
                                    <option value="1" @if($user->is_verification == 1) selected @endif>{{ __('admin-account.control.enable') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class='form-group'>
                            {{ Form::label('is_vip_client', __('admin-account.control.vip_client'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_vip_client" id="is_vip_client">
                                    <option value="0" @if($user->is_vip_client == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_vip_client == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            {{ Form::label('auth', __('admin-account.control.google_auth'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                @if(config('admin.is_demo_mode') == true)
                                    <small class="text-danger">{{ __('admin-account.control.not_available_demo') }}</small>
                                @else
                                    <a href="google/">{{ __('admin-account.control.get_new_key') }}</a>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('backup_code', __('admin-account.control.reserve_code'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                @if(config('admin.is_demo_mode') == true)
                                    <small class="text-danger">{{ __('admin-account.control.not_available_demo') }}</small>
                                @else
                                    @if($checkRecovery)
                                        <span class="text-danger">{{ __('admin-account.control.code_activated') }}</span>
                                    @else
                                        <a href="{{ admin_base_path('/account/users/'.$user->id.'/backup_codes/?created=true') }}">{{ __('admin-account.control.get_code') }}</a>
                                    @endif
                                @endif
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            {{ Form::label('admin_phone', __('admin-account.control.phone_admin'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('admin_phone', null, array('class' => 'form-control width-300')) }}
                            </div>
                        </div>


                        <hr />

                        <div class='form-group'>
                            {{ Form::label('is_enabled_restapi', 'Разрешить доступ к REST API', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                <select class="selectpicker" name="is_enabled_restapi" id="is_enabled_restapi">
                                    <option value="0" @if($user->is_enabled_restapi == 0) selected @endif>{{ __('admin-account.control.no') }}</option>
                                    <option value="1" @if($user->is_enabled_restapi == 1) selected @endif>{{ __('admin-account.control.yes') }}</option>
                                </select>
                            </div>
                        </div>


                        <hr />

                        <div class='form-group'>
                            {{ Form::label('is_follow_referral', __('Включить подсчет страниц в "Панели оператора"'), ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-5">
                                <div class="material-switch switch-input-1 mt-7">
                                    <input id="is_enable_order_paginate" name="is_enable_order_paginate" type="checkbox" value="1" @if($user->is_enable_order_paginate == 1) checked @endif />
                                    <label for="is_enable_order_paginate" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="default-panel-footer default-panel-footer-right">
                        <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.control.save') }}</md-button>
                        <md-button ng-href="{{config('admin.directory')}}/account/users">{{ __('admin-account.control.cancel') }}</md-button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="x_panel">
                <div class="user_heading bg-primary-700">
                    <div class="user_heading_avatar">
                        <img src="{{ Gravatar::get($user->email, ['size' => 256]) }}" class="img-circle img-responsive" alt="">
                        <h6>{{ $user->name }}</h6>
                        <span> {{$user->roles()->pluck('name')->implode(', ')}}</span>
                    </div>
                    <div class="user_heading_content">
                        <ul class="user_stats">
                            <li><h4>@if($user->order_num > 0)  {{ $user->order_num }} @else 0 @endif <span class="sub-heading">Кол. обменов</span></h4></li>
                            <li><h4>@if($user->order_num > 0) ${{ iex_number_format($user->successOrderSum(), 2, true) }} @else 0 @endif<span class="sub-heading">Сумма обменов</span></h4></li>
                        </ul>
                    </div>
                </div>
                <div class="x_content">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">{{ __('admin-account.users.email') }}</div>
                            <div class="col-sm-6 tip user_heading_p">{{ $user->email }}</div>
                        </div>
                    </div>

                    @if(isset($user->referral_links))
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">{{ __('admin-account.control.referral_hash')  }}</div>
                                <div class="col-sm-6 tip user_heading_p">
                                    {{ $user->referral_links->code }} <a class="float-right" data-toggle="modal" href="#edit_referral_hash">{{ __('admin-account.control.edit_link') }}</a>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(isset($user->fromReferral))
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">{{ __('admin-account.control.from_partner') }}</div>
                                <div class="col-sm-6 tip user_heading_p">
                                    <a href="{{ admin_base_path('/account/users/'.$user->fromReferral->referral_link->user->id.'/edit') }}">{{ $user->fromReferral->referral_link->user->name }}</a>
                                </div>
                            </div>
                        </div>
                    @endif


                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">{{ __('admin-account.users.sorting_by_register_at') }}</div>
                            <div class="col-sm-6 tip user_heading_p">{{ \Illuminate\Support\Carbon::parse($user->created_at)->translatedFormat('d M Y H:i') }}</div>
                        </div>
                    </div>

                    @if($user->last_activity_at != null)
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">{{ __('admin-account.users.sorting_by_last_visit') }}</div>
                                <div class="col-sm-6 tip user_heading_p">{{ \Illuminate\Support\Carbon::parse($user->last_activity_at)->translatedFormat('d M Y H:i') }}</div>
                            </div>
                        </div>
                    @endif

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">{{ __('admin-account.control.ip_address') }}</div>
                            <div class="col-sm-6 tip user_heading_p">
                                <a href="{{ admin_base_path('account/logs_auth/?ip_address='.$user->ip_address) }}">{{ $user->ip_address }}</a><br />
                            </div>
                        </div>
                    </div>

                    @if($user->user_agent != null)
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">{{ __('admin-account.control.user_agent') }}</div>
                                <div class="col-sm-6 tip user_heading_p">
                                    {{ $user->user_agent }}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>


    <!-- Модальное окно для настройки экспорта данных -->
    <div class="modal fade" id="edit_referral_hash" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
                @csrf
                @method('PUT')
                <input type="hidden" name="edit_referral_hash" value="enable">
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-account.control.edit_referral_hash') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-account.control.referral_name') }}</label>
                            <div class="col-md-8">
                                <input name="referral_name" type="text" class="form-control" value="{{$user->referral_links->code}}" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <hr />
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-account.control.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-account.control.cancel') }}</md-button>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
