@extends('admin.layouts.app')

@section('title', __('admin-account.users.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users'))

@section('top-block')

    @can('admin_roles')
        <md-button data-toggle="modal" data-target="#create_user" class="btn-float" layout="column">
            <md-icon  md-font-icon="fa fa-user"></md-icon>
            <span>{{ __('admin-account.users.add_user_button') }}</span>
        </md-button>
    @endif

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings_columns" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-table"></md-icon>
            <span>{{ __('Настройка страницы') }}</span>
        </md-button>


        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <md-icon  md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('admin-account.users.settings') }}</span>
        </md-button>
    @endcan
    <style>

        .online-user-title {
            padding-right: 20px;
            color: #0275d8;
            font-weight: 500;
        }
        .user-widget-custom {
            margin-top: 13px;
            margin-left: 10px;
        }

        .user-widget-online-label {
            color: #b50ad4 !important;
        }

        .user-widget-verify-label {
            color: #478e14 !important;
        }

        .user-widget-clients-label {
            color: #188fff !important;
        }

        .user-widget-lock-label {
            color: #d02b2b !important;
        }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgb(251 251 251);
        }

        .new-x_panel_wrapper .table>tbody>tr>td, .new-x_panel_wrapper .table>tbody>tr>th, .new-x_panel_wrapper .table>tfoot>tr>td, .new-x_panel_wrapper .table>tfoot>tr>th, .new-x_panel_wrapper .table>thead>tr>td, .new-x_panel_wrapper .table>thead>tr>th {
            border: none;
        }


    </style>
@endsection

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="md-icon-button @if(iEXSetting('default_user_allow_filter') == 0) collapsed @endif" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>

        <div class="x_content collapse @if(iEXSetting('default_user_allow_filter') == 1) in @endif" id="showFilters">
            <form class="form-horizontal" method="get" action="">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">
                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.id') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="id" value="{{ is_filter_search($filter, 'id') }}" placeholder="{{ __('admin-account.users.search_by_id') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.name') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name"
                                               value="{{ is_filter_search($filter, 'name') }}" placeholder="{{ __('admin-account.users.search_by_name') }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox1" value="1" type="checkbox" name="checkbox_full_name" @if(isset($filter['checkbox_full_name'])) checked @endif>
                                            <label class="form-check-label" for="checkbox1">{{ __('admin-account.users.exact_name_match') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.email') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="email" value="{{ is_filter_search($filter, 'email') }}" placeholder="{{ __('admin-account.users.search_by_email') }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox2" value="1" type="checkbox" name="checkbox_full_email" @if(isset($filter['checkbox_full_email'])) checked @endif>
                                            <label class="form-check-label" for="checkbox2">{{ __('admin-account.users.exact_email') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.ip_address') }}:</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="ip_address" value="{{ is_filter_search($filter, 'ip_address') }}" placeholder="{{ __('admin-account.users.search_by_ip') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.referral_hash_label') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="referral" value="{{ is_filter_search($filter, 'referral') }}" placeholder="{{ __('admin-account.users.search_by_referral_hash') }}">
                                        <div style="font-size: 12px; margin-top: 3px;" class="text-muted">{{ __('admin-account.users.referral_hash_hint') }}</div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.group_label') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('group[]', $filterAllRoles, is_filter_search($filter, 'group'),
                                             ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'multiple', 'data-width' => '100%']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.referral_program_label') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('referral_program[]', $programs, is_filter_search($filter, 'referral_program'),
                                            ['class' => 'selectpicker', 'data-width' => '100%', 'data-live-search' => 'true', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.filter_by_label') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('filter_by', $userFilterBy, is_filter_search($filter, 'filter_by'),
                                            ['class' => 'selectpicker', 'data-width' => '100%']) !!}
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox5" value="1" type="checkbox" name="is_ban" @if(isset($filter['is_ban'])) checked @endif>
                                            <label class="form-check-label" for="checkbox5">{{ __('admin-account.users.banned_label') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox8" value="1" type="checkbox" name="is_withdrawal" @if(isset($filter['is_withdrawal'])) checked @endif>
                                            <label class="form-check-label" for="checkbox8">{{ __('admin-account.users.autopayment_bonus_label') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox9" value="1" type="checkbox" name="is_unique_user" @if(isset($filter['is_unique_user'])) checked @endif>
                                            <label class="form-check-label" for="checkbox9">{{ __('admin-account.users.individual_label') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox30" value="1" type="checkbox" name="is_admin_panel" @if(isset($filter['is_admin_panel'])) checked @endif>
                                            <label class="form-check-label" for="checkbox30">{{ __('admin-account.users.admin_label') }}</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox6" value="1" type="checkbox" name="is_verified" @if(isset($filter['is_verified'])) checked @endif>
                                            <label class="form-check-label" for="checkbox6">{{ __('admin-account.users.verify_email') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox7" value="1" type="checkbox" name="is_disabled" @if(isset($filter['is_disabled'])) checked @endif>
                                            <label class="form-check-label" for="checkbox7">{{ __('admin-account.users.disabled_label') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox10" value="1" type="checkbox" name="is_order" @if(isset($filter['is_order'])) checked @endif>
                                            <label class="form-check-label" for="checkbox10">{{ __('admin-account.users.prohibition_order') }}</label>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" id="is_provider" value="1" type="checkbox" name="is_provider" @if(isset($filter['is_provider'])) checked @endif>
                                            <label class="form-check-label" for="is_provider">{{ __('admin-account.users.from_provider') }}</label>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.register_created_at') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input  value="{{ is_filter_search($filter, 'from_date_register') }}" name="from_date_register" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-account.users.to_label') }}: <input  value="{{ is_filter_search($filter, 'to_date_register') }}" name="to_date_register" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.last_visit_at') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input value="{{ is_filter_search($filter, 'from_last_activity') }}" name="from_last_activity" style="width: 140px;display: inline-block;    margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-account.users.to_label') }}: <input value="{{ is_filter_search($filter, 'to_last_activity') }}" name="to_last_activity" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.last_auth_at') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input value="{{ is_filter_search($filter, 'from_last_login_at') }}" name="from_last_login_at" style="width: 140px;display: inline-block;    margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-account.users.to_label') }}: <input value="{{ is_filter_search($filter, 'to_last_login_at') }}" name="to_last_login_at" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.last_auth_logout') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input value="{{ is_filter_search($filter, 'from_last_logout_at') }}" name="from_last_logout_at" style="width: 140px;display: inline-block;    margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-account.users.to_label') }}: <input value="{{ is_filter_search($filter, 'to_last_logout_at') }}" name="to_last_logout_at" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.count_order_label') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input value="{{ is_filter_search($filter, 'from_order_num') }}" name="from_order_num" style="width: 70px;display: inline-block;margin-right: 10px;text-align: center" type="text" class="form-control">
                                        {{ __('admin-account.users.to_label') }}: <input value="{{ is_filter_search($filter, 'to_order_num') }}" name="to_order_num" style="width: 70px;display: inline-block;text-align: center" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.referral_bonus_label') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input value="{{ is_filter_search($filter, 'from_referral_num') }}" name="from_referral_num" style="width: 70px;display: inline-block;margin-right: 10px;text-align: center" type="text" class="form-control">
                                        {{ __('admin-account.users.to_label') }}: <input value="{{ is_filter_search($filter, 'to_referral_num') }}" name="to_referral_num" style="width: 70px;display: inline-block;text-align: center" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.cashback_bonus_label') }}:</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.users.from_label') }}: <input value="{{ is_filter_search($filter, 'from_cashback_num') }}" name="from_cashback_num" style="width: 70px;display: inline-block;margin-right: 10px;text-align: center" type="text" class="form-control">
                                        {{ __('admin-account.users.to_label') }}: <input value="{{ is_filter_search($filter, 'to_cashback_num') }}" name="to_cashback_num" style="width: 70px;display: inline-block;text-align: center" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.provider_label') }}:</label>
                                    <div class="col-md-6">
                                        {!! Form::select('provider[]', $listAuthSystems, is_filter_search($filter, 'provider'),
                                            ['class' => 'selectpicker', 'data-width' => '205px', 'data-live-search' => 'true', 'multiple', 'data-size' => '10']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('admin-account.users.count_user_page') }}:</label>
                                    <div class="col-md-8">
                                        <input type="text" style="width: 205px;text-align: center" class="form-control" name="per_page" value="{{ is_filter_search($filter, 'per_page', config('exchange.pagination_limit.users')) }}" placeholder="{{ __('admin-account.users.count_user_page') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>
                <div style="margin: 0 20px;">{{ __('admin-account.users.position_soring_user') }}</div>
                <hr/>
                <div class="row task-filter-body user-sorting">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-around stretch">
                            <div class="form-group">
                                <label class="control-label">{{ __('admin-account.users.sorting_by_id') }}:</label>
                                <br />
                                {!! Form::select('sorting_ids', $userSelectSorting, is_filter_search($filter, 'sorting_ids'), ['class' => 'selectpicker']) !!}
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{ __('admin-account.users.sorting_by_email') }}:</label>
                                <br />
                                {!! Form::select('sorting_email', $userSelectSorting, is_filter_search($filter, 'sorting_email'), ['class' => 'selectpicker']) !!}
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{ __('admin-account.users.sorting_by_order') }}:</label>
                                <br />
                                {!! Form::select('sorting_order_num', $userSelectSorting, is_filter_search($filter, 'sorting_order_num'), ['class' => 'selectpicker']) !!}
                            </div>


                            <div class="form-group">
                                <label class="control-label">{{ __('admin-account.users.sorting_by_register_at') }}:</label>
                                <br />
                                {!! Form::select('sorting_created_at', $userSelectSorting, is_filter_search($filter, 'sorting_created_at'),
                                ['class' => 'selectpicker']) !!}
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{ __('admin-account.users.sorting_by_last_visit') }}:</label>
                                <br />
                                {!! Form::select('sorting_last_activity', $userSelectSorting, is_filter_search($filter, 'sorting_last_activity'),
                                ['class' => 'selectpicker']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.users.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-account.users.clear_filter') }}</md-button>
                </div>

            </form>
        </div>
    </div>

    <div class="padding-lr-0">
        <dzv class="x_panel new-x_panel_wrapper">
            <div class="x_title" layout="row">
                <h2>{{ __('admin-account.users.list_users_count') }} ({{ $items->total() }}) </h2>
                <div flex=""></div>
                @if(Module::find('ExportData')->isEnabled())
                    <div class="heading-right-button">
                        <md-button data-toggle="modal" data-target="#export_users">{{ __('admin-account.users.export_users') }}</md-button>
                    </div>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-border-2 table-striped iex-adaptive__table">
                    <thead>
                    <tr>
                        <th></th>
                        @if(in_array('name', $admin_user_hidden_columns))
                            <th>{{ __('Имя') }}</th>
                        @endif
                        @if(in_array('email', $admin_user_hidden_columns))
                            <th>{{ __('E-mail') }}</th>
                        @endif
                        @if(in_array('count_exchange', $admin_user_hidden_columns))
                            <th>{{ __('Кол. обменов') }}</th>
                        @endif
                        @if(in_array('balance', $admin_user_hidden_columns))
                            <th>{{ __('Баланс') }}</th>
                        @endif
                        @if(in_array('ip_address_browser', $admin_user_hidden_columns))
                            <th>{{ __('IP Адрес/Браузер') }}</th>
                        @endif
                        @if(in_array('created_at', $admin_user_hidden_columns))
                            <th>{{ __('Дата регистрации') }}</th>
                        @endif
                        @if(in_array('last_exchange', $admin_user_hidden_columns))
                            <th>{{ __('Посл. посещение') }}</th>
                        @endif
                        <th></th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(count($items) > 0)
                        @foreach($items as $item)
                            <tr style="@if($item->deactivation) background: #ffefef; @elseif($item->isBanned()) background-color: rgb(255 197 197 / 12%); border-left: 3px solid #dc0000; @elseif(!is_null($item->email_verified_at)) background-color: #6ff15b1f;  @endif">
                                <th>
                                    <img src="{{ Gravatar::get($item->email, ['size' => 32]) }}" class="img-circle img-responsive" alt="">
                                </th>
                                @if(in_array('name', $admin_user_hidden_columns))
                                    <td data-label="{{  __('Имя') }}">
                                        <a style="font-weight: bold" href="{{config('admin.directory')}}/account/users/{{$item->id}}/edit">
                                            @if($item->is_vip_client)
                                                <span style="color: #bd258e">{{ $item->name }}</span>
                                            @else
                                                {{ $item->name }}
                                            @endif
                                            &nbsp;<i class="fal fa-pencil"></i>
                                        </a>
                                        @if($item->roles()->pluck('name')->implode(', ') != null)
                                            <div style="font-size:12px;color: red;font-weight: 400">
                                                {{$item->roles()->pluck('name')->implode(', ')}}
                                                @if(!is_null($item->role_expired_at))
                                                    <span style="color: #989e31">({{ __('admin-account.users.temporary') }})</span>
                                                @endif
                                            </div style="font-size:12px;color: red;font-weight: 400">
                                        @endif

                                        @if($item->is_verify_account == 1)
                                            <div>
                                                <small class="text-success">Личность подтверждена</small>
                                            </div>
                                        @else
                                            <div>
                                                <small class="text-danger">Личность не подтверждена</small>
                                            </div>
                                        @endif

                                        <!-- От реферала -->
                                        @if(isset($item->fromReferral))
                                            <div style="font-size:11px;" class="text-muted">
                                                {{ __('admin-account.users.from_partner') }}:
                                                <a href="{{ admin_base_path('/account/users/?id='.$item->fromReferral->referral_link->user->id) }}">{{ $item->fromReferral->referral_link->user->name }}</a>
                                            </div>
                                        @endif

                                        <!--Последние обмены-->
                                        @if(isset($item->lastedOrderAt))
                                            <div style="font-size:11px;" class="text-muted">
                                                {{ __('admin-account.users.last_exchange_label') }}: {{ \Illuminate\Support\Carbon::parse($item->lastedOrderAt->updated_at)->diffForHumans() }}
                                            </div>
                                        @endif

                                        <!-- Провайдер -->
                                        @if(isset($item->provider))
                                            <div style="font-size:11px;" class="text-muted">
                                                {{ __('admin-account.users.provider_label') }}:
                                                <a href="{{ admin_base_path('/account/users?provider[]='.$item->provider) }}">{{ $item->provider }}</a>
                                            </div>
                                        @endif

                                        @if($item->isBanned() and isset($item->bans[0]))
                                            <div class="clearfix"></div>
                                            <small class="text-danger">
                                                {{ __('admin-account.users.client_blocked_to') }} <br/> {{ \Illuminate\Support\Carbon::parse($item->bans[0]->expired_at)->translatedFormat('d M Y H:i') }}
                                            </small>
                                        @endif

                                    </td>
                                @endif
                                @if(in_array('email', $admin_user_hidden_columns))
                                    <td data-label="{{ __('E-mail') }}">
                                        <div class>
                                            @if(!is_null($item->email_verified_at))
                                                <i class="far fa-user-check text-success"></i>
                                            @endif
                                            {{ $item->email }}
                                        </div>
                                        @if(is_null($item->email_verified_at))
                                            <div style="margin-top: 3px; font-size: 11px;" class="text-danger">{{ __('admin-account.users.not_email_verify') }}</div>
                                            <small>
                                                <a href="?verify_email&id={{ $item->id }}">{{ __('admin-account.users.resend') }}</a>
                                            </small>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('count_exchange', $admin_user_hidden_columns))
                                    <td data-label="{{ __('Кол. обменов') }}">
                                        @if($item->order_num > 0)
                                            {{ $item->order_num }}
                                            (<b>${{ iex_number_format($item->successOrderSum(), 2) }}</b>)<br />
                                            <a class="text-primary" href="users/{{$item->id}}/download_order" style="font-size:11px">{{ __('admin-account.users.loading_order') }}</a>
                                        @else
                                            <small class="text-muted">{{ __('admin-account.users.not_order_label') }}</small>
                                        @endif
                                    </td>
                                @endif

                                @if(in_array('balance', $admin_user_hidden_columns))
                                    <td style="font-size: 12px;color: #999;" data-label="{{ __('Баланс') }}">
                                        @if(isset($item->user_balance))
                                            @if(config('crypto.currency_payout_position') == 'left')
                                                {{ __('admin-account.users.referral_label') }}: {{ config('crypto.currency_payout_sign') }}{{ $item->user_balance->balance }}<br />
                                                {{ __('admin-account.users.cashback_label') }}: {{ config('crypto.currency_payout_sign') }}{{ $item->user_balance->balance_reward }}<br />
                                            @else
                                                {{ __('admin-account.users.referral_label') }}: {{ $item->user_balance->balance }} {{ config('crypto.currency_payout_sign') }}<br />
                                                {{ __('admin-account.users.cashback_label') }}: {{ $item->user_balance->balance_reward }} {{ config('crypto.currency_payout_sign') }}<br />
                                            @endif
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('ip_address_browser', $admin_user_hidden_columns))
                                    <td data-label="{{ __('IP Адрес/Браузер') }}">

                                        @if($item->is_hidden_ip_address == 1)
                                            <small class="text-muted">IP Адрес скрыт</small>
                                        @else
                                            @if(!is_null($item->ip_address))
                                                <a href="logs_auth/?ip_address={{$item->ip_address}}">{{ $item->ip_address }}</a><br />
                                                <small class="text-muted">{{ __('admin-account.users.matches') }}: {{ $item->countIPAddress() }}</small>
                                            @else
                                                <span class="text-muted">{{ __('admin-account.users.undefined') }}</span>
                                            @endif
                                        @endif

                                        @if(!empty($item->user_browser))
                                            <small class="text-muted">/ {{ $item->user_browser }}</small>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('created_at', $admin_user_hidden_columns))
                                    <td data-label="{{ __('Дата регистрации') }}">
                                        <div class=""> {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}</div>
                                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                    </td>
                                @endif
                                @if(in_array('last_exchange', $admin_user_hidden_columns))
                                    <td data-label="{{ __('Посл. посещение') }}">
                                        @if($item->last_activity_at != null)
                                            <div class="">{{ \Illuminate\Support\Carbon::parse($item->last_activity_at)->translatedFormat('d M Y H:i') }}</div>

                                            @if(\Illuminate\Support\Carbon::parse($item->last_activity_at)->addMinutes(3) > \Illuminate\Support\Carbon::now() )
                                                <small class="text-success">Online</small>
                                            @else
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->last_activity_at)->diffForHumans() }}</small>
                                            @endif

                                        @else
                                            <span class="text-muted">{{ __('admin-account.users.undefined') }}</span>
                                        @endif
                                    </td>
                                @endif
                                <td>

                                    <ul class="breadcrumb-elements" hide-xs layout="row" layout-align="center center">

                                        <li class="dropdown">
                                            <md-button href="#" class="md-icon-button" data-toggle="dropdown">
                                                <i class="far fa-ellipsis-v font-size-16"></i>
                                            </md-button>
                                            <ul class="dropdown-menu dropdown-menu-left">
                                                <li><a ng-href="logs_auth/?id_user={{$item->id}}">{{ __('admin-account.users.log_auth') }}</a></li>

                                                <li><a ng-href="users/{{$item->id}}/internal_account">{{ __('admin-account.users.internal_account') }}</a></li>

                                                <li><a ng-href="{{ admin_base_path('/tools/verification-card?send_action=on&id_user='.$item->id) }}">{{ __('admin-account.users.verification_card') }}</a></li>

                                                <li class="divider"></li>
                                                <li><a ng-href="users/{{$item->id}}/referral?filter=new">{{ __('admin-account.users.referrals_client') }}</a></li>
                                                <li><a ng-href="users/{{$item->id}}/referral_statistics">{{ __('admin-account.users.referrals_statistics') }}</a></li>

                                                <li class="divider"></li>
                                                <li><a ng-href="users/{{$item->id}}/payouts">{{ __('admin-account.users.history_payout') }}</a></li>
                                                <li><a ng-href="users/{{$item->id}}/banned_histories">{{ __('admin-account.users.history_bans') }}</a></li>
                                                <li><a ng-href="users/{{$item->id}}/balances">{{ __('admin-account.users.control_balance') }}</a></li>
                                                <li><a ng-href="{{ admin_base_path('applications?id_user='.$item->id) }}">{{ __('admin-account.users.order_user') }}</a></li>
                                                <li class="divider"></li>
                                                @if($item->isBanned())
                                                    <li><a ng-href="users/{{$item->id}}/revoke_user">{{ __('admin-account.users.unbanned') }}</a></li>
                                                @else
                                                    <li><a ng-href="users/{{$item->id}}/ban" style="color: orangered">{{ __('admin-account.users.banned') }}</a></li>
                                                @endif


                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </dzv>

        <div class="pagination-right">
            {!! $items->appends($filter)->links() !!}
        </div>
    </div>



    <!-- Модальное окно для настройки экспорта данных -->
    @if(Module::find('ExportData')->isEnabled())
        <div class="modal fade" id="export_users" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" >
                <form action="?" method="POST" class="form-horizontal">
                    <input type="hidden" name="export_users" value="enable">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header ui-dialog-titlebar">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-account.users.export_user_title') }}</span>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label class="control-label col-md-4">{{ __('admin-account.users.export_fields') }}</label>
                                <div class="col-md-8">
                                    <select class="form-control selectpicker" name="columns[]" multiple data-width="100%" required>
                                        <option value="id">{{ __('admin-account.users.id') }}</option>
                                        <option value="name">{{ __('admin-account.users.name') }}</option>
                                        <option value="email">{{ __('admin-account.users.email') }}</option>
                                        <option value="created_at">{{ __('admin-account.users.register_created_at') }}</option>
                                        <option value="updated_at">{{ __('admin-account.users.last_visit_at') }}</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <hr />
                            <div class="form-group">
                                <label class="control-label col-md-4">{{ __('admin-account.users.group_label') }}</label>
                                <div class="col-md-8">
                                    {!! Form::select('group[]', $filterAllRoles, [],
                                                         ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '100%']) !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">{{ __('admin-account.users.register_created_at') }}</label>
                                <div class="col-md-8">
                                    {{ __('admin-account.users.from_label') }}: <input name="from_date_register" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                    {{ __('admin-account.users.to_label') }}: <input name="to_date_register" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">{{ __('admin-account.users.date_last_visit') }}</label>
                                <div class="col-md-8">
                                    {{ __('admin-account.users.from_label') }}: <input name="from_last_activity" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                    {{ __('admin-account.users.to_label') }}: <input name="to_last_activity" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <hr />
                            <div class="form-group">
                                <label class="control-label col-md-4">{{ __('admin-account.users.type_export_field') }}</label>
                                <div class="col-md-8">
                                    <select class="form-control selectpicker" name="export_type" data-width="100%" required>
                                        <option value="">-- {{ __('admin-account.users.no_selected') }} --</option>
                                        @foreach(config('modules-config.exportdata.options.lists') as $export)
                                            <option value="{{$export}}">{{ __('admin-account.users.export_to', ['name' => $export]) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <hr />
                            <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                            <md-button type="submit" class="md-primary md-raised">{{ __('admin-account.users.export_button') }}</md-button>
                            <md-button type="button" data-dismiss="modal">{{ __('admin-account.users.cancel') }}</md-button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-account.users.settings_title') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-account.users.enable_log_auth') }}</label>
                            <div class="col-md-6">
                                <select class="selectpicker" name="enable_user_auth_log" data-width="250px">
                                    <option value="0"  @if(iEXSetting('enable_user_auth_log') == 0) selected @endif>{{ __('admin-account.users.yes') }}</option>
                                    <option value="1"  @if(iEXSetting('enable_user_auth_log') == 1) selected @endif>{{ __('admin-account.users.no') }}</option>
                                </select>
                            </div>
                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-account.users.default_filter') }}</label>
                            <div class="col-md-6">
                                <select class="selectpicker" name="default_user_allow_filter" data-width="250px">
                                    <option value="0"  @if(iEXSetting('default_user_allow_filter') == 0) selected @endif>{{ __('admin-account.users.hidden') }}</option>
                                    <option value="1"  @if(iEXSetting('default_user_allow_filter') == 1) selected @endif>{{ __('admin-account.users.visible') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-account.users.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-account.users.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Окно добавления нового пользователя -->
    <div class="modal fade" id="create_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            {{ Form::open(['url' => admin_base_path('/account/users'),'class' => 'form-horizontal']) }}
            @csrf
            <div class="modal-content">
                <div class="modal-header ui-dialog-titlebar">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="ui-dialog-title" id="myModalLabel">Добавить пользователя</span>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>{{ __('admin-account.control.name') }}:</label>
                                {{ Form::text('name', '', ['class' => 'form-control']) }}
                            </div>
                            <div class="col-sm-6">
                                <label>{{ __('admin-account.control.password') }}:</label>
                                {{ Form::password('password', ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>{{ __('admin-account.control.email') }}:</label>
                                {{ Form::email('email', '', ['class' => 'form-control']) }}
                            </div>
                            <div class="col-sm-6">
                                <label>{{ __('admin-account.control.group_label') }}:</label>
                                {{ Form::select('roles[]', $roles->pluck('name', 'id'), [],
                                 ['multiple', 'class' => 'selectpicker', 'data-width'=>'100%', 'data-live-search' => 'true']) }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-primary md-raised">{{ __('admin-account.users.save') }}</md-button>
                    <md-button type="button" data-dismiss="modal">{{ __('admin-account.users.cancel') }}</md-button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- Модальное окно для настроек таблицы -->
    <div class="modal fade" id="settings_columns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings_columns">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка страницы') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Колонки') }}</label>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[name]" value="name" type="checkbox" name="admin_user_hidden_columns[name]" @if(in_array('name', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[name]">{{ __('Имя') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[email]" value="email" type="checkbox" name="admin_user_hidden_columns[email]" @if(in_array('email', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[email]">{{ __('E-mail') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[count_exchange]" value="count_exchange" type="checkbox" name="admin_user_hidden_columns[count_exchange]" @if(in_array('count_exchange', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[count_exchange]">{{ __('Кол. обменов') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[balance]" value="balance" type="checkbox" name="admin_user_hidden_columns[balance]" @if(in_array('balance', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[balance]">{{ __('Баланс') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[ip_address_browser]" value="ip_address_browser" type="checkbox" name="admin_user_hidden_columns[ip_address_browser]" @if(in_array('ip_address_browser', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[ip_address_browser]">{{ __('IP Адрес/Браузер') }}</label>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[created_at]" value="created_at" type="checkbox" name="admin_user_hidden_columns[created_at]" @if(in_array('created_at', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[created_at]">{{ __('Дата регистрации') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_user_hidden_columns[last_exchange]" value="last_exchange" type="checkbox" name="admin_user_hidden_columns[last_exchange]" @if(in_array('last_exchange', $admin_user_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_user_hidden_columns[last_exchange]">{{ __('Посл. посещение') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
