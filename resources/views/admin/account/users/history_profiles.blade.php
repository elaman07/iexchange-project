@extends('admin.layouts.app')

@section('title', __('admin-account.history_profiles.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.history_profiles'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-account.history_profiles.created_at') }}</th>
            <th>{{ __('admin-account.history_profiles.user_id') }}</th>
            <th>{{ __('admin-account.history_profiles.event') }}</th>
            <th>{{ __('admin-account.history_profiles.who_changed') }}</th>
            <th>{{ __('admin-account.history_profiles.what_changed') }}</th>
            <th>{{ __('admin-account.history_profiles.url_page') }}</th>
            <th>{{ __('admin-account.history_profiles.ip_address') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($histories) > 0)
            @foreach($histories as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>
                        @if($log->id_user > 0)
                            <a href="{{ admin_base_path('/account/users?action=filter&id='.$log->id_user) }}"><b>{{$log->user->name}}</b></a>
                        @else
                            <small class="text-muted">{{ __('admin-account.history_profiles.system_name') }}</small>
                        @endif
                    </td>
                    <td style="max-width: 300px">{{ $log->event_name }}</td>
                    <td>
                        @if($log->id_changed > 0)
                            <a href="{{ admin_base_path('/account/users?action=filter&id='.$log->id_changed) }}"><b>{{$log->user_changed->name}}</b></a>
                        @else
                            <small class="text-muted">{{ __('admin-account.history_profiles.system_name') }}</small>
                        @endif
                    </td>
                    <td>{!! $log->text !!}</td>
                    <td>{{ $log->url }}</td>
                    <td>{{ $log->ip_address }}</td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9"><p align="center"><br />{{ __('admin-account.list_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $histories->links() !!}
    </div>
@endsection

@section('footer-notify')
    <div class="alert alert-warning">{{ __('admin-account.history_profiles.description') }}</div>
@endsection