@extends('admin.layouts.app')

@section('title', __('admin-account.payouts_history.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.payouts'))

@section('no-block-content')

    @if($user->user_balance->balane and $user->user_balance->balance_reward == 0)
        <div class="alert alert-danger">{{ __('admin-account.payouts_history.client_not_bonus') }}</div>
    @else
        @if($is_payouts > 0)

            <div class="alert alert-warning">

                <div class="flash-alert-row" layout="row" layout-align="start center">
                    <i class="fal fa-exclamation-triangle"></i>
                    <div class="flash-alert-column">
                        <div class="flash-alert-title">Предупреждение</div>
                        <div class="flash-alert-content"> {{ __('admin-account.payouts_history.client_not_handler') }}</div>
                    </div>
                </div>
            </div>
        @else
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ __('admin-account.payouts_history.application_for_payment') }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal" method="post" action="">
                        @csrf
                        <div class="row task-filter-body">
                            <div class="padding-20">
                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ __('admin-account.payouts_history.payment_system') }}</label>
                                    <div class="col-md-4">
                                        <select name="id_currency" class="selectpicker">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">
                                                    {{ sprintf("%s %s", $currency->payment->name, $currency->code_currency->name) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ __('admin-account.payouts_history.number_account') }}</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="score" placeholder="{{ __('admin-account.payouts_history.number_account_hint') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">{{ __('admin-account.payouts_history.type_payout') }}</label>
                                    <div class="col-md-6">
                                        <select name="purpose" class="selectpicker">
                                            <option value="cashback_and_referral">{{ __('admin-account.payouts_history.all') }}</option>
                                            <option value="referral">{{ __('admin-account.payouts_history.referral') }}</option>
                                            <option value="cashback">{{ __('admin-account.payouts_history.cashback') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.payouts_history.create') }}</md-button>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    @endif

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-account.payouts_history.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-account.payouts_history.id') }}</th>
                    <th>{{ __('admin-account.payouts_history.currency') }}</th>
                    <th>{{ __('admin-account.payouts_history.number_account') }}</th>
                    <th>{{ __('admin-account.payouts_history.referral') }}</th>
                    <th>{{ __('admin-account.payouts_history.cashback') }}</th>
                    <th>{{ __('admin-account.payouts_history.status') }}</th>
                    <th>{{ __('admin-account.payouts_history.created_at') }}</th>
                    <th>{{ __('admin-account.payouts_history.updated_at') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($payouts) > 0)
                    @foreach($payouts as $payout)
                        <tr>
                            <th>{{ $payout->id }}</th>
                            <td>
                                {{ $payout->currency->payment->name }}
                                {{ $payout->currency->code_currency->name }}
                            </td>
                            <td>
                                {{ $payout->score }}
                            </td>
                            <td>
                                @if($payout->referral)
                                    @if(config('crypto.currency_payout_position') == 'left')
                                        {{ config('crypto.currency_payout_sign') }} {{ $payout->base_referral }} руб
                                    @else
                                        {{ $payout->base_referral }} {{ config('crypto.currency_payout_sign') }}<br />
                                    @endif
                                @else
                                    {{ __('admin-account.payouts_history.no') }}
                                @endif
                            </td>
                            <td>
                                @if($payout->reward)
                                    @if(config('crypto.currency_payout_position') == 'left')
                                        {{ config('crypto.currency_payout_sign') }} {{ $payout->base_reward }} руб
                                    @else
                                        {{ $payout->base_reward }} {{ config('crypto.currency_payout_sign') }}<br />
                                    @endif
                                @else
                                    {{ __('admin-account.payouts_history.no') }}
                                @endif
                            </td>
                            <td>
                                @if($payout->status)
                                    <span class="text-success">{{ __('admin-account.payouts_history.done') }}</span>
                                @else
                                    <span class="text-danger">{{ __('admin-account.payouts_history.process') }}</span>
                                @endif
                            </td>
                            <td>{{ $payout->created_at }}</td>
                            <td>{{ $payout->updated_at }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10"><p align="center"><br />{{ __('admin-account.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
