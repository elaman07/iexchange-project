@extends('admin.layouts.app')

@section('title', __('admin-account.codes.title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.balance', $user))

@section('content')

    <div style="padding: 30px;text-align: center">
        <p class="text-muted">{{ __('admin-account.codes.text') }}</p>
        <md-button ng-href="?download=true" class="md-raised md-primary">{{ __('admin-account.codes.download_code') }}</md-button>
    </div>

@endsection