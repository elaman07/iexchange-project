@extends('admin.layouts.app')

@section('title', __('admin-account.control.add_user'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.add'))

@section('content')
    {{ Form::open(array('url' => admin_base_path('/account/users'),'class' => 'form-horizontal')) }}

    <div class="default-panel-body">
    <div class="form-group">
        {{ Form::label('name', __('admin-account.control.name'), ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
        {{ Form::text('name', '', array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('email', __('admin-account.control.email'), ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
        {{ Form::email('email', '', array('class' => 'form-control')) }}
        </div>
    </div>

    <div class='form-group'>
        {{ Form::label('email', __('admin-account.control.rule'), ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
            @foreach ($roles as $role)
                <div class="checkbox checkbox-primary">
                    {{ Form::checkbox('roles[]',  $role->id, null ,['id' => str_slug($role->name)]) }}
                    {{ Form::label(str_slug($role->name), ucfirst($role->name),['class' => '']) }}
                    <br>
                </div>
            @endforeach
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('password', __('admin-account.control.password') ,['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
        {{ Form::password('password', array('class' => 'form-control')) }}
        </div>

    </div>

    <div class="form-group">
        {{ Form::label('password', __('admin-account.control.confirm_password'), ['class' => 'col-sm-2 control-label']) }}
        <div class="col-sm-10">
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
        </div>
    </div>

    </div>

    <div class="default-panel-footer default-panel-footer-right">
        <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.control.save') }}</md-button>
        <md-button ng-href="{{ admin_base_path('/account/users') }}" class="md-raised">{{ __('admin-account.control.cancel') }}</md-button>
    </div>

    {{ Form::close() }}

@endsection