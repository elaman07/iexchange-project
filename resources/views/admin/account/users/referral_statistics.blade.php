@extends('admin.layouts.app')

@section('title', __('admin-account.referral_stat.title', ['name' => $user->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.account.users.referral_statistics', $user))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-account.referral_stat.created_at') }}</th>
            <th>{{ __('admin-account.referral_stat.earned_month') }}</th>
            <th>{{ __('admin-account.referral_stat.attracted_month') }}</th>
            <th>{{ __('admin-account.referral_stat.last_success_bid') }}</th>
            <th>{{ __('admin-account.referral_stat.last_client') }}</th>
            <th>{{ __('admin-account.referral_stat.min_bonus_month') }}</th>
            <th>{{ __('admin-account.referral_stat.max_bonus_month') }}</th>
        </tr>

        </thead>
        <tbody>
        @foreach($array_amount as $key => $value)
            <tr>
                <th>{{ $value['snapshot'] }}</th>
                <td>{{ $value['earned'] }}</td>
                <td>
                    @if( $value['attracted'] == 0)
                        <span class="label label-danger">{{ $value['attracted'] }}</span>
                    @else
                        <span class="label label-success">{{ $value['attracted'] }}</span>
                    @endif
                </td>
                <td>
                    @if(!is_null($value['last_order']))
                        <a href="{{ admin_base_path('/applications?id='.$value['last_order']['id']) }}">
                            <b>{{ __('admin-account.referral_stat.order_number', ['id' => $value['last_order']['id']]) }}</b>
                        </a><br />

                        @if(!is_null($value['last_order']))
                            <small class="text-muted">{{ __('admin-account.referral_stat.ref_bonus') }}: {{$value['last_order']['referral_log']['bonus']}}</small>
                        @else
                            <small class="text-muted">{{ __('admin-account.referral_stat.ref_bonus') }}: 0</small>
                        @endif
                    @else
                        <span class="text-danger">{{ __('admin-account.referral_stat.not_found') }}</span>
                    @endif
                </td>
                <td>
                    @if(!is_null($value['last_order']))
                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$value['last_order']['user']['id'].'&sorting=id') }}">
                            <b>{{$value['last_order']['user']['name']}}</b>
                        </a><br />
                        <small class="text-muted" data-toggle="tooltip" data-placement="top" title="{{ __('admin-account.referral_stat.last_activity') }}">
                            @if(!is_null($value['last_order']['user']['last_activity_at']))
                                {{ \Illuminate\Support\Carbon::parse($value['last_order']['user']['last_activity_at'])->diffForHumans() }}
                            @else
                                {{ __('admin-account.referral_stat.undefined') }}
                            @endif
                        </small>
                    @else
                        <span class="text-muted">0</span>
                    @endif
                </td>
                <td>
                    @if(!is_null($value['min_bonus']))
                        <a href="{{ admin_base_path('/applications?id='.$value['min_bonus']['id_task']) }}">
                            <b>{{ __('admin-account.referral_stat.order_number', ['id' => $value['min_bonus']['id_task']]) }}</b>
                        </a><br />
                        <small class="text-muted">{{ __('admin-account.referral_stat.ref_bonus') }}: {{$value['min_bonus']['bonus_number']}} руб</small>
                    @else
                        <span class="text-muted">0</span>
                    @endif
                </td>
                <td>
                    @if(!is_null($value['max_bonus']))
                        <a href="{{ admin_base_path('/applications?id='.$value['max_bonus']['id_task']) }}">
                            <b>{{ __('admin-account.referral_stat.order_number', ['id' => $value['max_bonus']['id_task']]) }}</b>
                        </a><br />
                        <small class="text-muted">{{ __('admin-account.referral_stat.ref_bonus') }}: {{$value['max_bonus']['bonus_number']}} руб</small>
                    @else
                        <span class="text-muted">0</span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection