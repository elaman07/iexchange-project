@extends('admin.layouts.app')

@section('title', __('admin-account.permissions.title'))

@section('top-block')
    <md-button ng-href="permissions/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-account.permissions.add_permission') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.account.permissions'))

@section('content')
    <div class="table-responsive">
        <table class="table table-border-2">

            <thead>
                <tr>
                    <th>{{ __('admin-account.permissions.rule') }}</th>
                    <th class="not-sortable"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <th>{{ $permission->name }}</th>
                    <td style="width: 150px">
                        <md-button class="md-icon-button" href="{{ admin_base_path('/account/permissions/'.$permission->id.'/edit') }}">
                            <i class="fa fa-fw fa-pencil"></i>
                        </md-button>

                        <form style="display: inline-block;"  method="POST" action="{{ route('permissions.destroy', $permission->id) }}">
                            <input name="_method" type="hidden" value="DELETE" autocomplete="off">
                            @csrf

                            <md-button class="md-icon-button md-warn" type="submit">
                                <i class="fa fa-fw fa-trash"></i>
                            </md-button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection