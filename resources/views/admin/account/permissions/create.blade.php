@extends('admin.layouts.app')

@section('title', __('admin-account.permissions.title_add'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.permissions.add'))

@section('content')
    {{ Form::open(array('url' => admin_base_path('/account/permissions'),'id' => 'create', 'class' => 'form-horizontal')) }}

    <div class="default-panel-body">
    <div class="form-group">
        {{ Form::label('name', __('admin-account.permissions.name')) }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('id_permissions_group', __('admin-account.permissions.group')) }}
        {{ Form::select('id_permissions_group', \App\Models\PermissionsGroup::all()->pluck('name','id'), null, ['class' => 'form-control selectpicker']) }}
    </div>

    <div class="form-group">
        {{ Form::label('title', __('admin-account.permissions.headline')) }}
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('text', __('admin-account.permissions.description')) }}
        {{ Form::textarea('text', null, ['class' => 'form-control', 'rows' => 5]) }}
    </div>

    <div class="form-group">
        @if(!$roles->isEmpty())
            <h4>{{ __('admin-account.permissions.permission_to_rule') }}</h4>
            @foreach ($roles as $role)

                <div class="checkbox checkbox-primary">
                    {{ Form::checkbox('roles[]',  $role->id, null ,['id' => str_slug($role->name)]) }}
                    {{ Form::label(str_slug($role->name), ucfirst($role->name),['class' => '']) }}
                    <br>
                </div>
            @endforeach
        @endif
    </div>

    </div>

    <div class="default-panel-footer default-panel-footer-right">
        <md-button class="md-raised md-primary" name="save" type="submit">
            <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp; {{ __('admin-account.permissions.add_button') }}</md-button>
        <md-button ng-href="{{ admin_base_path('/account/permissions') }}">{{ __('admin-account.permissions.back') }}</md-button>
    </div>
    {{ Form::close() }}
@endsection