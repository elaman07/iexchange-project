@extends('admin.layouts.app')

@section('title', __('admin-account.admin_log.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.admin_logs_auth'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-account.log.filter') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-account.admin_log.id_user') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-account.admin_log.email_user') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'email_address', is_filter_search($filter, 'email_address'), ['class' => 'form-control']) }}
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox2" value="1" type="checkbox" name="checkbox_full_email" @if(isset($filter['checkbox_full_email'])) checked @endif>
                                            <label class="form-check-label" for="checkbox2">{{ __('admin-account.admin_log.exact_email') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-account.admin_log.ip_address') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'ip_address', is_filter_search($filter, 'ip_address'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-account.admin_log.created_at') }}</label>
                                    <div class="col-md-8">
                                        {{ __('admin-account.admin_log.from') }}: <input  value="{{ is_filter_search($filter, 'from_created_at') }}" name="from_created_at" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-account.admin_log.to') }}: <input  value="{{ is_filter_search($filter, 'to_created_at') }}" name="to_created_at" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-account.admin_log.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-account.admin_log.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-account.admin_log.title_label') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-account.admin_log.date') }}</th>
                    <th>{{ __('admin-account.admin_log.user') }}</th>
                    <th>{{ __('admin-account.admin_log.old_ip_address') }}</th>
                    <th>{{ __('admin-account.admin_log.current_ip_address') }}</th>
                    <th>{{ __('admin-account.admin_log.status') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($logs) > 0)
                    @foreach($logs as $log)
                        <tr>
                            <td>
                                <div>{{ $log->created_at }}</div>
                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</small>
                            </td>
                            <td>
                                <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$log->user->id}}">
                                    <b>{{ $log->user->name }}</b>
                                </a>
                                <br />
                                <small>{{ $log->user->email }}</small>
                            </td>
                            <td>
                                @if(!is_null($log->prev_ip_address))
                                    <a href="{{ admin_base_path('account/logs_auth?ip_address='.$log->prev_ip_address) }}">{{ $log->prev_ip_address }}</a>
                                @else
                                    <small class="text-muted">{{ __('admin-account.admin_log.undefined') }}</small>
                                @endif
                            </td>
                            <td>
                                <a href="{{ admin_base_path('account/logs_auth?ip_address='.$log->ip_address) }}">{{ $log->ip_address }}</a>
                            </td>
                            <td>
                                @if($log->is_successful == 1)
                                    <span class="text-success">{{ __('admin-account.admin_log.success_auth') }}</span>
                                @else
                                    <span class="text-danger">{{ __('admin-account.admin_log.failed_auth') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-account.list_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
