@extends('admin.layouts.app')

@section('title', __('admin-account.block_ip.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.account.block_ip'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-account.block_ip.add_title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content panel-body">
            <form action="@if(!empty($editData['filter_name']))?edit=true&id={{$editData->id}}@endif" method="post" class="form-horizontal">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('admin-account.block_ip.add_ip_email') }}:</label>
                    <div class="col-md-4 col-sm-4">
                        <input class="form-control width-350" type="text" name="filter_name" value="{{$editData['filter_name']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('admin-account.block_ip.add_ban_expiration') }}:</label>
                    <div class="col-md-2">
                        <input  class="form-control datetimepicker"
                                type="text" name="expired_at" autocomplete="off" value="{{$editData['expired_at']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('admin-account.block_ip.add_reason_ban') }}:</label>
                    <div class="col-md-10 col-sm-9">
                        <textarea class="form-control" style="width: 350px" rows="5" name="description">{{$editData['description']}}</textarea>
                    </div>
                </div>

                <div class="text-right">
                    @if(!empty($editData['filter_name']))
                        <md-button ng-href="{{config('admin.directory')}}/account/block_ip" class="">{{ __('admin-account.block_ip.reset_button') }}</md-button>
                    @endif
                    <md-button class="md-primary md-raised" type="submit">{{ __('admin-account.block_ip.save_button') }}</md-button>
                </div>

            </form>

        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-account.block_ip.title_label') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th style="width: 200px">{{ __('admin-account.block_ip.table_filter') }}</th>
                    <th style="width: 190px">{{ __('admin-account.block_ip.add_ban_expiration') }}</th>
                    <th>{{ __('admin-account.block_ip.add_reason_ban') }}</th>
                    <th style="width: 70px"></th>
                </tr>

                </thead>
                <tbody>
                    @if(count($banned_list) > 0)
                        @foreach($banned_list as $banned)
                            <tr>
                                <td>{{$banned->filter_name}}</td>
                                <td>{{$banned->expired_at}}</td>
                                <td>{{$banned->description}}</td>
                                <td ng-controller="CommonController  as ctrl">

                                    <md-menu md-position-mode="target-right target">
                                        <md-button aria-label="Open demo menu" class="md-icon-button" ng-click="ctrl.openMenu($mdMenu, $event)">
                                            <md-icon aria-hidden="true" md-menu-origin md-font-set="material-icons">more_vert</md-icon>
                                        </md-button>

                                        <md-menu-content width="3">
                                            <md-menu-item class="menu-items-list">
                                                <md-button ng-href="?id={{$banned->id}}">
                                                    <span md-menu-align-target> {{ __('admin-account.block_ip.edit') }}</span>
                                                </md-button>
                                            </md-menu-item>
                                            <md-menu-item class="menu-items-list">
                                                <md-button href="?delete_id={{$banned->id}}">
                                                    <span md-menu-align-target style="color: orangered"> {{ __('admin-account.block_ip.unban') }}</span>
                                                </md-button>
                                            </md-menu-item>
                                        </md-menu-content>
                                    </md-menu>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4"><p align="center"><br />{{ __('admin-account.block_ip.list_empty') }}</p></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    {!! $banned_list->links() !!}
@endsection
