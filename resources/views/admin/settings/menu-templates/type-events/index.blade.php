@extends('admin.layouts.app')

@section('title', 'Типы почтовых и СМС событий')

@section('top-block')
    <md-button ng-href="type-events/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить тип</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.settings.menu-templates.type-events'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Дата обновления</th>
            <th>Тип</th>
            <th>Название</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($events) > 0)
            @foreach($events as $event)
                <tr>
                    <th>{{$event->updated_at}}</th>
                    <td> {{ $event->event_name }}</td>
                    <td>
                        {{ $event->name }}
                    </td>
                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="type-events/{{$event->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-tools.advantage.edit_button') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['type-events.destroy', $event->id] ]) !!}
                            {!! Form::submit(__('admin-tools.advantage.delete_button'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection