@extends('admin.layouts.app')

@section('title', 'Добавление нового события')

@section('breadcrumbs', Breadcrumbs::render('admin.settings.menu-templates.type-events.create'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/settings/menu-templates/type-events') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="event_name" class="col-sm-2 control-label">Тип события:</label>
                <div class="col-sm-5">
                    <input type="text" name="event_name" class="form-control">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Вид события:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="event_type" data-width="300">
                        <option value="0">Почтовые события</option>
                    </select>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Название:</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="description" class="col-sm-2 control-label">Описание:	</label>
                <div class="col-sm-8">
                    <textarea rows="5" name="description" id="ckeditor" class="form-control"></textarea>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/settings/menu-templates/type-events') }}">Отменить</md-button>
        </div>
    </form>
@endsection
