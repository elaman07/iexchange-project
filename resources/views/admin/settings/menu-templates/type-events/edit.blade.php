@extends('admin.layouts.app')

@section('title', 'Редактирование события '.$event->event_name)

@section('breadcrumbs', Breadcrumbs::render('admin.settings.menu-templates.type-events', $event))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/settings/menu-templates/type-events/'.$event->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="event_name" class="col-sm-2 control-label">Тип события:</label>
                <div class="col-sm-5">
                    {{ $event->event_name }}
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Вид события:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="event_type" data-width="300">
                        <option value="0" @if($event->event_type == 0) selected @endif>Почтовые события</option>
                    </select>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Название:</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control" value="{{ $event->name }}">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="description" class="col-sm-2 control-label">Описание:	</label>
                <div class="col-sm-8">
                    <textarea rows="5" id="ckeditor" name="description" class="form-control">{!! $event->description !!}</textarea>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/settings/menu-templates/type-events') }}">Отменить</md-button>
        </div>
    </form>
@endsection
