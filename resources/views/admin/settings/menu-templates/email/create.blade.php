@extends('admin.layouts.app')

@section('title', 'Добавление почтового шаблона')

@section('breadcrumbs', Breadcrumbs::render('admin.settings.menu-templates.email-template.create'))

@section('content')
    <div class="iex-custom-data-fields">
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/settings/menu-templates/email-template') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="event_name" class="col-sm-2 control-label">Тип почтового события:</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" name="id_event_type" data-live-search="true">
                        @foreach($events as $event)
                            <option value="{{ $event->id }}">[{{ $event->event_name }}] - {{$event->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Статус:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="status" data-width="300">
                        <option value="0">Активен</option>
                        <option value="0">Не активен</option>
                    </select>
                </div>
            </div>

            <hr />
            <div class="currency-field-label cfl-mt-25">Поля письма</div>
            <div class="clearfix"></div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Кому:</label>
                <div class="col-sm-5">
                    <input type="text" name="email_to" class="form-control">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Тема:</label>
                <div class="col-sm-5">
                    <input type="text" name="subject" class="form-control">
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/settings/menu-templates/email-template') }}">Отменить</md-button>
        </div>
    </form>
    </div>
@endsection
