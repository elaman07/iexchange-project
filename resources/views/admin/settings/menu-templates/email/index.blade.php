@extends('admin.layouts.app')

@section('title', 'Почтовые шаблоны')

@section('top-block')
    <md-button ng-href="email-template/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить шаблон</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.settings.menu-templates.email-template'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Тип почтового события</th>
            <th>Тема</th>
            <th>Дата обновления</th>
            <th>Статус</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($templates) > 0)
            @foreach($templates as $template)
                <tr>
                    <th>

                        <a href="email-template/{{$template->id}}/edit">
                            @if(isset($template->template_type_event))
                                [{{ $template->template_type_event->event_name }}] {{ $template->template_type_event->name }}
                            @else
                                <span class="text-danger">Тип не найден</span>
                            @endif
                            &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>{{ $template->subject }}</td>
                    <td>{{ $template->updated_at }}</td>
                    <td>
                        @if($template->status == 0)
                            <span class="label label-success">Активен</span>
                        @else
                            <span class="label label-warning">Не активен</span>
                        @endif
                    </td>
                    <td style="width: 50px;">
                        <div class="col-md-4">

                            <a class="text-danger" href="email-template/{{$template->id}}/delete">
                                <i class="fad fa-trash"></i>
                            </a>

{{--                            {!! Form::open(['method' => 'DELETE', 'route' => ['email-template.destroy', $template->id] ]) !!}--}}
{{--                            {!! Form::button('<i class="fa fa-fw fa-trash"></i>', ['class' => 'btn btn-danger btn-sm']) !!}--}}
{{--                            {!! Form::close() !!}--}}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
