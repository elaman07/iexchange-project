@extends('admin.layouts.app')

@section('title', 'Редактирование почтового шаблона #'.$template->id)

@section('breadcrumbs', Breadcrumbs::render('admin.settings.menu-templates.email-template.edit', $template))

@section('content')
    <div class="iex-custom-data-fields">
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/settings/menu-templates/email-template/'.$template->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="event_name" class="col-sm-2 control-label">Тип почтового события:</label>
                <div class="col-sm-5">
                    <a class="text-underline" href="{{ admin_base_path('/settings/menu-templates/type-events/'.$template->template_type_event->id.'/edit') }}">{{ $template->template_type_event->name }}</a> [{{ $template->template_type_event->event_name }}]
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="event_name" class="col-sm-2 control-label">Дата обновления:</label>
                <div class="col-sm-5">
                    {{ $template->updated_at }}
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Статус:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" name="event_type" data-width="300">
                        <option value="0" @if($template->status == 0) selected @endif>Активен</option>
                        <option value="1" @if($template->status == 1) selected @endif>Не активен</option>
                    </select>
                </div>
            </div>

            <hr />
            <div class="currency-field-label cfl-mt-25">Поля письма</div>
            <div class="clearfix"></div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Кому:</label>
                <div class="col-sm-5">
                    <input type="text" name="email_to" class="form-control" value="{{ $template->email_to }}">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Тема:</label>
                <div class="col-sm-5">
                    <input type="text" name="subject" class="form-control" value="{{ $template->subject }}">
                </div>
            </div>

            <hr />
            <div class="currency-field-label cfl-mt-25">Поля письма</div>
            <div class="clearfix"></div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="name" class="col-sm-2 control-label">Тема оформления:</label>
                <div class="col-sm-5">
                    <select class="selectpicker" name="template">
                        <option value="0" @if($template->template == 0) selected @endif>По умолчанию</option>
                        <option value="1" @if($template->template == 1) selected @endif>Свой шаблон</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="description" class="col-sm-2 control-label">Описание:	</label>
                <div class="col-sm-8">
                    <textarea rows="10" id="text_template" name="text_template" class="form-control">{!! $template->text_template !!}</textarea>

                    <hr />
                    <b>Доступные поля:</b><br /><br />

                    @php
                        echo preg_replace('/\[([^\[\]]++|(?R))*+\]/', '<a style="text-decoration: underline;" onclick=\'insertTextareaBBCode(this, "[$1]", "text_template")\' href="#">[$1]</a>', $template->template_type_event->description);
                    @endphp

                    <hr />
                    <ul>
                        @foreach(config('admin-settings.email_templates.interface') as $key => $value)
                            <li><a style="text-decoration: underline;" onclick='insertTextareaBBCode(this, "{{ $key }}", "text_template")' href='#'>{{ $key }}</a> - {{ $value }}</li>
                        @endforeach
                    </ul>


                </div>
            </div>
        </div>


        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/settings/menu-templates/email-template') }}">Отменить</md-button>
        </div>
    </form>
    </div>
@endsection
