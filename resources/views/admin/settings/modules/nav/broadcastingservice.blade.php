@php
    $array_main = [
    [
        'title' => __('Настройка параметров модуля'),
        'url' => 'general',
        'icon' => 'far fa-home',
        'name' => __('Настройки'),
        'desc' => __('Синхронизация со службами в режиме реального времени')
    ],
    [
        'title' => __('Настройка уведомлений'),
        'url' => 'notification',
        'icon' => 'far fa-bell',
        'name' => __('Уведомления'),
        'desc' => __('Настройка информации для уведомлений')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')
