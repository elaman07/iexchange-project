@php
    $array_main = [
    [
        'title' => __('Настройка валют'),
        'url' => 'settings_currency',
        'icon' => 'far fa-home',
        'name' => __('Настройка валют'),
        'desc' => __('Настройка параметров валют для главной страницы')
    ],
    [
        'title' => __('Настройка резерва'),
        'url' => 'settings_reserve',
        'icon' => 'far fa-wallet',
        'name' => __('Настройка резерва'),
        'desc' => __('Настройка параметров резерва')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')
