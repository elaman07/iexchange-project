@php
    $array_main = [
    [
        'title' => __('Настройка системы'),
        'url' => 'general',
        'icon' => 'far fa-home',
        'name' => __('Общие'),
        'desc' => __('Базовые настройки обменного пункта')
    ],
    [
        'title' => __('Настройки заявок'),
        'url' => 'tasks',
        'icon' => 'far fa-shopping-basket',
        'name' => __('Заявки'),
        'desc' => __('Настройки, для работы с заявками')
    ],
    [
        'title' => __('Настройки безопасности'),
        'url' => 'security',
        'icon' => 'far fa-lock',
        'name' => __('Безопасность'),
        'desc' => __('Параметры для увеличения безопасности обменника')
    ],
    [
        'title' => __('Настройки оптимизации'),
        'url' => 'optimization',
        'icon' => 'far fa-tachometer-fast',
        'name' => __('Оптимизация'),
        'desc' => __('Настройка оптимизации, для ускорения загрузки страниц')
    ],
    [
        'title' => __('Настройка уведомлений'),
        'url' => 'notification',
        'icon' => 'far fa-bell',
        'name' => __('Уведомление'),
        'desc' => __('Настройка уведомлений, чтобы всегда держать в курсе событий')
    ],
    [
        'title' => __('Настройка почты'),
        'url' => 'mail',
        'icon' => 'far fa-inbox',
        'name' => __('Почта'),
        'desc' => __('Параметры для синхронизации с почтовыми сервисами')
    ],
    [
        'title' => __('Настройка пользователей'),
        'url' => 'users',
        'icon' => 'far fa-user',
        'name' => __('Посетители'),
        'desc' => __('Настройка пользовательских параметров')
    ],
    [
        'title' => __('Дополнительные настройки'),
        'url' => 'other',
        'icon' => 'far fa-search-plus',
        'name' => __('Дополнительно'),
        'desc' => __('Прочие настройки для работы с обменным пунктом')
    ],
];
@endphp

@extends('admin.settings.modules.default_nav')
