@php
    $array_main = [
    [
        'title' => __('Настройка параметров модуля'),
        'url' => 'logs',
        'icon' => 'far fa-home',
        'name' => __('Настройки'),
        'desc' => __('Правила очистки логов')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')

