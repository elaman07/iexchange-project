@php
    $array_main = [
    [
        'title' => __('Настройка параметров модуля'),
        'url' => 'bslist',
        'icon' => 'far fa-home',
        'name' => __('Настройки'),
        'desc' => __('Интеграция с черным списком BestChange')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')
