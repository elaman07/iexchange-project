@php
    $array_main = [
    [
        'title' => __('Настройка параметров модуля'),
        'url' => 'general',
        'icon' => 'far fa-home',
        'name' => __('Настройки'),
        'desc' => __('Основные настройки работы с изображениями')
    ],
    [
        'title' => __('Настройка загрузки файлов на сервер'),
        'url' => 'cloud',
        'icon' => 'far fa-cloud-upload',
        'name' => __('Загрузка файлов'),
        'desc' => __('Настройка загрузки файлов на сервер')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')
