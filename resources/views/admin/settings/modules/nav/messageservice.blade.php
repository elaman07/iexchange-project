@php
    $array_main = [
    [
        'title' => __('Настройка параметров модуля'),
        'url' => 'general',
        'icon' => 'far fa-home',
        'name' => __('Настройки'),
        'desc' => __('Подключение СМС уведомлений')
    ],
    [
        'title' => __('Настройка уведомлений'),
        'url' => 'notification',
        'icon' => 'far fa-bell',
        'name' => __('Уведомление'),
        'desc' => __('Настройка уведомлений')
    ],
    [
        'title' => __('Настройка SMS шаблонов'),
        'url' => 'template',
        'icon' => 'far fa-camera',
        'name' => __('Шаблоны'),
        'desc' => __('Настройка SMS шаблонов для уведомлений')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')
