@php
    $array_main = [
    [
        'title' => __('Настройка параметров модуля'),
        'url' => 'chat',
        'icon' => 'far fa-home',
        'name' => __('Настройки'),
        'desc' => __('Подключение онлайн чата')
    ], [
        'title' => __('Настройка чата в заявках'),
        'url' => 'chat-order',
        'icon' => 'far fa-user',
        'name' => __('Настройки чата в заявках'),
        'desc' => __('Подключение онлайн чата к заявкам')
    ], [
        'title' => __('Опции для JivoSite'),
        'url' => 'chat-jivosite',
        'icon' => 'fab fa-rocketchat',
        'name' => __('Опции для JivoSite'),
        'desc' => __('Доп. опции для работы с чатом')
    ]
];
@endphp
@extends('admin.settings.modules.default_nav')
