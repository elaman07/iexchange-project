@foreach($array_main as $value)
    <li class="setting-sidenav-menu-item">
        <a layout="row" href="#{{ $value['url'] }}" data-tab-title="{{ $value['title'] }}" data-custom-tab="tab" role="tab" data-toggle="tab">
            <i flex="5" class="{{ $value['icon'] }}"></i>
            <div class="sss tip">
                <div class="setting-sidenav-menu-item-title"> {{ $value['name'] }} </div>
                <div class="setting-sidenav-menu-item-text"> {{ $value['desc'] }} </div>
            </div>
        </a>
    </li>
@endforeach
