<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="general">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Секретный ключ</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите секретный ключ для безопасности webhook.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="TELEGRAM_SECURITY_PREFIX" name="TELEGRAM_SECURITY_PREFIX"  value="{{ config('telegram.prefix') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отображать ссылку на Telegram бота:</h6>
                    <span class="text-muted text-size-small hidden-xs">Если включено, то на главной странице, внизу блока для выбора направлений появится ссылка на Telegram бот для обменов</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_telegram_bot_block" name="is_enabled_telegram_bot_block" type="checkbox" value="1" @if(iEXSetting('is_enabled_telegram_bot_block') == 1) checked @endif />
                        <label for="is_enabled_telegram_bot_block" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Ссылка на Telegram Bot</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите ссылку на telegram бота.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="link_telegram_bot_block" name="link_telegram_bot_block"  value="{{ iEXSetting('link_telegram_bot_block') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Заголовок:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите заголовок для Telegram на главной странице</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="telegram_block_title" keyName="telegram_block_title" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Краткое описание:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите краткое описание для Telegram на главной странице</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="telegram_block_description" keyName="telegram_block_description" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Стиль отображения</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите стиль отображения блок для Telegram обменов</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="telegram_block_style">
                        <option value="0" @if(iEXSetting('telegram_block_style') == 0) selected @endif>Стандартный</option>
                        <option value="1" @if(iEXSetting('telegram_block_style') == 1) selected @endif>Красивый</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Название кнопки:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите название кнопки telegram бота, которая будет отображена на главном</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="telegram_block_button" keyName="telegram_block_button" />
                </td>
            </tr>


        </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="client_bot">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Активировать клиентский бот:</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Если включено, то через бот можно принимать заявки.<br />
                        <span style="color: red;">Внимание!!!</span> Перед тем как включить данную функцию, убедитесь что все настроено
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enable_telegram_exchange" name="is_enable_telegram_exchange" type="checkbox" value="1" @if(iEXSetting('is_enable_telegram_exchange') == 1) checked @endif />
                        <label for="is_enable_telegram_exchange" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Токен доступа Telegram-бота:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите токен доступа, которую получили в боте</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="TELEGRAM_BOT_TOKEN" name="TELEGRAM_BOT_TOKEN" value="{{ config('telegram.bots.iex.token') }}">
                    @if(!empty(config('telegram.bots.iex.token')))
                        <a href="?service_api=telegram&type_api_service=register_client_bot">Зарегистрировать Webhook бота</a>
                    @endif
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Имя Telegram бота</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите имя которую создали в BotFather (example_bot)</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="TELEGRAM_BOT_NAME" name="TELEGRAM_BOT_NAME" value="{{ config('telegram.bots.iex.username') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Список заблокированных клиентов:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите через "," ID или USERNAME клиента, которого хотите заблокировать в Telegram Bot</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="telegram_bot_banned_ids" name="telegram_bot_banned_ids" value="{{ iEXSetting('telegram_bot_banned_ids') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Тип блокировки клиентов</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите, по какому типу желаете блокировать клиентов</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="telegram_bot_banned_type" data-width="300">
                        <option value="id" @if(iEXSetting('telegram_bot_banned_type') == 'id') selected @endif>По ID</option>
                        <option value="username" @if(iEXSetting('telegram_bot_banned_type') == 'username') selected @endif>По имени пользователя</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">В разделе "Отзывы" включить возможность добавлять отзыв</h6>
                    <span class="text-muted text-size-small hidden-xs">Еслии включена функция, то в разделе "Отзывы" появится возможность для добавления новых комментариев</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="telegram_bot_client_is_create_review" name="telegram_bot_client_is_create_review" type="checkbox" value="1" @if(iEXSetting('telegram_bot_client_is_create_review') == 1) checked @endif />
                        <label for="telegram_bot_client_is_create_review" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Текст для команды "О боте"</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите описание для данной команды</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="telegram_bot_client_about" rows="6">{{ iEXSetting('telegram_bot_client_about') }}</textarea>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Текст для команды "Отзывы"</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите описание для данной команды</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="telegram_bot_client_review" rows="6">{{ iEXSetting('telegram_bot_client_review') }}</textarea>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Текст для команды "Поддержка"</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите описание для данной команды</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="telegram_bot_client_support" rows="6">{{ iEXSetting('telegram_bot_client_support') }}</textarea>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Текст для команды "Старт"</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите описание для данной команды</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="telegram_bot_client_start" rows="6">{{ iEXSetting('telegram_bot_client_start') }}</textarea>
                </td>
            </tr>

        </table>
    </div>
    <div role="tabpanel" class="tab-pane" id="admin_bot">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Токен доступа Telegram-бота:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите токен доступа, которую получили в боте</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="ADMIN_TELEGRAM_BOT_TOKEN" name="ADMIN_TELEGRAM_BOT_TOKEN" value="{{ config('telegram.bots.admin.token') }}">
                    @if(!empty(config('telegram.bots.admin.token')))
                        <a href="?service_api=telegram&type_api_service=register_admin_bot">Зарегистрировать Webhook бота</a>
                    @endif
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Имя Telegram бота</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите имя которую создали в BotFather (example_bot)</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="ADMIN_TELEGRAM_BOT_NAME" name="ADMIN_TELEGRAM_BOT_NAME" value="{{ config('telegram.bots.admin.username') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">User ID администраторов</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите список ID администраторов для доступа для telegram боту, Если у вас несколько ID, добавьте через знак ",". Узнать USER ID можно используя бот <a target="_blank" href="https://t.me/getmyid_bot">https://t.me/getmyid_bot</a> </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="telegram_chat_id" name="telegram_chat_id" value="{{ iEXSetting('telegram_chat_id') }}">
                </td>
            </tr>

        </table>
    </div>
    <div role="tabpanel" class="tab-pane" id="notice_bot">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">ID Канала</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите ID канала в Telegram. Этот пункт необходим если предпрочитаете получать уведомления в канал.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="TELEGRAM_CHANNEL_ID" name="TELEGRAM_CHANNEL_ID" value="{{ config('telegram.channel_id') }}">

                    <a href="?service_api=telegram&type_api_service=get_id_channel">Получить ID канала</a>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Способ уведомлений</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка способ отправки уведомлений</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="telegram_type_notification">
                        <option value="0" @if(iEXSetting('telegram_type_notification') == 0) selected @endif>Бот</option>
                        <option value="1" @if(iEXSetting('telegram_type_notification') == 1) selected @endif>Канал</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Вид уведомлений</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        <b>Компактный</b> - этот вид отсылает только короткие уведомления<br />
                        <b>Расширенный</b> - используя этот способ в Telegram канал будет отсылаться подробная информация о новых заявках.
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="telegram_view_notification">
                        <option value="0" @if(iEXSetting('telegram_view_notification') == 0) selected @endif>Компактный</option>
                        <option value="1" @if(iEXSetting('telegram_view_notification') == 1) selected @endif>Расширенный</option>
                    </select>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Токен доступа Telegram-бота:</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите токен доступа, которую получили в боте</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="NOTICE_TELEGRAM_BOT_TOKEN" name="NOTICE_TELEGRAM_BOT_TOKEN" value="{{ config('telegram.bots.notice.token') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Имя Telegram бота</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите имя которую создали в BotFather (example_bot)</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="NOTICE_TELEGRAM_BOT_NAME" name="NOTICE_TELEGRAM_BOT_NAME" value="{{ config('telegram.bots.notice.username') }}">
                </td>
            </tr>

        </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="notification">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить уведомление в Telegram</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, позволяет отправлять уведомления в telegram bot</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="ENABLE_TELEGRAM" name="ENABLE_TELEGRAM" type="checkbox" value="1" @if(config('telegram.enable') == 1) checked @endif />
                        <label for="ENABLE_TELEGRAM" class="label-success"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить отправлять статистику в Telegram</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', каждый день в 00:01, администраторам в telegram будет отправляться соответствующая статистика обменов.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_tg_send_stat" name="is_tg_send_stat" type="checkbox" value="1" @if(iEXSetting('is_tg_send_stat') == 1) checked @endif />
                        <label for="is_tg_send_stat" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать в Telegram уведомление для операторов</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при создании новой заявки на сайте, операторам в telegram будет отправляться соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="enable_tg_notify_operator" name="enable_tg_notify_operator" type="checkbox" value="1" @if(iEXSetting('enable_tg_notify_operator') == 1) checked @endif />
                        <label for="enable_tg_notify_operator" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление в процессе создания заявки</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Если 'Включено', то после в процессе создания заявки, клиент получит соответствующее уведомление. Уведомление придет после на кнопки "Перейти к оплате"
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_notify_process_create_order" name="is_notify_process_create_order" type="checkbox" value="1" @if((int)iEXSetting('is_notify_process_create_order') == 1) checked @endif />
                        <label for="is_notify_process_create_order" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление о сбоях при автовыплате</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при неудачных попытках автовыплаты средств клиентам, в Telegram будет отправляться соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_notify_failed_autopayment" name="is_notify_failed_autopayment" type="checkbox" value="1" @if(iEXSetting('is_notify_failed_autopayment') == 1) checked @endif />
                        <label for="is_notify_failed_autopayment" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление в случае блокировок клиента</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то в случае блокировок клиента из-за неудачных попыток ввода дополнительного кода, в Telegram будет отправляться соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_notify_telegram_blocked_client" name="is_notify_telegram_blocked_client" type="checkbox" value="1" @if(iEXSetting('is_notify_telegram_blocked_client') == 1) checked @endif />
                        <label for="is_notify_telegram_blocked_client" class="label-success"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление при получении новых заявок на верификацию счетов</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то при создании новой заявки на верификацию банковских карт, в Telegram будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="telegram_order_verification_card" name="telegram_order_verification_card" type="checkbox" value="1" @if(iEXSetting('telegram_order_verification_card') == 1) checked @endif />
                        <label for="telegram_order_verification_card" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление при получении новых заявок на выплат</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при создании новой заявки на выплату бонусных вознаграждений, в Telegram будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="telegram_order_withdrawal" name="telegram_order_withdrawal" type="checkbox" value="1" @if(iEXSetting('telegram_order_withdrawal') == 1) checked @endif />
                        <label for="telegram_order_withdrawal" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить отправить в Telegram номер счета получателя</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', в Telegram будет отправлен номер счета получателя.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="telegram_to_shot_notification" name="telegram_to_shot_notification" type="checkbox" value="1" @if((int)iEXSetting('telegram_to_shot_notification') == 1) checked @endif />
                        <label for="telegram_to_shot_notification" class="label-success"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Вид отправки номера счета получателя</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', в Telegram будет отправлен номер счета получателя.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="interface_to_shot_notification" data-width="300">
                        <option value="allow_id" @if(iEXSetting('interface_to_shot_notification') == 'allow_id') selected @endif>Отправить с ID заявки</option>
                        <option value="to_shot" @if(iEXSetting('interface_to_shot_notification') == 'to_shot') selected @endif>Отправить только номер счета</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить оповещение о сбоях получения курсов</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то при сбоях получения курсов в Telegram будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="telegram_failed_update_courses" name="telegram_failed_update_courses" type="checkbox" value="1" @if(iEXSetting('telegram_failed_update_courses') == 1) checked @endif />
                        <label for="telegram_failed_update_courses" class="label-info"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление об успешных авторизациях в админпанели</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то после успешной авторизации в админпанели в Telegram будет отправлено соответствующие уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_successful_admin_auth" name="is_successful_admin_auth" type="checkbox" value="1" @if((int)iEXSetting('is_successful_admin_auth') == 1) checked @endif />
                        <label for="is_successful_admin_auth" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление о неудачных авторизациях в админпанели</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то после неудачных авторизациях в админпанели в Telegram будет отправлено соответствующие уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_failed_admin_auth" name="is_failed_admin_auth" type="checkbox" value="1" @if((int)iEXSetting('is_failed_admin_auth') == 1) checked @endif />
                        <label for="is_failed_admin_auth" class="label-primary"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление при получении 1-го подтверждения</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то при получении 1-го подтверждения от сети blockchain, в Telegram будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_notify_first_confirm_blockchain" name="is_notify_first_confirm_blockchain" type="checkbox" value="1" @if((int)iEXSetting('is_notify_first_confirm_blockchain') == 1) checked @endif />
                        <label for="is_notify_first_confirm_blockchain" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление о совершенной автовыплате</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то при успешной автовыплаты в Telegram будет отправлено соответствующее уведомление</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_order_notify_auto_payment" name="is_order_notify_auto_payment" type="checkbox" value="1" @if((int)iEXSetting('is_order_notify_auto_payment') == 1) checked @endif />
                        <label for="is_order_notify_auto_payment" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление клиентам о статусах заявок</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', и если  в личном кабинете клиент указал ID Чата в telegram, то в этом случае будет отправлено соответствующее уведомление о статусах заявок.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_notify_order_telegram_client" name="is_notify_order_telegram_client" type="checkbox" value="1" @if((int)iEXSetting('is_notify_order_telegram_client') == 1) checked @endif />
                        <label for="is_notify_order_telegram_client" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление в случае проблем с направлениями</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Eсли 'Включено', и если при обновлении курсов с направлениями возникнут проблемы, в Telegram будет отправлено соответствующее уведомление.
                        <br />
                        Рекомендуется включить данную опцию
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="enable_telegram_courses_fail" name="enable_telegram_courses_fail" type="checkbox" value="1" @if((int)iEXSetting('enable_telegram_courses_fail') == 1) checked @endif />
                        <label for="enable_telegram_courses_fail" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать Telegram уведомление в случае если у заявка была отложено</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Eсли 'Включено', и в случае если заявка по какой та причине будет отложено, в Telegram будет отправлено соответствующее уведомление.
                        <br />
                        Рекомендуется включить данную опцию
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_notify_order_telegram_postponed" name="is_notify_order_telegram_postponed" type="checkbox" value="1" @if((int)iEXSetting('is_notify_order_telegram_postponed') == 1) checked @endif />
                        <label for="is_notify_order_telegram_postponed" class="label-primary"></label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
