
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general">
            <table class="table table-2 table-striped">
                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Название сайта') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите название вашего обменника</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <x-forms.language.input tabName="sitename" keyName="sitename" />
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Префикс для Cookie') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Укажите имя префикса для названия cookie (без точек и пробелов)') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" id="SESSION_COOKIE" name="SESSION_COOKIE" value="{{ config('session.cookie')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Режим вывода ошибок (error_reporting)') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Выберите уровень обработки ошибок. Данный параметр определяет типы ошибок, о которых PHP информирует выводом текстового сообщения в окно браузера. Мы рекомендует не менять это значение') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! Form::select('ERROR_REPORTING', config('app.error_reporting_list'), config('app.error_reporting'),
                                ['class' => 'form-control selectpicker', 'data-width' => '300px', 'data-live-search' => 'true']) !!}
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Домашняя страница сайта') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Укажите имя основного домена на котором располагается ваш сайт. Например: http://site.com/ Внимание, наличие слэша на конце в имени домена обязательно') }}.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" id="APP_URL" name="APP_URL" value="{{ config('app.url') }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Краткое название для заголовка') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Укажите краткое название вашего сайта. (Например: Крипто обменник)') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <x-forms.language.input tabName="sitename_desc" keyName="sitename_desc" />
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Часовой пояс') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            {{ __('Выберете часовой пояс, по которому будет работать ваш сайт и сервер. Текущее время сервера с учетом часового пояса') }}: {{ \Carbon\Carbon::now()->format('d.m.Y H:i:s') }}
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! \Timezones::create('APP_TIMEZONE', config('app.timezone')) !!}
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Используемый язык') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Выберите язык, который будет использоваться при работе с системой') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! Form::select('APP_LOCALE', config('app.all_locale'), config('app.locale'),
                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'data-live-search' => 'true']) !!}
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Автоматическое определение языка') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __("Если 'Включено', то язык будет определен автоматически системой") }}.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_language_detection" name="is_language_detection" type="checkbox" value="1" @if(iEXSetting('is_language_detection') == 1) checked @endif />
                            <label for="is_language_detection" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Отключить опцию смены языка') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __("Если 'Включено', то у клиентов не будет возможности изменить язык сайта") }}.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_language_deactivation" name="is_language_deactivation" type="checkbox" value="1" @if(iEXSetting('is_language_deactivation') == 1) checked @endif />
                            <label for="is_language_deactivation" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Авто-определение языка авторизованных пользователей') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __("Если 'Включено', и если пользователь авторизован, то будет загружаться тот язык, которая используется пользователем") }}.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_language_selected_user" name="is_language_selected_user" type="checkbox" value="1" @if(iEXSetting('is_language_selected_user') == 1) checked @endif />
                            <label for="is_language_selected_user" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Описание (Description) сайта') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Краткое описание, не более 200 символов') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <x-forms.language.textarea tabName="description" keyName="description" />
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Ключевые слова (Keywords) для сайта') }}:</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Введите через запятую основные ключевые слова для вашего сайта') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <x-forms.language.textarea tabName="keywords" keyName="keywords" />
                    </td>
                </tr>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="tasks">
            <table class="table table-2 table-striped">

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Публичный ID заявки для клиентов') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Выберите из списка основную ID заявки которая будут видеть клиенты') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="client_id_type_for_order" data-width="300px" data-live-search="true">
                            <option value="0" @if(iEXSetting('client_id_type_for_order') == 0) selected @endif>{{ __('По ID Заявки (стандартный)') }}</option>
                            <option value="1" @if(iEXSetting('client_id_type_for_order') == 1) selected @endif>{{ __('По алгоритму (новая)') }}</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Разрешить записывать данные заявок в файл') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Если данная функция включена, то в данные заявок будут записываться в файл на сервере') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="is_save_order_data_to_file">
                            <option value="0" @if(iEXSetting('is_save_order_data_to_file') == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if(iEXSetting('is_save_order_data_to_file') == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Включить автогенерацию email адресов') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('В случае включения данной настройки, у новых клиентов будет возможность автоматически сгенерировать email адрес') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_enable_autogen_order_email" name="is_enable_autogen_order_email" type="checkbox" value="1" @if(iEXSetting('is_enable_autogen_order_email') == 1) checked @endif />
                            <label for="is_enable_autogen_order_email" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Отключить поле для ввода e-mail адреса') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            {{ __('В случае включения данной настройки, у новых пользователей не будет возможности указать email адрес, клиент зарегистрируете через рандомный email') }}.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_disabled_email_field_optional" name="is_disabled_email_field_optional" type="checkbox" value="1" @if(iEXSetting('is_disabled_email_field_optional') == 1) checked @endif />
                            <label for="is_disabled_email_field_optional" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Отключить подсчет кол-во заявок') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('В случае включения данной настройки, счетчик заявок не будет отображаться') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_hidden_order_count" name="is_hidden_order_count" type="checkbox" value="1" @if(iEXSetting('is_hidden_order_count') == 1) checked @endif />
                            <label for="is_hidden_order_count" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Интервал обновления курсов') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Укажите в секундах интервал автоматического обновления курсов на главной странице. Рекомендуемое значение не ниже 15 сек') }}.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="interval_rates" value="{{ iEXSetting('interval_rates', 15) }}" class="form-control" style="width:300px;">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Таймер обратного отсчета в окне "Подтверждение обмена"') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Укажите в секундах таймер после которого будет активна кнопка подтверждения заявки. Рекомендуемое значение: 3 сек') }}.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="interval_rates_confirm_dialog" value="{{ iEXSetting('interval_rates_confirm_dialog', 3) }}" class="form-control" style="width:300px;">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Разрешить передавать заявку другому оператору') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __("Если 'Включено', то у оператора будет возможность передать заявку другому оператору") }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_allow_transfer_operator" name="is_allow_transfer_operator" type="checkbox" value="1" @if(iEXSetting('is_allow_transfer_operator') == 1) checked @endif />
                            <label for="is_allow_transfer_operator" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Активность оператора в заявке') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Укажите количество минут в течение которых заявка может находится в работе у оператора. 0 - Отключает ограничение') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width:300px;" id="order_active_operator" name="order_active_operator" value="{{ iEXSetting('order_active_operator', 0)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Макс. количество раз, которое можно нажать "Оплатить и завершить"</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите максимальное количество раз, которое можно нажать на кнопку "Оплатить и завершить".</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="max_num_autopay_button" value="{{ iEXSetting('max_num_autopay_button', 0) }}" class="form-control"  style="width:300px;">
                        <div class="text-muted" style="font-size: 12px;margin-top: 5px">0 - отключает ограничение</div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить авто-возврат средств</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной функцию, и в случае если вы отклоняете заявку с причинов "Возврат", клиент автоматически получит свои средства обратно</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_order_auto_refund" name="is_order_auto_refund" type="checkbox" value="1" @if(iEXSetting('is_order_auto_refund') == 1) checked @endif />
                            <label for="is_order_auto_refund" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Автоматические баны за мошеннические заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">Вы можете включить автоматический бан клиентов за мошеннические заявки. Если при отклонении заявки оператор выбирает пункт "Мошенническая заявка" то клиент в первый раз банится на 1 день если повторится ситуация на 2 недели. 3 попытка создать мошенническую заявку окажется в перманентом бане.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="BAN_CHEATER_USER" name="BAN_CHEATER_USER" type="checkbox" value="1" @if(config('admin.ban_cheater_user') == 1) checked @endif />
                            <label for="BAN_CHEATER_USER" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить добавить мошенников в черный список Bestchange</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной функцию, система автоматически будет добавлять мошенников в черный список bestchange</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="bestchange_blacklist_add" name="bestchange_blacklist_add" type="checkbox" value="1" @if(iEXSetting('bestchange_blacklist_add') == 1) checked @endif />
                            <label for="bestchange_blacklist_add" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить заморозить мошеннические заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной функцию, клиенты проверяются в списке bestchange на предмет мошенничества, если клиент найдется в базе то заявка будет заморожено.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_freeze_scam_order" name="is_freeze_scam_order" type="checkbox" value="1" @if(iEXSetting('is_freeze_scam_order') == 1) checked @endif />
                            <label for="is_freeze_scam_order" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить блокировать клиентов за спам заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной функцию, клиенты которые создают спам заявки будут заблокированы на 1 день</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_blocked_spam_order" name="is_blocked_spam_order" type="checkbox" value="1" @if(iEXSetting('is_blocked_spam_order') == 1) checked @endif />
                            <label for="is_blocked_spam_order" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Автоматически занести IP адрес и Email в черный список</h6>
                        <span class="text-muted text-size-small hidden-xs">Если данная функция включена, то у оператора есть возможность занести данные клиента в черный список. В черный список заносятся в случае если оператор выбрал пункт "Ваша заявка признана мошеннической".</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="allow_order_blacklist" name="allow_order_blacklist" type="checkbox" value="1" @if(iEXSetting('allow_order_blacklist', 0) == 1) checked @endif />
                            <label for="allow_order_blacklist" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-Mail уведомление администратору при получении новых заявок</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при создании новой заявки с сайта, на E-Mail указанный в настройках почты будет отправлено соответствующее уведомление (Сообщение будут отправлена администраторам).</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_mail_notify_order_manager" name="is_mail_notify_order_manager" type="checkbox" value="1" @if(iEXSetting('is_mail_notify_order_manager') == 1) checked @endif />
                            <label for="is_mail_notify_order_manager" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-Mail уведомление в случае восстановление заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при восстановление удаленной заявки, на E-Mail клиента будет отправлено соответствующее уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_notify_order_restore" name="is_notify_order_restore" type="checkbox" value="1" @if(iEXSetting('is_notify_order_restore') == 1) checked @endif />
                            <label for="is_notify_order_restore" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-Mail уведомление в случае отложенной заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при отложенной заявки, на E-Mail клиента будет отправлено соответствующее уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_notify_order_defer" name="is_notify_order_defer" type="checkbox" value="1" @if(iEXSetting('is_notify_order_defer') == 1) checked @endif />
                            <label for="is_notify_order_defer" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-Mail уведомление в случае занесения реквизитов в черный список</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то в случае добавление реквизитов клиента в черный список, на E-Mail указанный в настройках будет отправлено соответствующее уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_order_shot_black_list" name="is_order_shot_black_list" type="checkbox" value="1" @if(iEXSetting('is_order_shot_black_list', 0) == 1) checked @endif />
                            <label for="is_order_shot_black_list" class="label-primary"></label>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Ограничить отображение чека с другого IP адреса</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то чек заявки будет доступен только клиенту который создал заявку</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="disable_check_display" name="disable_check_display" type="checkbox" value="1" @if(iEXSetting('disable_check_display') == 1) checked @endif />
                            <label for="disable_check_display" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить пересчет заявки в случае окончания установленной времени</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной настройки, и в случае если клиент оплачивает уже после окончания макс. установленной времени, заявка будет автоматически пересчитано по новому курсу.
                            <br /> Эта функция необходимо для оплаты через мерчант.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_recount_to_merchant" name="is_recount_to_merchant" type="checkbox" value="1" @if(iEXSetting('is_recount_to_merchant') == 1) checked @endif />
                            <label for="is_recount_to_merchant" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-mail уведомление после пересчета для мерчантов</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то после переадресации со страницы платежной системы, и если сумма отправляемая изменена, клиент получит соответствующее уведомление на E-mail.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_recount_to_merchant_notify" name="is_recount_to_merchant_notify" type="checkbox" value="1" @if(iEXSetting('is_recount_to_merchant_notify') == 1) checked @endif />
                            <label for="is_recount_to_merchant_notify" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Дополнительная проверка заявки (Coin -> Coin)</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то в процессе создания заявки будет проведена дополнительная проверка актуальности курса из CoinMarketCap. Проверятся в случае если обмен совершается между криптовалютами.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_extended_order_check" name="is_extended_order_check" type="checkbox" value="1" @if(iEXSetting('is_extended_order_check') == 1) checked @endif />
                            <label for="is_extended_order_check" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отображаемые заявки в личном кабинете</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка статусы заявок которые будут доступны клиенту</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! Form::select('displayed_statuses[]', $statuses, explode(',', iEXSetting('displayed_statuses')),
                            ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '300px', 'data-live-search' => 'true']) !!}
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Определить новичка:</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка пункты по которым необходимо определить новичка</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! Form::select('order_identify_newbie[]', $typeDefineBeginner, explode(',', iEXSetting('order_identify_newbie')),
                               ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '300px', 'data-live-search' => 'true']) !!}
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить автовыплаты</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то после получения средств через мерчант клиенту сразу будут переведены средства.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_enabled_autopayment" name="is_enabled_autopayment" type="checkbox" value="1" @if(iEXSetting('is_enabled_autopayment') == 1) checked @endif />
                            <label for="is_enabled_autopayment" class="label-info"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить лог мерчантов</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то все действия связанные с мерчантом будут записываться в лог.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_enabled_log_merchant" name="is_enabled_log_merchant" type="checkbox" value="1" @if(iEXSetting('is_enabled_log_merchant') == 1) checked @endif />
                            <label for="is_enabled_log_merchant" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Если не заполнена "инструкция к оплате" в настройках мерчанта</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите из списка время, в течении которого клиенту необходимо произвести оплату и отправить заявку на обработку</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker form-control" name="type_instruction_merchant">
                            <option value="0" @if(iEXSetting('type_instruction_merchant') == 0) selected @endif>Ничего не выводить</option>
                            <option value="1" @if(iEXSetting('type_instruction_merchant') == 1) selected @endif>Выводить Инструкцию к оплате из направлений обмена</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить лог статусов заявок</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то все изменения статусов будут записываться в лог.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_enabled_log_status" name="is_enabled_log_status" type="checkbox" value="1" @if(iEXSetting('is_enabled_log_status') == 1) checked @endif />
                            <label for="is_enabled_log_status" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить регулярное архивирование заказов</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то будут заявки будут архивироваться автоматически.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_auto_archived" name="is_auto_archived" type="checkbox" value="1" @if(iEXSetting('is_auto_archived') == 1) checked @endif />
                            <label for="is_auto_archived" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить отклонять заявки в случае неполной оплаты через мерчант</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то в случае если клиент оплатит через мерчант неполную сумму, заявка будет отклонена (Рекомендую включить данную опцию).</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_disabled_order_merchant" name="is_disabled_order_merchant" type="checkbox" value="1" @if(iEXSetting('is_disabled_order_merchant') == 1) checked @endif />
                            <label for="is_disabled_order_merchant" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Через какое время отправлять в архив (дни):</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите количество дней после которого заявка будет архививироано</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width:300px;" id="archived_days" name="archived_days" value="{{ iEXSetting('archived_days', 100)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">В каких статусах отправлять в архив</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка статусы заявок которые будут архивированы</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! Form::select('archived_statuses[]', $statuses, explode(',', iEXSetting('archived_statuses')),
                            ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '300px', 'data-live-search' => 'true']) !!}
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Время актуальности заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                    Укажите из списка время, в течении которого клиенту необходимо произвести оплату и отправить заявку на обработку
                                </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="max_time_task" data-width="300px" data-live-search="true">
                            @foreach($max_time_task as $key => $value)
                                <option value="{{ $key }}"  @if(iEXSetting('max_time_task') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Время выполнение заявки</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                    Укажите из списка время, в течении которого администратор должен обработать заявку и перевести средства клиенту.
                                </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="lead_time_task" data-width="300px" data-live-search="true">
                            @foreach($lead_time_task as $key => $value)
                                <option value="{{ $key }}" @if(iEXSetting('lead_time_task') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>

{{--                <tr>--}}
{{--                    <td class="col-xs-6 col-sm-6 col-md-7">--}}
{{--                        <h6 class="media-heading text-semibold">Статусы заявок для операторов</h6>--}}
{{--                        <span class="text-muted text-size-small hidden-xs">--}}
{{--                            Выберите доступные статусы для операторов. Это функция необходимо в случае если вы не хотите обращать внимание на неактуальные заявки.--}}
{{--                        </span>--}}
{{--                    </td>--}}
{{--                    <td class="col-xs-6 col-sm-6 col-md-5">--}}
{{--                        {!! Form::select('order_priority[]', $statuses, explode(',', iEXSetting('order_priority')),--}}
{{--                            ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '300px', 'data-live-search' => 'true']) !!}--}}
{{--                    </td>--}}
{{--                </tr>--}}

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить зачислить в резерв</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', у оператора будет возможность заранее пополнить резерв на сумму которую отдал клиент. Функция будет доступна при просмотре выбранной заявки.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="allow_transfer_reserve" name="allow_transfer_reserve" type="checkbox" value="1" @if(iEXSetting('allow_transfer_reserve') == 1) checked @endif />
                            <label for="allow_transfer_reserve" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Минимальная сумма зачисления в резерв</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите минимальную сумму в рублях, после которого у оператора будет возможность зачислить полученную сумму в резерв.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 300px;text-align: center;" name="give_transfer_reserve" value="{{iEXSetting('give_transfer_reserve', 5000)}}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Максимальное количество заявок для одного клиента</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите максимальное количество заявой которую может создать один пользователь. 0 - отключает ограничение</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 300px;text-align: center;" name="max_num_order_user" value="{{iEXSetting('max_num_order_user', 0)}}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Макс. кол-во заявок для одного клиента за час</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите максимальное количество заявой которую может создать один пользователь за час. 0 - отключает ограничение</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 300px;text-align: center;" name="max_num_order_user_hour" value="{{iEXSetting('max_num_order_user_hour', 0)}}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Макс. кол-во заявок для одного клиента в день</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите максимальное количество заявой которую может создать один пользователь в течении дня. 0 - отключает ограничение</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 300px;text-align: center;" name="max_num_order_user_day" value="{{iEXSetting('max_num_order_user_day', 0)}}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить номер телефона</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Если 'Включено', то при создании заявки клиент обязательно должен указать номер телефона
                                             </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_order_phone_number" name="is_order_phone_number" type="checkbox" value="1" @if(iEXSetting('is_order_phone_number') == 1) checked @endif />
                            <label for="is_order_phone_number" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Маска для номера телефона</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите маску для номера телефона.<br/> Пример: +0 (000) 000-00-00-0000</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="angular_phone_mask" class="form-control" style="width: 300px" value="{{ iEXSetting('angular_phone_mask') }}"/>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отобржение номера телефона</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Выберите из списка позицию, где необходимо отображать поле ввода номера телефона
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="btn-group colors" data-toggle="buttons">
                            <label class="btn btn-default  @if(iEXSetting('pos_order_phone_number') == 0) active @endif">
                                <input type="radio" name="pos_order_phone_number" value="0" autocomplete="off" @if(iEXSetting('pos_order_phone_number') == 0) checked @endif> Сверху
                            </label>
                            <label class="btn btn-default  @if(iEXSetting('pos_order_phone_number') == 1) active @endif">
                                <input type="radio" name="pos_order_phone_number" value="1" autocomplete="off"  @if(iEXSetting('pos_order_phone_number') == 1) checked @endif> Снизу
                            </label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отобржение e-mail адреса</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Выберите из списка позицию, где необходимо отображать поле ввода e-mail адреса
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">

                        <div class="btn-group colors" data-toggle="buttons">
                            <label class="btn btn-default  @if(iEXSetting('pos_order_email') == 0) active @endif">
                                <input type="radio" name="pos_order_email" value="0" autocomplete="off" @if(iEXSetting('pos_order_email') == 0) checked @endif> Сверху
                            </label>
                            <label class="btn btn-default  @if(iEXSetting('pos_order_email') == 1) active @endif">
                                <input type="radio" name="pos_order_email" value="1" autocomplete="off"  @if(iEXSetting('pos_order_email') == 1) checked @endif> Снизу
                            </label>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="optimization">
            <table class="table table-2 table-striped">

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кеширование на сайте</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Кеширование существенно сокращает нагрузку на сервер, сводя количество запросов к минимуму
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache" name="ENABLE_CACHE" type="checkbox" value="1" @if(config('cache.enable_cache') == true) checked @endif />
                            <label for="enable_cache" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Принудительная очистка кэша (Для направлений):</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Вы можете задать интервал в минутах, после которого будет осуществляться принудительная очистка кэша. Например задав 10, кеш будет очищаться каждые 10 минут.
                            Если задать 0, то очистка кэша будет производится автоматически по мере изменения информации в базе данных. Настоятельно рекомендуем не указывать больше 10 минут.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 200px;text-align: center" id="CLEAR_CACHE_EXCHANGE" name="CLEAR_CACHE_EXCHANGE" value="{{ config('cache.clear_cache_exchange')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Принудительная очистка кэша (Для остальных):</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Вы можете задать интервал в минутах, после которого будет осуществляться принудительная очистка кэша. Например задав 30, кеш будет очищаться каждые 30 минут.
                            Если задать 0, то очистка кэша будет производится автоматически по мере изменения информации в базе данных.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 200px; text-align: center" id="CLEAR_CACHE_MINUTES" name="CLEAR_CACHE_MINUTES" value="{{ config('cache.clear_cache')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Тип кеширования на сайте:</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите тип кеширования, который будет использовать скрипт для кеширования MySQL запросов. Файловый кеш работает на всех хостингах, перед включением Memcache, Redis, etc... вам необходимо настроить параметры в конфигурационном файле "cache"</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select name="CACHE_DRIVER" class="selectpicker" data-width="200px">
                            <option value="file" @if(config('cache.default') == 'file') selected @endif>Файловый кэш</option>
                            <option value="redis" @if(config('cache.default') == 'redis') selected @endif>Redis</option>
                            <option value="memcached" @if(config('cache.default') == 'memcached') selected @endif>Memcached</option>
                            <option value="database" @if(config('cache.default') == 'database') selected @endif>Database</option>
                            <option value="apc" @if(config('cache.default') == 'apc') selected @endif>APC</option>
                        </select>
                    </td>
                </tr>

                <!-- Connection Redis  -->
                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Данные для подключения к Redis серверу:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                           Если вы включили Redis кеширование, то вам необходимо задать параметры для подключения к Redis серверу. Если у вас включено файловое кеширование, то данную настройку можно оставить 127.0.0.1.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" id="REDIS_HOST" name="REDIS_HOST" style="width: 200px" value="{{ config('database.redis.cache.host')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Порт для подключения к Redis серверу:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                           Если вы включили Redis кеширование, то вам необходимо задать порт для подключения к Redis серверу.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 200px;text-align: center;" id="REDIS_PORT" name="REDIS_PORT" value="{{ config('database.redis.cache.port')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Пароль для сервера Redis:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                           Если вы включили Redis кеширование, то для некоторых серверов Redis может потребоваться пароль для авторизации на этом сервере, в зависимости от настроек Redis сервера. Данное поле является необязательным и если авторизация на сервере не требуется, то оставьте поле пустым.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 200px" id="REDIS_PASSWORD" name="REDIS_PASSWORD" value="{{ config('database.redis.cache.password')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Кешировать направлении обмена</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Включение или отключения кэшировании направлении обменов
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="ENABLE_CACHE_EXCHANGE" name="ENABLE_CACHE_EXCHANGE" type="checkbox" value="1" @if(config('cache.enable_cache_exchange') == true) checked @endif />
                            <label for="ENABLE_CACHE_EXCHANGE" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Тип кэширования курсов</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списока тип кеширования курсов, это функция позволяет значительно снизить нагрузку.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="form-control selectpicker" name="type_cached_rates" data-width="200px">
                            <option value="0" @if(iEXSetting('type_cached_rates') == 0) selected @endif>Серверный (не рекомендуется)</option>
                            <option value="1" @if(iEXSetting('type_cached_rates') == 1) selected @endif>Redis</option>
                            <option value="2" @if(iEXSetting('type_cached_rates') == 2) selected @endif>Файловый</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование отзывов на главном</h6>
                        <span class="text-muted text-size-small hidden-xs">Данная опция позволяет включить на сайте кеширование отзывов, что позволяет снизить нагрузку на сайт. Кеширование отзывов работает только при совместном включении общего кеширования на сайте.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_reviews_exchange" name="enable_cache_reviews_exchange" type="checkbox" value="1" @if(iEXSetting('enable_cache_reviews_exchange') == true) checked @endif />
                            <label for="enable_cache_reviews_exchange" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование тарифов</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                               Данная опция позволяет включить на сайте кеширование тарифов, что позволяет снизить нагрузку на сайт.
                                                Кеширование тарифов работает только при совместном включении общего кеширования на сайте.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_tariffs" name="enable_cache_tariffs" type="checkbox" value="1" @if(iEXSetting('enable_cache_tariffs') == true) checked @endif />
                            <label for="enable_cache_tariffs" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование Footer</h6>
                        <div class="text-muted text-size-small hidden-xs">
                            Данная опция позволяет включить на сайте кеширование footer на главной странице, что позволяет снизить нагрузку на сайт.
                            Кеширование footer работает только при совместном включении общего кеширования на сайте.
                        </div>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_footer" name="enable_cache_footer" type="checkbox" value="1" @if(iEXSetting('enable_cache_footer') == true) checked @endif />
                            <label for="enable_cache_footer" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование Уведомлений</h6>
                        <div class="text-muted text-size-small hidden-xs">
                            Данная опция позволяет включить на сайте кеширование уведомлений, что позволяет снизить нагрузку на сайт.
                            Кеширование уведомлений работает только при совместном включении общего кеширования на сайте.
                        </div>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_notification" name="enable_cache_notification" type="checkbox" value="1" @if(iEXSetting('enable_cache_notification') == true) checked @endif />
                            <label for="enable_cache_notification" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование блока "Преимущества"</h6>
                        <div class="text-muted text-size-small hidden-xs">
                            Данная опция позволяет включить на сайте кеширование преимуществ, что позволяет снизить нагрузку на сайт.
                            Кеширование преимуществ работает только при совместном включении общего кеширования на сайте.
                        </div>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_advantages" name="enable_cache_advantages" type="checkbox" value="1" @if(iEXSetting('enable_cache_advantages') == true) checked @endif />
                            <label for="enable_cache_advantages" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование блока "Партнеры"</h6>
                        <div class="text-muted text-size-small hidden-xs">
                            Данная опция позволяет включить на сайте кеширование партнеров, что позволяет снизить нагрузку на сайт.
                            Кеширование партнеров работает только при совместном включении общего кеширования на сайте.
                        </div>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_partners" name="enable_cache_partners" type="checkbox" value="1" @if(iEXSetting('enable_cache_partners') == true) checked @endif />
                            <label for="enable_cache_partners" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование раздела "Контакты"</h6>
                        <div class="text-muted text-size-small hidden-xs">
                            Данная опция позволяет включить на сайте кеширование контактов, что позволяет снизить нагрузку на сайт.
                            Кеширование страницы работает только при совместном включении общего кеширования на сайте.
                        </div>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_contacts" name="enable_cache_contacts" type="checkbox" value="1" @if(iEXSetting('enable_cache_contacts') == true) checked @endif />
                            <label for="enable_cache_contacts" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование статистики на главном</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                               Данная опция позволяет включить на сайте кеширование статистики, что позволяет снизить нагрузку на сайт.
                                                Кеширование отзывов работает только при совместном включении общего кеширования на сайте.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_statistics" name="enable_cache_statistics" type="checkbox" value="1" @if(iEXSetting('enable_cache_statistics') == true) checked @endif />
                            <label for="enable_cache_statistics" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование новостей на главном</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                               Данная опция позволяет включить на сайте кеширование новостей, что позволяет снизить нагрузку на сайт.
                                                Кеширование отзывов работает только при совместном включении общего кеширования на сайте.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_cache_news" name="enable_cache_news" type="checkbox" value="1" @if(iEXSetting('enable_cache_news') == true) checked @endif />
                            <label for="enable_cache_news" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кэширование вопросов и ответов на сайте</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                               Данная опция позволяет включить на сайте кеширование вопросов и ответов, что позволяет снизить нагрузку на сайт.
                                                Кеширование вопросов и ответов работает только при совместном включении общего кеширования на сайте.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enabled_cache_day_faq" name="enabled_cache_day_faq" type="checkbox" value="1" @if(iEXSetting('enabled_cache_day_faq') == true) checked @endif />
                            <label for="enabled_cache_day_faq" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Время кэширования вопросов и ответов (в днях):</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                 Вы можете задать интервал в днях, после которого будет осуществляться принудительная очистка кэша. Например задав 10, кеш будет очищаться каждые 10 дней.
                                                 Если задать 0, то очистка кэша будет производится автоматически по мере изменения информации в базе данных. Настоятельно рекомендуем не указывать меньше 1 дня, так как информация в этом разделе именяется крайне редко.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width:200px; text-align: center;" id="clear_cache_day_faq" name="clear_cache_day_faq" value="{{ iEXSetting('clear_cache_day_faq', 1)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить кеширование последних обменов на сайте</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                               Данная опция позволяет включить на сайте кеширование последних обменов при показе на главной странице, что позволяет снизить нагрузку на сайт. Кеширование последних обменов работает только при совместном включении общего кеширования на сайте.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="ENABLE_CACHE_LAST_EXCHANGE" name="ENABLE_CACHE_LAST_EXCHANGE" type="checkbox" value="1" @if(config('cache.enable_cache_last_exchange') == true) checked @endif />
                            <label for="ENABLE_CACHE_LAST_EXCHANGE" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Кешировать пользовательскую статистику:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Eсли 'Отключено', то статистика пользовательских обменов будет обновляться с каждым просмотром.
                                                Eсли 'Включено', кэш будет обновляться каждые 2 часа.
                                                Включение данной опции позволяет сэкономить процессорное время.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="CACHE_USER_STAT" name="CACHE_USER_STAT" type="checkbox" value="1" @if(config('cache.user_stat') == true) checked @endif />
                            <label for="CACHE_USER_STAT" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отключить подсчет заявок в конструкторе курсов</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Данная опция позволяет существенно снижает нагрузку на модуль Конструктор курсов
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_disabled_count_course_designer" name="is_disabled_count_course_designer" type="checkbox" value="1" @if(iEXSetting('is_disabled_count_course_designer') == true) checked @endif />
                            <label for="is_disabled_count_course_designer" class="label-primary"></label>
                        </div>
                    </td>
                </tr>



                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Кешировать счетчик заявок:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Eсли 'Отключено', то счетчик заявок будет обновляться с каждым созданием новой заявки.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="CACHE_ORDER_COUNT" name="CACHE_ORDER_COUNT" type="checkbox" value="1" @if(config('cache.order_count') == true) checked @endif />
                            <label for="CACHE_ORDER_COUNT" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

            </table>
        </div>

        <div role="tabpanel" class="tab-pane" id="security">
            <table class="table table-2 table-striped">
                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">URL админ панели:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Вы можете изменить имя файла админпанели. По умолчанию это /iexadmin, если вы указываете новый URL путь.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" id="APP_ADMIN_PATH" name="APP_ADMIN_PATH" value="{{ config('admin.route_path')  }}" placeholder="Введите путь к админ панели" style="width: 300px">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить защиту Google Authenticator</h6>
                        <span class="text-muted text-size-small hidden-xs">
                        При включении данной настройки, при входе в панель управления, необходимо пройти двухфакторную авторизацию<br />

                        <b class="text-danger">Внимание:</b> Перед включением данной функции, создайте ключ. "Список пользователей - Пользователь - Редактирование данных"
                    </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_google_auth" name="is_google_auth" type="checkbox" value="1" @if(iEXSetting('is_google_auth') == 1) checked @endif />
                            <label for="is_google_auth" class="label-success"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Выберите установленную защиту от DDOS</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Выберите из списка установленную вами систему защиту от DDOS Атак.<br />
                            <span class="text-danger">Внимание!</span> Если нет такой защиты, рекомендуем установить
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="form-control selectpicker" name="proxiesfilter_sources_ddos" data-width="300px" data-live-search="true">
                            <option value="">Не установлено</option>
                            @foreach(config('ddos.services') as $value)
                                <option value="{{ $value }}" @if(iEXSetting('proxiesfilter_sources_ddos') == $value) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>


                        <div class="text-primary input-p-text">
                            <a href="?reload_ip_address">Обновить IP Адреса</a>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">HTTPS Only Cookies</h6>
                        <span class="text-muted text-size-small hidden-xs">Если опция включена, файлы cookie сеанса будут отправляться обратно на сервер только в том случае, если в браузере установлено соединение HTTPS.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="SESSION_SECURE_COOKIE" name="SESSION_SECURE_COOKIE" type="checkbox" value="1" @if(config('session.secure') == true) checked @endif />
                            <label for="SESSION_SECURE_COOKIE" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Выберите механизм хранения данных сессий</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка драйвер, которую хотите использовать для хранения пользовательских сессий<br />
                            <span class="text-danger">Внимание!</span> При переключении режима хранения сессий все пользователи потеряют авторизацию (данные сессий будут уничтожены).
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="form-control selectpicker" name="SESSION_DRIVER" data-width="300px" data-live-search="true">
                            @foreach(config('session.sessions_available') as $key => $value)
                                <option value="{{ $key }}" @if(config('session.driver') == $key) selected @endif>
                                    {{ $value }}
                                </option>
                            @endforeach
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Время жизни хранения данных сессии, в минутах:</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Укажите в минутах время жизни сессионных данных, данная функция позволяет сделать похищение авторизованной сессии неэффективным.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 300px" id="session_lifetime" name="session_lifetime" value="{{ iEXSetting('session_lifetime', 120)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешенные домены</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Данная опция необходима для настройки Cors.
                            Укажите список разрешенных доменов, разделяя запятыми.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <textarea class="form-control" rows="5" name="security_allowed_domains">{{ iEXSetting('security_allowed_domains') }}</textarea>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Максимальное количество ошибочных авторизаций</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Укажите максимальное количество ошибочных вводов пароля на сайте. После превышения данного лимита, для IP пользователя будет установлена автоматическая блокировка на указанное в настройках количество минут. Данная мера позволяет предотвратить подбор паролей злоумышленниками к аккаунтам пользователей.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:300px; text-align: center;" id="LOGIN_MAX_ATTEMPTS" name="LOGIN_MAX_ATTEMPTS" value="{{ config('auth.max_attempts')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Таймаут в минутах после нескольких вводов неверного пароля</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Укажите промежуток времени, на который будет блокироваться возможность авторизации для IP посетителя, после указанного выше максимального количества ошибочных вводов пароля. Таймаут указывается в минутах. Не рекомендуется делать это значение менее 10 минут
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="width: 300px; text-align: center;" id="LOGIN_DECAY_MINUTES" name="LOGIN_DECAY_MINUTES" value="{{ config('auth.decay_minutes')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Проверка неудачных попыток только по IP</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Если 'Отключено', то неудачные попытки авторизаций будут проверятся по нескольким параметрам. Email и IP.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="SwitchOptionPrimary4-is-ip-throttle-key" name="is_ip_throttle_key" type="checkbox" value="1" @if(iEXSetting('is_ip_throttle_key') == true) checked @endif />
                            <label for="SwitchOptionPrimary4-is-ip-throttle-key" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Контроль изменения IP адреса</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Средний уровень - происходит автоматический сброс авторизации на сайте при изменении IP адреса у пользователей имеющих доступ в админцентр
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="form-control selectpicker" name="ip_change_control" data-width="300px">
                            <option value="0" @if(iEXSetting('ip_change_control') == 0) selected @endif>Отсутствует</option>
                            <option value="1"  @if(iEXSetting('ip_change_control') == 1) selected @endif>Средний уровень</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить ограничитель по IP</h6>
                        <span class="text-muted text-size-small hidden-xs">Если опция включена, то админ панель будет доступна только для определенных IP адресов</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_firewall_enabled" name="is_firewall_enabled" type="checkbox" value="1" @if(iEXSetting('is_firewall_enabled') == 1) checked @endif />
                            <label for="is_firewall_enabled" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Список IP для которых разрешена авторизация в админпанели скрипта</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Вы можете ограничить авторизацию в админпанели только для определенных IP адресов. Внимание, будьте бдительны при изменении данной настройки.
                            Доступ к админпанели будет возможен только с указанных IP адресов. Вы можете указать несколько адресов, через запятую и по одному на каждую строчку. Вы можете указать как полный IP адрес, так и маску,
                            например: 192.48.25.71 или 129.42.0.0/16 Для того чтобы не устанавливать никаких ограничений по IP, оставьте данное поле пустым.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <textarea class="form-control" rows="5" style="margin-bottom: 10px;" name="admin_allowed_ip">{{ iEXSetting('admin_allowed_ip') }}</textarea>

                        <div class="alert alert-warning alert-lite">Ваш IP-адрес был определен как: {{ \Request::ip() }}. Если это так, скопируйте его и вставьте в поле ввода сверху.</div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Сбрасывать ключ авторизации при каждом входе?</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Eсли 'Включено', каждая успешная авторизация пользователя на сайте будет сбрасывать его ключ авторизации. Это сделает невозможным войти под одним именем пользователя более, чем с одного компьютера.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="ONE_SESSION_USER" name="ONE_SESSION_USER" type="checkbox" value="1" @if(config('auth.one_session_user') == true) checked @endif />
                            <label for="ONE_SESSION_USER" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Тип кода безопасности Captcha:</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите тип кода безопасности, который будет использоваться. Вы можете установить использование reCaptcha, либо установить код сервиса hCaptcha.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="is_security_captcha_type">
                            <option value="0" @if(iEXSetting('is_security_captcha_type') == 0) selected @endif>reCaptcha</option>
                            <option value="1" @if(iEXSetting('is_security_captcha_type') == 1) selected @endif>hCaptcha</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Публичный ключ сервиса CAPTCHA:</h6>
                        <span class="text-muted text-size-small hidden-xs">Получить ключ вы можете по ссылке: http://www.google.com/recaptcha Внимание, настоятельно рекомендуется зарегистрироваться на сервисе и сгенерировать для своего сайта уникальную пару ключей, установив разрешение на использование только на своем домене. Использование стандартной пары ключей, не дает должного эффекта по защите от спам роботов.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" id="NOCAPTCHA_SITEKEY" name="NOCAPTCHA_SITEKEY" value="{{ config('captcha.sitekey') }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Секретный ключ сервиса CAPTCHA:</h6>
                        <span class="text-muted text-size-small hidden-xs">Получить ключ вы можете по ссылке: http://www.google.com/recaptcha Внимание, настоятельно рекомендуется зарегистрироваться на сервисе и сгенерировать для своего сайта уникальную пару ключей, установив разрешение на использование только на своем домене. Использование стандартной пары ключей, не дает должного эффекта по защите от спам роботов.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" id="NOCAPTCHA_SECRET" name="NOCAPTCHA_SECRET" value="{{ config('captcha.secret') }}">
                    </td>
                </tr>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="notification">
            <table class="table table-2 table-striped">

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-mail уведомление при корректировки резерва</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            В случае включения данной настройки, при ручной корректировки резерва на почту будет отправлено соответствующее уведомление
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_notify_change_reserve" name="is_notify_change_reserve" type="checkbox" value="1" @if(iEXSetting('is_notify_change_reserve') == 1) checked @endif />
                            <label for="is_notify_change_reserve" class="label-primary"></label>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-mail уведомление при получении новых заявок на выплат</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при создании новой заявки на выплату бонусных вознаграждений, на E-Mail указанный в настройках будет отправлено соответствующее уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="mail_order_withdrawal" name="mail_order_withdrawal" type="checkbox" value="1" @if(iEXSetting('mail_order_withdrawal') == 1) checked @endif />
                            <label for="mail_order_withdrawal" class="label-success"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Вид отправки E-Mail уведомления клиентам о новой заявке</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка способ отправки E-Mail уведомления на почту клиентам о создании заявки.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="form-control selectpicker" name="type_notify_email_new_order">
                            <option value="0" @if(iEXSetting('type_notify_email_new_order') == 0) selected @endif>Не уведомлять</option>
                            <option value="1" @if(iEXSetting('type_notify_email_new_order') == 1) selected @endif>При создании заявки</option>
                            <option value="2" @if(iEXSetting('type_notify_email_new_order') == 2) selected @endif>При нажатии кнопки "Я оплатил"</option>
                            <option value="3" @if(iEXSetting('type_notify_email_new_order') == 3) selected @endif>При создании и верификации заявки</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-Mail уведомление при изменение статуса заявки (Для клиентов)</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при изменение статуса заявки, на E-Mail указанный в профиле клиента будет отправлено соответствующее уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="SwitchOptionPrimary5-2" name="notify_change_status" type="checkbox" value="1" @if(iEXSetting('notify_change_status') == 1) checked @endif />
                            <label for="SwitchOptionPrimary5-2" class="label-success"></label>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="mail">
            <table class="table table-2 table-striped">
                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">E-Mail адрес службы поддержки:</h6>
                        <span class="text-muted text-size-small hidden-xs">Введите E-Mail адрес службы поддержки. Этот E-mail будет прикреплен сообщения которые будут отправлять на почту клиентам.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="project_email_support" value="{{ iEXSetting('project_email_support') }}" class="form-control" style="width: 300px">

                            <div style="font-size: 11px; color: #999; padding-top: 5px;">
                                <a href="?test=send_mail">Отправить тестовое сообщение</a>
                            </div>

                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">E-Mail адрес администратора:</h6>
                        <span class="text-muted text-size-small hidden-xs">Введите E-Mail адрес администратора сайта. На этот адрес будут отправлять информации о новых заявкам, резервах и т.д. Если у вас несколько получателей, укажите их через запятую. (Пример: test@example.com,test2@example.com)</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="email_addresses_admin" value="{{ iEXSetting('email_addresses_admin') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">SMTP хост</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите хост smtp</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="MAIL_HOST" value="{{ config('mail.mailers.smtp.host') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">SMTP Порт</h6>
                        <span class="text-muted text-size-small hidden-xs">Обычно — 25</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="MAIL_PORT" value="{{ config('mail.mailers.smtp.port') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">SMTP Имя пользователя</h6>
                        <span class="text-muted text-size-small hidden-xs">Не требуется в большинстве случаев, когда используется 'localhost'</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="MAIL_USERNAME" value="{{ config('mail.mailers.smtp.username') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">SMTP Пароль</h6>
                        <span class="text-muted text-size-small hidden-xs">Не требуется в большинстве случаев, когда используется 'localhost'</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="password" name="MAIL_PASSWORD" value="{{ config('mail.mailers.smtp.password') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">E-mail для авторизации на SMTP сервере в качестве отправителя</h6>
                        <span class="text-muted text-size-small hidden-xs">Данная настройка является необязательной, однако некоторые бесплатные почтовые сервисы, например yandex.ru, требуют, чтобы в качестве E-mail адреса отправителя был указан именно адрес, зарегистрированный на их почтовом сервисе.	</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="MAIL_FROM_ADDRESS" value="{{ config('mail.from.address') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">SMTP От имени</h6>
                        <span class="text-muted text-size-small hidden-xs">Не требуется в большинстве случаев, когда используется 'localhost'</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        @if(config('admin.is_demo_mode') == true)
                            <small class="text-danger">Недоступен в демо режиме</small>
                        @else
                            <input type="text" name="mail_from_name" value="{{ iEXSetting('mail_from_name', 'Support') }}" class="form-control" style="width: 300px">
                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Использовать защищенный протокол для отправки писем через SMTP сервер</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите протокол шифрования при отправке писем с использованием SMTP сервера</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="btn-group colors" data-toggle="buttons">
                            <label class="btn btn-default  @if(config('mail.mailers.smtp.encryption') == 'tls') active @endif">
                                <input type="radio" name="MAIL_ENCRYPTION" value="tls" autocomplete="off" @if(config('mail.mailers.smtp.encryption') == 'tls') checked @endif> TLS
                            </label>
                            <label class="btn btn-default  @if(config('mail.mailers.smtp.encryption') == 'ssl') active @endif">
                                <input type="radio" name="MAIL_ENCRYPTION" value="ssl" autocomplete="off"  @if(config('mail.mailers.smtp.encryption') == 'ssl') checked @endif> SSL
                            </label>
                        </div>
                    </td>
                </tr>


            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="users">
            <table class="table table-2 table-striped">

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отключить автоматическую регистрацию клиентов</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            Eсли 'Включено', то пользователь может создать заявку без создания учетной записи.
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="auto_register" name="auto_register" type="checkbox" value="1" @if(iEXSetting('auto_register') == 1) checked @endif />
                            <label for="auto_register" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Разрешить записывать номер счета') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Если данная функция включена, то после успешного выполнения заявки, номера счета клиента будет сохранен. В след. раз клиент может выбрать номер счета сразу из списке доступных') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="is_saved_user_stories">
                            <option value="0" @if(iEXSetting('is_saved_user_stories') == 0) selected @endif>{{ __('Да') }}</option>
                            <option value="1" @if(iEXSetting('is_saved_user_stories') == 1) selected @endif>{{ __('Нет') }}</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">{{ __('Разрешить вывести галочку "Не запоминать введенные данные"') }}</h6>
                        <span class="text-muted text-size-small hidden-xs">{{ __('Если данная функция включена, то на главной странице обмена появится галочка "Не запоминать введенные данные"') }}</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <select class="selectpicker" name="is_dot_not_remember_data_order">
                            <option value="0" @if(iEXSetting('is_dot_not_remember_data_order') == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if(iEXSetting('is_dot_not_remember_data_order') == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить регистрацию нескольких пользователей с одного IP</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной опции, будет разрешена пользователю регистрация нескольких логинов с одного IP адреса, в противном случае если IP посетителя использовался другим зарегистрированным пользователем, то регистрация будет запрещена.	</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="reg_multi_ip" name="reg_multi_ip" type="checkbox" value="1" @if(iEXSetting('reg_multi_ip') == 1) checked @endif />
                            <label for="reg_multi_ip" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить Geo IP</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной настройки, будут собраны геолокационные данные пользователя. (Страна, IP, Код страны, координаты и.т.д)</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_geo_ip" name="is_geo_ip" type="checkbox" value="1" @if(iEXSetting('is_geo_ip') == 1) checked @endif />
                            <label for="is_geo_ip" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить уведомление о входах с новых устройств</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной настройки, и случае авторизаций с новых устройств на email пользователя будет отправлено соответствующие уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_notify_new_device" name="is_notify_new_device" type="checkbox" value="1" @if(iEXSetting('is_notify_new_device') == 1) checked @endif />
                            <label for="is_notify_new_device" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать Email уведомление об успешной авторизации</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то после успешной авторизации клиента в системе на почту будет отправлено соответствующие уведомление.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_user_successful_login" name="is_user_successful_login" type="checkbox" value="1" @if(iEXSetting('is_user_successful_login') == 1) checked @endif />
                            <label for="is_user_successful_login" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать Email уведомление для подтверждения E-mail</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то после успешной регистрации на Email адрес пользователя будет отправлено соответствующие уведомление для подтверждения E-mail адреса.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_user_verified_email" name="is_user_verified_email" type="checkbox" value="1" @if(iEXSetting('is_user_verified_email') == 1) checked @endif />
                            <label for="is_user_verified_email" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить проверить E-mail адрес на уникальность</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то в процессе регистрации поле E-mail будет проверяться на уникальность</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_user_email_unique" name="is_user_email_unique" type="checkbox" value="1" @if(iEXSetting('is_user_email_unique') == 1) checked @endif />
                            <label for="is_user_email_unique" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать Email уведомление о неудачных авторизациях</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', в случае многочисленных неудачных авторизациях, на Email адрес пользователя будет отправлено соответствующие уведомление с Geo данными.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_lockout_auth" name="is_lockout_auth" type="checkbox" value="1" @if(iEXSetting('is_lockout_auth') == 1) checked @endif />
                            <label for="is_lockout_auth" class="label-primary"></label>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить записывать информацию о клиенте</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то в лог будет записываться информация о клиентах которые посещают главную страницу</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_write_client_log" name="is_write_client_log" type="checkbox" value="1" @if(iEXSetting('is_write_client_log') == 1) checked @endif />
                            <label for="is_write_client_log" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить записывать лог авторизаций</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то в лог будет записываться информация об авторизованных пользователях</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="enable_user_auth_log" name="enable_user_auth_log" type="checkbox" value="1" @if(iEXSetting('enable_user_auth_log') == 1) checked @endif />
                            <label for="enable_user_auth_log" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Максимальное количество зарегистрированных пользователей:</h6>
                        <span class="text-muted text-size-small hidden-xs">0 если ограничений нет</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:150px; text-align: center;" id="MAX_USERS" name="MAX_USERS" value="{{ config('auth.max_users')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Имя по умолчанию:</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите имя пользователя по умолчанию.
                                                Во основном используется при автоматической регистрации пользователей.
                                                При необходимости разрешено зафиксировать ID. <b>Пример:</b> Пользователь_:id: или Пользователь_:random:</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <x-forms.language.input tabName="username_new_user" keyName="username_new_user" />
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить хранение истории паролей:</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то каждое изменение пароля будет записываться в историю.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_user_password_history" name="is_user_password_history" type="checkbox" value="1" @if(iEXSetting('is_user_password_history') == 1) checked @endif />
                            <label for="is_user_password_history" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить дополнительную фильтрацию имени:</h6>
                        <span class="text-muted text-size-small hidden-xs">При включении данной настройки, при регистрации нового имени пользователя будут применены дополнительные фильтры. Этот фильтр исключает нежелательные символы в имени.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_filter_username" name="is_filter_username" type="checkbox" value="1" @if(iEXSetting('is_filter_username') == 1) checked @endif />
                            <label for="is_filter_username" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Минимальная сумма выплаты бонусов:</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите минимальную сумму выплаты бонусных вознаграждений в валюте. По достижении данной суммы, в личном кабинете клиента активируется кнопка "Создать заявку на выплату".</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:150px; text-align: center;" id="minimum_bonus_payout" name="minimum_bonus_payout" value="{{ iEXSetting('minimum_bonus_payout', 100)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Минимальная сумма автовывода бонусных вознаграждений:</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите минимальную сумму автовывода бонусных вознаграждений в рублях. По достижении данной суммы, заявки на выплату будут созданы автоматически.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:150px; text-align: center;" id="min_auto_withdrawal" name="min_auto_withdrawal" value="{{ iEXSetting('min_auto_withdrawal', 5000)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Отсылать E-Mail уведомление при авто-регистрации пользователя:</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при автоматической регистрации пользователя, на E-Mail указанный при создании заявки будет отправлен логин и пароль от аккаунта. Этот параметр не обязателен для включения.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_email_auto_user" name="is_email_auto_user" type="checkbox" value="1" @if(iEXSetting('is_email_auto_user') == 1) checked @endif />
                            <label for="is_email_auto_user" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Авторизовать пользователей на домене и всех его поддоменах</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                                Eсли поле заполнено, то пользователи при авторизации на сайте, будут авторизовываться на домене всех его поддоменах. Например, если пользователь авторизовался на домене второго уровня domen.ru, то данные его авторизации будут доступны и на поддомене test.domen.ru и на всех других поддоменах. Для того чтобы активировать единую авторизацию для всех поддоменов укажите *.domen.ru. Это функция необходимо для пользователей, которые устанавливают скрипт на домен и поддомены и хотят сделать для них единую авторизацию посетителей.
                                            </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:350px;" id="SESSION_DOMAIN" name="SESSION_DOMAIN" value="{{ config('session.domain')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Автоматическое отключение зарегистрированных пользователей:</h6>
                        <span class="text-muted text-size-small hidden-xs">Количество дней, через которого пользователь будет деактивирован, если он не посещал сайт 0 = ограничений нет</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:150px; text-align: center;" id="MAX_USER_DAY" name="MAX_USER_DAY" value="{{ config('auth.max_users_day')  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Сколько дней хранить пользователей с неподтвержденной регистрацией:</h6>
                        <span class="text-muted text-size-small hidden-xs">Количество дней, через которого пользователь будет деактивирован в случае если он не подтвердил e-mail адрес. 0 = ограничений нет</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" class="form-control" style="max-width:150px; text-align: center;" id="new_user_registration_cleanup_days" name="new_user_registration_cleanup_days" value="{{ iEXSetting('new_user_registration_cleanup_days', 0)  }}">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить активацию реферальной программы после верификации:</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то только после верификации e-mail адреса клиент может получить реферальные бонусы от заявок.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="SwitchOptionPrimary8-is-verified-referral" name="is_verified_referral" type="checkbox" value="1" @if(iEXSetting('is_verified_referral') == 1) checked @endif />
                            <label for="SwitchOptionPrimary8-is-verified-referral" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Автоматически запомнить вход в систему:</h6>
                        <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то только после автоматической регистрации пользователя активируется алгоритм "Запомнить меня".</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_remember_login" name="is_remember_login" type="checkbox" value="1" @if(iEXSetting('is_remember_login') == 1) checked @endif />
                            <label for="is_remember_login" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Скрыть валюты из раздела "Мои кошельки"</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка валюты, которые хотите скрыть в разделе "Мои кошельки", в настройках личного кабинета клиента.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {{ Form::select('ids_currencies_account_my_wallets[]', $currencies_reserves, explode(',', iEXSetting('ids_currencies_account_my_wallets')), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '8', 'multiple', 'data-width' => '300px']) }}
                    </td>
                </tr>

            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="other">
            <table class="table table-2 table-striped">

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить функцию "Копировать данные клиента по одному клику"</h6>
                        <span class="text-muted text-size-small hidden-xs">
                            При включении данной функции, в карточке заявок у вас будет возможность копировать информацию о клиенте, по клику на текст.<br />
                        </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_order_user_no_copydata" name="is_order_user_no_copydata" type="checkbox" value="1" @if(iEXSetting('is_order_user_no_copydata') == 1) checked @endif />
                            <label for="is_order_user_no_copydata" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить внутренний счет</h6>
                        <span class="text-muted text-size-small hidden-xs">В случае включения данной настройки, активируется возможность пополнения внутреннего счета клиента.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="ENABLE_INTERNAL_ACCOUNT" name="ENABLE_INTERNAL_ACCOUNT" type="checkbox" value="1" @if(config('payment.enable_internal_account') == 1) checked @endif />
                            <label for="ENABLE_INTERNAL_ACCOUNT" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Вид отображения номера счета в чеке</h6>
                        <span class="text-muted text-size-small hidden-xs">Выберите из списка вид отображения номер счета.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        {!! Form::select('i_ordercheck_from_shot', $typeFormatOrderCheck, iEXSetting('i_ordercheck_from_shot'),
                               ['class' => 'form-control selectpicker', 'data-width' => '300']) !!}
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Включить подтверждение вывода вознаграждений</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                        Eсли 'Включено', то клиенту на почту будет отправлено уведомление о подтверждение вывода бонусных вознаграждений
                                    </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_verified_payouts_bonus" name="is_verified_payouts_bonus" type="checkbox" value="1" @if(iEXSetting('is_verified_payouts_bonus') == 1) checked @endif />
                            <label for="is_verified_payouts_bonus" class="label-success"></label>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить отображать отключенные направления</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                         В случае включения данной настройки, на главной странице будет отображены все созданные направления, при этом отключенные направления будут не кликабельны.
                                    </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_visible_disabled_directions" name="is_visible_disabled_directions" type="checkbox" value="1" @if(iEXSetting('is_visible_disabled_directions') == 1) checked @endif />
                            <label for="is_visible_disabled_directions" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Активировать Google Analytics</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то с сервера Goolge будут подгружены данные в реальном времени</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_enabled_google_analytics" name="is_enabled_google_analytics" type="checkbox" value="1" @if(iEXSetting('is_enabled_google_analytics') == 1) checked @endif />
                            <label for="is_enabled_google_analytics" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">View ID</h6>
                        <span class="text-muted text-size-small hidden-xs">Идентификатор Google Analytics, для которого вы хотите отобразить данные.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="ANALYTICS_VIEW_ID" value="{{config('analytics.view_id')}}" class="form-control" style="width:100%;max-width:250px">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Google Аналитика</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите Идентификатор из Google Аналитики для отслеживания трафика</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="google_analytics" value="{{ iEXSetting('google_analytics') }}" class="form-control" style="width:100%;max-width:250px">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Yandex Метрика</h6>
                        <span class="text-muted text-size-small hidden-xs">Укажите Идентификатор из Яндекс Метрики для отслеживания трафика</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <input type="text" name="yandex_metrika" value="{{ iEXSetting('yandex_metrika') }}" class="form-control" style="width:100%;max-width:250px">
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Активировать "Панель оператора"</h6>
                        <span class="text-muted text-size-small hidden-xs">
                                        Если 'Включено', то операторы могут обрабатывать заявки в этом панели
                                    </span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="ENABLE_PANEL_OPERATOR" name="ENABLE_PANEL_OPERATOR" type="checkbox" value="1" @if(config('crypto.panel_operator') == 1) checked @endif />
                            <label for="ENABLE_PANEL_OPERATOR" class="label-primary"></label>
                        </div>
                    </td>
                </tr>


                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Скрыть направления обменов от гостей</h6>
                        <span class="text-muted text-size-small hidden-xs">Направлении не будут доступны неавторизованным клиентам</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="hide_exchange_guest" name="hide_exchange_guest" type="checkbox" value="1" @if(iEXSetting('hide_exchange_guest') == 1) checked @endif />
                            <label for="hide_exchange_guest" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить записывать лог e-mail уведомлений</h6>
                        <span class="text-muted text-size-small hidden-xs">Если 'Включено', то в лог будет записываться отправленные e-mail письма</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_enable_email_notification" name="is_enable_email_notification" type="checkbox" value="1" @if(iEXSetting('is_enable_email_notification') == 1) checked @endif />
                            <label for="is_enable_email_notification" class="label-primary"></label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-6 col-sm-6 col-md-7">
                        <h6 class="media-heading text-semibold">Разрешить запоминать выбранные фильтры валют</h6>
                        <span class="text-muted text-size-small hidden-xs">В случае включении данной настройки, после выбора фильтра валют система запомнит последний выбранный и после обновления страницы переключится на этот фильтр.</span>
                    </td>
                    <td class="col-xs-6 col-sm-6 col-md-5">
                        <div class="material-switch">
                            <input id="is_loading_save_filter_currency" name="is_loading_save_filter_currency" type="checkbox" value="1" @if(iEXSetting('is_loading_save_filter_currency') == 1) checked @endif />
                            <label for="is_loading_save_filter_currency" class="label-primary"></label>
                        </div>
                    </td>
                </tr>
                <!--- Для админки в "Заявки" --->
            </table>
        </div>

    </div>
