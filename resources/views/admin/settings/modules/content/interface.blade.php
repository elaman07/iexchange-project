<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="base">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Интерфейс обменника</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка стиль выбора направлений</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="interface_exchange" data-width="300px">
                        <option value="1" @if(iEXSetting('interface_exchange') == 1) selected @endif>Стиль 1 (По умолчанию)</option>
                        <option value="2" @if(iEXSetting('interface_exchange') == 2) selected @endif>Стиль 2</option>
                        <option value="3" @if(iEXSetting('interface_exchange') == 3) selected @endif>Стиль 3</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Вид соглашения с "правилам сервиса"</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите тип работы блока ввода данных на странице обмена</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="is_style_agreement_rules" data-width="300px">
                        <option value="0" @if(iEXSetting('is_style_agreement_rules') == 0) selected @endif>По умолчанию</option>
                        <option value="1" @if(iEXSetting('is_style_agreement_rules') == 1) selected @endif>Использовать галочки</option>
                    </select>
                    <div class="form-check">
                        <input class="form-check-input" id="is_style_agreement_checkbox" value="1" type="checkbox" name="is_style_agreement_checkbox" @if(iEXSetting('is_style_agreement_checkbox') == 1) checked @endif>
                        <label class="form-check-label" for="is_style_agreement_checkbox">Деактивировать активные галочки</label>
                    </div>

                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить запомнить поставленные галочки для правил</h6>
                    <span class="text-muted text-size-small hidden-xs">Если включено, и если клиент один раз согласился с правилами обмена то в будущем эти галочки будут отключены для него. Данная функция будет доступна только авторизованным клиентам</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_exchange_rules_remember_checkbox" name="is_exchange_rules_remember_checkbox" type="checkbox" value="1" @if(iEXSetting('is_exchange_rules_remember_checkbox') == 1) checked @endif />
                        <label for="is_exchange_rules_remember_checkbox" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Описание для раздела "Авторизация"</h6>
                    <span class="text-muted text-size-small hidden-xs">Добавите дополнительную информацию для раздела "Авторизация"</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.textarea-ckeditor ckeditorId="ckeditor" tabName="interface_regauth_text1" keyName="interface_regauth_text1" />
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Описание для раздела "Регистрация"</h6>
                    <span class="text-muted text-size-small hidden-xs">Добавите дополнительную информацию для раздела "Регистрация"</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.textarea-ckeditor ckeditorId="ckeditor" tabName="interface_regauth_text2" keyName="interface_regauth_text2" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Рядом с полями ввода сумм обменов отображать</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка опцию, которая будет отображаться рядом с суммой обмена на главной странице</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="type_view_field_exchange_amount">
                        <option value="0" @if(iEXSetting('type_view_field_exchange_amount') == 0) selected @endif>Код валюты и логотип</option>
                        <option value="1" @if(iEXSetting('type_view_field_exchange_amount') == 1) selected @endif>Только логотип валюты</option>
                        <option value="2" @if(iEXSetting('type_view_field_exchange_amount') == 2) selected @endif>Только код валюты</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Вид смены языка</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите вид вывода смены языков на сайте</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="type_view_language_format">
                        <option value="0" @if(iEXSetting('type_view_language_format') == 0) selected @endif>Иконка</option>
                        <option value="1" @if(iEXSetting('type_view_language_format') == 1) selected @endif>Код страны</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить последние обмены</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение последних обменов пользователя</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="SwitchOptionPrimary7-4" name="visible_last_exchange" type="checkbox" value="1" @if(iEXSetting('visible_last_exchange') == 1) checked @endif />
                        <label for="SwitchOptionPrimary7-4" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить избранные курсы</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение избранных курсов на главной странице</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_top_selected_courses" name="is_enabled_top_selected_courses" type="checkbox" value="1" @if(iEXSetting('is_enabled_top_selected_courses') == 1) checked @endif />
                        <label for="is_enabled_top_selected_courses" class="label-primary"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить преимущество</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение преимущество</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="SwitchOptionPrimary9-16" name="visible_advantage" type="checkbox" value="1" @if(iEXSetting('visible_advantage') == 1) checked @endif />
                        <label for="SwitchOptionPrimary9-16" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить статистику</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                        Включение и отключение статистики на главном
                                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_statistics" name="visible_statistics" type="checkbox" value="1" @if(iEXSetting('visible_statistics') == 1) checked @endif />
                        <label for="visible_statistics" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить отзывы</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                        Включение и отключение отзывов на главном
                                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_reviews" name="visible_reviews" type="checkbox" value="1" @if(iEXSetting('visible_reviews') == 1) checked @endif />
                        <label for="visible_reviews" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Количество последних отзывов</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                         Укажите количество последних отзывов, которые будут отображены на сайте.
                                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="count_reviews_exchange" name="count_reviews_exchange"
                           style="max-width:150px; text-align: center;"
                           value="{{ iEXSetting('count_reviews_exchange', 20) }}">
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить новости</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                        Если 'Включено', на главной странице появится список новостей.
                                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_news" name="visible_news" type="checkbox" value="1" @if(iEXSetting('visible_news') == 1) checked @endif />
                        <label for="visible_news" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Количество последних новостей</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                         Укажите количество последних новостей, которые будут отображены на сайте.
                                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="count_news_exchange" name="count_news_exchange"
                           style="max-width:150px; text-align: center;"
                           value="{{ iEXSetting('count_news_exchange', 20) }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить партнеров</h6>
                    <span class="text-muted text-size-small hidden-xs">Если 'Включено', на главной странице появится список партнеров.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_partners" name="visible_partners" type="checkbox" value="1" @if(iEXSetting('visible_partners') == 1) checked @endif />
                        <label for="visible_partners" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Количество последний обменов</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                        Укажите количество последних обменов, которые будут отображены на сайте.
                                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="count_last_exchange" name="count_last_exchange"
                           style="max-width:150px; text-align: center;"
                           value="{{ iEXSetting('count_last_exchange', 10) }}">
                </td>
            </tr>
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить Footer</h6>
                    <span class="text-muted text-size-small hidden-xs"> При включении данной функции, на главной странице внизу будет появляться дополнительный блок с информацией.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enable_footer" name="is_enable_footer" type="checkbox" value="1" @if(iEXSetting('is_enable_footer') == 1) checked @endif />
                        <label for="is_enable_footer" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить баннер</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение баннера</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_banner" name="visible_banner" type="checkbox" value="1" @if(iEXSetting('visible_banner') == 1) checked @endif />
                        <label for="visible_banner" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить баннер для мобильной версии</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение баннера для мобильной версии</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_banner_mobile" name="visible_banner_mobile" type="checkbox" value="1" @if(iEXSetting('visible_banner_mobile') == 1) checked @endif />
                        <label for="visible_banner_mobile" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить использование цветов для валют</h6>
                    <span class="text-muted text-size-small hidden-xs"> При включении данной функции, цвета которую вы используете в валютах будут применены для работы с обменами .</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_gradient_text_color" name="is_gradient_text_color" type="checkbox" value="1" @if(iEXSetting('is_gradient_text_color') == 1) checked @endif />
                        <label for="is_gradient_text_color" class="label-primary"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить популярные направления</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение популярных направлений</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_popular_exchange" name="visible_popular_exchange" type="checkbox" value="1" @if(iEXSetting('visible_popular_exchange') == 1) checked @endif />
                        <label for="visible_popular_exchange" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Количество популярных направлений</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите количество популярных направлений, которые будут отображены на сайте.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="count_popular_exchange" name="count_popular_exchange" style="max-width:150px; text-align: center;" value="{{ iEXSetting('count_popular_exchange', 10) }}">
                </td>
            </tr>
        </table>
    </div>

    <div role="tabpanel" class="tab-pane active" id="style">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Тип отображения фона</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Выберите из списка тип отображения фона
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="row">
                        <div class="col-md-6" style="margin-left: -15px; margin-right: -15px;">
                            <select class="form-control selectpicker" name="exchange_fon_type">
                                <option value="0" @if(iEXSetting('exchange_fon_type') == 0) selected @endif>По умолчанию</option>
                                <option value="1" @if(iEXSetting('exchange_fon_type') == 1) selected @endif>Использовать свой фон</option>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <div class="col-md-6">
                            <select class="form-control selectpicker" name="type_view_background">
                                <option value="0" @if(iEXSetting('type_view_background') == 0) selected @endif>По умолчанию</option>
                                <option value="1" @if(iEXSetting('type_view_background') == 1) selected @endif>Фиксированный</option>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Стиль для фона</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Добавьте дополнительный стиль для фона, согласна правилам https://developer.mozilla.org/en-US/docs/Web/CSS/background
                    </span>
                </td>
                <td class="col-md-12">
                    <input type="text" class="form-control" id="exchange_fon_type_rules" name="exchange_fon_type_rules" value="{{ iEXSetting('exchange_fon_type_rules') }}">
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Выберите фон</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Загрузите фон для отображения на сайте
                    </span>
                </td>

                <td class="col-xs-6 col-sm-6 col-md-5 multi-language-form-group">

                    <div class="control-label-br" layout="row">
                        <span flex></span>
                        <ul class="nav nav-pills lang-tabs">
                            @foreach(['Default', 'Dark'] as $key_image => $value_image)
                                <li @if($key_image == 0) class="active" @endif>
                                    <a class="lang-tabs-link" data-toggle="tab" href="#image-fon-{{ $value_image }}">
                                        {{ $value_image }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div id="image-fon-Default" class="tab-pane fade in active">
                        <input type="file" name="exchange_fon_file" class="form-control" accept="image/*">
                    </div>

                    <div id="image-fon-Dark" class="tab-pane fade in">
                        <input type="file" name="exchange_fon_file_dark" class="form-control" accept="image/*">
                    </div>

                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Стиль отображения меню</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка подходящий стиль навигационного меню который будет отображаться на вверху обменника</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="iex_interface_toolbar_row_menu" data-width="300px">
                        <option value="2" @if(iEXSetting('iex_interface_toolbar_row_menu') == 2) selected @endif>Меню рядом с логотипом</option>
                        <option value="3" @if(iEXSetting('iex_interface_toolbar_row_menu') == 3) selected @endif>Меню внизу логотипа</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Установить свой шрифт</h6>
                    <span class="text-muted text-size-small hidden-xs">Установите свой шрифт, воспользовавшись сервисом
                        <a target="_blank" href="https://fonts.google.com/">https://fonts.google.com/</a>
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="iex_interface_design_font_family" rows="5">{{ iEXSetting('iex_interface_design_font_family', null) }}</textarea>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Загрузить свой стиль (Стандартный)</h6>
                    <span class="text-muted text-size-small hidden-xs">Загрузите свой стандартный стиль дизайна, примеры доступны в документации</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="iex_interface_design_exchange" rows="5">{{ iEXSetting('iex_interface_design_exchange', null) }}</textarea>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Загрузить свой стиль (Темный)</h6>
                    <span class="text-muted text-size-small hidden-xs">Загрузите свой темный стиль дизайна, примеры доступны в документации</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <textarea class="form-control" name="iex_interface_dark_design_exchange" rows="5">{{ iEXSetting('iex_interface_dark_design_exchange', null) }}</textarea>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Custom название "Ввод данных"</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка отображения блоков</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="iex_interface_text_entry" data-width="300px">
                        <option value="0" @if(iEXSetting('iex_interface_text_entry') == 0) selected @endif>Ввод данных</option>
                        <option value="1" @if(iEXSetting('iex_interface_text_entry') == 1) selected @endif>Меняем "Валюта" на "Валюта"</option>
                    </select>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Стиль шапки сайта</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка стиль отображения шапки сайта</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="iex_interface_header_style" data-width="300px">
                        <option value="0" @if(iEXSetting('iex_interface_header_style') == 0) selected @endif>Динамическое</option>
                        <option value="1" @if(iEXSetting('iex_interface_header_style') == 1) selected @endif>Фиксированное</option>
                    </select>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Стиль сайта</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка стиль сайта по умолчанию</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="iex_interface_dynamic_colors" data-width="300px">
                        <option value="0" @if(iEXSetting('iex_interface_dynamic_colors') == 0) selected @endif>По умолчанию</option>
                        <option value="1" @if(iEXSetting('iex_interface_dynamic_colors') == 1) selected @endif>Светлое</option>
                        <option value="2" @if(iEXSetting('iex_interface_dynamic_colors') == 2) selected @endif>Темное</option>
                    </select>
                </td>
            </tr>

        </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="tech_break">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Технический перерыв (Название)</h6>
                    <span class="text-muted text-size-small hidden-xs">
                       Укажите заголовок для отображения в техническом перерыве
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="tech_breach_title" keyName="tech_breach_title" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Технический перерыв (Описание)</h6>
                    <span class="text-muted text-size-small hidden-xs">
                       Укажите текст для отображения в техническом перерыве
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.textarea tabName="tech_breach_text" keyName="tech_breach_text" />
                </td>
            </tr>
        </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="hello">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Заголовок</h6>
                    <div class="text-muted text-size-small hidden-xs">
                        Укажите название приветствия
                    </div>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="welcome_title" keyName="welcome_title" />
                </td>
            </tr>

            <tr>
                <td class="col-md-5">

                    <h6 class="media-heading text-semibold">Описание для сайта:</h6>
                    <div class="text-muted text-size-small hidden-xs">
                        Введите описание для сайта на главной страницы
                    </div>
                </td>
                <td class="col-md-7">
                    <x-forms.language.textarea-ckeditor ckeditorId="ckeditor" tabName="welcome_description" keyName="welcome_description" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отображать описание</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Включение и отключение описании сайта на главной странице
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="SwitchOptionPrimary7-5" name="visible_description" type="checkbox" value="1" @if(iEXSetting('visible_description') == 1) checked @endif />
                        <label for="SwitchOptionPrimary7-5" class="label-primary"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Название для шапки</h6>
                    <div class="text-muted text-size-small hidden-xs">
                        Укажите название, которая будет отображаться внизу шапки сайта
                    </div>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="main_title_header" keyName="main_title_header" />
                </td>
            </tr>

            <tr>
                <td class="col-md-5">

                    <h6 class="media-heading text-semibold">Краткий текст для шапки:</h6>
                    <div class="text-muted text-size-small hidden-xs">
                        Введите краткое описание, которая будет отображаться внизу шапки сайта
                    </div>
                </td>
                <td class="col-md-7">
                    <x-forms.language.textarea tabName="main_value_header" keyName="main_value_header" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отображать информацию на сайте</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Включение и отключение отображения информации на сайте
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="visible_main_header_title" name="visible_main_header_title" type="checkbox" value="1" @if(iEXSetting('visible_main_header_title') == 1) checked @endif />
                        <label for="visible_main_header_title" class="label-primary"></label>
                    </div>
                </td>
            </tr>

        </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="image">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Favicon и Иконки сайта</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Загрузите иконку в формате png и размер не ниже 200x200
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="file" name="favicon" class="form-control">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Логотип</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Загрузите логотип для отображения на пользовательской странице.<br/>
                        Доступные любые форматы изображений.
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5 multi-language-form-group">

                    <div class="control-label-br" layout="row">
                        <span flex></span>
                        <ul class="nav nav-pills lang-tabs">
                            @foreach(['Default', 'Dark'] as $key_image => $value_image)
                                <li @if($key_image == 0) class="active" @endif>
                                    <a class="lang-tabs-link" data-toggle="tab" href="#image-{{ $value_image }}">
                                        {{ $value_image }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div id="image-Default" class="tab-pane fade in active">
                        <input type="file" name="logotype" class="form-control" accept="image/*">
                    </div>

                    <div id="image-Dark" class="tab-pane fade in">
                        <input type="file" name="logotype_dark" class="form-control" accept="image/*">
                    </div>

                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Текстовый логотип</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Укажите текст который будет отображен вместо изображения
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="logotype_text" class="form-control" value="{{ iEXSetting('logotype_text') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Тип отображения логотипа</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Выберите из списка тип отображения логотипа
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="logotype_type">
                        <option value="0" @if(iEXSetting('logotype_type') == 0) selected @endif>Изображение (Стандарт)</option>
                        <option value="1" @if(iEXSetting('logotype_type') == 1) selected @endif>Текстовый логотип</option>
                    </select>

                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Логотип (мобильный)</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Загрузите мобильный логотип для отображения на пользовательской странице<br />
                        Доступные любые форматы изображений.
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="file" name="logotype_mobile" class="form-control">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Текстовый логотип (мобильная)</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Укажите мобильный текст который будет отображен вместо изображения
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="logotype_text_mobile" class="form-control" value="{{ iEXSetting('logotype_text_mobile') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Тип отображения логотипа  (мобильная)</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Выберите из списка тип отображения логотипа для мобильных версий
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="logotype_type_mobile">
                        <option value="0" @if(iEXSetting('logotype_type_mobile') == 0) selected @endif>Изображение (Стандарт)</option>
                        <option value="1" @if(iEXSetting('logotype_type_mobile') == 1) selected @endif>Текстовый логотип</option>
                    </select>

                </td>
            </tr>

        </table>
    </div>
</div>
