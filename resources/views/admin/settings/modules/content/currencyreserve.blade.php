<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="settings_currency">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Тип выбора валют</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка тип выбора валют.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="type_selected_currency" data-width="300">
                        <option value="0" @if(iEXSetting('type_selected_currency') == 0) selected @endif>При нажатии (Стандарт)</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отображение фильтров валют</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, активируется фильтрация валют</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="allow_filter_currency" name="allow_filter_currency" type="checkbox" value="1" @if(iEXSetting('allow_filter_currency') == 1) checked @endif />
                        <label for="allow_filter_currency" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Стиль отображения фильтров валют</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка стиль отображения фильтров</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="allow_filter_currency_select" data-width="300">
                        <option value="0" @if(iEXSetting('allow_filter_currency_select') == 0) selected @endif>Фильтр валют внутри поиска</option>
                        <option value="1" @if(iEXSetting('allow_filter_currency_select') == 1) selected @endif>Отдельный фильтр валют</option>
                        <option value="2" @if(iEXSetting('allow_filter_currency_select') == 2) selected @endif>Группирование валют</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Настройка отображения валют для стиля "Группирование валют"') }}:</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Укажите кол-во отображения валют по умолчанию') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="count_individual_num_currency" name="count_individual_num_currency" value="{{ iEXSetting('count_individual_num_currency', 2) }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ 'Скрыть кнопку "Показать все валюты" для стиля "Группирование валют"' }}</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, и если выбрано групповое отображение валют, кнопка "Показать все валюты" будет отключена</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_disabled_group_filter_direction" name="is_disabled_group_filter_direction" type="checkbox" value="1" @if(iEXSetting('is_disabled_group_filter_direction') == 1) checked @endif />
                        <label for="is_disabled_group_filter_direction" class="label-primary"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Вид отображения валют</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из подходящий вид отображения блоков "Отдаю" и "Получаю".</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="currency_display_type_dynamic" data-width="300">
                        <option value="0" @if(iEXSetting('currency_display_type_dynamic') == 0) selected @endif>По умолчанию</option>
                        <option value="1" @if(iEXSetting('currency_display_type_dynamic') == 1) selected @endif>Компактный (для больших списков)</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить скрывать валюты в которых нет резерва</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                                При включении данной настройки, при выборе валют клиенту будет недоступно направление в котором недостаточно резерва
                                            </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_hide_currency_reserve" name="is_hide_currency_reserve" type="checkbox" value="1" @if(iEXSetting('is_hide_currency_reserve') == 1) checked @endif />
                        <label for="is_hide_currency_reserve" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить отображение сумм обмена в USD</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной функции, на главной странице сумм обмена рядом будет отображаться всегда доп. сумма обменв USD.
                            <br/>
                            <b class="text-danger">Внимание!!!</b> Перед тем как включить данную функцию, проверьте, есть ли у вас все пары к USD в модуле "Работа парсеров"
                        .</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_convert_exchange_to_usd" name="is_enabled_convert_exchange_to_usd" type="checkbox" value="1" @if(iEXSetting('is_enabled_convert_exchange_to_usd') == 1) checked @endif />
                        <label for="is_enabled_convert_exchange_to_usd" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Выбранные валюты, при загрузке страницы обмена</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка способ загрузки главного направления.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker form-control" name="currency_exchange_selected_default">
                        <option value="0" @if(iEXSetting('currency_exchange_selected_default') == 0) selected @endif>Сохраненные (рекомендуется)</option>
                        <option value="1" @if(iEXSetting('currency_exchange_selected_default') == 1) selected @endif>Из выбранного направления ниже</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Показывать по умолчанию, направление обмена (для клиентов)</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка направление, которую хотите использовать как главную.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker form-control" name="id_direction_exchange_main" data-size="8" data-live-search="true">
                        @foreach($exchange_main as $item)
                            <option value="{{ $item['id'] }}" @if($item['is_main'] == 1) selected @endif data-subtext="@if($item['status'] == 1) Включена @else Отключена @endif">{{ $item['name'] }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Способ загрузки сумм обмена по умолчанию</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка способ загрузки главного направления.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker form-control" name="currency_exchange_selected_price">
                        <option value="0" @if(iEXSetting('currency_exchange_selected_price') == 0) selected @endif>Автоматически (рекомендуется)</option>
                        <option value="1" @if(iEXSetting('currency_exchange_selected_price') == 1) selected @endif>Из настроек поля "конвертировать по"</option>
                        <option value="2" @if(iEXSetting('currency_exchange_selected_price') == 2) selected @endif>Мин. сумма обмена</option>
                    </select>
                </td>
            </tr>
        </table>
    </div>


    <div role="tabpanel" class="tab-pane" id="settings_reserve">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить оповещение об окончании резервов</h6>
                    <span class="text-muted text-size-small hidden-xs">В случае включения данной настройки, в соотвествии с настройками на выбранные сервисы будут отправлены уведомления.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_reserve_threshold" name="is_enabled_reserve_threshold" type="checkbox" value="1" @if(iEXSetting('is_enabled_reserve_threshold') == 1) checked @endif />
                        <label for="is_enabled_reserve_threshold" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить автоматический отчет по резервам</h6>
                    <span class="text-muted text-size-small hidden-xs">В случае включении данной настройки, каждый день будет создаваться отчет по резервам и заявкам.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_auto_reserves_report" name="is_auto_reserves_report" type="checkbox" value="1" @if(iEXSetting('is_auto_reserves_report') == 1) checked @endif />
                        <label for="is_auto_reserves_report" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Активировать неограниченный резерв</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то пользователи могут создать заявки не зависимо от суммы резерва</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="unlimited_reserve" name="unlimited_reserve" type="checkbox" value="1" @if(iEXSetting('unlimited_reserve') == 1) checked @endif />
                        <label for="unlimited_reserve" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            {{--<tr>--}}
                {{--<td class="col-xs-6 col-sm-6 col-md-7">--}}
                    {{--<h6 class="media-heading text-semibold">Неограниченный резерв (валюты)</h6>--}}
                    {{--<span class="text-muted text-size-small hidden-xs">--}}
                            {{--Выбираем валюты которым будет доступен неограниченный резерв--}}
                        {{--</span>--}}
                {{--</td>--}}
                {{--<td class="col-xs-6 col-sm-6 col-md-5">--}}
                    {{--<select class="selectpicker form-control" name="unlimited_reserve_currency[]"--}}
                            {{--multiple data-actions-box="true" data-selected-text-format="count" data-live-search="true" data-width="250px">--}}
                        {{--@foreach($currencies as $currency)--}}
                            {{--<option value="{{ $currency->id  }}"  @if($currency->unlimited_reserve == 1) selected @endif>--}}
                                {{--{{ $currency->payment->name  }}--}}
                                {{--{{ $currency->code_currency->name  }}--}}
                            {{--</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</td>--}}
            {{--</tr>--}}

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Не превышать получаемую суммы выше резерва</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                                При включении данной настройки, клиент не сможет вводить сумму выше установленного резерва
                                            </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_not_exceed_amount_reserve" name="is_not_exceed_amount_reserve" type="checkbox" value="1" @if(iEXSetting('is_not_exceed_amount_reserve') == 1) checked @endif />
                        <label for="is_not_exceed_amount_reserve" class="label-primary"></label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить округление резервов</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, резервы будут отображаться (1 млн, 10 тыс)...</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_reserves_rounding" name="is_reserves_rounding" type="checkbox" value="1" @if(iEXSetting('is_reserves_rounding') == 1) checked @endif />
                        <label for="is_reserves_rounding" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить отображение резервов</h6>
                    <span class="text-muted text-size-small hidden-xs">Включение и отключение резервов на главной странице</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="block_visible_reserve" name="block_visible_reserve" type="checkbox" value="1" @if(iEXSetting('block_visible_reserve') == 1) checked @endif />
                        <label for="block_visible_reserve" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Вид отображения резервов</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка вид отображения резервов</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker" name="template_block_reserve">
                        <option value="0" @if(iEXSetting('template_block_reserve') == 0) selected @endif>Стандартный</option>
                        <option value="1" @if(iEXSetting('template_block_reserve') == 1) selected @endif>С прокруткой</option>
                        <option value="2" @if(iEXSetting('template_block_reserve') == 2) selected @endif>Усовершенствованный</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Настройка отображения резервов для стиля "Усовершенствованный"</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите кол-во вертикальных столбцов для компактного отображения резервов в усовершенствованном стиле. <br />
                        <div style="color: red">Рекомендуемое значение: 3</div>
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="count_template_block_reserve_2" name="count_template_block_reserve_2" value="{{ iEXSetting('count_template_block_reserve_2', 3) }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Разрешить отображение резервов и курсов на главном</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                                При включении данной настройки, на главной странице выбора валюты будут 2 вкладки (Курс и Резервы)
                                            </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_out_course_reserve" name="is_enabled_out_course_reserve" type="checkbox" value="1" @if(iEXSetting('is_enabled_out_course_reserve') == 1) checked @endif />
                        <label for="is_enabled_out_course_reserve" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить отображение уведомление в случае недостаточности резерве</h6>
                    <span class="text-muted text-size-small hidden-xs">
                                                При включении данной настройки, в случае если несколько клиентов одновременно открывают модальное окно и после того как один закончил оплату и нажал кнопку "Я оплатил" другой клиент получит уведомление о нехватки резерва.
                                            </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_reserve_shortage_notice" name="is_reserve_shortage_notice" type="checkbox" value="1" @if(iEXSetting('is_reserve_shortage_notice') == 1) checked @endif />
                        <label for="is_reserve_shortage_notice" class="label-primary"></label>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Скрыть отображение резервов на главном</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите из списка валюты, резервы которых не будут отображены на главной странице.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    {{ Form::select('ids_currencies_reserves_home_view[]', $currencies_reserves, explode(',', iEXSetting('ids_currencies_reserves_home_view')), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '8', 'multiple', 'data-width' => '300px']) }}
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отключить отображение резервов  в списке "Получаю"</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной функции, в стандартной версии при выборе валют "Получаю", отображаемый резерв справа от названия валюты будет отключена</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_web_hidden_reserve_out" name="is_web_hidden_reserve_out" type="checkbox" value="1" @if(iEXSetting('is_web_hidden_reserve_out') == 1) checked @endif />
                        <label for="is_web_hidden_reserve_out" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отключить отображение резервов в мобильной версии в списке "Получаю"</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной функции, в мобильной версии при выборе валют "Получаю", отображаемый резерв справа от названия валюты будет отключена</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_mobile_hidden_reserve_out" name="is_mobile_hidden_reserve_out" type="checkbox" value="1" @if(iEXSetting('is_mobile_hidden_reserve_out') == 1) checked @endif />
                        <label for="is_mobile_hidden_reserve_out" class="label-primary"></label>
                    </div>
                </td>
            </tr>

        </table>
    </div>
</div>
