<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="bslist">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Включить базу мошенников') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('При включении данной настройки, скрипт будет автоматически искать мошенников по IP, Email. Эту функцию включите в случае если у вас имеется ключ и id в bestchange.') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_bestchange_blacklist" name="is_bestchange_blacklist" type="checkbox" value="1" @if(iEXSetting('is_bestchange_blacklist') == 1) checked @endif />
                        <label for="is_bestchange_blacklist" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Выберите способ запроса к данным') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __('Выберите из списка пункт, с которого будет производиться поиск данных') }}
                        <br/>
                        <span class="text-danger">BestChange:</span> {{ __('Этот параметр выберите в случае если ваш обменник находится в списке BestChange') }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="bestchange_bs_data">
                        <option value="0" @if(iEXSetting('bestchange_bs_data') == 0) selected @endif>{{ __('BestChange') }}</option>
                        <option value="1" @if(iEXSetting('bestchange_bs_data') == 1) selected @endif>{{ __('Локальная база') }}</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Bestchange ID') }}:</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Введите логин вашего аккаунта в bestchange') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="bestchange_api_id" name="bestchange_api_id" value="{{ iEXSetting('bestchange_api_id') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Bestchange Key') }}:</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Введите API KEY который указан в разделе мошенники') }}.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="bestchange_api_key" name="bestchange_api_key" value="{{ iEXSetting('bestchange_api_key') }}">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Критерий поиска') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите критерий поиска мошенников') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    {!! Form::select('bestchange_api_categories[]', $config['categories'], explode(',', iEXSetting('bestchange_api_categories')),
                    ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '300']) !!}
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Типы колонок') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите колонку в котором необходимо искать ключевые параметры') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    {!! Form::select('bestchange_api_columns', $config['columns'], iEXSetting('bestchange_api_columns'),
                   ['class' => 'form-control selectpicker', 'data-width' => '300']) !!}
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Тип поиска мошенников') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите порядок фильтрации базы') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    {!! Form::select('bestchange_api_type', $config['types'], iEXSetting('bestchange_api_type'),
                   ['class' => 'form-control selectpicker', 'data-width' => '300']) !!}
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Разрешить создании заявок') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __("Eсли 'Включено', то клиенты которые были найдены в списке мошенников могут создавать заявки.") }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="bestchange_allow_order" name="bestchange_allow_order" type="checkbox" value="1" @if(iEXSetting('bestchange_allow_order') == 1) checked @endif />
                        <label for="bestchange_allow_order" class="label-primary"></label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
