<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="general">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Служба отправки СМС</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, позволяет отправлять уведомления на номер телефона</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="selectpicker form-control" id="SMS_DRIVER" name="SMS_DRIVER" data-live-search="true">
                        @foreach(config('admin-settings.sms-list') as $key => $driver)
                            <option value="{{ $key }}" @if(config('crypto.sms_driver') == $key) selected @endif>{{ $driver }}</option>
                        @endforeach
                    </select>

                    <div style="margin-top: 10px;">
                        <a href="#settingsSMS" data-toggle="modal">Настроить SMS службы</a>
                    </div>
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить SMS уведомление</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, позволяет отправлять уведомления на номер телефона</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="enable_sms_notification" name="enable_sms_notification" type="checkbox" value="1" @if(iEXSetting('enable_sms_notification') == 1) checked @endif />
                        <label for="enable_sms_notification" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить SMS уведомление при входе в админ-панель</h6>
                    <span class="text-muted text-size-small hidden-xs">При включении данной настройки, позволяет отправлять уведомления на номер телефона перед входом в панель управления</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="enable_admin_sms_notification" name="enable_admin_sms_notification" type="checkbox" value="1" @if(iEXSetting('enable_admin_sms_notification') == 1) checked @endif />
                        <label for="enable_admin_sms_notification" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Системный Номер телефона:</h6>
                    <span class="text-muted text-size-small hidden-xs">Введите системный номер телефона. Этот номер необходим для получения служебных уведомлений о новых заявках и.т.д</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" class="form-control" id="sms_phone_number_system" name="sms_phone_number_system" value="{{ iEXSetting('sms_phone_number_system') }}">
                </td>
            </tr>
        </table>
    </div>

    <div role="tabpanel" class="tab-pane active" id="notification">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать SMS уведомление при получении новых заявок</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при создании новой заявки с сайта, на системный номер телефона указанный в настройках будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="sms_new_order" name="sms_new_order" type="checkbox" value="1" @if(iEXSetting('sms_new_order') == 1) checked @endif />
                        <label for="sms_new_order" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать SMS уведомление при успешном выполнении заявки</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при успешном выполнении заявки, на Номер телефона указанный в профиле клента будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="sms_notify_success_order" name="sms_notify_success_order" type="checkbox" value="1" @if(iEXSetting('sms_notify_success_order') == 1) checked @endif />
                        <label for="sms_notify_success_order" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Отсылать SMS уведомление при успешном выплате вознаграждений</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', при успешном выполнении заявки о выплате бонусных вознаграждений, на Номер телефона указанный в профиле клента будет отправлено соответствующее уведомление.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="sms_notify_success_payouts" name="sms_notify_success_payouts" type="checkbox" value="1" @if(iEXSetting('sms_notify_success_payouts') == 1) checked @endif />
                        <label for="sms_notify_success_payouts" class="label-success"></label>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div role="tabpanel" class="tab-pane active" id="template">
        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Привязать к SMS сообщениям название сервиса</h6>
                    <span class="text-muted text-size-small hidden-xs">Eсли 'Включено', то к SMS сообщениям будет привязан префикс в виде название сервиса. Пример: {{ iEXContentLanguage('sitename') }}: текст сообщения</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="sms_message_prefix_website" name="sms_message_prefix_website" type="checkbox" value="1" @if(iEXSetting('sms_message_prefix_website') == 1) checked @endif />
                        <label for="sms_message_prefix_website" class="label-success"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">SMS сообщение при успешном выполнении заявки</h6>
                    <span class="text-muted text-size-small hidden-xs">Введите текст SMS сообщения который получит пользователь при успешном выполнении заявки</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="sms_message_success_order" value="{{ iEXSetting('sms_message_success_order', 'Заявка №:id успешно выполнено.') }}" class="form-control">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">SMS сообщение при успешном выплате вознаграждений</h6>
                    <span class="text-muted text-size-small hidden-xs">Введите текст SMS сообщения который получит пользователь при успешном выплате бонусных вознаграждений</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="sms_message_success_payouts" value="{{ iEXSetting('sms_message_success_payouts', 'Заявка №:id на выплату вознаграждений успешно выполнено.') }}" class="form-control">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">SMS сообщение при получении новых заявок (для админов)</h6>
                    <span class="text-muted text-size-small hidden-xs">Введите текст SMS сообщения который получит оператор при поступлении новой заявки</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="sms_message_success_order_admin" value="{{ iEXSetting('sms_message_success_order_admin', 'Поступила новая заявка №:id') }}" class="form-control">
                </td>
            </tr>
        </table>
    </div>
</div>

<!-- Модальное окно для настроек -->
<div class="modal fade" id="settingsSMS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" >

        <div class="modal-content">
            <div class="modal-header ui-dialog-titlebar">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <span class="ui-dialog-title" id="myModalLabel">Настройки SMS Служб</span>
            </div>
            <div class="modal-body">

                @foreach(config('admin-settings.sms-fields') as $field_name)
                    <h4 style="text-align: center;padding-top: 10px;">{{ $field_name['name'] }}</h4>
                    <div class="sms-config-fields">
                        @foreach($field_name['fields'] as $field_value)
                            <div class="form-group">
                                <label class="control-label col-md-6">{{ $field_value['name'] }}</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="{{ $field_value['key'] }}" value="{{ config($field_value['config']) }}">
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
            <div class="modal-footer">
                <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                <md-button type="button" data-dismiss="modal">Отмена</md-button>
            </div>
        </div>
    </div>
</div>
