<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="general">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Драйвер обработки загружаемых изображений</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Выберите из списка драйвер изображений, Если вы желаете переключиться на Imagick убедитесь что она установлена у вас на сервере
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="IMAGE_DRIVER">
                        <option value="" @if(config('image.driver') == '') selected @endif>Автоматически</option>
                        <option value="imagick"  @if(config('image.driver') == 'imagick') selected @endif>Imagick</option>
                        <option value="gd"  @if(config('image.driver') == 'gd') selected @endif>GD</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Конвертировать все загружаемые изображения в единый формат изображений</h6>
                    <span class="text-muted text-size-small hidden-xs">Вы можете включить автоматическое конвертирование изображений при их загрузке на сервер, в единый формат. При включении рекомендуется выбирать более современный формат, например WEBP, для экономии трафика и ускорения загрузки страниц вашего сайта.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="images_converted_to">
                        <option value="">Отключено</option>
                        <option value="png" @if(iEXSetting('images_converted_to') == 'png') selected @endif>PNG</option>
                        <option value="jpg" @if(iEXSetting('images_converted_to') == 'jpg') selected @endif>JPG</option>
                        <option value="webp" @if(iEXSetting('images_converted_to') == 'webp') selected @endif>WEBP</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Максимально допустимые размеры оригинального изображения</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Существует две возможности использования данной настройки:
                        <br><br>
                        <b>Первая:</b> Вы вводите допустимые размеры в пикселях любой из сторон оригинального изображения.
                        Например: <b>500</b>.
                        <br><br><b>Вторая:</b>
                        Вы задаете ширину и высоту оригинального изображения в формате ширина x высота. Например: <b>500x200</b>
                        <br><br>
                        Если размер будет больше, то оригинальное изображение будет автоматически уменьшено до указанного размера, иначе изображение будет пережато без изменения размера.
                        Вы можете указать 0, если хотите чтобы изображение оставалось оригинальным.
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="images_max_up_side" value="{{ iEXSetting('images_max_up_side', 0) }}" class="form-control" style="text-align:center;max-width:150px">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Качество сжатия JPEG и WEBP изображений</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Качество сжатия JPEG и WEBP изображений при копировании на сервер. Выставляется значение от 0 до 100. Чем больше значение, тем лучше качество изображений, но также больше и их вес на сервере.
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="image_quality" value="{{ iEXSetting('image_quality', 85) }}" class="form-control" style="text-align:center;max-width:150px">
                </td>
            </tr>
        </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="cloud">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Место хранения изображений платежных систем') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите в каком хранилище будут сохраняться изображения платежных системы при их загрузки на сервер.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="storage_disk_payment_system">
                        <option value="">{{ __('По умолчанию') }}</option>
                        @foreach($storages_files as $item)
                            <option value="{{ $item->disk }}"  @if(iEXSetting('storage_disk_payment_system') == $item->disk) selected @endif>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Место хранения изображений верификации карт') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите в каком хранилище будут сохраняться изображения верификации карт клиентов при их загрузки на сервер.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="storage_disk_verification_card">
                        <option value="">{{ __('По умолчанию') }}</option>
                        @foreach($storages_files as $item)
                            <option value="{{ $item->disk }}" @if(iEXSetting('storage_disk_verification_card') == $item->disk) selected @endif>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Место хранения изображений новостей') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите в каком хранилище будут сохраняться изображения новостей при их загрузки на сервер.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="storage_disk_news">
                        <option value="">{{ __('По умолчанию') }}</option>
                        @foreach($storages_files as $item)
                            <option value="{{ $item->disk }}" @if(iEXSetting('storage_disk_news') == $item->disk) selected @endif>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
        </table>
    </div>
</div>
