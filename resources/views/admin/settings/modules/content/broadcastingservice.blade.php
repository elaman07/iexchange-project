<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="general">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить модуль</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        В случае включения данной настройки, клиенты будут получения уведомления в реальном времени.
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_module_socket" name="is_enabled_module_socket" type="checkbox" value="1" @if(iEXSetting('is_enabled_module_socket') == 1) checked @endif />
                        <label for="is_enabled_module_socket" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Драйвера Broadcaster</h6>
                    <span class="text-muted text-size-small hidden-xs">Выберите драйвер для активации модуля "вещания событий".</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="BROADCAST_DRIVER" data-width="300px">
                        <option value="pusher" @if(config('broadcasting.default') == 'pusher') selected @endif>Pusher</option>
                        <option value="redis" @if(config('broadcasting.default') == 'redis') selected @endif>Redis</option>
                        <option value="log" @if(config('broadcasting.default') == 'log') selected @endif>Log</option>

                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Pusher App ID</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите app_id из сервиса pusher.com.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="PUSHER_APP_ID" value="{{ config('broadcasting.connections.pusher.app_id') }}" class="form-control" style="width: 300px;">
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Pusher App Key</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите key из сервиса pusher.com.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="PUSHER_APP_KEY" value="{{ config('broadcasting.connections.pusher.key') }}" class="form-control" style="width: 300px;">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Pusher App Secret</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите secret из сервиса pusher.com.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="PUSHER_APP_SECRET" value="{{ config('broadcasting.connections.pusher.secret') }}" class="form-control" style="width: 300px;">
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Pusher App Cluster</h6>
                    <span class="text-muted text-size-small hidden-xs">Укажите cluster из сервиса pusher.com.</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" name="PUSHER_APP_CLUSTER" value="{{ config('broadcasting.connections.pusher.cluster') }}" class="form-control" style="width: 300px;">
                </td>
            </tr>
        </table>
    </div>
    <div role="tabpanel" class="tab-pane" id="notification">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Краткий текст после успешного обмена</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Укажите краткий текст сообщения для клиента
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.textarea tabName="s_order_notify_text" keyName="s_order_notify_text" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить отображение окна успешного завершения заявки</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Если 'Включено', то при успешном завершении заявки, клиент получится уведомление в модальном окне в режиме реального времени
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="iex_modal_success_order" name="iex_modal_success_order" type="checkbox" value="1" @if(iEXSetting('iex_modal_success_order') == 1) checked @endif />
                        <label for="iex_modal_success_order" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">Включить PUSH уведомление об успешной завершении заявки</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        Если 'Включено', то при успешном завершении заявки, клиент получится PUSH уведомление в правом нижнем углу
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="iex_snackbar_success_order" name="iex_snackbar_success_order" type="checkbox" value="1" @if(iEXSetting('iex_snackbar_success_order') == 1) checked @endif />
                        <label for="iex_snackbar_success_order" class="label-primary"></label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
