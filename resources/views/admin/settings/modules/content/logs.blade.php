<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="logs">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление лога подтверждений (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_confirmation_clear" value="{{ iEXSetting('settings_log_confirmation_clear', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление лога старых статусов заявок (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_old_status_clear" value="{{ iEXSetting('settings_log_old_status_clear', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление логов автоматических выплат (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_autopayment_clear" value="{{ iEXSetting('settings_log_autopayment_clear', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление логов проверки оплата (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_checkpayment_clear" value="{{ iEXSetting('settings_log_checkpayment_clear', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление логов направление обменов (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_direction_e_clear" value="{{ iEXSetting('settings_log_direction_e_clear', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Архивация партнерских переходов (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_partners_transition_clear" value="{{ iEXSetting('settings_log_partners_transition_clear', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление лога неверифицированных карт (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_unverified_card" value="{{ iEXSetting('settings_log_unverified_card', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Удаление лога запросов к источникам курсов (дней)') }}</h6>
                    <span class="text-muted text-size-small hidden-xs"></span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <input type="text" style="width: 150px;text-align: center" class="form-control" name="settings_log_parser_http" value="{{ iEXSetting('settings_log_parser_http', 30) }}">
                    <div class="small-text-1 text-muted">0 - {{ __('отключает ограничения') }}</div>
                </td>
            </tr>
        </table>
    </div>
</div>
