<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="chat">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Включить онлайн чат') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __('В случае включения данной настройки, на главной странице будет отображен онлайн чат для клиентов') }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_online_chat" name="is_enabled_online_chat" type="checkbox" value="1" @if(iEXSetting('is_enabled_online_chat') == 1) checked @endif />
                        <label for="is_enabled_online_chat" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Тип онлайн чата') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите онлайн чат для поддержки клиентов') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="chat_type" data-size="5">
                        <option value="0" @if(iEXSetting('chat_type') == 0) selected @endif>-- {{ __('Не выбрано') }} --</option>
                        @foreach(config('chat.lists') as $key => $value)
                            <option value="{{$key}}" @if(iEXSetting('chat_type') == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('App ID') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Идентификатор приложения которую вы получили после регистрации в онлайн чате.') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="chat_app_id" keyName="chat_app_id" />
                </td>
            </tr>
        </table>
    </div>

    <div role="tabpanel" class="tab-pane active" id="chat-order">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Включить чат') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __('В случае включения данной настройки, в каждой заявки клиенты могут начать диалог с оператором') }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enable_order_online_chat" name="is_enable_order_online_chat" type="checkbox" value="1" @if(iEXSetting('is_enable_order_online_chat') == 1) checked @endif />
                        <label for="is_enable_order_online_chat" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Для каких заявок отображать чат') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Выберите из списка статусы заявок которые будут доступны для создания диалога с операторами') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    {!! Form::select('chat_displayed_statuses[]', $statuses, explode(',', iEXSetting('chat_displayed_statuses')),
                        ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '300px', 'data-live-search' => 'true', 'data-size' => '8']) !!}
                </td>
            </tr>


            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Включить уведомление операторам в Telegram') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __('В случае включения данной настройки, после каждого нового сообщения от клиентов, менеджеры будут получать сообщение в Telegram канал') }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="is_enabled_online_chat_tg_notify" name="is_enabled_online_chat_tg_notify" type="checkbox" value="1" @if(iEXSetting('is_enabled_online_chat_tg_notify') == 1) checked @endif />
                        <label for="is_enabled_online_chat_tg_notify" class="label-primary"></label>
                    </div>
                </td>
        </table>
    </div>


    <div role="tabpanel" class="tab-pane active" id="chat-jivosite">
        <table class="table table-2 table-striped">
            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Разрешить отправить сообщение в чат о новой заявке') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __('Выберите из список способ отправки сообщения в чат JivoSite') }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="jivosite_type_message" data-size="5">
                        <option value="0" @if(iEXSetting('jivosite_type_message') == 0) selected @endif>-- {{ __('Не отправлять') }} --</option>
                        <option value="1" @if(iEXSetting('jivosite_type_message') == 1) selected @endif>{{ __('При создании заявки') }}</option>
                        <option value="2" @if(iEXSetting('jivosite_type_message') == 2) selected @endif>{{ __('При нажатии кнопки "Я оплатил"') }}</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Текст сообщения о новой заявке') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('Укажите текст сообщения, которая будет отправлять в чат') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <x-forms.language.input tabName="jivosite_text_message" keyName="jivosite_text_message" />
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('Разрешить отправить информацию о клиенте в чат') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">
                        {{ __('При включении данной функции, в форму чата будут автоматически проставлены данные о клиенте') }}
                    </span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="jivosite_info_user" name="jivosite_info_user" type="checkbox" value="1" @if(iEXSetting('jivosite_info_user') == 1) checked @endif />
                        <label for="jivosite_info_user" class="label-primary"></label>
                    </div>
                </td>
            </tr>

        </table>
    </div>

</div>
