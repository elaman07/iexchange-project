@extends('admin.layouts.app')

@section('title', 'Настроить SMS службы')

@section('breadcrumbs', Breadcrumbs::render('admin.settings.sms'))

@section('content')

    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="exchange_rate1" class="col-sm-2 control-label">Конфигурация драйверов</label>
                <div class="col-sm-10">
                    <!-- Настройки NEXMO Драйвера -->
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Nexmo API Key</div>
                        <input type="text" name="nexmo_api_sms_key" class="form-control" id="nexmo_api_sms_key" value="{{ config('services.nexmo.key') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Nexmo API Secret</div>
                        <input type="text" name="nexmo_api_sms_secret" class="form-control" id="NEXMO_API_SECRET" value="{{ config('services.nexmo.secret') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Nexmo Номер отправителя</div>
                        <input type="text" name="nexmo_api_sms_from" class="form-control" id="nexmo_api_sms_from" value="{{ config('services.nexmo.sms_from') }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <!-- Настройки SMSCRu Драйвера -->
                    <div class="form-group col-md-4">
                        <div class="control-label-br">SMSC Login</div>
                        <input type="text" name="smsc_sms_login" class="form-control" id="smsc_sms_login" value="{{ config('services.smscru.login') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">SMSC Password</div>
                        <input type="text" name="smsc_sms_password" class="form-control" id="smsc_sms_password" value="{{ config('services.smscru.secret') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">SMSC Имя отправителя</div>
                        <input type="text" name="smsc_sms_from" class="form-control" id="smsc_sms_from" value="{{ config('services.smscru.sender') }}">
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <!-- Настройки SMSCRu Драйвера -->
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Twilio username</div>
                        <input type="text" name="twilio_username" class="form-control" id="twilio_username" value="{{ config('services.twilio.username') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Twilio password</div>
                        <input type="text" name="twilio_password" class="form-control" id="twilio_password" value="{{ config('services.twilio.password') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Twilio Auth token</div>
                        <input type="text" name="twilio_auth_token" class="form-control" id="twilio_auth_token" value="{{ config('services.twilio.auth_token') }}">
                    </div>

                    <div class="form-group col-md-4">
                        <div class="control-label-br">Twilio Account sid</div>
                        <input type="text" name="twilio_account_sid" class="form-control" id="twilio_account_sid" value="{{ config('services.twilio.account_sid') }}">
                    </div>

                    <div class="form-group col-md-4">
                        <div class="control-label-br">Twilio From</div>
                        <input type="text" name="twilio_from" class="form-control" id="twilio_from" value="{{ config('services.twilio.from') }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <!-- Настройки SMSCRu Драйвера -->
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Plivo Auth id</div>
                        <input type="text" name="plivo_auth_id" class="form-control" id="plivo_auth_id" value="{{ config('services.plivo.auth_id') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Plivo Auth token</div>
                        <input type="text" name="plivo_auth_token" class="form-control" id="plivo_auth_token" value="{{ config('services.plivo.auth_token') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">Plivo From number</div>
                        <input type="text" name="plivo_from_number" class="form-control" id="plivo_from_number" value="{{ config('services.plivo.from_number') }}">
                    </div>

                </div>
            </div>

        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/settings/sms') }}">Назад</md-button>
        </div>
    </form>

@endsection