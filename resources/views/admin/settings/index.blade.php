@extends('admin.layouts.app')

@section('title', $selected_setting['title'])

@section('disable-body-wrapper', ' ')

@section('hide-admin-header')

@stop


@section('custom-body-class', 'custom-admin-body-wrapper')

@section('breadcrumbs', Breadcrumbs::render('admin.settings'))

@section('no-block-content')

    <style>
        .setting-sidenav-bg {
            min-height: 100vh;
        }


        .setting-toolbar-menu {
            padding: 0;
            margin: 0;
        }

        .setting-toolbar h1 {
            font-weight: normal;
        }

        .systemsettings .modal-footer {
            position: fixed;
            background: #fff;
            right: 0;
            left: 0;
            z-index: 10;
            bottom: 0;
        }

        .systemsettings .tab-content {
            padding-bottom: 100px;
        }

        .setting-sidenav-menu-footer {
            width: 70%;
            margin: 0 auto;
            padding: 30px 00px;
            color: #999;
            font-size: 14px;
            line-height: 1.4;
            text-align: center;
        }

        .setting-sidenav-wrapper {
            width: 100%;
            padding: 3rem 6rem !important;
        }

        .setting-sidenav-title h2 {
            font-family: Nunito,sans-serif;
        }

        .systemsettings-toggle-menu {
            display:none;
            padding: 10px;
            background: #FFF;
            width: 100%;
        }

        @media (min-width: 100px) and (max-width: 599px)
        {
            .setting-sidenav-wrapper {
                padding: 3rem 1rem !important
            }
        }

        @media (max-width: 768px) {
            .systemsettings .table > tbody > tr > td:first-child {
                padding-bottom: 0;
            }

            .systemsettings .table > tbody > tr > td {
                display: block;
                width: 100%;
            }

            .systemsettings-toggle-menu {
                display:block;
            }

            .setting-sidenav {
                padding-top:60px
            }
        }
    </style>

    <div flex ng-controller="SettingsSidenav as ctrl">

        <md-button ng-click="toggleSettings()" class="systemsettings-toggle-menu">
            <i class="far fa-bars"></i>
        </md-button>


        <div layout="row" class="setting-sidenav-bg">
            <md-sidenav flex="25" class="md-sidenav-left setting-sidenav" md-component-id="settings" md-is-locked-open="$mdMedia('gt-md')" md-whiteframe="4">
                <md-toolbar class="setting-toolbar">
                    <ul  class="setting-toolbar-menu">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <h1 class="md-toolbar-tools">
                                    @yield('title')<span class="caret"></span>
                                </h1>
                            </a>

                            <ul class="dropdown-menu">
                                @foreach(config('admin-settings.categories') as $key => $value)
                                    <li @if($selected_setting['route'] == $key) class="active" @endif>
                                        <a href="?mid={{$key}}"><i class="{{ $value['type'] ?? 'far' }} {{ $value['icon'] }}"></i>{{ $value['title'] }}</a>
                                    </li>
                                    @if(isset($value['line_break']) and $value['line_break'] == true)
                                        <li class="divider"></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </md-toolbar>


                <md-content>
                    <ul class="setting-sidenav-menu" style="padding: 0" id="settingsTab{{$selected_setting['route']}}" >
                        @if(isset($selected_setting['is_module']) and $selected_setting['is_module'] == true)
                            @includeIf($selected_setting['route'].'::settings_nav')
                        @else
                            @includeIf('admin.settings.modules.nav.'.$selected_setting['route'])
                        @endif
                    </ul>

                    <div class="setting-sidenav-menu-footer">
                        {!! __('Нажмите на заголовок <b>«:name»</b>, чтобы получить другие пункты меню', ['name' =>  $selected_setting['title']]) !!}
                    </div>

                </md-content>
            </md-sidenav>


            <!-- Данные настроек -->
            <md-content class="setting-sidenav-wrapper">
                <div class="setting-sidenav-title" style="border: none;margin-top: 5px;">
                    <h2></h2>
                    <div class="clearfix"></div>
                </div>


                <div class="x_content mt-8">
                    <div class="systemsettings margin-b-0">
                        <div class="table-responsives">
                            <form action="?mid={{ $selected_setting['route'] }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                @if(isset($selected_setting['is_module']) and $selected_setting['is_module'] == true)
                                    @includeIf($selected_setting['route'].'::settings_content')
                                @else
                                    @includeIf('admin.settings.modules.content.'.$selected_setting['route'])
                                @endif

                                <div class="modal-footer">
                                    <md-button type="submit" class="md-raised md-primary">{{ __('Сохранить') }}</md-button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </md-content>
        </div>

    </div>

@endsection

@section('js_bottom')
    <script type="text/javascript">
        $(function() {
            $('#settingsTab{{$selected_setting['route']}} a:first').tab('show');
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var id = $(e.target).attr("href").substr(1);
                localStorage.setItem('iEXSettingsTab{{ $selected_setting['route'] }}', id);
            });

            var selectedTab = localStorage.getItem('iEXSettingsTab{{ $selected_setting['route'] }}');
            if (selectedTab) {
                $('#settingsTab{{ $selected_setting['route'] }} a[href="#' + selectedTab + '"]').tab('show');
            }
        });
    </script>
@endsection
