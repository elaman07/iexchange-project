@extends('admin.layouts.app')

@section('title', 'Журнал событий')

@section('breadcrumbs', Breadcrumbs::render('admin.settings.instruments.events'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройка журнала</span>
        </md-button>
    @endcan
@endsection

@section('title-right-button')
    <md-button disabled style="color: #999;opacity: 1;">
        Всего событий: {{ $events->total() }}
    </md-button>
@endsection

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title" layout="row">
            <h2>@yield('title')</h2>
            <span flex=""></span>
            <md-button disabled style="color: #999;opacity: 1;">
                Всего событий: {{ $events->total() }}
            </md-button>
        </div>
        <div class="clearfix"></div>
        <div class="x_content">


            @if(config('admin.is_demo_mode') == true)
                <div class="alert alert-danger">Недоступно в демо режиме</div>
            @else
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>Дата создания</th>
                        <th>Событие</th>
                        <th>Объект</th>
                        <th>IP Адрес</th>
                        <th>URL</th>
                        <th>Пользователь</th>
                        <th>Админ</th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(count($events) > 0)
                        @foreach($events as $log)
                            <tr>
                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($log->created_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($log->created_at)->diffForHumans() }}</small>
                                </td>
                                <td style="max-width: 300px">{{ $log->event }}</td>
                                <td>{{ $log->type_event }}</td>
                                <td>{{ $log->ip_address }}</td>
                                <td>{{ $log->url }}</td>
                                <td>
                                    @if($log->id_user > 0)
                                        <div>
                                            <a href="{{ admin_path() }}/account/users?action=filter&id={{$log->id_user}}&sorting=id"><b>{{$log->user->name}}</b></a>
                                        </div>
                                        <small>{{$log->user->email}}</small>
                                    @else
                                        <small class="text-muted">Система</small>
                                    @endif
                                </td>
                                <td>
                                    @if($log->id_admin > 0)
                                        <div>
                                            <a href="{{ admin_path() }}/account/users?action=filter&id={{$log->id_admin}}&sorting=id"><b>{{$log->user_admin->name}}</b></a>
                                        </div>
                                        <small>{{$log->user_admin->email}}</small>
                                    @else
                                        <small class="text-muted">не определен</small>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9"><p align="center"><br />Список пуст</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            @endif

        </div>
    </div>
    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройка параметров журнала событий</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-8">Включить журнал событий</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_enabled" name="event_log_is_enabled" type="checkbox" value="1" @if(iEXSetting('event_log_is_enabled') == 1) checked @endif />
                                    <label for="event_log_is_enabled" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Сколько дней хранить события</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" style="max-width:150px; text-align: center;" id="event_log_cleanup_days" name="event_log_cleanup_days" value="{{ iEXSetting('event_log_cleanup_days', 7)  }}">
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать выход из системы</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_logout" name="event_log_is_logout" type="checkbox" value="1" @if(iEXSetting('event_log_is_logout') == 1) checked @endif />
                                    <label for="event_log_is_logout" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать успешный вход</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_login_success" name="event_log_is_login_success" type="checkbox" value="1" @if(iEXSetting('event_log_is_login_success') == 1) checked @endif />
                                    <label for="event_log_is_login_success" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать ошибки входа</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_login_fail" name="event_log_is_login_fail" type="checkbox" value="1" @if(iEXSetting('event_log_is_login_fail') == 1) checked @endif />
                                    <label for="event_log_is_login_fail" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать регистрацию нового пользователя</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_register" name="event_log_is_register" type="checkbox" value="1" @if(iEXSetting('event_log_is_register') == 1) checked @endif />
                                    <label for="event_log_is_register" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать запросы на смену пароля</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_password_request" name="event_log_is_password_request" type="checkbox" value="1" @if(iEXSetting('event_log_is_password_request') == 1) checked @endif />
                                    <label for="event_log_is_password_request" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать смену пароля</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_password_change" name="event_log_is_password_change" type="checkbox" value="1" @if(iEXSetting('event_log_is_password_change') == 1) checked @endif />
                                    <label for="event_log_is_password_change" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать редактирование пользователя</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_user_edit" name="event_log_is_user_edit" type="checkbox" value="1" @if(iEXSetting('event_log_is_user_edit') == 1) checked @endif />
                                    <label for="event_log_is_user_edit" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-8">Записывать изменение групп пользователя</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_is_user_groups" name="event_log_is_user_groups" type="checkbox" value="1" @if(iEXSetting('event_log_is_user_groups') == 1) checked @endif />
                                    <label for="event_log_is_user_groups" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-8">Сохранять историю изменения полей профиля пользователя:</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="event_log_user_profile_history" name="event_log_user_profile_history" type="checkbox" value="1" @if(iEXSetting('event_log_user_profile_history') == 1) checked @endif />
                                    <label for="event_log_user_profile_history" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $events->links() !!}
    </div>
@endsection
