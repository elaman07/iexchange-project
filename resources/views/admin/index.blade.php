@extends('admin.layouts.app')

@section('title', $desktop->name)

@section('main-top-block')

    {{--    @can('admin_update_system')--}}
    {{--        <md-button hide-xs ng-href="{{ admin_base_path('/update_system') }}"  class="md-raised md-primary" >--}}
    {{--            <md-icon md-font-icon="fa fa-refresh text-primary"></md-icon>--}}
    {{--            <span>Система обновлений</span>--}}
    {{--        </md-button>--}}
    {{--    @endcan--}}

    <style>
        .cursor-scroll .x_title .icon-button-widget {
            opacity: 0;
            -webkit-transition: opacity 0.2s ease-in-out;
            -moz-transition: opacity 0.2s ease-in-out;
            -ms-transition: opacity 0.2s ease-in-out;
            -o-transition: opacity 0.2s ease-in-out;
            transition: opacity 0.2s ease-in-out;
        }
        .cursor-scroll .x_title:hover .icon-button-widget {
            opacity: 1;
        }

        .widget-empty {
            text-align: center;
        }

        .widget-empty .widget-empty-title {
            font-size: 16px;
            color: #7b7b7b;
        }

        .widget-empty lottie-player {
            width: 300px;
            height: 300px;
            margin: 0 auto;
        }

    </style>

    <div hide-xs class="panel-button-widget" layout="row" layout-align="end center" ng-controller="CommonController  as ctrl">
        <md-menu-bar class="widgets-multi-menu-bar" style="z-index: 0">
            <md-menu class="widgets-multi-menu-bar">
                <md-button class="md-raised box-desktop-button md-primary" ng-click="ctrl.openMenu($mdMenu, $event)">

                    @lang('admin.home.add_widget')
                    &nbsp;&nbsp;
                    <i class="far fa-angle-down"></i>
                </md-button>

                <md-menu-content class="widgets-multi-menu-content" width="3">
                    @foreach(config('widgets.admin-widgets') as $key => $item)
                        <md-menu-item>
                            <md-menu class="nested-menu">
                                <md-button>
                                    <span md-menu-align-target> {{ $item['name'] }}</span>
                                </md-button>

                                <md-menu-content class="widgets-multi-nested-content" width="3">
                                    @foreach($item['items'] as $nested)
                                        <md-menu-item>
                                            <md-button ng-href="{{ admin_base_path('/widgets/'.$desktop->id.'/'.$nested['id'].'/add') }}">{{ $nested['name'] }}</md-button>
                                        </md-menu-item>
                                    @endforeach
                                </md-menu-content>
                            </md-menu>
                        </md-menu-item>
                    @endforeach

                </md-menu-content>
            </md-menu>
        </md-menu-bar>
        <md-menu-bar class="widgets-multi-menu-bar" style="z-index: 0">
            <md-menu class="widgets-multi-menu-bar">
                <md-button class="md-raised box-desktop-button" ng-click="ctrl.openMenu($mdMenu, $event)">
                    <i class="far fa-cog"></i>&nbsp;&nbsp;
                    @lang('admin.home.settings')
                    &nbsp;&nbsp;
                    <i class="far fa-angle-down"></i>
                </md-button>

                <md-menu-content class="widgets-multi-nested-content" width="3">
                    <md-menu-item>
                        <md-button data-toggle="modal" href="#create_desktop">@lang('admin.home.create_new_desktop')</md-button>
                    </md-menu-item>

                    <md-menu-item>
                        <md-button data-toggle="modal" href="#setting_desktop">@lang('admin.home.setting_current_desktop')</md-button>
                    </md-menu-item>

                    <md-divider></md-divider>

                    <md-menu-item>
                        <md-button data-toggle="modal" href="{{ admin_base_path('/widgets/config_desktops') }}">@lang('admin.home.settings_all_desktop')</md-button>
                    </md-menu-item>

                    <md-divider></md-divider>

                    <md-menu-item>
                        <md-button ng-href="{{ admin_base_path('/widgets/reset') }}">@lang('admin.home.clear_current_settings')</md-button>
                    </md-menu-item>

                </md-menu-content>
            </md-menu>
        </md-menu-bar>
    </div>
@endsection


@section('custom-nav-title')
    <ul style="margin: 0;padding-left: 0">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <h5>
                    @yield('title') <span class="caret"></span>
                </h5>
            </a>

            <ul class="dropdown-menu">
                @foreach($desktops as $value)
                    <li @if($desktop->id == $value->id) class="active" @endif>
                        <a href="?page_id={{$value->id}}">
                            <div layout="row" layout-align="start center">
                                <i class="fal fa-desktop"></i>
                                <div class="widget-title-flex">
                                    {{ $value->name }}<br />
                                    <small class="widget-subtitle">Виджетов: {{ $value->gadgets->count() }}</small>
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach

                <li class="divider"></li>
                <li>
                    <a data-target="#create_desktop" data-toggle="modal">
                        <i class="fal fa-plus"></i>
                        @lang('admin.home.add_desktop')
                    </a>
                </li>
            </ul>
        </li>
    </ul>
@endsection

@section('custom-js-head')
    <style>
        .ui-state-highlight { width: 49%; margin-bottom: 10px; background: #ccc; height: auto; }
        .ui-sortable-placeholder {
            background: transparent !important;
            border: 1px dashed rgba(15,23,42, 1) !important;
            border-radius: 1rem;
        }
    </style>
@endsection

<!-- На главной странице, нет необходимости -->
{{--@section('breadcrumbs', Breadcrumbs::render('admin.index', $desktop))--}}

@section('no-block-content')
    @if($errors->any())
        <div class="alert alert-info text-center">{{$errors->first()}}</div>
    @endif

{{--    @if(cache()->get('iex_new_version') != config('version.current'))--}}
{{--        <div class="alert alert-warning">--}}
{{--            <div class="flash-alert-row layout-align-start-center layout-row" layout="row" layout-align="start center">--}}
{{--                <i class="fal fa-question-circle"></i>--}}
{{--                <div class="flash-alert-column">--}}
{{--                    <div class="flash-alert-title">Новая версия iEXExchanger <b>{{ cache()->get('iex_new_version') }}</b></div>--}}
{{--                    <div class="flash-alert-content">Для получения обновлений, свяжитесь со службой поддержки iEXExchanger. Текущая версия: {{ config('version.current') }}</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}

    @if($desktop->gadgets->count() > 0)
    <div layout="row wrap" layout-xs="column" layout-wrap  class="admin-home-layout" style="    margin-right: -10px;">
        @for($i = 0; $i < $desktop->columns; $i++)
            <div flex-xs="100" flex="{{ isset($desktop->flex_nums[$i]) ? $desktop->flex_nums[$i] : 33 }}" data-column="column{{ $i }}" id="sortable_column{{ $i }}"  class="connectedSortable">
                @foreach($desktop->gadgets->where('column_id', 'column'.$i) as $gadget)
                    <div class="cursor-scroll" id="item-{{$gadget->id}}" data-position="{{$gadget->sorting}}" style="display: block;position: relative;padding-right: 10px;">
                        @widget($gadget->alias, ['item' => $gadget->toArray()])
                    </div>
                @endforeach
            </div>
        @endfor
    </div>
    @else
        <div class="widget-empty">
            <div class="widget-empty-icon">
                <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
                <lottie-player src="/partials/lf20_r71cen62.json"  background="transparent"  speed="1"  loop  autoplay></lottie-player>
            </div>
            <h2 class="widget-empty-title">Виджетов не найдено</h2>
        </div>
    @endif

    <div class="modal fade" id="create_desktop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/widgets/create_desktop') }}">
                <input type="hidden" name="export_users" value="enable">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">@lang('admin.home.settings_desktop_title')</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_name')</label>
                            <div class="col-md-6">
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_column')</label>
                            <div class="col-md-6" id="requisites-list">
                                <select name="columns" class="form-control selectpicker">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #1</label>
                            <div class="col-md-6">
                                <input type="text" name="flex_nums[0]" value="50" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #2</label>
                            <div class="col-md-6">
                                <input type="number" name="flex_nums[1]" value="50" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #3</label>
                            <div class="col-md-6">
                                <input type="number" name="flex_nums[2]" value="0" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #4</label>
                            <div class="col-md-6">
                                <input type="number" name="flex_nums[3]" value="0" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin.home.save_button') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin.home.cancel_button') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="setting_desktop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/widgets/update_desktop/'.$desktop->id) }}">
                <input type="hidden" name="export_users" value="enable">
                @csrf
                @method('PUT')
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">@lang('admin.home.settings_desktop_title')</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_name')</label>
                            <div class="col-md-6">
                                <input type="text" name="name" value="{{$desktop->name}}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_column')</label>
                            <div class="col-md-6">
                                <select name="columns" class="form-control selectpicker">
                                    <option value="1" @if($desktop->columns == 1) selected @endif>1</option>
                                    <option value="2" @if($desktop->columns == 2) selected @endif>2</option>
                                    <option value="3" @if($desktop->columns == 3) selected @endif>3</option>
                                    <option value="4" @if($desktop->columns == 4) selected @endif>4</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #1</label>
                            <div class="col-md-6">
                                <input type="text" name="flex_nums[0]" value="{{$desktop->flex_nums[0] ?? 50 }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #2</label>
                            <div class="col-md-6">
                                <input type="number" name="flex_nums[1]" value="{{ $desktop->flex_nums[1] ?? 50 }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #3</label>
                            <div class="col-md-6">
                                <input type="number" name="flex_nums[2]" value="{{ $desktop->flex_nums[2] ?? 0 }}" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">@lang('admin.home.desktop_field_width') #4</label>
                            <div class="col-md-6">
                                <input type="number" name="flex_nums[3]" value="{{ $desktop->flex_nums[3] ?? 0 }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin.home.save_button') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin.home.cancel_button') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js_bottom')
    <script>
        $( function()
        {
            console.log('ssss')
            var ul_sortable = $('#sortable_column0, #sortable_column1, #sortable_column2, #sortable_column3');
            ul_sortable.sortable({
                revert:true,

                connectWith: ".connectedSortable",
                cancel: '.x_content',
                update: function(event, ui)
                {
                    var drop_column = $(this).attr('data-column');
                    $.ajax({
                        data:  $(this).sortable('serialize').toString(),
                        type: 'POST',
                        url: '{{ admin_base_path('/widgets/'.$desktop->id) }}/'+ drop_column+'/sorting',
                        success: function (result) {
                        }
                    });
                },

            });
            // ul_sortable.disableSelection();
        } );
    </script>
@endsection
