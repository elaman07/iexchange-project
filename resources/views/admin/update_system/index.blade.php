@extends('admin.layouts.app')

@section('title', __('Система обновлений'))

@section('breadcrumbs', Breadcrumbs::render('admin.update_system'))

@section('top-block')
    <md-button target="_blank" ng-href="https://exchanger.iexbase.com/license" class="btn-float" layout="column">
        <i class="fad fa-file text-info"></i>
        <span>{{ __('Лицензионное Соглашение') }}</span>
    </md-button>
@endsection

@section('no-block-content')

    @if(config('version.current') < $server_version)
        <div class="alert alert-warning">
            {{ __('Выпущено обновление iEXExchanger') }}. <b>v.{{ $server_version }}</b>
        </div>
    @else
        <div class="alert alert-success">
            {{ __('У вас установлено последняя версия iEXExchanger') }}.
        </div>
    @endif

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('Обновление системы') }}</h2>
        </div>
        <div class="x_content system-updates">

            <div class="padding-20">
                <h4>{{ __('Обновление с') }} 6.5 {{ __('до') }} {{ config('version.current') }} ({{ __('актуальная') }})</h4>
                <a target="_blank" class="btn btn-primary" href="?actions=upgrade&v={{ config('version.current') }}&step=1">{{ __('Выполнить') }} ({{ __('Шаг') }} 1)</a>
            </div>
            <hr />

            <div class="padding-20">
                <h4>{{ __('Обновление с') }} 6.х {{ __('до') }} 6.5</h4>
                <a target="_blank" class="btn btn-primary" href="?actions=upgrade&v=6.5&step=1">{{ __('Выполнить') }} ({{ __('Шаг') }} 1)</a>
                <a target="_blank" class="btn btn-primary" href="?actions=upgrade&v=6.5&step=2">{{ __('Выполнить') }} ({{ __('Шаг') }} 2)</a>
                <a target="_blank" class="btn btn-primary" href="?actions=upgrade&v=6.5&step=3">{{ __('Выполнить') }} ({{ __('Шаг') }} 3)</a>
            </div>
            <hr />

            <div class="padding-20">
                <h4>{{ __('Обновление с') }} 6.0 {{ __('до') }} 6.1</h4>
                <a target="_blank" class="btn btn-primary" href="?actions=upgrade&v=6.1&step=1">{{ __('Выполнить') }} ({{ __('Шаг') }} 1)</a>
                <a target="_blank" class="btn btn-primary" href="?actions=upgrade&v=6.1&step=2">{{ __('Выполнить') }} ({{ __('Шаг') }} 2)</a>
            </div>
        </div>
    </div>
@endsection
