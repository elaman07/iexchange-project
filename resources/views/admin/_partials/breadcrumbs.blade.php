<style>
    .breadcrumb > li i {
        display: inline-block;
        font-size: 13px;
        color: #333;
    }

    .position-left {
        margin-right: 8px;
    }
    .breadcrumb > .active {
        opacity: 0.75;
    }

    .iex-black-theme .page-header-default .breadcrumb-line a {
        color: rgba(255,255,255,.9)!important;
        text-shadow: none!important;
    }

    .iex-black-theme .dropdown-menu > li > a {
        border-left: 4px transparent;
    }

    .page-header-wrapper .breadcrumb-line {
        font-size: 14px;
    }
</style>
@if (count($breadcrumbs))
    <ul class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item">
                    @if(isset($breadcrumb->icon))
                        <i class="{{ $breadcrumb->icon }}"></i>
                    @endif
                    <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                </li>
            @else
                <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
            @endif
        @endforeach
    </ul>
@endif


