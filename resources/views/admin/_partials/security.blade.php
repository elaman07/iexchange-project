 @if(config('setup.enabled') == 1)
    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-component alert-danger-color">
        <b>{{ __('Внимание ошибка безопасности') }}:</b>
        <br>{{ __('После установки скрипта на сервер Вы не отключили модуль установщика - это делает Ваш сайт уязвимым. Прежде чем продолжить работу, отключить данный файл с Вашего сервера!') }}
    </div>
@endif

{{--@foreach($iexErrorModel as $key => $value)--}}
{{--    @if($key == 'parser-exchange')--}}
{{--        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-component alert-danger-color">--}}
{{--            <b>{{ __('Ошибка в парсинге') }}</b>--}}
{{--            <br>--}}
{{--            {{ $value }}--}}
{{--        </div>--}}
{{--    @endif--}}
{{--@endforeach--}}
