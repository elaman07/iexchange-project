@foreach($adminMenuSidenav as $key => $category_1)
    @if($currentUser->hasAnyPermission($category_1['rules']))
        @if(isset($category_1['line']))
            <div class="vertical-navigation-linear">
                <div class="vertical-navigation-linear-divider"></div>
            </div>
            <div class="clearfix"></div>
        @endif

        @if(isset($category_1['headline']))
            <li class="menu-subheader">
                <div class="sidenav-nav-title-wrapper">
                    <div class="sidenav-nav-title">{{ $category_1['headline']['title'] }}</div>
                    <div class="sidenav-nav-subtitle">{{ $category_1['headline']['subtitle'] }}</div>
                </div>
            </li>
        @endif
        <li>
            <a><i class="{{ $category_1['icon'] }}"></i>
                <span class="child_menu_title">
                    {{ __(str_replace('__', '.', $key)) }}
                </span>
                <div class="md-ripple-container"></div>
            </a>
            @if(count($category_1['categories']) > 0)
                <ul class="nav child_menu">
                    @foreach($category_1['categories'] as $category_2)
                        @if(isset($category_2['line']))
{{--                            <li class="menu-divider-line"></li>--}}
                        @else
                            @if(!isset($category_2['config']))
                            <!-- С правами -->
                                @if(isset($category_2['rule']) and $currentUser->can($category_2['rule']))
                                    <li @if(!isset($category_2['url']))class="single_child_menu"@endif>
                                        <a @if(isset($category_2['url']))href="{{ $category_2['url'] }}" @endif">
{{--                                        <md-icon md-font-icon="fa fa-fw fa-circle"></md-icon>--}}
                                        <span @if(isset($category_2['color']))style="color:{{ $category_2['color'] }}"@endif">{{ __($category_2['name']) }}</span>
                                        </a>
                                        @if(isset($category_2['child']) and count($category_2['child']) > 0)
                                            <ul class="nav child_menu">
                                                @foreach($category_2['child'] as $child)
                                                    @if(isset($child['role']) and $currentUser->can($child['role']))
                                                        <li>
                                                            <a href="{{ $child['url'] }}">
{{--                                                                <md-icon md-font-icon="fa fa-fw fa-circle"></md-icon>--}}
                                                                <span>{{ __($child['name']) }}</span>
                                                            </a>
                                                        </li>
                                                    @else
                                                        <li @if(!isset($child['url']))class="single_child_menu"@endif>
                                                            <a @if(isset($child['url']))href="{{ $child['url'] }}" @endif">
{{--                                                            <md-icon md-font-icon="fa fa-fw fa-circle"></md-icon>--}}
                                                            <span>{{ __($child['name']) }}</span>
                                                            </a>

                                                            @if(isset($child['subcategory']) and count($child['subcategory']) > 0)
                                                                <ul class="nav child_menu">
                                                                    @foreach($child['subcategory'] as $subcategory)
                                                                        <li>
                                                                            <a href="{{ $subcategory['url'] }}">
{{--                                                                                <md-icon md-font-icon="fa fa-fw fa-circle"></md-icon>--}}
                                                                                <span>{{ __($subcategory['name']) }}</span>
                                                                            </a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @endif
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @endif
@endforeach
