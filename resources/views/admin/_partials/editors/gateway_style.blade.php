<style>
    .bbcode-editor {
        float: left;
        padding: 5px 10px;
        border-radius: 3px;
        border: 1px solid #e0e0e0;
        box-shadow: 0 1px 2px #e0e0e0;
        margin: 0 5px 5px 0;
        color: #333;
        font-size: 12px;
    }

    .bbcode-editor:hover {
        background: #fafafa;
        border: 1px solid #d9d9d9;
    }
</style>
