@extends('admin.layouts.app')

@section('title', __('admin-basic.explorer.title'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#add_explorer" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-basic.explorer.add_link') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.payments.explorer'))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-basic.explorer.payment') }}</th>
            <th>{{ __('admin-basic.explorer.link') }}</th>
            <th>{{ __('admin-basic.explorer.status') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($explorers) > 0)
            @foreach($explorers as $explorer)
                <tr>
                    <th>
                        <a href="payments-explorer/{{$explorer->id}}/edit">
                            {{ $explorer->payment->name }}  &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>{{$explorer->link}}</td>
                    <td>
                        @if($explorer->status == 0)
                            <span class="text-success">{{ __('admin-basic.explorer.enable') }}</span>
                        @else
                            <span class="text-danger">{{ __('admin-basic.explorer.disable') }}</span>
                        @endif
                    </td>
                    <td style="width: 100px;">
                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['payments-explorer.destroy', $explorer->id] ]) !!}
                            {!! Form::submit(__('admin-basic.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>


    <!-- Модальное окно для добавления ссылки -->
    <div class="modal fade" id="add_explorer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form action="?" method="POST" class="form-horizontal">
                <input type="hidden" name="export_users" value="enable">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-basic.explorer.add_title') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-basic.explorer.payment') }}</label>
                            <div class="col-md-8">
                                {!! Form::select('payment', $payments, null,
                                                ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-width' => '100%']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-basic.explorer.link') }}</label>
                            <div class="col-md-8">
                                {!! Form::input('type', 'link', null, ['class' => 'form-control','required']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-basic.explorer.notification') }}</label>
                            <div class="col-md-8">
                                {!! Form::textarea('text', null, ['class' => 'form-control', 'rows' => '3']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-basic.explorer.status') }}</label>
                            <div class="col-md-8">
                                <select class="form-control selectpicker" name="status">
                                    <option value="0">{{ __('admin-basic.explorer.enable') }}</option>
                                    <option value="1">{{ __('admin-basic.explorer.disable') }}</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-basic.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
