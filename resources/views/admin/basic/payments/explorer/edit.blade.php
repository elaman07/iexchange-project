@extends('admin.layouts.app')

@section('title', __('admin-basic.explorer.edit_title'))


@section('breadcrumbs', Breadcrumbs::render('admin.basic.payments.explorer.change', $explorer))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/payments-explorer/'.$explorer->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group">
                <label class="control-label col-md-4">{{ __('admin-basic.explorer.payment') }}</label>
                <div class="col-md-8">
                    {!! Form::select('payment', $payments, $explorer->id_payment,
                                    ['class' => 'selectpicker', 'required', 'data-live-search' => 'true']) !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">{{ __('admin-basic.explorer.link') }}</label>
                <div class="col-md-6">
                    {!! Form::input('type', 'link', $explorer->link, ['class' => 'form-control','required']) !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">{{ __('admin-basic.explorer.notification') }}</label>
                <div class="col-md-8">
                    {!! Form::textarea('text', $explorer->text, ['class' => 'form-control', 'rows' => '3']) !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">{{ __('admin-basic.explorer.status') }}</label>
                <div class="col-md-8">
                    <select class="selectpicker" name="status">
                        <option value="0" @if($explorer->status == 0) selected @endif>{{ __('admin-basic.explorer.enable') }}</option>
                        <option value="1" @if($explorer->status == 1) selected @endif>{{ __('admin-basic.explorer.disable') }}</option>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>


        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/payments-explorer') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection
