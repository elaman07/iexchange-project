@extends('admin.layouts.app')

@section('title', __('Платежные системы'))

@section('top-block')

    <md-button data-toggle="modal" data-target="#template" class="btn-float" layout="column">
        <i class="fad fa-upload text-muted"></i>
        <span class="text-muted">{{ __('Готовые данные') }}</span>
    </md-button>

    <md-button ng-href="payments/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить ПС') }}</span>
    </md-button>

@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.payments'))

@section('content')
    <form style="width:100%;margin-right: 10px;" method="POST" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th class="not-sortable"></th>
                <th class="not-sortable">{{ __('Логотип') }}</th>
                <th>{{ __('Название ПС') }}</th>
                <th>{{ __('Привязанные валюты') }}</th>
            </tr>

            </thead>
            <tbody>

            @if(count($payments) > 0)
                @foreach($payments as $payment)
                    <tr>
                        <th style="width: 100px">
                            <input type="hidden" name="item_id[]" value="{{ $payment->id }}">
                            <div class="form-check">
                                <input class="form-check-input" id="checkbox{{$payment->id}}" value="{{$payment->id}}" type="checkbox" name="payment_checkbox[]">
                                <label class="form-check-label" for="checkbox{{$payment->id}}"></label>
                            </div>
                        </th>
                        <td style="width: 100px">

                            @if((int)iEXSetting('is_module_storage_disk') == 1 and !empty(iEXSetting('storage_disk_payment_system')) and $payment->is_local_image == 1)
                                <img src="{{ iex_dynamic_read_view_image('/payment_systems/'.$payment->logo) }}" width="30" alt="{{ $payment->name }}"/>
                            @else
                                @if(iex_file_check(public_path('/storage/payment_systems/'.$payment->logo)))
                                    <img width="30" src="/storage/payment_systems/{{ $payment->logo }}" alt="{{ $payment->name }}"/>
                                @else
                                    <small class="text-danger">{{ __('admin-basic.no_photo') }}</small>
                                @endif
                            @endif
                        </td>
                        <td style="width: 30%;">
                            <div class="multi-language-form-group-input">
                                    <ul class="nav nav-pills lang-tabs">
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $payment->id }}-{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                    <div id="name-{{ $payment->id }}-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                        <input type="text" class="form-control"  name="name{{$form_lang['field']}}[{{ $payment->id }}]" value="{{ $payment->getTranslation('name', $form_lang_key) }}">
                                    </div>
                                @endforeach
                            </div>
                            @if($payment->is_import == 1)
                                <small class="text-muted font-weight-normal" style="margin-top: 5px;margin-left: 2px">{{ __('Импортированный') }}</small>
                            @endif
                        </td>
                        <td>
                            @if($payment->currencies->count() > 0)
                                <ul class="list-unstyled" height="100px" slim-scroll>
                                    @foreach($payment->currencies as $value)
                                        <li>- <a style="font-size: 11px;color: @if($value->status == 0)#00539e @elseif($value->status == 2) red @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/currency/'.$value->id.'/edit') }}">{{ $value->payment->name }} @if($value->status == 2) [{{ __('в архиве') }}] @endif</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <small class="text-muted">{{ __('Не найдено') }}</small>
                            @endif
                        </td>
                        <td style="width: 150px">
                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/basic/payments/'.$payment->id.'/edit')  }}">
                                <i class="fas fa-pencil"></i>
                            </md-button>

                            @if($payment->currencies->count() == 0)
                                <md-button class="md-icon-button" ng-href="{{ admin_base_path('/basic/payments/'.$payment->id.'/delete')  }}">
                                    <i class="fas fa-trash text-danger"></i>
                                </md-button>
                            @else
                                <md-button class="md-icon-button" disabled>
                                    <i class="fas fa-lock text-warning"></i>
                                </md-button>
                            @endif


                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="0">{{ __('admin-basic.action') }}</option>
                        <option value="trashed">{{ __('В корзину') }}</option>
                        <option data-divider="true"></option>
                        <option value="deleteglobal" data-content="<span class='label label-danger'>{{ __('Важное') }}</span>&nbsp;&nbsp;{{ __('Удалить навсегда') }}"></option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>




    <!-- Модальное окно для импорта данных -->
    <div class="modal fade" id="template" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form action="?import_payments" method="POST" class="form-horizontal">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Импорт готовых платежных систем') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('Платежные системы') }}</label>
                            <div class="col-md-8">
                                {!! Form::select('imports_data[]', $imports, null, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '10', 'multiple']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отменить') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
