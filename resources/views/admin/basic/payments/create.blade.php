@extends('admin.layouts.app')

@section('title', __('Добавить платежную систему'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.payments.add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/payments') }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Платежная система') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название ПС') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $key => $item)
                                        <li @if($item['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $key }}">
                                                {{ $key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $key => $item)
                            <div id="{{ $key }}" style="padding-left: 0;" class="tab-pane fade in @if($item['active'] == true) active @endif">
                                <input type="text" name="name{{$item['field']}}" class="form-control">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Логотип') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(['Image', 'Auto'] as $key_image => $value_image)
                                        <li @if($key_image == 0) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#image-{{ $value_image }}">
                                                {{ $value_image }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div id="image-Image" class="tab-pane fade in active">
                            <input type="file" name="logo" class="form-control" id="inputFile" accept="image/*">
                        </div>

                        <div id="image-Auto" class="tab-pane fade in">
                            <input type="text" name="logo_code" class="form-control" id="inputFile">
                            <div class="input-p-text">{{ __('Укажите код валюты, чтобы найти иконку для платежной системы. Пример: BTC') }}</div>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/payments') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
