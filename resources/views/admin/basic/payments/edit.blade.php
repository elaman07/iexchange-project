@extends('admin.layouts.app')

@section('title', __('Изменить платежную системы'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.payments.change', $item->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/payments/'.$item->id) }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Платежная система') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название ПС') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" value="{{ $item->getTranslation('name', $form_lang_key) }}" id="name{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Логотип') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(['Image', 'Auto'] as $key_image => $value_image)
                                        <li @if($key_image == 0) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#image-{{ $value_image }}">
                                                {{ $value_image }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div id="image-Image" class="tab-pane fade in active">
                            <input type="file" name="logo" class="form-control" id="inputFile" accept="image/*">
                            <br />
                            {{ __('Выбран') }}:

                            @if((int)iEXSetting('is_module_storage_disk') == 1 and !empty(iEXSetting('storage_disk_payment_system')) and $item->is_local_image == 1)
                                <a target="_blank" href="{{ iex_dynamic_read_view_image('/payment_systems/'.$item->logo) }}">{{  $item->logo }}</a>
                            @else
                                <a target="_blank" href="{{ url('/storage/payment_systems/'.$item->logo) }}">{{  $item->logo }}</a>
                            @endif
                        </div>

                        <div id="image-Auto" class="tab-pane fade in">
                            <input type="text" name="logo_code" class="form-control" id="inputFile">
                            <div class="input-p-text">{{ __('Укажите код валюты, чтобы найти иконку для платежной системы. Пример: BTC') }}</div>
                        </div>
                    </div>



                    <div class="clearfix"></div>
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/payments') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
