@extends('admin.layouts.app')

@section('title','Изменить скидку')

@section('breadcrumbs', Breadcrumbs::render('admin.basic.discounts.change', $data->id))

@section('content')

    <form class="form-horizontal" method="post" action="?id={{$data->id}}">
        <div class="default-panel-body">
            {{csrf_field()}}
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Сумма больше</label>
                <div class="col-sm-10">
                    <input type="text" name="amount" class="form-control" id="inputName" placeholder="Укажите сумму больше" value="{{$data->amount}}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDiscount" class="col-sm-2 control-label">Процент скидки</label>
                <div class="col-sm-10">
                    <input type="text" name="discount" class="form-control" id="inputDiscount" value="{{$data->discount}}" required>
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Сохранить</md-button>
            <md-button ng-href="{{config('app.url')}}{{config('admin.directory')}}/basic/discounts">Назад</md-button>
        </div>
    </form>
@endsection