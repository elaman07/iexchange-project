@extends('admin.layouts.app')

@section('title','Скидки пользователей')


@section('top-block')
    <md-button ng-href="discounts/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить скидку</span>
    </md-button>
@endsection


@section('breadcrumbs', Breadcrumbs::render('admin.basic.discounts'))

@section('content')
    <table id="default-table" class="table table-border-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>Сумма больше</th>
            <th>Процент скидки</th>
            <th class="not-sortable"></th>
        </tr>

        </thead>
        <tbody>

        @foreach($discounts as $item)
            <tr>
                <th>{{$item->id}}</th>
                <td> > {{$item->amount}}</td>
                <td>{{$item->discount}}%</td>
                <td style="width:150px;">

                    <md-button href="{{config('admin.directory')}}/basic/discounts/change?id={{$item->id}}" class="md-icon-button" >
                        <i class="fa fa-pencil"></i>
                    </md-button>

                    <md-button href="{{config('admin.directory')}}/basic/discounts/delete?id={{$item->id}}" class="md-icon-button" >
                        <i class="text-danger fa fa-trash"></i>
                    </md-button>

                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@endsection