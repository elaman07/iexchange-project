@extends('admin.layouts.app')

@section('title','Изменить запись')

@section('breadcrumbs', Breadcrumbs::render('admin.basic.whitelist.edit', $item->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{admin_path('basic/whitelist/'.$item->id)}}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Значение</label>
                <div class="col-sm-10">
                    <input type="text" name="value" class="form-control" id="value" value="{{$item['value']}}" placeholder="Введите значение (счет, email ...)" required>
                    <small><b>Пример:</b> example@iexbase.com</small>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Тип</label>
                <div class="col-sm-3">
                    <select class="form-control selectpicker" name="type" required>
                        @foreach($types as $key => $value)
                            <option value="{{$key}}" @if($item['type'] == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Описание</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="5" name="text">{{$item->text}}</textarea>
                </div>
            </div>

        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Обновить</md-button>
            <md-button ng-href="{{admin_path('basic/whitelist')}}">Назад</md-button>
        </div>
    </form>

@endsection