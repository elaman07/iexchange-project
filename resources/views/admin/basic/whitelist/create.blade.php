@extends('admin.layouts.app')

@section('title','Добавить запись')

@section('breadcrumbs', Breadcrumbs::render('admin.basic.whitelist.add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{admin_path()}}/basic/whitelist">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Значение</label>
                <div class="col-sm-10">
                    @if(isset($query['format']) and $query['format'] == 'list')
                        <textarea class="form-control" name="value" rows="5" placeholder="Добавьте значения с новой строки"></textarea>
                    @else
                        <input type="text" name="value" class="form-control" id="value" placeholder="Введите значение (счет, email ...)" required>
                        <small><b>Пример:</b> example@iexbase.com</small>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Тип</label>
                <div class="col-sm-3">
                    <select class="form-control selectpicker" name="type" required>
                        @foreach($types as $key => $value)
                            <option value="{{$key}}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Описание</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="5" name="text"></textarea>
                </div>
            </div>

        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Добавить</md-button>
            <md-button ng-href="{{config('app.url')}}{{admin_path()}}/basic/whitelist">Назад</md-button>
        </div>
    </form>

@endsection