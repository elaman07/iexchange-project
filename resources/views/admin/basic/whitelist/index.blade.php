@extends('admin.layouts.app')

@section('title','Белый список')

@section('top-block')
    <md-button ng-href="whitelist/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить запись</span>
    </md-button>

    <md-button ng-href="whitelist/create?format=list" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить списком</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройки</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.whitelist'))


@section('title-right-button')
    <md-button class="md-warn" href="{{ admin_base_path('/basic/whitelist/?clear_all') }}">Очистить список</md-button>
@endsection

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Значение</label>
                                    <div class="col-md-8">
                                        {!! Form::input('text', 'from_value', is_filter_search($filter, 'from_value'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Типы</label>
                                    <div class="col-md-8">
                                        {!! Form::select('types[]', $types, is_filter_search($filter, 'types'),
                                            ['class' => 'selectpicker', 'data-width' => '100%', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Дата создания</label>
                                    <div class="col-md-8">
                                        От: <input  value="{{ is_filter_search($filter, 'from_created_at') }}" name="from_created_at" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        До: <input  value="{{ is_filter_search($filter, 'to_created_at') }}" name="to_created_at" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">Применить фильтры</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">Очистить фильтры</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Белый список</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>Значение</th>
                    <th>Тип</th>
                    <th>Описание</th>
                    <th>Действие</th>
                </tr>

                </thead>
                <tbody>
                @if(count($lists) > 0)
                    @foreach($lists as $list)
                        <tr>
                            <th scope="row">
                                {{ $list->created_at }}
                            </th>
                            <td>{{$list->value}}</td>
                            <td>{{$types[$list->type]}}</td>
                            <td style="width: 300px">{!! nl2br($list->text) !!}</td>
                            <td style="width: 200px;">
                                <div class="col-md-6">
                                    <a href="whitelist/{{$list->id}}/edit" class="btn btn-info btn-sm">Изменить</a>
                                </div>

                                <div class="col-md-6">
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['whitelist.destroy', $list->id] ]) !!}
                                    {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-sm']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4"><p align="center"><br />Список пуст</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройки белого списка</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">Активировать белый список</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_local_whitelist" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_local_whitelist') == 0) selected @endif>Нет</option>
                                    <option value="1"  @if(iEXSetting('is_local_whitelist') == 1) selected @endif>Да</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
