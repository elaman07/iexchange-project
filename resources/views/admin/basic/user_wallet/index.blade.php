@extends('admin.layouts.app')

@section('title','Счета пользователей')

@section('breadcrumbs', Breadcrumbs::render('admin.basic.user_wallets'))

@section('no-block-content')

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">ID Пользователя</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'id_user', is_filter_search($filter, 'id_user'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Валюта</label>
                                    <div class="col-md-6">
                                        {!! Form::select('currencies_out[]', $currencies, is_filter_search($filter, 'currencies_out'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple', 'data-size' => 8, 'data-live-search' => 'true']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Номер счета</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'to_shot', is_filter_search($filter, 'to_shot'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">Применить фильтры</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">Очистить фильтры</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Счета пользователей</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Номер счета</th>
                    <th>Клиент</th>
                    <th>Валюта</th>
                    <th>№ заявки</th>
                    <th>Дата создания</th>
                </tr>

                </thead>
                <tbody>
                @foreach($wallets as $wallet)
                    <tr>
                        <td scope="row">
                            <div class="font-bold">{{ $wallet->to_shot }}</div>
                            @if(\App\Models\BlacklistOrder::where('value', '=', $wallet->to_shot)->exists())
                                <small class="text-danger">Счет найден в черном списке</small>
                            @endif
                        </td>
                        <td>
                            <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$wallet->user->id}}">
                                <b>{{ $wallet->user->name }}</b>
                            </a>
                            <br />
                            <small class="text-muted">{{ $wallet->user->email }}</small>
                        </td>
                        <td>
                            {{$wallet->direction_exchange->currency2->payment->name}}
                            {{$wallet->direction_exchange->currency2->code_currency->name}}
                        </td>

                        <td>
                            <a href="{{ admin_base_path('/applications/details/'.$wallet->id.'?actions=arhive') }}">№{{ $wallet->id }}</a>
                        </td>

                        <td>
                            {{ $wallet->created_at->format('d.m.Y H:i:s') }}
                            <br />
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($wallet->created_at)->diffForHumans() }}</small>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $wallets->links() !!}
    </div>
@endsection
