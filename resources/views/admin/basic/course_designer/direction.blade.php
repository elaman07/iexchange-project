@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.direction.title') )

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.direction', $currency, $data))

@section('no-block-content')

    <style>
        .alert-custom-1 {
            margin-top: -25px;
            margin-left: -20px;
            margin-right: -20px;
            font-size: 13px;
            text-align: center;
        }

        .alert-custom-2 {
            background: #920303;
            color: #f7c6c6;
            text-align: center;
            font-size: 13px;
            line-height: 1.6;
            width: 100%;
        }

        .course-designer md-list-item > p {
            font-size: 13px;
        }

        .bestchange-parser-exchange {
            margin-right: 15px;
        }
    </style>

    <form class="form-horizontal" action="?" method="post">
    <div style="margin-right: 10px;" class="x_panel">
        <div class="x_title" layout="row">
            <h2>{{ __('admin-basic.course_designer.direction.basic') }}</h2>
            <div flex=""></div>
            <div class="heading-right-button">
                @if(!$favorite)
                    <md-button class="md-primary" href="?actions=add_favorites">
                        <span class="if-collapsed2">{{ __('admin-basic.course_designer.direction.add_to_favorite') }}</span>
                    </md-button>
                @else
                    <md-button class="md-warn" href="?actions=remove_favorites">
                        <span class="if-collapsed2">{{ __('admin-basic.course_designer.direction.remove_to_favorite') }}</span>
                    </md-button>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <md-list class="course-designer">
                <md-list-item >
                    <p flex="20">
                        <b>{{ __('admin-basic.course_designer.direction.text_parsing_course') }}:</b>
                    </p>
                    <p flex="30">
                        <select name="parser" class="form-control selectpicker" data-width="300px">
                            @if(isset($data->id_bestchange_rates) and $data->id_bestchange_rates == 0)
                                <option value="1" disabled>{{ __('admin-basic.course_designer.direction.bestchange') }}</option>
                            @else
                                <option value="1" @if($data->enable_bestchange == 1) selected @endif>{{ __('admin-basic.course_designer.direction.bestchange') }}</option>
                            @endif

                                @if(isset($data->id_competitor) and $data->id_competitor == 0)
                                    <option value="2" disabled>{{ __('admin-basic.course_designer.direction.competitors_course') }}</option>
                                @else
                                    <option value="2" @if($data->enable_competitors == 1) selected @endif>
                                        {{ __('admin-basic.course_designer.direction.competitors_course') }} ({{ $data->competitor_rates->competitor_link->name }})
                                    </option>
                                @endif

                            @if(isset($data->id_crypto_parser) and $data->id_crypto_parser == 0)
                                <option value="0" disabled>{{ __('admin-basic.course_designer.direction.other') }}</option>
                            @else
                                <option value="0" @if(isset($data->id_crypto_parser) and $data->id_crypto_parser > 0 and $data->enable_bestchange == 0) selected @endif>
                                    @if(isset($data->parser_exchange) and isset($data->parser_exchange->group_parse_exchange))
                                        {{ $data->parser_exchange->group_parse_exchange->name }}
                                    @endif
                                </option>
                            @endif
                        </select>
                    </p>
                </md-list-item>

                <md-divider></md-divider>

                <md-list-item >
                    <p flex="20">
                        <b>{{ __('admin-basic.course_designer.direction.actual_course') }}:</b>
                    </p>
                    <p flex="30">
                        @if($data->is_error_rate)
                            <span class="text-danger">Курс не настроен</span>
                        @else
                            {{ $data->exchange_rate }}
                        @endif
                    </p>
                </md-list-item>

                <md-divider></md-divider>
                <md-list-item >
                    <p flex="20">
                        <b>{{ __('admin-basic.course_designer.direction.status') }}:</b>
                    </p>
                    <div flex="30" class="material-switch" style="margin-top: 3px;">
                        <input id="status" name="status" @if($data->status == 1) checked @endif type="checkbox" value="1">
                        <label for="status" class="label-success"></label>
                    </div>
                </md-list-item>

                @if($alt_count > 0)
                    <md-divider></md-divider>
                    <md-list-item >
                        <p flex="20">
                            <b>{{ __('admin-basic.course_designer.direction.possible_direction') }}:</b>
                        </p>
                        <div flex="40" class="material-switch" style="margin-top: 3px;">
                            <a href="{{$alt_data->id}}">
                                {{$data->currency2->payment->name.' '.$data->currency2->code_currency->name}}
                                →
                                {{$data->currency1->payment->name.' '.$data->currency1->code_currency->name}}
                            </a>
                        </div>

                    </md-list-item>
                    <md-divider></md-divider>
                    <md-list-item >
                        <p flex="20">
                            <b>{{ __('admin-basic.course_designer.direction.search_by_currency') }}:</b>
                        </p>
                        <div flex="40" class="material-switch" style="margin-top: 3px;">
                            <a href="{{ admin_base_path('/basic/course_designer/'.$data->id_currency1) }}">{{$data->currency1->payment->name.' '.$data->currency1->code_currency->name}}</a>,
                            <a href="{{ admin_base_path('/basic/course_designer/'.$data->id_currency2) }}">{{$data->currency2->payment->name.' '.$data->currency2->code_currency->name}}</a>
                        </div>
                    </md-list-item>
                @endif


        </div>
    </div>

        <div layout="row" layout-align="space-between stretch">
            <div style="margin-right: 10px;" class="x_panel">
                <div class="x_title" layout="row">
                    <h2>{{ __('admin-basic.course_designer.direction.default_commission') }}</h2>
                    <span flex=""></span>
                    @if($data->enable_bestchange == 0)
                        <div class="material-switch" style="margin-top: 3px;">
                            <input id="enable_group_commission" name="enable_group_commission" @if($data->enable_group_commission == 1) checked @endif type="checkbox" value="1">
                            <label for="enable_group_commission" class="label-success"></label>
                        </div>
                    @endif
                </div>
                <div class="x_content">
                    @if($data->enable_bestchange == 1)
                        <div layout="column" layout-align="center center" style="    height: 145px;">
                            <div class="alert-custom-2 alert">
                                {{ __('admin-basic.course_designer.direction.default_commission_bestchange') }}
                            </div>
                        </div>
                    @else
                        <div class="default-panel-body">
                            <div class="alert-custom-1 alert alert-warning">
                                {{ __('admin-basic.course_designer.direction.disable_default_commission') }}
                            </div>
                            <select data-live-search="true" name="id_group_commission" class="form-control selectpicker" disabled>
                                <option value="0" @if($data->id_group_commission == 0) selected @endif>-- {{ __('admin-basic.course_designer.direction.not_specified') }} --</option>
                                @foreach($group_commission as $value)
                                    <option @if($data->id_group_commission == $value->id) selected @endif value="{{$value->id}}" data-subtext="(Описание: {{$value->name}})">Комиссия {{$value->give}}%</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                </div>
            </div>

            <div style="margin-right: 10px;" class="x_panel">
                <div class="x_title">
                    <h2>{{ __('admin-basic.course_designer.direction.manipulated_commission') }}</h2>
                </div>
                <div class="x_content">
                    <div class="default-panel-body">
                        <div class="form-group">
                            <label for="sign" class="col-sm-4 control-label">{{ __('admin-basic.course_designer.direction.commission_percent') }}</label>
                            <div class="col-sm-8">
                                <input type="text" name="profit" class="form-control" value="{{$data->profit}}">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="x_panel">
                <div class="x_title" layout="row" layout-align="start center">
                    <h2>{{ __('admin-basic.course_designer.direction.position_bestchange') }}</h2>
                    <span flex></span>
                    @if(isset($data->bestchange_rates) and !empty($data->bestchange_rates->source_name))
                        <small class="text-muted bestchange-parser-exchange">{{ $data->bestchange_rates->source_name }}</small>
                    @endif
                </div>
                <div class="x_content">
                    <div class="default-panel-body">

                        <div class="alert-custom-1 alert alert-warning">
                            {{ __('admin-basic.course_designer.direction.actual_enable_bestchange') }}
                        </div>

                        <div class="form-group">
                            <label for="sign" class="col-sm-4 control-label">{{ __('admin-basic.course_designer.direction.position') }}</label>
                            <div class="col-sm-8">
                                <input type="number" name="bestchange_position" class="form-control" value="{{$data->bestchange_position}}">
                                <div style="margin-top: 5px; font-size: 12px;" class="text-muted">
                                    <b>0</b> - {{ __('admin-basic.course_designer.direction.position_hint') }}
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group">
                            <label for="sign" class="col-sm-4 control-label">{{ __('admin-basic.course_designer.direction.step') }}</label>
                            <div class="col-sm-8">
                                <input type="text" name="bestchange_step" class="form-control" value="{{ $data->bestchange_step }}">
                                <div style="margin-top: 5px; font-size: 12px;" class="text-muted">
                                    <b>0</b> - {{ __('admin-basic.course_designer.direction.disable_limit') }}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-basic.save') }}</md-button>
        </div>
    </form>
@endsection
