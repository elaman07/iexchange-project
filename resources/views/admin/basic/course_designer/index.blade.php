@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer'))

@section('top-block')

    <md-button ng-href="course_designer/favorites" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-star text-primary"></md-icon>
        <span>{{ __('Избранные') }}</span>
    </md-button>

    <md-button ng-href="course_designer/commission" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-percent text-primary"></md-icon>
        <span>{{ __('Комиссии') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <i class="far fa-cog"></i>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title" layout="row" layout-align="start center">
            <h2>{{ __('admin-basic.course_designer.title') }}</h2>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-basic.course_designer.currency') }}</th>
                    <th>{{ __('admin-basic.course_designer.receiving') }} ←</th>
                    <th>{{ __('admin-basic.course_designer.give') }} →</th>
                    <th>{{ __('admin-basic.course_designer.count_direction') }}</th>
                    <th>{{ __('admin-basic.course_designer.count_order') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($currencies as $currency)
                    <tr>
                        <td>
                            <a href="course_designer/{{$currency->id}}">
                                <b>{{$currency->payment->name}} ({{$currency->code_currency->name}})</b>
                            </a>

                            @if((int)iEXSetting('is_enabled_base_main_direction') == 1)
                                @php
                                    $mainstream = $currency->mainstream();
                                @endphp

                                @if(!is_null($mainstream))
                                    <small class="display-block text-muted">{{ __('admin-basic.course_designer.make_direction') }}:
                                        <a href="{{ admin_base_path('/basic/course_designer/'.$currency->id.'/'.$mainstream->id) }}">{{ direction_name($mainstream) }}</a>
                                    </small>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if(!iEXSetting('is_disabled_count_course_designer'))
                                @if(isset($currency->currency_analytics))
                                    {{ number_format((float)$currency->currency_analytics->in_amount, $currency->number_format, '.', ' ') }} {{ $currency->code_currency->sign }}
                                @else
                                    0
                                @endif
                            @else
                                <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                            @endif
                        </td>
                        <td>
                            @if(!iEXSetting('is_disabled_count_course_designer'))
                                @if(isset($currency->currency_analytics))
                                    {{ number_format((float)$currency->currency_analytics->out_amount, $currency->number_format, '.', ' ') }} {{ $currency->code_currency->sign }}
                                @else
                                    0
                                @endif
                            @else
                                <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                            @endif
                        </td>
                        <td>
                            @if(!iEXSetting('is_disabled_count_course_designer'))
                                <div>{{ __('admin-basic.course_designer.give_direction') }}: <b>{{ $currency->direction_exchange_in_count }}</b></div>
                                <div>{{ __('admin-basic.course_designer.receive_direction') }}: <b>{{ $currency->direction_exchange_out_count }}</b></div>
                            @else
                                <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                            @endif
                        </td>
                        <td>
                            @if(!iEXSetting('is_disabled_count_course_designer'))
                                <div>
                                    <a href="{{route('admin.application')}}?action=filter&currencies_in={{$currency->id}}&status[]=4">
                                        @if(isset($currency->currency_analytics))
                                            Всего отдали: <b>{{ $currency->currency_analytics->in_order_count }}</b>
                                        @else
                                            Всего отдали: <b>0</b>
                                        @endif

                                    </a>
                                </div>

                                <div>
                                    <a href="{{route('admin.application')}}?action=filter&currencies_out={{$currency->id}}&status[]=4">
                                        @if(isset($currency->currency_analytics))
                                            Всего получили: <b>{{ $currency->currency_analytics->out_order_count }}</b>
                                        @else
                                            Всего получили: <b>0</b>
                                        @endif
                                    </a>
                                </div>
                            @else
                                <small class="text-danger">{{ __('admin-basic.course_designer.disable') }}</small>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить отображение главного направления</label>
                            <div class="col-md-6" id="is_enabled_geo_direction_country">
                                <select class="selectpicker" name="is_enabled_base_main_direction" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_enabled_base_main_direction') == 0) selected @endif>{{ __('admin-basic.no') }}</option>
                                    <option value="1"  @if(iEXSetting('is_enabled_base_main_direction') == 1) selected @endif>{{ __('admin-basic.yes') }}</option>
                                </select>

                                <div class="input-p-text text-danger">Включение данной функции, снижает производительность</div>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-6">Показывать по умолчанию, направление обмена (для клиентов)</label>
                            <div class="col-md-6">
                                <select class="selectpicker" name="id_direction_exchange_main" data-width="250px" data-live-search="true">
                                    @foreach($exchange_main_data as $item)
                                        <option value="{{ $item['id'] }}" @if($item['is_main'] == 1) selected @endif data-subtext="@if($item['status'] == 1) Включена @else Отключена @endif">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-basic.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
