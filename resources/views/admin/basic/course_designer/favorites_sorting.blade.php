@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.sorting_direction.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.favorites_sorting'))

@section('custom-js-head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 50%; margin: 0 auto;text-align: center; }
        #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em;  height: 4rem;}
        #sortable li span { position: absolute; margin-left: -1.3em; }
        .ui-state-highlight {  height: 4rem; line-height: 1.2em; }
    </style>
@endsection


@section('content')
    <ul id="sortable">
        @foreach($favorites as $key => $value)
            <li class="ui-state-default" id="item-{{$value->id}}" data-position="{{$key}}">
                    {{ $value->direction_exchange->currency1->payment->name.' '.$value->direction_exchange->currency1->code_currency->name }}
                    →
                    {{ $value->direction_exchange->currency2->payment->name.' '.$value->direction_exchange->currency2->code_currency->name }}
            </li>
        @endforeach
    </ul>
@endsection

@section('js_bottom')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        $( function() {
            $( "#sortable" ).sortable();
            $( "#sortable" ).disableSelection();
        } );
    </script>

    <script>
        $( function()
        {
            var ul_sortable = $('#sortable');
            ul_sortable.sortable({
                cursor: 'move',
                placeholder: "ui-state-highlight",
                update: function()
                {
                    var sortable_data = ul_sortable.sortable('serialize');
                    $.ajax({
                        data: sortable_data,
                        type: 'POST',
                        url: '/admin/private/sorting/favorites_sorting',
                        success: function (result) {
                            console.log(result);
                        }
                    });
                }
            });
            ul_sortable.disableSelection();
        } );
    </script>
@endsection