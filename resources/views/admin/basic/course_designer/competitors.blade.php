@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.competitors.title', ['name' => direction_name($direction)]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.competitors', $direction))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-basic.course_designer.competitors.exchange') }}</th>
            <th>{{ __('admin-basic.course_designer.competitors.give') }}</th>
            <th>{{ __('admin-basic.course_designer.competitors.receiving') }}</th>
            <th>{{ __('admin-basic.course_designer.competitors.reserve') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($competitors as $value)
            <tr>
                <td>
                    <b>{{ $value['name'] }}</b><br/>
                    <small>
                        <a target="_blank" href="https://www.bestchange.ru/{{ mb_strtolower($value['name']) }}-exchanger.html">
                            {{ __('admin-basic.course_designer.competitors.detail_exchange') }}
                        </a>
                    </small>
                </td>
                <td>
                    {{ $value['rate_give'] }}
                </td>
                <td style="width: 200px">
                    {{ $value['rate_receiver'] }}
                </td>
                <td style="width: 200px;">
                    {{ $value['reserve'] }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection