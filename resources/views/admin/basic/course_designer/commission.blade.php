@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.commission.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.commission'))

@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-basic.course_designer.commission.direction_name') }}</th>
                <th>{{ __('admin-basic.course_designer.commission.group_commission') }}</th>
                <th>{{ __('admin-basic.course_designer.commission.commission_percent') }}</th>
                <th>{!! __('admin-basic.course_designer.commission.commission_currency') !!}</th>
            </tr>
            </thead>

            <tbody>
            @foreach($directions as $direction)
                <tr>
                    <td>
                        <input type="hidden" name="item_id[{{$direction->id}}]" value="{{$direction->id}}">
                        <div>
                            <a href="{{ admin_base_path('basic/direction_exchange/'.$direction->id.'/edit') }}">
                                <b>{{ direction_name($direction) }}</b>
                            </a>
                        </div>
                        @if($direction->enable_bestchange)
                            <small class="text-muted">{{ __('admin-basic.course_designer.commission.active_parsing_bestchange') }}</small>
                        @endif
                    </td>
                    <td>
                        @if($direction->enable_group_commission)
                            <div class="label label-info">{{ __('admin-basic.yes') }} ({{ $direction->group_commission->give }}%)</div>
                        @else
                            <div class="label label-warning">{{ __('admin-basic.no') }}</div>
                        @endif
                    </td>
                    <td>
                        <input type="text" class="form-control" style="width: 50px;text-align: center;" name="profit[{{$direction->id}}]" value="{{$direction->profit}}">
                    </td>

                    <td>
                        <input type="text" class="form-control" style="width: 100px" name="profit_s[{{$direction->id}}]" value="{{$direction->profit_s}}">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-basic.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
