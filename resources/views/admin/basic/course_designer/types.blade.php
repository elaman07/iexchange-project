@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.types.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.types'))


@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-basic.course_designer.types.direction_name') }}</th>
                <th>{{ __('admin-basic.course_designer.types.tags') }}</th>
                <th>{{ __('admin-basic.course_designer.types.type') }}</th>
            </tr>
            </thead>

            <tbody>
            @foreach($directions as $direction)
                <tr>
                    <td>
                        <div>
                            <a href="{{ admin_base_path('basic/direction_exchange/'.$direction->id.'/edit') }}">
                                <b>{{ direction_name($direction) }}</b>
                            </a>
                        </div>
                    </td>
                    <td>
                        <input type="hidden" name="directions[]" value="{{$direction->id}}">
                        {!! Form::select('export_label_param['.$direction->id.'][]', $export_labels,
                             explode(',', $direction->export_label_param), ['multiple' => true, 'class' => 'form-control selectpicker'])  !!}
                    </td>

                    <td>
                        <div class="form-check">
                            <input class="form-check-input" id="checkbox{{$direction->id}}" value="1" type="checkbox" name="hidden_export_label_param[{{$direction->id}}]" @if($direction->hidden_export_label_param == 1) checked @endif>
                            <label class="form-check-label" for="checkbox{{$direction->id}}">{{ __('admin-basic.course_designer.types.automatic_direction') }}</label>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-basic.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
