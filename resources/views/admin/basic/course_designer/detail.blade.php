@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.detail.title', ['payment' => $item->payment->name, 'code' => $item->code_currency->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.detail', $item))

@section('no-block-content')

    <style>
        .group-input-select {
            margin-left: -1px;
            height: 35px;
        }

        .p-r-20 {
            padding-right: 10px;
        }

        .n-mb-10 {
            margin-bottom: 10px;
        }

        .btn-white {
            color: #000;
            background-color: #fff;
            border-color: #f3f3f3;
            box-shadow: 0 2px 1px -1px #0003, 0 1px 1px #00000024, 0 1px 3px #0000001f;
        }
        </style>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2>Массивные корректировки</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters">

            <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/course_designer/'.$item->id) }}">
                @csrf
                <input type="hidden" name="sign_update" value="in">
                <div class="default-panel-body">
                    <div class="form-group">
                        <label for="profit" class="col-sm-2 control-label">Настройки "Отдаю"</label>
                        <div class="col-sm-10">
                            <div class="form-group col-md-3">
                                <div class="control-label-br">{{ __('Выберите направление') }}</div>
                                <select class="form-control selectpicker" name="item_id_in[]" multiple data-live-search="true" data-actions-box="true" data-size="10">
                                    <option value="0">-- Не выбрано --</option>
                                    @foreach($in as $detail)
                                        <option value="{{ $detail->id }}">{{ direction_name($detail) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-3">
                                <div class="control-label-br">Позиция BestChange</div>
                                <input type="text" name="bestchange_position_in" class="form-control" value="">
                            </div>

                            <div class="form-group col-md-3">
                                <div class="control-label-br">Прибыль в %</div>
                                <input type="text" name="profit_in" class="form-control" value="">
                            </div>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group">
                        <label for="profit" class="col-sm-2 control-label">Настройки "Получаю"</label>
                        <div class="col-sm-10">
                            <div class="form-group col-md-3">
                                <div class="control-label-br">{{ __('Выберите направление') }}</div>
                                <select class="form-control selectpicker" name="item_id_out[]" multiple data-live-search="true" data-actions-box="true" data-size="10">
                                    <option value="0">-- Не выбрано --</option>
                                    @foreach($out as $detail)
                                        <option value="{{ $detail->id }}">{{ direction_name($detail) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-3">
                                <div class="control-label-br">Позиция BestChange</div>
                                <input type="text" name="position_bestchange_out" class="form-control" value="">
                            </div>

                            <div class="form-group col-md-3">
                                <div class="control-label-br">Прибыль в %</div>
                                <input type="text" name="profit_out" class="form-control" value="">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="md-raised md-primary md-button" type="submit">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div layout="column">
        <form style="width:100%;margin-right: 10px;" method="post" action="?form=in">
            @csrf
            <div class="x_panel new-x_panel_wrapper">
                <div class="x_title" layout="row" layout-align="start center">
                    <h2>
                        {{ __('admin-basic.course_designer.detail.give', ['payment' => $item->payment->name, 'code' => $item->code_currency->name]) }}
                    </h2>
                    <span flex=""></span>
                    <div class="heading-right-button">
                        <md-button ng-href="?status_in=allow" class="md-primary">{{ __('admin-basic.course_designer.detail.enable_all') }}</md-button>
                        <md-button ng-href="?status_in=disabled" class="md-warn">{{ __('admin-basic.course_designer.detail.disable_all') }}</md-button>
                    </div>
                </div>
                <div class="x_content">

                    @foreach($in as $detail)
                        <input type="hidden" name="item_id[{{$detail->id}}]" value="{{ $detail->id }}">
                        <div class="row justify-content-center iex-content__bootstrap5-table" @if($detail->status == 0)style="background-color: rgb(255 197 197 / 12%);border-left: 2px solid #dc0000;" @endif>
                            <div class="col-lg-4">

                                <div class="iex-content__bootstrap5-table__wrap iex-content__bootstrap5-start-center">
                                    <div class="iex-content__bootstrap5-table__item">
                                        <div class="iex-content__bootstrap5-table__header">
                                            <a class="iex-content__bootstrap5-table__ft" href="{{ admin_base_path('basic/direction_exchange/'.$detail->id.'/edit') }}">
                                                <b @if($detail->is_main == 1) style="color: green;" @endif>
                                                    {{ $detail->currency1->payment->name.' '.$detail->currency1->code_currency->name }}
                                                    →
                                                    {{ $detail->currency2->payment->name.' '.$detail->currency2->code_currency->name }}
                                                </b>  &nbsp;<i class="fal fa-pencil"></i>
                                            </a>

                                            @if($detail->is_main == 1)
                                                <small class="text-muted display-block">{{ __('admin-basic.course_designer.detail.base_direction') }}</small>
                                            @endif

                                            <div class="form-check">
                                                <input class="form-check-input" value="{{$detail->id}}" type="checkbox" name="check[]" id="checkbox{{$detail->id}}" autocomplete="off">
                                                <label class="form-check-label" for="checkbox{{$detail->id}}">
                                                    Выбрать
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="iex-content__bootstrap5-table__wrap iex-content__bootstrap5-start-center">
                                    <div class="iex-content__bootstrap5-table__item">
                                       <div style="margin-bottom: 10px">
                                           <div>
                                               @if(isset($detail->id_bestchange_rates) and $detail->enable_bestchange == 1 and $detail->is_disable_bs_error == 1)
                                                   <div class="label label-danger">Ошибка Bestchange</div>
                                               @else
                                                   @if($detail->is_error_rate)
                                                       <span class="text-danger">Курс не настроен</span>
                                                   @else
                                                       {{ $detail->exchange_rate }}
                                                   @endif
                                               @endif
                                           </div>
                                           <div style="font-size: 11px; color:#999;">
                                               {{ $detail->parser_source_name }}
                                           </div>
                                       </div>
                                        <select class="form-control selectpicker course_directions_rates" id="course_directions_rates_{{ $detail->id }}" data-width="200px" name="is_type_parser">
                                            <option value="0">-- Не выбрано --</option>
                                            @if($detail->id_bestchange_rates > 0)
                                                <option value="1" @if($detail->enable_bestchange == 1) selected @endif data-subtext="(1 - {{ $detail->bestchange_rates->summa }})">BestChange</option>
                                            @endif
                                            @if(isset($detail->parser_exchange))
                                                <option value="2"  @if($detail->enable_bestchange == 0) selected @endif data-subtext="(1 - {{ $detail->parser_exchange->summa }})">{{ $detail->parser_exchange->group_parse_exchange->name }}</option>
                                            @endif
                                        </select>


                                        <div id="course_direction_custom_parse_{{ $detail->id }}" @if(isset($detail->parser_exchange) and $detail->enable_bestchange == 0) style="display: block;" @else style="display: none;" @endif>
                                            <div class="input-group">
                                                <input style="width: 165px;" type="text" name="add_course1[{{ $detail->id }}]" class="form-control" value="{{ $detail->add_course1 ?? 0}}">
                                                <span class="input-group-addon" id="basic-addon2" style="background: #fff;">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                @if($detail->enable_bestchange == 1)
                                    <h3 class="iex-content__bootstrap5-table__title">Позиция BestChange</h3>

                                    <div class="iex-content__bootstrap5-table__item" style="max-width: 150px">
                                        <input type="text" style="text-align: center;" name="bestchange_position[{{$detail->id}}]" class="group-input-select form-control" value="{{$detail->bestchange_position}}">
                                        <div class="input-p-text">
                                            @if($detail->bestchange_position == 0)
                                                {{ __('admin-basic.course_designer.detail.position_hint', ['position' => iEXSetting('bestchange_position', 10)]) }}
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    <h3 class="iex-content__bootstrap5-table__title">Групповая комиссия</h3>

                                    <div class="iex-content__bootstrap5-table__item" style="max-width: 200px">
                                        <select data-live-search="true" name="id_group_commission" class="form-control selectpicker" data-size="5" data-show-subtext="true">
                                            <option value="0" @if($detail->id_group_commission == 0) selected @endif>-- {{ __('Ничего не выбрано') }} --</option>
                                            @foreach($group_commission as $value)
                                                <option @if($detail->id_group_commission == $value->id) selected @endif value="{{$value->id}}" data-subtext="(Комиссия {{$value->receiving}}%)">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                            </div>

                            <div class="col-lg-2">
                                <h3 class="iex-content__bootstrap5-table__title">Прибыль</h3>

                                <div class="iex-content__bootstrap5-table__item">

                                    <div class="input-group" style="width: 100px;margin-bottom: 2px">
                                        <input type="text" name="profit[{{$detail->id}}]" class="form-control" value="{{ $detail->profit }}">
                                        <span class="input-group-addon" id="basic-addon3">%</span>
                                    </div>

                                    <div class="input-group" style="width: 100px;">
                                        <input type="text" name="profit_s[{{$detail->id}}]" class="form-control" value="{{ $detail->profit_s }}">
                                        <span class="input-group-addon" id="basic-addon3" style="width: 35px;">S</span>
                                    </div>

                                </div>

                            </div>

                            <div class="col-lg-1">
                                <div layout="column" layout-align="center end">
                                    <div class="iex-content__bootstrap5-table__wrap">
                                        <div class="iex-content__bootstrap5-table__title ">Партнерские</div>
                                        <div class="iex-content__bootstrap5-table__item float-right">
                                            <div class="material-switch">
                                                <input id="SwitchOptionPrimary0-{{$detail->id}}" name="is_not_partner[{{$detail->id}}]" type="checkbox" value="0" @if($detail->is_not_partner == 0) checked @endif />
                                                <label for="SwitchOptionPrimary0-{{$detail->id}}" class="label-primary"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach


                    <div class="default-panel-footer">
                        <div class="pull-right">
                            <div style="display: inline-block">
                                <select name="actions" class="form-control selectpicker" data-width="200px">
                                    <option value="save">Сохранить</option>
                                    <option value="activation">Включить</option>
                                    <option value="deactivation">Отключить</option>
                                </select>
                            </div>
                            <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>

        <form style="width:100%;margin-right: 10px;" method="post" action="?form=out">
            @csrf
            <div style="margin-right: 10px;" class="x_panel new-x_panel_wrapper">
                <div class="x_title" layout="row" layout-align="start center">
                        <h2>
                            {{ __('admin-basic.course_designer.detail.receiving', ['payment' => $item->payment->name, 'code' => $item->code_currency->name]) }}
                        </h2>
                    <span flex=""></span>
                    <div class="heading-right-button">
                        <md-button ng-href="?status_out=allow" class="md-primary">{{ __('admin-basic.course_designer.detail.enable_all') }}</md-button>
                        <md-button ng-href="?status_out=disabled" class="md-warn">{{ __('admin-basic.course_designer.detail.disable_all') }}</md-button>
                    </div>
                </div>
                <div class="x_content">
                    @if(count($out) > 0)
                        @foreach($out as $detail)
                            <input type="hidden" name="item_id[{{$detail->id}}]" value="{{ $detail->id }}">
                            <div class="row justify-content-center iex-content__bootstrap5-table" @if($detail->status == 0)style="background-color: rgb(255 197 197 / 12%);border-left: 2px solid #dc0000;" @endif>
                                <div class="col-lg-4">

                                    <div class="iex-content__bootstrap5-table__wrap iex-content__bootstrap5-start-center">
                                        <div class="iex-content__bootstrap5-table__item">
                                            <div class="iex-content__bootstrap5-table__header">
                                                <a class="iex-content__bootstrap5-table__ft" href="{{ admin_base_path('basic/direction_exchange/'.$detail->id.'/edit') }}">
                                                    <b @if($detail->is_main == 1) style="color: green;" @endif>
                                                        {{ $detail->currency1->payment->name.' '.$detail->currency1->code_currency->name }}
                                                        →
                                                        {{ $detail->currency2->payment->name.' '.$detail->currency2->code_currency->name }}
                                                    </b>  &nbsp;<i class="fal fa-pencil"></i>
                                                </a>

                                                @if($detail->is_main == 1)
                                                    <small class="text-muted display-block">{{ __('admin-basic.course_designer.detail.base_direction') }}</small>
                                                @endif

                                                <div class="form-check">
                                                    <input class="form-check-input" value="{{$detail->id}}" type="checkbox" name="check[]" id="checkbox{{$detail->id}}" autocomplete="off">
                                                    <label class="form-check-label" for="checkbox{{$detail->id}}">
                                                        Выбрать
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="iex-content__bootstrap5-table__wrap iex-content__bootstrap5-start-center">
                                        <div class="iex-content__bootstrap5-table__item">
                                            <div style="margin-bottom: 10px">
                                                <div>
                                                    @if(isset($detail->id_bestchange_rates) and $detail->enable_bestchange == 1 and $detail->is_disable_bs_error == 1)
                                                        <div class="label label-danger">Ошибка Bestchange</div>
                                                    @else
                                                        @if($detail->is_error_rate)
                                                            <span class="text-danger">Курс не настроен</span>
                                                        @else
                                                            {{ $detail->exchange_rate }}
                                                        @endif
                                                    @endif
                                                </div>
                                                <div style="font-size: 11px; color:#999;">
                                                    {{ $detail->parser_source_name }}
                                                </div>
                                            </div>
                                            <select class="form-control selectpicker course_directions_rates" id="course_directions_rates_{{ $detail->id }}" data-width="200px" name="is_type_parser">
                                                <option value="0">-- Не выбрано --</option>
                                                @if($detail->id_bestchange_rates > 0)
                                                    <option value="1" @if($detail->enable_bestchange == 1) selected @endif data-subtext="(1 - {{ $detail->bestchange_rates->summa }})">BestChange</option>
                                                @endif
                                                @if(isset($detail->parser_exchange))
                                                    <option value="2"  @if($detail->enable_bestchange == 0) selected @endif data-subtext="(1 - {{ $detail->parser_exchange->summa }})">{{ $detail->parser_exchange->group_parse_exchange->name }}</option>
                                                @endif
                                            </select>


                                            <div id="course_direction_custom_parse_{{ $detail->id }}" @if(isset($detail->parser_exchange) and $detail->enable_bestchange == 0) style="display: block;" @else style="display: none;" @endif>
                                                <div class="input-group">
                                                    <input style="width: 165px;" type="text" name="add_course1[{{ $detail->id }}]" class="form-control" value="{{ $detail->add_course1 ?? 0}}">
                                                    <span class="input-group-addon" id="basic-addon2" style="background: #fff;">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    @if($detail->enable_bestchange == 1)
                                        <h3 class="iex-content__bootstrap5-table__title">Позиция BestChange</h3>

                                        <div class="iex-content__bootstrap5-table__item" style="max-width: 150px">
                                            <input type="text" style="text-align: center;" name="bestchange_position[{{$detail->id}}]" class="group-input-select form-control" value="{{$detail->bestchange_position}}">
                                            <div class="input-p-text">
                                                @if($detail->bestchange_position == 0)
                                                    {{ __('admin-basic.course_designer.detail.position_hint', ['position' => iEXSetting('bestchange_position', 10)]) }}
                                                @endif
                                            </div>
                                        </div>
                                    @else
                                        <h3 class="iex-content__bootstrap5-table__title">Групповая комиссия</h3>

                                        <div class="iex-content__bootstrap5-table__item" style="max-width: 200px">
                                            <select data-live-search="true" name="id_group_commission" class="form-control selectpicker" data-size="5" data-show-subtext="true">
                                                <option value="0" @if($detail->id_group_commission == 0) selected @endif>-- {{ __('Ничего не выбрано') }} --</option>
                                                @foreach($group_commission as $value)
                                                    <option @if($detail->id_group_commission == $value->id) selected @endif value="{{$value->id}}" data-subtext="(Комиссия {{$value->receiving}}%)">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-2">
                                    <h3 class="iex-content__bootstrap5-table__title">Прибыль</h3>

                                    <div class="iex-content__bootstrap5-table__item">

                                        <div class="input-group" style="width: 100px;margin-bottom: 2px">
                                            <input type="text" name="profit[{{$detail->id}}]" class="form-control" value="{{ $detail->profit }}">
                                            <span class="input-group-addon" id="basic-addon3">%</span>
                                        </div>

                                        <div class="input-group" style="width: 100px;">
                                            <input type="text" name="profit_s[{{$detail->id}}]" class="form-control" value="{{ $detail->profit_s }}">
                                            <span class="input-group-addon" id="basic-addon3" style="width: 35px;">S</span>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-1">
                                    <div layout="column" layout-align="center end">
                                        <div class="iex-content__bootstrap5-table__wrap">
                                            <div class="iex-content__bootstrap5-table__title ">Партнерские</div>
                                            <div class="iex-content__bootstrap5-table__item float-right">
                                                <div class="material-switch">
                                                    <input id="SwitchOptionPrimary0-{{$detail->id}}" name="is_not_partner[{{$detail->id}}]" type="checkbox" value="0" @if($detail->is_not_partner == 0) checked @endif />
                                                    <label for="SwitchOptionPrimary0-{{$detail->id}}" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                        <div class="default-panel-footer">
                            <div class="pull-right">

                                <div style="display: inline-block">
                                    <select name="actions" class="form-control selectpicker" data-width="200px">
                                        <option value="save"> {{ __('admin-basic.save') }}</option>
                                        <option value="activation"> {{ __('admin-basic.course_designer.detail.enable') }}</option>
                                        <option value="deactivation"> {{ __('admin-basic.course_designer.detail.disable') }}</option>
                                    </select>
                                </div>
                                <md-button type="submit" class="md-primary md-raised"> {{ __('admin-basic.run') }}</md-button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    @else
                        <div class="alert alert-danger">
                            {{ __('admin-basic.course_designer.detail.direction_not_found') }}
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>

@endsection

@section('js_bottom')
    <script type="text/javascript">
        jQuery(function($){
            $('.course_directions_rates').change(function(){

                var id = $(this).attr('id').replace('course_directions_rates_','');
                var vale = $(this).val();

                if(vale == 2){
                    $('#course_direction_custom_parse_'+id).show();
                } else {
                    $('#course_direction_custom_parse_'+id).hide();
                }
            });
        });
    </script>
@endsection
