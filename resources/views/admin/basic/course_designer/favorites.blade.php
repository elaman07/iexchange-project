@extends('admin.layouts.app')

@section('title', __('admin-basic.course_designer.detail.favorites_title'))

@section('top-block')
    <md-button ng-href="favorites/sorting" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-filter"></md-icon>
        <span>{{ __('admin-basic.course_designer.detail.sorting') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.course_designer.favorites'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title" layout="row" >
            <h2>{{ __('admin-basic.course_designer.detail.favorites_title') }}</h2>
        </div>
        <div class="x_content">
            @if($favorites->count() > 0)
                <form style="width:100%;margin-right: 10px;" method="post" action="?form=in">
                    @csrf
                    <table class="table table-border-2" style="margin-bottom: 0">
                        <thead>
                        <tr>
                            <th>{{ __('admin-basic.course_designer.detail.direction_name') }}</th>
                            <th>{{ __('admin-basic.course_designer.detail.course_type') }}</th>
                            <th>{{ __('admin-basic.course_designer.detail.position') }}</th>
                            <th>{{ __('admin-basic.course_designer.detail.commission') }}</th>
                            <th style="width: 100px;">{{ __('admin-basic.course_designer.detail.partners') }}</th>
                            <th style="width: 100px;">{{ __('admin-basic.course_designer.detail.cashback') }}</th>
                            <th style="width: 100px;" class="not-sortable">{{ __('admin-basic.course_designer.detail.status') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($favorites as $detail)
                            <tr @if($detail->direction_exchange->status == 0)style="background-color: rgba(255, 197, 197, 0.34);" @endif>
                                <td>
                                    <a href="{{$detail->id_currency1}}/{{$detail->direction_exchange->id}}">
                                        <b>
                                            {{ $detail->direction_exchange->currency1->payment->name.' '.$detail->direction_exchange->currency1->code_currency->name }}
                                            →
                                            {{ $detail->direction_exchange->currency2->payment->name.' '.$detail->direction_exchange->currency2->code_currency->name }}
                                        </b>
                                    </a>
                                </td>
                                <td>
                                    <div>
                                        @if(isset($detail->id_bestchange_rates) and $detail->enable_bestchange == 1 and $detail->is_disable_bs_error == 1)
                                            <div class="label label-danger">Ошибка Bestchange</div>
                                        @else
                                            @if($detail->is_error_rate)
                                                <span class="text-danger">Курс не настроен</span>
                                            @else
                                                {{ $detail->exchange_rate }}
                                            @endif
                                        @endif
                                    </div>
                                    <div style="font-size: 11px; color:#999;">
                                        {{ $detail->parser_source_name }}
                                    </div>
                                </td>
                                <td style="width: 200px">
                                    <div class="col-md-10">
                                        <div class="input-group input-iex-group">
                                            <input type="text"
                                                   style="text-align: center;"
                                                   name="bestchange_position[{{$detail->direction_exchange->id}}]"
                                                   class="group-input-select form-control" value="{{$detail->direction_exchange->bestchange_position}}">
                                            <span class="input-group-addon">
                                                @if($detail->direction_exchange->bestchange_position == 0)
                                                    <i class="help-button visible-lg-inline-block text-primary-600 fa fa-question-circle"
                                                       data-toggle="popover" data-placement="right" data-trigger="hover"
                                                       data-content="{{ __('admin-basic.course_designer.detail.position_hint', ['position' => config('crypto.bestchange_position')]) }}"></i>
                                                @else
                                                    <i class="help-button visible-lg-inline-block text-success-600 fa fa-check-circle"
                                                       data-toggle="popover" data-placement="right" data-trigger="hover"
                                                       data-content="{{ __('admin-basic.course_designer.detail.position_hint', ['position' => $detail->direction_exchange->bestchange_position]) }}"></i>
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 200px;">
                                    <input type="text" style="width: 100px;text-align: center;" name="profit[{{$detail->direction_exchange->id}}]" class="group-input-select form-control" value="{{$detail->direction_exchange->profit}}">
                                </td>
                                <td>
                                    <div class="material-switch">
                                        <input id="SwitchOptionPrimary0-{{$detail->direction_exchange->id}}" name="is_not_partner[{{$detail->direction_exchange->id}}]" type="checkbox" value="0" @if($detail->direction_exchange->is_not_partner == 0) checked @endif />
                                        <label for="SwitchOptionPrimary0-{{$detail->direction_exchange->id}}" class="label-primary"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="material-switch">
                                        <input id="SwitchOptionPrimary01-{{$detail->direction_exchange->id}}" name="is_not_bonus[{{$detail->direction_exchange->id}}]" type="checkbox" value="0" @if($detail->direction_exchange->is_not_bonus == 0) checked @endif />
                                        <label for="SwitchOptionPrimary01-{{$detail->direction_exchange->id}}" class="label-primary"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="material-switch">
                                        <input id="SwitchOptionPrimary{{$detail->direction_exchange->id}}" name="status[{{$detail->direction_exchange->id}}]" type="checkbox" value="1" @if($detail->direction_exchange->status == 1) checked @endif />
                                        <label for="SwitchOptionPrimary{{$detail->direction_exchange->id}}" class="label-success"></label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="default-panel-footer">
                        <div class="pull-right">
                            <div style="display: inline-block">
                                <select name="actions" class="form-control selectpicker" data-width="200px">
                                    <option value="save"> {{ __('admin-basic.save') }}</option>
                                </select>
                            </div>
                            <md-button type="submit" class="md-primary md-raised"> {{ __('admin-basic.run') }}</md-button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            @else
                <div class="alert alert-warning text-center">{{ __('admin-basic.course_designer.detail.favorites_not_found') }}</div>
            @endif
        </div>
    </div>
@endsection
