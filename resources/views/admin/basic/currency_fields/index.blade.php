@extends('admin.layouts.app')

@section('title', __('admin-basic.currency_field.title'))

@section('video__instruction', 'https://www.youtube.com/embed/z7x_67P1mp0?autoplay=1&amp;mute=0')

@section('top-block')
    <md-button ng-href="currency_fields/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-basic.currency_field.add_field') }}</span>
    </md-button>

    <md-button ng-href="currency_fields/sorting" class="btn-float" layout="column">
        <i class="far fa-sort"></i>
        <span class="text-muted">Сортировка полей Отдаю</span>
    </md-button>

    <md-button ng-href="currency_fields/sorting_out" class="btn-float" layout="column">
        <i class="far fa-sort"></i>
        <span class="text-muted">Сортировка полей Получаю</span>
    </md-button>

@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency_fields'))

@section('x_panel_class', 'new-x_panel_wrapper')

@section('content')

    <form action="?" method="post">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-basic.currency_field.name') }}</th>
                <th>{{ __('admin-basic.currency_field.key') }}</th>
                <th>Привязанные валюты</th>
                <th>{{ __('admin-basic.currency_field.min_char') }}</th>
                <th>{{ __('admin-basic.currency_field.max_char') }}</th>
                <th>{{ __('admin-basic.currency_field.required') }}</th>
                <th>{{ __('admin-basic.currency_field.available') }}</th>
                <th>{{ __('admin-basic.currency_field.status') }}</th>
                <th class="not-sortable"></th>
            </tr>

            </thead>
            <tbody>

            @if(count($fields) > 0)
                @foreach($fields as $field)
                    <tr>
                        <th>
                            <input type="hidden" name="item_id[{{ $field->id }}]">
                            @if(empty($field->name))
                                <a href="{{ admin_base_path('/basic/currency_fields/'.$field->id.'/edit') }}">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="{{ admin_base_path('/basic/currency_fields/'.$field->id.'/edit') }}">{{$field->name}}   &nbsp;<i class="fal fa-pencil"></i></a>
                            @endif
                        </th>
                        <td>{{ $field->key_id }}</td>

                        <td>
                            @if($field->currencies_in->count() > 0 or $field->currencies_out->count() > 0 )

                                @if($field->currencies_in->count() > 0)
                                    <div style="color: #888;margin-left: 5px;font-size: 11px">Отдаю</div>
                                    <ul class="list-unstyled" height="100px" slim-scroll>
                                        @foreach($field->currencies_in as $value)
                                            <li>- <a style="font-size: 11px;color: @if($value->status == 0)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/currency/'.$value->id.'/edit') }}">{{ $value->payment->name }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif

                                    @if($field->currencies_out->count() > 0)
                                        <div style="color: #888;margin-left: 5px;font-size: 11px">Получаю</div>
                                        <ul class="list-unstyled" height="100px" slim-scroll>
                                            @foreach($field->currencies_out as $value)
                                                <li>- <a style="font-size: 11px;color: @if($value->status == 0)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/currency/'.$value->id.'/edit') }}">{{ $value->payment->name }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                            @else
                                <small class="text-muted">Не найдено</small>
                            @endif
                        </td>

                        <td style="width: 150px">
                            <input type="text" style="width: 70px;text-align: center" name="min_char[{{$field->id}}]" class="form-control" value="{{$field->min_char}}" required>
                        </td>

                        <td style="width: 150px">
                            <input type="text" style="width: 70px;text-align: center" name="max_char[{{$field->id}}]" class="form-control" value="{{$field->max_char}}" required>
                        </td>
                        <td style="width: 150px">
                            <select class="selectpicker" name="obligatory_field[{{$field->id}}]" data-width="100px">
                                <option value="0"  @if($field->obligatory_field == 0) selected @endif>{{ __('admin-basic.yes') }}</option>
                                <option value="1"  @if($field->obligatory_field == 1) selected @endif>{{ __('admin-basic.no') }}</option>
                            </select>
                        </td>

                        <td style="width:300px">
                            <select class="form-control selectpicker" name="field_type[{{$field->id}}]" data-width="200px">
                                <option value="string_and_integer" @if($field->field_type == 'string_and_integer') selected @endif>{{ __('admin-basic.currency_field.string_and_number') }}</option>
                                <option value="string" @if($field->field_type == 'string') selected @endif>{{ __('admin-basic.currency_field.string') }}</option>
                                <option value="integer" @if($field->field_type == 'integer') selected @endif>{{ __('admin-basic.currency_field.number') }}</option>
                            </select>
                        </td>

                        <td>
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{ $field->id }}" name="status[{{ $field->id }}]" type="checkbox" value="0" @if($field->status == 0) checked @endif />
                                <label for="SwitchOptionPrimary{{ $field->id }}" class="label-success"></label>
                            </div>
                        </td>

                        <td style="width: 50px;">
                            <md-button href="currency_fields/{{ $field->id }}/destroy" class="md-icon-button text-danger" >
                                <i class="fas fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-basic.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
