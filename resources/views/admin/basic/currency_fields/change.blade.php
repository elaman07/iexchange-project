@extends('admin.layouts.app')

@section('title', __('admin-basic.currency_field.edit_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency_fields.change', $field->id))

@section('content')

    <style>
        .currency-notify {
            padding: 20px 10px;
            box-shadow: none;
            border: 1px solid #da0202;
            background: #ffe6e6;
            margin-top: 3px;
        }

        .currency-notify b {
            font-weight: 500;
            display: block;
            margin-bottom: 10px;
            font-size: 13px;
        }

        .currency-notify ul li {
            margin-bottom: 5px;
            font-size: 13px;
        }

        .currency-notify p {
            font-size: 13px;
            margin-bottom: 15px;
        }

        .btn-question-block {

            width: 100%;
            margin-top: 10px;
            background: #e0e4ff;
            /* text-align: center; */
            display: block;
            padding: 9px;
            text-align: left;
            border-radius: 2px;
        }

        .btn-question-block a {
            color: #303f9f;
        }

        .btn-question-block a:hover {
            color: #fa0a0a;
        }

        .btn-question-block a::before {
            content: '';
        }
    </style>

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency_fields/'.$field->id) }}">
        @csrf
        @method('PUT')

        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Основное</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-basic.currency_field.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" value="{{ $field->getTranslation('name', $form_lang_key) }}" id="name{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.id_key') }}</div>
                        <input type="text" name="key_id" class="form-control" value="{{ $field->key_id  }}" required>
                        <div class="btn-question-block">
                            <a data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-question-circle"></i>
                                Подробнее
                            </a>
                        </div>
                        <div class="collapse" id="collapseExample">
                            <div class="alert alert-danger alert-mini currency-notify">
                                <b>Внимание!!!</b>
                                <p>При заполнение данного поля, следуйте примерами которые предоставлены ниже</p>
                                <hr />
                                <b>Статические поля</b>
                                <ul>
                                    <li>sender_fullname - Ф.И.О Отправителя</li>
                                    <li>recipient_fullname - Ф.И.О Получателя</li>
                                    <li>income_unk - Для криптовалютных тэгов отдаю (Destination tag, Memo)</li>
                                    <li>outcome_unk - Для криптовалютных тэгов получаю (Destination tag, Memo)</li>
                                </ul>
                                <hr />
                                <b>Димамические поля</b>
                                <ul>
                                    <li>income_* - Дополнительные поля для отдаю, вместо <i>*</i> укажите уникальное имя без пробелов. Пример: income_account, income_account_id</li>
                                    <li>outcome_* - Дополнительные поля для получаю, вместо <i>*</i> укажите уникальное имя без пробелов. Пример: outcome_account, outcome_account_id</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Где выводить?</div>
                        <select class="form-control selectpicker" name="when_print">
                            <option value="0" @if($field->when_print == 0) selected @endif>{{ __('admin-basic.currency_field.in') }}</option>
                            <option value="1" @if($field->when_print == 1) selected @endif>{{ __('admin-basic.currency_field.out') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Тип поля') }}</div>
                        <select class="form-control selectpicker" name="type_field">
                            <option value="0" @if($field->type_field == 0) selected @endif>{{ __('Текстовое поле') }}</option>
                            <option value="1" @if($field->type_field == 1) selected @endif>{{ __('Список') }}</option>
                            <option value="2" @if($field->type_field == 2) selected @endif>{{ __('Выбор') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Варианты (через запятую, с новой строки)') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#list_text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="list_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea class="form-control" name="list_text{{$form_lang['field']}}" rows="3">{{ $field->getTranslation('list_text', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach

                        <div class="input-p-text">Для типов полей: "Список", "Выбор"</div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-basic.currency_field.description') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#description-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="description-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea class="form-control" name="description{{$form_lang['field']}}" rows="3">{{ $field->getTranslation('description', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-basic.currency_field.checkbox') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#checkbox_title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="checkbox_title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="checkbox_title{{$form_lang['field']}}" class="form-control" value="{{ $field->getTranslation('checkbox_title', $form_lang_key) }}">
                                <div class="input-p-text">{{ __('admin-basic.currency_field.checkbox_hint') }}</div>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.min_count_char') }}</div>
                        <input type="number" name="min_char" class="form-control" value="{{ $field->min_char }}" required>
                        <div class="input-p-text"><b>0</b> - {{ __('admin-basic.currency_field.disable_limit') }}</div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.max_count_char') }}</div>
                        <input type="number" name="max_char" class="form-control" value="{{ $field->max_char }}" required>
                        <div class="input-p-text"><b>0</b> - {{ __('admin-basic.currency_field.disable_limit') }}</div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-basic.currency_field.first_char') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#first_char-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="first_char-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="first_char{{$form_lang['field']}}" class="form-control" value="{{ $field->getTranslation('first_char', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Прочие опции</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.required_field') }}</div>
                        <select class="form-control selectpicker" name="obligatory_field" required>
                            <option value="0" @if($field->obligatory_field == 0) selected @endif>{{ __('admin-basic.yes') }}</option>
                            <option value="1" @if($field->obligatory_field == 1) selected @endif>{{ __('admin-basic.no') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Удалить пробелы в поле</div>
                        <select class="form-control selectpicker" name="remove_spaces" required>
                            <option value="0" @if($field->remove_spaces == 0) selected @endif>{{ __('admin-basic.no') }}</option>
                            <option value="1" @if($field->remove_spaces == 1) selected @endif>{{ __('admin-basic.yes') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.type_field') }}</div>
                        <select class="form-control selectpicker" name="field_type">
                            <option value="string_and_integer" @if($field->field_type == 'string_and_integer') selected @endif>{{ __('admin-basic.currency_field.string_and_number') }}</option>
                            <option value="string" @if($field->field_type == 'string') selected @endif>{{ __('admin-basic.currency_field.string') }}</option>
                            <option value="integer" @if($field->field_type == 'integer') selected @endif>{{ __('admin-basic.currency_field.number') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.lang_text') }}</div>
                        <select class="form-control selectpicker" name="language_field">
                            <option value="0" @if($field->language_field == 0) selected @endif>{{ __('admin-basic.currency_field.no_limit') }}</option>
                            <option value="1" @if($field->language_field == 1) selected @endif>{{ __('admin-basic.currency_field.only_russian') }}</option>
                            <option value="2" @if($field->language_field == 2) selected @endif>{{ __('admin-basic.currency_field.only_english') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-basic.currency_field.status') }}</div>
                        <select class="form-control selectpicker" name="status" required>
                            <option value="0" @if($field->status == 0) selected @endif>{{ __('admin-basic.currency_field.enable_field') }}</option>
                            <option value="1" @if($field->status == 1) selected @endif>{{ __('admin-basic.currency_field.disable_field') }}</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Привязать поле к валютам Отдаю</div>
                        {{ Form::select('currencies_in[]', $currencies, $field->currencies_in, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15']) }}
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Привязать поле к валютам Получаю</div>
                        {{ Form::select('currencies_out[]', $currencies, $field->currencies_out, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '15']) }}
                    </div>

                </div>
            </div>
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/currency_fields') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>
@endsection
