@extends('admin.layouts.app')

@section('title', __('Изменить реквизит :name', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.requisites.change', $item))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction-requisites/'.$item->id) }}">
        @csrf
        @method('PUT')

        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Счет') }}</label>
                <div class="col-md-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название счета') }}</div>
                        <input type="text" name="name" class="form-control" value="{{ $item->name }}" id="account_number" placeholder="{{ __('Укажите название счета') }}" required>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Номер счета') }}</div>
                        <input type="text" name="account_number" class="form-control" value="{{ $item->account_number }}" id="account_number" placeholder="{{ __('Укажите номер счета') }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Привязать реквизит к направлениям обмена') }}</div>
                        {{ Form::select('directions_exchanges[]', $directions_exchanges, $item->direction_exchange, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15']) }}
                    </div>
                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <div class="material-switch switch-input-1">
                            <input id="status" name="status" type="checkbox" @if($item->status == 1) checked @endif value="1" />
                            <label for="status" class="label-success"></label>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Лимиты') }}</label>
                <div class="col-md-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Лимит просмотров') }}</div>
                        <input type="text" name="limit_views" value="{{ $item->limit_views }}" class="form-control" id="limit_views">
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Дневной лимит') }}</div>
                        <input type="text" name="limit_day" class="form-control" id="limit_day" value="{{ $item->limit_day }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Месячный лимит') }}</div>
                        <input type="text" name="limit_month" class="form-control" id="limit_month" value="{{ $item->limit_month  }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction-requisites') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
