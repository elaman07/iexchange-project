@extends('admin.layouts.app')

@section('title', __('Реквизиты направлений'))

@section('top-block')

    <md-button ng-href="direction-requisites/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить реквизит') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.requisites'))


@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('Реквизиты направлений') }}</h2>
            <div class="clearfix"></div>
        </div>

        <div class="x_content" id="showSettings">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('Название') }}</th>
                    <th>{{ __('Номер счета') }}</th>
                    <th>{{ __('Привязанные направления') }}</th>
                    <th>{{ __('Просмотров') }}</th>
                    <th>{{ __('Дневной лимит') }}</th>
                    <th>{{ __('Месячный лимит') }}</th>
                    <th class="not-sortable">{{ __('Статус') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <th>
                            <input type="hidden" name="item_id[{{ $item->id }}]">
                            @if(empty($item->name))
                                <a href="{{ admin_base_path('/basic/direction-requisites/'.$item->id.'/edit') }}">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        {{ __('Нет названия') }} &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="{{ admin_base_path('/basic/direction-requisites/'.$item->id.'/edit') }}">{{ $item->name }}   &nbsp;<i class="fal fa-pencil"></i></a>
                            @endif
                        </th>
                        <td>
                            {{ $item->account_number }}
                        </td>
                        <td>
                            @if($item->direction_exchange->count() > 0)
                                <ul class="list-unstyled">
                                    @foreach($item->direction_exchange as $value)
                                        <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ direction_name($value) }}</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <small class="text-danger">{{ __('Не найдено') }}</small>
                            @endif
                        </td>
                        <td>
                            {{ $item->view }}
                        </td>
                        <td>
                            @if($item->limit_day == 0)
                                <span class="text-primary">{{ __('отключен') }}</span>
                            @else
                                {{ $item->limit_day }}
                            @endif
                        </td>
                        <td>
                            @if($item->limit_month == 0)
                                <span class="text-primary">{{ __('отключен') }}</span>
                            @else
                                {{ $item->limit_month }}
                            @endif
                        </td>
                        <td>
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="0" @if($item->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                            </div>
                        </td>

                        <td style="width:100px;">
                            <md-button href="direction-requisites/{{ $item->id }}/delete" class="md-icon-button text-danger">
                                <i class="far fa-trash"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
