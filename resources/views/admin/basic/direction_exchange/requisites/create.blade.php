@extends('admin.layouts.app')

@section('title', __('Добавить реквизит'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.requisites.create'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction-requisites') }}">
        @csrf
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Счет') }}</label>
                <div class="col-md-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название счета') }}</div>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" id="account_number" placeholder="{{ __('Укажите название счета') }}" required>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Номер счета') }}</div>
                        <input type="text" name="account_number" class="form-control" value="{{ old('account_number') }}" id="account_number" placeholder="{{ __('Укажите номер счета') }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Привязать реквизит к направлениям обмена') }}</div>
                        {{ Form::select('directions_exchanges[]', $directions_exchanges, [], ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15']) }}
                    </div>
                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <div class="material-switch switch-input-1">
                            <input id="status" name="status" type="checkbox" @if(old('status') == 1) checked @endif value="1" />
                            <label for="status" class="label-success"></label>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Лимиты') }}</label>
                <div class="col-md-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Лимит просмотров') }}</div>
                        <input type="text" name="limit_views" value="{{ old('limit_views', 0) }}" class="form-control" id="limit_views">
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Дневной лимит') }}</div>
                        <input type="text" name="limit_day" class="form-control" id="limit_day" value="{{ old('limit_day', 0) }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Месячный лимит') }}</div>
                        <input type="text" name="limit_month" class="form-control" id="limit_month" value="{{ old('limit_month', 0)  }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction-requisites') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
