@extends('admin.layouts.app')

@section('title', __('Доп. поля'))

@section('video__instruction', 'https://www.youtube.com/embed/TZgK1V5FVfg?autoplay=1&amp;mute=0')

@section('top-block')
    <md-button ng-href="fields/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить доп. поле') }}</span>
    </md-button>

    <md-button ng-href="fields/sorting" class="btn-float" layout="column">
        <i class="far fa-sort"></i>
        <span>{{ __('Сортировка полей') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.fields'))

@section('content')

    <form action="?" method="post">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Название') }}</th>
                <th>{{ __('Ключ') }}</th>
                <th>{{ __('Мин. символы') }}</th>
                <th>{{ __('Привязанные направления') }}</th>
                <th>{{ __('Макс. символы') }}</th>
                <th>{{ __('Обязательный') }}</th>
                <th>{{ __('Тип поля') }}</th>
                <th>{{ __('Статус') }}</th>
                <th class="not-sortable"></th>
            </tr>

            </thead>
            <tbody>

            @if(count($fields) > 0)
                @foreach($fields as $field)
                    <tr>
                        <th>
                            <input type="hidden" name="item_id[{{ $field->id }}]">
                            @if(empty($field->name))
                                <a href="{{ admin_base_path('/basic/direction_exchange/fields/'.$field->id.'/edit') }}">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        {{ __('Нет названия') }} &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="{{ admin_base_path('/basic/direction_exchange/fields/'.$field->id.'/edit') }}">{{$field->name}}   &nbsp;<i class="fal fa-pencil"></i></a>
                            @endif
                        </th>

                        <td>{{ $field->key_id }}</td>
                        <td>
                            <input type="text" style="width: 70px;text-align: center" name="min_char[{{$field->id}}]" class="form-control" value="{{$field->min_char}}" required>
                        </td>
                        <td>
                            @if($field->direction_exchange->count() > 0)
                                <ul class="list-unstyled">
                                    @foreach($field->direction_exchange as $value)
                                        <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ direction_name($value) }}</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <small class="text-danger">{{ __('Не найдено') }}</small>
                            @endif
                        </td>

                        <td>
                            <input type="text" style="width: 70px;text-align: center" name="max_char[{{$field->id}}]" class="form-control" value="{{$field->max_char}}" required>
                        </td>
                        <td>
                            <select class="selectpicker" name="obligatory_field[{{$field->id}}]" data-width="100px">
                                <option value="0"  @if($field->obligatory_field == 0) selected @endif>{{ __('Да') }}</option>
                                <option value="1"  @if($field->obligatory_field == 1) selected @endif>{{ __('Нет') }}</option>
                            </select>
                        </td>

                        <td>
                            <select class="form-control selectpicker" name="field_type[{{$field->id}}]" data-width="300px">
                                <option value="0" @if($field->field_type == 0) selected @endif>{{ __('Любые символы') }}</option>
                                <option value="1" @if($field->field_type == 1) selected @endif>{{ __('Цифры') }}</option>
                                <option value="2" @if($field->field_type == 2) selected @endif>{{ __('Буквы') }}</option>
                                <option value="3" @if($field->field_type == 3) selected @endif>{{ __('Латинские буквы и цифры') }}</option>
                                <option value="4" @if($field->field_type == 4) selected @endif>{{ __('Латинские буквы') }}</option>
                            </select>
                        </td>


                        <td>
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{ $field->id }}" name="status[{{ $field->id }}]" type="checkbox" value="0" @if($field->status == 0) checked @endif />
                                <label for="SwitchOptionPrimary{{ $field->id }}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width:100px;">
                            <md-button href="fields/{{$field->id}}/delete" class="md-icon-button" >
                                <i class="fa fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('Сохранить') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
