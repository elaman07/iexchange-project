@extends('admin.layouts.app')

@section('title', __('Изменить доп. поле').' '.$field->name)

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.fields.edit', $field))

@section('content')

    <style>
        .currency-notify {
            padding: 20px 10px;
            box-shadow: none;
            border: 1px solid #da0202;
            background: #ffe6e6;
            margin-top: 3px;
        }

        .currency-notify b {
            font-weight: 500;
            display: block;
            margin-bottom: 10px;
            font-size: 13px;
        }

        .currency-notify ul li {
            margin-bottom: 5px;
            font-size: 13px;
        }

        .currency-notify p {
            font-size: 13px;
            margin-bottom: 15px;
        }

        .btn-question-block {

            width: 100%;
            margin-top: 10px;
            background: #e0e4ff;
            /* text-align: center; */
            display: block;
            padding: 9px;
            text-align: left;
            border-radius: 2px;
        }

        .btn-question-block a {
            color: #303f9f;
        }

        .btn-question-block a:hover {
            color: #fa0a0a;
        }

        .btn-question-block a::before {
            content: '';
        }
    </style>

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction_exchange/fields/'.$field->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Основное') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang_value['field']}}" class="form-control" value="{{ $field->getTranslation('name', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Ключ') }}</div>
                        <input type="text" name="key_id" class="form-control" value="{{ $field->key_id }}" required>
                        <div class="btn-question-block">
                            <a data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-question-circle"></i>
                                {{ __('Подробнее') }}
                            </a>
                        </div>
                        <div class="collapse" id="collapseExample">
                            <div class="alert alert-danger alert-mini currency-notify">
                                <b>{{ __('Внимание') }}!!!</b>
                                <p>{{ __('При заполнение данного поля, следуйте примерами которые предоставлены ниже') }}</p>
                                <hr />
                                <b>Статические поля</b>
                                <ul>
                                    <li>wmid - {{ __('Wmid Webmoney') }}</li>
                                    <li>passport - {{ __('Номер паспорта') }}</li>
                                    <li>lastname - {{ __('Фамилия') }}</li>
                                    <li>firstname - {{ __('Имя') }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Короткое описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#description-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="description-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <textarea class="form-control" name="description{{$form_lang_value['field']}}" rows="3">{{ $field->getTranslation('description', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Мин. кол-во символов') }}</div>
                        <input type="number" name="min_char" class="form-control" value="{{ $field->min_char }}" required>
                        <div class="input-p-text"><b>0</b> - {{ __('отключает ограничение') }}</div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Макс. кол-во символов') }}</div>
                        <input type="number" name="max_char" class="form-control" value="{{ $field->max_char }}" required>
                        <div class="input-p-text"><b>0</b> - {{ __('отключает ограничение') }}</div>
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Прочие опции') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Обязательное поле') }}</div>
                        <select class="form-control selectpicker" name="obligatory_field" required>
                            <option value="0" @if($field->obligatory_field == 0) selected @endif>{{ __('Да') }}</option>
                            <option value="1" @if($field->obligatory_field == 1) selected @endif>{{ __('Нет') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Удалить пробелы в поле') }}</div>
                        <select class="form-control selectpicker" name="remove_spaces" required>
                            <option value="0" @if($field->remove_spaces == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if($field->remove_spaces == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Тип поля') }}</div>
                        <select class="form-control selectpicker" name="field_type">
                            <option value="0" @if($field->field_type == 0) selected @endif>{{ __('Любые символы') }}</option>
                            <option value="1" @if($field->field_type == 1) selected @endif>{{ __('Цифры') }}</option>
                            <option value="2" @if($field->field_type == 2) selected @endif>{{ __('Буквы') }}</option>
                            <option value="3" @if($field->field_type == 3) selected @endif>{{ __('Латинские буквы и цифры') }}</option>
                            <option value="4" @if($field->field_type == 4) selected @endif>{{ __('Латинские буквы') }}</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Язык текста') }}</div>
                        <select class="form-control selectpicker" name="language_field">
                            <option value="0" @if($field->language_field == 0) selected @endif>{{ __('Без ограничений') }}</option>
                            <option value="1" @if($field->language_field == 1) selected @endif>{{ __('Только русский') }}</option>
                            <option value="2" @if($field->language_field == 2) selected @endif>{{ __('Только английский') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status" required>
                            <option value="0" @if($field->status == 0) selected @endif>{{ __('Активное поле') }}</option>
                            <option value="1" @if($field->status == 1) selected @endif>{{ __('Не активное поле') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Привязать поле к направлениям обмена') }}</div>
                        {{ Form::select('directions_exchanges[]', $directions_exchanges, $field->direction_exchange, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '10']) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction_exchange/fields') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
