@extends('admin.layouts.app')

@section('title', __('Удаленные направления'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.trashed'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('Удаленные направления') }}</h2>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <form method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th class="not-sortable"></th>
                        <th>{{ __('ID') }}</th>
                        <th>{{ __('Направление обмена') }}</th>
                        <th>{{ __('Курс обмена') }}</th>
                        <th>{{ __('Автокорректировка курса') }}</th>
                        <th>{{ __('BestChange парсер') }}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(count($exchanges) > 0)
                        @foreach($exchanges as $item)
                            <tr @if($item->duplicate() > 1) style="background-color: #ffe0e0" @endif>
                                <th style="width: 60px">
                                    <input type="hidden" name="item_id[{{$item->id}}]" value="{{ $item->id }}">
                                    <div class="form-check" style="height: 15px;">
                                        <input id="checkbox{{$item->id}}" value="{{$item->id}}" type="checkbox" name="check[]">
                                        <label for="checkbox{{$item->id}}"></label>
                                    </div>
                                </th>
                                <td>
                                    {{$item->id}}
                                </td>
                                <td>
                                    <a href="{{ admin_base_path('basic/direction_exchange/'.$item->id.'/edit') }}">
                                        <b>{{$item->currency1->payment->name.' '.$item->currency1->code_currency->name}}
                                            → {{$item->currency2->payment->name.' '.$item->currency2->code_currency->name}}</b>
                                    </a>
                                </td>

                                <td>
                                    @if(isset($item->id_bestchange_rates) and $item->enable_bestchange == 1 and $item->is_disable_bs_error == 0)
                                        @if($item->is_enable_alt_bs_parser == 1 and isset($item->bs_alt_parser->group_parse_exchange))
                                            <s class="text-danger" style="font-size: 11px;">(Bestchange)</s> - {{ $item->bs_alt_parser->group_parse_exchange->name }}
                                        @else
                                            {{ __('BestChange') }}
                                        @endif
                                    @elseif($item->enable_competitors == true and $item->id_competitor)
                                        {{ __('Источник') }}: {{ $detail->competitor_rates->competitor_link->name  }}
                                    @elseif(isset($item->id_crypto_parser) and $item->id_crypto_parser > 0 and $item->enable_bestchange == 0)
                                        {{ $item->parser_exchange->group_parse_exchange->name }}
                                    @endif

                                    <div style="font-size: 11px; color:#999;">
                                        {!! converter_exception($item) !!}
                                    </div>
                                </td>
                                <td>
                                    @if($item->id_crypto_parser == 0)
                                        <span class="text-muted">-- {{ __('Нет значения') }} --</span>
                                    @else
                                        @if(isset($item->parser_exchange))
                                            <div> {{$item->parser_exchange->name}}</div>

                                            <div style="font-size: 11px;" class="text-primary" data-toggle="tooltip" title="{{ __('Источник') }} {{ $item->parser_exchange->group_parse_exchange->name }}">{{ $item->parser_exchange->group_parse_exchange->name }}</div>
                                        @endif
                                        @if($item->enable_group_commission > 0 and $item->id_group_commission > 0)
                                            <div style="font-size: 11px;color: #999;">{{ $item->group_commission->name}}</div>
                                        @endif
                                    @endif
                                </td>

                                <td>
                                    @if($item->enable_bestchange == true and $item->id_bestchange_rates)
                                        {{ $item->bestchange_rates->name  }}
                                        <br />
                                        <small class="text-muted">
                                            @if($item->bestchange_position == 0)
                                                {{ __('По умолчанию') }}: {{config('crypto.bestchange_position')}}
                                            @else
                                                {{ __('Позиция') }}: {{$item->bestchange_position}}
                                            @endif
                                        </small>


                                        @if(isset($item->bestchange_rates) and !empty($item->bestchange_rates->source_name))
                                            <div style="font-size: 11px" class="text-info">{{ __('Источник') }}: <b>{{ $item->bestchange_rates->source_name }}</b></div>
                                        @endif

                                    @else
                                        <span class="text-muted">{{ __('Отключен') }}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">

                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="reject">{{ __('Восстановить') }}</option>
                             </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>

    </div>

@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $exchanges->links() !!}
    </div>
@endsection
