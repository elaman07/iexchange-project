@extends('admin.layouts.app')

@section('title', __('Уведомление для направлений'))

@section('top-block')
    <md-button ng-href="direction-exchange-notification/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.notification'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Направление') }}</th>
            <th>{{ __('Текст') }}</th>
            <th>{{ __('Статус') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($notifications) > 0)
            @foreach($notifications as $notification)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/basic/direction-exchange-notification/'.$notification->id.'/edit') }}">
                            {{ $notification->direction_exchange->tech_name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>{!! $notification->description !!}</td>
                    <td>
                        @if($notification->status == 0)
                            <span class="text-danger">{{ __('Отключено') }}</span>
                        @else
                            <span class="text-success">{{ __('Включено') }}</span>
                        @endif
                    </td>

                    <td style="width: 50px;">
                        {!! Form::open(['id' => 'direction-exchange-notification_destroy_'.$notification->id, 'method' => 'DELETE', 'route' => ['direction-exchange-notification.destroy', $notification->id] ]) !!}
                        {!! Form::close() !!}

                        <a href="javascript:void(0)" onclick="document.getElementById('direction-exchange-notification_destroy_{{ $notification->id }}').submit()">
                            <i class="fas fa-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
