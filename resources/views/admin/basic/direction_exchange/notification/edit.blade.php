@extends('admin.layouts.app')

@section('title', __('Изменить уведомление'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.notification.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('basic/direction-exchange-notification/'.$item->id) }}">
        @csrf
        @method('put')

        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Изменить уведомление') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status" >
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Отключено') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Включено') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Направление') }}</div>
                        <select class="form-control selectpicker" name="id_direction_exchange" data-live-search="true" data-size="10">
                            @foreach($directions as $direction)
                                <option value="{{ $direction->id }}" @if($direction->id == $item->id_direction_exchange) selected @endif> {{ direction_name($direction) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Текст уведомления') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#description-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="description-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea id="ckedtor-multieditor-i{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-i{{ $form_lang_key }}" name="description{{$form_lang['field']}}" rows="5">{!! $item->getTranslation('description', $form_lang_key) !!}</textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Цвет текста') }}</div>
                        <div id="cp1" class="input-group colorpicker-component">
                            <input type="text" name="text_color" class="form-control" value="{{ $item->text_color }}" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фон текста') }}</div>
                        <div id="cp2" class="input-group colorpicker-component">
                            <input type="text" name="bg_color" class="form-control" value="{{ $item->bg_color }}" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('По расписанию') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Включить по расписанию') }}</div>
                        <select class="selectpicker" name="is_enabled_schedule">
                            <option value="0" @if($item->is_enabled_schedule == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if($item->is_enabled_schedule == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Период отображения (От)') }}</div>
                        <input type="text" name="from_time" class="form-control timepicker" value="{{$item->from_time}}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Период отображения (До)') }}</div>
                        <input type="text" name="to_time" class="form-control timepicker" value="{{$item->to_time}}">
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Всплывающее окно') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Включить всплывающее окно') }}</div>
                        <select class="form-control selectpicker" name="is_order_detail">
                            <option value="0" @if($item->is_order_detail == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if($item->is_order_detail == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}" class="form-control" value="{{ $item->getTranslation('title', $form_lang_key) }}" id="title{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('basic/direction-exchange-notification') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
