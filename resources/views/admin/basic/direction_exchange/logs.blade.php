@extends('admin.layouts.app')

@section('title', __('Лог направлений обменов'))

@section('top-block')

    <md-button ng-href="{{ admin_base_path('/basic/direction_exchange/logs?clear_logs') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-trash text-danger"></md-icon>
        <span class="text-danger">{{ __('Очистить лог') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.logs'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Дата создания') }}</th>
            <th>{{ __('Направление') }}</th>
            <th>{{ __('Где?') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Уровень риска') }}</th>
            <th>{{ __('Текст') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>
                        @if(isset($log->direction_exchange))
                            <a href="{{ admin_base_path('/basic/direction_exchange/'.$log->id_direction_exchange.'/edit') }}">{{ direction_name($log->direction_exchange) }}</a>
                        @else
                            {{ __('Не определен') }}
                        @endif
                    </td>
                    <td>
                        @if($log->where_error == 1)
                            {{ __('Парсер курсов') }}
                        @endif
                    </td>
                    <td>
                        @if($log->status == 1)
                            <div class="text-danger">{{ __('Ошибка') }}</div>
                        @endif
                    </td>
                    <td>
                        @if($log->level_risk == 1)
                            <div class="label label-danger">{{ __('Высокий') }}</div>
                        @endif
                    </td>
                    <td>{{ $log->text }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $logs->links() !!}
    </div>
@endsection
