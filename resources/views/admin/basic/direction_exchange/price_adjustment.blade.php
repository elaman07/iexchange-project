@extends('admin.layouts.app')

@section('title', __('Общая корректировка цен'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.price_adjustment'))

@section('top-block')

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection


@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Направление обмена') }}</th>
                <th>{{ __('Минимальная цена (Отдаю)') }}</th>
                <th>{{ __('Максимальная цена (Отдаю)') }}</th>
                <th>{{ __('Минимальная цена (Получаю)') }}</th>
                <th>{{ __('Максимальная цена (Получаю)') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($directions as $direction)
                <tr>
                    <td>
                        <input type="hidden" name="item_id[]" value="{{ $direction->id }}">


                        <div>
                            <a href="{{ admin_base_path('basic/direction_exchange/'.$direction->id.'/edit') }}">
                                <b>{{ $direction->tech_name }}</b>
                            </a>
                        </div>
                        @if($direction->enable_bestchange)
                            <small class="text-muted">{{ __('Активирован парсинг BestChange') }}</small>
                        @endif
                    </td>
                    <td>
                        <input type="text" class="form-control" style="width: 200px;" name="min_price1[{{$direction->id}}]" value="{{$direction->min_price1}}">
                        <div class="form-check">
                            <input class="form-check-input" id="is_manual_min_price1[{{$direction->id}}]" value="1" type="checkbox" name="is_manual_min_price1[{{$direction->id}}]" @if($direction->is_manual_min_price1 == 1) checked @endif>
                            <label class="form-check-label" for="is_manual_min_price1[{{$direction->id}}]">{{ __('Ручное обновление мин. цены') }}</label>
                        </div>
                    </td>

                    <td>
                        <input type="text" class="form-control" style="width: 200px" name="max_price1[{{$direction->id}}]" value="{{$direction->max_price1}}">
                        <div class="form-check">
                            <input class="form-check-input" id="is_manual_max_price1[{{$direction->id}}]" value="1" type="checkbox" name="is_manual_max_price1[{{$direction->id}}]" @if($direction->is_manual_max_price1 == 1) checked @endif>
                            <label class="form-check-label" for="is_manual_max_price1[{{$direction->id}}]">{{ __('Ручное обновление макс. цены') }}</label>
                        </div>
                    </td>

                    <td>
                        <input type="text" class="form-control" style="width: 200px" name="min_price2[{{$direction->id}}]" value="{{$direction->min_price2}}">
                        <div class="form-check">
                            <input class="form-check-input"" id="is_manual_min_price2[{{$direction->id}}]" value="1" type="checkbox" name="is_manual_min_price2[{{$direction->id}}]" @if($direction->is_manual_min_price2 == 1) checked @endif>
                            <label class="form-check-label" for="is_manual_min_price2[{{$direction->id}}]">{{ __('Ручное обновление мин. цены') }}</label>
                        </div>
                    </td>

                    <td>
                        <input type="text" class="form-control" style="width: 200px" name="max_price2[{{$direction->id}}]" value="{{$direction->max_price2}}">
                        <div class="form-check">
                            <input class="form-check-input" id="is_manual_max_price2[{{$direction->id}}]" value="1" type="checkbox" name="is_manual_max_price2[{{$direction->id}}]" @if($direction->is_manual_max_price2 == 1) checked @endif>
                            <label class="form-check-label" for="is_manual_max_price2[{{$direction->id}}]">{{ __('Ручное обновление макс. цены') }}</label>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('Сохранить') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Кол-во записей на страницу') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="price_adjustment_pagination" value="{{ iEXSetting('price_adjustment_pagination', 200) }}">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $directions->links() !!}
    </div>
@endsection
