@extends('admin.layouts.app')

@section('title', __('Изменить групповую комиссию').' '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.commission.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction-exchange-commission/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('Название') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control" value="{{$item->name}}" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="receiving" class="col-sm-2 control-label">{{ __('Прибавление к курсу') }}</label>
                <div class="col-sm-3">
                    <input type="text" name="receiving" class="form-control" id="receiving" value="{{$item->receiving}}" placeholder="{{ __('Укажите значение дополнительное для курса: Пример: 1%, 1, -1, /1, -1%..)') }}" required>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction-exchange-commission') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
