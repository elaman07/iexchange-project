@extends('admin.layouts.app')

@section('title', __('Список групповых комиссий'))

@section('top-block')
    <md-button ng-href="direction-exchange-commission/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.commission'))


@section('content')
    <form method="POST" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Название') }}</th>
                <th>{{ __('Прибавление к курсу') }}</th>
                <th>{{ __('Направления') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($groups) > 0)
                @foreach($groups as $group)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('basic/direction-exchange-commission/'.$group->id.'/edit') }}">
                                {{ $group->name }}
                            </a>
                        </th>
                        <td  style="width: 200px">
                            <input type="text" name="group_out[{{$group->id}}]" style="width: 70px;text-align: center;" class="form-control" value="{{$group->receiving}}">
                        </td>

                        <td style="width: 300px">
                            @if($group->direction_exchange->count() > 0)
                                <ul class="list-unstyled">
                                    @foreach($group->direction_exchange as $value)
                                        <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/direction_exchange/'.$value->id.'/edit') }}">{{ $value->tech_name }}</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <small class="text-danger">{{ __('Не найдено') }}</small>
                            @endif
                        </td>
                        <td style="width: 100px;">

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('basic/direction-exchange-commission/'.$group->id.'/delete')  }}">
                                <i class="far fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('Сохранить') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
