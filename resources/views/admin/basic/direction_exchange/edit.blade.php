@extends('admin.layouts.app')

@section('title', __('Изменить направление').' '.$data->tech_name)

@section('disable-body-wrapper', ' ')

{{--@section('custom-nav-title')--}}
{{--    {{ __('admin-basic.direction_exchange.main.edit_title') }}--}}
{{--    «<span class="ex_title1" style="color: #b72727;"></span> - <span style="color: #489a39" class="ex_title2"></span>»--}}
{{--@endsection--}}

@section('custom-js-head')
    <style>

        .custom-col-select {
            margin-left: 5px;
        }


        .control-label-br {
            font-weight: 500;
            margin-bottom: 10px;
            font-size: 13px;
            color: #999;
        }

        .default-panel-body {
            padding-left: 0;
        }

        .default-panel-body .col-md-3 {
            padding-left: 0;
        }

        .tabs-left>li.active>a, .tabs-left>li.active>a:focus, .tabs-left>li.active>a:hover {
            border-left: 0;
            border-radius: 0;
        }

        .tabs-left>li.active>a, .tabs-left>li.active>a:hover, .tabs-left>li.active>a:focus {
            border-bottom-color: transparent;
            border-right-color: transparent;
            border: none;
            background: #303f9f1a;
            color: #333 !important;
        }

        .nav-tabs, .nav-tabs>li>a, .nav-tabs > li > a:hover {
            border: none !important;
        }

        .nav-tabs, .nav-tabs>li>a {
            color: #757575;
        }

        .nav-tabs > li > a:hover {
            background: rgba(233, 233, 233, 0.65);
            color: #333 !important;
        }

        .title-give {
            top: -30px;
        }

        .direction-col-body {
            padding: 0;
            margin-top: -7px;
            height: 100%;
            flex-direction: row;
            box-sizing: border-box;
            display: flex;
            place-content: stretch flex-start;
            align-items: stretch;
            max-height: 100%;
        }

        .direction-col-content {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .direction-col-body .direction-col-line {
            border-right: 1px solid #e9e9e9;
            padding: 0;
        }

        .tabs-left>li:last-child {
            margin-bottom: 0;
        }

        .bootstrap-select.form-control {
            background: transparent;
        }

        .direction-exchange-toolbar {
            background: transparent !important;
            padding-top: 2rem;
            min-height: auto;
            border-bottom: 1px solid #e9e9e9e9;
            padding-bottom: 2rem;
            margin: 0;
            font-family: Nunito,sans-serif;
        }

        .direction-exchange-toolbar > h1 {
            display: block;
            font-weight: 600;
        }

        .setting-sidenav-title h2 {
            font-family: Nunito,sans-serif;
        }

        @media (min-width: 100px) and (max-width: 599px)
        {
            .setting-sidenav-wrapper {
                padding: 10px;
            }
        }

        .text-direction__rate {
            font-size: 13px;
            color: #5e5e5e;
            background: #e5e7eb;
            padding: 7px 10px;
            border-radius: 5px;
            margin-left: 5px;
            font-weight: 500;
        }

        .setting-sidenav-menu-item.active .text-direction__rate {
            background: #f9fbfc;
        }


        .direction__icon-tab__selected {
            font-size: 15px !important;
            margin-left: 5px !important;
            color: #22a522 !important;
        }
    </style>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.change',$data->id))

{{--@section('nav')--}}
{{--    @can('admin_limit_editing_directions')--}}
{{--        <div class="alert alert-danger text-center">  {{ __('admin-basic.direction_exchange.main.admin_limit_editing_directions') }}</div>--}}
{{--    @endcan--}}
{{--@endsection--}}

@section('hide-admin-header')

@stop

@section('no-block-content')

    <div layout="row" flex ng-controller="SettingsSidenav as ctrl" class="setting-sidenav-bg">
        <md-sidenav flex="25" class="md-sidenav-left setting-sidenav" md-component-id="settings" md-is-locked-open="$mdMedia('gt-md')" md-whiteframe="4">


            <md-toolbar class="direction-exchange-toolbar">
                <h1 class="md-toolbar-tools" style="color: #000">
                    {{ __('Изменить направление') }}<br />
                    «<span class="ex_title1" style="color: #b72727;"></span> - <span style="color: #489a39" class="ex_title2"></span>»
                </h1>

            </md-toolbar>
            <md-content >
                <ul class="setting-sidenav-menu" style="padding: 0" id="directionTab" ng-click="close()">
                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#basic_settings" role="tab" data-toggle="tab" data-tab-title="{{ __('Основные настройки') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Основные настройки') }} </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#course" role="tab" data-toggle="tab" data-tab-title="{{ __('Курс') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">
                                    {{ __('Курс') }}

                                    @if(!empty($data->exchange_rate) or $data->is_error_rate)
                                    <span class="text-direction__rate">
                                        @if($data->is_error_rate)
                                            <span class="text-danger">{{ __('Курс не настроен') }}</span>
                                        @else
                                            {{ $data->exchange_rate }}
                                        @endif
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>

                    @if(\Module::find('Cities')->isEnabled() == 1)
                        <li class="setting-sidenav-menu-item">
                            <a layout="row" href="#cities" role="tab" data-toggle="tab" data-tab-title="{{ __('Города') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                                <div class="sss tip">
                                    <div class="setting-sidenav-menu-item-title">{{ __('Города') }}</div>
                                </div>
                            </a>
                        </li>
                    @endif

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#bestchange_parser" role="tab" data-toggle="tab" data-tab-title="{{ __('BestChange парсер') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">
                                    {{ __('BestChange парсер') }}
                                    @if($data->enable_bestchange == 1)
                                        <i class="far fa-check direction__icon-tab__selected"></i>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#autocorrect_course" role="tab" data-toggle="tab" data-tab-title="{{ __('Автокорректировка курса') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">
                                    {{ __('Автокорректировка курса') }}
                                    @if($data->id_crypto_parser > 0)
                                        <i class="far fa-check direction__icon-tab__selected"></i>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#rate_limitations" role="tab" data-toggle="tab" data-tab-title="{{ __('Ограничения курса обмена') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Ограничения курса обмена') }} </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#commission_of_exchange_service" data-tab-title="{{ __('Комиссии') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">
                                    {{ __('Комиссии') }}
                                    @if($data->id_group_commission > 0 and isset($data->group_commission))
                                        <span class="text-direction__rate">{{ $data->group_commission->receiving }}%</span>
                                    @elseif($data->profit > 0)
                                        <span class="text-direction__rate">{{ $data->profit }}%{{ $data->profit_s > 0 ? ' / '.$data->profit_s.'%' : '' }}</span>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#sum_of_exchanges" data-tab-title="{{ __('Сумма обмена') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Сумма обмена') }} </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#information_for_user" data-tab-title="{{ __('Инструкции и SEO') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Инструкции и SEO') }} </div>

                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#direction_requisites" data-tab-title="{{ __('Реквизиты') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Реквизиты') }} </div>

                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#displaying_fields" data-tab-title="{{ __('Отображение полей') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Отображение полей') }} </div>

                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#merchant_api" data-tab-title="{{ __('Мерчанты и Выплаты') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Мерчанты и Выплаты') }} </div>

                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#restrictions_and_checks" data-tab-title="{{ __('Ограничения и проверки') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Ограничения и проверки') }} </div>

                            </div>
                        </a>
                    </li>
                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#removing_unpaid_applications" data-tab-title="{{ __('Удаление неоплаченных заявок') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Удаление неоплаченных заявок') }} </div>
                            </div>
                        </a>
                    </li>
                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#config_export_file" data-tab-title="{{ __('Настройка экспортных файлов') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Настройка экспортных файлов') }} </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#partner_program" data-tab-title="{{ __('Партнерская программа') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Партнерская программа') }} </div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#telegram_bot" data-tab-title="{{ __('Telegram Bot') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Telegram Bot') }}</div>
                            </div>
                        </a>
                    </li>
                </ul>
            </md-content>
        </md-sidenav>
        <!-- Данные -->
        <md-content class="setting-sidenav-wrapper">

            <div class="setting-sidenav-title" style="border: none;margin-top: 5px;" layout="row" layout-align-xs="start center">
                <md-button ng-click="toggleSettings()" class="md-primary md-icon-button" hide-gt-md>
                    <i class="fa fa-fw fa-bars"></i>
                </md-button>
                <h2></h2>
                <div class="clearfix"></div>
            </div>

            <div class="x_content mt-8">
                <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction_exchange/'.$data->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="tab-content">
                        <div class="tab-pane row" id="basic_settings">
                            <!-- Направление -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Направление') }}</label>
                                <div class="col-sm-10">
                                      <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Отдаете') }}</div>
                                        <select id="id_currency1" class="form-control selectpicker" name="id_currency1" data-live-search="true" data-size="10">
                                            @foreach($currencies as $item)
                                                <option value="{{$item->id}}" @if($data->id_currency1 == $item->id) selected @endif>{{$item->payment->name}} {{$item->code_currency->name}} @if($item->status == 1) ({{ __('отключенная валюта') }}) @endif</option>
                                            @endforeach
                                        </select>
                                    </div>

                                      <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Получаете') }}</div>
                                        <select id="id_currency2" class="form-control selectpicker" name="id_currency2" data-live-search="true" data-size="10">
                                            @foreach($currencies as $item)
                                                <option value="{{$item->id}}" @if($data->id_currency2 == $item->id) selected @endif>{{$item->payment->name}} {{$item->code_currency->name}}  @if($item->status == 1) ({{ __('отключенная валюта') }}) @endif</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <div class="form-group col-lg-12">
                                        <div class="control-label-br">{{ __('Название направления (техническое)') }}</div>
                                        <input type="text" name="tech_name" class="form-control" value="{{ $data->tech_name }}">
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                      <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Группа') }}</div>
                                        <select id="id_group_direction" class="form-control selectpicker" name="id_group_direction" data-live-search="true" data-size="10">
                                            <option value="0">-- {{ __('Нет значения') }} --</option>
                                            @foreach($groups_direction as $item)
                                                <option value="{{$item->id}}" @if($data->id_group_direction == $item->id) selected @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>

                                        <div class="input-p-text ">{!! __('Выберите из списка группу, для удобного поиска направлений') !!}</div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <!-- Статус -->
                                      <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Статус') }}</div>
                                        <select id="status" class="form-control selectpicker" name="status">
                                            <option value="0" @if($data->status == 0) selected @endif >{{ __('Отключен') }}</option>
                                            <option value="1" @if($data->status == 1) selected @endif >{{ __('Включен') }}</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                      <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Режимы отображения направлений') }}</div>
                                        {!! Form::select('ids_direction_modes[]', $direction_modes,  $data->direction_modes, ['multiple' => true, 'class' => 'form-control selectpicker', 'data-width' => '100%', 'data-size' => '10', 'data-live-search' => 'true'])  !!}

                                        <div class="input-p-text">{{ __('Используется для работы обменника в нескольких режимах. К примеру для Ночного режима один тип направлений для дневного другой.') }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="course">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Ручной курс обмена') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-lg-6">
                                        1 =
                                        <input type="text" class="form-control" name="manual_rate_value" value="{{ $data->manual_rate_value ?? 0 }}" style="width: 200px;display: inline-block;">
                                    </div>
                                </div>
                            </div>


                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Курсы обмена из формулы') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <select class="form-control selectpicker show-tick" name="id_parser_formula_rate" data-live-search="true" data-size="15">
                                            <option value="0" @if($data->id_parser_formula_rate == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($parser_formula as $parser_formula_value)
                                                <option value="{{ $parser_formula_value->id }}" @if($data->id_parser_formula_rate == $parser_formula_value->id) selected @endif>{{ $parser_formula_value->title }} ({{ $parser_formula_value->value }} → {{ $parser_formula_value->summa }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Курс обмена из файла') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <select class="form-control selectpicker show-tick" name="id_file_parser_rate" data-live-search="true" data-size="15">
                                            <option value="0" @if($data->id_file_parser_rate == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($file_parser_groups as $file_parser_group)
                                                <optgroup label="{{ $file_parser_group->name }}">
                                                    @foreach($file_parser_group->file_parser_rates as $value)
                                                        <option value="{{ $value->id }}" @if($data->id_file_parser_rate == $value->id) selected @endif>{{ $value->name }} ({{ $value->value }} → {{ $value->summa }})</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />


                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Курсы конкурентов') }}</label>

                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Курсы конкурентов') }}</div>
                                        <select class="form-control selectpicker show-tick" name="id_competitor" data-live-search="true" data-size="15">
                                            <option value="0" @if($data->id_competitor == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($competitors_links as $competitors_link)
                                                <optgroup label="{{$competitors_link->name}}">
                                                    @foreach($competitors_link->competitor_rates as $competitor_rate)
                                                        <option value="{{$competitor_rate->id}}" @if($data->id_competitor == $competitor_rate->id) selected @endif>{{$competitor_rate->name}} ({{$competitor_rate->value}} → {{$competitor_rate->summa}})</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Мин. курс') }}</div>
                                        <input type="text" class="form-control" name="cr_min_sum" value="{{ $data->cr_min_sum }}">
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Макс. курс') }}</div>
                                        <input type="text" class="form-control" name="cr_max_sum" value="{{ $data->cr_max_sum }}">
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Сбросить на курс') }}</div>
                                        <select class="form-control selectpicker show-tick" name="cr_id_new_rate" data-live-search="true" data-size="15">
                                            <option value="0" @if($data->cr_id_new_rate == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($group_rates as $group)
                                                @if(isset($parser_exchange[$group->id]))
                                                    <optgroup label="{{$group->name}}">
                                                        @foreach($parser_exchange[$group->id] as $item)
                                                            <option value="{{ $item['id'] }}" @if($data->cr_id_new_rate == $item['id']) selected @endif>{{$item['name']}} ({{$item['value']}} → {{$item['summa']}})</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="cr_add_course" class="form-control" value="{{ $data->cr_add_course }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Кратность суммы обмены') }}</label>

                                <div class="col-md-10">
                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Сумма') }}</div>
                                        <select class="form-control selectpicker show-tick" name="multiplicity_type" data-live-search="true" data-size="15">
                                            <option value="0" @if($data->multiplicity_type == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            <option value="1" @if($data->multiplicity_type == 1) selected @endif>{{ __('Сумма Отдаю') }}</option>
                                            <option value="2" @if($data->multiplicity_type == 2) selected @endif>{{ __('Сумма Получаю') }}</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Кратность') }}</div>
                                        <input type="text" class="form-control" name="multiplicity_amount" value="{{ $data->multiplicity_amount ?? 0 }}">
                                    </div>

                                    <div class="form-group col-md-6 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Комментарий в случае ошибки') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#multiplicity_comment-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="multiplicity_comment-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea class="form-control" name="multiplicity_comment{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('multiplicity_comment', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        @if(\Module::find('Cities')->isEnabled() == 1)
                            <div class="tab-pane cities__block" id="cities" data-direction-id="{{ $data->id }}">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Список городов') }}</label>
                                    <div class="col-md-10">
                                           <div class="form-group col-lg-6">
                                            {{ Form::select('ids_cities[]', $cities_data, $data->direction_cities, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Настройка городов') }}</label>
                                    <div class="col-md-10">
                                        @if(count($data->direction_exchange_cities) > 0)
                                        @foreach($data->direction_exchange_cities as $value)
                                            <div class="cities__block-item cities__block-item-{{ $value->id }}">
                                                <h3>{{ $value->city->name }}</h3>

                                                   <div class="form-group col-lg-6">
                                                    <div class="control-label-br">{{ __('Выберите страну') }}</div>
                                                    <select class="form-control selectpicker cities_item-field" data-live-search="true" data-size="10" name="id_country" data-field-name="id_country" data-city-id="{{ $value->id }}">
                                                        @foreach($geo_countries as $key_country => $value_country)
                                                            <option value="{{ $key_country }}" @if($key_country == $value->id_country) selected @endif>{{ $value_country }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                   <div class="form-group col-lg-6">
                                                    <div class="control-label-br">{{ __('Добавлять к курсу') }}</div>
                                                    <input type="text" name="add_comm" data-field-name="add_comm" data-city-id="{{ $value->id }}" class="form-control cities_item-field" value="{{ $value->add_comm }}">
                                                </div>

                                                   <div class="form-group col-lg-6">
                                                    <div class="control-label-br">{{ __('Метки параметра param (для XML)') }}</div>
                                                    <input  type="text" name="param" data-field-name="param"  data-city-id="{{ $value->id }}" class="form-control cities_item-field" value="{{ $value->param }}">
                                                </div>

                                                <div class="clearfix"></div>
                                                   <div class="form-group col-lg-6">
                                                    <div class="control-label-br">{{ __('Минимальная сумма') }}</div>
                                                    <input  type="text" name="min_price" data-field-name="min_price" data-city-id="{{ $value->id }}" class="form-control cities_item-field" value="{{ $value->min_price }}">
                                                </div>

                                                   <div class="form-group col-lg-6">
                                                    <div class="control-label-br">{{ __('Максимальная сумма') }}</div>
                                                    <input type="text" name="max_price" data-field-name="max_price" data-city-id="{{ $value->id }}" class="form-control cities_item-field" value="{{ $value->max_price }}">
                                                </div>

                                                <div class="clearfix"></div>
                                                <md-button class="md-raised md-primary event_cities_click" type="submit">{{ __('Сохранить') }}</md-button>
                                                <md-button class="md-raised md-warn event_cities_click_trash" type="submit">{{ __('Удалить') }}</md-button>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endforeach
                                        @else
                                            <div class="text-danger">
                                                {{ __('Чтобы настроить данную вкладку, добавьте хотя-бы один город из раздела Список городов') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        @endif

                                <div class="tab-pane" id="bestchange_parser">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('BestChange парсер') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <select name="enable_bestchange" class="form-control selectpicker">
                                            <option value="0" @if($data->enable_bestchange == 0) selected @endif>{{ __('Отключить') }}</option>
                                            <option value="1" @if($data->enable_bestchange == 1) selected @endif>{{ __('Включить') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Настройка Bestchange') }}</label>
                                <div class="col-md-10">

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Курсы из BestChange') }}</div>
                                        <select id="id_bestchange_rates" data-live-search="true" name="id_bestchange_rates" class="form-control selectpicker" data-size="20">
                                            <option value="0" @if($data->id_bestchange_rates == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            <option value="add">-- {{ __('Добавить новый парсер') }} --</option>
                                            @foreach($bestchange_rates as $value)
                                                <option value="{{$value->id}}" @if($data->id_bestchange_rates == $value->id) selected @endif data-subtext="({{$value->value}}-{{$value->summa}})">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="show_detail_id_bestchange_rates">

                                        <div class="clearfix"></div>

                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Отдаете') }}</div>
                                            {!! Form::select('bestchange_rates_exchanger_id1', $bestchange_currencies, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Получаете') }}</div>
                                            {!! Form::select('bestchange_rates_exchanger_id2', $bestchange_currencies, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Знаков после запятой') }}</div>
                                            <input type="text" class="form-control" name="bestchange_rates_number_format" value="{{ old('bestchange_rates_number_format', 10) }}">
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Позиция по умолчанию') }}</div>
                                            <input type="text" class="form-control" name="bestchange_rates_position" value="{{ old('bestchange_rates_position', 0) }}">
                                            <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                                <b>0</b> - {{ __('берется позиция по умолчанию') }} <b>{{ config('crypto.bestchange_position') }}</b>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Позиция') }}</div>
                                        <input type="text" name="bestchange_position" class="form-control" value="{{ $data->bestchange_position }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_change_bestchange_range" value="1" type="checkbox" name="is_change_bestchange_range" @if($data->is_change_bestchange_range == 1) checked @endif>
                                            <label class="form-check-label" for="is_change_bestchange_range">{{ __('Менять позицию в диапазоне') }}</label>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Диапазон') }}</div>
                                        <input type="text" name="bestchange_range" class="form-control" value="{{ $data->bestchange_range }}">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <div class="col-sm-6" style="padding: 0">
                                            <div class="control-label-br">{{ __('От') }}</div>
                                            <input type="text" name="bestchange_range_from" class="form-control timepicker" value="{{ $data->bestchange_range_from }}">
                                        </div>

                                        <div class="col-sm-6" style="padding-right: 0;">
                                            <div class="control-label-br">{{ __('До') }}</div>
                                            <input type="text" name="bestchange_range_to" class="form-control timepicker" value="{{ $data->bestchange_range_to }}">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">{{ __('Если поля пуcтые, ограничение снимается') }}</div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Шаг') }}</div>
                                        <input type="text" name="bestchange_step" class="form-control" value="{{ $data->bestchange_step }}">
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />


                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Город') }}</div>
                                        <select class="form-control selectpicker" name="bestchange_city" data-live-search="true" data-size="10">
                                            @foreach($bs_cities as $key => $value)
                                                <option value="{{ $key }}" @if($data->bestchange_city == $key) selected @endif>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />



                                    <div class="text-center">
                                        <a class="btn btn-default" data-toggle="collapse" href="#bestChangeOtherConfiguration" role="button" aria-expanded="false" aria-controls="bestChangeOtherConfiguration">{{ __('Показать доп. настройки') }}</a>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="collapse" id="bestChangeOtherConfiguration">
                                        <br />
                                        <br />
                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Сбросить на другой парсинг') }}</div>
                                            <select class="form-control selectpicker show-tick" name="id_bs_alt_parser" data-live-search="true" data-size="10">
                                                <option value="0"  @if($data->id_bs_alt_parser == 0) selected @endif>-- {{ __('Не укзаано') }} --</option>
                                                @foreach($group_rates as $group)
                                                    @if(isset($parser_exchange[$group->id]))
                                                        <optgroup label="{{$group->name}}">
                                                            @foreach($parser_exchange[$group->id] as $item)
                                                                <option value="{{ $item['id'] }}" @if($data->id_bs_alt_parser == $item['id']) selected @endif>{{$item['name']}} ({{$item['value']}} → {{$item['summa']}})</option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endif
                                                @endforeach
                                            </select>

                                            <div class="input-p-text">{{ __('С этого источника будут парсится только если в Bestchange отсутствует курсы по выбранному направлению') }}</div>
                                        </div>

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="bs_alt_parser_course" class="form-control" value="{{ $data->bs_alt_parser_course }}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Минимальный резерв для позиции') }}</div>
                                            <input type="text" name="bestchange_min_reserve" class="form-control" value="{{ (!empty($data->bestchange_min_reserve) ? $data->bestchange_min_reserve : 0) }}">
                                            <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Максимальный резерв для позиции') }}</div>
                                            <input type="text" name="bestchange_max_reserve" class="form-control" value="{{ (!empty($data->bestchange_max_reserve) ? $data->bestchange_max_reserve : 0) }}">
                                            <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                         <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Черный список ID обменников') }}</div>
                                            {!! Form::select('bestchange_bl[]', $bs_exchangers, explode(',', $data->bestchange_bl), ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '10']) !!}
                                        </div>


                                         <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Белый список ID обменников') }}</div>
                                            {!! Form::select('bestchange_wl[]', $bs_exchangers, explode(',', $data->bestchange_wl), ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '10']) !!}
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                         <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Мин. курс') }}</div>
                                            <input type="text" name="bc_min_sum" class="form-control" value="{{ $data->bc_min_sum }}">
                                        </div>

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Микс. курс') }}</div>
                                            <input type="text" name="bc_max_sum" class="form-control" value="{{ $data->bc_max_sum }}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />


                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="bc_new_commission" class="form-control" value="{{ $data->bc_new_commission }}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>
                                            <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                                {{ __('Это поле будет использовано в случае если курс BestChange превышает установленный максимум, либо сбросить на другой парсинг') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Автокорректировка курса') }}</div>
                                            <select class="form-control selectpicker show-tick" name="bc_id_new_rate" data-live-search="true" data-size="10">
                                                <option value="0"  @if($data->bc_id_new_rate == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                                @foreach($group_rates as $group)
                                                    @if(isset($parser_exchange[$group->id]))
                                                        <optgroup label="{{$group->name}}">
                                                            @foreach($parser_exchange[$group->id] as $item)
                                                                <option value="{{ $item['id'] }}" @if($data->bc_id_new_rate == $item['id']) selected @endif>{{$item['name']}} ({{$item['value']}} → {{$item['summa']}})</option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="bc_add_course" class="form-control" value="{{ $data->bc_add_course }}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-2">
                                            <div class="control-label-br">{{ __('Сбрасывать на свой курс') }}</div>
                                            <select name="bc_enable_your_course" class="form-control selectpicker">
                                                <option value="0" @if($data->bc_enable_your_course == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($data->bc_enable_your_course == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>

                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Сбросить на курс') }}</div>
                                            <select class="form-control selectpicker" name="bc_id_your_exchange" data-live-search="true" data-size="10">
                                                <option value="0" @if($data->bc_id_your_exchange == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                                @foreach($your_exchange as $course)
                                                    <option value="{{$course->id}}" @if($data->bc_id_your_exchange == $course->id) selected @endif>{{$course->name}} (1 → {{$course->exchange_rate}})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                           <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="bc_your_add_course" class="form-control" value="{{ $data->bc_your_add_course }}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="autocorrect_course">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Автокорректировка курса') }}</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Список курсов') }}</div>
                                        <select class="form-control selectpicker show-tick" name="id_crypto_parser" data-live-search="true" data-show-subtext="true" data-size="10">
                                            <option value="0" @if($data->id_crypto_parser == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($group_rates as $group)
                                                @if(isset($parser_exchange[$group->id]))
                                                    <optgroup label="{{$group->name}}">
                                                        @foreach($parser_exchange[$group->id] as $item)
                                                            <option value="{{ $item['id'] }}" @if($data->id_crypto_parser == $item['id']) selected @endif>{{$item['name']}} ({{$item['value']}} → {{$item['summa']}})</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="add_course1" class="form-control" value="{{ $data->add_course1 }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                        <div class="input-group" style="margin-top: 5px">
                                            <input type="text" name="add_course1_s" class="form-control" value="{{ $data->add_course1_s }}">
                                            <span class="input-group-addon" id="basic-addon2">S</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Свой курс') }}</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Список курсов') }}</div>
                                        <select class="form-control selectpicker" name="id_your_exchange" data-live-search="true"  data-size="10">
                                            <option value="0"  @if($data->id_your_exchange == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($your_exchange as $course)
                                                <option value="{{  $course->id }}" @if($data->id_your_exchange == $course->id) selected @endif>{{  $course->name }} (1 → {{ iex_number_format($course->exchange_rate, $course->number_format) }})</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="your_add_course1" class="form-control" value="{{ $data->your_add_course1 }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                        <div class="input-group" style="margin-top: 5px">
                                            <input type="text" name="your_add_course1_s" class="form-control" value="{{ $data->your_add_course1_s }}">
                                            <span class="input-group-addon" id="basic-addon2">S</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="rate_limitations">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">{{ __('Ограничения курса обмена') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Мин. курс') }}</div>
                                        <input type="text" name="rl_min2_course" class="form-control" value="{{ $data->rl_min2_course }}">
                                    </div>
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Макс. курс') }}</div>
                                        <input type="text" name="rl_max2_course" class="form-control" value="{{ $data->rl_max2_course }}">
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Сбросить на стандартный курс') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Автокорректировка курса') }}</div>
                                        <select class="form-control selectpicker show-tick" name="rl_id_parser_exchange" data-live-search="true" data-size="10">
                                            <option value="0"  @if($data->rl_id_parser_exchange == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($group_rates as $group)
                                                @if(isset($parser_exchange[$group->id]))
                                                    <optgroup label="{{$group->name}}">
                                                        @foreach($parser_exchange[$group->id] as $item)
                                                            <option value="{{ $item['id'] }}" @if($data->rl_id_parser_exchange == $item['id']) selected @endif>{{$item['name']}} ({{$item['value']}} → {{$item['summa']}})</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="rl_add_course" class="form-control" value="{{ $data->rl_add_course }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Свой курс') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Сбросить на курс') }}</div>
                                        <select class="form-control selectpicker" name="rl_id_your_exchange" data-live-search="true"  data-size="10">
                                            <option value="0"  @if($data->rl_id_your_exchange == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($your_exchange as $course)
                                                <option value="{{$course->id}}" @if($data->rl_id_your_exchange == $course->id) selected @endif>{{$course->name}} (1 → {{$course->exchange_rate}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="rl_your_add_course" class="form-control" value="{{ $data->rl_your_add_course }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane exchange_amount__block" data-direction-id="{{ $data->id }}" id="commission_of_exchange_service">

                            <div class="form-group">
                                <label for="profit" class="col-sm-2 control-label">{{ __('Прибыль') }}</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Тип расчета прибыли') }}</div>
                                        <select class="form-control selectpicker" name="type_profit_field">
                                            <option value="0" @if($data->type_profit_field == 0) selected @endif>{{ __('По умолчанию') }}</option>
                                            <option value="1" @if($data->type_profit_field == 1) selected @endif>{{ __('Динамический') }}</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('В процентах') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="profit" class="form-control" value="{{ $data->profit }}">
                                            <span class="input-group-addon" id="basic-addon3">%</span>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('В валюте') }}</div>
                                        <input type="text" name="profit_s" class="form-control" value="{{ $data->profit_s }}">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="profit" class="col-sm-2 control-label">{{ __('Групповая комиссия') }}</label>
                                <div class="col-sm-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Список комиссий') }}</div>
                                        <select data-live-search="true" name="id_group_commission" class="form-control selectpicker" data-size="10" data-show-subtext="true">
                                            <option value="0" @if($data->id_group_commission == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                            @foreach($group_commission as $value)
                                                <option @if($data->id_group_commission == $value->id) selected @endif value="{{$value->id}}" data-subtext="(Комиссия {{$value->receiving}}%)">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Комиссия зависящая от суммы обмена') }}</label>
                                <div class="col-sm-10">

                                    <a class="btn btn-primary" href="?actions=exchange_amount&elem=1">{{ __('Добавить 1 элемент') }}</a>
                                    <a class="btn btn-primary" href="?actions=exchange_amount&elem=3">{{ __('Добавить 3 элемента') }}</a>
                                    <div class="clearfix"></div>
                                    <hr />

                                    @if(!isset($data->direction_exchange_percentage_amount) or count($data->direction_exchange_percentage_amount) == 0)
                                        <div class="text-danger">{{ __('Нет добавленных элементов для настройки сумм') }}</div>
                                    @else


                                        <div class="form-group col-md-2">
                                            <div class="control-label-br">{{ __('Включить метку для клиентов') }}</div>
                                            <select name="is_notify_exchange_amount" class="form-control selectpicker">
                                                <option value="0" @if($data->is_notify_exchange_amount == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($data->is_notify_exchange_amount == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                            <p class="input-p-text">
                                                {{ __('После включения данной функции, клиент будет уведомлен о том, что в зависимости от суммы будет меняться комиссия') }}
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        @foreach($data->direction_exchange_percentage_amount as $percentage_amount)
                                            <div class="exchange_amount__block-item exchange_amount__block-item-{{ $percentage_amount->id }}">
                                                <div class="form-group col-md-4">
                                                    <div class="control-label-br">{{ __('Сумма обмена') }}</div>
                                                    <input type="text" name="exchange_amount" data-field-name="exchange_amount" class="form-control exchange_amount_item-field" value="{{ $percentage_amount->amount }}" data-percentage-amount-id="{{ $percentage_amount->id }}">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <div class="control-label-br">{{ __('Прибавление к курсу') }}</div>
                                                    <input type="text" name="exchange_percentage" data-field-name="exchange_percentage" class="form-control exchange_amount_item-field" value="{{ $percentage_amount->percentage }}" data-percentage-amount-id="{{ $percentage_amount->id }}">
                                                </div>

                                                <div class="clearfix"></div>
                                                <md-button class="md-raised md-primary event_exchange_amount_click" type="submit">{{ __('Сохранить') }}</md-button>
                                                <md-button class="md-raised md-warn event_exchange_amount_click_trash" type="submit">{{ __('Удалить') }}</md-button>
                                                <div class="clearfix"></div>
                                                <br />
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="commission1" class="col-sm-2 control-label">{{ __('Дополнительная комиссия') }}</label>
                                <div class="col-sm-10">

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Для отдаю') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="oth_comm_percent" class="form-control" value="{{ $data->oth_comm_percent }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                        <br />
                                        <div class="input-group">
                                            <input type="text" name="oth_comm_currency" class="form-control" value="{{ $data->oth_comm_currency }}">
                                            <span class="input-group-addon" id="basic-addon2">S</span>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Для получаю') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="oth_comm2_percent" class="form-control" value="{{ $data->oth_comm2_percent }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                        <br />
                                        <div class="input-group">
                                            <input type="text" name="oth_comm2_currency" class="form-control" value="{{ $data->oth_comm2_currency }}">
                                            <span class="input-group-addon" id="basic-addon2">S</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Минимальная комиссия (Для отдаю)') }}</div>
                                        <input type="text" name="oth_min_comm" class="form-control" value="{{ $data->oth_min_comm }}">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Минимальная комиссия (Для получаю)') }}</div>
                                        <input type="text" name="oth_min2_comm" class="form-control" value="{{ $data->oth_min2_comm }}">
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="commission1" class="col-sm-2 control-label">{{ __('Комиссия платежной системы') }}</label>
                                <div class="col-sm-10">

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Для отдаю') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="pay_comm_percent" class="form-control" value="{{ $data->pay_comm_percent }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                        <br />
                                        <div class="input-group">
                                            <input type="text" name="pay_comm_currency" class="form-control" value="{{ $data->pay_comm_currency }}">
                                            <span class="input-group-addon" id="basic-addon2">S</span>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Для получаю') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="pay_comm2_percent" class="form-control" value="{{ $data->pay_comm2_percent }}">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                        <br />
                                        <div class="input-group">
                                            <input type="text" name="pay_comm2_currency" class="form-control" value="{{ $data->pay_comm2_currency }}">
                                            <span class="input-group-addon" id="basic-addon2">S</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Минимальная комиссия (Для отдаю)') }}</div>
                                        <input type="text" name="pay_min_comm" class="form-control" value="{{ $data->pay_min_comm }}">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Минимальная комиссия (Для получаю)') }}</div>
                                        <input type="text" name="pay_min2_comm" class="form-control" value="{{ $data->pay_min2_comm }}">
                                    </div>

                                </div>
                            </div>



                            {{--                                <div class="clearfix"></div>--}}
                            {{--                                <hr />--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.direction_exchange.main.sign_commission') }}</label>--}}
                            {{--                                    <div class="col-sm-6">--}}
                            {{--                                        <select name="sign" class="selectpicker">--}}
                            {{--                                            <option value="0" @if($data->sign == 0) selected @endif>{{ __('admin-basic.no') }}</option>--}}
                            {{--                                            <option value="1" @if($data->sign == 1) selected @endif>{{ __('admin-basic.yes') }}</option>--}}
                            {{--                                        </select>--}}

                            {{--                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">{{ __('admin-basic.direction_exchange.main.sign_commission_hint') }}</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                        </div>
                        <div class="tab-pane" id="sum_of_exchanges">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">{{ __('Минимальная сумма') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Отдаете') }}</div>
                                        <input type="text" name="min_price1" class="form-control" value="{{ $data->min_price1 }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_manual_min_price1" value="1" type="checkbox" name="is_manual_min_price1" @if($data->is_manual_min_price1 == 1) checked @endif>
                                            <label class="form-check-label" for="is_manual_min_price1">{{ __('Корректировать только вручную') }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Получаете') }}</div>
                                        <input type="text" name="min_price2" class="form-control" value="{{ $data->min_price2 }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_manual_min_price2" value="1" type="checkbox" name="is_manual_min_price2" @if($data->is_manual_min_price2 == 1) checked @endif>
                                            <label class="form-check-label" for="is_manual_min_price2">{{ __('Корректировать только вручную') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Максимальная сумма') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <input type="text" name="max_price1" class="form-control" value="{{ $data->max_price1 }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_manual_max_price1" value="1" type="checkbox" name="is_manual_max_price1" @if($data->is_manual_max_price1 == 1) checked @endif>
                                            <label class="form-check-label" for="is_manual_max_price1">{{ __('Корректировать только вручную') }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <input type="text" name="max_price2" class="form-control" value="{{ $data->max_price2 }}">
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_manual_max_price2" value="1" type="checkbox" name="is_manual_max_price2" @if($data->is_manual_max_price2 == 1) checked @endif>
                                            <label class="form-check-label" for="is_manual_max_price2">{{ __('Корректировать только вручную') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                        </div>
                        <div class="tab-pane" id="information_for_user">
                            <div class="form-group">
                                <label for="deadline" class="col-sm-2 control-label">{{ __('Информация для пользователя') }}</label>
                                <div class="col-sm-10">

                                    <div class="form-group col-md-12">
                                        <div class="control-label-br">{{ __('Срок выполнения обмена') }}</div>
                                        <textarea class="form-control" rows="5" name="deadline">{{ $data->deadline }}</textarea>
                                    </div>


                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Инструкция по оплате') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#instructions-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="instructions-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-multieditor-i{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-i{{ $form_lang_key }}" name="instructions{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('instructions', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                        <div class="form-check form-check-ckeditor-input">
                                            <input class="form-check-input" id="is_email_instructions" value="1" type="checkbox" name="is_email_instructions" @if($data->is_email_instructions == 1) checked @endif>
                                            <label class="form-check-label" for="is_email_instructions">{{ __('Разрешить отсылать информацию клиентам на почту') }}</label>
                                        </div>
                                        <div class="input-p-text">
                                            <b>{{ __('Доступные тэги') }}:</b><br />
                                            <ul>
                                                <li>[city] - город</li>
                                                <li>[country] - страна</li>
                                                <li>[direction] - направление</li>
                                                <li>[created_at] - дата создания заявки</li>
                                                <li>[rate] - курс обмена</li>
                                                <li>[in_amount] - сумма отдаю</li>
                                                <li>[out_amount] - сумма получаю</li>
                                                <li>[in_code] - код отдаю</li>
                                                <li>[out_code] - код получаю</li>
                                                <li>[in_currency] - валюта отдаю</li>
                                                <li>[out_currency] - валюта получаю</li>
                                                <li>[public_id] - Public ID</li>
                                                <li>[order_id] - ID Заявки</li>
                                                <li>[profit_percent] - Процент прибыли</li>
                                                <li>[account] - {{ __('номер счета для оплаты') }}</li>
                                                <li>[to_account] - {{ __('номер счета клиента (получаю)') }}</li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Описание обмена') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#desc_exchange-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="desc_exchange-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-multieditor-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-{{ $form_lang_key }}" name="desc_exchange{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('desc_exchange', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Дополнительное описание обмена') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#desc_exchange_dop-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="desc_exchange_dop-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-multieditor-desc_exchange_dop{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-desc_exchange_dop{{ $form_lang_key }}" name="desc_exchange_dop{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('desc_exchange_dop', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach

                                        <div class="form-check form-check-ckeditor-input">
                                            <input class="form-check-input" id="is_email_desc_exchange_dop" value="1" type="checkbox" name="is_email_desc_exchange_dop" @if($data->is_email_desc_exchange_dop == 1) checked @endif>
                                            <label class="form-check-label" for="is_email_desc_exchange_dop">{{ __('Разрешить отсылать информацию клиентам на почту') }}</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Формальное описание перед вводом данных') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#formalization_text-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="formalization_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-formalization_text-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-formalization_text-{{ $form_lang_key }}" name="formalization_text{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('formalization_text', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Дополнительный текст в процессе оплаты (внизу)') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#other_docs-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="other_docs-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-other_docs-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-other_docs-{{ $form_lang_key }}" name="other_docs{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('other_docs', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach

                                        <div class="form-check form-check-ckeditor-input">
                                            <input class="form-check-input" id="is_email_other_docs" value="1" type="checkbox" name="is_email_other_docs" @if($data->is_email_other_docs == 1) checked @endif>
                                            <label class="form-check-label" for="is_email_other_docs">{{ __('Разрешить отсылать информацию клиентам на почту') }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Предупреждение при обмене (внизу)') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#notice_process_desc-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="notice_process_desc-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-notice_process_desc-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-notice_process_desc-{{ $form_lang_key }}" name="notice_process_desc{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('notice_process_desc', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>


                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Текст для статуса Заявка выполнена') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#text_order_success-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="text_order_success-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-text_order_success-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-text_order_success-{{ $form_lang_key }}" name="text_order_success{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('text_order_success', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>


                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Текст для статуса Заявка отклонена') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#text_order_failed-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="text_order_failed-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-text_order_failed-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-text_order_failed-{{ $form_lang_key }}" name="text_order_failed{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('text_order_failed', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group col-md-12 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Текст для окна "Подтверждения обмена"') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#text_order_confirm-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="text_order_confirm-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea id="ckedtor-text_order_confirm-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-text_order_confirm-{{ $form_lang_key }}" name="text_order_confirm{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('text_order_confirm', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="deadline" class="col-sm-2 control-label">{{ __('Дополнительно') }}</label>
                                <div class="col-sm-10">

                                    <div class="form-group col-md-6 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Кастомное название кнопки Я оплатил') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#order_button_i_pay-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="order_button_i_pay-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <input type="text" name="order_button_i_pay{{$form_lang['field']}}" class="form-control" value="{{ $data->getTranslation('order_button_i_pay', $form_lang_key) }}" id="order_button_i_pay{{  $form_lang['field'] }}">
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group col-md-6 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Описание над кнопкой Я оплатил') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#order_button_i_pay_text-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="order_button_i_pay_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <textarea class="form-control" name="order_button_i_pay_text{{$form_lang['field']}}" rows="5">{!! $data->getTranslation('order_button_i_pay_text', $form_lang_key) !!}</textarea>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <div class="form-group col-md-6 multi-language-form-group">
                                        <div class="control-label-br">
                                            <div layout="row">
                                                <div>{{ __('Название кнопки "Подтвердить и обменять""') }}</div>
                                                <span flex></span>
                                                <ul class="nav nav-pills lang-tabs">
                                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                                            <a class="lang-tabs-link" data-toggle="tab" href="#order_button_i_confirm-{{ $form_lang_key }}">
                                                                {{ $form_lang_key }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                            <div id="order_button_i_confirm-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                <input type="text" name="order_button_i_confirm{{$form_lang['field']}}" class="form-control" value="{{ $data->getTranslation('order_button_i_confirm', $form_lang_key) }}" id="order_button_i_confirm{{  $form_lang['field'] }}">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="seo_title" class="col-sm-2 control-label">{{ __('SEO') }}</label>
                                <div class="col-sm-10">
                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Название обмена') }}</div>
                                        <input type="text" name="seo_title" class="form-control" value="{{ $data->seo_title }}">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Ключевые слова') }}</div>
                                        <input type="text" name="seo_keywords" class="form-control" value="{{ $data->seo_keywords }}">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-md-12">
                                        <div class="control-label-br">{{ __('SEO Ссылка') }}</div>
                                        <div class="input-group">
                                            <div class="input-group-addon">{{ config('app.url') }}/obmen/</div>
                                            <input type="text" class="form-control" id="parent_url" value="{{ $data->parent_url }}" name="parent_url">
                                        </div>
                                        <div class="input-p-text">{{ __('Пример') }} (bitcoin-to-advcash-usd)
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <div class="form-group col-md-12">
                                        <div class="control-label-br">{{ __('Описание') }}</div>
                                        <textarea id="ckeditor" class="form-control" name="seo_description">{{ $data->seo_description }}</textarea>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="tab-pane" id="direction_requisites">
                            <div class="form-group">
                                <label for="profit" class="col-sm-2 control-label">{{ __('Настройка реквизитов') }}</label>
                                <div class="col-sm-10">

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Тип вывода реквизитов') }}</div>

                                        <select class="form-control selectpicker" name="type_output_requisites">
                                            <option value="0" @if($data->type_output_requisites == 0) selected @endif>{{ __('По умолчанию') }}</option>
                                            <option value="1" @if($data->type_output_requisites == 1) selected @endif>{{ __('Рандомно') }}</option>
                                        </select>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Реквизиты') }}</div>

                                        {{ Form::select('ids_requisites[]', $requisites, $data->direction_requisites, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="tab-pane" id="displaying_fields">
                            <div class="form-group">
                                <label for="profit" class="col-sm-2 control-label">{{ __('Дополнительные поля') }}</label>
                                <div class="col-sm-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Доп. поля') }}</div>
                                        {{ Form::select('ids_custom_fields[]', $custom_fields, $data->direction_field, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="tab-pane" id="merchant_api">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Мерчант') }}</label>
                                <div class="col-sm-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Список мерчантов') }}</div>
                                        {{ Form::select('ids_merchant[]', $merchants, $data->merchants, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '8']) }}
                                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"> {{ __('Этот параметр необходим для приема средств от клиентов') }}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="tab-pane" id="restrictions_and_checks">
                            <div class="form-group">
                                <label for="is_not_bonus" class="col-sm-2 control-label">{{ __('Выплачивать Cashback') }}</label>
                                <div class="col-sm-3">
                                    <div class="material-switch switch-input-1">
                                        <input id="is_not_bonus" name="is_not_bonus" type="checkbox" @if($data->is_not_bonus == 0) checked @endif value="1" />
                                        <label for="is_not_bonus" class="label-primary"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="reserve_ogr" class="col-sm-2 control-label">{{ __('Лимит резерва') }}</label>
                                <div class="col-md-10">

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Лимит резерва для направления') }}</div>
                                        <input type="text" name="reserve_max_limit" class="form-control" value="{{ $data->reserve_max_limit }}">
                                    </div>

                                    <div class="clearfix"></div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Лимит резерва в сутки') }}</div>
                                        <input type="text" name="reserve_limit_day" class="form-control" value="{{ $data->reserve_limit_day }}">
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Лимит резерва в месяц') }}</div>
                                        <input type="text" name="reserve_limit_month" class="form-control" value="{{ $data->reserve_limit_month }}">
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="reserve_ogr" class="col-sm-2 control-label">{{ __('Версия сайта') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-sm-6">
                                        {!! Form::select('device[]', $exchangeDevice, explode(',', $data->device), ['multiple' => true, 'class' => 'form-control selectpicker', 'data-width' => '300px'])  !!}
                                        <div style="font-size: 12px; margin-top: 5px;" class="text-muted">{{ __('Если не выбрано, то снимается ограничение') }}</div>
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_hidden_not_device" value="1" type="checkbox" name="is_hidden_not_device" @if($data->is_hidden_not_device == 1) checked @endif>
                                            <label class="form-check-label" for="is_hidden_not_device">{{ __('Скрыть направление если тип устройства недоступно') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="reserve_ogr" class="col-sm-2 control-label">{{ __('Язык') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-sm-6">
                                        {!! Form::select('languages[]', $exchangeLanguage, explode(',', $data->languages), ['multiple' => true, 'class' => 'form-control selectpicker', 'data-width' => '300px'])  !!}
                                        <div style="font-size: 12px; margin-top: 5px;" class="text-muted">{{ __('Если не выбрано, то снимается ограничение') }}</div>
                                        <div class="form-check">
                                            <input class="form-check-input" id="is_hidden_not_locale" value="1" type="checkbox" name="is_hidden_not_locale" @if($data->is_hidden_not_locale == 1) checked @endif>
                                            <label class="form-check-label" for="is_hidden_not_locale">{{ __('Скрыть направление если страна недоступно') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="reserve_ogr" class="col-sm-2 control-label">{{ __('Отключить авто-регистрацию?') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-sm-6">
                                        <select class="form-control selectpicker" name="is_disable_auto_reg" data-width="300px">
                                            <option value="0" @if($data->is_disable_auto_reg == 0) selected @endif>{{ __('Нет') }}</option>
                                            <option value="1" @if($data->is_disable_auto_reg == 1) selected @endif>{{ __('Да') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="reserve_ogr" class="col-sm-2 control-label">{{ __('GEO IP') }}</label>
                                <div class="col-md-10">
                                     <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Запрещенные страны') }}</div>
                                        @if(iEXSetting('is_enabled_geo_direction_country') == 1)
                                            {!! Form::select('ids_geo_forbidden_countries[]', $geo_countries,  $data->direction_forbidden_countries, ['multiple' => true, 'class' => 'form-control selectpicker', 'data-width' => '100%', 'data-size' => '10', 'data-live-search' => 'true'])  !!}
                                        @else
                                            <small class="text-danger">{!! __('НаправлениеСтранаПравила') !!}</small>
                                        @endif
                                    </div>

                                     <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Разрешенные страны') }}</div>
                                        @if(iEXSetting('is_enabled_geo_direction_country') == 1)
                                            {!! Form::select('ids_geo_allowed_countries[]', $geo_countries,  $data->direction_allowed_countries, ['multiple' => true, 'class' => 'form-control selectpicker', 'data-width' => '100%', 'data-size' => '10', 'data-live-search' => 'true'])  !!}
                                        @else
                                             <small class="text-danger">{!! __('НаправлениеСтранаПравила') !!}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="num_transaction" class="col-sm-2 control-label">{{ __('Номер денежного перевода') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Статус поля') }}</div>
                                        <select class="form-control selectpicker" name="is_num_transaction">
                                            <option value="0" @if($data->is_num_transaction == 0) selected @endif>{{ __('Отключен') }}</option>
                                            <option value="1" @if($data->is_num_transaction == 1) selected @endif>{{ __('Включен') }}</option>
                                        </select>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Название поля') }}</div>
                                        <input type="text" name="num_transaction_label" class="form-control" value="{{ $data->num_transaction_label }}">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="num_transaction" class="col-sm-2 control-label">{{ __('Примечание к транзакции') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Примечание к транзакции') }}</div>
                                        <select class="form-control selectpicker" name="is_note_tx">
                                            <option value="0" @if($data->is_note_tx == 0) selected @endif>{{ __('Отключен') }}</option>
                                            <option value="1" @if($data->is_note_tx == 1) selected @endif>{{ __('Включен') }}</option>
                                        </select>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Название поля') }}</div>
                                        <input type="text" name="note_tx_label" class="form-control" value="{{ $data->note_tx_label }}">
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="num_transaction" class="col-sm-2 control-label">{{ __('X19') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <select class="form-control selectpicker" name="x19_mode" data-size="10" data-live-search="true">
                                            <option value="0" @if($data->x19_mode == 0) selected @endif>{{ __('Не выбрано') }}</option>
                                            @foreach(config('payment.webmoney.x19_list') as $key => $value)
                                                <option value="{{ $key }}" @if($data->x19_mode == $key) selected @endif>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="is_holding_direction" class="col-sm-2 control-label">{{ __('Заморозить направление') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <div class="material-switch switch-input-1">
                                            <input id="is_holding_direction" name="is_holding_direction" type="checkbox" @if($data->is_holding_direction == 1) checked @endif value="1" />
                                            <label for="is_holding_direction" class="label-primary"></label>
                                        </div>

                                        <div class="input-p-text">{{ __('После включения данной функции, клиент не могут создавать заявки по данному направлению но в списке она останется') }}</div>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Отображать по расписанию') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Включить по расписанию') }}</div>
                                        <select class="form-control selectpicker" name="is_enabled_exchange" data-width="200px">
                                            <option value="0" @if($data->is_enabled_exchange == 0) selected @endif>{{ __('Нет') }}</option>
                                            <option value="1" @if($data->is_enabled_exchange == 1) selected @endif>{{ __('Да') }}</option>
                                        </select>
                                    </div>


                                    <div class="clearfix"></div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Период отображения направлений (От)') }}</div>
                                        <input type="text" name="from_on_time" class="form-control timepicker" value="{{ $data->from_on_time  }}">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Период отображения направлений (До)') }}</div>
                                        <input type="text" name="to_on_time" class="form-control timepicker" value="{{ $data->to_on_time }}">
                                    </div>

                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Верификация') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <div class="control-label-br">{{ __('Только для верифицированных пользователей') }}</div>
                                        <select class="form-control selectpicker" name="is_verified_account" data-width="200px">
                                            <option value="0" @if($data->is_verified_account == 0) selected @endif>{{ __('Нет') }}</option>
                                            <option value="1" @if($data->is_verified_account == 1) selected @endif>{{ __('Да') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label for="is_holding_direction" class="col-sm-2 control-label">{{ __('Запрещенные IP адреса (с новой строки)') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-6">
                                        <textarea class="form-control" name="not_ip" rows="5">{!! $data->not_ip !!}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Лимиты на обмены') }}</label>
                                <div class="col-md-10">
                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного IP') }}</div>
                                        <input type="text" name="max_order_one_ip" class="form-control" value="{{ $data->max_order_one_ip }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного IP в день') }}</div>
                                        <input type="text" name="max_order_one_ip_day" class="form-control" value="{{ $data->max_order_one_ip_day }}">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок от одного пользователя') }}</div>
                                        <input type="text" name="max_order_one_user" class="form-control" value="{{ $data->max_order_one_user }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок от одного пользователя в день') }}</div>
                                        <input type="text" name="max_order_one_user_day" class="form-control" value="{{ $data->max_order_one_user_day }}">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного e-mail') }}</div>
                                        <input type="text" name="max_order_one_email" class="form-control" value="{{ $data->max_order_one_email }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного e-mail в день') }}</div>
                                        <input type="text" name="max_order_one_email_day" class="form-control" value="{{ $data->max_order_one_email_day }}">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного счета Отдаю') }}</div>
                                        <input type="text" name="max_order_one_account1" class="form-control" value="{{ $data->max_order_one_account1 }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного счета Отдаю в день') }}</div>
                                        <input type="text" name="max_order_one_account1_day" class="form-control" value="{{ $data->max_order_one_account1_day }}">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного счета Получаю') }}</div>
                                        <input type="text" name="max_order_one_account2" class="form-control" value="{{ $data->max_order_one_account2 }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="control-label-br">{{ __('Макс. кол-во заявок с одного счета Получаю в день') }}</div>
                                        <input type="text" name="max_order_one_account2_day" class="form-control" value="{{ $data->max_order_one_account2_day }}">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <!-- Ограничить редактирование для некоторых группы пользователей -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Кол-во успешных обменов, который необходимо иметь клиенту') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <input type="text" name="min_count_exchanges_client" class="form-control" value="{{ $data->min_count_exchanges_client }}">
                                        <div class="input-p-text "><b>0</b> - {{ __('снимает ограничение') }}</div>
                                    </div>
                                </div>
                            </div>

                            <!-- Ограничить редактирование для некоторых группы пользователей -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('(Отдаю) Макс. сумма обмена для новичка') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <input type="text" name="max_amount_newbie" class="form-control" value="{{ $data->max_amount_newbie }}">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <!-- Ограничить редактирование для некоторых группы пользователей -->
                            <div class="form-group">
                                <label for="is_restrict_editing" class="col-sm-2 control-label">{{ __('Разрешить редактировать направление для всех групп пользователей') }}</label>
                                <div class="col-sm-3">
                                    <div class="material-switch switch-input-1">
                                        <input id="is_restrict_editing" name="is_restrict_editing" type="checkbox" @if($data->is_restrict_editing == 0) checked @endif value="1" />
                                        <label for="is_restrict_editing" class="label-primary"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />


                            <div class="form-group">
                                <label for="is_hidden_tariffs" class="col-sm-2 control-label">{{ __('Не показывать это направление в тарифах') }}</label>
                                <div class="col-sm-3">
                                    <div class="material-switch switch-input-1">
                                        <input id="is_hidden_tariffs" name="is_hidden_tariffs" type="checkbox" @if($data->is_hidden_tariffs == 1) checked @endif value="1" />
                                        <label for="is_hidden_tariffs" class="label-primary"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />



                            <div class="form-group">
                                <label for="is_enable_user_discount" class="col-sm-2 control-label">{{ __('Разрешить применение пользовательской скидки?') }}</label>
                                <div class="col-sm-3">
                                    <select class="form-control selectpicker" name="is_enable_user_discount" data-width="200px">
                                        <option value="0" @if($data->is_enable_user_discount == 0) selected @endif>{{ __('Да') }}</option>
                                        <option value="1" @if($data->is_enable_user_discount == 1) selected @endif>{{ __('Нет') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="removing_unpaid_applications">

                            <label class="col-sm-2 control-label">{{ __('Удаление неоплаченных заявок') }}</label>
                            <div class="col-md-10">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">{{ __('Удалять заявки со статусом') }}</div>
                                    {!! Form::select('auto_del_order_status[]', $auto_del_order_status, explode(',', $data->auto_del_order_status),
                                    ['class' => 'form-control selectpicker', 'multiple', 'data-width' => '50%', 'data-live-search' => 'true']) !!}
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group col-md-2">
                                    <div class="control-label-br">{{ __('Через сколько дней') }}</div>
                                    <input type="text" name="auto_del_order_day" class="form-control" value="{{ $data->auto_del_order_day ?? 0}}">
                                </div>

                                <div class="form-group col-md-2">
                                    <div class="control-label-br">{{ __('Через сколько часов') }}</div>
                                    <input type="text" name="auto_del_order_hour" class="form-control" value="{{ $data->auto_del_order_hour ?? 0}}">
                                </div>

                                <div class="form-group col-md-2">
                                    <div class="control-label-br">{{ __('Через сколько минут') }}</div>
                                    <input type="text" name="auto_del_order_minute" class="form-control" value="{{ $data->auto_del_order_minute ?? 0}}">
                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                        <div class="tab-pane" id="config_export_file">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Настройки экспорта') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Парсить направление') }}</div>
                                        <select class="form-control selectpicker" name="allow_export">
                                            <option value="0" @if($data->allow_export == 0) selected @endif>{{ __('Да') }}</option>
                                            <option value="1" @if($data->allow_export == 1) selected @endif>{{ __('По расписанию') }}</option>
                                            <option value="2" @if($data->allow_export == 2) selected @endif>{{ __('Отключить') }}</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Отображать направление по расписанию (От)') }}</div>
                                        <input type="text" name="allow_export_from" class="form-control timepicker" value="{{ $data->allow_export_from  }}">
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Отображать направление по расписанию (До)') }}</div>
                                        <input type="text" name="allow_export_to" class="form-control timepicker" value="{{ $data->allow_export_to }}">
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Метка (floating)') }}</div>
                                        <input type="text" class="form-control" name="label_floating" value="{{ $data->label_floating  }}">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Метка (delay)') }}</div>
                                        <input type="text" class="form-control" name="label_delay" value="{{ $data->label_delay  }}">
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Метка для параметра param') }}</div>
                                        {!! Form::select('export_label_param[]', $exchangeExportLabels, explode(',', $data->export_label_param), ['multiple' => true, 'class' => 'form-control selectpicker', 'data-live-search' => 'true'])  !!}
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Отображать мин. цену') }}</div>
                                        <select class="form-control selectpicker" name="export_label_minamount">
                                            <option value="0" @if($data->export_label_minamount == 0) selected @endif>-- {{ __('По умолчанию') }} --</option>
                                            <option value="1" @if($data->export_label_minamount == 1) selected @endif>{{ __('Да') }}</option>
                                            <option value="2" @if($data->export_label_minamount == 2) selected @endif>{{ __('Нет') }}</option>
                                        </select>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Отображать макс. цену') }}</div>
                                        <select class="form-control selectpicker" name="export_label_maxamount">
                                            <option value="0" @if($data->export_label_maxamount == 0) selected @endif>-- {{ __('По умолчанию') }} --</option>
                                            <option value="1" @if($data->export_label_maxamount == 1) selected @endif>{{ __('Да') }}</option>
                                            <option value="2" @if($data->export_label_maxamount == 2) selected @endif>{{ __('Нет') }}</option>
                                        </select>
                                    </div>


                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />

                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Прочие опция') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Режим обмена') }}</div>
                                        <select class="form-control selectpicker" name="hidden_export_label_param">
                                            <option value="0" @if($data->hidden_export_label_param == 0) selected @endif>{{ __('По умолчанию') }}</option>
                                            <option value="1" @if($data->hidden_export_label_param == 1) selected @endif>{{ __('Автоматический (принудительно)') }}</option>
                                            <option value="2" @if($data->hidden_export_label_param == 2) selected @endif>{{ __('Ручной (принудительно)') }}</option>
                                        </select>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Перевод от') }}</div>
                                        <select class="form-control selectpicker" name="xml_juridical">
                                            <option value="0" @if($data->xml_juridical == 0) selected @endif>{{ __('Физического лица') }}</option>
                                            <option value="1" @if($data->xml_juridical == 1) selected @endif>{{ __('Юридического лица') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="partner_program">

                            <div class="form-group">
                                <label class="col-sm-2 control-label">@lang('Прибыль для партнеров')</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">@lang('Прибыль для партнеров в %')</div>
                                        <div class="input-group">
                                            <input type="text" name="profit_partner" class="form-control" value="{{ $data->profit_partner }}">
                                            <span class="input-group-addon" id="basic-addon3">%</span>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">@lang('Отчисления от прибыли обменника, это поле используется для партнерской программы.')</div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <hr />


                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{ __('Настройки партнерских выплат') }}</label>
                                <div class="col-md-10">
                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Партнерские выплаты') }}</div>
                                        <select class="form-control selectpicker" name="is_not_partner" data-width="100px">
                                            <option value="0" @if($data->is_not_partner == 0) selected @endif>{{ __('Да') }}</option>
                                            <option value="1" @if($data->is_not_partner == 1) selected @endif>{{ __('Нет') }}</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>


                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Фиксированная сумма выплаты') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="fixed_payout" class="form-control" value="{{ $data->fixed_payout }}">
                                            <span class="input-group-addon" id="basic-addon2">{{ config('crypto.currency_payout') }}</span>
                                        </div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                            <b>0</b> - {{ __('отключает действие') }}
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Мин. сумма выплаты') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="minimum_payout" class="form-control" value="{{ $data->minimum_payout }}">
                                            <span class="input-group-addon" id="basic-addon2">{{ config('crypto.currency_payout') }}</span>
                                        </div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                            <b>0</b> - {{ __('отключает действие') }}
                                        </div>
                                    </div>

                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Макс. сумма выплаты') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="maximum_payout" class="form-control" value="{{ $data->maximum_payout }}">
                                            <span class="input-group-addon" id="basic-addon2">{{ config('crypto.currency_payout') }}</span>
                                        </div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                            <b>0</b> - {{ __('отключает действие') }}
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />


                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Индивидуальный процент партнерской программы') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="individual_percentage" class="form-control" value="{{ $data->individual_percentage }}">
                                            <span class="input-group-addon" id="basic-addon3">%</span>
                                        </div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                            <b>0</b> -  {{ __('берется из партнерской программы') }}
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <hr />


                                       <div class="form-group col-lg-6">
                                        <div class="control-label-br">{{ __('Максимальный партнерский процент') }}</div>
                                        <div class="input-group">
                                            <input type="text" name="max_percent_partner" class="form-control" value="{{ $data->max_percent_partner }}">
                                            <span class="input-group-addon" id="basic-addon3">%</span>
                                        </div>
                                        <div class="text-muted" style="font-size: 12px; margin-top: 8px">
                                            <b>0</b> - {{ __('отключает ограничения') }}
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>


                        <div class="tab-pane" id="telegram_bot">

                            <label class="col-sm-2 control-label">{{ __('Настройка') }}</label>
                            <div class="col-md-10">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">{{ __('Показывать направление в Telegram Bot') }}</div>
                                    <select class="form-control selectpicker" name="is_allow_telegram_bot" data-width="200px">
                                        <option value="0" @if($data->is_allow_telegram_bot == 0) selected @endif>{{ __('Да') }}</option>
                                        <option value="1" @if($data->is_allow_telegram_bot == 1) selected @endif>{{ __('Нет') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    </div>


                    <div class="modal-footer">
                        <md-button class="md-raised md-primary" @can('admin_limit_editing_directions') disabled @endif type="submit">{{ __('Сохранить') }}</md-button>
                        <md-button ng-href="{{ admin_base_path('/basic/direction_exchange') }}">{{ __('Назад') }}</md-button>
                    </div>
                </form>
            </div>
        </md-content>
    </div>
@endsection

@section('js_bottom')
    <script type="text/javascript">
        $(function()
        {
            $('.show_detail_group_commission').hide();
            $('#group_commission').on('change', function () {
                var id = $(this).val();
                if (id == "add") {
                    $('.show_detail_group_commission').show();
                } else {
                    $('.show_detail_group_commission').hide();
                }
            });


            $('.show_detail_id_bestchange_rates').hide();
            $('#id_bestchange_rates').on('change', function () {
                var id = $(this).val();
                if (id == "add") {
                    $('.show_detail_id_bestchange_rates').show();
                } else {
                    $('.show_detail_id_bestchange_rates').hide();
                }
            });


            $(document).on('click', '.event_cities_click', function()
            {
                let cities_block = $(this).parents('.cities__block');
                let direction_id = cities_block.attr('data-direction-id');
                let city_item = $(this).parents('.cities__block-item');

                cities_block.find('input, select').attr('disabled',true);
                cities_block.find('.event_cities_click, .event_cities_click_trash').attr('disabled',true);

                let input_fields = '';
                city_item.find('.cities_item-field').each(function() {
                    input_fields += '&city_id='+$(this).attr('data-city-id')+'&'+$(this).attr('data-field-name') + '=' + $(this).val();
                });

                let param = 'direction_id='+direction_id + input_fields;

                $.ajax({
                    type: "POST",
                    url: "/admin/private/direction_exchange_cities?actions=update",
                    dataType: 'json',
                    data: param,
                    success: function(r) {

                        if(r['status'] == 0) {
                            cities_block.find('input, select').attr('disabled', false);
                            cities_block.find('.event_cities_click, .event_cities_click_trash').attr('disabled',false);
                        }

                    }
                });

                return false;
            });

            $(document).on('click', '.event_cities_click_trash', function() {
                let cities_block = $(this).parents('.cities__block');
                let direction_id = cities_block.attr('data-direction-id');
                let city_item = $(this).parents('.cities__block-item');

                cities_block.find('input, select').attr('disabled',true);
                cities_block.find('.event_cities_click, .event_cities_click_trash').attr('disabled',true);

                let input_fields = '';

                let city_id = 0;
                city_item.find('.cities_item-field').each(function() {
                    city_id = $(this).attr('data-city-id');
                    input_fields += '&city_id='+$(this).attr('data-city-id')+'&'+$(this).attr('data-field-name') + '=' + $(this).val();
                });

                let param = 'direction_id='+direction_id + input_fields;

                $.ajax({
                    type: "POST",
                    url: "/admin/private/direction_exchange_cities?actions=trash",
                    dataType: 'json',
                    data: param,
                    success: function(r) {

                        if(r['status'] == 0) {
                            $('.cities__block-item-'+city_id).remove();
                            cities_block.find('input, select').attr('disabled', false);
                            cities_block.find('.event_cities_click, .event_cities_click_trash').attr('disabled',false);
                        }

                    }
                });

                return false;
            });

            $(document).on('click', '.event_exchange_amount_click', function()
            {
                let exchange_amount_block = $(this).parents('.exchange_amount__block');
                let direction_id = exchange_amount_block.attr('data-direction-id');
                let city_item = $(this).parents('.exchange_amount__block-item');

                exchange_amount_block.find('input, select').attr('disabled',true);
                exchange_amount_block.find('.event_exchange_amount_click, .event_exchange_amount_click_trash').attr('disabled',true);

                let input_fields = '';
                city_item.find('.exchange_amount_item-field').each(function() {
                    input_fields += '&'+$(this).attr('data-field-name') + '=' + $(this).val();
                    input_fields += '&percentage_amount_id='+$(this).attr('data-percentage-amount-id')+'&'+$(this).attr('data-field-name') + '=' + $(this).val();
                });

                let param = 'direction_id='+direction_id + input_fields;

                $.ajax({
                    type: "POST",
                    url: "/admin/private/direction_exchange_percent_amount?actions=update",
                    dataType: 'json',
                    data: param,
                    success: function(r) {

                        if(r['status'] == 0) {
                            exchange_amount_block.find('input, select').attr('disabled', false);
                            exchange_amount_block.find('.event_exchange_amount_click, .event_exchange_amount_click_trash').attr('disabled',false);
                        }

                    }
                });

                return false;
            });

            $(document).on('click', '.event_exchange_amount_click_trash', function() {
                let exchange_amount_block = $(this).parents('.exchange_amount__block');
                let direction_id = exchange_amount_block.attr('data-direction-id');
                let city_item = $(this).parents('.exchange_amount__block-item');

                exchange_amount_block.find('input, select').attr('disabled',true);
                exchange_amount_block.find('.event_exchange_amount_click, .event_exchange_amount_click_trash').attr('disabled',true);

                let input_fields = '';
                let percentage_amount_id = 0;
                city_item.find('.exchange_amount_item-field').each(function() {
                    percentage_amount_id = $(this).attr('data-percentage-amount-id');
                    input_fields += '&'+$(this).attr('data-field-name') + '=' + $(this).val();
                    input_fields += '&percentage_amount_id='+$(this).attr('data-percentage-amount-id')+'&'+$(this).attr('data-field-name') + '=' + $(this).val();
                });

                let param = 'direction_id='+direction_id + input_fields;

                $.ajax({
                    type: "POST",
                    url: "/admin/private/direction_exchange_percent_amount?actions=trash",
                    dataType: 'json',
                    data: param,
                    success: function(r) {

                        if(r['status'] == 0) {
                            $('.exchange_amount__block-item-'+percentage_amount_id).remove();
                            exchange_amount_block.find('input, select').attr('disabled', false);
                            exchange_amount_block.find('.event_exchange_amount_click, .event_exchange_amount_click_trash').attr('disabled',false);
                        }

                    }
                });

                return false;
            });

            $('#directionTab a:first').tab('show');
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var id = $(e.target).attr("href").substr(1);
                localStorage.setItem('selectedTab_edit', id);
            });

            var selectedTab = localStorage.getItem('selectedTab_edit');
            if (selectedTab) {
                $('#directionTab a[href="#' + selectedTab + '"]').tab('show');
            }


            /* visible title */
            function set_visible_title() {

                var title1 = $('#id_currency1 option:selected').html().replace(new RegExp("-",'g'),'');
                var title2 = $('#id_currency2 option:selected').html().replace(new RegExp("-",'g'),'');
                $('.ex_title1').html(title1);
                $('.ex_title2').html(title2);
            }
            $('#id_currency1, #id_currency2').change(function(){
                set_visible_title();
            });
            set_visible_title();
            /* end visible title */
        });
    </script>
@endsection
