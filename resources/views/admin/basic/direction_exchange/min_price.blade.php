@extends('admin.layouts.app')

@section('title', __('Корректировка минимальной цены'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.min_price'))

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title" layout="row">
            <h2>{{ __('Отдаю') }}</h2>
            <div flex=""></div>
            <div class="heading-right-button">
                <md-button href="?generate_min=true">{{ __('Генерация мин. цены') }}</md-button>
                <md-button href="?generate_max=true">{{ __('Генерация макс. цены') }}</md-button>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" action="?form=min_price1">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('Валюта') }}</th>
                        <th>{{ __('Дата обновления') }}</th>
                        <th>{{ __('Мин. цена') }}</th>
                        <th>{{ __('Макс. цена') }}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($exchange1 as $value)
                        <tr>
                            <td>{{$value->currency1->payment->name}} {{$value->currency1->code_currency->name}}</td>
                            <td>{{ \Illuminate\Support\Carbon::parse($value->updated_at)->diffForHumans() }}</td>
                            <td style="width: 200px">
                                <input type="text" name="min_price[{{$value->currency1->id}}]" class="form-control" value="{{ $value->min_price1 }}">
                            </td>
                            <td style="width: 200px">
                                <input type="text" name="max_price[{{$value->currency1->id}}]" class="form-control" value="{{ $value->max_price1 }}">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Получаю') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <form method="post" action="?form=min_price2">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('Валюта') }}</th>
                        <th>{{ __('Дата обновления') }}</th>
                        <th>{{ __('Мин. цена') }}</th>
                        <th>{{ __('Макс. цена') }}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($exchange2 as $value)
                        <tr>
                            <td>{{$value->currency2->payment->name}} {{$value->currency2->code_currency->name}}</td>
                            <td>{{ \Illuminate\Support\Carbon::parse($value->updated_at)->diffForHumans() }}</td>
                            <td style="width: 200px">
                                <input type="text" name="min_price[{{$value->currency2->id}}]" class="form-control" value="{{ $value->min_price2 }}">
                            </td>
                            <td style="width: 200px">
                                <input type="text" name="max_price[{{$value->currency2->id}}]" class="form-control" value="{{ $value->max_price2 }}">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>

@endsection
