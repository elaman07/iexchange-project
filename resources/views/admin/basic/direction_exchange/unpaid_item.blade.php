@extends('admin.layouts.app')

@section('title', __('Автоматическое удаление неоплаченных заявок'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.unpaid_item'))

@section('content')

    <form action="?" method="POST" class="form-horizontal">
        @csrf
        <div class="default-panel-body">
            <div class="form-group">
                <label for="sitename" class="col-sm-2 control-label">{{ __('Авто. удаление неоплаченных заявок') }}</label>
                <div class="col-sm-3">
                    <select class="form-control selectpicker" name="auto_delete">
                        <option value="0" @if($item->auto_delete == 0) selected @endif>{{ __('Нет') }}</option>
                        <option value="1" @if($item->auto_delete == 1) selected @endif>{{ __('Да') }}</option>
                    </select>
                </div>
            </div>

            <hr />

            <div class="form-group">
                <label for="sitename" class="col-sm-2 control-label">{{ __('Настройка неоплаченных заявок') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-4">
                        <div class="control-label-br">{{ __('Удалить заявки со статусом') }}</div>
                        {!! Form::select('order_status[]', $statuses, explode(',', $item->order_status),
                        ['class' => 'form-control selectpicker', 'multiple']) !!}
                    </div>

                    <div class="form-group col-md-4">
                        <div class="control-label-br">{{ __('Использовать планировщик задач') }}</div>
                        <select class="form-control selectpicker" name="rules_cron">
                            <option value="0" @if($item->rules_cron == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if($item->rules_cron == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-4">
                        <div class="control-label-br">{{ __('Через сколько часов') }}</div>
                        <input type="text" name="removal_time" style="width: 150px;text-align: center;" class="form-control" id="removal_time" placeholder="{{ __('Через сколько часов будут удалены заявки') }}" value="{{$item->removal_time}}" required>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="control-label-br">{{ __('Через сколько минут') }}</div>
                        <input type="text" name="removal_minute" style="width: 150px;text-align: center;" class="form-control" id="removal_minute" placeholder="{{ __('Через сколько минут будут удалены заявки') }}" value="{{$item->removal_minute}}" required>
                    </div>

                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction_exchange') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
