@extends('admin.layouts.app')

@section('title', __('Изменить шаблон'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_templates.edit', $item))


@section('content')
    <div class="iex-custom-data-fields">
        <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction_exchange/templates/'.$item->id) }}">
            <div class="default-panel-body">
                @csrf
                @method('PUT')

                <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                    <label for="event_name" class="col-sm-2 control-label">{{ __('Название шаблона') }}:</label>
                    <div class="col-sm-5">
                        <b>{{ $template_name}}</b>
                    </div>
                </div>

                <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                    <label for="event_name" class="col-sm-2 control-label">{{ __('Тип отображения информации') }}:</label>
                    <div class="col-sm-5">
                        <select class="form-control selectpicker" name="type_view_info">
                            <option value="0" @if($item->type_view_info == 0) selected @endif>{{ __('Выводить только описание из направлении') }}</option>
                            <option value="1" @if($item->type_view_info == 1) selected @endif>{{ __('Всегда отображать текст ниже') }}</option>
                            <option value="2" @if($item->type_view_info == 2) selected @endif>{{ __('Если не заполнено описание в направлении, выводить текст ниже') }}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                    <label for="event_name" class="col-sm-2 control-label">{{ __('Дата обновления') }}:</label>
                    <div class="col-sm-5">
                        {{ $item->updated_at }}
                    </div>
                </div>


                <div class="clearfix"></div>
                <hr />

                <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                    <label for="description" class="col-sm-2 control-label">{{ __('Информация') }}:</label>
                    <div class="col-sm-8">
                        <div class="control-label-br">
                            <div layout="row">
                                <div></div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#text-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <textarea id="ckedtor-text-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-text-{{ $form_lang_key }}" name="text{{$form_lang['field']}}" rows="5">{!! $item->getTranslation('text', $form_lang_key) !!}</textarea>

                                <div class="clearfix"></div>
                                <hr />
                                <ul>
                                    @foreach($template_bb_code as $key => $value)
                                        <li><a style="text-decoration: underline;" onclick='insertTextareaCkeditorBBCode(this, "{{ $key }}", "ckedtor-text-{{ $form_lang_key }}")' href='#'>{{ $key }}</a> - {{ $value }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


            <div class="default-panel-footer default-panel-footer-right">
                <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
                <md-button ng-href="{{ admin_base_path('/basic/direction_exchange/templates') }}">{{ __('Отменить') }}</md-button>
            </div>
        </form>
    </div>
@endsection
