@extends('admin.layouts.app')

@section('title', __('Шаблоны для направлений'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#create" class="btn-float" layout="column">
        <i class="far fa-plus"></i>
        <span>{{ __('Добавить шаблон') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_templates'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название шаблона') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($templates) > 0)
            @foreach($templates as $template)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('basic/direction_exchange/templates/'.$template->id.'/edit') }}">{{ $types[$template->id_type] ?? ''}}  &nbsp;<i class="fal fa-pencil"></i></a>
                    </th>
                    <td style="width: 100px;">
                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['direction_exchange-templates.destroy', $template->id] ]) !!}
                            {!! Form::submit(__('Удалить'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>



    <!-- Модальное окно для создания валюты -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction_exchange/templates') }}">
                <input type="hidden" name="action" value="create">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Добавить новый шаблон') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Выберите тип шаблона') }}</label>
                            <div class="col-md-6">
                                <select class="form-control selectpicker" name="id_type_template">
                                    @foreach($types as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Создать') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
