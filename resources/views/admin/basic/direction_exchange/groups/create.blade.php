@extends('admin.layouts.app')

@section('title', __('Добавить новую группу'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.groups.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction-exchange-groups') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('Название') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control">
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon> {{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction-exchange-groups') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
