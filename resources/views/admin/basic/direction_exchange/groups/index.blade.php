@extends('admin.layouts.app')

@section('title', __('Группы направлений обменов'))

@section('top-block')
    <md-button ng-href="direction-exchange-groups/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить группу') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.groups'))


@section('content')
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Название') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($groups) > 0)
                @foreach($groups as $group)
                    <tr>
                        <td>{{ $group->name }}</td>
                        <td style="width: 200px;">
                            <div class="col-md-6">
                                <a href="direction-exchange-groups/{{$group->id}}/edit" class="btn btn-info btn-sm">{{ __('Изменить') }}</a>
                            </div>

                            <div class="col-md-6">
                                {!! Form::open(['method' => 'DELETE', 'route' => ['direction-exchange-groups.destroy', $group->id] ]) !!}
                                {!! Form::submit(__('admin-basic.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
@endsection
