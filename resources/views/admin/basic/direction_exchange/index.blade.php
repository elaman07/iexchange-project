@extends('admin.layouts.app')

@section('title', __('Направления обменов'))

@section('video__instruction', 'https://www.youtube.com/embed/DCpnRSXR1rI?autoplay=1&amp;mute=1')


@section('top-block')

    <md-button data-toggle="modal" data-target="#create" class="btn-float" layout="column">
        <i class="far fa-plus text-primary"></i>
        <span>{{ __('Добавить направление') }}</span>
    </md-button>


    <md-button data-toggle="modal" data-target="#settings_columns" class="btn-float  iex-settings-color" layout="column">
        <md-icon md-font-icon="fa fa-table"></md-icon>
        <span>{{ __('Настройка страницы') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <i class="far fa-cog"></i>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange'))

@section('no-block-content')

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title x_title_filter">
                    <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                        <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                        <div flex=""></div>
                        <div class="heading-right-button">
                            <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                            <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                        </div>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
                    <form class="form-horizontal" method="get" action="?">
                        <input type="hidden" name="send_action" value="on">

                        <div class="row task-filter-body">
                            <div class="padding-20">
                                <div class="task-filter-content" layout="row" layout-xs="column" layout-align="space-between stretch">

                                    <div flex-xs="100" flex="50">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Сортировка') }}</label>
                                            <div class="col-md-6">
                                                {!! Form::select('status[]', $exchangeStatusses, is_filter_search($filter, 'status'),
                                                    ['class' => 'form-control selectpicker', 'multiple']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Сортировать по') }}</label>
                                            <div class="col-md-6">
                                                {!! Form::select('sorting_by', $exchangeSorting, is_filter_search($filter, 'sorting_by'),
                                                   ['class' => 'form-control selectpicker']) !!}
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Включен Bestchange парсер') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="SwitchOptionPrimary1-1" name="is_enable_bestchange" type="checkbox" value="1" @if(is_filter_search($filter, 'is_enable_bestchange') == 1) checked @endif />
                                                    <label for="SwitchOptionPrimary1-1" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Включена групповая комиссия') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="SwitchOptionPrimary2" name="is_group_commission" type="checkbox" value="1" @if(is_filter_search($filter, 'is_group_commission') == 1) checked @endif />
                                                    <label for="SwitchOptionPrimary2" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Запрещен экспорт в файл') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="SwitchOptionPrimary10" name="is_allow_export" type="checkbox" value="1" @if(is_filter_search($filter, 'is_allow_export') == 1) checked @endif />
                                                    <label for="SwitchOptionPrimary10" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Замороженные направления') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="is_holding_direction" name="is_holding_direction" type="checkbox" value="1" @if(is_filter_search($filter, 'is_holding_direction') == 1) checked @endif />
                                                    <label for="is_holding_direction" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div flex-xs="100" flex="50">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Название валюты Отдаю') }}</label>
                                            <div class="col-md-6">
                                                {!! Form::select('currencies_in[]', $currencies_filter, is_filter_search($filter, 'currencies_in'),
                                                     ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'multiple', 'data-size' => '10']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Название валюты Получаю') }}</label>
                                            <div class="col-md-6">
                                                {!! Form::select('currencies_out[]', $currencies_filter, is_filter_search($filter, 'currencies_out'),
                                                    ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'multiple', 'data-size' => '10']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Группа') }}</label>
                                            <div class="col-md-6">
                                                {!! Form::select('id_group_direction[]', $groups_direction, is_filter_search($filter, 'id_group_direction'),
                                                    ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'multiple', 'data-size' => '10']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Кэшбэк отключен') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="SwitchOptionPrimary3" name="is_not_bonus" type="checkbox" value="1" @if(is_filter_search($filter, 'is_not_bonus') == 1) checked @endif />
                                                    <label for="SwitchOptionPrimary3" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Парт. выплаты отключены') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="SwitchOptionPrimary4" name="is_not_partner" type="checkbox" value="1" @if(is_filter_search($filter, 'is_not_partner') == 1) checked @endif />
                                                    <label for="SwitchOptionPrimary4" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{ __('Ошибки в курсе') }}</label>
                                            <div class="col-md-6">
                                                <div class="material-switch switch-input-1">
                                                    <input id="is_error_rate" name="is_error_rate" type="checkbox" value="1" @if(is_filter_search($filter, 'is_error_rate') == 1) checked @endif />
                                                    <label for="is_error_rate" class="label-primary"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-primary">{{ __('Применить фильтры') }}</md-button>
                            <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        @if(iEXSetting('is_enabled_uncreated_directions') == 1)
            <div class="col-xs-12">
                <div class="x_panel new-x_panel_wrapper">
                    <div class="x_title x_title_filter">
                        <a class="collapsed md-icon-button" data-toggle="collapse" href="#showUnDirection" layout="row">
                            <h2>Недостающих направлений {{ count($uncreated_directions) }}</h2>
                            <div flex=""></div>
                            <div class="heading-right-button">
                                <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                                <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                            </div>
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content collapse" id="showUnDirection" style="margin-top: 0;">

                        @if(iEXSetting('is_enabled_uncreated_directions') == 1)
                            @if(count($uncreated_directions) > 0)
                                <div slim-scroll="" height="300px">

                                    <table class="table table-border-2">
                                        <tbody>
                                        @foreach($uncreated_directions as $value)
                                            <tr>
                                                <td>
                                                    {{ $value['currency1']['name'] }} → {{ $value['currency2']['name'] }}
                                                </td>

                                                <td>
                                                    <a href="{{ admin_base_path('/basic/direction_exchange/create?id_currency1='.$value['currency1']['id'].'&id_currency2='.$value['currency2']['id']) }}">Создать</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="empty-content">
                                    {{ __('admin.home.nothing_found') }}
                                </div>
                            @endif
                        @else
                            <div class="empty-content text-danger">
                                <small>Чтобы получить список недостающих направлений, включите функцию в окне "Настройки"</small>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif

        <div class="col-lg-12">

            @if(iEXSetting('is_enabled_possible_directions') == 1 or iEXSetting('is_enabled_uncreated_directions') == 1)
                <div class="alert alert-danger">
                    {{ __('У вас включены функции, которые значительно снижают производительность данного раздела.') }}<br />
                    <span class="text-danger">{{ __('Рекомендуем зайти в Настройки и отключить пункты Включить возможные направления и Включить список недостающих направлений') }}</span>
                </div>
            @endif


            <div class="x_panel new-x_panel_wrapper">
                <div class="x_title" layout="row" layout-align="start center">
                    <h2>{{ __('Направления обменов') }}</h2>
                    <div flex></div>
                    <div class="heading-right-button">
                        <md-button class="md-primary" href="?update_rates">{{ __('Обновить курсы') }}</md-button>
                    </div>
                    <div class="clearfix"></div>

                </div>

                <div class="x_content">
                    <form method="post" action="?">
                        @csrf


                        <table class="table table-border-2 iex-adaptive__table">
                            <thead>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input class="form-check-input iex-select-all" id="iex-checkbox-all" type="checkbox" name="iex-select-all">
                                        <label class="form-check-label" for="iex-checkbox-all"></label>
                                    </div>
                                </th>
                                @if(in_array('direction_name', $admin_direction_hidden_columns))
                                    <th>{{ __('Направление') }}</th>
                                @endif
                                @if(in_array('course', $admin_direction_hidden_columns))
                                    <th>{{ __('Курс') }}</th>
                                @endif
                                @if(in_array('auto_currect', $admin_direction_hidden_columns))
                                    <th>{{ __('Автокорректировка курса') }}</th>
                                @endif
                                @if(in_array('curs_bestchange', $admin_direction_hidden_columns))
                                    <th>{{ __('Курс BestChange') }}</th>
                                @endif
                                @if(in_array('profit', $admin_direction_hidden_columns))
                                    <th>{{ __('Прибыль') }}</th>
                                @endif
                                @if(in_array('status', $admin_direction_hidden_columns))
                                    <th>{{ __('Статус') }}</th>
                                @endif
                                <th class="not-sortable"></th>
                            </tr>

                            </thead>
                            <tbody>
                            @if(count($exchanges) > 0)
                                @foreach($exchanges as $item)
                                    <tr @if($item->duplicate() > 1) style="background-color: #ffe0e0" @elseif($item->status == 0) style="background-color: rgb(255 197 197 / 12%);border-left: 3px solid #dc0000;" @endif>
                                        <th>
                                            <input type="hidden" name="item_id[{{$item->id}}]" value="{{ $item->id }}">
                                            <div class="form-check">
                                                <input class="form-check-input iex-select-item" id="checkbox{{$item->id}}" value="{{$item->id}}" type="checkbox" name="check[]">
                                                <label class="form-check-label" for="checkbox{{$item->id}}"></label>
                                            </div>
                                        </th>
                                        @if(in_array('direction_name', $admin_direction_hidden_columns))
                                            <td data-label="{{ __('Направление') }}">
                                                <a style="margin-right: 5px;" target="_blank" href="{{ url(sprintf('/?cur_from=%s&cur_to=%s', $item->currency1->designation_xml, $item->currency2->designation_xml)) }}" class="md-icon-button" >
                                                    <i class="text-success fas fa-globe"></i>
                                                </a>

                                                <a href="{{ admin_base_path('basic/direction_exchange/'.$item->id.'/edit') }}">
                                                    <b>{{  $item->tech_name }}  &nbsp;<i class="fal fa-pencil"></i></b>
                                                </a>

                                                @if((int)iEXSetting('is_enabled_possible_directions') == 1)
                                                    @if(\App\Models\DirectionExchange::where('id_currency1', $item->id_currency2)->where('id_currency2', $item->id_currency1)->count() > 0)
                                                        <br />
                                                        <small class="small-possible">{{ __('admin-basic.direction_exchange.index.possible') }}:
                                                            <a href="direction_exchange/{{\App\Models\DirectionExchange::where('id_currency1', $item->id_currency2)->where('id_currency2', $item->id_currency1)->first()->id}}/edit">
                                                                {{$item->currency2->payment->name.' '.$item->currency2->code_currency->name}} → {{$item->currency1->payment->name.' '.$item->currency1->code_currency->name}}
                                                            </a>
                                                        </small>
                                                    @endif
                                                @endif


                                                <div>
                                                    @if($item->is_not_bonus == 0)
                                                        <small class="text-success">{{ __('Выплата Cashback: Включен') }}</small>
                                                    @else
                                                        <small class="text-danger">{{ __('Выплата Cashback: Выключен') }}</small>
                                                    @endif
                                                </div>

                                                <div>
                                                    @if($item->is_not_partner == 0)
                                                        <small class="text-success">{{ __('Выплата партнерам: Включен') }}</small>
                                                    @else
                                                        <small class="text-danger">{{ __('Выплата партнерам: Выключен') }}</small>
                                                    @endif
                                                </div>

                                                <div>
                                                    @if($item->x19_mode > 0)
                                                        <small class="text-muted">
                                                            {{ __('WM X19') }}: {{ config('payment.webmoney.x19_list')[$item->x19_mode] }}
                                                        </small>
                                                    @endif
                                                </div>


                                                @if($item->first_order_id > 0)
                                                    <div>
                                                        <small class="text-muted">
                                                            {{ __('Первая заявка') }}: <a href="{{ admin_base_path('tx/'.$item->first_order_id.'?actions=arhive') }}">№{{ $item->first_order_id }}</a>
                                                        </small>
                                                    </div>
                                                @endif


                                                @if($item->last_order_id > 0 and $item->last_order_id != $item->first_order_id)
                                                    <div>
                                                        <small class="text-muted">
                                                            {{ __('Посл. заявка') }}:  <a href="{{ admin_base_path('tx/'.$item->last_order_id.'?actions=arhive') }}">№{{ $item->last_order_id }}</a> [{{ \Illuminate\Support\Carbon::parse($item->last_order_at)->diffForHumans() }}]
                                                        </small>
                                                    </div>
                                                @endif

                                            </td>
                                        @endif
                                        @if(in_array('course', $admin_direction_hidden_columns))
                                            <td data-label="{{ __('Курс') }}">
                                                <div>
                                                    @if($item->is_error_rate)
                                                        <span class="text-danger">{{ __('Курс не настроен') }}</span>
                                                    @else
                                                        {{ $item->exchange_rate }}
                                                    @endif
                                                </div>

                                                <div style="font-size: 11px; color:#999;">
                                                    @if(isset($item->id_bestchange_rates) and $item->enable_bestchange == 1 and $item->is_disable_bs_error == 0)
                                                        @if($item->is_enable_alt_bs_parser == 1 and isset($item->bs_alt_parser->group_parse_exchange))
                                                            <s class="text-danger" style="font-size: 11px;">(Bestchange)</s> - {{ $item->bs_alt_parser->group_parse_exchange->name }}
                                                        @else
                                                            {{ __('BestChange') }}
                                                        @endif
                                                    @elseif(isset($item->partner_parser_rates) and $item->id_partner_parser_rate > 0)
                                                        @if(isset($item->partner_parser_rates->partner_parser_group))
                                                            {{ $item->partner_parser_rates->partner_parser_group->name }}
                                                        @else
                                                            <span class="text-danger">{{ __('Undefined') }}</span>
                                                        @endif
                                                    @elseif($item->id_competitor)
                                                        {{ __('Источник') }}: {{ $item->competitor_rates->competitor_link->name  }}
                                                    @elseif(isset($item->id_crypto_parser) and $item->id_crypto_parser > 0 and $item->enable_bestchange == 0)
                                                        @if(isset($item->parser_exchange) and isset($item->parser_exchange->group_parse_exchange))
                                                            {{ $item->parser_exchange->group_parse_exchange->name }}
                                                        @else
                                                            <span class="text-danger">{{ __('Error parser') }}</span>
                                                        @endif
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                        @if(in_array('auto_currect', $admin_direction_hidden_columns))
                                            <td data-label="{{ __('Автокорректировка курса') }}">
                                                @if(iEXSetting('is_direction_exchange_selected_all_courses') == 0)
                                                    <select class="form-control selectpicker" name="id_crypto_parser[{{$item->id}}]" data-live-search="true" data-show-subtext="true" data-size="10">
                                                        <option value="0" @if($item->id_crypto_parser == 0) selected @endif>-- {{ __('Не указано') }} --</option>
                                                        @foreach($group_rates as $group)
                                                            <optgroup label="{{  $group->name }}">
                                                                @foreach($group->parser_exchange_enabled as $parser)
                                                                    <option value="{{$parser->id}}" @if($item->id_crypto_parser == $parser->id) selected @endif data-subtext="{{ $group->name }}">{{  $parser->name }} ({{  $parser->value }} → {{  $parser->summa }})</option>
                                                                @endforeach
                                                            </optgroup>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    @if($item->id_crypto_parser == 0)
                                                        <span class="text-muted">-- {{ __('Нет значения') }} --</span>
                                                    @else
                                                        @if(isset($item->parser_exchange) and isset($item->parser_exchange->group_parse_exchange))
                                                            <div>{{$item->parser_exchange->name}}</div>
                                                            <small class="text-muted" data-toggle="tooltip" title="{{ __('Источник') }} {{ $item->parser_exchange->group_parse_exchange->name }}">{{ $item->parser_exchange->group_parse_exchange->name }}</small>
                                                        @endif
                                                        @if($item->enable_group_commission > 0 and $item->id_group_commission > 0)
                                                            <div style="font-size: 11px;color: #999;">{{ $item->group_commission->name}}</div>
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                        @endif
                                        @if(in_array('curs_bestchange', $admin_direction_hidden_columns))
                                            <td data-label="{{ __('Курс BestChange') }}">
                                                @if($item->enable_bestchange == true and $item->id_bestchange_rates)
                                                    @if(isset($item->bestchange_rates))
                                                        {{ $item->bestchange_rates->name  }}
                                                    @else
                                                        <small class="text-danger">{{ __('Не определен') }}</small>
                                                    @endif
                                                    <br />
                                                    <small class="text-muted">
                                                        @if($item->bestchange_position == 0)
                                                            {{ __('По умолчанию') }}: {{ iEXSetting('bestchange_position', 10) }}
                                                        @else
                                                            {{ __('Позиция') }}: {{$item->bestchange_position}}
                                                        @endif
                                                    </small>


                                                    @if(isset($item->bestchange_rates) and !empty($item->bestchange_rates->source_name))
                                                        <div style="font-size: 11px" class="text-info">{{ __('Источник') }}: <b>{{ $item->bestchange_rates->source_name }}</b></div>
                                                    @endif

                                                @else
                                                    <span class="text-muted">{{ __('Отключен') }}</span>
                                                @endif
                                            </td>
                                        @endif
                                        @if(in_array('profit', $admin_direction_hidden_columns))
                                            <td data-label="{{ __('Прибыль') }}">
                                                <div class="adaptive-table__input">
                                                    <div class="input-group" style="width: 100px;margin-bottom: 5px">
                                                        <input type="text" name="profit[{{$item->id}}]" class="form-control" value="{{ $item->profit }}">
                                                        <span class="input-group-addon" id="basic-addon3">%</span>
                                                    </div>

                                                    <div class="input-group" style="width: 100px">
                                                        <input type="text" name="profit_s[{{$item->id}}]" class="form-control" value="{{ $item->profit_s }}">
                                                        <span class="input-group-addon" id="basic-addon3">S</span>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>
                                            </td>
                                        @endif
                                        @if(in_array('status', $admin_direction_hidden_columns))
                                            <td data-label="{{ __('Статус') }}">
                                                @if($item->status == 1)
                                                    <span class="text-success">{{ __('Направление включено') }}</span>
                                                @else
                                                    <span class="text-danger">{{ __('Направление отключено') }}</span>
                                                @endif
                                            </td>
                                        @endif
                                        <td style="width:100px;">
                                            <md-button href="direction_exchange/{{$item->id}}/delete" class="md-icon-button" >
                                                <i class="fas fa-trash text-danger"></i>
                                            </md-button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        <div class="default-panel-footer">
                            <div class="pull-right">

                                <div style="display: inline-block">
                                    <select name="actions" class="form-control selectpicker" data-width="200px">
                                        <option value="save">{{ __('Сохранить') }}</option>
                                        <option value="activation">{{ __('Активировать') }}</option>
                                        <option value="deactivation">{{ __('Деактивировать') }}</option>
                                        <option value="duplicate">{{ __('Сделать дубликат') }}</option>
                                        <option value="delete">{{ __('Удалить') }}</option>
                                        <option data-divider="true"></option>
                                        <option value="deleteglobal" data-content="<span class='label label-danger'>{{ __('Важное') }}</span>&nbsp;&nbsp;{{ __('Удалить навсегда') }}"></option>
                                    </select>
                                </div>
                                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>


                </div>

            </div>

            <div class="pagination-right">
                {!! $exchanges->appends($filter)->links() !!}
            </div>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction_exchange') }}">
                <input type="hidden" name="action" value="create">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Добавить направление') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Отдаете') }}</label>
                            <div class="col-md-6">
                                <select id="id_currency1" class="form-control selectpicker" name="id_currency1" data-live-search="true" data-size="10">
                                    @foreach($currencies_filter as $key => $value)
                                        <option value="{{ $key }}" >{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Получаете') }}</label>
                            <div class="col-md-6">
                                <select id="id_currency2" class="form-control selectpicker" name="id_currency2" data-live-search="true" data-size="10">
                                    @foreach($currencies_filter as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Создать') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Использовать один тип сортировки') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_only_first_direction" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_only_first_direction') == 0) selected @endif>{{ __('Да') }}</option>
                                    <option value="1"  @if(iEXSetting('is_only_first_direction') == 1) selected @endif>{{ __('Нет') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить возможные направления') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_enabled_possible_directions" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_enabled_possible_directions') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_enabled_possible_directions') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>

                                <div class="input-p-text text-danger">Включение данной функции, очень сильно снижает производительность</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить список недостающих направлений') }}</label>
                            <div class="col-md-6" id="is_enabled_uncreated_directions">
                                <select class="selectpicker" name="is_enabled_uncreated_directions" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_enabled_uncreated_directions') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_enabled_uncreated_directions') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>

                                <div class="input-p-text text-danger">{{ __('Включение данной функции, очень сильно снижает производительность') }}</div>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить возможность выборки стран для установки ограничений в направлениях') }}</label>
                            <div class="col-md-6" id="is_enabled_geo_direction_country">
                                <select class="selectpicker" name="is_enabled_geo_direction_country" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_enabled_geo_direction_country') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_enabled_geo_direction_country') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Обновление мин/макс. суммы обмена') }}</label>
                            <div class="col-md-6">
                                <select class="selectpicker" name="is_direction_exchange_update_minmax" data-width="250px" data-live-search="true">
                                    <option value="0" @if(iEXSetting('is_direction_exchange_update_minmax') == 0) selected @endif>{{ __('По умолчанию') }}</option>
                                    <option value="1" @if(iEXSetting('is_direction_exchange_update_minmax') == 1) selected @endif>{{ __('Через Whitebit (auto)') }}</option>
                                </select>
                            </div>
                        </div>


                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Автоматическая генерация мин. цены направлений') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="generate_min_price" name="generate_min_price" type="checkbox" value="1" @if(iEXSetting('generate_min_price') == 1) checked @endif />
                                    <label for="generate_min_price" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Автоматическая генерация макс. цены направлений') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="generate_max_price" name="generate_max_price" type="checkbox" value="1" @if(iEXSetting('generate_max_price') == 1) checked @endif />
                                    <label for="generate_max_price" class="label-primary"></label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Отключить выборки курсов в общем списке направлений') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_direction_exchange_selected_all_courses" name="is_direction_exchange_selected_all_courses" type="checkbox" value="1" @if(iEXSetting('is_direction_exchange_selected_all_courses') == 1) checked @endif />
                                    <label for="is_direction_exchange_selected_all_courses" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек таблицы -->
    <div class="modal fade" id="settings_columns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings_columns">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка страницы') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Колонки') }}</label>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_direction_hidden_columns[direction_name]" value="direction_name" type="checkbox" name="admin_direction_hidden_columns[direction_name]" @if(in_array('direction_name', $admin_direction_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_direction_hidden_columns[direction_name]">{{ __('Направление') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_direction_hidden_columns[course]" value="course" type="checkbox" name="admin_direction_hidden_columns[course]" @if(in_array('course', $admin_direction_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_direction_hidden_columns[course]">{{ __('Курс') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_direction_hidden_columns[auto_currect]" value="auto_currect" type="checkbox" name="admin_direction_hidden_columns[auto_currect]" @if(in_array('auto_currect', $admin_direction_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_direction_hidden_columns[auto_currect]">{{ __('Автокорректировка курса') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_direction_hidden_columns[curs_bestchange]" value="curs_bestchange" type="checkbox" name="admin_direction_hidden_columns[curs_bestchange]" @if(in_array('curs_bestchange', $admin_direction_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_direction_hidden_columns[curs_bestchange]">{{ __('Курс BestChange') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_direction_hidden_columns[profit]" value="profit" type="checkbox" name="admin_direction_hidden_columns[profit]" @if(in_array('profit', $admin_direction_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_direction_hidden_columns[profit]">{{ __('Прибыль') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_direction_hidden_columns[status]" value="status" type="checkbox" name="admin_direction_hidden_columns[status]" @if(in_array('status', $admin_direction_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_direction_hidden_columns[status]">{{ __('Статус') }}</label>
                                </div>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Кол-во записей на страницу') }}</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" value="{{ iEXSetting('num_direction_paginate', 20) }}" name="num_direction_paginate">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
