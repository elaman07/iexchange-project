@extends('admin.layouts.app')

@section('title', __('Изменить название режима').' '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.modes.change', $item))

@section('content')

    <form class="form-horizontal form-block__end" method="post" action="{{ admin_base_path('/basic/direction-exchange-modes/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Изменить режим') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" class="form-control" value="{{$item->name}}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Привязать к направлениям') }}</div>
                        {{ Form::select('ids_direction_exchange[]', $exchanges, $item->direction_exchange, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15']) }}
                    </div>
                </div>
            </div>


        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction-exchange-modes') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
