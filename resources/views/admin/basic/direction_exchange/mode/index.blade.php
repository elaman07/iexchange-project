@extends('admin.layouts.app')

@section('title', __('Режимы направлений'))

@section('top-block')
    <md-button ng-href="direction-exchange-modes/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить режим') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.modes'))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Кол-во направлений') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($modes) > 0)
            @foreach($modes as $mode)
                <tr>
                    <td>{{ $mode->name }}</td>
                    <td>
                        @if(iEXSetting('is_enabled_module_direction_mode') == 0)
                            <div class="text-danger">{{ __('Включите модуль в Настройках для работы с режимами для направлений') }}</div>
                        @else
                            @if($mode->status == 0)
                                <div class="text-warning">{{ __('Отключена') }}</div>

                                <a href="?status_mode=enable&id={{ $mode->id }}">{{ __('Активировать') }}</a>
                            @else
                                <div class="text-success">{{ __('Включена') }}</div>

                                <a href="?status_mode=disable&id={{ $mode->id }}">{{ __('Отключить') }}</a>
                            @endif
                        @endif
                    </td>
                    <td>{{ isset($mode->direction_exchange) ? $mode->direction_exchange->count() : 0 }}</td>
                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="direction-exchange-modes/{{$mode->id}}/edit" class="btn btn-info btn-sm">{{ __('Изменить') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['direction-exchange-modes.destroy', $mode->id] ]) !!}
                            {!! Form::submit(__('Удалить'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки модуля') }}</span>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="form-group col-md-6">
                                    <div class="control-label-br">{{ __('Включить модуль') }}</div>
                                    <select class="form-control selectpicker" name="is_enabled_module_direction_mode">
                                        <option value="0"  @if(iEXSetting('is_enabled_module_direction_mode') == 0) selected @endif>{{ __('Нет') }}</option>
                                        <option value="1"  @if(iEXSetting('is_enabled_module_direction_mode') == 1) selected @endif>{{ __('Да') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отмена') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
