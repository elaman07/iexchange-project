@extends('admin.layouts.app')

@section('title', 'Добавить новый режим')

@section('breadcrumbs', Breadcrumbs::render('admin.basic.direction_exchange.modes.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/direction-exchange-modes') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Добавить режим') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Привязать к направлениям') }}</div>
                        {{ Form::select('ids_direction_exchange[]', $exchanges, [], ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15']) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/direction-exchange-modes') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
