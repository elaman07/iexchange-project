@extends('admin.layouts.app')

@section('title', __('admin-requisites.add_item_button'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.add'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites') }}">
        @csrf
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Счет</label>
                <div class="col-md-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.currency') }}</div>
                        <select class="form-control selectpicker" data-live-search="true" name="id_currency" data-size="10">
                            @foreach($currencies as $item)
                                <option value="{{$item->id}}" @if(old('id_currency') == $item->id) selected @endif>{{ $item->payment->name .' '. $item->code_currency->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.account_number') }}</div>
                        <input type="text" name="account_number" class="form-control" value="{{ old('account_number') }}" id="account_number" placeholder="{{ __('admin-requisites.account_number_hint') }}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.cardowner') }}</div>
                        <select class="form-control selectpicker" name="id_user" data-live-search="true">
                            <option value="0">-- {{ __('admin-requisites.not_specified') }} --</option>
                            @foreach($users as $item)
                                <option value="{{$item->id}}" @if(old('id_user') == $item->id) selected @endif>{{ $item->name}} ({{$item->email}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.group') }}</div>
                        <select class="form-control selectpicker" name="id_group">
                            @foreach($groups as $item)
                                <option value="{{$item->id}}" @if(old('id_group') == $item->id) selected @endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <div style="font-size: 13px;margin-top: 10px;color: #999;">{{ __('admin-requisites.group_hint') }}</div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Информационные поля</div>
                        {{ Form::select('info_fields[]', $info_fields, null, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                        <div style="font-size: 13px;margin-top: 10px;color: #999;">{{ __('admin-requisites.group_hint') }}</div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.status') }}</div>
                        <div class="material-switch switch-input-1">
                            <input id="status" name="status" type="checkbox" @if(old('status') == 1) checked @endif value="1" />
                            <label for="status" class="label-success"></label>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('admin-requisites.limits') }}</label>
                <div class="col-md-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.limit_views') }}</div>
                        <input type="text" name="limit_views" value="{{ old('limit_views', 0) }}" class="form-control" id="limit_views" placeholder="{{ __('admin-requisites.limit_views_placeholder') }}">
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('admin-requisites.disable_limit') }}</div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.day_limit_label') }}</div>
                        <input type="text" name="limit_day" class="form-control" id="limit_day" value="{{ old('limit_day', 0) }}" placeholder="{{ __('admin-requisites.day_limit_placeholder') }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('admin-requisites.disable_limit') }}</div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-requisites.month_limit_label') }}</div>
                        <input type="text" name="limit_month" class="form-control" id="limit_month" value="{{ old('limit_month', 0)  }}" placeholder="{{ __('admin-requisites.month_limit_placeholder') }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('admin-requisites.disable_limit') }}</div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Изображение') }}</label>
                <div class="col-md-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="photo_status">
                            <option value="0" @if(old('photo_status') == 0) selected @endif>Отключен</option>
                            <option value="1" @if(old('photo_status') == 1) selected @endif>Включен</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фото') }}</div>
                        <input type="file" name="photo_name" class="form-control" id="photo_name">
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-requisites.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites') }}">{{ __('admin-requisites.back') }}</md-button>
        </div>
    </form>
@endsection
