@extends('admin.layouts.app')

@section('title', __('Транзитные реквизиты'))

@section('top-block')
    <md-button ng-href="requisites-transit/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить реквизит') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.transit'))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Номер счета') }}</th>
            <th>{{ __('Валюта') }}</th>
            <th>{{ __('Статус') }}</th>
            <th>{{ __('Кол. Использований') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <th>
                        <a href="requisites-transit/{{$item->id}}/edit">
                            {{ $item->account }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>{{ (isset($item->currency) and ($item->currency->payment)) ? $item->currency->payment->name : ''}} {{ (isset($item->currency) and ($item->currency->code_currency)) ? $item->currency->code_currency->name : ''}}</td>
                    <td>
                        @if($item->status == 0)
                            <span class="text-danger">{{ __('Отключен') }}</span>
                        @else
                            <span class="text-success">{{ __('Включен') }}</span>
                        @endif
                    </td>
                    <td>{{ $item->view }}</td>

                    <td style="width: 50px;">
                        {!! Form::open(['id' => 'requisites_transit_destroy_'.$item->id, 'method' => 'DELETE', 'route' => ['requisites-transit.destroy', $item->id] ]) !!}
                        {!! Form::close() !!}

                        <a href="javascript:void(0)" onclick="document.getElementById('requisites_transit_destroy_{{ $item->id }}').submit()">
                            <i class="fas fa-trash text-danger"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-requisites.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
