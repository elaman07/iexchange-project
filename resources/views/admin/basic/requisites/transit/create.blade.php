@extends('admin.layouts.app')

@section('title', __('Добавить реквизит'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.transit.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites-transit') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label class="col-sm-2 control-label">Счет</label>
                <div class="col-md-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Валюта') }}</div>
                        <select class="form-control selectpicker" data-live-search="true" name="id_currency" data-size="10">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}" @if(old('id_currency') == $currency->id) selected @endif>
                                    {{ $currency->payment->name .' '. $currency->code_currency->name }} ({{ $currency->status == 1 ? __('отключена') : __('включена') }})
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Номер счета') }}</div>
                        <input type="text" name="account" class="form-control" value="{{ old('account') }}" id="account" placeholder="{{ __('Укажите номер счета') }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <div class="material-switch switch-input-1">
                            <input id="status" name="status" type="checkbox" @if(old('status') == 1) checked @endif value="1" />
                            <label for="status" class="label-success"></label>
                        </div>
                    </div>
                </div>
            </div>



        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites-transit') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
