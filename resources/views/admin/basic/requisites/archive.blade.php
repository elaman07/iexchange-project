@extends('admin.layouts.app')

@section('title', __('admin-requisites.archive_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.archive'))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-requisites.created_at_archive') }}</th>
            <th>{{ __('admin-requisites.currency') }}</th>
            <th>{{ __('admin-requisites.account_number') }}</th>
            <th>{!! __('admin-requisites.exchange_today') !!}</th>
            <th>{!! __('admin-requisites.exchange_month') !!}</th>
            <th class="not-sortable">{{ __('admin-requisites.status') }}</th>
            <th class="not-sortable"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($archives as $item)
            <tr>
                <th>
                    @if(!is_null($item->history_at))
                        {{ $item->history_at }}
                    @else
                        {{ $item->updated_at }}
                    @endif
                </th>
                <td>{{ $item->currency->payment->name.' '.$item->currency->code_currency->name }}</td>
                <td>
                    @if($item->generate_address)
                        <span class="text-success">{{ __('admin-requisites.dynamic') }}</span>
                    @else
                        {{ \Str::limit($item->account_number, 50) }}
                    @endif
                </td>
                <td>
                    @if($item->generate_address)
                        <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                    @else
                        {{ $item->summa_day(). ' '. $item->currency->code_currency->sign }}
                    @endif
                </td>
                <td>
                    @if($item->generate_address)
                        <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                    @else
                        {{ $item->summa_month(). ' '. $item->currency->code_currency->sign }}
                    @endif
                </td>
                <td>
                    <span class="label label-warning">{{ __('admin-requisites.archived') }}</span>
                </td>
                <td style="width:150px;">
                    <md-button href="archive?id={{ $item->id }}&restore" class="md-raised md-primary">
                        {{ __('admin-requisites.restore') }}
                    </md-button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $archives->links() !!}
    </div>
@endsection