@extends('admin.layouts.app')

@section('title', __('admin-requisites.black_list.edit_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.blacklist.change', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites-blacklist/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group">
                <label class="control-label col-md-4">{{ __('admin-requisites.black_list.account_number') }}</label>
                <div class="col-md-8">
                    {!! Form::input('text', 'score', $item->score, ['class' => 'form-control', 'required']) !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">{{ __('admin-requisites.black_list.text') }}</label>
                <div class="col-md-6">
                    {!! Form::textarea('text', $item->text, ['class' => 'form-control', 'rows' => 5]) !!}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-requisites.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites-blacklist') }}">{{ __('admin-requisites.back') }}</md-button>
        </div>
    </form>

@endsection