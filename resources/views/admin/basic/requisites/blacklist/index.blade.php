@extends('admin.layouts.app')

@section('title', __('admin-requisites.black_list.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.blacklist'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#add" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-requisites.black_list.add_item') }}</span>
    </md-button>
@endsection

@section('content')
    <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-requisites.black_list.id') }}</th>
                <th>{{ __('admin-requisites.black_list.account_number') }}</th>
                <th>{{ __('admin-requisites.black_list.text') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($requisites) > 0)
                @foreach($requisites as $requisite)
                    <tr>
                        <th>{{$requisite->id}}</th>
                        <td>{{$requisite->score}}</td>
                        <td>{{$requisite->text}}</td>
                        <td style="width: 200px;">
                            <div class="col-md-6">
                                <a href="requisites-blacklist/{{$requisite->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-requisites.edit') }}</a>
                            </div>

                            <div class="col-md-6">
                                {!! Form::open(['method' => 'DELETE', 'route' => ['requisites-blacklist.destroy', $requisite->id] ]) !!}
                                {!! Form::submit(__('admin-requisites.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-requisites.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>

    <!-- Модальное окно для добавления ссылки -->
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form action="?" method="POST" class="form-horizontal">
                <input type="hidden" name="export_users" value="enable">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-requisites.black_list.add_item') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-requisites.black_list.account_number') }}</label>
                            <div class="col-md-8">
                                {!! Form::input('text', 'score', null, ['class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('admin-requisites.black_list.text') }}</label>
                            <div class="col-md-8">
                                {!! Form::textarea('text', null, ['class' => 'form-control', 'rows' => 5]) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-requisites.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-requisites.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
