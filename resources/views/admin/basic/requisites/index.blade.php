@extends('admin.layouts.app')

@section('title', __('admin-requisites.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites'))

@section('video__instruction', 'https://www.youtube.com/embed/kbc7hTFENpE?autoplay=1&amp;mute=0')

@section('top-block')

    <md-button ng-href="requisites/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить реквизит') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')
    <style>
        md-tabs [role="tabpanel"] {transition: none;}
        md-tabs md-ink-bar {transition: none;}

        .x_panel md-tabs md-content {
            background: #fff;
        }
    </style>

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">
                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-requisites.account_number') }}</label>
                                    <div class="col-md-6">
                                        {{ Form::input('text', 'account_number', is_filter_search($filter, 'account_number'), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-requisites.currency') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('id_currencies[]', $currencies, is_filter_search($filter, 'id_currencies'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-requisites.status') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('status[]', $statuses, is_filter_search($filter, 'status'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-requisites.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-requisites.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <!-- Счета из общей группы -->
    @if(count($bases_groups) > 0 or !empty($filter))
        <div class="x_panel">
            <div class="x_title">
                <h2>{{ __('admin-requisites.common_group_requisites') }}</h2>
                <div class="clearfix"></div>
            </div>

            <div class="x_content" id="showSettings">
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-requisites.currency') }}</th>
                        <th>{{ __('admin-requisites.account_number') }}</th>
                        <th>{{ __('admin-requisites.views') }}</th>
                        <th>{!! __('admin-requisites.day_limit') !!}</th>
                        <th>{!! __('admin-requisites.month_limit') !!}</th>
                        <th>{!! __('admin-requisites.exchange_today') !!}</th>
                        <th>{!! __('admin-requisites.exchange_week') !!}</th>
                        <th>{!! __('admin-requisites.exchange_month') !!}</th>
                        <th class="not-sortable">{{ __('admin-requisites.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bases_groups as $item)
                        <tr>
                            <th>
                                <input type="hidden" name="requisites_data[{{$item->id}}]">
                                <a href="requisites/{{$item->id}}/edit">
                                    {{ $item->currency->payment->name.' '.$item->currency->code_currency->name }}
                                </a>
                            </th>
                            <td>
                                @if($item->generate_address)
                                    <span class="text-success">{{ __('admin-requisites.dynamic') }}</span>
                                @else
                                    <input type="text" name="account_number[{{$item->id}}]" style="width: 300px" class="form-control" value="{{ $item->account_number }}">
                                @endif
                            </td>
                            <td>
                                @if($item->generate_address)
                                    <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                @else
                                    <div>{{ $item->view }}</div>
                                    <a href="requisites/{{$item->id}}/zero_out" style="font-size: 11px;">{{ __('admin-requisites.zero_out') }}</a>
                                @endif
                            </td>
                            <td>
                                @if($item->limit_day == 0)
                                    <span class="text-primary">{{ __('admin-requisites.disable') }}</span>
                                @else
                                    {{ $item->limit_day. ' '. $item->currency->code_currency->sign }}
                                @endif
                            </td>
                            <td>
                                @if($item->limit_month == 0)
                                    <span class="text-primary">{{ __('admin-requisites.disable') }}</span>
                                @else
                                    {{ $item->limit_month. ' '. $item->currency->code_currency->sign }}
                                @endif
                            </td>
                            <td>
                                @if($item->generate_address)
                                    <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                @else
                                    {{ $item->summa_day(). ' '. $item->currency->code_currency->sign }}
                                @endif
                            </td>
                            <td>
                                @if($item->generate_address)
                                    <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                @else
                                    {{ $item->summa_month(). ' '. $item->currency->code_currency->sign }}
                                @endif
                            </td>
                            <td>
                                @if($item->generate_address)
                                    <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                @else
                                    {{ $item->summa_week(). ' '. $item->currency->code_currency->sign }}
                                @endif
                            </td>
                            <td>
                                <div class="material-switch">
                                    <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="0" @if($item->status == 1) checked @endif />
                                    <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                                </div>
                            </td>

                            <td style="width:150px;">

                                <md-button class="md-icon-button" ng-href="requisites/{{ $item->id }}/duplicate">
                                    <i class="far fa-copy"></i>
                                </md-button>


                                <md-button href="requisites/?id={{$item->id}}&delete" class="md-icon-button md-warn" >
                                    <i class="far fa-trash"></i>
                                </md-button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
    @if(empty($filter))
        <div class="x_panel tab-capitalize">

            <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites') }}">
                @csrf
                <md-tabs md-border-bottom md-dynamic-height>
                    @foreach($groups as $group)
                        <md-tab label="{{$group->name}}">
                            <md-content>
                                <table class="manager-table-2 table table-border-2">
                                    <thead>
                                    <tr>
                                        <th>{{ __('admin-requisites.currency') }}</th>
                                        <th>{{ __('admin-requisites.account_number') }}</th>
                                        <th>{{ __('admin-requisites.views') }}</th>
                                        <th>{!! __('admin-requisites.day_limit') !!}</th>
                                        <th>{!! __('admin-requisites.month_limit') !!}</th>
                                        <th>{!! __('admin-requisites.exchange_today') !!}</th>
                                        <th>{!! __('admin-requisites.exchange_week') !!}</th>
                                        <th>{!! __('admin-requisites.exchange_month') !!}</th>
                                        <th class="not-sortable">{{ __('admin-requisites.status') }}</th>
                                        <th class="not-sortable"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(count($group->requisites) > 0)
                                        @php
                                            $requisites = $group->requisites()->paginate((int)iEXSetting('admin_requisites_pagination'));
                                        @endphp
                                        @foreach($requisites as $item)
                                            <tr @if($item->limit_views > 0) @if($item->view > $item->limit_views) style="border-left: 3px solid #d10606;" @else style="border-left: 3px solid #cfdb14;" @endif @endif>
                                                <th>
                                                    <input type="hidden" name="requisites_data[{{$item->id}}]">

                                                    <a href="requisites/{{$item->id}}/edit">
                                                        {{ $item->currency->payment->name.' '.$item->currency->code_currency->name }}
                                                    </a>
                                                </th>
                                                <td>
                                                    @if($item->generate_address)
                                                        <span class="text-success">{{ __('admin-requisites.dynamic') }}</span>
                                                    @else
                                                        <input type="text" name="account_number[{{$item->id}}]" style="width: 300px" class="form-control" value="{{ $item->account_number }}">
                                                    @endif
                                                        @if($item->limit_views > 0)
                                                            @if($item->view > $item->limit_views)
                                                                <div style="margin-top: 5px; color: red;font-size: 11px" class="text-muted">
                                                                    Превышен установленный лимит показов {{ $item->view }} из {{ $item->limit_views }}
                                                                </div>
                                                            @else
                                                                <div style="margin-top: 5px; font-size: 11px" class="text-warning">
                                                                    Ограниченный показ счета: {{ $item->view }} из {{ $item->limit_views }}
                                                                </div>
                                                            @endif
                                                        @endif

                                                </td>
                                                <td>
                                                    @if($item->generate_address)
                                                        <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                                    @else
                                                        <div>{{ $item->view }}</div>
                                                        <a href="requisites/{{$item->id}}/zero_out" style="font-size: 11px;">{{ __('admin-requisites.zero_out') }}</a>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->limit_day == 0)
                                                        <span class="text-primary">{{ __('admin-requisites.disable') }}</span>
                                                    @else
                                                        {{ $item->limit_day. ' '. $item->currency->code_currency->sign }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->limit_month == 0)
                                                        <span class="text-primary">{{ __('admin-requisites.disable') }}</span>
                                                    @else
                                                        {{ $item->limit_month. ' '. $item->currency->code_currency->sign }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->generate_address)
                                                        <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                                    @else
                                                        {{ $item->summa_day(). ' '. $item->currency->code_currency->sign }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->generate_address)
                                                        <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                                    @else
                                                        {{ $item->summa_month(). ' '. $item->currency->code_currency->sign }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->generate_address)
                                                        <span class="text-warning">{{ __('admin-requisites.not_available') }}</span>
                                                    @else
                                                        {{ $item->summa_week(). ' '. $item->currency->code_currency->sign }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="material-switch">
                                                        <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="0" @if($item->status == 1) checked @endif />
                                                        <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                                                    </div>
                                                </td>

                                                <td style="width:50px;">
                                                    <md-button href="requisites/?id={{$item->id}}&delete" class="md-icon-button md-warn" >
                                                        <i class="fas fa-trash"></i>
                                                    </md-button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        <tr>
                                            <td colspan="10">
                                                <div class="pagination-right">
                                                    {{ $requisites->links() }}
                                                </div>
                                            </td>
                                        </tr>

                                    @else
                                        <tr>
                                            <td colspan="10"><p align="center"><br />{{ __('admin-requisites.list_is_empty') }}</p></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </md-content>
                        </md-tab>
                    @endforeach
                </md-tabs>

                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('admin-requisites.save') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-requisites.run') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    @endif


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-basic.black_list.settings_title') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">Включить модуль "Общие реквизиты"</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_iexrequisites_enabled" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_iexrequisites_enabled') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_iexrequisites_enabled') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="admin_currency_pagination" class="control-label col-md-6">{{ __('Кол-во записей на странице') }}</label>
                            <div class="col-sm-6">
                                <input type="text" name="admin_requisites_pagination" class="form-control" value="{{ iEXSetting('admin_requisites_pagination', 20) }}">
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />
                        <x-forms.language.textarea label="Текст при запросе реквизита" tabName="description_request_payment" keyName="description_request_payment" />
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-basic.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
