@extends('admin.layouts.app')

@section('title', 'Список информационных полей')

@section('top-block')
    <md-button ng-href="requisites-info-fields/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">Добавить поле</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.info-fields'))


@section('content')
    <form action="?" method="post">
        @csrf
        <input type="hidden" name="action" value="update">

        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Название</th>
                <th>Значение</th>
                <th>Привязанные реквизиты</th>
                <th>Дата создания</th>
                <th>Посл. обновление</th>
                <th>Статус</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($fields) > 0)
                @foreach($fields as $field)
                    <tr>
                        <th>
                            <input type="hidden" name="item_id[{{ $field->id }}]">
                            @if(empty($field->key_name))
                                <a href="{{ admin_base_path('/basic/requisites-info-fields/'.$field->id.'/edit') }}">
                                    <small class="text-danger">
                                        <i class="far fa-exclamation-circle"></i>
                                        Нет названия &nbsp;<i class="fal fa-pencil"></i>
                                    </small>
                                </a>
                            @else
                                <a href="{{ admin_base_path('/basic/requisites-info-fields/'.$field->id.'/edit') }}">{{ $field->key_name }}   &nbsp;<i class="fal fa-pencil"></i></a>
                            @endif
                        </th>
                        <td>{{ $field->value_name }}</td>
                        <td>
                            @if($field->requisites->count() > 0)
                                <ul class="list-unstyled">
                                    @foreach($field->requisites as $value)
                                        <li>- <a style="font-size: 11px;color: @if($value->status == 1)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/requisites/'.$value->id.'/edit') }}">{{ $value->account_number }}</a></li>
                                    @endforeach
                                </ul>
                            @else
                                <small class="text-danger">Не найдено</small>
                            @endif
                        </td>
                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($field->created_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($field->created_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <div class=""> {{ \Illuminate\Support\Carbon::parse($field->updated_at)->translatedFormat('d M Y H:i') }}</div>
                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($field->updated_at)->diffForHumans() }}</small>
                        </td>

                        <td>
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{ $field->id }}" name="status[{{ $field->id }}]" type="checkbox" value="1" @if($field->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{ $field->id }}" class="label-success"></label>
                            </div>
                        </td>
                        <td style="width: 100px;">
                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('basic/requisites-info-fields/'.$field->id.'/delete')  }}">
                                <i class="fas fa-trash text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-requisites.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-requisites.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-requisites.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection
