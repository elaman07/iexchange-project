@extends('admin.layouts.app')

@section('title', 'Изменить поле '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.info-fields.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites-info-fields/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Доп. поле</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-requisites.groups.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#key_name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="key_name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="key_name{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('key_name', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>Значение</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#value_name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="value_name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="value_name{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('value_name', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-requisites.groups.disabled') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-requisites.groups.enabled') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-requisites.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites-info-fields') }}">{{ __('admin-requisites.back') }}</md-button>
        </div>
    </form>

@endsection
