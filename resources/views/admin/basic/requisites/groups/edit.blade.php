@extends('admin.layouts.app')

@section('title', __('admin-requisites.groups.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.groups.change', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites-groups/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-requisites.groups.name') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control" value="{{$item->name}}" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-requisites.groups.status') }}</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" name="status" data-width="150">
                        <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-requisites.groups.disabled') }}</option>
                        <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-requisites.groups.enabled') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-requisites.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites-groups') }}">{{ __('admin-requisites.back') }}</md-button>
        </div>
    </form>

@endsection