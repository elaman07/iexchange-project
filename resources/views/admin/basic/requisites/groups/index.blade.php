@extends('admin.layouts.app')

@section('title', __('admin-requisites.groups.title'))

@section('top-block')
    <md-button ng-href="requisites-groups/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-requisites.groups.add_item') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.groups'))


@section('content')
    <form style="width:100%;margin-right: 10px;" method="get" action="?">
        @csrf
        <input type="hidden" name="action" value="update">

        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-requisites.groups.id') }}</th>
                <th>{{ __('admin-requisites.groups.name') }}</th>
                <th>{{ __('admin-requisites.groups.position') }}</th>
                <th>{{ __('admin-requisites.groups.status') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($groups) > 0)
                @foreach($groups as $group)
                    <tr>
                        <th>{{$group->id}}</th>
                        <td>{{$group->name}}</td>
                        <td>
                            <input type="text" name="sorting[{{$group->id}}]" class="form-control" value="{{$group->sorting}}" required style="width: 70px;text-align: center;">
                        </td>
                        <td>
                            @if($group->status == 0)
                                <span class="text-danger">{{ __('admin-requisites.groups.disabled') }}</span>
                            @else
                                <span class="text-success">{{ __('admin-requisites.groups.enabled') }}</span>
                            @endif
                        </td>
                        <td style="width: 200px;">
                            <div class="col-md-6">
                                <a href="requisites-groups/{{$group->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-requisites.edit') }}</a>
                            </div>

                            <div class="col-md-6">
                                {!! Form::open(['method' => 'DELETE', 'route' => ['requisites-groups.destroy', $group->id] ]) !!}
                                {!! Form::submit(__('admin-requisites.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-requisites.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-requisites.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-requisites.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection