
@extends('admin.layouts.app')

@section('title', __('Изменить реквизит'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.change', $requisite->id))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites/'.$requisite->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Счет') }}</label>
                <div class="col-md-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Валюта') }}</div>
                        <select class="form-control selectpicker" data-live-search="true" name="id_currency" data-size="10">
                            @foreach($currencies as $item)
                                <option value="{{$item->id}}" @if($requisite->id_currency == $item->id) selected @endif>{{ $item->payment->name .' '. $item->code_currency->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Номер счета') }}</div>
                        <input type="text" name="account_number" class="form-control" value="{{ $requisite->account_number }}" id="account_number" required>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Владелец') }}</div>
                        <select class="form-control selectpicker" name="id_user" data-live-search="true">
                            <option value="0">-- {{ __('Не указано') }} --</option>
                            @foreach($users as $item)
                                <option value="{{$item->id}}" @if($requisite->id_user == $item->id) selected @endif>{{ $item->name}} ({{$item->email}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>


                    <!-- Custom текст для реквизитов -->
                    @if($requisite->account_number == '[request_payment]')
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-12 multi-language-form-group">
                                <div class="control-label-br">
                                    <div layout="row">
                                        <div>{{ __('Текст для реквизитов по запросу') }}</div>
                                        <span flex></span>
                                        <ul class="nav nav-pills lang-tabs">
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <li @if($form_lang['active'] == true) class="active" @endif>
                                                    <a class="lang-tabs-link" data-toggle="tab" href="#text_request_payment-{{ $form_lang_key }}">
                                                        {{ $form_lang_key }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                    <div id="text_request_payment-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                        <textarea rows="5" name="text_request_payment{{$form_lang['field']}}" id="text_request_payment{{$form_lang['field']}}" class="form-control">{{ $requisite->getTranslation('text_request_payment', $form_lang_key) }}</textarea>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Группа') }}</div>
                        <select class="form-control selectpicker" name="id_group">
                            @foreach($groups as $item)
                                <option value="{{$item->id}}" @if($requisite->id_group == $item->id) selected @endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Информационные поля') }}</div>
                        {{ Form::select('info_fields[]', $info_fields, $requisite->requisites_info_fields, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <div class="material-switch switch-input-1">
                            <input id="status" name="status" type="checkbox" @if($requisite->status == 1) checked @endif value="1" />
                            <label for="status" class="label-success"></label>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Уникальный номер счета') }}</div>
                        <div class="material-switch switch-input-1">
                            <input id="is_unique_shot" name="is_unique_shot" type="checkbox" @if($requisite->is_unique_shot == 1) checked @endif value="1" />
                            <label for="is_unique_shot" class="label-success"></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Лимиты') }}</label>
                <div class="col-md-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Лимит просмотров') }}</div>
                        <input type="text" name="limit_views" value="{{ $requisite->limit_views }}" class="form-control" id="limit_views">
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Дневной лимит') }}</div>
                        <input type="text" name="limit_day" class="form-control" id="limit_day" value="{{ $requisite->limit_day }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Месячный лимит') }}</div>
                        <input type="text" name="limit_month" class="form-control" id="limit_month" value="{{ $requisite->limit_month }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>0</b> - {{ __('отключает ограничения') }}</div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Изображение') }}</label>
                <div class="col-md-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="photo_status">
                            <option value="0" @if($requisite->photo_status == 0) selected @endif>Отключен</option>
                            <option value="1" @if($requisite->photo_status == 1) selected @endif>Включен</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Фото') }}</div>
                        <input type="file" name="photo_name" class="form-control" id="photo_name">
                        <br />
                        @if(!empty($requisite->photo_name))
                            {{ __('Выбран') }}: <a target="_blank" href="{{ url('/storage/'.$requisite->photo_name) }}">{{  $requisite->photo_name }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
