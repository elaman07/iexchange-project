@extends('admin.layouts.app')

@section('title', 'Изменить поле '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('admin.basic.requisites.fields.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/requisites-fields/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Доп. поле</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Валюты</div>
                        {{ Form::select('currencies[]', $currencies, $item->currencies, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '15']) }}
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-requisites.groups.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang_value['field']}}" class="form-control"  value="{{ $item->getTranslation('name', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Значение</div>
                        <input type="text" name="value" class="form-control" value="{{ $item->value }}" required>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Префикс</div>
                        <input type="text" name="prefix" value="{{ $item->prefix }}" class="form-control" placeholder="Введите префикс доп. поля">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-requisites.field_comment') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <textarea type="text" name="comment{{$form_lang_value['field']}}" class="form-control" rows="5" placeholder="{{ __('admin-requisites.field_comment_placeholder') }}">{{ $item->getTranslation('comment', $form_lang_key) }}</textarea>
                                <div class="input-p-text">{{ __('admin-requisites.field_comment_hint') }}</div>
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Статус</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-requisites.groups.disabled') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-requisites.groups.enabled') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-requisites.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/requisites-fields') }}">{{ __('admin-requisites.back') }}</md-button>
        </div>
    </form>

@endsection
