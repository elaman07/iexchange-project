@extends('admin.layouts.app')

@section('title', __('Архив валют'))

@section('breadcrumbs', Breadcrumbs::render('admin.currency.archive'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-basic.currency.archive_title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>
                            <div class="form-check">
                                <input class="form-check-input iex-select-all" id="iex-checkbox-all" type="checkbox" name="iex-select-all">
                                <label class="form-check-label" for="iex-checkbox-all"></label>
                            </div>
                        </th>
                        <th>{{ __('Валюта') }}</th>
                        <th>{{ __('Код валюты') }}</th>
                        <th>{{ __('Получено') }} ←</th>
                        <th>{{ __('Отправлено') }} →</th>
                        <th>{{ __('XML') }}</th>
                        <th class="not-sortable">{{ __('Статус') }}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($currencies as $item)
                        <tr @if(!isset($item->reserve)) style="background-color: #ffefef" @endif>
                            <th>
                                <div class="form-check">
                                    <input class="form-check-input iex-select-item" id="checkbox{{$item->id}}" value="{{$item->id}}" type="checkbox" name="currency_checkbox[]">
                                    <label class="form-check-label" for="checkbox{{$item->id}}"></label>
                                </div>
                            </th>
                            <td>
                                @if(isset($item->payment))
                                    {{ $item->payment->name }}
                                @else
                                    <span>{{ __('Без названия') }}</span>
                                @endif
                            </td>
                            <td data-label="{{ __('Код валюты') }}">{{ $item->code_currency->name }}</td>
                            <td>
                                @if(isset($item->currency_analytics))
                                    {{ iex_number_format((float)$item->currency_analytics->in_amount, $item->number_format, true) }}
                                @else
                                    0
                                @endif
                            </td>
                            <td>
                                @if(isset($item->currency_analytics))
                                    {{ iex_number_format((float)$item->currency_analytics->out_amount, $item->number_format, true) }}
                                @else
                                    0
                                @endif
                            </td>
                            <td style="width: 100px;">{{$item->designation_xml}}</td>

                            <td>
                                <label class="label label-warning">{{ __('admin-basic.currency.archived') }}</label>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="archive">{{ __('Вернуть') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
