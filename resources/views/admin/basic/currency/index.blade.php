@extends('admin.layouts.app')

@section('title', __('Валюты'))

@section('video__instruction', 'https://www.youtube.com/embed/v-C19ziSZjA?autoplay=1&amp;mute=1')

@section('top-block')
    <md-button data-toggle="modal" data-target="#create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить валюту') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings_columns" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-table"></md-icon>
            <span>{{ __('Настройка страницы') }}</span>
        </md-button>

        <md-button data-toggle="modal" data-target="#settings" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.currency'))

@section('no-block-content')

    <style>
        .bg__notifytext-red {
            max-width: 300px;
            display: block;
            background: #ffe4e4;
            color: #b91212;
            padding: 10px;
            border-radius: 5px;
            margin-top: 3px;
        }

        .text-decoration-underline {
            text-decoration: underline;
        }

        .currency-name__block-link b{
            font-size: 14px;
        }
    </style>

    <div class="x_panel">
        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Статус') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('status[]', $statuses, is_filter_search($filter, 'status'),
                                            ['class' => 'form-control selectpicker', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Сортировать по') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('sorting_by', $sorting, is_filter_search($filter, 'sorting_by'),
                                           ['class' => 'form-control selectpicker']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Группа') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('id_group[]', $groups, is_filter_search($filter, 'id_group'),
                                            ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Платежные системы') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('payments[]', $payments, is_filter_search($filter, 'payments'),
                                            ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Коды валют') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('code_currency[]', $code_currencies, is_filter_search($filter, 'code_currency'),
                                            ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Фильтры валют') }}</label>
                                    <div class="col-md-6">
                                        {!! Form::select('filter_currency[]', $filter_currencies, is_filter_search($filter, 'filter_currency'),
                                            ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('Обозначение XML') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" name="designation_xml" value="{{ is_filter_search($filter, 'designation_xml') }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Применить фильтры') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                </div>
            </form>
        </div>
    </div>

    <div class="pagination-right">
        {!! $valuta->appends($filter)->links() !!}
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Валюты') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" action="?">
                @csrf

                <table class="table table-border-2 iex-adaptive__table">
                    <thead>
                    <tr>
                        <th>
                            <div class="form-check">
                                <input class="form-check-input iex-select-all" id="iex-checkbox-all" type="checkbox" name="iex-select-all">
                                <label class="form-check-label" for="iex-checkbox-all"></label>
                            </div>
                        </th>
                        @if(in_array('currency', $admin_currency_hidden_columns))
                            <th>{{ __('Валюта') }}</th>
                        @endif
                        @if(in_array('code_currency', $admin_currency_hidden_columns))
                            <th>{{ __('Код валюты') }}</th>
                        @endif
                        @if(in_array('xml', $admin_currency_hidden_columns))
                            <th>{{ __('XML') }}</th>
                        @endif
                        @if(in_array('merchant', $admin_currency_hidden_columns))
                            <th>{{ __('Мерчант') }}</th>
                        @endif
                        @if(in_array('payment', $admin_currency_hidden_columns))
                            <th>{{ __('Выплата') }}</th>
                        @endif
                        @if(in_array('reserve', $admin_currency_hidden_columns))
                            <th>{{ __('Резерв') }}</th>
                        @endif
                        @if(in_array('receiving', $admin_currency_hidden_columns))
                            <th>{{ __('Получено') }} ←</th>
                        @endif
                        @if(in_array('sending', $admin_currency_hidden_columns))
                            <th>{{ __('Отправлено') }} →</th>
                        @endif
                        @if(in_array('number_format', $admin_currency_hidden_columns))
                            <th data-toggle="tooltip" data-title="{{ __('Кол-во знаков после запятой') }}">{{ __('Знаков после запятой') }}</th>
                        @endif

                        @if(in_array('last_updated', $admin_currency_hidden_columns))
                            <th class="not-sortable">{{ __('Посл. обновление') }}</th>
                        @endif
                        @if(in_array('status', $admin_currency_hidden_columns))
                            <th class="not-sortable">{{ __('Статус') }}</th>
                        @endif
                        <th class="not-sortable"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($valuta) > 0)
                        @foreach($valuta as $item)
                            <tr @if(!isset($item->reserve)) class="table-empty-reserve" @endif @if($item->status == 1) style="background-color: rgb(255 197 197 / 12%);border-left: 3px solid #dc0000;opacity: .6;" @endif>
                                <th>
                                    <div class="form-check">
                                        <input class="form-check-input iex-select-item" id="checkbox{{$item->id}}" value="{{$item->id}}" type="checkbox" name="currency_checkbox[]">
                                        <label class="form-check-label" for="checkbox{{$item->id}}"></label>
                                    </div>
                                </th>
                                @if(in_array('currency', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Валюта') }}">

                                        @if(isset($item->payment))
                                            <a class="currency-name__block-link @if($item->status != 0) link-danger-unactive @endif" href="{{ admin_base_path('/basic/currency/'.$item->id.'/edit') }}">

                                                @if((int)iEXSetting('is_module_storage_disk') == 1 and !empty(iEXSetting('storage_disk_payment_system')) and $item->payment->is_local_image == 1)
                                                    <img src="{{ iex_dynamic_read_view_image('/payment_systems/'.$item->payment->logo) }}" width="15" alt="{{ $item->payment->name }}"/>
                                                @else
                                                    @if(!empty($item->payment->logo) and iex_file_check(public_path('/storage/payment_systems/'.$item->payment->logo)))
                                                        <img width="15" src="/storage/payment_systems/{{ $item->payment->logo }}" alt="{{ $item->payment->name }}"/>
                                                    @endif
                                                @endif

                                                <b>{{ $item->payment->name }}  &nbsp;<i class="fal fa-pencil"></i></b>
                                            </a>
                                            <br />
                                        @else
                                            <span>{{ __('Без названия') }}</span>
                                        @endif

                                        <div class="padding-tb-5">
                                            @if($item->id_group == 0)
                                                <small class="label label-warning label-small">{{ __('Нет группы') }}</small>
                                            @else
                                                <small class="label label-default label-small">{{ __('Группа') }}: {{ $item->group->name }}</small>
                                            @endif
                                        </div>

                                        @if(!empty($item->is_valid_account))
                                            <small class="text-success display-block">{{ __('Валидатор') }}: {{$item->is_valid_account}}</small>
                                        @endif

                                        @if(iEXSetting('is_currency_disabled_admin_filter') == 0 and isset($item->filter_currency))
                                            <small class="text-muted display-block">{{ __('Фильтр') }}: <b>{{ $item->filter_currency->name }}</b></small>
                                        @endif

                                        @if($item->is_enabled_verification == 1)
                                            @if($item->min_amount_verification == 0)
                                                <div class="bg__notifytext-red small">
                                                    {{ __('Чтобы активировать верификацию карт, укажите значение в поле Мин. сумма обмена для Отдаю') }}
                                                </div>
                                            @else
                                                <div class="text-success small">{{ __('Верификация карт: Активна') }}</div>
                                            @endif
                                        @endif

                                        @if($item->direction_exchange_in_count > 0 )
                                            <div class="small">
                                                <a  href="{{ admin_base_path('basic/direction_exchange?sorting_by=id-desc&currencies_in[]='.$item->id) }}">{{ __('Направлений отдаю') }}: {{ $item->direction_exchange_in_count }}</a>
                                            </div>
                                        @endif

                                        @if($item->direction_exchange_out_count > 0)
                                            <div class="small">
                                                <a  href="{{ admin_base_path('basic/direction_exchange?sorting_by=id-desc&currencies_out[]='.$item->id) }}">{{ __('Направлений получаю') }}: {{ $item->direction_exchange_out_count }} </a>
                                            </div>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('code_currency', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Код валюты') }}">{{ $item->code_currency->name }}</td>
                                @endif
                                @if(in_array('xml', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('XML') }}" style="width: 130px;">

                                        @if(isset($bestchange_codes[$item->designation_xml]) and $bestchange_codes[$item->designation_xml] == $item->designation_xml)
                                            <label class="label label-success">
                                                <i class="far fa-check"></i>&nbsp;
                                                {{ $item->designation_xml }}
                                            </label>
                                        @else
                                            <label class="label label-warning">
                                                <i class="far fa-exclamation-circle"></i>&nbsp;
                                                {{ $item->designation_xml }}
                                            </label>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('merchant', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Мерчант') }}">
                                        @if($item->status == 0)
                                            {{ Form::select('gateways_merchants['.$item->id.'][]', $gateways_merchants, $item->merchants, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size'=> '10', 'data-width' => '200px']) }}
                                        @else
                                            <div class="text-danger">{{ __('admin-basic.currency.currency_disabled') }}</div>
                                        @endif
                                        @if($item->is_getblockbot_tx_in == 1)
                                            <div class="clearfix"></div>
                                            <div class="currency-aml-enabled">{{ __('AML анализ транзакций активен') }}</div>
                                        @endif

                                        @if($item->kyc_enabled == 1)
                                            <div class="clearfix"></div>
                                            <div class="currency-aml-enabled">{{ __('AML уведомление активна') }}</div>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('payment', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Выплата') }}">
                                        @if($item->status == 0)
                                            <select data-live-search="true" class="form-control selectpicker" name="id_pay[{{ $item->id }}]"  data-size="8">
                                                <option value="0">--  {{ __('Не выбран') }} --</option>
                                                @foreach($pays as $pay)
                                                    <option value="{{$pay->id}}" @if($item->id_pay == $pay->id) selected @endif>{{$pay->name}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <div class="text-danger">{{ __('Валюта отключена') }}</div>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('reserve', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Резерв') }}">
                                        @if(isset($item->reserve))
                                            {{--                                        <md-button class="md-primary" href="{{ admin_base_path('analytics/reserves?currencies[]='.$item->id) }}">--}}
                                            {{--                                            {{$item->reserve->summa}}--}}
                                            {{--                                        </md-button>--}}

                                            @if(iex_reserve_amount($item->reserve) >= 0)
                                                <b @if(iex_reserve_amount($item->reserve) > 0) class="text-success" @endif>
                                                    {{ iex_number_format((float)iex_reserve_amount($item->reserve), $item->number_format, true, true) }}
                                                </b>
                                            @else
                                                <b class="text-danger">{{ iex_number_format((float)iex_reserve_amount($item->reserve), $item->number_format, true, true) }}</b>
                                            @endif
                                        @else
                                            <input type="text" class="form-control" name="reserve_value[{{ $item->id }}]" value="0" style="width: 150px">
                                        @endif
                                        <br />
                                        {{--<a class="btn btn-default btn-xs" href="{{config('admin.directory')}}/basic/reserves">Изменить</a>--}}
                                    </td>
                                @endif
                                @if(in_array('receiving', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Получено ←') }}">
                                        @if(isset($item->currency_analytics))
                                            {{ iex_number_format((float)$item->currency_analytics->in_amount, $item->number_format, true) }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('sending', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Отправлено →') }}">
                                        @if(isset($item->currency_analytics))
                                            {{ iex_number_format((float)$item->currency_analytics->out_amount, $item->number_format, true) }}
                                        @else
                                            0
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('number_format', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Знаков после запятой') }}">
                                        <input type="text" class="form-control" style="width: 50px;text-align: center;" name="number_format[{{$item->id}}]" value="{{$item->number_format}}">
                                    </td>
                                @endif
                                @if(in_array('last_updated', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Посл. обновление') }}">
                                        <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                                        @if(isset($item->updated_user))
                                            <small class="text-muted">/ <a href="{{ admin_base_path(sprintf('/account/users?action=filter&id=%s&sorting=id', $item->updated_user->id)) }}">{{$item->updated_user->name}}</a></small>
                                        @endif
                                    </td>
                                @endif
                                @if(in_array('status', $admin_currency_hidden_columns))
                                    <td data-label="{{ __('Статус') }}">
                                        @if($item->status == 0)
                                            <span class="text-success">{{ __('Активно') }}</span>
                                        @else
                                            <span class="text-danger">{{ __('Не активно') }}</span>
                                        @endif
                                    </td>
                                @endif
                                <td>

                                    <input type="hidden" name="item_id[]" value="{{ $item->id }}">

                                    <div layout="row" layout-align="end center">
                                        <md-button title="{{ __('Сделать дубликат валюты') }}" href="currency/{{$item->id}}/duplicate" class="md-icon-button" >
                                            <i class="far fa-copy"></i>
                                        </md-button>

                                        @if($item->direction_exchange_in_count == 0 and $item->direction_exchange_out_count == 0)
                                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/basic/currency/'.$item->id.'/delete')  }}">
                                                <i class="fas fa-trash text-danger"></i>
                                            </md-button>
                                        @else
                                            <md-button class="md-icon-button" disabled>
                                                <i class="fas fa-lock text-warning"></i>
                                            </md-button>
                                        @endif

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('Сохранить') }}</option>
                                <option value="archive">{{ __('Архивировать') }}</option>
                                <option data-divider="true"></option>
                                <option value="activation">{{ __('Включить') }}</option>
                                <option value="deactivation">{{ __('Отключить') }}</option>
                                <option data-divider="true"></option>
                                <option value="trashed">{{ __('В корзину') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для создания валюты -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency') }}">
                <input type="hidden" name="action" value="create">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Добавить валюту') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Платежная система') }}</label>
                            <div class="col-md-6">
                                {!! Form::select('id_payment', $payments, is_filter_search($filter, 'payments'), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Код валюты') }}</label>
                            <div class="col-md-6">
                                {!! Form::select('id_code_currency', $code_currencies, is_filter_search($filter, 'code_currency'), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Создать') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройки модуля валют') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить лог валют') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_currency_enabled_log" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_currency_enabled_log') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_currency_enabled_log') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label for="code" class="col-sm-6 control-label">{{ __('Включить автоматический пересчет заявок') }}</label>
                            <div class="col-sm-6">
                                <div class="material-switch switch-input-1">
                                    <input id="currency_is_recount_default" name="currency_is_recount_default" @if(iEXSetting('currency_is_recount_default') == 1) checked @endif type="checkbox" value="1" />
                                    <label for="currency_is_recount_default" class="label-primary"></label>
                                </div>
                                <div style="margin-top: 5px; font-size: 12px;" class="text-muted">{{ __('Если включено, то в процессе выполнения заявки, курс обмена будет обновлен на актуальный и получаемая клиентом сумма также обновится') }}.</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="control-label col-md-6">{{ __('Пересчитать, если курс изменился > чем') }}</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" name="currency_recount_percent" class="form-control" value="{{ iEXSetting('currency_recount_percent') }}">
                                    <span class="input-group-addon" id="basic-addon3">%</span>
                                </div>

                                <div style="margin-top: 5px; font-size: 12px;" class="text-muted">
                                    {{ __('Пересчитается получаемая сумма клиента при условии если курс изменился больше чем на указанный выше процент') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="control-label col-md-6">{{ __('Пересчитать курс') }}</label>
                            <div class="col-md-6">
                                <input style="margin-bottom: 10px" placeholder="{{ __('Через сколько часов') }}" name="currency_recount_time_hours" type="text" class="form-control" value="{{ iEXSetting('currency_recount_time_hours') }}">

                                <input placeholder="{{ __('Через сколько минут') }}" name="currency_recount_time_minutes" type="text" class="form-control" value="{{ iEXSetting('currency_recount_time_minutes') }}">
                            </div>
                        </div>


                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('В списке валют, скрыть отображение фильтров') }}</label>
                            <div class="col-md-6" id="is_currency_disabled_admin_filter">
                                <select class="selectpicker" name="is_currency_disabled_admin_filter" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_currency_disabled_admin_filter') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_currency_disabled_admin_filter') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Разрешить отмечать популярные валюты') }}</label>
                            <div class="col-md-6" id="is_currencies_fire_angular">
                                <select class="selectpicker" name="is_currencies_fire_angular" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_currencies_fire_angular') == 0) selected @endif>{{ __('Нет') }}</option>
                                    <option value="1"  @if(iEXSetting('is_currencies_fire_angular') == 1) selected @endif>{{ __('Да') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для настроек таблицы -->
    <div class="modal fade" id="settings_columns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings_columns">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка страницы') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Колонки') }}</label>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[currency]" value="currency" type="checkbox" name="admin_currency_hidden_columns[currency]" @if(in_array('currency', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[currency]">{{ __('Валюта') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[code_currency]" value="code_currency" type="checkbox" name="admin_currency_hidden_columns[code_currency]" @if(in_array('code_currency', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[code_currency]">{{ __('Код валюты') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[xml]" value="xml" type="checkbox" name="admin_currency_hidden_columns[xml]" @if(in_array('xml', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[xml]">{{ __('XML') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[merchant]" value="merchant" type="checkbox" name="admin_currency_hidden_columns[merchant]" @if(in_array('merchant', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[merchant]">{{ __('Мерчант') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[payment]" value="payment" type="checkbox" name="admin_currency_hidden_columns[payment]" @if(in_array('payment', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[payment]">{{ __('Выплата') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[reserve]" value="reserve" type="checkbox" name="admin_currency_hidden_columns[reserve]" @if(in_array('reserve', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[reserve]">{{ __('Резерв') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[receiving]" value="receiving" type="checkbox" name="admin_currency_hidden_columns[receiving]" @if(in_array('receiving', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[receiving]">{{ __('Получено') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[sending]" value="sending" type="checkbox" name="admin_currency_hidden_columns[sending]" @if(in_array('sending', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[sending]">{{ __('Отправлено') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[number_format]" value="number_format" type="checkbox" name="admin_currency_hidden_columns[number_format]" @if(in_array('number_format', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[number_format]">{{ __('Кол-во знаков после запятой') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[last_updated]" value="last_updated" type="checkbox" name="admin_currency_hidden_columns[last_updated]" @if(in_array('last_updated', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[last_updated]">{{ __('Посл. обновление') }}</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_currency_hidden_columns[status]" value="status" type="checkbox" name="admin_currency_hidden_columns[status]" @if(in_array('status', $admin_currency_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_currency_hidden_columns[status]">{{ __('Статус') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group">
                            <label for="admin_currency_pagination" class="control-label col-md-6">{{ __('Кол-во записей на странице') }}</label>
                            <div class="col-sm-6">
                                <input type="text" name="admin_currency_pagination" class="form-control" value="{{ iEXSetting('admin_currency_pagination', 20) }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $valuta->appends($filter)->links() !!}
    </div>
@endsection
