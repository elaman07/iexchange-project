@extends('admin.layouts.app')

@section('title', __('Добавить метку'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.labels.create'))


@section('content')


    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency-labels') }}" enctype="multipart/form-data">
        @csrf

        <div class="default-panel-body">


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Добавить метку</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#title-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="title-{{ $form_lang_key }}" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang['field']}}" class="form-control">
                            </div>
                        @endforeach
                    </div>


                    <div class="clearfix"></div>

                </div>
            </div>

            <div class="clearfix"></div>
            <hr />
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Настройка дизайна</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Стиль фона</div>
                        <div id="cp1" class="input-group colorpicker-component">
                            <input type="text" value="{{ old('bg_color') }}" name="bg_color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">Стиль текста</div>
                        <div id="cp2" class="input-group colorpicker-component">
                            <input type="text" value="{{ old('text_color') }}" name="text_color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Иконка') }}</div>
                        <input type="file" name="image" class="form-control" accept="image/*">
                    </div>

                    <div class="clearfix"></div>

                </div>

            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/currency-labels') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
