@extends('admin.layouts.app')

@section('title', __('Метки для валют'))

@section('top-block')
    <md-button ng-href="currency-labels/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить метку') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.labels'))


@section('content')
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Название') }}</th>
                <th>{{ __('Количество валют') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($labels) > 0)
                @foreach($labels as $label)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('/basic/currency-labels/'.$label->id.'/edit') }}">
                                {{ $label->title }}  &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>
                            <a href="{{ admin_base_path('basic/currency?sorting_by=default&filter_currency[]='.$label->id) }}">
                                {{ __('Найдено валют') }}: {{ $label->currencies->count() }}
                            </a>
                        </td>

                        <td style="width: 50px;">
                            {!! Form::open(['id' => 'currency-labels_destroy_'.$label->id, 'method' => 'DELETE', 'route' => ['currency-labels.destroy', $label->id] ]) !!}
                            {!! Form::close() !!}

                            <a href="javascript:void(0)" onclick="document.getElementById('currency-labels_destroy_{{ $label->id }}').submit()">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
@endsection
