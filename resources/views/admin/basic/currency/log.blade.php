@extends('admin.layouts.app')

@section('title', __('admin-basic.log_currency.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.currency.logs'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-basic.log_currency.created_at') }}</th>
            <th>{{ __('admin-basic.log_currency.currency') }}</th>
            <th>{{ __('admin-basic.log_currency.status') }}</th>
            <th>{{ __('admin-basic.log_currency.where_from') }}</th>
            <th>{{ __('admin-basic.log_currency.text') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>{{ $log->currency->payment->name }} {{ $log->currency->code_currency->name }}</td>
                    <td>
                        @if($log->status == 0)
                            <label class="label label-success">{{ __('admin-basic.log_currency.added') }}</label>
                        @elseif($log->status == 1)
                            <label class="label label-info">{{ __('admin-basic.log_currency.update') }}</label>
                        @elseif($log->status == 2)
                            <label class="label label-primary">{{ __('admin-basic.log_currency.duplicate') }}</label>
                        @elseif($log->status == 3)
                            <label class="label label-danger">{{ __('admin-basic.log_currency.delete') }}</label>
                        @endif
                    </td>
                    <td>
                        @if($log->status == 0)
                            {{ __('admin-basic.log_currency.from_currency') }}
                        @elseif($log->status == 1)
                            {{ __('admin-basic.log_currency.from_command') }}
                        @elseif($log->status == 2)
                            {{ __('admin-basic.log_currency.from_notify') }}
                        @elseif($log->status == 3)
                            {{ __('admin-basic.log_currency.from_autopayment') }}
                        @elseif($log->status == 4)
                            {{ __('admin-basic.log_currency.from_direction') }}
                        @elseif($log->status == 5)
                            {{ __('admin-basic.log_currency.from_requisite') }}
                        @endif
                    </td>
                    <td style="width: 30%">
                        {!! $log->text !!}
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    {!! $logs->links() !!}
@endsection