@extends('admin.layouts.app')

@section('title', 'Лог прибыли')

@section('breadcrumbs', Breadcrumbs::render('admin.currency.log_profit'))

@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-basic.log_currency.created_at') }}</th>
            <th>{{ __('admin-basic.log_currency.currency') }}</th>
            <th>{{ __('admin-basic.log_profit.table_order') }}</th>
            <th>{{ __('admin-basic.log_profit.percent_profit') }}</th>
            <th>{{ __('admin-basic.log_profit.amount_profit') }}</th>
            <th>{{ __('admin-basic.log_profit.amount_profit_usd') }}</th>
        </tr>

        </thead>
        <tbody>
        @if(count($logs) > 0)
            @foreach($logs as $log)
                <tr>
                    <td>{{ $log->created_at }}</td>
                    <td>{{ $log->currency->payment->name }} {{ $log->currency->code_currency->name }}</td>
                    <td>
                        @if(isset($log->tasks))
                            <a href="{{ admin_base_path('/applications/details/'.$log->tasks->id.'/?actions=arhive') }}">{{ $log->tasks->id }}</a>
                        @else
                            <small class="text-danger">
                                {{ __('admin-basic.log_profit.empty') }}
                            </small>
                        @endif
                    </td>
                    <td>{{ $log->percent }}%</td>
                    <td>{{ $log->amount }} {{ $log->currency->code_currency->sign }}</td>
                    <td>$ {{ $log->amount_usd }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection

@section('pagination')
    {!! $logs->links() !!}
@endsection
