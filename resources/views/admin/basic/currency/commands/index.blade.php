@extends('admin.layouts.app')

@section('title', __('admin-basic.currency-command.title'))

@section('video__instruction', 'https://www.youtube.com/embed/WsD3RHjZMrs?autoplay=1&amp;mute=0')

@section('top-block')
    <md-button ng-href="currency-command/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-basic.currency-command.add_command') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.command'))


@section('content')
    <form method="post" action="?action=update">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-basic.currency-command.created_at') }}</th>
                <th>{{ __('admin-basic.currency-command.currency') }}</th>
                <th>{{ __('admin-basic.currency-command.position') }}</th>
                <th>{{ __('admin-basic.currency-command.name') }}</th>
                <th>{{ __('admin-basic.currency-command.value') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($groups) > 0)
                @foreach($groups as $group)
                    <tr>
                        <th>{{ $group->created_at }}</th>
                        <td>{{$group->currency->payment->name}} {{$group->currency->code_currency->name}}</td>
                        <td>
                            <input type="text" class="form-control" style="width: 50px;text-align: center;" name="sorting[{{$group->id}}]" value="{{$group->sorting}}">
                        </td>
                        <td>{{$group->name}}</td>
                        <td>{{$group->amount}}</td>
                        <td style="width: 200px;">
                            <div class="col-md-6">
                                <a href="currency-command/{{$group->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-basic.edit') }}</a>
                            </div>

                            <div class="col-md-6">
                                <a href="currency-command/{{$group->id}}/delete" class="btn btn-danger btn-sm">{{ __('admin-basic.delete') }}</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('admin-basic.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $groups->links() !!}
    </div>
@endsection
