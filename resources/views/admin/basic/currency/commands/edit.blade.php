@extends('admin.layouts.app')

@section('title', __('admin-basic.currency-command.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.command.change', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency-command/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="code" class="control-label col-md-2 col-sm-3">{{ __('admin-basic.currency-command.currency') }}</label>
                <div class="col-md-4">
                    <select class="selectpicker" name="id_currency" data-live-search="true">
                        @foreach($currencies as $currency)
                            <option value="{{$currency->id}}" @if($item->id_currency == $currency->id) selected @endif>{{ $currency->payment->name }} {{ $currency->code_currency->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-command.name') }}</label>
                <div class="col-sm-4">
                    <input type="text" name="name" class="form-control" value="{{$item->name}}" required>
                </div>
            </div>


            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-command.value') }}</label>
                <div class="col-sm-2">
                    <input type="text" name="amount" class="form-control" value="{{$item->amount}}" required>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-command.sorting') }}</label>
                <div class="col-sm-2">
                    <input type="text" name="sorting" class="form-control" value="{{$item->sorting}}" required>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/currency-command') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection