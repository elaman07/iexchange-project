@extends('admin.layouts.app')

@section('disable-body-wrapper', ' ')


@section('title', __('Изменить валюту').' '.$info->payment->name.' '. $info->code_currency->name)

@section('breadcrumbs', Breadcrumbs::render('admin.currency.edit', $info))

@section('hide-admin-header')

@stop

@section('custom-body-class', 'custom-admin-body-wrapper')

@section('no-block-content')

    <style>
        .bootstrap-select.form-control {
            background: transparent;
        }

        .currency-exchange-toolbar {
            background: transparent !important;
            padding-top: 2rem;
            min-height: auto;
            border-bottom: 1px solid #e9e9e9e9;
            padding-bottom: 2rem;
            margin: 0;
            font-family: Nunito,sans-serif;
        }

        .currency-exchange-toolbar > h1 {
            display: block;
            font-weight: 600;
            height: auto;
        }
    </style>


    @if($info->is_archive == 1)
        <div class="alert alert-danger text-danger">
            {{ __('Выбранная вами валюта находится в архиве и недоступна к использованию') }}.
            [<a href="?actions=return_archive">{{ __('Вернуть из архива') }}</a>]
        </div>
    @endif
    <div layout="row" flex ng-controller="SettingsSidenav as ctrl" class="setting-sidenav-bg">
        <md-sidenav flex="25" class="md-sidenav-left setting-sidenav" md-component-id="settings" md-is-locked-open="$mdMedia('gt-md')" md-whiteframe="4">

            <md-toolbar class="currency-exchange-toolbar">
                <h1 class="md-toolbar-tools" style="color: #000">{{ __('Изменить валюту') }} "{{ $info->payment->name }} {{ $info->code_currency->name }}"</h1>

            </md-toolbar>
            <md-content>
                <ul class="setting-sidenav-menu" style="padding: 0" id="directionTab">
                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#main" role="tab" data-toggle="tab" data-tab-title="{{ __('Основные настройки') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Основные настройки') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#reserve" role="tab" data-toggle="tab" data-tab-title="{{ __('Резервы и лимиты') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Резервы и лимиты') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#merchant_autopayment" role="tab" data-toggle="tab" data-tab-title="{{ __('Мерчанты и выплаты') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Мерчанты и выплаты') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#fields" role="tab" data-toggle="tab" data-tab-title="{{ __('Настройка полей') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Настройка полей') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#aml_analyses" role="tab" data-toggle="tab" data-tab-title="{{ __('AML анализ') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('AML анализ') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#fee" role="tab" data-toggle="tab" data-tab-title="{{ __('Комиссии платежных систем') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Комиссии платежных систем') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#verification" role="tab" data-toggle="tab" data-tab-title="{{ __('Верификация') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Верификация') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#currency_requisites" data-tab-title="{{ __('Реквизиты') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title"> {{ __('Реквизиты') }} </div>

                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#recount" role="tab" data-toggle="tab" data-tab-title="{{ __('Пересчет заявок') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Пересчет заявок') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#partners" role="tab" data-toggle="tab" data-tab-title="{{ __('Партнерская программа') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Партнерская программа') }}</div>
                            </div>
                        </a>
                    </li>

                    <li class="setting-sidenav-menu-item">
                        <a layout="row" href="#other" role="tab" data-toggle="tab" data-tab-title="{{ __('Прочие опции') }}" data-custom-tab="tab" role="tab" data-toggle="tab">
                            <div class="sss tip">
                                <div class="setting-sidenav-menu-item-title">{{ __('Прочие опции') }}</div>
                            </div>
                        </a>
                    </li>
                </ul>
            </md-content>
        </md-sidenav>
        <!-- Данные -->
        <md-content class="setting-sidenav-wrapper">
            <div class="setting-sidenav-bodywrap">
                <div class="setting-sidenav-title" style="border: none;margin-top: 5px;" layout="row">
                    <md-button ng-click="toggleSettings()" class="md-primary" hide-gt-md>
                        <i class="fa fa-fw fa-bars"></i>
                    </md-button>
                    <h2></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="x_content mt-8">
                    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency/'.$info->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="tab-content">


                            <div class="tab-pane row active" id="main">

                                <!-- Статус валюты -->
                                <div class="form-group">
                                    <label for="status" class="col-sm-2 control-label">{{ __('Статус') }}</label>
                                    <div class="col-sm-4">
                                        <select class="form-control selectpicker" name="status">
                                            <option value="0" @if($info->status == 0) selected @endif>{{ __('Активная валюта') }}</option>
                                            <option value="1" @if($info->status == 1) selected @endif>{{ __('Не активная валюта') }}</option>
                                            <option value="2" @if($info->status == 2) selected @endif>{{ __('Архивная валюта') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- new line -->
                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Валюта') }}</label>
                                    <div class="col-sm-10">
                                        <!-- Валюта -->
                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Выберите ПС') }}</div>
                                            {{ Form::select('id_payment', $payments, $info->id_payment, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '8']) }}
                                        </div>

                                        <!-- Код валюты -->
                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Код валюты') }}</div>
                                            {{ Form::select('id_code_currency', $code_currencies, $info->id_code_currency, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '8']) }}
                                            <div class="form-check">
                                                <input class="form-check-input" id="checkbox1" value="0" type="checkbox" name="visible_code_currency" @if($info->visible_code_currency == 0) checked @endif>
                                                <label class="form-check-label" for="checkbox1">{{ __('Отображать код валюты') }}</label>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <!-- Группа -->
                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Группа') }}</div>
                                            {{ Form::select('id_group', $groups, $info->id_group, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true']) }}
                                        </div>



                                        <div class="clearfix"></div>
                                        <hr />

                                        <!-- Обозначение для XML (Вручную) -->
                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Обозначение для XML (Вручную)') }}</div>
                                            <input type="text" name="designation_xml" class="form-control" value="{{$info['designation_xml']}}">
                                        </div>

                                        <!-- Обозначение для XML (Автоматически) -->
                                        <div class="form-group col-sm-6">
                                            <div class="control-label-br">{{ __('Обозначение для XML (Автоматически)') }}</div>
                                            <select name="designation_xml_select" class="form-control selectpicker"  data-live-search="true" data-size="8">
                                                <option value="">-- {{ __('Не выбрано') }} --</option>
                                                @foreach($bestchange_codes as $value)
                                                    <option value="{{$value['code']}}" @if($info->designation_xml == $value['code']) selected @endif>{{$value['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Знаков после запятой (для сайта)') }}</div>
                                            <input type="number" name="number_format" class="form-control"  value="{{ (int)$info->number_format }}" required>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Знаков после запятой (для XML)') }}</div>
                                            <input type="number" name="number_format_xml" class="form-control"  value="{{ (int)$info->number_format_xml }}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Конвертировать по') }}</div>
                                            <input type="number" name="convert_by" class="form-control" value="{{$info->convert_by}}">
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Разрешить разделять сумму пробелом') }}</div>
                                            <div class="material-switch switch-input-1">
                                                <input id="is_allow_amount_space" name="is_allow_amount_space" type="checkbox" @if($info->is_allow_amount_space == 1) checked @endif value="1" />
                                                <label for="is_allow_amount_space" class="label-primary"></label>
                                            </div>

                                            <div class="input-p-text">
                                                {{ __('После включения данной функции, суммы в заявка будут иметь такой формат: 1 000, 10 000') }}
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <hr />

                                        <!-- Фильтр -->
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Фильтр') }}</div>
                                            {{ Form::select('id_filter_currency', $filter_currencies, $info->id_filter_currency, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => 4]) }}
                                        </div>

                                        <!-- Метки -->
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Метка') }}</div>
                                            <select class="form-control selectpicker" name="id_label" data-live-search="true", data-size="4">
                                                <option value="0">-- {{ __('Не выбран') }} --</option>
                                                @foreach($labels as $label_key => $label_value)
                                                    <option value="{{ $label_key }}" @if($label_key == $info->id_label) selected @endif>{{ $label_value }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />


                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Технический заголовок (не обязательно)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#tech_currency_name-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="tech_currency_name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" name="tech_currency_name{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('tech_currency_name', $form_lang_key) }}" id="tech_currency_name{{  $form_lang['field'] }}">
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>

                            <div class="tab-pane row" id="reserve">

                                <div class="form-group">
                                    <label for="reserve_title" class="col-sm-2 control-label">{{ __('Резервы') }}</label>
                                    <div class="col-sm-10">

                                        <!-- Ограничить слив валюты -->
                                        <div class="form-group row-no-gutters col-sm-6">
                                            <div class="control-label-br">{{ __('Максимальная сумма резерва (Отдаю)') }}</div>
                                            <input type="text" name="max_limit_in_reserve" class="form-control" value="{{(!empty($info->max_limit_in_reserve) ? $info->max_limit_in_reserve : 0)}}">
                                            <div style="margin-top: 5px; font-size: 12px;" class="text-muted">
                                                {{ __('Эта функция ограничиваем пополнение резерва одной валюты больше чем указано') }}.
                                            </div>
                                        </div>

                                        <!-- Макс. отображаемое значение резерва валюты -->
                                        <div class="form-group row-no-gutters col-sm-6">
                                            <div class="control-label-br">{{ __('Макс. отображаемое значение резерва валюты') }}</div>
                                            <input type="text" name="max_display_reserve" class="form-control" value="{{ $info->max_display_reserve }}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <br />

                                        <!-- Не учитывать процент в резерве -->
                                        <div class="form-group row-no-gutters col-sm-6">
                                            <div class="control-label-br">{{ __('Доп. процент который будет сниматься с резерва') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="profit_percent_reserve" class="form-control" value="{{ $info->profit_percent_reserve }}">
                                                <span class="input-group-addon" id="basic-addon3">%</span>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label for="reserve_title" class="col-sm-2 control-label">{{ __('Лимиты') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Кол-во заявок со статусом Ожидается оплата (за час)') }}</div>
                                            <input type="text" name="hour_limit_order_pending" class="form-control" value="{{ $info->hour_limit_order_pending }}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Кол-во заявок со статусом В процессе оплаты (за час)') }}</div>
                                            <input type="text" name="hour_limit_order_process" class="form-control" value="{{ $info->hour_limit_order_process }}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />


                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Дневной лимит для Отдаю') }}</div>
                                            <input type="text" name="day_limit_give" class="form-control" value="{{ $info->day_limit_give }}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Дневной лимит для Получаю') }}</div>
                                            <input type="text" name="day_limit_receive" class="form-control" value="{{ $info->day_limit_receive }}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Месячный лимит для Отдаю') }}</div>
                                            <input type="text" name="month_limit_in" class="form-control" value="{{ $info->month_limit_in }}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Месячный лимит для Получаю') }}</div>
                                            <input type="text" name="month_limit_out" class="form-control" value="{{ $info->month_limit_out }}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane row" id="merchant_autopayment">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Мерчант') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Список мерчантов') }}</div>
                                            {{ Form::select('ids_merchant[]', $merchants, $info->merchants, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => '8']) }}
                                        </div>

                                        <div class="clearfix"></div>


                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Название сети для мерчанта (не обязательно)') }}</div>
                                            <input type="text" name="network_code" class="form-control" value="{{ $info->network_code }}">
                                            <div class="input-p-text">{{ __('Рекомендуем использовать для мерчантов - со сложной механикой получения валют (Если незнаете как работает данная функция, обратитесь к администратору)') }}.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Выплата') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Список выплат') }}</div>
                                            <select data-live-search="true" class="form-control selectpicker" name="id_pay"  data-size="8">
                                                <option value="0">--  {{ __('Не выбрано') }} --</option>
                                                @foreach($pays as $pay)
                                                    <option value="{{$pay->id}}" @if($info->id_pay == $pay->id) selected @endif>{{$pay->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Название сети для выплат (не обязательно)') }}</div>
                                            <input type="text" name="network_code_out" class="form-control" value="{{ $info->network_code_out }}">
                                            <div class="input-p-text">{{ __('Рекомендуем использовать для выплат - со сложной механикой получения валют (Если незнаете как работает данная функция, обратитесь к администратору)') }}.</div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Мин. сумма автовыплаты') }}</div>
                                            <input type="text" class="form-control" name="out_pay_min_amount" value="{{ $info->out_pay_min_amount }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Макс. сумма автовыплаты') }}</div>
                                            <input type="text" class="form-control" name="out_pay_max_amount" value="{{ $info->out_pay_max_amount }}">
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Суточный лимит для автовыплаты') }}</div>
                                            <input type="text" class="form-control" name="out_pay_day_limit" value="{{ $info->out_pay_day_limit }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Месячный лимит для автовыплаты') }}</div>
                                            <input type="text" class="form-control" name="out_pay_month_limit" value="{{ $info->out_pay_month_limit }}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Автовыплата в случае если у заявки статус "Оплаченная заявка"') }}</div>
                                            <select class="selectpicker form-control" name="auto_pay_order_pay">
                                                <option value="0" @if($info->auto_pay_order_pay == 0) selected @endif>{{ __('По умолчанию') }}</option>
                                                <option value="1" @if($info->hold_delay == 1) selected @endif>{{ __('Да') }}</option>
                                                <option value="2" @if($info->auto_pay_order_pay == 2) selected @endif>{{ __('Нет') }}</option>
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить задержку автовыплаты') }}</div>
                                            <div class="material-switch switch-input-1">
                                                <input id="is_hold" name="is_hold" type="checkbox" @if($info->is_hold == 1) checked @endif value="1" />
                                                <label for="is_hold" class="label-primary"></label>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Задержка автовыплаты в часах') }}</div>
                                            <input type="text" name="hold_in_hours" class="form-control" value="{{(!empty($info->hold_in_hours) ? $info->hold_in_hours : 0)}}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Задержка для') }}</div>
                                            <select class="selectpicker form-control" name="hold_delay">
                                                <option value="0" @if($info->hold_delay == 0) selected @endif>{{ __('Новых счетов') }}</option>
                                                <option value="1" @if($info->hold_delay == 1) selected @endif>{{ __('Всех') }}</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="tab-pane row" id="fields">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Дополнительные поля') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Для Отдаю') }}</div>
                                            {{ Form::select('in_custom_fields[]', $in_custom_fields, $info->currency_in_fields, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Для Получаю') }}</div>
                                            {{ Form::select('out_custom_fields[]', $out_custom_fields, $info->currency_out_fields, ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Настройка полей') }}</label>
                                    <div class="col-sm-10">

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Название поле для Номера счета') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#account_number_field-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="account_number_field-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" placeholder="{{ __('admin-basic.currency.account_number_field_'.$form_lang_key) }}" name="account_number_field{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('account_number_field', $form_lang_key) }}" id="account_number_field{{  $form_lang['field'] }}">
                                                    <div class="input-p-text">{{ __('Название отображается в окне оплаты') }}.</div>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Комментарий для поля "Номера счета"') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#account_number_field_text-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="account_number_field_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea type="text" name="account_number_field_text{{$form_lang['field']}}" class="form-control" rows="3"
                                                              id="account_number_field_text{{$form_lang['field']}}">{{ $info->getTranslation('account_number_field_text', $form_lang_key) }}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Скрыть номер счета для Отдаю') }}</div>
                                            <select class="form-control selectpicker" name="visible_give">
                                                <option value="0" @if($info->visible_give == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->visible_give == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Скрыть номер счета для Получаю') }}</div>
                                            <select class="form-control selectpicker" name="visible_receiving">
                                                <option value="0" @if($info->visible_receiving == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->visible_receiving == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Первый символ') }}</div>
                                            <input type="text" name="first_value" class="form-control" value="{{  $info->first_value }}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Мин. кол-во символов') }}</div>
                                            <input  type="number" name="min_char" class="form-control" value="{{$info->min_char}}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Макс. кол-во символов') }}</div>
                                            <input type="number" name="max_char" class="form-control" value="{{$info->max_char}}">
                                            <div class="input-p-text">
                                                <b>0</b> - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-12 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Текст ошибки для полей (со счета/на счет)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#min_max_error_message-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="min_max_error_message-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" name="min_max_error_message{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('min_max_error_message', $form_lang_key) }}" id="min_max_error_message{{$form_lang['field']}}">
                                                </div>
                                            @endforeach

                                            <p class="input-p-text">{{ __('Укажите текст сообщения об ошибки, в случае если номер счета не указан или не соответствует установленным мин./макс. символам') }}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Название поля со счета') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#field_name_from-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="field_name_from-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" placeholder="{{ __('Укажите названия поля для реквизитов') }}" name="field_name_from{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('field_name_from', $form_lang_key) }}" id="field_name_from{{$form_lang['field']}}">
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Название поля на счет') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#field_name_to-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="field_name_to-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" placeholder="{{ __('Укажите названия поля для реквизитов') }}" name="field_name_to{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('field_name_to', $form_lang_key) }}" id="field_name_to{{$form_lang['field']}}">
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Комментарий для поля со счета') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#field_comment_from-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="field_comment_from-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="field_comment_from{{$form_lang['field']}}" name="field_comment_from{{$form_lang['field']}}" class="form-control">{{ $info->getTranslation('field_comment_from', $form_lang_key) }}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Комментарий для поля на счет') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#field_comment_to-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="field_comment_to-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="field_comment_to{{$form_lang['field']}}" name="field_comment_to{{$form_lang['field']}}" class="form-control">{{ $info->getTranslation('field_comment_to', $form_lang_key) }}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Маска поля') }}</div>
                                            <input type="text" name="mask_input" class="form-control" value="{{$info->mask_input}}">
                                            <div class="input-p-text">{{ __('Формат вводимых номеров (Пример счета СберОнлайн: 0000 0000 0000 0000 00)') }}</div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <hr />


                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Разрешенные символы') }}</div>
                                            <select class="form-control selectpicker" name="allowed_char">
                                                <option value="0" @if($info->allowed_char == 0) selected @endif>{{ __('Любые символы') }}</option>
                                                <option value="1" @if($info->allowed_char == 1) selected @endif>{{ __('Цифры') }}</option>
                                                <option value="2" @if($info->allowed_char == 2) selected @endif>{{ __('Буквы') }}</option>
                                                <option value="3" @if($info->allowed_char == 3) selected @endif>{{ __('Латинские буквы и цифры') }}</option>
                                                <option value="4" @if($info->allowed_char == 4) selected @endif>{{ __('Латинские буквы') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Удалять пробелы в реквизитах') }}</div>
                                            <select class="form-control selectpicker" name="remove_spaces_requisite">
                                                <option value="0" @if($info->remove_spaces_requisite == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->remove_spaces_requisite == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>


                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Валидатор номера счета') }}</div>
                                            <select class="form-control selectpicker" name="is_valid_account" data-live-search="true" data-size="10">
                                                <option value="">-- {{ __('Не выбрано') }} --</option>
                                                @foreach($validators as $key => $value)
                                                    <option value="{{$key}}" @if($info['is_valid_account'] == $key) selected @endif>{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Текст ошибки для валидатора') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#valid_account_error-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="valid_account_error-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" name="valid_account_error{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('valid_account_error', $form_lang_key) }}" id="valid_account_error{{$form_lang['field']}}">
                                                </div>
                                            @endforeach

                                            <p class="input-p-text">Укажите текст сообщения об ошибки, если клиент указал неправильный номер счета</p>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane row" id="aml_analyses">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Источник по умолчанию') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">{{ __('Выберите сервис для AML проверок') }}</div>
                                            <select class="form-control selectpicker" name="aml_service">
                                                <option value="" @if($info->aml_service == null) selected @endif>-- {{ __('Не выбрано') }} --</option>

                                                @foreach($aml_services as $aml)
                                                    <option value="{{ $aml->aml_name }}" @if($info->aml_service == $aml->aml_name) selected @endif>{{ $aml->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Конфигурация</label>
                                    <div class="col-sm-10">

                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">Проверка счета Отдаю</div>
                                            <select class="form-control selectpicker" name="is_in_aml_check_wallet">
                                                <option value="0" @if($info->is_in_aml_check_wallet == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_in_aml_check_wallet == 1) selected @endif>{{ __('Да') }}</option>
                                                <option value="2" @if($info->is_in_aml_check_wallet == 2) selected @endif>{{ __('Да и запретить создать заявку') }}</option>
                                            </select>
                                        </div>


                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">Проверка счета Получаю</div>
                                            <select class="form-control selectpicker" name="is_out_aml_check_wallet">
                                                <option value="0" @if($info->is_out_aml_check_wallet == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_out_aml_check_wallet == 1) selected @endif>{{ __('Да') }}</option>
                                                <option value="2" @if($info->is_out_aml_check_wallet == 2) selected @endif>{{ __('Да и запретить создать заявку') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6 row-no-gutters"></div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">{{ __('Проверка Транзакции Отдаю') }}</div>
                                            <select class="form-control selectpicker" name="is_in_aml_check_tx">
                                                <option value="0" @if($info->is_in_aml_check_tx == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_in_aml_check_tx == 1) selected @endif>{{ __('Да') }}</option>
                                                <option value="2" @if($info->is_in_aml_check_tx == 2) selected @endif>{{ __('Да и запретить выплату заявки') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">{{ __('Сумма обменов') }} "{{ __('От') }}"</div>
                                            <input type="text" name="in_aml_tx_amount" class="form-control" value="{{ $info->in_aml_tx_amount  ?? 0 }}">
                                            <div class="input-p-text">
                                                0 - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    </div>
                                </div>



                                <div class="clearfix"></div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Настройки') }}</label>
                                    <div class="col-sm-10">

                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">{{ __('Стоимость каждой проверки (в USD)') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="aml_check_cost" class="form-control" value="{{ $info->aml_check_cost }}">
                                                <span class="input-group-addon" id="basic-addon2">$</span>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" id="is_aml_check_cost_reserve" value="0" @if($info->is_aml_check_cost_reserve == 1) checked @endif type="checkbox" name="is_aml_check_cost_reserve">
                                                <label class="form-check-label" for="is_aml_check_cost_reserve">{{ __('Учитывать в резерве') }}</label>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6 row-no-gutters">
                                            <div class="control-label-br">{{ __('Дневной лимит проверок') }}</div>
                                            <input type="number" name="aml_day_limit_count" class="form-control" value="{{ $info->aml_day_limit_count }}">
                                            <div class="input-p-text">
                                                0 - {{ __('отключает ограничение') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>


                                <div class="clearix"></div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Прочие') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить уведомление об AML Проверки') }}</div>
                                            <div class="material-switch switch-input-1">
                                                <input id="kyc_enabled" name="kyc_enabled" @if($info->kyc_enabled == 1) checked @endif type="checkbox" value="1" />
                                                <label for="kyc_enabled" class="label-primary"></label>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить галочку о соглашении с условиями AML / CTF & KYC') }}</div>
                                            <div class="material-switch switch-input-1">
                                                <input id="is_kyc_checkbox" name="is_kyc_checkbox" @if($info->is_kyc_checkbox == 1) checked @endif type="checkbox" value="1" />
                                                <label for="is_kyc_checkbox" class="label-primary"></label>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('AML текст перед оплатой (для отдаю)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#aml_text_in-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="aml_text_in-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-aml_text_in-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-aml_text_in-{{ $form_lang_key }}" name="aml_text_in{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('aml_text_in', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('AML текст перед оплатой (для получаю)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#aml_text_out-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="aml_text_out-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-aml_text_out-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-aml_text_out-{{ $form_lang_key }}" name="aml_text_out{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('aml_text_out', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane row" id="fee">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Комиссии платежных систем (Для мерчанта)') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('В процентах') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="commission_merchant_percent" class="form-control" value="{{ $info->commission_merchant_percent }}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('В валюте') }}</div>
                                            <input type="text" name="commission_merchant_currency" class="form-control" value="{{ $info->commission_merchant_currency }}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <br />
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Для неверифицированного счета') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="fee_no_verified_merchant" class="form-control" value="{{ $info->fee_no_verified_merchant }}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>

                                            <div class="input-p-text">
                                                {{ __('Параметр, исключительно для информационных целей') }}.
                                                P.S: {{ __('Используется для Perfect Money') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Комиссии для Получаю') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('В валюте') }}</div>
                                            <input type="number" name="commission_payment_currency" class="form-control" value="{{ $info->commission_payment_currency }}">
                                            <div class="input-p-text">
                                                {{ __('Укажите сумму комиссии платежной системы при выплате. (Пример: QIWI при выплате на банковские карты, снимает доп. комиссию в валюте)') }}<br />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Комиссии для Резерва (Получаю)') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Комиссия от перевода (%)') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="transfer_percent_reserve" class="form-control" value="{{$info->transfer_percent_reserve}}">
                                                <span class="input-group-addon" id="basic-addon2">%</span>
                                            </div>

                                            <div class="input-p-text">{{ __('Комиссия которая будет учитываться в резерва. (Пример: Сбербанк берет 1% комиссии от переводов.)') }} <br /> {{ __('Это необходимо для корректной работы резервов') }}</div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Комиссия от перевода (в валюте)') }}</div>
                                            <input type="text" name="transfer_amount_reserve" class="form-control" value="{{$info->transfer_amount_reserve}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane row" id="verification">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Настройка верификации') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить верификацию счета Отдаю') }}</div>
                                            <select name="is_enabled_verification" class="form-control selectpicker">
                                                <option value="0" @if($info->is_enabled_verification == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_enabled_verification == 1) selected @endif>{{ __('Да') }}</option>
                                                <option value="2" @if($info->is_enabled_verification == 2) selected @endif>{{ __('Да, если мин. сумма Отдаю больше чем') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Мин. сумма обмена для Отдаю') }}</div>
                                            <input type="text" name="min_amount_verification" class="form-control" value="{{ $info->min_amount_verification }}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <br />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить верификацию счета Получаю') }}</div>
                                            <select name="is_out_enabled_verification" class="form-control selectpicker">
                                                <option value="0" @if($info->is_out_enabled_verification == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_out_enabled_verification == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Мин. сумма обмена для Получаю') }}</div>
                                            <input type="text" name="min_out_amount_verification" class="form-control" value="{{$info->min_out_amount_verification}}">
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-12 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Тест верификации карт') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#verification_text-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="verification_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea class="form-control" name="verification_text{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('verification_text', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-12 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Информация для верификации карт') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#verification_info-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="verification_info-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-verification_info-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-verification_info-{{ $form_lang_key }}" name="verification_info{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('verification_info', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />


                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить верификацию только для выбранных клиентов') }}</div>
                                            <div class="material-switch switch-input-1">
                                                <input id="is_user_verification" name="is_user_verification" type="checkbox" value="1" @if($info->is_user_verification == 1) checked @endif />
                                                <label for="is_user_verification" class="label-primary"></label>
                                            </div>

                                            <div class="input-p-text">{{ __('currency_verification_comment_client') }}</div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <hr />


                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Разрешить верификацию номера счета в личном кабинете') }}</div>
                                            <select class="form-control selectpicker" name="is_verified_cabinet">
                                                <option value="0" @if($info->is_verified_cabinet == 0) selected @endif>{{ __('Да') }}</option>
                                                <option value="1" @if($info->is_verified_cabinet == 1) selected @endif>{{ __('Нет') }}</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="currency_requisites">
                                <div class="form-group">
                                    <label for="profit" class="col-sm-2 control-label">{{ __('Настройка реквизитов') }}</label>
                                    <div class="col-sm-10">

                                        <div class="form-group col-lg-6">
                                            <div class="control-label-br">{{ __('Тип вывода реквизитов') }}</div>

                                            <select class="form-control selectpicker" name="type_output_requisites">
                                                <option value="0" @if($info->type_output_requisites == 0) selected @endif>{{ __('По умолчанию') }}</option>
                                                <option value="1" @if($info->type_output_requisites == 1) selected @endif>{{ __('Рандомно') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>


                            <div class="tab-pane row" id="recount">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Статус пересчета') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Активировать индивидуальный пересчет заявок') }}</div>
                                            <select class="form-control selectpicker" name="is_unique_recount_order" >
                                                <option value="0" @if($info->is_unique_recount_order == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_unique_recount_order == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6"></div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Настройка') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить автоматический пересчет заявок') }}</div>
                                            <select class="form-control selectpicker" name="is_enable_auto_recount_order">
                                                <option value="0" @if($info->is_enable_auto_recount_order == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_enable_auto_recount_order == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                            <div class="input-p-text">
                                                {{ __('Если включено, то в процессе выполнения заявки, курс обмена будет обновлен на актуальный и получаемая клиентом сумма также обновится') }}.
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6"></div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Пересчитать, если курс изменился > чем') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="unique_recount_percent" class="form-control" value="{{ $info->unique_recount_percent }}">
                                                <span class="input-group-addon" id="basic-addon3">%</span>
                                            </div>

                                            <div class="input-p-text">
                                                {{ __('Пересчитается получаемая сумма клиента при условии если курс изменился больше чем на указанный выше процент') }}
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Пересчитать курс') }}</div>
                                            <input style="margin-bottom: 10px" placeholder="{{ __('Через сколько часов') }}" name="recount_time_hours" type="text" class="form-control" value="{{ $info->recount_time_hours }}">
                                            <div style="margin-top: 10px; margin-bottom: 10px; font-size: 12px;" class="text-muted">{{ __('Укажите кол-во часов, по истечении которого курс будет обновлен') }}</div>
                                            <input placeholder="{{ __('Через сколько минут') }}" name="recount_time_minutes" type="text" class="form-control" value="{{ $info->recount_time_minutes }}">
                                            <div style="margin-top: 10px; margin-bottom: 10px; font-size: 12px;" class="text-muted">{{ __('Укажите кол-во минут, по истечении которого курс будет обновлен') }}</div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Текст внизу курса обмена (на главной)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#recount_course_text-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="recount_course_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea class="form-control" name="recount_course_text{{$form_lang['field']}}">{!! $info->getTranslation('recount_course_text', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane row" id="partners">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Использовать данную валюту для выплат партнерских средств') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Для всех партнеров') }}</div>
                                            <select class="form-control selectpicker" name="is_payment_default">
                                                <option value="0" @if($info->is_payment_default == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_payment_default == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Для индивидуальных партнеров') }}</div>
                                            <select class="form-control selectpicker" name="is_payment_unique">
                                                <option value="0" @if($info->is_payment_unique == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_payment_unique == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>

                                            <div class="input-p-text">
                                                {!! __('currency_partner_individual_text') !!}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Комиссия ПС при выплате (В процентах)') }}</div>
                                            <div class="input-group">
                                                <input type="text" name="payout_commission" class="form-control" value="{{$info->payout_commission}}">
                                                <span class="input-group-addon" id="basic-addon3">%</span>
                                            </div>

                                            <p class="input-p-text">{{ __('Укажите дополнительную комиссию платежной системы при выплате средств партнеру') }}</p>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Комиссия ПС при выплате (В валюте)') }}</div>
                                            <input type="text" name="payout_commission_amount" class="form-control" value="{{$info->payout_commission_amount}}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane row" id="other">


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('QR Код') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('QRCode') }}</div>
                                            <select name="is_qrcode" class="form-control selectpicker">
                                                <option value="0" @if($info->is_qrcode == 0) selected @endif>{{ __('Отключить') }}</option>
                                                <option value="1" @if($info->is_qrcode == 1) selected @endif>{{ __('Включить') }}</option>
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Префикс для QRCode') }}</div>
                                            <input type="text" class="form-control" id="prefix_qrcode" value="{{$info->prefix_qrcode}}" name="prefix_qrcode" placeholder="{{ __('admin-basic.currency.prefix_qrcode_placeholder') }}">
                                            <div class="input-p-text">{{ __('Пример bitcoin или bitcoincash') }}</div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('QRCode Amount') }}</div>
                                            <select name="is_qrcode_amount" class="form-control selectpicker">
                                                <option value="0" @if($info->is_qrcode_amount == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_qrcode_amount == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <hr />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ __('Прочие настройки') }}</label>
                                    <div class="col-sm-10">

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Разрешить прием заявок?') }}</div>
                                            <select class="form-control selectpicker" name="is_allow_order">
                                                <option value="0" @if($info->is_allow_order == 0) selected @endif>{{ __('Да') }}</option>
                                                <option value="1" @if($info->is_allow_order == 1) selected @endif>{{ __('Нет') }}</option>
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить отображение этапов в карточке заявок?') }}</div>
                                            <select class="form-control selectpicker" name="is_enabled_step_order">
                                                <option value="0" @if($info->is_enabled_step_order == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_enabled_step_order == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>


                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Привязать чек к заявке?') }}</div>
                                            <select class="form-control selectpicker" name="is_allow_file">
                                                <option value="0" @if($info->is_allow_file == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_allow_file == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>


                                        <div class="clearfix"></div>
                                        <hr />
                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить Загруженость сети Blockchain') }}</div>
                                            <select class="form-control selectpicker" name="blockchain_network_congestion">
                                                <option value="0" @if($info->blockchain_network_congestion == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->blockchain_network_congestion == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('E-mail верификация') }}</div>
                                            <select class="form-control selectpicker" name="is_email_verification_modal">
                                                <option value="0" @if($info->is_email_verification_modal == 0) selected @endif>{{ __('Нет') }}</option>
                                                <option value="1" @if($info->is_email_verification_modal == 1) selected @endif>{{ __('Да') }}</option>
                                            </select>

                                            <div style="font-size: 12px; margin-top: 5px;" class="text-muted">
                                                {{ __('При включении данной функции, клиент обязательно должен верифицировать e-mail адрес в окне оплаты') }}
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-12 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Инструкция к оплате') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#instruction_exchange-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="instruction_exchange-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-instruction_exchange-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-instruction_exchange-{{ $form_lang_key }}" name="instruction_exchange{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('instruction_exchange', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach

                                            <div class="input-p-text">
                                                <b>{{ __('Доступные тэги') }}:</b><br />
                                                <ul>
                                                    <ul>
                                                        <li>[city] - город</li>
                                                        <li>[country] - страна</li>
                                                        <li>[direction] - направление</li>
                                                        <li>[created_at] - дата создания заявки</li>
                                                        <li>[rate] - курс обмена</li>
                                                        <li>[in_amount] - сумма отдаю</li>
                                                        <li>[out_amount] - сумма получаю</li>
                                                        <li>[in_code] - код отдаю</li>
                                                        <li>[out_code] - код получаю</li>
                                                        <li>[in_currency] - валюта отдаю</li>
                                                        <li>[out_currency] - валюта получаю</li>
                                                        <li>[public_id] - Public ID</li>
                                                        <li>[order_id] - ID Заявки</li>
                                                        <li>[profit_percent] - Процент прибыли</li>
                                                        <li>[account] - {{ __('номер счета для оплаты') }}</li>
                                                        <li>[to_account] - {{ __('номер счета клиента (получаю)') }}</li>
                                                    </ul>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-12 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Описание обмена') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#desc_exchange-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="desc_exchange-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-multieditor-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-multieditor-{{ $form_lang_key }}" name="desc_exchange{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('desc_exchange', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>


                                        <div class="form-group col-md-12 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Формальное описание перед вводом данных') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#formalization_text-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="formalization_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-formalization_text-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-formalization_text-{{ $form_lang_key }}" name="formalization_text{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('formalization_text', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Дополнительный текст в процессе оплаты (внизу) (Для отдаю)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#other_docs_in-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="other_docs_in-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-other_docs_in-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-other_docs_in-{{ $form_lang_key }}" name="other_docs_in{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('other_docs_in', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Дополнительный текст в процессе оплаты (внизу) (Для получаю)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#other_docs_out-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="other_docs_out-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea id="ckedtor-other_docs_out-{{ $form_lang_key }}" class="ckeditor-multieditor-for form-control ckedtor-other_docs_out-{{ $form_lang_key }}" name="other_docs_out{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('other_docs_out', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Стиль валюты') }}</div>
                                            <div id="cp2" class="input-group colorpicker-component">
                                                <input type="text" value="{{ $info->text_color }}" name="text_color" class="form-control" />
                                                <span class="input-group-addon"><i></i></span>
                                            </div>


                                            @if(!empty(iEXSetting('color_style_hash')) and !is_numeric(iEXSetting('color_style_hash')))
                                                <div class="input-p-text">
                                                    {{ __('Посл. обновление') }}: {{ \Illuminate\Support\Carbon::parse(iEXSetting('color_style_hash'))->diffForHumans() }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="clearfix"></div>
                                        <hr />


                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Уведомление (Для отдаю)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#notice_in-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="notice_in-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea class="form-control" name="notice_in{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('notice_in', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>


                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Уведомление (Для получаю)') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#notice_out-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="notice_out-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea class="form-control" name="notice_out{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('notice_out', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="form-group col-md-6">
                                            <div class="control-label-br">{{ __('Включить баннер Принимаем банковские карты') }}</div>
                                            <div class="material-switch switch-input-1">
                                                <input id="is_income_banner" name="is_income_banner" @if($info->is_income_banner == 1) checked @endif type="checkbox" value="1" />
                                                <label for="is_income_banner" class="label-primary"></label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr />

                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Кастомное название кнопки Перейти к оплате') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#button_create_order-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="button_create_order-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <input type="text" name="button_create_order{{$form_lang['field']}}" class="form-control" value="{{ $info->getTranslation('button_create_order', $form_lang_key) }}" id="button_create_order{{  $form_lang['field'] }}">
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group col-md-6 multi-language-form-group">
                                            <div class="control-label-br">
                                                <div layout="row">
                                                    <div>{{ __('Описание под кнопкой Перейти к оплате') }}</div>
                                                    <span flex></span>
                                                    <ul class="nav nav-pills lang-tabs">
                                                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                            <li @if($form_lang['active'] == true) class="active" @endif>
                                                                <a class="lang-tabs-link" data-toggle="tab" href="#button_create_order_text-{{ $form_lang_key }}">
                                                                    {{ $form_lang_key }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                                <div id="button_create_order_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                                    <textarea class="form-control" name="button_create_order_text{{$form_lang['field']}}" rows="5">{!! $info->getTranslation('button_create_order_text', $form_lang_key) !!}</textarea>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
                            <md-button ng-href="{{ admin_base_path('/basic/currency') }}">{{ __('Назад') }}</md-button>
                        </div>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </md-content>
    </div>

@endsection

@section('js_bottom')
    <script type="text/javascript">
        jQuery(function($) {
            $('#directionTab a:first').tab('show');
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var id = $(e.target).attr("href").substr(1);
                localStorage.setItem('selectedTab_currency_edit', id);
            });

            var selectedTab = localStorage.getItem('selectedTab_currency_edit');
            if (selectedTab) {
                $('#directionTab a[href="#' + selectedTab + '"]').tab('show');
            }
        });
    </script>
@endsection

