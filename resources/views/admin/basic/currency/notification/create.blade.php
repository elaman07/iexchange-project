@extends('admin.layouts.app')

@section('title', __('admin-basic.currency-notification.add_notification'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.notification.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('basic/currency-notification') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-notification.currency') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="id_currency" data-live-search="true">
                        @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}"> {{ $currency->payment->name }} {{ $currency->code_currency->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-notification.status') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="status" data-width="150">
                        <option value="0">{{ __('admin-basic.currency-notification.disable') }}</option>
                        <option value="1">{{ __('admin-basic.currency-notification.enable') }}</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-notification.class') }}</label>
                <div class="col-sm-4">
                    <select class="form-control selectpicker" name="css_class">
                        <option value="modal-n-danger">modal-n-danger</option>
                        <option value="modal-n-green">modal-n-green</option>
                        <option value="modal-n-primary">modal-n-primary</option>
                        <option value="modal-n-info">modal-n-info</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">

                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-notification.text_notification') }}</label>
                <div class="col-sm-4">
                    <textarea rows="5" placeholder="{{ __('admin-basic.currency-notification.text_notification_hint') }}" name="description" class="form-control" required></textarea>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/currency-notification') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection