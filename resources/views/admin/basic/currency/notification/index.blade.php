@extends('admin.layouts.app')

@section('title', __('admin-basic.currency-notification.title'))

@section('top-block')
    <md-button ng-href="currency-notification/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-basic.currency-notification.add_notification') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.notification'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-basic.currency-notification.id') }}</th>
            <th>{{ __('admin-basic.currency-notification.currency') }}</th>
            <th>{{ __('admin-basic.currency-notification.text') }}</th>
            <th>{{ __('admin-basic.currency-notification.class') }}</th>
            <th>{{ __('admin-basic.currency-notification.status') }}</th>
            <th>{{ __('admin-basic.action') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($notifications) > 0)
            @foreach($notifications as $notification)
                <tr>
                    <th>{{$notification->id}}</th>
                    <td>{{ $notification->currency->payment->name }} {{ $notification->currency->code_currency->name }}</td>
                    <td style="width: 30%">{{$notification->description}}</td>
                    <td>
                        {{ $notification->css_class }}
                    </td>
                    <td>
                        @if($notification->status == 0)
                            <span class="text-danger">{{ __('admin-basic.currency-notification.disable') }}</span>
                        @else
                            <span class="text-success">{{ __('admin-basic.currency-notification.enable') }}</span>
                        @endif
                    </td>

                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="currency-notification/{{$notification->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-basic.edit') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['currency-notification.destroy', $notification->id] ]) !!}
                            {!! Form::submit(__('admin-basic.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection