@extends('admin.layouts.app')

@section('title', __('admin-basic.currency-network.edit_title', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.network.change', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency-network/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')


            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-basic.currency-network.name') }}</label>
                <div class="col-sm-4">
                    <input type="text" name="name" class="form-control" value="{{$item->name}}" required>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/currency-network') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection
