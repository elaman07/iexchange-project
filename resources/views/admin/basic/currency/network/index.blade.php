@extends('admin.layouts.app')

@section('title', __('admin-basic.currency-network.title'))

@section('top-block')
    <md-button ng-href="currency-network/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-basic.currency-network.add_button') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.network'))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-basic.currency-command.created_at') }}</th>
            <th>{{ __('admin-basic.currency-command.name') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($groups) > 0)
            @foreach($groups as $group)
                <tr>
                    <th>{{ $group->created_at }}</th>
                    <td>{{$group->name}}</td>
                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="currency-network/{{$group->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-basic.edit') }}</a>
                        </div>

                        <div class="col-md-6">
                            <a href="currency-network/{{$group->id}}/delete" class="btn btn-danger btn-sm">{{ __('admin-basic.delete') }}</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
