@extends('admin.layouts.app')

@section('title', __('Добавить группу'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.groups.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/currency-groups') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('Название') }}</label>
                <div class="col-sm-2">
                    <input type="text" name="name" class="form-control">
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/currency-groups') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
