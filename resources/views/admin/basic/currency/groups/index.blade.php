@extends('admin.layouts.app')

@section('title', __('Группы валют'))

@section('top-block')
    <md-button ng-href="currency-groups/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить группу') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.currency.groups'))


@section('content')
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('Название') }}</th>
                <th>{{ __('Количество валют') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @if(count($groups) > 0)
                @foreach($groups as $group)
                    <tr>
                        <th>
                            <a href="{{ admin_base_path('/basic/currency-groups/'.$group->id.'/edit') }}">
                                {{ $group->name }}  &nbsp;<i class="fal fa-pencil"></i>
                            </a>
                        </th>
                        <td>
                            <a href="{{ admin_base_path('basic/currency?sorting_by=default&filter_currency[]='.$group->id) }}">
                                {{ __('Найдено валют') }}: {{ $group->currencies->count() }}
                            </a>
                        </td>

                        <td style="width: 50px;">
                            {!! Form::open(['id' => 'currency-groups_destroy_'.$group->id, 'method' => 'DELETE', 'route' => ['currency-groups.destroy', $group->id] ]) !!}
                            {!! Form::close() !!}

                            <a href="javascript:void(0)" onclick="document.getElementById('currency-groups_destroy_{{ $group->id }}').submit()">
                                <i class="fas fa-trash text-danger"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
@endsection
