@extends('admin.layouts.app')

@section('title','Изменить свой курс')

@section('breadcrumbs', Breadcrumbs::render('admin.basic.your_course.change',$item->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/your_course/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Название курса</label>
                <div class="col-sm-10">
                    <div class="form-group col-sm-6">
                        <div class="control-label-br">Пара</div>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Укажите название курса" value="{{$item->name}}" required>
                        <div class="input-p-text"><b>Пример:</b> QWRUB - SBERRUB</div>
                    </div>


                </div>
            </div>

            <div class="clearfix"></div>

            <div class="form-group">
                <label for="exchange_rate1" class="col-sm-2 control-label">Курс</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">Курс обмена</div>
                        <input type="text" name="exchange_rate" class="form-control" id="exchange_rate" placeholder="Укажите курс обмена" value="{{ $item->exchange_rate }}">
                    </div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">Знаков после запятой</div>
                        <input type="text" name="number_format" class="form-control" id="number_format" placeholder="Знаков, после запятой" value="{{ $item->number_format }}" required>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Обновить</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/your_course') }}">Назад</md-button>
        </div>
    </form>

@endsection
