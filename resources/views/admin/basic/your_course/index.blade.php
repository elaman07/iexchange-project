@extends('admin.layouts.app')

@section('title','Свой курс ЦБ')

@section('top-block')
    <md-button ng-href="your_course/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">Добавить свой курс</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.your_course'))

@section('content')

    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Название</th>
            <th>Курс обмена</th>
            <th>Кол-во направлений</th>
            <th class="not-sortable"></th>
        </tr>

        </thead>
        <tbody>

        @foreach($yours as $your)
            <tr>
                <th>
                    <a href="{{ admin_base_path('/basic/your_course/'.$your->id.'/edit') }}">{{ $your->name }}</a>
                </th>
                <td>1 - {{ $your->exchange_rate }}</td>
                <td>{{ $your->direction_exchange->count() }}</td>
                <td style="width:100px;">

                    <md-button href="your_course/{{$your->id}}/destroy" class="md-icon-button">
                        <i class="fas text-danger fa-trash"></i>
                    </md-button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
