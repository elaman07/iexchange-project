@extends('admin.layouts.app')

@section('title', __('admin-reserve.add_item'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.add'))

@section('content')
    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            @csrf


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Добавить резерв</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-reserve.currency') }}</div>
                        <select class="form-control selectpicker" data-live-search="true" name="id_currency" required>
                            @foreach($currencies as $item)
                                <option value="{{$item->id}}">{{ $item->payment->name .' '. $item->code_currency->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-reserve.group') }}</div>
                        <select class="form-control selectpicker" name="id_group" required>
                            <option value="0">-- {{ __('admin-reserve.not_chosen') }} --</option>
                            @foreach($groups as $item)
                                <option value="{{$item->id}}" @if(old('id_group') == $item->id) selected @endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-reserve.amount') }}</div>
                        <input type="text" name="summa" class="form-control" id="summa" placeholder="{{ __('admin-reserve.amount_placeholder') }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('admin-reserve.comment') }}</div>
                        <textarea class="form-control" rows="5" name="comment" placeholder="{{ __('admin-reserve.comment_placeholder') }}"></textarea>
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-reserve.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves') }}">{{ __('admin-reserve.back') }}</md-button>
        </div>
    </form>
@endsection
