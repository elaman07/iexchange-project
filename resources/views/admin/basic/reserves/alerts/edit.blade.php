@extends('admin.layouts.app')

@section('title',  __('admin-reserve.alert.edit_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.alerts.change', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/reserves-alerts/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">{{  __('admin-reserve.alert.reserve') }}</label>
                <div class="col-sm-5">
                    <select class="selectpicker" data-live-search="true" name="id_reserve" disabled required>
                        @foreach($reserves as $reserve)
                            <option value="{{$reserve->id}}"  @if($item->id_reserve == $reserve->id) selected @endif>{{ $reserve->currency->payment->name .' '. $reserve->currency->code_currency->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">{{  __('admin-reserve.alert.threshold_label') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="threshold" class="form-control" value="{{ $item->threshold }}">
                </div>
            </div>

            <hr />

            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-reserve.alert.enable_telegram') }}</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" name="is_telegram" data-width="150">
                        <option value="0" @if($item->is_telegram == 0) selected @endif>{{ __('admin-reserve.no') }}</option>
                        <option value="1" @if($item->is_telegram == 1) selected @endif>{{ __('admin-reserve.yes') }}</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-reserve.alert.enable_email') }}</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" name="is_email" data-width="150">
                        <option value="0" @if($item->is_email == 0) selected @endif>{{ __('admin-reserve.no') }}</option>
                        <option value="1" @if($item->is_email == 1) selected @endif>{{ __('admin-reserve.yes') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-reserve.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves-alerts') }}">{{ __('admin-reserve.back') }}</md-button>
        </div>
    </form>

@endsection