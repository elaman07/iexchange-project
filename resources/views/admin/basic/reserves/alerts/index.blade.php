@extends('admin.layouts.app')

@section('title', __('admin-reserve.alert.title'))

@section('top-block')
    <md-button ng-href="reserves-alerts/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-reserve.alert.add_item') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.alerts'))


@section('content')
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('admin-reserve.alert.id') }}</th>
            <th>{{ __('admin-reserve.alert.reserve') }}</th>
            <th>{{ __('admin-reserve.alert.telegram_notify') }}</th>
            <th>{{ __('admin-reserve.alert.email_notify') }}</th>
            <th>{{ __('admin-reserve.alert.current_reserve') }}</th>
            <th>{!! __('admin-reserve.alert.threshold') !!}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($alerts) > 0)
            @foreach($alerts as $alert)
                <tr>
                    <th>{{$alert->id}}</th>
                    <td>{{$alert->reserves->currency->payment->name}} {{$alert->reserves->currency->code_currency->name}}</td>
                    <td>
                        @if($alert->is_telegram)
                            <div class="label label-success">{{ __('admin-reserve.enable') }}</div>
                        @else
                            <div class="label label-info">{{ __('admin-reserve.disable') }}</div>
                        @endif
                    </td>
                    <td>
                        @if($alert->is_email)
                            <div class="label label-success">{{ __('admin-reserve.enable') }}</div>
                        @else
                            <div class="label label-info">{{ __('admin-reserve.disable') }}</div>
                        @endif
                    </td>
                    <td>{{ $alert->reserves->summa }} {{$alert->reserves->currency->code_currency->name}}</td>
                    <td>{{ $alert->threshold }} {{$alert->reserves->currency->code_currency->name}}</td>
                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="reserves-alerts/{{$alert->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-reserve.edit') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['reserves-alerts.destroy', $alert->id] ]) !!}
                            {!! Form::submit(__('admin-reserve.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-reserve.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection