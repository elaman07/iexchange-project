@extends('admin.layouts.app')

@section('title', __('admin-reserve.show_title', ['payment' => $reserv->currency->payment->name, 'code' => $reserv->currency->code_currency->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.show', $id))

@section('content')
    @if(count($items) == 0)
        <div class="alert alert-info text-center">{{ __('admin-reserve.list_is_empty') }}</div>
    @else
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('admin-reserve.order_id') }}</th>
                <th>{{ __('admin-reserve.direction_name') }}</th>
                <th>{{ __('admin-reserve.amount') }}</th>
                <th>{{ __('admin-reserve.filmed_fee') }}</th>
                <th>{{ __('admin-reserve.filmed_no_fee') }}</th>
                <th>{{ __('admin-reserve.fee') }}</th>
                <th>{{ __('admin-reserve.after_before') }}</th>
                <th>{{ __('admin-reserve.created_at') }}</th>
            </tr>
            </thead>

            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>
                        @if($item->id_task == 0)
                            {{ __('admin-reserve.undefined') }}
                        @else
                            <a href="{{config('admin.directory')}}/applications?action=filter&id={{$item->id_task}}">
                                <strong>{{$item->id_task}}</strong>
                            </a>
                        @endif
                    </td>
                    <td>
                        @if(!isset($item->tasks))
                            {{ __('admin-reserve.undefined') }}
                        @else
                            <a href="{{config('admin.directory')}}/applications/details/{{$item->id_task}}?actions=arhive">
                                {{ direction_name($item->tasks, true) }}
                            </a>
                        @endif
                    </td>
                    <td>
                        @if($item->type == 'plus')
                            <b class="text-success">+{{$item->summa}} {{$reserv->currency->code_currency->sign}}</b>
                        @else
                            <span class="text-danger">-{{ $item->summa }} {{$reserv->currency->code_currency->sign}}</span>
                        @endif
                    </td>
                    <td>
                        @if(is_null($item->summa_not_fee))
                            <small class="text-muted">{{ __('admin-reserve.not_data') }}</small>
                        @else
                            @if($item->type == 'plus')
                                <b class="text-success">+{{$item->summa_with_fee}} {{$reserv->currency->code_currency->sign}}</b>
                            @else
                                <span class="text-danger">-{{ $item->summa_with_fee }} {{$reserv->currency->code_currency->sign}}</span>
                            @endif
                        @endif
                    </td>
                    <td>
                        @if(is_null($item->summa_not_fee))
                            <small class="text-muted">{{ __('admin-reserve.not_data') }}</small>
                        @else
                            @if($item->type == 'plus')
                                <b class="text-success">+{{$item->summa_not_fee}} {{$reserv->currency->code_currency->sign}}</b>
                            @else
                                <span class="text-danger">-{{ $item->summa_not_fee }} {{$reserv->currency->code_currency->sign}}</span>
                            @endif
                        @endif
                    </td>
                    <td>
                        @if($item->type == 'plus')
                            @if($item->commission > 0)
                                {{ iex_number_format($item->commission, $reserv->currency->number_format) }} {{$reserv->currency->code_currency->sign}}
                            @else
                                0 {{$reserv->currency->code_currency->sign}}
                            @endif
                        @else
                            @if($item->commission > 0)
                                <span class="text-danger">- {{ iex_number_format($item->commission, $reserv->currency->number_format) }} {{$reserv->currency->code_currency->sign}}</span>
                            @else
                                0 {{$reserv->currency->code_currency->sign}}
                            @endif
                        @endif
                    </td>
                    <td>
                        {{ (isset($item->history) ? $item->history : __('admin-reserve.not_found')) }}
                    </td>
                    <td>
                        {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y, H:i') }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('pagination')
    {!! $items->links() !!}
@endsection
