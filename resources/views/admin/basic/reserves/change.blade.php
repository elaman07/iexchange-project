@extends('admin.layouts.app')

@section('title', __('admin-reserve.edit_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.change', $item->id))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form class="form-horizontal" method="post" action="?id={{$item->id}}">
                <div class="default-panel-body">
                    @csrf
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">{{ __('admin-reserve.currency') }}</label>
                        <div class="col-sm-6">
                            <b>{{ $item->currency->payment->name .' '. $item->currency->code_currency->name }}</b>
                        </div>
                    </div>

                    <hr/>
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">Резерв из файла</label>
                        <div class="col-sm-6">
                            @if(iEXSetting('is_enabled_reserves_from_file') == 1)
                                <select class="form-control selectpicker" name="id_file_reserve" data-width="300px" required>
                                    <option value="0">-- Не выбрано --</option>
                                    @foreach($reserve_files_groups as $file_group)
                                        <optgroup label="{{ $file_group->name }}">
                                            @foreach($file_group->reserves_files as $file_value)
                                                <option value="{{ $file_value->id }}" @if($file_value->id == $item->id_file_reserve) selected @endif>{{ $file_value->name }} (Резерв: {{ $file_value->amount }})</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach

                                </select>
                            @else
                                <small class="text-danger">По умолчанию функция отключена. <br />Включите в разделе "Основное - Корректировка резерва - Список резервов - Настройки"</small>
                            @endif
                        </div>
                    </div>

                    <hr/>
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">Резерв с сервера</label>
                        <div class="col-sm-6">
                            @if(iEXSetting('is_enabled_reserves_from_server') == 1)
                            <select class="form-control selectpicker" name="id_server_reserve"  data-live-search="true" required>

                                <option value="">-- Не выбрано --</option>
                                @foreach($online_balance as $server_group)
                                    <optgroup label="Сервис {{ $server_group['name'] }}">
                                        @foreach($server_group['balances'] as $server_key => $server_value)
                                            <option value="{{ $server_key }}" @if($server_key == $item->id_server_reserve) selected @endif>{{ $server_value}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach

                            </select>
                            @else
                                <small class="text-danger">По умолчанию функция отключена. <br />Включите в разделе "Основное - Корректировка резерва - Список резервов - Настройки"</small>
                            @endif
                        </div>
                    </div>


                    <hr/>
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">{{ __('admin-reserve.group') }}</label>
                        <div class="col-sm-6">
                            <select class="form-control selectpicker" name="id_group" data-width="300px" required>
                                <option value="0">-- {{ __('admin-reserve.not_chosen') }} --</option>
                                @foreach($groups as $group)
                                    <option value="{{$group->id}}" @if($item->id_group == $group->id) selected @endif>{{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <hr/>

                    <div class="form-group">
                        <label for="summa" class="col-sm-2 control-label">{{ __('admin-reserve.amount') }}</label>
                        <div class="col-sm-6">
                            {{ Form::input('text', 'summa', $item->summa, ['required', 'id' => 'summa', 'class' => 'form-control']) }}

                            <div class="form-check">
                                <input class="form-check-input" id="is_fixed_reserve" value="1" type="checkbox" name="is_fixed_reserve" @if($item->is_fixed_reserve == 1) checked @endif>
                                <label class="form-check-label" for="is_fixed_reserve">{{ __('admin-reserve.fixed_reserve') }}</label>
                            </div>
                        </div>

                    </div>
                    <hr />

                    <div class="form-group">
                        <label for="comment" class="col-sm-2 control-label">{{ __('admin-reserve.comment') }}</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" rows="5" name="comment" placeholder="{{ __('admin-reserve.comment_placeholder') }}">{{$item->comment}}</textarea>
                        </div>
                    </div>

                    <hr />
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">{{ __('admin-reserve.main_reserve') }}</label>
                        <div class="col-sm-6">
                            <select class="selectpicker" data-live-search="true" name="id_main">
                                <option value="0">-- {{ __('admin-reserve.not_chosen') }} --</option>
                                @foreach($reserves as $reserve)
                                    <option value="{{$reserve->id}}" @if($item->id_main == $reserve->id) selected @endif>
                                        {{ $reserve->currency->payment->name .' '. $reserve->currency->code_currency->name}}
                                    </option>
                                @endforeach
                            </select>

                            <small style="margin-top: 10px;display: block" class="text-danger">{{ __('admin-reserve.main_reserve_hint') }}</small>
                        </div>
                    </div>
                </div>


                <div class="default-panel-footer default-panel-footer-right">
                    <md-button class="md-raised md-primary" type="submit">{{ __('admin-reserve.save') }}</md-button>
                    @if(isset($filter['location']))
                        <md-button ng-href="{{ admin_base_path($filter['location']) }}">{{ __('admin-reserve.back') }}</md-button>
                    @else
                        <md-button ng-href="{{ admin_base_path('/basic/reserves') }}">{{ __('admin-reserve.back') }}</md-button>
                    @endif
                </div>
            </form>

        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-analytics.event_reserve.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-analytics.event_reserve.created_at') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.manager') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.currency') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.type') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.event') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.value_from') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.value_to') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.comment') }}</th>
                </tr>
                </thead>
                <tbody>
                @if(count($analytics) > 0)
                    @foreach($analytics as $data)
                        <tr>
                            <td>
                                <div class=""> {{ \Illuminate\Support\Carbon::parse($data->created_at)->translatedFormat('d M Y H:i') }}</div>
                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($data->created_at)->diffForHumans() }}</small>
                            </td>

                            <td>
                                <div>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_user}}&sorting=id"><b>{{$data->user->name}}</b></a>
                                </div>
                                <small>{{$data->user->email}}</small>
                            </td>
                            <td>
                                {{$data->currency->payment->name}} {{$data->currency->code_currency->name}}
                            </td>

                            <td>
                                @if($data->type == 0)
                                    <span class="text-danger"><i class="far fa-level-down-alt"></i> &nbsp;Уменьшен</span>
                                @else
                                    <span class="text-success"><i class="far fa-level-up-alt"></i> &nbsp;Пополнен</span>
                                @endif
                            </td>

                            <td>{{$data->text}}</td>
                            <td>{{$data->value_before}}</td>
                            <td>{{$data->value_after}}</td>
                            <td style="width: 200px">
                                @if(is_null($data->comment))
                                    <small class="text-muted">{{ __('admin-analytics.event_reserve.undefined') }}</small>
                                @else
                                    {{$data->comment}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-analytics.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $analytics->appends($filter)->links() !!}
    </div>
@endsection
