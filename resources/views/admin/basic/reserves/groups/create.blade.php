@extends('admin.layouts.app')

@section('title', __('admin-reserve.groups.add_title'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.groups.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/reserves-groups') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-reserve.groups.name') }}</label>
                <div class="col-sm-5">
                    <input type="text" name="name" class="form-control">
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">{{ __('admin-reserve.groups.status') }}</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" name="status" data-width="150">
                        <option value="0">{{ __('admin-reserve.disable') }}</option>
                        <option value="1">{{ __('admin-reserve.enable') }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon> {{ __('admin-reserve.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves-groups') }}">{{ __('admin-reserve.back') }}</md-button>
        </div>
    </form>

@endsection
