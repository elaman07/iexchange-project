@extends('admin.layouts.app')

@section('title', __('admin-reserve.title'))

@section('video__instruction', 'https://www.youtube.com/embed/hpkfpdiwmwY?autoplay=1&amp;mute=0')

@section('top-block')


    <md-button ng-href="reserves/reduce" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-minus text-primary"></md-icon>
        <span>{{ __('admin-reserve.reduce_button') }}</span>
    </md-button>

    <md-button ng-href="reserves/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-reserve.add_item') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-reserve.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves'))

@section('no-block-content')

    <style>
        md-tabs [role="tabpanel"] {transition: none;}
        md-tabs md-ink-bar {transition: none;}
        .abs-summary-reserve {
            position: absolute;
            right: 20px;
            top: 16px;
        }

        .is-fixed-reserve {
            margin-top: 5px;font-size: 11px;opacity: .8; color: #d10606;
        }
    </style>


    @if(count($default_reserves))
        <div class="alert alert-warning">
            <p style="font-size: 15px">
                Кол-во Найденых резервов, которые находятся вне категорий: <b>{{ count($default_reserves) }}</b>
            </p>
            <br />
            <md-button href="?transferred_to_group" class="md-raised md-primary" >
                Распредеть по группам
            </md-button>
        </div>
    @endif

    <div class="x_panel x_panel-md_custom tab-capitalize">
        <div class="abs-summary-reserve text-muted">
            {{ __('admin-reserve.common_reserve') }}: <b style="font-weight: 500">${{ number_format($convert_reserve, 0, '.', ' ') }}</b>
        </div>

        <form method="post" action="?">
            @csrf
            <md-tabs md-border-bottom md-dynamic-height class="new-x_panel_wrapper">
                @foreach($groups as $group)
                    <md-tab label="{{ $group->name }}">
                        <table class="table table-border-2">
                            <thead>
                            <tr>
                                <th>{{ __('admin-reserve.currency') }}</th>
                                <th>{{ __('admin-reserve.amount') }}</th>
                                <th>{{ __('admin-reserve.who_updated') }}</th>
                                <th>{{ __('admin-reserve.created_at') }}</th>
                                <th>{{ __('admin-reserve.last_updated') }}</th>
                                <th>{{ __('admin-reserve.event') }}</th>
                                <th class="not-sortable"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($group->reserve) > 0)
                                @foreach($group->reserve as $reserv)
                                    <tr @if($reserv->is_fixed_reserve == 1) style="border-left: 3px solid #d10606;" @endif>
                                        <td>
                                            <a href="reserves/show/{{$reserv->id}}" style="color: #00539e;font-weight: bold;">{{$reserv->currency->payment->name}} {{$reserv->currency->code_currency->name}}</a>
                                            @if($reserv->main_reserves->count() > 0)
                                                <div style="color: #888;margin-left: 5px;">
                                                    <small>Связанные резервы</small>
                                                    @foreach($reserv->main_reserves as $main_reserve)
                                                        <div>
                                                            <a href="{{ admin_base_path('/basic/reserves/change?id='.$main_reserve->id) }}" style="font-size: 11px;color: #00539e;font-weight: bold;">
                                                                - {{ $main_reserve->currency->payment->name }} {{ $main_reserve->currency->code_currency->name }}
                                                            </a>
                                                            &nbsp;&nbsp;
                                                            <a href="{{ admin_base_path('/basic/reserves/delete?id='.$main_reserve->id) }}">
                                                                <i class="text-danger far fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            @if($reserv->id_main == 0)
                                                <input type="text" class="form-control" style="width: 300px" name="reserve[{{$reserv->id}}]" value="{{ $reserv->summa }}">
                                                @if($reserv->is_fixed_reserve == 1)
                                                    <div class="text-muted is-fixed-reserve">{{ __('admin-reserve.static_reserved') }}</div>
                                                @endif
                                            @else
                                                <small class="text-danger">
                                                    {{ __('admin-reserve.uses_reserve') }}
                                                    {{$reserv->main_reserve->currency->payment->name}}
                                                    {{$reserv->main_reserve->currency->code_currency->name}}
                                                </small>
                                            @endif
                                        </td>
                                        <td style="width:200px">
                                            @if(isset($reserv->user))
                                                <a href="{{admin_base_path('/account/users?action=filter&id='.$reserv->user->id.'&sorting=id')}}"><b>{{$reserv->user->name}}</b></a>
                                            @else
                                                <span class="text-muted">{{ __('admin-reserve.system_name') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class=""> {{ \Illuminate\Support\Carbon::parse($reserv->created_at)->translatedFormat('d M Y H:i') }}</div>
                                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($reserv->created_at)->diffForHumans() }}</small>
                                        </td>
                                        <td>
                                            <div class=""> {{ \Illuminate\Support\Carbon::parse($reserv->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                            <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($reserv->updated_at)->diffForHumans() }}</small>
                                        </td>
                                        <td>
                                            <a href="{{admin_base_path('/analytics/reserves?currencies[]='.$reserv->id_currency)}}">
                                                {{ __('admin-reserve.all_event') }}: <b>{{ $reserv->event_reserve->count() }}</b>
                                            </a>
                                        </td>
                                        <td style="width:250px;">
                                            <div class="btn-margin-default">

                                                @if($reserv->is_star == 0)
                                                    <md-button href="?id_reserve={{ $reserv->id }}&star_status=enabled" class="md-icon-button" >
                                                        <i class="far fa-star"></i>
                                                    </md-button>
                                                @else
                                                    <md-button href="?id_reserve={{ $reserv->id }}&star_status=disabled" class="md-icon-button" >
                                                        <i class="fas fa-star text-success-700"></i>
                                                    </md-button>
                                                @endif



                                                @if($reserv->currency->id_auto_reserve)
                                                    <md-button href="reserves/money/{{$reserv->id}}" class="md-icon-button" >
                                                        <i title="{{ __('admin-reserve.update_serve_button') }}" class="text-info far fa-coin"></i>
                                                    </md-button>
                                                @else
                                                    <md-button disabled="" href="#" class="md-icon-button" >
                                                        <i class="fas fa-lock"></i>
                                                    </md-button>
                                                @endif

                                                <md-button href="reserves/change?id={{$reserv->id}}" class="md-icon-button" >
                                                    <i class="fas fa-pen"></i>
                                                </md-button>
                                                <md-button href="reserves/delete?id={{$reserv->id}}" class="md-icon-button" >
                                                    <i class="text-danger fas fa-trash"></i>
                                                </md-button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10"><p align="center"><br />{{ __('admin-reserve.list_is_empty') }}</p></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </md-tab>
                @endforeach
            </md-tabs>
            <div class="default-panel-footer">
                <div class="pull-right">

                    <div style="display: inline-block">
                        <select name="actions" class="form-control selectpicker" data-width="200px">
                            <option value="save">{{ __('admin-reserve.save') }}</option>
                        </select>
                    </div>
                    <md-button type="submit" class="md-primary md-raised">{{ __('admin-reserve.run') }}</md-button>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-reserve.settings_title') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-reserve.max_number_format_reserve') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="max_number_format_reserve" value="{{ iEXSetting('max_number_format_reserve', 10) }}">
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить модуль Резерв из файла') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_reserves_from_file" name="is_enabled_reserves_from_file" type="checkbox" value="1" @if(iEXSetting('is_enabled_reserves_from_file') == 1) checked @endif />
                                    <label for="is_enabled_reserves_from_file" class="label-primary"></label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Включить модуль Резерв из сервера') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_reserves_from_server" name="is_enabled_reserves_from_server" type="checkbox" value="1" @if(iEXSetting('is_enabled_reserves_from_server') == 1) checked @endif />
                                    <label for="is_enabled_reserves_from_server" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-reserve.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-reserve.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
