@extends('admin.layouts.app')

@section('title', __('admin-reserve.reduce_button'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.reduce'))

@section('content')
    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            @csrf



            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Уменьшить резерв</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-reserve.currency') }}</div>
                        <select class="form-control selectpicker" data-live-search="true" name="id_currency" required>
                            @if(count($popular_reserves) > 0)
                                <optgroup label="Избранные валюты">
                                    @foreach($popular_reserves as $reserve)
                                        <option value="{{$reserve->currency->id}}">{{ $reserve->currency->payment->name .' '. $reserve->currency->code_currency->name}}</option>
                                    @endforeach
                                </optgroup>
                            @endif

                            @foreach($currencies as $item)
                                <option value="{{$item->id}}">{{ $item->payment->name .' '. $item->code_currency->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-reserve.amount') }}</div>
                        <input type="text" name="summa" class="form-control" id="summa" placeholder="{{ __('admin-reserve.amount_placeholder') }}" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('admin-reserve.comment') }}</div>
                        <textarea class="form-control" name="comment" rows="5" placeholder="{{ __('admin-reserve.comment_placeholder') }}"></textarea>
                    </div>


                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-reserve.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves') }}">{{ __('admin-reserve.back') }}</md-button>
        </div>
    </form>
@endsection
