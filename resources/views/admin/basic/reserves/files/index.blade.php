@extends('admin.layouts.app')

@section('title', __('Резервы из файла'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.files'))

@section('top-block')
    <md-button ng-href="reserves-files/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Добавить резерв из файла') }}</span>
    </md-button>
@endsection

@section('no-block-content')

    @if(count($links) > 0)
        <div class="panel panel-default">

            <div class="panel-heading nav-tabs-solid-header group-parser-header">
                <ul class="nav nav-tabs nav-tabs-solid group-parser-nav">
                    @foreach($links as $key => $value)
                        <li class="@if($key == 0) active @endif">
                            <a md-ink-ripple="'#000'" href="#crypto-{{ $value->id }}" data-toggle="tab" aria-expanded="false">
                                {{ $value->name }}
                                <span class="badge badge-light">{{ $value->reserves_files->count() }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="panel-tab-content tab-content">
                @foreach($links as $key => $value)
                    <div class="tab-pane @if($key == 0) active @endif" id="crypto-{{$value->id}}">
                        <form style="width:100%;margin-right: 10px;" method="post" action="?">
                            @csrf
                            <table class="manager-table-2 table table-border-2">
                                <thead>
                                <tr>
                                    <th>{{ __('Название') }}</th>
                                    <th>{{ __('Резерв') }}</th>
                                    <th>{{ __('Дата создания') }}</th>
                                    <th>{{ __('Посл. обновление') }}</th>
                                    <th>{{ __('Статус') }}</th>
                                    <th class="not-sortable"></th>
                                </tr>

                                </thead>
                                <tbody>

                                @if(count($value->reserves_files) > 0)
                                    @foreach($value->reserves_files as $file)
                                        <tr>
                                            <th>
                                                <a href="{{ admin_base_path('/basic/reserves-files/'.$file->id.'/edit') }}">
                                                    {{ $file->name }}  &nbsp;<i class="fal fa-pencil"></i>
                                                </a>
                                            </th>
                                            <td>
                                                @if($file->amount == null)
                                                    <span class="text-danger">Не указан</span>
                                                @else
                                                    {{ $file->amount }}
                                                @endif
                                            </td>

                                            <td>
                                                <div class=""> {{ \Illuminate\Support\Carbon::parse($file->created_at)->translatedFormat('d M Y H:i') }}</div>
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($file->created_at)->diffForHumans() }}</small>
                                            </td>


                                            <td>
                                                <div class=""> {{ \Illuminate\Support\Carbon::parse($file->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($file->updated_at)->diffForHumans() }}</small>
                                            </td>

                                            <td>
                                                @if($file->status == 1)
                                                    <span class="label label-success">Активен</span>
                                                @else
                                                    <span class="label label-danger">Отключен</span>
                                                @endif
                                            </td>
                                            <td style="width: 100px">
                                                <md-button class="md-icon-button" ng-href="{{ admin_base_path('basic/reserves-files/'.$file->id.'/delete')  }}">
                                                    <i class="fad fa-trash text-danger"></i>
                                                </md-button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10"><p align="center"><br />Список пуст</p></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="alert alert-danger text-center">Нет данных</div>
    @endif
@endsection
