@extends('admin.layouts.app')

@section('title', __('Добавить источник'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.files.groups.create'))


@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/reserves-files-group') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Новый источник') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Ссылка на файл') }}</div>
                        <input type="text" name="link"  placeholder="{{ __('Укажите ссылку на файл резервов') }}" value="{{ old('link') }}" class="form-control" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status" data-width="150">
                            <option value="0" @if(old('status') == 0) selected @endif>{{ __('Отключено') }}</option>
                            <option value="1" @if(old('status') == 1) selected @endif>{{ __('Включено') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves-files-group') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
