@extends('admin.layouts.app')

@section('title', __('Изменить источник :name', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.files.groups.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/reserves-files-group/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('put')


            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Изменить источник') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" value="{{$item->name}}" class="form-control" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Ссылка на файл') }}</div>
                        <input type="text" name="link"  placeholder="{{ __('Укажите ссылку на файл резервов') }}" value="{{$item->link}}" class="form-control" required>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status" data-width="150">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Отключено') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Включено') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves-files-group') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
