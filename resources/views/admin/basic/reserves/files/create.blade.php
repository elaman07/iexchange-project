@extends('admin.layouts.app')

@section('title', __('Добавить резерв из файла'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.reserves.files.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/reserves-files') }}">
        <div class="default-panel-body">
            @csrf

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Новый резерв') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Источник') }}</div>
                        @if(count($links) == 0)
                            <div class="text-danger">{{ __('Нет доступных источников резервов') }}</div>
                            <small>
                                <a href="{{ admin_base_path('/basic/reserves-files-group') }}">{{ __('Добавить источник резервов из файла') }}</a>
                            </small>
                        @else
                            <select class="selectpicker" name="id_group" id="id_group" data-live-search="true" required>
                                @foreach($links as $link)
                                    <option value="{{ $link->id }}" @if(old('id_group') == $link->id) selected @endif>{{ $link->name }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-2">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select name="status" class="form-control selectpicker">
                            <option value="0" @if(old('status') == 0) selected @endif>{{ __('Отключено') }}</option>
                            <option value="1" @if(old('status') == 1) selected @endif>{{ __('Включено') }}</option>
                        </select>
                    </div>


                    <div class="form-group col-md-3">
                        <div class="control-label-br">{{ __('Знаки после, запятой') }}</div>
                        <input type="text" name="number_format" class="form-control" id="number_format" value="{{ old('number_format', 0) }}" required>
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/reserves-files') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
