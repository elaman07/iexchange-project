@extends('admin.layouts.app')

@section('title', __('Добавить код'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.code_currency.add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/code_currency') }}">
        @csrf
        <div class="default-panel-body">

            <!-- Код валюты -->
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Код валюты') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Код валюты') }}</div>
                        <input type="text" name="code" class="form-control" value="{{ old('name') }}" id="code" placeholder="{{ __('Укажите код валюты') }}" required>
                        <div class="input-p-text"><b>{{ __('Пример') }}:</b> RUB or USD</div>
                    </div>


                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Символ валюты') }}</div>
                        <input type="text" name="sign" class="form-control" value="{{ old('sign') }}" placeholder="{{ __('Укажите символ валюты') }}" required>
                        <div style="margin-top: 5px; font-size: 12px;" class="text-muted"><b>{{ __('Пример') }}:</b> € or руб</div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr/>

            <!-- Автокорректировка курса -->
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Автокорректировка курса') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Курс') }}</div>
                        <select class="form-control selectpicker show-tick" name="id_parser_exchange" data-live-search="true" data-size="10">
                            <option value="0">-- {{ __('Не указано') }} --</option>
                            @foreach($group_parsers as $group)
                                <optgroup label="{{$group->name}}">
                                    @foreach($group->parser_exchange_enabled as $value)
                                        <option @if(old('id_parser_exchange') == $value->id) selected @endif value="{{$value->id}}">{{$value->name}} ({{$value->value}} → {{$value->summa}})</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Добавляем к курсу') }}</div>
                        <input type="text" name="add_to_course" class="form-control" value="{{ old('add_to_course', 0) }}" required>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr/>

            <!-- Курс по формуле -->
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Курс по формуле') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Курс') }}</div>
                        <select class="form-control selectpicker show-tick" name="id_parser_formula" data-live-search="true" data-size="10">
                            <option value="0">-- {{ __('Не указано') }} --</option>
                            @foreach($rates_formula as $item)
                                <option value="{{ $item->id }}" @if(old('id_parser_formula') == $item->id) selected @endif>{{  $item->title }} (1 → {{ $item->summa }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Добавляем к курсу') }}</div>
                        <input type="text" name="add_to_course_formula" class="form-control" value="{{ old('add_to_course_formula', 0) }}" required>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/code_currency') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>

@endsection
