@extends('admin.layouts.app')

@section('title', __('Коды валют'))

@section('video__instruction', 'https://www.youtube.com/embed/64pcBTJkEa4?autoplay=1&amp;mute=0')

@section('top-block')

    <md-button data-toggle="modal" data-target="#template" class="btn-float" layout="column">
        <i class="fad fa-upload text-muted"></i>
        <span class="text-muted">{{ __('Готовые данные') }}</span>
    </md-button>

    <md-button ng-href="code_currency/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить код') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings_columns" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-table"></md-icon>
            <span>{{ __('Настройка страницы') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.code_currency'))


@section('x_panel_class', 'new-x_panel_wrapper')
@section('content')
    <form style="width:100%;margin-right: 10px;" method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th class="not-sortable"></th>
                @if(in_array('code_currency', $admin_code_hidden_columns))
                    <th>{{ __('Код валюты') }}</th>
                @endif
                @if(in_array('symbol', $admin_code_hidden_columns))
                    <th>{{ __('Символ') }}</th>
                @endif
                @if(in_array('pegged_currencies', $admin_code_hidden_columns))
                    <th>{{ __('Привязанные валюты') }}</th>
                @endif
                @if(in_array('reserve', $admin_code_hidden_columns))
                    <th>{{ __('Резерв') }}</th>
                @endif
                @if(in_array('course', $admin_code_hidden_columns))
                    <th>{{ __('Курс обмена') }}</th>
                @endif
                @if(in_array('auto_currect', $admin_code_hidden_columns))
                    <th>{{ __('Автокорректировка курса') }}</th>
                @endif
                @if(in_array('auto_currect_formula', $admin_code_hidden_columns))
                    <th>{{ __('Курс по формуле') }}</th>
                @endif
            </tr>

            </thead>
            <tbody>
            @if(count($codes) > 0)
                @foreach($codes as $code)
                    <tr>
                        <th style="width: 100px">
                            <div class="form-check">
                                <input class="form-check-input" id="checkbox{{ $code->id }}" value="{{ $code->id }}" type="checkbox" name="check[]">
                                <label class="form-check-label" for="checkbox{{ $code->id }}"></label>
                            </div>

                            <input type="hidden" class="form-control" name="item_id[{{$code->id}}]" value="{{ $code->id }}">
                        </th>
                        @if(in_array('code_currency', $admin_code_hidden_columns))
                            <th>
                                <div>
                                    <a href="code_currency/{{ $code->id }}/edit">
                                        {{ $code->name }}  &nbsp;<i class="fal fa-pencil"></i>
                                    </a>
                                </div>

                                @if($code->is_import == 1)
                                    <small class="text-muted font-weight-normal">{{ __('Импортированный') }}</small>
                                @endif

                            </th>
                        @endif
                        @if(in_array('symbol', $admin_code_hidden_columns))
                            <td>{{ $code->sign }}</td>
                        @endif
                        @if(in_array('pegged_currencies', $admin_code_hidden_columns))
                            <td>
                                @if(count($code->currency) > 0)
                                    <ul class="list-unstyled" height="100px" slim-scroll>
                                        @foreach($code->currency as $value)
                                            <li>- <a style="font-size: 11px;color: @if($value->status == 0)#00539e @else #a94442 @endif;font-weight: bold;" href="{{ admin_base_path('/basic/currency/'.$value->id.'/edit') }}">{{ $value->payment->name }}</a></li>
                                        @endforeach
                                    </ul>
                                @else
                                    <small class="text-muted">{{ __('Не найдено') }}</small>
                                @endif
                            </td>
                        @endif
                        @if(in_array('reserve', $admin_code_hidden_columns))
                            <td>
                                @php
                                    $reserve_amount = $code->reserve_amount();
                                @endphp

                                @if($reserve_amount > 0)
                                    <span class="text-success">{{ $reserve_amount }} {{ $code->name }}</span>
                                @elseif($reserve_amount == 0)
                                    {{ $reserve_amount }} {{ $code->name }}
                                @else
                                    <span class="text-danger">{{ $reserve_amount }} {{ $code->name }}</span>
                                @endif
                            </td>
                        @endif
                        @if(in_array('course', $admin_code_hidden_columns))
                            <td>
                                @if($code->id_parser_exchange > 0 and isset($code->parser_exchange))
                                    {{ handleDynamicFeeAutomatic($code->add_to_course, $code->parser_exchange->summa) }}
                                @elseif($code->id_parser_formula > 0 and isset($code->parser_formula))
                                    {{ handleDynamicFeeAutomatic($code->add_to_course_formula, $code->parser_formula->summa) }}
                                @else
                                    1 USD =
                                    <input type="text" class="form-control" name="internal_rate[{{$code->id}}]" value="{{ $code->internal_rate }}" style="width: 100px;display: inline-block;"> {{ $code->name }}
                                @endif
                            </td>
                        @endif

                        @if(in_array('auto_currect', $admin_code_hidden_columns))
                            <td>
                                @if(isset($group_rates))
                                    <select class="form-control selectpicker" name="id_parser_exchange[{{$code->id}}]"  data-live-search="true" data-size="8">
                                        <option value="0">-- {{ __('Не указано') }} --</option>
                                        @foreach($group_rates as $group)
                                            @if(isset($parser_exchange[$group->id]))
                                                <optgroup label="{{  $group->name }}">
                                                    @foreach($parser_exchange[$group->id] as $item)
                                                        <option value="{{ $item['id'] }}" @if($code->id_parser_exchange == $item['id']) selected @endif>{{  $item['name'] }} ({{ $item['value'] }} → {{ $item['summa'] }})</option>
                                                    @endforeach
                                                </optgroup>
                                            @endif
                                        @endforeach
                                    </select>
                                    <input type="text" class="form-control" name="add_to_course[{{$code->id}}]" value="{{ $code->add_to_course }}">
                                @else
                                    <small class="text-danger">{{ __('Добавьте пары в разделе Работа парсеров') }}</small>
                                @endif
                            </td>
                        @endif

                        @if(in_array('auto_currect_formula', $admin_code_hidden_columns))
                            <td>
                                <select class="form-control selectpicker" name="id_parser_formula[{{$code->id}}]"  data-live-search="true" data-size="8">
                                    <option value="0">-- {{ __('Не указано') }} --</option>
                                    @foreach($rates_formula as $item)
                                        <option value="{{ $item->id }}" @if($code->id_parser_formula == $item->id) selected @endif>{{  $item->title }} (1 → {{ $item->summa }})</option>
                                    @endforeach
                                </select>
                                <input type="text" class="form-control" name="add_to_course_formula[{{$code->id}}]" value="{{ $code->add_to_course_formula }}">
                            </td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">

                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">{{ __('Сохранить') }}</option>
                        <option value="trashed">{{ __('В корзину') }}</option>

                        <option data-divider="true"></option>
                        <option value="deleteglobal" data-content="<span class='label label-danger'>{{ __('Важное') }}</span>&nbsp;&nbsp;{{ __('Удалить навсегда') }}"></option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('Выполнить') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>


    <!-- Модальное окно для настроек таблицы -->
    <div class="modal fade" id="settings_columns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings_columns">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Настройка страницы') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Колонки') }}</label>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[code_currency]" value="code_currency" type="checkbox" name="admin_code_hidden_columns[code_currency]" @if(in_array('code_currency', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[code_currency]">{{ __('Код валюты') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[symbol]" value="symbol" type="checkbox" name="admin_code_hidden_columns[symbol]" @if(in_array('symbol', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[symbol]">{{ __('Символ') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[pegged_currencies]" value="pegged_currencies" type="checkbox" name="admin_code_hidden_columns[pegged_currencies]" @if(in_array('pegged_currencies', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[pegged_currencies]">{{ __('Привязанные валюты') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[reserve]" value="reserve" type="checkbox" name="admin_code_hidden_columns[reserve]" @if(in_array('reserve', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[reserve]">{{ __('Резерв') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[course]" value="course" type="checkbox" name="admin_code_hidden_columns[course]" @if(in_array('course', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[course]">{{ __('Курс обмена') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[auto_currect]" value="auto_currect" type="checkbox" name="admin_code_hidden_columns[auto_currect]" @if(in_array('auto_currect', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[auto_currect]">{{ __('Автокорректировка курса') }}</label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" id="admin_code_hidden_columns[auto_currect_formula]" value="auto_currect_formula" type="checkbox" name="admin_code_hidden_columns[auto_currect_formula]" @if(in_array('auto_currect_formula', $admin_code_hidden_columns)) checked @endif>
                                    <label class="form-check-label" for="admin_code_hidden_columns[auto_currect_formula]">{{ __('Курс по формуле') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group">
                            <label for="admin_code_pagination" class="control-label col-md-6">{{ __('Кол-во записей на странице') }}</label>
                            <div class="col-sm-6">
                                <input type="text" name="admin_code_pagination" class="form-control" value="{{ iEXSetting('admin_code_pagination', 20) }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно для импорта данных -->
    <div class="modal fade" id="template" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form action="?import_payments" method="POST" class="form-horizontal">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Импорт готовых кодов') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-4">{{ __('Коды валюты') }}</label>
                            <div class="col-md-8">
                                {!! Form::select('imports_data[]', $imports, null, ['class' => 'form-control selectpicker', 'required', 'data-live-search' => 'true', 'data-size' => '15', 'multiple']) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Сохранить') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Отмена') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $codes->links() !!}
    </div>
@endsection
