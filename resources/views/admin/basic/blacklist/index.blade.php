@extends('admin.layouts.app')

@section('title', __('admin-basic.black_list.title'))

@section('top-block')
    <md-button ng-href="blacklist/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('admin-basic.black_list.add_item') }}</span>
    </md-button>


    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float  iex-settings-color" layout="column">
            <md-icon md-font-icon="fa fa-cog"></md-icon>
            <span>{{ __('admin-basic.currency.settings_label') }}</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.blacklist'))


@section('title-right-button')
    <md-button class="md-warn" href="{{ admin_base_path('/basic/blacklist/?clear_all') }}">{{ __('admin-basic.black_list.clear_list') }}</md-button>
@endsection

@section('no-block-content')
    <div class="x_panel">

        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-basic.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-basic.black_list.value') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::input('text', 'from_value', is_filter_search($filter, 'from_value'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-basic.black_list.types') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::select('types[]', $types, is_filter_search($filter, 'types'),
                                            ['class' => 'selectpicker', 'data-width' => '100%', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-basic.black_list.created_at') }}</label>
                                    <div class="col-md-8">
                                        {{ __('admin-basic.from') }}: <input  value="{{ is_filter_search($filter, 'from_created_at') }}" name="from_created_at" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-basic.to') }}: <input  value="{{ is_filter_search($filter, 'to_created_at') }}" name="to_created_at" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-basic.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-basic.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="pagination-right">
        {!! $lists->appends($filter)->links() !!}
    </div>

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-basic.black_list.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-basic.black_list.created_at') }}</th>
                    <th>{{ __('admin-basic.black_list.value') }}</th>
                    <th>{{ __('admin-basic.black_list.type') }}</th>
                    <th>{{ __('admin-basic.black_list.description') }}</th>
                    <th>{{ __('admin-basic.action') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($lists) > 0)
                    @foreach($lists as $list)
                        <tr>
                            <th scope="row">
                                {{ $list->created_at }}

                                @if($list->is_iex == 1)
                                    <br />
                                    <small class="text-success">Запись занесен в единную базу</small>
                                @endif
                            </th>
                            <td>{!! nl2br($list->value) !!}</td>
                            <td>
                                @if($list->type == 3)
                                    Все
                                @else
                                    {{$types[$list->type]}}
                                @endif
                            </td>
                            <td style="width: 300px">{!! nl2br($list->text) !!}</td>
                            <td style="width: 200px;">
                                <div class="col-md-6">
                                    <a href="blacklist/{{$list->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-basic.edit') }}</a>
                                </div>

                                <div class="col-md-6">
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['blacklist.destroy', $list->id] ]) !!}
                                    {!! Form::submit(__('admin-basic.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-basic.black_list.settings_title') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-basic.black_list.enable_black_list') }}</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_local_blacklist" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_local_blacklist') == 0) selected @endif>{{ __('admin-basic.no') }}</option>
                                    <option value="1"  @if(iEXSetting('is_local_blacklist') == 1) selected @endif>{{ __('admin-basic.yes') }}</option>
                                </select>
                            </div>
                        </div>


                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Включить запись в единную базу iEXExchanger</label>
                            <div class="col-md-6" id="requisites-list">
                                <select class="selectpicker" name="is_bs_unified_base_iex" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_bs_unified_base_iex') == 0) selected @endif>{{ __('admin-basic.yes') }}</option>
                                    <option value="1"  @if(iEXSetting('is_bs_unified_base_iex') == 1) selected @endif>{{ __('admin-basic.no') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-basic.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-basic.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $lists->appends($filter)->links() !!}
    </div>
@endsection
