@extends('admin.layouts.app')

@section('title', __('admin-basic.filter_currency.title'))

@section('top-block')
    <md-button ng-href="filter_currency/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">@lang('admin-basic.filter_currency.add_filter')</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('admin.basic.filter_currency'))

@section('content')


    <table class="table table-border-2">
        <thead>
        <tr>
            <th>@lang('admin-basic.filter_currency.name')</th>
            <th>@lang('admin-basic.filter_currency.num_currency')</th>
            <th class="not-sortable"></th>
        </tr>

        </thead>
        <tbody>
        @foreach($filters as $filter)
            <tr>
                <th>
                    <a href="{{ admin_base_path('/basic/filter_currency/'.$filter->id.'/edit') }}">
                        {{ $filter->name }}  &nbsp;<i class="fal fa-pencil"></i>
                    </a>
                </th>
                <td>
                    <a href="{{ admin_base_path('basic/currency?sorting_by=default&filter_currency[]='.$filter->id) }}">
                        @lang('admin-basic.filter_currency.found'): {{ $filter->currencies->count() }}
                    </a>
                </td>
                <td style="width: 100px;">
                    <div class="col-md-6">
                        {!! Form::open(['method' => 'DELETE', 'route' => ['filter_currency.destroy', $filter->id] ]) !!}
                        {!! Form::submit(__('admin-basic.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
