@extends('admin.layouts.app')

@section('title', __('admin-basic.filter_currency.add_filter'))

@section('breadcrumbs', Breadcrumbs::render('admin.basic.filter_currency.create'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/basic/filter_currency') }}">
        @csrf

        <div class="default-panel-body">

            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">{{ __('Добавить фильтр') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}"  placeholder="{{ __('admin-basic.filter_currency.name_hint') }}" class="form-control" id="name{{$form_lang['field']}}">
                                <div class="input-p-text"><b>{{ __('admin-basic.filter_currency.example') }}:</b> RUB or COIN</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/basic/filter_currency') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection
