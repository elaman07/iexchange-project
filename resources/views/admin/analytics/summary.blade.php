@extends('admin.layouts.app')

@section('title', __('admin-analytics.summary.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.summary'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('admin-analytics.summary.settings') }}</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')

    <style>
        .audience_items {
            padding-top: 30px;
        }

        .audience_items .audience_item_title {
            font-size: 13px;
            color: #777;
        }

        .audience_items .audience_item_value {
            font-size: 20px;
            margin-top: 5px;
            color: #000;
        }

        .audience_items .audience_item {
            padding-bottom: 30px;
        }
    </style>


    @if(iEXSetting('is_enabled_google_analytics') == true)
        <div class="x_panel">
            <div class="x_title">
                <h2>{{ __('admin-analytics.summary.audience_overview') }}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div layout="row" layout-wrap class="audience_items" layout-align="space-around center">
                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.users_label') }}</div>
                        <div class="audience_item_value">{{ $audience_review['totalsForAllResults']['ga:users'] }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.new_user_label') }}</div>
                        <div class="audience_item_value">{{ $audience_review['totalsForAllResults']['ga:newUsers'] }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.session_label') }}</div>
                        <div class="audience_item_value">{{ $audience_review['totalsForAllResults']['ga:sessions'] }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.session_per_user_label') }}</div>
                        <div class="audience_item_value">{{ iex_number_format($audience_review['totalsForAllResults']['ga:sessionsPerUser'], 2) }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.page_view_label') }}</div>
                        <div class="audience_item_value">{{ $audience_review['totalsForAllResults']['ga:pageviews'] }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.page_view_session_per_user') }}</div>
                        <div class="audience_item_value">{{ iex_number_format($audience_review['totalsForAllResults']['ga:pageviewsPerSession'], 2) }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.avg_session') }}</div>
                        <div class="audience_item_value">{{ gmdate('H:i:s', $audience_review['totalsForAllResults']['ga:avgSessionDuration']) }}</div>
                    </div>

                    <div class="audience_item" flex="25" layout="column" layout-align="center center">
                        <div class="audience_item_title">{{ __('admin-analytics.summary.bounce_rate') }}</div>
                        <div class="audience_item_value">{{ iex_number_format($audience_review['totalsForAllResults']['ga:bounceRate']) }}%</div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div layout="row" layout-xs="column" layout-align="space-between none" class="admin-home-layout">
        <div flex-xs="100" flex="50">
           <div style="padding-right: 10px;">
               <div class="x_panel">
                   <div class="x_title" layout="row">
                       <h2>{{ __('admin-analytics.summary.clients') }}</h2>
                       <div flex></div>
                       <div class="heading-right-button">
                           <md-button href="?"  @if(!isset($stat_google)) disabled @endif >{{ __('admin-analytics.summary.7day') }}</md-button>
                           {{--<md-button href="?stat_google=30d" @if(isset($stat_google) and $stat_google == '30d') disabled @endif>30 дн.</md-button>--}}
                       </div>
                       <div class="clearfix"></div>
                   </div>
                   <br />
                   <div class="x_content">
                       <div id="google-analytics" style="width: 100%"></div>
                   </div>
               </div>
               <div class="x_panel">
                   <div class="x_title">
                       <h2>{{ __('admin-analytics.summary.user_come_from') }}</h2>
                       <div class="clearfix"></div>
                   </div>
                   <br />
                   <div class="x_content">
                       @if(config('admin.is_demo_mode') == true)
                           <div class="alert alert-danger">{{ __('admin-analytics.disable_demo_version') }}</div>
                       @else
                           <table class="table table-border-2">
                               <thead>
                               <tr>
                                   <th>{{ __('admin-analytics.summary.website_page') }}</th>
                                   <th>{{ __('admin-analytics.summary.views') }}</th>
                                   <th></th>
                               </tr>

                               </thead>
                               <tbody>
                               <span style="display: none">{{$key = 1}}</span>
                               @foreach($fetchMostVisitedPages  as $value)
                                   <tr>
                                       <td>{{$value['url']}}</td>
                                       <td>{{$value['pageViews']}}</td>
                                   </tr>
                               @endforeach
                               </tbody>

                           </table>
                       @endif
                   </div>
               </div>
               <div class="x_panel">
                   <div class="x_title">
                       <h2>{{ __('admin-analytics.summary.pages_user_view') }}</h2>
                       <div class="clearfix"></div>
                   </div>
                   <br />
                   <div class="x_content">
                       @if(config('admin.is_demo_mode') == true)
                           <div class="alert alert-danger">{{ __('admin-analytics.disable_demo_version') }}</div>
                       @else
                           <table class="table table-border-2">
                               <thead>
                               <tr>
                                   <th>{{ __('admin-analytics.summary.website_label') }}</th>
                                   <th>{{ __('admin-analytics.summary.page_view') }}</th>
                                   <th></th>
                               </tr>

                               </thead>
                               <tbody>
                               @foreach($fetchTopReferrers as $value)
                                   <tr>
                                       <td>{{\Str::limit($value['url'], 50)}}</td>
                                       <td>{{$value['pageViews']}}</td>
                                   </tr>
                               @endforeach
                               </tbody>

                           </table>
                       @endif
                   </div>
               </div>
           </div>
        </div>
        <div style="padding-right: 0"  flex-xs="100" flex="50">
            <div>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ __('admin-analytics.summary.browser') }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="x_content">
                        <div id="statistics-browser" style="width: 100%;display: block"></div>
                    </div>
                </div>
                <div class="x_panel">
                    <div class="x_title" layout="row">
                        <h2>{{ __('admin-analytics.summary.report_archive') }} </h2>
                        <div flex></div>
                            <div class="heading-right-button">
                                <md-button href="?clear=archive_reserve" class="md-warn">{{ __('admin-analytics.summary.clear') }}</md-button>
                            </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if(config('admin.is_demo_mode') == true)
                            <div class="alert alert-danger">{{ __('admin-analytics.disable_demo_version') }}</div>
                        @else
                            <table class="table table-border-2">
                                <thead>
                                <tr>
                                    <th>{{ __('admin-analytics.summary.created_at') }}</th>
                                    <th>{{ __('admin-analytics.summary.description') }}</th>
                                    <th>{{ __('admin-analytics.summary.action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($items) > 0)
                                    @foreach($items as $item)
                                        <tr>
                                            <th>{{$item->created_at->format('d.m.Y H:i')}}</th>
                                            <td>{{ __('admin-analytics.summary.total_report') }}</td>
                                            <td style="width: 200px">
                                                <a target="_blank" class="md-primary" style="margin-right: 10px" ng-href="?open={{$item->name}}">{{ __('admin-analytics.summary.open') }}</a>
                                                | &nbsp;&nbsp;
                                                <a class="md-primary" ng-href="?download={{$item->name}}">{{ __('admin-analytics.summary.download') }}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9"><p align="center"><br />{{ __('admin-analytics.list_is_empty') }}</p></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        @endif
                        <div class="pagination-right">
                            {!! $items->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-analytics.summary.settings_report_title') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-analytics.summary.is_auto_reserves_report') }}</label>
                            <div class="col-md-6">
                                <select class="selectpicker" name="is_auto_reserves_report" data-width="250px">
                                    <option value="0"  @if(iEXSetting('is_auto_reserves_report') == 0) selected @endif>{{ __('admin-analytics.summary.yes') }}</option>
                                    <option value="1"  @if(iEXSetting('is_auto_reserves_report') == 1) selected @endif>{{ __('admin-analytics.summary.no') }}</option>
                                </select>
                            </div>
                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-analytics.summary.report_analytics_status') }}</label>
                            <div class="col-md-6">
                                <select class="selectpicker" name="report_analytics_status" data-width="250px">
                                    <option value="0"  @if(iEXSetting('report_analytics_status') == 0) selected @endif>{{ __('admin-analytics.summary.yes') }}</option>
                                    <option value="1"  @if(iEXSetting('report_analytics_status') == 1) selected @endif>{{ __('admin-analytics.summary.no') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('admin-analytics.summary.report_analytics_email') }}</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="report_analytics_email" value="{{ iEXSetting('report_analytics_email') }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-analytics.summary.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-analytics.summary.back') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js_bottom')

    @if($stat_google == '30d')
        <script src="/admin-assets/statistics/google_30d.js"></script>
    @else
        <script src="/admin-assets/statistics/google_7d.js"></script>
    @endif
@endsection
