@extends('admin.layouts.app')

@section('title', __('admin-analytics.event_reserve.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.event_reserve'))

@section('no-block-content')

    <div class="x_panel">

        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-analytics.event_reserve.filters') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-analytics.event_reserve.type_event') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::select('types[]', $types, is_filter_search($filter, 'types'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-analytics.event_reserve.currency') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::select('currencies[]', $currencies, is_filter_search($filter, 'currencies'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('admin-analytics.event_reserve.created_at') }}</label>
                                    <div class="col-md-8">
                                        {{ __('admin-analytics.event_reserve.from') }}: <input  value="{{ is_filter_search($filter, 'from_created_at') }}" name="from_created_at" style="width: 140px;display: inline-block;margin-right: 10px;" type="text" class="form-control datetimepicker">
                                        {{ __('admin-analytics.event_reserve.to') }}: <input  value="{{ is_filter_search($filter, 'to_created_at') }}" name="to_created_at" style="width: 140px;display: inline-block;" type="text" class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-analytics.event_reserve.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-analytics.event_reserve.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-analytics.event_reserve.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('admin-analytics.event_reserve.created_at') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.manager') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.currency') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.type') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.event') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.value_from') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.value_to') }}</th>
                    <th>{{ __('admin-analytics.event_reserve.comment') }}</th>
                </tr>
                </thead>
                <tbody>
                @if(count($items) > 0)
                    @foreach($items as $item)
                        <tr>
                            <td>
                                <div class=""> {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}</div>
                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                            </td>

                            <td>
                                <div>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->id_user}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                </div>
                                <small>{{$item->user->email}}</small>
                            </td>
                            <td>
                                {{$item->currency->payment->name}} {{$item->currency->code_currency->name}}
                            </td>

                            <td>
                                @if($item->type == 0)
                                    <span class="text-danger"><i class="far fa-level-down-alt"></i> &nbsp;Уменьшен</span>
                                @else
                                    <span class="text-success"><i class="far fa-level-up-alt"></i> &nbsp;Пополнен</span>
                                @endif
                            </td>

                            <td>{{$item->text}}</td>
                            <td>{{$item->value_before}}</td>
                            <td>{{$item->value_after}}</td>
                            <td style="width: 200px">
                                @if(is_null($item->comment))
                                    <small class="text-muted">{{ __('admin-analytics.event_reserve.undefined') }}</small>
                                @else
                                    {{$item->comment}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-analytics.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('pagination')
    <div class="pagination-right">
        {!! $items->links() !!}
    </div>
@endsection
