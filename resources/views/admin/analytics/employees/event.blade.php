@extends('admin.layouts.app')

@section('title','Событии менеджера '.$user->name)

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees.event', $user))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Событии менеджера "{{ $user->name }}"</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="pagination-right">
            </div>
            <table class="table table-border-2 cbtable">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>Заявка №</th>
                    <th>Тип</th>
                    <th>Бонусы</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $item)
                    <tr>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            <a href="{{config('admin.directory')}}/applications/details/{{$item->id_task}}?actions=arhive">
                                №{{$item->id_task}}
                            </a>
                        </td>
                        <td>
                            @if($item->type == 1)
                                Процент от заявки
                            @endif
                        </td>
                        <td>
                            {{ number_format($item->amount, 0,'.',' ') }} руб
                        </td>

                        <td>
                            <a href="?delete={{$item->id}}">Удалить</a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
