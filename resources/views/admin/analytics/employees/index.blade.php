@extends('admin.layouts.app')

@section('title','Сотрудники')

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees'))

@section('top-block')
    <md-button data-toggle="modal" href="#fine" class="md-warn btn-float" layout="column">
        <md-icon md-font-icon="fa fa-ban text-danger"></md-icon>
        <span>Выписать штраф</span>
    </md-button>

    <md-button ng-href="employees/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить сотрудника</span>
    </md-button>
@endsection

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Сотрудники</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="pagination-right">
            </div>
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Менеджер</th>
                    <th>Месячная зарплата</th>
                    <th>Штрафов за месяц</th>
                    <th>Событий за месяц</th>
                    <th>Процент от заявок</th>
                    <th>Статус</th>
                    <th>Итого</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($employees) > 0)
                    @foreach($employees as $item)
                        <tr @if($item->status == 2)style="background: #fddada;" @endif>
                            <td>
                                <div>
                                    <a href="{{config('admin.directory')}}/account/users?action=filter&id={{$item->user->id}}&sorting=id"><b>{{$item->user->name}}</b></a>
                                </div>
                                <small>{{$item->user->email}}</small>
                            </td>
                            <td>{{ number_format($item->salary, 0,'.',' ') }} руб</td>
                            <td align="right">
                                @if($last_month)
                                    <a href="employees/{{ $item->id }}/fine?last_month=true">
                                        {{ $item->fine_employees->count() }}
                                        <b style="color: #a23232;">(-{{$item->fine_employees()->sum('amount')}} руб)</b>
                                    </a>
                                @else
                                    <a href="employees/{{ $item->id }}/fine">
                                        {{ $item->fine_employees->count() }}
                                        <b style="color: #a23232;">(-{{$item->fine_employees()->sum('amount')}} руб)</b>
                                    </a>
                                @endif
                            </td>

                            <td align="right">
                                @if($last_month)
                                    <a href="employees/{{ $item->id }}/event?last_month=true">
                                        {{ $item->event_employees->count() }}
                                        <b style="color: #39a232;">(+{{$item->event_employees()->sum('amount')}} руб)</b>
                                    </a>
                                @else
                                    <a href="employees/{{ $item->id }}/event">
                                        {{ $item->event_employees->count() }}
                                        <b style="color: #39a232;">(+{{$item->event_employees()->sum('amount')}} руб)</b>
                                    </a>
                                @endif
                            </td>
                            <td>{{ $item->percent }}%</td>
                            <td>
                                @if($item->status == 1)
                                    <span class="text-success">Работает</span>
                                @else
                                    <span class="text-danger">Уволен</span>
                                @endif

                            </td>
                            <th>
                                {{number_format($item->total(),2,'.',' ')}} руб
                            </th>


                            <td style="width: 100px">

                                <ul class="breadcrumb-elements" hide-xs layout="row" layout-align="center center">

                                    <li class="dropdown">
                                        <md-button href="#" class="md-icon-button" data-toggle="dropdown">
                                            <i class="far fa-ellipsis-v font-size-16"></i>
                                        </md-button>

                                        <ul class="dropdown-menu dropdown-menu-left">
                                            <li><a ng-href="employees/{{$item->id}}/edit">Изменить</a></li>
                                            <li class="divider"></li>
                                            <li><a ng-href="employees/{{$item->id}}/cards">Банковские карты</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />Список пуст</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    @if($last_month == true)
        <md-button class="md-primary text-center md-raised" href="?">Вернуть назад</md-button>
    @else
        <md-button class="md-warn text-center md-raised" href="?last_month=true">Отчет за прошлый месяц {{ \Carbon\Carbon::today()->subMonth()->startOfMonth()->toDateString() }}</md-button>
    @endif

    <form action="?actions=fine" method="post"  class="form-horizontal">
        @csrf
        <div class="modal fade" id="fine" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Штрафовать менеджера</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Менеджер</label>
                            <div class="col-sm-9">
                                <select class="selectpicker" name="employees">
                                    <option value="0">-- Не выбрано --</option>
                                    @foreach($employees as $item)
                                        <option value="{{$item->id}}">{{$item->user->name}} ({{$item->user->email}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Сумма штрафа (руб)</label>
                            <div class="col-sm-9">
                                <input type="number" name="amount" class="form-control" id="inputEmail3" placeholder="Укажите сумму штрафа в руб" value="0">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Причина:</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="text" placeholder="Укажите причину штрафа:"></textarea>
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label for="inputEmail4" class="col-sm-3 control-label">Номер заявки:</label>
                            <div class="col-sm-9">
                                <input type="number" name="number" class="form-control" id="inputEmail4" placeholder="Укажите номер заявки (не обязательно)">
                                <div class="form-check">
                                    <input id="checkbox-0" type="checkbox" name="amount_task" class="form-check-input" value="1">
                                    <label for="checkbox-0" class="help-block form-check-label">Штрафовать на сумму которая указана в заявке</label>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Применить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
