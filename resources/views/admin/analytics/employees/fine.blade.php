@extends('admin.layouts.app')

@section('title','Штрафы менеджера '.$user->name)

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees.fine', $user))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Штрафы менеджера "{{ $user->name }}"</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="pagination-right">
            </div>
            <table class="table table-border-2 cbtable">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>Событие</th>
                    <th>Сумма штрафа</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fines as $item)
                    <tr>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            @if($item->number != 0)
                                <a href="{{config('admin.directory')}}/applications/details/{{$item->number}}?actions=arhive">
                                    <small>№{{$item->number}}</small>
                                </a>
                                <br />
                            @endif
                            @if($item->text != null)
                                {!! $item->text !!}
                            @else
                                <div class="text-danger">Причина не указано</div>
                            @endif
                        </td>
                        <td>{{ number_format($item->amount, 0,'.',' ') }} руб</td>
                        <td>
                            <a href="?delete={{$item->id}}">Удалить</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection