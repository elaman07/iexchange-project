@extends('admin.layouts.app')

@section('title','Отчет по сотрудниками')

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees'))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>Отчет по сотрудниками</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="pagination-right">
            </div>
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>Менеджер</th>
                        <th>Права</th>
                        <th>Статистика за сегодня</th>
                        <th>Статистика за неделю</th>
                        <th>Статистика за месяц</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $item)
                        <tr>
                            <td>
                                <div>
                                    <a href="{{ admin_base_path('/account/users?action=filter&id='.$item->id) }}"><b>{{$item->name}}</b></a>
                                </div>
                                <small>{{ $item->email }}</small>
                            </td>
                            <td>{{$item->roles()->pluck('name')->implode(', ')}}</td>
                            <td align="right">
                                <div>
                                    <span data-toggle="tooltip" data-placement="top" title="Выполненные заявки за сегодня">
                                        {{ $item->countStatTask(4,'today') }}
                                    </span>
                                    /
                                    <span data-toggle="tooltip" data-placement="top" title="Отклоненные заявки за сегодня">
                                        {{ $item->countStatTask(5,'today') }}
                                    </span>
                                    (<b style="color: #41719a;">{{ $item->totalStatTask('today') }} руб.</b>)
                                </div>
                            </td>
                            <td align="right">
                                <div>
                                    <span data-toggle="tooltip" data-placement="top" title="Выполненные заявки за неделю">
                                        {{ $item->countStatTask(4, 'week') }}
                                    </span>
                                    /
                                    <span data-toggle="tooltip" data-placement="top" title="Отклоненные заявки за неделю">
                                        {{ $item->countStatTask(5, 'week') }}
                                    </span>
                                    (<b style="color: #41719a;">{{ $item->totalStatTask('week') }} руб.</b>)
                                </div>
                            </td>

                            <td align="right">
                                <div>
                                    <span data-toggle="tooltip" data-placement="top" title="Выполненные заявки за месяц">
                                        {{ $item->countStatTask(4, 'month') }}
                                    </span>
                                    /
                                    <span data-toggle="tooltip" data-placement="top" title="Отклоненные заявки за месяц">
                                        {{ $item->countStatTask(5, 'month') }}
                                    </span>
                                    (<b style="color: #41719a;">{{ $item->totalStatTask('month') }} руб.</b>)

                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection
