@extends('admin.layouts.app')

@section('title','Изменить детали сотрудника '.$item->user->name)

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees.edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            {{csrf_field()}}
            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">Начало работы</label>
                <div class="col-sm-4">
                    <input type="text" name="started_work" class="datetimepicker form-control"
                           id="sign"
                           value="{{$item->started_work}}"
                           placeholder="Дата вступления в должность (не обязательно)">
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">График работы (c - по)</label>
                <div class="col-sm-2">
                    <select class="selectpicker" name="job_from" data-width="100%" required>
                        <option value="1" @if($item->job_from == 1) selected @endif>Понедельник</option>
                        <option value="2" @if($item->job_from == 2) selected @endif>Вторник</option>
                        <option value="3" @if($item->job_from == 3) selected @endif>Среда</option>
                        <option value="4" @if($item->job_from == 4) selected @endif>Четверг</option>
                        <option value="5" @if($item->job_from == 5) selected @endif>Пятница</option>
                        <option value="6" @if($item->job_from == 6) selected @endif>Суббота</option>
                        <option value="7" @if($item->job_from == 7) selected @endif>Воскресенье</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="selectpicker" name="job_to" data-width="100%" required>
                        <option value="1" @if($item->job_to == 1) selected @endif>Понедельник</option>
                        <option value="2" @if($item->job_to == 2) selected @endif>Вторник</option>
                        <option value="3" @if($item->job_to == 3) selected @endif>Среда</option>
                        <option value="4" @if($item->job_to == 4) selected @endif>Четверг</option>
                        <option value="5" @if($item->job_to == 5) selected @endif>Пятница</option>
                        <option value="6" @if($item->job_to == 6) selected @endif>Суббота</option>
                        <option value="7" @if($item->job_to == 7) selected @endif>Воскресенье</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Ежемесячная оплата</label>
                <div class="col-sm-4">
                    <input type="number" name="salary" value="{{$item->salary}}" class="form-control" placeholder="Укажите ежемесячную зарплату менеджера" required>
                </div>
            </div>

            <hr />
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Процент от заявок</label>
                <div class="col-sm-4">
                    <input type="number" name="percent" class="form-control" value="{{$item->percent}}" required>
                    <small style="margin-top: 1px"><b>0</b> - отключает процент</small>
                </div>
            </div>

            <hr />

            <div class="form-group">
                <label for="bonus_rub" class="col-sm-2 control-label">Бонус от заявок (в руб)</label>
                <div class="col-sm-4">
                    <input id="bonus_rub" type="number" name="bonus_rub" class="form-control" value="{{$item->bonus_rub}}" required>
                    <small style="margin-top: 1px"><b>0</b> - отключает опции</small>
                </div>
            </div>

            <hr />

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Статус</label>
                <div class="col-sm-4">
                    <select class="selectpicker" name="status" data-width="100%" required>
                        <option value="1" @if($item->status == 1) selected @endif>Работает</option>
                        <option value="2" @if($item->status == 2) selected @endif>Уволен</option>
                    </select>
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/analytics/employees') }}">Назад</md-button>
        </div>
    </form>

@endsection
