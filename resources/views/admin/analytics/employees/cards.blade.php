@extends('admin.layouts.app')

@section('title','Банковские карты менеджера '.$user->name)

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees.cards', $user))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Событии менеджера "{{ $user->name }}"</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="pagination-right">
            </div>
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>Номер карты</th>
                    <th>Прикреплена к валюте</th>
                    <th>Оборот в месяц</th>
                </tr>
                </thead>
                <tbody>
                @if(count($items) > 0)
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->created_at }}</td>
                            <td>
                                {{ $item->account_number }}
                            </td>
                            <td>
                                {{ $item->currency->payment->name }} {{ $item->currency->code_currency->name }}
                            </td>
                            <td>
                                {{ number_format($item->tasks_many->where('created_at', '>=', \Carbon\Carbon::today()->startOfMonth())->where('status', 4)->sum('give_price'), 0,'.',' ') }} руб
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
