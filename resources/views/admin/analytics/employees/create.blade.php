@extends('admin.layouts.app')

@section('title','Добавить нового сотрудника')

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.employees.create'))

@section('content')

    <form class="form-horizontal" method="post" action="?">
        <div class="default-panel-body">
            {{csrf_field()}}
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Менеджер</label>
                <div class="col-sm-4">
                    <select class="selectpicker" name="manager">
                        <option value="0">-- Не выбрано --</option>
                        @foreach($admins as $item)
                            <option value="{{$item->id}}">{{$item->name}} ({{$item->email}})</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">Начало работы</label>
                <div class="col-sm-4">
                    <input type="text" name="started_work" class="datetimepicker form-control"
                           id="sign" placeholder="Дата вступления в должность (не обязательно)">
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">График работы (c - по)</label>
                <div class="col-sm-2">
                    <select class="selectpicker" name="job_from" data-width="100%" required>
                        <option value="1">Понедельник</option>
                        <option value="2">Вторник</option>
                        <option value="3">Среда</option>
                        <option value="4">Четверг</option>
                        <option value="5">Пятница</option>
                        <option value="6">Суббота</option>
                        <option value="7">Воскресенье</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="selectpicker" name="job_to" data-width="100%" required>
                        <option value="1">Понедельник</option>
                        <option value="2">Вторник</option>
                        <option value="3">Среда</option>
                        <option value="4">Четверг</option>
                        <option value="5">Пятница</option>
                        <option value="6">Суббота</option>
                        <option value="7">Воскресенье</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Ежемесячная оплата</label>
                <div class="col-sm-4">
                    <input type="number" name="salary" class="form-control" placeholder="Укажите ежемесячную зарплату менеджера" required>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Процент от заявок</label>
                <div class="col-sm-4">
                    <input type="number" name="percent" class="form-control" value="0" required>
                    <small style="margin-top: 1px"><b>0</b> - отключает процент</small>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Бонус от заявок (в руб)</label>
                <div class="col-sm-4">
                    <input type="number" name="bonus_rub" class="form-control" value="0" required>
                    <small style="margin-top: 1px"><b>0</b> - отключает опции</small>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">Статус</label>
                <div class="col-sm-4">
                    <select class="selectpicker" name="status" data-width="100%" required>
                        <option value="1">Работает</option>
                        <option value="2">Уволен</option>
                    </select>
                </div>
            </div>



        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Добавить</md-button>
            <md-button ng-href="{{ admin_base_path('/analytics/employees') }}">Назад</md-button>
        </div>
    </form>

@endsection
