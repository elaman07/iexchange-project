@extends('admin.layouts.app')

@section('title', __('admin-analytics.exchange.title'))

@section('breadcrumbs', Breadcrumbs::render('admin.analytics.exchange'))

@section('no-block-content')
    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-analytics.exchange.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th> {{ __('admin-analytics.exchange.created_at') }}</th>
                    <th> {{ __('admin-analytics.exchange.total_to_rub') }}</th>
                    <th> {{ __('admin-analytics.exchange.total_to_usd') }}</th>
                    <th> {{ __('admin-analytics.exchange.manager') }}</th>
                    <th> {{ __('admin-analytics.exchange.detail') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($items) > 0)
                    @foreach($items as $item)
                        <tr>
                            <th>{{\Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y')}}</th>
                            <td>{{number_format($item->rub, 0,'.', ' ')}} руб</td>
                            <td>{{number_format($item->usd, 0,'.', ' ')}} $</td>
                            <td>
                                @foreach(\App\Models\User::role(\Spatie\Permission\Models\Role::all())->get() as $user)
                                    @if($user->managerHistory('count', $item->created_at) > 0)
                                        <div>
                                            <a href="{{ admin_base_path('/account/users?action=filter&id='.$user->id) }}"><b>{{ $user->name }}</b></a>
                                            -
                                            <small>{{ __('admin-analytics.exchange.order_label') }}: {{ $user->managerHistory('count', $item->created_at) }}</small>
                                        </div>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <a href="{{ admin_base_path('/applications?status[]=4&updated_at='.\Carbon\Carbon::parse($item->updated_at)->startOfDay()) }}">
                                    {{ __('admin-analytics.exchange.list_order') }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9"><p align="center"><br />{{ __('admin-analytics.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $items->links() !!}
    </div>
@endsection
