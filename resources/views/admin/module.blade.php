@extends('admin.layouts.app')

@section('title', __('Список плагинов'))

@section('breadcrumbs', Breadcrumbs::render('admin.plugins'))

@section('x_panel_class' , 'new-x_panel_wrapper')
@section('content')

    <style>

        .module-version {
            padding: 5px 0 0 0;
            font-weight: 500;
        }

        .module-description {
            font-size: 14px;
        }

        .module-name {
            font-weight: 600;
        }

        .module-status {
            padding: 5px 0 0 0;
            display: block;
            font-weight: 500;
        }

        .table {
            margin-bottom: 0;
        }

        .table-disable-tr {
            background-color: rgb(255 197 197 / 12%);
            border-left: 2px solid #dc0000 !important;
        }


    </style>

    <table class="table table-border-2">
        <thead>
        <tr>
            <th></th>
            <th>{{ __('Заголовок') }}</th>
            <th>{{ __('Описание') }}</th>
            <th>{{ __('Автор') }}</th>
        </tr>

        </thead>
        <tbody>


        @if($modules_count > 0)
            @foreach($modules as $module)
                <tr @if(!$module->isEnabled()) class="table-disable-tr" @endif>

                    <td>
                        @if($module->get('icon'))
                            <img width="50"  src="/storage/plugins/{{ $module->get('icon') }}"/>
                        @else
                            <img width="50" src="/storage/plugins/default_icon.png"/>
                        @endif
                    </td>

                    <td>
                       <div class="module-name">
                           {{ $module->get('title') }}
                           @if($module->get('version'))
                               v.{{ $module->get('version') }}
                           @endif
                           &nbsp;
                           @if($module->isEnabled())
                               <i class="fas fa-check-circle text-success" data-toggle="tooltip" data-title="Плагин включен"></i>
                           @else
                               <i class="fas fa-exclamation-circle text-danger" data-toggle="tooltip" data-title="Плагин отключен"></i>
                           @endif

                       </div>
                        @if($module->isEnabled())
                            <a class="module-status" href="?action=disable&module={{ $module->getName() }}">{{ __('Отключить') }}</a>
                        @else
                            <a class="module-status" href="?action=enable&module={{ $module->getName() }}">{{ __('Включить') }}</a>
                        @endif
                    </td>
                    <td>
                        <div class="module-description">
                            @if($module->get('description'))
                                {{ $module->get('description') }}
                            @else
                                {{ __('Нет описания') }}
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="module-author">
                            @if($module->get('author'))
                                {{ $module->get('author') }}
                            @else
                                iEXExchanger
                            @endif
                        </div>
                    </td>

                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
