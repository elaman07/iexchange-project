<?php


return [

    'level' => [
        'add_limit' => 'Добавить лимит',
        'manager' => 'Менеджер',
        'groups' => 'Группа',
        'status' => 'Статус',
        'edit_limit' => 'Изменить лимит',
        'limit_operator' => 'Лимиты для операторов',
        'group_limit' => 'Группы лимитов',
        'created_at' => 'Дата добавления',
        'add_group' => 'Добавить группу',
        'name' => 'Название',
        'from_limit' => 'Суммы обменов (От)',
        'to_limit' => 'Суммы обменов (До)',
        'edit_group' => 'Изменить группу'
    ],

    'logs' => [
        'log_autopayment' => 'Лог автовыплат',
        'id_order' => 'ID заявки',
        'created_at' => 'Дата создания',
        'type' => 'Тип',
        'event' => 'Событие',
        'error' => 'Ошибка',
        'done' => 'Успешно',
        'log_check_payment' => 'Лог проверки оплат',
        'provider' => 'Провайдер',
        'event_type' => 'Тип события',
        'log_merchant' => 'Лог мерчантов',
        'settings' => 'Настройки',
        'redirect' => 'Переадресация',
        'cancel' => 'Отмена',
        'settings_log_merchant' => 'Настройки лога мерчантов',
        'enable_log_merchant' => 'Включить лог мерчантов'
    ],

    'settings' => 'Настройки',
    'filter' => 'Фильтры',
    'show' => 'Показать',
    'hide' => 'Скрыть',
    'save_filter' => 'Применить фильтры',
    'clear_filter' => 'Очистить фильтры',
    'disable' => 'Отключено',
    'enable' => 'Включено',
    'save' => 'Сохранить',
    'back' => 'Назад',
    'list_empty' => 'Список пуст',
    'yes' => 'Да',
    'no' => 'Нет'
];