<?php

return [

    'bonus' => [
        'add_program_title' => 'Добавить программу',
        'program_name' => 'Программа',
        'name' => 'Название',
        'created_at' => 'Дата создания',
        'name_program_hint' => 'Название программы',
        'write_english' => 'Укажите на английском',
        'output_register' => 'Выдать сразу после регистрации',
        'summa_plus' => 'Сумма больше',
        'title_label' => 'Заголовок',
        'title_label_hint' => 'Заголовок программы: Пример(Новичек,Серебряный партнёр ..)',
        'percent' => 'Процент',
        'description' => 'Описание',
        'edit_program_title' => 'Изменить программу :name',
        'bonus_program' => 'Бонусные программы'
    ],


    'exchanges' => [
        'title' => 'Бонусные обмены',
        'id_order' => 'ID заявки',
        'id_order_from_to' => 'ID заявки (От и До)',
        'user' => 'Пользователь',
        'created_at' => 'Дата создания',
        'event' => 'Событие',
        'percent' => 'Процент',
        'cashback' => 'Кэшбэк',
        'referral' => 'Реферал',
        'cashback_disable' => 'Кэшбэк отключен',
        'partners_exchange_title' => 'Партнерские обмены',
        'groups' => 'Сгруппированные',
        'partner_money' => 'Партнер заработал',
        'partner_percent' => 'Партнерский процент'
    ],

    'exchanges_group' => [
        'title' => 'Сгруппированные партнерские обмены',
        'partner' => 'Партнер',
        'count_success_exchange' => 'Количество успешных обменов',
        'all_money_partner' => 'Всего заработал партнер',
        'details' => 'Подробнее'
    ],

    'bonus_logs' => [
        'title' => 'Не начисленные бонусы',
        'created_at' => 'Дата создания',
        'id_order' => 'ID заявки',
        'user' => 'Пользователь',
        'event' => 'Событие',
    ],

    'condition' => [
        'cashback_title' => 'Условия кэшбэка',
        'info_partner_page' => 'Информация на партнерской странице',
        'info_private_account' => 'Информация для личного аккаунта',
        'info_private_account_right' => 'Крат. информация для личного аккаунта (правый блок)',
        'monitoring_title' => 'Условия для мониторингов',
        'info_monitoring' => 'Информация для мониторингов',
        'referral_title' => 'Условия реферальной программы'
    ],

    'referral' => [
        'add_program' => 'Добавить программу',
        'program_name' => 'Программа',
        'write_english' => 'Укажите на английском',
        'program_all_user' => 'Программа доступа всем пользователям',
        'output_register' => 'Выдать сразу после регистрации',
        'title_label' => 'Заголовок',
        'title_label_hint' => 'Заголовок программы: Пример(Новичек,Серебряный партнёр ..)',
        'description' => 'Описание',
        'website' => 'Адрес сайта',
        'website_hint' => 'Адрес сайта (не реферальный адрес)',
        'percent_exchange' => 'Процент с обмена',
        'percent_exchange_hint' => 'Укажите процент с каждого обмена',
        'count_referral' => 'Количество рефералов',
        'count_referral_hint' => 'Укажите макс. количество рефералов процентной программы',
        'exchange' => 'Обмены',
        'transition' => 'Переходы',
        'statistics' => 'Статистика',
        'settings' => 'Настройки',
        'created_at' => 'Дата создания',
        'name' => 'Название',
        'link' => 'Ссылка',
        'percent' => 'Процент',
        'referral_program' => 'Реферальная программа',
        'settings_referral_program' => 'Настройки реферальной программы',
        'enable_referral_program' => 'Включить реферальную программу',
        'in_partner_money_from' => 'Начислять партнерское вознаграждение от',
        'amount_exchange' => 'Суммы обменов',
        'profit_settings_direction' => 'Прибыли в настройках направления',
        'withdraw_bonus' => 'Выплачивать бонусы',
        'withdraw_timetable' => 'Выплачивать по расписанию',
        'period_withdraw_bonus' => 'Период выплат бонусов',
        'enable_log_referral_program' => 'Включить лог реферальной программы',
        'minimum_amount_bonus' => 'Минимальная сумма бонуса',
        'maximum_amount_bonus' => 'Максимальная сумма бонуса',
        'day_limit_bonus' => 'Суточный лимит бонусов',
        'month_limit_bonus' => 'Месячный лимит бонусов',
        'life_span_referral' => 'Сроки жизни реферала',
        'time_life_span_day' => 'Время жизни cookie (в днях)',
        'minimum_withdraw' => 'Минимальная выплата',
        'notify_withdraw' => 'Уведомлять при выплате вознаграждений',
        'forever' => 'Навсегда',
        'cookie_limit' => 'Ограничить по Cookie'
    ],

    'referral_logs' => [
        'title' => 'Лог реферальной программы',
        'created_at' => 'Дата добавления',
        'partner' => 'Партнер',
        'referral' => 'Реферал',
        'text' => 'Текст',
        'type' => 'Тип',
        'bonus_done' => 'Бонус получен',
        'bonus_failed' => 'Бонус не получен',
        'new_referral' => 'Новый реферал'
    ],

    'referrals' => [
        'list_referral' => 'Список рефералов',
        'created_at' => 'Дата создания',
        'user' => 'Пользователь',
        'referral' => 'Реферал',
    ],

    'statistics' => [
        'title' =>  'Реферальная статистика',
        'transition' => 'Партнерские переходы',
        'transition_today' => 'Партнерских переходов (за сегодня)',
        'transition_week' => 'Партнерских переходов (за неделю)',
        'transition_month' => 'Партнерских переходов (за месяц)',
        'partner_register' => 'Партнерских регистраций',
        'partner_register_today' => 'Партнерских регистраций (за сегодня)',
        'partner_register_week' => 'Партнерских регистраций (за неделю)',
        'partner_register_month' => 'Партнерских регистраций  (за месяц)',
        'exchange' => 'Партнерских обменов',
        'exchange_today' => 'Партнерских обменов (за сегодня)',
        'exchange_week' => 'Партнерских обменов (за неделю)',
        'exchange_month' => 'Партнерских обменов (за месяц)'
    ],

    'transition_exchange' => [
        'title' => 'Реферальные направления',
        'referral' => 'Реферал',
        'hash' => 'Хэш',
        'direction' => 'Направление',
        'transition' => 'Переходов',
    ],

    'transition' => [
        'title' => 'Реферальные переходы',
        'direction' => 'Реф. направления',
        'clear_transition' => 'Очистить переходы',
        'user' => 'Пользователь',
        'ip_address' => 'IP Адрес',
        'created_at' => 'Дата создания',
        'user_agent' => 'User Agent',
        'referral' => 'Реферал',
        'hash' => 'Хэш',
        'transition' => 'Переходов'
    ],

    'monitoring' => [
        'title' => 'Настройки для мониторингов',
        'monitoring_info' => 'Информация для мониторингов'
    ],

    'settings' => [
        'title' => 'Настройка реферальной системы и cashback',
        'code_currency' => 'Код валюты',
        'code_currency_hint' => 'Выберите код валюты для работы с реферальной системой и системой cashback',
        'name_currency' => 'Название валюты',
        'name_currency_hint' => 'Введите название валюты для отображения',
        'symbol_currency' => 'Символ валюты',
        'symbol_currency_hint' => 'Укажите символ валюты',
        'position_view_symbol' => 'Позиция отображения символа',
        'position_view_symbol_hint' => 'Выберите позицию отображения символа валюты',
        'default' => 'Стандарт',
        'left' => 'Слево'
    ],

    'guest' => 'Гость',
    'filter' => 'Фильтры',
    'show' => 'Показать',
    'hide' => 'Скрыть',
    'yes' => 'Да',
    'no' => 'Нет',
    'add' => 'Добавить',
    'back' => 'Назад',
    'edit' => 'Изменить',
    'cancel' => 'Отменить',
    'save_filter' => 'Применить фильтры',
    'clear_filter' => 'Очистить фильтры',
    'list_empty' => 'Список пуст',
    'undefined' => 'Не определен',
    'save' => 'Сохранить',
];
