<?php

return [


    'select_theme_title' => 'Выбор оформления панели',
    'colors' => 'Цвета',
    'support' => 'Техническая поддержка',
    'script_iex' => 'Скрипт iEXExchanger',

    /***
     * Главная страница панели управлений
    */
    'home' => [
        'update_system ' => 'Система обновлений',

        'add_widget' => 'Добавить виджет',

        'desktop_title' => 'Рабочий стол',
        'add_desktop' => 'Добавить рабочий стол',
        'settings_desktop_title' => 'Настройки рабочего стола',
        'desktop_field_name' => 'Название рабочего стола',
        'desktop_field_column' => 'Столбцов',
        'desktop_field_width' => 'Ширина столбца',

        'create_new_desktop' => 'Создать новый рабочий стол',
        'setting_current_desktop' => 'Настроить текущий рабочий стол',
        'settings_all_desktop' => 'Настроить все рабочие столы',
        'clear_current_settings' => 'Сбросить текущие настройки',

        'title' =>  'Панель управления',
        'check_update' => 'Проверить обновление',
        'clear_cache' => 'Очистить кэш',
        'settings' => 'Настройки',
        'users' => 'Пользователей',
        'all_users' => 'Всего зарегистировано',
        'verified_users' => 'Верифицированных',
        'new_users_today' => 'Новых регистраций за сегодня',
        'all_clients' => 'Список клиентов',
        'attendance'    =>  'Посещаемость',
        'count_users' => 'Кол. пользователей',
        'count_view_page' => 'Кол. просмотров страниц',
        'online_users' => 'Активные пользователи сейчас',
        'statistics_google' => 'Статистика из Google Analytics',
        'count_orders' => 'Заявок',
        'count_hidden' => 'Счетчик скрыт',
        'all_count_orders' => 'Общее количество заявок',
        'successful_count_order' => 'Выполненных заявок',

        'statistics_order' => 'Статистика заявок',
        'direction_label' =>  'Направлений',
        'all_count_direction' => 'Всего направлений',
        'online_direction' => 'Активных направлений',
        'list_direction' => 'Список направлений',
        'last_event_reserve' => 'Последние события по резервам',
        'all_reserve_event' => 'Все события',
        'table_data_manager' => 'Дата/Менеджер',
        'table_currency' => 'Валюта',
        'table_value_to' => 'Значение до',
        'table_value_from' => 'Значение после',
        'reserve_reduced' => 'Уменьшен',
        'reserve_perfect' => 'Пополнен',
        'nothing_found' => 'Ничего не найдено',
        'all_amount_exchange_label' => 'Общая сумма обменов',
        'more_details' => 'Подробнее',
        'today' => 'Сегодня',
        'yesterday' => 'Вчера',
        'amount_exchange_order_label' => 'заявок',
        'desktop_label' => 'Рабочий стол',
        'general_label' => 'Общее',
        'successful_order_label' => 'Выполнен',
        'failed_order_label' => 'Отменен',
        'lasted_exchange_label' => 'Последние обмены',
        'last_exchange_id' => 'ID',
        'last_exchange_direction' => 'Направление',
        'last_exchange_status' => 'Статус',
        'last_exchange_created' => 'Создана',
        'operator_has_accepted' => ':operator принял',
        'application_rejected' => 'Заявка отклонена',
        'list_is_empty' => 'Список пуст',
        'new_clients_label' => 'Новые клиенты',
        'new_client_id' => 'ID',
        'new_client_name' => 'Имя',
        'new_client_partner' => 'Партнер',
        'new_client_last_visit' => 'Последний визит',
        'new_client_exchange' => 'Обменов',
        'new_client_email' => 'E-mail',
        'new_client_rules' => 'Права',
        'not_found' => 'Не найдено',
        'not_defined' => 'Не определен',
        'not_exchanges' => 'Нет обменов',
        'last_activity_label' => 'Последняя активность',
        'last_activity_id' => 'ID',
        'last_activity_name' => 'Имя',
        'last_activity_partner' => 'Партнер',
        'last_activity_visit' => 'Последний визит',
        'last_activity_exchange' => 'Обменов',
        'popular_direction_label' => 'Популярные направления',
        'count_exchange' => 'Количество обменов',
        'favorites_exchange_label' => 'Избранные направления',
        'type_course' => 'Тип курса',
        'position_bestchange' => 'Позиция Bestchange',
        'commission' => 'Комиссия',
        'save_button' => 'Сохранить',
        'cancel_button' => 'Отмена',
        'run_button' => 'Выполнить',
        'online_admin_users_label' => 'Пользователи в панели управления',
        'last_top_partner_label' => 'Топ 3 партнера',
        'top_partners_partner' => 'Партнер',
        'top_partners_exchange' => 'Обменов',
        'top_partners_exchange_today' => 'Обменов (за сегодня)',
        'top_partners_balance' => 'Баланс',
        'top_partners_total_earned' => 'Всего заработал',
        'about_system' => 'О системе',
        'project_based' => 'Проект работает на основе :project',
        'project_version' => 'Версия :version',
        'project_version_php' => 'Версия PHP',
        'project_development_name' => 'Разработчики продукта',
        'project_licensed_domain' => 'Лицензированные домены',
        'settings_home_page_label' => 'Настройка главной страницы',
        'allow_enable_favorites_direction' => 'Разрешить отображение избранных направлений',
        'allow_enable_partner' => 'Разрешить отображение топ партнеров',
        'allow_enable_users_admin_online' => 'Разрешить отображение пользователей в админпанели'
    ],

    /**
     * Резервные коды
     */
    'backup_codes' => [
        'title' => 'Проверка безопасности',
        'title_label' => 'Резервный код :num',
        'write_code' => 'Введите код №:num',
        'save' => 'Войти'
    ],

    /**
     * Google Authenticator
     */
    'google2fa' => [
        'title' => 'Панель управления',
        'title_label' => 'Последний этап авторизации',
        'write_code' => 'Код Google Authenticator',
        'save' => 'Войти'
    ],

    /**
     * SMS Вход
     */
    'phone_codes' => [
        'title' => 'Проверка безопасности',
        'title_label' => 'Код безопасности',
        'write_code' => 'Введите код',
        'save' => 'Войти'
    ],
];