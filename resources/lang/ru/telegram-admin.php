<?php

return [
    'start_commands' => [
        'common_reserve' => '💵 Суммарный резерв',
        'statistics' => '📊 Статистика',
        'on_website' => '🔈 Включить сайт',
        'off_website' => '⭕️ Отключить сайт'
    ],

    'commands' => [
        'start' => 'Запуск Telegram бота',
        'common_reserve' => 'Суммарный доступный резерв',
        'statistics' => 'Статистика обменов',
        'on_website' => 'Включить обменник',
        'off_website' => 'Отключить обменник',
    ],
];
