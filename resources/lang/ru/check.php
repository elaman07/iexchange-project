<?php

return [
    'amount'                => 'Сумма',
    'confirm_status'        => 'Статус подтверждения',
    'confirmations'         => 'Подтверждений',
    'course'                => 'Курс обмена',
    'exchange'              => 'Отдаете',
    'information_headline'  => 'Информация о транзакции',
    'number_order'          => 'Номер заявки',
    'receive'               => 'Получаете',
    'receive_account'       => 'Счет получателя',
    'sender_account'        => 'Счет отправителя',
    'status'                => 'Статус',
    'title'                 => ':sitename - чек заявки №:id',
    'updated_at'            => 'Время Выполнения',
    'valuta'                => 'Валюта',
];
