<?php

return [
    'new_order_title' => 'Поступила новая заявка :id',

    'messages' => [
        'order_id' => 'Заявка №',
        'course' => 'Курс',
        'created_at' => 'Дата создания',
        'wallet' => 'Кошелек',
        'undefined' => 'Не указано',
        'direction' => 'Направление',
        'give_client' => 'Отдает клиент',
        'payment_title' => 'ПС',
        'amount' => 'Сумма',
        'from_shot' => 'Со счета',
        'out_title' => 'Переводит сервис',
        'to_shot' => 'На счет',
        'user_info' => 'Информация о пользователе',
        'name' => 'Имя'
    ]
];
