<?php

return [

    /**
     * Фильтр по: IP или E-Mail
    */
    'block_ip' => [
        'title' => 'Фильтр по: IP или E-Mail',
        'add_title' => 'Добавление в фильтр IP адреса или E-Mail',
        'add_ip_email' => 'IP или E-mail',
        'add_ban_expiration' => 'Срок окончания бана',
        'add_reason_ban' => 'Причина бана',
        'reset_button' => 'Сброс',
        'save_button' => 'Сохранить',
        'title_label' => 'Список заблокированных IP адресов, E-Mail адресов',
        'table_filter' => 'Фильтр',
        'edit' => 'Редактировать',
        'unban' => 'Разблокировать',
        'list_empty' => 'Список пуст',
    ],

    /**
     * Лог авторизаций в админпанели
    */
    'admin_log' => [
        'title' => 'Лог авторизаций в админпанели',
        'filter' => 'Фильтры',
        'show' => 'Показать',
        'hide' => 'Скрыть',
        'id_user' => 'ID пользователя',
        'email_user' => 'Email пользователя',
        'exact_email' => 'Точный email',
        'ip_address' => 'IP адрес',
        'created_at' => 'Дата создания',
        'from' => 'От',
        'to' => 'До',
        'save_filter' => 'Применить фильтры',
        'clear_filter' => 'Очистить фильтры',
        'title_label' => 'Лог авторизаций в админпанели',
        'date' => 'Дата',
        'user' => 'Пользователь',
        'old_ip_address' => 'Пред. IP адрес',
        'current_ip_address' => 'Текущий IP адрес',
        'status' => 'Статус',
        'undefined' => 'Не определен',
        'success_auth' => 'Успешная авторизация',
        'failed_auth' => 'Google2fa введен неверно',
    ],

    /**
     * Лог авторизация
    */
    'log' => [
        'title' => 'Лог авторизаций',
        'filter' => 'Фильтры',
        'show' => 'Показать',
        'hide' => 'Скрыть',
        'id_user' => 'ID пользователя',
        'email_user' => 'Email пользователя',
        'exact_email' => 'Точный email',
        'ip_address' => 'IP адрес',
        'created_at' => 'Дата создания',
        'from' => 'От',
        'to' => 'До',
        'status_auth' => 'Статус авторизации',
        'save_filter' => 'Применить фильтры',
        'clear_filter' => 'Очистить фильтры',
        'title_label' => 'Лог авторизаций',
        'date' => 'Дата',
        'user' => 'Пользователь',
        'old_ip_address' => 'Пред. IP адрес',
        'current_ip_address' => 'Текущий IP адрес',
        'current_browser' => 'Текущий браузер',
        'os' => 'OS',
        'country_city' => 'Страна, Город',
        'status' => 'Статус',
        'undefined' => 'Не определен',
        'success_auth' => 'Успешная авторизация',
        'failed_auth' => 'Неудачная авторизация',
    ],

    /**
     * Права доступа
     */
    'permissions' => [
        // Добавить права
        'title_add' => 'Создать права',
        'name' => 'Название',
        'group' => 'Группа',
        'headline' => 'Заголовок',
        'description' => 'Описание',
        'permission_to_rule' => 'Назначить разрешение ролям',
        'add_button' => 'Добавить',
        'back' => 'Назад',
        'title_edit' => 'Изменить права',
        'edit_button' => 'Изменить',
        'add_permission' => 'Добавить права',
        'rule' => 'Права',
        'title' => 'Права доступа'
    ],

    /**
     * Роли
    */
    'roles' => [
        'title_add' => 'Создать группу',
        'name' => 'Название группы',
        'name_hint' => 'Краткое название группы не более 20 символов.',
        'permission_list' => 'Список разрешений',
        'add_button' => 'Добавить',
        'back' => 'Назад',
        'title_edit' => 'Группы пользователей',
        'allow_export' => 'Разрешить экпорт группы',
        'allow_export_hint' => 'Если включено, то все пользователи из этой группы будут экпортироваться',
        'edit_button' => 'Отредактировать группу',
        'title' => 'Группы пользователей',
        'add_role' => 'Добавить роль',
        'id' => 'ID',
        'users' => 'Пользователей',
        'has_access_user' => 'имеет доступ к админ панели',
        'edit' => 'Редактировать',
        'delete' => 'Удалить'
    ],

    /**
     * Реферальная статистика клиента
    */
    'referral_stat' => [
        'title' => 'Реферальная статистика клиента :name',
        'created_at' => 'Дата создания',
        'earned_month' => 'Заработано за месяц',
        'attracted_month' => 'Привлечено за месяц',
        'last_success_bid' => 'Посл. успешная заявка',
        'last_client' => 'Посл.  Клиент',
        'min_bonus_month' => 'Мин. бонус за месяц',
        'max_bonus_month' => 'Макс. бонус за месяц',
        'order_number' => 'Заявка №:id',
        'ref_bonus' => 'Реф. бонус',
        'not_found' => 'Не найдено',
        'last_activity' => 'Последняя активность',
        'undefined' => 'Не определен'
    ],

    /**
     * Рефералы клиента
    */
    'referral' => [
        'title' => 'Рефералы клиента',
        'exchange_at' => 'Дата обмена',
        'email' => 'Email',
        'placeholder' => 'Укажите E-mail адрес',
        'exchange' => 'Обменов',
        'last_exchange' => 'Посл. визит',
        'action' => 'Действие',
        'undefined' => 'Не определена',
        'throw_on' => 'Перекинуть на :name',
        'save' => 'Сохранить',
        'run' => 'Выполнить',
        'add_referral' => 'Добавить реферала',
        'close' => 'Закрыть',
    ],

    /**
     * Список пользователей
    */
    'users' => [
        'title' => 'Пользователи',
        'add_user_button' => 'Добавить пользователя',
        'settings' => 'Настройки',
        'clients_label' => 'Клиентов',
        'allow_admin_label' => 'Доступ к панели',
        'blocked_label' => 'Заблокированные',
        'verification_label' => 'Верифицированные',
        'provider_labels'    =>  'Провайдеры',
        'filters' => 'Фильтры',
        'show' => 'Показать',
        'hide' => 'Скрыть',
        'id' => 'ID',
        'name' => 'Имя',
        'email' => 'E-mail',
        'search_by_id' => 'Искать по ID',
        'search_by_name' => 'Искать по Имени',
        'exact_name_match' => 'Точное совпадение имени',
        'search_by_email' => 'Искать по E-mail адресу',
        'exact_email' => 'Точное совпадение e-mail`а',
        'ip_address' => 'IP Адрес',
        'ip_address_browser' => 'IP Адрес/Браузер',
        'search_by_ip' => 'Искать по IP адресу',
        'referral_hash_label' => 'Реферальный хэш',
        'search_by_referral_hash' => 'Искать по реферальному хешу',
        'referral_hash_hint' => 'Можно указать реферальное имя или хэш',
        'group_label' => 'Группа',
        'referral_program_label' => 'Реферальная программа',
        'filter_by_label' => 'Фильтровать ПО',
        'banned_label' => 'Забанен',
        'autopayment_bonus_label' => 'Автовыплата бонусов',
        'individual_label' => 'Индивидуальный',
        'admin_label' => 'В админпанели',
        'verify_email' => 'Верифицирован',
        'disabled_label' => 'Отключенные',
        'prohibition_order' => 'Запрет на заявки',
        'register_created_at' => 'Дата регистрации',
        'from_provider' => 'Через провайдер',
        'from_label' => 'От',
        'to_label' => 'До',
        'last_visit_at' => 'Дата посл. посещения',
        'last_auth_at' => 'Дата посл. входа',
        'last_auth_logout' => 'Дата посл. выхода',
        'count_order_label' => 'Количество заявок',
        'referral_bonus_label' => 'Реферальные бонусы',
        'cashback_bonus_label' => 'Cashback бонусы',
        'provider_label' => 'Провайдер',
        'count_user_page' => 'Количество пользователей на страницу',
        'position_soring_user' => 'Порядок сортировки пользователей',
        'sorting_by_id' => 'ID',
        'sorting_by_email' => 'E-mail',
        'sorting_by_order' => 'Заявки',
        'sorting_by_register_at' => 'Дата регистрации',
        'sorting_by_last_visit' => 'Посл. посещение',
        'save_filter' => 'Применить фильтры',
        'clear_filter' => 'Очистить фильтры',
        'list_users_count' => 'Список пользователей',
        'export_users' => 'Экспорт пользователей',
        'balance' => 'Баланс',
        'count_order' => 'Кол. обменов',
        'temporary' => 'временный',
        'from_partner' => 'От партнера',
        'last_exchange_label' => 'Посл. обмен',
        'not_email_verify' => 'Не верифицирован',
        'email_verify' => 'Верифицирован',
        'loading_order' => 'Загрузить заявки',
        'not_order_label' => 'Нет обменов',
        'referral_label' => 'Реферальные',
        'cashback_label' => 'Cashback',
        'yes' => 'Да',
        'no' => 'Нет',
        'client_blocked_to' => 'Клиент заблокирован до',
        'client_no_blocked' => 'Клиент не заблокирован',
        'matches' => 'Совпадений',
        'undefined' => 'Не определен',
        'edit_button' => 'Редактировать',
        'log_auth' => 'Лог авторизаций',
        'verification_card' => 'Верификации карт',
        'referrals_client' => 'Рефералы клиента',
        'referrals_statistics' => 'Реферальная статистика',
        'history_payout' => 'История выплат',
        'history_bans' => 'История банов',
        'control_balance' => 'Управление балансом',
        'order_user' => 'Заявки пользователя',
        'unbanned' => 'Разблокировать',
        'banned' => 'Заблокировать',
        'export_user_title' => 'Экспорт пользователей',
        'export_fields' => 'Экспортируемые поля',
        'date_last_visit' => 'Дата последнего посещения',
        'type_export_field' => 'Тип экспорта',
        'no_selected' => 'Не выбрано',
        'export_to' => 'Экпортировать в :name',
        'export_button' => 'Экспортировать',
        'cancel' => 'Отмена',
        'settings_title' => 'Настройки пользователей',
        'enable_log_auth' => 'Включить лог авторизаций',
        'default_filter' => 'По умолчанию фильтры',
        'visible' => 'Отображать',
        'hidden' => 'Скрывать',
        'save' => 'Сохранить',
        'resend' => 'Отправить повторно',
        'internal_account' => 'Внутренний счет'
    ],

    /**
     * Создание и обновление пользователей
    */
    'control' => [
        'edit_title' => 'Редактировать пользователя :name',
        'referral_hash' => 'Реферальный хэш',
        'edit_link' => 'Изменить',
        'from_partner' => 'От партнера',
        'other_user_link' => 'Другие пользователи',
        'user_agent' => 'User Agent',
        'undefined' => 'Не определено',
        'ip_address' => 'IP Адрес',
        'name' => 'Имя',
        'phone' => 'Номер телефона',
        'email' => 'Email',
        'not_verified' => 'Не верифицирован',
        'verified' => 'Верифицировать',
        'referral_program' => 'Реферальная программа',
        'limit_create_order' => 'Применить лимит перед созданием заявки',
        'yes' => 'Да',
        'no' => 'Нет',
        'new_password' => 'Новый пароль',
        'group_label' => 'Группа',
        'group_until' => 'В группе до',
        'favorite_referral' => 'Избранный реферал',
        'unique_payment_system_payout' => 'Предоставить уникальные ПС для выплаты бонусов',
        'allow_telescope' => 'Разрешить доступ к telescope',
        'allow_horizon' => 'Разрешить доступ к horizon',
        'allow_backup' => 'Разрешить доступ к Резервному копированию',
        'allow_create_order' => 'Разрешить создавать заявки',
        'status_account' => 'Статус аккаунта',
        'enable' => 'Включена',
        'disable' => 'Отключена',
        'notify_email_auth' => 'Уведомление на e-mail при авторизации',
        'enable_reset_password' => 'Включить восстановление пароля',
        'pay_referral_bonus' => 'Выплачивать реферальные бонусы',
        'pay_cashback' => 'Выплачивать cashback',
        'active_verification_shot' => 'Активировать верификацию счета',
        'vip_client' => 'VIP Клиент',
        'google_auth' => 'Google Authorization',
        'not_available_demo' => 'Недоступен в демо режиме',
        'get_new_key' => 'Получить новый ключ',
        'reserve_code' => 'Резервные коды',
        'code_activated' => 'Коды были получены ранее',
        'get_code' => 'Получить коды',
        'phone_admin' => 'Номер телефона (Для входа в админ-панель)',
        'save_settings' => 'Сохранить настройки',
        'cancel' => 'Отмена',
        'edit_referral_hash' => 'Изменить реферальных хэш',
        'referral_name' => 'Реферальное имя',
        'save' => 'Сохранить',
        'add_user' => 'Создать пользователя',
        'rule' => 'Роли',
        'password' => 'Пароль',
        'confirm_password' => 'Подтверждение пароля'
    ],

    /**
     * История профилей
    */
    'history_profiles' => [
        'title' => 'История профилей',
        'created_at' => 'Дата создания',
        'user_id' => 'Пользователь',
        'event' => 'Событие',
        'who_changed' => 'Кто изменил',
        'what_changed' => 'Что изменено',
        'url_page' => 'URL страницы',
        'ip_address' => 'IP Адрес',
        'system_name' => 'Система',
        'description' => 'Включить или выключить сохранение истории изменения профилей пользователей вы можете в настройках журнала событий.'
    ],

    /**
     * Настройки Google Auth
    */
    'google' => [
        'title' => 'Настройка Google Authenticator',
        'description' => 'Настройте свою двухфакторную аутентификацию, сканируя штрих-код ниже. Кроме того, вы можете использовать код'
    ],

    /**
     * История банов
    */
   'banned_histories' => [
       'title' => 'История банов :name',
       'id' => 'ID',
       'start_ban' => 'Начало бана',
       'end_ban' => 'Окончание бана',
       'comment' => 'Комментарий',
   ],

    /***
     * Забанить пользователя
    */
    'ban' => [
        'title' => 'Забанить пользователя :name',
        'expired_at' => 'Срок блокировки',
        'comment' => 'Причина блокировки',
        'comment_hint' => 'Опишите, по какой причине вы собираетесь заблокировать клиента...',
        'save' => 'Сохранить',
        'cancel' => 'Назад'
    ],

    /**
     * Баланс пользователя
    */
    'balance' => [
        'title' => 'Баланс пользователя :name',
        'cashback' => 'Баланс Кэшбэка',
        'referral' => 'Баланс партнерских выплат',
        'save' => 'Сохранить',
        'cancel' => 'Назад',
        'history_balance' => 'История баланса (:count)',
        'created_at' => 'Дата создания',
        'manager' => 'Менеджер',
        'type_balance' => 'Тип баланса',
        'actions' => 'Действие',
        'before' => 'Было',
        'after' => 'Стало',
        'text' => 'Текст',
        'cashback_label' => 'Кэшбэк',
        'referral_bonus' => 'Реферальный бонус',
        'deposit' => 'Пополнение',
        'withdraw' => 'Вывести'
    ],

    /**
     * История выплат
    */
    'payouts_history' => [
        'title' => 'История выплат',
        'client_not_bonus' => 'У клиента нет бонусов',
        'client_not_handler' => 'У клиента есть необработанная заявка на выплату',
        'application_for_payment' => 'Заявка на выплату',
        'payment_system' => 'Платежная система',
        'number_account' => 'Номер счета',
        'number_account_hint' => 'Введите номер счета',
        'type_payout' => 'Тип выплаты',
        'all' => 'Все',
        'referral' => 'Реферальные',
        'cashback' => 'Кэшбэк',
        'create' => 'Создать',
        'id' => 'ID',
        'currency' => 'Валюта',
        'status' => 'Статус',
        'created_at' => 'Дата создания',
        'updated_at' => 'Посл. обновление',
        'no' => 'Нет',
        'done' => 'Выполнено',
        'process' => 'В процессе...'
    ],

    /**
     * Резервные коды
    */
    'codes' => [
        'title' => 'Резервные коды для :name',
        'text' => 'Коды создаются и скачиваются тольк один раз. Храните резервные коды в надежном месте.',
        'download_code' => 'Скачать коды'
    ],

    /**
     * Настройки
    */
    'settings' => [
        'title' => 'Настройки пользователей',
        'type_balance' => 'Тип  баланса',
        'save' => 'Сохранить настройки'
    ],

    'internal_account' => [
        'title' => 'Внутренний счет :name',
        'code_currency' => 'Код валюты',
        'balance' => 'Баланс'
    ],


    'list_empty' => 'Список пуст',
];
