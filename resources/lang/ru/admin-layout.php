<?php

return [
    'title' => 'Администраторская',
    'new_order' => 'Новые заявки',
    'scam' => 'Мошенник',
    'list_postponed_order' => 'Список отложенных заявок',
    'notification_new_order' => 'Уведомление о новых заявках',
    'list_payout_bonuses' => 'Уведомление о выдаче вознаграждений',
    'notification_request_reserve' => 'Уведомление о запросе резерв',
    'verification_wallet' => 'Верификация счетов',
    'review_clients' => 'Отзывы клиентов',
    'settings' => 'Настройки',
    'job' => 'Статус работы сервиса',
    'view_site' => 'Просмотр сайта',


    'other' => 'Дополнительно',
    'activity_card' => 'Активная карта',
    'limit_today' => 'Лимит на сегодня: :limit руб',
    'logout' => 'Выход',
    'card' => 'Карта',
    'limit_today_card' => 'Превышен дневной установленный лимит',
    'list_section' => 'Список разделов',

];
