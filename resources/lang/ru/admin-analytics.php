<?php

return [
    /**
     * Список событий по резервам
    */
    'event_reserve' => [
        'title' => 'Список событий по резервам',
        'filters' => 'Фильтры',
        'show' => 'Показать',
        'hide' => 'Скрыть',
        'type_event' => 'Тип события',
        'currency' => 'Валюта',
        'created_at' => 'Дата создания',
        'from' => 'От',
        'to' => 'До',
        'save_filter' => 'Применить фильтры',
        'clear_filter' => 'Очистить фильтры',
        'manager' => 'Менеджер',
        'type' => 'Тип',
        'event' => 'Событие',
        'value_from' => 'Значение до',
        'value_to' => 'Значение после',
        'comment' => 'Комментарий',
        'reduced' => 'Уменьшен',
        'increased' => 'Пополнен',
        'undefined' => 'Не указано',
    ],

    /**
     * Общая сумма обменов
    */
    'exchange' => [
        'title' => 'Общая сумма обменов',
        'created_at' => 'Дата',
        'total_to_rub' => 'Сумма обменов в RUB',
        'total_to_usd' => 'Сумма обменов в USD',
        'manager' => 'Заявки выполняли',
        'detail' => 'Детали',
        'order_label' => 'заявок',
        'list_order' => 'Список заявок'
    ],

    /**
     * Сводный отчет
    */
    'summary' => [
        'title' => 'Сводный отчет',
        'settings' => 'Настройки',
        'audience_overview' => 'Обзор аудитории',
        'users_label' => 'Пользователи',
        'new_user_label' => 'Новые пользователи',
        'session_label' => 'Сеансы',
        'session_per_user_label' => 'Сеансов на пользователя',
        'page_view_label' => 'Просмотры страниц',
        'page_view_session_per_user' => 'Страниц/сеанс',
        'avg_session' => 'Сред. длительность сеанса',
        'bounce_rate' => 'Показатель отказов',
        'clients' => 'Посетители',
        '7day' => '7 дн.',
        'user_come_from' => 'Откуда приходят пользователи',
        'website_page' => 'Адрес страницы',
        'views' => 'Просмотры',
        'pages_user_view' => 'Какие страницы просматривают пользователи?',
        'website_label' => 'Адрес сайта',
        'page_view' => 'Переходов',
        'browser' => 'Браузеры',
        'report_archive' => 'Архив отчетов',
        'clear' => 'Очистить',
        'created_at' => 'Дата создания',
        'description' => 'Описание',
        'action' => 'Действие',
        'total_report' => 'Итоговый отчет',
        'open' => 'Открыть',
        'download' => 'Скачать',
        'settings_report_title' => 'Настройки сводного отчета',
        'is_auto_reserves_report' => 'Включить автоматический отчет по резервам',
        'yes' => 'Да',
        'no' => 'Нет',
        'report_analytics_status' => 'Разрешить отправить уведомление на E-mail',
        'report_analytics_email' => 'E-mail адрес для уведомлений',
        'save' => 'Сохранить',
        'back' => 'Отмена'

    ],



    'disable_demo_version' => 'Недоступно в демо режиме',
    'list_is_empty' => 'Список пуст',
];