<?php

return [

    'list_address' => 'Список адресов',
    'address' => 'Адрес',
    'provider' => 'Провайдер',
    'key' => 'Ключ',
    'order_id' => '№ заявки',
    'not_data' => 'не определен'
];