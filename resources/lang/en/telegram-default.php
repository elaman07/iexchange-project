<?php

return [
    'new_order_title' => 'New order received :id',

    'messages' => [
        'order_id' => 'Order ID',
        'course' => 'Rate',
        'created_at' => 'Created At',
        'wallet' => 'Wallet',
        'undefined' => 'No data',
        'direction' => 'Direction',
        'give_client' => 'Gives the client',
        'payment_title' => 'PS',
        'amount' => 'Amount',
        'from_shot' => 'From shot',
        'out_title' => 'The client receives',
        'to_shot' => 'To shot',
        'user_info' => 'User information',
        'name' => 'Name'
    ]
];
