<?php

return [
    'hello_user' => 'Hello :name',
    'create_order_desc' => 'Dear customer, you have created an order',
    'order_info_title' => 'Order Information',
    'order_status'    => 'Order status',
    'order_rate'  => 'Exchange rate',
    'city_country' => 'Country/City',
    'transfer_to' => 'Transfer to account',
    'in' => 'In',
    'account_number' => 'Account number',
    'empty_shot' => 'Not indicated',
    'out' => 'Out',
    'created_at' => 'Date of operation',
    'direction_exchange' => 'Exchange direction',
    'open_order' => 'Open order',
    'description' => 'Description',

    'application_accepted'  =>  [
        'subject'   =>  'Your order is accepted!'
    ],

    'failed_step_auth' => 'Failed authorization attempt',
    'lockout_desc' => 'Dear customer, we have detected a large number of failed authorizations and for security purposes your account has been temporarily blocked..',
    'detail' => 'Details',
    'last_date_attempts' => 'Last date attempts',
    'sign_new_device' => 'Sign in from a new device',
    'sign_new_device_desc' => 'You are signed in to your :sitename account from a new device or location.',
    'recount_order' => 'Recalculation of the order №:id',
    'you_get' => 'You receive',
    'new_rate' => 'New rate',
    'recount_desc' => 'Dear client, your application has been recalculated due to the fact that you did not manage to pay it within the set time',
    'status_your_order' => 'Status of your order',
    'restore_desc' => 'Dear client, order <b>№:id</b> has been restored and is being processed',
    'order_success' => 'Your order has been successfully processed',
    'order_ares' => 'Your order has been suspended',
    'order_postponed' => 'Your order is on hold',
    'order_delete' => 'Your order has been deleted',
    'you_order_success_desc' => 'Your order <b>№:id</b> has been successfully processed. Funds are transferred to your details.',
    'link_check' => 'Link to check',
    'order_status_message' => 'Thank you for the perfect exchange!<br />
We will be very happy if you leave a review about our service on any site convenient for you..',
    'social_link_title' => 'In social networks',
    'sites_link_title' => 'On sites',
    'cause' => 'Cause',
    'fraudulent_activities' => 'Fraudulent activities',
    'order_status_desc' => 'If you are sure that the application was deleted by mistake, immediately contact the support service on our website or write to us by mail',
    'connect_operator' => 'Contact the operator',
    'order_status_debtors' => 'You are on the list of debtors and your application is frozen until the reasons are clarified and the losses incurred by the exchangers are compensated',
    'footer_email_support' => 'If you have any questions, you can contact the support service on our website or write to us by mail',
    'verify_title' => 'Account activation',
    'verify_text' => 'You have successfully registered on the site',
    'access_account' => 'Account access',
    'verify_desc' => 'Use your e-mail as a login.<br/>
You can change your password in your personal account settings..',
    'verify_notify_account' => 'In order to get full access to your personal account, you need to activate your account.
After activation, you will be able to enjoy all the benefits of the service',
    'track_status' => 'Track the status of all your order',
    'referral_discount' => 'Participate in the referral program and discount systems',
    'verify_button_text' => 'Please follow the link to activate your account and get full access:',
    'verify_button' => 'Activate account',
    'register_on_site' => 'Registration on the site'
];
