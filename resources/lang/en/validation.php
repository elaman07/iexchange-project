<?php

return [
    'accepted'              => 'You should take :attribute.',
    'phone'                 => 'The :attribute field contains an invalid number.',
    'password'              => 'The password is incorrect.',
    'active_url'            => 'Field :attribute contains an invalid URL.',
    'after'                 => 'In the field :attribute must be a date after :date.',
    'after_or_equal'        => 'In the field :attribute must be a date after or equal to :date.',
    'alpha'                 => 'Field :attribute can only contain letters.',
    'alpha_dash'            => 'Field :attribute can only contain letters, numbers, and hyphens.',
    'alpha_num'             => 'Field :attribute can only contain letters and numbers.',
    'array'                 => 'Field :attribute should be an array.',
    'before'                => 'In the field :attribute should be the date to :date.',
    'before_or_equal'       => 'In the field :attribute must be a date before or equal to :date.',
    'between'               => [
        'array'     => 'The number of items in field :attribute should be between :min and :max.',
        'file'      => 'The file size in field :attribute must be between :min and :max K(a).',
        'numeric'   => 'Field :attribute should be between :min and :max.',
        'string'    => 'The number of symbols in field :attribute should be between :min and :max.',
    ],
    'boolean'               => 'Field :attribute must have a Boolean value.',
    'confirmed'             => 'Field :attribute is not the same as confirmation.',
    'custom'                => [
        'attribute-name'    => [
            'rule-name' => 'custom-message',
        ],
    ],
    'date'                  => 'Field :attribute is not a date.',
    'date_format'           => 'Field :attribute does not match the format :format.',
    'different'             => 'Fields :attribute and :other must be different.',
    'digits'                => 'Length numeric fields :attribute should be :digits.',
    'digits_between'        => 'Length numeric fields :attribute must be between :min and :max.',
    'dimensions'            => 'Field :attribute has invalid dimensions of the image.',
    'distinct'              => 'Field :attribute contains a duplicate value.',
    'ends_with'             => 'The :attribute must end with one of the following: :values',
    'email'                 => 'Field :attribute must be a valid email address.',
    'exists'                => 'The value chosen for :attribute is incorrect.',
    'file'                  => 'Field :attribute must be a file.',
    'filled'                => 'Field :attribute is required.',
    'image'                 => 'Field :attribute should be an image.',
    'in'                    => 'The value chosen for :attribute is wrong.',
    'in_array'              => 'Field :attribute not exist in :other.',
    'integer'               => 'Field :attribute must be an integer.',
    'ip'                    => 'Field :attribute must be a valid IP address.',
    'ipv4'                  => 'Field :attribute must be a valid IPv4 address.',
    'ipv6'                  => 'Field :attribute must be a valid IPv6 address.',
    'json'                  => 'Field :attribute must be JSON string.',
    'max'                   => [
        'array'     => 'The number of items in field :attribute cannot exceed :max.',
        'file'      => 'The file size in field :attribute cannot be more than :max K(a).',
        'numeric'   => 'Field :attribute cannot be more than :max.',
        'string'    => 'The number of symbols in field :attribute cannot exceed :max.',
    ],
    'mimes'                 => 'Field :attribute has to be one of the following file types: :values.',
    'mimetypes'             => 'Field :attribute has to be one of the following file types: :values.',
    'min'                   => [
        'array'     => 'The number of items in field :attribute must be at least :min.',
        'file'      => 'The file size in field :attribute must be at least :min K(a).',
        'numeric'   => 'Field :attribute must be at least :min.',
        'string'    => 'The number of symbols in field :attribute must be at least :min.',
    ],
    'not_in'                => 'The value chosen for :attribute is wrong.',
    'numeric'               => 'Field :attribute must be a number.',
    'present'               => 'Field :attribute must be present.',
    'regex'                 => 'Field :attribute has incorrect format.',
    'required'              => 'Field :attribute is required.',
    'required_if'           => 'Field :attribute is required when :other equals :value.',
    'required_unless'       => 'Field :attribute is required when :other not equal :values.',
    'required_with'         => 'Field :attribute is required when :values listed.',
    'required_with_all'     => 'Field :attribute is required when :values listed.',
    'required_without'      => 'Field :attribute is required when :values is not specified.',
    'required_without_all'  => 'Field :attribute is required when none of :values is not specified.',
    'same'                  => 'The value of :attribute should match :other.',
    'size'                  => [
        'array'     => 'The number of items in field :attribute should equal :size.',
        'file'      => 'The file size in field :attribute must be equals :size K(a).',
        'numeric'   => 'Field :attribute should equal :size.',
        'string'    => 'The number of symbols in field :attribute should equal :size.',
    ],
    'string'                => 'Field :attribute should be a string.',
    'timezone'              => 'Field :attribute must be a valid time zone.',
    'unique'                => 'The value field :attribute already exists.',
    'uploaded'              => 'Download fields :attribute failed.',
    'url'                   => 'Field :attribute has incorrect format.',
];
