<?php

return [
    'password'  => 'The password must be at least six characters and match the confirmation.',
    'reset'     => 'Your password has been reset!',
    'sent'      => 'Link to reset password has been sent!',
    'token'     => 'Incorrect reset code password.',
    'user'      => 'Could not find user with the specified email address.',
];
