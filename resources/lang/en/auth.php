<?php

return [
    'failed'    => 'The user name and password do not match.',
    'throttle'  => 'Too many login attempts. Please try again in :seconds seconds.',
];
