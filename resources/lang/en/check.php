<?php

return [
    'amount'                => 'The amount',
    'confirm_status'        => 'Confirmation status',
    'confirmations'         => 'Evidence',
    'course'                => 'Exchange rate',
    'exchange'              => 'Give',
    'information_headline'  => 'Information about the transaction',
    'number_order'          => 'The application number',
    'receive'               => 'Get',
    'receive_account'       => 'The recipient\'s account',
    'sender_account'        => 'The expense of the sender',
    'status'                => 'Status',
    'title'                 => ':sitename - application check №:id',
    'updated_at'            => 'Run Time',
    'valuta'                => 'Currency',
];
