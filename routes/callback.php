<?php
/*
|--------------------------------------------------------------------------
| Callbacks
|--------------------------------------------------------------------------
|
| Здесь собраны все роутерные пути для уведомлений о платежах
|
*/


// Callbacks
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'callbacks', 'namespace' => 'Callbacks'], function ()
{
    // Withdraw
    Route::prefix('webhook')->group(function() {
        Route::post('/whitebit_events/{security_hash?}', 'WebhookController@whitebit');
    });



    Route::prefix('v1')->group(function ()
    {
        /// Оплата внутреннего счета
        if(config('payment.enable_internal_account')) {
            Route::get('/internalaccount/status_account/{security_hash?}', 'InternalAccountController@status');
        }

        Route::post('/ipn/{payment_system}/{security_hash?}', 'IPNController@status')->where(['payment_system' => '[a-z]+'])->name('merchant.ipn');
        Route::match(['get', 'post'], '/receive_money/{payment_system}/{security_hash?}', 'ReceiveMoneyController@status')->name('merchant.receive_money');
    });


//    Route::prefix('v1')->group(function()
//    {
//
//        Route::post('/ipn/{payment_system}/{security_hash?}', 'IPNController@status')
//            ->where(['payment_system' => '[a-z]+'])->name('merchant.ipn');
//
//
//
////        Route::post('/{payment_system}/webhook/{security_hash?}', 'HookController@hook')->name('merchant.webhook');
////
////        Route::post('/{payment_system}/status/{security_hash?}', 'StatusController@handle')
////            ->where(['payment_system' => '[a-z]+'])->name('merchant.status');
////
////        Route::post('/{payment_system}/ipn/{security_hash?}', 'IPNStatusController@handle')
////            ->where(['payment_system' => '[a-z]+'])->name('merchant.ipn');
////
////        Route::match(['get', 'post'], '/{payment_system}/receive_money/{security_hash?}', 'ReceiveMoneyController@handler')->name('merchant.receive_money');
////
////        /// Оплата внутреннего счета
////        if(config('payment.enable_internal_account') == true) {
////            Route::get('/{payment_system}/status_account/{security_hash?}', 'InternalAccountController@handle');
////        }
//    });
});
