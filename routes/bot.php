<?php

use Illuminate\Support\Facades\Route;

Route::get('/bot/getUpdates', 'TelegramController@getUpdates');
Route::get('/bot/set', 'TelegramController@set');
Route::get('/bot/unset', 'TelegramController@uninstall');
Route::post('/bot/hook', 'TelegramController@hook');

