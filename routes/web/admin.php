<?php

use App\Http\Middleware\AdminRoles;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Панель администрирования
|--------------------------------------------------------------------------
|
| Здесь собраны все роутерные пути администраторской панели управления
| Настройка ролей, внутренняя авторизация и.т.д
|
*/


// Для
Route::group(['prefix' => 'merchant-verify', 'namespace' => 'Callbacks', 'middleware' => [AdminRoles::class, 'auth', 'security-admin:admin']], function() {
    // Яндекс.Деньги
    Route::prefix('yoomoney')->group(function() {
        Route::get('/callback', 'PrivateVerifyController@yandexCallback');
        Route::get('/{account}', 'PrivateVerifyController@yandexLink');
    });

    // Qiwi
    Route::prefix('qiwi')->group(function() {
        Route::get('/{account}', 'PrivateVerifyController@qiwiLink');
    });
});


// Авторизация в панели управления
Route::prefix(config('admin.route_path'))->middleware(['security-admin:admin'])->group(function() {
    Route::get('/authorization_login', 'AuthorizationController@showLoginForm')->name('authentication.index');
    Route::post('/authorization_login', 'AuthorizationController@login');
});


Route::post('/2faGoogleVerify', function () {
    return redirect(config('admin.directory'));
})->name('2faGoogleVerify')->middleware('2fa');

Route::post('/2fa')->name('2fa')->middleware('2fa', 'auth');

// Ввод резервного ключа
Route::get('/2faBackupCode', 'Administrator\BackupCodeController@index')->middleware(['auth', AdminRoles::class]);
Route::post('/2faBackupCode', 'Administrator\BackupCodeController@handle')->middleware(['auth', AdminRoles::class]);

// Входа по коду безопасности
Route::get('/confirmAuthOrderPage', 'Administrator\ConfirmAuthOrderPageController@index')->middleware(['auth', AdminRoles::class]);
Route::post('/confirmAuthOrderPage', 'Administrator\ConfirmAuthOrderPageController@handle')->middleware(['auth', AdminRoles::class]);


// Вход по номеру телефона
Route::get('/2faPhone', 'Administrator\AdminPhoneController@index')->middleware(['auth', AdminRoles::class]);
Route::post('/2faPhone', 'Administrator\AdminPhoneController@handle')->middleware(['auth', AdminRoles::class]);

Route::namespace('Administrator')->group(function()
{
    $adminRoles = [AdminRoles::class, 'auth', 'security-admin:admin', 'admin.monitoring'];
    if((int)iEXSetting('is_google_auth') == 1)
        $adminRoles = array_merge($adminRoles, ['2fa']);

    if(config('admin.is_backup_code')) {
        $adminRoles = array_merge($adminRoles, ['2fa-code']);
    }

    if(iEXSetting('enable_admin_sms_notification'))
        $adminRoles = array_merge($adminRoles, ['2fa-phone']);

    // Отключаем в демо режиме
    if(config('admin.is_demo_mode'))
        $adminRoles = array_merge($adminRoles, ['admin.demo']);


    //'auth','web','role:Разработчик|Администратор|Модератор|Служба поддержки'
    Route::middleware($adminRoles)->group(function() {
        //Динамическое создание имени администраторской
        Route::prefix(config('admin.route_path'))->group(function() {

            // API
            Route::group(['prefix' => 'api'], function() {
                Route::resource('/', 'APIController');
                Route::get('/logs', 'APILogsController@index');
            });

            // Добавление ссылок в избранное
            Route::group(['prefix' => 'favorites'], function() {
               Route::get('/add', 'FavoritesController@add');
               Route::get('/{id}/delete', 'FavoritesController@delete');
                Route::get('/control', 'FavoritesController@control');
                Route::post('/control', 'FavoritesController@controlPost');
                Route::post('/sorting', 'FavoritesController@sorting');
            });

            // Создание рабочего стола
            Route::post('/widgets/create_desktop', 'WidgetController@create_desktop');
            // Настройка рабочего стола
            Route::put('/widgets/update_desktop/{id}', 'WidgetController@update_desktop');
            // Редактирование рабочего стола
            Route::get('/widgets/edit_desktop/{id}', 'WidgetController@edit_desktop');
            // Удаление рабочего стола
            Route::get('/widgets/destroy_desktop/{id}', 'WidgetController@desktop_destroy');

            // Сбросить все настройки
            Route::get('/widgets/reset', 'WidgetController@reset');

            // Настройка всех рабочих столов
            Route::get('/widgets/config_desktops', 'WidgetController@config_desktops');

            // Редактирование виджета
            Route::get('/widgets/{id_desktop}/{id}/add', 'WidgetController@add');

            // Редактирование виджета
            Route::get('/widgets/{hash_id}/edit', 'WidgetController@edit');
            // Редактирование виджета
            Route::put('/widgets/{hash_id}/edit', 'WidgetController@update');

            // Удаляем виджет
            Route::get('/widgets/{hash_id}/delete', 'WidgetController@delete');

            Route::post('/widgets/desktop/sorting', 'WidgetController@sorting_desktops');
            Route::post('/widgets/{id_desktop}/{column}/sorting', 'WidgetController@sorting');

            Route::prefix('tx')->middleware('confirm-auth-order-page')->namespace('Applications')->group(function() {
                Route::match(['get', 'post'], '/', 'MTApplicationController@index')->name('admin.application.tx');
                Route::match(['get','post','put','delete'],'/{id}', 'MTApplicationController@detail');
            });


            // Список модулей
            Route::get('/modules', 'ModuleController@index');

            Route::match(['get','post','put'], '/', 'HomeController@index');

            // Система обновлений
            Route::group(['prefix' => 'update_system', 'middleware' => ['permission:admin_update_system', 'demo-route']], function() {
                Route::get('/', 'UpdateSystemController@index');
                Route::post('/', 'UpdateSystemController@store');
            });


            Route::group(['prefix' => 'basic', 'namespace' => 'Basic'], function ()
            {
                // Лог прибыли
                Route::get('/currency/log_profit', 'CurrencyController@logProfit')->middleware(['permission:currencies']);

                // Лог валюты
                Route::get('/currency/logs', 'CurrencyController@log')->middleware(['permission:currencies']);


                // Метки для валют
                Route::resource('currency-labels', 'CurrencyLabelsController');

                // Шаблоны для валют
                Route::resource('currency-templates', 'CurrencyTemplateController');

                // Валюты
                Route::match(['get','post'], '/currency/archive', 'CurrencyController@archive')->middleware(['permission:currencies']);
                Route::get('/currency/sorting', 'CurrencyController@sortingReserves')->middleware(['permission:currencies']);
                Route::get('/currency/sorting_admin', 'CurrencyController@sortingAdmin')->middleware(['permission:currencies']);
                Route::get('currency/{id}/delete', 'CurrencyController@destroy')->middleware(['permission:currencies']);
                Route::resource('currency', 'CurrencyController')->middleware(['permission:currencies']);
                Route::get('/currency/{id}/duplicate', 'CurrencyController@duplicate')->middleware(['permission:currencies']);

                // Уведомления для валют
                Route::get('/currency-notification/sorting', 'CurrencyNotificationController@sorting')->middleware(['permission:currencies']);
                Route::resource('currency-notification', 'CurrencyNotificationController')->middleware(['permission:currencies']);
                // Группы валют
                Route::get('/currency-groups/sorting', 'CurrencyGroupController@sorting')->middleware(['permission:currencies']);
                Route::resource('currency-groups', 'CurrencyGroupController')->middleware(['permission:currencies']);

                // Сети
                Route::get('/currency-network/{id}/delete', 'CurrencyNetworkController@destroy')->middleware(['permission:currencies']);
                Route::resource('currency-network', 'CurrencyNetworkController')->middleware(['permission:currencies']);

                // Быстрые команды
                Route::get('/currency-command/{id}/delete', 'CurrencyCommandController@destroy')->middleware(['permission:currencies']);
                Route::resource('currency-command', 'CurrencyCommandController')->middleware(['permission:currencies']);

                // Черный список пользователей
                Route::resource('blacklist', 'BlackListController')->middleware(['permission:order_blacklist']);

                // Белый список пользователей
                Route::resource('whitelist', 'WhiteListController')->middleware(['permission:order_whitelist']);

                //Скидки пользователей
                Route::get('/discounts','DiscountController@index')->middleware(['permission:user_discounts']);
                Route::match(['get','post'],'/discounts/create','DiscountController@create')->middleware(['permission:user_discounts']);
                Route::match(['get','post'],'/discounts/change','DiscountController@change')->middleware(['permission:user_discounts']);
                Route::get('/discounts/delete','DiscountController@delete')->middleware(['permission:user_discounts']);

                // Счета пользователей
                Route::get('/user_wallets', 'UserWalletController@index')->middleware(['permission:user_wallets'])->name('admin.basic.user_wallets');

                //Запрос резерв
                Route::get('/reserve_request', 'ReserveRequestController@index')->middleware(['permission:reserves'])->name('admin.basic.reserve_request');
                Route::get('/reserve_request/complete', 'ReserveRequestController@complete')->middleware(['permission:reserves']);
                Route::get('/reserve_request/delete', 'ReserveRequestController@delete')->middleware(['permission:reserves']);

                // Транзитные реквизиты
                Route::resource('requisites-transit', 'RequisitesTransitController')->middleware(['permission:requisites_system']);

                //Реквизиты
                Route::match(['get', 'post'], '/requisites/archive', 'RequisitesController@archive');
                Route::get('/requisites/{id}/zero_out', 'RequisitesController@zero_out');
                Route::get('/requisites/{id}/duplicate', 'RequisitesController@duplicate');
                Route::resource('requisites', 'RequisitesController')->middleware(['permission:requisites_system']);
                Route::get('/requisites-groups/sorting', 'RequisitesGroupController@sorting');
                Route::resource('requisites-groups', 'RequisitesGroupController')->middleware(['permission:requisites_system']);
                Route::resource('requisites-blacklist', 'RequisitesBlacklistController')->middleware(['permission:requisites_blacklist']);

                Route::get('requisites-fields/{id}/delete', 'RequisitesFieldController@destroy')->middleware(['permission:requisites_system']);
                Route::resource('requisites-fields', 'RequisitesFieldController')->middleware(['permission:requisites_system']);

                Route::get('requisites-info-fields/{id}/delete', 'RequisitesInfoFieldController@destroy')->middleware(['permission:requisites_system']);
                Route::resource('requisites-info-fields', 'RequisitesInfoFieldController')->middleware(['permission:requisites_system']);


                //Свой курс ЦБ
                Route::get('/your_course/{id}/destroy', 'YourCourseController@destroy')->middleware(['permission:your_course']);
                Route::resource('your_course', 'YourCourseController')->middleware(['permission:your_course']);

                // Доп. поля валют
                Route::get('/currency_fields/{id}/destroy', 'CurrencyFieldsController@destroy')->middleware(['permission:currencies']);
                Route::get('/currency_fields/sorting', 'CurrencyFieldsController@sorting')->middleware(['permission:currencies']);
                Route::get('/currency_fields/sorting_out', 'CurrencyFieldsController@sorting_out')->middleware(['permission:currencies']);
                Route::resource('currency_fields', 'CurrencyFieldsController')->middleware(['permission:currencies']);

                //Коды валют
                Route::resource('code_currency', 'CodeCurrencyController')->middleware(['permission:currency_codes']);

                //Фильтры для валют
                Route::get('/filter_currency/sorting', 'FilterCurrencyController@sorting')->middleware(['permission:currency_filters']);
                Route::resource('/filter_currency', 'FilterCurrencyController')->middleware(['permission:currency_filters']);

                //Платежные системы
                Route::get('/payments/{id}/delete', 'PaymentsController@delete_url')->middleware(['permission:payment_system']);
                Route::resource('payments', 'PaymentsController')->middleware(['permission:payment_system']);
                Route::resource('payments-explorer', 'PaymentExplorerController')->middleware(['permission:payment_system']);

                //Напрвления обменов
                Route::group(['prefix' => 'direction_exchange', 'middleware' => ['permission:direction_exchange']], function()
                {
                    Route::get('/fields/{id}/delete', 'DirectionFieldsController@delete');
                    Route::get('/fields/sorting', 'DirectionFieldsController@sorting');
                    Route::resource('/fields', 'DirectionFieldsController');
                    Route::get('/logs', 'DirectionExchangeController@logs');
                    Route::match(['get','post'],'/min_price','DirectionExchangeController@min_price');
                    Route::match(['get','post'],'/price_adjustment', 'DirectionExchangeController@price_adjustment');
                    Route::match(['get','post'],'/sorting-tariffs', 'DirectionExchangeController@sorting_tariffs');
                    Route::match(['get','post'],'/sorting-tariffs/{id}', 'DirectionExchangeController@sorting_tariff_id');
                    Route::match(['get','post'],'/sorting', 'DirectionExchangeController@sorting');
                    Route::match(['get','post'],'/sorting/{id}', 'DirectionExchangeController@sortingId');
                    Route::match(['get','post'],'/sorting-admin', 'DirectionExchangeController@sorting_admin');
                    Route::match(['get','post'],'/unpaid_item','DirectionExchangeController@unpaid_item');
                    Route::match(['get', 'post'], '/{id}/duplicate', 'DirectionExchangeController@duplicate');
                    Route::get('/{id}/delete', 'DirectionExchangeController@destroy');
                    Route::match(['get', 'post'], '/trashed', 'DirectionExchangeController@trashed');

                    // Шаблоны для валют
                    Route::resource('templates', 'DirectionTemplateController')->name('destroy','direction_exchange-templates.destroy');

                });
                Route::resource('direction_exchange', 'DirectionExchangeController')->middleware(['permission:direction_exchange']);

                Route::get('/direction-exchange-commission/{id}/delete', 'DirectionExchangeCommissionController@destroy')->middleware(['permission:direction_exchange']);
                Route::resource('direction-exchange-commission', 'DirectionExchangeCommissionController')->middleware(['permission:direction_exchange']);

                // Реквизиты для направлений
                Route::get('/direction-requisites/{id}/delete', 'DirectionRequisitesController@destroy')->middleware(['permission:direction_exchange']);
                Route::resource('direction-requisites', 'DirectionRequisitesController')->middleware(['permission:direction_exchange']);

                // Уведомление для направлений
                Route::resource('direction-exchange-notification', 'DirectionNotificationController')->middleware(['permission:direction_exchange']);
                // Группы направлений
                Route::resource('direction-exchange-groups', 'DirectionExchangeGroupController');
                // Режимы работ
                Route::resource('direction-exchange-modes', 'DirectionExchangeModeController');

                //Конструктор курсов
                Route::match(['get', 'post'], '/course_designer/types', 'CourseDesignerController@types')->middleware(['permission:course_designer']);
                Route::match(['get','post'],'/course_designer/commission', 'CourseDesignerController@commission')->middleware(['permission:course_designer']);
                Route::match(['get','post'],'/course_designer/favorites', 'CourseDesignerController@favorites')->middleware(['permission:course_designer']);
                Route::match(['get','post'],'/course_designer/favorites/sorting', 'CourseDesignerController@favoriteSorting')->middleware(['permission:course_designer']);
                Route::match(['get','post'], '/course_designer/{id}/competitors', 'CourseDesignerController@competitors')->middleware(['permission:course_designer']);
                Route::match(['get','post'], '/course_designer', 'CourseDesignerController@index')->middleware(['permission:course_designer']);
                Route::match(['get','post'], '/course_designer/{id}', 'CourseDesignerController@detail')->middleware(['permission:course_designer']);
                Route::match(['get','post'], '/course_designer/{id}/{item_id}', 'CourseDesignerController@direction')->middleware(['permission:course_designer']);


                // Файлы из резерва
                Route::get('reserves-files/{id}/delete','ReservesFilesController@destroy');
                Route::resource('reserves-files', 'ReservesFilesController');

                Route::get('reserves-files-group/sorting', 'ReserveFileGroupController@sorting');
                Route::get('reserves-files-group/{id}/delete','ReserveFileGroupController@destroy');
                Route::resource('reserves-files-group', 'ReserveFileGroupController');


                //Корректировка резервов
                Route::match(['get','post'],'/reserves','ReservesController@index')->middleware(['permission:reserves']);
                Route::get('/reserves/show/{id}','ReservesController@show')->middleware(['permission:reserves']);
                Route::get('/reserves/delete','ReservesController@destroy')->middleware(['permission:reserves']);
                Route::get('/reserves/sorting', 'ReservesController@sortingReserves')->middleware(['permission:reserves']);
                Route::get('/reserves/money/{id}', 'ReservesController@money')->middleware(['permission:reserves']);


                Route::match(['get','post'],'/reserves/create','ReservesController@create')->middleware(['permission:reserves']);
                Route::match(['get','post'],'/reserves/reduce','ReservesController@reduce')->middleware(['permission:reserves']);
                Route::match(['get','post'],'/reserves/change','ReservesController@change')->middleware(['permission:reserves']);
                Route::resource('reserves-groups', 'ReservesGroupController')->middleware(['permission:reserves']);
                Route::resource('reserves-alerts', 'ReservesAlertController')->middleware(['permission:reserves']);
            });

            //Платежные шлюзы
            Route::group(['prefix' => 'gateways', 'namespace' => 'Gateways', 'middleware' => ['permission:admin_merchant']], function()
            {
                // Список мерчантов
                Route::get('merchant/{id}/delete', 'MerchantController@destroy');
                Route::resource('merchant', 'MerchantController');

                // Добавить мульти мерчанта
                Route::get('multi-merchant/{alias}/delete', 'MultiMerchantController@delete');
                Route::resource('multi-merchant', 'MultiMerchantController');


                // Добавить мульти выплат
                Route::get('multi-payment/{alias}/delete', 'MultiPaymentController@delete');
                Route::resource('multi-payment', 'MultiPaymentController');


                // Список систем автовыплат
                Route::get('autopayment/{id}/delete', 'AutoPaymentController@destroy');
                Route::resource('autopayment', 'AutoPaymentController');


                Route::get('/logs/event-merchant', 'LogController@merchant_event')->name('event-merchant-log');
                Route::get('/logs/event-autopayment', 'LogController@autopayment_event')->name('event-autopayment-log');
                Route::get('/logs/event-orders', 'LogController@orders_event')->name('event-orders-log');
            });

            //Настройки
            Route::group(['prefix' => 'settings', 'namespace' => 'Settings', 'middleware' => ['permission:admin_settings']], function() {
                Route::match(['get','post'],'/','SettingController@index');
                Route::match(['get', 'post'], '/contacts', 'SettingController@contacts');
                Route::match(['get','post'],'/themes','SettingController@themes');


                Route::group(['prefix' => 'menu-templates'], function() {
                    Route::get('/email-template/{id}/delete', 'EmailTemplatesController@destroy');
                    Route::resource('email-template', 'EmailTemplatesController');
                    Route::resource('type-events', 'TypeEventsController');
                });

                Route::group(['prefix' => 'instruments'], function() {
                    Route::match(['get','post'],'/event_logs', 'EventController@event_logs');
                });
            });

            //Аналитика
            Route::group(['prefix' => 'analytics', 'namespace' => 'Analytics', 'middleware' => ['permission:admin_analytics']], function() {
               // Route::match(['get','post'], '/summary', 'AnalyticsController@summary');

                Route::group(['prefix' => 'employees'], function() {
                    Route::match(['get','post'],'/','EmployeesController@index');
                    Route::match(['get','post'],'/{id}/fine','EmployeesController@fine');
                    Route::match(['get','post'],'/{id}/event','EmployeesController@event');
                    Route::match(['get','post'],'/{id}/cards','EmployeesController@cards');
                    Route::match(['get','post'],'/{id}/edit','EmployeesController@edit');
                    Route::match(['get','post'], '/create','EmployeesController@create');
                    Route::match(['get','post'],'/report','EmployeesController@report');
                });
                Route::get('/reserves', 'EventController@reserve')->name('event_reserve.index');
                Route::get('/exchange', 'AnalyticsController@exchange');
            });

            //Инструменты
            Route::group(['prefix' => 'tools', 'namespace' => 'Tools'], function()
            {

                // Хранилище
                Route::get('/file-storages/{id}/delete','FileStoragesController@destroy');
                Route::resource('file-storages', 'FileStoragesController');

                // Правила сайта (Custom)
                Route::get('/rules_pages/sorting', 'RulesController@sorting');
                Route::resource('rules_pages', 'RulesController');


                // Баннеры
                Route::get('/banners/sorting', 'BannerController@sorting');
                Route::resource('banners', 'BannerController');
                Route::get('/banners/{id}/destroy','BannerController@destroy');
                // Кнопки для баннеров
                Route::resource('banners-button', 'BannerButtonController');
                Route::get('/banners-button/{id}/destroy','BannerButtonController@destroy');


                Route::group(['prefix' => 'geoip'], function() {
                    Route::match(['get', 'post'], '/countries', 'GEOIPController@countries');
                   Route::match(['get', 'post'], '/settings', 'GEOIPController@settings');
                });

                //Партнеры
                Route::get('/partners/{id}/destroy','PartnerController@destroy')->middleware(['permission:partners']);
                Route::get('/partners/sorting','PartnerController@sorting')->middleware(['permission:partners']);
                Route::resource('/partners','PartnerController')->middleware(['permission:partners']);

                Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
                Route::get('/logs-email', 'LogEmailController@index');

                Route::resource('news', 'NewsController')->middleware('permission:admin_news');

                Route::get('/faq-category/sorting', 'FaqCategoryController@sorting')->middleware('permission:admin_faq');
                Route::resource('faq-category', 'FaqCategoryController')->middleware('permission:admin_faq');

                Route::get('/reviews/{id}/delete', 'ReviewsController@destroy')->middleware('permission:admin_reviews');
                Route::resource('reviews', 'ReviewsController')->middleware('permission:admin_reviews');

                // Статистика
                Route::get('/statistics/{id}/destroy', 'StatisticsController@destroy');
                Route::get('/statistics/sorting', 'StatisticsController@sorting');
                Route::resource('statistics', 'StatisticsController');

                Route::get('/faq/sorting', 'FaqController@sorting')->middleware('permission:admin_faq');
                Route::get('/faq/{id}/delete', 'FaqController@destroy')->middleware('permission:admin_faq');
                Route::resource('faq','FaqController')->middleware('permission:admin_faq');

                //Уведомлении
                Route::get('/notification/sorting', 'NotificationController@sorting');
                Route::get('/notification/{id}/on', 'NotificationController@on');
                Route::get('/notification/{id}/off', 'NotificationController@off');
                Route::resource('notification', 'NotificationController');

                // Уведомления в реальном времени
                Route::resource('live-notification', 'LiveNotificationController');

                // Преимущество
                Route::get('/advantage/sorting', 'AdvantageController@sorting');
                Route::get('/advantage/{id}/destroy','AdvantageController@destroy');
                Route::resource('advantage',  'AdvantageController');

                // Ссылки на отзывы
                Route::get('/links_reviews/sorting', 'LinksReviewsController@sorting');
                Route::resource('links_reviews', 'LinksReviewsController');

                Route::resource('links_reviews_group', 'LinksReviewsGroupController');

                // Ссылки на Footer
                Route::get('/links_footers/sorting', 'LinksFootersController@sorting');
                Route::resource('links_footers', 'LinksFootersController');

                Route::resource('links_footers_group', 'LinkFooterGroupController');

                // Ссылки на соц.сети
                Route::get('/social-reviews/sorting', 'SocialReviewsController@sorting');
                Route::get('/social-reviews/{id}/delete', 'SocialReviewsController@destroy');
                Route::resource('social-reviews', 'SocialReviewsController');

                // Сотрудничество и PR
                Route::get('collaboration-pr/sorting', 'CollaborationPRController@sorting')->middleware(['permission:admin_contact']);
                Route::get('collaboration-pr/{id}/delete', 'CollaborationPRController@destroy')->middleware(['permission:admin_contact']);
                Route::resource('collaboration-pr', 'CollaborationPRController')->middleware(['permission:admin_contact']);

                // Контакты
                Route::get('contacts/sorting', 'ContactController@sorting')->middleware(['permission:admin_contact']);
                Route::get('contacts/{id}/delete', 'ContactController@destroy')->middleware(['permission:admin_contact']);
                Route::resource('contacts', 'ContactController')->middleware(['permission:admin_contact']);

                // Система авторизаций
                Route::get('auth-system/sorting', 'AuthSystemController@sorting')->middleware(['permission:admin_social_auth']);
                Route::get('/auth-system/{id}/delete', 'AuthSystemController@destroy')->middleware(['permission:admin_social_auth']);
                Route::resource('auth-system', 'AuthSystemController')->middleware(['permission:admin_social_auth']);

                Route::match(['get','post'],'/generator_currency','ToolsController@generator_currency')->middleware(['permission:currencies']);
                Route::match(['get','post'],'/firewall','ToolsController@firewall')->middleware(['permission:admin_firewall']);
                Route::resource('pages','PagesController')->middleware(['permission:admin_pages']);

                // Сортировка меню
                Route::get('menu/sorting','MenuController@sorting')->middleware(['permission:admin_pages']);
                Route::resource('menu','MenuController')->middleware(['permission:admin_pages']);


                Route::get('job-schedule/{id}/destroy','JobScheduleController@destroy')->middleware(['permission:admin_status_job']);
                Route::resource('job-schedule','JobScheduleController')->middleware(['permission:admin_status_job']);
                Route::match(['get','post'],'/job','JobController@index')->middleware(['permission:admin_status_job']);
            });

            Route::group(['prefix' => 'crypto', 'namespace' => 'Crypto', 'middleware' => ['permission:admin_parser']], function() {

                // Лог обновлений курсов
                Route::get('/log_update_courses', 'ParserLogController@log_update_courses');

                Route::get('api-keys/delete_all', 'APIKeyController@delete_all');
                Route::get('api-keys/{id}/delete', 'APIKeyController@destroy');
                Route::resource('api-keys', 'APIKeyController');

                Route::get('/all_parser', 'ParseController@items');
                Route::get('/parser/history', 'ParseController@history');
                Route::get('/parser/sorting', 'ParseController@sorting');
                Route::get('/parser/logs', 'ParseController@logs');
                Route::get('/parser/history/{id}', 'ParseController@historyPair');
                Route::get('/parser/delete/{id}','ParseController@delete');
                Route::resource('parser', 'ParseController');


                Route::match(['get','post'],'/group', 'GroupController@index');
                Route::group(['namespace' => 'BestChange'], function() {
                    Route::match(['get','post'],'/bestchange/settings', 'SettingsController@index');
                    Route::get('/bestchange/history', 'BestChangeController@history');
                    Route::get('/bestchange/history/{id}', 'BestChangeController@historyPair');
                    Route::get('/bestchange/log', 'BestChangeController@logData');
                    Route::get('/bestchange/error_log/{id}', 'BestChangeController@errorLogId');
                    Route::resource('bestchange', 'BestChangeController');
                });


                Route::get('competitors-parser/history','CompetitorsParserController@history');
                Route::get('competitors-parser/{id}/history','CompetitorsParserController@historyPair');
                Route::get('competitors-parser/{id}/delete','CompetitorsParserController@destroy');
                Route::resource('competitors-parser', 'CompetitorsParserController');

                Route::get('competitors-link/sorting', 'CompetitorsLinkController@sorting');
                Route::get('competitors-link/{id}/delete','CompetitorsLinkController@destroy');
                Route::resource('competitors-link', 'CompetitorsLinkController');


                // Дополнительный коэффициент для формуля
                Route::get('formula-coefficient/{id}/delete', 'FormulaCoefficientController@destroy');
                Route::resource('formula-coefficient', 'FormulaCoefficientController');

                // Парсер по формуле
                Route::get('/parser-formula/history', 'ParserFormulaController@history');
                Route::get('parser-formula/{id}/delete', 'ParserFormulaController@destroy');
                Route::resource('parser-formula', 'ParserFormulaController');

                // Парсер из файла
                Route::get('file-parser/{id}/delete','FileParserController@destroy');
                Route::resource('file-parser', 'FileParserController');

                Route::get('file-parser-group/sorting', 'FileParserGroupController@sorting');
                Route::get('file-parser-group/{id}/delete','FileParserGroupController@destroy');
                Route::resource('file-parser-group', 'FileParserGroupController');

                Route::get('partner-parser/{id}/delete','PartnerParserController@destroy');
                Route::resource('partner-parser', 'PartnerParserController');

                // Парсинг партнеров
                Route::get('partner-parser-group/sorting', 'PartnerParserGroupController@sorting');
                Route::get('partner-parser-group/{id}/delete','PartnerParserGroupController@destroy');
                Route::resource('partner-parser-group', 'PartnerParserGroupController');

            });

            Route::group(['prefix' => 'applications', 'namespace' => 'Applications', 'middleware' => ['permission:admin_tasks']], function()
            {
                Route::match(['get','post'],'/', 'ApplicationController@index')->name('admin.application');
                Route::match(['get','post'],'/details/{id}','ApplicationController@details');
                Route::get('/postponed','ApplicationController@postponed');
                Route::match(['get','post'],'/settings', 'ApplicationController@settings');


                Route::get('/application-steps/sorting', 'ApplicationStepsController@sorting');
                Route::get('/application-steps/{id}/destroy', 'ApplicationStepsController@destroy');
                Route::resource('application-steps', 'ApplicationStepsController');

                Route::match(['get','post'], '/status-log', 'LogController@statusLog')->name('admin.application.status-log');
                Route::get('/confirmation-log', 'ApplicationController@confirmationLog')->name('admin.application.confirmation-log');
                Route::match(['get','post'], '/merchant-log', 'LogController@merchantLog')->name('admin.application.merchant-log');
                Route::match(['get','post'], '/autopayment-log', 'LogController@autoPaymentLog')->name('admin.application.autopayment-log');
                Route::match(['get','post'], '/checkpay-log', 'LogController@checkpayLog')->name('admin.application.checkpay-log');
                Route::match(['get','post'], '/aml-log', 'LogController@amlLog')->name('admin.application.aml-log');


                Route::match(['get','post'], '/archive','ApplicationController@archive');
                Route::match(['get','post'], '/favorites','ApplicationController@favorites');
                Route::match(['get','post'], '/spam','ApplicationController@spam');

                Route::match(['get', 'post'], '/trashed', 'ApplicationController@trashed')->middleware(['permission:admin_order_trashed']);

                // Список клиентов которые делали обмены
                Route::match(['get', 'post'],'/buyers', 'ApplicationController@buyers');

                //Заявки на выплату
                Route::match(['get','post'],'/payment_bonuses', 'PaymentBonusesController@index')->middleware(['permission:admin_tasks'])->name('admin.application.payment_bonuses');
                Route::get('/payment_bonuses/complete', 'PaymentBonusesController@complete')->middleware(['permission:admin_tasks']);
                Route::get('/payment_bonuses/delete', 'PaymentBonusesController@delete')->middleware(['permission:admin_tasks']);
                Route::match(['GET','POST'], '/payment_bonuses/{id}','PaymentBonusesController@details')->middleware(['permission:admin_tasks']);

                // Причины отклонения заявки
                Route::resource('reasons-rejection', 'ReasonsRejectionController');

                // Причины отложенной заявки
                Route::resource('reasons-defer', 'ReasonsDeferController');

                // Уровни операторов
                Route::get('/levels/{id}/delete', 'LevelController@destroy');
                Route::resource('levels', 'LevelController');

                // Уровни операторов
                Route::get('/levels-group/{id}/delete', 'LevelGroupController@destroy');
                Route::resource('levels-group', 'LevelGroupController');

            });

            Route::group(['prefix' => 'account','namespace' => 'Account', 'middleware' => ['permission:admin_users']], function()
            {
                Route::get('/users/{id}/internal_account', 'InternalAccountController@index');

                Route::get('/users/history_profiles', 'UserController@history_profiles')->middleware('permission:admin_users');
                Route::resource('users', 'UserController')->middleware('permission:admin_users');
                Route::match(['get','post'], '/users/{id}/google','UserController@google');

                //Загрузить заявки
                Route::match(['get', 'post'], '/users/{id}/download_order', 'UserController@download_order');


                Route::get('/users/{id}/application', 'UserController@application');
                Route::get('/users/{id}/statistic', 'UserController@statistic');
                Route::match(['get','post'],'/users/{id}/referral', 'UserController@referral');
                Route::match(['get','post'],'/users/{id}/ban', 'UserController@ban');
                Route::match(['get','post'],'/users/{id}/revoke_user', 'UserController@revokeUser');
                Route::match(['get', 'post'],'/users/{id}/referral_statistics', 'UserController@referralStatistics');

                // Управление балансом
                Route::match(['get', 'post'], '/users/{id}/balances', 'UserController@balances');

                // Резервные коды
                Route::get('/users/{id}/backup_codes', 'BackupCodesController@codes');
                Route::get('/users/{id}/backup_codes/download', 'BackupCodesController@download');

                // История выплат
                Route::resource('/users/{id}/payouts', 'PayoutsController');
                // История блокировок
                Route::get('/users/{id}/banned_histories', 'UserController@bannedHistories');


                Route::resource('roles', 'RoleController')->middleware('permission:admin_roles');
                Route::get('/roles/{id}/delete', 'RoleController@destroy')->middleware('permission:admin_roles');
                Route::resource('permissions', 'PermissionController')->middleware('permission:admin_roles');
                Route::match(['get','post'], '/settings', 'SettingsController@index');

                // Лог авторизаций
                Route::get('/logs_auth', 'LogController@index')->name('admin.account.logs_auth');
                // Лог авторизаций в админпанели
                Route::get('/admin_logs_auth', 'LogController@admin_logs')->name('admin.account.admin_logs_auth');

                // Фильтр по IP, Email ...
                Route::match(['get','post'],'/block_ip', 'BlockIpController@index')->middleware('permission:admin_blockip');
            });

            //Партнерам
            Route::group(['prefix'  =>  'affiliate', 'namespace' => 'AffiliateProgram', 'middleware' => ['permission:admin_affiliate_program']], function () {

                // Реферальная система
                Route::get('/referral/statistics', 'ReferralController@statistics');
                Route::get('/referral/transitions', 'ReferralController@transitions')->name('affiliate.referral.transitions');
                Route::get('/referral/transitions_exchange', 'ReferralController@transitionExchange');
                Route::get('/referral/referrals', 'ReferralController@referrals');
                Route::get('/referral/logs', 'ReferralController@logs');
                Route::get('/referral/exchanges', 'ReferralController@exchanges')->name('affiliate.referral.exchanges');
                Route::get('/referral/exchanges_group', 'ReferralController@exchanges_group')->name('affiliate.referral.exchanges_group');
                Route::resource('referral', 'ReferralController');
                //

                // Бонусная программа
                Route::get('/bonus/exchanges', 'BonusController@exchanges')->name('affiliate.bonus.exchanges');
                Route::get('/bonus/logs', 'BonusController@logs')->name('affiliate.bonus.logs');
                Route::resource('bonus', 'BonusController');

                // Партнерские условия
                Route::group(['prefix' => 'conditions'], function() {
                    Route::match(['get','post'],'/referral','ConditionController@referral');
                    Route::match(['get','post'],'/cashback','ConditionController@cashback');
                    Route::match(['get', 'post'], '/monitoring', 'ConditionController@monitoring');
                });


                Route::match(['get', 'post'], '/settings', 'SettingsController@index');
            });


            // Верификация
            Route::group(['prefix' => 'verifications', 'namespace' => 'Verifications'], function() {

                // Верификация личности
                Route::resource('accounts', 'VerificationAccountController')->middleware(['permission:admin_users']);

                // Верификация
                Route::resource('cards', 'VerificationCardController')->middleware(['permission:admin_verification_card'])->name('index', 'verifications-card.index');
                Route::resource('card-category', 'VerificationCardCategoryController')->middleware(['permission:admin_verification_card']);
                Route::resource('card-instructions', 'VerificationCardInstructionController')->middleware(['permission:admin_verification_card']);

            });

        });

        //Для AJAX запросов в админке
        Route::prefix('admin')->group(function()
        {
            // Роутер для предзагрузки данных
            Route::group(['prefix' => 'ajax', 'namespace' => 'AJAX'], function() {
                Route::get('/order', 'OrderController@index');
            });


            Route::prefix('private')->group(function ()
            {
                Route::post('/direction_exchange_cities', 'PrivateController@directionExchangeCities');
                Route::post('/direction_exchange_percent_amount', 'PrivateController@directionExchangePercentAmount');

                Route::get('/operator_settings', 'PrivateController@getOperatorSettings');
                Route::post('/operator_settings', 'PrivateController@postOperatorSettings');
                Route::get('/list_operators', 'PrivateController@listOperators');
                Route::post('/change_operators', 'PrivateController@changeOperators');

                Route::post('/ajax_operator_payment','PrivateController@ajaxOperatorPayment');
                Route::post('/success_operator_payment','PrivateController@successOperatorPayment');

                Route::post('/list_receiving', 'PrivateController@listReceiving');

                Route::post('/check_receipts', 'PrivateController@check_receipts');
                Route::post('/status_site', 'PrivateController@status_site');
                Route::get('/check_task', 'PrivateController@check_task');
                Route::post('/info_task/{id}', 'PrivateController@info_task');
                Route::post('/update_position', 'PrivateController@update_position');
                Route::post('/update_sorting_currency', 'SortingController@update_position');

                Route::group(['prefix' => 'sorting'], function() {
                    Route::post('/banners', 'SortingController@cryptoBanners');
                    Route::post('/crypto_parser', 'SortingController@cryptoParser');
                    Route::post('/competitors_link', 'SortingController@competitors_link');
                    Route::post('/file_parser_group', 'SortingController@file_parser_group');
                    Route::post('/currency_reserves', 'SortingController@currencyReserves');
                    Route::post('/currency_notification', 'SortingController@currencyNotification');
                    Route::post('/favorites_sorting', 'SortingController@favoriteSorting');
                   Route::post('/basic_reserves', 'SortingController@basicReserves');
                    Route::post('/notification', 'SortingController@notification');
                    Route::post('/advantage', 'SortingController@advantage');
                    Route::post('/partners', 'SortingController@partners');
                    Route::post('/links_reviews', 'SortingController@links_reviews');
                    Route::post('/requisites-group', 'SortingController@requisites_group');
                    Route::post('/verification-requirement', 'SortingController@verificationRequirement');
                    Route::post('/menu', 'SortingController@menu');
                    Route::post('/rules_pages', 'SortingController@sortingRulesPages');
                    Route::post('/filter_currency', 'SortingController@filter_currency');
                    Route::post('/group_currency', 'SortingController@group_currency');
                    Route::post('/faq', 'SortingController@faq');
                    Route::post('/faq_category', 'SortingController@faq_category');
                    Route::post('/selected_course', 'SortingController@selected_course');
                    Route::post('/auth-system', 'SortingController@socialAuthSystem');
                    Route::post('/contact', 'SortingController@sortingContacts');
                    Route::post('/social-review', 'SortingController@socialReview');
                    Route::post('/collaboration-pr',  'SortingController@collaborationPR');
                    Route::post('/statistics', 'SortingController@statistics');
                    Route::post('/currency_tariffs', 'SortingController@sortingTariffs');
                    Route::post('/direction_admin', 'SortingController@sortingDirectionAdmin');
                    Route::post('/direction_field', 'SortingController@sortingDirectionField');
                    Route::post('/currency_tariffs/{id}', 'SortingController@sortingTariffId');
                    Route::post('/your_course_group', 'SortingController@sortingYourCourseGroup');
                    Route::post('/direction_currency/{id}', 'SortingController@sortingDirectionCurrency');
                    Route::post('/currency_admin', 'SortingController@sortingCurrencyAdmin');
                    Route::post('/currency_field','SortingController@sortingCurrencyField');
                    Route::post('/currency_field_out','SortingController@sortingCurrencyFieldOut');
                    Route::post('/application-steps','SortingController@sortingApplicationStep');
            });
            });

            Route::prefix('json')->group(function () {
                Route::get('/list_task', 'JsonController@list_task');
                Route::get('/list_task/statuses', 'JsonController@list_task_statuses');
                Route::get('/users', 'JsonController@users');
                Route::get('/live_task', 'JsonController@live_task');
                Route::get('/deferred_task', 'JsonController@deferred_task');
                Route::get('/push_task', 'JsonController@push_task');
                Route::get('/live_tx','JsonController@live_tx');
            });
        });
    });

    //Внутренняя авторизация в администратороской панели
    Route::get(config('admin.route_path').'/login','LoginController@index')->name('admin_login');
    Route::post(config('admin.route_path').'/login','LoginController@login');
});


