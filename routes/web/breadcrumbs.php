<?php


use Diglactic\Breadcrumbs\Breadcrumbs;

try {
    //Главная
    Breadcrumbs::for('admin.home', function ($breadcrumbs) {
        $breadcrumbs->push(__('Главная'),  admin_base_path('/'), ['icon' => 'fa fa-home position-left']);
    });


    //Главная
    Breadcrumbs::for('admin.index', function ($breadcrumbs, $desktop) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push($desktop->name,  admin_base_path('/'));
    });

    //Главная
    Breadcrumbs::for('admin.plugins', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Список плагинов'),  admin_base_path('/modules'));
    });

    //Главная
    Breadcrumbs::for('admin.update_system', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Система обновлений'),  admin_base_path('/update_system'));
    });


    Breadcrumbs::for('admin.favorites.control', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Управление избранными страницами'),  admin_base_path('favorites/control'));
    });

    //Редактировать виджет
    Breadcrumbs::for('admin.widgets.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push($item->name,  admin_base_path('/widgets/'.$item->id_desktop.'/'.$item->id.'/edit'));
    });

    //Управление рабочими столами
    Breadcrumbs::for('admin.widgets.config_desktop', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Управление рабочими столами'),  admin_base_path('/widgets/config_desktops'));
    });

    //Управление рабочими столами
    Breadcrumbs::for('admin.widgets.edit_desktop', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.widgets.config_desktop');
        $breadcrumbs->push($item->name,  admin_base_path('/widgets/edit_desktops/'.$item->id));
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Валюты
    Breadcrumbs::for('admin.currency', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Валюты'),  config('admin.directory').'/basic/currency');
    });

    Breadcrumbs::for('admin.currency.archive', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Архив валют'),  config('admin.directory').'/basic/currency/archive');
    });

    Breadcrumbs::for('admin.currency.logs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Лог валют'), admin_base_path('/basic/currency/logs'));
    });

    Breadcrumbs::for('admin.currency.log_profit', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Лог прибыли'), admin_base_path('/basic/currency/log_profit'));
    });

    Breadcrumbs::for('admin.currency.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Сортировка валют для резерва'),  config('admin.directory').'/basic/currency/sorting');
    });

    Breadcrumbs::for('admin.currency.sorting_admin', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Сортировка валют в админке'),  config('admin.directory').'/basic/currency/sorting_admin');
    });

    Breadcrumbs::for('admin.currency.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Изменить валюту').' '.$item->payment->name.' '.$item->code_currency->name,  config('admin.directory').'/basic/currency/'.$item->id.'/edit');
    });


    // Метки валют
    Breadcrumbs::for('admin.basic.currency.labels', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Метки для валют'),  config('admin.directory').'/basic/currency-labels');
    });

    Breadcrumbs::for('admin.basic.currency.labels.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.currency.labels');
        $breadcrumbs->push(__('Добавить метку'),  config('admin.directory').'/basic/currency-labels/create');
    });

    Breadcrumbs::for('admin.basic.currency.labels.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.currency.labels');
        $breadcrumbs->push(__('Изменить метку').' '.$item->title,  config('admin.directory').'/basic/currency-labels/'.$item->id.'/edit');
    });

    // Группа валют
    Breadcrumbs::for('admin.basic.currency.groups', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Группы валют'),  config('admin.directory').'/basic/currency-groups');
    });

    Breadcrumbs::for('admin.basic.currency.groups.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.currency.groups');
        $breadcrumbs->push(__('Добавить группу'),  config('admin.directory').'/basic/currency-groups/create');
    });

    Breadcrumbs::for('admin.basic.currency.groups.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.currency.groups');
        $breadcrumbs->push(__('Изменить группу').' '.$item->name,  config('admin.directory').'/basic/currency-groups/'.$item->id.'/edit');
    });

    // Уведомление
    Breadcrumbs::for('admin.basic.currency.notification', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Уведомление для валют'),  admin_base_path('/basic/currency-notification'));
    });

    Breadcrumbs::for('admin.basic.currency.notification.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.currency.notification');
        $breadcrumbs->push(__('Добавить уведомление'),  admin_base_path('/basic/currency-notification/create'));
    });

    Breadcrumbs::for('admin.basic.currency.notification.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.currency.notification');
        $breadcrumbs->push(__('Изменить уведомление'),  admin_base_path('/basic/currency-notification/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.basic.currency.notification.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.currency.notification');
        $breadcrumbs->push(__('Сортировка уведомлений'),  admin_base_path('/basic/currency-notification/sorting'));
    });


    // Быстрые команды
    Breadcrumbs::for('admin.basic.currency.command', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.currency');
        $breadcrumbs->push(__('Список команд'),  config('admin.directory').'/basic/currency-command');
    });

    Breadcrumbs::for('admin.basic.currency.command.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.currency.command');
        $breadcrumbs->push(__('Добавить команду'),  config('admin.directory').'/basic/currency-command/create');
    });

    Breadcrumbs::for('admin.basic.currency.command.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.currency.command');
        $breadcrumbs->push(__('Изменить команду').' '.$item->name,  config('admin.directory').'/basic/currency-command/'.$item->id.'/edit');
    });


//    // Сети
//    Breadcrumbs::for('admin.basic.currency.network', function ($breadcrumbs) {
//        $breadcrumbs->parent('admin.currency');
//        $breadcrumbs->push(__('Список сетей'),  admin_base_path('/basic/currency-network'));
//    });
//
//    Breadcrumbs::for('admin.basic.currency.network.create', function ($breadcrumbs) {
//        $breadcrumbs->parent('admin.basic.currency.network');
//        $breadcrumbs->push(__('admin-basic.currency-network.add_button'),  admin_base_path('/basic/currency-network/create'));
//    });
//
//    Breadcrumbs::for('admin.basic.currency.network.change', function ($breadcrumbs, $item) {
//        $breadcrumbs->parent('admin.basic.currency.network');
//        $breadcrumbs->push(__('admin-basic.currency-network.edit_title', ['name' => $item->name]),  admin_base_path('/basic/currency-network/'.$item->id.'/edit'));
//    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Коды валют
    Breadcrumbs::for('admin.basic.code_currency', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Коды валют'),  config('admin.directory').'/basic/code_currency');
    });

    Breadcrumbs::for('admin.basic.code_currency.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.code_currency');
        $breadcrumbs->push(__('Добавить код валюты'),  config('admin.directory').'/basic/code_currency/add');
    });

    Breadcrumbs::for('admin.basic.code_currency.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.code_currency');
        $breadcrumbs->push(__('Изменить код валюты'),  config('admin.directory').'/basic/code_currency/change/?id='.$id);
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Фильтры для валют
    Breadcrumbs::for('admin.basic.filter_currency', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Фильтры для валют'),  config('admin.directory').'/basic/filter_currency');
    });

    Breadcrumbs::for('admin.basic.filter_currency.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.filter_currency');
        $breadcrumbs->push(__('Добавить фильтр'),  config('admin.directory').'/basic/filter_currency/create');
    });

    Breadcrumbs::for('admin.basic.filter_currency.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.filter_currency');
        $breadcrumbs->push(__('Изменить фильтр'),  config('admin.directory').'/basic/filter_currency/'.$id.'/edit');
    });

    Breadcrumbs::for('admin.basic.filter_currency.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.filter_currency');
        $breadcrumbs->push(__('Сортировка фильтров'),  config('admin.directory').'/basic/filter_currency/sorting');
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //Свой курс ЦБ
    Breadcrumbs::for('admin.basic.your_course', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Свой курс ЦБ'),  config('admin.directory').'/basic/your_course');
    });

    Breadcrumbs::for('admin.basic.your_course.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.your_course');
        $breadcrumbs->push(__('Добавить свой курс'),  config('admin.directory').'/basic/your_course/add');
    });

    Breadcrumbs::for('admin.basic.your_course.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.your_course');
        $breadcrumbs->push(__('Изменить свой курс'),  config('admin.directory').'/basic/your_course/change/?id='.$id);
    });

    // Группа для своего курса
    Breadcrumbs::for('admin.basic.your_course.groups', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.your_course');
        $breadcrumbs->push(__('Список групп'),  admin_base_path('/basic/your-course-group'));
    });

    Breadcrumbs::for('admin.basic.your_course.groups.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.your_course.groups');
        $breadcrumbs->push(__('Добавить группу'),  admin_base_path('/basic/your-course-group/create'));
    });

    Breadcrumbs::for('admin.basic.your_course.groups.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.your_course.groups');
        $breadcrumbs->push(__('Изменить группу').' '.$item->name,  admin_base_path('/basic/your-course-group/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.basic.your_course.groups.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.your_course.groups');
        $breadcrumbs->push(__('Сортировка групп'),  admin_base_path('/basic/your-course-group/sorting'));
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //Платежные системы
    Breadcrumbs::for('admin.basic.payments', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Платежные системы',  config('admin.directory').'/basic/payments');
    });

    Breadcrumbs::for('admin.basic.payments.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.payments');
        $breadcrumbs->push('Добавить платежную систему',  config('admin.directory').'/basic/payments/add');
    });

    Breadcrumbs::for('admin.basic.payments.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.payments');
        $breadcrumbs->push('Изменить платежную систему',  config('admin.directory').'/basic/payments/change/?id='.$id);
    });

    Breadcrumbs::for('admin.basic.payments.explorer', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.payments');
        $breadcrumbs->push('Blockchain Explorer',  config('admin.directory').'/basic/payments-explorer');
    });

    Breadcrumbs::for('admin.basic.payments.explorer.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.payments.explorer');
        $breadcrumbs->push('Изменить Blockchain Explorer',  config('admin.directory').'/basic/payments-explorer/'.$item->id.'/edit');
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //Платежные реквизиты
    Breadcrumbs::for('admin.basic.requisites', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Платежные реквизиты',  config('admin.directory').'/basic/requisites');
    });

    Breadcrumbs::for('admin.basic.requisites.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Добавить реквизит',  config('admin.directory').'/basic/requisites/add');
    });

    Breadcrumbs::for('admin.basic.requisites.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Изменить реквизит',  config('admin.directory').'/basic/requisites/change/?id='.$id);
    });

    // Архивированные реквзиты
    Breadcrumbs::for('admin.basic.requisites.archive', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Архивированные реквизиты',  config('admin.directory').'/basic/requisites/archive');
    });

    // Черный список реквизитов
    Breadcrumbs::for('admin.basic.requisites.blacklist', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Черный список реквизитов',  config('admin.directory').'/basic/requisites-blacklist');
    });
    Breadcrumbs::for('admin.basic.requisites.blacklist.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.requisites.blacklist');
        $breadcrumbs->push('Изменить Реквизит в черном списке',  config('admin.directory').'/basic/requisites-blacklist/'.$item->id.'/edit');
    });

    // Группа платежных реквизитов
    Breadcrumbs::for('admin.basic.requisites.groups', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Группы платежных реквизитов',  config('admin.directory').'/basic/requisites-groups');
    });

    Breadcrumbs::for('admin.basic.requisites.groups.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites.groups');
        $breadcrumbs->push('Добавить группу',  config('admin.directory').'/basic/requisites-groups/create');
    });

    Breadcrumbs::for('admin.basic.requisites.groups.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.requisites.groups');
        $breadcrumbs->push('Изменить группу '.$item->name,  config('admin.directory').'/basic/requisites-groups/edit/?id='.$item->id);
    });

    Breadcrumbs::for('admin.basic.requisites.groups.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites.groups');
        $breadcrumbs->push('Сортировка групп',  config('admin.directory').'/basic/requisites-groups/sorting');
    });

    // Транзитные реквизиты
    Breadcrumbs::for('admin.basic.requisites.transit', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Транзитные реквизиты',  config('admin.directory').'/basic/requisites-transit');
    });

    Breadcrumbs::for('admin.basic.requisites.transit.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites.transit');
        $breadcrumbs->push('Добавить реквизит',  config('admin.directory').'/basic/requisites-transit/create');
    });

    Breadcrumbs::for('admin.basic.requisites.transit.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.requisites.transit');
        $breadcrumbs->push('Изменить реквизит',  config('admin.directory').'/basic/requisites-transit/'.$item->id.'/edit');
    });


    // Дополнительные поля
    Breadcrumbs::for('admin.basic.requisites.fields', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Список полей для реквизитов',  admin_base_path('/basic/requisites-fields'));
    });

    Breadcrumbs::for('admin.basic.requisites.fields.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites.fields');
        $breadcrumbs->push('Добавить поле',  admin_base_path('/basic/requisites-fields/create'));
    });

    Breadcrumbs::for('admin.basic.requisites.fields.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.requisites.fields');
        $breadcrumbs->push('Изменить поле '.$item->name,  admin_base_path('/basic/requisites-fields/'.$item->id.'/edit'));
    });

    // Информационные поля
    Breadcrumbs::for('admin.basic.requisites.info-fields', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites');
        $breadcrumbs->push('Список информационных полей',  admin_base_path('/basic/requisites-info-fields'));
    });

    Breadcrumbs::for('admin.basic.requisites.info-fields.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.requisites.info-fields');
        $breadcrumbs->push('Добавить поле',  admin_base_path('/basic/requisites-info-fields/create'));
    });

    Breadcrumbs::for('admin.basic.requisites.info-fields.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.requisites.info-fields');
        $breadcrumbs->push('Изменить поле '.$item->name,  admin_base_path('/basic/requisites-info-fields/'.$item->id.'/edit'));
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Конструктор курсов
    Breadcrumbs::for('admin.basic.course_designer', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Конструктор курсов',  config('admin.directory').'/basic/course_designer');
    });

    Breadcrumbs::for('admin.basic.course_designer.detail', function ($breadcrumbs, $data) {
        $breadcrumbs->parent('admin.basic.course_designer');
        $breadcrumbs->push('Детали валюты '.$data->payment->name.' '.$data->code_currency->name,  config('admin.directory').'/basic/course_designer/'.$data->id);
    });

    Breadcrumbs::for('admin.basic.course_designer.direction', function ($breadcrumbs, $item, $data) {
        $breadcrumbs->parent('admin.basic.course_designer.detail', $item);
        $breadcrumbs->push(direction_name($data, false),  config('admin.directory').'/basic/course_designer/'.$item->id.'/'.$data->id);
    });

    Breadcrumbs::for('admin.basic.course_designer.commission', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.course_designer');
        $breadcrumbs->push('Комиссии направлений',  config('admin.directory').'/basic/course_designer/commission');
    });

    Breadcrumbs::for('admin.basic.course_designer.types', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.course_designer');
        $breadcrumbs->push('Типы направлений',  config('admin.directory').'/basic/course_designer/types');
    });

    Breadcrumbs::for('admin.basic.course_designer.competitors', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.course_designer.detail', $item->currency1);
        $breadcrumbs->push('Конкуренты',  config('admin.directory').'/basic/course_designer/'.$item->id.'/competitors');
    });

    Breadcrumbs::for('admin.basic.course_designer.favorites', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.course_designer');
        $breadcrumbs->push('Избранные направлений',  config('admin.directory').'/basic/course_designer/favorites');
    });

    Breadcrumbs::for('admin.basic.course_designer.favorites_sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.course_designer.favorites');
        $breadcrumbs->push('Сортировка избранных направлений',  config('admin.directory').'/basic/course_designer/favorites/sorting');
    });


//Направление обмена
    Breadcrumbs::for('admin.basic.direction_exchange', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Направление обмена',  config('admin.directory').'/basic/direction_exchange');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.trashed', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Удаленные направления',  admin_base_path('basic/direction_exchange/trashed'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.fields', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Доп. поля',  admin_base_path('basic/direction_exchange/fields'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.fields.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange.fields');
        $breadcrumbs->push('Добавить Доп. поле',  admin_base_path('basic/direction_exchange/fields'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.fields.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.fields');
        $breadcrumbs->push('Изменить Доп. поле '.$item->name,  admin_base_path('basic/direction_exchange/fields'));
    });


    Breadcrumbs::for('admin.basic.direction_exchange.logs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Лог направлений обменов',  config('admin.directory').'/basic/direction_exchange/logs');
    });


    Breadcrumbs::for('admin.basic.direction_exchange.rates', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Корректировка курса',  config('admin.directory').'/basic/direction_exchange/rates');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Добавить направление',  config('admin.directory').'/basic/direction_exchange/add');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Изменить направление',  config('admin.directory').'/basic/direction_exchange/change/?id='.$id);
    });

    // Группа направлений
    Breadcrumbs::for('admin.basic.direction_exchange.groups', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Группы направлений обменов',  config('admin.directory').'/basic/direction-exchange-groups');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.groups.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange.groups');
        $breadcrumbs->push('Добавить группу',  config('admin.directory').'/basic/direction-exchange-groups/create');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.groups.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.groups');
        $breadcrumbs->push('Изменить группу '.$item->name,  config('admin.directory').'/basic/direction-exchange-groups/'.$item->id.'/edit');
    });

    // Режим для направлений
    Breadcrumbs::for('admin.basic.direction_exchange.modes', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Список режимов для направлений',  config('admin.directory').'/basic/direction-exchange-modes');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.modes.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange.modes');
        $breadcrumbs->push('Добавить новый режим',  config('admin.directory').'/basic/direction-exchange-modes/create');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.modes.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.modes');
        $breadcrumbs->push('Изменить название режима '.$item->name,  config('admin.directory').'/basic/direction-exchange-modes/'.$item->id.'/edit');
    });

    // Реквизиты направлений
    Breadcrumbs::for('admin.basic.direction_exchange.requisites', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Реквизиты для направлений',  config('admin.directory').'/basic/direction-requisites');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.requisites.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange.requisites');
        $breadcrumbs->push('Добавить реквизит',  config('admin.directory').'/basic/direction-requisites/create');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.requisites.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.requisites');
        $breadcrumbs->push('Изменить реквизит '.$item->name,  config('admin.directory').'/basic/direction-requisites/'.$item->id.'/edit');
    });


    Breadcrumbs::for('admin.basic.direction_exchange.unpaid_item', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Автоматическое удаление неоплаченных заявок',  config('admin.directory').'/basic/direction_exchange/unpaid_item');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Сортировка направлений',  config('admin.directory').'/basic/direction_exchange/sorting');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.sorting_id', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.sorting');
        $breadcrumbs->push('Сортировка направлений для '.$item->payment->name.' '.$item->code_currency->name,  admin_base_path('/basic/direction_exchange/sorting/'.$item->id));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.sorting_tariffs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Сортировка валют для тарифов',  config('admin.directory').'/basic/direction_exchange/sorting-tariffs');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.sorting_tariffs_id', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.sorting_tariffs');
        $breadcrumbs->push('Сортировка направлений для валюты '.$item->payment->name.' '. $item->code_currency->name,  config('admin.directory').'/basic/direction_exchange/sorting-tariffs/'.$item->id);
    });

    Breadcrumbs::for('admin.basic.direction_exchange.min_price', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Корректировка минимальной цены',  config('admin.directory').'/basic/direction_exchange/min_price');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.price_adjustment', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Общая корректировка цен',  config('admin.directory').'/basic/direction_exchange/price_adjustment');
    });

    Breadcrumbs::for('admin.basic.direction_exchange.enable_disable', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Включение и отключение направлений',  config('admin.directory').'/basic/direction_exchange/enable_disable');
    });

    // Уведомление направлений
    Breadcrumbs::for('admin.basic.direction_exchange.notification', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Уведомление для направлений',  admin_path('basic/direction-exchange-notification'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.notification.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange.notification');
        $breadcrumbs->push('Добавить уведомление',  admin_path('basic/direction-exchange-notification/create'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.notification.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.direction_exchange.notification');
        $breadcrumbs->push('Изменить уведомление',  admin_path('basic/direction-exchange-notification/'.$id.'/edit'));
    });


    Breadcrumbs::for('admin.basic.direction_exchange.commission', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push('Список групповых комиссий',  admin_base_path('/basic/direction-exchange-commission'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.commission.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange.commission');
        $breadcrumbs->push('Добавить групповую комиссию ',  admin_base_path('/basic/direction-exchange-commission/create'));
    });

    Breadcrumbs::for('admin.basic.direction_exchange.commission.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_exchange.commission');
        $breadcrumbs->push('Изменить групповую комиссию '.$item->name,  admin_base_path('/basic/direction-exchange-commission/'.$item->id.'/edit'));
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Доп. поля валют
    Breadcrumbs::for('admin.basic.currency_fields', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Доп. поля валют',  config('admin.directory').'/basic/currency_fields');
    });

    Breadcrumbs::for('admin.basic.currency_fields.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.currency_fields');
        $breadcrumbs->push('Добавить доп. поле',  config('admin.directory').'/basic/currency_fields/add');
    });

    Breadcrumbs::for('admin.basic.currency_fields.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.currency_fields');
        $breadcrumbs->push('Изменить доп. поле',  config('admin.directory').'/basic/currency_fields/change/?id='.$id);
    });


    //Шаблоны для валют
    Breadcrumbs::for('admin.basic.currency_templates', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Шаблоны для валют'),  admin_base_path('/basic/currency-templates'));
    });

    Breadcrumbs::for('admin.basic.currency_templates.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.currency_templates');
        $breadcrumbs->push(__('Изменить шаблон'),  admin_base_path('/basic/currency-templates/'.$item->id.'/edit'));
    });


    //Шаблоны для валют
    Breadcrumbs::for('admin.basic.direction_templates', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.direction_exchange');
        $breadcrumbs->push(__('Шаблоны для направлений'),  admin_base_path('/basic/direction_exchange/templates'));
    });

    Breadcrumbs::for('admin.basic.direction_templates.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.direction_templates');
        $breadcrumbs->push(__('Изменить шаблон'),  admin_base_path('/basic/direction_exchange/templates/'.$item->id.'/edit'));
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Счета пользователей
    Breadcrumbs::for('admin.basic.user_wallets', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Счета пользователей',  config('admin.directory').'/basic/user_wallets');
    });


//Скидки пользователей
    Breadcrumbs::for('admin.basic.discounts', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Скидки пользователей',  config('admin.directory').'/basic/discounts');
    });

    Breadcrumbs::for('admin.basic.discounts.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.discounts');
        $breadcrumbs->push('Добавить скидку',  config('admin.directory').'/basic/discounts/add');
    });

    Breadcrumbs::for('admin.basic.discounts.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.discounts');
        $breadcrumbs->push('Изменить скидку',  config('admin.directory').'/basic/discounts/change/?id='.$id);
    });

    Breadcrumbs::for('admin.basic.blacklist', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Черный список',  config('admin.directory').'/basic/blacklist');
    });

    Breadcrumbs::for('admin.basic.blacklist.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.blacklist');
        $breadcrumbs->push('Добавить запись',  config('admin.directory').'/basic/blacklist/create');
    });

    Breadcrumbs::for('admin.basic.blacklist.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.blacklist');
        $breadcrumbs->push('Изменить запись',  config('admin.directory').'/basic/blacklist/edit/?id='.$id);
    });


    // Белый список
    Breadcrumbs::for('admin.basic.whitelist', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Белый список',  config('admin.directory').'/basic/whitelist');
    });

    Breadcrumbs::for('admin.basic.whitelist.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.whitelist');
        $breadcrumbs->push('Добавить запись',  config('admin.directory').'/basic/whitelist/create');
    });

    Breadcrumbs::for('admin.basic.whitelist.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.whitelist');
        $breadcrumbs->push('Изменить запись',  config('admin.directory').'/basic/whitelist/'.$id.'/edit');
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///
///
/// /////////////
    Breadcrumbs::for('admin.basic.reserves.files', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push(__('Резервы из файла'), admin_base_path('/basic/reserves-files'));
    });

    Breadcrumbs::for('admin.basic.reserves.files.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push(__('Добавить резерв из файла'),  admin_base_path('/basic/reserves-files/create'));
    });

    Breadcrumbs::for('admin.basic.reserves.files.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push(__('Изменить резерв из файла'),  admin_base_path('/basic/reserves-files/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.basic.reserves.files.groups', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves.files');
        $breadcrumbs->push(__('Группы резервов из файла'), admin_base_path('/basic/reserves-files-group'));
    });

    Breadcrumbs::for('admin.basic.reserves.files.groups.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push(__('Добавить источник'),  admin_base_path('/basic/reserves-files-group/create'));
    });

    Breadcrumbs::for('admin.basic.reserves.files.groups.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push(__('Изменить источник :name', ['name' => $item->name]),  admin_base_path('/basic/reserves-files-group/'.$item->id.'/edit'));
    });

//Корректировка резерва
    Breadcrumbs::for('admin.basic.reserves', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Корректировка резерва',  config('admin.directory').'/basic/reserves');
    });

    Breadcrumbs::for('admin.basic.reserves.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Добавить резерв',  config('admin.directory').'/basic/reserves/add');
    });

    Breadcrumbs::for('admin.basic.reserves.reduce', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Уменьшить резерв',  config('admin.directory').'/basic/reserves/reduce');
    });

    Breadcrumbs::for('admin.basic.reserves.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Изменить резерв',  config('admin.directory').'/basic/reserves/change/?id='.$id);
    });

    Breadcrumbs::for('admin.basic.reserves.show', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Детали резерва',  config('admin.directory').'/basic/reserves/show/?id='.$id);
    });

    Breadcrumbs::for('admin.basic.reserves.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Сортировка резервов для админпанели',  config('admin.directory').'/basic/reserves/sorting');
    });


// Группа платежных реквизитов
    Breadcrumbs::for('admin.basic.reserves.groups', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Группы резервов',  config('admin.directory').'/basic/reserves-groups');
    });

    Breadcrumbs::for('admin.basic.reserves.groups.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves.groups');
        $breadcrumbs->push('Добавить группу',  config('admin.directory').'/basic/reserves-groups/create');
    });

    Breadcrumbs::for('admin.basic.reserves.groups.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.reserves.groups');
        $breadcrumbs->push('Изменить группу '.$item->name,  config('admin.directory').'/basic/reserves-groups/edit/?id='.$item->id);
    });

    // Оповещение
    Breadcrumbs::for('admin.basic.reserves.alerts', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves');
        $breadcrumbs->push('Оповещение для резервов',  config('admin.directory').'/basic/reserves-alerts');
    });

    Breadcrumbs::for('admin.basic.reserves.alerts.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.basic.reserves.alerts');
        $breadcrumbs->push('Добавить оповещение',  config('admin.directory').'/basic/reserves-alerts/create');
    });

    Breadcrumbs::for('admin.basic.reserves.alerts.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.basic.reserves.alerts');
        $breadcrumbs->push('Изменить оповещение ',  config('admin.directory').'/basic/reserves-alerts/'.$item->id.'/edit/');
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Корректировка резерва
    Breadcrumbs::for('admin.basic.reserve_request', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Запросы резерва',  config('admin.directory').'/basic/reserve_request');
    });




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Заявки
    Breadcrumbs::for('admin.applications', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Заявки',  config('admin.directory').'/applications');
    });

    Breadcrumbs::for('admin.applications.archive', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Архивированные заявки',  config('admin.directory').'/applications/archive');
    });

    Breadcrumbs::for('admin.applications.buyers', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Список частых клиентов',  config('admin.directory').'/applications/buyers');
    });

    Breadcrumbs::for('admin.applications.favorites', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Избранные заявки',  config('admin.directory').'/applications/favorites');
    });

    Breadcrumbs::for('admin.applications.spam', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Заявки в спаме',  config('admin.directory').'/applications/spam');
    });

    Breadcrumbs::for('admin.applications.live', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Live заявки',  config('admin.directory').'/applications/live');
    });

    Breadcrumbs::for('admin.applications.status-log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Лог статусов заявок',  config('admin.directory').'/applications/status-log');
    });

    Breadcrumbs::for('admin.applications.checkpay-log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Лог проверки оплат',  config('admin.directory').'/applications/checkpay-log');
    });

    Breadcrumbs::for('admin.applications.confirmation-log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Лог подтверждений',  config('admin.directory').'/applications/confirmation-log');
    });

    Breadcrumbs::for('admin.applications.merchant-log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Лог мерчантов',  config('admin.directory').'/applications/merchant-log');
    });

    Breadcrumbs::for('admin.applications.autopayment-log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Лог автовыплат',  config('admin.directory').'/applications/autopayment-log');
    });


    Breadcrumbs::for('admin.applications.postponed', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Отложенные заявки',  config('admin.directory').'/applications/postponed');
    });

    Breadcrumbs::for('admin.applications.details', function ($breadcrumbs,$id) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Информация о заявке №'.$id,  config('admin.directory').'/applications/details/'.$id);
    });


    Breadcrumbs::for('admin.applications.payment_bonuses', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Заявки на выплату',  config('admin.directory').'/applications/payment_bonuses');
    });

    Breadcrumbs::for('admin.applications.settings', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Статусы заявок',  config('admin.directory').'/applications/settings');
    });

    Breadcrumbs::for('admin.applications.trashed', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Удаленные заявки',  config('admin.directory').'/applications/trashed');
    });

    Breadcrumbs::for('admin.applications.reasons-rejection', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications.settings');
        $breadcrumbs->push('Причины отклонения заявок',  config('admin.directory').'/applications/reasons-rejection');
    });

    Breadcrumbs::for('admin.applications.reasons-rejection.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.applications.reasons-rejection');
        $breadcrumbs->push('Изменить причину',  config('admin.directory').'/applications/reasons-rejection'.$item->id.'/edit');
    });

    Breadcrumbs::for('admin.applications.reasons-defer', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications.settings');
        $breadcrumbs->push('Причины отложенных заявок',  config('admin.directory').'/applications/reasons-defer');
    });

    Breadcrumbs::for('admin.applications.reasons-defer.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications.reasons-defer');
        $breadcrumbs->push('Добавить причину',  config('admin.directory').'/applications/reasons-defer/create');
    });

    Breadcrumbs::for('admin.applications.reasons-defer.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.applications.reasons-defer');
        $breadcrumbs->push('Изменить причину',  config('admin.directory').'/applications/reasons-defer'.$item->id.'/edit');
    });


    Breadcrumbs::for('admin.applications.levels', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications');
        $breadcrumbs->push('Лимиты для операторов', admin_base_path('/applications/levels'));
    });

    Breadcrumbs::for('admin.applications.levels.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications.levels');
        $breadcrumbs->push('Добавить лимит', admin_base_path('/applications/levels/create'));
    });

    Breadcrumbs::for('admin.applications.levels.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.applications.levels');
        $breadcrumbs->push('Изменить лимит',  admin_base_path('/applications/levels/'.$item->id.'/edit'));
    });


    Breadcrumbs::for('admin.applications.levels-group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications.levels');
        $breadcrumbs->push('Группы лимитов', admin_base_path('/applications/levels-group'));
    });

    Breadcrumbs::for('admin.applications.levels-group.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.applications.levels-group');
        $breadcrumbs->push('Добавить группу', admin_base_path('/applications/levels-group/create'));
    });

    Breadcrumbs::for('admin.applications.levels-group.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.applications.levels-group');
        $breadcrumbs->push('Изменить группу '.$item->name,  admin_base_path('/applications/levels-group/'.$item->id.'/edit'));
    });








////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Работа парсеров
    Breadcrumbs::for('admin.crypto.parser', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Работа парсеров',  config('admin.directory').'/crypto/parser');
    });

    Breadcrumbs::for('admin.crypto.parser.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Добавить новый курс',  config('admin.directory').'/crypto/parser/create');
    });

    Breadcrumbs::for('admin.crypto.parser.change', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Изменить пару '.$item->name,  config('admin.directory').'/crypto/parser/edit/'.$item->id);
    });

    Breadcrumbs::for('admin.crypto.parser.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Сортировка источников',  config('admin.directory').'/crypto/parser/sorting');
    });

    Breadcrumbs::for('admin.crypto.parser.history', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('История курсов',  config('admin.directory').'/crypto/parser/history');
    });

    Breadcrumbs::for('admin.crypto.parser.history_id', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('История курсов '.$item->name,  config('admin.directory').'/crypto/parser/history/'.$item->id);
    });

    Breadcrumbs::for('admin.crypto.parser.show', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push(sprintf('Курсы источника %s', $item->name),  config('admin.directory').'/crypto/parser/'.$item->id);
    });

    // API Ключи источников
    Breadcrumbs::for('admin.crypto.parser.api_keys', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('API Ключи источников',  admin_base_path('/crypto/api-keys'));
    });

    Breadcrumbs::for('admin.crypto.parser.api_keys.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser.api_keys');
        $breadcrumbs->push('Добавить ключ',  admin_base_path('/crypto/api-keys/create'));
    });

    Breadcrumbs::for('admin.crypto.parser.api_keys.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.parser.api_keys');
        $breadcrumbs->push('Изменить ключ '.$item->api_key,  admin_base_path('/crypto/api-keys/'.$item->id.'/edit'));
    });

//Группы
    Breadcrumbs::for('admin.crypto.group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Группы',  config('admin.directory').'/crypto/group');
    });

    //BestChange парсер
    Breadcrumbs::for('admin.crypto.bestchange', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('BestChange парсер',  config('admin.directory').'/crypto/bestchange');
    });

    Breadcrumbs::for('admin.crypto.bestchange.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('Добавить новый курс',  config('admin.directory').'/crypto/bestchange/create');
    });

    Breadcrumbs::for('admin.crypto.bestchange.settings', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('Настройки парсинга',  config('admin.directory').'/crypto/bestchange/settings');
    });

    Breadcrumbs::for('admin.crypto.bestchange.log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('Bestchange Лог',  admin_base_path('/crypto/bestchange/log'));
    });

    Breadcrumbs::for('admin.crypto.bestchange.error_log', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('Bestchange: Лог ошибок '.$item->name,  admin_base_path('/crypto/bestchange/error_log/'.$item->id));
    });

    Breadcrumbs::for('admin.crypto.bestchange.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('Изменить курс',  config('admin.directory').'/crypto/bestchange/edit/'.$id);
    });

    Breadcrumbs::for('admin.crypto.bestchange.history', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('История курсов',  config('admin.directory').'/crypto/bestchange/history');
    });

    Breadcrumbs::for('admin.crypto.bestchange.history_id', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.bestchange');
        $breadcrumbs->push('История курсов '.$item->name,  config('admin.directory').'/crypto/bestchange/history/'.$item->id);
    });

    // Конкуренты
    Breadcrumbs::for('admin.crypto.competitors-parser', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Курсы конкурентов',  admin_base_path('/crypto/competitors-parser'));
    });

    Breadcrumbs::for('admin.crypto.competitors-parser.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.competitors-parser');
        $breadcrumbs->push('Добавить курс',  admin_base_path('/crypto/competitors-parser/create'));
    });

    Breadcrumbs::for('admin.crypto.competitors-parser.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.competitors-parser');
        $breadcrumbs->push('Изменить курс '.$item->name,  admin_base_path('/crypto/competitors-parser/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.crypto.competitors-parser.history', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.competitors-parser');
        $breadcrumbs->push('История курсов',  admin_base_path('/crypto/competitors-parser/history'));
    });

    Breadcrumbs::for('admin.crypto.competitors-parser.history_id', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.competitors-parser');
        $breadcrumbs->push('История курсов '.$item->name,  admin_base_path('/crypto/competitors-parser/history/'.$item->id));
    });


    // Ссылки конкурентов
    Breadcrumbs::for('admin.crypto.competitors-link', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.competitors-parser');
        $breadcrumbs->push('Ссылки конкурентов',  admin_base_path('/crypto/competitors-link'));
    });

    Breadcrumbs::for('admin.crypto.competitors-link.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.competitors-link');
        $breadcrumbs->push('Добавить ссылку',  admin_base_path('/crypto/competitors-link/create'));
    });

    Breadcrumbs::for('admin.crypto.competitors-link.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.competitors-link');
        $breadcrumbs->push('Изменить ссылку '.$item->name,  admin_base_path('/crypto/competitors-link/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.crypto.competitors-link.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.competitors-link');
        $breadcrumbs->push('Сортировка ссылок конкурентов',  admin_base_path('/crypto/competitors-link/sorting'));
    });

    // Формула курсов обмена
    Breadcrumbs::for('admin.crypto.parser-formula', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Курсы обмена из формулы',  admin_base_path('/crypto/parser-formula'));
    });

    Breadcrumbs::for('admin.crypto.parser-formula.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser-formula');
        $breadcrumbs->push('Добавить формулу',  admin_base_path('/crypto/parser-formula/create'));
    });

    Breadcrumbs::for('admin.crypto.parser-formula.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.parser-formula');
        $breadcrumbs->push('Обновить формулу '.$item->name,  admin_base_path('/crypto/parser-formula/'.$item->id.'/edit'));
    });


    // Доп. коэффициент для формулы
    Breadcrumbs::for('admin.crypto.formula-coefficient', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Коэффициенты для формул',  admin_base_path('/crypto/formula-coefficient'));
    });

    Breadcrumbs::for('admin.crypto.formula-coefficient.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.formula-coefficient');
        $breadcrumbs->push('Добавить коэффициент',  admin_base_path('/crypto/formula-coefficient/create'));
    });

    Breadcrumbs::for('admin.crypto.formula-coefficient.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.formula-coefficient');
        $breadcrumbs->push('Изменить коэффициент '.$item->name,  admin_base_path('/crypto/formula-coefficient/'.$item->id.'/edit'));
    });



    // Файлы курсов обмена
    Breadcrumbs::for('admin.crypto.file-parser', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Курсы обмена из файлов',  admin_base_path('/crypto/file-parser'));
    });

    Breadcrumbs::for('admin.crypto.file-parser.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.file-parser');
        $breadcrumbs->push('Добавить курс',  admin_base_path('/crypto/file-parser/create'));
    });

    Breadcrumbs::for('admin.crypto.file-parser.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.file-parser');
        $breadcrumbs->push('Изменить курс '.$item->name,  admin_base_path('/crypto/file-parser/'.$item->id.'/edit'));
    });

    // Ссылки конкурентов
    Breadcrumbs::for('admin.crypto.file-parser-group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.file-parser');
        $breadcrumbs->push('Группы курсов из файла',  admin_base_path('/crypto/file-parser-group'));
    });

    Breadcrumbs::for('admin.crypto.file-parser-group.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.file-parser-group');
        $breadcrumbs->push('Добавить источник',  admin_base_path('/crypto/file-parser-group/create'));
    });

    Breadcrumbs::for('admin.crypto.file-parser-group.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.crypto.file-parser-group');
        $breadcrumbs->push('Изменить источник '.$item->name,  admin_base_path('/crypto/file-parser-group/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.crypto.file-parser-group.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.file-parser-group');
        $breadcrumbs->push('Сортировка источников',  admin_base_path('/crypto/file-parser-group/sorting'));
    });


    // Файлы курсов обмена
    Breadcrumbs::for('admin.crypto.partner-parser', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.parser');
        $breadcrumbs->push('Список курсов всех партнеров',  admin_base_path('/crypto/partner-parser'));
    });

    Breadcrumbs::for('admin.crypto.partner-parser-group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.partner-parser');
        $breadcrumbs->push('Список партнеров для парсинга',  admin_base_path('/crypto/partner-parser-group'));
    });

    Breadcrumbs::for('admin.crypto.partner-parser-group.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.partner-parser-group');
        $breadcrumbs->push('Добавить партнера',  admin_base_path('/crypto/partner-parser-group/create'));
    });

    Breadcrumbs::for('admin.crypto.partner-parser-group.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.crypto.partner-parser-group');
        $breadcrumbs->push('Сортировка партнеров',  admin_base_path('/crypto/partner-parser-group/sorting'));
    });


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Пользователи
    Breadcrumbs::for('admin.account.users', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Пользователи',  config('admin.directory').'/account/users');
    });

    Breadcrumbs::for('admin.account.verification_account', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Верификация личности',  config('admin.directory').'/account/verify');
    });

    Breadcrumbs::for('admin.account.internal_account', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push(' Внутренний счет '.$item->name,  admin_base_path('/account/users/'.$item->id.'/internal_account'));
    });

//Пользователи
    Breadcrumbs::for('admin.account.users.payouts', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('История выплат',  config('admin.directory').'/account/users/payouts');
    });

    Breadcrumbs::for('admin.account.block_ip', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Фильтр по: IP или E-Mail',  config('admin.directory').'/account/block_ip');
    });

    Breadcrumbs::for('admin.account.logs_auth', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Лог авторизаций',  config('admin.directory').'/account/logs_auth');
    });

    Breadcrumbs::for('admin.account.admin_logs_auth', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Лог авторизаций в админпанели',  config('admin.directory').'/account/admin_logs_auth');
    });


    Breadcrumbs::for('admin.account.users.application', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Заявки пользователя',  config('admin.directory').'/account/users');
    });

    Breadcrumbs::for('admin.account.users.referral', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Рефералы клиента',  config('admin.directory').'/account/referral');
    });

    Breadcrumbs::for('admin.account.users.referral_statistics', function ($breadcrumbs, $user) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Реферальная статистика клиента '.$user->name,  config('admin.directory').'/account/users/'.$user->id.'/referral_statistics');
    });


    Breadcrumbs::for('admin.account.users.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Создать пользователя',  config('admin.directory').'/account/users/create');
    });

    Breadcrumbs::for('admin.account.users.change', function ($breadcrumbs, $user) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Редактировать пользователя '.$user->name,  config('admin.directory').'account/users/'.$user->id.'/edit');
    });

    Breadcrumbs::for('admin.account.users.balance', function ($breadcrumbs, $user) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Баланс пользователя '. $user->name,  config('admin.directory').'account/users/'.$user->id.'/balances');
    });

    Breadcrumbs::for('admin.account.users.google', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Настройка Google Authenticator',  config('admin.directory').'account/users/'.$id.'/google');
    });

    Breadcrumbs::for('admin.account.users.ban', function ($breadcrumbs, $user) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Забанить пользователя '.$user->name,  config('admin.directory').'account/users/'.$user->id.'/ban');
    });

    Breadcrumbs::for('admin.account.users.banned_history', function ($breadcrumbs, $user) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('История банов '.$user->name,  config('admin.directory').'/account/users/'.$user->id.'/banned_histories');
    });

    Breadcrumbs::for('admin.account.users.history_profiles', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('История профилей',  config('admin.directory').'/account/users/history_profiles');
    });



//Группы пользователей
    Breadcrumbs::for('admin.account.roles', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Группы пользователей',  config('admin.directory').'/account/roles');
    });

    Breadcrumbs::for('admin.account.roles.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.roles');
        $breadcrumbs->push('Создать группу',  config('admin.directory').'/account/roles/create');
    });

    Breadcrumbs::for('admin.account.roles.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.account.roles');
        $breadcrumbs->push('Изменить группу',  config('admin.directory').'account/roles/'.$id.'/edit');
    });

//Права доступа
    Breadcrumbs::for('admin.account.permissions', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.users');
        $breadcrumbs->push('Права доступа',  config('admin.directory').'/account/permissions');
    });

    Breadcrumbs::for('admin.account.permissions.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.account.permissions');
        $breadcrumbs->push('Создать права',  config('admin.directory').'/account/permissions/create');
    });

    Breadcrumbs::for('admin.account.permissions.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.account.permissions');
        $breadcrumbs->push('Изменить права',  config('admin.directory').'account/permissions/'.$id.'/edit');
    });



//Настройки системы
    Breadcrumbs::for('admin.settings', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Настройки продукта',  config('admin.directory').'/settings');
    });


    Breadcrumbs::for('admin.settings.instruments.events', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Журнал событий',  admin_base_path('/settings/instruments/event_logs'));
    });


    Breadcrumbs::for('admin.settings.scammers', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Настройка базы мошенников Bestchange',  config('admin.directory').'/settings/scammers');
    });

    Breadcrumbs::for('admin.settings.contacts', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Настройка контактов',  config('admin.directory').'/settings/contacts');
    });

    Breadcrumbs::for('admin.templates', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Настройка темы',  config('admin.directory').'/settings/themes');
    });

    Breadcrumbs::for('admin.settings.interface.home', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Настройка главной страницы',  admin_base_path('/settings/interface/home'));
    });

    Breadcrumbs::for('admin.settings.interface.image', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Настройка изображений',  admin_base_path('/settings/interface/image'));
    });

    Breadcrumbs::for('admin.settings.interface.menu', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Настройка меню',  admin_base_path('/settings/interface/menu'));
    });

    Breadcrumbs::for('admin.settings.interface.welcome', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Настройка приветствия',  admin_base_path('/settings/interface/welcome'));
    });

    // Почтовые шаблоны
    Breadcrumbs::for('admin.settings.menu-templates.email-template', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Почтовые шаблоны',  admin_base_path('/settings/menu-templates/email-template'));
    });

    Breadcrumbs::for('admin.settings.menu-templates.email-template.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings.menu-templates.email-template');
        $breadcrumbs->push('Добавление почтового шаблона',  admin_base_path('/settings/menu-templates/email-template/create'));
    });

    Breadcrumbs::for('admin.settings.menu-templates.email-template.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.settings.menu-templates.email-template');
        $breadcrumbs->push('Редактирование почтового шаблона #'.$item->id,  admin_base_path('/settings/menu-templates/email-template/'.$item->id.'/edit'));
    });

    // Почтовые шаблоны
    Breadcrumbs::for('admin.settings.menu-templates.type-events', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings');
        $breadcrumbs->push('Типы почтовых и СМС событий',  admin_base_path('/settings/menu-templates/type-events'));
    });

    Breadcrumbs::for('admin.settings.menu-templates.type-events.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.settings.menu-templates.type-events');
        $breadcrumbs->push('Добавление нового события',  admin_base_path('/settings/menu-templates/type-events/create'));
    });

    Breadcrumbs::for('admin.settings.menu-templates.type-events.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.settings.menu-templates.type-events');
        $breadcrumbs->push('Редактирование события '.$item->event_name,  admin_base_path('/settings/menu-templates/type-events/'.$item->id.'/edit'));
    });



    //Мерчтанты
    Breadcrumbs::for('admin.gateways', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
       // $breadcrumbs->push('Список платежных шлюзов',  config('admin.directory').'/gateways');
    });
    // Список мерчантов
    Breadcrumbs::for('admin.gateways.merchant', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways');
        $breadcrumbs->push('Список мерчантов',  admin_base_path('gateways/merchant'));
    });

    Breadcrumbs::for('admin.gateways.merchant.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways.merchant');
        $breadcrumbs->push('Добавить мерчант',  admin_base_path('gateways/merchant/create'));
    });

    Breadcrumbs::for('admin.gateways.merchant.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.gateways.merchant');
        $breadcrumbs->push('Изменить мерчант '.$item->name,  admin_base_path('gateways/merchant/'.$item->id.'/edit'));
    });

    // Список автовыплат
    Breadcrumbs::for('admin.gateways.autopayment', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways');
        $breadcrumbs->push('Список автовыплат',  admin_base_path('gateways/autopayment'));
    });

    Breadcrumbs::for('admin.gateways.autopayment.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways.autopayment');
        $breadcrumbs->push('Добавить автовыплату',  admin_base_path('gateways/autopayment/create'));
    });

    Breadcrumbs::for('admin.gateways.autopayment.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.gateways.autopayment');
        $breadcrumbs->push('Изменить автовыплату',  admin_base_path('gateways/autopayment/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.gateways.autopayment.event-merchant', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways');
        $breadcrumbs->push('Лог запросов к мерчанту',  admin_base_path('gateways/logs/event-merchant'));
    });

    Breadcrumbs::for('admin.gateways.autopayment.event-autopayment', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways');
        $breadcrumbs->push('Лог запросов для выплат',  admin_base_path('gateways/logs/event-autopayment'));
    });

    Breadcrumbs::for('admin.gateways.autopayment.event-orders', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways');
        $breadcrumbs->push('Лог запросов для ордеров',  admin_base_path('gateways/logs/event-orders'));
    });


    // Список автовыплат
    Breadcrumbs::for('admin.gateways.logs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.gateways.autopayment');
        $breadcrumbs->push('Список автовыплат',  admin_base_path('gateways/autopayment'));
    });

    //Новости
    Breadcrumbs::for('admin.tools.geoip.settings', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Настройки Geo IP',  admin_base_path('/tools/geoip/settings'));
    });

    Breadcrumbs::for('admin.tools.geoip.countries', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Страны',  admin_base_path('/tools/geoip/countries'));
    });


    //Список хранилищ
    Breadcrumbs::for('admin.tools.file-storages', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список хранилищ',  admin_base_path('/tools/file-storages'));
    });

    Breadcrumbs::for('admin.tools.file-storages.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.file-storages');
        $breadcrumbs->push('Изменить хранилище '.$item->name,  admin_base_path('tools/file-storages/'.$item->id.'/edit'));
    });

    //Новости
    Breadcrumbs::for('admin.tools.news', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Новости',  admin_base_path('/tools/news'));
    });

    Breadcrumbs::for('admin.tools.news.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.news');
        $breadcrumbs->push('Добавить новость',  admin_base_path('tools/news/create'));
    });

    Breadcrumbs::for('admin.tools.news.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.news');
        $breadcrumbs->push('Изменить новость №'.$id,  admin_base_path('tools/news/'.$id.'/edit'));
    });


    //Отзывы
    Breadcrumbs::for('admin.tools.reviews', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список отзывов',  admin_base_path('/tools/reviews'));
    });

    Breadcrumbs::for('admin.tools.reviews.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.reviews');
        $breadcrumbs->push('Добавить отзыв',  admin_base_path('tools/reviews/create'));
    });

    Breadcrumbs::for('admin.tools.reviews.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.reviews');
        $breadcrumbs->push('Изменить отзыв №'.$id,  admin_base_path('tools/reviews/'.$id.'/edit'));
    });


    //Система авторизаций
    Breadcrumbs::for('admin.tools.auth-system', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Система авторизаций',  admin_base_path('/tools/auth-system'));
    });

    Breadcrumbs::for('admin.tools.auth-system.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.auth-system');
        $breadcrumbs->push('Добавить систему',  admin_base_path('tools/auth-system/create'));
    });

    Breadcrumbs::for('admin.tools.auth-system.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.auth-system');
        $breadcrumbs->push('Изменить систему '.$item->name,  admin_base_path('tools/auth-system/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.auth-system.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.auth-system');
        $breadcrumbs->push('Сортировка сервисов',  admin_base_path('tools/auth-system/sorting'));
    });

    //Контакты
    Breadcrumbs::for('admin.tools.contacts', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список контактов',  admin_base_path('/tools/contacts'));
    });

    Breadcrumbs::for('admin.tools.contacts.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.contacts');
        $breadcrumbs->push('Добавить контакт',  admin_base_path('tools/contacts/create'));
    });

    Breadcrumbs::for('admin.tools.contacts.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.contacts');
        $breadcrumbs->push('Изменить контакт '.$item->name,  admin_base_path('tools/contacts/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.contacts.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.contacts');
        $breadcrumbs->push('Сортировка контактов',  admin_base_path('tools/contacts/sorting'));
    });


    //Сотрудничество и PR
    Breadcrumbs::for('admin.tools.collaboration-pr', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.contacts');
        $breadcrumbs->push('Список контактов для сотрудичество и PR',  admin_base_path('/tools/collaboration-pr'));
    });

    Breadcrumbs::for('admin.tools.collaboration-pr.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.collaboration-pr');
        $breadcrumbs->push('Добавить контакт',  admin_base_path('tools/collaboration-pr/create'));
    });

    Breadcrumbs::for('admin.tools.collaboration-pr.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.collaboration-pr');
        $breadcrumbs->push('Изменить контакт '.$item->name,  admin_base_path('tools/collaboration-pr/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.collaboration-pr.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.collaboration-pr');
        $breadcrumbs->push('Сортировка контактов',  admin_base_path('tools/collaboration-pr/sorting'));
    });

    Breadcrumbs::for('admin.tools.job', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Статус работы сервиса',  config('admin.directory').'/tools/job');
    });

    Breadcrumbs::for('admin.tools.log', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Просмотр журнала логов',  config('admin.directory').'/tools/log');
    });

//Статистика
    Breadcrumbs::for('admin.tools.statistics', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Статистика',  config('admin.directory').'/tools/statistics');
    });

    // Уведомлении
    Breadcrumbs::for('admin.tools.notification', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Уведомлении',  admin_path('tools/notification'));
    });

    Breadcrumbs::for('admin.tools.notification.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.notification');
        $breadcrumbs->push('Добавить уведомление',  admin_path('tools/notification/create'));
    });

    Breadcrumbs::for('admin.tools.notification.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.notification');
        $breadcrumbs->push('Изменить уведомление',  admin_path('tools/notification/edit/?id='.$id));
    });

    Breadcrumbs::for('admin.tools.notification.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.notification');
        $breadcrumbs->push('Сортировка уведомлений',  admin_path('tools/notification/sorting'));
    });

    // Live Уведомлении
    Breadcrumbs::for('admin.tools.live-notification', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.notification');
        $breadcrumbs->push('Уведомления в реальном времени',  admin_path('tools/live-notification'));
    });

    Breadcrumbs::for('admin.tools.live-notification.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.live-notification');
        $breadcrumbs->push('Добавить уведомление',  admin_path('tools/live-notification/create'));
    });

    Breadcrumbs::for('admin.tools.live-notification.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.live-notification');
        $breadcrumbs->push('Изменить уведомление',  admin_path('tools/live-notification/edit/?id='.$id));
    });


    // Баннеры
    Breadcrumbs::for('admin.tools.banners', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список баннеров',  admin_base_path('tools/banners'));
    });
    Breadcrumbs::for('admin.tools.banners.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.banners');
        $breadcrumbs->push('Сортировка баннеров',  admin_base_path('tools/banners/sorting'));
    });

    Breadcrumbs::for('admin.tools.banners.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.banners');
        $breadcrumbs->push('Добавить баннер',  admin_path('tools/banners/create'));
    });

    Breadcrumbs::for('admin.tools.banners.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.banners');
        $breadcrumbs->push('Изменить баннер',  admin_base_path('tools/banners/'.$id.'/edit'));
    });

    // Баннеры для кнопок
    Breadcrumbs::for('admin.tools.banners.button', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.banners');
        $breadcrumbs->push('Список кнопок для баннера',  admin_base_path('tools/banners-button'));
    });

    Breadcrumbs::for('admin.tools.banners.button.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.banners.button');
        $breadcrumbs->push('Добавить кнопку',  admin_path('tools/banners-button/create'));
    });

    Breadcrumbs::for('admin.tools.banners.button.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.banners.button');
        $breadcrumbs->push('Изменить кнопку',  admin_base_path('tools/banners-button/'.$id.'/edit'));
    });

    // Преимущество
    Breadcrumbs::for('admin.tools.advantage', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список преимуществ',  admin_base_path('tools/advantage'));
    });

    Breadcrumbs::for('admin.tools.advantage.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.advantage');
        $breadcrumbs->push('Добавить новое преимущество',  admin_path('tools/advantage/create'));
    });

    Breadcrumbs::for('admin.tools.advantage.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.advantage');
        $breadcrumbs->push('Изменить преимущество',  admin_base_path('tools/advantage/edit/?id='.$id));
    });

    Breadcrumbs::for('admin.tools.advantage.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.advantage');
        $breadcrumbs->push('Сортировка преимуществ',  admin_base_path('tools/advantage/sorting/'));
    });

    // Ссылки на отзывы
    Breadcrumbs::for('admin.tools.links_reviews', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Ссылки на отзывы',  admin_base_path('tools/links_reviews'));
    });

    Breadcrumbs::for('admin.tools.links_reviews.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_reviews');
        $breadcrumbs->push('Добавить ссылку',  admin_path('tools/links_reviews/create'));
    });

    Breadcrumbs::for('admin.tools.links_reviews.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.links_reviews');
        $breadcrumbs->push('Изменить ссылку',  admin_base_path('tools/links_reviews/'.$id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.links_reviews.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_reviews');
        $breadcrumbs->push('Сортировка ссылок',  admin_base_path('tools/links_reviews/sorting/'));
    });

    // Группа ссылок на отзывы
    Breadcrumbs::for('admin.tools.links_reviews_group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_reviews');
        $breadcrumbs->push('Список групп',  admin_base_path('tools/links_reviews_group'));
    });

    Breadcrumbs::for('admin.tools.links_reviews_group.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_reviews_group');
        $breadcrumbs->push('Добавить группу',  admin_path('tools/links_reviews_group/create'));
    });

    Breadcrumbs::for('admin.tools.links_reviews_group.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.links_reviews_group');
        $breadcrumbs->push('Изменить группу',  admin_base_path('tools/links_reviews_group/'.$id.'/edit'));
    });


    // Ссылки для Footer
    Breadcrumbs::for('admin.tools.links_footers', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Ссылки для Footer',  admin_base_path('tools/links_footers'));
    });

    Breadcrumbs::for('admin.tools.links_footers.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_footers');
        $breadcrumbs->push('Добавить ссылку',  admin_path('tools/links_footers/create'));
    });

    Breadcrumbs::for('admin.tools.links_footers.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.links_footers');
        $breadcrumbs->push('Изменить ссылку',  admin_base_path('tools/links_footers/'.$id.'/edit'));
    });

    // Группа ссылок для Footer
    Breadcrumbs::for('admin.tools.links_footers_group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_footers');
        $breadcrumbs->push('Список групп',  admin_base_path('tools/links_footers_group'));
    });

    Breadcrumbs::for('admin.tools.links_footers_group.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.links_footers_group');
        $breadcrumbs->push('Добавить группу',  admin_path('tools/links_footers_group/create'));
    });

    Breadcrumbs::for('admin.tools.links_footers_group.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.links_footers_group');
        $breadcrumbs->push('Изменить группу',  admin_base_path('tools/links_footers_group/'.$id.'/edit'));
    });

    // Ссылки на соц. сети
    Breadcrumbs::for('admin.tools.social-reviews', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Ссылки на соц. сети',  admin_base_path('tools/social-reviews'));
    });

    Breadcrumbs::for('admin.tools.social-reviews.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.social-reviews');
        $breadcrumbs->push('Добавить ссылку',  admin_path('tools/social-reviews/create'));
    });

    Breadcrumbs::for('admin.tools.social-reviews.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.social-reviews');
        $breadcrumbs->push('Изменить ссылку',  admin_base_path('tools/social-reviews/'.$id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.social-reviews.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.social-reviews');
        $breadcrumbs->push('Сортировка ссылок',  admin_base_path('tools/social-reviews/sorting/'));
    });

    // Статистика
    Breadcrumbs::for('admin.tools.info-statistics', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список статистики',  admin_base_path('tools/statistics'));
    });

    Breadcrumbs::for('admin.tools.info-statistics.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.info-statistics');
        $breadcrumbs->push('Добавить статистику',  admin_base_path('tools/statistics/create'));
    });

    Breadcrumbs::for('admin.tools.info-statistics.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.info-statistics');
        $breadcrumbs->push('Изменить статистику '.$item->name,  admin_base_path('tools/statistics/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.info-statistics.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.info-statistics');
        $breadcrumbs->push('Сортировка статистики',  admin_base_path('tools/statistics/sorting/'));
    });

    // Верификация карты
    Breadcrumbs::for('admin.tools.verification_card', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Верификация карт',  admin_base_path('verifications/cards'));
    });

    // Категории верификации карт
    Breadcrumbs::for('admin.tools.verification-card-category', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.verification_card');
        $breadcrumbs->push('Список Категории',  config('admin.directory').'/verifications/card-category');
    });


    Breadcrumbs::for('admin.tools.verification-card-category.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.verification-card-category');
        $breadcrumbs->push('Добавить категорию',  config('admin.directory').'/verifications/card-category/create');
    });

    Breadcrumbs::for('admin.tools.verification-card-category.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.verification-card-category');
        $breadcrumbs->push('Изменить категорию № '.$item->id,  config('admin.directory').'/verifications/card-category/'.$item->id.'/edit');
    });

    // Инструкция верификации карт
    Breadcrumbs::for('admin.tools.verification-card-instructions', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.verification_card');
        $breadcrumbs->push('Список Инструкций',  config('admin.directory').'/verifications/card-instructions');
    });


    Breadcrumbs::for('admin.tools.verification-card-instructions.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.verification-card-instructions');
        $breadcrumbs->push('Добавить инструкцию',  config('admin.directory').'/verifications/card-instructions/create');
    });

    Breadcrumbs::for('admin.tools.verification-card-instructions.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.verification-card-instructions');
        $breadcrumbs->push('Изменить инструкцию № '.$item->id,  config('admin.directory').'/verifications/card-instructions/'.$item->id.'/edit/');
    });


    Breadcrumbs::for('admin.tools.logs-404', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Лог 404 ошибок',  admin_path('tools/logs-404'));
    });


    Breadcrumbs::for('admin.tools.logs-email', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Лог E-mail уведомлений',  admin_path('tools/logs-email\''));
    });


//Страницы
    Breadcrumbs::for('admin.tools.pages', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список страниц',  config('admin.directory').'/tools/pages');
    });

    Breadcrumbs::for('admin.tools.pages.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.pages');
        $breadcrumbs->push('Добавить страницу',  config('admin.directory').'/tools/pages/create');
    });

    Breadcrumbs::for('admin.tools.pages.change', function ($breadcrumbs, $page) {
        $breadcrumbs->parent('admin.tools.pages');
        $breadcrumbs->push('Изменить страницу '.$page->page_title,  admin_base_path('/tools/pages/'.$page->page_id.'/edit'));
    });

    //Правила сайта
    Breadcrumbs::for('admin.tools.rules_pages', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список правил',  config('admin.directory').'/tools/rules_pages');
    });

    Breadcrumbs::for('admin.tools.rules_pages.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.rules_pages');
        $breadcrumbs->push('Добавить ссылку',  config('admin.directory').'/tools/rules_pages/create');
    });

    Breadcrumbs::for('admin.tools.rules_pages.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.rules_pages');
        $breadcrumbs->push('Изменить ссылку '.$item->title,  config('admin.directory').'/tools/rules_pages/'.$item->id.'/edit');
    });

    Breadcrumbs::for('admin.tools.rules_pages.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.rules_pages');
        $breadcrumbs->push('Сортировка ссылок',  config('admin.directory').'/tools/rules_pages/sorting');
    });



    //Навигация
    Breadcrumbs::for('admin.tools.menu', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список навигационного меню',  config('admin.directory').'/tools/menu');
    });

    Breadcrumbs::for('admin.tools.menu.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.menu');
        $breadcrumbs->push('Добавить ссылку',  config('admin.directory').'/tools/menu/create');
    });

    Breadcrumbs::for('admin.tools.menu.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.menu');
        $breadcrumbs->push('Изменить ссылку',  config('admin.directory').'/tools/menu/'.$id.'/edit');
    });

    Breadcrumbs::for('admin.tools.menu.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.menu');
        $breadcrumbs->push('Сортировка меню',  config('admin.directory').'/tools/menu/sorting');
    });


//Вопросы и ответы
    Breadcrumbs::for('admin.tools.faq', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('FAQ - Часто задаваемые вопросы',  config('admin.directory').'/tools/faq');
    });

    Breadcrumbs::for('admin.tools.faq.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.faq');
        $breadcrumbs->push('Добавить FAQ',  config('admin.directory').'/tools/faq/create');
    });

    Breadcrumbs::for('admin.tools.faq.edit', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.faq');
        $breadcrumbs->push('Изменить FAQ',  config('admin.directory').'/tools/faq/'.$id.'/edit');
    });

    Breadcrumbs::for('admin.tools.faq.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.faq');
        $breadcrumbs->push('Сортировка вопросов и ответов',  config('admin.directory').'/tools/faq/sorting');
    });

    Breadcrumbs::for('admin.tools.faq.category', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.faq');
        $breadcrumbs->push('Список категорий',  config('admin.directory').'/tools/faq-category');
    });



    Breadcrumbs::for('admin.tools.faq.category.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.faq.category');
        $breadcrumbs->push('Добавить категорию',  admin_base_path('/tools/faq-category/create'));
    });

    Breadcrumbs::for('admin.tools.faq.category.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.tools.faq.category');
        $breadcrumbs->push('Изменить категорию '.$item->name,  admin_base_path('/tools/faq-category/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.tools.faq.category.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.faq.category');
        $breadcrumbs->push('Сортировка категорий',  config('admin.directory').'/tools/faq-category/sorting');
    });

//Документация
    Breadcrumbs::for('admin.tools.docs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Документация - Список статей',  config('admin.directory').'/tools/docs');
    });

    Breadcrumbs::for('admin.tools.docs.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.docs');
        $breadcrumbs->push('Добавить статью',  config('admin.directory').'/tools/docs/create');
    });

    Breadcrumbs::for('admin.tools.docs.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.docs');
        $breadcrumbs->push('Изменить статью',  config('admin.directory').'/tools/docs/'.$id.'/edit');
    });


//Firewall
    Breadcrumbs::for('admin.tools.firewall', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Firewall',  config('admin.directory').'/tools/firewall');
    });

//Экспорт курсов
    Breadcrumbs::for('admin.tools.generator_currency', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Экспорт курсов',  config('admin.directory').'/tools/generator_currency');
    });

    //Резервное копирование
    Breadcrumbs::for('admin.backup', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Резервное копирование',  admin_base_path('/backup'));
    });

    Breadcrumbs::for('admin.backup.disk', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.backup');
        $breadcrumbs->push('Диск: '.$item['name'],  admin_base_path('/backup/'.$item['name']));
    });


//Реферальная система
    Breadcrumbs::for('admin.affiliate.referral', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Реферальные программы',  config('admin.directory').'/affiliate/referral');
    });

    //Реферальная система
    Breadcrumbs::for('admin.affiliate.settings', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Настройка реферальной системы и cashback',  admin_base_path('/affiliate/settings'));
    });

    Breadcrumbs::for('admin.affiliate.referral.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Добавить программу',  admin_base_path('/affiliate/referral/create'));
    });

    Breadcrumbs::for('admin.affiliate.referral.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Изменить программу '.$item->name,  admin_base_path('/affiliate/referral/'.$item->id.'/edit'));
    });

    Breadcrumbs::for('admin.affiliate.referral.statistics', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Реферальная статистика',  config('admin.directory').'/affiliate/referral/statistics');
    });

    Breadcrumbs::for('admin.affiliate.referral.logs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Лог реферальной программы',  config('admin.directory').'/affiliate/referral/logs');
    });

    Breadcrumbs::for('admin.affiliate.referral.transitions', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Реферальные переходы',  admin_base_path('/affiliate/referral/transitions'));
    });

    Breadcrumbs::for('admin.affiliate.referral.transitions_exchange', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral.transitions');
        $breadcrumbs->push('Реферальные направления',  admin_base_path('/affiliate/referral/transitions_exchange'));
    });

    Breadcrumbs::for('admin.affiliate.referral.referrals', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Список рефералов',  admin_base_path('/affiliate/referral/referrals'));
    });

    Breadcrumbs::for('admin.affiliate.referral.exchanges', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Партнерские обмены',  admin_base_path('/affiliate/referral/exchanges'));
    });


    Breadcrumbs::for('admin.affiliate.referral.exchanges_group', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral.exchanges');
        $breadcrumbs->push('Сгрупированные партнерские обмены',  admin_base_path('/affiliate/referral/exchanges_group'));
    });

    //Бонусная программа
    Breadcrumbs::for('admin.affiliate.bonus', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Бонусные программы',  config('admin.directory').'/affiliate/bonus');
    });

    Breadcrumbs::for('admin.affiliate.bonus.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.bonus');
        $breadcrumbs->push('Добавить программу',  config('admin.directory').'/affiliate/bonus/create');
    });

    Breadcrumbs::for('admin.affiliate.bonus.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.affiliate.bonus');
        $breadcrumbs->push('Изменить программу '.$item->name,  config('admin.directory').'/affiliate/bonus/'.$item->id.'/edit');
    });

    Breadcrumbs::for('admin.affiliate.bonus.exchanges', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.bonus');
        $breadcrumbs->push('Бонусные обмены',  admin_base_path('/affiliate/bonus/exchanges'));
    });

    Breadcrumbs::for('admin.affiliate.bonus.logs', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.bonus');
        $breadcrumbs->push('Не начисленные бонусы',  admin_base_path('/affiliate/bonus/logs'));
    });

    // Партнерские условия
    Breadcrumbs::for('admin.affiliate.conditions.referral', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.referral');
        $breadcrumbs->push('Условия реферальной программы',  admin_base_path('affiliate/conditions/referral'));
    });

    Breadcrumbs::for('admin.affiliate.conditions.monitoring', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Условия для мониторингов',  admin_base_path('/affiliate/conditions/monitoring'));
    });

    Breadcrumbs::for('admin.affiliate.conditions.cashback', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.affiliate.bonus');
        $breadcrumbs->push('Условия кэшбэка',  admin_base_path('/affiliate/conditions/cashback'));
    });



// Аналитика
    Breadcrumbs::for('admin.analytics.summary', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Сводный отчет',  config('admin.directory').'/analytics/summary');
    });

    Breadcrumbs::for('admin.analytics.event_reserve', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Список событий по резервам',  config('admin.directory').'/analytics/event_reserve');
    });

    Breadcrumbs::for('admin.analytics.exchange', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Общая сумма обменов',  config('admin.directory').'/analytics/exchange');
    });


//Аналитика - Сотрудники
    Breadcrumbs::for('admin.analytics.employees', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Сотрудники',  config('admin.directory').'/analytics/employees');
    });

    Breadcrumbs::for('admin.analytics.employees.fine', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.analytics.employees');
        $breadcrumbs->push('Штрафы менеджера '.$item->name,  config('admin.directory').'/analytics/employees/fine');
    });

    Breadcrumbs::for('admin.analytics.employees.event', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.analytics.employees');
        $breadcrumbs->push('Событии менеджера '.$item->name,  config('admin.directory').'/analytics/employees/event');
    });

    Breadcrumbs::for('admin.analytics.employees.cards', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.analytics.employees');
        $breadcrumbs->push('Банковские карты менеджера '.$item->name,  config('admin.directory').'/analytics/employees/cards');
    });

    Breadcrumbs::for('admin.analytics.employees.edit', function ($breadcrumbs, $item) {
        $breadcrumbs->parent('admin.analytics.employees');
        $breadcrumbs->push('Изменить детали сотрудника '.$item->user->name,  config('admin.directory').'/analytics/employees/'.$item->id.'/edit');
    });

    Breadcrumbs::for('admin.analytics.employees.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.analytics.employees');
        $breadcrumbs->push('Добавить сотрудника',  config('admin.directory').'/analytics/employees/create');
    });


    //Партнеры
    Breadcrumbs::for('admin.tools.partners', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push('Партнеры',  config('admin.directory').'/tools/partners');
    });

    Breadcrumbs::for('admin.tools.partners.add', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.partners');
        $breadcrumbs->push('Добавить партнера',  config('admin.directory').'/tools/partners/add');
    });

    Breadcrumbs::for('admin.tools.partners.change', function ($breadcrumbs, $id) {
        $breadcrumbs->parent('admin.tools.partners');
        $breadcrumbs->push('Изменить партнера',  config('admin.directory').'/tools/partners/change/?id='.$id);
    });


    Breadcrumbs::for('admin.tools.partners.sorting', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.tools.partners');
        $breadcrumbs->push('Сортировка партнеров',  config('admin.directory').'/tools/partners/sorting');
    });


    //Главная
    Breadcrumbs::for('admin.api', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.home');
        $breadcrumbs->push(__('Список API'),  admin_base_path('/api'));
    });


} catch (\Diglactic\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {
    \Log::debug($e->getMessage());
}

