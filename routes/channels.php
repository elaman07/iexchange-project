<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Models\User;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('order.user.{id_user}', function($user, $id_user) {
    return (int) $user->id === (int) $id_user;
});

Broadcast::channel('order.chat.notification.{id_user}', function($user, $id_user) {
    return (int) $user->id === (int) $id_user;
});

Broadcast::channel('order.chat.messages.{public_id}', function (User $user, $public_id) {
    return $user->id === \App\Models\Task::where('public_id', $public_id)->first()->id_user;
});
