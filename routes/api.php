<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['auth:sanctum'], 'namespace' => 'App\\Common\\UserAPI\\Http\\Controllers', 'prefix' => 'v2'], function()
{
    Route::get('/ping', 'PingController@ping');

    Route::group(['prefix' => 'account'], function() {
        Route::get('/info', 'AccountController@info');
    });

    Route::group(['prefix' => 'exchange'], function() {
        Route::get('/details', 'ExchangeController@details');
    });

    Route::group(['prefix' => 'order'], function() {
        Route::get('/status', 'OrderController@status');
        Route::post('/create', 'OrderController@create');
        Route::post('/confirm','OrderController@confirm');
        Route::post('/cancel','OrderController@cancel');
    });

    Route::group(['prefix' => 'partner'], function() {
        Route::get('/info', 'PartnerController@info');
        Route::get('/links', 'PartnerController@links');
        Route::get('/exchanges', 'PartnerController@exchanges');
        Route::get('/payouts', 'PartnerController@payouts');
    });
});


//Route::group(['middleware' => ['auth:api','throttle'], 'namespace' => 'Api\V2', 'prefix' => 'v2'], function()
//{
//    Route::group(['middleware' => ['scope:account']], function()
//    {
//        Route::get('account', 'UserController@getUser');
//        Route::get('account/logs', 'UserController@getLogAuth');
//        Route::put('account', 'UserController@putuser');
//        Route::put('password', 'UserController@password');
//
//        //Список рефералов
//        Route::apiResource('referrals', 'ReferralController');
//
//        Route::group(['prefix' => 'referral_incomes'], function() {
//            Route::apiResource('/', 'ReferralIncomesController');
//            Route::get('total', 'ReferralIncomesController@total');
//        });
//
//        Route::get('referrals/total', 'ReferralController@total');
//
//        Route::get('/ping', 'PingController@ping');
//        Route::get('reserves','ReserveController@index');
//        Route::get('faq/getCategories', 'FaqController@getCategories');
//        Route::get('faq', 'FaqController@getFaq');
//        Route::get('faq/{item_id}', 'FaqController@getById')->where('item_id', '[0-9]+');
//
//        Route::post('/payouts','PayoutsController@create');
//    });
//
//    Route::group(['middleware' => ['scope:orders']], function() {
//        Route::get('orders', 'OrderController@getOrder');
//        Route::get('orders/{order_id}', 'OrderController@getOrderId')->where('order_id', '[0-9]+');
//        Route::post('orders/getState','OrderController@getState');
//    });
//
//    Route::group(['middleware' => ['scope:payments']], function() {
//        Route::get('payment-systems', 'PaymentController@getPayment');
//        Route::get('payment-systems/{payment_id}', 'PaymentController@getPaymentId')->where('payment_id', '[0-9]+');
//    });
//});
