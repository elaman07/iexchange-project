<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Common\Packages\Crypto\Facades\CryptoFacade;
use App\Common\Packages\SecureHeaders\SecureHeadersMiddleware;
use App\Http\Middleware\BlacklistMiddleware;
use Illuminate\Support\Facades\Route;
use Spatie\Csp\AddCspHeaders;


//Для тестирования
$iex_test_route = __DIR__.'/test.php';
if(env('APP_ENV') == 'local' and \File::exists($iex_test_route)) {
    include_once $iex_test_route;
}

//Для администраторов
include_once __DIR__.'/web/admin.php';

// Для уведомлений о платежах
include_once __DIR__.'/callback.php';

// Дополнительно
include_once __DIR__.'/actions.php';


Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::get('/email-verified', 'Auth\VerificationController@successVerified');


Route::permanentRedirect('/.well-known/carddav', '/');
Route::permanentRedirect('/.well-known/caldav', '/');
Route::permanentRedirect('/.well-known/security.txt', '/');
Route::permanentRedirect('/error.log', '/');

/**
 * Страницы к ошибкой 404
*/
foreach (config('exchange.custom_404_pages') as $item)
{
    Route::get($item, function() {
        abort(404);
    });
}

// Серверный XML курс
if((int)iEXSetting('grates_static') == 1)
{
    Route::get(sprintf('/%s.xml', iEXSetting('grates_filename')), function(Request $request)
    {
        if(isJobOffline()) {
            return response()->xml(null, 200, [], 'rates');
        }

        $xml = CryptoFacade::setSchema('xml');
        return response()->xml([
            'item' => $xml->getViewArray()
        ], 200, [], 'rates');
    });
}


// Авторизация через соц. сети
Route::get('/oauth/{provider}', 'Auth\OAuthController@redirectToProvider');
Route::get('/oauth/{provider}/callback', 'Auth\OAuthController@callback');

Route::middleware('auth')->get('/banned', 'HomeController@banned');
Route::middleware('auth')->get('/banned/ip', 'HomeController@bannedIp');


// Новая версия мерчанта
Route::group(['prefix' => 'payment_status'], function() {
    Route::get('/checkout/{hash}', 'PaymentStatusController@checkout')->name('merchant.checkout');
    Route::get('/checkout_request/{hash}', 'PaymentStatusController@checkout_request')->name('merchant.checkout_request');
});

// Подтверждение различных событий
Route::group(['prefix' => 'confirm'], function() {
    Route::get('/{id}/payouts', 'ConfirmController@confirmPayouts');
});


// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

/**
 * Routing часть доступна не-заблокированным клиентам
*/
Route::group(['middleware' => [
    'client-log', 'forbid-banned-user', 'xss-filter', AddCspHeaders::class,
    SecureHeadersMiddleware::class, BlacklistMiddleware::class]], function()
{
    Route::prefix('api')->group(function()
    {


        // Password Reset Routes...
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


        //Авторизация
        Route::post('/login','Auth\LoginController@login');
        //Регистрация
        Route::post('/register','Auth\RegisterController@register');
        // Быстрая регистрация
        Route::post('/fast_register', 'Auth\FastRegisterController@register');

        Route::prefix('v1')->namespace('Api\V1')->group(function()
        {
            // API Партнеров
            Route::get('/rates_partner', 'RatesPartnerController@rates');
            // API Платежных реквизитов
            Route::post('/post_requisites', 'PostRequisitesController@index');

            // Платежные системы
            Route::get('/payment_systems', 'PaymentSystemsController@index')->middleware(['auth']);

            Route::get('/user_currencies', 'PaymentSystemsController@user_currencies')->middleware(['auth']);
            Route::get('/user_wallets', 'PaymentSystemsController@user_wallets')->middleware(['auth']);
            Route::get('/delete_wallet/{id}', 'PaymentSystemsController@delete_wallet')->middleware(['auth']);


            // Информация о пользователе
            Route::get('/sessions/current', 'SessionController@current');

            Route::group(['prefix' => 'referrals'], function() {
                Route::get('/user', 'ReferralIncomesController@user');
                Route::put('/settings', 'ReferralIncomesController@settings');
            });

            // Статические страницы
            Route::get('/static_page/{type}', 'StaticPageController@static_page');

            // Список новостей
            Route::get('/getNews', 'NewsController@index');
            Route::get('/getNews/{id}', 'NewsController@show');

            // Список отзывов
            Route::post('/addReview', 'ReviewsController@addReview');
            Route::get('/getReviews', 'ReviewsController@index');
            Route::get('/reviewInfo', 'ReviewsController@reviewInfo');

            // Верификация учетной записи
            Route::get('/user-verify/info', 'UserVerifyController@index');
            Route::post('/user-verify/create', 'UserVerifyController@create');


            Route::get('/verification-card/info', 'VerificationController@index');
            Route::post('/verification-card/create', 'VerificationController@create');
            Route::get('/verification-card/{order_id}/status', 'VerificationController@status');

            Route::get('partners', 'PartnersController@index');
            Route::get('faq', 'FAQController@index');

            Route::group(['prefix' => 'pages'], function() {
                Route::get('/{page_slug}', 'PageController@getPageId');
            });


            Route::get('/referral_incomes', 'ReferralIncomesController@items')->middleware(['auth']);


            // Новый роутер для работы с заявками
            Route::get('/orders','AccountOrderController@orders')->middleware(['auth']);
            Route::post('/has_currency_reserve', 'AccountOrderController@hasReserve');
            Route::group(['prefix' => 'order'], function()
            {
                // Детали заявки оплаты
                Route::post('/create', 'AccountOrderController@create');
                Route::post('/add_image', 'AccountOrderController@addImage');
                Route::post('/{public_id}/process', 'AccountOrderController@process');
                Route::post('/{public_id}/confirm', 'AccountOrderController@confirm');
                Route::get('/{public_id}/status', 'AccountOrderController@status');
                Route::post('/{public_id}/cancel', 'AccountOrderController@cancel');
                Route::get('/{public_id}/account', 'AccountOrderController@account');


            });

            Route::group(['prefix' => 'user'], function() {

                Route::get('/update_restapi', 'AccountController@update_restapi')->middleware(['auth']);

                Route::get('/', 'AccountController@user')->middleware(['auth']);
                Route::put('/', 'AccountController@user_update')->middleware(['auth']);

                // Обновление пароля
                Route::put('/change_password', 'AccountController@update_password')->middleware(['auth']);

                Route::post('/user_style', 'AccountController@user_style')->middleware(['auth']);
                Route::get('/is_auth', 'AccountController@isAuth');
                Route::get('/user_balance', 'AccountController@user_balance')->middleware(['auth']);

            });


            // Аккаунт -> Партнерам
            Route::group(['prefix' => 'account-partners', 'middleware' => ['auth']], function() {

                Route::get('/', 'AccountPartnerController@info');
                Route::get('/histories', 'AccountPartnerController@histories');
                Route::put('/update', 'AccountPartnerController@updateUsername');

                // Информация о партнерском аккаунте
                Route::get('/withdrawal', 'AccountWithdrawalController@withdrawalInfo');
                // Создание заявки на выплату бонусов
                Route::post('/withdrawal', 'AccountWithdrawalController@create');
                // История выводов
                Route::get('/withdrawal-histories', 'AccountWithdrawalController@histories');
            });


            Route::group(['prefix' => 'job'], function () {
                Route::get('/status_site','StatusController@index');
                Route::get('/status_work', 'StatusController@fetchStatusWork');
            });

            Route::get('/getSettings','SettingsController@index');
            Route::get('/getMenu','SettingsController@menu');
//            Route::get('/getPaymentBonuses', 'BonusController@index');
//            Route::get('/getOrderBonuses', 'BonusController@bonus');
//            Route::post('/postPaymentBonuses', 'BonusController@handler');
        });
    });

    Route::get('/logout','Auth\LoginController@logout')->middleware(['auth']);
    Route::get('/check/{id}', 'CheckController@handler');


    Route::post('/events/broadcasting/list', [\App\Http\Controllers\Broadcasting\OrderChatController::class, 'fetchMessages']);
    Route::post('/events/broadcasting/create', [\App\Http\Controllers\Broadcasting\OrderChatController::class, 'sendMessage']);


    Route::get('/ieximg/default.jpg', function (Request $request)
    {
        $filename = null;
        if($request->has('filename')) {
            $filename = security_xss($request->get('filename'));
        }

        $type = null;
        if($request->has('type')) {
            $type = security_xss($request->get('type'));
        }

        $disk = null;
        if($type == 'payment') {
            $disk = iEXSetting('storage_disk_payment_system');
        } elseif($type == 'verifications_card') {
            $disk = iEXSetting('storage_disk_verification_card');
        } elseif($type == 'news') {
            $disk = iEXSetting('storage_disk_news');
        }

        try {
            return Storage::disk($disk)->download($filename, $filename, [
                'Content-Disposition' => 'inline'
            ]);
        }catch (\Exception $exception) {
            return response()->file(public_path('/images/default-hd.png'));
        }
    });


    //Валюты
    Route::get('/obmen/{parent_url}', 'HomeController@exchange');

    Route::get('/lang/{language}', 'HomeController@language');
    Route::get('/', 'HomeController@index');
    Route::get('/{any?}', 'HomeController@index');
    Route::get('/{any?}/{slug?}', 'HomeController@index');
    Route::get('/account/{any?}/{slug?}', 'HomeController@index');
    Route::get('/login/{any?}/{slug?}', 'HomeController@index');
});
