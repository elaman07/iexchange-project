<?php
/**
 * Пользователькое API для FrontEnd
 */

// Получение курсов
Route::get('/getExchange', 'ExchangeController@getCompiler');
Route::get('/update_courses', 'ExchangeController@getUpdateCompiler');

// Тарифы
Route::get('/tariffs', 'TarifController@currencies');


// Первый запуск
Route::get('/initial', 'StartController@initial');

Route::group(['prefix' => 'sessions'], function() {
    Route::get('/current', 'SessionController@current');
});


// Информация о правилах
Route::get('/rules-categories', 'RulesPageController@categories');
Route::get('/rules-view/{id}', 'RulesPageController@view');
