<?php

namespace Modules\AMLServices;

use Illuminate\Support\Facades\Facade;


class AMLServiceFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'amlservices.factory';
    }
}

