@extends('admin.layouts.app')

@section('title', __('AML Сервисы'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('Добавить сервис') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('amlservices::index'))

@section('content')

    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название AML') }}</th>
            <th>{{ __('Тип') }}</th>
            <th>{{ __('Статус') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/modules/amlservices/'.$item->id.'/edit') }}">
                            {{ $item->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>

                    <td>
                        {{ $aml_services[$item->aml_type] ?? null }}
                    </td>

                    <td>
                        @if($item->status == 0)
                            <div class="label label-danger">{{ __('Отключен') }}</div>
                        @else
                            <div class="label label-success">{{ __('Включен') }}</div>
                        @endif
                    </td>

                    <td style="width: 100px;">
                        <md-button title="{{ __('Удалить') }}" href="amlservices/{{$item->id}}/delete" class="md-icon-button">
                            <i class="fas fa-trash text-danger"></i>
                        </md-button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>


    <!-- Модальное окно для создания хранилище -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/amlservices') }}">
                <input type="hidden" name="action" value="create">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('Добавить сервис') }}</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('Тип AML сервиса') }}</label>
                            <div class="col-md-6">
                                {!! Form::select('aml_type', $aml_services, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 8]) !!}
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('Создать') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('Назад') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
