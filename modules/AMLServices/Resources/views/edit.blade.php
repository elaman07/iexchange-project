@extends('admin.layouts.app')

@section('title', __('Изменить AML сервис :name', ['name' => $item->name]))

@section('breadcrumbs', Breadcrumbs::render('amlservices::edit', $item))

@section('content')

    <form action="{{ admin_base_path('/modules/amlservices/'.$item->id) }}" method="POST" class="form-horizontal">
        @csrf
        @method('PUT')

        <div class="default-panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Основное') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Название') }}</div>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $item->name }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Тип AML сервиса') }}</div>
                        {{ Form::select('aml_type', $aml_services, $item->aml_type, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '15', 'disabled']) }}
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="form-control selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Отключен') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Включен') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr />

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Настройки AML') }}</label>
                <div class="col-sm-10">
                    @foreach(\Config::get('modules-config.amlservices.forms.'.$item->aml_type) as $key => $value)
                        <div class="form-group col-md-6">
                            <div class="control-label-br">{{ $value }}</div>
                            <input type="text" name="{{ $key }}" class="form-control" id="{{ $key }}" value="{{ $data[$key] ?? null }}">
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="clearfix"></div>
        </div>


        <div class="clearfix"></div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/amlservices') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
