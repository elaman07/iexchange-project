<?php

namespace Modules\AMLServices\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AMLService extends Model
{
    use HasFactory;

    protected $table = 'aml_services';

    protected $fillable = [
        'name',
        'status',
        'id_manager',
        'aml_type',
        'aml_name'
    ];
}
