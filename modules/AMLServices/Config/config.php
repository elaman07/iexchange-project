<?php

return [
    'name' => 'AMLServices',

    'admin-menu' => [
        'rule' => 'admin_other_permissions',
        'url' =>  admin_base_path('/modules/amlservices'),
        'name' => 'AML Анализ'
    ],

    'aml_available' => [
        'amlbot' => 'AMLBot',
        'getblock' => 'GETBlock',
        'bitok' =>  'Bitok'
    ],

    'services' => [
        'getblock' => [
            'token' => '',
            'risk_level' => '',
            //'save_data' => '',
            'max_risk_dark_market' => '',
            'max_risk_dark_service' => '',
            'max_risk_exchange_fraudulent' => '',
            'max_risk_gambling' => '',
            'max_risk_mixer' => '',
            'max_risk_ransom' => '',
            'max_risk_sanctions' => '',
            'max_risk_scam' => '',
            'max_risk_stolen_coins' => '',
            'max_risk_terrorism_financing' => ''
        ],

        'bitok' => [
            'api_key' => '',
            'api_secret' => '',
            'risk_level' => ''
        ],

        'amlbot' => [
            'access_id' => '',
            'access_key' => '',
            'risk_level' => ''
        ]
    ],

    'forms' => [
        'getblock' => [
            'token' => 'Токен',
            'risk_level' => 'Критический уровень риска',
            'max_risk_dark_market' => 'Макс. риск для (dark market)',
            'max_risk_dark_service' => 'Макс. риск для (dark service)',
            'max_risk_exchange_fraudulent' => 'Макс. риск для (exchange fraudulent)',
            'max_risk_gambling' => 'Макс. риск для (gambling)',
            'max_risk_mixer' => 'Макс. риск для (mixer)',
            'max_risk_ransom' => 'Макс. риск для (ransom)',
            'max_risk_sanctions' => 'Макс. риск для (sanctions)',
            'max_risk_scam' => 'Макс. риск для (mixer)',
            'max_risk_stolen_coins' => 'Макс. риск для (stolen_coins)',
            'max_risk_terrorism_financing' => 'Макс. риск для (terrorism_financing)'
        ],

        'bitok' => [
            'api_key' => 'API Ключ',
            'api_secret' => 'API Secret',
            'risk_level' => 'Уровни риска'
        ],

        'amlbot' => [
            'access_id' => 'Access ID',
            'access_key' => 'Access key',
            'risk_level' => 'Критический уровень риска'
        ]
    ]
];
