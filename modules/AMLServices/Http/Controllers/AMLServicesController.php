<?php

namespace Modules\AMLServices\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Modules\AMLServices\Entities\AMLService;

class AMLServicesController extends Controller
{
    protected array $aml_services = [
        'amlbot' => 'AMLBot',
        'getblock' => 'GETBlock',
        'bitok' =>  'Bitok'
    ];


    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $items = AMLService::orderBy('id')->get();
        return view('amlservices::index', [
            'aml_services' => $this->aml_services,
            'items' => $items
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public function store(Request $request)
    {
        // Проверка входных параметров
        $validator = Validator::make($request->all(), [
            'aml_type' => ['required']
        ]);

        // Если при создании найдены ошибки, дальше не пускаем
        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            // Добавление нового фьд
            $item = AMLService::create([
                'aml_type'  => $request->get('aml_type')
            ]);

            $item->update([
                'aml_name' => $request->get('aml_type').'_'.$item->id
            ]);

            $dir = storage_path('/aml_services/');
            $filename = storage_path('/aml_services/'.$item->aml_name).'.php';

            if(!File::isDirectory($dir)) {
                File::makeDirectory($dir, 0775);
            }

            // Получение параметров, по умолчанию
            $defaultParameters = \Config::get('modules-config.amlservices.services.'.$item->aml_type);

            if(!File::exists($filename))
            {
                File::put($filename,  encrypt_protect_gateway_keys($defaultParameters));
            }

            flash("AML сервис {$item->name} успешно добавлен", ['alert alert-success']);
            return redirect()->to(
                admin_base_path('modules/amlservices/'.$item->id.'/edit')
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(int $id)
    {
        $item = AMLService::find($id);
        $data = protect_aml_keys($item->aml_name);
        return view('amlservices::edit', [
            'aml_services' => $this->aml_services,
            'item' => $item,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        $item = AMLService::find($id);

        $validator = Validator::make($request->all(), [
            'name'  =>  ['required']
        ]);

        if($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
        } else {

            $config = protect_aml_keys($item->aml_name);

            // Получение параметров, по умолчанию
            $defaultParameters = \Config::get('modules-config.amlservices.services.'.$item->aml_type);
            $only_keys = collect($defaultParameters)->keys()->toArray();


            $env_data = collect($request->all())->only($only_keys)->toArray();

            if(empty($defaultParameters)) {
                $mergeParams = $config;
            } else {
                $mergeParams = array_merge($defaultParameters, $config, $env_data);
            }

            $filename = storage_path('/aml_services/'.$item->aml_name).'.php';
            if(File::isFile($filename)) {
                File::put($filename,  encrypt_protect_gateway_keys($mergeParams));
            }

            $options = [
                'name'                          =>  $request->get('name'),
                'status'                        =>  ($request->has('status') ? 1 : 0)
            ];

            // Обновление настроек в базе
            $item->update($options);
            flash("AML Сервис {$item->name} успешно обновлен", ['alert alert-success']);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        if(config('admin.is_demo_mode')) {
            flash('В demo версии данная функция недоступна', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = AMLService::find($id);

        // Удаляем конфигурационный файл
        $filename = storage_path('/aml_services/'.$item->aml_name).'.php';
        iex_file_delete($filename);

        flash("AML Сервис {$item->name} успешно удален", ['alert alert-success']);
        $item->delete();

        return redirect()->to(
            admin_base_path('modules/amlservices')
        );
    }
}
