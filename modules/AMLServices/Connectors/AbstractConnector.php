<?php

namespace Modules\AMLServices\Connectors;

abstract class AbstractConnector
{
    protected array $config;

    protected string $driver;

    protected string $hash;

    public function __construct(array $config)
    {
        $this->config = $config;
    }


    /**
     * Создание уникального ID
     *
     * @param array $options
     * @param $type
     * @return string
     */
    public function generateHash(array $options, $type) : string
    {
        // Уникальный ID кэша
        $this->hash = hash('md5', sprintf('%s.%s.%s', implode(':', $options), $this->driver, $this->config['token']));
        return $this->hash;
    }
}
