<?php

namespace Modules\AMLServices\Connectors;

use App\Models\GetBlockRequest;
use Illuminate\Support\Facades\Http;
use Modules\AMLServices\AMLServiceException;

class GETBlockConnector extends AbstractConnector implements ConnectorInterface
{
    /**
     * Кэш префикса
     *
     * @var string
     */
    protected string $cache_prefix = 'getblock_cache__';

    /**
     * Название сервиса
     *
     * @var string
    */
    protected string $driver = 'getblock';


    /**
     * Проверка адреса
     *
     * @param string $name
     * @param array $options
     * @return array
     * @throws AMLServiceException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function query(string $name, array $options = []): array
    {
        return match ($name) {
            'address' => $this->address($options),
            'tx' => $this->tx($options)
        };
    }

    /**
     * Проверка адреса
     *
     * @throws \Exception
     */
    protected function address($options): array
    {
        // Уникальный ID кэша
        $cache_check_code = hash('md5',
            implode(':', [$options['address'], $options['asset'], $this->config['token'], 'checkup.checkaddr_v2'])
        );

        $getHash = [];
        if(\Cache::has($this->cache_prefix.$cache_check_code)) {
            $getHash = json_decode(\Cache::get($this->cache_prefix.$cache_check_code),true);
        }


        // Если кэш пустой
        if(empty($getHash))
        {
            // Получить hash
            $getHashRequest = $this->request();
            $paramsArray = [
                'jsonrpc' => '2.0',
                'id' => time(),
                'method' => 'checkup.checkaddr',
                'params' => [
                    'addr' => $options['address'],
                    'currency' => $options['asset']
                ]
            ];

            $getHash = $getHashRequest->post('/rpc/v1/request', $paramsArray)->json();

            // Записываем запросы в лог
            GetBlockRequest::create([
                'url' => 'https://api.getblock.net/rpc/v1/request',
                'headers' => isset($getHashRequest->getOptions()['headers']) ? json_encode($getHashRequest->getOptions()['headers']) : null,
                'options' => json_encode($paramsArray),
                'response' => json_encode($getHash)
            ]);

            // Задержка в секундах
            if(isset($options['is_sleep']) and $options['is_sleep'] > 0) {
                sleep($options['is_sleep']);
            }
        }


        if(isset($getHash['error'])) {
            throw new \Exception(json_encode($getHash));
        }

        // Получаем результаты анализа
        if(isset($getHash['result']) and isset($getHash['result']['check']) and !empty($getHash['result']['check']['hash']))
        {
            $txHash = $getHash['result']['check']['hash'];

            // Записываем данные в HASH
            if(!\Cache::has($this->hash)) {
                \Cache::set($this->hash, json_encode($getHash));
            }

            // Уникальный ID кэша
            $cache_code = hash('md5',
                implode(':', [$options['address'], $options['asset'], $this->config['token'], 'checkup.getresult'])
            );

            $paramsArray = [
                'jsonrpc' => '2.0',
                'id' => time(),
                'method' => 'checkup.getresult',
                'params' => [
                    'hash' => $txHash,
                ]
            ];
            $responseRequest = $this->request();
            $response = $responseRequest->post('/rpc/v1/request', $paramsArray)->json();

            // Создаем запрос
            GetBlockRequest::create([
                'url' => 'https://api.getblock.net/rpc/v1/request',
                'headers' => isset($responseRequest->getOptions()['headers']) ? json_encode($responseRequest->getOptions()['headers']) : null,
                'options' => json_encode($paramsArray),
                'response' => json_encode($response)
            ]);

            if(isset($response['result']) and isset($response['result']['check']))
            {
                $risk_score = (float)($response['result']['check']['status'] == 'SUCCESS') ? $response['result']['check']['report']['riskscore']*100 : 0;

                return [
                    'status' => $response['result']['check']['status'],
                    'risk_score' => $risk_score,
                    'is_high_risk' => $this->isLevelRisk($risk_score)
                ];
            }
        }

        throw new AMLServiceException('Error AML Analyses');
    }

    public function isLevelRisk($risk_score): bool
    {
        return $risk_score >= $this->config['risk_level'];
    }

    public function getValidateManyRisk(string $name): float
    {
        return (float)$this->config['max_risk_'.\Str::lower($name)];
    }

    /**
     * Проверка транзакции
     * @param $options
     * @return array
     * @throws AMLServiceException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function tx($options): array
    {
        // Уникальный ID кэша
        $cache_check_code = hash('md5',
            implode(':', [$options['address'], $options['asset'], $options['tx'], $this->config['token'], 'checkup.checktx'])
        );

        $getHash = [];
        if(\Cache::has($this->cache_prefix.$cache_check_code)) {
            $getHash = json_decode(\Cache::get($this->cache_prefix.$cache_check_code),true);
        }

        $responseRequest = $this->request();

        // Если кэш не найден
        if(empty($getHash))
        {
            $paramsArray = [
                'jsonrpc' => '2.0',
                'id' => time(),
                'method' => 'checkup.checktx',
                'params' => [
                    'tx' => $options['tx'],
                    'addr' => $options['address'],
                    'currency' => $options['asset']
                ]
            ];
            $getHash = $responseRequest->post('/rpc/v1/request', $paramsArray)->json();

            GetBlockRequest::create([
                'url' => 'https://api.getblock.net/rpc/v1/request',
                'headers' => isset($responseRequest->getOptions()['headers']) ? json_encode($responseRequest->getOptions()['headers']) : null,
                'options' => json_encode($paramsArray),
                'response' => json_encode($getHash)
            ]);

            // Задержка в секундах
            if(isset($options['is_sleep']) and $options['is_sleep'] > 0) {
                sleep($options['is_sleep']);
            }
        }

        // Если API не настроен
        if(isset($getHash['error'])) {
            throw new \Exception(json_encode($getHash['error']));
        }

        // Результаты анализа
        if(isset($getHash['result']) and isset($getHash['result']['check']) and !empty($getHash['result']['check']['hash']))
        {
            $txHash = $getHash['result']['check']['hash'];

            if(!\Cache::has($this->cache_prefix.$cache_check_code)) {
                \Cache::set($this->cache_prefix.$cache_check_code, json_encode($getHash));
            }

            $paramsArray = [
                'jsonrpc' => '2.0',
                'id' => time(),
                'method' => 'checkup.getresult',
                'params' => [
                    'hash' => $txHash,
                ]
            ];
            $response = $responseRequest->post('/rpc/v1/request', $paramsArray)->json();

            GetBlockRequest::create([
                'url' => 'https://api.getblock.net/rpc/v1/request',
                'headers' => isset($responseRequest->getOptions()['headers']) ? json_encode($responseRequest->getOptions()['headers']) : null,
                'options' => json_encode($paramsArray),
                'response' => json_encode($response)
            ]);
            return $response;
        }


        throw new AMLServiceException('Error AML Analyses');
    }

    /**
     * Запрос
     *
     * @return \Illuminate\Http\Client\PendingRequest
     */
    public function request(): \Illuminate\Http\Client\PendingRequest
    {
        return Http::baseUrl('https://api.getblock.net')->withHeaders([
            'Content-Type' => 'application/json'
        ])->withToken($this->config['token'])->asJson();
    }
}
