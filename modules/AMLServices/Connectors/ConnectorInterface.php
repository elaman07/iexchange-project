<?php

namespace Modules\AMLServices\Connectors;

interface ConnectorInterface
{
    /**
     * Отправка запроса
    */
    public function query(string $name, array $options);
}
