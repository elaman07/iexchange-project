<?php

namespace Modules\AMLServices\Connectors;

use Illuminate\Contracts\Container\Container;
use InvalidArgumentException;
use Modules\AMLServices\AMLServiceException;

class ConnectionFactory
{
    /**
     * The IoC container instance.
     *
     * @var \Illuminate\Contracts\Container\Container
     */
    protected Container $container;

    /**
     * Получаем название драйвера
     *
     * @var string
    */
    protected string $driver;

    /**
     * Create a new connection factory instance.
     *
     * @param  \Illuminate\Contracts\Container\Container  $container
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Establish a PDO connection based on the configuration.
     *
     * @param null $connector
     * @return BitokConnector|GETBlockConnector
     * @throws AMLServiceException
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    public function make($connector = null, $name = null)
    {
        $config = $this->parseConfig($connector);
        $this->driver = $name;

        return $this->createConnector($config);
    }

    /**
     * Создаем соединение
     *
     * @param $config
     * @return BitokConnector|GETBlockConnector
     */
    public function createConnector($config): BitokConnector|GETBlockConnector
    {
        return match ($this->driver) {
            'getblock' => new GETBlockConnector($config),
            'bitok' => new BitokConnector($config),
            default => throw new InvalidArgumentException("Unsupported driver [{$this->driver}]."),
        };
    }

    /**
     * Получаем конфигурацию по выбранному AML Сервису
     *
     * @param string $connector
     * @return array
     * @throws AMLServiceException
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     * @throws \Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException
     */
    protected function parseConfig(string $connector): array
    {
        $config = protect_aml_keys($connector);

        if(empty($config))
            throw new AMLServiceException('AML Config '.$connector.' not found');

        return $config;
    }
}
