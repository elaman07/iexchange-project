<?php

namespace Modules\AMLServices\Connectors;

use Illuminate\Support\Facades\Http;
use Modules\AMLServices\AMLServiceException;

class AMLBotConnector extends AbstractConnector implements ConnectorInterface
{
    /**
     * Кэш префикса
     *
     * @var string
     */
    protected string $cache_prefix = 'amlbot_cache__';

    /**
     * Название сервиса
     *
     * @var string
     */
    protected string $driver = 'amlbot';

    /**
     * Отправка запроса
     *
     * @throws \Exception
     */
    public function query(string $name, array $options): array
    {
        return match ($name) {
            'address' => $this->address($options),
            'tx' => $this->tx($options)
        };
    }

    /**
     * Проверка адреса
     *
     * @throws \Exception
     */
    protected function address($options): array
    {
        // Уникальный ID кэша
        $cache_code = hash('md5', implode(':', [
            $options['address'], $options['asset'], $this->config['access_key'], $this->config['access_id']
        ]));

        $token = hash('md5', implode(':', [
            $options['address'],
            $this->config['access_key'],
            $this->config['access_id']
        ]));

        $paramsArray = [
            'accessId' => $this->config['access_id'],
            'locale' => 'en_US',
            'hash' => $options['address'],
            'asset' => $options['asset'],
            'token' => $token
        ];

        // Если данные записаны, то повторно берем из кэша
        if(\Cache::has($this->cache_prefix.$cache_code)) {
            return json_decode(
                \Cache::get($this->cache_prefix.$cache_code)
                ,true);
        }

        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded'
        ])->asForm()->post('https://extrnlapiendpoint.silencatech.com/', $paramsArray)->json();

        // Записываем данные в HASH
        if(!\Cache::has($this->hash)) {
            \Cache::set($this->cache_prefix.$cache_code, json_encode($response));
        }

        return $response;
    }

    public function isLevelRisk($risk_score): bool
    {
        return $risk_score >= $this->config['risk_level'];
    }

    /**
     * Проверка транзакции
     * @param $options
     * @return array
     * @throws AMLServiceException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function tx($options): array
    {
        // Уникальный ID кэша
        $cache_code = hash('md5', implode(':', [
            $options['address'], $options['asset'], $options['tx'], 'deposit', $this->config['access_key'], $this->config['access_id']
        ]));

        $token = hash('md5', implode(':', [
            $options['address'],
            $this->config['access_key'],
            $this->config['access_id']
        ]));

        $paramsArray = [
            'accessId' => $this->config['access_id'],
            'locale' => 'en_US',
            'hash' => $options['tx'],
            'address' => $options['address'],
            'direction' => $options['type'],
            'asset' => $options['asset'],
            'token' => $token
        ];

        // Если данные записаны, то повторно берем из кэша
        if(\Cache::has($this->cache_prefix.$cache_code)) {
            return json_decode(
                \Cache::get($this->cache_prefix.$cache_code)
                ,true
            );
        }

        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded'
        ])->asForm()->post('https://extrnlapiendpoint.silencatech.com/', $paramsArray)->json();

        if($response['result'] == 1) {
            \Cache::set($this->cache_prefix . $cache_code, json_encode($response));
        }

        return $response;
    }
}
