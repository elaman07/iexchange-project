<?php

namespace Modules\AMLServices\Connectors;

use Illuminate\Support\Facades\Http;

class BitokConnector extends AbstractConnector implements ConnectorInterface
{
    /**
     * Название сервиса
     *
     * @var string
     */
    protected string $driver = 'bitok';

    /**
     * Отправка запроса
     */
    public function query(string $name, array $options)
    {
        return match ($name) {
            'address' => $this->address($options),
            'tx' => $this->tx($options)
        };
    }

    /**
     * Проверка по адресу
     *
     * @param array $options
     * @return array
     */
    protected function address(array $options)
    {
        $paramsArray = [
            'client_id' => time(),
            'attempt_id' => time(),
            'direction' => $options['direction'],
            'network' => $options['asset'],
            'token_id' => null,
            'amount' => (float)$options['amount']
        ];

        if($options['direction'] == 'incoming') {
            $paramsArray['input_address'] = $options['address'];
        }

        if($options['direction'] == 'outgoing') {
            $paramsArray['output_address'] = $options['address'];
        }

        $response = $this->request('/v1/transfers/register-attempt/', 'post', $paramsArray);

        return [
            'status' => 1,
            'risk_level' => $response['risk_level']
        ];
    }


    protected function tx(array $options)
    {
        $paramsArray = [
            'client_id' => $options['client_id'] ?? time(),
            'direction' => $options['direction'],
            'network' => $options['asset'],
            'tx_hash' => $options['tx'],
            'token_id' => null
        ];

        if($options['direction'] == 'incoming') {
            $paramsArray['output_address'] = $options['address'];
        }

        $response = $this->request('/v1/transfers/register/', 'post', $paramsArray);


        if($response['tx_status'] == 'bound') {
            $response['is_high_level'] = $this->isLevelRisk($response['risk_level']);
        }

        return $response;
    }

    public function isLevelRisk($risk_score): bool
    {
        return in_array($risk_score, explode(',', $this->config['risk_level']));
    }

    /**
     * Соединение с сервисом AML
     */
    public function request(string $path, string $method = 'get', $json_payload = [])
    {
        $kyt = 'https://kyt-api.bitok.org';
        $timestamp = time() * 1000;
        $http_method = \Str::upper($method);
        $endpoint_with_query_params = $path;

        $str_to_sign =
            $http_method . "\n" .
            $endpoint_with_query_params . "\n" .
            $timestamp;

        if(!empty($json_payload)) {
            $str_to_sign .= "\n" . json_encode($json_payload, JSON_UNESCAPED_SLASHES);
        }

        $built_signature = hash_hmac('sha256', $str_to_sign, $this->config['api_secret'], true);
        $signature = base64_encode($built_signature);
        $header = [
            'API-KEY-ID' => $this->config['api_key'],
            'API-TIMESTAMP' => $timestamp,
            'API-SIGNATURE' => $signature
        ];

        return Http::baseUrl($kyt)->withHeaders($header)->{$method}($endpoint_with_query_params, $json_payload)->json();
    }
}
