<?php
namespace Modules\ExportData\Entities;

use Illuminate\Database\Eloquent\Model;


class ExportData extends Model
{
    protected $table = 'export_data';

    protected $fillable = [
        'name', 'format_export', 'is_cron', 'count', 'export_value', 'is_filter', 'is_allow_filter'
    ];
}
