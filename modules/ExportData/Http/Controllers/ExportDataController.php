<?php

namespace Modules\ExportData\Http\Controllers;


use Brotzka\DotenvEditor\DotenvEditor;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Modules\ExportData\Entities\ExportData;

class ExportDataController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'exportdata_driver',
        'exportdata_is_cron'
    ];

    /**
     * Модуль экпорта данных
     *
     * @return Renderable
     */
    public function index()
    {
        $exports = ExportData::all();

        return view('exportdata::index', compact('exports'));
    }


    /**
     * Обработка данных
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if(config('admin.is_demo_mode') == true or config('admin.is_rent') == true)
            return redirect()->back();

        // Настройки

        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        // Сохранение данные
        foreach ($request->export as $key => $value)
        {
            // Если нет значений, пропускаем
            if(empty($value)) continue;

            $item = ExportData::find($key);

            $class = '\\App\\Exports\\'.$item->export_value;
            $class_collection = $class.'Collection';
            $first = Carbon::now()->format('Y_m_d').'_'. random_int(0, 999999);
            $ext = $first.'_'.Str::studly($item->export_value).'.'.mb_strtolower($value);

            // Если существует класс экспорта
            if(class_exists($class) and class_exists($class_collection))
            {
                if($request->get('actions') == 'save') {
                    if($item->is_filter == 1)
                        Excel::store(new $class(true), $ext, iEXSetting('exportdata_driver', 'local'), excel_type($value));
                    else
                        Excel::store(new $class_collection(), $ext, iEXSetting('exportdata_driver', 'local'), excel_type($value));
                } else {
                    if($item->is_filter == 1)
                        export_filter_data($class, $ext, $value);
                    else
                        export_data($class_collection, $ext, $value);
                }

                // Обновляем счетчик
                $item->update(['count' => $item->count + 1]);
            }
        }

        return redirect()->back();
    }


    /**
     * Редактирование данных
     *
     * @param int $id
     * @return Renderable
     */
    public function edit(int $id): Renderable
    {
        $find = ExportData::find($id);

        return view('exportdata::edit', compact('find'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $find = ExportData::find($id);
        $find->update([
            'is_filter' => $request->is_filter,
            'is_cron'      =>    $request->is_cron,
            'format_export' => $request->format_export
        ]);

        flash('Настройки успешно сохранены', ['alert alert-success']);

        return redirect()->back();
    }
}
