@extends('admin.layouts.app')

@section('title', __('exportdata::messages.title'))

@section('breadcrumbs', Breadcrumbs::render('exportdata::index'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройки</span>
        </md-button>
    @endcan
@endsection

@section('content')
    <form style="width:100%;margin-right: 10px;" method="post" action="?">
        @csrf

        <table class="table table-border-2">
            <thead>
            <tr>
                <th>{{ __('exportdata::messages.id') }}</th>
                <th>{{ __('exportdata::messages.name') }}</th>
                <th>{{ __('exportdata::messages.count_download') }}</th>
                <th>{{ __('exportdata::messages.filter') }}</th>
                <th>{{ __('exportdata::messages.action') }}</th>
                <th></th>
            </tr>

            </thead>
            <tbody>

            @foreach($exports as $export)
                <tr>
                    <th>{{$export->id}}</th>
                    <td>{{$export->name}}</td>
                    <td>{{$export->count}}</td>
                    <td style="width: 200px">
                        @if($export->is_filter)
                            <span class="text-success">{{ __('exportdata::messages.enable') }}</span>
                        @else
                            <span class="text-danger">{{ __('exportdata::messages.disable') }}</span>
                        @endif
                    </td>

                    <td style="width: 300px;">
                        <select class="form-control selectpicker" name="export[{{$export->id}}]" data-width="200">
                            <option value="">-- {{ __('exportdata::messages.no_selected') }} --</option>
                            @foreach(config('modules-config.exportdata.options.lists') as $item)
                                <option value="{{$item}}">{{ __('exportdata::messages.export_to', ['name' => $item]) }}</option>
                            @endforeach
                        </select>

                        @if(config('admin.is_demo_mode') == true)
                            <small style="display: block;width: 200px" class="text-danger">{{ __('exportdata::messages.export_is_demo') }}</small>
                        @endif
                    </td>

                    <td style="width: 100px;" ng-controller="CommonController  as ctrl">

                        <md-menu md-position-mode="target-right target">
                            <md-button aria-label="Open demo menu" class="md-icon-button" ng-click="ctrl.openMenu($mdMenu, $event)">
                                <md-icon aria-hidden="true" md-menu-origin md-font-set="material-icons">more_vert</md-icon>
                            </md-button>

                            <md-menu-content width="3">
                                <md-menu-item class="menu-items-list">
                                    <md-button ng-href="{{ admin_base_path('/modules/exportdata/'.$export->id.'/edit') }}">
                                        <span md-menu-align-target> {{ __('exportdata::messages.edit') }}</span>
                                    </md-button>
                                </md-menu-item>
                            </md-menu-content>
                        </md-menu>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="download">{{ __('exportdata::messages.download') }}</option>
                        <option data-divider="true"></option>
                        <option value="save">{{ __('exportdata::messages.save') }}</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">{{ __('exportdata::messages.run') }}</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>



    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройка экспорта данных</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('exportdata::messages.driver_export') }}</label>
                            <div class="col-md-6">
                                <select name="exportdata_driver" class="form-control selectpicker">
                                    @foreach(config('filesystems.disks') as $key => $value)
                                        @if($key != 'archive')
                                            <option value="{{$key}}" @if(iEXSetting('exportdata_driver') == $key) selected @endif>{{$key}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('exportdata::messages.enable_cron') }}</label>
                            <div class="col-md-6">
                                <div class="material-switch">
                                    <input id="exportdata_is_cron" name="exportdata_is_cron" type="checkbox" value="1" @if(iEXSetting('exportdata_is_cron') == 1) checked @endif />
                                    <label for="exportdata_is_cron" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
