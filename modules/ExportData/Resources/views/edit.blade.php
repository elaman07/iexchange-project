@extends('admin.layouts.app')

@section('title', __('exportdata::messages.edit_title', ['name' => $find->name]))

@section('breadcrumbs', Breadcrumbs::render('exportdata::edit', $find))

@section('content')
    <form style="margin-top: -6px;" action="{{ admin_base_path('/modules/exportdata/'.$find->id) }}" method="POST" class="form-horizontal">
        @csrf
        @method('PUT')

        <table class="table table-2 table-striped">

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('exportdata::messages.enable_filter') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('exportdata::messages.enable_filter_hint') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">

                    @if($find->is_allow_filter == 0)
                        <div class="material-switch">
                            <input id="SwitchOptionPrimary1-1" name="is_filter" type="checkbox" value="1" @if($find->is_filter == 1) checked @endif />
                            <label for="SwitchOptionPrimary1-1" class="label-primary"></label>
                        </div>
                    @else
                        <div class="text-danger">{{ __('exportdata::messages.not_filters') }}</div>
                    @endif
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('exportdata::messages.enable_cron') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('exportdata::messages.enable_cron_hint') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <div class="material-switch">
                        <input id="SwitchOptionPrimary1-2" name="is_cron" type="checkbox" value="1" @if($find->is_cron == 1) checked @endif />
                        <label for="SwitchOptionPrimary1-2" class="label-primary"></label>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="col-xs-6 col-sm-6 col-md-7">
                    <h6 class="media-heading text-semibold">{{ __('exportdata::messages.format_export') }}</h6>
                    <span class="text-muted text-size-small hidden-xs">{{ __('exportdata::messages.format_export_hint') }}</span>
                </td>
                <td class="col-xs-6 col-sm-6 col-md-5">
                    <select class="form-control selectpicker" name="format_export" data-width="200">
                        <option value="">-- {{ __('exportdata::messages.no_selected') }} --</option>
                        @foreach(config('modules-config.exportdata.options.lists') as $item)
                            <option value="{{$item}}" @if($find->format_export == $item) selected @endif>{{ __('exportdata::messages.export_to', ['name' => $item]) }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
        </table>

        <div class="modal-footer">
            <md-button type="submit" class="md-raised md-success">{{ __('exportdata::messages.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/exportdata') }}">{{ __('exportdata::messages.back') }}</md-button>
        </div>
    </form>
@endsection
