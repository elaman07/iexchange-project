<?php

return [
    'name' => 'ExportData',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/exportdata'),
        'name' => 'Экспорт данных'
    ],


    'options' => [
        'lists'  => [
            'XLSX',
            'CSV',
            'TSV',
            'ODS',
            'XLS',
            'HTML',
            'MPDF',
            'DOMPDF',
            'TCPDF'
        ]
    ]
];
