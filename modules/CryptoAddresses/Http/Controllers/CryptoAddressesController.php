<?php

namespace Modules\CryptoAddresses\Http\Controllers;

use App\Models\WalletsAddresses;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;

class CryptoAddressesController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'ethereum_gas_limit',
        'ethereum_gas_price',
        'ethereum_token_fee',
        'ethereum_token_gas_limit',
        'ethereum_token_gas_price',
        'bnb_fee',
        'enable_my_fee_ethereum',
        'enable_my_fee_ethereum_token',
        'only_eth_token_gas_price',
        'is_fee_eth_manually',
        'only_eth_gas_price',
        'ethereum_gas_limit_refund',
        'ethereum_gas_price_refund'
    ];

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request  $request)
    {
        // Удаляем ненужные ключи из массива
        if($request->has('send_action'))
        {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('cryptoaddresses.index', $conditions);
        }

        $wallets = WalletsAddresses::filter($request->all())->whereNotNull('private_key')
            ->whereNotNull('service_name')->orderByDesc('id')->paginate(20);

        $addresses = WalletsAddresses::groupBy('service_name')->get()->pluck('service_name', 'service_name');

        return view('cryptoaddresses::address', [
            'wallets' => $wallets,
            'addresses' => $addresses,
            'filter' => $request->all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }
    }

}
