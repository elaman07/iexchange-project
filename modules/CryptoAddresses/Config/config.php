<?php

return [
    'name' => 'CryptoAddresses',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/cryptoaddresses'),
        'name' => 'Crypto адреса'
    ]
];
