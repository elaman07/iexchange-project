@extends('admin.layouts.app')

@section('title', __('cryptoaddresses::messages.list_address'))

@section('breadcrumbs', Breadcrumbs::render('cryptoaddresses::address'))

@section('top-block')
    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройки</span>
        </md-button>
    @endcan
@endsection

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2><i class="fa fa-fw fa-filter"></i>  {{ __('admin-basic.filters') }}</h2>
            <div class="heading-right-button">
                <md-button class="collapsed" data-toggle="collapse" href="#showFilters">
                    <span class="if-collapsed">{{ __('admin-basic.show') }}</span>
                    <span class="if-not-collapsed">{{ __('admin-basic.hide') }}</span>
                </md-button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('cryptoaddresses::messages.provider') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::select('provider[]', $addresses, is_filter_search($filter, 'provider'),
                                               ['class' => 'selectpicker', 'data-width' => '100%', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-3">{{ __('cryptoaddresses::messages.address') }}</label>
                                    <div class="col-md-8">
                                        {!! Form::input('text', 'address', is_filter_search($filter, 'address'),
                                            ['class' => 'form-control selectpicker', 'data-width' => '300px', 'multiple']) !!}
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('admin-basic.save_filter') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('admin-basic.clear_filter') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('admin-crypto.list_address') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            @if(config('admin.is_demo_mode') == true)
                <div class="alert alert-danger">{{ __('admin-basic.disable_demo') }}</div>
            @else
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-basic.created_at') }}</th>
                        <th>{{ __('cryptoaddresses::messages.order_id') }}</th>
                        <th>{{ __('cryptoaddresses::messages.provider') }}</th>
                        <th>{{ __('cryptoaddresses::messages.address') }}</th>
                        <th>{{ __('cryptoaddresses::messages.key') }}</th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(count($wallets) > 0)
                        @foreach($wallets as $wallet)
                            <tr>
                                <td>{{ $wallet->created_at }}</td>
                                <td>
                                    @if(isset($wallet->tasks))
                                        <a href="{{ admin_base_path('/applications?id='.$wallet->tasks->id) }}">№{{ $wallet->tasks->id }}</a>
                                    @else
                                        <small class="text-danger">{{ __('cryptoaddresses::messages.not_data') }}</small>
                                    @endif
                                </td>
                                <td>{{ $wallet->service_name }}</td>
                                <td>{{ $wallet->address }}</td>
                                <td>{{ $wallet->private_key }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            @endif
        </div>
    </div>




    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройка шлюзов</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-6">Расчитывать вручную (Только отправки на Ethereum адрес)</label>
                            <div class="col-md-6">
                                <select class="form-control selectpicker" name="is_fee_eth_manually">
                                    <option value="0" @if(iEXSetting('is_fee_eth_manually') == 0)selected @endif>Нет</option>
                                    <option value="1" @if(iEXSetting('is_fee_eth_manually') == 1)selected @endif>Да</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">GAS Limit (Ethereum)</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_gas_limit" value="{{ iEXSetting('ethereum_gas_limit', 42000) }}">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-6">GAS Price (Ethereum)</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_gas_price" value="{{ iEXSetting('ethereum_gas_price', 10) }}">
                                <div class="checkbox">
                                    <input class="checkbox-input" id="only_eth_gas_price" value="1" type="checkbox" name="only_eth_gas_price" @if(iEXSetting('only_eth_gas_price') == 1) checked @endif>
                                    <label for="only_eth_gas_price">Отправить только это значение</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Выставить свою комиссию (Ethereum)</label>
                            <div class="col-md-6">
                                <select class="form-control selectpicker" name="enable_my_fee_ethereum">
                                    <option value="0" @if(iEXSetting('enable_my_fee_ethereum') == 0)selected @endif>Нет</option>
                                    <option value="1" @if(iEXSetting('enable_my_fee_ethereum') == 1)selected @endif>Да</option>
                                </select>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group">
                            <label class="control-label col-md-6">Использовать свои комиссии (Ethereum Token)</label>
                            <div class="col-md-6">
                                <select class="form-control selectpicker" name="enable_my_fee_ethereum_token">
                                    <option value="0" @if(iEXSetting('enable_my_fee_ethereum_token') == 0)selected @endif>Нет</option>
                                    <option value="1" @if(iEXSetting('enable_my_fee_ethereum_token') == 1)selected @endif>Да</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Комиссия для токенов (Ethereum)</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_token_fee" value="{{ iEXSetting('ethereum_token_fee', 0.0005) }}">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-6">GAS Limit (Ethereum Token)</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_token_gas_limit" value="{{ iEXSetting('ethereum_token_gas_limit', 70000) }}">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-6">GAS Price (Ethereum Token)</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_token_gas_price" value="{{ iEXSetting('ethereum_token_gas_price', 10) }}">
                                <div class="checkbox">
                                    <input class="checkbox-input" id="only_eth_token_gas_price" value="1" type="checkbox" name="only_eth_token_gas_price" @if(iEXSetting('only_eth_token_gas_price') == 1) checked @endif>
                                    <label for="only_eth_token_gas_price">Отправить только это значение</label>
                                </div>
                            </div>
                        </div>


                        <hr />
                        <h5>Для возврата комиссии</h5>

                        <div class="form-group">
                            <label class="control-label col-md-6">GAS Limit (EthereumToken) возврат комиссии</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_gas_limit_refund" value="{{ iEXSetting('ethereum_gas_limit_refund', 21000) }}">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-6">GAS Price (EthereumToken) возврат комиссии</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="ethereum_gas_price_refund" value="{{ iEXSetting('ethereum_gas_price_refund', 15) }}">
                            </div>
                        </div>

                        <hr />


                        <div class="form-group">
                            <label class="control-label col-md-6">Fee (Binance Chain)</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="bnb_fee" value="{{ iEXSetting('bnb_fee', 0.000375) }}">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('pagination')
    {!! $wallets->appends($filter)->links() !!}
@endsection
