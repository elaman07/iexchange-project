@extends('admin.layouts.app')

@section('title', 'Настройка архивирования заявок')

@section('breadcrumbs', Breadcrumbs::render('orderarchive::index'))

@section('content')

    <div class="iex-custom-data-fields">
        <form class="form-horizontal" method="post" action="?">
            @csrf
            <div class="default-panel-body">

                <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Разрешить клиентам выгрузить историю обменов</label>
                    <div class="col-sm-3">
                        <div class="material-switch switch-input-1">
                            <input id="orderarchive_enable_upload_history" name="orderarchive_enable_upload_history" type="checkbox" @if(iEXSetting('orderarchive_enable_upload_history') == 1) checked @endif value="1" />
                            <label for="orderarchive_enable_upload_history" class="label-primary"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="orderarchive_count_order" class="col-sm-3 control-label">Количество заявок для архивации</label>
                    <div class="col-sm-3">
                        <input type="text" name="orderarchive_count_order" class="form-control" value="{{ iEXSetting('orderarchive_count_order') }}">
                    </div>
                </div>
            </div>

            <div class="default-panel-footer default-panel-footer-right">
                <md-button class="md-raised md-primary" type="submit">
                    <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            </div>

        </form>
    </div>

@endsection