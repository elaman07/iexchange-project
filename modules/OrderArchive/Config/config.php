<?php

return [
    'name' => 'OrderArchive',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/order-archive'),
        'name' => 'Архивирование заявок'
    ],
];
