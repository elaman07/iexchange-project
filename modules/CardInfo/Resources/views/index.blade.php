@extends('admin.layouts.app')

@section('title', 'Определение данных по банк. карте')

@section('breadcrumbs', Breadcrumbs::render('cardinfo::index'))

@section('content')

    <div class="iex-custom-data-fields">
        <form class="form-horizontal" method="post" action="?">
            @csrf
            <div class="default-panel-body">

                <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Запоминать проверенные карты</label>
                    <div class="col-sm-3">
                        <div class="material-switch switch-input-1">
                            <input id="cardinfo_save_data" name="cardinfo_save_data" type="checkbox" @if(iEXSetting('cardinfo_save_data') == 1) checked @endif value="1" />
                            <label for="cardinfo_save_data" class="label-primary"></label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">Источник определения данных</label>
                    <div class="col-sm-3">
                        <select class="form-control selectpicker" name="cardinfo_driver">
                            <option value="binlist" @if(iEXSetting('cardinfo_driver') == 'binlist') selected @endif>binlist.net (без api)</option>
                            <option value="bincodes" @if(iEXSetting('cardinfo_driver') == 'bincodes') selected @endif>bincodes.com (c api)</option>
                            <option value="mrbin" @if(iEXSetting('cardinfo_driver') == 'mrbin') selected @endif>mrbin (с api)</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">API Ключ</label>
                    <div class="col-sm-3">
                        <input type="text" name="cardinfo_api_key" class="form-control" value="{{ iEXSetting('cardinfo_api_key') }}">
                    </div>
                </div>

                <hr />
                <div class="form-group">
                    <label for="code" class="col-sm-3 control-label">{{ __('admin-basic.currency.select_payment_name') }}</label>
                    <div class="col-sm-3">
                        {{ Form::select('ids_currencies[]', $currencies, $selected_card,
                     ['class' => 'form-control selectpicker', 'multiple', 'data-live-search' => 'true', 'data-size' => 8]) }}
                    </div>
                </div>

            </div>

            <div class="default-panel-footer default-panel-footer-right">
                <md-button class="md-raised md-primary" type="submit">
                    <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            </div>

        </form>
    </div>

@endsection
