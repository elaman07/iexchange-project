<?php

return [
    'name' => 'CardInfo',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/cardinfo'),
        'name' => 'Информация по картам'
    ]
];
