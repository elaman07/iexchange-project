<?php
namespace Modules\CardInfo;

use Illuminate\Support\Facades\Facade;
use Modules\CardInfo\Adapters\AdapterInterface;

/**
 * @method static AdapterInterface driver(string $name = 'binlist')
 */
class CardInfoFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'cardinfo';
    }
}
