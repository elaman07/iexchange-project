<?php
namespace Modules\CardInfo;

use Illuminate\Support\Manager;
use Modules\CardInfo\Adapters\BinCodesAdapter;
use Modules\CardInfo\Adapters\BinlistAdapter;
use Modules\CardInfo\Adapters\MrBinAdapter;

class CardInfoManager extends Manager
{
    /**
     * Драйвер Binlist
     */
    public function createBinlistDriver()
    {
        return new BinlistAdapter();
    }

    /**
     * Драйвер BinCodes
     */
    public function createBinCodesDriver()
    {
        return new BinCodesAdapter();
    }

    public function createMrBinDriver()
    {
        return new MrBinAdapter();
    }

    /**
     * Драйвер по умолчанию
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return 'binlist';
    }
}
