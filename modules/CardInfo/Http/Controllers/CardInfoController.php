<?php

namespace Modules\CardInfo\Http\Controllers;

use App\Models\Currency;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CardInfoController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'cardinfo_driver',
        'cardinfo_save_data',
        'cardinfo_api_key'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        $currencies = Currency::active()->get()->map(function($item) {
            return [
                'id' => $item->id,
                'name' => $item->payment->name.' '.$item->code_currency->name
            ];
        })->pluck('name', 'id');

        $selected_card = Currency::active()->where('is_card_detail', '=', 1)->get()->map(function($item) {
            return $item->id;
        });


        return view('cardinfo::index', compact('currencies', 'selected_card'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // Обновление конфига
        $array = [];
        foreach ($this->allowFiltered as $item) {
            if($request->has($item) and is_array($request->get($item))) {
                $array[$item] = implode(',', $request->get($item));
            } else {
                $array[$item] = ($request->has($item) ? $request->get($item): null);
            }
        }

        iEXSetting($array);

        if($request->has('ids_currencies')) 
        {
            Currency::query()->update(['is_card_detail' => 0]);
            foreach ($request->get('ids_currencies') as $key => $value) {
                Currency::find($value)->update(['is_card_detail' => 1]);
            }
        }

        flash('Настройки успешно сохранены', ['alert alert-success']);
        return redirect()->back();
    }
}
