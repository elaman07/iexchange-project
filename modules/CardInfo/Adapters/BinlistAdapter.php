<?php
namespace Modules\CardInfo\Adapters;

class BinlistAdapter extends AbstractAdapter implements AdapterInterface
{
    /**
     * Кэш префикса
     *
     * @var string
     */
    protected $cache_prefix = 'cardinfo_binlist_';

    /**
     * Ссылка на адаптер
     *
     * @var string
    */
    protected $url = 'https://lookup.binlist.net';

    /**
     * Устанавливаем номер карты
     *
     * @param string $card_number
     * @return BinlistAdapter
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function setCardNumber(string $card_number)
    {
        $card_number = preg_replace('/\D/', '', $card_number);
        $this->prefix = str_pad(substr((string) $card_number, 0, static::PREFIX_LENGTH), static::PREFIX_LENGTH, '0');

        $this->response = collect($this->call('/'.$this->prefix));
        return $this;
    }

    /**
     * Платежная система
     *
     * @return string
     */
    public function getPaymentSystem()
    {
        if($this->response->has('scheme')) {
            return $this->response->get('scheme');
        }
        return null;
    }

    /**
     * Тип карты
     *
     * @return string
     */
    public function getType()
    {
        if($this->response->has('type')) {
            return $this->response->get('type');
        }
        return null;
    }

    /**
     * Валидация карт
     */
    public function getValidate()
    {
        if($this->response->has('number')) {
            return $this->response->get('number');
        }
        return null;
    }

    /**
     * Брэнд
     *
     * @return string
     */
    public function getBrand()
    {
        if($this->response->has('brand')) {
            return $this->response->get('brand');
        }
        return null;
    }

    /**
     * Страна
     *
     * @return string
     */
    public function getCountryName()
    {
        if(isset($this->response['country'])) {
            return (isset($this->response['country']['name']) ? $this->response['country']['name'] : null);
        }
        return null;
    }

    /**
     * Валюта страны
     *
     * @return string
     */
    public function geCurrency()
    {
        if(isset($this->response['country'])) {
            return (isset($this->response['country']['currency']) ? $this->response['country']['currency'] : null);
        }
        return null;
    }

    /**
     * Название банка
     *
     * @return string
     */
    public function getBankName()
    {
        if(isset($this->response['bank'])) {
            return (isset($this->response['bank']['name']) ? $this->response['bank']['name'] : null);
        }
        return null;
    }

    /**
     * Адрес сайта банка
     *
     * @return string
     */
    public function getBankUrl()
    {
        if(isset($this->response['bank'])) {
            return (isset($this->response['bank']['url']) ? $this->response['bank']['url'] : null);
        }
        return null;
    }

    /**
     * Контактный номер поддержки
     *
     * @return string
     */
    public function getBankSupportNumber()
    {
        if(isset($this->response['bank'])) {
            return (isset($this->response['bank']['phone']) ? $this->response['bank']['phone'] : null);
        }
        return null;
    }

    /**
     * Получаем результат в массиве
     *
     * @return array
     */
    public function getRawData()
    {
        return $this->response->all();
    }
}