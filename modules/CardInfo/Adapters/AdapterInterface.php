<?php
namespace Modules\CardInfo\Adapters;

interface AdapterInterface
{
    /**
     * @param string $card_number
     * @return $this
     */
    public function setCardNumber(string $card_number);

    public function getPaymentSystem();

    public function getType();

    public function getValidate();

    public function getBrand();

    public function getCountryName();

    public function geCurrency();

    public function getBankName();

    public function getBankUrl();

    public function getBankSupportNumber();

    public function getRawData();
}