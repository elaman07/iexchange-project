<?php
namespace Modules\CardInfo\Adapters;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;


abstract class AbstractAdapter implements AdapterInterface
{
    /**
     * Кэш префикса
     *
     * @var string
     */
    protected $cache_prefix = 'default_cardinfo';

    /**
     * Сколько цифр используется для поиска
     *
     * @var integer
     */
    protected const PREFIX_LENGTH = 6;

    /**
     * Инициализация GuzzleHttp
     *
     * @var Client
     */
    protected $httpClient;

    /**
     * Собираем информацию о результатах
     *
     * @var Collection
     */
    protected $response;

    /**
     * Ссылка на адаптер
     *
     * @var string
    */
    protected $url = null;

    /**
     * API Ключ
     *
     * @var string
    */
    protected $api_keys = null;

    /**
     * Получаем префикс карты
     *
     * @var string
    */
    protected $prefix = null;

    /**
     * Конструктор
     *
     * @return void
    */
    public function __construct()
    {
        $this->api_keys = (string)iEXSetting('cardinfo_api_key');
    }

    /**
     * Запрос на получения данных
     * @param string $path
     * @param int $prefix
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function call($path = '/', $options = [])
    {
        $this->httpClient = new Client([
            'base_uri' => $this->url,
            'defaults' => [
                'headers' => ['content-type' => 'application/json']
            ]
        ]);

        // Если данные записаны, то повторно берем из кэша
        if((int)iEXSetting('cardinfo_save_data') == 1 and \Cache::has($this->cache_prefix.$this->prefix))
            return \Cache::get($this->cache_prefix.$this->prefix);

        if(is_array($options) and !empty($options)) {
            $response = $this->httpClient->post($path, $options);
        }else {
            $response = $this->httpClient->get($path);
        }
        $data = json_decode($response->getBody()->getContents(), true);

        if(iEXSetting('cardinfo_save_data') == 1) {
            \Cache::set($this->cache_prefix.$this->prefix, $data);
        }

        return $data;
    }
}
