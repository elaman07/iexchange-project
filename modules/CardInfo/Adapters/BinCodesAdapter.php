<?php
namespace Modules\CardInfo\Adapters;

class BinCodesAdapter extends AbstractAdapter implements AdapterInterface
{
    /**
     * Кэш префикса
     *
     * @var string
     */
    protected $cache_prefix = 'cardinfo_bincodes_';

    /**
     * Ссылка на адаптер
     *
     * @var string
    */
    protected $url = 'https://api.bincodes.com';

    /**
     * Устанавливаем номер карты
     *
     * @param string $card_number
     * @return BinCodesAdapter
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function setCardNumber(string $card_number)
    {
        $card_number = preg_replace('/\D/', '', $card_number);
        $this->prefix = str_pad(substr((string) $card_number, 0, static::PREFIX_LENGTH), static::PREFIX_LENGTH, '0');

        $this->response = collect($this->call('/bin/?format=json&api_key='.$this->api_keys.'&bin='.$this->prefix));
        return $this;
    }

    /**
     * Платежная система
     *
     * @return string
     */
    public function getPaymentSystem()
    {
        if($this->response->has('card')) {
            return $this->response->get('card');
        }
        return null;
    }

    /**
     * Тип карты
     *
     * @return string
     */
    public function getType()
    {
        if($this->response->has('type')) {
            return $this->response->get('type');
        }
        return null;
    }

    /**
     * Валидация карт
     */
    public function getValidate()
    {
        if($this->response->has('valid')) {
            return $this->response->get('valid');
        }
        return null;
    }

    /**
     * Брэнд
     *
     * @return string
     */
    public function getBrand()
    {
        if($this->response->has('level')) {
            return $this->response->get('level');
        }
        return null;
    }

    /**
     * Страна
     *
     * @return string
     */
    public function getCountryName()
    {
        if($this->response->has('country')) {
            return $this->response->get('country');
        }
        return null;
    }

    /**
     * Валюта страны
     *
     * @return string
     */
    public function geCurrency()
    {
        if($this->response->has('countrycode')) {
            return $this->response->get('countrycode');
        }
        return null;
    }

    /**
     * Название банка
     *
     * @return string
     */
    public function getBankName()
    {
        if($this->response->has('bank')) {
            return $this->response->get('bank');
        }
        return null;
    }

    /**
     * Адрес сайта банка
     *
     * @return string
     */
    public function getBankUrl()
    {
        if($this->response->has('website')) {
            return mb_strtolower($this->response->get('website'));
        }
        return null;
    }

    /**
     * Контактный номер поддержки
     *
     * @return string
     */
    public function getBankSupportNumber()
    {
        if($this->response->has('phone')) {
            return $this->response->get('phone');
        }
        return null;
    }

    /**
     * Получаем результат в массиве
     *
     * @return array
     */
    public function getRawData()
    {
        return $this->response->all();
    }
}