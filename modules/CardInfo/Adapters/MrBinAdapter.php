<?php

namespace Modules\CardInfo\Adapters;

use Illuminate\Support\Facades\Http;

class MrBinAdapter extends AbstractAdapter implements AdapterInterface
{
    /**
     * Кэш префикса
     *
     * @var string
     */
    protected $cache_prefix = 'cardinfo_mrbin_';

    /**
     * Ссылка на адаптер
     *
     * @var string
     */
    protected $url = 'https://mrbin.io';

    /**
     * Устанавливаем номер карты
     *
     * @param string $card_number
     * @return BinlistAdapter
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function setCardNumber(string $card_number)
    {
        $card_number = preg_replace('/\D/', '', $card_number);
        $this->prefix = str_pad(substr((string) $card_number, 0, static::PREFIX_LENGTH), static::PREFIX_LENGTH, '0');

        $http = Http::withToken($this->api_keys, 'Basic')->post($this->url.'/bins/bin/getFull', [
            'fullBin' => $card_number
        ])->json();

        $this->response = collect($http);
        return $this;
    }

    /**
     * Платежная система
     *
     * @return string
     */
    public function getPaymentSystem()
    {
        if($this->response->has('paymentSystem')) {
            return $this->response->get('paymentSystem');
        }
        return null;
    }

    /**
     * Тип карты
     *
     * @return string
     */
    public function getType()
    {
        if(isset($this->response['product']) and $this->response['product']['category']) {
            return $this->response['product']['category'];
        }
        return null;
    }

    /**
     * Валидация карт
     */
    public function getValidate()
    {
        return null;
    }

    /**
     * Брэнд
     *
     * @return string
     */
    public function getBrand()
    {
        return null;
    }

    /**
     * Страна
     *
     * @return string
     */
    public function getCountryName()
    {
        return null;
    }

    /**
     * Валюта страны
     *
     * @return string
     */
    public function geCurrency()
    {
        return null;
    }

    /**
     * Название банка
     *
     * @return string
     */
    public function getBankName()
    {
        if(isset($this->response['bankName'])) {
            return $this->response['bankName'];
        }
        return null;
    }

    /**
     * Адрес сайта банка
     *
     * @return string
     */
    public function getBankUrl()
    {
        return null;
    }

    /**
     * Контактный номер поддержки
     *
     * @return string
     */
    public function getBankSupportNumber()
    {
        return null;
    }

    /**
     * Получаем результат в массиве
     *
     * @return array
     */
    public function getRawData()
    {
        return $this->response->all();
    }
}
