<?php

namespace Modules\Debtors\Entities;

use Illuminate\Database\Eloquent\Model;

class Debtor extends Model
{
    protected $table = 'debtors';

    protected $fillable = [
        'value',
        'text',
        'is_bestchange'
    ];
}
