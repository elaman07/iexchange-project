<?php

namespace Modules\Debtors\Http\Controllers;



use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Debtors\Entities\Debtor;

class DebtorsController extends Controller
{

    public function index(Request $request)
    {
        // Очистить весь список
        if($request->has('clear_all')) {
            Debtor::query()->delete();
            flash('Список должников успешно очищен', ['alert alert-success']);

            return redirect()->back();
        }

        $lists = Debtor::orderByDesc('id')->paginate(20);
        return view('debtors::index', [
            'lists' =>  $lists,
            'filter' => $request->all(),
        ]);
    }

    public function create(Request $request)
    {
        return view('debtors::create', [
            'query' =>  $request->all()
        ]);
    }

    public function store(Request $request)
    {
        Debtor::create([
            'value' =>  ($request->has('value') ? $request->get('value') : ''),
            'text' =>  ($request->has('text') ? $request->get('text') : ''),
            'is_bestchange' =>  ($request->has('is_bestchange') ? $request->get('is_bestchange') : 0),
        ]);

        if($request->has('is_bestchange')) {
            addToBestchangeList($request->get('value'), $request->get('text'));
        }

        return redirect(admin_path('modules/debtors'));
    }

    public function edit($id) {
        $item = Debtor::findOrFail($id);
        return view('debtors::edit', [
            'item'  =>  $item
        ]);
    }

    /**
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $blacklist = Debtor::findOrFail($id);

        if($request->has('is_bestchange')) {
            addToBestchangeList($request->get('value'), $request->get('text'));
        }

        $blacklist->update([
            'value' =>  ($request->has('value') ? $request->get('value') : ''),
            'text' =>  ($request->has('text') ? $request->get('text') : ''),
            'is_bestchange' =>  ($request->has('is_bestchange') ? $request->get('is_bestchange') : 0),
        ]);


        flash('Запись успешно добавлена', ['alert alert-success']);
        return redirect()->route('debtors.index')
            ->with('flash_message',
                'Page'. $blacklist->name.' updated!');
    }

    public function destroy($id)
    {
        $role = Debtor::findOrFail($id);
        $role->delete();

        return redirect()->route('debtors.index');
    }
}
