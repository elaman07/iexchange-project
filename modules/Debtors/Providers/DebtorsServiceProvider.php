<?php

namespace Modules\Debtors\Providers;

use Diglactic\Breadcrumbs\Breadcrumbs;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class DebtorsServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Debtors';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'debtors';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerBreadcrumbs();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), 'modules-config.'.$this->moduleNameLower
        );
    }


    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register breadcrumbs
     */
    public function registerBreadcrumbs()
    {
        Breadcrumbs::for('debtors::index', function ($breadcrumbs) {
            $breadcrumbs->parent('admin.plugins');
            $breadcrumbs->push('Список должников',  config('admin.directory').'/modules/debtors');
        });

        Breadcrumbs::for('debtors::add', function ($breadcrumbs) {
            $breadcrumbs->parent('debtors::index');
            $breadcrumbs->push('Добавить запись',  config('admin.directory').'/modules/debtors/create');
        });

        Breadcrumbs::for('debtors::edit', function ($breadcrumbs, $id) {
            $breadcrumbs->parent('debtors::index');
            $breadcrumbs->push('Изменить запись',  config('admin.directory').'/modules/debtors/edit/?id='.$id);
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
