<?php

return [
    'title' => 'Список должников',
    'add_item' => 'Добавить запись',
    'clear_list' => 'Очистить список',
    'value' => 'Значение',
    'types' => 'Типы',
    'type' => 'Тип',
    'created_at' => 'Дата создания',
    'description' => 'Описание',
    'all' => 'Все',
    'add_to_bestchange' => 'Добавить в черный список Bestchange',
    'value_hint' => 'Добавьте контактные данные с новой строки',
    'edit_item' => 'Изменить запись'
];
