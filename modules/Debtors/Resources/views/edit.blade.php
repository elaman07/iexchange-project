@extends('admin.layouts.app')

@section('title', __('debtors::messages.edit_item'))

@section('breadcrumbs', Breadcrumbs::render('debtors::edit', $item->id))

@section('content')

    <form class="form-horizontal" method="post" action="{{admin_path('modules/debtors/'.$item->id)}}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">{{ __('debtors::messages.value') }}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="value" rows="5" placeholder="{{ __('debtors::messages.value_hint') }}">{{$item->value}}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">{{ __('debtors::messages.description') }}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="5" name="text">{{$item->text}}</textarea>
                </div>
            </div>

        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_path('modules/debtors') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection
