@extends('admin.layouts.app')

@section('title', __('debtors::messages.title'))

@section('top-block')
    <md-button ng-href="debtors/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('debtors::messages.add_item') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('debtors::index'))


@section('title-right-button')
    <md-button class="md-warn" href="{{ admin_base_path('/modules/debtors/?clear_all') }}">{{ __('admin-basic.debtors.clear_list') }}</md-button>
@endsection

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('debtors::messages.title') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('debtors::messages.created_at') }}</th>
                    <th>{{ __('debtors::messages.value') }}</th>
                    <th>{{ __('debtors::messages.description') }}</th>
                    <th>{{ __('admin-basic.action') }}</th>
                </tr>

                </thead>
                <tbody>
                @if(count($lists) > 0)
                    @foreach($lists as $list)
                        <tr>
                            <th scope="row">
                                {{ $list->created_at }}
                            </th>
                            <td>{!! nl2br($list->value) !!}</td>
                            <td style="width: 300px">{!! nl2br($list->text) !!}</td>
                            <td style="width: 200px;">
                                <div class="col-md-6">
                                    <a href="debtors/{{$list->id}}/edit" class="btn btn-info btn-sm">{{ __('admin-basic.edit') }}</a>
                                </div>

                                <div class="col-md-6">
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['debtors.destroy', $list->id] ]) !!}
                                    {!! Form::submit(__('admin-basic.delete'), ['class' => 'btn btn-danger btn-sm']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4"><p align="center"><br />{{ __('admin-basic.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('pagination')
    <div class="pagination-right">
        {!! $lists->links() !!}
    </div>
@endsection
