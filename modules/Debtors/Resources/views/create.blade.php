@extends('admin.layouts.app')

@section('title', __('debtors::messages.add_item'))

@section('breadcrumbs', Breadcrumbs::render('debtors::add'))

@section('content')

    <form class="form-horizontal" method="post" action="{{admin_path('modules/debtors') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">{{ __('debtors::messages.value') }}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="value" rows="5" placeholder="{{ __('debtors::messages.value_hint') }}"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">{{ __('debtors::messages.description') }}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="5" name="text"></textarea>
                </div>
            </div>

            @if(config('scammer.enabled'))
                <hr />
                <div class="form-group">
                    <label class="control-label col-md-2">{{ __('debtors::messages.add_to_bestchange') }}</label>
                    <div class="col-md-6">
                        <div class="material-switch switch-input-1">
                            <input id="is_bestchange" name="is_bestchange" type="checkbox" value="1" />
                            <label for="is_bestchange" class="label-primary"></label>
                        </div>
                    </div>
                </div>
            @endif


        </div>
        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;{{ __('admin-basic.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/debtors') }}">{{ __('admin-basic.back') }}</md-button>
        </div>
    </form>

@endsection
