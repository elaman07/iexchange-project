<?php

namespace Modules\PromoCodes\Http\Controllers;

use App\Models\DirectionExchange;
use App\Models\PromoCode;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class PromoCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return RedirectResponse | \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        if($request->has('closed'))
        {
            PromoCode::findOrFail($request->get('id'))->update([
                'status' => 2
            ]);
            flash('Промо-код закрыт', ['alert alert-success']);
            return redirect()->back();
        }

        $promo_codes = PromoCode::all();

        return view('promocodes::index', [
            'promo_codes' => $promo_codes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $direction_exchange = DirectionExchange::active()->pluck('tech_name', 'id');
        return view('promocodes::create', [
            'directions' => $direction_exchange
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'count_uses' => ['required', 'numeric'],
            'discount_percent' => 'required',
            'expired_at'    => ['required']
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {


            PromoCode::create([
                'id_manager' => $request->user()->id,
                'name' => $request->name,
                'status' => $request->has('status') ? $request->get('status') : 0,
                'expired_at' => $request->expired_at,
                'code' => $request->has('code') ? $request->get('code') : null,
                'count_uses' => $request->has('count_uses') ? $request->get('count_uses') : 0,
                'discount_percent' => $request->has('discount_percent') ? $request->get('discount_percent') : 0,
                'permitted_directions'  => $request->has('permitted_directions') ? implode(',', $request->get('permitted_directions')) : null,
                'forbidden_directions'  => $request->has('forbidden_directions') ? implode(',', $request->get('forbidden_directions')) : null
            ]);

            flash('Промокод успешно добавлен', ['alert alert-success']);
            return redirect()->back();
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(int $id)
    {
        $item = PromoCode::find($id);
        $direction_exchange = DirectionExchange::active()->pluck('tech_name', 'id');
        return view('promocodes::edit', [
            'item' => $item,
            'directions' => $direction_exchange
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'count_uses' => ['required', 'numeric'],
            'discount_percent' => 'required',
            'expired_at'    => ['required']
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $promo = PromoCode::findOrFail($id);
            $promo->update([
                'name' => $request->name,
                'status' => $request->has('status') ? $request->get('status') : 0,
                'expired_at' => $request->expired_at,
                'code' => $request->has('code') ? $request->get('code') : null,
                'count_uses' => $request->has('count_uses') ? $request->get('count_uses') : 0,
                'discount_percent' => $request->has('discount_percent') ? $request->get('discount_percent') : 0,
                'permitted_directions'  => $request->has('permitted_directions') ? implode(',', $request->get('permitted_directions')) : null,
                'forbidden_directions'  => $request->has('forbidden_directions') ? implode(',', $request->get('forbidden_directions')) : null
            ]);

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
