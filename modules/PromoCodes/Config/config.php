<?php

return [
    'name' => 'PromoCodes',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/promocodes'),
        'name' => 'Промо-коды'
    ]
];
