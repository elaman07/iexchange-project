@extends('admin.layouts.app')

@section('title', 'Добавить промокод')

@section('breadcrumbs', Breadcrumbs::render('promocodes::create'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/promocodes') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label class="col-sm-2 control-label">Основное</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Название промокода</div>
                        <input type="text" name="name" class="form-control" value="" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Срок окончания действий кода</div>
                        <input type="text" name="expired_at" class="form-control datetimepicker" value="">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Статус</div>
                        <select class="selectpicker" name="status">
                            <option value="0">Не активен</option>
                            <option value="1">Активен</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Промокод</div>
                        <input type="text" name="code" class="form-control" value="{{ Hashids::connection('promocode')->encode(random_int(0, 99999)) }}">
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Кол-во использований</div>
                        <input type="number" value="0" name="count_uses" class="form-control">
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Скидка (в процентах)</div>
                        <input type="text" value="0" name="discount_percent" class="form-control">
                    </div>


                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-3">
                        <div class="control-label-br">Разрешенные направления</div>
                        {{ Form::select('permitted_directions[]', $directions, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10', 'multiple']) }}
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Запрещенные направления</div>
                        {{ Form::select('forbidden_directions[]', $directions, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10', 'multiple']) }}
                    </div>

                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/promocodes') }}">Назад</md-button>
        </div>
    </form>
@endsection
