@extends('admin.layouts.app')

@section('title', 'Редактировать промокод '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('promocodes::edit', $item))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/promocodes/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')


            <div class="form-group">
                <label class="col-sm-2 control-label">Основное</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Название промокода</div>
                        <input type="text" name="name" class="form-control" value="{{ $item->name }}" required>
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Срок окончания действий кода</div>
                        <input type="text" name="expired_at" class="form-control datetimepicker" value="{{ $item->expired_at }}">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Статус</div>
                        <select class="selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>Не активен</option>
                            <option value="1" @if($item->status == 1) selected @endif>Активен</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Промокод</div>
                        <input type="text" name="code" class="form-control" value="{{ $item->code }}">
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Кол-во использований</div>
                        <input type="number" value="{{ $item->count_uses }}" name="count_uses" class="form-control">
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Скидка (в процентах)</div>
                        <input type="text" value="{{ $item->discount_percent }}" name="discount_percent" class="form-control">
                    </div>


                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Разрешенные направления</div>
                        {{ Form::select('permitted_directions[]', $directions, explode(',', $item->permitted_directions), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10', 'multiple']) }}
                    </div>

                    <div class="form-group col-md-3">
                        <div class="control-label-br">Запрещенные направления</div>
                        {{ Form::select('forbidden_directions[]', $directions, explode(',', $item->forbidden_directions), ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => '10', 'multiple']) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/promocodes') }}">Назад</md-button>
        </div>
    </form>
@endsection
