@extends('admin.layouts.app')

@section('title', 'Промокоды')

@section('breadcrumbs', Breadcrumbs::render('promocodes::index'))

@section('top-block')

    <md-button ng-href="{{ admin_base_path('/modules/promocodes/create') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить промокод</span>
    </md-button>
@endsection


@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Название</th>
            <th>Код активации</th>
            <th>Кол-во использований</th>
            <th>Дата Создания</th>
            <th>Дата завершения</th>
            <th>Статус</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if(count($promo_codes) > 0)
            @foreach($promo_codes as $promo)
                <tr>
                    <th>
                        <a href="{{ admin_base_path('/modules/promocodes/'.$promo->id.'/edit') }}">
                            {{ $promo->name }} &nbsp;<i class="fal fa-pencil"></i>
                        </a>
                    </th>
                    <td>
                        {{ $promo->code }}
                    </td>
                    <td>
                        Использовано   {{ $promo->used }} из {{ $promo->count_uses }}
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($promo->created_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($promo->created_at)->diffForHumans() }}</small>
                    </td>

                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($promo->expired_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($promo->expired_at)->diffForHumans() }}</small>
                    </td>
                    <td>
                        @if($promo->status == 1)
                            <div class="text-warning">
                                <i class="far fa-alarm-clock"></i>&nbsp;
                                Активный
                            </div>
                        @else
                            <div class="text-success">
                                <i class="far fa-check"></i>&nbsp;
                                Завершен
                            </div>
                        @endif
                    </td>

                    <td style="width: 140px">
                        @if($promo->status != 2)
                            <a style="font-size: 12px;margin-top: 3px;font-weight: 500;" href="?id={{ $promo->id }}&closed">Отключить</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />Список пуст</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
