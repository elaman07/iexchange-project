<?php

namespace Modules\Contests\Http\Controllers;

use App\Models\CodeCurrency;
use App\Models\LinksReview;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Modules\Contests\Entities\ContestModel;
use Modules\Contests\Entities\ContestsUser;

class ContestsController extends Controller
{
    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_view_contests_home',
        'is_view_contests_account'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse
     */
    public function index(Request $request)
    {
        if($request->has('closed'))
        {
            if(config('admin.is_demo_mode') == true) {
                flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
                return redirect()->back();
            }


            ContestModel::findOrFail($request->get('id'))->update([
                'status' => 2
            ]);
            \Cache::forget('actual-lottery-module');
            flash('Конкурс завершен', ['alert alert-success']);
            return redirect()->back();
        }


        $contests = ContestModel::orderBy('status')->get();

        return view('contests::index', [
            'contests' => $contests
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        $codes = CodeCurrency::active()->get();
        return view('contests::create', compact('codes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if($request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);

            flash('Настройки успешно сохранение', ['alert alert-success']);
            return \redirect()->to(admin_base_path('/modules/contests'));
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $destinationPath = public_path('/storage/contests');

            $filename_home = null;
            $filename_account = null;
            if($request->hasFile('icon_url_home'))
            {
                $icon_url_home = $request->file('icon_url_home');
                $filename_home = sprintf('%s.%s', Str::random(10), $icon_url_home->getClientOriginalExtension());
                if(!\File::isDirectory(public_path('storage/contests/'))) {
                    \File::makeDirectory(public_path('storage/contests/'), 0777, true, true);
                }
                $icon_url_home->move($destinationPath, $filename_home);
            }

            if($request->hasFile('icon_url_account'))
            {
                $icon_url_account = $request->file('icon_url_account');
                $filename_home = sprintf('%s.%s', Str::random(10), $icon_url_account->getClientOriginalExtension());
                if(!\File::isDirectory(public_path('storage/contests/'))) {
                    \File::makeDirectory(public_path('storage/contests/'), 0777, true, true);
                }
                $icon_url_account->move($destinationPath, $filename_home);
            }

            $options = [
                'id_manager' => $request->user()->id,
                'name' => $request->name,
                'status' => $request->status,
                'duration' => $request->duration,
                'is_manual_bank' => $request->is_manual_bank,
                'bank' => ($request->has('bank') ? $request->get('bank') : 0),
                'bank_base' => ($request->has('bank') ? $request->get('bank') : 0),
                'id_code_currency' => ($request->has('id_code_currency') ? $request->get('id_code_currency') : 0),
                'code_name' => ($request->has('code_name') ? $request->get('code_name') : null),
                'code_sign' => ($request->has('code_sign') ? $request->get('code_sign') : null),
                'code_position' => ($request->has('code_position') ? $request->get('code_position') : 0),
                'percent' => ($request->has('percent') ? $request->get('percent') : 0),

                'title_color' => ($request->has('title_color') ? $request->get('title_color') : null),
                'subtitle_color' => ($request->has('subtitle_color') ? $request->get('subtitle_color') : null),
                'icon_url_home' => $filename_home,
                'icon_url_account' => $filename_account
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['subtitle'][$key] = $request->get('subtitle'.$item['field']);
                $options['button_name'][$key] = $request->get('button_name'.$item['field']);
                $options['info_title'][$key] = $request->get('info_title'.$item['field']);
                $options['info_text'][$key] = $request->get('info_text'.$item['field']);
            }

            flash('Конкурс успешно добавлен', ['alert alert-success']);
            $contest = ContestModel::create($options);


            return redirect()->to(
                admin_base_path('/modules/contests/'.$contest->id.'/edit')
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|RedirectResponse
     */
    public function edit(int $id)
    {
        $codes = CodeCurrency::active()->get();
        $item = ContestModel::findOrFail($id);


        return view('contests::edit', compact('item', 'codes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $contests = ContestModel::findOrFail($id);


            $destinationPath = public_path('/storage/contests');

            $filename_home = $contests->icon_url_home;
            $filename_account = $contests->icon_url_account;
            if($request->hasFile('icon_url_home'))
            {
                $icon_url_home = $request->file('icon_url_home');
                $filename_home = sprintf('%s.%s', Str::random(10), $icon_url_home->getClientOriginalExtension());
                if(!\File::isDirectory(public_path('storage/contests/'))) {
                    \File::makeDirectory(public_path('storage/contests/'), 0777, true, true);
                }
                $icon_url_home->move($destinationPath, $filename_home);
            }

            if($request->hasFile('icon_url_account'))
            {
                $icon_url_account = $request->file('icon_url_account');
                $filename_account = sprintf('%s.%s', Str::random(10), $icon_url_account->getClientOriginalExtension());
                if(!\File::isDirectory(public_path('storage/contests/'))) {
                    \File::makeDirectory(public_path('storage/contests/'), 0777, true, true);
                }
                $icon_url_account->move($destinationPath, $filename_account);
            }


            $options = [
                'name' => $request->name,
                'status' => $request->has('status') ? $request->get('status') : 0,
                'duration' => $request->duration,
                'is_manual_bank' => $request->has('is_manual_bank') ? $request->get('is_manual_bank') : 0,
                'bank' => ($request->has('bank') ? $request->get('bank') : 0),
                'bank_base' => ($request->has('bank') ? $request->get('bank') : 0),
                'id_code_currency' => ($request->has('id_code_currency') ? $request->get('id_code_currency') : 0),
                'code_name' => ($request->has('code_name') ? $request->get('code_name') : null),
                'code_sign' => ($request->has('code_sign') ? $request->get('code_sign') : null),
                'code_position' => ($request->has('code_position') ? $request->get('code_position') : 0),
                'percent' => ($request->has('percent') ? $request->get('percent') : 0),

                'title_color' => ($request->has('title_color') ? $request->get('title_color') : null),
                'subtitle_color' => ($request->has('subtitle_color') ? $request->get('subtitle_color') : null),
                'icon_url_home' => $filename_home,
                'icon_url_account' => $filename_account
            ];

            foreach (config('app.form_lang') as $key => $item) {
                $options['title'][$key] = $request->get('title'.$item['field']);
                $options['subtitle'][$key] = $request->get('subtitle'.$item['field']);
                $options['info_title'][$key] = $request->get('info_title'.$item['field']);
                $options['info_text'][$key] = $request->get('info_text'.$item['field']);
                $options['button_name'][$key] = $request->get('button_name'.$item['field']);
            }

            $contests->update($options);

            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Renderable
     */
    public function destroy(int $id)
    {

        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        //
    }


    public function users(Request $request)
    {

        $item = ContestModel::find($request->id);
        $users = ContestsUser::filter($request->all())->where('id_contest', $item->id)->cursor();

        if($request->has('actions') and $request->get('actions') == 'winners')
        {

            if(config('admin.is_demo_mode') == true) {
                flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
                return redirect()->back();
            }

            // Количество победителей
            $count_winner = $request->has('count_winner') ? $request->get('count_winner') : 1;
            // Получаем победителей
            $winner_ids = ContestsUser::where([
                ['id_contest', $item->id],
                ['status', 1]
            ])->inRandomOrder()->limit($count_winner)->get()->pluck('id', 'id')->toArray();
            $item->contests_has_user()->sync($winner_ids);

            return redirect()->to(
                admin_base_path(sprintf('/modules/contests/%s/users', $item->id))
            );
        }


        if($request->getMethod() == 'POST' and $request->actions == 'save')
        {
            foreach ($request->data as $key => $value)
            {
                $user = ContestsUser::find($key);
                $user->update([
                    'status'        => $request->status[$key] ?? $user->status,
                    'bonus'         => $request->bonus[$key] ?? $user->bonus,
                    'id_monitoring' => $request->id_monitoring[$key] ?? $user->id_monitoring
                ]);
            }
            flash('Данные успешно обновлены', ['alert alert-success']);
            return redirect()->back();
        }

        $link_reviews = LinksReview::all();


        return view('contests::users', [
            'item' => $item,
            'users' => $users,
            'link_reviews' => $link_reviews
        ]);
    }
}
