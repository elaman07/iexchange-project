<?php
namespace Modules\Contests\Http\Controllers;

use App\Models\Faq;
use App\Models\FaqCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\Contests\Entities\ContestFaqModel;

class ContestsFaqController extends Controller
{
    /**
     * Список вопросов и ответов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $faq = ContestFaqModel::orderBy('sorting')->get();

        return view('contests::faq.index', [
            'faq'   =>  $faq
        ]);
    }

    /**
     * Форма добавленя вопросов и ответов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create() {
        return view('contests::faq.create');
    }

    /**
     * Обработка и добавление вопросов и ответов
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // Доп. настройки
        if($request->has('actions'))
        {
            foreach ($request->get('item_id') as $value) {
                ContestFaqModel::find($value)->update([
                    'status'    =>  ($request->status[$value] ?? 0)
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->back();
        }

        $options = [];
        $options['status'] = 1;
        foreach (config('app.form_lang') as $key => $item) {
            $options['title'][$key] = $request->get('title'.$item['field']);
            $options['description'][$key] = $request->get('description'.$item['field']);
        }

        ContestFaqModel::create($options);

        flash('Запись успешно добавлена', ['alert alert-success']);
        return redirect()->route('contests-faq.index');

    }

    /**
     * Форма редактирования вопросов и ответов
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function edit(int $id)
    {
        $faq = ContestFaqModel::findOrFail($id);

        return view('contests::faq.edit', compact('faq'));
    }

    /**
     * Обработка и редактирование вопросов и ответов
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $faq = ContestFaqModel::findOrFail($id);
        $options = [];
        foreach (config('app.form_lang') as $key => $item) {
            $options['title'][$key] = $request->get('title'.$item['field']);
            $options['description'][$key] = $request->get('description'.$item['field']);
        }
        $faq->update($options);


        flash('Запись успешно обновлена', ['alert alert-success']);
        return redirect()->route('contests-faq.index');
    }

    /**
     * Удаление вопросов и ответов
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $faq = ContestFaqModel::findOrFail($id);
        flash("Запись {$faq->name} успешно удалена", ['alert alert-success']);
        $faq->delete();
        return redirect()->route('contests-faq.index');
    }

    /**
     * Сортировка вопросов и ответов
     */
    public function sorting()
    {
        $faq = Faq::orderBy('sorting')->get();

        return view('contests::faq.sorting', [
            'faqs' => $faq
        ]);
    }
}
