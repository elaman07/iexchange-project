<?php
namespace Modules\Contests\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Contests\Entities\ContestConditionModel;

class ContestsConditionController extends Controller
{

    /**
     * Список требований
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $items = ContestConditionModel::orderBy('id')->get();

        return view('contests::condition.index', [
            'items' => $items
        ]);
    }

    /**
     * Добавить требование
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('contests::condition.create');
    }

    /**
     * Обработка и добавление условий
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $options = [];
            $options['id_manager'] = $request->user()->id;
            $options['sorting'] = $request->has('sorting') ? $request->get('sorting') : 1;
            foreach (config('app.form_lang') as $key => $item) {
                $options['description'][$key] = $request->get('description'.$item['field']);
            }

            ContestConditionModel::create($options);

            flash('Условие успешно добавлено', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ContestConditionModel::findOrFail($id);
        return view('contests::condition.edit', compact('item'));
    }

    /**
     * Обработка и обновление
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = ContestConditionModel::findOrFail($id);

            $options = [];
            $options['sorting'] = $request->has('sorting') ? $request->get('sorting') : 1;
            foreach (config('app.form_lang') as $key => $value) {
                $options['description'][$key] = $request->get('description'.$value['field']);
            }

            $item->update($options);

            flash('Условие успешно обновлено', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        $item = ContestConditionModel::findOrFail($id);
        $item->delete();

        flash('Условие успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }
}
