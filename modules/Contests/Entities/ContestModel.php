<?php

namespace Modules\Contests\Entities;

use App\Models\CodeCurrency;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Translatable\HasTranslations;

class ContestModel extends Model
{
    use HasFactory, HasTranslations;

    protected $table = 'contests';

    protected $translatable = [
        'title',
        'subtitle',
        'button_name',
        'info_title',
        'info_text'
    ];

    protected $fillable = [
        'id_user',
        'id_manager',
        'status',
        'name',
        'duration',
        'max_limit_user',
        'is_manual_bank',
        'bank',
        'bank_base',
        'percent',

        'id_code_currency',
        'code_name',
        'code_sign',
        'code_position',

        'title',
        'subtitle',
        'button_name',

        'title_color',
        'subtitle_color',
        'icon_url_home',
        'icon_url_account',

        'info_title',
        'info_text'
    ];


    /**
     * Информационные поля
     */
    public function contests_has_user(): MorphToMany
    {
        return $this->morphToMany(
            ContestsUser::class,
            'model',
            'contests_has_contests_users',
            'model_id',
            'contests_user_id'
        );
    }

    public function contests_user(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ContestsUser::class,'id_contest','id');
    }

    public function contests_waiting_user(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ContestsUser::class,'id_contest','id')->where('status', '=',0);
    }

    public function code_currency(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(CodeCurrency::class,'id','id_code_currency');
    }
}
