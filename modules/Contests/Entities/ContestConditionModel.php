<?php

namespace Modules\Contests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class ContestConditionModel extends Model
{
    use HasFactory, HasTranslations;

    protected $table = 'contests_conditions';

    protected $fillable = [
        'id_manager',
        'description',
        'sorting'
    ];


    protected $translatable = [
        'description'
    ];
}
