<?php

namespace Modules\Contests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class ContestFaqModel extends Model
{
    use HasFactory, HasTranslations;

    protected $table = 'contests_faq';

    protected $fillable = [
        'title',
        'description',
        'user_id',
        'status',
        'sorting'
    ];

    protected $translatable = [
        'title', 'description'
    ];
}
