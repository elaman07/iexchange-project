<?php

namespace Modules\Contests\Entities;

use App\Models\LinksReview;
use App\Models\User;
use EloquentFilter\Filterable;
use Google\Service\CivicInfo\Contest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class ContestsUser extends Model
{
    use HasFactory, Filterable;

    protected $table = 'contests_users';

    protected $fillable = [
        'id_user',
        'id_monitoring',
        'status',
        'name',
        'email',
        'link',
        'bonus',
        'id_contest',
        'code_sign_bonus',
        'code_name_bonus',
        'percent',
        'status'
    ];


    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\ContestsUserFilter::class);
    }


    public function user() {
        return $this->hasOne(User::class,'id','id_user');
    }

    public function link_review() {
        return $this->hasOne(LinksReview::class,'id','id_monitoring');
    }

    public function contests_user_winner(): MorphToMany
    {
        return $this->morphedByMany(ContestModel::class, 'model','contests_has_contests_users','contests_user_id');
    }

}
