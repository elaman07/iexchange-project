<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => admin_base_path('/modules/', true), 'middleware' => admin_middleware_route()], function()
{
    Route::match(['get', 'post'], 'contests/{id}/users', 'ContestsController@users');
    Route::resource('contests', 'ContestsController');
    Route::resource('contests-conditions', 'ContestsConditionController');


    Route::get('contests-faq/{id}/delete', 'ContestsFaqController@destroy');
    Route::resource('contests-faq', 'ContestsFaqController');
});


Route::group(['prefix' => 'api'], function() {
    Route::prefix('v3')->group(function() {
        Route::get('contests', 'APIContestsController@index');
        Route::post('addContests', 'APIContestsController@add');
        Route::get('faqContest', 'APIContestsController@faq');
    });
});
