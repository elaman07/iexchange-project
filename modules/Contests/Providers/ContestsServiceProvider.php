<?php

namespace Modules\Contests\Providers;

use Diglactic\Breadcrumbs\Breadcrumbs;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ContestsServiceProvider extends ServiceProvider
{
    private $baseModuleRoute = 'modules/contests/';

    private $baseModuleRoute2 = 'modules/contests-conditions/';

    private $baseModuleRoute3 = 'modules/contests-faq/';


    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Contests';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'contests';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerBreadcrumbs();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), 'modules-config.'.$this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register breadcrumbs
     */
    public function registerBreadcrumbs()
    {
        Breadcrumbs::for('contests::index', function ($breadcrumbs) {
            $breadcrumbs->parent('admin.plugins');
            $breadcrumbs->push('Конкурсы',  admin_base_path($this->baseModuleRoute));
        });

        Breadcrumbs::for('contests::create', function ($breadcrumbs) {
            $breadcrumbs->parent('contests::index');
            $breadcrumbs->push('Новый конкурс',  admin_base_path($this->baseModuleRoute.'/create'));
        });

        Breadcrumbs::for('contests::edit', function ($breadcrumbs, $item) {
            $breadcrumbs->parent('contests::index');
            $breadcrumbs->push('Редактировать конкурс '.$item->name,  admin_base_path($this->baseModuleRoute.$item->id.'/edit'));
        });

        Breadcrumbs::for('contests::users', function ($breadcrumbs, $item) {
            $breadcrumbs->parent('contests::index');
            $breadcrumbs->push(' Список участников розыгрыша '.$item->name,  admin_base_path($this->baseModuleRoute.$item->id.'/users'));
        });

        Breadcrumbs::for('contests::conditions.index', function ($breadcrumbs) {
            $breadcrumbs->parent('contests::index');
            $breadcrumbs->push('Условие конкурса',  admin_base_path($this->baseModuleRoute2));
        });

        Breadcrumbs::for('contests::conditions.create', function ($breadcrumbs) {
            $breadcrumbs->parent('contests::conditions.index');
            $breadcrumbs->push('Добавить условие',  admin_base_path($this->baseModuleRoute2).'/create');
        });

        Breadcrumbs::for('contests::conditions.edit', function ($breadcrumbs, $item) {
            $breadcrumbs->parent('contests::conditions.index');
            $breadcrumbs->push('Изменить условие №'.$item->id,  admin_base_path($this->baseModuleRoute2).$item->id.'/edit');
        });


        Breadcrumbs::for('contests::faq.index', function ($breadcrumbs) {
            $breadcrumbs->parent('contests::index');
            $breadcrumbs->push('Список вопросов и ответов',  admin_base_path($this->baseModuleRoute3));
        });

        Breadcrumbs::for('contests::faq.create', function ($breadcrumbs) {
            $breadcrumbs->parent('contests::faq.index');
            $breadcrumbs->push('Добавить FAQ',  admin_base_path($this->baseModuleRoute3).'/create');
        });

        Breadcrumbs::for('contests::faq.edit', function ($breadcrumbs, $item) {
            $breadcrumbs->parent('contests::faq.index');
            $breadcrumbs->push('Изменить FAQ №'.$item->id,  admin_base_path($this->baseModuleRoute3).$item->id.'/edit');
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
