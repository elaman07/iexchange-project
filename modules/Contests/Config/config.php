<?php

return [
    'name' => 'Contests',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/contests'),
        'name' => 'Конкурсы'
    ]
];
