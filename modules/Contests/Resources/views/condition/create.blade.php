@extends('admin.layouts.app')

@section('title', __('Добавить условие'))

@section('breadcrumbs', Breadcrumbs::render('contests::conditions.create'))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/contests-conditions') }}">
        @csrf
        <div class="default-panel-body">


            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Новое условие') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Текст условия') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="description{{$form_lang_value['field']}}" class="form-control" value="">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">{{ __('Порядковый номер') }}</label>
                <div class="col-sm-10">
                    <input type="number" name="sorting" class="form-control" value="1" required style="width: 150px">
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Добавить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/contests-conditions') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
