@extends('admin.layouts.app')

@section('title', __('Условие конкурса'))

@section('top-block')
    <md-button ng-href="contests-conditions/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить условие</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('contests::conditions.index'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('ID') }}</th>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Дата обновления') }}</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($items) > 0)
            @foreach($items as $item)
                <tr>
                    <th>{{$item->id}}</th>
                    <td>{!! $item->description !!}</td>
                    <td>{{$item->updated_at}}</td>

                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="contests-conditions/{{$item->id}}/edit" class="btn btn-info btn-sm">{{ __('Изменить') }}</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['contests-conditions.destroy', $item->id] ]) !!}
                            {!! Form::submit(__('Удалить'), ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Ничего не найдено') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
