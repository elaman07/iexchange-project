@extends('admin.layouts.app')

@section('title',  __('Изменить условие').' №'.$item->id)

@section('breadcrumbs', Breadcrumbs::render('contests::conditions.edit', $item))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/contests-conditions/'.$item->id) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Изменить условие') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Текст условия') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="description{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('description', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="sign" class="col-sm-2 control-label">{{ __('Порядковый номер') }}</label>
                <div class="col-sm-10">
                    <input type="number" name="sorting" class="form-control" value="{{ $item->sorting }}" required style="width: 150px">
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/contests-conditions') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
