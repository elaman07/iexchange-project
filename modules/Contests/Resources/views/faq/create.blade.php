@extends('admin.layouts.app')

@section('title', __('admin-tools.faq.add_faq'))

@section('breadcrumbs', Breadcrumbs::render('contests::faq.create'))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/contests-faq') }}">
        @csrf
        <div class="default-panel-body">


            <div class="form-group">
                <label class="col-sm-2 control-label">Новая запись</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>Название</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang_value['field']}}" class="form-control" value="">
                            </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-12 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>Описание</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#button_name{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="button_name{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <textarea class="form-control" name="description{{$form_lang_value['field']}}" id="{{$form_lang_value['text']}}" rows="5"></textarea>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.faq.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/contests-faq') }}">{{ __('admin-tools.faq.back') }}</md-button>
        </div>
    </form>
@endsection
