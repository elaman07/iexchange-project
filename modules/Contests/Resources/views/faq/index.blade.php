@extends('admin.layouts.app')

@section('title', __('admin-tools.faq.title'))


@section('top-block')


    <md-button ng-href="contests-faq/create" class="btn-float" layout="column">
        <i class="far fa-plus add-item-button-color"></i>
        <span class="add-item-button-color">{{ __('admin-tools.faq.add_faq') }}</span>
    </md-button>
@endsection


@section('breadcrumbs', Breadcrumbs::render('contests::faq.index'))

@section('no-block-content')

    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('admin-tools.faq.title') }}</h2>
            <div class="clearfix"></div>
        </div>


        <div class="x_content">
            <form method="post" action="?">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>{{ __('admin-tools.faq.name') }}</th>
                        <th>{{ __('admin-tools.faq.status') }}</th>
                        <th>{{ __('admin-tools.faq.published_at') }}</th>
                        <th>Посл. обновление</th>
                        <th>{{ __('admin-tools.faq.action') }}</th>
                    </tr>

                    </thead>
                    <tbody>

                    @if(count($faq) > 0)
                        @foreach($faq as $item)
                            <tr>
                                <th>
                                    <a href="{{ admin_base_path('/modules/contests-faq/'.$item->id.'/edit') }}">
                                        {{ $item->title }} &nbsp;<i class="fal fa-pencil"></i>
                                    </a>
                                </th>

                                <td>
                                    <input type="hidden" name="item_id[]" value="{{$item->id}}">

                                    <div class="material-switch">
                                        <input id="SwitchOptionPrimary{{$item->id}}" name="status[{{$item->id}}]" type="checkbox" value="1" @if($item->status == 1) checked @endif />
                                        <label for="SwitchOptionPrimary{{$item->id}}" class="label-success"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->created_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                </td>

                                <td>
                                    <div class=""> {{ \Illuminate\Support\Carbon::parse($item->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                    <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($item->updated_at)->diffForHumans() }}</small>
                                </td>

                                <td style="width: 100px">
                                    <md-button class="md-icon-button" ng-href="{{ admin_base_path('/modules/contests-faq/'.$item->id.'/delete')  }}">
                                        <i class="fas fa-trash text-danger"></i>
                                    </md-button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />{{ __('admin-tools.list_is_empty') }}</p></td>
                        </tr>
                    @endif
                    </tbody>

                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">{{ __('admin-tools.faq.save') }}</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.faq.run') }}</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
@endsection
