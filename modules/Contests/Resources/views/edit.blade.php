@extends('admin.layouts.app')

@section('title', __('Редактировать конкурс').' '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('contests::edit', $item))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/contests/'.$item->id) }}" enctype="multipart/form-data">
        <div class="default-panel-body">
            @csrf
            @method('PUT')


            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Основное') }}</label>
                <div class="col-sm-10">

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Название конкурса') }}</div>
                        <input type="text" name="name" class="form-control" value="{{ $item->name }}" required>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Продолжительность конкурса') }}</div>
                        <input type="text" name="duration" class="form-control datetimepicker" value="{{ $item->duration }}">
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Статус') }}</div>
                        <select class="selectpicker" name="status">
                            <option value="0" @if($item->status == 0) selected @endif>{{ __('Не активен') }}</option>
                            <option value="1" @if($item->status == 1) selected @endif>{{ __('Активен') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#info_title-{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="info_title-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="info_title{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('info_title', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Описание') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#info_text-{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="info_text-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <textarea rows="5" class="form-control" name="info_text{{$form_lang_value['field']}}">{{ $item->getTranslation('info_text', $form_lang_key) }}</textarea>
                            </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <hr />

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Ручное увеличение банка') }}</div>
                        <select class="selectpicker" name="is_manual_bank">
                            <option value="0" @if($item->is_manual_bank == 0) selected @endif>{{ __('Нет') }}</option>
                            <option value="1" @if($item->is_manual_bank == 1) selected @endif>{{ __('Да') }}</option>
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Депозит банка') }}</div>
                        <input type="text" name="bank" class="form-control" value="{{ $item->bank }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Процент в банк от заявок') }}</div>
                        <input type="text" value="{{ $item->percent }}" name="percent" class="form-control">
                        <p class="input-p-text">{{ __('Укажите процент, который будет зачисляться в банк от исполненных заявок') }}.</p>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Код валюты') }}</div>
                        <select class="selectpicker" name="id_code_currency" data-size="10" data-live-search="true">
                            @foreach($codes as $code)
                                <option value="{{ $code->id }}" @if($item->id_code_currency == $code->id) selected @endif>{{ $code->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Название валюты') }}</div>
                        <input type="text" name="code_name" class="form-control" placeholder="{{ __('Укажите название валюты для отображения') }}" value="{{ $item->code_name }}">
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Символ валюты') }}</div>
                        <input type="text" name="code_sign" class="form-control" placeholder="{{ __('Укажите символ валюты') }}" value="{{ $item->code_sign }}">
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Иконка для главной') }}</div>
                        <input type="file" name="icon_url_home" class="form-control" accept="image/*">
                        <div class="input-p-text">
                            @if(!is_null($item->icon_url_home))
                                {{ __('Текущая иконка') }}:
                                <a target="_blank" href="/storage/contests/{{ $item->icon_url_home }}">{{ $item->icon_url_home }}</a>
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Иконка для личного кабинета') }}</div>
                        <input type="file" name="icon_url_account" class="form-control" accept="image/*">
                        <div class="input-p-text">
                            @if(!is_null($item->icon_url_account))
                                {{ __('Текущая иконка') }}:
                                <a target="_blank" href="/storage/contests/{{ $item->icon_url_account }}">{{ $item->icon_url_account }}</a>
                            @endif
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <hr />


                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{{ __('Для главной страницы') }}</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Заголовок') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="title{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('title', $form_lang_key) }}">
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Цвет заголовка') }}</div>
                        <div id="cp1" class="input-group colorpicker-component">
                            <input type="text" value="{{ $item->title_color }}" name="title_color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>


                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Краткий текст') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#subtitle{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="subtitle{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="subtitle{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('subtitle', $form_lang_key) }}" >
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-md-6">
                        <div class="control-label-br">{{ __('Цвет краткого текста') }}</div>
                        <div id="cp2" class="input-group colorpicker-component">
                            <input type="text" value="{{ $item->subtitle_color }}" name="subtitle_color" class="form-control" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-group col-md-6 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('Название кнопки') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                                        <li @if($form_lang_value['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#button_name{{ $form_lang_key }}">{{ $form_lang_key }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang_value)
                            <div id="button_name{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang_value['active'] == true) active @endif">
                                <input type="text" name="button_name{{$form_lang_value['field']}}" class="form-control" value="{{ $item->getTranslation('button_name', $form_lang_key) }}" >
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('Сохранить') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/contests') }}">{{ __('Назад') }}</md-button>
        </div>
    </form>
@endsection
