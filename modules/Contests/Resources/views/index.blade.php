@extends('admin.layouts.app')

@section('title', __('Конкурсы'))

@section('breadcrumbs', Breadcrumbs::render('contests::index'))

@section('video__instruction', 'https://www.youtube.com/embed/YNSAmjCzATw?autoplay=1&amp;mute=0')


@section('top-block')

    <md-button ng-href="{{ admin_base_path('/modules/contests-faq') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-question text-primary"></md-icon>
        <span>{{ __('Вопросы и Ответы') }}</span>
    </md-button>

    <md-button ng-href="{{ admin_base_path('/modules/contests-conditions') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-list text-primary"></md-icon>
        <span>{{ __('Условии конкурса') }}</span>
    </md-button>

    <md-button ng-href="{{ admin_base_path('/modules/contests/create') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>{{ __('Новый конкурс') }}</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">{{ __('Настройки') }}</span>
        </md-button>
    @endcan
@endsection


@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>{{ __('Название') }}</th>
            <th>{{ __('Сумма в банке') }}</th>
            <th>{{ __('Участники') }}</th>
            <th>{{ __('Победители') }}</th>
            <th>{{ __('Дата Создания') }}</th>
            <th>{{ __('Дата завершения') }}</th>
            <th>{{ __('Статус') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if(count($contests) > 0)
            @foreach($contests as $contest)
                <tr style="{{ $contest->status == 2 ? 'border-left: 3px solid #50ff50;background: #f6fff6;' : 'border-left: 3px solid #ffc950;background: #fff9ec;'}}">
                    <th>
                        <a href="{{ admin_base_path('modules/contests/'.$contest->id.'/edit') }}">{{ $contest->name }} &nbsp;<i class="fal fa-pencil"></i></a>
                    </th>
                    <td>{{ iex_number_format($contest->bank, 2, true) }} {{ $contest->code_sign }}</td>
                    <td>
                        <a href="{{ admin_base_path('/modules/contests/'.$contest->id.'/users') }}">{{ __('Всего участников') }}: <b>{{ $contest->contests_user->count() }}</b></a>
                        <br />
                        @if($contest->contests_waiting_user->count() > 0)
                            <small class="text-danger">
                                {{ __('В ожидании') }}: {{ $contest->contests_waiting_user->count() }}</small>
                        @endif
                    </td>
                    <td>
                        @if(count($contest->contests_has_user) > 0)
                            <ul class="list-unstyled">
                                @foreach($contest->contests_has_user as $value)
                                    <li>- <a style="font-size: 11px;font-weight: bold;" href="{{ admin_base_path(sprintf('/modules/contests/%s/users?email=%s', $contest->id, $value->email)) }}">{{ $value->name }} ({{ $value->email }})</a></li>
                                @endforeach
                            </ul>
                        @else
                            <small class="text-muted">{{ __('Победители не выбраны') }}</small>
                        @endif
                    </td>
                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($contest->created_at)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($contest->created_at)->diffForHumans() }}</small>
                    </td>

                    <td>
                        <div class=""> {{ \Illuminate\Support\Carbon::parse($contest->duration)->translatedFormat('d M Y H:i') }}</div>
                        <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($contest->duration)->diffForHumans() }}</small>
                    </td>
                    <td>
                        @if($contest->status == 1)
                            <div class="text-warning">
                                <i class="far fa-alarm-clock"></i>&nbsp;
                                {{ __('Активный') }}
                            </div>
                        @else
                            <div class="text-success">
                                <i class="far fa-check"></i>&nbsp;
                                {{ __('Завершен') }}
                            </div>
                        @endif
                    </td>

                    <td style="width: 140px">
                        @if($contest->status != 2)
                            <a style="font-size: 12px;margin-top: 3px;font-weight: 500;" href="?id={{ $contest->id }}&closed">{{ __('Завершить конкурс') }}</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
            </tr>
        @endif
        </tbody>
    </table>


    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('admin-tools.contact.settings') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">Разрешить отображение конкурсов на главной странице</label>
                            <div class="col-md-6">
                                <select name="is_view_contests_home" class="form-control selectpicker">
                                    <option value="0" @if(iEXSetting('is_view_contests_home') == 0) selected @endif>Да</option>
                                    <option value="1" @if(iEXSetting('is_view_contests_home') == 1) selected @endif>Нет</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-6">Разрешить отображение конкурсов в личном кабинете</label>
                            <div class="col-md-6">
                                <select name="is_view_contests_account" class="form-control selectpicker">
                                    <option value="0" @if(iEXSetting('is_view_contests_account') == 0) selected @endif>Да</option>
                                    <option value="1" @if(iEXSetting('is_view_contests_account') == 1) selected @endif>Нет</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('admin-tools.contact.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('admin-tools.contact.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
