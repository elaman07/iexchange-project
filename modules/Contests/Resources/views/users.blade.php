@extends('admin.layouts.app')

@section('title', 'Список участников розыгрыша '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('contests::users', $item))

@section('no-block-content')

    <div style="margin-right: 10px;" class="x_panel">
        <div class="x_title" layout="row">
            <h2>{{ 'Список участников розыгрыша '.$item->name }}</h2>
            <div flex=""></div>
            <div class="heading-right-button">
                @if($item->contests_has_user()->count() == 0)
                <md-button class="md-primary" data-toggle="modal" href="#winner">
                    Выбрать победителей
                </md-button>
                @else
                    <md-button style="color: #5cb85c">
                        <i class="far fa-check"></i>
                        Победители выбраны
                    </md-button>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form method="post" action="?">
                <input type="hidden" name="actions" value="update">
                @csrf
                <table class="table table-border-2">
                    <thead>
                    <tr>
                        <th>Дата добавления</th>
                        <th>Название</th>
                        <th>E-mail</th>
                        <th>Ссылка на отзыв</th>
                        <th>Клиент</th>
                        <th>Отзыв с сервиса</th>
                        <th>Статус клиента</th>
                        <th style="width: 200px">Бонус</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($users) > 0)
                        @foreach($users as $user)
                            <tr @if(!empty($user->contests_user_winner->first()) ) style="border-left: 3px solid #00bd00;background: #efffef;" @endif>
                                <th>
                                    {{ $user->created_at }}

                                    @if(!empty($user->contests_user_winner->first()))
                                        <div class="text-success">Победитель</div>
                                    @endif
                                </th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->link }}</td>
                                <td>
                                    <div>
                                        <a href="{{ admin_base_path('/account/users?action=filter&id='.$user->user->id.'&sorting=id') }}">
                                            <b>{{$user->user->name}}</b>
                                        </a>
                                    </div>
                                    <small>{{ $user->user->email }}</small>
                                </td>
                                <td>
                                    <select class="selectpicker" name="id_monitoring[{{$user->id}}]">
                                        @foreach($link_reviews as $link)
                                            <option value="{{ $link->id }}" @if($link->id == $user->id_monitoring) selected @endif>{{ $link->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    @if($user->status == 0)
                                        <select class="selectpicker" name="status[{{$user->id}}]">
                                            <option value="0" @if($user->status == 0) selected @endif>--</option>
                                            <option value="1" @if($user->status == 1) selected @endif>Одобрить участие</option>
                                            <option value="2" @if($user->status == 2) selected @endif>Отклонить участие</option>
                                        </select>
                                    @elseif($user->status == 1)
                                        <label class="label label-success">Участвует в розыгрыше</label>
                                    @else
                                        <label class="label label-danger">Отклонен</label>
                                    @endif

                                </td>
                                <td>
                                    <input type="hidden" name="data[{{ $user->id }}]"  value="{{ $user->id }}">
                                    @if($user->status == 1)
                                        @if($user->bonus > 0)
                                            <span class="text-success">Бонус в размере {{ $user->bonus }} зачислен</span>
                                        @else
                                            <input type="text" name="bonus[{{ $user->id }}]" class="form-control" style="width:150px" value="{{ $user->bonus }}">
                                        @endif
                                    @else
                                        <span class="text-warning">Бонусы выплачиваются только одобренным клиентам</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10"><p align="center"><br />Список пуст</p></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="default-panel-footer">
                    <div class="pull-right">
                        <div style="display: inline-block">
                            <select name="actions" class="form-control selectpicker" data-width="200px">
                                <option value="save">Сохранить</option>
                                <option value="delete">Удалить</option>
                            </select>
                        </div>
                        <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>



    <form method="post" class="form-horizontal" action="?id={{ $item->id }}">
        @csrf
        <input type="hidden" name="actions" value="winners">
        <div class="modal fade" id="winner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Выбрать победителей</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="code" class="control-label col-md-6">Кол-во победителей</label>
                            <div class="col-md-6">
                                <input style="margin-bottom: 10px" placeholder="Выберите кол-во победителей" name="count_winner" type="text" class="form-control" value="1">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Выбрать</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection
