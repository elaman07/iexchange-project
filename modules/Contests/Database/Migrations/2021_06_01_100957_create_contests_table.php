<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contests', function (Blueprint $table) {
            $table->id();
            $table->integer('id_manager')->default(0);
            $table->integer('status')->default(0);
            $table->string('name')->nullable();
            $table->string('duration')->nullable();
            $table->integer('max_limit_user')->default(0);
            $table->integer('is_manual_bank')->default(0);

            $table->string('bank_base')->default(0);
            $table->string('bank')->default(0);

            // Курсы
            $table->integer('id_code_currency')->default(0);
            $table->string('code_name')->nullable();
            $table->string('code_sign')->nullable();
            $table->integer('code_position')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contests');
    }
}
