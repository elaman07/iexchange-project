<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contests_users', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->default(0);
            $table->integer('id_monitoring')->default(0);
            $table->integer('status')->default(0);
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('link')->nullable();

            // Бонус
            $table->float('bonus')->default(0);
            $table->integer('id_contest')->default(0);
            $table->string('code_sign_bonus')->nullable();
            $table->string('code_name_bonus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contests_users');
    }
}
