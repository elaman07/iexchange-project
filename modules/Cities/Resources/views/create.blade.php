@extends('admin.layouts.app')

@section('title', 'Добавить новый город')

@section('breadcrumbs', Breadcrumbs::render('cities::create'))

@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/cities', true) }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group">
                <label class="col-sm-2 control-label">Добавить город</label>
                <div class="col-sm-10">
                    <div class="tab-content">

                        <div class="form-group col-md-3 multi-language-form-group">
                            <div class="control-label-br">
                                <div layout="row">
                                    <div>Название города</div>
                                    <span flex></span>
                                    <ul class="nav nav-pills lang-tabs">
                                        @foreach(config('app.form_lang') as $key => $item)
                                            <li @if($item['active'] == true) class="active" @endif>
                                                <a class="lang-tabs-link" data-toggle="tab" href="#{{ $key }}">
                                                    {{ $key }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @foreach(config('app.form_lang') as $key => $item)
                                <div id="{{ $key }}" style="padding-left: 0;" class="tab-pane fade in @if($item['active'] == true) active @endif">
                                    <input type="text" name="name{{$item['field']}}" class="form-control">
                                </div>
                            @endforeach
                        </div>


                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group col-md-3">
                            <div class="control-label-br">Обозначение для XML</div>
                            <input type="text" name="designation_xml" class="form-control" id="designation_xml" required>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-3">
                            <div class="control-label-br">{{ __('admin-tools.menu.status') }}</div>
                            <select name="status" class="form-control selectpicker">
                                <option value="0">{{ __('admin-tools.menu.un_active') }}</option>
                                <option value="1">{{ __('admin-tools.menu.active') }}</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.menu.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/cities', true) }}">{{ __('admin-tools.menu.back') }}</md-button>
        </div>
    </form>
@endsection
