@extends('admin.layouts.app')

@section('title', 'Изменить город '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('cities::edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/cities/'.$item->id, true) }}">
        <div class="default-panel-body">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="profit" class="col-sm-2 control-label">Обновить город</label>
                <div class="col-sm-10">
                    <div class="form-group col-md-3 multi-language-form-group">
                        <div class="control-label-br">
                            <div layout="row">
                                <div>{{ __('admin-tools.menu.name') }}</div>
                                <span flex></span>
                                <ul class="nav nav-pills lang-tabs">
                                    @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                                        <li @if($form_lang['active'] == true) class="active" @endif>
                                            <a class="lang-tabs-link" data-toggle="tab" href="#name-{{ $form_lang_key }}">
                                                {{ $form_lang_key }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @foreach(config('app.form_lang') as $form_lang_key => $form_lang)
                            <div id="name-{{ $form_lang_key }}" style="padding-left: 0;" class="tab-pane fade in @if($form_lang['active'] == true) active @endif">
                                <input type="text" name="name{{$form_lang['field']}}" class="form-control" value="{{ $item->getTranslation('name', $form_lang_key) }}" id="name{{$form_lang['field']}}">
                            </div>
                        @endforeach
                    </div>

                        <div class="clearfix"></div>
                        <hr />

                        <div class="form-group col-md-3">
                            <div class="control-label-br">Обозначение для XML</div>
                            <input type="text" name="designation_xml" class="form-control" id="slug" value="{{ $item->designation_xml }}" required>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-3">
                            <div class="control-label-br">{{ __('admin-tools.menu.status') }}</div>
                            <select name="status" class="form-control selectpicker">
                                <option value="0" @if($item->status == 0) selected @endif>{{ __('admin-tools.menu.un_active') }}</option>
                                <option value="1" @if($item->status == 1) selected @endif>{{ __('admin-tools.menu.active') }}</option>
                            </select>
                        </div>
                    </div>
            </div>
        </div>


        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">{{ __('admin-tools.menu.save') }}</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/cities', true) }}">{{ __('admin-tools.menu.back') }}</md-button>
        </div>
    </form>

@endsection
