@extends('admin.layouts.app')

@section('title', 'Города')

@section('breadcrumbs', Breadcrumbs::render('cities::index'))

@section('top-block')

    <md-button ng-href="cities/create" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить город</span>
    </md-button>
@endsection


@section('no-block-content')
    <div class="x_panel">

        <div class="x_title x_title_filter">
            <a class="collapsed md-icon-button" data-toggle="collapse" href="#showFilters" layout="row">
                <h2><i class="fa fa-fw fa-filter"></i>  {{ __('Фильтры') }}</h2>
                <div flex=""></div>
                <div class="heading-right-button">
                    <span class="if-collapsed"><i class="fa fa-fw fa-angle-down"></i></span>
                    <span class="if-not-collapsed"><i class="fa fa-fw fa-angle-up"></i></span>
                </div>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content collapse" id="showFilters" style="margin-top: 0;">
            <form class="form-horizontal" method="get" action="?">
                <input type="hidden" name="send_action" value="on">

                <div class="row task-filter-body">
                    <div class="padding-20">
                        <div class="task-filter-content" layout="row" layout-align="space-between stretch">

                            <div flex="50">
                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('Заголовок') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ is_filter_search($filter, 'name') }}" >
                                        <div class="form-check">
                                            <input class="form-check-input" id="checkbox_name" value="1" type="checkbox" name="checkbox_name" @if(isset($filter['checkbox_name'])) checked @endif>
                                            <label class="form-check-label" for="checkbox_name">{{ __('Точное совпадение') }}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">{{ __('Обозначение XML') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="designation_xml"
                                               value="{{ is_filter_search($filter, 'designation_xml') }}" >
                                        <div class="form-check">
                                            <input class="form-check-input" id="designation_xml" value="1" type="checkbox" name="checkbox_designation_xml" @if(isset($filter['checkbox_designation_xml'])) checked @endif>
                                            <label class="form-check-label" for="designation_xml">{{ __('Точное совпадение') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div flex="50">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <md-button type="submit" class="md-raised md-primary">{{ __('Сохранить фильтры') }}</md-button>
                    <md-button ng-href="?" class="md-raised md-warn">{{ __('Очистить фильтры') }}</md-button>
                </div>
            </form>
        </div>
    </div>


    <div class="x_panel new-x_panel_wrapper">
        <div class="x_title">
            <h2>{{ __('Города') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table class="table table-border-2">
                <thead>
                <tr>
                    <th>Заголовок</th>
                    <th>Обозначение XML</th>
                    <th>Статус</th>
                    <th>Дата обновления</th>
                    <th></th>
                </tr>

                </thead>
                <tbody>

                @if(count($cities) > 0)
                    @foreach($cities as $city)
                        <tr>
                            <th>
                                <a href="{{ admin_base_path('/modules/cities/'.$city->id.'/edit', true) }}">
                                    {{ $city->name }} &nbsp;<i class="fal fa-pencil"></i>
                                </a>
                            </th>
                            <th>
                                {{ $city->designation_xml }}
                            </th>
                            <td>
                                @if($city->status == 0)
                                    <span class="text-danger">{{ __('Отключен') }}</span>
                                @else
                                    <span class="text-success">{{ __('Включен') }}</span>
                                @endif
                            </td>
                            <td>
                                <div class=""> {{ \Illuminate\Support\Carbon::parse($city->updated_at)->translatedFormat('d M Y H:i') }}</div>
                                <small class="text-muted">{{ \Illuminate\Support\Carbon::parse($city->updated_at)->diffForHumans() }}</small>
                            </td>

                            <td style="width: 100px;">
                                <md-button class="md-icon-button" ng-href="{{ admin_base_path('/modules/cities/'.$city->id.'/destroy', true)  }}">
                                    <i class="fa fa-fw fa-trash text-danger"></i>
                                </md-button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10"><p align="center"><br />{{ __('Список пуст') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
    </div>
@endsection
