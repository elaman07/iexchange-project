<?php

namespace Modules\Cities\Http\Controllers;

use App\Models\GeoCountryList;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Modules\Cities\Entities\CitiesModel;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $cities = CitiesModel::filter($request->all())->get();

        // Удаляем ненужные ключи из массива
        if($request->has('send_action')) {
            $conditions = Arr::except($request->all(),['send_action']);
            return \redirect()->route('cities.index', $conditions);
        }

        $filter = $request->all();
        return view('cities::index', compact('cities', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('cities::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'designation_xml' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }
            $options['designation_xml'] = $request->has('designation_xml') ? $request->get('designation_xml') : '';
            $options['status'] = $request->has('status') ? $request->get('status') : 0;

            CitiesModel::create($options);

            flash('Город успешно добавлен', ['alert alert-success']);
            return redirect(
                admin_base_path('/modules/cities/create', true)
            );
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(int $id)
    {
        $item = CitiesModel::findOrFail($id);
        return view('cities::edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $menu = CitiesModel::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'designation_xml' => 'required'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back();
        } else {

            $options = [];

            foreach (config('app.form_lang') as $key => $item) {
                $options['name'][$key] = $request->get('name'.$item['field']);
            }
            $options['designation_xml'] = $request->has('designation_xml') ? $request->get('designation_xml') : '';
            $options['status'] = $request->has('status') ? $request->get('status') : 0;

            $menu->update($options);

            flash('Город успешно обновлен', ['alert alert-success']);
            return redirect(admin_base_path('/modules/cities', true));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {

        if(config('admin.is_demo_mode') == true) {
            flash('Нельзя редактировать в demo версии', ['alert alert-danger']);
            return redirect()->back();
        }

        $item = CitiesModel::findOrFail($id);

        flash('Город '.$item->name.' успешно удален', ['alert alert-success']);

        $item->delete();

        return redirect()->route('cities.index');
    }
}
