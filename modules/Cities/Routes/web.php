<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => admin_base_path('/modules/', true), 'middleware' => admin_middleware_route()], function()
{
    Route::get('/cities/{id}/destroy', 'CitiesController@destroy');
    Route::resource('cities', 'CitiesController');
});
