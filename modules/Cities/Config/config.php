<?php

return [
    'name' => 'Cities',

    'admin-menu' => [
        'rule' => 'admin_access_global_control',
        'url' =>  admin_base_path('/modules/cities'),
        'name' => 'Города'
    ]
];
