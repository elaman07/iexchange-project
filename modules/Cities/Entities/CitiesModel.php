<?php

namespace Modules\Cities\Entities;

use App\Common\Support\HasTranslations;
use App\Models\DirectionExchange;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class CitiesModel extends Model
{
    use HasFactory, HasTranslations, Filterable;

    protected $table = 'cities';

    protected $fillable = [
        'name',
        'status',
        'designation_xml',
        'created_user_id',
        'updated_user_id',

    ];

    public $translatable  = [
        'name',
    ];

    public function modelFilter() {
        return $this->provideFilter(\App\Models\Filters\CitiesModelFilter::class);
    }

}
