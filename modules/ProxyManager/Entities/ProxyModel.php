<?php

namespace Modules\ProxyManager\Entities;


use App\Models\Requisites;
use Illuminate\Database\Eloquent\Model;


/**
 * Modules\ProxyManager\Entities\ProxyModel
 *
 * @property int $id
 * @property string|null $address
 * @property string|null $auth
 * @property int $id_user
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $user_agent
 * @property-read \Illuminate\Database\Eloquent\Collection|Requisites[] $requisites
 * @property-read int|null $requisites_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereAuth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProxyModel whereUserAgent($value)
 * @mixin \Eloquent
 */
class ProxyModel extends Model
{
    protected $table = 'proxies_payment';

    protected $fillable = [
        'id_user',
        'address',
        'auth',
        'user_agent'
    ];

    public function requisites() {
        return $this->hasMany(Requisites::class,'id_proxy','id');
    }
}
