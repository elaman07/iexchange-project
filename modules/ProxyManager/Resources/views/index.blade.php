@extends('admin.layouts.app')

@section('title', 'Proxy менеджер')

@section('top-block')
    <md-button ng-href="{{ admin_base_path('/modules/proxy_manager/create') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить прокси</span>
    </md-button>

@endsection

@section('breadcrumbs', Breadcrumbs::render('proxymanager::index'))

@section('content')
    @csrf
    <table class="table table-border-2">
        <thead>
        <tr>
            <th>Дата Создания</th>
            <th>Адрес</th>
            <th>Авторизация</th>
            <th></th>
        </tr>

        </thead>
        <tbody>

        @if(count($proxies) > 0)
            @foreach($proxies as $proxy)
                <tr>
                    <th>{{ $proxy->created_at }}</th>
                    <td>{{ $proxy->address }}</td>
                    <td>{{ $proxy->auth }}</td>
                    <td style="width: 200px;">
                        <div class="col-md-6">
                            <a href="{{ admin_base_path('modules/proxy_manager/'.$proxy->id.'/edit') }}" class="btn btn-info btn-sm">Изменить</a>
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'DELETE', 'route' => ['proxy_manager.destroy', $proxy->id] ]) !!}
                            {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="10"><p align="center"><br />Список пуст</p></td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
