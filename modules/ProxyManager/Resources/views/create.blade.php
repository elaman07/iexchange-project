@extends('admin.layouts.app')

@section('title','Добавить прокси')

@section('breadcrumbs', Breadcrumbs::render('proxymanager::create'))


@section('content')
    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/proxy_manager') }}">
        <div class="default-panel-body">
            @csrf
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Адрес</label>
                <div class="col-sm-5">
                    <input type="text" name="address" placeholder="IP:PORT" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Авторизация</label>
                <div class="col-sm-5">
                    <input type="text" name="auth" placeholder="LOGIN:PASSWORD" class="form-control">
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">User Agent</label>
                <div class="col-sm-5">
                    <input type="text" name="user_agent" placeholder="Proxy iEXExchanger/v1" class="form-control">
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Сохранить</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/proxy_manager') }}">Назад</md-button>
        </div>
    </form>
@endsection
