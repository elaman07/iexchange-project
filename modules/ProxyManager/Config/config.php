<?php

return [
    'name' => 'ProxyManager',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/proxy_manager'),
        'name' => 'Proxy менеджер'
    ],
];
