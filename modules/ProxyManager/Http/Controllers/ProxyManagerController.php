<?php

namespace Modules\ProxyManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\ProxyManager\Entities\ProxyModel;

class ProxyManagerController extends Controller
{
    /**
     * Список proxy
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $proxies = ProxyModel::all();
        return view('proxymanager::index', compact('proxies'));
    }

    /**
     * Добавить proxy
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('proxymanager::create');
    }

    /**
     * Обработка и добавление proxy
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            ProxyModel::create([
                'address' => $request->get('address'),
                'auth' => ($request->has('auth') ? $request->get('auth') : null),
                'user_agent' => ($request->has('user_agent') ? $request->get('user_agent') : null),
                'id_user' => $request->user()->id
            ]);

            flash('Proxy успешно добавлен', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Форма редактирования proxy
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $proxy = ProxyModel::findOrFail($id);
        return view('proxymanager::edit', compact('proxy'));
    }

    /**
     * Обработка и обновление proxy
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item = ProxyModel::findOrFail($id);

            $item->update([
                'address' => $request->get('address'),
                'auth' => ($request->has('auth') ? $request->get('auth') : null),
                'user_agent' => ($request->has('user_agent') ? $request->get('user_agent') : null),
                'id_user' => $request->user()->id
            ]);

            flash('Proxy успешно обновлен', ['alert alert-success']);
            return redirect()->back();
        }
    }

    /**
     * Удаление proxy
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $item = ProxyModel::findOrFail($id);

        if ($item->requisites()->count() > 0) {
            flash('К proxy привязаны реквизиты, удаление невозможно', ['alert alert-danger']);
            return redirect()->back();
        }

        $item->delete();

        flash('Proxy успешно удалена', ['alert alert-success']);
        return redirect()->back();
    }
}
