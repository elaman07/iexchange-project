<?php

namespace Modules\CurrentOrderId\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\{DB, Validator};

class CurrentOrderIdController extends Controller
{
    /**
     * Текущий ID заявки
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('currentorderid::index');
    }

    /**
     * Обработка запроса
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|numeric'
        ]);

        if ($validator->fails())
        {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            DB::update("ALTER TABLE tasks AUTO_INCREMENT = {$request->get('order_id')};");
            flash('Новый ID заявки успешно установлен', ['alert alert-success']);
            return redirect()->back();
        }
    }
}
