@extends('admin.layouts.app')

@section('title', __('currentorderid::messages.title'))

@section('breadcrumbs', Breadcrumbs::render('currentorderid::order_id'))

@section('no-block-content')
    <div class="x_panel">
        <div class="x_title" style="border: none;margin-top: 10px;">
            <h2>{{ __('currentorderid::messages.title') }}</h2>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <div class="systemsettings">
                <div class="table-responsives">
                    <form action="?" method="POST" class="form-horizontal">
                        @csrf
                        <table class="table table-2 table-striped">
                            <tr>
                                <td class="col-xs-6 col-sm-6 col-md-7">
                                    <h6 class="media-heading text-semibold">{{ __('currentorderid::messages.title') }}</h6>
                                    <span class="text-muted text-size-small hidden-xs">{!! __('currentorderid::messages.description') !!}</span>
                                </td>
                                <td class="col-xs-6 col-sm-6 col-md-5">
                                    <input type="text" class="form-control" id="order_id" name="order_id" value="" placeholder="{{ __('currentorderid::messages.title_hint') }}">
                                </td>
                            </tr>
                        </table>

                        <div class="modal-footer">
                            <md-button type="submit" class="md-raised md-success">{{ __('currentorderid::messages.save') }}</md-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
