<?php

return [
    'name' => 'CurrentOrderId',

    'existing-category' => [
        'category_name' => 'admin-menu.categories.admin-layout__sections__order.categories',
        'category_value' => [
            'rule' => ['admin_tasks', 'admin_other_permissions'],
            'name' => 'currentorderid::messages.admin-menu',
            'url' => admin_base_path('/applications/currentorderid')
        ]
    ]
];
