<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => admin_base_path('/modules/', true), 'middleware' => admin_middleware_route(['permission:admin_access_cloudflare', 'demo-route'])], function() {
    Route::resource('cloudflare', 'CloudflareController');
});
