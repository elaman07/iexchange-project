<?php

return [
    'name' => 'Cloudflare',

    'admin-menu' => [
        'rule' => 'admin_merchant',
        'url' =>  admin_base_path('/modules/cloudflare'),
        'name' => 'Управление Cloudflare'
    ]
];
