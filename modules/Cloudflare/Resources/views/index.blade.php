@extends('admin.layouts.app')

@section('title', __('cloudflare::messages.title'))

@section('top-block')
    <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-cog text-primary"></md-icon>
        <span>{{ __('cloudflare::messages.setting') }}</span>
    </md-button>
@endsection

@section('breadcrumbs', Breadcrumbs::render('cloudflare::index'))

@section('no-block-content')



    <div class="x_panel">
        <div class="x_title">
            <h2>{{ __('cloudflare::messages.add_rule') }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content panel-body">
            <form action="?add_rule" method="post" class="form-horizontal">
                @csrf
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('cloudflare::messages.write_ip') }}</label>
                    <div class="col-md-3 col-sm-3">
                        <input class="form-control width-350" type="text" name="ip_address" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('cloudflare::messages.write_ip_dia') }}</label>
                    <div class="col-md-3 col-sm-3">
                        <input class="form-control width-350" type="text" name="ip_address_range" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('cloudflare::messages.mode') }}</label>
                    <div class="col-md-2">
                        <select class="form-control selectpicker" name="mode">
                            <option value="whitelist">{{ __('cloudflare::messages.whitelist') }}</option>
                            <option value="blacklist">{{ __('cloudflare::messages.blacklist') }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-3">{{ __('cloudflare::messages.description') }}:</label>
                    <div class="col-md-10 col-sm-9">
                        <textarea class="form-control" style="width: 350px" rows="5" name="notes"></textarea>
                    </div>
                </div>

                <div class="text-right">
                    <md-button class="md-primary md-raised" type="submit">{{ __('cloudflare::messages.save') }}</md-button>
                </div>

            </form>

        </div>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2>@yield('title')</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <table id="default-table" class="table table-border-2">
                <thead>
                <tr>
                    <th>{{ __('cloudflare::messages.value') }}</th>
                    <th>{{ __('cloudflare::messages.mode') }}</th>
                    <th>{{ __('cloudflare::messages.description') }}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($rules) > 0)
                    @foreach($rules as $rule)
                        <tr>
                            <td>{{ $rule->configuration->value }}</td>

                            <td>
                                @if($rule->mode == 'whitelist')
                                    <div class="label label-success">{{ __('cloudflare::messages.whitelist') }}</div>
                                @elseif($rule->mode == 'block')
                                    <div class="label label-danger">{{ __('cloudflare::messages.block') }}</div>
                                @endif
                            </td>

                            <td>{{ $rule->notes }}</td>

                            <td>
                                <md-button title="Удалить" href="cloudflare/?delete={{ $rule->id }}" class=" md-icon-button" >
                                    <i class="text-danger fa fa-trash"></i>
                                </md-button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10"><p align="center"><br />{{ __('cloudflare::messages.list_is_empty') }}</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">{{ __('cloudflare::messages.setting_connection') }}</span>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('cloudflare::messages.email_account') }}</label>
                            <div class="col-md-6">
                                <input type="email" name="cloudflare_email" class="form-control" value="{{ config('cloudflare.connection.email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('cloudflare::messages.api_key') }}</label>
                            <div class="col-md-6">
                                <input type="text" name="cloudflare_api" class="form-control" value="{{ config('cloudflare.connection.api_key') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">{{ __('cloudflare::messages.zone_id') }}</label>
                            <div class="col-md-6">
                                <input type="text" name="cloudflare_zone_id" class="form-control" value="{{ config('cloudflare.zone_id') }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">{{ __('cloudflare::messages.save') }}</md-button>
                        <md-button type="button" data-dismiss="modal">{{ __('cloudflare::messages.cancel') }}</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

