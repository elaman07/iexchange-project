<?php

return [
    'title' => 'Управление Cloudflare',
    'clear_cache' => 'Очистить кэш',
    'setting' => 'Настройка',
    'add_rule' => 'Добавить правила',
    'write_ip' => 'Введите IP-адрес',
    'write_ip_dia' => 'Введите IP-адрес (Диапазон)',
    'mode' => 'Применение',
    'whitelist' => 'Белый список',
    'blacklist' => 'Черный список',
    'description' => 'Описание',
    'value' => 'Значение',
    'block' => 'Заблокирован',
    'setting_connection' => 'Настройка соединения',
    'email_account' => 'E-mail аккаунта',
    'api_key' => 'API Ключ',
    'zone_id' => 'Zone ID',
    'save' => 'Сохранить',
    'list_is_empty' => 'Ничего не найдено',
    'cancel'  => 'Назад'
];
