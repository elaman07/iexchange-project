<?php

namespace Modules\Cloudflare\Http\Controllers;

use App\Http\Controllers\Controller;
use Brotzka\DotenvEditor\DotenvEditor;
use Illuminate\Http\Request;

class CloudflareController extends Controller
{
    /**
     * Список настроек
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     * @throws \Brotzka\DotenvEditor\Exceptions\DotEnvException
     */
    public function index(Request $request)
    {
        // Ограничитель в демо версии
        iex_demo_version_limitation();

        $rules = [];
        if(!empty(config('cloudflare.connection.email')) and !empty(config('cloudflare.connection.api_key')) and !empty(config('cloudflare.zone_id')))
        {
            $data = new \Cloudflare\API\Endpoints\AccessRules(
                iex_cloudflare_api()
            );

            // Добавление новых правил
            if($request->has('add_rule'))
            {
                $config = new \Cloudflare\API\Configurations\AccessRules();

                if($request->has('ip_address') and !empty($request->get('ip_address'))) {
                    $config->setIP($request->get('ip_address'));
                }

                if($request->has('ip_address_range') and !empty($request->get('ip_address_range'))) {
                    $config->setIPRange($request->get('ip_address_range'));
                }

                $data->createRule(
                    config('cloudflare.zone_id'),
                    $request->get('mode'),
                    $config,
                    $request->has('notes') ? $request->get('notes') : null
                );

                flash('Правило добавлено', ['alert alert-success']);
                return redirect()->back();
            }

            if($request->has('delete')) {
                $data->deleteRule(
                    config('cloudflare.zone_id'),
                    $request->get('delete')
                );

                flash('Правило успешно удалена', ['alert alert-success']);
                return redirect()->back();
            }

            try {
                $rules = $data->listRules(config('cloudflare.zone_id'), '', '', '','', 1,100)->result;
            }catch (\Exception $exception) {
                $rules = [];
                flash('Cloudflare не настроен', ['alert alert-danger']);
            }

        }

        return view('cloudflare::index', [
            'rules' => $rules
        ]);
    }

    public function store(Request  $request)
    {
        if($request->has('action') and $request->action == 'settings')
        {
            $env = new DotenvEditor();
            $env->changeEnv([
                'CLOUDFLARE_EMAIL'         =>  ($request->has('cloudflare_email') ? $request->get('cloudflare_email') : null),
                'CLOUDFLARE_APIKEY'        =>  ($request->has('cloudflare_api') ? $request->get('cloudflare_api') : null),
                'CLOUDFLARE_ZONE_ID'       =>  ($request->has('cloudflare_zone_id') ? $request->get('cloudflare_zone_id') : null),
            ]);

            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }

    }
}
