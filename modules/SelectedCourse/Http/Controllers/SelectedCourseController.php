<?php

namespace Modules\SelectedCourse\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\GroupParserExchange;
use Illuminate\Http\Request;
use Modules\SelectedCourse\Entities\SelectedCourse;

class SelectedCourseController extends Controller
{

    /**
     * Дополнительные фильтры
     *
     * @var array
     */
    protected $allowFiltered = [
        'is_enabled_top_selected_courses'
    ];


    /**
     * Главная страница избранных курсов
     */
    public function index()
    {
        $lists = SelectedCourse::orderBy('sorting')->paginate(20);

        return view('selectedcourse::index', compact('lists'));
    }

    /**
     * Форма добавления нового курса
     */
    public function create()
    {
        $parsers = GroupParserExchange::where('status','=', 1)->get();
        return view('selectedcourse::create', compact('parsers'));
    }

    /**
     * Обработка и добавление курса
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Доп. настройки
        if($request->has('action') and $request->action == 'settings')
        {
            // Обновление конфига
            $array = [];
            foreach ($this->allowFiltered as $item) {
                if($request->has($item) and is_array($request->get($item))) {
                    $array[$item] = implode(',', $request->get($item));
                } else {
                    $array[$item] = ($request->has($item) ? $request->get($item): null);
                }
            }
            iEXSetting($array);
            flash('Настройки успешно сохранены', ['alert alert-success']);
            return redirect()->back();
        }


        if($request->has('actions'))
        {
            foreach ($request->get('service_name') as $key => $item)
            {
                SelectedCourse::find($key)->update([
                    'name'  =>  ($request->name[$key] ?? null),
                    'service_name'  =>  ($request->service_name[$key] ?? null),
                    'status'  =>  ($request->status[$key] ?? 0),
                ]);
            }

            flash('Данные обновлены', ['alert alert-success']);
            return redirect()->route('selectedcourse.index');
        }

        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required'],
            'id_crypto_parser' => ['required', 'exists:parser_exchange,id'],
            'number_format' =>  ['required', 'numeric'],
            'service_name'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            SelectedCourse::create([
                'name'  =>  $request->get('name'),
                'service_name' => $request->get('service_name'),
                'id_crypto_parser'  =>  $request->get('id_crypto_parser'),
                'status'    =>  ($request->has('status') ? $request->get('status') : 0),
                'number_format' =>  ($request->has('number_format') ? $request->get('number_format') : 0),
            ]);

            flash('Избранный курс успешно добавлен', ['alert alert-success']);
            return redirect()->route('selectedcourse.index');
        }
    }

    /**
     * Форма редактирования избранных курсов
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = SelectedCourse::find($id);
        $parsers = GroupParserExchange::where('status','=', 1)->get();

        return view('selectedcourse::edit', compact('parsers', 'item'));
    }

    /**
     * Обработка и обновление курса
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Request $request)
    {
        $item = SelectedCourse::find($id);
        $validator = \Validator::make($request->all(), [
            'name'  =>  ['required'],
            'id_crypto_parser' => ['required', 'exists:parser_exchange,id'],
            'number_format' =>  ['required', 'numeric'],
            'service_name'  =>  ['required']
        ]);

        if ($validator->fails()) {
            flash($validator->messages()->first(), ['alert alert-danger']);
            return redirect()->back()->withInput();
        } else {

            $item->update([
                'name'  =>  $request->get('name'),
                'service_name' => $request->get('service_name'),
                'id_crypto_parser'  =>  $request->get('id_crypto_parser'),
                'status'    =>  ($request->has('status') ? $request->get('status') : 0),
                'number_format' =>  ($request->has('number_format') ? $request->get('number_format') : 0),
            ]);

            flash('Избранный курс успешно обновлен', ['alert alert-success']);
            return redirect()->route('selectedcourse.index');
        }
    }

    /**
     * Удаление избранного курса
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        $item = SelectedCourse::find($id);
        $item->delete();

        flash('Избранный курс успешно удален', ['alert alert-success']);
        return redirect()->route('selectedcourse.index');
    }

    /**
     * Сортировка избранных курсов
     */
    public function sorting()
    {
        $faq = SelectedCourse::orderBy('sorting')->get();

        return view('selectedcourse::sorting', [
            'faqs' => $faq
        ]);
    }
}
