<?php

namespace Modules\SelectedCourse\Providers;

use Diglactic\Breadcrumbs\Breadcrumbs;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SelectedCourseServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'SelectedCourse';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'selectedcourse';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerBreadcrumbs();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), 'modules-config.'.$this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register breadcrumbs
     */
    public function registerBreadcrumbs()
    {
        Breadcrumbs::for('selectedcourse::index', function ($breadcrumbs) {
            $breadcrumbs->parent('admin.plugins');
            $breadcrumbs->push('Список избранных курсов',  admin_base_path('/modules/selectedcourse'));
        });

        Breadcrumbs::for('selectedcourse::create', function ($breadcrumbs) {
            $breadcrumbs->parent('selectedcourse::index');
            $breadcrumbs->push('Добавить избранный курс',  admin_base_path('/modules/selectedcourse'));
        });

        Breadcrumbs::for('selectedcourse::edit', function ($breadcrumbs, $item) {
            $breadcrumbs->parent('selectedcourse::index');
            $breadcrumbs->push('Изменить избранный курс '.$item->name,  admin_base_path('/modules/selectedcourse/'.$item->id.'/edit'));
        });

        Breadcrumbs::for('selectedcourse::sorting', function ($breadcrumbs) {
            $breadcrumbs->parent('selectedcourse::index');
            $breadcrumbs->push('Сортировка избранных курсов',  admin_base_path('/modules/selectedcourse/sorting'));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
