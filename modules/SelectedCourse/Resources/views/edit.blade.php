@extends('admin.layouts.app')

@section('title','Изменить избранный курс '.$item->name)

@section('breadcrumbs', Breadcrumbs::render('selectedcourse::edit', $item))

@section('content')

    <form class="form-horizontal" method="post" action="{{ admin_base_path('/modules/selectedcourse/'.$item->id) }}">
        @csrf
        @method('PUT')
        <div class="default-panel-body">
            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Название</label>
                <div class="col-sm-5">
                    <input type="text" name="name" placeholder="Укажите название" value="{{$item->name}}" class="form-control" required>
                    <div class="text-muted" style="font-size: 12px;margin-top: 10px">Пример: <b>BTC/USD</b></div>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Выберите из списка курс</label>
                <div class="col-sm-5">
                    <select class="selectpicker show-tick" name="id_crypto_parser" data-live-search="true">
                        <option value="0"  selected>-- Не указано --</option>
                        @foreach($parsers as $group)
                            <optgroup label="{{$group->name}}">
                                @foreach($group->parser_exchange_enabled as $value)
                                    <option value="{{$value->id}}" @if($item->id_crypto_parser == $value->id) selected @endif>{{$value->name}} ({{$value->value}} → {{$value->summa}})</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Сервис</label>
                <div class="col-sm-5">
                    <input type="text" name="service_name" value="{{ $item->service_name }}" placeholder="Укажите название сервиса откуда парсится курс" class="form-control" required>
                    <div class="text-muted" style="font-size: 12px;margin-top: 10px">Пример: <b>Binance</b></div>
                </div>
            </div>

            <hr />

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Знаков, после запятой</label>
                <div class="col-sm-5">
                    <input type="text" name="number_format" value="{{ $item->number_format }}" class="form-control" required>
                </div>
            </div>

            <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                <label for="sign" class="col-sm-2 control-label">Статус</label>
                <div class="col-sm-5">
                    <select class="selectpicker" name="status">
                        <option value="0" @if($item->status == 0) selected @endif>Не активен</option>
                        <option value="1" @if($item->status == 1) selected @endif>Активен</option>
                    </select>
                </div>
            </div>

        </div>

        <div class="default-panel-footer default-panel-footer-right">
            <md-button class="md-raised md-primary" type="submit">
                <md-icon md-font-icon="fa fa-floppy-o"></md-icon>&nbsp;&nbsp;Обновить</md-button>
            <md-button ng-href="{{ admin_base_path('/modules/selectedcourse') }}">Назад</md-button>
        </div>
    </form>
@endsection
