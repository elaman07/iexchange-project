@extends('admin.layouts.app')

@section('title','Избранные курсы')

@section('top-block')
    <md-button ng-href="{{ admin_base_path('/modules/selectedcourse/create') }}" class="btn-float" layout="column">
        <md-icon md-font-icon="fa fa-plus text-primary"></md-icon>
        <span>Добавить курс</span>
    </md-button>

    @can('admin_settings')
        <md-button data-toggle="modal" data-target="#settings" class="btn-float" layout="column">
            <md-icon style="color: #303f9f" md-font-icon="fa fa-cog"></md-icon>
            <span style="color: #303f9f">Настройки</span>
        </md-button>
    @endcan
@endsection

@section('breadcrumbs', Breadcrumbs::render('selectedcourse::index'))


@section('content')
    <form method="post" action="?">
        @csrf
        <table class="table table-border-2">
            <thead>
            <tr>
                <th>Дата создания</th>
                <th>Название</th>
                <th>Курс валюты</th>
                <th>Сервис</th>
                <th>Статус</th>
                <th>Действие</th>
            </tr>

            </thead>
            <tbody>
            @if(count($lists) > 0)
                @foreach($lists as $list)
                    <tr>
                        <th scope="row">{{ $list->created_at }}</th>
                        <td>
                            <input style="width: 200px" type="text" name="name[{{$list->id}}]" class="form-control" value="{{$list->name}}">
                        </td>
                        <td>
                            @if(isset($list->parser_exchange))
                                {{$list->parser_exchange->value_default}} → {{$list->parser_exchange->summa_default}}
                            @else
                                <div class="text-danger">курс удален</div>
                            @endif
                        </td>
                        <td>
                            <input style="width: 250px" type="text" name="service_name[{{$list->id}}]" class="form-control" value="{{$list->service_name}}">
                        </td>
                        <td>
                            <div class="material-switch">
                                <input id="SwitchOptionPrimary{{$list->id}}" name="status[{{$list->id}}]" type="checkbox" value="1" @if($list->status == 1) checked @endif />
                                <label for="SwitchOptionPrimary{{$list->id}}" class="label-success"></label>
                            </div>

                        </td>
                        <td style="width: 150px">
                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/modules/selectedcourse/'.$list->id.'/edit') }}">
                                <i class="fa fa-fw fa-pencil"></i>
                            </md-button>

                            <md-button class="md-icon-button" ng-href="{{ admin_base_path('/modules/selectedcourse/'.$list->id.'/delete')  }}">
                                <i class="fa fa-fw fa-close text-danger"></i>
                            </md-button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4"><p align="center"><br />Список пуст</p></td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="default-panel-footer">
            <div class="pull-right">
                <div style="display: inline-block">
                    <select name="actions" class="form-control selectpicker" data-width="200px">
                        <option value="save">Сохранить</option>
                    </select>
                </div>
                <md-button type="submit" class="md-primary md-raised">Выполнить</md-button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>

    <!-- Модальное окно для настроек -->
    <div class="modal fade" id="settings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <form class="form-horizontal" method="post" action="?">
                <input type="hidden" name="action" value="settings">
                @csrf
                <div class="modal-content">
                    <div class="modal-header ui-dialog-titlebar">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="ui-dialog-title" id="myModalLabel">Настройка избранных курсов</span>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-md-8">Включить избранные курсы</label>
                            <div class="col-md-4">
                                <div class="material-switch switch-input-1">
                                    <input id="is_enabled_top_selected_courses" name="is_enabled_top_selected_courses" type="checkbox" value="1" @if(iEXSetting('is_enabled_top_selected_courses') == 1) checked @endif />
                                    <label for="is_enabled_top_selected_courses" class="label-primary"></label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <md-button type="submit" class="md-primary md-raised">Сохранить</md-button>
                        <md-button type="button" data-dismiss="modal">Отмена</md-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
