<?php

namespace Modules\SelectedCourse\Entities;


use App\Models\ParserExchange;
use Illuminate\Database\Eloquent\Model;

class SelectedCourse extends Model
{
    protected $table = 'selected_courses';

    protected $fillable = [
        'id_crypto_parser',
        'name',
        'service_name',
        'number_format',
        'status',
        'sorting'
    ];

    public function parser_exchange() {
        return $this->belongsTo(ParserExchange::class,'id_crypto_parser');
    }
}
