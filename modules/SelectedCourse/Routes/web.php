<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => admin_base_path('/modules', true)], function() {
    Route::get('/selectedcourse/sorting', 'SelectedCourseController@sorting')->middleware(['permission:admin_selected_course']);
    Route::get('/selectedcourse/{id}/delete', 'SelectedCourseController@destroy')->middleware(['permission:admin_selected_course']);
    Route::resource('selectedcourse', 'SelectedCourseController')->middleware(['permission:admin_selected_course']);
});
