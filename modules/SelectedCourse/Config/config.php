<?php

return [
    'name' => 'SelectedCourse',

    'admin-menu' => [
        'rule' => 'admin_selected_course',
        'name' => 'Избранные курсы',

        // Подкатегория
        'child' => [
            ['url' => admin_base_path('/modules/selectedcourse'), 'name' => 'admin-layout.sections.list_selected_course'],
            ['url' => admin_base_path('/modules/selectedcourse/sorting'), 'name' => 'admin-layout.sections.sorting_selected_course'],
        ]
    ]
];
