<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'telegram-bot-api' => [
        'token'     => env('TELEGRAM_BOT_TOKEN', 'YOUR BOT TOKEN HERE'),
        'channel_id' => env('TELEGRAM_CHANNEL_ID', 'DEFAULT')
    ],

    'nexmo' => [
        'key' => env('NEXMO_API_KEY', 'YOUT_NEXMO_API_KEY'),
        'secret' => env('NEXMO_API_SECRET', 'YOUT_NEXMO_API_KEY'),
        'sms_from' => env('NEXMO_API_SMS_FROM', 'YOUT_NEXMO_API_KEY'),
    ],

    'smscru' => [
        'login'  => env('SMSC_LOGIN', 'YOUT_SMSC_LOGIN'),
        'secret' => env('SMSC_SECRET', 'YOUT_SMSC_SECRET'),
        'sender' => env('SMSC_SENDER', 'YOUT_SMSC_SENDER'),
        'extra'  => [
            // any other API parameters
            // 'tinyurl' => 1
        ],
    ],

    'sms_ru' => [
        'api_id' => env('SMS_API_ID', null)
    ],

    
    'vkontakte' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'google' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'yandex' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'facebook' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'tumblr' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' =>  ''
    ],

    'coinbase' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'github' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'yahoo' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'medium' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'twitter' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'paypal' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'pinterest' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'discord' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'dribbble' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'linkedin' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'stripe' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'stackexchange' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'slack' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'odnoklassniki' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'gitlab' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'dropbox' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'bitbucket' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'twitch' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'wordpress' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],

    'zendesk' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect' => ''
    ],
];