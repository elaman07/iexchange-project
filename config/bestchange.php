<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Архив Bestchange
   |--------------------------------------------------------------------------
   |
   | Полный путь для загрузки все курсов, обменников, валют из сервиса
   | bestchange, архивом "zip"
   |
   */
    'bestchange_zip'    =>  storage_path('app/bestchange/info.zip'),

    /*
    |--------------------------------------------------------------------------
    | Хранилище Bestchange
    |--------------------------------------------------------------------------
    |
    | Путь где будут храниться все разорхивированные данные сервиса bestchange
    | хранилище временное после записи данных в базу она будет очищена
    |
    */
    'bestchange_repository' =>  storage_path('app/bestchange/info'),
    'bestchange_currencies' =>  storage_path('app/bestchange/info/bm_cy.dat'),
    'bestchange_exchangers' =>  storage_path('app/bestchange/info/bm_exch.dat'),
    'bestchange_rates'      =>  storage_path('app/bestchange/info/bm_rates.dat'),
    'bestchange_codes'      =>  storage_path('app/bestchange/e-currency-codes'),
    'bestchange_cities'      =>  storage_path('app/bestchange/info/bm_cities.dat'),

    'bestchange_json' => storage_path('/app/bestchange/codes.json'),

];
