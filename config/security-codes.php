<?php

return [
    // Код подтверждения заявок через "Панель оператора"
    'order-confirm' => env('SECURITY_CODES_ORDER_CONFIRM'),
    // Код подтверждения для входа раздел "Панель оператора"
    'order-page' => env('SECURITY_CODES_ORDER_PAGE')
];
