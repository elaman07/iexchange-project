<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Пароль для защиты платежных данных
    |--------------------------------------------------------------------------
    |
    | Этот ключ используется службой iEX для защиты ключей платежных шлюзов
    | от посторонних глаз.
    |
    | Внимание: Настоятельно рекомендуем установить сложный пароль
    |
    */
    'password' => env('APP_PAYMENT_PASSWORD', null),

    /*
    |--------------------------------------------------------------------------
    | Доступные платежные шлюзы
    |--------------------------------------------------------------------------
    |
    | В этом параметре перечислен, список всех доступных мерчантов и выплаты
    |
    | Рекомендуется ничего не менять, для того чтобы платежные шлюзы работали
    | корректно.
    */
    'providers' => [

        // Доступные параметры для выплаты
        'payouts' => ['advcash','payeer', 'perfectmoney'],

        'validators' => [
            'errors' => [
                'is_merchant_log' => 'Включите проверку логов',
                'allow_ip_address' => 'Ограничение по IP адресу не указано',
                'is_check_api' => 'Отключена проверка платежа через API',
                'security_hash' => 'Секретный ключ не указан'
            ],

            'levels' => [
                'is_merchant_log' => 5,
                'allow_ip_address' => 4,
                'is_check_api' => 4,
                'security_hash' => 4
            ]
        ]
    ],


    /**
    |--------------------------------------------------------------------------
    | Колонка ID транзакции для платежных систем
    |--------------------------------------------------------------------------
    |
    | В данном массиве мы собираем список полей для получения ID транзакции
    |
    | !!! Работает для callback router !!!
    |
     */
    'callback_columns' => [
        'westwallet' => 'label',
        'fourbill'   => 'order_id',
        'advcash'    => 'ac_order_id',
        'paykassa'   => 'order_id',
        'perfectmoney' => 'PAYMENT_ID',
        'epaycore'  =>  'epc_order_id',
        'obmenka'   =>  'order_id',
        'payeer'    =>  'm_orderid',
        'webmoney'  =>  'LMI_PAYMENT_NO',
        'firekassa' =>  'order_id',
        'firekassawallet' =>  'order_id',
        'mpcvip'    =>  'order_id',
        'mpcvipform'    =>  'order_id',
        'alfabitpay' => 'order_id',
        'bitconce' => 'custom_id',
        'payport'   => 'order_id',
        'cryptoswap' => 'order_id',
        'aifory' => 'order_id'
    ],

    'enable_internal_account' => env('ENABLE_INTERNAL_ACCOUNT', false),

    'webmoney' => [
        'x19_list' => [
            1 => 'QIWI -> Webmoney',
            2 => 'Яндекс.Деньги -> Webmoney',
            3 => 'Webmoney -> QIWI',
            4 => 'Webmoney -> Яндекс.Деньги',
            5 => 'Наличные -> Webmoney',
            6 => 'Банковский счет -> Webmoney',
            7 => 'Банковская карта -> Webmoney',
            8 => 'Webmoney -> Наличные',
            9 => 'Webmoney -> Банковский счет',
            10 => 'Webmoney -> Банковская карта',
            11 => 'Webmoney -> Webmoney',
            12 => 'Webmoney -> Bitcoin',
            13 => 'Webmoney -> Bitcoin Cash',
            14 => 'Webmoney -> PayPal',
            15 => 'Webmoney -> Skrill (Moneybookers)',
            16 => 'Skrill (Moneybookers) -> Webmoney',
            17 => 'PayPal -> Webmoney',
            18 => 'EasyPay -> Webmoney',
            19 => 'Webmoney -> EasyPay'
        ]
    ]
];
