<?php
return [

    /**
     * Настройка соединения с CloudFlare
    */
    'connection' => [
        'email' => env('CLOUDFLARE_EMAIL', null),
        'api_key' => env('CLOUDFLARE_APIKEY', null)
    ],

    'zone_id' => env('CLOUDFLARE_ZONE_ID', null)
];