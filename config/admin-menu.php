<?php

/**
 * Меню админки
*/
return [
    'categories' => [
        // Основное
        'Основное' => [
            'headline' => [
                'title' => 'Обменник',
                'subtitle' => 'Основные разделы обменника'
            ],
            'line' => true, // Добавляем линию
            'icon' => 'fal fa-home fa-custom-theme',
            'rules' => ['order_debtors', 'currencies', 'currency_codes', 'currency_filters', 'admin_selected_course', 'your_course', 'payment_system', 'direction_exchange', 'additional_fields', 'user_discounts', 'reserves', 'course_designer', 'order_blacklist', 'order_whitelist', 'requisites_system'],
            // Основная категория
            'categories' => [
                [
                    'rule' => 'currencies',
                    'name' => 'Валюты',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/currency'), 'name' => 'Список валют'],
                        ['url' => admin_base_path('/basic/currency-templates'), 'name' => 'Шаблоны для валют'],
                        ['url' => admin_base_path('/basic/currency_fields'), 'name' => 'Доп. поля валют', 'rule' => 'additional_fields'],
                        ['url' => admin_base_path('/basic/currency-command'), 'name' => 'Быстрые команды'],
                        ['url' => admin_base_path('/basic/currency/archive'), 'name' => 'Архив валют'],
                        ['url' => admin_base_path('/basic/currency-labels'), 'name' => 'Метки для валют'],
                        [
                            'name' => 'Группы валют',
                            'subcategory' => [
                                ['url' => admin_base_path('/basic/currency-groups'), 'name' => 'Список групп'],
                                ['url' => admin_base_path('/basic/currency-groups/sorting'), 'name' => 'Сортировка групп'],
                            ]
                        ],
                        ['url' => admin_base_path('/basic/currency/sorting'), 'name' => 'Сортировка для резерва'],
                        ['url' => admin_base_path('/basic/currency/sorting_admin'), 'name' => 'Сортировка в админке'],
                        [
                            'name' => 'Уведомление для валют',
                            'subcategory' => [
                                ['url' => admin_base_path('/basic/currency-notification'), 'name' => 'Список уведомлений'],
                                ['url' => admin_base_path('/basic/currency-notification/sorting'), 'name' => 'Сортировка уведомлений'],
                            ]
                        ],
                        [
                            'name' => 'Логи',
                            'subcategory' => [
                                ['url' => admin_base_path('/basic/currency/logs'), 'name' => 'Лог валют'],
                            ]
                        ]
                    ]
                ], [
                    'rule' => 'currency_codes',
                    'name' => 'Коды валют',
                    'url' => admin_base_path('/basic/code_currency')
                ], [
                    'rule' => 'currency_filters',
                    'name' => 'Фильтры для валют',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/filter_currency'), 'name' => 'Список фильтров'],
                        ['url' => admin_base_path('/basic/filter_currency/create'), 'name' => 'Добавить фильтр'],
                        ['url' => admin_base_path('/basic/filter_currency/sorting'), 'name' => 'Сортировка фильтров']
                    ]
                ], [
                    'rule' => 'your_course',
                    'name' => 'Свой курс',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/your_course'), 'name' => 'Список курсов'],
                        ['url' => admin_base_path('/basic/your_course/create'), 'name' => 'Добавить курс']
                    ]
                ], [
                    'rule' => 'payment_system',
                    'name' => 'Платежные системы',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/payments'), 'name' => 'Список платежных систем'],
                        ['url' => admin_base_path('/basic/payments-explorer'), 'name' => 'Blockchain Explorer'],
                    ]
                ], [
                    'rule' => 'requisites_system',
                    'name' => 'Платежные реквизиты',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/requisites'), 'name' => 'Список реквизитов'],
                        ['url' => admin_base_path('/basic/requisites/create'), 'name' => 'Добавить реквизит'],
                        ['url' => admin_base_path('/basic/requisites/archive'), 'name' => 'Архив реквизитов'],
                        ['url' => admin_base_path('/basic/requisites-blacklist'), 'name' => 'Черный список реквизитов', 'role' => 'requisites_blacklist'],
                        ['url' => admin_base_path('/basic/requisites-groups'), 'name' => 'Группы'],
                        ['url' => admin_base_path('/basic/requisites-info-fields'), 'name' => 'Информационные поля'],
                        ['url' => admin_base_path('/basic/requisites-fields'), 'name' => 'Дополнительные поля'],
                        ['url' => admin_base_path('/basic/requisites-transit'), 'name' => 'Транзитные реквизиты'],
                        ['url' => admin_base_path('/basic/requisites-groups/sorting'), 'name' => 'Сортировка групп'],
                    ]
                ], [
                    'rule' => 'course_designer',
                    'name' => 'Конструктор курсов',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/course_designer'), 'name' => 'Список курсов'],
                        ['url' => admin_base_path('/basic/course_designer/favorites'), 'name' => 'Избранные направления'],
                        ['url' => admin_base_path('/basic/course_designer/types'), 'name' => 'Типы направлений'],
                        ['url' => admin_base_path('/basic/course_designer/commission'), 'name' => 'Комиссии направлений'],
                    ]
                ], [
                    'rule' => 'direction_exchange',
                    'name' => 'Направление обмена',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/direction_exchange'), 'name' => 'Направление обмена'],
                        ['url' => admin_base_path('/basic/direction-exchange-groups'), 'name' => 'Группы направлений'],
                        ['url' => admin_base_path('/basic/direction-exchange-modes'), 'name' => 'Режимы направлений'],
                        ['url' => admin_base_path('/basic/direction_exchange/fields'), 'name' => 'Доп. поля'],
                        ['url' => admin_base_path('/basic/direction-requisites'), 'name' => 'Реквизиты для направлений'],
                        ['url' => admin_base_path('/basic/direction_exchange/templates'), 'name' => ' Шаблоны для направлений'],
                        ['url' => admin_base_path('/basic/direction_exchange/unpaid_item'), 'name' => 'Автоматическое удаление неоплаченных заявок'],
                        ['url' => admin_base_path('/basic/direction-exchange-notification'), 'name' => 'Уведомление направлений'],
                        ['url' => admin_base_path('/basic/direction_exchange/sorting'), 'name' => 'Сортировка направлений'],
                        ['url' => admin_base_path('/basic/direction_exchange/sorting-tariffs'), 'name' => 'Сортировка валют для тарифов'],
                        ['url' => admin_base_path('/basic/direction-exchange-commission'), 'name' => 'Сгруппированная комиссия'],
                        ['url' => admin_base_path('/basic/direction_exchange/trashed'), 'name' => 'Удаленные направления'],
                        ['url' => admin_base_path('/basic/direction_exchange/logs'), 'name' => 'Лог направлений'],
                        [
                            'name' => 'Корректировка цен',
                            'subcategory' => [
                                ['url' => admin_base_path('/basic/direction_exchange/min_price'), 'name' => 'Групповая корректировка'],
                                ['url' => admin_base_path('/basic/direction_exchange/price_adjustment'), 'name' => 'Общая корректировка'],
                            ]
                        ],
                    ]
                ], [
                    'rule' => 'user_wallets',
                    'name' => 'Счета пользователей',
                    'url' => admin_base_path('/basic/user_wallets')
                ], [
                    'rule' => 'order_blacklist',
                    'name' => 'Черный список',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/blacklist'), 'name' => 'Черный список'],
                        ['url' => admin_base_path('/settings?mid=bsblacklist'), 'name' => 'Черный список BestChange'],
                    ]
                ], [
                    'rule' => 'order_whitelist',
                    'name' => 'Белый список',
                    'url' => admin_base_path('/basic/whitelist')
                ], [
                    'rule' => 'reserves',
                    'name' => 'Корректировка резерва',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/basic/reserves'), 'name' => 'Список резервов'],
                        ['url' => admin_base_path('/basic/reserves/sorting'), 'name' => 'Сортировка резервов'],
                        ['url' => admin_base_path('/basic/reserves-groups'), 'name' => 'Группы'],
                        ['url' => admin_base_path('/basic/reserves-alerts'), 'name' => 'Оповещении'],
                        [
                            'name' => 'Резервы из файла',
                            'subcategory' => [
                                ['url' => admin_base_path('/basic/reserves-files'), 'name' => 'Список резервов'],
                                ['url' => admin_base_path('/basic/reserves-files-group'), 'name' => 'Группы'],
                            ]
                        ],
                    ]
                ]
            ]
        ],

        // Заявки
        'admin-layout__sections__order' => [
            'icon' => 'fa fa-shopping-basket fa-custom-theme',
            'rules' => ['admin_tasks', 'claims_payment'],
            'categories' => [
                [
                    'rule' => 'admin_tasks',
                    'name' => 'Заявки',
                    'url' => admin_base_path('/applications')
                ],[
                    'rule' => 'admin_tasks',
                    'name' => 'Этапы заявки',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/applications/application-steps'), 'name' => 'Список этапов'],
                        ['url' => admin_base_path('/applications/application-steps/sorting'), 'name' => 'Сортировка этапов']
                    ]
                ], [
                    'rule' => 'admin_tasks',
                    'name' => 'Отложенные заявки',
                    'color' => '#b2ba0f',
                    'url' => admin_base_path('/applications/postponed')
                ], [
                    'rule' => 'admin_tasks',
                    'name' => 'Избранные заявки',
                    'color' => '#ff8100',
                    'config' => 'is_enabled_favorites_order', // Настройка
                    'url' => admin_base_path('/applications/favorites')
                ], [
                    'rule' => 'admin_tasks',
                    'name' => 'Архивированные заявки',
                    'url' => admin_base_path('/applications/archive')
                ], [
                    'rule' => 'admin_order_trashed',
                    'name' => 'Удаленные заявки',
                    'color' => '#a0a0a0',
                    'config' => 'is_enabled_delete_order',
                    'url' => admin_base_path('/applications/trashed')
                ], [
                    'rule' => 'admin_tasks',
                    'name' => 'Спам заявки',
                    'color' => '#9694ff',
                    'config' => 'is_enabled_spam_order',
                    'url' => admin_base_path('/applications/spam')
                ], ['line' => true],
                [
                    'rule' => 'admin_tasks',
                    'name' => 'Логи заявок',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/applications/status-log'), 'name' => 'Лог статусов заявок'],
                        ['url' => admin_base_path('/applications/confirmation-log'), 'name' => 'Лог подтверждений'],
                        ['url' => admin_base_path('/applications/merchant-log'), 'name' => 'Лог мерчантов'],
                        ['url' => admin_base_path('/applications/autopayment-log'), 'name' => 'Лог автовыплат'],
                        ['url' => admin_base_path('/applications/checkpay-log'), 'name' => 'Лог проверки оплат'],
                        ['url' => admin_base_path('/applications/aml-log'), 'name' => 'AML лог'],
                    ]
                ], [
                    'rule' => 'admin_order_limits',
                    'name' => 'Лимиты для операторов',
                    'url' => admin_base_path('/applications/levels')
                ], [
                    'rule' => 'admin_tasks',
                    'name' => 'Список частых клиентов',
                    'url' => admin_base_path('/applications/buyers')
                ], [
                    'rule' => 'claims_payment',
                    'name' => 'Заявки на выплату',
                    'url' => admin_base_path('/applications/payment_bonuses')
                ], [
                    'rule' => 'admin_settings',
                    'name' => 'Статусы заявок',
                    'url' => admin_base_path('/applications/settings')
                ],
            ]
        ],

        // Мерчант и api
        'Мерчант и API' => [
            'line' => true, // Добавляем линию
            'icon' => 'fal fa-server fa-custom-theme',
            'rules' => ['admin_merchant'],
            'categories' => [
                [
                    'rule' => 'admin_merchant',
                    'url' => admin_base_path('/gateways/merchant'),
                    'name' => 'Список мерчантов'
                ],

                [
                    'rule' => 'admin_merchant',
                    'url' => admin_base_path('/gateways/autopayment'),
                    'name' => 'Список автовыплат'
                ],
                [
                    'rule' => 'admin_merchant',
                    'name' => 'Лог запросов',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/gateways/logs/event-merchant'), 'name' => 'Для мерчанта'],
                        ['url' => admin_base_path('/gateways/logs/event-autopayment'), 'name' => 'Для автовыплат'],
                        ['url' => admin_base_path('/gateways/logs/event-orders'), 'name' => 'Для ордеров'],
                    ]
                ]
            ]
        ],

        // Работа парсеров
        'Работа парсеров' => [
            'icon' => 'fal fa-robot fa-custom-theme',
            'rules' => ['admin_parser'],
            'categories' => [
                [
                    'rule' => 'admin_parser',
                    'name' => 'Работа парсеров',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/crypto/parser'), 'name' => 'Список пар'],
                        ['url' => admin_base_path('/crypto/all_parser'), 'name' => 'Все курсы из источников'],
                        ['url' => admin_base_path('/crypto/parser/sorting'), 'name' => 'Сортировка источников'],
                        ['url' => admin_base_path('crypto/api-keys'), 'name' => 'API Ключи'],
                        ['url' => admin_base_path('/crypto/parser/history'), 'name' => 'История курсов'],
                        ['url' => admin_base_path('/crypto/parser/logs'), 'name' => 'Лог запросов'],
                    ]
                ], [
                    'rule' => 'admin_parser',
                    'name' => 'BestChange парсер',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/crypto/bestchange'), 'name' => 'Список пар'],
                        ['url' => admin_base_path('/crypto/bestchange/history'), 'name' => 'История курсов'],
                        ['url' => admin_base_path('crypto/bestchange/settings'), 'name' => 'Настройки'],
                        ['url' => admin_base_path('/crypto/bestchange/log'), 'name' => 'Bestchange лог'],
                    ]
                ], [
                    'rule' => 'admin_parser',
                    'name' => 'Парсер конкурентов',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/crypto/competitors-parser'), 'name' => 'Список пар'],
                        ['url' => admin_base_path('/crypto/competitors-link'), 'name' => 'Источники'],
                        ['url' => admin_base_path('crypto/competitors-link/sorting'), 'name' => 'Сортировка источников'],
                        ['url' => admin_base_path('/crypto/competitors-parser/history'), 'name' => 'История курсов'],
                    ]
                ],[
                    'rule' => 'admin_parser',
                    'name' => 'Парсер из формулы',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/crypto/parser-formula'), 'name' => 'Список формул'],
                        ['url' => admin_base_path('/crypto/formula-coefficient'), 'name' => 'Коэффициент'],
                        ['url' => admin_base_path('/crypto/parser-formula/history'), 'name' => 'История курсов'],
                    ]
                ], [
                    'rule' => 'admin_parser',
                    'name' => 'Парсер из файла',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/crypto/file-parser'), 'name' => 'Список пар'],
                        ['url' => admin_base_path('/crypto/file-parser-group'), 'name' => 'Группы'],
                        ['url' => admin_base_path('crypto/file-parser-group/sorting'), 'name' => 'Сортировка групп'],
                    ]
                ]
            ]
        ],

        // Мерчант и api
        'admin-layout__sections__modules' => [],


        // Пользователи
        'Пользователи' => [
            'icon' => 'fal fa-user-alt fa-custom-theme',
            'rules' => ['admin_users'],
            'categories' => [
                [
                    'rule' => 'admin_users',
                    'name' => 'Список пользователей',
                    'url' => admin_base_path('/account/users')
                ], [
                    'rule' => 'admin_roles',
                    'name' => 'Список групп пользователей',
                    'url' => admin_base_path('/account/roles')
                ], [
                    'rule' => 'admin_roles',
                    'name' => 'Права доступа',
                    'url' => admin_base_path('/account/permissions')
                ], [
                    'rule' => 'admin_users',
                    'name' => 'История профилей',
                    'url' => admin_base_path('/account/users/history_profiles')
                ], [
                    'rule' => 'admin_users',
                    'name' => 'Лог авторизаций',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/account/logs_auth'), 'name' => 'Общий лог'],
                        ['url' => admin_base_path('/account/admin_logs_auth'), 'name' => 'Лог админпанели'],
                    ]
                ], [
                    'rule' => 'admin_blockip',
                    'name' => 'Фильтр по: IP или E-Mail',
                    'url' => admin_base_path('/account/block_ip')
                ]
            ],
        ],

        // Партнерам
        'Партнерам' => [
            'icon' => 'fal fa-handshake fa-custom-theme',
            'rules' => ['admin_affiliate_program'],
            'categories' => [
                [
                    'name' => 'Реферальная система',
                    'rule' => 'admin_affiliate_program',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/affiliate/referral'), 'name' => 'Реферальные программы'],
                        ['url' => admin_base_path('/affiliate/referral/statistics'), 'name' => 'Реферальная статистика'],
                        ['url' => admin_base_path('/affiliate/referral/transitions'), 'name' => 'Реферальные переходы'],
                        ['url' => admin_base_path('/affiliate/referral/transitions_exchange'), 'name' => 'Реферальные направления'],
                        ['url' => admin_base_path('/affiliate/referral/exchanges'), 'name' => 'Партнерские обмены'],
                        ['url' => admin_base_path('/affiliate/referral/exchanges_group'), 'name' => 'Сгруппированные обмены'],
                        ['url' => admin_base_path('/affiliate/referral/referrals'), 'name' => 'Список рефералов'],
                        ['url' => admin_base_path('/affiliate/referral/logs'), 'name' => 'Лог реферальной программы'],
                    ]
                ], [
                    'name' => 'Бонусная система',
                    'rule' => 'admin_affiliate_program',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/affiliate/bonus'), 'name' => 'Бонусные программы'],
                        ['url' => admin_base_path('/affiliate/bonus/exchanges'), 'name' => 'Бонусные обмены'],
                        ['url' => admin_base_path('/affiliate/bonus/logs'), 'name' => 'Не начисленные бонусы'],
                    ]
                ], [
                    'name' => 'Информация',
                    'rule' => 'admin_affiliate_program',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/affiliate/conditions/referral'), 'name' => 'Условия реферальной программы'],
                        ['url' => admin_base_path('/affiliate/conditions/cashback'), 'name' => 'Условия кэшбэка'],
                        ['url' => admin_base_path('/affiliate/conditions/monitoring'), 'name' => 'Условия для мониторингов'],
                    ]
                ], [
                    'name' => 'Настройки',
                    'rule' => 'admin_affiliate_program',
                    'url' => admin_base_path('/affiliate/settings')
                ],
            ]
        ],


        // Партнерам
        'Верификации' => [
            'icon' => 'fal fa-id-card fa-custom-theme',
            'rules' => ['admin_verification_card'],
            'categories' => [
                [
                    'name' => 'Верификация карт',
                    'rule' => 'admin_verification_card',
                    'url' => admin_base_path('/verifications/cards')
                ],
                [
                    'name' => 'Верификация личности',
                    'rule' => 'admin_verification_card',
                    'url' => admin_base_path('/verifications/accounts')
                ]
            ]
        ],

        // Настройки
        'Настройки' => [
            'headline' => [
                'title' => 'Настройки',
                'subtitle' => 'Настройка сайта, утилиты, аналитика'
            ],
            'line' => true,
            'icon' => 'fal fa-hammer fa-custom-theme',
            'rules' => ['admin_settings', 'admin_update_system'], //admin_backup
            'categories' => [
                [
                    'rule' => 'admin_settings',
                    'name' => 'Настройка системы',
                    'url' => admin_base_path('/settings')
                ], [
                    'rule' => 'admin_settings',
                    'name' => 'Почтовые и SMS события',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/settings/menu-templates/email-template'), 'name' => 'Почтовые шаблоны'],
                        ['url' => admin_base_path('/settings/menu-templates/type-events'), 'name' => 'Типы событий'],
                    ]
                ], [
                    'rule' => 'admin_settings',
                    'name' => 'Инструменты',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/settings/instruments/event_logs'), 'name' => 'Журнал событий'],
                      //  ['url' => admin_base_path('/backup'), 'name' => 'admin-layout.sections.backup', 'role' => 'admin_backup'],
                    ]
                ],[
                    'rule' => 'admin_update_system',
                    'name' => 'Обновление системы',
                    'url' => admin_base_path('/update_system')
                ],
            ]
        ],

        // Аналитика
        'Аналитика' => [
            'icon' => 'fal fa-analytics fa-custom-theme',
            'rules' => ['admin_analytics'],
            'categories' => [
                [
                    'rule' => 'admin_analytics',
                    'name' => 'Сотрудники',
                    'url' => admin_base_path('/analytics/employees')
                ], [
                    'rule' => 'admin_analytics',
                    'name' => 'Отчет по сотрудникам',
                    'url' => admin_base_path('/analytics/employees/report')
                ], [
                    'rule' => 'admin_analytics',
                    'name' => 'Список событий по резервам',
                    'url' => admin_base_path('/analytics/reserves')
                ], [
                    'rule' => 'admin_analytics',
                    'name' => 'Суммы обменов',
                    'url' => admin_base_path('/analytics/exchange')
                ],
            ]
        ],

        // Утилиты
        'Утилиты' => [
            'icon' => 'fal fa-folders fa-custom-theme',
            'rules' => ['admin_news', 'admin_status_job', 'admin_contact', 'admin_pages', 'admin_faq', 'admin_reviews', 'admin_social_auth', 'admin_export_courses', 'admin_scheduler', 'partners'],
            'categories' => [
                [
                    'rule' => 'admin_export_courses',
                    'name' => 'Экспорт курсов',
                    'url' => admin_base_path('/tools/generator_currency')
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Навигационное меню',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/menu'), 'name' => 'Список меню'],
                        ['url' => admin_base_path('/tools/menu/sorting'), 'name' => 'Сортировка меню'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Правила сайта',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/rules_pages'), 'name' => 'Список правил'],
                        ['url' => admin_base_path('/tools/rules_pages/sorting'), 'name' => 'Сортировка правил'],
                    ]
                ], [
                    'rule' => 'partners',
                    'name' => 'Партнеры',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/partners'), 'name' => 'Список партнеров'],
                        ['url' => admin_base_path('/tools/partners/create'), 'name' => 'Добавить партнера'],
                        ['url' => admin_base_path('/tools/partners/sorting'), 'name' => 'Сортировка партнеров'],
                    ]
                ], [
                    'rule' => 'admin_pages',
                    'name' => 'Страницы',
                    'url' => admin_base_path('/tools/pages')
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Уведомления',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/notification'), 'name' => 'Список уведомлений'],
                        ['url' => admin_base_path('/tools/notification/sorting'), 'name' => 'Сортировка уведомлений'],
                        ['url' => admin_base_path('/tools/live-notification'), 'name' => 'Live Уведомления'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Хранилище файлов',
                    'url' => admin_base_path('/tools/file-storages')
                ], [
                    'rule' => 'admin_social_auth',
                    'name' => 'Система авторизации',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/auth-system'), 'name' => 'Список сервисов'],
                        ['url' => admin_base_path('/tools/auth-system/sorting'), 'name' => 'Сортировка сервисов'],
                    ]
                ], [
                    'rule' => 'admin_reviews',
                    'name' => 'Отзывы',
                    'url' => admin_base_path('/tools/reviews')
                ], [
                    'rule' => 'admin_contact',
                    'name' => 'Контакты',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/contacts'), 'name' => 'Список контактов'],
                        ['url' => admin_base_path('/tools/contacts/sorting'), 'name' => 'Сортировка контактов'],
                        ['url' => admin_base_path('/tools/collaboration-pr'), 'name' => 'Сотрудничество и PR'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Преимущества',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/advantage'), 'name' => 'Список преимуществ'],
                        ['url' => admin_base_path('/tools/advantage/sorting'), 'name' => 'Сортировка преимуществ'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Статистика',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/statistics'), 'name' => 'Список записей'],
                        ['url' => admin_base_path('/tools/statistics/sorting'), 'name' => 'Сортировка статистики'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Ссылки на отзывы',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/links_reviews'), 'name' => 'Список ссылок'],
                        ['url' => admin_base_path('/tools/links_reviews_group'), 'name' => 'Группы для ссылок'],
                        ['url' => admin_base_path('/tools/links_reviews/sorting'), 'name' => 'Сортировка ссылок'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Ссылки на соц. сети',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/social-reviews'), 'name' => 'Список ссылок'],
                        ['url' => admin_base_path('/tools/social-reviews/sorting'), 'name' => 'Сортировка ссылок'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Ссылки для Footer',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/links_footers'), 'name' => 'Список ссылок'],
                        ['url' => admin_base_path('/tools/links_footers_group'), 'name' => 'Список групп']
                    ]
                ], [
                    'rule' => 'admin_news',
                    'name' => 'Новости',
                    'url' => admin_base_path('/tools/news')
                ], [
                    'rule' => 'admin_status_job',
                    'name' => 'Статус работы сервиса',
                    'child' => [
                        ['url' => admin_base_path('/tools/job'), 'name' => 'Настройки'],
                        ['url' => admin_base_path('/tools/job-schedule'), 'name' => 'Расписание']
                    ]
                ], [
                    'rule' => 'admin_faq',
                    'name' => 'Вопросы и ответы',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/faq'), 'name' => 'Список ответов'],
                        ['url' => admin_base_path('/tools/faq/sorting'), 'name' => 'Сортировка ответов'],
                        ['url' => admin_base_path('/tools/faq-category'), 'name' => 'Список категорий'],
                        ['url' => admin_base_path('/tools/faq-category/sorting'), 'name' => 'Сортировка категорий'],
                    ]
                ],[
                    'rule' => 'admin_other_permissions',
                    'name' => 'Баннеры',

                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/banners'), 'name' => 'Список баннеров'],
                        ['url' => admin_base_path('/tools/banners-button'), 'name' => 'Список кнопок'],
                        ['url' => admin_base_path('/tools/banners/sorting'), 'name' => 'Сортировка баннеров'],
                    ]
                ],[
                    'rule' => 'admin_other_permissions',
                    'name' => 'GEO IP',
                    // Подкатегория
                    'child' => [
                        ['url' => admin_base_path('/tools/geoip/settings'), 'name' => 'Настройки'],
                        ['url' => admin_base_path('/tools/geoip/countries'), 'name' => 'Список стран'],
                    ]
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Просмотр журнала логов',
                    'url' => admin_base_path('/tools/logs')
                ], [
                    'rule' => 'admin_other_permissions',
                    'name' => 'Лог E-mail уведомлений',
                    'url' => admin_base_path('/tools/logs-email')
                ]
            ]
        ],

        // API
        'API' => [
            'icon' => 'fal fa-router fa-custom-theme',
            'rules' => ['admin_api'],
            'categories' => [
                [
                    'rule' => 'admin_api',
                    'name' => 'Список API',
                    'url' => admin_base_path('/api')
                ], [
                    'rule' => 'admin_api',
                    'name' => 'Лог',
                    'url' => admin_base_path('/api/logs')
                ]
            ]
        ],
    ]
];
