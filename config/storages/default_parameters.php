<?php


return [
    'yandex' => [
        'driver' => '',
        'key' => '',
        'secret' => '',
        'endpoint' => '',
        'region' => '',
        'bucket' => '',
    ],

    's3' => [
        'driver' => '',
        'key' => '',
        'secret' => '',
        'endpoint' => '',
        'region' => '',
        'bucket' => '',
        'url' => '',
    ],

    's3_all' => [
        'driver' => '',
        'key' => '',
        'secret' => '',
        'endpoint' => '',
        'region' => '',
        'bucket' => '',
        'url' => '',
    ],

    'ftp' => [
        'driver' => '',
        'host'     => '',
        'username' => '',
        'password' => '',
        'port'     => ''
    ],
];
