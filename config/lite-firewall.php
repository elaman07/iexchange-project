<?php
return [

    // Enable / disable firewall
    'enabled' => env('FIREWALL_ENABLED', true),

    'whitelist' => [
        'admin' => [],
    ],

    'responses' => [
        'whitelist' =>  [
            'code'  =>  403,
            'message' => null,
            'redirect_to' => null
        ]
    ]
];