<?php

return [

    'currencies' => [
        'instruction' => [
            '[city]' => 'Город',
            '[country]' => 'Страна',
            '[direction]' => 'Направление',
            '[created_at]' => 'Дата создания заявки',
            '[rate]' => 'Курс обмена',
            '[in_amount]' => 'Сумма Отдаю',
            '[out_amount]' => 'Сумма Получаю',
            '[in_code]' => 'Код Отдаю',
            '[out_code]' => 'Код Получаю',
            '[in_currency]' => 'Валюта Отдаю',
            '[out_currency]' => 'Валюта Получаю',
            '[public_id]' => 'Public ID',
            '[order_id]' => 'ID Заявки',
            '[profit_percent]' => 'Процент прибыли'
        ]
    ],


    /**
     * BB Editor
    */
    'editor' => [
        '[order_id]' => 'ID Заявки',
        '[created_at]' => 'Дата создания',
        '[updated_at]' => 'Дата изменения',
        '[ip_address]' => 'IP Адрес',
        '[email]' => 'E-mail',
        '[income_amount]' => 'Сумма Отдаете',
        '[outcome_amount]' => 'Сумма Получаете',
        '[income_currency]' => 'Название валюты Отдаю',
        '[outcome_currency]' => 'Название валюты Получаю',
        '[course]' => 'Курс обмена',
        '[unique_code]' => 'Уникальный код',
        '[check_url]' => 'Ссылка на обмен',
        '[app_name]' => 'Название сайта',
        '[public_id]' => 'Public ID'
    ],

    'email_templates' => [
        'interface' => [
            '[default_email_from]' => 'E-Mail адрес по умолчанию (устанавливается в настройках)',
            '[site_name]' => 'Название сайта (устанавливается в настройках)',
            '[server_name]' => 'URL сервера (устанавливается в настройках)'
        ]
    ],


    'sms-list' => [
        'nexmo' =>  'Nexmo',
        'smscru' => 'SMSC',
        'smsru'    => 'SMSRu'
    ],

    'sms-fields' => [
        [
            'name' => 'Nexmo',
            'alias' => 'nexmo',
            'fields' => [
                ['key' => 'NEXMO_API_KEY', 'name' => 'Nexmo API Key', 'config' => 'services.nexmo.key'],
                ['key' => 'NEXMO_API_SECRET', 'name' => 'Nexmo API Secret', 'config' => 'services.nexmo.secret'],
                ['key' => 'NEXMO_API_SMS_FROM', 'name' => 'Nexmo Номер отправителя', 'config' => 'services.nexmo.sms_from'],
            ]
        ],
        [
            'name' => 'SMSC',
            'alias' => 'smscru',
            'fields' => [
                ['key' => 'SMSC_LOGIN', 'name' => 'Логин', 'config' => 'services.smscru.login'],
                ['key' => 'SMSC_SECRET', 'name' => 'Пароль', 'config' => 'services.smscru.secret'],
                ['key' => 'SMSC_SENDER', 'name' => 'Номер отправителя', 'config' => 'services.smscru.sender'],
            ]
        ],
        [
            'name' => 'SMS Ru',
            'alias' => 'smsru',
            'fields' => [
                ['key' => 'SMS_API_ID', 'name' => 'API ID', 'config' => 'services.sms_ru.api_id']
            ]
        ]
    ],

    'categories' => [
        'main' => [
            'line_break' => false,
            'route' => 'main',
            'icon' => 'fa-cogs',
            'title' => 'Настройки продукта'
        ],

        'interface' => [
            'line_break' => false,
            'route' => 'interface',
            'icon' => 'fa-layer-group',
            'title' => 'Настройки интерфейса'
        ],

        'images' => [
            'line_break' => false,
            'route' => 'images',
            'icon' => 'fa-images',
            'title' => 'Настройки изображения'
        ],


        'bsblacklist' => [
            'line_break' => true,
            'route' => 'bsblacklist',
            'icon' => 'fa-user',
            'title' => 'Черный список BestChange'
        ],

        'currencyreserve' => [
            'route' => 'currencyreserve',
            'icon' => 'fa-list-alt',
            'title' => 'Резервы и Валюты',
            'line_break' => true,
        ],

        'onlinechat' => [
            'route' => 'onlinechat',
            'icon' => 'fa-comment-lines',
            'title' => 'Онлайн чат'
        ],

        'logs' => [
            'line_break' => true,
            'route' => 'logs',
            'icon' => 'fa-grip-lines',
            'title' => 'Настройки логов'
        ],

        'messageservice' => [
            'route' => 'messageservice',
            'icon' => 'fa-phone',
            'title' => 'Служба сообщений'
        ],
        'telegramservice' => [
            'route' => 'telegramservice',
            'icon' => 'fa-telegram-plane',
            'type' => 'fab',
            'title' => 'Служба Telegram'
        ],

        'broadcastingservice' => [
            'route' => 'broadcastingservice',
            'icon' => 'fa-podcast',
            'title' => 'Служба Broadcasting'
        ]
    ],

    'categories_breadcrumb' => [
        'main' => [
            'line_break' => false,
            'route' => 'main',
            'icon' => 'fa-cogs',
            'title' => 'Настройки продукта'
        ],

        'interface' => [
            'line_break' => false,
            'route' => 'interface',
            'icon' => 'fa-layer-group',
            'title' => 'Настройки интерфейса'
        ],

        'images' => [
            'line_break' => false,
            'route' => 'images',
            'icon' => 'fa-images',
            'title' => 'Настройки изображения'
        ],



        'currencyreserve' => [
            'route' => 'currencyreserve',
            'icon' => 'fa-list-alt',
            'title' => 'Резервы и Валюты',
            'line_break' => true,
        ],

        'onlinechat' => [
            'route' => 'onlinechat',
            'icon' => 'fa-comment-lines',
            'title' => 'Онлайн чат'
        ],

        'messageservice' => [
            'route' => 'messageservice',
            'icon' => 'fa-phone',
            'title' => 'Служба сообщений'
        ],
        'telegramservice' => [
            'route' => 'telegramservice',
            'icon' => 'fa-telegram-plane',
            'type' => 'fab',
            'title' => 'Служба Telegram'
        ],

        'broadcastingservice' => [
            'route' => 'broadcastingservice',
            'icon' => 'fa-podcast',
            'title' => 'Служба Broadcasting'
        ]
    ],


    'updates' => [
        'main' => [
            'driver' => 'MainConfig',
            'env' => [
                'SESSION_COOKIE',
                'ERROR_REPORTING',
                'ENABLE_INTERNAL_ACCOUNT',
                'APP_URL',
                'APP_TIMEZONE',
                'APP_LOCALE',
                'BAN_CHEATER_USER',
                'ENABLE_CACHE',
                'CLEAR_CACHE_EXCHANGE',
                'CLEAR_CACHE_MINUTES',
                'CACHE_DRIVER',
                'ENABLE_CACHE_EXCHANGE',
                'ENABLE_CACHE_LAST_EXCHANGE',
                'CACHE_USER_STAT',
                'CACHE_ORDER_COUNT',
                'APP_ADMIN_PATH',
                'SESSION_SECURE_COOKIE',
                'LOGIN_MAX_ATTEMPTS',
                'LOGIN_DECAY_MINUTES',
                'ONE_SESSION_USER',
                'NOCAPTCHA_SITEKEY',
                'NOCAPTCHA_SECRET',
                'NOCAPTCHA_THEME',
                'MAIL_HOST',
                'MAIL_PORT',
                'MAIL_PASSWORD',
                'MAIL_FROM_ADDRESS',
                'MAIL_USERNAME',
                'MAIL_ENCRYPTION',
                'MAX_USERS',
                'SESSION_DOMAIN',
                'MAX_USER_DAY',
                'ANALYTICS_VIEW_ID',
                'BESTCHANGE_POSITION',
                'ENABLE_PANEL_OPERATOR',
                'SESSION_DRIVER',
                'REDIS_HOST',
                'REDIS_PORT',
                'REDIS_PASSWORD'
            ],
            'json' => [
                'is_firewall_enabled',
                'is_saved_user_stories',
                'is_dot_not_remember_data_order',
                'is_mail_notify_order_manager',
                'is_language_detection',
                'is_hidden_order_count',
                'is_enable_autogen_order_email',
                'interval_rates',
                'interval_rates_confirm_dialog',
                'is_allow_transfer_operator',
                'order_active_operator',
                'max_num_autopay_button',
                'is_order_auto_refund',
                'bestchange_blacklist_add',
                'is_freeze_scam_order',
                'is_blocked_spam_order',
                'allow_order_blacklist',
                'is_notify_order_restore',
                'is_language_deactivation',
                'is_notify_order_defer',
                'disable_check_display',
                'is_recount_to_merchant',
                'is_recount_to_merchant_notify',
                'is_extended_order_check',
                'displayed_statuses',
                'order_identify_newbie',
                'is_enabled_autopayment',
                'is_enabled_log_merchant',
                'is_enabled_log_status',
                'is_auto_archived',
                'is_disabled_order_merchant',
                'archived_statuses',
                'max_time_task',
                'lead_time_task',
                'order_priority',
                'allow_transfer_reserve',
                'give_transfer_reserve',
                'max_num_order_user',
                'max_num_order_user_hour',
                'max_num_order_user_day',
                'is_order_phone_number',
                'pos_order_phone_number',
                'pos_order_email',
                'type_cached_rates',
                'enable_cache_reviews_exchange',
                'enable_cache_tariffs',
                'enable_cache_contacts',
                'enable_cache_statistics',
                'enable_cache_footer',
                'enable_cache_notification',
                'enable_cache_advantages',
                'enable_cache_partners',
                'enable_cache_news',
                'enabled_cache_day_faq',
                'clear_cache_day_faq',
                'is_disabled_count_course_designer',
                'security_allowed_domains',
                'is_ip_throttle_key',
                'ip_change_control',
                'admin_allowed_ip',
                'captcha_restoring_password',
                'captcha_login_admin',
                'is_notify_change_reserve',
                'mail_order_withdrawal',
                'notify_change_status',
                'reg_multi_ip',
                'is_geo_ip',
                'is_notify_new_device',
                'is_user_successful_login',
                'is_user_verified_email',
                'is_user_email_unique',
                'is_lockout_auth',
                'is_write_client_log',
                'enable_user_auth_log',
                'is_user_password_history',
                'is_filter_username',
                'minimum_bonus_payout',
                'min_auto_withdrawal',
                'is_email_auto_user',
                'new_user_registration_cleanup_days',
                'is_verified_referral',
                'is_remember_login',
                'i_ordercheck_from_shot',
                'i_ordercheck_to_shot',
                'i_ordercheck_personal',
                'is_verified_payouts_bonus',
                'is_visible_disabled_directions',
                'is_enabled_google_analytics',
                'google_analytics',
                'yandex_metrika',
                'hide_exchange_guest',
                'is_enable_email_notification',
                'is_loading_save_filter_currency',
                'auto_register',
                'mail_from_name',
                'angular_phone_mask',
                'session_lifetime',
                'proxiesfilter_sources_ddos',
                'is_order_shot_black_list',
                'is_google_auth',
                'is_disabled_email_field_optional',
                'is_order_user_no_copydata',
                'is_security_captcha_type',
                'project_email_support',
                'email_addresses_admin',
                'client_id_type_for_order',
                'is_language_selected_user',
                'is_save_order_data_to_file',
                'type_instruction_merchant',
                'ids_currencies_account_my_wallets',
                'type_notify_email_new_order'
            ],

            'language' => [
                'sitename',
                'sitename_desc',
                'description',
                'keywords',
                'username_new_user',
            ]
        ],

        'interface' => [
            'driver' => 'InterfaceConfig',
            'env' => [
            ],


            'json' => [
                'type_view_field_exchange_amount',
                'type_view_language_format',
                'visible_description',
                'visible_my_order',
                'is_enable_footer',
                'count_last_exchange',
                'visible_partners',
                'count_news_exchange',
                'visible_news',
                'count_reviews_exchange',
                'visible_reviews',
                'visible_statistics',
                'visible_advantage',
                'is_enabled_top_selected_courses',
                'visible_last_exchange',
                'visible_popular_exchange',
                'count_popular_exchange',
                'visible_banner',
                'visible_banner_mobile',
                'interface_exchange',
                'exchange_fon_type',
                'exchange_fon_type_rules',
                'exchange_fon_file',
                'type_view_background',
                'logotype_text',
                'logotype_type',
                'logotype_text_mobile',
                'logotype_type_mobile',
                'is_gradient_text_color',
                'iex_interface_text_entry',
                'interface_regauth_text1',
                'interface_regauth_text2',
                'is_style_agreement_rules',
                'is_style_agreement_checkbox',
                'is_exchange_rules_remember_checkbox',
                'iex_interface_style_mainpage',
                'iex_interface_toolbar_row_menu',
                'iex_interface_design_exchange',
                'iex_interface_dark_design_exchange',
                'visible_main_header_title',
                'iex_interface_design_font_family',
                'iex_interface_header_style',
                'iex_interface_dynamic_colors'
            ],

            'language' => [
                'interface_regauth_text1',
                'interface_regauth_text2',
                'welcome_title',
                'welcome_description',
                'tech_breach_title',
                'tech_breach_text',
                'main_title_header',
                'main_value_header'
            ]
        ],
        'currencyreserve' => [
            'driver' => 'CurrencyReserveConfig',
            'env' => [],
            'json' => [
                'type_selected_currency',
                'allow_filter_currency',
                'allow_filter_currency_select',
                'is_hide_currency_reserve',
                'is_enabled_reserve_threshold',
                'is_auto_reserves_report',
                'unlimited_reserve',
                'is_not_exceed_amount_reserve',
                'is_reserves_rounding',
                'block_visible_reserve',
                'template_block_reserve',
                'count_template_block_reserve_2',
                'is_enabled_out_course_reserve',
                'is_reserve_shortage_notice',
                'currency_display_type_dynamic',
                'count_individual_num_currency',
                'is_disabled_group_filter_direction',
                'is_enabled_convert_exchange_to_usd',
                'ids_currencies_reserves_home_view',
                'currency_exchange_selected_default',
                'currency_exchange_selected_price',
                'is_mobile_hidden_reserve_out',
                'is_web_hidden_reserve_out'
            ]
        ],

        'logs' => [
            'driver' => 'LogsConfig',
            'env' => [

            ],
            'json' => [
                'settings_log_partners_transition_clear',
                'settings_log_checkpayment_clear',
                'settings_log_autopayment_clear',
                'settings_log_old_status_clear',
                'settings_log_confirmation_clear',
                'settings_log_direction_e_clear',
                'settings_log_unverified_card',
                'settings_log_parser_http'
            ]
        ],

        'images' => [
            'driver' => 'ImagesConfig',
            'env' => [
                'IMAGE_DRIVER',
            ],
            'json' => [
                'storage_disk_payment_system',
                'storage_disk_verification_card',
                'storage_disk_news',
                'images_converted_to',
                'image_quality',
                'images_max_up_side'
            ]
        ],

        'broadcastingservice' => [
            'driver' => 'broadcastingService',
            'env' => [
                'BROADCAST_DRIVER',
                'PUSHER_APP_ID',
                'PUSHER_APP_KEY',
                'PUSHER_APP_SECRET',
                'PUSHER_APP_CLUSTER'
            ],
            'json' => [
                'is_enabled_module_socket',
                'iex_modal_success_order',
                'iex_snackbar_success_order'
            ],
            'language' => [
                's_order_notify_text',
            ]
        ],

        'messageservice' => [
            'driver' => 'messageServiceConfig',
            'env' => [
                'SMS_DRIVER',
                'NEXMO_API_KEY',
                'NEXMO_API_SECRET',
                'NEXMO_API_SMS_FROM',
                'SMSC_LOGIN',
                'SMSC_SECRET',
                'SMSC_SENDER'
            ],
            'json' => [
                'enable_sms_notification',
                'enable_admin_sms_notification',
                'sms_phone_number_system',
                'sms_new_order',
                'sms_notify_success_order',
                'sms_notify_success_payouts',
                'sms_message_prefix_website',
                'sms_message_success_order',
                'sms_message_success_payouts',
                'sms_message_success_order_admin'
            ]
        ],

        'onlinechat' => [
            'driver' => 'onlineChatConfig',
            'env' => [
            ],

            'json' => [
                'is_enabled_online_chat',
                'is_enable_order_online_chat',
                'is_enabled_online_chat_tg_notify',
                'chat_type',
                'chat_displayed_statuses',
                'jivosite_type_message',
                'jivosite_info_user'
            ],

            'language' => [
                'chat_app_id',
                'jivosite_text_message'
            ]
        ],

        'telegramservice' => [
            'driver' => 'telegramServiceConfig',
            'env' => [
                'ENABLE_TELEGRAM',
                'TELEGRAM_CHANNEL_ID',
                'TELEGRAM_BOT_TOKEN',
                'TELEGRAM_BOT_NAME',

                'ADMIN_TELEGRAM_BOT_TOKEN',
                'ADMIN_TELEGRAM_BOT_NAME',
                'TELEGRAM_SECURITY_PREFIX',

                'NOTICE_TELEGRAM_BOT_NAME',
                'NOTICE_TELEGRAM_BOT_TOKEN'
            ],
            'json' => [
                'is_tg_send_stat',
                'enable_tg_notify_operator',
                'is_notify_failed_autopayment',
                'is_notify_telegram_blocked_client',
                'telegram_order_verification_card',
                'telegram_order_withdrawal',
                'telegram_to_shot_notification',
                'interface_to_shot_notification',
                'telegram_failed_update_courses',
                'is_successful_admin_auth',
                'is_failed_admin_auth',
                'is_notify_first_confirm_blockchain',
                'is_order_notify_auto_payment',
                'is_notify_order_telegram_client',
                'telegram_view_notification',
                'telegram_type_notification',
                'tg_stat_chat_id',
                'telegram_chat_id',
                'enable_telegram_courses_fail',
                'is_notify_order_telegram_postponed',
                'is_notify_process_create_order',
                'is_enable_telegram_exchange',
                'is_telegram_bot_home',
                'telegram_bot_banned_ids',
                'telegram_bot_banned_type',
                'is_enabled_telegram_bot_block',
                'telegram_block_style',
                'telegram_bot_client_about',
                'telegram_bot_client_review',
                'telegram_bot_client_is_create_review',
                'telegram_bot_client_support',
                'telegram_bot_client_start',
                'link_telegram_bot_block'
            ],

            'language' => [
                'telegram_block_title',
                'telegram_block_description',
                'telegram_block_style',
                'telegram_block_button',
            ],
        ],

        'bsblacklist' => [
            'driver' => 'bsBlackListConfig',
            'env' => [
            ],
            'json' => [
                'bestchange_api_categories',
                'bestchange_bs_data',
                'is_bestchange_blacklist',
                'bestchange_api_id',
                'bestchange_api_key',
                'bestchange_api_columns',
                'bestchange_api_type',
                'bestchange_allow_order'
            ]
        ]
    ]
];
