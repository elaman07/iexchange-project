<?php


return [

    // Статусы перед удалением заявки
    'status_reject' =>  [
        0   =>  'Платёж не поступил',
        1   =>  'Возврат',
        2   =>  'Повторная заявка (дубликат)',
        3   =>  'Ваша заявка признана мошеннической'
    ],

    // Проверить оплату
    'check_payment' =>  [
        'Bitcoin', 'BitcoinCash', 'Litecoin', 'Qtum', 'Dash', 'Monero', 'Zcash', 'Ripple'
    ],

    // Комиссии (Для Binance)
    'commission'    =>  [
        'bitcoin_cash'      =>  0.001,
        'ripple'            =>  0.25,
        'cardano'           =>  1,
        'zcash'             =>  0.0002,
        'nem'               =>  0.05,
        'eos'               =>  0.05
    ]
];