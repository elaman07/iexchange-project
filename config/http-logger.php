<?php

return [

    /*
     * Профиль журнала, который определяет, должен ли быть зарегистрирован запрос.
     * Следует реализовать `LogProfile`.
     */
    'log_profile' => \App\Services\HttpLogger\LogNonGetRequests::class,

    /*
     * Средство записи журнала использовалось для записи запроса в журнал.
     * Следует реализовать `LogWriter`.
     */
    'log_writer' => \App\Services\HttpLogger\DefaultLogWriter::class,

    /*
     * Отфильтруйте поля тела, которые никогда не будут зарегистрированы.
     */
    'except' => [
        'password',
        'password_confirmation',
    ],

];
