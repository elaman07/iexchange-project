<?php


return [
    /**
     * ADVCash - Параметры по умолчанию
    */
    'advcash' => [
        'merchant' => [
            'sci_account_email' => '',
            'sci_name' => '',
            'sci_password' => '',
            'api_name' => '',
            'api_secret' => ''
        ],

        'pay' => [
            'api_account_email' => '',
            'api_name' => '',
            'api_secret' => '',
            'api_r_wallet' => '',
            'api_u_wallet' => '',
            'api_e_wallet' => '',
            'api_h_wallet' => ''
        ]
    ],

    /**
     * PerfectMoney - Параметры по умолчанию
     */
    'perfectmoney' => [

        'merchant' => [
            'account_id' => '',
            'account_password' => '',
            'account_usd' => '',
            'account_eur' => '',
            'account_gold' => '',
            'account_btc' => '',
            'account_name' => '',
            'alternate_phrase' => ''
        ],

        'pay' => [
            'account_id' => '',
            'account_password' => '',
            'account_usd' => '',
            'account_eur' => '',
            'account_gold' => '',
            'account_btc' => ''
        ]
    ],

    /**
     * Payeer
     */
    'payeer' => [
        'merchant' => [
            'merchant_id' => '',
            'secret_key' => '',
        ],

        'pay' => [
            'account_id' => '',
            'api_id' => '',
            'api_key' => ''
        ]
    ],

    /**
     * BlockIo
     */
    'blockio' => [
        'merchant' => [
            'secret_pin' => '',
            'api_key_btc' => '',
            'api_key_ltc' => '',
            'api_key_doge' => '',
            'confirm_btc' => '',
            'confirm_ltc' => '',
            'confirm_doge' => ''
        ],

        'pay' => [
            'secret_pin' => '',
            'api_key_btc' => '',
            'api_key_ltc' => '',
            'api_key_doge' => ''
        ]
    ],

    /**
     * ePayCore
    */
    'epaycore' => [
        'merchant' => [
            'merchant_id' => '',
            'merchant_key' => ''
        ],

        'pay' => [
            'api_id' => '',
            'api_secret' => '',
            'account_usd' => '',
            'account_uah' => '',
            'account_rub' => ''
        ]
    ],

    'obmenka' => [

        'merchant' => [
            'kassa_id' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'kassa_id' => '',
            'secret_key' => ''
        ]
    ],

    'nixmoney' => [
        'merchant' => [
            'account_password' => '',
            'account_email' => '',
            'account_name' => '',
            'account_usd' => '',
            'account_eur' => '',
            'account_btc' => '',
            'account_ltc' => '',
            'account_ppc' => '',
            'account_ftc' => '',
            'account_crt' => '',
            'account_gbc' => '',
            'account_doge' => '',
        ],

        'pay' => [
            'account_password' => '',
            'account_email' => '',
            'account_usd' => '',
            'account_eur' => '',
            'account_btc' => '',
            'account_ltc' => '',
            'account_ppc' => '',
            'account_ftc' => '',
            'account_crt' => '',
            'account_gbc' => '',
            'account_doge' => '',
        ]
    ],

    'adgroup' => [

        'merchant' => [
            'client_id' => '',
            'client_secret' => '',
            'password' => ''
        ],

        'pay' => [
            'client_id' => '',
            'client_secret' => '',
            'password' => '',
            'user_id' => ''
        ]
    ],

    'mpcvip' => [

        'merchant' => [
            'api_key' => '',
            'api_secret' => '',
            'user_agent' => ''
        ],

        'pay' => [
            'api_key' => '',
            'api_secret' => '',
            'user_agent' => ''
        ]
    ],


    'mpcvipform' => [

        'merchant' => [
            'api_key' => '',
            'api_secret' => '',
        ]
    ],

    'aifory' => [

        'merchant' => [
            'api_key' => '',
            'api_secret' => '',
            'user_agent' => ''
        ],

        'pay' => [
            'api_key' => '',
            'api_secret' => '',
            'user_agent' => ''
        ]
    ],

    'merchant001' => [

        'merchant' => [
            'api_token' => ''
        ],

        'pay' => [
            'api_token' => ''
        ]
    ],

    'webmoney' => [

        'merchant' => [
            'wmz_purse' => '',
            'wmr_purse' => '',
            'wme_purse' => '',
            'wmu_purse' => '',
            'wmb_purse' => '',
            'wmy_purse' => '',
            'wmx_purse' => '',
            'wmk_purse' => '',
            'wml_purse' => '',
            'wmh_purse' => '',
            'wmz_secret_key' => '',
            'wmr_secret_key' => '',
            'wme_secret_key' => '',
            'wmu_secret_key' => '',
            'wmb_secret_key' => '',
            'wmy_secret_key' => '',
            'wmx_secret_key' => '',
            'wmk_secret_key' => '',
            'wml_secret_key' => '',
            'wmh_secret_key' => '',
            'wmz_x20_secret_key' => '',
            'wmr_x20_secret_key' => '',
            'wme_x20_secret_key' => '',
            'wmu_x20_secret_key' => '',
            'wmb_x20_secret_key' => '',
            'wmy_x20_secret_key' => '',
            'wmx_x20_secret_key' => '',
            'wmk_x20_secret_key' => '',
            'wml_x20_secret_key' => '',
            'wmh_x20_secret_key' => ''
        ],

        'pay' => [
            'wmid' => '',
            'key_pass' => '',
            'wmz_purse' => '',
            'wmr_purse' => '',
            'wme_purse' => '',
            'wmu_purse' => '',
            'wmb_purse' => '',
            'wmy_purse' => '',
            'wmx_purse' => '',
            'wmk_purse' => '',
            'wml_purse' => '',
            'wmh_purse' => ''
        ]
    ],

    'westwallet' => [

        'merchant' => [
            'public_key' => '',
            'private_key' => ''
        ],

        'pay' => [
            'public_key' => '',
            'private_key' => ''
        ],

        'options' => [
            'priority_fee' => 'medium'
        ]
    ],

    'alfabitpay' => [

        'merchant' => [
            'api_key' => '',
            'master_key' => ''
        ],

        'pay' => [
            'api_key' => '',
            'master_key' => ''
        ],

        'options' => [
        ]
    ],

    'whitebitcrypto' => [

        'merchant' => [
            'public_key' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'public_key' => '',
            'secret_key' => ''
        ]
    ],

    'whitebitcodes' => [

        'merchant' => [
            'public_key' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'public_key' => '',
            'secret_key' => ''
        ]
    ],

    'fourbill' => [

        'merchant' => [
            'point_id' => '',
            'acc_id' => '',
            'wallet_id' => '',
            'service_id' => '',
            'token_id' => ''
        ],

        'pay' => [
            'point_id' => '',
            'acc_id' => '',
            'wallet_id' => '',
            'service_id' => '',
            'token_id' => ''
        ]
    ],

    'binancechain' => [

        'merchant' => [
            'password' => ''
        ],

        'pay' => [
            'private_key' => '',
            'main_address' => ''
        ],

        'options' => [
            'exchange_fee' => '0.000375'
        ]
    ],


    'tron' => [

        'merchant' => [
            'private_key' => '',
            'main_address' => ''
        ],

        'pay' => [
            'private_key' => '',
            'main_address' => ''
        ]
    ],


    'ripple' => [

        'merchant' => [
            'main_address' => '',
            'private_key' => ''
        ],

        'pay' => [
            'private_key' => '',
            'main_address' => ''
        ]
    ],

    'stellar' => [

        'merchant' => [
            'main_address' => '',
            'private_key' => ''
        ],

        'pay' => [
            'private_key' => '',
            'main_address' => ''
        ]
    ],

    'rpc' => [

        'merchant' => [
            'rpc_host' => '',
            'rpc_port' => '',
            'rpc_username' => '',
            'rpc_password' => ''
        ],

        'pay' => [
            'rpc_host' => '',
            'rpc_port' => '',
            'rpc_username' => '',
            'rpc_password' => ''
        ]
    ],

    'paypal' => [
        'merchant' => [
            'login_account' => ''
        ]
    ],

    'paykassa' => [

        'merchant' => [
            'id_shop' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'id_shop' => '',
            'secret_key' => '',
            'api_id' => '',
            'api_secret' => ''
        ]
    ],

    'liqpay' => [
        'merchant' => [
            'public_key' => '',
            'private_key' => ''
        ]
    ],

    'binance' => [

        'merchant' => [
            'api_key' => '',
            'secret_key' => ''
        ],


        'pay' => [
            'api_key' => '',
            'secret_key' => ''
        ]
    ],


    'billline' => [

        'merchant' => [
            'merchant_id' => '',
            'secret_key' => ''
        ],


        'pay' => [
            'merchant_id' => '',
            'secret_key' => ''
        ]
    ],

    'alikassa' => [
        'merchant' => [
            'account_email' => '',
            'merchant_uuid' => '',
            'secret_key' => ''
        ]
    ],

    'stripe' => [
        'merchant' => [
            'api_key' => '',
            'public_key' => ''
        ]
    ],

    'kunacodes' => [
        'merchant' => [
            'public_key' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'public_key' => '',
            'secret_key' => ''
        ]
    ],

    'exmogiftcard' => [

        'merchant' => [
            'public_key' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'public_key' => '',
            'secret_key' => ''
        ]
    ],

    'kunapay' => [

        'merchant' => [
            'public_key' => '',
            'private_key' => ''
        ],

        'pay' => [
            'public_key' => '',
            'private_key' => ''
        ]
    ],

    'cryptoprocessing' => [
        'merchant' => [
            'api_key' => '',
            'api_secret' => ''
        ],
        'pay' => [
            'api_key' => '',
            'api_secret' => ''
        ]
    ],

    'changecoins' => [
        'merchant' => [
            'api_key' => '',
            'api_secret' => ''
        ],
        'pay' => [
            'api_key' => '',
            'api_secret' => ''
        ]
    ],

    'firekassa' => [
        'merchant' => [
            'site_url' => '',
            'secret_key' => '',
            'site_name' => ''
        ],
        'pay' => [
            'site_url' => '',
            'secret_key' => '',
            'site_name' => ''
        ]
    ],

    'firekassawallet' => [
        'merchant' => [
            'site_url' => '',
            'secret_key' => '',
            'site_name' => ''
        ]
    ],

    'privatbank' => [
        'pay' => [
            'merchant_id' => '',
            'password' => '',
            'card' => ''
        ]
    ],

    'yoomoney' => [
        'merchant' => [
            'account_id' => '',
            'password' => '',
            'client_id' => '',
            'client_secret' => ''
        ],

        'pay' => [
            'access_token' => '',
            'account_id' => '',
            'client_id' => '',
            'client_secret' => ''
        ]
    ],

    /**
     * FixedFloat
     */
    'fixedfloat' => [
        'merchant' => [
            'api_key' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'api_key' => '',
            'secret_key' => ''
        ]
    ],

    /**
     * Garantex
     */
    'garantex' => [
        'merchant' => [
            'private_key' => '',
            'uuid' => ''
        ],

        'pay' => [
            'private_key' => '',
            'uuid' => ''
        ]
    ],

    /**
     * Rapira
     */
    'rapira' => [
        'merchant' => [
            'private_key' => '',
            'uuid' => ''
        ],

//        'pay' => [
//            'private_key' => '',
//            'uuid' => ''
//        ]
    ],

    /**
     * cryptomus
     */
    'cryptomus' => [
        'merchant' => [
            'merchant_id' => '',
            'secret_key' => ''
        ],

        'pay' => [
            'merchant_id' => '',
            'secret_key' => '',
            'payout_key' => ''
        ]
    ],
    /**
     * cryptomus
     */
    'iexrequisites' => [
        'merchant' => [
            'site_url' => ''
        ]
    ],

    'bitconce' => [
        'merchant' => [
            'token_merchant' => ''
        ],
        'pay' => [
            'token_merchant' => '',
            'token_pay' => ''
        ]
    ],

    'payport' => [
        'merchant' => [
            'merchant_id' => '',
            'api_key' => '',
            'api_key3' => '',
            'user_url' => ''
        ],
        'pay' => [
            'api_key' => '',
            'api_key3' => '',
            'user_url' => ''
        ]
    ],

    'cryptoswap' => [
        'merchant' => [
            'token_key' => '',
        ],
        'pay' => [
            'token_key' => '',
        ]
    ],


    'payone' => [
        'merchant' => [
            'api_host' => '',
            'api_ip' => '',
            'api_port' => ''
        ],
        'pay' => [
            'api_host' => '',
            'api_ip' => '',
            'api_port' => ''
        ]
    ],

    'internalaccount' => [
        'merchant' => [
        ],
        'pay' => [

        ]
    ],
];
