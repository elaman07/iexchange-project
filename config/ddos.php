<?php

return [
    'services' => [
        'cloudflare',
        'stormwall',
    ],

    'proxies' => [
        'stormwall' => [
            '185.121.240.0/22',
            '188.0.150.0/24',
            '193.84.78.0/24'
        ]
    ]
];
