<?php


use Illuminate\Support\Facades\Facade;
use Illuminate\Support\ServiceProvider;

return [

    'developer_role' => 'Разработчик',

    'force_https' => env('FORCE_HTTPS'),

    'debug_blacklist' => [
        '_COOKIE' => array_keys($_COOKIE),
        '_SERVER' => array_keys($_SERVER),
        '_ENV' => array_keys($_ENV),
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */
    'name' => env('APP_NAME', 'Sitename'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */
    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */
    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL', null),

    // Включение и отключени SSL
    'enable_ssl' => env('ENABLE_SSL', true),

    // Текущий статус ошибок
    'error_reporting' => env('ERROR_REPORTING', -1),

    // Список возможных ошибок
    'error_reporting_list' => [
        0 => 'Все включено',
        1 => 'Только ошибки',
        2 => 'Ошибки и предупреждения',
        3 => 'Не выводить (Не рекомендуется)'
    ],

    'form_lang' => [
        'ru' => [
            'text' => 'ckeditor',
            'name' => 'RU',
            'title' => 'Русский',
            'img' => 'ru.png',
            'active' => true,
            'field' => '',
            'ckeditor_id' => 1
        ],

        'en' => [
            'text' => 'ckeditor2',
            'name' => 'EN',
            'title' => 'English',
            'img' => 'en.png',
            'active' => false,
            'field' => '_en',
            'ckeditor_id' => 2
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => env('APP_TIMEZONE','Europe/Moscow'),

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => env('APP_LOCALE', 'ru'),

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'ru',

    /*
   |--------------------------------------------------------------------------
   | Faker Locale
   |--------------------------------------------------------------------------
   |
   | This locale will be used by the Faker PHP library when generating fake
   | data for your database seeds. For example, this will be used to get
   | localized telephone numbers, street address information and more.
   |
   */
    'faker_locale' => 'en_US',

    /*
   |--------------------------------------------------------------------------
   | Список доступных языков
   |--------------------------------------------------------------------------
   |
   | Список все доступных языков который будет использоваться
    | поставщиком услуг перевода. Вы можете установить это значение
    | на любой из районов, которые будут поддерживаться приложением.
   |
    */
    'all_locale' =>  [
        'ru'    =>  'Русский',
        'en'    =>  'English'
    ],

    'locale_codes' => ['ru', 'en'],

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Maintenance Mode Driver
    |--------------------------------------------------------------------------
    |
    | These configuration options determine the driver used to determine and
    | manage Laravel's "maintenance mode" status. The "cache" driver will
    | allow maintenance mode to be controlled across multiple machines.
    |
    | Supported drivers: "file", "cache"
    |
    */

    'maintenance' => [
        'driver' => 'file',
        // 'store'  => 'redis',
    ],

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => ServiceProvider::defaultProviders()->merge([
        \App\Common\Packages\ReferralSystem\ReferralSystemServiceProvider::class,


        \Cog\Laravel\Ban\Providers\BanServiceProvider::class,
        //\SocialiteProviders\Manager\ServiceProvider::class, // add
        /*
         * Package Service Providers...
         */
        Intervention\Image\ImageServiceProvider::class,
        Bugsnag\BugsnagLaravel\BugsnagServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\TelescopeServiceProvider::class,
        App\Providers\ConfigServiceProvider::class,
        App\Providers\HorizonServiceProvider::class
    ])->toArray(),

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => Facade::defaultAliases()->merge([
        'IRedis' => Illuminate\Support\Facades\Redis::class,
        'Image'         => Intervention\Image\Facades\Image::class,
        'DotenvEditor'  => Brotzka\DotenvEditor\DotenvEditorFacade::class,
        'Pages'         => App\Common\Packages\Pages\Pages::class,
        'Analytics'     => Spatie\Analytics\AnalyticsFacade::class,
        'Google2FA'     => PragmaRX\Google2FALaravel\Facade::class,
        'Crypto'    =>  App\Common\Packages\Crypto\Facades\CryptoFacade::class,
        'Converter' =>  App\Common\Packages\Converter\Facades\ConvertFacade::class,
        'Payment'   =>  App\Common\Packages\Payment\PaymentFacade::class,
        'Cashback'  =>  App\Common\Packages\Cashback\Facades\CashbackFacade::class,
        'ReferralSystem'  =>  App\Common\Packages\ReferralSystem\ReferralSystemFacade::class,
        'GeoIP'     =>  App\Common\Packages\GeoIP\Facades\GeoIP::class,
        'Transaction'   =>  \App\Common\Packages\Transaction\Facades\TransactionFacade::class,
        'Payout'        =>  \App\Common\Packages\Payout\Facades\PayoutFacade::class,
        'CryptoCompiler'    =>  \App\Common\Packages\Crypto\Facades\CompilerFacade::class,
        'XSSCleaner' => \App\Common\Packages\XSSFilter\XSSCleaner::class,
        'Timezones' =>  \App\Common\Packages\Timezones\Facades\TimezonesFacade::class,
        'Luhn'      =>  \App\Common\Packages\Luhn\Facades\LuhnFacade::class,
        'Order'     =>  \App\Common\Packages\Order\Facades\OrderFacade::class,
        'Bugsnag'   => Bugsnag\BugsnagLaravel\Facades\Bugsnag::class,
        'Telegram' => Telegram\Bot\Laravel\Facades\Telegram::class,
        'Security' => GrahamCampbell\Security\Facades\Security::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'BackupCode' => \App\Common\Packages\BackupCode\Facades\BackupCodeFacade::class,
        'Gravatar' => Creativeorange\Gravatar\Facades\Gravatar::class,
        'Angular'   =>  \App\Common\Packages\Angular\AngularFacade::class,
        'UpdateClient' => \App\Common\Packages\Update\Facades\UpdateClientFacade::class,
        'X19' => \App\Common\Packages\X19\Facades\X19Facade::class,
        'AMLBotFacade' => \Modules\AMLBot\AMLBotFacade::class,
        'GetBlockBotFacade' => \Modules\GetBlockBot\GetBlockBotFacade::class,
        'Module' => \Nwidart\Modules\Facades\Module::class,
        'HCaptcha' => \App\Common\Packages\HCaptcha\Facades\HCaptcha::class,
        'ImageSystem' => \App\Common\Packages\ImageSystem\Facades\ImageSystemFacade::class
    ])->toArray(),

    'cms' => 'iEXExchanger'
];
