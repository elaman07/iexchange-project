<?php
return [

    /*
    |--------------------------------------------------------------------------
    | iEXExchanger - License key
    |--------------------------------------------------------------------------
    |
    | Это UUID Ключ, который необходим для активации лицензии на продукт
    |
    | Ключ указывается перед установкой iEXExchanger
    | У ключа нет никаких ограничений
    |
    */
    'license_key' => env('LICENSE_KEY', null),


    /*
    |--------------------------------------------------------------------------
    | iEXExchanger - License data
    |--------------------------------------------------------------------------
    |
    | Это Data Ключ, который также необходим для активации лицензии на продукт
    |
    | Ключ указывается перед установкой iEXExchanger
    | У ключа нет никаких ограничений
    |
    */
    'license_data' => env('LICENSE_DATA', null),

    /*
    |--------------------------------------------------------------------------
    | iEXExchanger - ID Кэширования
    |--------------------------------------------------------------------------
    |
    | Сбрасывает кэширование
    |
    | Внимание: Если незнаете для чего это значение, не трогайте поле во избежаения
    | конфликтов в исходном коде продукта
    |
    */
    'license_cache_id' => 1,


    /*
    |--------------------------------------------------------------------------
    | iEXExchanger - Version key
    |--------------------------------------------------------------------------
    |
    | Это версия лицензионного ключа
    |
    | Внимание: Если незнаете для чего это значение, не трогайте поле во избежаения
    | конфликтов в исходном коде продукта
    |
    */
    'license_version' => 1,

    /*
    |--------------------------------------------------------------------------
    | iEXExchanger - License domain
    |--------------------------------------------------------------------------
    |
    | Здесь хранится список лицензированных доменов
    |
    | Внимание: Если незнаете для чего это значение, не трогайте поле во избежаения
    | конфликтов в исходном коде продукта
    |
    */
    'license_domain' => null,


    'update_site_proxy_addr' => env('UPDATE_SITE_PROXY_ADDR', null),

    'update_site_proxy_port' => env('UPDATE_SITE_PROXY_PORT', null),

    'update_site_proxy_user' => env('UPDATE_SITE_PROXY_USER', null),

    'update_site_proxy_pass' => env('UPDATE_SITE_PROXY_PASS', null),

    'stable_versions_only' => env('STABLE_VERSIONS_ONLY', null),

    'update_autocheck' => env('UPDATE_AUTOCHECK', null),

    'update_stop_autocheck' => env('UPDATE_STOP_AUTOCHECK', null),
];
