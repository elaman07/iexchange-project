<?php
return [
    'currencies' => [
        'instruction' => [
            "<ul>
            <li>[city] - город</li>
            <li>[country] - страна</li>
            <li>[direction] - направление</li>
            <li>[created_at] - дата создания заявки</li>
            <li>[rate] - курс обмена</li>
            <li>[in_amount] - сумма отдаю</li>
            <li>[out_amount] - сумма получаю</li>
            <li>[in_code] - код отдаю</li>
            <li>[out_code] - код получаю</li>
            <li>[in_currency] - валюта отдаю</li>
            <li>[out_currency] - валюта получаю</li>
            <li>[public_id] - Public ID</li>
            <li>[order_id] - ID Заявки</li>
            <li>[profit_percent] - Процент прибыли</li>
        </ul>"
        ]
    ]
];
