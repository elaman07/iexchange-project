<?php

/***
 * Конфигурационный файл который отвечает за компонент "Crypto"
 */
return [

    'hash_order' => 'ripemd128',

    'merchant_hash' => env('MERCHANT_HASH', null),

    /*
     |--------------------------------------------------------------------------
     | Валюта для cashback и реферальной системы
     |--------------------------------------------------------------------------
     |
     | Если включено то собираем отправляем запрос на GEO,
     | для получения личной информации
     |
     */
    'currency_id' => env('PAYOUT_CURRENCY_ID', 1),
    'currency_payout' => env('PAYOUT_CURRENCY_NAME', 'RUB'),
    'currency_payout_sign' => env('PAYOUT_CURRENCY_SIGN', 'руб'),
    'currency_payout_sign_alt' => env('PAYOUT_CURRENCY_SIGN_ALT', '₽'),
    'currency_payout_position' => env('PAYOUT_CURRENCY_POSITION', 'right'),

    /*
    |--------------------------------------------------------------------------
    | Собирать личные данные при авторизации
    |--------------------------------------------------------------------------
    |
    | Если включено то собираем отправляем запрос на GEO,
    | для получения личной информации
    |
    */
    'geo_ip_auth'   =>  env('GEO_IP_AUTH', true),

    'is_order_device' => env('IS_ORDER_DEVICE', false),

    /*
    |--------------------------------------------------------------------------
    | Панель оператора
    |--------------------------------------------------------------------------
    |
    | Если 'Включено', то операторы могут обрабатывать заявки в новом панели
    |
    */
    'panel_operator'  => env('ENABLE_PANEL_OPERATOR'),

    // Номера счетов которые нельзя добавить в раздел "Платежные реквизиты"
    'requisites'    =>  [
        'blacklist'    =>  [
            ''
        ]
    ],

    /*
     |--------------------------------------------------------------------------
     | Список валидаторов
     |--------------------------------------------------------------------------
     |
     | Здесь вы можете найти список всех доступных валидаторов для
     | проверки криптовалютных адресов на корректность
     |
     */
    'validators_support'    =>  [
        'yandexmoney'       =>  'Яндекс.Деньги',
        'luhn'              =>  'Банковские карты',
        'phone'             =>  'Номер телефона',
        'bitcoin'           =>  'Bitcoin',
        'bitcoincash'       =>  'Bitcoin Cash',
        'ethereum'          =>  'Ethereum',
        'ethereumtoken'     =>  'Ethereum Token',
        'litecoin'          =>  'Litecoin',
        'dash'              =>  'Dash',
        'zcash'             =>  'Zcash',
        'tron'              =>  'Tron',
        'trontoken'         =>  'Tron Token',
        'stellar'           =>  'Stellar',
        'bitcoingold'       =>  'Bitcoin Gold',
        'cardano'           =>  'Cardano',
        'dogecoin'          =>  'Dogecoin',
        'eos'               =>  'EOS',
        'ethereumclassic'   => 'Ethereum Classic',
        'monero'            =>  'Monero',
        'ripple'            => 'Ripple',
        'solana'            => 'Solana',
        'tezos'             => 'Tezos',
        'binancebep2'       => 'Binance BEP2',
        'binancebep20'      => 'Binance BEP20',
        'atom'              => 'Cosmos (ATOM)',
        'polkadot'          => 'Polkadot',
        'avax'              => 'AVAX',
        'near'              => 'NEAR'
    ],

    'sms_driver' => env('SMS_DRIVER', 'nexmo'),


    /*
    |--------------------------------------------------------------------------
    | Версия приложения
    |--------------------------------------------------------------------------
    |
    | Устанавливаем актуальную версию приложения
    |
    */
    'version_update'    =>  '8.0.5',
    'version_date'      =>  \Carbon\Carbon::parse('26-01-2024 20:55')->timestamp,
    'full_version'      =>  '8.0.5'.\Carbon\Carbon::parse('26-01-2024 16:43')->timestamp,

    /*
    |--------------------------------------------------------------------------
    | Настройка компилятора
    |--------------------------------------------------------------------------
    |
    | Все необходимые настройки для генерации файла направление обменов
    | Название файла, тип файла ...
    |
    */
    'compiler'  =>  [
        'version'                   =>     'v8.0.5',
        'path'                      =>     public_path('/uploads/rates.json'),
        'cache_path'                =>     public_path('/uploads/cs.json'),
        'redis_prefix'              =>     'iex-rates'
    ]
];
