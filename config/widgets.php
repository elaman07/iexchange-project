<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Виджеты для панели управления
    |--------------------------------------------------------------------------
    |
    | Это сгруппированный список виджетов для главной страницы в панели управления
    |
    */
    'admin-widgets' => [
        [
            'name' => 'Обменник',
            'items' => [

                [
                    'id' => 'd6854415799d60428342c344042dbb8f',
                    'name' => 'Избранные направления',
                    'alias' => 'AdminFavoritesDirection'
                ],

                [
                    'id' => 'b5gjcdwf8jlb3vhsaxl7zs4iwr3wa31b',
                    'name' => 'Поиск заявок',
                    'alias' => 'OrderIdWidget'
                ],

                [
                    'id' => '1ac78d52264d1d1fb4df65b5f1d36532',
                    'name' => 'Последние события по резервам',
                    'alias' => 'AdminEventReserve'
                ],

                [
                    'id' => '5cde01d3fd5c506c0cad3b904de908fe',
                    'name' => 'Топ 3 партнера',
                    'alias' => 'AdminTopPartners'
                ],

                [
                    'id' => 'f1bdnbb0wgrhsh774zi4i8qp3iwfje',
                    'name' => 'Активные резервы',
                    'alias' => 'AdminEnabledReserves'
                ],

                [
                    'id' => '9ni4glkobr25wehhd03bpskqivis28',
                    'name' => 'Суммарный резерв',
                    'alias' => 'AdminCommonReserve'
                ],

                [
                    'id' => 'tqthpmretk0gobtsk1esf4vfel073x',
                    'name' => 'Топ постоянных клиентов',
                    'alias' => 'AdminBuyerClient'
                ],

                [
                    'id' => 'wpife3zby4wlh68j8trfmkf7npyjzmha',
                    'name' => 'Суммарные суммы обменов',
                    'alias' => 'AdminCurrencyExchange'
                ],
                [
                    'id' => '5cde01d3fd5c506c0c31fb904de908fe',
                    'name' => 'Статистика парсинга',
                    'alias' => 'CryptoParserWidget'
                ],
                [
                    'id' => '1btof6gj1nlarwncprtbkigyo07c0o5',
                    'name' => 'VIP Клиенты',
                    'alias' => 'VipClientWidget'
                ],

                [
                    'id' => 'vxmeohrgkk4j72a272260fpd815fwo6h',
                    'name' => 'Статистика прибыли',
                    'alias' => 'OrderProfitWidget'
                ],

                [
                    'id' => 'n2kf8pw2l1e51lexsbi4qux6q42ye1zv',
                    'name' => 'Мерчанты',
                    'alias' => 'AdminMerchant'
                ],

                [
                    'id' => 'ab40pfxzvmi2xpqzn0x1dax3xfj6qbyb',
                    'name' => 'Статус операторов',
                    'alias' => 'AdminStatusOperator'
                ]
            ]
        ],

        [
            'name' => 'Веб-Аналитика',
            'items' => [
                [
                    'id' => 'lvklog4oogcj564fbnugfk4nreow2n8w',
                    'name' => 'Счетчики',
                    'alias' => 'AdminHeaderStat'
                ],
                [
                    'id' => 'ae531cc8e198faa1fd39506343e758ae',
                    'name' => 'Статистика',
                    'alias' => 'AdminStatistics'
                ],

                [
                    'id' => '4e4e0f79ef5dd32272ce1e7e71c6e820',
                    'name' => 'Общие суммы обменов',
                    'alias' => 'AdminTotalExchange'
                ],

                [
                    'id' => 'p2tk7nxxvak6fkrv5zkjl6i9wdhn32',
                    'name' => 'Статистика валют',
                    'alias' => 'AdminCurrencyStat'
                ],[
                    'id' => 'bhpb5unlnpexwbbq8rwk0cm7qa7j242',
                    'name' => 'Активность клиентов',
                    'alias' => 'ActivityClient'
                ], [
                    'id' => 'rvdeeymyrypnznjwbxvagbf',
                    'name' => 'Средний курс заявок по направлениям',
                    'alias' => 'AvgDirectionCourse'
                ],
            ]
        ],

        [
            'name' => 'Пользователи',
            'items' => [
                [
                    'id' => 'c3024cb0f2ca3632cdus897394ed8df8',
                    'name' => 'Статистика пользователей',
                    'alias' => 'AdminUserStat'
                ],

                [
                    'id' => 'c3024cb0f2ca3632cdac897394ed8df8',
                    'name' => 'Активность пользователей',
                    'alias' => 'AdminUserActivity'
                ]
            ]
        ],

        [
            'name' => 'Настройки',
            'items' => [
                [
                    'id' => 'c3024cb0f2ca3632cdff897394ed8df8',
                    'name' => 'Пользователи в панели управления',
                    'alias' => 'AdminOnline'
                ],

                [
                    'id' => 's2454db0f5ca3632cdff897394edsd11',
                    'name' => 'Cache control',
                    'alias' => 'AdminCacheControl'
                ],

                [
                    'id' => 'e13fdb2f43be9cb0c887daa5407b60c9',
                    'name' => 'О системе',
                    'alias' => 'AboutProduct'
                ]
            ]
        ],


        [
            'name' => 'Логи',
            'items' => [
                [
                    'id' => 'c3024cb0f2ca135da1ff897394ed8df8',
                    'name' => 'Лог ошибок направлений',
                    'alias' => 'AdminDirectionLogWidget'
                ]
            ]
        ],[
            'name' => 'Прочие',
            'items' => [
                [
                    'id' => 'xqrbxmt2afq2f5zyol96gjt78kxhlah',
                    'name' => 'Новости от iEXExchanger',
                    'alias' => 'IEXNewsWidget'
                ],
                [
                    'id' => 'nl0j7isg7y8zjcmofhcsmehhghf30c04',
                    'name' => 'Анализ сайта',
                    'alias' => 'PerfmonPanelWidget'
                ],
                [
                    'id' => '1ab032v26hes4411v6o0pfuyeacaf2e1',
                    'name' => 'IP Адрес сервера',
                    'alias' => 'ServerIpWidget'
                ]
            ]
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Все доступные виджеты
    |--------------------------------------------------------------------------
    |
    | Здесь находится список всех существующих виджетов.
    |
    */
    'available' => [
        'd6854415799d60428342c344042dbb8f' => [
            'name' => 'Избранные направления',
            'alias' => 'AdminFavoritesDirection'
        ],

        'ae531cc8e198faa1fd39506343e758ae' => [
            'name' => 'Статистика',
            'alias' => 'AdminStatistics'
        ],

        '1ac78d52264d1d1fb4df65b5f1d36532' => [
            'name' => 'Последние события по резервам',
            'alias' => 'AdminEventReserve'
        ],

        '4e4e0f79ef5dd32272ce1e7e71c6e820' => [
            'name' => 'Общие суммы обменов',
            'alias' => 'AdminTotalExchange'
        ],

        'e13fdb2f43be9cb0c887daa5407b60c9' => [
            'name' => 'О системе',
            'alias' => 'AboutProduct'
        ],

        'c3024cb0f2ca3632cdff897394ed8df8' => [
            'name' => 'Пользователи в панели управления',
            'alias' => 'AdminOnline'
        ],

        '5cde01d3fd5c506c0cad3b904de908fe' => [
            'name' => 'Топ 3 партнера',
            'alias' => 'AdminTopPartners'
        ],

        'f1bdnbb0wgrhsh774zi4i8qp3iwfje' => [
            'name' => 'Активные резервы',
            'alias' => 'AdminEnabledReserves'
        ],

        '9ni4glkobr25wehhd03bpskqivis28' => [
            'name' => 'Суммарный резерв',
            'alias' => 'AdminCommonReserve'
        ],

        'p2tk7nxxvak6fkrv5zkjl6i9wdhn32' => [
            'name' => 'Статистика валют',
            'alias' => 'AdminCurrencyStat'
        ],

        'tqthpmretk0gobtsk1esf4vfel073x' => [
            'name' => 'Топ постоянных клиентов',
            'alias' => 'AdminBuyerClient'
        ],

        'wpife3zby4wlh68j8trfmkf7npyjzmha' => [
            'name' => 'Суммарные суммы обменов',
            'alias' => 'AdminCurrencyExchange',
            'inputs' => [
                [
                    'key' => 'wa_currency_exchange_type',
                    'type' => 'select',
                    'name' => 'Выберите тип отображения суммы обменов',
                    'list'  => [
                        ['key' => 0, 'name' => 'По актуальным курсам'],
                        ['key' => 1, 'name' => 'По фиксированному курсу']
                    ]
                ]
            ],

            'params' => [
                'wa_currency_exchange_type'
            ]
        ],

        '5cde01d3fd5c506c0c31fb904de908fe' => [
            'name' => 'Статистика парсинга',
            'alias' => 'CryptoParserWidget'
        ],

        'c3024cb0f2ca135da1ff897394ed8df8' => [
            'name' => 'Лог ошибок направлений',
            'alias' => 'AdminDirectionLogWidget'
        ],
        's2454db0f5ca3632cdff897394edsd11' => [
            'name' => 'Cache Control',
            'alias' => 'AdminCacheControl'
        ],

        '1btof6gj1nlarwncprtbkigyo07c0o5' => [
            'name' => 'VIP Клиенты',
            'alias' => 'VipClientWidget'
        ],

        'xqrbxmt2afq2f5zyol96gjt78kxhlah' => [
            'name' => 'Новости от iEXExchanger',
            'alias' => 'IEXNewsWidget'
        ],


        'bhpb5unlnpexwbbq8rwk0cm7qa7j242' => [
            'name' => 'Активность клиентов',
            'alias' => 'ActivityClient'
        ],

        'vxmeohrgkk4j72a272260fpd815fwo6h' => [
            'name' => 'Статистика прибыли',
            'alias' => 'OrderProfitWidget'
        ],

        'b5gjcdwf8jlb3vhsaxl7zs4iwr3wa31b' => [
            'name' => 'Поиск заявок',
            'alias' => 'OrderIdWidget'
        ],

        'n2kf8pw2l1e51lexsbi4qux6q42ye1zv' => [
            'name' => 'Мерчанты',
            'alias' => 'AdminMerchant'
        ],

        'lvklog4oogcj564fbnugfk4nreow2n8w' => [
            'name' => 'Счетчики',
            'alias' => 'AdminHeaderStat'
        ],

        'ab40pfxzvmi2xpqzn0x1dax3xfj6qbyb' => [
            'name' => 'Статус операторов',
            'alias' => 'AdminStatusOperator'
        ],

        'nl0j7isg7y8zjcmofhcsmehhghf30c04' => [
            'name' => 'Анализ сайта',
            'alias' => 'PerfmonPanelWidget'
        ],

        'rvdeeymyrypnznjwbxvagbf' => [
            'name' => 'Средний курс заявок по направлениям',
            'alias' => 'AvgDirectionCourse'
        ],

        '1ab032v26hes4411v6o0pfuyeacaf2e1' => [
            'name' => 'IP Адрес сервера',
            'alias' => 'ServerIpWidget'
        ],

        'c3024cb0f2ca3632cdus897394ed8df8' => [
            'name' => 'Статистика пользователей',
            'alias' => 'AdminUserStat'
        ],
        'c3024cb0f2ca3632cdac897394ed8df8' => [
            'name' => 'Активность пользователей',
            'alias' => 'AdminUserActivity'
        ]
    ]
];
